﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlRemoveData_2A.ascx.cs" Inherits="SmartsVer1.Control.Services.ctrlRemoveData_2A" %>
<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>

<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript for ref lib -->
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>
<!--- local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }

    .rotate {
        -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
        -moz-transform: rotate(90deg); /* Firefox */
        -ms-transform: rotate(90deg); /* IE 9 */
        transform: rotate(90deg); /* Standard syntax */
    }
    /*custom anchor defi for menu items*/
    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }    
</style>
<!--- Page Menu -->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!-- Breadcrumb -->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../Viewer/fesViewer.aspx"><i class="fa fa-dashboard"></i> SMARTs Home</a></li>
                <li><a>SMARTs</a></li>
                <li><a href="../../Services/fecRemoveData.aspx">Remove Data</a></li>
            </ol>
        </div>
    </div>
</div>
<!--- Page Content --->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <asp:UpdateProgress ID="uprgsRD" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlRD">
                        <ProgressTemplate>
                            <div id="divRDProcessing" runat="server" class="updateProgress">
                                <asp:Image ID="imgRDProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlRD">
                        <ContentTemplate>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="input-group">
                                        <span class="input-group-addon bg-light-blue">Service Type</span>
                                        <asp:DropDownList ID="ddlSrvType" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                            OnSelectedIndexChanged="ddlSrvType_SelectedIndexChanged">
                                            <asp:ListItem Value="0" Text="--- Select Service Type ---" />
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="input-group">
                                        <span class="input-group-addon bg-light-blue">Operator</span>
                                        <asp:DropDownList ID="ddlOpr" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control" Enabled="false"
                                            OnSelectedIndexChanged="ddlOpr_SelectedIndexChanged">
                                            <asp:ListItem Value="0" Text="--- Select Global Operator ---" />
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="input-group">
                                        <span class="input-group-addon bg-light-blue">Well Pad</span>
                                        <asp:DropDownList ID="ddlPads" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                            Enabled="false" OnSelectedIndexChanged="ddlPads_SelectedIndexChanged">
                                            <asp:ListItem Value="0" Text="--- Select Well Pad ---" />
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="input-group">
                                        <span class="input-group-addon bg-light-blue">Well/Lateral</span>
                                        <asp:DropDownList ID="ddlWell" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                            Enabled="False" OnSelectedIndexChanged="ddlWell_SelectedIndexChanged">
                                            <asp:ListItem Value="0" Text="--- Select Well / Lateral ---" />
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="input-group">
                                        <span class="input-group-addon bg-light-blue">Well Section</span>
                                        <asp:DropDownList ID="ddlWellSection" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                            Enabled="False" OnSelectedIndexChanged="ddlWellSection_SelectedIndexChanged">
                                            <asp:ListItem Value="0" Text="--- Select Well Section ---" />
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="input-group">
                                        <span class="input-group-addon bg-light-blue">Run</span>
                                        <asp:DropDownList ID="ddlRun" runat="server" AutoPostBack="true" AppendDataBoundItems="true" CssClass="form-control"
                                            Enabled="false" OnSelectedIndexChanged="ddlRun_SelectedIndexChanged">
                                            <asp:ListItem Value="0" Text="--- Select Run ---" />
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="text-right">
                                        <asp:LinkButton ID="btnDeleteAll" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnDeleteAll_Click"><i class="fa fa-trash"> Delete All</i></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <asp:GridView runat="server" ID="gvwData" CssClass="mydatagrid" AllowPaging="True" AllowSorting="True"
                                        PageSize="15" AutoGenerateColumns="False" DataKeyNames="sfdID" Visible="False" SelectedRowStyle-BackColor="#CCFFCC"
                                        PagerStyle-CssClass="pager" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                        OnPageIndexChanging="gvwData_PageIndexChanging" EmptyDataText="No Data found for selected Run" ShowHeaderWhenEmpty="True"
                                        OnRowDeleting="gvwData_RowDeleting">
                                        <Columns>
                                            <asp:ButtonField CommandName="Delete" Text="Delete" ButtonType="Button">
                                                <ControlStyle CssClass="btn btn-warning text-center"></ControlStyle>
                                            </asp:ButtonField>
                                            <asp:BoundField DataField="sfdID" Visible="false" />
                                            <asp:BoundField DataField="srvyRowID" HeaderText="Survey No" />
                                            <asp:BoundField DataField="sfdDepth" HeaderText="Depth" />
                                            <asp:BoundField DataField="sfdGx" HeaderText="Gx" />
                                            <asp:BoundField DataField="sfdGy" HeaderText="Gy" />
                                            <asp:BoundField DataField="sfdGz" HeaderText="Gz" />
                                            <asp:BoundField DataField="sfdBx" HeaderText="Bx" />
                                            <asp:BoundField DataField="sfdBy" HeaderText="By" />
                                            <asp:BoundField DataField="sfdBz" HeaderText="Bz" />
                                            <asp:BoundField DataField="cTime" HeaderText="Created On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                            <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div runat="server" id="delEnclosure" visible="false">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <h4><i runat="server" id="iDel"><span runat="server" id="spnDel"></span></i></h4>
                                        <asp:Label ID="lblSuccess" runat="server" Text="" Visible="false" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="input-group" style="width: 100%;">
                                        <asp:Button ID="btnMetaCancel" runat="server" Text="Cancel Selection" CssClass="btn btn-warning text-center" Width="15%"
                                            OnClick="btnMetaCancel_Click" />
                                        <asp:Button ID="btnDetailCancel" runat="server" Text="Done" CssClass="btn btn-primary text-center" Width="15%" Visible="False"
                                            OnClick="btnDetailCancel_Click" />
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- /page content -->