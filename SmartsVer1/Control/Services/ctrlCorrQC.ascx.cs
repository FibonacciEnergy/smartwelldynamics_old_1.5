﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net.Mail;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartsVer1.Helpers.AccountHelpers;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using Corr = SmartsVer1.Helpers.QCHelpers.RawCorr;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using DL = SmartsVer1.Helpers.DemogHelpers.DataLoader;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using OPR = SmartsVer1.Helpers.QCHelpers.Opr;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using CHRT = SmartsVer1.Helpers.ChartHelpers.Gauges;
using UNT = SmartsVer1.Helpers.DemogHelpers.UnitsConverter;
using Color = System.Drawing.Color;
using log4net;

namespace SmartsVer1.Control.Services
{
    public partial class ctrlCorrQC : System.Web.UI.UserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ctrlCorrQC));
        const Int32 ClientTypID = 4;
        const Int32 ServiceID = 46;
        Int32 SysClientID = -99, regCount = 0, wellCount = 0, runCount = 0, mrmCount = 0, mbhaCount = 0;
        String LoginName, username, domain, GetClientDBString;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UserProfile usrP = UserProfile.GetUserProfile(Membership.GetUser().UserName);
                SysClientID = Convert.ToInt32(usrP.clntID);
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientTypID);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = 0;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(SysClientID, LoginName);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");

                Int32 sStatus = DMG.getServiceAvailability(ServiceID);
                if (sStatus.Equals(2))
                {}
                else
                {}
                this.createAssetCharts(null, null);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void createAssetCharts(object sender, EventArgs e)
        {
            try
            {
                List<Int32> wellsDetails = CHRT.getClientCorrQCSummary(GetClientDBString);
                regCount = wellsDetails[0];
                wellCount = wellsDetails[1];
                mrmCount = wellsDetails[2];
                runCount = wellsDetails[3];
                mbhaCount = wellsDetails[4];
                this.divGuageJob.InnerHtml = "<h3>" + regCount.ToString() + "</h3><span>Service Registration Requests</span>";
                this.divGuageWell.InnerHtml = "<h3>" + wellCount.ToString() + "</h3><span>In-Process Well(s)/Lateral(s)</span>";
                this.divAscWell.InnerHtml = "<h3>" + mrmCount.ToString() + "</h3><span>Missing Ref. Mags.</span>";
                this.divMRM.InnerHtml = "<h3>" + runCount.ToString() + "</h3><span>Active Run</span>";
                this.divMsngBHA.InnerHtml = "<h3>" + mbhaCount.ToString() + "</h3><span>Active Run(s) Missing BHA Signature</span>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwGlobalOptr_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(SysClientID);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = e.NewPageIndex;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
                this.btnMetaCancel.CssClass = "btn btn-warning text-center";
                this.btnMetaCancel.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwGlobalOptr_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow dRow = this.gvwGlobalOptr.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.ancHeading.InnerHtml = "<strong> " + oName + "</strong>";
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                System.Data.DataTable padTable = AST.getClientWellPadTableForOperator(oprID, SysClientID);
                this.gvwWellPads.DataSource = padTable;
                this.gvwWellPads.PageIndex = 0;
                this.gvwWellPads.SelectedIndex = -1;
                this.gvwWellPads.DataBind();
                this.divOptrList.Visible = false;
                this.divPad.Visible = true;
                this.btnMetaCancel.CssClass = "btn btn-warning text-center";
                this.btnMetaCancel.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellPads_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                System.Data.DataTable padTable = AST.getClientWellPadTableForOperator(oprID, SysClientID);
                this.gvwWellPads.DataSource = padTable;
                this.gvwWellPads.PageIndex = e.NewPageIndex;
                this.gvwWellPads.SelectedIndex = -1;
                this.gvwWellPads.DataBind();
                this.btnMetaCancel.CssClass = "btn btn-warning text-center";
                this.btnMetaCancel.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellPads_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow oRow = this.gvwGlobalOptr.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                GridViewRow pRow = this.gvwWellPads.SelectedRow;
                String pName = Convert.ToString(Server.HtmlDecode(pRow.Cells[2].Text));
                this.ancHeading.InnerHtml = "<strong> " + oName + " > " + pName + "</strong>";
                Int32 optrID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 pdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                System.Data.DataTable pdTable = AST.getMSAWellForWellPadTable(optrID, SysClientID, pdID, ServiceID, GetClientDBString);
                this.gvwWellList.DataSource = pdTable;
                this.gvwWellList.PageIndex = 0;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();
                this.gvwWellList.Visible = true;
                this.divOptrList.Visible = false;
                this.divPad.Visible = false;
                this.divWellList.Visible = true;
                this.btnMetaCancel.Enabled = true;
                this.btnMetaCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow oRow = this.gvwGlobalOptr.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                GridViewRow pRow = this.gvwWellPads.SelectedRow;
                String pName = Convert.ToString(Server.HtmlDecode(pRow.Cells[2].Text));
                GridViewRow wRow = this.gvwWellList.SelectedRow;
                String wName = Convert.ToString(Server.HtmlDecode(wRow.Cells[2].Text));
                this.ancHeading.InnerHtml = "<strong> " + oName + " > " + pName + " > " + wName + "</strong>";
                this.divOptrList.Visible = false;
                this.divPad.Visible = false;
                this.divWellList.Visible = false;
                this.divSectionList.Visible = true;
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                HttpContext.Current.Session["wellName"] = Convert.ToString(this.gvwWellList.SelectedRow.Cells[2].Text);
                System.Data.DataTable wsTable = OPR.getWellSectionToWellTable(wlID, GetClientDBString);
                this.gvwSectionList.DataSource = wsTable;
                this.gvwSectionList.DataBind();
                this.gvwSectionList.SelectedIndex = -1;
                this.divRunList.Visible = false;
                this.gvwRunList.DataBind();
                this.gvwRunList.SelectedIndex = -1;
                this.divDataEntryList.Visible = false;
                this.gvwDataEntryList.SelectedIndex = -1;
                this.gvwDataEntryList.DataBind();
                this.btnDetailCancel.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 pdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                System.Data.DataTable pdTable = AST.getWellForWellPadTable(pdID, GetClientDBString);
                this.gvwWellList.DataSource = pdTable;
                this.gvwWellList.PageIndex = e.NewPageIndex;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = ((DataRowView)e.Row.DataItem).Row;
                    Button selBtn = e.Row.Cells[0].Controls[0] as Button;
                    if (dr["wstID"].Equals("In Process"))
                    {
                        selBtn.Enabled = true;
                    }
                    else
                    {
                        selBtn.Enabled = false;
                        selBtn.CssClass = "btn btn-default text-center";
                    }
                    String stC = Convert.ToString(dr["stClr"]);
                    //String cD = Convert.ToString(dr["cdColor"]);
                    e.Row.Cells[7].BackColor = Color.FromName(stC.ToString());
                    //e.Row.Cells[10].BackColor = Color.FromName(cD);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSectionList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow oRow = this.gvwGlobalOptr.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                GridViewRow pRow = this.gvwWellPads.SelectedRow;
                String pName = Convert.ToString(Server.HtmlDecode(pRow.Cells[2].Text));
                GridViewRow wRow = this.gvwWellList.SelectedRow;
                String wName = Convert.ToString(Server.HtmlDecode(wRow.Cells[2].Text));                
                GridViewRow dRow = this.gvwSectionList.SelectedRow;
                String sName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.ancHeading.InnerHtml = "<strong> " + oName + " > " + pName + " > " + wName + " > " + sName + "</strong>";
                this.divOptrList.Visible = false;
                this.divPad.Visible = false;
                this.divWellList.Visible = false;
                this.divSectionList.Visible = false;
                this.divRunList.Visible = true;
                Int32 optrID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 wpdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 wsID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                System.Data.DataTable runList = OPR.getRunForWellSectionTable(optrID, wpdID, wlID, wsID, GetClientDBString);
                this.gvwRunList.DataSource = runList;
                this.gvwRunList.PageIndex = 0;
                this.gvwRunList.SelectedIndex = -1;
                this.gvwRunList.DataBind();                
                this.divDataEntryList.Visible = false;
                this.gvwDataEntryList.DataBind();                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSectionList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                System.Data.DataTable wsTable = OPR.WellSectionNameToWellTable(wlID, GetClientDBString);
                this.gvwSectionList.DataSource = wsTable;
                this.gvwSectionList.PageIndex = e.NewPageIndex;
                this.gvwSectionList.DataBind();
                this.gvwSectionList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRunList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow oRow = this.gvwGlobalOptr.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                GridViewRow pRow = this.gvwWellPads.SelectedRow;
                String pName = Convert.ToString(Server.HtmlDecode(pRow.Cells[2].Text));
                GridViewRow wRow = this.gvwWellList.SelectedRow;
                String wName = Convert.ToString(Server.HtmlDecode(wRow.Cells[2].Text));
                GridViewRow dRow = this.gvwSectionList.SelectedRow;
                String sName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                GridViewRow rRow = this.gvwRunList.SelectedRow;
                String rName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.ancHeading.InnerHtml = "<strong> " + oName + " > " + pName + " > " + wName + " > " + sName + " > " + rName + "</strong>";
                this.divRunList.Visible = false;
                this.divDataEntryList.Visible = true;
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 rID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                System.Data.DataTable demTable = AST.getDataEntryMethodsList();
                this.gvwDataEntryList.DataSource = demTable;
                this.gvwDataEntryList.DataBind();
                this.btnDetailCancel.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRunList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 wsID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                System.Data.DataTable runList = OPR.getRunForSelectedWellSectionTable(wlID, wsID, GetClientDBString);
                this.gvwRunList.DataSource = runList;
                this.gvwRunList.PageIndex = e.NewPageIndex;
                this.gvwRunList.DataBind();
                this.gvwRunList.Visible = true;
                this.gvwRunList.SelectedIndex = -1;
                this.gvwRunList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwDataEntryList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                if (this.gvwDataEntryList.SelectedValue.Equals(1))
                {
                    this.divDataEntryList.Visible = false;
                    this.divManData.Visible = true;
                    this.txtQCManualProcess.Text = "";
                    this.txtQCManualProcess.Focus();
                    this.btnMetaCancel.Visible = false;
                    this.btnCancelManualUpload.Visible = true;
                    this.btnCancelFileUpload.Visible = false;
                }
                else
                {
                    this.divDataEntryList.Visible = false;
                    this.divManData.Visible = false;
                    this.divFileData.Visible = true;
                    this.btnMetaCancel.Visible = false;
                    this.btnCancelManualUpload.Visible = false;
                    this.btnCancelFileUpload.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        protected void btnMetaCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.ancHeading.InnerHtml = "Operator Company";
                this.divOptrList.Visible = true;
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientTypID);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = 0;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
                this.divPad.Visible = false;
                this.gvwWellPads.DataSource = null;
                this.gvwWellPads.DataBind();
                this.divWellList.Visible = false;
                this.gvwWellList.DataBind();
                this.gvwWellList.SelectedIndex = -1;
                this.divSectionList.Visible = false;
                this.gvwSectionList.DataBind();
                this.gvwSectionList.SelectedIndex = -1;
                this.divRunList.Visible = false;
                this.gvwRunList.DataBind();
                this.gvwRunList.SelectedIndex = -1;
                this.divDataEntryList.Visible = false;
                this.gvwDataEntryList.DataBind();
                this.gvwDataEntryList.SelectedIndex = -1;
                this.btnMetaCancel.Enabled = false;
                this.btnMetaCancel.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCancelFileUpload_Click(object sender, EventArgs e)
        {
            try
            {                                
                this.FileUploadControl.Dispose();                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        


        protected void btnQCManualUpload_Click(object sender, EventArgs e)
        {
            try
            {
            // Variables and initializing them
            System.Data.DataTable iResult = new System.Data.DataTable(), headTable = new System.Data.DataTable();
            List<System.Data.DataTable> dataTables = new List<System.Data.DataTable>();
            Int32 jD = -99, wD = -99, RunID = -99, sqcID = -99;            
            String dataRowText = Convert.ToString(this.txtQCManualProcess.Text);
            
                if (String.IsNullOrEmpty(dataRowText))
                {
                    this.lblManual.Text = " Please enter Raw Survey Data row before proceeding";
                    this.lblManual.Visible = true;
                    this.lblManual.ForeColor = Color.Red;
                    this.txtQCManualProcess.Text = "";

                    this.txtQCManualProcess.Focus();

                }
                else    // Else Block if the text box is not empty
                {
                    //iResult = DL.uploadManualRawDataRow(ServiceID, jD, wD, RunID, sqcID, dataRowText, GetClientDBString);
                    //dataTables.Add(headTable);
                    //dataTables.Add(iResult);
                    //HttpContext.Current.Session["rawDataTables"] = dataTables;
                    //HttpContext.Current.Session["dataEntryMethod"] = 1; //Manual Data Entry
                    //this.divManData.Visible = false;
                    //this.gvwViewManualDisplay.DataSource = iResult;
                    //this.gvwViewManualDisplay.DataBind();
                    //this.gvwViewManualDisplay.Focus();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwViewManualDisplay_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                List<System.Data.DataTable> dataTables = new List<System.Data.DataTable>();
                dataTables = (List<System.Data.DataTable>)HttpContext.Current.Session["rawDataTables"];
                System.Data.DataTable rawTable = dataTables[1];
                this.gvwViewManualDisplay.DataSource = rawTable;
                this.gvwViewManualDisplay.PageIndex = e.NewPageIndex;
                this.gvwViewManualDisplay.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCancelManualUpload_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtQCManualProcess.Text = null;                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btn_FileUpload_Click(object sender, EventArgs e)
        {
            try
            {
                List<System.Data.DataTable> iReply = new List<System.Data.DataTable>();
                Int32 oprID = -99, padID = -99, welID = -99, secD = -99, rID = -99, demID = -99;
                Decimal startDepth = -99.00M, endDepth = -99.00M;
                String fileName, pathToFile, dfName, dfExtension;
                List<String> noDataLines = new List<String>();
                Dictionary<String, String> headKeyValue = new Dictionary<String, String>(), dataLines = new Dictionary<String, String>();
                
                if (FileUploadControl.HasFile)
                {
                    try
                    {
                        oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                        padID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                        welID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                        secD = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                        rID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                        pathToFile = WebConfigurationManager.AppSettings["tempFiles"];
                        fileName = Path.Combine(pathToFile, FileUploadControl.FileName);
                        dfName = Path.GetFileNameWithoutExtension(fileName);
                        dfExtension = Path.GetExtension(fileName);
                        if (dfExtension.Equals(".txt") || dfExtension.Equals(".csv"))
                        {
                            if (dfExtension.Equals(".txt"))
                            { demID = 4; }
                            else { demID = 2; }
                            logger.Info("User: " + username + " Uploading File: " + FileUploadControl.FileName + " UploadPath: " + pathToFile);
                            // Save the file to our local path
                            FileUploadControl.SaveAs(fileName);

                            if (File.Exists(fileName))
                            {
                                iReply = DL.uploadStandardDataTextFile(welID, secD, rID, dfName, dfExtension, fileName, ServiceID, demID, LoginName, domain, GetClientDBString);
                                HttpContext.Current.Session["rawDataTables"] = iReply;
                                HttpContext.Current.Session["fileName"] = dfName;
                                HttpContext.Current.Session["fileExtension"] = dfExtension;
                                HttpContext.Current.Session["dataEntryMethod"] = demID; //Date Entry method = 2
                                System.Data.DataTable dataTable = new System.Data.DataTable();
                                dataTable = iReply[1];
                                this.gvwViewManualDisplay.DataSource = dataTable;
                                this.gvwViewManualDisplay.DataBind();
                                startDepth = Convert.ToDecimal(dataTable.Compute("min([Depth])", String.Empty));
                                endDepth = Convert.ToDecimal(dataTable.Compute("max([Depth])", String.Empty));
                                ArrayList rfList = OPR.getRefMagList(oprID, SysClientID, padID, welID, GetClientDBString);
                                Dictionary<Int32, String> ornValue = new Dictionary<Int32, String>();
                                Int32 iDecO = -99;
                                String decOrient = String.Empty;
                                Decimal iDip = -99.0000M, iDec = -99.0000M, iBT = -99.0M, iGT = -99.0000M;
                                iDec = Convert.ToDecimal(rfList[1]);
                                iDecO = Convert.ToInt32(rfList[2]);
                                decOrient = DMG.getCardinalName(iDecO);
                                iDip = Convert.ToDecimal(rfList[3]);
                                iBT = Convert.ToDecimal(rfList[4]);
                                iGT = Convert.ToDecimal(rfList[6]);
                                ornValue.Add(iDecO, decOrient);
                                this.txtStartDepth.Text = startDepth.ToString("#0.00");
                                this.txtEndDepth.Text = endDepth.ToString("#0.00");
                                this.txtWellDip.Text = iDip.ToString();
                                this.txtWellDec.Text = iDec.ToString();
                                this.ddlWellDecOrient.DataSource = ornValue;
                                this.ddlWellDecOrient.DataValueField = "Key";
                                this.ddlWellDecOrient.DataTextField = "Value";
                                this.ddlWellDecOrient.DataBind();
                                this.txtWellBTotal.Text = iBT.ToString("0.00");
                                this.txtWellGTotal.Text = iGT.ToString("0.00000");
                                this.divUploadedData.Visible = true;
                                //this.rawDEnclosure.Visible = true;
                                //this.rawDEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                                //this.iRDE.Attributes["class"] = "icon fa fa-check-circle";
                                //this.spnRDE.InnerText = " Success!";
                                //this.FileUploadStatusLabel.Text = "Successfully uploaded Raw Data File";
                                this.btnProcessRawData.Visible = true;
                                this.divUnits.Visible = true;
                                this.divMSARefMags.Visible = true;
                                logger.Info("User: " + username + " FileUpload: " + fileName + " Status: Successful!");
                                FileUploadControl.Dispose();
                                this.divFileData.Visible = false;
                            }
                            else
                            {
                                this.rawDEnclosure.Visible = true;
                                this.rawDEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iRDE.Attributes["class"] = "icon fa fa-warning";
                                this.spnRDE.InnerText = " Error!";
                                this.FileUploadStatusLabel.Text = "Failure!!! Unable to upload Raw Data File";
                                this.divUploadedData.Visible = false;
                                logger.Info("User: " + username + " FileUpload: " + fileName + " Status: Failure");
                                FileUploadControl.Dispose();
                            }
                        }
                        else
                        {
                            this.rawDEnclosure.Visible = true;
                            this.rawDEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                            this.iRDE.Attributes["class"] = "icon fa fa-danger";
                            this.spnRDE.InnerText = " !!! Error !!!";
                            this.FileUploadStatusLabel.Text = "!!! File Type ERROR !!! (only .txt or .csv files are allowed)";
                            this.divUploadedData.Visible = false;
                            FileUploadControl.Dispose();
                        }
                    }
                    catch (Exception ex)
                    {
                        this.rawDEnclosure.Visible = true;
                        this.rawDEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                        this.iRDE.Attributes["class"] = "icon fa fa-danger";
                        this.spnRDE.InnerText = " !!! Error !!!";
                        this.FileUploadStatusLabel.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
                        this.divUploadedData.Visible = false;
                        logger.Fatal("User: " + username + " Fatal Error During File Upload: " + FileUploadStatusLabel.Text);
                        FileUploadControl.Dispose();
                    }
                }
                else
                {
                    this.rawDEnclosure.Visible = true;
                    this.rawDEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                    this.iRDE.Attributes["class"] = "icon fa fa-danger";
                    this.spnRDE.InnerText = " !!! Error !!!";
                    this.FileUploadStatusLabel.Text = "Upload status: Specify the file to be uploaded ";
                    this.divUploadedData.Visible = false;
                    this.btnCancelFileUpload.Visible = true;
                }

                this.btnCancelFileUpload.Visible = false;
                this.btnProcessSrvyCancel.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProcessRawData_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable iResult = new System.Data.DataTable();
                Int32 dataMethod, OperatorID, WellPadID, WellID, WellSectionID, RunID, chk_FlipGx, chk_FlipGy, chk_FlipGz, chk_FlipBx, chk_FlipBy, chk_FlipBz;
                Int32 lenU = -99, acU = -99, magU = -99, mdOrn = -99;
                Decimal startDepth = -99.00M, endDepth = -99.00M, refDip = -99.00M, refMDec = -99.00M, refBTotal = -99.00M, refGTotal = -99.00M;
                if (rdbFT.Checked) { lenU = 4; } else { lenU = 5; }
                if (rdbGss.Checked) { magU = 1; } else if (rdbT.Checked) { magU = 2; } else if (rdbMLIT.Checked) { magU = 3; } else if (rdbMCT.Checked) { magU = 4; } else { magU = 5; }
                if (rdbCMS.Checked) { acU = 1; } else if (rdbFSC.Checked) { acU = 2; } else if (rdbG.Checked) { acU = 3; } else if (rdbMSC.Checked) { acU = 4; } else { acU = 5; }
                OperatorID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                WellPadID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                WellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                WellSectionID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                RunID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                dataMethod = Convert.ToInt32(HttpContext.Current.Session["dataEntryMethod"]);
                if (this.chk_FlipGx.Checked) { chk_FlipGx = -1; } else { chk_FlipGx = 1; }
                if (this.chk_FlipGy.Checked) { chk_FlipGy = -1; } else { chk_FlipGy = 1; }
                if (this.chk_FlipGz.Checked) { chk_FlipGz = -1; } else { chk_FlipGz = 1; }
                if (this.chk_FlipBx.Checked) { chk_FlipBx = -1; } else { chk_FlipBx = 1; }
                if (this.chk_FlipBy.Checked) { chk_FlipBy = -1; } else { chk_FlipBy = 1; }
                if (this.chk_FlipBz.Checked) { chk_FlipBz = -1; } else { chk_FlipBz = 1; }
                List<System.Data.DataTable> dataTables = new List<System.Data.DataTable>(), readyTables = new List<System.Data.DataTable>();
                dataTables = (List<System.Data.DataTable>)HttpContext.Current.Session["rawDataTables"];
                System.Data.DataTable rawdataTable = new System.Data.DataTable(), fileHead = new System.Data.DataTable(), fileBody = new System.Data.DataTable(), fileDataTable = new System.Data.DataTable();
                rawdataTable = (System.Data.DataTable)dataTables[0];
                foreach (DataRow rw in rawdataTable.Rows)
                {
                    if (lenU.Equals(4)) { rw.SetField("Depth", (UNT.convFeetToMeters(Convert.ToDecimal(rw["Depth"])).ToString("0.00"))); } else { rw.SetField("Depth", Convert.ToDecimal(rw["Depth"]).ToString("0.00")); }                   
                    rw.SetField("Gx", Convert.ToDecimal(rw["Gx"]) * Convert.ToDecimal(chk_FlipGx));
                    if (!acU.Equals(5)) { rw.SetField("Gx", UNT.convAccelerometerToAccelerationDueToGravity(Convert.ToDecimal(rw["Gx"]), acU).ToString("0.00000")); } else { rw.SetField("Gx", Convert.ToDecimal(rw["Gx"]).ToString("0.00000")); }
                    rw.SetField("Gy", Convert.ToDecimal(rw["Gy"]) * Convert.ToDecimal(chk_FlipGy));
                    if (!acU.Equals(5)) { rw.SetField("Gy", UNT.convAccelerometerToAccelerationDueToGravity(Convert.ToDecimal(rw["Gy"]), acU).ToString("0.00000")); } else { rw.SetField("Gy", Convert.ToDecimal(rw["Gy"]).ToString("0.00000")); }
                    rw.SetField("Gz", Convert.ToDecimal(rw["Gz"]) * Convert.ToDecimal(chk_FlipGz));
                    if (!acU.Equals(5)) { rw.SetField("Gz", UNT.convAccelerometerToAccelerationDueToGravity(Convert.ToDecimal(rw["Gz"]), acU).ToString("0.00000")); } else { rw.SetField("Gz", Convert.ToDecimal(rw["Gz"]).ToString("0.00000")); }
                    rw.SetField("Bx", Convert.ToDecimal(rw["Bx"]) * Convert.ToDecimal(chk_FlipBx));
                    if (!magU.Equals(5)) { rw.SetField("Bx", UNT.convMagnetometerToNanoTesla(Convert.ToDecimal(rw["Bx"]), magU).ToString("0.00")); } else { rw.SetField("Bx", Convert.ToDecimal(rw["Bx"]).ToString("0.00")); }
                    rw.SetField("By", Convert.ToDecimal(rw["By"]) * Convert.ToDecimal(chk_FlipBy));
                    if (!magU.Equals(5)) { rw.SetField("By", UNT.convMagnetometerToNanoTesla(Convert.ToDecimal(rw["By"]), magU).ToString("0.00")); } else { rw.SetField("By", Convert.ToDecimal(rw["By"]).ToString("0.00")); }
                    rw.SetField("Bz", Convert.ToDecimal(rw["Bz"]) * Convert.ToDecimal(chk_FlipBz));
                    if (!magU.Equals(5)) { rw.SetField("Bz", UNT.convMagnetometerToNanoTesla(Convert.ToDecimal(rw["Bz"]), magU).ToString("0.00")); } else { rw.SetField("Bz", Convert.ToDecimal(rw["Bz"]).ToString("0.00")); }
                }                
                rawdataTable.AcceptChanges();
                fileDataTable = (System.Data.DataTable)dataTables[1];
                foreach (DataRow fRow in fileDataTable.Rows)
                {
                    fRow.SetField("lenU", lenU);
                    fRow.SetField("acu", acU);
                    fRow.SetField("magU", magU);
                }
                fileDataTable.AcceptChanges();
                HttpContext.Current.Session["readyData"] = rawdataTable;
                String dFile = Convert.ToString(HttpContext.Current.Session["fileName"]);
                String dExt = Convert.ToString(HttpContext.Current.Session["fileExtension"]);
                readyTables.Add(rawdataTable);
                readyTables.Add(fileDataTable);
                HttpContext.Current.Session["rdTables"] = readyTables;
                ArrayList rfList = new ArrayList();                
                if (rdbWell.Checked) {
                    startDepth = Convert.ToDecimal(this.txtStartDepth.Text);
                    rfList.Add(startDepth);
                    endDepth = Convert.ToDecimal(this.txtEndDepth.Text);
                    rfList.Add(endDepth);
                    rfList.Add(lenU);
                    refDip = Convert.ToDecimal(this.txtWellDip.Text);
                    rfList.Add(refDip);
                    refMDec = Convert.ToDecimal(this.txtWellDec.Text);
                    rfList.Add(refMDec);
                    mdOrn = Convert.ToInt32(this.ddlWellDecOrient.SelectedValue);
                    rfList.Add(mdOrn);
                    refBTotal = Convert.ToDecimal(this.txtWellBTotal.Text);
                    rfList.Add(refBTotal);
                    rfList.Add(magU);
                    refGTotal = Convert.ToDecimal(this.txtWellGTotal.Text);
                    rfList.Add(refGTotal);
                    rfList.Add(acU);
                }
                else if (rdbNOAA.Checked) {
                    startDepth = Convert.ToDecimal(this.txtNOAASDepth.Text);
                    rfList.Add(startDepth);
                    endDepth = Convert.ToDecimal(this.txtNOAAEDepth.Text);
                    rfList.Add(endDepth);
                    rfList.Add(lenU);
                    refDip = Convert.ToDecimal(this.txtNOAADip.Text);
                    rfList.Add(refDip);
                    refMDec = Convert.ToDecimal(this.txtNOAADec.Text);
                    rfList.Add(refMDec);
                    mdOrn = Convert.ToInt32(this.ddlNOAAOrientation.SelectedValue);
                    rfList.Add(mdOrn);
                    refBTotal = Convert.ToDecimal(this.txtNOAABTotal.Text);
                    rfList.Add(refBTotal);
                    rfList.Add(magU);
                    refGTotal = Convert.ToDecimal(this.txtNOAAGTotal.Text);
                    rfList.Add(refGTotal);
                    rfList.Add(acU);
                }
                else {
                    startDepth = Convert.ToDecimal(this.txtBGSSDepth.Text);
                    rfList.Add(startDepth);
                    endDepth = Convert.ToDecimal(this.txtBGSEDepth.Text);
                    rfList.Add(endDepth);
                    rfList.Add(lenU);
                    refDip = Convert.ToDecimal(this.txtBGSDip.Text);
                    rfList.Add(refDip);
                    refMDec = Convert.ToDecimal(this.txtBGSDec.Text);
                    rfList.Add(refMDec);
                    mdOrn = Convert.ToInt32(this.ddlBGSOrientation.SelectedValue);
                    rfList.Add(mdOrn);
                    refBTotal = Convert.ToDecimal(this.txtBGSBTotal.Text);
                    rfList.Add(refBTotal);
                    rfList.Add(magU);
                    refGTotal = Convert.ToDecimal(this.txtBGSGTotal.Text);
                    rfList.Add(refGTotal);
                    rfList.Add(acU);
                }
                this.divReadyData.Visible = true;
                this.gvwReadyData.DataSource = rawdataTable;
                this.gvwReadyData.PageIndex = 0;
                this.gvwReadyData.SelectedIndex = -1;
                this.gvwReadyData.DataBind();
                this.btnProcessRawData.Visible = false;
                this.btnProcessSrvyCancel.Visible = false;
                HttpContext.Current.Session["dtMethod"] = dataMethod;
                HttpContext.Current.Session["selectedIFRValues"] = rfList;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProcessSrvyCancel_Click(object sender, EventArgs e)
        {
            try
            {
                HttpContext.Current.Session["fileName"] = null;
                HttpContext.Current.Session["fileExtension"] = null;
                HttpContext.Current.Session["rawDataTables"] = null;
                HttpContext.Current.Session["InputData"] = null;
                this.FileUploadControl.Dispose();
                this.FileUploadStatusLabel.Text = null;
                this.rdbFT.Checked = false;
                this.rdbMT.Checked = false;
                this.rdbCMS.Checked = false;
                this.rdbFSC.Checked = false;
                this.rdbG.Checked = false;
                this.rdbMSC.Checked = false;
                this.rdbMG.Checked = false;
                this.rdbGss.Checked = false;
                this.rdbMLIT.Checked = false;
                this.rdbMCT.Checked = false;
                this.rdbNT.Checked = false;
                this.chk_NoSignTransformation.Checked = true;
                this.chk_NoSignTransformation.Enabled = true;
                this.chk_FlipBx.Checked = false;
                this.chk_FlipBx.Enabled = false;
                this.chk_FlipBy.Checked = false;
                this.chk_FlipBy.Enabled = false;
                this.chk_FlipBz.Checked = false;
                this.chk_FlipBz.Enabled = false;
                this.chk_FlipGx.Checked = false;
                this.chk_FlipGx.Enabled = false;
                this.chk_FlipGy.Checked = false;
                this.chk_FlipGy.Enabled = false;
                this.chk_FlipGz.Checked = false;
                this.chk_FlipGz.Enabled = false;
                this.divManData.Visible = false;
                this.btnCancelManualUpload_Click(null, null);
                this.btnCancelFileUpload_Click(null, null);
                this.gvwViewManualDisplay.DataSource = null;
                this.gvwViewManualDisplay.DataBind();
                this.btnProcessRawData.Visible = false;
                this.btnProcessSrvyCancel.Visible = false;
                this.FileUploadControl.Focus();
                this.rawDEnclosure.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProcessReadyData_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iReply12 = -99;
                ArrayList iResult = new ArrayList();
                System.Data.DataTable iResultData = new System.Data.DataTable();
                Int32 dataMethod = Convert.ToInt32(HttpContext.Current.Session["dtMethod"]);
                String dFile = Convert.ToString(HttpContext.Current.Session["fileName"]);
                String dExt = Convert.ToString(HttpContext.Current.Session["fileExtension"]);
                Int32 OperatorID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 WellPadID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                Int32 WellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 WellSectionID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                Int32 RunID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                List<System.Data.DataTable> dataTables = new List<System.Data.DataTable>();
                dataTables = (List<System.Data.DataTable>)HttpContext.Current.Session["rdTables"];
                ArrayList ifr = (ArrayList)HttpContext.Current.Session["selectedIFRValues"];
                               
                iResult = Corr.processRawData(dataMethod, ServiceID, 1, dFile, dExt, OperatorID, SysClientID, WellPadID, WellID, WellSectionID, RunID, dataTables, ifr, LoginName, domain, GetClientDBString);
                iReply12 = Convert.ToInt32(iResult[0]);
                HttpContext.Current.Session["AveragedData"] = iResult[2];
                if(iReply12.Equals(-1))
                {
                    this.div12Surveys.Visible = true;
                    this.div12Enclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.i12.Attributes["class"] = "icon fa fa-warning";
                    this.spn12.InnerText = " Warning!";
                    this.lbl12.Text = " Minimum Survey Count not met. (Require atleast 12 Survey(s) before analysis)";
                    this.div12Enclosure.Visible = true; 
                }
                else if (iReply12 > 1 && iReply12 < 12)
                {
                    this.div12Surveys.Visible = true;
                    this.div12Enclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.i12.Attributes["class"] = "icon fa fa-warning";
                    this.spn12.InnerText = " Warning!";
                    this.lbl12.Text = " Minimum Survey Count not met. (Require atleast 12 Survey(s) before analysis)";
                    this.div12Enclosure.Visible = true;
                }
                else
                {
                    this.div12Surveys.Visible = true;
                    this.div12Enclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                    this.i12.Attributes["class"] = "icon fa fa-check-circle";
                    this.spn12.InnerText = " Success";
                    this.lbl12.Text = " Minimum Survey requirement met";
                    this.div12Enclosure.Visible = true;
                }                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCancelReadyData_Click(object sender, EventArgs e)
        {
            try
            {
                HttpContext.Current.Session["rawDataTables"] = null;
                HttpContext.Current.Session["InputData"] = null;
                this.gvwReadyData.DataSource = null;
                this.gvwReadyData.DataBind();
                this.divReadyData.Visible = false;
                this.btnProcessRawData.Visible = true;
                this.btnProcessSrvyCancel.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDetailCancel_Click(object sender, EventArgs e)
        {
            try
            {                
                this.btnCancelManualUpload_Click(null, null);
                this.btnCancelFileUpload_Click(null, null);
                this.btnProcessSrvyCancel_Click(null, null);
                this.btnCancelReadyData_Click(null, null);
                this.btnProceed12Clear_Click(null, null);
                this.btnCancelAnalysis_Click(null, null);
                this.btnCancelBz_Click(null, null);
                this.btnCancelNGZ_Click(null, null);
                this.divMagU.Visible = false;
                this.divAclU.Visible = false;
                this.divLenU.Visible = false;
                this.divReadyData.Visible = false;
                this.btnMetaCancel_Click(null, null);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProceed12_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable iResult = new System.Data.DataTable();
                iResult = (System.Data.DataTable)HttpContext.Current.Session["AveragedData"];
                Int32 WellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 SectionID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                Int32 RunID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                this.gvwAvgFilter.DataSource = iResult;
                this.gvwAvgFilter.PageIndex = 0;
                this.gvwAvgFilter.SelectedIndex = -1;
                this.gvwAvgFilter.DataBind();
                this.divAvgFilter.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProceed12Clear_Click(object sender, EventArgs e)
        {
            try
            {
                this.div12Surveys.Visible = false;
                this.div12Enclosure.Visible = false;
                this.btnCancelReadyData_Click(null, null);
                this.divReadyData.Visible = false;
                this.btnProcessSrvyCancel_Click(null, null);
                this.btnMetaCancel_Click(null, null);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwAvgFilter_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = ((DataRowView)e.Row.DataItem).Row;
                    String clrGT = Convert.ToString(dr["metGTAvg"]);
                    String clrBT = Convert.ToString(dr["metBTAvg"]);
                    String clrDip = Convert.ToString(dr["metDipAvg"]);                    
                    CheckBox chkSurvey = (CheckBox)e.Row.FindControl("srvyCheck");
                    e.Row.Cells[5].BackColor = System.Drawing.Color.FromName(clrGT);
                    e.Row.Cells[11].BackColor = System.Drawing.Color.FromName(clrBT);                    
                    e.Row.Cells[14].BackColor = System.Drawing.Color.FromName(clrDip);
                    if (clrDip.Equals("Green") && clrBT.Equals("Green") && clrGT.Equals("Green"))
                    {
                        chkSurvey.Checked = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwAvgFilter_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable iResult = new System.Data.DataTable();
                iResult = (System.Data.DataTable)HttpContext.Current.Session["AveragedData"];
                this.gvwAvgFilter.DataSource = iResult;
                this.gvwAvgFilter.PageIndex = e.NewPageIndex;
                this.gvwAvgFilter.SelectedIndex = -1;
                this.gvwAvgFilter.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAverages_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 rowID = -99, rowCount = -99;
                String rDepthVal = String.Empty, gxValue = String.Empty, gyValue = String.Empty, gzValue = String.Empty, bxValue = String.Empty, byValue = String.Empty, bzValue = String.Empty;
                Decimal rDepth = -99.00M, gX = -99.00M, gY = -99.00M, gZ = -99.00M, bX = -99.00M, bY = -99.00M, bZ = -99.00M;
                System.Data.DataTable selDataTable = new System.Data.DataTable();
                DataColumn sID = selDataTable.Columns.Add("sID", typeof(Int32));
                DataColumn sDepth = selDataTable.Columns.Add("Depth", typeof(Decimal));
                DataColumn sGx = selDataTable.Columns.Add("Gx", typeof(Decimal));
                DataColumn sGy = selDataTable.Columns.Add("Gy", typeof(Decimal));
                DataColumn sGz = selDataTable.Columns.Add("Gz", typeof(Decimal));
                DataColumn sBx = selDataTable.Columns.Add("Bx", typeof(Decimal));
                DataColumn sBy = selDataTable.Columns.Add("By", typeof(Decimal));
                DataColumn sBz = selDataTable.Columns.Add("Bz", typeof(Decimal));
                foreach (GridViewRow dRow in gvwAvgFilter.Rows)
                {
                    CheckBox selData = dRow.FindControl("srvyCheck") as CheckBox;
                    if (selData.Checked)
                    {
                        rowID = Convert.ToInt32(dRow.Cells[0].Text);
                        rDepthVal = Convert.ToString(dRow.Cells[1].Text);
                        gxValue = Convert.ToString(dRow.Cells[2].Text);
                        gxValue = gxValue.Replace(" ", "");
                        gyValue = Convert.ToString(dRow.Cells[3].Text);
                        gyValue = gyValue.Replace(" ", "");
                        gzValue = Convert.ToString(dRow.Cells[4].Text);
                        gzValue = gzValue.Replace(" ", "");
                        bxValue = Convert.ToString(dRow.Cells[8].Text);
                        bxValue = bxValue.Replace(" ", "");
                        byValue = Convert.ToString(dRow.Cells[9].Text);
                        byValue = byValue.Replace(" ", "");
                        bzValue = Convert.ToString(dRow.Cells[10].Text);
                        bzValue = bzValue.Replace(" ", "");
                        rDepth = Convert.ToDecimal(rDepthVal);
                        gX = Convert.ToDecimal(gxValue);
                        gY = Convert.ToDecimal(gyValue);
                        gZ = Convert.ToDecimal(gzValue);
                        bX = Convert.ToDecimal(bxValue);
                        bY = Convert.ToDecimal(byValue);
                        bZ = Convert.ToDecimal(bzValue);
                        selDataTable.Rows.Add(rowID, rDepth, gX, gY, gZ, bX, bY, bZ);
                        selDataTable.AcceptChanges();
                    }
                }
                HttpContext.Current.Session["selectedAveragesData"] = selDataTable;
                rowCount = Corr.getMSAMinRowCount(selDataTable);
                if (rowCount >= 12)
                {
                    this.divAvg12Surveys.Visible = true;
                    this.encAvg12.Attributes["class"] = "alert alert-success alert-dismissable";
                    this.iAvg12.Attributes["class"] = "icon fa fa-check-circle";
                    this.spnAvg12.InnerText = " Success";
                    this.lblAvg12.Text = " Minimum Survey requirement met";
                    this.encAvg12.Visible = true;
                    this.btnProceedAvg12.Enabled = true;
                }
                else
                {
                    this.divAvg12Surveys.Visible = true;
                    this.encAvg12.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iAvg12.Attributes["class"] = "icon fa fa-warning";
                    this.spnAvg12.InnerText = "!!! Warning !!!";
                    this.lblAvg12.Text = " Minimum Survey requirement not met";
                    this.encAvg12.Visible = true;
                    this.btnProceedAvg12.Enabled = false;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAveragesClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.divAvg12Surveys.Visible = false;
                this.encAvg12.Visible = false;
                this.lblAvg12.Text = String.Empty;
                this.btnProceedAvg12.Enabled = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProceedAvg12_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable dataTable = (System.Data.DataTable)HttpContext.Current.Session["selectedAveragesData"];
                ArrayList ifr = new ArrayList();
                ifr = (ArrayList)HttpContext.Current.Session["selectedIFRValues"];
                Int32 OprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 WellpadID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                Int32 WellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 SectionID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                Int32 RunID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                System.Data.DataTable rVecTable = Corr.getRevVectorAnalysis(OprID, SysClientID, WellpadID, WellID, SectionID, RunID, dataTable, ifr, GetClientDBString);
                HttpContext.Current.Session["rVecTable"] = rVecTable;
                this.divRevVectors.Visible = true;
                this.gvwRevVectors.DataSource = rVecTable;
                this.gvwRevVectors.PageIndex = 0;
                this.gvwRevVectors.SelectedIndex = -1;
                this.gvwRevVectors.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProceedAvg12Clear_Click(object sender, EventArgs e)
        {
            try
            {
                this.divAvg12Surveys.Visible = false;
                this.encAvg12.Visible = false;
                this.lblAvg12.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwBzFilter_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwBzFilter_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = ((DataRowView)e.Row.DataItem).Row;
                    CheckBox chkSurvey = (CheckBox)e.Row.FindControl("srvyCheck");
                    chkSurvey.Checked = true;                    
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnNGZProceed_Click(object sender, EventArgs e)
        {
            try            
            {
                Int32 rowID = -99;
                String rDepthVal = String.Empty, gxValue = String.Empty, gyValue = String.Empty, gzValue = String.Empty, bxValue = String.Empty, byValue = String.Empty, bzValue = String.Empty;
                Decimal rDepth = -99.00M, gX = -99.00M, gY = -99.00M, gZ = -99.00M, bX = -99.00M, bY = -99.00M, bZ = -99.00M;
                System.Data.DataTable selDataTable = new System.Data.DataTable(), ngzTable = new System.Data.DataTable(); ;
                DataColumn sID = selDataTable.Columns.Add("sID", typeof(Int32));
                DataColumn sDepth = selDataTable.Columns.Add("Depth", typeof(Decimal));
                DataColumn sGx = selDataTable.Columns.Add("Gx", typeof(Decimal));
                DataColumn sGy = selDataTable.Columns.Add("Gy", typeof(Decimal));
                DataColumn sGz = selDataTable.Columns.Add("Gz", typeof(Decimal));
                DataColumn sBx = selDataTable.Columns.Add("Bx", typeof(Decimal));
                DataColumn sBy = selDataTable.Columns.Add("By", typeof(Decimal));
                DataColumn sBz = selDataTable.Columns.Add("Bz", typeof(Decimal));
                foreach (GridViewRow dRow in gvwBzFilter.Rows)
                {
                    CheckBox selData = dRow.FindControl("srvyCheck") as CheckBox;
                    if (selData.Checked)
                    {
                        rowID = Convert.ToInt32(dRow.Cells[0].Text);
                        rDepthVal = Convert.ToString(dRow.Cells[1].Text);
                        gxValue = Convert.ToString(dRow.Cells[2].Text);
                        gxValue = gxValue.Replace(" ", "");
                        gyValue = Convert.ToString(dRow.Cells[3].Text);
                        gyValue = gyValue.Replace(" ", "");
                        gzValue = Convert.ToString(dRow.Cells[4].Text);
                        gzValue = gzValue.Replace(" ", "");
                        bxValue = Convert.ToString(dRow.Cells[5].Text);
                        bxValue = bxValue.Replace(" ", "");
                        byValue = Convert.ToString(dRow.Cells[6].Text);
                        byValue = byValue.Replace(" ", "");
                        bzValue = Convert.ToString(dRow.Cells[7].Text);
                        bzValue = bzValue.Replace(" ", "");
                        rDepth = Convert.ToDecimal(rDepthVal);
                        gX = Convert.ToDecimal(gxValue);
                        gY = Convert.ToDecimal(gyValue);
                        gZ = Convert.ToDecimal(gzValue);
                        bX = Convert.ToDecimal(bxValue);
                        bY = Convert.ToDecimal(byValue);
                        bZ = Convert.ToDecimal(bzValue);
                        selDataTable.Rows.Add(rowID, rDepth, gX, gY, gZ, bX, bY, bZ);
                        selDataTable.AcceptChanges();
                    }
                }
                ArrayList ifr = new ArrayList();
                ifr = (ArrayList)HttpContext.Current.Session["selectedIFRValues"];
                Int32 OprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 WellpadID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                Int32 WellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 SectionID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                Int32 RunID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                ngzTable = Corr.getNoGoZoneTable(OprID, SysClientID, WellpadID, WellID, SectionID, RunID, selDataTable, ifr, GetClientDBString);
                HttpContext.Current.Session["ngzTable"] = ngzTable;//for page change in gridview only
                this.divNGZ.Visible = true;
                this.gvwNGZ.DataSource = ngzTable;
                this.gvwNGZ.PageIndex = 0;
                this.gvwNGZ.SelectedIndex = -1;
                this.gvwNGZ.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwNGZ_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = ((DataRowView)e.Row.DataItem).Row;
                    CheckBox chkSurvey = (CheckBox)e.Row.FindControl("srvyCheck");
                    String clrNGZ = Convert.ToString(dr["ngzClr"]);
                    e.Row.Cells[8].BackColor = System.Drawing.Color.FromName(clrNGZ);
                    if (clrNGZ.Equals("Green"))
                    {                        
                        chkSurvey.Checked = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwNGZ_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable iResult = new System.Data.DataTable();
                iResult = (System.Data.DataTable)HttpContext.Current.Session["ngzTable"];
                this.gvwNGZ.DataSource = iResult;
                this.gvwNGZ.PageIndex = e.NewPageIndex;
                this.gvwNGZ.SelectedIndex = -1;
                this.gvwNGZ.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCancelNGZ_Click(object sender, EventArgs e)
        {
            try
            {
                this.gvwNGZ.DataSource = null;
                this.gvwNGZ.DataBind();
                this.divNGZ.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCancelBz_Click(object sender, EventArgs e)
        {
            try
            {
                this.gvwBzFilter.DataSource = null;
                this.gvwBzFilter.DataBind();
                this.divExternalInterference.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDeltas_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnClearDeltas_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProceedQuad_Click(object sender, EventArgs e)
        {
            try
            {
                //Int32 iResult = -99;
                Int32 JobID = -99;
                Int32 WellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 SectionID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                Int32 RunID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                List<KeyValuePair<String, Decimal>> refVals = new List<KeyValuePair<String, Decimal>>();
                refVals = (List<KeyValuePair<String, Decimal>>)HttpContext.Current.Session["ifrVals"];
                //iResult = Corr.InputToAnalysis(ServiceID, JobID, WellID, SectionID, RunID, refVals, username, domain, GetClientDBString);
                //if (iResult.Equals(1))
                {
                    System.Data.DataTable itaTable = Corr.getCorrectionInputToAnalysisTable(JobID, WellID, SectionID, RunID, refVals, GetClientDBString);
                    this.btnProcessReadyData.Visible = false;
                    this.btnCancelReadyData.Visible = false;
                    if (!IsPostBack)
                    {
                        this.createCharts();
                    }
                    else 
                    {
                        this.createCharts();
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void createCharts()
        {
            try
            {
                String result = String.Empty;
                String echartsBT = String.Empty;
                String echartsDip = String.Empty;
                String echartsGT = String.Empty;
                System.Data.DataTable dsChartData = new System.Data.DataTable();
                Int32 wD = Convert.ToInt32(this.gvwWellList.SelectedValue);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void createBsCharts()
        {
            try
            {
                String result = String.Empty;
                String echartsBx = String.Empty;
                String echartsDip = String.Empty;
                String echartsGT = String.Empty;
                System.Data.DataTable dsChartData = new System.Data.DataTable();
                Int32 wD = Convert.ToInt32(this.gvwWellList.SelectedValue);
                dsChartData = Corr.getDataTableforBsGraph(wD, GetClientDBString);
                echartsBx = CHRT.getChartsCorrAnalysisBx("BodyContent_ctrlCorrQC_divBRBxChart", dsChartData);                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ManualQCSurveyProcessor(Int32 jobID, Int32 welID, Int32 sqcID, String srvyDate, String srvyTime, Decimal srDepth, Decimal srGx, Decimal srGy, Decimal srGz, Decimal srBx, Decimal srBy, Decimal srBz, Int32 srRun, Int32 srRowID)
        {
            try
            {}
            catch (Exception ex) { throw new System.Exception(ex.ToString()); }
        }

        protected void btnClearResDetail_Click(object sender, EventArgs e)
        {
            try
            {}
            catch (Exception ex) { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbFT_CheckedChanged(object sender, EventArgs e)
        {
            this.rdbMT.Checked = false;
            this.rdbCMS.Focus();
            this.rdbCMS.Checked = false;
            this.rdbFSC.Checked = false;
            this.rdbG.Checked = false;
            this.rdbMSC.Checked = false;
            this.rdbMG.Checked = false;
        }

        protected void rdbMT_CheckedChanged(object sender, EventArgs e)
        {
            this.rdbFT.Checked = false;
            this.rdbCMS.Focus();
            this.rdbCMS.Checked = false;
            this.rdbFSC.Checked = false;
            this.rdbG.Checked = false;
            this.rdbMSC.Checked = false;
            this.rdbMG.Checked = false;
        }

        protected void rdbCMS_CheckedChanged(object sender, EventArgs e)
        {
            this.rdbFSC.Checked = false;
            this.rdbG.Checked = false;
            this.rdbMSC.Checked = false;
            this.rdbMG.Checked = false;
            this.rdbGss.Focus();
            this.rdbGss.Checked = false;
            this.rdbT.Checked = false;
            this.rdbMLIT.Checked = false;
            this.rdbMCT.Checked = false;
            this.rdbNT.Checked = false;
        }

        protected void rdbFSC_CheckedChanged(object sender, EventArgs e)
        {
            this.rdbCMS.Checked = false;
            this.rdbG.Checked = false;
            this.rdbMSC.Checked = false;
            this.rdbMG.Checked = false;
            this.rdbGss.Focus();
            this.rdbGss.Checked = false;
            this.rdbT.Checked = false;
            this.rdbMLIT.Checked = false;
            this.rdbMCT.Checked = false;
            this.rdbNT.Checked = false;   
        }

        protected void rdbG_CheckedChanged(object sender, EventArgs e)
        {
            this.rdbCMS.Checked = false;
            this.rdbFSC.Checked = false;
            this.rdbMSC.Checked = false;
            this.rdbMG.Checked = false;
            this.rdbGss.Focus();
            this.rdbGss.Checked = false;
            this.rdbT.Checked = false;
            this.rdbMLIT.Checked = false;
            this.rdbMCT.Checked = false;
            this.rdbNT.Checked = false;
        }

        protected void rdbMSC_CheckedChanged(object sender, EventArgs e)
        {
            this.rdbCMS.Checked = false;
            this.rdbFSC.Checked = false;
            this.rdbG.Checked = false;
            this.rdbMG.Checked = false;
            this.rdbGss.Focus();
            this.rdbGss.Checked = false;
            this.rdbT.Checked = false;
            this.rdbMLIT.Checked = false;
            this.rdbMCT.Checked = false;
            this.rdbNT.Checked = false;
        }

        protected void rdbMG_CheckedChanged(object sender, EventArgs e)
        {
            this.rdbCMS.Checked = false;
            this.rdbFSC.Checked = false;
            this.rdbG.Checked = false;
            this.rdbMSC.Checked = false;
            this.rdbGss.Focus();
            this.rdbGss.Checked = false;
            this.rdbT.Checked = false;
            this.rdbMLIT.Checked = false;
            this.rdbMCT.Checked = false;
            this.rdbNT.Checked = false;   
        }

        protected void rdbGss_CheckedChanged(object sender, EventArgs e)
        {
            this.rdbT.Checked = false;
            this.rdbMLIT.Checked = false;
            this.rdbMCT.Checked = false;
            this.rdbNT.Checked = false;
            this.btnProcessRawData.Enabled = true;
            this.btnProcessRawData.CssClass = "btn btn-info text-center";
        }

        protected void rdbT_CheckedChanged(object sender, EventArgs e)
        {
            this.rdbGss.Checked = false;
            this.rdbMLIT.Checked = false;
            this.rdbMCT.Checked = false;
            this.rdbNT.Checked = false;
            this.btnProcessRawData.Enabled = true;
            this.btnProcessRawData.CssClass = "btn btn-info text-center";
        }

        protected void rdbMLIT_CheckedChanged(object sender, EventArgs e)
        {
            this.rdbGss.Checked = false;
            this.rdbT.Checked = false;
            this.rdbMCT.Checked = false;
            this.rdbNT.Checked = false;
            this.btnProcessRawData.Enabled = true;
            this.btnProcessRawData.CssClass = "btn btn-info text-center";
        }

        protected void rdbMCT_CheckedChanged(object sender, EventArgs e)
        {
            this.rdbGss.Checked = false;
            this.rdbT.Checked = false;
            this.rdbMLIT.Checked = false;
            this.rdbNT.Checked = false;
            this.btnProcessRawData.Enabled = true;
            this.btnProcessRawData.CssClass = "btn btn-info text-center";
        }

        protected void rdbNT_CheckedChanged(object sender, EventArgs e)
        {
            this.rdbGss.Checked = false;
            this.rdbT.Checked = false;
            this.rdbMLIT.Checked = false;
            this.rdbMCT.Checked = false;
            this.btnProcessRawData.Enabled = true;
            this.btnProcessRawData.CssClass = "btn btn-info text-center";
        }

        protected void chk_NoSignTransformation_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.chk_NoSignTransformation.Checked = false;
                this.chk_NoSignTransformation.Enabled = false;
                this.chk_FlipGx.Enabled = true;
                this.chk_FlipGy.Enabled = true;
                this.chk_FlipGz.Enabled = true;
                this.chk_FlipBx.Enabled = true;
                this.chk_FlipBy.Enabled = true;
                this.chk_FlipBz.Enabled = true;
                this.btnProcessRawData.Enabled = true;
                this.btnProcessRawData.CssClass = "btn btn-info text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProcessSolution_Click(object sender, EventArgs e)
        {
            try
            {
                ArrayList selRows = new ArrayList();
                ArrayList resultList = new ArrayList();
                Int32 itrCount = -99, resCount = -99, JobID = -99, WellID = -99, RunID = -99, SQCID = -99, selID = -99, iReply = -99;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void imgbtnSolar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://www.n3kl.org/sun/noaa.html");
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void imgbtnGeoMag_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://www.n3kl.org/sun/noaa.html");
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAnalyze_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable iResult = new System.Data.DataTable();
                Int32 JobID = -99, WellID = -99, SecID = -99, RunID = -99;
                WellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                SecID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                RunID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                iResult = Corr.getBiasRemoved(JobID, WellID, SecID, RunID, GetClientDBString);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCancelAnalysis_Click(object sender, EventArgs e)
        {
            try
            {
                
                HttpContext.Current.Session["InputData"] = null;                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwInputData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = ((DataRowView)e.Row.DataItem).Row;
                    String clrBT = Convert.ToString(dr["metBTSQC"]);
                    String clrDip = Convert.ToString(dr["metDipSQC"]);
                    String clrGT = Convert.ToString(dr["metGTSQC"]);
                    CheckBox chkSurvey = (CheckBox)e.Row.FindControl("srvyCheck");
                    e.Row.Cells[7].BackColor = System.Drawing.Color.FromName(clrDip);
                    e.Row.Cells[13].BackColor = System.Drawing.Color.FromName(clrBT);
                    e.Row.Cells[14].BackColor = System.Drawing.Color.FromName(clrGT);
                    if (clrDip.Equals("Green") && clrBT.Equals("Green") && clrGT.Equals("Green"))
                    {
                        chkSurvey.Checked = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwInputData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable dataTable = (System.Data.DataTable)HttpContext.Current.Session["AnalysisInputData"];
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwReadyData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable rdData = (System.Data.DataTable)HttpContext.Current.Session["readyData"];
                this.gvwReadyData.DataSource = rdData;
                this.gvwReadyData.PageIndex = e.NewPageIndex;
                this.gvwReadyData.SelectedIndex = -1;
                this.gvwReadyData.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbWell_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbWell.Checked = true;
                this.txtWellDip.Enabled = true;
                this.txtWellDec.Enabled = true;
                this.txtWellBTotal.Enabled = true;
                this.txtWellGTotal.Enabled = true;
                this.rdbNOAA.Checked = false;
                this.txtNOAADip.Enabled = false;
                this.txtNOAADec.Enabled = false;
                this.ddlNOAAOrientation.Items.Clear();
                this.ddlNOAAOrientation.DataSource = null;
                this.ddlNOAAOrientation.DataBind();
                this.ddlNOAAOrientation.Items.Insert(0, new ListItem("--- Select Orientation", "-1"));
                this.ddlNOAAOrientation.SelectedIndex = -1;
                this.ddlNOAAOrientation.Enabled = false;
                this.txtNOAABTotal.Enabled = false;
                this.txtNOAAGTotal.Enabled = false;
                this.rdbBGS.Checked = false;
                this.txtBGSDip.Enabled = false;
                this.txtBGSDec.Enabled = false;
                this.ddlBGSOrientation.Items.Clear();
                this.ddlBGSOrientation.DataSource = null;
                this.ddlBGSOrientation.DataBind();
                this.ddlBGSOrientation.Items.Insert(0, new ListItem("--- Select Orientation", "-1"));
                this.ddlBGSOrientation.SelectedIndex = -1;
                this.ddlBGSOrientation.Enabled = false;
                this.txtBGSBTotal.Enabled = false;
                this.txtBGSGTotal.Enabled = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbNOAA_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbWell.Checked = false;
                this.txtWellDip.Enabled = false;
                this.txtWellDec.Enabled = false;
                this.txtWellBTotal.Enabled = false;
                this.txtWellGTotal.Enabled = false;
                this.rdbNOAA.Checked = true;
                this.txtNOAADip.Enabled = true;
                this.txtNOAADec.Enabled = true;
                Dictionary<Int32, String> ornValue = DMG.getLngCardinals();
                this.ddlNOAAOrientation.DataSource = ornValue;
                this.ddlNOAAOrientation.DataTextField = "Value";
                this.ddlNOAAOrientation.DataValueField = "Key";
                this.ddlNOAAOrientation.Items.Insert(0, new ListItem("--- Select Orientation ---", "-1"));
                this.ddlNOAAOrientation.DataBind();
                this.ddlNOAAOrientation.SelectedIndex = -1;
                this.ddlNOAAOrientation.Enabled = true;
                this.txtNOAABTotal.Enabled = true;
                this.txtNOAAGTotal.Enabled = true;
                this.rdbBGS.Checked = false;
                this.txtBGSDip.Enabled = false;
                this.txtBGSDec.Enabled = false;
                this.ddlBGSOrientation.Items.Clear();
                this.ddlBGSOrientation.DataSource = null;
                this.ddlBGSOrientation.DataBind();
                this.ddlBGSOrientation.Items.Insert(0, new ListItem("--- Select Orientation", "-1"));
                this.ddlBGSOrientation.SelectedIndex = -1;
                this.ddlBGSOrientation.Enabled = false;
                this.txtBGSBTotal.Enabled = false;
                this.txtBGSGTotal.Enabled = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbBGS_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbWell.Checked = false;
                this.txtWellDip.Enabled = false;
                this.txtWellDec.Enabled = false;
                this.txtWellBTotal.Enabled = false;
                this.txtWellGTotal.Enabled = false;
                this.rdbNOAA.Checked = false;
                this.txtNOAADip.Enabled = false;
                this.txtNOAADec.Enabled = false;
                this.ddlNOAAOrientation.Items.Clear();
                this.ddlNOAAOrientation.DataSource = null;
                this.ddlNOAAOrientation.DataBind();
                this.ddlNOAAOrientation.Items.Insert(0, new ListItem("--- Select Orientation", "-1"));
                this.ddlNOAAOrientation.SelectedIndex = -1;
                this.ddlNOAAOrientation.Enabled = false;
                this.txtNOAABTotal.Enabled = false;
                this.txtNOAAGTotal.Enabled = false;
                this.rdbBGS.Checked = true;
                this.txtBGSDip.Enabled = true;
                this.txtBGSDec.Enabled = true;
                Dictionary<Int32, String> ornValue = DMG.getLngCardinals();
                this.ddlBGSOrientation.DataSource = ornValue;
                this.ddlBGSOrientation.DataTextField = "Value";
                this.ddlBGSOrientation.DataValueField = "Key";
                this.ddlBGSOrientation.Items.Insert(0, new ListItem("--- Select Orientation ---", "-1"));
                this.ddlBGSOrientation.DataBind();
                this.ddlBGSOrientation.SelectedIndex = -1;
                this.ddlBGSOrientation.Enabled = true;
                this.txtBGSBTotal.Enabled = true;
                this.txtBGSGTotal.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAnalyzeBias_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 JobID = -99, WellID = -99, SecID = -99, RunID = -99;
                WellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                SecID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                RunID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                System.Data.DataTable HVTable = Corr.getVertHorzComponents(JobID, WellID, SecID, RunID, GetClientDBString);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAnalyzeBiasCancel_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnFindBias_Click(object sender, EventArgs e)
        {
            try
            {
                List<Decimal> iReply = new List<Decimal>();
                Int32 JobID = -99, WellID = -99, SecID = -99, RunID = -99;
                WellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                SecID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                RunID = Convert.ToInt32(this.gvwRunList.SelectedValue);

                System.Data.DataTable HVTable = Corr.FindBias(JobID, WellID, SecID, RunID, GetClientDBString);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnFindBiasCancel_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRevVectors_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = ((DataRowView)e.Row.DataItem).Row;
                    String clrGx = Convert.ToString(dr["clrGx"]);
                    String clrGy = Convert.ToString(dr["clrGy"]);
                    String clrGz = Convert.ToString(dr["clrGz"]);
                    String clrBx = Convert.ToString(dr["clrBx"]);
                    String clrBy = Convert.ToString(dr["clrBy"]);
                    String clrBz = Convert.ToString(dr["clrBz"]);
                    CheckBox chkSurvey = (CheckBox)e.Row.FindControl("srvyCheck");
                    e.Row.Cells[4].BackColor = System.Drawing.Color.FromName(clrGx);
                    e.Row.Cells[7].BackColor = System.Drawing.Color.FromName(clrGy);
                    e.Row.Cells[10].BackColor = System.Drawing.Color.FromName(clrGz);
                    e.Row.Cells[13].BackColor = System.Drawing.Color.FromName(clrBx);
                    e.Row.Cells[16].BackColor = System.Drawing.Color.FromName(clrBy);
                    e.Row.Cells[19].BackColor = System.Drawing.Color.FromName(clrBz);
                    if (clrGx.Equals("Green") && clrGy.Equals("Green") && clrGz.Equals("Green") && clrBx.Equals("Green") && clrBy.Equals("Green") && clrBz.Equals("Green"))
                    {
                        chkSurvey.Checked = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRevVectors_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable rVecTable = (System.Data.DataTable)HttpContext.Current.Session["rVecTable"];
                this.gvwRevVectors.DataSource = rVecTable;
                this.gvwRevVectors.PageIndex = e.NewPageIndex;
                this.gvwRevVectors.SelectedIndex = -1;
                this.gvwRevVectors.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRVProceed_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable iResult = new System.Data.DataTable(), selBHAFilterTable = new System.Data.DataTable();
                DataColumn srvID = selBHAFilterTable.Columns.Add("srvID", typeof(Int32));
                DataColumn sDepth = selBHAFilterTable.Columns.Add("Depth", typeof(Decimal));
                DataColumn sGx = selBHAFilterTable.Columns.Add("Gx", typeof(Decimal));
                DataColumn sGy = selBHAFilterTable.Columns.Add("Gy", typeof(Decimal));
                DataColumn sGz = selBHAFilterTable.Columns.Add("Gz", typeof(Decimal));
                DataColumn sBx = selBHAFilterTable.Columns.Add("Bx", typeof(Decimal));
                DataColumn sBy = selBHAFilterTable.Columns.Add("By", typeof(Decimal));
                DataColumn sBz = selBHAFilterTable.Columns.Add("Bz", typeof(Decimal));
                Int32 rowID = -99, operatorID = -99, wellpadID = -99, wellID = -99, sectionID = -99, runID = -99;
                operatorID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                wellpadID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                wellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                sectionID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                runID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                Decimal rDepth = -99.00M, gX = -99.00M, gY = -99.00M, gZ = -99.00M, bX = -99.00M, bY = -99.00M, bZ = -99.00M;
                String rDepthVal = String.Empty, gxValue = String.Empty, gyValue = String.Empty, gzValue = String.Empty, bxValue = String.Empty, byValue = String.Empty, bzValue = String.Empty;
                ArrayList ifrList = new ArrayList();
                ifrList = (ArrayList)HttpContext.Current.Session["selectedIFRValues"];
                foreach (GridViewRow dRow in gvwRevVectors.Rows)
                {
                    CheckBox selData = dRow.FindControl("srvyCheck") as CheckBox;
                    if (selData.Checked)
                    {
                        rowID = Convert.ToInt32(dRow.Cells[0].Text);
                        rDepthVal = Convert.ToString(dRow.Cells[1].Text);
                        gxValue = Convert.ToString(dRow.Cells[2].Text);
                        gxValue = gxValue.Replace(" ", "");
                        gyValue = Convert.ToString(dRow.Cells[5].Text);
                        gyValue = gyValue.Replace(" ", "");
                        gzValue = Convert.ToString(dRow.Cells[8].Text);
                        gzValue = gzValue.Replace(" ", "");
                        bxValue = Convert.ToString(dRow.Cells[11].Text);
                        bxValue = bxValue.Replace(" ", "");
                        byValue = Convert.ToString(dRow.Cells[14].Text);
                        byValue = byValue.Replace(" ", "");
                        bzValue = Convert.ToString(dRow.Cells[17].Text);
                        bzValue = bzValue.Replace(" ", "");
                        rDepth = Convert.ToDecimal(rDepthVal);
                        gX = Convert.ToDecimal(gxValue);
                        gY = Convert.ToDecimal(gyValue);
                        gZ = Convert.ToDecimal(gzValue);
                        bX = Convert.ToDecimal(bxValue);
                        bY = Convert.ToDecimal(byValue);
                        bZ = Convert.ToDecimal(bzValue);
                        selBHAFilterTable.Rows.Add(rowID, rDepth, gX, gY, gZ, bX, bY, bZ);
                        selBHAFilterTable.AcceptChanges();

                    }
                }
                iResult = Corr.getBHABzFilter(operatorID, SysClientID, wellpadID, wellID, sectionID, runID, selBHAFilterTable, ifrList, GetClientDBString);
                this.divExternalInterference.Visible = true;
                this.gvwBzFilter.DataSource = iResult;
                this.gvwBzFilter.PageIndex = 0;
                this.gvwBzFilter.SelectedIndex = -1;
                this.gvwBzFilter.DataBind();

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRVClear_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable rVecTable = (System.Data.DataTable)HttpContext.Current.Session["rVecTable"];
                this.gvwRevVectors.DataSource = rVecTable;
                this.gvwRevVectors.PageIndex = 0;
                this.gvwRevVectors.SelectedIndex = -1;
                this.gvwRevVectors.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnBadDataroceed_Click(object sender, EventArgs e)
        {

        }                                                                                                     
    }
}