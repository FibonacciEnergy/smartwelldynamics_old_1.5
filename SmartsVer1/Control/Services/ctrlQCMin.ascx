﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlQCMin.ascx.cs" Inherits="SmartsVer1.Control.Services.ctrlQCMin" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<!-- Stylesheet -->
<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dataTables.bootstrap.min.css" rel="stylesheet" />
<!-- Javascript for ref lib -->
<script src="../../Scripts/jquery-3.3.1.min.js"></script>
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<!-- Javascript for Footer -->
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>
<!--- local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }
    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }
</style>
<asp:UpdateProgress ID="uprgrsMeta" runat="server" DisplayAfter="0" DynamicLayout="true" AssociatedUpdatePanelID="upnlQC">
    <ProgressTemplate>
        <div id="divMetaProcessing" runat="server" class="updateProgress">
            <asp:Image ID="imgMetaProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<asp:UpdatePanel runat="server" ID="upnlQC">
    <ContentTemplate>
        <!--- Page Menu -->
        <div class="row bg-black">
            <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom: 2px; padding-left: 2px; padding-top: 2px">
                <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
            </div>
            <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
                <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
            </div>
        </div>
        <div class="row bg-black">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
                <asp:LoginView ID='LoginView1' runat='server'>
                    <AnonymousTemplate>
                        <li style="vertical-align: middle; display: table-cell;">
                            <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                                ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                        </li>
                    </AnonymousTemplate>
                    <LoggedInTemplate>
                        <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">(
                            <asp:LoginName ID="LoginName1" runat="server" />
                            )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                        </li>
                    </LoggedInTemplate>
                </asp:LoginView>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row bg-white">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div id="acrdQCOuter" runat="server" class="panel panel-danger">
                        <div id="panelOuterHeading" class="panel-heading">
                            <h5 class="panel-title" style="text-align: left">
                                <a id="ancQCOuter" runat="server" class="disabled" href="#">SMARTs Raw Data Audit/QC</a>
                            </h5>
                        </div>
                        <div id="panelOuterBody" runat="server" class="panel-body">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div id="divProgress" class="panel panel-danger">
                                    <div class="panel-body">
                                        <ol id="olStepper" runat="server" class="stepper" data-stepper-steps="7">
                                            <li id="liStep1" runat="server" class="stepper-todo">Operator Company</li>
                                            <li id="liStep2" runat="server" class="stepper-todo">Well Pad</li>
                                            <li id="liStep3" runat="server" class="stepper-todo">Well/Lateral</li>
                                            <li id="liStep4" runat="server" class="stepper-todo">Section</li>
                                            <li id="liStep5" runat="server" class="stepper-todo">Run</li>
                                            <li id="liStep6" runat="server" class="stepper-todo">Data</li>
                                            <li id="liStep7" runat="server" class="stepper-todo">Audit/QC</li>
                                        </ol>
                                    </div>
                                </div>
                                <div class="panel panel-danger">
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="panel panel-info">
                                                <div class="panel-heading">
                                                    <h5 class="panel-title" style="text-align: left">
                                                        <a id="ancHead" runat="server" class="disabled" href="#">Operator Company</a>
                                                    </h5>
                                                </div>
                                                <div id="divOpr" runat="server" class="panel-body">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue">Operator Company</span>
                                                        <asp:DropDownList ID="ddlGlobalOperatorList" runat="server" AutoPostBack="true" AppendDataBoundItems="true"
                                                            class="form-control" OnSelectedIndexChanged="ddlGlobalOperatorList_SelectedIndexChanged">
                                                            <asp:ListItem Text="--- Select Operator Company ---" Value="0"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div id="divPad" runat="server" visible="false" class="panel-body">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue">Wellpad</span>
                                                        <asp:DropDownList ID="ddlWellPad" runat="server" AutoPostBack="true" AppendDataBoundItems="true"
                                                            class="form-control" OnSelectedIndexChanged="ddlWellPad_SelectedIndexChanged">
                                                            <asp:ListItem Text="--- Select Wellpad ---" Value="0"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div id="divWell" runat="server" visible="false" class="panel-body">
                                                    <div class="table-responsive">
                                                        <asp:GridView ID="gvwWellList" runat="server" CssClass="mydatagrid" EmptyDataText="No Wells associated with the selected Service Order #"
                                                            AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="welID" OnSelectedIndexChanged="gvwWellList_SelectedIndexChanged"
                                                            ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwWellList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                            PageSize="15" PagerSettings-Mode="Numeric" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Center" PagerStyle-VerticalAlign="Middle"
                                                            HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow"
                                                            OnRowDataBound="gvwWellList_RowDataBound">
                                                            <Columns>
                                                                <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                    <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                </asp:ButtonField>
                                                                <asp:BoundField DataField="welID" Visible="false" />
                                                                <asp:BoundField DataField="welName" HeaderText="Well / Lateral" />
                                                                <asp:BoundField DataField="srvName" HeaderText="Registered Service" />
                                                            </Columns>
                                                        </asp:GridView>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div runat="server" id="wellEnclosure" visible="false">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                <h4><i runat="server" id="iWell"><span runat="server" id="spnWell"></span></i></h4>
                                                                <asp:Label ID="lblWell" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divSec" runat="server" visible="false" class="panel-body">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue">Well Section</span>
                                                        <asp:DropDownList ID="ddlSectionList" runat="server" AutoPostBack="true" AppendDataBoundItems="true"
                                                            class="form-control" OnSelectedIndexChanged="ddlSectionList_SelectedIndexChanged">
                                                            <asp:ListItem Text="--- Select Well Section ---" Value="0"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div id="divRun" runat="server" visible="false" class="panel-body">
                                                    <div class="input-group">
                                                        <span class="input-group-addon spanLabel bg-light-blue">Run</span>
                                                        <asp:DropDownList ID="ddlRunList" runat="server" AutoPostBack="true" AppendDataBoundItems="true"
                                                            class="form-control" OnSelectedIndexChanged="ddlRunList_SelectedIndexChanged">
                                                            <asp:ListItem Text="--- Select Run ---" Value="0"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div runat="server" id="runEnclosure" visible="false">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                <h4><i runat="server" id="iRun"><span runat="server" id="spnRun"></span></i></h4>
                                                                <asp:Label ID="lblRun" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divManData" runat="server" visible="false" class="panel-body">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon bg-light-blue">Input Survey</span>
                                                            <asp:TextBox ID="txtQCManualProcess" runat="server" CssClass="form-control" placeholder="Format: Timestamp*, Depth*, Gx*, Gy*, Gz*, Bx*, By*, Bz* ...  ( * required )" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div runat="server" id="txtDataEnclosure" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iTXTDMessage"><span runat="server" id="txtDHeader"></span></i></h4>
                                                            <asp:Label runat="server" ID="lblManual" Text="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divUploadedData" runat="server" visible="false" class="panel-body">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div id="divSrvCheck" runat="server">
                                                            <asp:GridView ID="gvwViewRawDisplay" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="drID"
                                                                SelectedRowStyle-CssClass="selectedrow" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                                EmptyDataText="No Data Uploaded" AllowPaging="True" AllowSorting="True" PagerStyle-CssClass="pager" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                OnPageIndexChanging="gvwViewRawDisplay_PageIndexChanging" Visible="false">
                                                                <Columns>
                                                                    <asp:BoundField DataField="drID" Visible="False" />
                                                                    <asp:BoundField DataField="Timestamp" HeaderText="Timestamp" />
                                                                    <asp:BoundField DataField="Depth" HeaderText="Depth" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="Gx" HeaderText="Ax / Gx" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="Gy" HeaderText="Ay / Gy" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="Gz" HeaderText="Az / Gz" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="Bx" HeaderText="Mx / Bx" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="By" HeaderText="My / By" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="Bz" HeaderText="Mz / Bz" ItemStyle-HorizontalAlign="Right" />
                                                                </Columns>
                                                            </asp:GridView>
                                                            <asp:GridView ID="gvwReadyRawData" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="drID"
                                                                SelectedRowStyle-CssClass="selectedrow" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                                EmptyDataText="No Data Uploaded" AllowPaging="True" AllowSorting="True" PagerStyle-CssClass="pager" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                OnPageIndexChanging="gvwReadyRawData_PageIndexChanging">
                                                                <Columns>
                                                                    <asp:BoundField DataField="drID" Visible="False" />
                                                                    <asp:BoundField DataField="Depth" HeaderText="Depth (m)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                    <asp:BoundField DataField="Gx" HeaderText="Ax / Gx (g)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.000000000}" />
                                                                    <asp:BoundField DataField="Gy" HeaderText="Ay / Gy (g)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.000000000}" />
                                                                    <asp:BoundField DataField="Gz" HeaderText="Az / Gz (g)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.000000000}" />
                                                                    <asp:BoundField DataField="Bx" HeaderText="Mx / Bx (nT)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                    <asp:BoundField DataField="By" HeaderText="My / By (nT)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                    <asp:BoundField DataField="Bz" HeaderText="Mz / Bz (nT)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                    <div id="divLenU" runat="server" class="form-group">
                                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="text-align: right; align-content: center; vertical-align: middle;">
                                                            <asp:Label ID="lblDepthU" runat="server" CssClass="control-label label-default" Font-Bold="true" Font-Size="Larger" Text="Input Depth Units" />
                                                        </div>
                                                        <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                                            <div id="divRDLenU" runat="server">
                                                                <asp:RadioButton ID="rdbFT" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Feet" OnCheckedChanged="rdbFT_CheckedChanged" />
                                                                <asp:RadioButton ID="rdbMT" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Meter" OnCheckedChanged="rdbMT_CheckedChanged" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="divAclU" runat="server" class="form-group">
                                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="text-align: right; align-content: center; vertical-align: middle;">
                                                            <asp:Label ID="lblAclU" runat="server" CssClass="control-label label-default" Font-Bold="true" Font-Size="Larger" Text="Input Accelerometer Units" />
                                                        </div>
                                                        <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                                            <div id="divRDAclU" runat="server">
                                                                <asp:RadioButton ID="rdbCMS" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Accl. due to Gravity (cm/sec. sq)"
                                                                    OnCheckedChanged="rdbCMS_CheckedChanged" />
                                                                <asp:RadioButton ID="rdbFSC" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Accl. due to Gravity (ft/sec. sq)"
                                                                    OnCheckedChanged="rdbFSC_CheckedChanged" />
                                                                <asp:RadioButton ID="rdbG" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Accl. due to Gravity (g)"
                                                                    OnCheckedChanged="rdbG_CheckedChanged" />
                                                                <asp:RadioButton ID="rdbMSC" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Accl. due to Gravity (m/sec. sq)"
                                                                    OnCheckedChanged="rdbMSC_CheckedChanged" />
                                                                <asp:RadioButton ID="rdbMG" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Accl. due to Gravity (mg)"
                                                                    OnCheckedChanged="rdbMG_CheckedChanged" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="divMagU" runat="server" class="form-group">
                                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="text-align: right; align-content: center; vertical-align: middle;">
                                                            <asp:Label ID="lblMagU" runat="server" CssClass="control-label label-default" Font-Bold="true" Font-Size="Larger" Text="Input Magnetometer Units" />
                                                        </div>
                                                        <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                                            <div id="divRDMagU" runat="server">
                                                                <asp:RadioButton ID="rdbGss" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Gauss (G)"
                                                                    OnCheckedChanged="rdbGss_CheckedChanged" />
                                                                <asp:RadioButton ID="rdbT" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Tesla (T)" OnCheckedChanged="rdbT_CheckedChanged" />
                                                                <asp:RadioButton ID="rdbMLIT" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Milli Tesla (mT)"
                                                                    OnCheckedChanged="rdbMLIT_CheckedChanged" />
                                                                <asp:RadioButton ID="rdbMCT" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="MicroTesla (µT)"
                                                                    OnCheckedChanged="rdbMCT_CheckedChanged" />
                                                                <asp:RadioButton ID="rdbNT" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="NanoTesla (nT)"
                                                                    OnCheckedChanged="rdbNT_CheckedChanged" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="divSensors" runat="server" class="form-group">
                                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="text-align: right; align-content: center; vertical-align: middle;">
                                                            <asp:Label ID="lbl_Flip" runat="server" CssClass="control-label label-default" Font-Bold="true" Font-Size="Larger" Text="Select Sensor(s) for data transformation" />
                                                        </div>
                                                        <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                                            <div id="divChecks" runat="server">
                                                                <fieldset>
                                                                    <asp:CheckBox ID="chk_NoSignTransformation" runat="server" Text="None" CssClass="checkbox-inline" CausesValidation="True"
                                                                        AutoPostBack="True" OnCheckedChanged="chk_NoSignTransformation_CheckedChanged" />
                                                                    <asp:CheckBox ID="chk_FlipGx" runat="server" Text="Gx" CssClass="checkbox-inline" Checked="false" AutoPostBack="true"
                                                                        OnCheckedChanged="chk_FlipGx_CheckedChanged" />
                                                                    <asp:CheckBox ID="chk_FlipGy" runat="server" Text="Gy" CssClass="checkbox-inline" Checked="false" AutoPostBack="true"
                                                                        OnCheckedChanged="chk_FlipGy_CheckedChanged" />
                                                                    <asp:CheckBox ID="chk_FlipGz" runat="server" Text="Gz" CssClass="checkbox-inline" Checked="false" AutoPostBack="true"
                                                                        OnCheckedChanged="chk_FlipGz_CheckedChanged" />
                                                                    <asp:CheckBox ID="chk_FlipBx" runat="server" Text="Bx" CssClass="checkbox-inline" Checked="false" AutoPostBack="true"
                                                                        OnCheckedChanged="chk_FlipBx_CheckedChanged" />
                                                                    <asp:CheckBox ID="chk_FlipBy" runat="server" Text="By" CssClass="checkbox-inline" Checked="false" AutoPostBack="true"
                                                                        OnCheckedChanged="chk_FlipBy_CheckedChanged" />
                                                                    <asp:CheckBox ID="chk_FlipBz" runat="server" Text="Bz" CssClass="checkbox-inline" Checked="false" AutoPostBack="true"
                                                                        OnCheckedChanged="chk_FlipBz_CheckedChanged" />
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divTransformData" runat="server" visible="false" class="panel-body">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                    <asp:GridView ID="gvwSelectedRaw" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="drID"
                                                                        SelectedRowStyle-CssClass="selectedrow" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                                        EmptyDataText="No Data Uploaded" AllowPaging="True" AllowSorting="True" PagerStyle-CssClass="pager" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                        OnPageIndexChanging="gvwSelectedRaw_PageIndexChanging" OnRowDataBound="gvwSelectedRaw_RowDataBound" Visible="false">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="Selected" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="srvyCheck" runat="server" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="drID" Visible="False" />
                                                                            <asp:BoundField DataField="Depth" HeaderText="Depth (m)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                            <asp:BoundField DataField="rGx" HeaderText="Ax / Gx (g)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: 0.000000000}" />
                                                                            <asp:BoundField DataField="rGy" HeaderText="Ay / Gy (g)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: 0.000000000}" />
                                                                            <asp:BoundField DataField="rGz" HeaderText="Az / Gz (g)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: 0.000000000}" />
                                                                            <asp:BoundField DataField="rBx" HeaderText="Mx / Bx (nT)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                            <asp:BoundField DataField="rBy" HeaderText="My / By (nT)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                            <asp:BoundField DataField="rBz" HeaderText="Mz / Bz (nT)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                    <asp:GridView ID="gvwRQCResults" runat="server" CssClass="mydatagrid" PageSize="1" AutoGenerateColumns="False" DataKeyNames="resID"
                                                                        Visible="False" SelectedRowStyle-CssClass="selectedrow"
                                                                        PagerStyle-CssClass="pager" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                        OnRowDataBound="gvwRQCResults_RowDataBound">
                                                                        <Columns>
                                                                            <asp:BoundField DataField="resID" HeaderText="resID" Visible="False" />
                                                                            <asp:BoundField DataField="srvyID" HeaderText="Survey ID" ItemStyle-HorizontalAlign="Center" Visible="false" />
                                                                            <asp:BoundField DataField="Depth" HeaderText="Survey Depth" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                            <asp:BoundField DataField="taSt" HeaderText="SMARTs Analysis" ItemStyle-HorizontalAlign="Left" Visible="false" />
                                                                            <asp:BoundField DataField="inc" HeaderText="Inclination°" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                            <asp:BoundField DataField="dip" HeaderText="Dip°" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                            <asp:BoundField DataField="bT" HeaderText="B-Total (nT)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                            <asp:BoundField DataField="gT" HeaderText="G-Total (g)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                    <asp:GridView ID="gvwRQCAzimuths" runat="server" CssClass="mydatagrid" PageSize="1" AutoGenerateColumns="False" DataKeyNames="resID"
                                                                        Visible="False" SelectedRowStyle-CssClass="selectedrow"
                                                                        PagerStyle-CssClass="pager" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle">
                                                                        <Columns>
                                                                            <asp:BoundField DataField="resID" HeaderText="resID" Visible="False" />
                                                                            <asp:BoundField DataField="rawAzm" HeaderText="Azimuth°" ItemStyle-HorizontalAlign="Right" />
                                                                            <asp:BoundField DataField="lcAzm" HeaderText="Long-Collar Corrected Azimuth°" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                            <asp:BoundField DataField="scAzm" HeaderText="Short-Collar Corrected Azimuth°" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                    <asp:GridView ID="gvwPlacement" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                                        DataKeyNames="srvID" SelectedRowStyle-BackColor="#CCFFCC" HeaderStyle-CssClass="header" ShowHeaderWhenEmpty="true"
                                                                        RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle" EmptyDataText="No Placement values found for selected depth">
                                                                        <Columns>
                                                                            <asp:BoundField HeaderText="srvID" Visible="false" DataField="srvID" />
                                                                            <asp:BoundField HeaderText="TVD" DataField="tvd" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                            <asp:BoundField HeaderText="North" DataField="north" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                            <asp:BoundField HeaderText="East" DataField="east" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #.00}" />
                                                                            <asp:BoundField HeaderText="Vertical Section" DataField="vs" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                            <asp:BoundField HeaderText="Subsea Elevation" DataField="subsea" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.0000}" />
                                                                            <asp:BoundField HeaderText="Dogleg" DataField="dls" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.0000}" />
                                                                            <asp:BoundField HeaderText="Northing" DataField="northing" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                            <asp:BoundField HeaderText="Easting" DataField="easting" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                        <div id="divWellDepthInfo">
                                                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                                                <div runat="server" id="enWDInfo" visible="false">
                                                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                                    <h4><i runat="server" id="iWDInfo"><span runat="server" id="spnWDInfo"></span></i></h4>
                                                                                    <asp:Label runat="server" ID="lblWDInfo" Text="" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div id="divRunDepthInfo">
                                                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                                                <div runat="server" id="enRDInfo" visible="false">
                                                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                                    <h4><i runat="server" id="iRDInfo"><span runat="server" id="spnRDInfo"></span></i></h4>
                                                                                    <asp:Label runat="server" ID="lblRDInfo" Text="" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="input-group" style="width: 100%;">
                                                        <asp:LinkButton ID="btnMetaCancel" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnMetaCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnQCManualUpload" runat="server" CssClass="btn btn-success text-center" Visible="false" OnClick="btnQCManualUpload_Click"><i class="fa fa-upload"> Upload Survey</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancelManualUpload" runat="server" CssClass="btn btn-warning text-center" Visible="false" OnClick="btnCancelManualUpload_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnProcessRawData" runat="server" CssClass="btn btn-default text-center" Enabled="false" Visible="false"
                                                            OnClick="btnProcessRawData_Click"><i class="fa fa-gears"> Process Raw Survey(s)</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnProcessSrvyCancel" runat="server" CssClass="btn btn-default text-center" Visible="false" OnClick="btnProcessSrvyCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnProcessReadyData" runat="server" CssClass="btn btn-success text-center" Visible="false" OnClick="btnProcessReadyData_Click"><i class="fa fa-life-ring"> Audit/QC</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancelReadyData" runat="server" CssClass="btn btn-warning text-center" Visible="false" OnClick="btnCancelReadyData_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnNextSurvey" runat="server" CssClass="btn btn-info text-center" Visible="false" OnClick="btnNextSurvey_Click"><i class="fa fa-upload"> Upload Next Survey</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnDetailCancel" runat="server" CssClass="btn btn-danger text-center" Visible="false" OnClick="btnDetailCancel_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnProcessReadyData" />
    </Triggers>
</asp:UpdatePanel>
