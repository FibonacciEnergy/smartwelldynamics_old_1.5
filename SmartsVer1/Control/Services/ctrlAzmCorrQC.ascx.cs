﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net.Mail;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartsVer1.Helpers.AccountHelpers;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using DI = SmartsVer1.Helpers.QCHelpers.DipIncMSA;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using DL = SmartsVer1.Helpers.DemogHelpers.DataLoader;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using OPR = SmartsVer1.Helpers.QCHelpers.Opr;
using CHRT = SmartsVer1.Helpers.ChartHelpers.Gauges;
using CHRTRes = SmartsVer1.Helpers.ChartHelpers.ChartResources;
using QC = SmartsVer1.QCHelpers.sdRawDataQC;
using Color = System.Drawing.Color;
using log4net;

namespace SmartsVer1.Control.Services
{
    public partial class ctrlAzmCorrQC : System.Web.UI.UserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ctrlAzmCorrQC));
        const Int32 ClientTypID = 4;
        const Int32 ServiceIDCorr = 47; //DemogDB Service Table ID
        const Int32 ServiceIDQC = 33; //DemogDB Service Table ID
        Int32 SysClientID = -99, jobCount = 0, wellCount = 0, runCount = 0, mrmCount = 0, mbhaCount = 0;
        String LoginName, username, domain, GetClientDBString;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UserProfile usrP = UserProfile.GetUserProfile(Membership.GetUser().UserName);
                SysClientID = Convert.ToInt32(usrP.clntID);
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientTypID);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = 0;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(SysClientID, LoginName);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");

                Int32 sStatus = DMG.getServiceAvailability(ServiceIDCorr);
                if (sStatus.Equals(2))
                {
                    //this.divMetaData.Visible = true;
                }
                else
                {
                    //this.divMetaData.Visible = false;
                }
                //this.createAssetCharts(null, null);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //protected void createAssetCharts(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        List<Int32> wellsDetails = CHRT.getClientRawQCSummary(GetClientDBString);
        //        jobCount = wellsDetails[0];
        //        wellCount = wellsDetails[1];
        //        mrmCount = wellsDetails[2];
        //        runCount = wellsDetails[3];
        //        mbhaCount = wellsDetails[4];
        //        this.divGuageJob.InnerHtml = "<h3>" + jobCount.ToString() + "</h3><span>Data Audit Job(s)</span>";
        //        this.divGuageWell.InnerHtml = "<h3>" + wellCount.ToString() + "</h3><span>Data Audit Well(s)</span>";
        //        this.divAscWell.InnerHtml = "<h3>" + mrmCount.ToString() + "</h3><span>Well(s) Missing Ref. Mags.</span>";
        //        this.divMRM.InnerHtml = "<h3>" + runCount.ToString() + "</h3><span>Data Audit Active Run</span>";
        //        this.divMsngBHA.InnerHtml = "<h3>" + mbhaCount.ToString() + "</h3><span>Active Run(s) Missing BHA Signature</span>";
        //    }
        //    catch (Exception ex)
        //    { throw new System.Exception(ex.ToString()); }
        //}

        protected void imgbtnSolar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://www.n3kl.org/sun/noaa.html");
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void imgbtnGeoMag_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://www.n3kl.org/sun/noaa.html");
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwGlobalOptr_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(SysClientID);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = e.NewPageIndex;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
                this.btnMetaCancel.CssClass = "btn btn-warning text-center";
                this.btnMetaCancel.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwGlobalOptr_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow dRow = this.gvwGlobalOptr.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.ancOprList.InnerHtml = "<strong> " + oName + "</strong>";
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                System.Data.DataTable padTable = AST.getClientWellPadTableForOperator(oprID, SysClientID);
                this.gvwWellPads.DataSource = padTable;
                this.gvwWellPads.PageIndex = 0;
                this.gvwWellPads.SelectedIndex = -1;
                this.gvwWellPads.DataBind();
                this.divPad.Visible = true;
                this.btnMetaCancel.CssClass = "btn btn-warning text-center";
                this.btnMetaCancel.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellPads_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                System.Data.DataTable padTable = AST.getClientWellPadTableForOperator(oprID, SysClientID);
                this.gvwWellPads.DataSource = padTable;
                this.gvwWellPads.PageIndex = e.NewPageIndex;
                this.gvwWellPads.SelectedIndex = -1;
                this.gvwWellPads.DataBind();
                this.btnMetaCancel.CssClass = "btn btn-warning text-center";
                this.btnMetaCancel.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellPads_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow oRow = this.gvwGlobalOptr.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                GridViewRow pRow = this.gvwWellPads.SelectedRow;
                String pName = Convert.ToString(Server.HtmlDecode(pRow.Cells[2].Text));
                Int32 pdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                System.Data.DataTable pdTable = AST.getWellForWellPadTable(pdID, GetClientDBString);
                this.gvwWellList.DataSource = pdTable;
                this.gvwWellList.PageIndex = 0;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();
                this.gvwWellList.Visible = true;
                this.btnMetaCancel.Enabled = true;
                this.btnMetaCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.divSectionList.Visible = true;
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                HttpContext.Current.Session["wellName"] = Convert.ToString(this.gvwWellList.SelectedRow.Cells[2].Text);
                //HttpContext.Current.Session["wellName"] = Convert.ToString(this.gvwWellList.SelectedRow.Cells[2].Text);
                System.Data.DataTable wsTable = OPR.getWellSectionToWellTable(wlID, GetClientDBString);
                this.gvwSectionList.DataSource = wsTable;
                this.gvwSectionList.DataBind();
                this.gvwSectionList.SelectedIndex = -1;
                this.divRunList.Visible = false;
                this.gvwRunList.DataBind();
                this.gvwRunList.SelectedIndex = -1;
                this.divDataEntryList.Visible = false;
                this.gvwDataEntryList.SelectedIndex = -1;
                this.gvwDataEntryList.DataBind();
                this.btnDetailCancel.Visible = true;
                GridViewRow dRow = this.gvwWellList.SelectedRow;
                String wName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.divWellList.Attributes["class"] = "panel-collapse collapse";
                this.divSectionList.Attributes["class"] = "panel-collapse collapse in";
                this.ancWellList.InnerHtml = "Well :<span style='text-decoration: underline; font-weight: bold'>" + wName + "</span>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSectionList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 wsID = Convert.ToInt32(this.gvwSectionList.SelectedValue);

                this.divRunList.Visible = true;
                System.Data.DataTable runList = OPR.getRunForSelectedWellSectionTable(wlID, wsID, GetClientDBString);
                this.gvwRunList.DataSource = runList;
                this.gvwRunList.DataBind();
                this.gvwRunList.SelectedIndex = -1;
                this.divDataEntryList.Visible = false;
                this.gvwDataEntryList.DataBind();
                GridViewRow dRow = this.gvwSectionList.SelectedRow;
                String sName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.divSectionList.Attributes["class"] = "panel-collapse collapse";
                this.divRunList.Attributes["class"] = "panel-collapse collapse in";
                this.ancSection.InnerHtml = "Well Section :<span style='text-decoration: underline; font-weight: bold'>" + sName + "</span>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSectionList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                System.Data.DataTable wsTable = OPR.WellSectionNameToWellTable(wlID, GetClientDBString);
                this.gvwSectionList.DataSource = wsTable;
                this.gvwSectionList.PageIndex = e.NewPageIndex;
                this.gvwSectionList.DataBind();
                this.gvwSectionList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRunList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.divDataEntryList.Visible = true;
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 rID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                System.Data.DataTable demTable = AST.getDataEntryMethodsList();
                this.gvwDataEntryList.DataSource = demTable;
                this.gvwDataEntryList.DataBind();
                this.btnDetailCancel.Visible = true;
                this.btnDeleteWellData.Enabled = true;
                GridViewRow dRow = this.gvwRunList.SelectedRow;
                String rName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.divRunList.Attributes["class"] = "panel-collapse collapse";
                this.divDataEntryList.Attributes["class"] = "panel-collapse collapse in";
                this.ancRun.InnerHtml = "Run :<span style='text-decoration: underline; font-weight: bold'>" + rName + "</span>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRunList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 wsID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                System.Data.DataTable runList = OPR.getRunForSelectedWellSectionTable(wlID, wsID, GetClientDBString);
                this.gvwRunList.DataSource = runList;
                this.gvwRunList.PageIndex = e.NewPageIndex;
                this.gvwRunList.DataBind();
                this.gvwRunList.Visible = true;
                this.gvwRunList.SelectedIndex = -1;
                this.gvwRunList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwDataEntryList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                if (this.gvwDataEntryList.SelectedValue.Equals(1))
                {
                    GridViewRow dRow = this.gvwDataEntryList.SelectedRow;
                    String deName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                    this.divDataEntryList.Attributes["class"] = "panel-collapse collapse";
                    this.ancDE.InnerHtml = "Data Entry Method :<span style='text-decoration: underline; font-weight: bold'>" + deName + "</span>";
                    this.LoadData.Visible = true;
                    this.divManData.Visible = true;
                    this.divFileData.Visible = false;
                    this.txtQCManualProcess.Text = "";
                    this.txtQCManualProcess.Focus();
                    this.btnMetaCancel.Visible = false;
                    this.btnCancelManualUpload.Visible = true;
                    this.btnCancelFileUpload.Visible = false;
                    this.btnDeleteWellData.Visible = false;
                    this.divDone.Visible = true;
                }
                else
                {
                    GridViewRow dRow = this.gvwDataEntryList.SelectedRow;
                    String deName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                    this.divDataEntryList.Attributes["class"] = "panel-collapse collapse";
                    this.ancDE.InnerHtml = "Data Entry Method :<span style='text-decoration: underline; font-weight: bold'>" + deName + "</span>";
                    this.LoadData.Visible = true;
                    this.divManData.Visible = false;
                    this.divFileData.Visible = true;
                    this.btnMetaCancel.Visible = false;
                    this.btnDeleteWellData.Visible = false;
                    this.btnCancelManualUpload.Visible = false;
                    this.btnCancelFileUpload.Visible = true;
                    this.divDone.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        protected void btnMetaCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.divWellList.Visible = false;
                this.gvwWellList.DataBind();
                this.gvwWellList.SelectedIndex = -1;
                this.divSectionList.Visible = false;
                this.gvwSectionList.DataBind();
                this.gvwSectionList.SelectedIndex = -1;
                this.divRunList.Visible = false;
                this.gvwRunList.DataBind();
                this.gvwRunList.SelectedIndex = -1;
                this.divDataEntryList.Visible = false;
                this.gvwDataEntryList.DataBind();
                this.gvwDataEntryList.SelectedIndex = -1;
                this.ancWellList.InnerHtml = "Well";
                this.ancSection.InnerHtml = "Well Section";
                this.ancRun.InnerHtml = "Run";
                this.ancDE.InnerHtml = "Data Entry Method";
                this.wdEnclosure.Visible = false;
                this.btnDeleteWellData.Enabled = false;
                this.btnDeleteWellData.Visible = false;
                this.btnMetaCancel.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCancelFileUpload_Click(object sender, EventArgs e)
        {
            try
            {
                this.FileUploadControl.Dispose();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }


        protected void btnQCManualUpload_Click(object sender, EventArgs e)
        {
            try
            {
                // Variables and initializing them
                System.Data.DataTable iResult = new System.Data.DataTable(), headTable = new System.Data.DataTable();
                List<System.Data.DataTable> dataTables = new List<System.Data.DataTable>();
                Int32 jD = -99, wD = -99, RunID = -99, sqcID = -99;
                String dataRowText = Convert.ToString(this.txtQCManualProcess.Text);

                if (String.IsNullOrEmpty(dataRowText))
                {
                    this.lblManual.Text = " Please enter Raw Survey Data row before proceeding";
                    this.lblManual.Visible = true;
                    this.lblManual.ForeColor = Color.Red;
                    this.txtQCManualProcess.Text = "";

                    this.txtQCManualProcess.Focus();

                }
                else    // Else Block if the text box is not empty
                {
                    //iResult = DL.uploadManualRawDataRow(ServiceIDCorr, jD, wD, RunID, sqcID, dataRowText, GetClientDBString);
                    //dataTables.Add(headTable);
                    //dataTables.Add(iResult);
                    //HttpContext.Current.Session["rawDataTables"] = dataTables;
                    //HttpContext.Current.Session["dataEntryMethod"] = 1; //Manual Data Entry
                    //this.divManData.Visible = false;
                    //this.gvwViewManualDisplay.DataSource = iResult;
                    //this.gvwViewManualDisplay.DataBind();

                    //this.gvwViewManualDisplay.Focus();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwViewManualDisplay_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                List<System.Data.DataTable> dataTables = new List<System.Data.DataTable>();
                dataTables = (List<System.Data.DataTable>)HttpContext.Current.Session["rawDataTables"];
                System.Data.DataTable rawTable = dataTables[1];
                this.gvwViewManualDisplay.DataSource = rawTable;
                this.gvwViewManualDisplay.PageIndex = e.NewPageIndex;
                this.gvwViewManualDisplay.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCancelManualUpload_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtQCManualProcess.Text = null;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btn_FileUpload_Click(object sender, EventArgs e)
        {
            try
            {
                List<System.Data.DataTable> iReply = new List<System.Data.DataTable>();
                Int32 wID = -99, secD = -99, jID = -99, rID = -99, demID = -99;
                String fileName, pathToFile, dfName, dfExtension;
                List<String> noDataLines = new List<String>();
                Dictionary<String, String> headKeyValue = new Dictionary<String, String>(), dataLines = new Dictionary<String, String>();

                if (FileUploadControl.HasFile)
                {
                    try
                    {
                        wID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                        secD = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                        rID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                        pathToFile = WebConfigurationManager.AppSettings["tempFiles"];
                        fileName = Path.Combine(pathToFile, FileUploadControl.FileName);
                        dfName = Path.GetFileNameWithoutExtension(fileName);
                        dfExtension = Path.GetExtension(fileName);
                        if (dfExtension.Equals(".txt") || dfExtension.Equals(".csv"))
                        {
                            if (dfExtension.Equals(".txt"))
                            { demID = 4; }
                            else { demID = 2; }
                            logger.Info("User: " + username + " Uploading File: " + FileUploadControl.FileName + " UploadPath: " + pathToFile);
                            // Save the file to our local path
                            FileUploadControl.SaveAs(fileName);

                            if (File.Exists(fileName))
                            {
                                iReply = DL.uploadDataTextFile(wID, secD, rID, dfName, dfExtension, fileName, ServiceIDCorr, demID, LoginName, domain, GetClientDBString);
                                HttpContext.Current.Session["rawDataTables"] = iReply;
                                HttpContext.Current.Session["fileName"] = dfName;
                                HttpContext.Current.Session["fileExtension"] = dfExtension;
                                HttpContext.Current.Session["dataEntryMethod"] = demID; //Date Entry method = 2
                                System.Data.DataTable dataTable = new System.Data.DataTable();
                                dataTable = iReply[1];
                                this.gvwViewManualDisplay.DataSource = dataTable;
                                this.gvwViewManualDisplay.DataBind();
                                this.divListBoxUploadedData.Visible = true;
                                this.rawDEnclosure.Visible = true;
                                this.rawDEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                                this.iRDE.Attributes["class"] = "icon fa fa-check-circle";
                                this.spnRDE.InnerText = " Success!";
                                this.FileUploadStatusLabel.Text = "Successfully uploaded Raw Data File";
                                this.btnProcessRawData.Visible = true;
                                logger.Info("User: " + username + " FileUpload: " + fileName + " Status: Successful!");
                                FileUploadControl.Dispose();
                                this.divFileData.Visible = false;
                            }
                            else
                            {
                                this.rawDEnclosure.Visible = true;
                                this.rawDEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iRDE.Attributes["class"] = "icon fa fa-warning";
                                this.spnRDE.InnerText = " Error!";
                                this.FileUploadStatusLabel.Text = "Failure!!! Unable to upload Raw Data File";
                                this.divListBoxUploadedData.Visible = false;
                                logger.Info("User: " + username + " FileUpload: " + fileName + " Status: Failure");
                                FileUploadControl.Dispose();
                            }
                        }
                        else
                        {
                            this.rawDEnclosure.Visible = true;
                            this.rawDEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                            this.iRDE.Attributes["class"] = "icon fa fa-danger";
                            this.spnRDE.InnerText = " !!! Error !!!";
                            this.FileUploadStatusLabel.Text = "!!! File Type ERROR !!! (only .txt or .csv files are allowed)";
                            this.divListBoxUploadedData.Visible = false;
                            FileUploadControl.Dispose();
                        }
                    }
                    catch (Exception ex)
                    {
                        this.rawDEnclosure.Visible = true;
                        this.rawDEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                        this.iRDE.Attributes["class"] = "icon fa fa-danger";
                        this.spnRDE.InnerText = " !!! Error !!!";
                        this.FileUploadStatusLabel.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
                        this.divListBoxUploadedData.Visible = false;
                        logger.Fatal("User: " + username + " Fatal Error During File Upload: " + FileUploadStatusLabel.Text);
                        FileUploadControl.Dispose();
                    }
                }
                else
                {
                    this.rawDEnclosure.Visible = true;
                    this.rawDEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                    this.iRDE.Attributes["class"] = "icon fa fa-danger";
                    this.spnRDE.InnerText = " !!! Error !!!";
                    this.FileUploadStatusLabel.Text = "Upload status: Specify the file to be uploaded ";
                    this.divListBoxUploadedData.Visible = false;
                    this.btnCancelFileUpload.Visible = true;
                }

                this.btnCancelFileUpload.Visible = false;
                this.btnProcessSrvyCancel.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProcessRawData_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable iResult = new System.Data.DataTable();
                Int32 dataMethod, jD, wD, wS, RunID, chk_FlipGx, chk_FlipGy, chk_FlipGz, chk_FlipBx, chk_FlipBy, chk_FlipBz;
                wD = Convert.ToInt32(this.gvwWellList.SelectedValue);
                wS = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                RunID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                dataMethod = Convert.ToInt32(HttpContext.Current.Session["dataEntryMethod"]);
                if (this.chk_FlipGx.Checked) { chk_FlipGx = -1; } else { chk_FlipGx = 1; }
                if (this.chk_FlipGy.Checked) { chk_FlipGy = -1; } else { chk_FlipGy = 1; }
                if (this.chk_FlipGz.Checked) { chk_FlipGz = -1; } else { chk_FlipGz = 1; }
                if (this.chk_FlipBx.Checked) { chk_FlipBx = -1; } else { chk_FlipBx = 1; }
                if (this.chk_FlipBy.Checked) { chk_FlipBy = -1; } else { chk_FlipBy = 1; }
                if (this.chk_FlipBz.Checked) { chk_FlipBz = -1; } else { chk_FlipBz = 1; }
                List<System.Data.DataTable> dataTables = new List<System.Data.DataTable>(), readyTables = new List<System.Data.DataTable>();
                dataTables = (List<System.Data.DataTable>)HttpContext.Current.Session["rawDataTables"];
                System.Data.DataTable rawdataTable = new System.Data.DataTable(), fileHead = new System.Data.DataTable(), fileBody = new System.Data.DataTable();
                fileHead = (System.Data.DataTable)dataTables[0];
                fileBody = (System.Data.DataTable)dataTables[1];
                rawdataTable = (System.Data.DataTable)dataTables[2];
                foreach (DataRow rw in rawdataTable.Rows)
                {
                    rw.SetField("Gx", Convert.ToDecimal(rw["Gx"]) * Convert.ToDecimal(chk_FlipGx));
                    rw.SetField("Gy", Convert.ToDecimal(rw["Gy"]) * Convert.ToDecimal(chk_FlipGy));
                    rw.SetField("Gz", Convert.ToDecimal(rw["Gz"]) * Convert.ToDecimal(chk_FlipGz));
                    rw.SetField("Bx", Convert.ToDecimal(rw["Bx"]) * Convert.ToDecimal(chk_FlipBx));
                    rw.SetField("By", Convert.ToDecimal(rw["By"]) * Convert.ToDecimal(chk_FlipBy));
                    rw.SetField("Bz", Convert.ToDecimal(rw["Bz"]) * Convert.ToDecimal(chk_FlipBz));
                }
                rawdataTable.AcceptChanges();
                HttpContext.Current.Session["readyData"] = rawdataTable;
                String dFile = Convert.ToString(HttpContext.Current.Session["fileName"]);
                String dExt = Convert.ToString(HttpContext.Current.Session["fileExtension"]);
                readyTables.Add(fileHead);
                readyTables.Add(fileBody);
                readyTables.Add(rawdataTable);
                HttpContext.Current.Session["rdTables"] = readyTables;
                
                Int32 welID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                //ArrayList rfList = OPR.getRefMagList(welID, GetClientDBString);
                Dictionary<Int32, String> ornValue = new Dictionary<Int32, String>();
                Int32 iDecO = -99;
                String decOrient = String.Empty;
                Decimal iDip = -99.0000M, iDec = -99.0000M, iBT = -99.0M, iGT = -99.0000M;
                ////////////////////////////////

                //Update  Reference Magnetics fetch routine required OperatorID, ClientID AND WellPadID

                //////////////////////////////



                //iDec = Convert.ToDecimal(rfList[7]);
                //iDecO = Convert.ToInt32(rfList[8]);
                //decOrient = DMG.getCardinalName(iDecO);
                //iDip = Convert.ToDecimal(rfList[9]);
                //iBT = Convert.ToDecimal(rfList[10]);
                //iGT = Convert.ToDecimal(rfList[11]);
                ornValue.Add(iDecO, decOrient);
                this.txtWellDip.Text = iDip.ToString();
                this.txtWellDec.Text = iDec.ToString();
                this.ddlWellDecOrient.DataSource = ornValue;
                this.ddlWellDecOrient.DataValueField = "Key";
                this.ddlWellDecOrient.DataTextField = "Value";
                this.ddlWellDecOrient.DataBind();
                this.txtWellBTotal.Text = iBT.ToString();
                this.txtWellGTotal.Text = iGT.ToString();
                this.divReadyData.Visible = true;
                this.gvwReadyData.DataSource = rawdataTable;
                this.gvwReadyData.DataBind();
                this.btnProcessRawData.Visible = false;
                this.btnProcessSrvyCancel.Visible = false;
                HttpContext.Current.Session["dtMethod"] = dataMethod;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbWell_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbWell.Checked = true;
                this.txtWellDip.Enabled = true;
                this.txtWellDec.Enabled = true;
                this.txtWellBTotal.Enabled = true;
                this.txtWellGTotal.Enabled = true;
                this.rdbNOAA.Checked = false;
                this.txtNOAADip.Enabled = false;
                this.txtNOAADec.Enabled = false;
                this.ddlNOAAOrientation.Items.Clear();
                this.ddlNOAAOrientation.DataSource = null;
                this.ddlNOAAOrientation.DataBind();
                this.ddlNOAAOrientation.Items.Insert(0, new ListItem("--- Select Orientation", "-1"));
                this.ddlNOAAOrientation.SelectedIndex = -1;
                this.ddlNOAAOrientation.Enabled = false;
                this.txtNOAABTotal.Enabled = false;
                this.txtNOAAGTotal.Enabled = false;
                this.rdbBGS.Checked = false;
                this.txtBGSDip.Enabled = false;
                this.txtBGSDec.Enabled = false;
                this.ddlBGSOrientation.Items.Clear();
                this.ddlBGSOrientation.DataSource = null;
                this.ddlBGSOrientation.DataBind();
                this.ddlBGSOrientation.Items.Insert(0, new ListItem("--- Select Orientation", "-1"));
                this.ddlBGSOrientation.SelectedIndex = -1;
                this.ddlBGSOrientation.Enabled = false;
                this.txtBGSBTotal.Enabled = false;
                this.txtBGSGTotal.Enabled = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwReadyData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable rdData = (System.Data.DataTable)HttpContext.Current.Session["readyData"];
                this.gvwReadyData.DataSource = rdData;
                this.gvwReadyData.PageIndex = e.NewPageIndex;
                this.gvwReadyData.SelectedIndex = -1;
                this.gvwReadyData.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwReadyData_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbNOAA_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbWell.Checked = false;
                this.txtWellDip.Enabled = false;
                this.txtWellDec.Enabled = false;
                this.txtWellBTotal.Enabled = false;
                this.txtWellGTotal.Enabled = false;
                this.rdbNOAA.Checked = true;
                this.txtNOAADip.Enabled = true;
                this.txtNOAADec.Enabled = true;
                Dictionary<Int32, String> ornValue = DMG.getLngCardinals();
                this.ddlNOAAOrientation.DataSource = ornValue;
                this.ddlNOAAOrientation.DataTextField = "Value";
                this.ddlNOAAOrientation.DataValueField = "Key";
                this.ddlNOAAOrientation.Items.Insert(0, new ListItem("--- Select Orientation ---", "-1"));
                this.ddlNOAAOrientation.DataBind();
                this.ddlNOAAOrientation.SelectedIndex = -1;
                this.ddlNOAAOrientation.Enabled = true;
                this.txtNOAABTotal.Enabled = true;
                this.txtNOAAGTotal.Enabled = true;
                this.rdbBGS.Checked = false;
                this.txtBGSDip.Enabled = false;
                this.txtBGSDec.Enabled = false;
                this.ddlBGSOrientation.Items.Clear();
                this.ddlBGSOrientation.DataSource = null;
                this.ddlBGSOrientation.DataBind();
                this.ddlBGSOrientation.Items.Insert(0, new ListItem("--- Select Orientation", "-1"));
                this.ddlBGSOrientation.SelectedIndex = -1;
                this.ddlBGSOrientation.Enabled = false;
                this.txtBGSBTotal.Enabled = false;
                this.txtBGSGTotal.Enabled = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbBGS_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbWell.Checked = false;
                this.txtWellDip.Enabled = false;
                this.txtWellDec.Enabled = false;
                this.txtWellBTotal.Enabled = false;
                this.txtWellGTotal.Enabled = false;
                this.rdbNOAA.Checked = false;
                this.txtNOAADip.Enabled = false;
                this.txtNOAADec.Enabled = false;
                this.ddlNOAAOrientation.Items.Clear();
                this.ddlNOAAOrientation.DataSource = null;
                this.ddlNOAAOrientation.DataBind();
                this.ddlNOAAOrientation.Items.Insert(0, new ListItem("--- Select Orientation", "-1"));
                this.ddlNOAAOrientation.SelectedIndex = -1;
                this.ddlNOAAOrientation.Enabled = false;
                this.txtNOAABTotal.Enabled = false;
                this.txtNOAAGTotal.Enabled = false;
                this.rdbBGS.Checked = true;
                this.txtBGSDip.Enabled = true;
                this.txtBGSDec.Enabled = true;
                Dictionary<Int32, String> ornValue = DMG.getLngCardinals();
                this.ddlBGSOrientation.DataSource = ornValue;
                this.ddlBGSOrientation.DataTextField = "Value";
                this.ddlBGSOrientation.DataValueField = "Key";
                this.ddlBGSOrientation.Items.Insert(0, new ListItem("--- Select Orientation ---", "-1"));
                this.ddlBGSOrientation.DataBind();
                this.ddlBGSOrientation.SelectedIndex = -1;
                this.ddlBGSOrientation.Enabled = true;
                this.txtBGSBTotal.Enabled = true;
                this.txtBGSGTotal.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProcessReadyData_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iReply5 = -99, operatorID = -99, wellpadID = -99, wellID = -99, wellsectionID, runID = -99;
                ArrayList iResult = new ArrayList();
                System.Data.DataTable iResultData = new System.Data.DataTable();
                Int32 dataMethod = Convert.ToInt32(HttpContext.Current.Session["dtMethod"]);
                String dFile = Convert.ToString(HttpContext.Current.Session["fileName"]);
                String dExt = Convert.ToString(HttpContext.Current.Session["fileExtension"]);
                operatorID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                wellpadID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                wellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                wellsectionID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                runID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                List<System.Data.DataTable> dataTables = new List<System.Data.DataTable>();
                dataTables = (List<System.Data.DataTable>)HttpContext.Current.Session["rdTables"];                               
                
                iResult = DI.processRawData(dataMethod, ServiceIDCorr, 1, dFile, dExt, operatorID, SysClientID, wellpadID, wellID, wellsectionID, runID, dataTables, LoginName, domain, GetClientDBString);                
                iReply5 = Convert.ToInt32(iResult[0]);
                if (iReply5.Equals(-1))
                {
                    this.div5Surveys.Visible = true;
                    this.div5Enclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.i5.Attributes["class"] = "icon fa fa-warning";
                    this.spn5.InnerText = " Warning!";
                    this.lbl5.Text = " Minimum Survey Count not met. (Require atleast 5 Survey(s) before analysis)";
                    this.div5Enclosure.Visible = true;
                }
                else if (iReply5 > 1 && iReply5 < 12)
                {
                    this.div5Surveys.Visible = true;
                    this.div5Enclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.i5.Attributes["class"] = "icon fa fa-warning";
                    this.spn5.InnerText = " Warning!";
                    this.lbl5.Text = " Minimum Survey Count not met. (Require atleast 5 Survey(s) before analysis)";
                    this.div5Enclosure.Visible = true;
                }
                else
                {
                    this.div5Surveys.Visible = true;
                    this.div5Enclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                    this.i5.Attributes["class"] = "icon fa fa-check-circle";
                    this.spn5.InnerText = " Success";
                    this.lbl5.Text = " Minimum Survey requirement met";
                    this.div5Enclosure.Visible = true;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCancelReadyData_Click(object sender, EventArgs e)
        {
            try
            {
                HttpContext.Current.Session["rawDataTables"] = null;
                HttpContext.Current.Session["InputData"] = null;
                this.gvwReadyData.DataSource = null;
                this.gvwReadyData.DataBind();
                this.divReadyData.Visible = false;
                this.btnProcessRawData.Visible = true;
                this.btnProcessSrvyCancel.Visible = true;
                this.LoadData.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProceed5_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable iResult = new System.Data.DataTable();
                List<Double> stdDevs = new List<Double> { };
                Double lcDev, scDev;
                Int32 JobID = -99;
                Int32 WellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 SectionID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                Int32 RunID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                System.Data.DataTable qcData = DI.getIncDipInputQCData(JobID, WellID, SectionID, RunID, GetClientDBString);
                stdDevs = DI.getQCStandardDeviations(qcData);
                lcDev = Convert.ToDouble(stdDevs[0]);
                scDev = Convert.ToDouble(stdDevs[1]);
                HttpContext.Current.Session["inputTable"] = qcData;
                this.qcdatacharts_Click(qcData);
                this.gvwInputData.DataSource = qcData;
                this.gvwInputData.DataBind();
                this.gvwInputData.SelectedIndex = -1;
                this.lblLCDevVal.Text = lcDev.ToString("0.00");
                this.lblSCDevVal.Text = scDev.ToString("0.00");
                this.divQCData.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        protected void btnProceed5Clear_Click(object sender, EventArgs e)
        {
            try
            {
                this.div5Surveys.Visible = false;
                this.div5Enclosure.Visible = false;
                this.btnCancelReadyData_Click(null, null);
                this.divReadyData.Visible = false;
                this.btnProcessSrvyCancel_Click(null, null);
                this.LoadData.Visible = false;
                this.btnMetaCancel_Click(null, null);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwInputData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {}
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwInputData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable dataTable = (System.Data.DataTable)HttpContext.Current.Session["inputTable"];
                this.gvwInputData.PageIndex = e.NewPageIndex;
                this.gvwInputData.SelectedIndex = -1;
                this.gvwInputData.DataSource = dataTable;
                this.gvwInputData.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAnalyze_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable iResult = new System.Data.DataTable();
                Int32 OperatorID = -99, WellPadID = -99, WellID = -99, SecID = -99, RunID = -99;
                List<Double> stdDevs = new List<Double> { };
                Double lcDev, scDev, crDev;
                OperatorID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                WellPadID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                WellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                SecID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                RunID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                List<KeyValuePair<String, Decimal>> ifr = new List<KeyValuePair<String, Decimal>>();
                ifr = (List<KeyValuePair<String, Decimal>>)HttpContext.Current.Session["ifrVals"];
                iResult = DI.getSolution(ServiceIDCorr, OperatorID, SysClientID, WellPadID, WellID, SecID, RunID, GetClientDBString);
                this.divSol.Visible = true;
                this.crrdatacharts_Click(iResult);
                stdDevs = DI.getSolStandardDeviations(iResult);
                lcDev = Convert.ToDouble(stdDevs[0]);
                scDev = Convert.ToDouble(stdDevs[1]);
                crDev = Convert.ToDouble(stdDevs[2]);
                this.gvwSol.DataSource = iResult;
                this.gvwSol.DataBind();
                HttpContext.Current.Session["solDataTable"] = iResult;
                this.lblSolLCVal.Text = lcDev.ToString("0.00");
                this.lblSolSCVal.Text = scDev.ToString("0.00");
                this.lblSolCRVal.Text = crDev.ToString("0.00");
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCancelAnalysis_Click(object sender, EventArgs e)
        {
            try
            {
                this.divQCData.Visible = false;
                this.gvwInputData.DataSource = null;
                this.gvwInputData.DataBind();
                HttpContext.Current.Session["inputTable"] = null;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProcessSrvyCancel_Click(object sender, EventArgs e)
        {
            try
            {
                HttpContext.Current.Session["fileName"] = null;
                HttpContext.Current.Session["fileExtension"] = null;
                HttpContext.Current.Session["rawDataTables"] = null;
                HttpContext.Current.Session["InputData"] = null;
                this.FileUploadControl.Dispose();
                this.FileUploadStatusLabel.Text = null;
                this.chk_NoSignTransformation.Checked = true;
                this.chk_NoSignTransformation.Enabled = true;
                this.chk_FlipBx.Checked = false;
                this.chk_FlipBx.Enabled = false;
                this.chk_FlipBy.Checked = false;
                this.chk_FlipBy.Enabled = false;
                this.chk_FlipBz.Checked = false;
                this.chk_FlipBz.Enabled = false;
                this.chk_FlipGx.Checked = false;
                this.chk_FlipGx.Enabled = false;
                this.chk_FlipGy.Checked = false;
                this.chk_FlipGy.Enabled = false;
                this.chk_FlipGz.Checked = false;
                this.chk_FlipGz.Enabled = false;
                this.divManData.Visible = false;
                this.btnCancelManualUpload_Click(null, null);
                this.btnCancelFileUpload_Click(null, null);
                this.gvwViewManualDisplay.DataSource = null;
                this.gvwViewManualDisplay.DataBind();
                this.btnProcessRawData.Visible = false;
                this.btnProcessSrvyCancel.Visible = false;
                this.FileUploadControl.Focus();
                this.rawDEnclosure.Visible = false;
                this.divDataEntryList.Attributes["class"] = "panel-collapse collapse in";
                this.ancDE.InnerHtml = "Data Entry Method";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void chk_NoSignTransformation_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.chk_NoSignTransformation.Checked = false;
                this.chk_NoSignTransformation.Enabled = false;
                this.chk_FlipGx.Enabled = true;
                this.chk_FlipGy.Enabled = true;
                this.chk_FlipGz.Enabled = true;
                this.chk_FlipBx.Enabled = true;
                this.chk_FlipBy.Enabled = true;
                this.chk_FlipBz.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDetailCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnCancelManualUpload_Click(null, null);
                this.btnCancelFileUpload_Click(null, null);
                this.btnProcessSrvyCancel_Click(null, null);
                this.btnCancelReadyData_Click(null, null);
                this.btnProceed5_Click(null, null);
                this.btnCancelAnalysis_Click(null, null);
                this.btnSolCancel_Click(null, null);                
                this.LoadData.Visible = false;
                this.divReadyData.Visible = false;
                this.div5Surveys.Visible = false;
                this.divQCData.Visible = false;
                this.divSol.Visible = false;
                this.divQC.Visible = false;
                this.divDone.Visible = false;
                this.btnMetaCancel_Click(null, null);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSolAccept_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResultFormatted = -99, iResult = -99, SurveyID = -99, OprID = -99, PadID = -99, WellID = -99, SectionID = -99, RunID = -99, EntryType = 4;
                Decimal sDepth = -99.00M, sGx = -99.00000M, sGy = -99.00000M, sGz = -99.00000M, sBx = -99.00M, sBy = -99.00M, sBz = -99.00M;
                OprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                PadID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                WellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                SectionID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                RunID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                System.Data.DataTable solTable = (System.Data.DataTable)HttpContext.Current.Session["solDataTable"];
                foreach (System.Data.DataRow row in solTable.Rows)
                {                    
                    SurveyID = Convert.ToInt32(row["srvRID"]);
                    sDepth = Convert.ToDecimal(row["Depth"]);
                    sGx = Convert.ToDecimal(row["Gx"]);
                    sGy = Convert.ToDecimal(row["Gy"]);
                    sGz = Convert.ToDecimal(row["Gz"]);
                    sBx = Convert.ToDecimal(row["Bx"]);
                    sBy = Convert.ToDecimal(row["By"]);
                    sBz = Convert.ToDecimal(row["Bz"]);
                    //iResult = QC.surveyQC(LoginName, domain, JobID, WellID, SectionID, RunID, String.Empty, String.Empty, sDepth, sGx, sGy, sGz, sBx, sBy, sBz, SurveyID, GetClientDBString, ServiceIDQC);
                }                                
                    this.divQC.Visible = true;
                    System.Data.DataTable resData = QC.getMetaResults(OprID, PadID, WellID, SectionID, RunID, GetClientDBString);
                    this.gvwRQCResults.DataSource = resData;
                    this.gvwRQCResults.DataBind();
                    this.gvwRQCResults.Focus();
                    Decimal maxDepthTI = -99.00M;
                    maxDepthTI = QC.getMaxDepthForTieInSurveys(RunID, SectionID, WellID, PadID, SysClientID, OprID, GetClientDBString);
                    System.Data.DataTable plansTable = QC.getTieInSurveysTable(maxDepthTI, RunID, SectionID, WellID, GetClientDBString);
                    this.gvwMaxRunTieIn.DataSource = plansTable;
                    this.gvwMaxRunTieIn.DataBind();
                    this.btnDetailCancel.Visible = true;
                    this.btnChart_Click(this, EventArgs.Empty); // Generate the Graphs                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnChart_Click(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.createChartQCTotal();
            }
            else
            {
                this.createChartQCTotal();
            }
        }

        protected void btnSolRej_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 JobID = -99, WellID = -99, SectionID = -99, RunID = -99, iResult = -99;
                WellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                SectionID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                RunID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                iResult = DI.getRejectSolution(JobID, WellID, SectionID, RunID, GetClientDBString);
                if (iResult.Equals(1))
                {
                    this.gvwSol.DataSource = null;
                    this.gvwSol.DataBind();
                    this.ltrSolChart.Text = String.Empty;
                    this.lblSolLCVal.Text = String.Empty;
                    this.lblSolSCVal.Text = String.Empty;
                    this.lblSolCRVal.Text = String.Empty;
                    this.divSolEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                    this.iSol.Attributes["class"] = "icon fa fa-check-circle";
                    this.spnSol.InnerText = " Success";
                    this.lblSol.Text = " Successfully removed proposed solution";
                    this.divSolEnclosure.Visible = true;
                }
                else
                {
                    this.gvwSol.DataSource = null;
                    this.gvwSol.DataBind();
                    this.ltrSolChart.Text = String.Empty;
                    this.lblSolLCVal.Text = String.Empty;
                    this.lblSolSCVal.Text = String.Empty;
                    this.lblSolCRVal.Text = String.Empty;
                    this.divSolEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iSol.Attributes["class"] = "icon fa fa-warning";
                    this.spnSol.InnerText = " Warning";
                    this.lblSol.Text = " Unable to remove proposed solution. (Please contact SMARTs Support before proceeding further for this Well)";
                    this.divSolEnclosure.Visible = true;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSolCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.divSol.Visible = false;
                this.gvwSol.DataSource = null;
                this.gvwSol.DataBind();
                this.ltrSolChart.Text = String.Empty;
                this.lblSolLCVal.Text = String.Empty;
                this.lblSolSCVal.Text = String.Empty;
                this.lblSolCRVal.Text = String.Empty;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRQCResults_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 resID = -99;
                Decimal Depth = -99.00M;
                GridViewRow dRow = this.gvwRQCResults.SelectedRow;
                Depth = Convert.ToDecimal(Server.HtmlDecode(dRow.Cells[3].Text));
                resID = Convert.ToInt32(this.gvwRQCResults.SelectedValue);
                Int32 jbID = -99;                
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 wsID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                Int32 rnID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                DataTable selSQC = QC.getResSQCTable(resID, GetClientDBString);
                //DataTable refMagTable = OPR.getReferenceMagnetics(wlID, GetClientDBString);
                DataTable resParam = QC.getResParameters(resID, GetClientDBString);
                DataTable qualfParam = QC.getResQualifiersTable(resID, GetClientDBString);
                DataTable BHATable = QC.getResBHATable(resID, GetClientDBString);
                DataTable NMCTable = QC.getNMCalcTable(resID, GetClientDBString);
                DataTable MinMaxTable = QC.getMinMaxTable(resID, GetClientDBString);
                DataTable plansTable = QC.getTieInSurveysTable(Depth, rnID, wsID, wlID, GetClientDBString);
                this.lblQCResMetaData.Visible = true;
                this.lblQCResParameters.Visible = true;
                this.lblRQCQualifiers.Visible = true;
                this.lblRQCBHA.Visible = true;
                this.lblNMSCalc.Visible = true;
                this.lblRQCMinMax.Visible = true;
                this.lblQCResSQC.Visible = true;
                this.lblTieInSurvey.Visible = true;
                this.gvwResSQC.DataSource = selSQC;
                this.gvwResSQC.DataBind();
                this.gvwResSQC.Visible = true;
                //this.gvwRefVals.DataSource = refMagTable;
                this.gvwRefVals.DataBind();
                this.gvwRefVals.Visible = true;
                this.gvwQCResParameters.DataSource = resParam;
                this.gvwQCResParameters.DataBind();
                this.gvwQCResParameters.Visible = true;
                this.gvwRQCQualifiers.DataSource = qualfParam;
                this.gvwRQCQualifiers.DataBind();
                this.gvwRQCQualifiers.Visible = true;
                this.gvwRQCBHA.DataSource = BHATable;
                this.gvwRQCBHA.DataBind();
                this.gvwRQCBHA.Visible = true;
                this.gvwNMSCalc.DataSource = NMCTable;
                this.gvwNMSCalc.DataBind();
                this.gvwNMSCalc.Visible = true;
                this.gvwRQCMinMax.DataSource = MinMaxTable;
                this.gvwRQCMinMax.DataBind();
                this.gvwRQCMinMax.Visible = true;
                this.gvwTieInSurvey.DataSource = plansTable;
                this.gvwTieInSurvey.DataBind();
                this.gvwTieInSurvey.Visible = true;
                this.btnClearResDetail.Visible = true;

                this.btnChart_Click(null, null);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRQCResults_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 optrID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 wpdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                Int32 wID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 sID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                Int32 rID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                DataTable resData = QC.getMetaResults(optrID, wpdID, wID, sID, rID, GetClientDBString);
                this.gvwRQCResults.DataSource = resData;
                this.gvwRQCResults.PageIndex = e.NewPageIndex;
                this.gvwRQCResults.SelectedIndex = -1;
                this.gvwRQCResults.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRQCQualifiers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = ((DataRowView)e.Row.DataItem).Row;
                    Int32 dC = Convert.ToInt32(dr["DWClr"]);
                    Int32 bC = Convert.ToInt32(dr["DYClr"]);
                    Int32 gC = Convert.ToInt32(dr["EAClr"]);
                    Int32 nG = Convert.ToInt32(dr["HVClr"]);

                    if (dC.Equals(1))
                    {
                        e.Row.Cells[1].BackColor = Color.LightGreen;
                    }
                    else
                    {
                        e.Row.Cells[1].BackColor = Color.Red;
                    }

                    if (bC.Equals(1))
                    {
                        e.Row.Cells[3].BackColor = Color.LightGreen;
                    }
                    else
                    {
                        e.Row.Cells[3].BackColor = Color.Red;
                    }

                    if (gC.Equals(1))
                    {
                        e.Row.Cells[5].BackColor = Color.LightGreen;
                    }
                    else
                    {
                        e.Row.Cells[5].BackColor = Color.Red;
                    }

                    if (nG.Equals(2))
                    {
                        e.Row.Cells[11].BackColor = Color.Red;
                    }
                    else
                    {
                        e.Row.Cells[11].BackColor = Color.LightGreen;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRQCBHA_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = ((DataRowView)e.Row.DataItem).Row;
                    Int32 ek = Convert.ToInt32(dr["EKClr"]);
                    Int32 eo = Convert.ToInt32(dr["EOClr"]);

                    if (ek.Equals(2) || ek.Equals(4) || ek.Equals(7))
                    {
                        e.Row.Cells[4].BackColor = Color.LightGreen;
                    }
                    else
                    {
                        e.Row.Cells[4].BackColor = Color.Red;
                    }

                    if (eo.Equals(1))
                    {
                        e.Row.Cells[8].BackColor = Color.LightGreen;
                    }
                    else
                    {
                        e.Row.Cells[8].BackColor = Color.Red;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnClearResDetail_Click(object sender, EventArgs e)
        {
            try
            {
                this.gvwRQCResults.SelectedRow.BackColor = System.Drawing.Color.Transparent;
                this.gvwRQCResults.SelectedIndex = -1;
                this.gvwResSQC.DataBind();
                this.gvwResSQC.Visible = false;
                this.gvwRefVals.DataBind();
                this.gvwRefVals.Visible = false;
                this.gvwQCResParameters.DataBind();
                this.gvwQCResParameters.Visible = false;
                this.gvwRQCQualifiers.DataBind();
                this.gvwRQCQualifiers.Visible = false;
                this.gvwRQCBHA.DataBind();
                this.gvwRQCBHA.Visible = false;
                this.gvwNMSCalc.DataBind();
                this.gvwNMSCalc.Visible = false;
                this.gvwRQCMinMax.DataBind();
                this.gvwRQCMinMax.Visible = false;
                this.gvwTieInSurvey.DataBind();
                this.gvwTieInSurvey.Visible = false;
                this.btnClearResDetail.Visible = false;
                this.lblQCResSQC.Visible = false;
                this.lblQCResMetaData.Visible = false;
                this.lblQCResParameters.Visible = false;
                this.lblRQCQualifiers.Visible = false;
                this.lblRQCBHA.Visible = false;
                this.lblNMSCalc.Visible = false;
                this.lblRQCMinMax.Visible = false;
                this.lblTieInSurvey.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void createChartQCTotal()
        {
            try
            {
                String result = String.Empty;
                String echarts = String.Empty;
                String echartsAzim = String.Empty;
                String echarts3D = String.Empty;
                decimal maxTVD = 0;
                String wellName = Convert.ToString(HttpContext.Current.Session["wellName"]);
                System.Data.DataTable dsChartData = new System.Data.DataTable();
                System.Data.DataTable dsChartDataAzim = new System.Data.DataTable();
                Int32 wsD = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                if (wsD.Equals(-2))
                {
                    Int32 wD = Convert.ToInt32(this.gvwWellList.SelectedValue);
                    dsChartData = CHRTRes.getDataTableforQCWellGraph(wD, GetClientDBString);
                    dsChartDataAzim = QC.getDataTableforQCWellGraphAZIM(wD, GetClientDBString);
                    maxTVD = CHRTRes.getMaxTVDforWell(wD, GetClientDBString);
                }
                else
                {
                    //Int32 srvID = Convert.ToInt32(this.gvw.gvwSrvGrpService.SelectedValue);
                    //Int32 welID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                    //Int32 RunID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                    //dsChartData = QC.getDataTableforQCGraph_1(wsD, srvID, RunID, GetClientDBString);
                    //dsChartDataAzim = QC.getDataTableforQCWelGraphAZIM_1(wsD, srvID, RunID, GetClientDBString);
                    //maxTVD = QC.getMaxTVDforWell(welID, GetClientDBString);
                }
                try
                {
                    echarts = CHRT.getChartsQCResultsECHRT01("BodyContent_ctrlAzmCorrQC_divECharts", dsChartData);
                    this.ltrECharts.Text = echarts;
                    echartsAzim = CHRT.getChartsQCResultsECHRT_AZM("BodyContent_ctrlAzmCorrQC_divEChartsAzim", dsChartDataAzim);
                    this.ltrEChartsAzim.Text = echartsAzim;
                    //echarts3D = CHRT.getChartsQCResultsECHRT_3D("BodyContent_ctrlAzmCorrQC_divECharts3D", wellName, dsChartData);
                    //echarts3D = CHRT.getChartsQCResultsECHRT_3D("BodyContent_ctrlAzmCorrQC_divECharts3D", String.Empty, dsChartData , maxTVD);
                    this.ltrECharts3D.Text = echarts3D;
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
                finally
                {
                    dsChartData.Dispose();
                    result = String.Empty;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private void qcdatacharts_Click(System.Data.DataTable qcDataTable)
        {
            try
            {
                String result = String.Empty;
                String echarts = String.Empty;
                echarts = CHRT.getChartsIncDipLongShortAzimuth("BodyContent_ctrlAzmCorrQC_divIncAzmCharts", qcDataTable);
                this.ltrIncAzmCharts.Text = echarts;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private void crrdatacharts_Click(System.Data.DataTable crDataTable)
        {
            try
            {
                String result = String.Empty;
                String echarts = String.Empty;
                echarts = CHRT.getChartsIncDipLongShortCorrAzimuth("BodyContent_ctrlAzmCorrQC_divSolChart", crDataTable);
                this.ltrSolChart.Text = echarts;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDeleteWellData_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, JobID = -99, WellID = -99, WellSectionID = -99, RunID = -99;
                WellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                WellSectionID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                RunID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                iResult = DI.getDeleteWellData(JobID, WellID, WellSectionID, RunID, GetClientDBString);
                if (iResult.Equals(1))
                {
                    this.wdEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                    this.iWD.Attributes["class"] = "icon fa fa-check-circle";
                    this.spnWD.InnerText = " Success";
                    this.lblWD.Text = " Successfully removed Well Data";
                    this.wdEnclosure.Visible = true;
                    this.btnDeleteWellData.Enabled = false;
                }
                else
                {
                    this.wdEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iWD.Attributes["class"] = "icon fa fa-warning";
                    this.spnWD.InnerText = " Warning";
                    this.lblWD.Text = " Unable to remove Well Data. (Internal Error)";
                    this.wdEnclosure.Visible = true;
                    this.btnDeleteWellData.Enabled = false;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}