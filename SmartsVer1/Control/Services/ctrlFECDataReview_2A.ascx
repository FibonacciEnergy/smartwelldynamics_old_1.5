﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlFECDataReview_2A.ascx.cs" Inherits="SmartsVer1.Control.Services.ctrlFECDataReview_2A" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

<!-- Stylesheet -->
<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dataTables.bootstrap.min.css" rel="stylesheet" />
<!-- Javascript for ref lib -->
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>
<!--- local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }

    .rotate {
        -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
        -moz-transform: rotate(90deg); /* Firefox */
        -ms-transform: rotate(90deg); /* IE 9 */
        transform: rotate(90deg); /* Standard syntax */
    }
    /*custom anchor defi for menu items*/
    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }
</style>
<asp:UpdateProgress ID="uprgrsMeta" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlMetaInfo">
    <ProgressTemplate>
        <div id="divMetaProcessing" runat="server" class="updateProgress">
            <asp:Image ID="imgMetaProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<asp:UpdatePanel runat="server" ID="upnlMetaInfo">
    <ContentTemplate>
        <!--- Page Menu -->
        <div class="row bg-black">
            <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom: 2px; padding-left: 2px; padding-top: 2px">
                <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
            </div>
            <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
                <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
            </div>
        </div>
        <div class="row bg-black">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
                <asp:LoginView ID='LoginView1' runat='server'>
                    <AnonymousTemplate>
                        <li style="vertical-align: middle; display: table-cell;">
                            <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                                ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                        </li>
                    </AnonymousTemplate>
                    <LoggedInTemplate>
                        <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">(
                            <asp:LoginName ID="LoginName1" runat="server" />
                            )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                        </li>
                    </LoggedInTemplate>
                </asp:LoginView>
            </div>
        </div>
        <!-- Page Breadcrumb -->
        <div class="row bg-white">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box-tools pull-right">
                    <ol class="breadcrumb">
                        <li><a href="../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                        <li>SMARTs</li>
                        <li><a href="../../Services/fecDataReview_2A.aspx">Well Audit/QC Review</a></li>
                    </ol>
                </div>
            </div>
        </div>
        <!--- Page Content --->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row bg-white">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h5 class="panel-title" style="text-align: left">
                                <a id="ancAscMain" runat="server" class="disabled" href="#">Review Audited Well Data</a>
                            </h5>
                        </div>
                        <div id="divRvwOuter" runat="server">
                            <div id="divOuterBody" class="panel-body">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div id="divGagesOuter" class="panel panel-danger panel-body">
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-sm-offset-1" style="padding-right: 5px; padding-right: 5px">
                                            <div class="small-box bg-green">
                                                <div id="divBTGuageJob" class="inner" runat="server">
                                                </div>
                                                <div class="icon">
                                                    <i class="ion ion-clipboard"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="padding-right: 5px; padding-right: 5px">
                                            <div class="small-box bg-light-blue">
                                                <div id="divBTGuageWell" class="inner" runat="server">
                                                </div>
                                                <div class="icon">
                                                    <i class="ion ion-pin"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="padding-right: 5px; padding-right: 5px">
                                            <div class="small-box bg-blue">
                                                <div id="divBTGuageTotalRun" class="inner" runat="server">
                                                </div>
                                                <div class="icon">
                                                    <i class="ion ion-pulse"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="padding-right: 5px; padding-right: 5px">
                                            <div class="small-box bg-purple">
                                                <div id="divBTGuageAud_12" class="inner" runat="server">
                                                </div>
                                                <div class="icon">
                                                    <i class="ion ion-star-half"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="padding-right: 5px; padding-right: 5px">
                                            <div class="small-box bg-fuchsia">
                                                <div id="divBTGuageAud_14Days" class="inner" runat="server">
                                                </div>
                                                <div class="icon">
                                                    <i class="ion ion-star"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="divMetaInfoOuter" class="panel panel-danger">
                                        <div class="panel-body">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div id="divProgress" class="panel panel-danger">
                                                    <div class="panel-body">
                                                        <ol id="olStepper" runat="server" class="stepper" data-stepper-steps="5">
                                                            <li id="liStep1" runat="server" class="stepper-todo">Operator Company</li>
                                                            <li id="liStep2" runat="server" class="stepper-todo">Well Pad</li>
                                                            <li id="liStep3" runat="server" class="stepper-todo">Well/Lateral</li>
                                                            <li id="liStep4" runat="server" class="stepper-todo">Section</li>
                                                            <li id="liStep5" runat="server" class="stepper-todo">Run</li>
                                                        </ol>
                                                    </div>
                                                </div>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:LinkButton runat="server" ID="btnMetaInfoReset" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnMetaInfoReset_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                </div>
                                                <div class="panel panel-info">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancHead" runat="server" class="disabled" href="#">Operator Company</a>
                                                        </h5>
                                                    </div>
                                                    <div id="divOpr" runat="server" class="panel-body">
                                                        <div class="table-responsive">
                                                            <asp:GridView runat="server" ID="gvwGlobalOptr" DataKeyNames="clntID" CssClass="mydatagrid" AllowPaging="True" AllowSorting="True"
                                                                AutoGenerateColumns="False" EmptyDataText="No Operator Company found."
                                                                OnPageIndexChanging="gvwGlobalOptr_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15"
                                                                HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow"
                                                                OnSelectedIndexChanged="gvwGlobalOptr_SelectedIndexChanged">
                                                                <Columns>
                                                                    <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                        <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                    </asp:ButtonField>
                                                                    <asp:BoundField DataField="clntID" Visible="False" />
                                                                    <asp:BoundField DataField="clntName" HeaderText="Operator Company">
                                                                        <ItemStyle CssClass="padRight" HorizontalAlign="Center" VerticalAlign="Middle" Font-Bold="True"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="wpdCount" HeaderText="Global Well Pad Count" Visible="false">
                                                                        <ItemStyle CssClass="padRight" HorizontalAlign="Right" VerticalAlign="Middle" Font-Bold="True"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="welCount" HeaderText="Global Well Count" Visible="false">
                                                                        <ItemStyle CssClass="padRight" HorizontalAlign="Right" VerticalAlign="Middle" Font-Bold="True"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="welGlobal" HeaderText="SMARTs Well Count">
                                                                        <ItemStyle CssClass="padRight" HorizontalAlign="Right" VerticalAlign="Middle" Font-Bold="True"></ItemStyle>
                                                                    </asp:BoundField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                    <div id="divPad" runat="server" visible="false" class="panel-body">
                                                        <div class="table-responsive">
                                                            <asp:GridView ID="gvwWellPads" runat="server" EmptyDataText="No Well Pad(s) found for selected Operator"
                                                                ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="wpdID"
                                                                AllowPaging="True" CssClass="mydatagrid" OnPageIndexChanging="gvwWellPads_PageIndexChanging" OnSelectedIndexChanged="gvwWellPads_SelectedIndexChanged"
                                                                AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                RowStyle-CssClass="rows">
                                                                <Columns>
                                                                    <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                        <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                    </asp:ButtonField>
                                                                    <asp:BoundField DataField="wpdID" HeaderText="wpd" ReadOnly="True" Visible="false" />
                                                                    <asp:BoundField DataField="welPadName" HeaderText="Well Pad" />
                                                                    <asp:BoundField DataField="field" HeaderText="O&G Field" />
                                                                    <asp:BoundField DataField="county" HeaderText="County/District/Division/Municipality" />
                                                                    <asp:BoundField DataField="state" HeaderText="State/Province" />
                                                                    <asp:BoundField DataField="country" HeaderText="Country/Nation" />
                                                                    <asp:BoundField DataField="sreg" HeaderText="Sub-Region" />
                                                                    <asp:BoundField DataField="reg" HeaderText="Major Region" />
                                                                    <asp:BoundField DataField="swlCount" HeaderText="SMARTs Well Count">
                                                                        <ItemStyle CssClass="padRight" HorizontalAlign="Right" VerticalAlign="Middle" Font-Bold="True"></ItemStyle>
                                                                    </asp:BoundField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                    <div id="divWell" runat="server" visible="false" class="panel-body">
                                                        <div class="table-responsive">
                                                            <asp:GridView ID="gvwWellList" runat="server" CssClass="mydatagrid" EmptyDataText="No Wells associated with the selected Service Order #"
                                                                AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="welID" OnSelectedIndexChanged="gvwWellList_SelectedIndexChanged"
                                                                ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwWellList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                PageSize="15" PagerSettings-Mode="Numeric" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Center" PagerStyle-VerticalAlign="Middle"
                                                                HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow"
                                                                OnRowDataBound="gvwWellList_RowDataBound">
                                                                <Columns>
                                                                    <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                        <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                    </asp:ButtonField>
                                                                    <asp:BoundField DataField="welID" Visible="false" />
                                                                    <asp:BoundField DataField="welName" HeaderText="Well / Lateral" />
                                                                    <asp:BoundField DataField="field" HeaderText="Oil & Gas Field" />
                                                                    <asp:BoundField DataField="county" HeaderText="County / District" />
                                                                    <asp:BoundField DataField="state" HeaderText="State / Province" />
                                                                    <asp:BoundField DataField="spud" HeaderText="Spud Date" />
                                                                    <asp:BoundField DataField="wstID" HeaderText="Status" />
                                                                </Columns>
                                                            </asp:GridView>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div runat="server" id="wellEnclosure" visible="false">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                    <h4><i runat="server" id="iWell"><span runat="server" id="spnWell"></span></i></h4>
                                                                    <asp:Label ID="lblWell" runat="server" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="divSec" runat="server" visible="false" class="panel-body">
                                                        <div class="table-responsive">
                                                            <asp:GridView ID="gvwSectionList" runat="server" CssClass="mydatagrid" EmptyDataText="No Well/Lateral Section associated with the selected Well"
                                                                AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="wswID" OnSelectedIndexChanged="gvwSectionList_SelectedIndexChanged"
                                                                ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwSectionList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                PageSize="15" PagerSettings-Mode="Numeric" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Center" PagerStyle-VerticalAlign="Middle"
                                                                HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                                                <Columns>
                                                                    <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                        <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                    </asp:ButtonField>
                                                                    <asp:BoundField DataField="wswID" Visible="false" />
                                                                    <asp:BoundField DataField="wsName" HeaderText="Well/Lateral Section Name" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                                    <asp:BoundField DataField="rnCount" HeaderText="Run Count" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                    <div id="divRun" runat="server" visible="false" class="panel-body">
                                                        <div class="table-responsive">
                                                            <asp:GridView ID="gvwRunList" runat="server" CssClass="mydatagrid" EmptyDataText="No Run associated with the selected Section"
                                                                AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="runID" OnSelectedIndexChanged="gvwRunList_SelectedIndexChanged"
                                                                ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwRunList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                PageSize="15" PagerSettings-Mode="Numeric" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Center" PagerStyle-VerticalAlign="Middle"
                                                                HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                                                <Columns>
                                                                    <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                        <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                    </asp:ButtonField>
                                                                    <asp:BoundField DataField="runID" Visible="false" />
                                                                    <asp:BoundField DataField="rnName" HeaderText="Run Name" />
                                                                    <asp:BoundField DataField="rnStatus" HeaderText="Status" />
                                                                    <asp:BoundField DataField="rnBHA" HeaderText="BHA Signature ID" />
                                                                    <asp:BoundField DataField="rnToolcode" HeaderText="Toolcode" />
                                                                    <asp:BoundField DataField="rnSQC" HeaderText="Survey Qualification Criteria" />
                                                                    <asp:BoundField DataField="rnStartD" HeaderText="Start Measured Depth(m)" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="rnEndD" HeaderText="End Measured Depth(m)" ItemStyle-HorizontalAlign="Right" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divResultsOuter" class="panel panel-success" runat="server" visible="false">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancResults" runat="server" class="disabled" href="#">Audited Data</a>
                                                        </h5>
                                                    </div>
                                                    <div id="divResults" runat="server">
                                                        <div class="panel-body">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div id="divNRes" runat="server" visible="false" class="viewer">
                                                                    <div id="nrEnclosure" runat="server" class="alert alert-warning alert-dismissable">
                                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                        <h4><i runat="server" id="iWellMessage" class="icon fa fa-warning"><span runat="server" id="spnWellHeader">Warning !!!</span></i>
                                                                        </h4>
                                                                        <asp:Label ID="lblNRes" runat="server" Text="No Survey's processed." />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="divDisplayButtons" class="pull-left col-xs-10 col-sm-10 col-md-10 col-lg-10" runat="server">
                                                                <div class="input-group" style="width: 100%;">
                                                                    <asp:LinkButton ID="btnDisplayChartsDataNoPlan" runat="server" Width="45%" CssClass="btn btn-primary text-center" OnClick="btnDisplayChartsDataNoPlan_Click"><i class="fa fa-2x"> Data Analysis Charts</i></asp:LinkButton>
                                                                    &nbsp;
                                                                                <asp:LinkButton ID="btnDisplayChartsDataPlan" runat="server" Width="45%" CssClass="btn btn-primary text-center" OnClick="btnDisplayChartsDataPlan_Click"><i class="fa fa-2x"> Data Analysis Charts & Well Plan</i></asp:LinkButton>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="panel-body">
                                                                    <div id="charts" runat="server">

                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-left: 0px; padding-right: 0px">
                                                                            <div class="panel-body" style="padding-left: 0px; padding-right: 0px">
                                                                                <h3 class="bg-gray box-title">Performance Quality Score Report</h3>
                                                                                <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                                                                                    <div class="box box-primary" style="padding-left: 0px; padding-right: 0px">
                                                                                        <div class="box-body" style="padding-left: 0px; padding-right: 0px">
                                                                                            <div id="divPerfHeatMapQS1" runat="server" style="height: 450px; padding-left: 5px; padding-right: 5px"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5" style="padding-left: 0px; padding-right: 0px">
                                                                                    <div class="box box-primary" style="padding-left: 0px; padding-right: 0px">
                                                                                        <div class="box-body">
                                                                                            <div id="divPerfQS2" runat="server" style="height: 450px; padding-left: 0px; padding-right: 0px"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-left: 0px; padding-right: 0px">

                                                                            <h3 class="bg-gray box-title">Analysis Chart </h3>
                                                                            <div id="divECharts" runat="server" style="height: 700px;"></div>

                                                                            <h3 class="bg-gray box-title">Azimuth Analysis Chart </h3>
                                                                            <div id="divEChartsAzim" runat="server" style="height: 450px;"></div>

                                                                            <h3 class="bg-gray box-title">Analysis Chart - 3D </h3>
                                                                            <div id="divECharts3D" runat="server" style="height: 700px;"></div>

                                                                            <h3 class="bg-gray box-title">Analysis Chart Polar </h3>
                                                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                                <div class="box box-primary">
                                                                                    <div class="box-body">
                                                                                        <div id="divPolar1" runat="server" style="height: 700px; padding-left: 5px; padding-right: 5px"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                                <div class="box box-primary">
                                                                                    <div class="box-body">
                                                                                        <div id="divPolar2" runat="server" style="height: 700px; padding-left: 5px; padding-right: 5px"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <asp:Literal ID="ltrECHarts" runat="server"></asp:Literal>
                                                                        <asp:Literal ID="ltrECHartsAzim" runat="server"></asp:Literal>
                                                                        <asp:Literal ID="ltrECHarts3D" runat="server"></asp:Literal>
                                                                        <asp:Literal ID="ltrECHartsPolar1" runat="server"></asp:Literal>
                                                                        <asp:Literal ID="ltrECHartsPolar2" runat="server"></asp:Literal>
                                                                        <asp:Literal ID="ltrECHartsPerfHeatMapQS1" runat="server"></asp:Literal>
                                                                        <asp:Literal ID="ltrECHartsPerfQS2" runat="server"></asp:Literal>

                                                                    </div>
                                                                    <div id="chartsWplan" runat="server" visible="false">

                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-left: 0px; padding-right: 0px">
                                                                            <div class="panel-body" style="padding-left: 0px; padding-right: 0px">
                                                                                <h3 class="bg-gray box-title">Performance Quality Score Report</h3>
                                                                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                                    <div class="box box-primary">
                                                                                        <div class="box-body">
                                                                                            <div id="divPerfHeatMapQS1wPlan" runat="server" style="height: 450px; padding-left: 5px; padding-right: 5px"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="padding-left: 0px; padding-right: 0px">
                                                                                    <div class="box box-primary" style="padding-left: 0px; padding-right: 0px">
                                                                                        <div class="box-body" style="padding-left: 0px; padding-right: 0px">
                                                                                            <div id="divPerfQS2wPlan" runat="server" style="height: 450px; padding-left: 0px; padding-right: 0px"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-left: 0px; padding-right: 0px">

                                                                            <h3 class="bg-gray box-title">Analysis Chart with Well Plan </h3>
                                                                            <div id="divEChartsWithPlan" runat="server" style="height: 500px;"></div>

                                                                            <h3 class="bg-gray box-title">Azimuth Analysis Chart with Well Plan </h3>
                                                                            <div id="divEChartsAzimWithPlan" runat="server" style="height: 450px;"></div>

                                                                            <h3 class="bg-gray box-title">Analysis Chart - 3D </h3>
                                                                            <div id="divECharts3DWithPlan" runat="server" style="height: 700px;"></div>

                                                                        </div>

                                                                        <asp:Literal ID="ltrECHartswithPlan" runat="server"></asp:Literal>
                                                                        <asp:Literal ID="ltrECHartsAzimwithPlan" runat="server"></asp:Literal>
                                                                        <asp:Literal ID="ltrECHarts3DwithPlan" runat="server"></asp:Literal>
                                                                        <asp:Literal ID="ltrECHartsPerfHeatMapQS1wPan" runat="server"></asp:Literal>
                                                                        <asp:Literal ID="ltrECHartsPerfQS2wPlan" runat="server"></asp:Literal>

                                                                    </div>
                                                                    <hr class="divider" role="separator" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="row">
                                                                    <div id="divResView" runat="server" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <asp:GridView ID="gvwRQCWellResults" runat="server" CssClass="mydatagrid" AllowPaging="True" AutoGenerateColumns="False"
                                                                                DataKeyNames="resID" OnSelectedIndexChanged="gvwRQCWellResults_SelectedIndexChanged" OnPageIndexChanging="gvwRQCWellResults_PageIndexChanging"
                                                                                AlternatingRowStyle-CssClass="AlternatingRowStyle" Visible="False" PageSize="15" PagerSettings-Mode="Numeric" PagerSettings-Position="Bottom"
                                                                                PagerStyle-HorizontalAlign="Center" PagerStyle-VerticalAlign="Middle" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                                RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow" OnRowEditing="gvwRQCWellResults_RowEditing"
                                                                                OnRowCancelingEdit="gvwRQCWellResults_RowCancelingEdit" OnRowUpdating="gvwRQCWellResults_RowUpdating">
                                                                                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                                                                <Columns>
                                                                                    <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button" ControlStyle-CssClass="btn btn-info text-center" />
                                                                                    <asp:TemplateField HeaderText="Survey No.">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblSrvyIDItem" runat="server" Text='<%# Bind("srvyID") %>' CssClass="form-control" BackColor="Transparent"></asp:Label>
                                                                                        </ItemTemplate>                                                                                        
                                                                                        <EditItemTemplate>
                                                                                            <asp:Label ID="lblSrvyIDEdit" runat="server" Text='<%# Eval("srvyID") %>' CssClass="form-control" BackColor="Transparent"></asp:Label>
                                                                                        </EditItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Measured Depth">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblDepthItem" runat="server" Text='<%# Bind("Depth") %>' CssClass="form-control" BackColor="Transparent"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                                                        <EditItemTemplate>
                                                                                            <asp:Label ID="lblDepthEdit" runat="server" Text='<%# Eval("Depth") %>' CssClass="form-control" BackColor="Transparent"></asp:Label>
                                                                                        </EditItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Selected Azimuth">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblAzmCriteria" runat="server" Text='<%# Bind("acID") %>' CssClass="form-control" BackColor="Transparent"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <ItemStyle BackColor="#ff66cc"></ItemStyle>
                                                                                        <EditItemTemplate>
                                                                                            <asp:DropDownList ID="ddlEditAzmCriteria" runat="server" CssClass="form-control" AutoPostBack="True" AppendDataBoundItems="true"
                                                                                                OnSelectedIndexChanged="ddlEditAzmCriteria_SelectedIndexChanged">
                                                                                            </asp:DropDownList>
                                                                                        </EditItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Well Section">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblWSItem" runat="server" Text='<%# Bind("WSection") %>' CssClass="form-control" BackColor="Transparent"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <EditItemTemplate>
                                                                                            <asp:Label ID="lblWSEdit" runat="server" Text='<%# Eval("WSection") %>' CssClass="form-control" BackColor="Transparent"></asp:Label>
                                                                                        </EditItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="SMARTs Analysis">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblTAItem" runat="server" Text='<%# Bind("taSt") %>' CssClass="form-control" BackColor="Transparent"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <EditItemTemplate>
                                                                                            <asp:Label ID="lblTAEdit" runat="server" Text='<%# Eval("taSt") %>' CssClass="form-control" BackColor="Transparent"></asp:Label>
                                                                                        </EditItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:CommandField ShowEditButton="True" ButtonType="Button" ControlStyle-CssClass="btn btn-info text-center"></asp:CommandField>
                                                                                </Columns>
                                                                                <HeaderStyle CssClass="header" />
                                                                                <PagerStyle CssClass="pager" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <RowStyle CssClass="rows" />
                                                                                <SelectedRowStyle CssClass="selectedrow" />
                                                                            </asp:GridView>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <asp:GridView ID="gvwRQCRunResults" runat="server" CssClass="mydatagrid" AllowPaging="True" AutoGenerateColumns="False"
                                                                                DataKeyNames="resID" OnSelectedIndexChanged="gvwRQCRunResults_SelectedIndexChanged" Visible="false" OnPageIndexChanging="gvwRQCRunResults_PageIndexChanging"
                                                                                AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" PagerSettings-Mode="Numeric" PagerSettings-Position="Bottom"
                                                                                PagerStyle-HorizontalAlign="Center" PagerStyle-VerticalAlign="Middle" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                                RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow" OnRowEditing="gvwRQCRunResults_RowEditing"
                                                                                OnRowCancelingEdit="gvwRQCRunResults_RowCancelingEdit" OnRowUpdating="gvwRQCRunResults_RowUpdating">
                                                                                <Columns>
                                                                                    <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button" ControlStyle-CssClass="btn btn-info text-center" />
                                                                                    <asp:TemplateField HeaderText="Survey No.">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblSrvyIDItem" runat="server" Text='<%# Bind("srvyID") %>' CssClass="form-control" BackColor="Transparent"></asp:Label>
                                                                                        </ItemTemplate>                                                                                        
                                                                                        <EditItemTemplate>
                                                                                            <asp:Label ID="lblSrvyIDEdit" runat="server" Text='<%# Eval("srvyID") %>' CssClass="form-control" BackColor="Transparent"></asp:Label>
                                                                                        </EditItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Measured Depth">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblDepthItem" runat="server" Text='<%# Bind("Depth") %>' CssClass="form-control" BackColor="Transparent"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                                                        <EditItemTemplate>
                                                                                            <asp:Label ID="lblDepthEdit" runat="server" Text='<%# Eval("Depth") %>' CssClass="form-control" BackColor="Transparent"></asp:Label>
                                                                                        </EditItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Selected Azimuth">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblSelAzmItem" runat="server" Text='<%# Bind("acID") %>' CssClass="form-control" BackColor="Transparent"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <ItemStyle BackColor="#ff66cc"></ItemStyle>
                                                                                        <EditItemTemplate>
                                                                                            <asp:DropDownList ID="ddlEditRunAzmCriteria" runat="server" CssClass="form-control" AutoPostBack="True" AppendDataBoundItems="true"
                                                                                                OnSelectedIndexChanged="ddlEditRunAzmCriteria_SelectedIndexChanged">
                                                                                            </asp:DropDownList>
                                                                                        </EditItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="SMARTs Analysis">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblTAItem" runat="server" Text='<%# Bind("taSt") %>' CssClass="form-control" BackColor="Transparent"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <EditItemTemplate>
                                                                                            <asp:Label ID="lblTAEdit" runat="server" Text='<%# Eval("taSt") %>' CssClass="form-control" BackColor="Transparent"></asp:Label>
                                                                                        </EditItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:CommandField ShowEditButton="True" ButtonType="Button" ControlStyle-CssClass="btn btn-info text-center"></asp:CommandField>
                                                                                </Columns>
                                                                                <HeaderStyle CssClass="header" />
                                                                                <PagerStyle CssClass="pager" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <RowStyle CssClass="rows" />
                                                                                <SelectedRowStyle CssClass="selectedrow" />
                                                                            </asp:GridView>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <asp:Label ID="lblMaxRunTieIn" runat="server" Text="End-Depth Tie-In" BackColor="LightBlue" class="form-control"
                                                                                ForeColor="Purple" Font-Size="Large" />
                                                                            <asp:GridView ID="gvwMaxRunTieIn" runat="server" CssClass="mydatagrid" AllowPaging="True"
                                                                                AutoGenerateColumns="False" DataKeyNames="tisID" ShowHeaderWhenEmpty="True" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                                HeaderStyle-CssClass="header">
                                                                                <Columns>
                                                                                    <asp:BoundField DataField="tisID" Visible="false" />
                                                                                    <asp:BoundField DataField="title" />
                                                                                    <asp:BoundField HeaderText="M. Depth" DataField="md" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                                    <asp:BoundField HeaderText="Inclination" DataField="inc" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                                    <asp:BoundField HeaderText="Azimuth" DataField="azm" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                                    <asp:BoundField HeaderText="TVD" DataField="tvd" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                                    <asp:BoundField HeaderText="North" DataField="north" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.0000}" />
                                                                                    <asp:BoundField HeaderText="East" DataField="east" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.0000}" />
                                                                                    <asp:BoundField HeaderText="VS" DataField="vs" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                                    <asp:BoundField HeaderText="Subsea" DataField="subsea" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <asp:Button ID="btnChkShot" runat="server" CssClass="btn btn-warning" Text="" Visible="false" />
                                                                            <asp:Label ID="lblChkShot" runat="server" Text="Check-Shot Survey" ForeColor="Purple" Visible="False" Font-Size="Large" CssClass="form-control" />
                                                                        </div>

                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <br />
                                                                            <asp:Label ID="lblDepth" runat="server" Text="" ForeColor="DarkBlue" BackColor="Wheat" Style="text-align: center" Font-Bold="true"
                                                                                Visible="False" Font-Size="Large" CssClass="form-control" />
                                                                            <asp:Label ID="lblQCResSQC" runat="server" Text="Survey Qualification Criterion" BackColor="LightBlue" CssClass="form-control"
                                                                                ForeColor="Purple" Visible="False" Font-Size="Large" />
                                                                            <asp:GridView ID="gvwResSQC" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="sqcID"
                                                                                Visible="False" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle">
                                                                                <Columns>
                                                                                    <asp:BoundField DataField="sqcID" Visible="false" />
                                                                                    <asp:BoundField DataField="sqcName" HeaderText="Criterion Name" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="24%" />
                                                                                    <asp:BoundField DataField="sqcDip" HeaderText="Dip Tolerance" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="24%" />
                                                                                    <asp:BoundField DataField="sqcBT" HeaderText="B-Total Tolerance" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="24%" />
                                                                                    <asp:BoundField DataField="sqcGT" HeaderText="G-Total Tolerance" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="24%" />
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <asp:Label ID="lblQCResMetaData" runat="server" Text="Survey Reference Values" BackColor="LightBlue" CssClass="form-control"
                                                                                ForeColor="Purple" Visible="False" Font-Size="Large" />
                                                                            <asp:GridView ID="gvwRefVals" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="rfmgID"
                                                                                Visible="False" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle">
                                                                                <Columns>
                                                                                    <asp:BoundField DataField="rfmgID" HeaderText="rfmgID" Visible="False" />
                                                                                    <asp:BoundField DataField="ifrID" HeaderText="IFR" ItemStyle-HorizontalAlign="Left" />
                                                                                    <asp:BoundField DataField="mdec" HeaderText="Magnetic Declination°" ItemStyle-HorizontalAlign="Center" />
                                                                                    <asp:BoundField DataField="dip" HeaderText="Dip°" ItemStyle-HorizontalAlign="Center" />
                                                                                    <asp:BoundField DataField="bTotal" HeaderText="B-Total" ItemStyle-HorizontalAlign="Center" />
                                                                                    <asp:BoundField DataField="gTotal" HeaderText="G-Total" ItemStyle-HorizontalAlign="Center" />
                                                                                    <asp:BoundField DataField="jcID" HeaderText="Convergence" ItemStyle-HorizontalAlign="Center" />
                                                                                    <asp:BoundField DataField="gconv" HeaderText="Grid Convergence" ItemStyle-HorizontalAlign="Center" />
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <asp:Label ID="lblQCResParameters" runat="server" Text="Survey Parameters" BackColor="LightBlue" CssClass="form-control"
                                                                                ForeColor="Purple" Visible="False" Font-Size="Large" />
                                                                            <asp:GridView ID="gvwQCResParameters" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                                                DataKeyNames="resID" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle">
                                                                                <Columns>
                                                                                    <asp:BoundField HeaderText="resID" Visible="false" DataField="resID" />
                                                                                    <asp:BoundField DataField="inc" HeaderText="Inclination°" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="14%" DataFormatString="{0: #0.00}" />
                                                                                    <asp:BoundField DataField="azm" HeaderText="Azimuth° (Grid)" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="14%" DataFormatString="{0: #0.00}" ItemStyle-BackColor="#ff66cc" />
                                                                                    <asp:BoundField DataField="dip" HeaderText="Dip°" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="14%" DataFormatString="{0: #0.00}" />
                                                                                    <asp:BoundField DataField="bTotal" HeaderText="B-Total (nT)" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="14%" DataFormatString="{0: #0.00}" />
                                                                                    <asp:BoundField DataField="gTotal" HeaderText="G-Total (g)" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="14%" DataFormatString="{0: #0.0000}" />
                                                                                    <asp:BoundField DataField="gtf" HeaderText="GTF°" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="14%" DataFormatString="{0: #0.00}" />
                                                                                    <asp:BoundField DataField="mtf" HeaderText="MTF°" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="14%" DataFormatString="{0: #0.00}" />
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <asp:Label ID="lblRQCQualifiers" runat="server" Text="Survey Qualifiers" BackColor="LightBlue" CssClass="form-control"
                                                                                ForeColor="Purple" Visible="False" Font-Size="Large" />
                                                                            <asp:GridView ID="gvwRQCQualifiers" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="resID"
                                                                                HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                                OnRowDataBound="gvwRQCQualifiers_RowDataBound">
                                                                                <Columns>
                                                                                    <asp:BoundField DataField="resID" HeaderText="resID" Visible="false" />
                                                                                    <asp:BoundField DataField="DW" HeaderText="Dip Qualifier" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" />
                                                                                    <asp:BoundField DataField="BC" HeaderText="ΔDip°" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" DataFormatString="{0: #0.00}" />
                                                                                    <asp:BoundField DataField="DY" HeaderText="B-Total Qualifier" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" />
                                                                                    <asp:BoundField DataField="CD" HeaderText="ΔB-Total (nT)" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" DataFormatString="{0: #0.00}" />
                                                                                    <asp:BoundField DataField="EA" HeaderText="G-Total Qualifier" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" />
                                                                                    <asp:BoundField DataField="CG" HeaderText="ΔG-Total (g)" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" DataFormatString="{0: #0.00000}" />
                                                                                    <asp:BoundField DataField="AO" HeaderText="Corrected Azimuth° (Grid)" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%"
                                                                                        DataFormatString="{0: #0.00}" />
                                                                                    <asp:BoundField DataField="AP" HeaderText="ΔAzimuth° Error" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" DataFormatString="{0: #0.00}" />
                                                                                    <asp:BoundField DataField="DD" HeaderText="Short Collar Azimuth°" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%"
                                                                                        DataFormatString="{0: #0.00}" />
                                                                                    <asp:BoundField DataField="DE" HeaderText="Δ( SCAZ - LCAZ )°" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" DataFormatString="{0: #0.00}" />
                                                                                    <asp:BoundField DataField="HV" HeaderText="No Go Zone" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" />
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <asp:Label ID="lblRQCBHA" runat="server" Text="BHA Analysis" BackColor="LightBlue" CssClass="form-control"
                                                                                ForeColor="Purple" Visible="False" Font-Size="Large" />
                                                                            <asp:GridView ID="gvwRQCBHA" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="resID"
                                                                                HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                                OnRowDataBound="gvwRQCBHA_RowDataBound">
                                                                                <Columns>
                                                                                    <asp:BoundField HeaderText="resID" Visible="false" DataField="resID" />
                                                                                    <asp:BoundField HeaderText="Azimuth" DataField="azmBHA" ItemStyle-HorizontalAlign="Center" />
                                                                                    <asp:BoundField HeaderText="ΔAzimuth BHA" DataField="colGE" ItemStyle-HorizontalAlign="Center" />
                                                                                    <asp:BoundField HeaderText="ΔAzimuth Motor" DataField="colEI" ItemStyle-HorizontalAlign="Center" />
                                                                                    <asp:BoundField HeaderText="ΔAzimuth Drill Collar" DataField="colEJ" ItemStyle-HorizontalAlign="Center" />
                                                                                    <asp:BoundField DataField="colEK" HeaderText="BHA Non-Mag Space" ItemStyle-HorizontalAlign="Center" />
                                                                                    <asp:BoundField HeaderText="ΔAzimuth Drill Collar (Downhole)" DataField="colEN" ItemStyle-HorizontalAlign="Center" />
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <asp:Label ID="lblNMSCalc" runat="server" Text="Non-Mag Space Calculations" BackColor="LightBlue" CssClass="form-control"
                                                                                ForeColor="Purple" Visible="False" Font-Size="Large" />
                                                                            <asp:GridView ID="gvwNMSCalc" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="resID"
                                                                                HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle">
                                                                                <Columns>
                                                                                    <asp:BoundField HeaderText="resID" Visible="false" DataField="resID" />
                                                                                    <asp:BoundField HeaderText="Std. DC Interference" DataField="D34" ItemStyle-HorizontalAlign="Center" />
                                                                                    <asp:BoundField HeaderText="Std. Mtr. Interference" DataField="D35" ItemStyle-HorizontalAlign="Center" />
                                                                                    <asp:BoundField HeaderText="Total BHA Interference" DataField="D37" ItemStyle-HorizontalAlign="Center" />
                                                                                    <asp:BoundField HeaderText="ΔLCAZ Error (BHA)" DataField="D38" ItemStyle-HorizontalAlign="Center" />
                                                                                    <asp:BoundField HeaderText="Bit-To-Sensor Distance" DataField="D39" ItemStyle-HorizontalAlign="Center" />
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <asp:Label ID="lblRQCMinMax" runat="server" Text="Min-Max" BackColor="LightBlue" CssClass="form-control"
                                                                                ForeColor="Purple" Visible="False" Font-Size="Large" />
                                                                            <asp:GridView ID="gvwRQCMinMax" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="resID"
                                                                                HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle">
                                                                                <Columns>
                                                                                    <asp:BoundField HeaderText="resID" Visible="false" DataField="resID" />
                                                                                    <asp:BoundField HeaderText="Min. Dip" DataField="EU" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                                                    <asp:BoundField HeaderText="Max. Dip" DataField="EV" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                                                    <asp:BoundField HeaderText="Dip Spread" DataField="EW" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                                                    <asp:BoundField HeaderText="Min. B-Total" DataField="EX" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                                                    <asp:BoundField HeaderText="Max. B-Total" DataField="EY" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                                                    <asp:BoundField HeaderText="B-Total Spread" DataField="EZ" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                                                    <asp:BoundField HeaderText="Min. G-Total" DataField="FA" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                                                    <asp:BoundField HeaderText="Max. G-Total" DataField="FB" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                                                    <asp:BoundField HeaderText="G-Total Spread" DataField="FC" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <asp:Label ID="lblPlacement" runat="server" Text="Placement Values" CssClass="form-control" BackColor="LightBlue" ForeColor="Purple" Visible="False" Font-Size="Large" />
                                                                            <asp:GridView ID="gvwPlacement" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                                                DataKeyNames="srvID" SelectedRowStyle-BackColor="#CCFFCC" HeaderStyle-CssClass="header" ShowHeaderWhenEmpty="true"
                                                                                RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle" EmptyDataText="No Placement values found for selected depth">
                                                                                <Columns>
                                                                                    <asp:BoundField HeaderText="srvID" Visible="false" DataField="srvID" />
                                                                                    <asp:BoundField HeaderText="TVD" DataField="tvd" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                                    <asp:BoundField HeaderText="North" DataField="north" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                                    <asp:BoundField HeaderText="East" DataField="east" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #.00}" />
                                                                                    <asp:BoundField HeaderText="Vertical Section" DataField="vs" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                                    <asp:BoundField HeaderText="Subsea Elevation" DataField="subsea" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.0000}" />
                                                                                    <asp:BoundField HeaderText="Dogleg" DataField="dls" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.0000}" />
                                                                                    <asp:BoundField HeaderText="Northing" DataField="northing" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                                    <asp:BoundField HeaderText="Easting" DataField="easting" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <asp:Label ID="lblTieInSurvey" runat="server" Text="Tie-In Survey" CssClass="form-control" BackColor="LightBlue" ForeColor="Purple" Visible="False" Font-Size="Large" />
                                                                            <asp:GridView ID="gvwTieInSurvey" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                                                DataKeyNames="tisID" SelectedRowStyle-BackColor="#CCFFCC" HeaderStyle-CssClass="header" ShowHeaderWhenEmpty="true"
                                                                                RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle" EmptyDataText="No Tie-In Surveys found for selected depth">
                                                                                <Columns>
                                                                                    <asp:BoundField HeaderText="tisID" Visible="false" DataField="tisID" />
                                                                                    <asp:BoundField HeaderText="" DataField="title" />
                                                                                    <asp:BoundField HeaderText="M. Depth" DataField="md" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #.00}" />
                                                                                    <asp:BoundField HeaderText="Inclination" DataField="inc" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #.00}" />
                                                                                    <asp:BoundField HeaderText="Azimuth" DataField="azm" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #.00}" />
                                                                                    <asp:BoundField HeaderText="TVD" DataField="tvd" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #.00}" />
                                                                                    <asp:BoundField HeaderText="North" DataField="north" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #.0000}" />
                                                                                    <asp:BoundField HeaderText="East" DataField="east" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #.0000}" />
                                                                                    <asp:BoundField HeaderText="VS" DataField="vs" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #.00}" />
                                                                                    <asp:BoundField HeaderText="Subsea" DataField="subsea" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #.00}" />
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                    <div class="input-group" style="width: 100%;">
                                                                        <asp:LinkButton ID="btnClear" runat="server" Enabled="false" CssClass="btn btn-default text-center" OnClick="btnClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                        <asp:LinkButton ID="btnDetailCancel" runat="server" CssClass="btn btn-danger text-center" OnClick="btnDetailCancel_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnDisplayChartsDataNoPlan" />
        <asp:PostBackTrigger ControlID="btnDisplayChartsDataPlan" />
        <asp:PostBackTrigger ControlID="gvwRQCWellResults" />
        <asp:PostBackTrigger ControlID="gvwRunList" />
        <asp:PostBackTrigger ControlID="gvwSectionList" />
        <asp:PostBackTrigger ControlID="btnClear" />
    </Triggers>
</asp:UpdatePanel>
