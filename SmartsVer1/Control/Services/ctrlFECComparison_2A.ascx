﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlFECComparison_2A.ascx.cs" Inherits="SmartsVer1.Control.Services.ctrlFECComparison_2A" %>
<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
<!-- Stylesheet -->
<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript -->
<script src="../../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>
<!--- Page local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }
    .rotate {
        -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
        -moz-transform: rotate(90deg); /* Firefox */
        -ms-transform: rotate(90deg); /* IE 9 */
        transform: rotate(90deg); /* Standard syntax */
    }
    .swdMenu {
        color: #ffffff;
    }
    a.disabled {
        pointer-events: none;
        cursor: default;
    }   
</style>
<!--- Page local script --->
<script type="text/javascript">
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("rotate");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").addClass("rotate");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("rotate");
        });
    });
</script>
<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li>
                    <asp:HyperLink runat="server" NavigateUrl="~/Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Home</asp:HyperLink>
                </li>
                <li>SMARTs</li>
                <li>
                    <asp:HyperLink runat="server" NavigateUrl="~/Services/fecDataComparison.aspx">Well/Lateral Comparison</asp:HyperLink>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel-group" id="acrdMain">
                <div class=" panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancCompMain" runat="server" class="disabled" data-toggle="collapse" data-parent="#BodyContent_ctrlFECComparison_2A_acrdMain"
                                href="#BodyContent_ctrlFECComparison_2A_divRvwOuter">Compare Audited Well(s) / Lateral(s)</a>
                        </h5>
                    </div>
                    <div class="panel-body">
                        <div class="panel panel-danger">
                            <div class="panel-body">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="col-sm-2 col-md-2 col-lg-2 col-sm-offset-1" style="padding-right: 5px; padding-right: 5px">
                                        <div class="small-box bg-green">
                                            <div id="divBTGuageJob" class="inner" runat="server">
                                            </div>
                                            <div class="icon">
                                                <i class="ion ion-clipboard"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="padding-right: 5px; padding-right: 5px">
                                        <div class="small-box bg-light-blue">
                                            <div id="divBTGuageWell" class="inner" runat="server">
                                            </div>
                                            <div class="icon">
                                                <i class="ion ion-pin"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="padding-right: 5px; padding-right: 5px">
                                        <div class="small-box bg-blue">
                                            <div id="divBTGuageTotalRun" class="inner" runat="server">
                                            </div>
                                            <div class="icon">
                                                <i class="ion ion-pulse"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="padding-right: 5px; padding-right: 5px">
                                        <div class="small-box bg-purple">
                                            <div id="divBTGuageAud_12" class="inner" runat="server">
                                            </div>
                                            <div class="icon">
                                                <i class="ion ion-star-half"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="padding-right: 5px; padding-right: 5px">
                                        <div class="small-box bg-fuchsia">
                                            <div id="divBTGuageAud_14Days" class="inner" runat="server">
                                            </div>
                                            <div class="icon">
                                                <i class="ion ion-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-danger">
                            <div class="with-border">
                                <div class="panel-body">
                                    <asp:UpdateProgress ID="upgrsDataReview" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlDataReview">
                                        <ProgressTemplate>
                                            <div id="divDataReviewProgressbar" runat="server" class="updateProgress">
                                                <asp:Image ID="imgMetaProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                    <asp:UpdatePanel ID="upnlDataReview" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-info">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancCompany" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFECComparison_2A_accordion" href="#BodyContent_ctrlFECComparison_2A_divOperator">
                                                                Operator Company </a>
                                                        </h5>
                                                    </div>
                                                    <div id="divOperator" runat="server" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <asp:GridView runat="server" ID="gvwGlobalOptr" DataKeyNames="clntID" CssClass="mydatagrid" AllowPaging="True" AllowSorting="True"
                                                                AutoGenerateColumns="False" EmptyDataText="No Well(s) found for selected Operator Company."
                                                                OnPageIndexChanging="gvwGlobalOptr_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15"
                                                                HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow"
                                                                OnSelectedIndexChanged="gvwGlobalOptr_SelectedIndexChanged">
                                                                <Columns>
                                                                    <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                        <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                    </asp:ButtonField>
                                                                    <asp:BoundField DataField="clntID" Visible="False" />
                                                                    <asp:BoundField DataField="clntName" HeaderText="Operator Company" >
                                                                           <ItemStyle CssClass="padRight" HorizontalAlign="Center" VerticalAlign="Middle" Font-Bold="True"></ItemStyle>
                                                                    </asp:BoundField >
                                                                    <asp:BoundField DataField="wpdCount" HeaderText="Global Well Pad Count" Visible="false" />
                                                                    <asp:BoundField DataField="welCount" HeaderText="Global Well Count"  Visible="false"/>
                                                                    <asp:BoundField DataField="welGlobal" HeaderText="SMARTs Well Count" >
                                                                               <ItemStyle CssClass="padRight" HorizontalAlign="Center" VerticalAlign="Middle" Font-Bold="True"></ItemStyle>
                                                                    </asp:BoundField >
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-info">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancPad" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFECComparison_2A_accordion" href="#BodyContent_ctrlFECComparison_2A_divWellPad">
                                                                Well Pad </a>
                                                        </h5>
                                                    </div>
                                                    <div id="divWellPad" runat="server" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <asp:GridView ID="gvwWellPads" runat="server" EmptyDataText="No Well Pad(s) found for selected Operator"
                                                                ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="wpdID"
                                                                AllowPaging="True" CssClass="mydatagrid" OnPageIndexChanging="gvwWellPads_PageIndexChanging" OnSelectedIndexChanged="gvwWellPads_SelectedIndexChanged"
                                                                AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                RowStyle-CssClass="rows">
                                                                <Columns>
                                                                    <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                        <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                    </asp:ButtonField>
                                                                    <asp:BoundField DataField="wpdID" HeaderText="wpd" ReadOnly="True" Visible="false" />
                                                                    <asp:BoundField DataField="welPadName" HeaderText="Well Pad" />
                                                                    <asp:BoundField DataField="field" HeaderText="O&G Field" />
                                                                    <asp:BoundField DataField="county" HeaderText="County/District/Division/Municipality" />
                                                                    <asp:BoundField DataField="state" HeaderText="State/Province" />
                                                                    <asp:BoundField DataField="welCount" HeaderText="Global Well/Lateral Count">
                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="swlCount" HeaderText="SMARTs Well Count">
                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                    </asp:BoundField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-info">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancWell" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFECComparison_2A_accordion" href="#BodyContent_ctrlFECComparison_2A_divWellList">
                                                                Well / Lateral </a>
                                                        </h5>
                                                    </div>
                                                    <div id="divWellList" runat="server" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <asp:GridView ID="gvwWellList" runat="server" CssClass="col-xs-12 col-sm-12 col-lg-12 mydatagrid" EmptyDataText="No Wells/Lateral found"
                                                                AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="welID" OnSelectedIndexChanged="gvwWellList_SelectedIndexChanged"
                                                                ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwWellList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                PageSize="15" PagerSettings-Mode="Numeric" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Center" PagerStyle-VerticalAlign="Middle"
                                                                HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                                                <Columns>
                                                                    <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button" ControlStyle-CssClass="btn btn-info text-center">
                                                                    </asp:ButtonField>
                                                                    <asp:BoundField DataField="welID" Visible="false" />
                                                                    <asp:BoundField DataField="welName" HeaderText="Well Name" />
                                                                    <asp:BoundField DataField="oprName" HeaderText="Operator Company" />
                                                                    <asp:BoundField DataField="fldName" HeaderText="Field" />
                                                                    <asp:BoundField DataField="cnyName" HeaderText="County/District" />
                                                                    <asp:BoundField DataField="apiuwi" HeaderText="API/UWI" />
                                                                    <asp:BoundField DataField="spudDate" HeaderText="Spud Date" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-info">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancSelected" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFECComparison_2A_accordion" href="#BodyContent_ctrlFECComparison_2A_divSelected">
                                                                Selected Wells/Laterals</a>
                                                        </h5>
                                                    </div>
                                                    <div id="divSelected" runat="server" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <asp:TextBox runat="server" ID="txtbxComments" CssClass="form-control" TextMode="MultiLine" Height="125px" MaxLength="2500"
                                                                BackColor="#F9F3FD" Enabled="false" />
                                                            <asp:Label runat="server" CssClass="form-control label-info" Text="*** Maximum Selection Limit: 8" Font-Size="Small" />
                                                            <asp:Label runat="server" ID="lblComments" CssClass="label" Text="" Font-Bold="true" ForeColor="Black" Font-Size="Small" />
                                                            <div id="divButtons" runat="server" class="input-group" style="width: 100%;">
                                                                <asp:LinkButton ID="btnCompareWellsData" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnCompareWellsData_Click"><i class="fa fa-check"> Compare</i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnClearMetaInfo" runat="server" CssClass="btn btn-warning text-center" OnClick="btnClearMetaInfo_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-success">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancChart" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFECComparison_2A_accordion" href="#BodyContent_ctrlFECComparison_2A_divChart">
                                                                Wells Comparison Chart - 3D </a>
                                                        </h5>
                                                    </div>
                                                    <div id="divChart" runat="server" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <div id="divECharts3D" runat="server" style="height: 700px;"></div>

                                                            <asp:Literal ID="ltrECHarts3DComparison" runat="server"></asp:Literal>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="input-group" style="width: 100%;">
                                                <asp:LinkButton ID="btnDetailCancel" runat="server" CssClass="btn btn-danger text-center" OnClick="btnDetailCancel_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="gvwWellList" />
                                            <asp:PostBackTrigger ControlID="btnClearMetaInfo" />
                                            <asp:PostBackTrigger ControlID="btnCompareWellsData" />
                                            <asp:PostBackTrigger ControlID="btnDetailCancel" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
