﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlFECenter2Analysis.ascx.cs" Inherits="SmartsVer1.Control.Services.ctrlFECenter2Analysis" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

<!-- Stylesheet -->
<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dataTables.bootstrap.min.css" rel="stylesheet" />
<!-- Javascript for ref lib -->
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>
<!--- local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }

    .rotate {
        -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
        -moz-transform: rotate(90deg); /* Firefox */
        -ms-transform: rotate(90deg); /* IE 9 */
        transform: rotate(90deg); /* Standard syntax */
    }
    /*custom anchor defi for menu items*/
    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }
</style>
<asp:UpdateProgress ID="uprgrsMeta" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlMetaInfo">
    <ProgressTemplate>
        <div id="divMetaProcessing" runat="server" class="updateProgress">
            <asp:Image ID="imgMetaProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<asp:UpdatePanel runat="server" ID="upnlMetaInfo">
    <ContentTemplate>
        <!--- Page Menu -->
        <div class="row bg-black">
            <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom: 2px; padding-left: 2px; padding-top: 2px">
                <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
            </div>
            <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
                <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
            </div>
        </div>
        <div class="row bg-black">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
                <asp:LoginView ID='LoginView1' runat='server'>
                    <AnonymousTemplate>
                        <li style="vertical-align: middle; display: table-cell;">
                            <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                                ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                        </li>
                    </AnonymousTemplate>
                    <LoggedInTemplate>
                        <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">(
                            <asp:LoginName ID="LoginName1" runat="server" />
                            )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                        </li>
                    </LoggedInTemplate>
                </asp:LoginView>
            </div>
        </div>
        <!-- Page Breadcrumb -->
        <div class="row bg-white">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box-tools pull-right">
                    <ol class="breadcrumb">
                        <li><a href="../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                        <li>SMARTs</li>
                        <li><a href="../../Services/fecAcCenter2Analysis.aspx">AC Center-to-Center Analysis</a></li>
                    </ol>
                </div>
            </div>
        </div>
        <!--- Page Content --->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row bg-white">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h5 class="panel-title" style="text-align: left">
                                <a id="ancAscMain" runat="server" class="disabled" href="#">Anti Collision Proxymity Analysis</a>
                            </h5>
                        </div>
                        <div runat="server"  class="col-xs-12 col-sm-12 col-md-12 col-lg-12 align-self-baseline" style="margin-top:15px;margin-bottom:15px;margin-left:3px">
                        <p style="font-size: x-large; font-family: Arial; text-align: center; color: darkgray; ">
                            <b>SMARTs Anti-Collision Feature for Planning, Real-Time Warning and Post Anlsysis</b>
                        </p>
                        </div>
                        <div id="divRvwOuter" runat="server">
                            <div id="divOuterBody" class="panel-body">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div id="divGagesOuter" class="panel panel-danger panel-body">
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-sm-offset-1" style="padding-right: 5px; padding-right: 5px">
                                            <div class="small-box bg-green">
                                                <div id="divBTGuageJob" class="inner" runat="server">
                                                </div>
                                                <div class="icon">
                                                    <i class="ion ion-clipboard"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="padding-right: 5px; padding-right: 5px">
                                            <div class="small-box bg-light-blue">
                                                <div id="divBTGuageWell" class="inner" runat="server">
                                                </div>
                                                <div class="icon">
                                                    <i class="ion ion-pin"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="padding-right: 5px; padding-right: 5px">
                                            <div class="small-box bg-blue">
                                                <div id="divBTGuageTotalRun" class="inner" runat="server">
                                                </div>
                                                <div class="icon">
                                                    <i class="ion ion-pulse"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="padding-right: 5px; padding-right: 5px">
                                            <div class="small-box bg-purple">
                                                <div id="divBTGuageAud_12" class="inner" runat="server">
                                                </div>
                                                <div class="icon">
                                                    <i class="ion ion-star-half"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="padding-right: 5px; padding-right: 5px">
                                            <div class="small-box bg-fuchsia">
                                                <div id="divBTGuageAud_14Days" class="inner" runat="server">
                                                </div>
                                                <div class="icon">
                                                    <i class="ion ion-star"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="divMetaInfoOuter" class="panel panel-danger">
                                        <div class="panel-body">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div id="divProgress" class="panel panel-danger">
                                                    <div class="panel-body">
                                                        <ol id="olStepper" runat="server" class="stepper" data-stepper-steps="5">
                                                            <li id="liStep1" runat="server" class="stepper-todo">Select Well / Lateral</li>
                                                            <li id="liStep2" runat="server" class="stepper-todo">Select Surrounding Well / Laterals</li>
                                                            <li id="liStep3" runat="server" class="stepper-todo">Select AC Threshold</li>
                                                            <li id="liStep4" runat="server" class="stepper-todo">Run Center to Center Analysis</li>
                                                        </ol>
                                                    </div>
                                                </div>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:LinkButton runat="server" ID="btnMetaInfoReset" CssClass="btn btn-danger text-center"  Width="150px" Enabled="false" OnClick="btnMetaInfoReset_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                </div>
                                                <br />
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-left: 0px">
                                                    <div class="panel panel-info">
                                                        <div class="panel-heading">
                                                            <h5 class="panel-title" style="text-align: left">
                                                                <a id="ancHead" runat="server" class="disabled" style="font-weight: bold;" href="#">Select Well/Lateral to be Analyzed</a>
                                                            </h5>
                                                        </div>
                                                        <div id="divWell" runat="server" visible="true" class="panel-body">
                                                            <div class="table-responsive">
                                                                <asp:GridView ID="gvwWellList" runat="server" CssClass="mydatagrid" EmptyDataText="No Wells associated with the selected Service Order #"
                                                                    AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="welID" OnSelectedIndexChanged="gvwWellList_SelectedIndexChanged"
                                                                    ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwWellList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                    PageSize="10" PagerSettings-Mode="Numeric" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Center" PagerStyle-VerticalAlign="Middle"
                                                                    HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                                                    <Columns>
                                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                            <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                        </asp:ButtonField>
                                                                        <asp:BoundField DataField="welID" Visible="false" />
                                                                        <asp:BoundField DataField="welName" HeaderText="Well / Lateral" />
                                                                        <asp:BoundField DataField="field" HeaderText="Oil & Gas Field" /> 
                                                                        <asp:BoundField DataField="county" HeaderText="County / District" />                                                                                                                                               
                                                                        <asp:BoundField DataField="state" HeaderText="State / Province" />
                                                                        <asp:BoundField DataField="optrN" HeaderText="Operator Company" />
                                                                        <asp:BoundField DataField="spud" HeaderText="Spud Date" />
                                                                    </Columns>
                                                                </asp:GridView>

                                                            </div>
                                                        </div>

                                                        <div id="divWellAC" runat="server" visible="false" class="panel-body">

                                                            <div class="panel panel-danger" style="font-size: larger">
                                                                <h3 style="text-align: center; margin-top: 5px; margin-bottom: 10px;">
                                                                    <a id="ancSelected" runat="server" style="font-weight: bold; color: darkblue;"  class="disabled" href="#">Select Wells/Laterals for Center to Center Analysis </a>
                                                                </h3>
                                                            </div>

                                                            <div class="table-responsive">
                                                                <asp:GridView ID="gvACWellComp" runat="server" CssClass="mydatagrid" EmptyDataText="No Wells associated with the selected Service Order #"
                                                                    AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="welID" OnSelectedIndexChanged="gvACWellComp_SelectedIndexChanged"
                                                                    ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvACWellComp_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                    PageSize="10" PagerSettings-Mode="Numeric" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Center" PagerStyle-VerticalAlign="Middle"
                                                                    HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                                                    <Columns>
                                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                            <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                        </asp:ButtonField>
                                                                        <asp:BoundField DataField="welID" Visible="false" />
                                                                        <asp:BoundField DataField="welName" HeaderText="Well / Lateral" />
                                                                        <asp:BoundField DataField="field" HeaderText="Oil & Gas Field" /> 
                                                                        <asp:BoundField DataField="county" HeaderText="County / District" />                                                                                                                                               
                                                                        <asp:BoundField DataField="state" HeaderText="State / Province" />
                                                                        <asp:BoundField DataField="optrN" HeaderText="Operator Company" />
                                                                        <asp:BoundField DataField="spud" HeaderText="Spud Date" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>

                                                        <div class="panel-body">
                                                            <div id="divSelected" runat="server" visible="false" class="panel-body">
                                                                <div class="panel-body">
                                                                    <asp:TextBox runat="server" ID="txtbxComments" CssClass="form-control" TextMode="MultiLine" Height="125px" MaxLength="2500"
                                                                        BackColor="#F9F3FD" Enabled="false" />
                                                                    <asp:Label ID="Label1" runat="server" CssClass="form-control label-info" Text="*** Maximum Selection Limit: 4" Font-Size="Small" />
                                                                    <asp:Label runat="server" ID="lblComments" CssClass="label" Text="" Font-Bold="true" ForeColor="Black" Font-Size="Small" />

                                                                </div>
                                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                    <div class="panel-body" style="padding-left: 4px; padding-top: 0px">
                                                                        <h2>
                                                                            <span class="input-group-addon" style="font-weight: bold; font-size: medium; background-color: lightgray; height: 60px;  color: darkblue;">Select Center to Center Theshold</span>
                                                                        </h2>
                                                                        <asp:DropDownList ID="ddlACSelectThreshold" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control" OnSelectedIndexChanged="ddlACSelectThreshold_SelectedIndexChanged">
                                                                            <asp:ListItem Selected="True" Value="0" Text="--- Select The Threashold for Center to Center Analysis ---"></asp:ListItem>
                                                                            <asp:ListItem Value="1">10</asp:ListItem>
                                                                            <asp:ListItem Value="2">20</asp:ListItem>
                                                                            <asp:ListItem Value="3">30</asp:ListItem>
                                                                            <asp:ListItem Value="4">40</asp:ListItem>
                                                                            <asp:ListItem Value="5">50</asp:ListItem>
                                                                            <asp:ListItem Value="6">60</asp:ListItem>
                                                                            <asp:ListItem Value="7">80</asp:ListItem>
                                                                            <asp:ListItem Value="8">100</asp:ListItem>
                                                                            <asp:ListItem Value="9">150</asp:ListItem>
                                                                            <asp:ListItem Value="10">200</asp:ListItem>
                                                                            <asp:ListItem Value="11">300</asp:ListItem>
                                                                            <asp:ListItem Value="12">350</asp:ListItem>
                                                                            <asp:ListItem Value="13">550</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <br />
                                                                        <div id="divButtons" runat="server" class="input-group" style="width: 100%;">
                                                                            <asp:LinkButton ID="btnProceedOne" runat="server" CssClass="btn btn-default text-center" Width="200px" Enabled="false" OnClick="btnProceedOne_Click"><i class="fa fa-check"> Proceed Next </i></asp:LinkButton>
                                                                            &nbsp;        
                                                                        <asp:LinkButton ID="btnClearSelection" runat="server" CssClass="btn btn-warning text-center" Width="200px" OnClick="btnClearSelection_Click"><i class="fa fa-refresh"> Clear Selection </i></asp:LinkButton>
                                                                        <asp:LinkButton ID="btnDisplayChart" runat="server" Visible="false"  CssClass="btn btn-primary text-center" OnClick="btnDisplayChart_Click" ><i class="fa fa-2x"></i></asp:LinkButton>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div id="divChart" class="panel" runat="server" visible="false">
                                                                <div class="panel-heading">
                                                                    <h2 class="bg-gray box-title" style="text-align: center">Anti-Collision Proximity Analysis - 3D</h2>
                                                                </div>
                                                                <div class="panel-body">
                                                                    <div id="divECharts3D" runat="server" style="height: 700px;"></div>
                                                                    <asp:Literal ID="ltrECHarts3DComparison" runat="server"></asp:Literal>
                                                                </div>
                                                            </div>


                                                            <div id="divAnalysisTable" runat="server" visible="false" class="panel-body">

                                                                <div class="panel panel-danger" style="font-size: larger">
                                                                    <h3 style="text-align: center; margin-top: 5px; margin-bottom: 10px;">
                                                                        <a id="a1" runat="server" style="font-weight: bold; color: darkblue" class="disabled" href="#">Anti Collision Proximity Analysis Report </a>
                                                                    </h3>
                                                                </div>



                                                                <div class="table-responsive">
                                                                    <asp:GridView ID="gvxProximityAnalysis" runat="server" CssClass="mydatagrid" EmptyDataText="No Analysis Data"
                                                                        AllowPaging="True" AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                        PageSize="40" PagerSettings-Mode="Numeric" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Center" PagerStyle-VerticalAlign="Middle"
                                                                        HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow"
                                                                        OnPageIndexChanging="gvxProximityAnalysis_PageIndexChanging">
                                                                        <Columns>
                                                                            <asp:BoundField DataField="Survey_number" Visible="false" />
                                                                            <asp:BoundField DataField="MDepth" HeaderText="Measured Depth" />
                                                                            <asp:BoundField DataField="TVD" HeaderText="TVD" />
                                                                            <asp:BoundField DataField="SubDepth" HeaderText="SubSea Depth" />
                                                                            <asp:BoundField DataField="Northing"  Visible="false" />
                                                                            <asp:BoundField DataField="Easting"  Visible="false" />
                                                                            <asp:BoundField DataField="WellName" HeaderText="Lateral being Analyzed" />
                                                                            <asp:BoundField DataField="CummDeltaNorthing" HeaderText="UTM Northing" />
                                                                            <asp:BoundField DataField="CummDeltaEasting" HeaderText="UTM Easting Date" />
                                                                            <asp:BoundField DataField="Surrounding_Lateral" HeaderText="Surrounting Lateral" />
                                                                            <asp:BoundField DataField="Distance_Comment" HeaderText="Analysis" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>

                                                            </div>
                                                        </div>


                                                        </div>


                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
    <Triggers>
                <asp:PostBackTrigger ControlID="btnProceedOne" />
                <asp:PostBackTrigger ControlID="btnDisplayChart" />
        <asp:PostBackTrigger ControlID="gvxProximityAnalysis" />  
    </Triggers>
</asp:UpdatePanel>

