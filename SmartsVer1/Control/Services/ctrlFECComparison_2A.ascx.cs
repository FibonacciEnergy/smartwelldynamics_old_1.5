﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using QC = SmartsVer1.QCHelpers.sdRawDataQC;
using CHRT = SmartsVer1.Helpers.ChartHelpers.Gauges;
using CHRTRes = SmartsVer1.Helpers.ChartHelpers.ChartResources;
using log4net;

namespace SmartsVer1.Control.Services
{
    public partial class ctrlFECComparison_2A : System.Web.UI.UserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ctrlFECComparison_2A));
        String LoginName, username, domain, GetClientDBString, wellName;
        Int32 ClientType = 4, ClientID = -99, jobCount = 0, wellCount = 0, runCount = 0, _24Count = 0, _14Count = 0;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {               
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientType);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = 0;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");
                this.createAssetCharts(null, null);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwGlobalOptr_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientType);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = e.NewPageIndex;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwGlobalOptr_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                System.Data.DataTable pdTable = AST.getComparisonWellPadTableForOperator(oprID, ClientID, GetClientDBString);
                this.gvwWellPads.DataSource = pdTable;
                this.gvwWellPads.PageIndex = 0;
                this.gvwWellPads.SelectedIndex = -1;
                this.gvwWellPads.DataBind();
                this.divOperator.Attributes["class"] = "panel-collapse collapse";
                this.divWellPad.Attributes["class"] = "panel-collapse collpase in";
                this.divWellList.Attributes["class"] = "panel-collapse collpase";
                this.divSelected.Attributes["class"] = "panel-collapse collapse in";
                this.divChart.Attributes["class"] = "panel-collapse collapse";
                GridViewRow dRow = this.gvwGlobalOptr.SelectedRow;
                String opName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.ancCompany.InnerHtml = "Operator Company: <span style='text-decoration: underline; font-weight: bold'>" + opName + "</span>";
                this.ancPad.InnerHtml = "Well Pad";
                this.ancWell.InnerHtml = "Well / Lateral";
                this.ancSelected.InnerHtml = "Selected Wells/Laterals";
                this.ancChart.InnerHtml = "Wells Comparison Chart - 3D";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellPads_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                System.Data.DataTable pdTable = AST.getComparisonWellPadTableForOperator(oprID, ClientID, GetClientDBString);
                this.gvwWellPads.DataSource = pdTable;
                this.gvwWellPads.PageIndex = e.NewPageIndex;
                this.gvwWellPads.SelectedIndex = -1;
                this.gvwWellPads.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellPads_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 pdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                System.Data.DataTable wlTable = AST.getClientWellTableWithResultsByWellPad(pdID, GetClientDBString);
                this.gvwWellList.DataSource = wlTable;
                this.gvwWellList.PageIndex = 0;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();
                this.divOperator.Attributes["class"] = "panel-collapse collapse";
                this.divWellPad.Attributes["class"] = "panel-collapse collapse";
                this.divWellList.Attributes["class"] = "panel-collapse collpase in";
                this.divSelected.Attributes["class"] = "panel-collapse collapse in";
                this.divChart.Attributes["class"] = "panel-collapse collapse";
                GridViewRow dRow = this.gvwWellPads.SelectedRow;
                String wpName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.ancPad.InnerHtml = "Well Pad: <span style='text-decoration: underline; font-weight: bold'>" + wpName + "</span>";
                this.ancWell.InnerHtml = "Well / Lateral";
                this.ancSelected.InnerHtml = "Selected Wells/Laterals";
                this.ancChart.InnerHtml = "Wells Comparison Chart - 3D";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDetailCancel_Click(object sender, EventArgs e)
        {
            try
            {                  
                this.btnClearMetaInfo_Click(null, null);
                this.btnCompareWellsData.Enabled = false;
                this.btnCompareWellsData.CssClass = "btn btn-default text-center";
                this.btnDetailCancel.Enabled = false;
                this.btnDetailCancel.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void createChartQCTotal()
        {
            try
            {
                String echarts3D = String.Empty;
                String wellListString = String.Empty;
                //System.Data.DataTable dsChartData = new System.Data.DataTable();
                System.Data.DataTable dsChartData_3DPlacement = new System.Data.DataTable();
                System.Data.DataTable getScalerEastingNorthing = new System.Data.DataTable();
                Decimal maxTVD = 0.00M;
                List<int> wellsIDs = new List<int>();
                foreach (var pair in (Dictionary<int, string>)Session["wellsID"])
                {
                    wellsIDs.Add(pair.Key);
                    wellListString += pair.Value + " , ";
                }
                //dsChartData = CHRTRes.getDataTableforQCWellCompare(wellsIDs, GetClientDBString);
                dsChartData_3DPlacement = CHRTRes.getDataTableforQCWellCompare(wellsIDs, GetClientDBString);
                maxTVD = CHRTRes.getMaxTVDforWellsInComparrison(wellsIDs, GetClientDBString);
                getScalerEastingNorthing = CHRTRes.getScallingFactorChartInfo_Comparison(wellsIDs, GetClientDBString);
                try
                {
                    echarts3D = CHRT.getChartsQCResultsECHRTComparison_3D("BodyContent_ctrlFECComparison_2A_divECharts3D", dsChartData_3DPlacement, getScalerEastingNorthing, maxTVD);
                    this.ltrECHarts3DComparison.Text = echarts3D;
                    logger.Info("User: " + username + " Data_Comparison: Display Charts for Well/Later: " + wellListString + "  Selected");
                }
                
                
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
                finally
                {
                    dsChartData_3DPlacement.Dispose();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
      
        protected void createAssetCharts(object sender, EventArgs e)
        {
            try
            {
                List<Int32> summaryDetails = CHRT.getClientDataReviewSummary(ClientID, GetClientDBString);
                jobCount = summaryDetails[0];
                wellCount = summaryDetails[1];
                runCount = summaryDetails[2];
                _24Count = summaryDetails[3];
                _14Count = summaryDetails[4];

                this.divBTGuageJob.InnerHtml = "<h3>" + jobCount.ToString() + "</h3><p>In Process Well(s)/Lateral(s)</p>";
                this.divBTGuageWell.InnerHtml = "<h3>" + wellCount.ToString() + "</h3><p>Well(s) / Lateral(s)</p>";
                this.divBTGuageTotalRun.InnerHtml = "<h3>" + runCount.ToString() + "</h3><p>Run(s)</p>";
                this.divBTGuageAud_12.InnerHtml = "<h3>" + _24Count.ToString() + "</h3><p>Audit/QC 24hr</p>";
                this.divBTGuageAud_14Days.InnerHtml = "<h3>" + _14Count.ToString() + "</h3><p>Audit/QC 14days</p>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.lblComments.Text = "";                
                Dictionary<Int32, String> wl = new Dictionary<Int32, String>();                
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                String wellname = Convert.ToString(this.gvwWellList.SelectedRow.Cells[2].Text);                
                if (Session["wellsID"] != null)
                {
                    Dictionary<int, string> temp = (Dictionary<int,string>)Session["wellsID"];
                    if (temp.Count <= 8)
                    {
                        wl.Add(wlID, wellname);
                        if (!temp.ContainsKey(wlID))
                        {
                            foreach (var pair in wl)
                            {
                                temp.Add(pair.Key, pair.Value);
                            }
                        }
                        else
                        {
                            this.lblComments.Text = "Well Already Added";
                        }
                    }
                    else {
                        this.lblComments.Text = "Wells Limit has Reached !";
                    }
                    HttpContext.Current.Session["wellsID"] = null;
                    HttpContext.Current.Session["wellsID"] = temp;
                }
                else 
                {
                    wl.Add(wlID, wellname);
                    HttpContext.Current.Session["wellsID"] = wl;
                }                
                this.btnDetailCancel.Enabled = true;
                this.btnDetailCancel.CssClass = "btn btn-danger text-center";
                List<String> printWell = new List<String>();
                foreach (var pair in (Dictionary<int,string>)Session["wellsID"])
                {
                  printWell.Add(pair.Value);
                }
                this.txtbxComments.Text = String.Join(Environment.NewLine, printWell) ;                
                this.divOperator.Attributes["class"] = "panel-collapse collapse";
                this.divWellPad.Attributes["class"] = "panel-collapse collapse";
                this.divWellList.Attributes["class"] = "panel-collapse collapse in";
                this.divSelected.Attributes["class"] = "panel-collapse collapse in";
                this.divChart.Attributes["class"] = "panel-collapse collapse";
                this.btnCompareWellsData.Enabled = true;
                this.btnCompareWellsData.CssClass = "btn btn-success text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }


        protected void gvwWellList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {                
                //SaveCheckedValues();
                System.Data.DataTable wlList = AST.getClientWellTableWithResults(GetClientDBString);
                this.gvwWellList.DataSource = wlList;
                this.gvwWellList.PageIndex = e.NewPageIndex;
                this.gvwWellList.DataBind();
                this.gvwWellList.SelectedIndex = -1;
                //PopulateCheckedValues();                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnClearMetaInfo_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientType);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = 0;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
                this.gvwWellPads.DataSource = null;
                this.gvwWellPads.DataBind();
                this.gvwWellList.DataSource = null;
                this.gvwWellList.DataBind();
                this.divOperator.Attributes["class"] = "panel-collapse collapse in";
                this.divWellPad.Attributes["class"] = "panel-collapse collapse";
                this.divWellList.Attributes["class"] = "panel-collapse collapse";
                this.divSelected.Attributes["class"] = "panel-collapse collapse in";
                this.divChart.Attributes["class"] = "panel-collapse collapse";
                this.ancCompany.InnerHtml = "Operator Company";
                this.ancPad.InnerHtml = "Well Pad";
                this.ancWell.InnerHtml = "Well / Lateral";
                this.ancSelected.InnerHtml = "Selected Wells/Laterals";
                this.ancChart.InnerHtml = "Wells Comparison Chart - 3D";
                this.lblComments.Text = "";
                this.txtbxComments.Text = "";
                this.btnCompareWellsData.Enabled = false;
                this.btnCompareWellsData.CssClass = "btn btn-default text-center";
                HttpContext.Current.Session["wellsID"] = null;                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCompareWellsData_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    this.createChartQCTotal(); // Creating Graph
                    this.divOperator.Attributes["class"] = "panel-collapse collapse";
                    this.divWellPad.Attributes["class"] = "panel-collapse collapse";
                    this.divWellList.Attributes["class"] = "panel-collapse collapse";
                    this.divSelected.Attributes["class"] = "panel-collapse collapse in";
                    this.divChart.Attributes["class"] = "panel-collapse collapse in";
                }
                else
                {
                    this.createChartQCTotal(); // Creating Graph
                    this.divOperator.Attributes["class"] = "panel-collapse collapse";
                    this.divWellPad.Attributes["class"] = "panel-collapse collapse";
                    this.divWellList.Attributes["class"] = "panel-collapse collapse";
                    this.divSelected.Attributes["class"] = "panel-collapse collapse in";
                    this.divChart.Attributes["class"] = "panel-collapse collapse in";
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }                    
    }
}