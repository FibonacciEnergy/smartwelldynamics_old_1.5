﻿using log4net;
using System;
using System.Web;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using ACCent = SmartsVer1.Helpers.ACCenter2Helpers.ACCenterToCenter;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using CHRT = SmartsVer1.Helpers.ChartHelpers.Gauges;
using CHRTRes = SmartsVer1.Helpers.ChartHelpers.ChartResources;
using Color = System.Drawing.Color;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using OPR = SmartsVer1.Helpers.QCHelpers.Opr;
using QC = SmartsVer1.QCHelpers.sdRawDataQC;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using SRV = SmartsVer1.Helpers.DemogHelpers.ServicesConfig;

namespace SmartsVer1.Control.Services
{

    public partial class ctrlFECenter2Analysis : System.Web.UI.UserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ctrlFECDataReview_2A));
        String LoginName, username, domain, GetClientDBString, wellName;
        Int32 ClientID = -99, jobCount = 0, wellCount = 0, runCount = 0, _24Count = 0, _14Count = 0;
        const Int32 serviceGroupID = 2;
        const Int32 ClientType = 4;

        System.Data.DataTable AnalysisTableInformation = new DataTable();

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);


                System.Data.DataTable wellfromPlacement = AST.getWellDataFromPlacement(GetClientDBString);
                this.gvwWellList.DataSource = wellfromPlacement;
                this.gvwWellList.PageIndex = 0;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();

                this.txtbxComments.Text = "";
                
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientType);

                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");
                if (Page.IsPostBack)
                {
                    this.createAssetCharts(null, null);
                }
                else
                {
                    this.createAssetCharts(null, null);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }

        }

        protected void btnChart_Click(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.createChartQCTotal(); // Creating Graph
            }
            else
            {
                this.createChartQCTotal(); // Creating Graph
            }
        }

        protected void createAssetCharts(object sender, EventArgs e)
        {
            try
            {
                List<Int32> summaryDetails = CHRT.getClientDataReviewSummary(ClientID, GetClientDBString);
                jobCount = summaryDetails[0];
                wellCount = summaryDetails[1];
                runCount = summaryDetails[2];
                _24Count = summaryDetails[3];
                _14Count = summaryDetails[4];

                this.divBTGuageJob.InnerHtml = "<h3>" + jobCount.ToString() + "</h3><p>In Process Well(s)/Lateral(s)</p>";
                this.divBTGuageWell.InnerHtml = "<h3>" + wellCount.ToString() + "</h3><p>Total Wells</p>";
                this.divBTGuageTotalRun.InnerHtml = "<h3>" + runCount.ToString() + "</h3><p>Total Run</p>";
                this.divBTGuageAud_12.InnerHtml = "<h3>" + _24Count.ToString() + "</h3><p>Audit/QC 24hr</p>";
                this.divBTGuageAud_14Days.InnerHtml = "<h3>" + _14Count.ToString() + "</h3><p>Audit/QC 14days</p>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnMetaInfoReset_Click(object sender, EventArgs e)
        {
            try
            {
                //System.Data.DataTable wellfromPlacement = AST.getWellDataFromPlacement(GetClientDBString);
                //this.gvwWellList.DataSource = wellfromPlacement;
                this.gvwWellList.PageIndex = 0;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();

                //this.gvACWellComp.DataSource = null;
                //this.gvACWellComp.DataBind();

                HttpContext.Current.Session["wellCompare"] = null;
                HttpContext.Current.Session["wellName"] = null;
                HttpContext.Current.Session["wellID"] = null;
                HttpContext.Current.Session["ACThreshold"] = null;

                this.liStep1.Attributes["class"] = "stepper-todo";
                this.liStep2.Attributes["class"] = "stepper-todo";
                this.liStep3.Attributes["class"] = "stepper-todo";
                this.liStep4.Attributes["class"] = "stepper-todo";

                this.ancHead.InnerHtml = "Select Well/Lateral to be Analyzed";

                this.btnMetaInfoReset.Enabled = false;
                this.btnMetaInfoReset.CssClass = "btn btn-danger text-center";

                this.gvACWellComp.Visible = false;
                this.divWellAC.Visible = false;
                this.divSelected.Visible = false;
                this.divChart.Visible = false;
                this.gvwWellList.Visible = true;
                this.txtbxComments.Text = "";
                this.btnProceedOne.Enabled = false;
                this.btnProceedOne.CssClass = "btn btn-default text-center";

                this.ddlACSelectThreshold.SelectedIndex = 0;
                this.AnalysisTableInformation.Clear();
                this.divAnalysisTable.Visible = false;

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                System.Data.DataTable WellsFromSameField = AST.getWellDataFromPlacementSameField(wlID, GetClientDBString);
                this.gvACWellComp.DataSource = WellsFromSameField;
                this.gvACWellComp.PageIndex = 0;
                this.gvACWellComp.SelectedIndex = -1;
                this.gvACWellComp.DataBind();

                this.gvACWellComp.Visible = true;
                this.divWellAC.Visible = true;
                //this.divSelected.Visible = true;

                this.gvwWellList.Visible = false;

                this.liStep1.Attributes["class"] = "stepper-done";
                this.liStep2.Attributes["class"] = "stepper-todo";
                this.liStep3.Attributes["class"] = "stepper-todo";
                this.liStep4.Attributes["class"] = "stepper-todo";

                GridViewRow wRow = this.gvwWellList.SelectedRow;

                
                String wName = Convert.ToString(this.gvwWellList.SelectedRow.Cells[2].Text);


                // Storing Well Name for chart label use
                HttpContext.Current.Session["wellName"] = wName;
                HttpContext.Current.Session["wellID"] = wlID;


                this.ancHead.InnerHtml = "<strong> Selected For Analysis: " + wName + "</strong>";
                this.btnMetaInfoReset.Enabled = true;
                this.btnMetaInfoReset.CssClass = "btn btn-danger text-center";

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {

                System.Data.DataTable wellfromPlacement = AST.getWellDataFromPlacement(GetClientDBString);
                this.gvwWellList.DataSource = wellfromPlacement;
                this.gvwWellList.PageIndex = e.NewPageIndex;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvACWellComp_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            try
            {
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                System.Data.DataTable WellsFromSameField = AST.getWellDataFromPlacementSameField(wlID, GetClientDBString);

                this.gvACWellComp.DataSource = WellsFromSameField;
                this.gvACWellComp.PageIndex = e.NewPageIndex;
                this.gvACWellComp.SelectedIndex = -1;
                this.gvACWellComp.DataBind();

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvACWellComp_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.lblComments.Text = "";
                this.divSelected.Visible = true;
                Dictionary<Int32, String> wl = new Dictionary<Int32, String>();
                Int32 wlID = Convert.ToInt32(this.gvACWellComp.SelectedValue);
                String wellname = Convert.ToString(this.gvACWellComp.SelectedRow.Cells[2].Text);
                if (Session["wellCompare"] != null)
                {
                    Dictionary<int, string> temp = (Dictionary<int, string>)Session["wellCompare"];
                    if (temp.Count <= 4)
                    {
                        wl.Add(wlID, wellname);
                        if (!temp.ContainsKey(wlID))
                        {
                            foreach (var pair in wl)
                            {
                                temp.Add(pair.Key, pair.Value);
                            }
                        }
                        else
                        {
                            this.lblComments.Text = "Well Already Added";
                        }
                    }
                    else
                    {
                        this.lblComments.Text = "Wells/Lateral Limit has Reached for Comparison !";
                    }
                    HttpContext.Current.Session["wellCompare"] = null;
                    HttpContext.Current.Session["wellCompare"] = temp;
                }
                else
                {
                    wl.Add(wlID, wellname);
                    HttpContext.Current.Session["wellCompare"] = wl;
                }

                List<String> printWell = new List<String>();
                foreach (var pair in (Dictionary<int, string>)Session["wellCompare"])
                {
                    printWell.Add(pair.Value);
                }
                this.txtbxComments.Text = String.Join(Environment.NewLine, printWell);
                

                this.liStep1.Attributes["class"] = "stepper-done";
                this.liStep2.Attributes["class"] = "stepper-done";
                this.liStep3.Attributes["class"] = "stepper-todo";
                this.liStep4.Attributes["class"] = "stepper-todo";

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }

        }

        protected void btnClearSelection_Click(object sender, EventArgs e)
        {
            try
            {
                HttpContext.Current.Session["wellCompare"] = null;
                HttpContext.Current.Session["wellName"] = null;
                HttpContext.Current.Session["wellID"] = null;
                HttpContext.Current.Session["ACThreshold"] = null;

                this.txtbxComments.Text = "";
                this.btnProceedOne.Enabled = false;
                this.btnProceedOne.CssClass = "btn btn-default text-center";

                this.ddlACSelectThreshold.SelectedIndex = 0;

                this.liStep1.Attributes["class"] = "stepper-done";
                this.liStep2.Attributes["class"] = "stepper-todo";
                this.liStep3.Attributes["class"] = "stepper-todo";
                this.liStep4.Attributes["class"] = "stepper-todo";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }

        }

        protected void ddlACSelectThreshold_SelectedIndexChanged(object sender, EventArgs e)
        {
            try {

                if (Convert.ToInt32(this.ddlACSelectThreshold.SelectedValue).Equals(0))
                {
                    HttpContext.Current.Session["ACThreshold"] = 0;
                }
                else
                {
                    Int32 printcount = Convert.ToInt32(this.ddlACSelectThreshold.SelectedItem.Text);
                    HttpContext.Current.Session["ACThreshold"] = printcount;
                }

                this.liStep1.Attributes["class"] = "stepper-done";
                this.liStep2.Attributes["class"] = "stepper-done";
                this.liStep3.Attributes["class"] = "stepper-done";
                this.liStep4.Attributes["class"] = "stepper-todo";

                this.btnProceedOne.Enabled = true;
                this.btnProceedOne.CssClass = "btn btn-primary text-center";

            
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProceedOne_Click(object sender, EventArgs e)
        {

            try {

                this.divSelected.Visible = false;
                this.divWellAC.Visible = false;
                this.divChart.Visible = true;

                Int32 wellToBeAnalyzed = Convert.ToInt32(HttpContext.Current.Session["wellID"]);

                String echarts3D = String.Empty;
                String wellListString = String.Empty;
                Int32 acThreshold = Convert.ToInt32(HttpContext.Current.Session["ACThreshold"]);

                System.Data.DataTable dsChartData_3DPlacement = new System.Data.DataTable();
                System.Data.DataTable getScalerEastingNorthing = new System.Data.DataTable();

                System.Data.DataTable getFilter = new System.Data.DataTable();

                List<int> wellsIDs = new List<int>();
                foreach (var pair in (Dictionary<int, string>)Session["wellCompare"])
                {
                    wellsIDs.Add(pair.Key);
                    wellListString += pair.Value + " , ";
                }

                // Well to be Analyzed
                wellsIDs.Add(wellToBeAnalyzed);

                if (!IsPostBack)
                {
                    //this.createChartQCTotal(); // Creating Graph
                    this.btnChart_Click(null, null);
                    this.generateDataTable();
                    
                    
                }
                else
                {
                   //this.createChartQCTotal(); // Creating Graph
                   this.btnChart_Click(null, null);
                   this.generateDataTable();
                }

                this.divChart.Visible = true;
                this.divAnalysisTable.Visible = true;

                this.liStep1.Attributes["class"] = "stepper-done";
                this.liStep2.Attributes["class"] = "stepper-done";
                this.liStep3.Attributes["class"] = "stepper-done";
                this.liStep4.Attributes["class"] = "stepper-done";

            
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }

        }


        protected void createChartQCTotal()
        {

            try
            {
                Int32 wellToBeAnalyzed = Convert.ToInt32(HttpContext.Current.Session["wellID"]);
                Int32 acThreshold = Convert.ToInt32(HttpContext.Current.Session["ACThreshold"]);
                String echarts3D = String.Empty;
                String wellListString = String.Empty;
                String wellName = Convert.ToString(HttpContext.Current.Session["wellName"]);

                System.Data.DataTable dsChartDataWellToBeAnalyzed = new System.Data.DataTable();
                System.Data.DataTable dsChartDataWellToBeAnalyzed_PLAN = new System.Data.DataTable();
                System.Data.DataTable dsChartDataWellToBeAnalyzed_Filtered = new System.Data.DataTable();

                System.Data.DataTable dsChartData_3DPlacement = new System.Data.DataTable();
                System.Data.DataTable dsChartData_3DPlacement_Filtered = new System.Data.DataTable();

                System.Data.DataTable getScalerEastingNorthing = new System.Data.DataTable();

                System.Data.DataTable FilteredValues_MainWell = new System.Data.DataTable();
                System.Data.DataTable FilteredValues_SurroundingWells = new System.Data.DataTable();

                System.Data.DataTable max_min_SubSea = new System.Data.DataTable();

                
                List<int> wellsIDs = new List<int>();
                foreach (var pair in (Dictionary<int, string>)Session["wellCompare"])
                {
                    wellsIDs.Add(pair.Key);
                    wellListString += pair.Value + " , ";
                }

                DataSet FilteredData = new DataSet();

                // Running the Analysis and returning the Data which are with in the Filtered thresold from Main well as well as from Surrounding
                FilteredData = ACCent.getDataWithInThresholdEN(acThreshold, wellToBeAnalyzed, wellsIDs, GetClientDBString);

                //FilteredValues_MainWell = ACCent.getDataWithInThresholdEN(acThreshold, wellToBeAnalyzed, wellsIDs, GetClientDBString);
                FilteredValues_SurroundingWells = ACCent.getDataWithInThresholdENSurrounding(acThreshold, wellToBeAnalyzed, wellsIDs, GetClientDBString);

                // Fetching the for Well to be Analyzed. The Actual plus the Plan
                dsChartDataWellToBeAnalyzed = ACCent.getDataSingleWellEastNorth(wellToBeAnalyzed, GetClientDBString);
                dsChartDataWellToBeAnalyzed_PLAN = ACCent.getPLANDataSingleWellEastNorth(wellToBeAnalyzed, GetClientDBString);

                // Surrounding Wells 
                dsChartData_3DPlacement = ACCent.getSurroundingWellDataEastNorth(wellsIDs, GetClientDBString);

                wellsIDs.Add(wellToBeAnalyzed);  // Adding Well to be Analyzed into WellIDs i.e. Adding one more well..

                getScalerEastingNorthing = CHRTRes.getScallingFactorChartInfo_Comparison(wellsIDs, GetClientDBString);
                max_min_SubSea = CHRTRes.getMinMaxSubSeaAC(wellsIDs, GetClientDBString);

                //FilteredValues_SurroundingWells = FilteredData.Tables[0]; // Filtered Data of Surrounding Wells meeting threshold
                FilteredValues_MainWell = FilteredData.Tables[1]; // Filtered Data of Main Well meeting threshold

                AnalysisTableInformation = FilteredValues_MainWell;


                try
                {
                    
                    echarts3D = CHRT.getChartsQCResultsECHRTAntiCollisionCent2Cent_3D("BodyContent_ctrlFECenter2Analysis_divECharts3D", wellName, dsChartDataWellToBeAnalyzed, dsChartData_3DPlacement, FilteredValues_MainWell, FilteredValues_SurroundingWells, dsChartDataWellToBeAnalyzed_PLAN, getScalerEastingNorthing, max_min_SubSea);
                    this.ltrECHarts3DComparison.Text = echarts3D; 
                    logger.Info("User: " + username + " Data_Comparison: Display Charts for Well/Later: " + wellListString + "  Selected");

                }


                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
                finally
                {
                    dsChartData_3DPlacement.Dispose();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }

        }

        protected void generateDataTable()
        {
            this.gvxProximityAnalysis.DataSource = AnalysisTableInformation;
            this.gvxProximityAnalysis.PageIndex = 0;
            this.gvxProximityAnalysis.SelectedIndex = -1;
            this.gvxProximityAnalysis.DataBind();
        
        }


        protected void gvxProximityAnalysis_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.btnChart_Click(null, null);
            
            this.gvxProximityAnalysis.DataSource = AnalysisTableInformation;
            this.gvxProximityAnalysis.PageIndex = e.NewPageIndex;
            this.gvxProximityAnalysis.SelectedIndex = -1;
            this.gvxProximityAnalysis.DataBind();
        }

        protected void btnDisplayChart_Click(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.createChartQCTotal(); // Creating Graph
            }
            else
            {
                this.createChartQCTotal(); // Creating Graph
            }
        }




    }
}


