﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlCorrQC.ascx.cs" Inherits="SmartsVer1.Control.Services.ctrlCorrQC" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

<!-- Stylesheet -->
<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dataTables.bootstrap.min.css" rel="stylesheet" />
<!-- Javascript for ref lib -->
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>
<!--- local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }
    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }
</style>
<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">        
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>SMARTs</li>
                <li><a href="../../Services/fesRawCorr_2A.aspx"> SMARTs Multi-Station Analysis</a></li>
            </ol>
        </div>
    </div>    
</div>
<!-- Page Content -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-danger">
                <div class="panel-body">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div id="divSolar" class="bg-danger">
                            <div class="panel-body">
                                <div class="col-xs-3 text-left">
                                    <asp:Image ID="imgSolar" runat="server" ImageUrl="~/Images/SolarRays.png" ImageAlign="Middle" />
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>
                                        <h4>Solar X-Rays Activity</h4>
                                    </div>
                                    <div id="divSolarLnk" runat="server">
                                        <asp:ImageButton ID="imgbSolarLink" runat="server" Width="40%" ImageUrl="http://www.n3kl.org/sun/images/status.gif?"
                                            ImageAlign="AbsMiddle" OnClick="imgbtnSolar_Click" CausesValidation="False" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div id="divMag" class="bg-danger">
                            <div class="panel-body">
                                <div class="col-xs-3">
                                    <asp:Image ID="imgMag" runat="server" ImageUrl="~/Images/MagField.png" ImageAlign="Middle" Height="90px" />
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>
                                        <h4>Geomagnetic Field Activity</h4>
                                    </div>
                                    <div id="divMagLnk" runat="server">
                                        <asp:ImageButton ID="imgbMag" runat="server" Width="40%" ImageUrl="http://www.n3kl.org/sun/images/kpstatus.gif?"
                                            ImageAlign="AbsMiddle" OnClick="imgbtnGeoMag_Click" CausesValidation="False" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div id="divGlobe" class="bg-danger">
                            <div class="panel-body">
                                <div class="col-xs-3">
                                    <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/MagField_2.png" ImageAlign="Middle" Height="90px" />
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>
                                        <h4>Regional Magnetic Activity</h4>
                                    </div>
                                    <div id="divGlobeLnk" runat="server">
                                        <a href="http://geomag.usgs.gov/realtime/" target="_blank" style="color: black;">USA</a> |
                                                    <a href="http://www.spaceweather.ca/current-actuelle/short-court/sfst-5-eng.php" target="_blank" style="color: black;">Canada</a>
                                        |
                                                    <a href="http://www.intermagnet.org/activitymap/activitymap-eng.php" target="_blank" style="color: black;">Global</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div id="panelGages" class="panel panel-danger">
                <div class="panel-body">
                    <div id="divGuages" runat="server" class="col-xs-12 col-sm-12 col-md-offset-1 col-lg-offset-1">
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                            <div class="small-box bg-blue">
                                <div id="divGuageJob" runat="server" class="inner" />
                                <div class="icon">
                                    <i class="fa fa-list" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                            <div class="small-box bg-orange-active">
                                <div id="divGuageWell" runat="server" class="inner" />
                                <div class="icon">
                                    <i class="fa fa-tasks" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                            <div class="small-box bg-olive">
                                <div id="divAscWell" runat="server" class="inner" />
                                <div class="icon">
                                    <i class="fa fa-chain-broken" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                            <div class="small-box bg-olive">
                                <div id="divMRM" runat="server" class="inner" />
                                <div class="icon">
                                    <i class="fa fa-heartbeat" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                            <div class="small-box bg-olive">
                                <div id="divMsngBHA" runat="server" class="inner" />
                                <div class="icon">
                                    <i class="fa fa-chain-broken" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<asp:UpdateProgress ID="upgrsCorrQC" runat="server" AssociatedUpdatePanelID="upnlCorrQC" DisplayAfter="0">
    <ProgressTemplate>
        <div id="divMetaProcessing" runat="server" class="updateProgress">
            <asp:Image ID="imgMetaProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<asp:UpdatePanel runat="server" ID="upnlCorrQC">
    <ContentTemplate>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row bg-white">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h5 class="panel-title" style="text-align: left">
                                <a id="ancHeading" runat="server" class="disabled" href="#">Operator Company</a>
                            </h5>
                        </div>
                        <div class="panel-body">
                            <div id="divCancelButtonRow" class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="input-group" style="width: 100%;">
                                        <asp:Button ID="btnMetaCancel" runat="server" Text="Reset" Enabled="false" CssClass="btn btn-default
                                            text-center"
                                            OnClick="btnMetaCancel_Click" />
                                    </div>
                                </div>
                            </div>
                            <div id="divOptrList" runat="server">
                                <div class="table-responsive">
                                    <asp:GridView runat="server" ID="gvwGlobalOptr" DataKeyNames="clntID" CssClass="mydatagrid" AllowPaging="True"
                                        AllowSorting="True"
                                        AutoGenerateColumns="False" EmptyDataText="No Operator Company found."
                                        OnPageIndexChanging="gvwGlobalOptr_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                        PageSize="15"
                                        HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow"
                                        OnSelectedIndexChanged="gvwGlobalOptr_SelectedIndexChanged">
                                        <Columns>
                                            <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                            </asp:ButtonField>
                                            <asp:BoundField DataField="clntID" Visible="False" />
                                            <asp:BoundField DataField="clntName" HeaderText="Operator Company" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div id="divPad" runat="server" visible="false">
                                <div class="table-responsive">
                                    <asp:GridView ID="gvwWellPads" runat="server" EmptyDataText="No Well Pad(s) found for selected Operator"
                                        ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="wpdID"
                                        AllowPaging="True" CssClass="mydatagrid" OnPageIndexChanging="gvwWellPads_PageIndexChanging"
                                        OnSelectedIndexChanged="gvwWellPads_SelectedIndexChanged"
                                        AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header"
                                        PagerStyle-CssClass="pager"
                                        RowStyle-CssClass="rows">
                                        <Columns>
                                            <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                            </asp:ButtonField>
                                            <asp:BoundField DataField="wpdID" HeaderText="wpd" ReadOnly="True" Visible="false" />
                                            <asp:BoundField DataField="welPadName" HeaderText="Well Pad" />
                                            <asp:BoundField DataField="field" HeaderText="O&G Field" />
                                            <asp:BoundField DataField="county" HeaderText="County/District/Division/Municipality" />
                                            <asp:BoundField DataField="state" HeaderText="State/Province" />
                                            <asp:BoundField DataField="country" HeaderText="Country/Nation" />
                                            <asp:BoundField DataField="sreg" HeaderText="Sub-Region" />
                                            <asp:BoundField DataField="reg" HeaderText="Major Region" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div id="divWellList" runat="server" visible="false">
                                <div class="table-responsive">
                                    <asp:GridView ID="gvwWellList" runat="server" CssClass="mydatagrid" EmptyDataText="No Wells associated
                                        with the selected Service Order #"
                                        AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="welID" OnSelectedIndexChanged="gvwWellList_SelectedIndexChanged"
                                        ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwWellList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                        PageSize="15" PagerSettings-Mode="Numeric" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Center" PagerStyle-VerticalAlign="Middle"
                                        HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow" OnRowDataBound="gvwWellList_RowDataBound">
                                        <Columns>
                                            <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                            </asp:ButtonField>
                                            <asp:BoundField DataField="welID" Visible="false" />
                                            <asp:BoundField DataField="welName" HeaderText="Well / Lateral" />
                                            <asp:BoundField DataField="field" HeaderText="Oil & Gas Field" />
                                            <asp:BoundField DataField="county" HeaderText="County / District" />
                                            <asp:BoundField DataField="state" HeaderText="State / Province" />
                                            <asp:BoundField DataField="spud" HeaderText="Spud Date" />
                                            <asp:BoundField DataField="wstID" HeaderText="Status" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div id="divSectionList" runat="server" visible="false">
                                <div class="table-responsive">
                                    <asp:GridView ID="gvwSectionList" runat="server" CssClass="mydatagrid" EmptyDataText="No Well Section associated with the selected Well"
                                        AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="wswID" OnSelectedIndexChanged="gvwSectionList_SelectedIndexChanged"
                                        ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwSectionList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                        PageSize="15" PagerSettings-Mode="Numeric" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Center" PagerStyle-VerticalAlign="Middle"
                                        HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                        <Columns>
                                            <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                            </asp:ButtonField>
                                            <asp:BoundField DataField="wswID" Visible="false" />
                                            <asp:BoundField DataField="wsName" HeaderText="Well Section Name" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="rnCount" HeaderText="Run Count" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div id="divRunList" runat="server" visible="false">
                                <div class="table-responsive">
                                    <asp:GridView ID="gvwRunList" runat="server" CssClass="mydatagrid" EmptyDataText="No Run associated
                                        with the selected Section"
                                        AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="runID" OnSelectedIndexChanged="gvwRunList_SelectedIndexChanged"
                                        ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwRunList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                        PageSize="15" PagerSettings-Mode="Numeric" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Center" PagerStyle-VerticalAlign="Middle"
                                        HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                        <Columns>
                                            <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                            </asp:ButtonField>
                                            <asp:BoundField DataField="runID" Visible="false" />
                                            <asp:BoundField DataField="runName" HeaderText="Run Name" />
                                            <asp:BoundField DataField="Inc" HeaderText="Inclination°" ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="Azm" HeaderText="Azimuth°" ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="TInc" HeaderText="Proposed End-Run Inclination°" ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="TAzm" HeaderText="Proposed End-Run Azimuth°" ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="stat" HeaderText="Status" />
                                            <asp:BoundField DataField="tcID" HeaderText="Toolcode" />
                                            <asp:BoundField DataField="sqcID" HeaderText="Survey Qualification Criteria" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div id="divDataEntryList" runat="server" visible="false">
                                <div class="table-responsive">
                                    <asp:GridView ID="gvwDataEntryList" runat="server" CssClass="mydatagrid" AllowPaging="True"
                                        AutoGenerateColumns="False" DataKeyNames="demID" OnSelectedIndexChanged="gvwDataEntryList_SelectedIndexChanged" ShowHeaderWhenEmpty="True"
                                        AlternatingRowStyle-CssClass="AlternatingRowStyle" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                        <Columns>
                                            <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                            </asp:ButtonField>
                                            <asp:BoundField DataField="demID" Visible="false" />
                                            <asp:BoundField DataField="demValue" HeaderText="Data Entry Method" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div id="divManData" runat="server" visible="false">
                                <div class="panel panel-danger panel-body">
                                    <div class="table-responsive">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-light-blue">Input Survey</span>
                                                <asp:TextBox ID="txtQCManualProcess" runat="server" CssClass="form-control" placeholder="Format: Timestamp, Depth (m)*, Gx (g)*, Gy (g)*, Gz (g)*, Bx (nT)*, By (nT)*, Bz (nT)* ( * required )" />
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div runat="server" id="txtDataEnclosure" visible="false">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                <h4><i runat="server" id="iTXTDMessage"><span runat="server" id="txtDHeader"></span></i></h4>
                                                <asp:Label runat="server" ID="lblManual" Text="" />
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="divManualDataButtons" class="input-group" style="width: 100%;">
                                                <asp:Button ID="btnQCManualUpload" runat="server" Text="Process Survey" CssClass="btn btn-info text-center" OnClick="btnQCManualUpload_Click" />
                                                <asp:Button ID="btnCancelManualUpload" runat="server" Text="Cancel" CssClass="btn btn-warning text-center" OnClick="btnCancelManualUpload_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divFileData" runat="server" visible="false">
                                <div class="panel panel-danger panel-body">
                                    <div id="divFileUploadControlRow" class="table-responsive">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group">
                                                <span id="spnFile" runat="server" class="input-group-addon bg-light-blue">Select Survey File</span>
                                                <asp:FileUpload ID="FileUploadControl" runat="server" Font-Size="Large" />
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div runat="server" id="filDEnclosure" visible="false">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                <h4><i runat="server" id="ifilDMessage"><span runat="server" id="spnFILED"></span></i></h4>
                                                <asp:Label ID="StatusLabel" runat="server" Text="" />
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group" style="width: 100%;">
                                                <asp:Button ID="btn_FileUpload" runat="server" Text="Upload Survey File" CssClass="btn btn-info text-center" OnClick="btn_FileUpload_Click" />
                                                <asp:Button ID="btnCancelFileUpload" runat="server" Text="Cancel" CssClass="btn btn-warning text-center" OnClick="btnCancelFileUpload_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divUploadedData" runat="server" visible="false">
                                <div class="table-responsive">
                                    <div class="panel panel-danger panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div runat="server" id="rawDEnclosure" visible="false">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                <h4><i runat="server" id="iRDE"><span runat="server" id="spnRDE"></span></i></h4>
                                                <asp:Label ID="FileUploadStatusLabel" runat="server" Text="" />
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="panel panel-info panel-body">
                                                <asp:GridView ID="gvwViewManualDisplay" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="drID"
                                                    SelectedRowStyle-CssClass="selectedrow" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                    EmptyDataText="No Data Uploaded" AllowPaging="True" AllowSorting="True" PagerStyle-CssClass="pager" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                    OnPageIndexChanging="gvwViewManualDisplay_PageIndexChanging">
                                                    <Columns>
                                                        <asp:BoundField DataField="drID" Visible="False" />
                                                        <asp:BoundField DataField="Timestamp" HeaderText="Timestamp" />
                                                        <asp:BoundField DataField="Depth" HeaderText="Depth" />
                                                        <asp:BoundField DataField="Gx" HeaderText="Gx" />
                                                        <asp:BoundField DataField="Gy" HeaderText="Gy" />
                                                        <asp:BoundField DataField="Gz" HeaderText="Gz" />
                                                        <asp:BoundField DataField="Bx" HeaderText="Bx" />
                                                        <asp:BoundField DataField="By" HeaderText="By" />
                                                        <asp:BoundField DataField="Bz" HeaderText="Bz" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="divMSARefMags" runat="server" class="panel panel-info panel-body" visible="false">
                                                <asp:Table ID="tblRefMag" runat="server" CssClass="table-responsive" GridLines="Both" Width="100%" CellPadding="10">
                                                    <asp:TableHeaderRow ID="TableHeaderRow1" runat="server" BackColor="Info" ForeColor="Black" Font-Size="Large" HorizontalAlign="Right">
                                                        <asp:TableCell ID="TableCell1" runat="server"></asp:TableCell>
                                                        <asp:TableCell ID="TableCell7" runat="server">
                                                            <asp:Label ID="lblSD" runat="server" Text="Start Depth" />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell8" runat="server">
                                                            <asp:Label ID="lblED" runat="server" Text="End Depth" />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell2" runat="server">
                                                            <asp:Label ID="lblDip" runat="server" Text="IFR Dip" />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell3" runat="server">
                                                            <asp:Label ID="lblDec" runat="server" Text="IFR MDec." />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell4" runat="server">
                                                            <asp:Label ID="lblDecO" runat="server" Text="MDec. Orientation" />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell5" runat="server">
                                                            <asp:Label ID="lblBT" runat="server" Text="IFR B-Total" />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell6" runat="server">
                                                            <asp:Label ID="lblGT" runat="server" Text="IFR G-Total" />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell9" runat="server"></asp:TableCell>
                                                    </asp:TableHeaderRow>
                                                    <asp:TableRow ID="TableRow1" runat="server" HorizontalAlign="Right">
                                                        <asp:TableCell ID="TableCell10" runat="server">
                                                            <asp:RadioButton ID="rdbWell" runat="server" Checked="true" CssClass="flat-red" OnCheckedChanged="rdbWell_CheckedChanged"
                                                                AutoPostBack="True" />
                                                            <asp:Label ID="lblWellIFRTitle" runat="server" Font-Size="Large" class="product-title" Text="Well IFR Ref. Mags." />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell16" runat="server">
                                                            <asp:TextBox ID="txtStartDepth" runat="server" CssClass="form-control text-right" Text="" placeholder="---" />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell17" runat="server">
                                                            <asp:TextBox ID="txtEndDepth" runat="server" CssClass="form-control text-right" Text="" placeholder="---" />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell11" runat="server">
                                                            <asp:TextBox ID="txtWellDip" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell12" runat="server">
                                                            <asp:TextBox ID="txtWellDec" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell13" runat="server">
                                                            <asp:DropDownList ID="ddlWellDecOrient" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                Enabled="false">
                                                            </asp:DropDownList>
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell14" runat="server">
                                                            <asp:TextBox ID="txtWellBTotal" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell15" runat="server">
                                                            <asp:TextBox ID="txtWellGTotal" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell18" runat="server"></asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow ID="TableRow2" runat="server" HorizontalAlign="Right">
                                                        <asp:TableCell ID="TableCell19" runat="server">
                                                            <asp:RadioButton ID="rdbNOAA" runat="server" Checked="false" CssClass="flat-red" OnCheckedChanged="rdbNOAA_CheckedChanged"
                                                                AutoPostBack="True" />
                                                            <asp:Label ID="lblNOAAIFRTitle" runat="server" Font-Size="Large" class="product-title" Text="NOAA Mag. Field" />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell25" runat="server">
                                                            <asp:TextBox ID="txtNOAASDepth" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell26" runat="server">
                                                            <asp:TextBox ID="txtNOAAEDepth" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell20" runat="server">
                                                            <asp:TextBox ID="txtNOAADip" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell21" runat="server">
                                                            <asp:TextBox ID="txtNOAADec" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell22" runat="server">
                                                            <asp:DropDownList ID="ddlNOAAOrientation" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                Enabled="false">
                                                                <asp:ListItem Value="0" Text="--- Select Orientation ---" />
                                                            </asp:DropDownList>
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell23" runat="server">
                                                            <asp:TextBox ID="txtNOAABTotal" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell24" runat="server">
                                                            <asp:TextBox ID="txtNOAAGTotal" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell27" runat="server">
                                                            <a id="ancNOAA" runat="server" href="https://www.ngdc.noaa.gov/geomag-web/?model=wmm#igrfwmm" target="_blank" style="font-size: large;
                                                                text-decoration: underline">NOAA Calc.</a>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow ID="TableRow3" runat="server" HorizontalAlign="Right">
                                                        <asp:TableCell ID="TableCell28" runat="server">
                                                            <asp:RadioButton ID="rdbBGS" runat="server" Checked="false" CssClass="flat-red" OnCheckedChanged="rdbBGS_CheckedChanged"
                                                                AutoPostBack="True" />
                                                            <asp:Label ID="lblBGSIFRTitle" runat="server" Font-Size="Large" class="product-title" Text="World Mag Model" />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell34" runat="server">
                                                            <asp:TextBox ID="txtBGSSDepth" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell35" runat="server">
                                                            <asp:TextBox ID="txtBGSEDepth" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell29" runat="server">
                                                            <asp:TextBox ID="txtBGSDip" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell30" runat="server">
                                                            <asp:TextBox ID="txtBGSDec" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell31" runat="server">
                                                            <asp:DropDownList ID="ddlBGSOrientation" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                Enabled="false">
                                                                <asp:ListItem Value="0" Text="--- Select Orientation ---" />
                                                            </asp:DropDownList>
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell32" runat="server">
                                                            <asp:TextBox ID="txtBGSBTotal" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell33" runat="server">
                                                            <asp:TextBox ID="txtBGSGTotal" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                        </asp:TableCell>
                                                        <asp:TableCell ID="TableCell36" runat="server">
                                                            <a id="ancBGS" runat="server" href="http://www.geomag.bgs.ac.uk/data_service/models_compass/wmm_calc.html" target="_blank"
                                                                style="font-size: large; text-decoration: underline">BGS Calculator</a>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div id="divUnits" runat="server" class="panel panel-info panel-body" visible="false">                                            
                                                <div id="divLenU" runat="server" class="form-group">
                                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="text-align: right; align-content: center; vertical-align: middle;">
                                                        <asp:Label ID="Label1" runat="server" CssClass="control-label label-default" Font-Bold="true" Font-Size="Larger" Text="Depth Units" />
                                                    </div>
                                                    <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                                        <div id="divRDLenU" runat="server">
                                                            <asp:RadioButton ID="rdbFT" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Feet" OnCheckedChanged="rdbFT_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbMT" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Meter" OnCheckedChanged="rdbMT_CheckedChanged" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divAclU" runat="server" class="form-group">
                                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="text-align: right; align-content: center; vertical-align: middle;">
                                                        <asp:Label ID="lblAclU" runat="server" CssClass="control-label label-default" Font-Bold="true" Font-Size="Larger" Text="Accelerometer Units" />
                                                    </div>
                                                    <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                                        <div id="divRDAclU" runat="server">
                                                            <asp:RadioButton ID="rdbCMS" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Accl. due to Gravity (cm/sec. sq)"
                                                                OnCheckedChanged="rdbCMS_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbFSC" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Accl. due to Gravity (ft/sec. sq)"
                                                                OnCheckedChanged="rdbFSC_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbG" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Accl. due to Gravity (g)"
                                                                OnCheckedChanged="rdbG_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbMSC" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Accl. due to Gravity (m/sec. sq)"
                                                                OnCheckedChanged="rdbMSC_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbMG" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Accl. due to Gravity (mg)"
                                                                OnCheckedChanged="rdbMG_CheckedChanged" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divMagU" runat="server" class="form-group">
                                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="text-align: right; align-content: center; vertical-align: middle;">
                                                        <asp:Label ID="lblMagU" runat="server" CssClass="control-label label-default" Font-Bold="true" Font-Size="Larger" Text="Magnetometer Units" />
                                                    </div>
                                                    <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                                        <div id="divRDMagU" runat="server">
                                                            <asp:RadioButton ID="rdbGss" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Gauss (G)"
                                                                OnCheckedChanged="rdbGss_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbT" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Tesla (T)" OnCheckedChanged="rdbT_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbMLIT" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Milli Tesla (mT)"
                                                                OnCheckedChanged="rdbMLIT_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbMCT" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="MicroTesla (µT)"
                                                                OnCheckedChanged="rdbMCT_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbNT" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="NanoTesla (nT)"
                                                                OnCheckedChanged="rdbNT_CheckedChanged" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divSensors" runat="server" class="form-group">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="text-align: right; align-content: center; vertical-align: middle;">
                                                            <asp:Label ID="lbl_Flip" runat="server" CssClass="control-label label-default" Font-Bold="true" Font-Size="Larger" Text="Select Sensor(s) for data transformation" />
                                                        </div>
                                                        <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                                            <div id="divChkFlips" runat="server" class="checkbox" style="">
                                                                <asp:CheckBox ID="chk_NoSignTransformation" runat="server" Text="None" CssClass="checkbox-inline" CausesValidation="True"
                                                                    Checked="true" AutoPostBack="True" OnCheckedChanged="chk_NoSignTransformation_CheckedChanged" />
                                                                <asp:CheckBox ID="chk_FlipGx" runat="server" Text="Gx" CssClass="checkbox-inline" Checked="false" Enabled="False" />
                                                                <asp:CheckBox ID="chk_FlipGy" runat="server" Text="Gy" CssClass="checkbox-inline" Checked="false" Enabled="False" />
                                                                <asp:CheckBox ID="chk_FlipGz" runat="server" Text="Gz" CssClass="checkbox-inline" Checked="false" Enabled="False" />
                                                                <asp:CheckBox ID="chk_FlipBx" runat="server" Text="Bx" CssClass="checkbox-inline" Checked="false" Enabled="False" />
                                                                <asp:CheckBox ID="chk_FlipBy" runat="server" Text="By" CssClass="checkbox-inline" Checked="false" Enabled="False" />
                                                                <asp:CheckBox ID="chk_FlipBz" runat="server" Text="Bz" CssClass="checkbox-inline" Checked="false" Enabled="False" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                        
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group" style="width: 100%;">
                                                <asp:Button ID="btnProcessRawData" runat="server" Text="Process Survey(s)" Enabled="false" CssClass="btn btn-default text-center"
                                                    OnClick="btnProcessRawData_Click" />
                                                <asp:Button ID="btnProcessSrvyCancel" runat="server" Text="Cancel Processing" CssClass="btn btn-warning text-center"
                                                     OnClick="btnProcessSrvyCancel_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divReadyData" runat="server" visible="false">
                                <div class="table-responsive">
                                    <div class="panel panel-danger panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div runat="server" id="Div3" visible="false">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                <h4><i runat="server" id="i1"><span runat="server" id="Span1"></span></i></h4>
                                                <asp:Label ID="Label2" runat="server" Text="" />
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <asp:GridView ID="gvwReadyData" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="drID"
                                                        SelectedRowStyle-CssClass="selectedrow" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                        EmptyDataText="No Data Uploaded" AllowPaging="True" AllowSorting="True" PagerStyle-CssClass="pager" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                        OnPageIndexChanging="gvwReadyData_PageIndexChanging">
                                                        <Columns>
                                                            <asp:BoundField DataField="drID" Visible="False" />
                                                            <asp:BoundField DataField="Timestamp" HeaderText="Timestamp" />
                                                            <asp:BoundField DataField="Depth" HeaderText="Depth (m)" />
                                                            <asp:BoundField DataField="Gx" HeaderText="Gx (g)" />
                                                            <asp:BoundField DataField="Gy" HeaderText="Gy (g)" />
                                                            <asp:BoundField DataField="Gz" HeaderText="Gz (g)" />
                                                            <asp:BoundField DataField="Bx" HeaderText="Bx (nT)" />
                                                            <asp:BoundField DataField="By" HeaderText="By (nT)" />
                                                            <asp:BoundField DataField="Bz" HeaderText="Bz (nT)" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnProcessReadyData" runat="server" CssClass="btn btn-info text-center" Text="Process Data" OnClick="btnProcessReadyData_Click" />
                                                    <asp:Button ID="btnCancelReadyData" runat="server" CssClass="btn btn-warning text-center" Text="Clear Data" OnClick="btnCancelReadyData_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="div12Surveys" runat="server" visible="false">
                                <div class="table-responsive">
                                    <div class="panel panel-danger panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div runat="server" id="div12Enclosure" visible="false">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                <h4><i runat="server" id="i12"><span runat="server" id="spn12"></span></i></h4>
                                                <asp:Label ID="lbl12" runat="server" Text="" />
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group" style="width: 100%;">
                                                <asp:Button ID="btnProceed12" runat="server" CssClass="btn btn-info text-center" Text="Analyze Data" OnClick="btnProceed12_Click" />
                                                <asp:Button ID="btnProceed12Clear" runat="server" CssClass="btn btn-warning text-center" Text="Clear Values"
                                                    OnClick="btnProceed12Clear_Click" CausesValidation="false" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divAvgFilter" runat="server" visible="false">
                                <div class="table-responsive">
                                    <div class="panel panel-danger panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:Label ID="lblAverage" runat="server" CssClass="label label-info" Text="Averages Filter for B-Total, G-Total, Dip" />
                                            <asp:GridView ID="gvwAvgFilter" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="srvID"
                                                SelectedRowStyle-CssClass="selectedrow" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                EmptyDataText="No Data Uploaded" AllowPaging="True" AllowSorting="True" PageSize="500" PagerStyle-CssClass="pager" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                OnRowDataBound="gvwAvgFilter_RowDataBound" OnPageIndexChanging="gvwAvgFilter_PageIndexChanging">
                                                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                                <Columns>
                                                    <asp:BoundField DataField="srvID" HeaderText="Survey No" />
                                                    <asp:BoundField DataField="avgDepth" HeaderText="MD" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="avgGx" HeaderText="Gx (g)" DataFormatString="{0: #0.00000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="avgGy" HeaderText="Gy (g)" DataFormatString="{0: #0.00000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="avgGz" HeaderText="Gz (g)" DataFormatString="{0: #0.00000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="avgGTotal" HeaderText="G-Total" DataFormatString="{0: #0.00000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="avgGTAvg" HeaderText="GT Avg." DataFormatString="{0: #0.00000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="avgGTDelta" HeaderText="Delta-GT" DataFormatString="{0: #0.00000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="avgBx" HeaderText="Bx (nT)" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="avgBy" HeaderText="By (nT)" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="avgBz" HeaderText="Bz (nT)" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="avgBp" HeaderText="Bp_New" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="avgBpDelta" HeaderText="Bp-Bz" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="avgBTotal" HeaderText="B-Total (nT)" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="avgBTAvg" HeaderText="BT Avg." DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="avgBTDelta" HeaderText="Delta BT" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="avgDip" HeaderText="Dip°" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="avgDipAvg" HeaderText="Dip Avg" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="avgDipDelta" HeaderText="Delta Dip" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>                                                                                                                                                            
                                                    <asp:TemplateField HeaderText="Selected" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="srvyCheck" runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="header" />
                                                <PagerStyle CssClass="pager" />
                                                <RowStyle CssClass="rows" />
                                                <SelectedRowStyle CssClass="selectedrow" />
                                            </asp:GridView>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnAverages" runat="server" CssClass="btn btn-info text-center" Text="Proceed Next" OnClick="btnAverages_Click" />
                                                    <asp:Button ID="btnAveragesClear" runat="server" CssClass="btn btn-warning text-center" Text="Reset"
                                                        OnClick="btnAveragesClear_Click" CausesValidation="false" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divAvg12Surveys" runat="server" visible="false">
                                <div class="table-responsive">
                                    <div class="panel panel-danger panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div runat="server" id="encAvg12" visible="false">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                <h4><i runat="server" id="iAvg12"><span runat="server" id="spnAvg12"></span></i></h4>
                                                <asp:Label ID="lblAvg12" runat="server" Text="" />
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group" style="width: 100%;">
                                                <asp:Button ID="btnProceedAvg12" runat="server" CssClass="btn btn-info text-center" Enabled="false" Text="Analyze Data" OnClick="btnProceedAvg12_Click" />
                                                <asp:Button ID="btnProceedAvg12Clear" runat="server" CssClass="btn btn-warning text-center" Text="Clear Values"
                                                    OnClick="btnProceedAvg12Clear_Click" CausesValidation="false" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divRevVectors" runat="server" visible="false">
                                <div class="table-responsive">
                                    <div class="panel panel-danger panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:Label ID="lblRV" runat="server" CssClass="label label-info" Text="Reverse Vector Comparison" />
                                            <asp:GridView ID="gvwRevVectors" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="sID"
                                                SelectedRowStyle-CssClass="selectedrow" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                EmptyDataText="No Data Uploaded" AllowPaging="True" AllowSorting="True" PageSize="500" PagerStyle-CssClass="pager" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                OnRowDataBound="gvwRevVectors_RowDataBound" OnPageIndexChanging="gvwRevVectors_PageIndexChanging">
                                                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                                <Columns>
                                                    <asp:BoundField DataField="sID" HeaderText="Survey No" />
                                                    <asp:BoundField DataField="Depth" HeaderText="MD" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Gx" HeaderText="Gx (g)" DataFormatString="{0: #0.00000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="calcGx" HeaderText="Rev. Gx (g)" DataFormatString="{0: #0.00000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="deltaGx" HeaderText="Delta-Gx (g)" DataFormatString="{0: #0.00000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Gy" HeaderText="Gy (g)" DataFormatString="{0: #0.00000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="calcGy" HeaderText="Rev. Gy (g)" DataFormatString="{0: #0.00000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="deltaGy" HeaderText="Delta-Gy (g)" DataFormatString="{0: #0.00000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Gz" HeaderText="Gz (g)" DataFormatString="{0: #0.00000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="calcGz" HeaderText="Rev. Gz (g)" DataFormatString="{0: #0.00000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="deltaGz" HeaderText="Delta-Gz (g)" DataFormatString="{0: #0.00000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Bx" HeaderText="Bx (nT)" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="calcBx" HeaderText="Rev. Bx (nT)" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="deltaBx" HeaderText="Delta-Bx (nT)" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="By" HeaderText="By (nT)" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="calcBy" HeaderText="Rev. By (nT)" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="deltaBy" HeaderText="Delta-By (nT)" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Bz" HeaderText="Bz (nT)" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="calcBz" HeaderText="Rev. Bz (nT)" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="deltaBz" HeaderText="Delta-Bz (nT)" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="Selected" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="srvyCheck" runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="header" />
                                                <PagerStyle CssClass="pager" />
                                                <RowStyle CssClass="rows" />
                                                <SelectedRowStyle CssClass="selectedrow" />
                                            </asp:GridView>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnRVProceed" runat="server" CssClass="btn btn-info text-center" Text="Compute BHA Filter" OnClick="btnRVProceed_Click" />
                                                    <asp:Button ID="btnRVClear" runat="server" CssClass="btn btn-warning text-center" Text="Clear Values"
                                                        OnClick="btnRVClear_Click" CausesValidation="false" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divExternalInterference" runat="server" visible="false">
                                <div class="table-responsive">
                                    <div class="panel panel-danger panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:GridView ID="gvwBzFilter" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="srvID"
                                                SelectedRowStyle-CssClass="selectedrow" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                EmptyDataText="No Data Uploaded" AllowPaging="True" AllowSorting="True" PageSize="500" PagerStyle-CssClass="pager" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                OnRowDataBound="gvwBzFilter_RowDataBound" OnPageIndexChanging="gvwBzFilter_PageIndexChanging">
                                                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                                <Columns>
                                                    <asp:BoundField DataField="srvID" HeaderText="Survey No" />
                                                    <asp:BoundField DataField="Depth" HeaderText="MD" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Gx" HeaderText="Gx (g)" DataFormatString="{0: #0.00000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Gy" HeaderText="Gy (g)" DataFormatString="{0: #0.00000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Gz" HeaderText="Gz (g)" DataFormatString="{0: #0.00000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Bx" HeaderText="Bx (nT)" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="By" HeaderText="By (nT)" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Bz" HeaderText="Bz (nT)" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Boxy" HeaderText="Boxy (nT)" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="dBoxy" HeaderText="Delta Boxy (nT)" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="BHA" HeaderText="BHA Interference" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="dBHABoxy" HeaderText="Delta Boxy-BHA Interference" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>                                                    
                                                    <asp:TemplateField HeaderText="Selected" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="srvyCheck" runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="header" />
                                                <PagerStyle CssClass="pager" />
                                                <RowStyle CssClass="rows" />
                                                <SelectedRowStyle CssClass="selectedrow" />
                                            </asp:GridView>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnNGZProceed" runat="server" CssClass="btn btn-success text-center" Text="Compute No-Go-Zone" OnClick="btnNGZProceed_Click" />
                                                    <asp:Button ID="btnCancelBz" runat="server" CssClass="btn btn-warning text-center" Text="Clear Values"
                                                        OnClick="btnCancelBz_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divNGZ" runat="server" visible="false">
                                <div class="table-responsive">
                                    <div class="panel panel-danger panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:GridView ID="gvwNGZ" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="srvID"
                                                SelectedRowStyle-CssClass="selectedrow" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                EmptyDataText="No Data Uploaded" AllowPaging="True" AllowSorting="True" PageSize="500" PagerStyle-CssClass="pager" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                OnRowDataBound="gvwNGZ_RowDataBound" OnPageIndexChanging="gvwNGZ_PageIndexChanging">
                                                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                                <Columns>
                                                    <asp:BoundField DataField="srvID" HeaderText="Survey No" />
                                                    <asp:BoundField DataField="Depth" HeaderText="MD" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Gx" HeaderText="Gx (g)" DataFormatString="{0: #0.00000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Gy" HeaderText="Gy (g)" DataFormatString="{0: #0.00000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Gz" HeaderText="Gz (g)" DataFormatString="{0: #0.00000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Bx" HeaderText="Bx (nT)" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="By" HeaderText="By (nT)" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Bz" HeaderText="Bz (nT)" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="ngz" HeaderText="No-Go Zone">
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="Selected" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="srvyCheck" runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="header" />
                                                <PagerStyle CssClass="pager" />
                                                <RowStyle CssClass="rows" />
                                                <SelectedRowStyle CssClass="selectedrow" />
                                            </asp:GridView>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnBadDataroceed" runat="server" CssClass="btn btn-success text-center" Text="Check Bad Data" OnClick="btnBadDataroceed_Click" />
                                                    <asp:Button ID="btnCancelNGZ" runat="server" CssClass="btn btn-warning text-center" Text="Clear Values"
                                                        OnClick="btnCancelNGZ_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                            <div class="input-group" style="width: 100%;">
                                <asp:Button ID="btnDetailCancel" runat="server" Text="Done" CssClass="btn btn-primary text-center" OnClick="btnDetailCancel_Click"
                                    CausesValidation="False" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btn_FileUpload" />
        <asp:PostBackTrigger ControlID="btnDetailCancel" />
    </Triggers>
</asp:UpdatePanel>

