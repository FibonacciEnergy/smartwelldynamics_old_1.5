﻿using log4net;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Color = System.Drawing.Color;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using UNT = SmartsVer1.Helpers.DemogHelpers.UnitsConverter;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using CHRT = SmartsVer1.Helpers.ChartHelpers.Gauges;
using CHRTRes = SmartsVer1.Helpers.ChartHelpers.ChartResources;
using OPR = SmartsVer1.Helpers.QCHelpers.Opr;
using QC = SmartsVer1.QCHelpers.sdRawDataQC;
using WPN = SmartsVer1.Helpers.QCHelpers.WellPlan;
using DL = SmartsVer1.Helpers.DemogHelpers.DataLoader;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;

namespace SmartsVer1.Control.Services
{
    public partial class ctrlRawQC_2A : System.Web.UI.UserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ctrlRawQC_2A));
        const Int32 serviceGroupID = 2;
        const Int32 ClientType = 4;
        String LoginName, username, domain, GetClientDBString;
        Int32 SysClientID = -99, regCount = 0, wellCount = 0, runCount = 0, mrmCount = 0, mbhaCount = 0;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                SysClientID = Convert.ToInt32(usrP.clntID);
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientType);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = 0;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(SysClientID, LoginName);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");
                if (!Page.IsPostBack)
                {
                    createAssetCharts(null, null);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void imgbtnSolar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://www.n3kl.org/sun/noaa.html");
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwGlobalOptr_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientType);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = e.NewPageIndex;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
                this.btnMetaCancel.CssClass = "btn btn-warning text-center";
                this.btnMetaCancel.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwGlobalOptr_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow dRow = this.gvwGlobalOptr.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.ancHead.InnerHtml = "<strong> " + oName + "</strong>";
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                System.Data.DataTable padTable = AST.getClientWellPadTableForOperator(oprID, SysClientID);
                this.gvwWellPads.DataSource = padTable;
                this.gvwWellPads.PageIndex = 0;
                this.gvwWellPads.SelectedIndex = -1;
                this.gvwWellPads.DataBind();
                this.divOpr.Visible = false;
                this.divPad.Visible = true;
                this.divWell.Visible = false;
                this.divSec.Visible = false;
                this.divRun.Visible = false;
                this.divDataEntry.Visible = false;
                this.btnMetaCancel.Enabled = true;
                this.btnMetaCancel.CssClass = "btn btn-warning text-center";
                this.liStep1.Attributes["class"] = "stepper-done";
                this.liStep2.Attributes["class"] = "stepper-todo";
                this.liStep3.Attributes["class"] = "stepper-todo";
                this.liStep4.Attributes["class"] = "stepper-todo";
                this.liStep5.Attributes["class"] = "stepper-todo";
                this.liStep6.Attributes["class"] = "stepper-todo";
                this.liStep7.Attributes["class"] = "stepper-todo";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellPads_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                System.Data.DataTable padTable = AST.getClientWellPadTableForOperator(oprID, SysClientID);
                this.gvwWellPads.DataSource = padTable;
                this.gvwWellPads.PageIndex = e.NewPageIndex;
                this.gvwWellPads.SelectedIndex = -1;
                this.gvwWellPads.DataBind();
                this.btnMetaCancel.CssClass = "btn btn-warning text-center";
                this.btnMetaCancel.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellPads_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow oRow = this.gvwGlobalOptr.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                GridViewRow pRow = this.gvwWellPads.SelectedRow;
                String pName = Convert.ToString(Server.HtmlDecode(pRow.Cells[2].Text));
                this.ancHead.InnerHtml = "<strong> " + oName + " > " + pName + "</strong>";
                Int32 pdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                System.Data.DataTable pdTable = AST.getWellForWellPadTable(pdID, GetClientDBString);
                this.gvwWellList.DataSource = pdTable;
                this.gvwWellList.PageIndex = 0;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();
                this.divOpr.Visible = false;
                this.divPad.Visible = false;
                this.divWell.Visible = true;
                this.divSec.Visible = false;
                this.divRun.Visible = false;
                this.divDataEntry.Visible = false;
                this.btnMetaCancel.Enabled = true;
                this.btnMetaCancel.CssClass = "btn btn-warning text-center";
                this.liStep1.Attributes["class"] = "stepper-done";
                this.liStep2.Attributes["class"] = "stepper-done";
                this.liStep3.Attributes["class"] = "stepper-todo";
                this.liStep4.Attributes["class"] = "stepper-todo";
                this.liStep5.Attributes["class"] = "stepper-todo";
                this.liStep6.Attributes["class"] = "stepper-todo";
                this.liStep7.Attributes["class"] = "stepper-todo";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = ((DataRowView)e.Row.DataItem).Row;
                    Button selBtn = e.Row.Cells[0].Controls[0] as Button;
                    if (dr["wstID"].Equals("In Process"))
                    {
                        selBtn.Enabled = true;
                    }
                    else
                    {
                        selBtn.Enabled = false;
                        selBtn.CssClass = "btn btn-default text-center";
                    }
                    String stC = Convert.ToString(dr["stClr"]);
                    //String cD = Convert.ToString(dr["cdColor"]);
                    e.Row.Cells[7].BackColor = Color.FromName(stC.ToString());
                    //e.Row.Cells[10].BackColor = Color.FromName(cD);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 welID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                GridViewRow dRow = this.gvwWellList.SelectedRow;
                Decimal welTarget = AST.getClientWellPlannedMaxMDepth(welID, GetClientDBString);
                String welStatus = Convert.ToString(Server.HtmlDecode(dRow.Cells[7].Text));
                Int32 checkPlan = WPN.checkWellPlanForSelectedWell(welID, GetClientDBString);
                
                //Int32 processFlag = AST.getWellToJobProcessFlag(srvID, jobID, welID, GetClientDBString);
                
                if (checkPlan < 1)
                {
                    this.wellEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                    this.iWell.Attributes["class"] = "icon fa fa-warning";
                    this.spnWell.InnerText = " Warning!";
                    this.lblWell.Text = "No Well Plan found for selected Well. Please upload Well Plan before requesting Survey Audit/QC";
                    this.wellEnclosure.Visible = true;
                }
                else if (welStatus.Equals("Complete"))
                {
                    this.wellEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                    this.iWell.Attributes["class"] = "icon fa fa-warning";
                    this.spnWell.InnerText = " Warning!";
                    this.lblWell.Text = "Well/Lateral processing Completed. Cannot Audit/QC Surveys for selected Well/Lateral";
                    this.wellEnclosure.Visible = true;
                }
                else if (welTarget < 0.00M)
                {
                    this.wellEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                    this.iWell.Attributes["class"] = "icon fa fa-warning";
                    this.spnWell.InnerText = " Warning!";
                    this.lblWell.Text = "No Well Plan found for selected Well. Please upload Well Plan before requesting Survey Audit/QC";
                    this.wellEnclosure.Visible = true;
                }
                else
                {
                    System.Data.DataTable wsTable = OPR.getWellSectionToWellTable(welID, GetClientDBString);
                    this.gvwSectionList.DataSource = wsTable;
                    this.gvwSectionList.DataBind();
                    this.gvwSectionList.SelectedIndex = -1;
                    this.divOpr.Visible = false;
                    this.divPad.Visible = false;
                    this.divWell.Visible = false;
                    this.divSec.Visible = true;
                    this.divRun.Visible = false;
                    this.divDataEntry.Visible = false;
                    GridViewRow oRow = this.gvwGlobalOptr.SelectedRow;
                    String oName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                    GridViewRow pRow = this.gvwWellPads.SelectedRow;
                    String pName = Convert.ToString(Server.HtmlDecode(pRow.Cells[2].Text));
                    String wName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                    this.ancHead.InnerHtml = "<strong> " + oName + " > " + pName + " > " + wName + "</strong>";                    
                    this.liStep1.Attributes["class"] = "stepper-done";
                    this.liStep2.Attributes["class"] = "stepper-done";
                    this.liStep3.Attributes["class"] = "stepper-done";                    
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 pdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                System.Data.DataTable pdTable = AST.getWellForWellPadTable(pdID, GetClientDBString);
                this.gvwWellList.DataSource = pdTable;
                this.gvwWellList.PageIndex = e.NewPageIndex;
                this.gvwWellList.DataBind();
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        protected void gvwSectionList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                System.Data.DataTable wsTable = OPR.WellSectionNameToWellTable(wlID, GetClientDBString);
                this.gvwSectionList.DataSource = wsTable;
                this.gvwSectionList.PageIndex = e.NewPageIndex;
                this.gvwSectionList.DataBind();
                this.gvwSectionList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSectionList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 wsID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                System.Data.DataTable runList = OPR.getRunForSelectedWellSectionTable(wlID, wsID, GetClientDBString);
                this.gvwRunList.DataSource = runList;
                this.gvwRunList.DataBind();
                this.gvwRunList.SelectedIndex = -1;
                this.gvwRunList.DataBind();
                this.divOpr.Visible = false;
                this.divPad.Visible = false;
                this.divWell.Visible = false;
                this.divSec.Visible = false;
                this.divRun.Visible = true;
                this.divDataEntry.Visible = false;
                GridViewRow oRow = this.gvwGlobalOptr.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                GridViewRow pRow = this.gvwWellPads.SelectedRow;
                String pName = Convert.ToString(Server.HtmlDecode(pRow.Cells[2].Text));
                GridViewRow wRow = this.gvwWellList.SelectedRow;
                String wName = Convert.ToString(Server.HtmlDecode(wRow.Cells[2].Text));                
                GridViewRow sRow = this.gvwSectionList.SelectedRow;
                String sName = Convert.ToString(Server.HtmlDecode(sRow.Cells[2].Text));
                this.ancHead.InnerHtml = "<strong> " + oName + " > " + pName + " > " + wName + " > " + sName + "</strong>";
                this.liStep1.Attributes["class"] = "stepper-done";
                this.liStep2.Attributes["class"] = "stepper-done";
                this.liStep3.Attributes["class"] = "stepper-done";
                this.liStep4.Attributes["class"] = "stepper-done";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }                

        protected void gvwRunList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 wsID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                System.Data.DataTable runList = OPR.getRunForSelectedWellSectionTable(wlID, wsID, GetClientDBString);
                this.gvwRunList.DataSource = runList;
                this.gvwRunList.PageIndex = e.NewPageIndex;
                this.gvwRunList.DataBind();
                this.gvwRunList.Visible = true;
                this.gvwRunList.SelectedIndex = -1;
                this.gvwRunList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRunList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 findRTI = -99, findBHA = -99, runID = -99, secID = -99, wellID = -99, padID = -99, optrID = -99;
                this.divOpr.Visible = false;
                this.divPad.Visible = false;
                this.divWell.Visible = false;
                this.divSec.Visible = false;                
                optrID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                padID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                wellID= Convert.ToInt32(this.gvwWellList.SelectedValue);
                secID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                runID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                findRTI = AST.findTIForRun(runID, secID, wellID, padID, SysClientID, optrID, GetClientDBString);
                findBHA = AST.findBHAForRun(runID, secID, wellID, padID, SysClientID, optrID, GetClientDBString);
                if (!findRTI.Equals(1))
                {
                    this.runEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                    this.iRun.Attributes["class"] = "icon fa fa-warning";
                    this.spnRun.InnerText = " Warning!";
                    this.lblRun.Text = "Missing Tie-In Survey for selected Run";
                    this.runEnclosure.Visible = true;
                }
                else if (!findBHA.Equals(1))
                {
                    this.runEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                    this.iRun.Attributes["class"] = "icon fa fa-warning";
                    this.spnRun.InnerText = " Warning!";
                    this.lblRun.Text = "Missing BHA-Signature for selected Run";
                    this.runEnclosure.Visible = true;
                }
                else
                {
                    this.divDataEntry.Visible = true;                    
                    this.divRun.Visible = false;
                }
                GridViewRow oRow = this.gvwGlobalOptr.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                GridViewRow pRow = this.gvwWellPads.SelectedRow;
                String pName = Convert.ToString(Server.HtmlDecode(pRow.Cells[2].Text));
                GridViewRow wRow = this.gvwWellList.SelectedRow;
                String wName = Convert.ToString(Server.HtmlDecode(wRow.Cells[2].Text));
                GridViewRow sRow = this.gvwSectionList.SelectedRow;
                String sName = Convert.ToString(Server.HtmlDecode(sRow.Cells[2].Text));                
                GridViewRow dRow = this.gvwRunList.SelectedRow;
                String rName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.ancHead.InnerHtml = "<strong> " + oName + " > " + pName + " > " + wName + " > " + sName + " > " + rName + "</strong>";
                this.liStep1.Attributes["class"] = "stepper-done";
                this.liStep2.Attributes["class"] = "stepper-done";
                this.liStep3.Attributes["class"] = "stepper-done";
                this.liStep4.Attributes["class"] = "stepper-done";
                this.liStep5.Attributes["class"] = "stepper-done";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCancelFileUpload_Click(object sender, EventArgs e)
        {
            try
            {
                this.FileUploadControl.Visible = true;
                this.FileUploadControl.Dispose();
                this.filDEnclosure.Visible = false;
                this.FileUploadStatusLabel.Text = "";
                this.divFileData.Visible = false;
                this.divDataEntry.Visible = true;
                this.ddlDataType.SelectedIndex = -1;
                this.ddlDataType.DataBind();
                this.ddlUploadMethod.SelectedIndex = -1;
                this.ddlUploadMethod.DataBind();
                this.ddlUploadMethod.Enabled = false;
                this.filDEnclosure.Visible = false;
                this.FileUploadStatusLabel.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnMetaCancel_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientType);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = 0;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
                this.divOpr.Visible = true;
                this.divPad.Visible = false;
                this.divWell.Visible = false;
                this.divSec.Visible = false;
                this.divRun.Visible = false;
                this.divDataEntry.Visible = false;
                this.divTransformData.Visible = false;
                this.divUploadedData.Visible = false;
                this.ancHead.InnerHtml = "Operator Company";
                this.liStep1.Attributes["class"] = "stepper-todo";
                this.liStep2.Attributes["class"] = "stepper-todo";
                this.liStep3.Attributes["class"] = "stepper-todo";
                this.liStep4.Attributes["class"] = "stepper-todo";
                this.liStep5.Attributes["class"] = "stepper-todo";
                this.liStep6.Attributes["class"] = "stepper-todo";
                this.liStep7.Attributes["class"] = "stepper-todo";
                this.ddlDataType.SelectedIndex = -1;
                this.ddlDataType.DataBind();
                this.ddlUploadMethod.SelectedIndex = -1;
                this.ddlUploadMethod.DataBind();
                this.ddlUploadMethod.Enabled = false;
                this.rdbCMS.Checked = false;
                this.rdbFSC.Checked = false;
                this.rdbG.Checked = false;
                this.rdbMSC.Checked = false;
                this.rdbMG.Checked = false;
                this.rdbGss.Checked = false;
                this.rdbT.Checked = false;
                this.rdbMLIT.Checked = false;
                this.rdbMCT.Checked = false;
                this.rdbNT.Checked = false;
                this.FileUploadControl.Dispose();
                this.FileUploadControl.FileContent.Flush();
                this.FileUploadControl.FileContent.Dispose();
                this.FileUploadControl.FileContent.Close();
                this.wellEnclosure.Visible = false;
                this.btnMetaCancel.Enabled = false;
                this.btnMetaCancel.CssClass = "btn btn-default text-center";                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwViewRawDisplay_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                List<System.Data.DataTable> dataTables = new List<System.Data.DataTable>();
                dataTables = (List<System.Data.DataTable>)HttpContext.Current.Session["rawDataTables"];
                System.Data.DataTable rawTable = dataTables[1];
                this.gvwViewRawDisplay.DataSource = rawTable;
                this.gvwViewRawDisplay.PageIndex = e.NewPageIndex;
                this.gvwViewRawDisplay.DataBind();
                this.liStep5.Attributes["class"] = "stepper-todo";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwViewResultsDispaly_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                List<System.Data.DataTable> dataTables = new List<System.Data.DataTable>();
                dataTables = (List<System.Data.DataTable>)HttpContext.Current.Session["rawDataTables"];
                System.Data.DataTable resTable = dataTables[1];
                this.gvwViewResultsDispaly.DataSource = resTable;
                this.gvwViewResultsDispaly.PageIndex = e.NewPageIndex;
                this.gvwViewResultsDispaly.DataBind();
                this.liStep5.Attributes["class"] = "stepper-todo";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnQCManualUpload_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable iResult = new System.Data.DataTable(), headTable = new System.Data.DataTable(), bodyTable = new System.Data.DataTable();
                List<System.Data.DataTable> dataTables = new List<System.Data.DataTable>();
                Int32 wD = -99, scD= -99, RunID = -99, ServiceID = -99, dtID = -99, demID = -99;
                String dataRowText = String.Empty;                
                wD = Convert.ToInt32(this.gvwWellList.SelectedValue);
                ServiceID = AST.getWellRegisteredServiceID(wD, GetClientDBString);
                scD = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                RunID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                dtID = Convert.ToInt32(this.ddlDataType.SelectedValue);
                if (dtID.Equals(1))
                {
                    dataRowText = Convert.ToString(this.txtQCManualProcess.Text);
                }
                else
                {
                    dataRowText = Convert.ToString(this.txtQCResultProcess.Text);
                }


                GridViewRow dRow = this.gvwWellList.SelectedRow;
                String wName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                logger.Info("User: " + username + " Raw Survey Uploaded for Well/Lateral: " + wName  );

                if (String.IsNullOrEmpty(dataRowText))
                    {
                        this.txtDataEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                        this.iTXTDMessage.Attributes["class"] = "icon fa fa-warning";
                        this.txtDHeader.InnerText = " Warning!";
                        this.lblManual.Text = " Please enter Survey above before clicking Upload Survey button. ";
                        this.txtDataEnclosure.Visible = true;
                        this.txtQCManualProcess.Text = "";
                    }
                    else if (dtID.Equals(1))   // text box is not empty
                    {
                        iResult = DL.uploadManualRawDataRow(ServiceID, wD, scD, RunID, demID, dtID, dataRowText, LoginName, domain, GetClientDBString);
                        dataTables.Add(headTable);
                        dataTables.Add(iResult);
                        HttpContext.Current.Session["rawUploadedDataTables"] = dataTables;
                        this.divManData.Visible = false;
                        this.gvwViewRawDisplay.DataSource = iResult;
                        this.gvwViewRawDisplay.PageIndex = 0;
                        this.gvwViewRawDisplay.SelectedIndex = -1;
                        this.gvwViewRawDisplay.DataBind();
                        this.gvwViewRawDisplay.Visible = true;
                        this.gvwViewResultsDispaly.Visible = false;
                        this.liStep1.Attributes["class"] = "stepper-done";
                        this.liStep2.Attributes["class"] = "stepper-done";
                        this.liStep3.Attributes["class"] = "stepper-done";
                        this.liStep4.Attributes["class"] = "stepper-done";
                        this.liStep5.Attributes["class"] = "stepper-done";
                        this.liStep6.Attributes["class"] = "stepper-todo";
                        this.liStep7.Attributes["class"] = "stepper-todo";
                        this.lblTotalRows.Text = String.Format("{0} {1}", "Total Uploaded Survey Rows : ", Convert.ToString(iResult.Rows.Count));
                        this.rdbCMS.Checked = false;
                        this.rdbFSC.Checked = false;
                        this.rdbG.Checked = false;
                        this.rdbMSC.Checked = false;
                        this.rdbMG.Checked = false;
                        this.rdbGss.Checked = false;
                        this.rdbT.Checked = false;
                        this.rdbMLIT.Checked = false; 
                        this.rdbMCT.Checked = false;
                        this.rdbNT.Checked = false;
                    }                                
                    else    // Results Data Row
                    {
                        iResult = DL.uploadManualResultsDataRow(ServiceID, wD, scD, RunID, demID, dtID, dataRowText, LoginName, domain, GetClientDBString);
                        dataTables.Add(headTable);
                        dataTables.Add(bodyTable);
                        dataTables.Add(iResult);
                        HttpContext.Current.Session["resUploadedDataTables"] = dataTables;
                        this.divManData.Visible = false;
                        this.gvwViewRawDisplay.Visible = false;
                        this.gvwViewResultsDispaly.DataSource = iResult;
                        this.gvwViewResultsDispaly.PageIndex = 0;
                        this.gvwViewResultsDispaly.SelectedIndex = -1;
                        this.gvwViewResultsDispaly.DataBind();
                        this.gvwViewResultsDispaly.Visible = true;
                        this.liStep1.Attributes["class"] = "stepper-done";
                        this.liStep2.Attributes["class"] = "stepper-done";
                        this.liStep3.Attributes["class"] = "stepper-done";
                        this.liStep4.Attributes["class"] = "stepper-done";
                        this.liStep5.Attributes["class"] = "stepper-done";
                        this.liStep6.Attributes["class"] = "stepper-todo";
                        this.liStep7.Attributes["class"] = "stepper-todo";
                        this.lblTotalRows.Text = String.Format("{0} {1}", "Total Uploaded Survey Rows : ", Convert.ToString(iResult.Rows.Count));
                        this.chk_NoSignTransformation.Checked = true;
                        this.chk_NoSignTransformation.Enabled = false;
                        this.chk_FlipGx.Enabled = false;
                        this.chk_FlipGy.Enabled = false;
                        this.chk_FlipGz.Enabled = false;
                        this.chk_FlipBx.Enabled = false;
                        this.chk_FlipBy.Enabled = false;
                        this.chk_FlipBz.Enabled = false;
                        this.rdbFT.Checked = false;
                        this.rdbFT.Enabled = false;
                        this.rdbMT.Checked = false;
                        this.rdbMT.Enabled = false;
                        this.rdbCMS.Checked = false;
                        this.rdbCMS.Enabled = false;
                        this.rdbFSC.Checked = false;
                        this.rdbFSC.Enabled = false;
                        this.rdbG.Checked = false;
                        this.rdbG.Enabled = false;
                        this.rdbMSC.Checked = false;
                        this.rdbMSC.Enabled = false;
                        this.rdbMG.Checked = false;
                        this.rdbMG.Enabled = false;
                        this.rdbGss.Checked = false;
                        this.rdbGss.Enabled = false;
                        this.rdbT.Checked = false;
                        this.rdbT.Enabled = false;
                        this.rdbMLIT.Checked = false;
                        this.rdbMLIT.Enabled = false;
                        this.rdbMCT.Checked = false;
                        this.rdbMCT.Enabled = false;
                        this.rdbNT.Checked = false;
                        this.rdbNT.Enabled = false;
                        this.btnProcessRawData.Enabled = true;
                        this.btnProcessRawData.CssClass = "btn btn-info text-center";
                        this.btnProcessSrvyCancel.Enabled = true;
                        this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";                        
                }
                this.divUploadedData.Visible = true;
                this.divDataEntry.Visible = false;
                this.divManData.Visible = false;
                this.divFileData.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCancelManualUpload_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtQCManualProcess.Text = "";
                this.txtQCResultProcess.Text = "";
                this.lblManual.Text = "";
                this.txtDataEnclosure.Visible = false;
                this.divManData.Visible = false;
                this.divDataEntry.Visible = true;
                this.ddlDataType.SelectedIndex = -1;
                this.ddlDataType.DataBind();
                this.ddlUploadMethod.SelectedIndex = -1;
                this.ddlUploadMethod.DataBind();
                this.ddlUploadMethod.Enabled = false;
                this.txtDataEnclosure.Visible = false;
                this.lblManual.Text = "";
                this.liStep5.Attributes["class"] = "stepper-todo";                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btn_FileUpload_Click(object sender, EventArgs e)
        {
            try
            {
                List<System.Data.DataTable> iReply = new List<System.Data.DataTable>();
                Int32 wID = -99, secD = -99, rID = -99, demID = -99, ServiceID = -99;
                String fileName, pathToFile, dfName, dfExtension;
                List<String> noDataLines = new List<String>();
                Dictionary<String, String> headKeyValue = new Dictionary<String, String>(), dataLines = new Dictionary<String, String>();
                if (FileUploadControl.HasFile)
                {
                    try
                    {                        
                        wID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                        ServiceID = AST.getWellRegisteredServiceID(wID, GetClientDBString);
                        secD = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                        rID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                        pathToFile = WebConfigurationManager.AppSettings["tempFiles"];
                        fileName = Path.Combine(pathToFile, FileUploadControl.FileName);
                        dfName = Path.GetFileNameWithoutExtension(fileName);
                        dfExtension = Path.GetExtension(fileName);
                        if (dfExtension.Equals(".txt") || dfExtension.Equals(".csv"))
                        {
                            if (dfExtension.Equals(".txt"))
                            { demID = 4; }
                            else { demID = 2; }

                            GridViewRow dRow = this.gvwWellList.SelectedRow;
                            String wName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));                            
                            logger.Info("User: " + username + " QC_Processing: Well/Lateral " + wName + " Uploading File: " + FileUploadControl.FileName + " UploadPath: " + pathToFile);
                            // Save the file to our local path
                            FileUploadControl.SaveAs(fileName);

                            if (File.Exists(fileName))
                            {
                                //iReply = DL.uploadDataTextFile(wID, secD, rID, dfName, dfExtension, fileName, ServiceID, demID, LoginName, domain, GetClientDBString);
                                iReply = DL.uploadStandardDataTextFile(wID, secD, rID, dfName, dfExtension, fileName, ServiceID, demID, LoginName, domain, GetClientDBString);
                                HttpContext.Current.Session["rawUploadedDataTables"] = iReply;
                                HttpContext.Current.Session["rawDataTables"] = iReply;
                                HttpContext.Current.Session["fileName"] = dfName;
                                HttpContext.Current.Session["fileExtension"] = dfExtension;
                                HttpContext.Current.Session["uploadedSurveyFile"] = fileName;
                                System.Data.DataTable dataTable = new System.Data.DataTable();
                                dataTable = iReply[1];
                                this.gvwViewRawDisplay.DataSource = dataTable;
                                this.gvwViewRawDisplay.PageIndex = 0;
                                this.gvwViewRawDisplay.SelectedIndex = -1;
                                this.gvwViewRawDisplay.DataBind();
                                this.gvwViewRawDisplay.Visible = true;
                                this.divUploadedData.Visible = true;
                                this.divFileData.Visible = false;
                                logger.Info("User: " + username + " FileUpload: " + fileName + " Status: Successful!");
                                FileUploadControl.PostedFile.InputStream.Flush();
                                FileUploadControl.PostedFile.InputStream.Close();
                                FileUploadControl.PostedFile.InputStream.Dispose();
                                FileUploadControl.Dispose();
                                FileUploadControl.FileContent.Flush();
                                FileUploadControl.FileContent.Dispose();
                                FileUploadControl.FileContent.Close();                                     
                                this.liStep5.Attributes["class"] = "stepper-todo";
                            }
                            else
                            {
                                this.filDEnclosure.Visible = true;
                                this.filDEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.ifilDMessage.Attributes["class"] = "icon fa fa-warning";
                                this.spnFILED.InnerText = " Error!";
                                this.FileUploadStatusLabel.Text = "Failure!!! Unable to upload Raw Data File";
                                logger.Info("User: " + username + " FileUpload: " + fileName + " Status: Failure");
                                FileUploadControl.PostedFile.InputStream.Flush();
                                FileUploadControl.PostedFile.InputStream.Close();
                                FileUploadControl.PostedFile.InputStream.Dispose();
                                FileUploadControl.Dispose();
                                FileUploadControl.FileContent.Flush();
                                FileUploadControl.FileContent.Dispose();
                                FileUploadControl.FileContent.Close();
                                this.liStep5.Attributes["class"] = "stepper-todo";
                            }
                        }
                        else
                        {
                            this.filDEnclosure.Visible = true;
                            this.filDEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                            this.ifilDMessage.Attributes["class"] = "icon fa fa-danger";
                            this.spnFILED.InnerText = " !!! Error !!!";
                            this.FileUploadStatusLabel.Text = "!!! File Type ERROR !!! (only .txt or .csv files are allowed)";
                            FileUploadControl.PostedFile.InputStream.Flush();
                            FileUploadControl.PostedFile.InputStream.Close();
                            FileUploadControl.PostedFile.InputStream.Dispose();
                            FileUploadControl.Dispose();
                            FileUploadControl.FileContent.Flush();
                            FileUploadControl.FileContent.Dispose();
                            FileUploadControl.FileContent.Close();
                            this.liStep5.Attributes["class"] = "stepper-todo";
                        }
                    }
                    catch (Exception ex)
                    {
                        this.filDEnclosure.Visible = true;
                        this.filDEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                        this.ifilDMessage.Attributes["class"] = "icon fa fa-danger";
                        this.spnFILED.InnerText = " !!! Error !!!";
                        this.FileUploadStatusLabel.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
                        logger.Fatal("User: " + username + " Fatal Error During File Upload: " + FileUploadStatusLabel.Text);
                        FileUploadControl.PostedFile.InputStream.Flush();
                        FileUploadControl.PostedFile.InputStream.Close();
                        FileUploadControl.PostedFile.InputStream.Dispose();
                        FileUploadControl.Dispose();
                        FileUploadControl.FileContent.Flush();
                        FileUploadControl.FileContent.Dispose();
                        FileUploadControl.FileContent.Close();
                        this.liStep5.Attributes["class"] = "stepper-todo";
                    }
                }
                else
                {
                    this.filDEnclosure.Visible = true;
                    this.filDEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                    this.ifilDMessage.Attributes["class"] = "icon fa fa-danger";
                    this.spnFILED.InnerText = " !!! Error !!!";
                    this.FileUploadStatusLabel.Text = "Upload status: Specify the file to be uploaded ";
                    this.btnCancelFileUpload.Visible = true;
                    this.liStep5.Attributes["class"] = "stepper-todo";
                    FileUploadControl.PostedFile.InputStream.Flush();
                    FileUploadControl.PostedFile.InputStream.Close();
                    FileUploadControl.PostedFile.InputStream.Dispose();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProcessRawData_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable iResult = new System.Data.DataTable(), selectedRawTable= new System.Data.DataTable(), selectedResultsTable = new System.Data.DataTable();
                DataColumn chk = selectedRawTable.Columns.Add("chk", typeof(Int32));
                DataColumn drID = selectedRawTable.Columns.Add("drID", typeof(Int32));
                DataColumn Depth = selectedRawTable.Columns.Add("Depth", typeof(Decimal));
                DataColumn rGx = selectedRawTable.Columns.Add("rGx", typeof(Decimal));
                DataColumn rGy = selectedRawTable.Columns.Add("rGy", typeof(Decimal));
                DataColumn rGz = selectedRawTable.Columns.Add("rGz", typeof(Decimal));
                DataColumn rBx = selectedRawTable.Columns.Add("rBx", typeof(Decimal));
                DataColumn rBy = selectedRawTable.Columns.Add("rBy", typeof(Decimal));
                DataColumn rBz = selectedRawTable.Columns.Add("rBz", typeof(Decimal));
                DataColumn rtimestamp = selectedRawTable.Columns.Add("rtimestamp", typeof(String));
                DataColumn rsChk = selectedResultsTable.Columns.Add("chk", typeof(Int32));
                DataColumn rsID = selectedResultsTable.Columns.Add("drID", typeof(Int32));
                DataColumn rsDepth = selectedResultsTable.Columns.Add("resDepth", typeof(Decimal));
                DataColumn rsInc = selectedResultsTable.Columns.Add("resInc", typeof(Decimal));
                DataColumn rsAzm = selectedResultsTable.Columns.Add("resAzm", typeof(Decimal));
                DataColumn rsDip = selectedResultsTable.Columns.Add("resDip", typeof(Decimal));
                DataColumn rsBTotal = selectedResultsTable.Columns.Add("resBTotal", typeof(Decimal));
                DataColumn rsGTotal = selectedResultsTable.Columns.Add("resGTotal", typeof(Decimal));
                DataColumn rsGTF = selectedResultsTable.Columns.Add("resGTF", typeof(Decimal));                
                Int32 wD = -99, wS = -99, RunID = -99, chk_FlipGx = -99, chk_FlipGy = -99, chk_FlipGz = -99, chk_FlipBx = -99, chk_FlipBy = -99, chk_FlipBz = -99, selectedService = -99;
                Int32 rawID = -99, resID = -99, rawChecked = -99, resChecked = -99;
                Int32 lenU = -99, magU = -99, aclU = -99, dtID = -99;
                Decimal rawDepth = -99.00M, rawGx = -99.000000000000M, rawGy = -99.000000000000M, rawGz = -99.000000000000M, rawBx = -99.000000000000M, rawBy = -99.000000000000M, rawBz = -99.000000000000M, resDepth = -99.00M, resInc = -99.000000000000M, resAzm = -99.000000000000M, resDip = -99.000000000000M, resBTotal = -99.000000000000M, resGTotal = -99.000000000000M, resGTF = -99.000000000000M;
                String rawDepthVal = String.Empty, rawGxVal = String.Empty, rawGyVal = String.Empty, rawGzVal = String.Empty, rawBxVal = String.Empty, rawByVal = String.Empty, rawBzVal = String.Empty, resDepthVal = String.Empty, resIncVal = String.Empty, resAzmVal = String.Empty, resDipVal = String.Empty, resBTotalVal = String.Empty, resGTotalVal = String.Empty, resGTFVal = String.Empty, rTime = String.Empty;
                Decimal depthResult = -99.00M, gxResult = -99.000000000000M, gyResult = -99.000000000000M, gzResult = -99.000000000000M, bxResult = -99.000000000000M, byResult = -99.000000000000M, bzResult = -99.000000000000M, incResult = -99.000000000000M, azmResult = -99.000000000000M, dipResult = -99.000000000000M, btResult = -99.000000000000M, gtResult = -99.000000000000M, gtfResult = -99.000000000000M;
                wD = Convert.ToInt32(this.gvwWellList.SelectedValue);
                wS = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                RunID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                selectedService = AST.getWellRegisteredServiceID(wD, GetClientDBString);
                if (this.chk_FlipGx.Checked) { chk_FlipGx = -1; } else { chk_FlipGx = 1; }
                if (this.chk_FlipGy.Checked) { chk_FlipGy = -1; } else { chk_FlipGy = 1; }
                if (this.chk_FlipGz.Checked) { chk_FlipGz = -1; } else { chk_FlipGz = 1; }
                if (this.chk_FlipBx.Checked) { chk_FlipBx = -1; } else { chk_FlipBx = 1; }
                if (this.chk_FlipBy.Checked) { chk_FlipBy = -1; } else { chk_FlipBy = 1; }
                if (this.chk_FlipBz.Checked) { chk_FlipBz = -1; } else { chk_FlipBz = 1; }
                if (this.rdbMT.Checked) { lenU = 5; } else { lenU = 4; }
                if (this.rdbCMS.Checked) { aclU = 1; } else if (this.rdbFSC.Checked) { aclU = 2; } else if (this.rdbG.Checked) { aclU = 3; } else if(this.rdbMSC.Checked){ aclU = 4;} else {aclU = 5;}
                if (this.rdbGss.Checked) { magU = 1; } else if (this.rdbT.Checked) { magU = 2; } else if (this.rdbMLIT.Checked) { magU = 3; } else if (this.rdbMCT.Checked) { magU = 4; } else { magU = 5; }
                List<System.Data.DataTable> dataTables = new List<System.Data.DataTable>(), readyTables = new List<System.Data.DataTable>();
                dtID = Convert.ToInt32(this.ddlDataType.SelectedValue);
                if (dtID.Equals(1)) //Raw Table
                {
                    dataTables = (List<System.Data.DataTable>)HttpContext.Current.Session["rawUploadedDataTables"];
                    System.Data.DataTable rawdataTable = new System.Data.DataTable(), fileHead = new System.Data.DataTable(), fileBody = new System.Data.DataTable();
                    fileBody = (System.Data.DataTable)dataTables[0];
                    rawdataTable = (System.Data.DataTable)dataTables[1];
                    foreach (System.Data.DataRow dRow in rawdataTable.Rows)
                    {
                        rawID = Convert.ToInt32(dRow["drID"]);
                        rawDepthVal = Convert.ToString(dRow["depth"]);
                        if (Decimal.TryParse(rawDepthVal, out depthResult))
                        {
                            rawDepth = Convert.ToDecimal(rawDepthVal);
                            if (lenU.Equals(4))
                            {
                                rawDepth = UNT.convFeetToMeters(rawDepth);
                            }
                        }
                        else
                        {
                            rawDepth = -99.00M;
                        }
                        rawGxVal = Convert.ToString(dRow["gx"]);
                        if (Decimal.TryParse(rawGxVal, out gxResult))
                        {
                            rawGx = Convert.ToDecimal(rawGxVal);
                            rawGx = rawGx * Convert.ToDecimal(chk_FlipGx);
                            rawGx = UNT.convAccelerometerToAccelerationDueToGravity(rawGx, aclU);
                        }
                        else
                        {
                            rawGx = -99.00M;
                        }
                        rawGyVal = Convert.ToString(dRow["gy"]);
                        if (Decimal.TryParse(rawGyVal, out gyResult))
                        {
                            rawGy = Convert.ToDecimal(rawGyVal);
                            rawGy = rawGy * Convert.ToDecimal(chk_FlipGy);
                            rawGy = UNT.convAccelerometerToAccelerationDueToGravity(rawGy, aclU);
                        }
                        else
                        {
                            rawGy = -99.00M;
                        }
                        rawGzVal = Convert.ToString(dRow["gz"]);
                        if (Decimal.TryParse(rawGzVal, out gzResult))
                        {
                            rawGz = Convert.ToDecimal(rawGzVal);
                            rawGz = rawGz * Convert.ToDecimal(chk_FlipGz);
                            rawGz = UNT.convAccelerometerToAccelerationDueToGravity(rawGz, aclU);
                        }
                        else
                        {
                            rawGz = -99.00M;
                        }
                        rawBxVal = Convert.ToString(dRow["bx"]);
                        if (Decimal.TryParse(rawBxVal, out bxResult))
                        {
                            rawBx = Convert.ToDecimal(rawBxVal);
                            rawBx = rawBx * Convert.ToDecimal(chk_FlipBx);
                            rawBx = UNT.convMagnetometerToNanoTesla(rawBx, magU);
                        }
                        else
                        {
                            rawBx = -99.00M;
                        }
                        rawByVal = Convert.ToString(dRow["by"]);
                        if (Decimal.TryParse(rawByVal, out byResult))
                        {
                            rawBy = Convert.ToDecimal(rawByVal);
                            rawBy = rawBy * Convert.ToDecimal(chk_FlipBy);
                            rawBy = UNT.convMagnetometerToNanoTesla(rawBy, magU);
                        }
                        else
                        {
                            rawBy = -99.00M;
                        }
                        rawBzVal = Convert.ToString(dRow["bz"]);
                        if (Decimal.TryParse(rawBzVal, out bzResult))
                        {
                            rawBz = Convert.ToDecimal(rawBzVal);
                            rawBz = rawBz * Convert.ToDecimal(chk_FlipBz);
                            rawBz = UNT.convMagnetometerToNanoTesla(rawBz, magU);
                        }
                        else
                        {
                            rawBz = -99.00M;
                        }
                        if ((!rawDepth.Equals(-99.00) && (!rawGx.Equals(-99.00)) && (!rawGy.Equals(-99.00)) && (!rawGz.Equals(-99.00))))
                        {
                            rawChecked = 1;
                        }
                        else
                        {
                            rawChecked = -1;
                        }
                        rTime = Convert.ToString(dRow["Timestamp"]);
                        if (String.IsNullOrEmpty(rTime)) { rTime = "---"; }
                        selectedRawTable.Rows.Add(rawChecked, rawID, rawDepth, rawGx, rawGy, rawGz, rawBx, rawBy, rawBz, rTime);
                        selectedRawTable.AcceptChanges();
                        HttpContext.Current.Session["selectedRawValues"] = selectedRawTable;
                        this.gvwSelectedRaw.DataSource = selectedRawTable;
                        this.gvwSelectedRaw.PageIndex = 0;
                        this.gvwSelectedRaw.SelectedIndex = -1;
                        this.gvwSelectedRaw.DataBind();
                        this.gvwSelectedRaw.Visible = true;
                        this.gvwSelectedResult.DataSource = null;
                        this.gvwSelectedResult.DataBind();
                        this.gvwSelectedResult.Visible = false;
                        this.divUploadedData.Visible = false;
                        this.divTransformData.Visible = true;
                        GridViewRow wellNameRow = this.gvwWellList.SelectedRow;
                        String wName = Convert.ToString(Server.HtmlDecode(wellNameRow.Cells[2].Text)); 
                        logger.Info("User: " + username + " QC_Processing Initiated for Well/Lateral: " + wName);
                    }
                }
                else //Results Table
                {
                    dataTables = (List<System.Data.DataTable>)HttpContext.Current.Session["resUploadedDataTables"];
                    System.Data.DataTable rawdataTable = new System.Data.DataTable(), fileHead = new System.Data.DataTable(), fileBody = new System.Data.DataTable();
                    fileHead = (System.Data.DataTable)dataTables[0];
                    fileBody = (System.Data.DataTable)dataTables[1];
                    rawdataTable = (System.Data.DataTable)dataTables[2];
                    foreach (System.Data.DataRow dRow in rawdataTable.Rows)
                    {
                        resID = Convert.ToInt32(dRow["drID"]);
                        resDepthVal = Convert.ToString(dRow["Depth"]);
                        if (Decimal.TryParse(resDepthVal, out depthResult))
                        {
                            resDepth = Convert.ToDecimal(resDepthVal);
                        }
                        else
                        {
                            resDepth = -99.00M;
                        }
                        resIncVal = Convert.ToString(dRow["ResInc"]);
                        if (Decimal.TryParse(resIncVal, out incResult))
                        {
                            resInc = Convert.ToDecimal(resIncVal);
                        }
                        else
                        {
                            resInc = -99.00M;
                        }
                        resAzmVal = Convert.ToString(dRow["ResAzm"]);
                        if (Decimal.TryParse(resAzmVal, out azmResult))
                        {
                            resAzm = Convert.ToDecimal(resAzmVal);
                        }
                        else
                        {
                            resAzm = -99.00M;
                        }
                        resDipVal = Convert.ToString(dRow["ResDip"]);
                        if (Decimal.TryParse(resDipVal, out dipResult))
                        {
                            resDip = Convert.ToDecimal(resDipVal);
                        }
                        else
                        {
                            resDip = -99.00M;
                        }
                        resBTotalVal = Convert.ToString(dRow["ResBT"]);
                        if (Decimal.TryParse(resBTotalVal, out btResult))
                        {
                            resBTotal = Convert.ToDecimal(resBTotalVal);
                        }
                        else
                        {
                            resBTotal = -99.00M;
                        }
                        resGTotalVal = Convert.ToString(dRow["ResGT"]);
                        if (Decimal.TryParse(resGTotalVal, out gtResult))
                        {
                            resGTotal = Convert.ToDecimal(resGTotalVal);
                        }
                        else
                        {
                            resGTotal = -99.00M;
                        }
                        resGTFVal = Convert.ToString(dRow["ResGTF"]);
                        if (Decimal.TryParse(resGTFVal, out gtfResult))
                        {
                            resGTF = Convert.ToDecimal(resGTFVal);
                        }
                        else
                        {
                            resGTF = -99.00M;
                        }
                        if ((!resDepth.Equals(-99.00M)) && (!resInc.Equals(-99.00M)) && (!resAzm.Equals(-99.00M)) && (!resDip.Equals(-99.00M)) && (!resBTotal.Equals(-99.00M)) && (!resGTotal.Equals(-99.00M)) && (!resGTF.Equals(-99.00M)))
                        {
                            resChecked = 1;
                        }
                        else
                        {
                            resChecked = -1;
                        }
                        selectedResultsTable.Rows.Add(resChecked, resID, resDepth, resInc, resAzm, resDip, resBTotal);
                        selectedResultsTable.AcceptChanges();
                        HttpContext.Current.Session["selectedResultValues"] = selectedResultsTable;
                        this.gvwSelectedRaw.DataSource = null;
                        this.gvwSelectedRaw.DataBind();
                        this.gvwSelectedRaw.Visible = false;
                        this.gvwSelectedResult.DataSource = selectedResultsTable;
                        this.gvwSelectedResult.PageIndex = 0;
                        this.gvwSelectedResult.SelectedIndex = -1;
                        this.gvwSelectedResult.DataBind();
                        this.gvwSelectedResult.Visible = true;
                        this.divUploadedData.Visible = false;
                        this.divTransformData.Visible = true;
                    }
                }
                this.liStep1.Attributes["class"] = "stepper-done";
                this.liStep2.Attributes["class"] = "stepper-done";
                this.liStep3.Attributes["class"] = "stepper-done";
                this.liStep4.Attributes["class"] = "stepper-done";
                this.liStep5.Attributes["class"] = "stepper-done";
                this.liStep6.Attributes["class"] = "stepper-done";
                this.liStep7.Attributes["class"] = "stepper-todo";                        
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProcessSrvyCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.liStep5.Attributes["class"] = "stepper-todo";
                this.gvwViewRawDisplay.DataSource = null;
                this.gvwViewRawDisplay.DataBind();
                this.gvwViewRawDisplay.Visible = false;
                this.gvwViewResultsDispaly.DataSource = null;
                this.gvwViewResultsDispaly.DataBind();
                this.gvwViewResultsDispaly.Visible = false;
                this.chk_NoSignTransformation.Checked = true;
                this.chk_NoSignTransformation.Enabled = true;
                this.chk_FlipBx.Checked = false;
                this.chk_FlipBx.Enabled = false;
                this.chk_FlipBy.Checked = false;
                this.chk_FlipBy.Enabled = false;
                this.chk_FlipBz.Checked = false;
                this.chk_FlipBz.Enabled = false;
                this.chk_FlipGx.Checked = false;
                this.chk_FlipGx.Enabled = false;
                this.chk_FlipGy.Checked = false;
                this.chk_FlipGy.Enabled = false;
                this.chk_FlipGz.Checked = false;
                this.chk_FlipGz.Enabled = false;
                this.rdbMT.Checked = false;
                this.rdbFT.Checked = false;
                this.rdbCMS.Checked = false;
                this.rdbFSC.Checked = false;
                this.rdbG.Checked = false;
                this.rdbMSC.Checked = false;
                this.rdbMT.Checked = false;
                this.rdbT.Checked = false;
                this.rdbGss.Checked = false;
                this.rdbNT.Checked = false;
                this.rdbMLIT.Checked = false;
                this.rdbMCT.Checked = false;
                HttpContext.Current.Session["rawDataTables"] = null;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDetailCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.lblManual.Text = "";
                this.lblManual.Visible = false;
                this.txtQCManualProcess.Text = "";                
                this.FileUploadControl.FileContent.Flush();
                this.FileUploadControl.FileContent.Dispose();
                this.FileUploadControl.FileContent.Close();
                this.FileUploadControl.Dispose();
                this.FileUploadStatusLabel.Visible = false;
                this.FileUploadStatusLabel.Text = null;
                this.chk_NoSignTransformation.Checked = false;
                this.chk_FlipBx.Checked = false;
                this.chk_FlipBy.Checked = false;
                this.chk_FlipBz.Checked = false;
                this.chk_FlipGx.Checked = false;
                this.chk_FlipGy.Checked = false;
                this.chk_FlipGz.Checked = false;
                this.lblQCResMetaData.Visible = false;
                this.lblQCResParameters.Visible = false;
                this.lblRQCQualifiers.Visible = false;
                this.lblRQCBHA.Visible = false;
                this.lblNMSCalc.Visible = false;
                this.lblRQCMinMax.Visible = false;
                this.gvwRefVals.Visible = false;
                this.lblTieInSurvey.Visible = false;
                this.gvwTieInSurvey.Visible = false;                
                this.btnMetaCancel_Click(null, null);
                this.btnMetaCancel.Visible = true;
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientType);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = 0;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
                this.gvwWellPads.DataSource = null;
                this.gvwWellPads.DataBind();
                this.gvwWellList.DataSource = null;
                this.gvwWellList.DataBind();
                this.gvwSectionList.DataSource = null;
                this.gvwSectionList.DataBind();
                this.gvwRunList.DataSource = null;
                this.gvwRunList.DataBind();
                this.ancHead.InnerHtml = "Operator Company";
                this.divOpr.Visible = true;
                this.divPad.Visible = false;
                this.divWell.Visible = false;
                this.divSec.Visible = false;
                this.divRun.Visible = false;
                this.divDataEntry.Visible = false;
                this.divTransformData.Visible = false;
                this.divUploadedData.Visible = false;
                this.divQC.Visible = false;
                this.enWDInfo.Visible = false;
                this.lblWDInfo.Text = "";
                this.enRDInfo.Visible = false;
                this.lblRDInfo.Text = "";
                this.liStep1.Attributes["class"] = "stepper-todo";
                this.liStep2.Attributes["class"] = "stepper-todo";
                this.liStep3.Attributes["class"] = "stepper-todo";
                this.liStep4.Attributes["class"] = "stepper-todo";
                this.liStep5.Attributes["class"] = "stepper-todo";
                this.liStep6.Attributes["class"] = "stepper-todo";
                this.liStep7.Attributes["class"] = "stepper-todo";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnClearResDetail_Click(object sender, EventArgs e)
        {
            try
            {
                this.gvwRQCResults.SelectedIndex = -1;
                this.gvwResSQC.DataBind();
                this.gvwResSQC.Visible = false;
                this.gvwRefVals.DataBind();
                this.gvwRefVals.Visible = false;
                this.gvwQCResParameters.DataBind();
                this.gvwQCResParameters.Visible = false;
                this.gvwRQCQualifiers.DataBind();
                this.gvwRQCQualifiers.Visible = false;
                this.gvwRQCBHA.DataBind();
                this.gvwRQCBHA.Visible = false;
                this.gvwNMSCalc.DataBind();
                this.gvwNMSCalc.Visible = false;
                this.gvwRQCMinMax.DataBind();
                this.gvwRQCMinMax.Visible = false;
                this.gvwPlacement.DataBind();
                this.gvwPlacement.Visible = false;
                this.gvwTieInSurvey.DataBind();
                this.gvwTieInSurvey.Visible = false;
                this.btnClearResDetail.Visible = false;
                this.lblQCResSQC.Visible = false;
                this.lblQCResMetaData.Visible = false;
                this.lblQCResParameters.Visible = false;
                this.lblRQCQualifiers.Visible = false;
                this.lblRQCBHA.Visible = false;
                this.lblNMSCalc.Visible = false;
                this.lblRQCMinMax.Visible = false;
                this.lblPlacement.Visible = false;
                this.lblTieInSurvey.Visible = false;
                this.enWDInfo.Visible = false;
                this.lblWDInfo.Text = "";
                this.enRDInfo.Visible = false;
                this.lblRDInfo.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRQCResults_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 resID = -99;
                Decimal Depth = -99.00M;
                GridViewRow dRow = this.gvwRQCResults.SelectedRow;
                Depth = Convert.ToDecimal(Server.HtmlDecode(dRow.Cells[3].Text));
                resID = Convert.ToInt32(this.gvwRQCResults.SelectedValue);
                Int32 opID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 pdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 wsID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                Int32 rnID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                DataTable selSQC = QC.getResSQCTable(resID, GetClientDBString);
                DataTable refMagTable = OPR.getReferenceMagneticsTable(opID, SysClientID, pdID, wlID, GetClientDBString);
                DataTable resParam = QC.getResParameters(resID, GetClientDBString);
                DataTable qualfParam = QC.getResQualifiersTable(resID, GetClientDBString);
                DataTable BHATable = QC.getResBHATable(resID, GetClientDBString);
                DataTable NMCTable = QC.getNMCalcTable(resID, GetClientDBString);
                DataTable MinMaxTable = QC.getMinMaxTable(resID, GetClientDBString);
                Int32 acID = QC.getAzimuthCriteriaIDForResultRow(resID, GetClientDBString);
                DataTable placeTable = QC.getPlacementTableforSelectedAzimuthCriteria(opID, pdID, wlID, Depth, acID, GetClientDBString);
                DataTable plansTable = QC.getTieInSurveysTable(Depth, rnID, wsID, wlID, GetClientDBString);
                this.lblQCResMetaData.Visible = true;
                this.lblQCResParameters.Visible = true;
                this.lblRQCQualifiers.Visible = true;
                this.lblRQCBHA.Visible = true;
                this.lblNMSCalc.Visible = true;
                this.lblRQCMinMax.Visible = true;
                this.lblQCResSQC.Visible = true;
                this.lblPlacement.Visible = true;
                this.lblTieInSurvey.Visible = true;
                this.gvwResSQC.DataSource = selSQC;
                this.gvwResSQC.DataBind();
                this.gvwResSQC.Visible = true;
                this.gvwRefVals.DataSource = refMagTable;
                this.gvwRefVals.DataBind();
                this.gvwRefVals.Visible = true;
                this.gvwQCResParameters.DataSource = resParam;
                this.gvwQCResParameters.DataBind();
                this.gvwQCResParameters.Visible = true;
                this.gvwRQCQualifiers.DataSource = qualfParam;
                this.gvwRQCQualifiers.DataBind();
                this.gvwRQCQualifiers.Visible = true;
                this.gvwRQCBHA.DataSource = BHATable;
                this.gvwRQCBHA.DataBind();
                this.gvwRQCBHA.Visible = true;
                this.gvwNMSCalc.DataSource = NMCTable;
                this.gvwNMSCalc.DataBind();
                this.gvwNMSCalc.Visible = true;
                this.gvwRQCMinMax.DataSource = MinMaxTable;
                this.gvwRQCMinMax.DataBind();
                this.gvwRQCMinMax.Visible = true;
                this.gvwPlacement.DataSource = placeTable;
                this.gvwPlacement.DataBind();
                this.gvwPlacement.Visible = true;
                this.gvwTieInSurvey.DataSource = plansTable;
                this.gvwTieInSurvey.DataBind();
                this.gvwTieInSurvey.Visible = true;
                this.btnClearResDetail.Visible = true;
                this.btnChart_Click(null, null);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void chk_NoSignTransformation_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.chk_NoSignTransformation.Checked = true;
                this.chk_FlipGx.Checked = false;
                this.chk_FlipGy.Checked = false;
                this.chk_FlipGz.Checked = false;
                this.chk_FlipBx.Checked = false;
                this.chk_FlipBy.Checked = false;
                this.chk_FlipBz.Checked = false;
                this.btnProcessRawData.Enabled = true;
                this.btnProcessRawData.CssClass = "btn btn-info text-center";
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void chk_FlipGx_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.chk_NoSignTransformation.Checked = false;
                this.btnProcessRawData.Enabled = true;
                this.btnProcessRawData.CssClass = "btn btn-info text-center";
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void chk_FlipGy_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.chk_NoSignTransformation.Checked = false;
                this.btnProcessRawData.Enabled = true;
                this.btnProcessRawData.CssClass = "btn btn-info text-center";
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void chk_FlipGz_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.chk_NoSignTransformation.Checked = false;
                this.btnProcessRawData.Enabled = true;
                this.btnProcessRawData.CssClass = "btn btn-info text-center";
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void chk_FlipBx_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.chk_NoSignTransformation.Checked = false;
                this.btnProcessRawData.Enabled = true;
                this.btnProcessRawData.CssClass = "btn btn-info text-center";
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void chk_FlipBy_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.chk_NoSignTransformation.Checked = false;
                this.btnProcessRawData.Enabled = true;
                this.btnProcessRawData.CssClass = "btn btn-info text-center";
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void chk_FlipBz_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.chk_NoSignTransformation.Checked = false;
                this.btnProcessRawData.Enabled = true;
                this.btnProcessRawData.CssClass = "btn btn-info text-center";
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRQCResults_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 optrID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 wpdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                Int32 wID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 sID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                Int32 rID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                DataTable resData = QC.getMetaResults(optrID, wpdID, wID, sID, rID, GetClientDBString);
                this.gvwRQCResults.DataSource = resData;
                this.gvwRQCResults.PageIndex = e.NewPageIndex;
                this.gvwRQCResults.SelectedIndex = -1;
                this.gvwRQCResults.DataBind();
                this.btnChart_Click(null, null);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwResSQC_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridViewRow row = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                TableHeaderCell cell = new TableHeaderCell();
                cell.Text = "Survey Qualification Criterion";
                cell.ColumnSpan = 4;
                row.Controls.Add(cell);
                row.BackColor = ColorTranslator.FromHtml("#3AC0F2");
                gvwResSQC.HeaderRow.Controls.AddAt(0, row);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void createChartQCTotal()
        {
            try
            {
                String result = String.Empty;
                String echarts = String.Empty;
                String echartsAzim = String.Empty;
                String echarts3D = String.Empty;
                decimal maxTVD = 0;
                GridViewRow wgRow = this.gvwWellList.SelectedRow;
                String wellName = Convert.ToString(Server.HtmlDecode(wgRow.Cells[2].Text));
                System.Data.DataTable dsChartData = new System.Data.DataTable();
                System.Data.DataTable dsChartDataAzim = new System.Data.DataTable();
                System.Data.DataTable getScalerEastingNorthing = new System.Data.DataTable();
                Int32 wsD = Convert.ToInt32(this.gvwWellList.SelectedValue);
                if (wsD.Equals(-2))
                {                    
                    Int32 wD = Convert.ToInt32(this.gvwWellList.SelectedValue);
                    Int32 srvID = AST.getWellRegisteredServiceID(wD, GetClientDBString);
                    dsChartData = CHRTRes.getDataTableforQCWellGraph(wD, GetClientDBString);
                    dsChartDataAzim = QC.getDataTableforQCWellGraphAZIM(wD, GetClientDBString);
                    maxTVD = CHRTRes.getMaxTVDforWell(wD, GetClientDBString);
                    getScalerEastingNorthing = CHRTRes.getScallingFactorChartInfo_WPlan(wD, GetClientDBString);
                }
                else
                {                    
                    Int32 sWellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                    Int32 srvID = AST.getWellRegisteredServiceID(sWellID, GetClientDBString);
                    Int32 secID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                    Int32 RunID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                    dsChartData = CHRTRes.getDataTableforQCGraph_1(sWellID, secID, srvID, RunID, GetClientDBString);
                    dsChartDataAzim = CHRTRes.getDataTableforQCWelGraphAZIM_1(sWellID, secID, srvID, RunID, GetClientDBString);
                    maxTVD = CHRTRes.getMaxTVDforWell(sWellID, GetClientDBString);
                    getScalerEastingNorthing = CHRTRes.getScallingFactorChartInfo_WPlan(sWellID, GetClientDBString);
                }
                try
                {
                    echarts = CHRT.getChartsQCResultsECHRT01("BodyContent_ctrlRawQC_2A_divECharts", dsChartData);
                    this.ltrECHarts.Text = echarts;
                    echartsAzim = CHRT.getChartsQCResultsECHRT_AZM("BodyContent_ctrlRawQC_2A_divEChartsAzim", dsChartDataAzim);
                    this.ltrECHartsAzim.Text = echartsAzim;
                    echarts3D = CHRT.getChartsQCResultsECHRT_3D_Definitive("BodyContent_ctrlRawQC_2A_divECharts3D", wellName, dsChartData, getScalerEastingNorthing , maxTVD);
                    this.ltrECHarts3D.Text = echarts3D;
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
                finally
                {
                    dsChartData.Dispose();
                    result = String.Empty;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnChart_Click(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.createChartQCTotal();
            }
            else
            {
                this.createChartQCTotal();
            }
        }

        protected void resetPage()
        {
            System.Data.DataTable opTable = CLNT.getOperatorCoTable(ClientType);
            this.gvwGlobalOptr.DataSource = opTable;
            this.gvwGlobalOptr.SelectedIndex = -1;
            this.gvwGlobalOptr.DataBind();            
            this.gvwWellList.SelectedIndex = -1;
            this.gvwWellList.DataBind();
            this.gvwSectionList.SelectedIndex = -1;
            this.gvwSectionList.DataBind();
            this.gvwRunList.SelectedIndex = -1;
            this.gvwRunList.DataBind();
            this.btnProcessRawData.Enabled = false;
        }

        protected void gvwRQCQualifiers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = ((DataRowView)e.Row.DataItem).Row;
                    Int32 dC = Convert.ToInt32(dr["DWClr"]);
                    Int32 bC = Convert.ToInt32(dr["DYClr"]);
                    Int32 gC = Convert.ToInt32(dr["EAClr"]);
                    Int32 nG = Convert.ToInt32(dr["HVClr"]);

                    if (dC.Equals(1))
                    {
                        e.Row.Cells[1].BackColor = Color.LightGreen;
                    }
                    else
                    {
                        e.Row.Cells[1].BackColor = Color.Red;
                    }

                    if (bC.Equals(1))
                    {
                        e.Row.Cells[3].BackColor = Color.LightGreen;
                    }
                    else
                    {
                        e.Row.Cells[3].BackColor = Color.Red;
                    }

                    if (gC.Equals(1))
                    {
                        e.Row.Cells[5].BackColor = Color.LightGreen;
                    }
                    else
                    {
                        e.Row.Cells[5].BackColor = Color.Red;
                    }

                    if (nG.Equals(2))
                    {
                        e.Row.Cells[11].BackColor = Color.Red;
                    }
                    else
                    {
                        e.Row.Cells[11].BackColor = Color.LightGreen;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRQCBHA_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = ((DataRowView)e.Row.DataItem).Row;
                    Int32 ek = Convert.ToInt32(dr["EKClr"]);

                    if (ek.Equals(2) || ek.Equals(4) || ek.Equals(7))
                    {
                        e.Row.Cells[4].BackColor = Color.LightGreen;
                    }
                    else
                    {
                        e.Row.Cells[4].BackColor = Color.Red;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void createAssetCharts(object sender, EventArgs e)
        {
            try
            {
                List<Int32> wellsDetails = CHRT.getClientRawQCSummary(GetClientDBString);
                regCount = wellsDetails[0];
                wellCount = wellsDetails[1];
                mrmCount = wellsDetails[2];
                runCount = wellsDetails[3];
                mbhaCount = wellsDetails[4];
                this.divGuageJob.InnerHtml = "<h3>" + regCount.ToString() + "</h3><span>Service Registration Requests</span>";
                this.divGuageWell.InnerHtml = "<h3>" + wellCount.ToString() + "</h3><span>In-Process Well(s)/Lateral(s)</span>";
                this.divAscWell.InnerHtml = "<h3>" + mrmCount.ToString() + "</h3><span>Missing Ref. Mags.</span>";
                this.divMRM.InnerHtml = "<h3>" + runCount.ToString() + "</h3><span>Active Run</span>";
                this.divMsngBHA.InnerHtml = "<h3>" + mbhaCount.ToString() + "</h3><span>Active Run(s) Missing BHA Signature</span>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected string loadGraphs()
        {
            try
            {
                StringBuilder strScript = new StringBuilder();

                strScript.Append("<script>");
                strScript.Append(" window.onload = function() { ");
                strScript.Append(" var multiline = document.getElementById('chrtQCChart').getContext(\"2d\", {alpha: false}); ");
                strScript.Append(" window.myLine = new Chart(multiline, configQCChart);  ");

                strScript.Append("    }; ");
                strScript.Append(" var colorNames = Object.keys(window.chartColors); ");

                strScript.Append("</script>");

                return strScript.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void imgbtnGeoMag_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://www.n3kl.org/sun/noaa.html");
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProcessReadyData_Click(object sender, EventArgs e)
        {
            try
            {
                ArrayList iResult = new ArrayList();
                Int32 wellFlag = -99, runFlag = -99;
                List<System.Data.DataTable> uploadedData = new List<System.Data.DataTable>();
                System.Data.DataTable selectedValues = new System.Data.DataTable();
                String fName = String.Empty, wellName = String.Empty, runName = String.Empty;
                String FileToBeProcessed = Convert.ToString(this.Session["uploadedSurveyFile"]);
                if (FileUploadControl.HasFile) { fName = FileUploadControl.FileName; } else { fName = "---"; }
                Int32 optrID = 0, srvID = 0, padID = 0, wellID = 0, sectionID = 0, runNameID = 0, RunID = 0, counter = 0, dt = 0, dem = 0;
                Int32 lenUnit = -99, acelUnit = -99, magUnit = -99;
                if(this.rdbFT.Checked){lenUnit = 4;}else{lenUnit = 5;}
                if(this.rdbCMS.Checked){acelUnit = 1;}else if(this.rdbFSC.Checked){acelUnit = 2;} else if(this.rdbG.Checked){acelUnit = 3;}else if(this.rdbMSC.Checked){acelUnit = 4;}else{acelUnit = 5;}
                if(this.rdbGss.Checked){magUnit = 1;}else if(this.rdbT.Checked){magUnit = 2;} else if(this.rdbMLIT.Checked){magUnit = 3;}else if(this.rdbMCT.Checked){magUnit = 4;}else{magUnit = 5;}
                optrID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                padID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                wellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                srvID = AST.getWellRegisteredServiceID(wellID, GetClientDBString);
                wellName = AST.getWellName(wellID, GetClientDBString);
                sectionID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                RunID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                dt = Convert.ToInt32(this.ddlDataType.SelectedValue);
                dem = Convert.ToInt32(this.ddlUploadMethod.SelectedValue);
                if(dt.Equals(1)) //Raw Surveys
                {
                    uploadedData = (List<System.Data.DataTable>)HttpContext.Current.Session["rawUploadedDataTables"];
                    selectedValues = (System.Data.DataTable)HttpContext.Current.Session["selectedRawValues"];
                }
                else  //Result Surveys
                {
                    uploadedData = (List<System.Data.DataTable>)HttpContext.Current.Session["resUploadedDataTables"];
                    selectedValues = (System.Data.DataTable)HttpContext.Current.Session["selectedResultValues"];
                }
                runNameID = AST.getRunNameIDFromRunID(RunID, GetClientDBString);
                runName = AST.getRunName(runNameID);
                counter = QC.getRunCount(RunID, sectionID, wellID, padID, SysClientID, optrID, GetClientDBString);
                iResult = QC.ProcessRawData(optrID, SysClientID, srvID, padID, wellID, sectionID, RunID, dt, dem, lenUnit, acelUnit, magUnit, fName, uploadedData, selectedValues, LoginName, domain, GetClientDBString);
                wellFlag = Convert.ToInt32(iResult[1]);
                runFlag = Convert.ToInt32(iResult[2]);
                this.liStep7.Attributes["class"] = "stepper-done";
                this.divTransformData.Visible = false;                
                this.divQC.Visible = true;
                System.Data.DataTable resultsData = QC.getMetaResults(optrID, padID, wellID, sectionID, RunID, GetClientDBString);
                this.gvwRQCResults.DataSource = resultsData;
                this.gvwRQCResults.DataBind();
                this.gvwRQCResults.Visible = true;
                Decimal maxWDepthTI = -99.00M;
                maxWDepthTI = QC.getMaxDepthForTieInSurveys(RunID, sectionID, wellID, padID, SysClientID, optrID, GetClientDBString);
                System.Data.DataTable wellPlansTable = QC.getTieInSurveysTable(maxWDepthTI, RunID, sectionID, wellID, GetClientDBString);
                this.gvwMaxRunTieIn.DataSource = wellPlansTable;
                this.gvwMaxRunTieIn.DataBind();
                this.btnChart_Click(this, EventArgs.Empty); // Generate the Graphs
                HttpContext.Current.Session["uploadedSurveyFile"] = "";
                this.divQC.Visible = true;
                GridViewRow wellNameRow = this.gvwWellList.SelectedRow;
                String wName = Convert.ToString(Server.HtmlDecode(wellNameRow.Cells[2].Text));
                if (runFlag.Equals(1))
                {
                    this.enRDInfo.Visible = true;
                    this.enRDInfo.Attributes["class"] = "alert alert-info alert-dismissable";
                    this.iRDInfo.Attributes["class"] = "icon fa fa-info-circle";
                    this.spnRDInfo.InnerText = " !!! Alert !!!";
                    this.lblRDInfo.Text = "Reached Run End Depth";
                }
                else if (runFlag.Equals(2))
                {
                    this.enRDInfo.Visible = true;
                    this.enRDInfo.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iRDInfo.Attributes["class"] = "icon fa fa-warning";
                    this.spnRDInfo.InnerText = " !!! Caution !!!";
                    this.lblRDInfo.Text = "Exceeded Run End Depth";
                }
                else
                {
                    this.enRDInfo.Visible = false;
                    this.lblRDInfo.Text = "";
                }
                if (wellFlag.Equals(1))
                {
                    this.enWDInfo.Visible = true;
                    this.enWDInfo.Attributes["class"] = "alert alert-info alert-dismissable";
                    this.iWDInfo.Attributes["class"] = "icon fa fa-info-circle";
                    this.spnWDInfo.InnerText = " !!! Alert !!!";
                    this.lblWDInfo.Text = "Reached Plan Target Depth";
                }
                else if (wellFlag.Equals(2))
                {
                    this.enWDInfo.Visible = true;
                    this.enWDInfo.Attributes["class"] = "alert alert-info alert-dismissable";
                    this.iWDInfo.Attributes["class"] = "icon fa fa-info-circle";
                    this.spnWDInfo.InnerText = " !!! Alert !!!";
                    this.lblWDInfo.Text = "Exceeded Planned Target Depth. (Working within In-Process threshold)";
                }
                else if (wellFlag.Equals(3))
                {
                    this.enWDInfo.Visible = true;
                    this.enWDInfo.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iWDInfo.Attributes["class"] = "icon fa fa-warning";
                    this.spnWDInfo.InnerText = " !!! Alert !!!";
                    this.lblWDInfo.Text = "Exceeded Planned Target Depth and In-Process threshold. Additional Data Processing is restricted";
                    AST.updateWellStatus(optrID, wellID, 1, GetClientDBString); //Demog Well Status
                    Helpers.DemogHelpers.Demographics.updateWellProcessRegistration(optrID, SysClientID, padID, wellID, serviceGroupID, srvID, 1, "SMARTs Platform", GetClientDBString);
                }
                else
                {
                    this.enWDInfo.Visible = false;
                    this.lblWDInfo.Text = "";
                }
                this.btnMetaCancel.Visible = false;
                logger.Info("User: " + username + " QC_Processed for Well/Lateral: " + wName);                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCancelReadyData_Click(object sender, EventArgs e)
        {
            try
            {
                this.gvwSelectedRaw.DataSource = null;
                this.gvwSelectedRaw.DataBind();
                this.gvwSelectedRaw.Visible = false;
                this.gvwSelectedResult.DataSource = null;
                this.gvwSelectedResult.DataBind();
                this.gvwSelectedResult.Visible = false;
                this.liStep5.Attributes["class"] = "stepper-todo";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwReadyRawData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable rawTable = new System.Data.DataTable();
                List<System.Data.DataTable> selTables = new List<System.Data.DataTable>();
                selTables = (List<System.Data.DataTable>)HttpContext.Current.Session["rawDataTables"];
                rawTable = (System.Data.DataTable)selTables[3];
                this.lblTotalRaw.Text = String.Format("{0} {1}", "Total Raw Survey Rows : ", Convert.ToInt32(rawTable.Rows.Count));
                this.lblTotalRaw.Visible = true;
                this.lblTotalResult.Visible = false;
                this.gvwReadyRawData.DataSource = rawTable;
                this.gvwReadyRawData.PageIndex = e.NewPageIndex;
                this.gvwReadyRawData.SelectedIndex = -1;
                this.gvwReadyRawData.DataBind();
                this.gvwReadyResultsData.DataSource = null;
                this.gvwReadyResultsData.DataBind();
                this.gvwReadyResultsData.Visible = false;
                this.liStep5.Attributes["class"] = "stepper-done";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwReadyResultsData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable resTable = new System.Data.DataTable();
                List<System.Data.DataTable> selTables = new List<System.Data.DataTable>();
                selTables = (List<System.Data.DataTable>)HttpContext.Current.Session["rawDataTables"];
                resTable = (System.Data.DataTable)selTables[3];
                this.gvwReadyRawData.DataSource = null;
                this.gvwReadyRawData.DataBind();
                this.gvwReadyRawData.Visible = false;
                this.gvwReadyResultsData.DataSource = resTable;
                this.gvwReadyResultsData.PageIndex = e.NewPageIndex;
                this.gvwReadyResultsData.SelectedIndex = -1;
                this.gvwReadyResultsData.DataBind();
                this.liStep5.Attributes["class"] = "stepper-done";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSelectedRaw_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {                
                System.Data.DataTable selectedRawTable = new System.Data.DataTable();
                selectedRawTable = (System.Data.DataTable)HttpContext.Current.Session["selectedRawValues"];
                this.gvwViewRawDisplay.Visible = false;
                this.gvwViewResultsDispaly.Visible = false;
                this.gvwSelectedRaw.DataSource = selectedRawTable;
                this.gvwSelectedRaw.PageIndex = e.NewPageIndex;
                this.gvwSelectedRaw.SelectedIndex = -1;
                this.gvwSelectedRaw.DataBind();
                this.gvwSelectedRaw.Visible = true;
                this.liStep5.Attributes["class"] = "stepper-todo";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSelectedResult_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = ((DataRowView)e.Row.DataItem).Row;
                    CheckBox chkSelected = e.Row.FindControl("srvyCheck") as CheckBox;
                    if (dr["chk"].Equals(1))
                    {
                        e.Row.BackColor = Color.LightGreen;
                        chkSelected.Checked = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSelectedResult_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable selectedResTable = new System.Data.DataTable();
                selectedResTable = (System.Data.DataTable)HttpContext.Current.Session["selectedRawValues"];
                this.gvwSelectedResult.DataSource = selectedResTable;
                this.gvwSelectedResult.PageIndex = e.NewPageIndex;
                this.gvwSelectedResult.SelectedIndex = -1;
                this.gvwSelectedResult.DataBind();
                this.liStep5.Attributes["class"] = "stepper-todo";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSelectedRaw_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = ((DataRowView)e.Row.DataItem).Row;
                    CheckBox chkSelected = e.Row.FindControl("srvyCheck") as CheckBox;
                    if (dr["chk"].Equals(1))
                    {
                        e.Row.BackColor = Color.LightGreen;
                        chkSelected.Checked = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbFT_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
                this.rdbMT.Checked = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbMT_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
                this.rdbFT.Checked = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbCMS_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
                this.rdbFSC.Checked = false;
                this.rdbG.Checked = false;
                this.rdbMSC.Checked = false;
                this.rdbMG.Checked = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbFSC_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
                this.rdbCMS.Checked = false;
                this.rdbG.Checked = false;
                this.rdbMSC.Checked = false;
                this.rdbMG.Checked = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbG_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
                this.rdbCMS.Checked = false;
                this.rdbFSC.Checked = false;
                this.rdbMSC.Checked = false;
                this.rdbMG.Checked = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbMSC_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
                this.rdbCMS.Checked = false;
                this.rdbFSC.Checked = false;
                this.rdbG.Checked = false;
                this.rdbMG.Checked = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbMG_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
                this.rdbCMS.Checked = false;
                this.rdbFSC.Checked = false;
                this.rdbG.Checked = false;
                this.rdbMSC.Checked = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbGss_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
                this.rdbT.Checked = false;
                this.rdbMLIT.Checked = false;
                this.rdbMCT.Checked = false;
                this.rdbNT.Checked = false;                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbT_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
                this.rdbGss.Checked = false;
                this.rdbMLIT.Checked = false;
                this.rdbMCT.Checked = false;
                this.rdbNT.Checked = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbMLIT_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
                this.rdbGss.Checked = false;
                this.rdbT.Checked = false;
                this.rdbMCT.Checked = false;
                this.rdbNT.Checked = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbMCT_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
                this.rdbGss.Checked = false;
                this.rdbT.Checked = false;
                this.rdbMLIT.Checked = false;
                this.rdbNT.Checked = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbNT_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
                this.rdbGss.Checked = false;
                this.rdbT.Checked = false;
                this.rdbMLIT.Checked = false;
                this.rdbMCT.Checked = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlDataType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.ddlUploadMethod.Enabled = true;
                this.ddlUploadMethod.SelectedIndex = -1;
                this.txtDataEnclosure.Visible = false;
                this.lblManual.Text = "";
                this.filDEnclosure.Visible = false;
                this.FileUploadStatusLabel.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlUploadMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 dt = Convert.ToInt32(this.ddlDataType.SelectedValue);
                    Int32 um = Convert.ToInt32(this.ddlUploadMethod.SelectedValue);
                    if (um.Equals(1))
                    {
                        if (dt.Equals(1))
                        {
                            this.divManData.Visible = true;
                            this.divFileData.Visible = false;
                            this.txtQCManualProcess.Text = "";
                            this.txtQCManualProcess.Visible = true;
                            this.txtQCResultProcess.Text = "";
                            this.txtQCResultProcess.Visible = false;
                        }
                        else
                        {
                            this.divManData.Visible = true;
                            this.divFileData.Visible = false;
                            this.txtQCManualProcess.Text = "";
                            this.txtQCManualProcess.Visible = false;
                            this.txtQCResultProcess.Text = "";
                            this.txtQCResultProcess.Visible = true;
                        }
                    }
                    else
                    {
                        this.divManData.Visible = false;
                        this.divFileData.Visible = true;
                    }
                    this.divDataEntry.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }                                                                                        
    }
}