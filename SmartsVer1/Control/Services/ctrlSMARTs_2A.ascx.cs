﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net.Mail;
using System.Web;
using System.Globalization;
using System.Web.Security;
using System.Web.UI.WebControls;
using CLR = System.Drawing.Color;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using SRV = SmartsVer1.Helpers.DemogHelpers.ServicesConfig;

namespace SmartsVer1.Control.Services
{
    public partial class ctrlSMARTs_2A : System.Web.UI.UserControl
    {
        String LoginName, username, domain, GetClientDBString;
        Int32 ClientID = -99;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);

                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
                
                System.Data.DataTable sgTable = SRV.getSrvGroupTable();
                this.gvwSrvGrp.DataSource = sgTable;
                this.gvwSrvGrp.PageIndex = 0;
                this.gvwSrvGrp.SelectedIndex = -1;
                this.gvwSrvGrp.DataBind();

                System.Data.DataTable srvStat = SRV.getServiceStatusTable();
                this.gvwSrvStatList.DataSource = srvStat;
                this.gvwSrvStatList.PageIndex = 0;
                this.gvwSrvStatList.SelectedIndex = -1;
                this.gvwSrvStatList.DataBind();
                System.Data.DataTable chgTable = SRV.getServiceChargeTable();
                this.gvwSrvChgUnit.DataSource = chgTable;
                this.gvwSrvChgUnit.PageIndex = 0;
                this.gvwSrvChgUnit.SelectedIndex = -1;
                this.gvwSrvChgUnit.DataBind();
                System.Data.DataTable priceTable = SRV.getStandardPricesTable();
                this.gvwStdPriceList.DataSource = priceTable;
                this.gvwStdPriceList.PageIndex = 0;
                this.gvwStdPriceList.SelectedIndex = -1;
                this.gvwStdPriceList.DataBind();
                System.Data.DataTable limTable = SRV.getMDProcessingLimitTable();
                this.gvwMDLimits.DataSource = limTable;
                this.gvwMDLimits.PageIndex = 0;
                this.gvwMDLimits.SelectedIndex = -1;
                this.gvwMDLimits.DataBind();
                //Calendar Controls
                this.clndrStart.StartDate = DateTime.Now;
                this.clndrEnd.StartDate = DateTime.Now;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {}
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        /// <summary>
        /// Service Group
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvwSrvGrp_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable sgTable = SRV.getSrvGroupTable();
                this.gvwSrvGrp.DataSource = sgTable;
                this.gvwSrvGrp.PageIndex = e.NewPageIndex;
                this.gvwSrvGrp.SelectedIndex = -1;
                this.gvwSrvGrp.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSrvGrp_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 gpID = Convert.ToInt32(this.gvwSrvGrp.SelectedValue);
                this.gvwSrvGrp.Visible = false;
                System.Data.DataTable srvTable = SRV.getGlobalServicesTable(gpID);
                this.gvwSrvGrpService.DataSource = srvTable;
                this.gvwSrvGrpService.SelectedIndex = -1;
                this.gvwSrvGrpService.DataBind();
                this.gvwSrvGrpService.Visible = true;
                this.btnSrvClear.Enabled = true;
                this.btnSrvClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSrvGrpService_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 gpID = Convert.ToInt32(this.gvwSrvGrp.SelectedValue);
                DataTable tableSRV = SRV.getServicesTable(gpID, GetClientDBString);
                this.gvwSrvGrpService.PageIndex = e.NewPageIndex;
                this.gvwSrvGrpService.SelectedIndex = -1;
                this.gvwSrvGrpService.DataSource = tableSRV;
                this.gvwSrvGrpService.DataBind();
                this.btnSrvClear.Enabled = true;
                this.btnSrvClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSrvClear_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable sgTable = SRV.getSrvGroupTable();
                this.gvwSrvGrp.DataSource = sgTable;
                this.gvwSrvGrp.DataBind();
                this.gvwSrvGrp.Visible = true;
                this.gvwSrvGrpService.DataBind();
                this.gvwSrvGrpService.Visible = false;
                this.btnSrvClear.Enabled = false;
                this.btnSrvClear.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSrvGrpAdd_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable sgTable = SRV.getSrvGroupTable();
                this.gvwSrvGrpAdd.DataSource = sgTable;
                this.gvwSrvGrpAdd.PageIndex = e.NewPageIndex;
                this.gvwSrvGrpAdd.SelectedIndex = -1;
                this.gvwSrvGrpAdd.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSrvGrpAdd_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 gpID = Convert.ToInt32(this.gvwSrvGrpAdd.SelectedValue);
                this.gvwSrvGrpAdd.Visible = false;
                this.divGrpParam.Visible = true;
                this.txtSASrvName.Text = "";
                this.txtSASrvVersion.Text = "";
                Dictionary<Int32, String> stList = SRV.getSrvStatList();
                this.ddlSASrvStatus.Items.Clear();
                this.ddlSASrvStatus.DataSource = stList;
                this.ddlSASrvStatus.DataTextField = "Value";
                this.ddlSASrvStatus.DataValueField = "Key";
                this.ddlSASrvStatus.DataBind();
                this.ddlSASrvStatus.Items.Insert(0, new ListItem("--- Select SMARTs Service Status ---", "-1"));
                this.ddlSASrvStatus.SelectedIndex = -1;
                this.btnAddNewClear.Enabled = true;
                this.btnAddNewClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlSASrvStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnAddNewSrv.Enabled = true;
                this.btnAddNewSrv.CssClass = "btn btn-success text-center";
                this.gpEnclosure.Visible = false;
                this.lblSrvSuccess.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnNewSrv_Click(object sender, EventArgs e)
        {
            try
            {
                btnSrvClear_Click(null, null);
                this.divSrvcsList.Attributes["class"] = "panel-collapse collapse";
                this.divAddSrvcs.Attributes["class"] = "panel-collapse collapse in";
                System.Data.DataTable sgTable = SRV.getSrvGroupTable();
                this.gvwSrvGrpAdd.DataSource = sgTable;
                this.gvwSrvGrpAdd.DataBind();
                this.divGrpParam.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewSrv_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, grpID = -99, sttID = -99;
                String sName = String.Empty, sVer = String.Empty;                
                grpID = Convert.ToInt32(this.gvwSrvGrpAdd.SelectedValue);
                sName = this.txtSASrvName.Text;
                sVer = this.txtSASrvVersion.Text;
                sttID = Convert.ToInt32(this.ddlSASrvStatus.SelectedValue);

                if (String.IsNullOrEmpty(sName) || String.IsNullOrEmpty(sVer))
                {
                    this.gpEnclosure.Visible = true;
                    this.gpEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iGP.Attributes["class"] = "icon fa fa-warning";
                    this.spnGP.InnerText = " Warning!";
                    this.lblSrvSuccess.Text = "Null values found. Please provide all values before inserting New Service";
                }
                else
                {
                    iResult = SmartsVer1.Helpers.DemogHelpers.ServicesConfig.insertService(grpID, sName, sttID, sVer);
                    if (iResult.Equals(1))
                    {
                        btnAddNewClear_Click(null, null);
                        this.gpEnclosure.Visible = true;
                        this.gpEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iGP.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnGP.InnerText = " Success!";
                        this.lblSrvSuccess.Text = "Successfully inserted New Service into selected Service Group.";
                    }
                    else
                    {
                        btnAddNewClear_Click(null, null);
                        this.gpEnclosure.Visible = true;
                        this.gpEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iGP.Attributes["class"] = "icon fa fa-warning";
                        this.spnGP.InnerText = " Warning!";
                        this.lblSrvSuccess.Text = "Unknown Error. Please try again.";
                    }
                }
                this.btnAddNewSrv.CssClass = "btn btn-default text-center";
                this.btnAddNewSrv.Enabled = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewClear_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable gpTable = SRV.getSrvGroupTable();
                this.gvwSrvGrpAdd.DataSource = gpTable;
                this.gvwSrvGrpAdd.DataBind();
                this.gvwSrvGrpAdd.Visible = true;
                this.divGrpParam.Visible = false;
                this.txtSASrvName.Text = "";
                this.txtSASrvVersion.Text = "";
                this.ddlSASrvStatus.Items.Clear();
                this.ddlSASrvStatus.DataBind();
                this.ddlSASrvStatus.Items.Insert(0, new ListItem("--- Select SMARTs Service Status ---", "-1"));
                this.ddlSASrvStatus.SelectedIndex = -1;
                this.gpEnclosure.Visible = false;
                this.btnAddNewClear.Enabled = false;
                this.btnAddNewClear.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSrvDone_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable sgTable = SRV.getSrvGroupTable();
                this.gvwSrvGrp.DataSource = sgTable;
                this.gvwSrvGrp.DataBind();
                this.btnAddNewClear_Click(null, null);
                this.divSrvcsList.Attributes["class"] = "panel-collapse collapse in";
                this.divAddSrvcs.Attributes["class"] = "panel-collapse collapse";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        /// <summary>
        /// Service Status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvwSrvStatList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable tableSTT = SRV.getServiceStatusTable();
                this.gvwSrvStatList.DataSource = tableSTT;
                this.gvwSrvStatList.PageIndex = e.NewPageIndex;
                this.gvwSrvStatList.SelectedIndex = -1;
                this.gvwSrvStatList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnStatAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnAddSrvStatCancel_Click(null, null);
                this.divSrvcsStat.Attributes["class"] = "panel-collapse collapse";
                this.divSrvcsStatAdd.Attributes["class"] = "panel-collapse collapse in";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddSrvStat_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                String value = String.Empty;
                value = Convert.ToString(this.txtStatAdd.Text);
                if (String.IsNullOrEmpty(value))
                {
                    this.txtStatAdd.Text = "";
                    this.enSrvStat.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iSrvStat.Attributes["class"] = "icon fa fa-warning";
                    this.spnSrvStat.InnerText = " Missing information!";
                    this.lblSrvStat.Text = "Detected Null or Empty value. Please provide all values.";
                    this.enSrvStat.Visible = true;
                }
                else
                {
                    if (iResult.Equals(1))
                    {
                        this.txtStatAdd.Text = "";
                        this.enSrvStat.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iSrvStat.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnSrvStat.InnerText = " Success!";
                        this.lblSrvStat.Text = "Successfully inserted Service Status value";
                        this.enSrvStat.Visible = true;
                    }
                    else
                    {
                        this.txtStatAdd.Text = "";
                        this.enSrvStat.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iSrvStat.Attributes["class"] = "icon fa fa-warning";
                        this.spnSrvStat.InnerText = " Warning!";
                        this.lblSrvStat.Text = "!!! Error !!! Please try again";
                        this.enSrvStat.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddSrvStatCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtStatAdd.Text = "";
                this.lblSrvStat.Text = "";
                this.enSrvStat.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddSrvStatDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnAddSrvStatCancel_Click(null, null);
                this.divSrvcsStat.Attributes["class"] = "panel-collapse collapse in";
                this.divSrvcsStatAdd.Attributes["class"] = "panel-collapse collapse";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        /// <summary>
        /// Service Charge Units
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSrvChg_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnAddChargeCancel_Click(null, null);
                this.divChargeList.Attributes["class"] = "panel-collapse collapse";
                this.divChargeAdd.Attributes["class"] = "panel-collapse collapse in";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSrvChgUnit_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable chgTable = SRV.getServiceChargeTable();
                this.gvwSrvChgUnit.DataSource = chgTable;
                this.gvwSrvChgUnit.PageIndex = e.NewPageIndex;
                this.gvwSrvChgUnit.SelectedIndex = -1;
                this.gvwSrvChgUnit.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddCharge_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                String chgValue = String.Empty;
                chgValue = Convert.ToString(this.txtChgUnit.Text);
                if (String.IsNullOrEmpty(chgValue))
                {
                    this.txtChgUnit.Text = "";
                    this.enSrvChg.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iSrvChg.Attributes["class"] = "icon fa fa-warning";
                    this.spnSrvChg.InnerText = " Missing information!";
                    this.lblSrvChgSuccess.Text = "Detected Null or Empty value. Please provide all values.";
                    this.enSrvChg.Visible = true;
                }
                else
                {
                    iResult = SRV.insertServiceChargeUnit(chgValue);
                    if (iResult.Equals(1))
                    {
                        this.txtChgUnit.Text = "";
                        this.enSrvChg.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iSrvChg.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnSrvChg.InnerText = " Success!";
                        this.lblSrvChgSuccess.Text = "Successfully inserted Service Charge Unit Value";
                        this.enSrvChg.Visible = true;
                    }
                    else
                    {
                        this.txtChgUnit.Text = "";
                        this.enSrvChg.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iSrvChg.Attributes["class"] = "icon fa fa-warning";
                        this.spnSrvChg.InnerText = " Warning!";
                        this.lblSrvChgSuccess.Text = "!!! Error !!! Please try again";
                        this.enSrvChg.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddChargeCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtChgUnit.Text = "";
                this.lblSrvChgSuccess.Text = "";
                this.enSrvChg.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddChargeDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnAddChargeCancel_Click(null, null);
                System.Data.DataTable chgTable = SRV.getServiceChargeTable();
                this.gvwSrvChgUnit.DataSource = chgTable;
                this.gvwSrvChgUnit.PageIndex = 0;
                this.gvwSrvChgUnit.SelectedIndex = -1;
                this.gvwSrvChgUnit.DataBind();
                this.divChargeList.Attributes["class"] = "panel-collapse collapse in";
                this.divChargeAdd.Attributes["class"] = "panel-collapse collapse";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        /// <summary>
        /// Standard Service Price List
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvwStdPriceList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable priceTable = SRV.getStandardPricesTable();
                this.gvwStdPriceList.DataSource = priceTable;
                this.gvwStdPriceList.PageIndex = e.NewPageIndex;
                this.gvwStdPriceList.SelectedIndex = -1;
                this.gvwStdPriceList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddSrvPrice_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnPriceAddCancel_Click(null, null);
                this.divPriceList.Attributes["class"] = "panel-collapse collapse";
                this.divPriceAdd.Attributes["class"] = "panel-collapse collapse in";
                Dictionary<Int32, String> srvGroup = SRV.getSrvGroupList();
                this.ddlPriceGroup.Items.Clear();
                this.ddlPriceGroup.DataSource = srvGroup;
                this.ddlPriceGroup.DataTextField = "Value";
                this.ddlPriceGroup.DataValueField = "Key";
                this.ddlPriceGroup.DataBind();
                this.ddlPriceGroup.Items.Insert(0, new ListItem("--- Select Service Group ---", "-1"));
                this.ddlPriceGroup.SelectedIndex = -1;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlPriceGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 sgID = Convert.ToInt32(this.ddlPriceGroup.SelectedValue);
                Dictionary<Int32, String> srvList = SRV.getServiceList(sgID);
                this.ddlPriceService.Items.Clear();
                this.ddlPriceService.DataSource = srvList;
                this.ddlPriceService.DataTextField = "Value";
                this.ddlPriceService.DataValueField = "Key";
                this.ddlPriceService.DataBind();
                this.ddlPriceService.Items.Insert(0, new ListItem("--- Select Service ---", "-1"));
                this.ddlPriceService.SelectedIndex = -1;
                this.ddlPriceService.Enabled = true;
                this.btnPriceAddCancel.CssClass = "btn btn-warning text-center";
                this.btnPriceAddCancel.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlPriceService_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtPrice.Text = "";
                this.txtPrice.Enabled = true;
                Dictionary<Int32, String> priceUnits = SRV.getServiceChargeList();
                this.ddlPriceUnit.Items.Clear();
                this.ddlPriceUnit.DataSource = priceUnits;
                this.ddlPriceUnit.DataTextField = "Value";
                this.ddlPriceUnit.DataValueField = "Key";
                this.ddlPriceUnit.DataBind();
                this.ddlPriceUnit.Items.Insert(0, new ListItem("--- Select Price Unit ---", "-1"));
                this.ddlPriceUnit.SelectedIndex = -1;
                this.ddlPriceUnit.Enabled = true;
                this.txtPriceStart.Text = DateTime.Now.ToString();
                this.txtPriceStart.Enabled = false;
                this.txtPriceEnd.Text = DateTime.Now.ToString();
                this.txtPriceEnd.Enabled = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlPriceUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.clndrStart.SelectedDate = DateTime.Now;
                this.txtPriceStart.Enabled = true;
                this.clndrEnd.SelectedDate = DateTime.Now;
                this.txtPriceEnd.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void clndrEnd_SelectedDateChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnPriceAdd.CssClass = "btn btn-success text-center";
                this.btnPriceAdd.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnPriceAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, sgpID = -99, srvID = -99, pUnit = -99;
                String priceGroup = String.Empty, priceInput = String.Empty;
                Decimal validPrice = -99.00M;
                DateTime start = DateTime.Now, end = DateTime.Now;
                sgpID = Convert.ToInt32(this.ddlPriceGroup.SelectedValue);
                srvID = Convert.ToInt32(this.ddlPriceService.SelectedValue);
                priceInput = Convert.ToString(this.txtPrice.Text);
                pUnit = Convert.ToInt32(this.ddlPriceUnit.SelectedValue);
                start = Convert.ToDateTime(this.clndrStart.SelectedDate);
                end = Convert.ToDateTime(this.clndrEnd.SelectedDate);
                if (String.IsNullOrEmpty(priceGroup) || String.IsNullOrEmpty(priceInput))
                {
                    this.enPrice.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iPrice.Attributes["class"] = "icon fa fa-warning";
                    this.spnPrice.InnerText = " Warning!";
                    this.lblPriceSuccess.Text = "!!! Error !!! Please enter a valid Price";
                    this.enPrice.Visible = true;
                }
                else if( !Decimal.TryParse(priceInput, out validPrice) )
                {
                    this.enPrice.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iPrice.Attributes["class"] = "icon fa fa-warning";
                    this.spnPrice.InnerText = " Warning!";
                    this.lblPriceSuccess.Text = "!!! Error !!! Invalid Price. Please enter a valid Price";
                    this.enPrice.Visible = true;
                }
                else if (end <= (start))
                {
                    this.enPrice.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iPrice.Attributes["class"] = "icon fa fa-warning";
                    this.spnPrice.InnerText = " Warning!";
                    this.lblPriceSuccess.Text = "!!! Error !!! Price End Date can not be the same as OR before Price Start Date";
                    this.enPrice.Visible = true;
                }
                else if (end > DateTime.Today.AddYears(3))
                {
                    this.enPrice.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iPrice.Attributes["class"] = "icon fa fa-warning";
                    this.spnPrice.InnerText = " Warning!";
                    this.lblPriceSuccess.Text = "!!! Error !!! Price End Date can not be longer than 3-Years";
                    this.enPrice.Visible = true;
                }
                else
                {
                    iResult = SRV.insertStandardPrice(sgpID, srvID, validPrice, pUnit, start, end);
                    if (iResult.Equals(1))
                    {
                        this.btnPriceAddCancel_Click(null, null);
                        this.enPrice.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iPrice.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnPrice.InnerText = " Success!";
                        this.lblPriceSuccess.Text = "Successfully inserted new Price in SMARTs";
                        this.enPrice.Visible = true;
                    }
                    else
                    {
                        this.enPrice.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iPrice.Attributes["class"] = "icon fa fa-warning";
                        this.spnPrice.InnerText = " Warning!";
                        this.lblPriceSuccess.Text = "!!! Error !!! Please try again";
                        this.enPrice.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnPriceAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.ddlPriceService.Items.Clear();
                this.ddlPriceService.DataBind();
                this.ddlPriceService.Items.Insert(0, new ListItem("--- Select Service ---", "-1"));
                this.ddlPriceService.SelectedIndex = -1;
                this.ddlPriceService.Enabled = false;
                this.txtPrice.Text = "";
                this.txtPrice.Enabled = false;
                this.ddlPriceUnit.Items.Clear();
                this.ddlPriceUnit.DataBind();
                this.ddlPriceUnit.Items.Insert(0, new ListItem("--- Select Price Unit ---", "-1"));
                this.ddlPriceUnit.SelectedIndex = -1;
                this.ddlPriceUnit.Enabled = false;
                this.txtPriceStart.Text = DateTime.Now.ToString();
                this.txtPriceStart.Enabled = false;
                this.txtPriceEnd.Text = DateTime.Now.ToString();
                this.txtPriceEnd.Enabled = false;
                this.lblPriceSuccess.Text = "";
                this.enPrice.Visible = false;
                Dictionary<Int32, String> srvGroup = SRV.getSrvGroupList();
                this.ddlPriceGroup.Items.Clear();
                this.ddlPriceGroup.DataSource = srvGroup;
                this.ddlPriceGroup.DataTextField = "Value";
                this.ddlPriceGroup.DataValueField = "Key";
                this.ddlPriceGroup.DataBind();
                this.ddlPriceGroup.Items.Insert(0, new ListItem("--- Select Service Group ---", "-1"));
                this.ddlPriceGroup.SelectedIndex = -1;
                this.btnPriceAdd.Enabled = false;
                this.btnPriceAdd.CssClass = "btn btn-default text-center";
                this.btnPriceAddCancel.Enabled = false;
                this.btnPriceAddCancel.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnPriceDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnPriceAddCancel_Click(null, null);
                System.Data.DataTable priceTable = SRV.getStandardPricesTable();
                this.gvwStdPriceList.DataSource = priceTable;
                this.gvwStdPriceList.PageIndex = 0;
                this.gvwStdPriceList.SelectedIndex = -1;
                this.gvwStdPriceList.DataBind();
                this.divPriceList.Attributes["class"] = "panel-collapse collapse in";
                this.divPriceAdd.Attributes["class"] = "panel-collapse collapse";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwMDLimits_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable limTable = SRV.getMDProcessingLimitTable();
                this.gvwMDLimits.DataSource = limTable;
                this.gvwMDLimits.PageIndex = e.NewPageIndex;
                this.gvwMDLimits.SelectedIndex = -1;
                this.gvwMDLimits.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnLimitNew_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtMDLim.Text = "";
                this.enLim.Visible = false;
                this.divMDList.Attributes["class"] = "panel-collapse collapse";
                this.divMDAdd.Attributes["class"] = "panel-collapse collapse in";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnLimAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, validValue = -99;
                String value = String.Empty;
                value = Convert.ToString(this.txtMDLim.Text);
                if (String.IsNullOrEmpty(value))
                {
                    this.enLim.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iLim.Attributes["class"] = "icon fa fa-warning";
                    this.spnLim.InnerText = " Warning!";
                    this.lblLimSuccess.Text = "!!! Error !!! Empty or Null value detected. Please provide a valid Limit value";
                    this.enLim.Visible = true;
                }
                else
                {
                    validValue = Convert.ToInt32(value);
                    iResult = SRV.insertMDProcessingLimit(validValue);
                    if (iResult.Equals(1))
                    {
                        this.btnLimAddCancel_Click(null, null);
                        this.enLim.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iLim.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnLim.InnerText = " Success!";
                        this.lblLimSuccess.Text = "Successfully inserted Measured Depth Processing Limit value";
                        this.enLim.Visible = true;
                    }
                    else if (iResult.Equals(-1))
                    {
                        this.btnLimAddCancel_Click(null, null);
                        this.enLim.Attributes["class"] = "alert alert-danger alert-dismissable";
                        this.iLim.Attributes["class"] = "icon fa fa-danger";
                        this.spnLim.InnerText = " Warning!";
                        this.lblLimSuccess.Text = "!!! Error !!! Found Duplicate value. Please insert a unique value";
                        this.enLim.Visible = true;
                    }
                    else
                    {
                        this.btnLimAddCancel_Click(null, null);
                        this.enLim.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iLim.Attributes["class"] = "icon fa fa-warning";
                        this.spnLim.InnerText = " Warning!";
                        this.lblLimSuccess.Text = "!!! Error !!! Please try again";
                        this.enLim.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnLimAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtMDLim.Text = "";
                this.lblLimSuccess.Text = "";
                this.enLim.Visible = false;                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnLimDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnLimAddCancel_Click(null, null);
                System.Data.DataTable limTable = SRV.getMDProcessingLimitTable();
                this.gvwMDLimits.DataSource = limTable;
                this.gvwMDLimits.PageIndex = 0;
                this.gvwMDLimits.SelectedIndex = -1;
                this.gvwMDLimits.DataBind();
                this.divMDList.Attributes["class"] = "panel-collapse collapse in";
                this.divMDAdd.Attributes["class"] = "panel-collapse collapse";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }                                
    }
}