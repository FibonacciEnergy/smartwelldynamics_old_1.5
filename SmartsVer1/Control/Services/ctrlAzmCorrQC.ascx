﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlAzmCorrQC.ascx.cs" Inherits="SmartsVer1.Control.Services.ctrlAzmCorrQC" %>

<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>

<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/Plugins/iCheck/all.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />

<!-- Javascript -->
<script src="../../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>
<!--- Page local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }

    .rotate {
        -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
        -moz-transform: rotate(90deg); /* Firefox */
        -ms-transform: rotate(90deg); /* IE 9 */
        transform: rotate(90deg); /* Standard syntax */
    }

    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper[data-stepper-steps="8"] li {
            width: 12%;
        }

        ol.stepper[data-stepper-steps="9"] li {
            width: 11%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }    
</style>
<!--- Page local script --->
<script type="text/javascript">
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("rotate");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").addClass("rotate");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("rotate");
        });
    });
</script>

<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>SMARTs</li>
                <li><a href="../../Services/fesAzmCorr_2A.aspx"> SMARTs Azimuth Audit/QC with Correction</a></li>
            </ol>
        </div>
    </div>
</div>

<!-- Page Content -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h1 class="box-title">Magnetic Acitivty</h1>
                </div>
                <div class="box-body">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div id="divSolar" class="bg-aqua with-border">
                            <div class="box-body">
                                <div class="col-xs-3 text-left">
                                    <asp:Image ID="imgSolar" runat="server" ImageUrl="~/Images/SolarRays.png" ImageAlign="Middle" />
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>
                                        <h4>Solar X-Rays Activity</h4>
                                    </div>
                                    <div id="divSolarLnk" runat="server">
                                        <asp:ImageButton ID="imgbSolarLink" runat="server" Width="25%" ImageUrl="http://www.n3kl.org/sun/images/status.gif?"
                                            ImageAlign="AbsMiddle" OnClick="imgbtnSolar_Click" CausesValidation="False" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div id="divMag" class="bg-aqua with-border">
                            <div class="box-body">
                                <div class="col-xs-3">
                                    <asp:Image ID="imgMag" runat="server" ImageUrl="~/Images/MagField.png" ImageAlign="Middle" Height="90px" />
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>
                                        <h4>Geomagnetic Field Activity</h4>
                                    </div>
                                    <div id="divMagLnk" runat="server">
                                        <asp:ImageButton ID="imgbMag" runat="server" Width="25%" ImageUrl="http://www.n3kl.org/sun/images/kpstatus.gif?"
                                            ImageAlign="AbsMiddle" OnClick="imgbtnGeoMag_Click" CausesValidation="False" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div id="divGlobe" class="bg-aqua with-border">
                            <div class="box-body">
                                <div class="col-xs-3">
                                    <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/MagField_2.png" ImageAlign="Middle" Height="90px" />
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>
                                        <h4>Regional Magnetic Activity</h4>
                                    </div>
                                    <div id="divGlobeLnk" runat="server">
                                        <a href="http://geomag.usgs.gov/realtime/" target="_blank" style="color: black;">USA</a> |
                                                    <a href="http://www.spaceweather.ca/current-actuelle/short-court/sfst-5-eng.php" target="_blank" style="color: black;">Canada</a>
                                        |
                                                    <a href="http://www.intermagnet.org/activitymap/activitymap-eng.php" target="_blank" style="color: black;">Global</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h1 class="box-title">Summary Information</h1>
            </div>
            <div class="box-body">
                <div class="row">
                    <div id="divGuages" runat="server" class="col-xs-12 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            <div class="small-box bg-blue">
                                <div id="divGuageJob" runat="server" class="inner" />
                                <div class="icon">
                                    <i class="fa fa-list" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            <div class="small-box bg-orange-active">
                                <div id="divGuageWell" runat="server" class="inner" />
                                <div class="icon">
                                    <i class="fa fa-tasks" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            <div class="small-box bg-olive">
                                <div id="divAscWell" runat="server" class="inner" />
                                <div class="icon">
                                    <i class="fa fa-chain-broken" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            <div class="small-box bg-olive">
                                <div id="divMRM" runat="server" class="inner" />
                                <div class="icon">
                                    <i class="fa fa-heartbeat" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            <div class="small-box bg-olive">
                                <div id="divMsngBHA" runat="server" class="inner" />
                                <div class="icon">
                                    <i class="fa fa-chain-broken" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<asp:UpdatePanel ID="upnlMain" runat="server">
    <ContentTemplate>
        <div class="row bg-white">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h1 class="box-title">Azimuth Audit/QC with Correction</h1>
                    </div>
                    <div class="box-body">
                        <asp:UpdateProgress ID="upgrsCorrQC" runat="server" AssociatedUpdatePanelID="upnlCorrQC" DisplayAfter="0">
                            <ProgressTemplate>
                                <div id="divCorrQCProgressbar" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                    z-index: 100;">
                                    <asp:Image ID="upgrsCorrQCImage" runat="server" CssClass="center" AlternateText="Processing ...." ForeColor="Purple" ImageAlign="Middle"
                                        ImageUrl="~/Images/Progress.gif" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:UpdatePanel runat="server" ID="upnlCorrQC">
                            <ContentTemplate>
                                <div class="panel-group" id="accordion">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h5 class="panel-title" style="text-align: left">
                                                <a id="ancOprList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlAzmCorrQC_accordion" href="#BodyContent_ctrlAzmCorrQC_divOpr">
                                                    Operator Company</a>
                                            </h5>
                                        </div>
                                        <div id="divOpr" runat="server" class="panel-collapse collapse in">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <asp:GridView runat="server" ID="gvwGlobalOptr" DataKeyNames="clntID" CssClass="mydatagrid" AllowPaging="True"
                                                        AllowSorting="True"
                                                        AutoGenerateColumns="False" EmptyDataText="No Operator Company found."
                                                        OnPageIndexChanging="gvwGlobalOptr_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                        PageSize="15"
                                                        HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow"
                                                        OnSelectedIndexChanged="gvwGlobalOptr_SelectedIndexChanged">
                                                        <Columns>
                                                            <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                            </asp:ButtonField>
                                                            <asp:BoundField DataField="clntID" Visible="False" />
                                                            <asp:BoundField DataField="clntName" HeaderText="Operator Company" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h5 class="panel-title" style="text-align: left">
                                                <a id="ancPadList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlAzmCorrQC_accordion" href="#BodyContent_ctrlAzmCorrQC_divPad">
                                                    Well Pad</a>
                                            </h5>
                                        </div>
                                        <div id="divPad" runat="server" class="panel-collapse collapse in">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <asp:GridView ID="gvwWellPads" runat="server" EmptyDataText="No Well Pad(s) found for selected Operator"
                                                        ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="wpdID"
                                                        AllowPaging="True" CssClass="mydatagrid" OnPageIndexChanging="gvwWellPads_PageIndexChanging"
                                                        OnSelectedIndexChanged="gvwWellPads_SelectedIndexChanged"
                                                        AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header"
                                                        PagerStyle-CssClass="pager"
                                                        RowStyle-CssClass="rows">
                                                        <Columns>
                                                            <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                            </asp:ButtonField>
                                                            <asp:BoundField DataField="wpdID" HeaderText="wpd" ReadOnly="True" Visible="false" />
                                                            <asp:BoundField DataField="welPadName" HeaderText="Well Pad" />
                                                            <asp:BoundField DataField="field" HeaderText="O&G Field" />
                                                            <asp:BoundField DataField="county" HeaderText="County/District/Division/Municipality" />
                                                            <asp:BoundField DataField="state" HeaderText="State/Province" />
                                                            <asp:BoundField DataField="country" HeaderText="Country/Nation" />
                                                            <asp:BoundField DataField="sreg" HeaderText="Sub-Region" />
                                                            <asp:BoundField DataField="reg" HeaderText="Major Region" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h5 class="panel-title" style="text-align: left">
                                                <a id="ancWellList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlAzmCorrQC_accordion" href="#BodyContent_ctrlAzmCorrQC_divWellList">
                                                    Well </a>
                                            </h5>
                                        </div>
                                        <div id="divWellList" runat="server" visible="false" class="panel-collapse collapse">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <asp:GridView ID="gvwWellList" runat="server" CssClass="mydatagrid" EmptyDataText="No Wells associated with the selected Job/Worker Order #"
                                                        AllowPaging="True" Width="99.6%" AutoGenerateColumns="False" DataKeyNames="welID" OnSelectedIndexChanged="gvwWellList_SelectedIndexChanged"
                                                        ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwWellList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                        PageSize="15" PagerSettings-Mode="Numeric" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Center" PagerStyle-VerticalAlign="Middle"
                                                        HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                                        <Columns>
                                                            <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                <ControlStyle CssClass="btn btn-primary text-center"></ControlStyle>
                                                            </asp:ButtonField>
                                                            <asp:BoundField DataField="welID" Visible="false" />
                                                            <asp:BoundField DataField="welName" HeaderText="Well Name" />
                                                            <asp:BoundField DataField="oprName" HeaderText="Operator Company" />
                                                            <asp:BoundField DataField="fldName" HeaderText="Field" />
                                                            <asp:BoundField DataField="cnyName" HeaderText="County/District" />
                                                            <asp:BoundField DataField="apiuwi" HeaderText="API/UWI" />
                                                            <asp:BoundField DataField="spudDate" HeaderText="Spud Date" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h5 class="panel-title" style="text-align: left">
                                                <a id="ancSection" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlAzmCorrQC_accordion" href="#BodyContent_ctrlAzmCorrQC_divSectionList">
                                                    Well Section </a>
                                            </h5>
                                        </div>
                                        <div id="divSectionList" runat="server" visible="false" class="panel-collapse collapse">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <asp:GridView ID="gvwSectionList" runat="server" CssClass="mydatagrid" EmptyDataText="No Well Section associated with the selected Well"
                                                        AllowPaging="True" Width="99.6%" AutoGenerateColumns="False" DataKeyNames="wswID" OnSelectedIndexChanged="gvwSectionList_SelectedIndexChanged"
                                                        ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwSectionList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                        PageSize="15" PagerSettings-Mode="Numeric" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Center" PagerStyle-VerticalAlign="Middle"
                                                        HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                                        <Columns>
                                                            <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                <ControlStyle CssClass="btn btn-primary text-center"></ControlStyle>
                                                            </asp:ButtonField>
                                                            <asp:BoundField DataField="wswID" Visible="false" />
                                                            <asp:BoundField DataField="wsName" HeaderText="Well Section Name" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                            <asp:BoundField DataField="rnCount" HeaderText="Run Count" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h5 class="panel-title" style="text-align: left">
                                                <a id="ancRun" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlAzmCorrQC_accordion" href="#BodyContent_ctrlAzmCorrQC_divRunList">
                                                    Run </a>
                                            </h5>
                                        </div>
                                        <div id="divRunList" runat="server" visible="false" class="panel-collapse collapse">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <asp:GridView ID="gvwRunList" runat="server" CssClass="mydatagrid" EmptyDataText="No Run associated with the selected Section"
                                                        AllowPaging="True" Width="99.6%" AutoGenerateColumns="False" DataKeyNames="runID" OnSelectedIndexChanged="gvwRunList_SelectedIndexChanged"
                                                        ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwRunList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                        PageSize="15" PagerSettings-Mode="Numeric" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Center" PagerStyle-VerticalAlign="Middle"
                                                        HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                                        <Columns>
                                                            <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                <ControlStyle CssClass="btn btn-primary text-center"></ControlStyle>
                                                            </asp:ButtonField>
                                                            <asp:BoundField DataField="runID" Visible="false" />
                                                            <asp:BoundField DataField="rnName" HeaderText="Run Name" />
                                                            <asp:BoundField DataField="rnStatus" HeaderText="Status" />
                                                            <asp:BoundField DataField="rnBHA" HeaderText="BHA Signature ID" />
                                                            <asp:BoundField DataField="rnToolcode" HeaderText="Toolcode" />
                                                            <asp:BoundField DataField="rnSQC" HeaderText="Survey Qualification Criteria" />
                                                            <asp:BoundField DataField="rnStartD" HeaderText="Start Measured Depth(m)" ItemStyle-HorizontalAlign="Right" />
                                                            <asp:BoundField DataField="rnEndD" HeaderText="End Measured Depth(m)" ItemStyle-HorizontalAlign="Right" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h5 class="panel-title" style="text-align: left">
                                                <a id="ancDE" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlAzmCorrQC_accordion" href="#BodyContent_ctrlAzmCorrQC_divDataEntryList">
                                                    Data
                                                    Entry Method </a>
                                            </h5>
                                        </div>
                                        <div id="divDataEntryList" runat="server" visible="false" class="panel-collapse collapse">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <asp:GridView ID="gvwDataEntryList" runat="server" CssClass="mydatagrid" AllowPaging="True"
                                                        AutoGenerateColumns="False" DataKeyNames="demID" OnSelectedIndexChanged="gvwDataEntryList_SelectedIndexChanged" ShowHeaderWhenEmpty="True"
                                                        AlternatingRowStyle-CssClass="AlternatingRowStyle" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                                        <Columns>
                                                            <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                <ControlStyle CssClass="btn btn-primary text-center"></ControlStyle>
                                                            </asp:ButtonField>
                                                            <asp:BoundField DataField="demID" Visible="false" />
                                                            <asp:BoundField DataField="demValue" HeaderText="Data Entry Method" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Accordian -->
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div runat="server" id="wdEnclosure" visible="false">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <h4><i runat="server" id="iWD"><span runat="server" id="spnWD"></span></i></h4>
                                        <asp:Label ID="lblWD" runat="server" Text="" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="input-group" style="width: 100%;">
                                        <asp:Button ID="btnMetaCancel" runat="server" Text="Clear Values" CssClass="btn btn-warning text-center" Visible="false"
                                            OnClick="btnMetaCancel_Click" />
                                        <asp:Button ID="btnDeleteWellData" runat="server" Text="Delete Well Data" CssClass="btn btn-warning text-center" Visible="false"
                                            OnClick="btnDeleteWellData_Click" Enabled="false" />
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
        <div id="LoadData" runat="server" class="row" visible="false">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <asp:UpdateProgress ID="uprgrsLoadData" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlLoadData">
                            <ProgressTemplate>
                                <div id="divLoadDataProgressbar" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle;
                                    text-align: center; z-index: 100;">
                                    <asp:Image ID="upgrsLoadDataImage" runat="server" CssClass="center" AlternateText="Processing ...." ForeColor="Purple" ImageAlign="Middle"
                                        ImageUrl="~/Images/Progress.gif" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:UpdatePanel runat="server" ID="upnlLoadData">
                            <ContentTemplate>
                                <div class="box-header with-border">
                                    <h1 class="box-title">Data Input</h1>
                                </div>
                                <div id="divManData" runat="server" class="box box-body" visible="false">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:TextBox ID="txtQCManualProcess" runat="server" Width="100%" CssClass="form-control" placeholder="Copy/Paste Raw Data Survey line" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:Label runat="server" ID="lbl_QCManualFormat" Text="Format: Enabled, Time, Depth (m)*, Solution #, Gx (g)*, Gy (g)*, Gz (g)*, Bx (nT)*, By (nT)*, Bz (nT)*, Inc (deg), Azm (deg), GTI, BTI, DIP (deg), BTotal (nT), G Axes, Type ( * required ) "
                                                ForeColor="Gray" Font-Size="Small" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div runat="server" id="txtDataEnclosure" visible="false">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                <h4><i runat="server" id="iTXTDMessage"><span runat="server" id="txtDHeader"></span></i></h4>
                                                <asp:Label runat="server" ID="lblManual" Text="" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div id="divManualDataButtons" class="input-group" style="width: 100%;">
                                            <asp:Button ID="btnQCManualUpload" runat="server" Text="Process Survey" CssClass="btn btn-info text-center" Width="10%"
                                                OnClick="btnQCManualUpload_Click" />
                                            <asp:Button ID="btnCancelManualUpload" runat="server" Text="Cancel" CssClass="btn btn-warning text-center" Width="10%" OnClick="btnCancelManualUpload_Click"
                                                Visible="false" />
                                        </div>
                                    </div>
                                </div>
                                <div id="divFileData" runat="server" class="box box-body" visible="false">
                                    <div id="divFileUploadControlRow" class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:FileUpload ID="FileUploadControl" runat="server" Font-Size="Large" placeholder="Select Survey file to Upload" />
                                            <div runat="server" id="filDEnclosure" visible="false">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                <h4><i runat="server" id="ifilDMessage"><span runat="server" id="spnFILED"></span></i></h4>
                                                <asp:Label ID="StatusLabel" runat="server" Text="" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="input-group" style="width: 100%;">
                                            <asp:Button ID="btn_FileUpload" runat="server" Text="Upload Survey File" CssClass="btn btn-info text-center" Width="10%"
                                                OnClick="btn_FileUpload_Click" />
                                            <asp:Button ID="btnCancelFileUpload" runat="server" Text="Cancel" CssClass="btn btn-warning text-center" Width="10%" OnClick="btnCancelFileUpload_Click" />
                                        </div>
                                    </div>
                                </div>
                                <div id="divListBoxUploadedData" class="box box-body" runat="server" visible="false">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div runat="server" id="rawDEnclosure" visible="false">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                            <h4><i runat="server" id="iRDE"><span runat="server" id="spnRDE"></span></i></h4>
                                            <asp:Label ID="FileUploadStatusLabel" runat="server" Text="" />
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:GridView ID="gvwViewManualDisplay" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="drID"
                                                SelectedRowStyle-CssClass="selectedrow" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                EmptyDataText="No Data Uploaded" AllowPaging="True" AllowSorting="True" PagerStyle-CssClass="pager" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                OnPageIndexChanging="gvwViewManualDisplay_PageIndexChanging">
                                                <Columns>
                                                    <asp:BoundField DataField="drID" Visible="False" />
                                                    <asp:BoundField DataField="Enabled" HeaderText="Enabled" />
                                                    <asp:BoundField DataField="Timestamp" HeaderText="Timestamp" />
                                                    <asp:BoundField DataField="Depth" HeaderText="Depth (m)" />
                                                    <asp:BoundField DataField="Solution" HeaderText="Solution" />
                                                    <asp:BoundField DataField="Gx" HeaderText="Gx (g)" />
                                                    <asp:BoundField DataField="Gy" HeaderText="Gy (g)" />
                                                    <asp:BoundField DataField="Gz" HeaderText="Gz (g)" />
                                                    <asp:BoundField DataField="Bx" HeaderText="Bx (nT)" />
                                                    <asp:BoundField DataField="By" HeaderText="By (nT)" />
                                                    <asp:BoundField DataField="Bz" HeaderText="Bz (nT)" />
                                                    <asp:BoundField DataField="Inc" HeaderText="Inclination" />
                                                    <asp:BoundField DataField="Azm" HeaderText="Azimuth" />
                                                    <asp:BoundField DataField="GTI" HeaderText="GTI" />
                                                    <asp:BoundField DataField="BTI" HeaderText="BTI" />
                                                    <asp:BoundField DataField="Dip" HeaderText="Dip" />
                                                    <asp:BoundField DataField="BTotal" HeaderText="B-Total" />
                                                    <asp:BoundField DataField="GAxes" HeaderText="GAxes" />
                                                    <asp:BoundField DataField="Type" HeaderText="Type" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                        <div id="divSensors" runat="server" class="box box-default">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="text-align: right; align-content: center; vertical-align: middle;">
                                                    <asp:Label ID="lbl_Flip" runat="server" CssClass="label label-default" Font-Bold="true" Font-Size="Larger" Text="Select Sensor(s) for data transformation" />
                                                </div>
                                                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9" style="text-align: left; align-content: center; vertical-align: middle;">
                                                    <div id="Div1" runat="server" class="checkbox" style="">
                                                        <asp:CheckBox ID="chk_NoSignTransformation" runat="server" Text="None" CssClass="checkbox-inline fa-check-square" CausesValidation="True"
                                                            Checked="True"
                                                            AutoPostBack="True" OnCheckedChanged="chk_NoSignTransformation_CheckedChanged" />
                                                        <asp:CheckBox ID="chk_FlipGx" runat="server" Text="Gx" CssClass="checkbox-inline fa-check-square" Checked="false" Enabled="False" />
                                                        <asp:CheckBox ID="chk_FlipGy" runat="server" Text="Gy" CssClass="checkbox-inline fa-check-square" Checked="false" Enabled="False" />
                                                        <asp:CheckBox ID="chk_FlipGz" runat="server" Text="Gz" CssClass="checkbox-inline fa-check-square" Checked="false" Enabled="False" />
                                                        <asp:CheckBox ID="chk_FlipBx" runat="server" Text="Bx" CssClass="checkbox-inline fa-check-square" Checked="false" Enabled="False" />
                                                        <asp:CheckBox ID="chk_FlipBy" runat="server" Text="By" CssClass="checkbox-inline fa-check-square" Checked="false" Enabled="False" />
                                                        <asp:CheckBox ID="chk_FlipBz" runat="server" Text="Bz" CssClass="checkbox-inline fa-check-square" Checked="false" Enabled="False" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group" style="width: 100%;">
                                                <asp:Button ID="btnProcessRawData" runat="server" Text="Process Survey(s)" Width="10%" CssClass="btn btn-info text-center"
                                                    OnClick="btnProcessRawData_Click" Visible="False" />
                                                <asp:Button ID="btnProcessSrvyCancel" runat="server" Text="Cancel Processing" Width="10%" CssClass="btn btn-warning text-center"
                                                    Visible="false" OnClick="btnProcessSrvyCancel_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnQCManualUpload" />
                                <asp:PostBackTrigger ControlID="btn_FileUpload" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
        <div id="divReadyData" runat="server" class="row" visible="false">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <asp:UpdateProgress ID="uprgrsReadyData" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlReadyData">
                            <ProgressTemplate>
                                <div id="divReadyDataProgressbar" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle;
                                    text-align: center; z-index: 100;">
                                    <asp:Image ID="upgrsReadyDataImage" runat="server" CssClass="center" AlternateText="Processing ...." ForeColor="Purple" ImageAlign="Middle"
                                        ImageUrl="~/Images/Progress.gif" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:UpdatePanel runat="server" ID="upnlReadyData">
                            <ContentTemplate>
                                <div class="box-header with-border">
                                    <h1 class="box-title">IFR Values</h1>
                                </div>
                                <div class="box box-body">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div id="divIFRHeading" runat="server" style="align-content: center;">
                                                    <div class="col-sm-2 col-md-2 col-lg-2">
                                                        &nbsp;
                                                    </div>
                                                    <div class="col-sm-1 col-md-1 col-lg-1">
                                                        <asp:Label ID="lblDip" runat="server" CssClass="label label-info" Width="100%" Font-Size="Large" Text="IFR Dip" />
                                                    </div>
                                                    <div class="col-sm-1 col-md-1 col-lg-1">
                                                        <asp:Label ID="lblDec" runat="server" CssClass="label label-info" Width="100%" Font-Size="Large" Text="IFR MDec." />
                                                    </div>
                                                    <div class="col-sm-2 col-md-2 col-lg-2">
                                                        <asp:Label ID="lblDecO" runat="server" CssClass="label label-info" Width="100%" Font-Size="Large" Text="MDec. Orientation" />
                                                    </div>
                                                    <div class="col-sm-1 col-md-1 col-lg-1">
                                                        <asp:Label ID="lblBT" runat="server" CssClass="label label-info" Width="100%" Font-Size="Large" Text="IFR B-Total" />
                                                    </div>
                                                    <div class="col-sm-1 col-md-1 col-lg-1">
                                                        <asp:Label ID="lblGT" runat="server" CssClass="label label-info" Width="100%" Font-Size="Large" Text="IFR G-Total" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div id="divWellIFR" runat="server" style="align-content: center;">
                                                    <div class="col-sm-2 col-md-2 col-lg-2 label label-default form-group text-right">
                                                        <asp:RadioButton ID="rdbWell" runat="server" Checked="true" CssClass="flat-red" OnCheckedChanged="rdbWell_CheckedChanged"
                                                            AutoPostBack="True" />
                                                        <asp:Label ID="lblWellIFRTitle" runat="server" Font-Size="Large" class="product-title" Text="Well IFR Ref. Mags" />
                                                    </div>
                                                    <div class="col-sm-1 col-md-1 col-lg-1">
                                                        <asp:TextBox ID="txtWellDip" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                    </div>
                                                    <div class="col-sm-1 col-md-1 col-lg-1">
                                                        <asp:TextBox ID="txtWellDec" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                    </div>
                                                    <div class="col-sm-2 col-md-2 col-lg-2">
                                                        <asp:DropDownList ID="ddlWellDecOrient" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true" Enabled="false">                                                        
                                                        </asp:DropDownList>                                                        
                                                    </div>
                                                    <div class="col-sm-1 col-md-1 col-lg-1">
                                                        <asp:TextBox ID="txtWellBTotal" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                    </div>
                                                    <div class="col-sm-1 col-md-1 col-lg-1">
                                                        <asp:TextBox ID="txtWellGTotal" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div id="divNOAAIFR" runat="server">
                                                    <div class="col-sm-2 col-md-2 col-lg-2 label label-default form-group text-right">
                                                        <asp:RadioButton ID="rdbNOAA" runat="server" Checked="false" CssClass="flat-red" OnCheckedChanged="rdbNOAA_CheckedChanged"
                                                            AutoPostBack="True" />
                                                        <asp:Label ID="lblNOAAIFRTitle" runat="server" Font-Size="Large" class="product-title" Text="NOAA Mag Field Calc." />
                                                    </div>
                                                    <div class="col-sm-1 col-md-1 col-lg-1">
                                                        <asp:TextBox ID="txtNOAADip" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                    </div>
                                                    <div class="col-sm-1 col-md-1 col-lg-1">
                                                        <asp:TextBox ID="txtNOAADec" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                    </div>
                                                    <div class="col-sm-2 col-md-2 col-lg-2">
                                                        <asp:DropDownList ID="ddlNOAAOrientation" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true" Enabled="false">
                                                            <asp:ListItem Value="0" Text="--- Select Orientation ---" />
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="col-sm-1 col-md-1 col-lg-1">
                                                        <asp:TextBox ID="txtNOAABTotal" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                    </div>
                                                    <div class="col-sm-1 col-md-1 col-lg-1">
                                                        <asp:TextBox ID="txtNOAAGTotal" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                    </div>
                                                    <div class="col-sm-2 col-md-2 col-lg-2">
                                                        <a id="ancNOAA" runat="server" href="https://www.ngdc.noaa.gov/geomag-web/?model=wmm#igrfwmm" target="_blank" style="font-size: large;
                                                            text-decoration: underline">NOAA Calc.</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div id="divBGSIFR" runat="server">
                                                    <div class="col-sm-2 col-md-2 col-lg-2 label label-default form-group text-right">
                                                        <asp:RadioButton ID="rdbBGS" runat="server" Checked="false" CssClass="flat-red" OnCheckedChanged="rdbBGS_CheckedChanged"
                                                            AutoPostBack="True" />
                                                        <asp:Label ID="lblBGSIFRTitle" runat="server" Font-Size="Large" CssClass="product-title" Text="World Mag Model Calc." />
                                                    </div>
                                                    <div class="col-sm-1 col-md-1 col-lg-1">
                                                        <asp:TextBox ID="txtBGSDip" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                    </div>
                                                    <div class="col-sm-1 col-md-1 col-lg-1">
                                                        <asp:TextBox ID="txtBGSDec" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                    </div>
                                                    <div class="col-sm-2 col-md-2 col-lg-2">
                                                        <asp:DropDownList ID="ddlBGSOrientation" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true" Enabled="false">
                                                            <asp:ListItem Value="0" Text="--- Select Orientation ---" />
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="col-sm-1 col-md-1 col-lg-1">
                                                        <asp:TextBox ID="txtBGSBTotal" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                    </div>
                                                    <div class="col-sm-1 col-md-1 col-lg-1">
                                                        <asp:TextBox ID="txtBGSGTotal" runat="server" CssClass="form-control text-right" Text="" Enabled="false" placeholder="---" />
                                                    </div>
                                                    <div class="col-sm-2 col-md-2 col-lg-2">
                                                        <a id="ancBGS" runat="server" href="http://www.geomag.bgs.ac.uk/data_service/models_compass/wmm_calc.html" target="_blank"
                                                            style="font-size: large; text-decoration: underline">BGS Calculator</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <asp:GridView ID="gvwReadyData" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="drID"
                                                    SelectedRowStyle-CssClass="selectedrow" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                    EmptyDataText="No Data Uploaded" AllowPaging="True" AllowSorting="True" PagerStyle-CssClass="pager" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                    OnPageIndexChanging="gvwReadyData_PageIndexChanging" OnSelectedIndexChanged="gvwReadyData_SelectedIndexChanged">
                                                    <Columns>
                                                        <asp:BoundField DataField="drID" Visible="False" />
                                                        <asp:BoundField DataField="Enabled" HeaderText="Enabled" />
                                                        <asp:BoundField DataField="Timestamp" HeaderText="Timestamp" />
                                                        <asp:BoundField DataField="Depth" HeaderText="Depth (m)" />
                                                        <asp:BoundField DataField="Solution" HeaderText="Solution" />
                                                        <asp:BoundField DataField="Gx" HeaderText="Gx (g)" />
                                                        <asp:BoundField DataField="Gy" HeaderText="Gy (g)" />
                                                        <asp:BoundField DataField="Gz" HeaderText="Gz (g)" />
                                                        <asp:BoundField DataField="Bx" HeaderText="Bx (nT)" />
                                                        <asp:BoundField DataField="By" HeaderText="By (nT)" />
                                                        <asp:BoundField DataField="Bz" HeaderText="Bz (nT)" />
                                                        <asp:BoundField DataField="Inc" HeaderText="Inclination°" />
                                                        <asp:BoundField DataField="Azm" HeaderText="Azimuth°" />
                                                        <asp:BoundField DataField="GTI" HeaderText="GTI" />
                                                        <asp:BoundField DataField="BTI" HeaderText="BTI" />
                                                        <asp:BoundField DataField="Dip" HeaderText="Dip°" />
                                                        <asp:BoundField DataField="BTotal" HeaderText="B-Total" />
                                                        <asp:BoundField DataField="GAxes" HeaderText="GAxes" />
                                                        <asp:BoundField DataField="Type" HeaderText="Type" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group" style="width: 100%;">
                                                <asp:Button ID="btnProcessReadyData" runat="server" CssClass="btn btn-info text-center" Width="15%" Text="Process Data" OnClick="btnProcessReadyData_Click" />
                                                <asp:Button ID="btnCancelReadyData" runat="server" CssClass="btn btn-warning text-center" Width="15%" Text="Clear Data" OnClick="btnCancelReadyData_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
        <div id="div5Surveys" runat="server" class="row" visible="false">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <asp:UpdateProgress ID="uprgrs5Surveys" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnl5Surveys">
                            <ProgressTemplate>
                                <div id="div5SurveysProgressbar" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle;
                                    text-align: center; z-index: 100;">
                                    <asp:Image ID="upgrs5SurveysImage" runat="server" CssClass="center" AlternateText="Processing ...." ForeColor="Purple" ImageAlign="Middle"
                                        ImageUrl="~/Images/Progress.gif" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:UpdatePanel runat="server" ID="upnl5Surveys">
                            <ContentTemplate>
                                <div class="box-header with-border">
                                    <h1 class="box-title">Input Data Row Count</h1>
                                </div>
                                <div class="box box-body">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div runat="server" id="div5Enclosure" visible="false">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                            <h4><i runat="server" id="i5"><span runat="server" id="spn5"></span></i></h4>
                                            <asp:Label ID="lbl5" runat="server" Text="" />
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="input-group" style="width: 100%;">
                                            <asp:Button ID="btnProceed5" runat="server" CssClass="btn btn-info text-center" Width="15%" Text="Analyze Data" OnClick="btnProceed5_Click" />
                                            <asp:Button ID="btnProceed5Clear" runat="server" CssClass="btn btn-warning text-center" Width="15%" Text="Clear Values"
                                                OnClick="btnProceed5Clear_Click" CausesValidation="false" />
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnProceed5" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
        <div id="divQCData" runat="server" class="row" visible="false">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <asp:UpdateProgress ID="uprgrsQCData" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlQCData">
                            <ProgressTemplate>
                                <div id="divQCDataProgressbar" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                    z-index: 100;">
                                    <asp:Image ID="upgrsQCDataImage" runat="server" CssClass="center" AlternateText="Processing ...." ForeColor="Purple" ImageAlign="Middle"
                                        ImageUrl="~/Images/Progress.gif" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:UpdatePanel runat="server" ID="upnlQCData">
                            <ContentTemplate>
                                <div class="box-header with-border">
                                    <h1 class="box-title">Select Data for Analysis</h1>
                                </div>
                                <div class="box box-body">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="divIncAzmCharts" runat="server" style="height: 750px;"></div>
                                            <asp:Literal ID="ltrIncAzmCharts" runat="server"></asp:Literal>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="divQCStdDev" runat="server" class="box box-info box-body">
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                        <asp:Label ID="lblLCDev" runat="server" CssClass="label label-default" Text="Sample Standard-Deviation (Long-Collar Azimuth°)" />
                                                    </div>
                                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                                        <asp:Label ID="lblLCDevVal" runat="server" Text="" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                        <asp:Label ID="lblSCDev" runat="server" CssClass="label label-default" Text="Sample Standard-Deviation (Short-Collar Azimuth°)" />
                                                    </div>
                                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                                        <asp:Label ID="lblSCDevVal" runat="server" Text="" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:GridView ID="gvwInputData" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="srvRID"
                                                SelectedRowStyle-CssClass="selectedrow" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                EmptyDataText="No Data Uploaded" AllowPaging="True" AllowSorting="True" PageSize="500" PagerStyle-CssClass="pager" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                OnRowDataBound="gvwInputData_RowDataBound" OnPageIndexChanging="gvwInputData_PageIndexChanging">
                                                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                                <Columns>
                                                    <asp:BoundField DataField="srvRID" HeaderText="Survey No" />
                                                    <asp:BoundField DataField="Depth" HeaderText="M. Depth" DataFormatString="{0: #.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="LCAzm" HeaderText="Long-Collar Azimuth°" DataFormatString="{0: #0.0000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="SCAzm" HeaderText="Short-Collar Azimuth°" DataFormatString="{0: #0.0000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Inc" HeaderText="Inclination°" DataFormatString="{0: #0.0000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="valDip" HeaderText="Dip°" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="valBTotal" HeaderText="B-Total (nT)" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="valGTotal" HeaderText="G-Total (g)" DataFormatString="{0: #0.00000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                </Columns>
                                                <HeaderStyle CssClass="header" />
                                                <PagerStyle CssClass="pager" />
                                                <RowStyle CssClass="rows" />
                                                <SelectedRowStyle CssClass="selectedrow" />
                                            </asp:GridView>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group" style="width: 100%;">
                                                <asp:Button ID="btnAnalyze" runat="server" CssClass="btn btn-success text-center" Width="15%" Text="Get Solution" OnClick="btnAnalyze_Click" />
                                                <asp:Button ID="btnCancelAnalysis" runat="server" CssClass="btn btn-warning text-center" Width="15%" Text="Clear Values"
                                                    OnClick="btnCancelAnalysis_Click" CausesValidation="false" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnAnalyze" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
        <div id="divSol" runat="server" class="row" visible="false">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <asp:UpdateProgress ID="uprgrsSol" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlSol">
                            <ProgressTemplate>
                                <div id="divSolProgressbar" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                    z-index: 100;">
                                    <asp:Image ID="imgSolImage" runat="server" CssClass="center" AlternateText="Processing ...." ForeColor="Purple" ImageAlign="Middle"
                                        ImageUrl="~/Images/Progress.gif" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:UpdatePanel runat="server" ID="upnlSol">
                            <ContentTemplate>
                                <div class="box-header with-border">
                                    <h1 class="box-title">Select Data for Analysis</h1>
                                </div>
                                <div class="box box-body">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="divSolChart" runat="server" style="height: 750px;"></div>
                                            <asp:Literal ID="ltrSolChart" runat="server"></asp:Literal>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="divSolStdDev" runat="server" class="box box-info box-body">
                                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                    <asp:Label ID="lblSolLC" runat="server" CssClass="label label-default" Text="Sample Standard-Deviation (Long-Collar Azimuth°)" />
                                                </div>
                                                <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
                                                    <asp:Label ID="lblSolLCVal" runat="server" Text="" />
                                                </div>
                                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                    <asp:Label ID="lblSolSC" runat="server" CssClass="label label-default" Text="Sample Standard-Deviation (Short-Collar Azimuth°)" />
                                                </div>
                                                <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
                                                    <asp:Label ID="lblSolSCVal" runat="server" Text="" />
                                                </div>
                                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                    <asp:Label ID="lblSolCR" runat="server" CssClass="label label-default" Text="Sample Standard-Deviation (Corrected Azimuth°)" />
                                                </div>
                                                <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
                                                    <asp:Label ID="lblSolCRVal" runat="server" Text="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:GridView ID="gvwSol" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="srvRID"
                                                SelectedRowStyle-CssClass="selectedrow" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                EmptyDataText="No Data Uploaded" AllowPaging="True" AllowSorting="True" PageSize="500" PagerStyle-CssClass="pager" AlternatingRowStyle-CssClass="AlternatingRowStyle">
                                                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                                <Columns>
                                                    <asp:BoundField DataField="srvRID" HeaderText="Survey No" />
                                                    <asp:BoundField DataField="Depth" HeaderText="M. Depth" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Gx" HeaderText="Gx" DataFormatString="{0: #0.00000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Gy" HeaderText="Gy" DataFormatString="{0: #0.00000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Gz" HeaderText="Gz" DataFormatString="{0: #0.00000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Bx" HeaderText="Bx" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="By" HeaderText="By" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Bz" HeaderText="Bz" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="LCAzm" HeaderText="Long-Collar Azimuth°" DataFormatString="{0: #0.0000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="SCAzm" HeaderText="Short-Collar Azimuth°" DataFormatString="{0: #0.0000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="CrrAzm" HeaderText="Corrected Azimuth°" DataFormatString="{0: #0.0000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="valInc" HeaderText="Inclination°" DataFormatString="{0: #0.0000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="valDip" HeaderText="Dip°" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="valBTotal" HeaderText="B-Total (nT)" DataFormatString="{0: #0.00}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="valGTotal" HeaderText="G-Total (g)" DataFormatString="{0: #0.00000}">
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:BoundField>
                                                </Columns>
                                                <HeaderStyle CssClass="header" />
                                                <PagerStyle CssClass="pager" />
                                                <RowStyle CssClass="rows" />
                                                <SelectedRowStyle CssClass="selectedrow" />
                                            </asp:GridView>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div runat="server" id="divSolEnclosure" visible="false">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                <h4><i runat="server" id="iSol"><span runat="server" id="spnSol"></span></i></h4>
                                                <asp:Label ID="lblSol" runat="server" Text="" />
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group" style="width: 100%;">
                                                <asp:Button ID="btnSolAccept" runat="server" CssClass="btn btn-success text-center" Width="15%" Text="Accept Solution" OnClick="btnSolAccept_Click" />
                                                <asp:Button ID="btnSolRej" runat="server" CssClass="btn btn-success text-center" Width="15%" Text="Re-Calculate Solution" OnClick="btnSolRej_Click" />
                                                <asp:Button ID="btnSolCancel" runat="server" CssClass="btn btn-warning text-center" Width="15%" Text="Clear Values" CausesValidation="false"
                                                    OnClick="btnSolCancel_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>   
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnSolAccept" />
                            </Triggers>                         
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
        <div id="divQC" runat="server" class="row" visible="false">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box box-primary">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="box-body">
                            <div id="charts" runat="server">
                                <h3>Analysis Chart </h3>
                                <div id="divECharts" runat="server" style="height: 700px;"></div>
                                <h3>Azimuth Analysis Chart </h3>
                                <div id="divEChartsAzim" runat="server" style="height: 450px;"></div>
                                <h3>Analysis Chart - 3D </h3>
                                <div id="divECharts3D" runat="server" style="height: 700px;"></div>
                                <asp:Literal ID="ltrECharts" runat="server"></asp:Literal>
                                <asp:Literal ID="ltrEChartsAzim" runat="server"></asp:Literal>
                                <asp:Literal ID="ltrECharts3D" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-body">
                        <asp:UpdateProgress ID="uprgrsQC" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlQC">
                            <ProgressTemplate>
                                <div id="divQCProgressbar" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                    z-index: 100;">
                                    <asp:Image ID="imgQCImage" runat="server" CssClass="center" AlternateText="Processing ...." ForeColor="Purple" ImageAlign="Middle"
                                        ImageUrl="~/Images/Progress.gif" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:UpdatePanel runat="server" ID="upnlQC">
                            <ContentTemplate>
                                <div class="box-header with-border">
                                    <h1 class="box-title">SMARTs Audit/QC</h1>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <asp:GridView ID="gvwRQCResults" runat="server" CssClass="mydatagrid" AllowPaging="True"
                                        AllowSorting="True" PageSize="15" AutoGenerateColumns="False" DataKeyNames="resID" SelectedRowStyle-CssClass="selectedrow"
                                        PagerStyle-CssClass="pager" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                        OnSelectedIndexChanged="gvwRQCResults_SelectedIndexChanged" OnPageIndexChanging="gvwRQCResults_PageIndexChanging">
                                        <Columns>
                                            <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                <ControlStyle CssClass="btn btn-primary text-center"></ControlStyle>
                                            </asp:ButtonField>
                                            <asp:BoundField DataField="resID" HeaderText="resID" Visible="False" />
                                            <asp:BoundField DataField="srvyID" HeaderText="Survey ID" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%" />
                                            <asp:BoundField DataField="Depth" HeaderText="Survey Depth" ItemStyle-Width="10%" />
                                            <asp:BoundField DataField="taSt" HeaderText="SMARTs Analysis" ItemStyle-Width="80%" ItemStyle-HorizontalAlign="Left" />
                                            <asp:BoundField DataField="chSt" Visible="false" />
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Label ID="lblMaxRunTieIn" runat="server" Text="End-Depth Tie-In" BackColor="LightBlue" Width="99.6%"
                                        ForeColor="Purple" Font-Size="Large" />
                                    <asp:GridView ID="gvwMaxRunTieIn" runat="server" CssClass="mydatagrid" AllowPaging="True"
                                        AutoGenerateColumns="False" DataKeyNames="tisID" ShowHeaderWhenEmpty="True" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                        HeaderStyle-CssClass="header">
                                        <Columns>
                                            <asp:BoundField DataField="tisID" Visible="false" />
                                            <asp:BoundField DataField="title" />
                                            <asp:BoundField HeaderText="TVD" DataField="tvd" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #.00}" />
                                            <asp:BoundField HeaderText="North" DataField="north" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #.0000}" />
                                            <asp:BoundField HeaderText="East" DataField="east" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #.0000}" />
                                            <asp:BoundField HeaderText="VS" DataField="vs" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #.00}" />
                                            <asp:BoundField HeaderText="Subsea" DataField="subsea" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #.00}" />
                                            <asp:BoundField HeaderText="DLS" DataField="dls" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #.00}" />
                                            <asp:BoundField HeaderText="Build Rate" DataField="brate" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #.00}" />
                                            <asp:BoundField HeaderText="Turn Rate" DataField="trate" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #.00}" />
                                            <asp:BoundField HeaderText="Northing" DataField="northing" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #.0000}" />
                                            <asp:BoundField HeaderText="Easting" DataField="easting" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #.0000}" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div id="divSQCGridRow" class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:Label ID="lblQCResSQC" runat="server" Text="Survey Qualification Criterion" BackColor="LightBlue" Width="99.6%"
                                                ForeColor="Purple" Visible="False" Font-Size="Large" />
                                            <asp:GridView ID="gvwResSQC" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="sqcID"
                                                Visible="False" SelectedRowStyle-BackColor="#CCFFCC" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle">
                                                <Columns>
                                                    <asp:BoundField DataField="sqcID" Visible="false" />
                                                    <asp:BoundField DataField="sqcName" HeaderText="Criterion Name" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="24%" />
                                                    <asp:BoundField DataField="sqcDip" HeaderText="Dip Tolerance" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="24%" />
                                                    <asp:BoundField DataField="sqcBT" HeaderText="B-Total Tolerance" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="24%" />
                                                    <asp:BoundField DataField="sqcGT" HeaderText="G-Total Tolerance" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="24%" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <div id="divMetaDataGridRow" class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:Label ID="lblQCResMetaData" runat="server" Text="Survey Reference Values" BackColor="LightBlue" Width="99.6%"
                                                ForeColor="Purple" Visible="False" Font-Size="Large" />
                                            <asp:GridView ID="gvwRefVals" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="rfmgID"
                                                Visible="False" SelectedRowStyle-BackColor="#CCFFCC" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle">
                                                <Columns>
                                                    <asp:BoundField DataField="rfmgID" HeaderText="rfmgID" Visible="False" />
                                                    <asp:BoundField DataField="mdec" HeaderText="Magnetic Declination°" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="24%" />
                                                    <asp:BoundField DataField="dip" HeaderText="Dip°" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="24%" />
                                                    <asp:BoundField DataField="bTotal" HeaderText="B-Total" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="24%" />
                                                    <asp:BoundField DataField="gTotal" HeaderText="G-Total" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="24%" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <div id="divParametersGridRow" class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:Label ID="lblQCResParameters" runat="server" Text="Survey Parameters" BackColor="LightBlue" Width="99.6%"
                                                ForeColor="Purple" Visible="False" Font-Size="Large" />
                                            <asp:GridView ID="gvwQCResParameters" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                DataKeyNames="resID"
                                                SelectedRowStyle-BackColor="#CCFFCC" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle">
                                                <Columns>
                                                    <asp:BoundField HeaderText="resID" Visible="false" DataField="resID" />
                                                    <asp:BoundField DataField="inc" HeaderText="Inclination°" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="14%" />
                                                    <asp:BoundField DataField="azm" HeaderText="Azimuth° (Grid)" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="14%" ControlStyle-BackColor="#999966" />
                                                    <asp:BoundField DataField="dip" HeaderText="Dip°" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="14%" />
                                                    <asp:BoundField DataField="bTotal" HeaderText="B-Total (nT)" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="14%" />
                                                    <asp:BoundField DataField="gTotal" HeaderText="G-Total (g)" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="14%" />
                                                    <asp:BoundField DataField="gtf" HeaderText="GTF°" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="14%" />
                                                    <asp:BoundField DataField="mtf" HeaderText="MTF°" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="14%" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <div id="divQualifierGridRow" class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:Label ID="lblRQCQualifiers" runat="server" Text="Survey Qualifiers" BackColor="LightBlue" Width="99.6%"
                                                ForeColor="Purple" Visible="False" Font-Size="Large" />
                                            <asp:GridView ID="gvwRQCQualifiers" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                DataKeyNames="resID"
                                                SelectedRowStyle-BackColor="#CCFFCC" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                OnRowDataBound="gvwRQCQualifiers_RowDataBound">
                                                <Columns>
                                                    <asp:BoundField DataField="resID" HeaderText="resID" Visible="false" />
                                                    <asp:BoundField DataField="DW" HeaderText="Dip Qualifier" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" />
                                                    <asp:BoundField DataField="BC" HeaderText="ΔDip°" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" />
                                                    <asp:BoundField DataField="DY" HeaderText="B-Total Qualifier" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" />
                                                    <asp:BoundField DataField="CD" HeaderText="ΔB-Total (nT)" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" />
                                                    <asp:BoundField DataField="EA" HeaderText="G-Total Qualifier" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" />
                                                    <asp:BoundField DataField="CG" HeaderText="ΔG-Total (g)" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" />
                                                    <asp:BoundField DataField="AO" HeaderText="Corrected Azimuth° (Grid)" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" />
                                                    <asp:BoundField DataField="AP" HeaderText="ΔAzimuth° Error" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" />
                                                    <asp:BoundField DataField="DD" HeaderText="Short Collar Azimuth°" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" />
                                                    <asp:BoundField DataField="DE" HeaderText="Δ( SCAZ - LCAZ )°" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" />
                                                    <asp:BoundField DataField="HV" HeaderText="No Go Zone" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <div id="divBHAGridRow" class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:Label ID="lblRQCBHA" runat="server" Text="BHA Analysis" BackColor="LightBlue" Width="99.6%"
                                                ForeColor="Purple" Visible="False" Font-Size="Large" />
                                            <asp:GridView ID="gvwRQCBHA" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                DataKeyNames="resID"
                                                SelectedRowStyle-BackColor="#CCFFCC" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                OnRowDataBound="gvwRQCBHA_RowDataBound">
                                                <Columns>
                                                    <asp:BoundField HeaderText="resID" Visible="false" DataField="resID" />
                                                    <asp:BoundField HeaderText="Azimuth°" DataField="azmBHA" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11.1%" />
                                                    <asp:BoundField HeaderText="ΔAzimuth° BHA" DataField="colGE" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11.1%" />
                                                    <asp:BoundField HeaderText="ΔAzimuth° Motor" DataField="colEI" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11.1%" />
                                                    <asp:BoundField HeaderText="ΔAzimuth° Drill Collar" DataField="colEJ" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11.1%" />
                                                    <asp:BoundField DataField="colEK" HeaderText="BHA Non-Mag Space" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11.1%" />
                                                    <asp:BoundField HeaderText="ΔAzimuth° BHA (Gaussmeter)" DataField="colEL" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11.1%" />
                                                    <asp:BoundField HeaderText="ΔAzimuth° Motor (Gaussmeter)" DataField="colEM" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11.1%" />
                                                    <asp:BoundField HeaderText="ΔAzimuth° Drill Collar (Downhole)" DataField="colEN" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11.1%" />
                                                    <asp:BoundField DataField="colEO" HeaderText="BHA Non-Mag. Space (Gaussmeter)" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11.1%" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <div id="divNMSCalcGridRow" class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:Label ID="lblNMSCalc" runat="server" Text="Non-Mag Space Calculations" BackColor="LightBlue" Width="99.6%"
                                                ForeColor="Purple" Visible="False" Font-Size="Large" />
                                            <asp:GridView ID="gvwNMSCalc" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                DataKeyNames="resID"
                                                SelectedRowStyle-BackColor="#CCFFCC" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle">
                                                <Columns>
                                                    <asp:BoundField HeaderText="resID" Visible="false" DataField="resID" />
                                                    <asp:BoundField HeaderText="Std. DC Interference" DataField="D34" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                    <asp:BoundField HeaderText="Std. Mtr. Interference" DataField="D35" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                    <asp:BoundField HeaderText="Total BHA Interference" DataField="D37" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                    <asp:BoundField HeaderText="ΔLCAZ° Error (BHA)" DataField="D38" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                    <asp:BoundField HeaderText="Bit-To-Sensor Distance" DataField="D39" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                    <asp:BoundField HeaderText="Gaussmeter DC Interference" DataField="G34" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                    <asp:BoundField HeaderText="Gaussmeter Mtr. Interference" DataField="G35" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                    <asp:BoundField HeaderText="Gaussmeter Total BHA Interference" DataField="G37" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                    <asp:BoundField HeaderText="Guassmeter ΔLCAZ Error° (BHA)" DataField="G38" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <div id="divMinMaxGridRow" class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:Label ID="lblRQCMinMax" runat="server" Text="Min-Max" BackColor="LightBlue" Width="99.6%"
                                                ForeColor="Purple" Visible="False" Font-Size="Large" />
                                            <asp:GridView ID="gvwRQCMinMax" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                DataKeyNames="resID"
                                                SelectedRowStyle-BackColor="#CCFFCC" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle">
                                                <Columns>
                                                    <asp:BoundField HeaderText="resID" Visible="false" DataField="resID" />
                                                    <asp:BoundField HeaderText="Min. Dip" DataField="EU" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                    <asp:BoundField HeaderText="Max. Dip" DataField="EV" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                    <asp:BoundField HeaderText="Dip Spread" DataField="EW" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                    <asp:BoundField HeaderText="Min. B-Total" DataField="EX" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                    <asp:BoundField HeaderText="Max. B-Total" DataField="EY" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                    <asp:BoundField HeaderText="B-Total Spread" DataField="EZ" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                    <asp:BoundField HeaderText="Min. G-Total" DataField="FA" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                    <asp:BoundField HeaderText="Max. G-Total" DataField="FB" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                    <asp:BoundField HeaderText="G-Total Spread" DataField="FC" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <div id="divTISRow" class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:Label ID="lblTieInSurvey" runat="server" Text="Tie-In Survey" BackColor="LightBlue" Width="99.6%"
                                                ForeColor="Purple" Visible="False" Font-Size="Large" />
                                            <asp:GridView ID="gvwTieInSurvey" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                DataKeyNames="tisID" SelectedRowStyle-BackColor="#CCFFCC" HeaderStyle-CssClass="header" ShowHeaderWhenEmpty="true"
                                                RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle" EmptyDataText="No Tie-In Surveys found for selected depth">
                                                <Columns>
                                                    <asp:BoundField HeaderText="tisID" Visible="false" DataField="tisID" />
                                                    <asp:BoundField HeaderText="" DataField="title" />
                                                    <asp:BoundField HeaderText="TVD" DataField="tvd" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField HeaderText="North" DataField="north" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField HeaderText="East" DataField="east" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField HeaderText="VS" DataField="vs" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField HeaderText="Subsea" DataField="subsea" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField HeaderText="DLS" DataField="dls" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField HeaderText="Build Rate" DataField="brate" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField HeaderText="Turn Rate" DataField="trate" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField HeaderText="Northing" DataField="northing" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField HeaderText="Easting" DataField="easting" ItemStyle-HorizontalAlign="Center" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <div id="divClearResBtnRow" class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group" style="width: 100%;">
                                                <asp:Button ID="btnClearResDetail" runat="server" Text="Clear Detail" CssClass="btn btn-warning text-center" Width="10%"
                                                    Font-Size="Large" OnClick="btnClearResDetail_Click" Visible="false" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnClearResDetail" />
                                <asp:PostBackTrigger ControlID="gvwRQCResults" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
        <div runat="server" id="divDone" visible="false" class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="input-group" style="width: 100%;">
                    <asp:Button ID="btnDetailCancel" runat="server" Text="Done" CssClass="btn btn-primary text-center" Width="10%" OnClick="btnDetailCancel_Click"
                        CausesValidation="False" />
                </div>
            </div>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnDetailCancel" />        
    </Triggers>
</asp:UpdatePanel>