﻿using log4net;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Color = System.Drawing.Color;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using UNT = SmartsVer1.Helpers.DemogHelpers.UnitsConverter;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using OPR = SmartsVer1.Helpers.QCHelpers.Opr;
using QC = SmartsVer1.QCHelpers.sdRawDataQC;
using WPN = SmartsVer1.Helpers.QCHelpers.WellPlan;
using DL = SmartsVer1.Helpers.DemogHelpers.DataLoader;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;

namespace SmartsVer1.Control.Services
{
    public partial class ctrlQCMin : System.Web.UI.UserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ctrlQCMin));
        const Int32 JobClassID = 1;
        const Int32 serviceGroupID = 2;
        const Int32 ClientType = 4;
        const Int32 dataType = 1;
        const Int32 dataEntryMethod = 1;
        const String dataFileName = "RawDataRow";
        String LoginName, username, domain, GetClientDBString;
        Int32 SysClientID = -99, regCount = 0, wellCount = 0, runCount = 0, mrmCount = 0, mbhaCount = 0;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                SysClientID = Convert.ToInt32(usrP.clntID);
                Dictionary<Int32, String> operatorsList = CLNT.getOperatorCoList(ClientType);
                this.ddlGlobalOperatorList.Items.Clear();
                this.ddlGlobalOperatorList.DataSource = operatorsList;
                this.ddlGlobalOperatorList.DataTextField = "Value";
                this.ddlGlobalOperatorList.DataValueField = "Key";
                this.ddlGlobalOperatorList.DataBind();
                this.ddlGlobalOperatorList.Items.Insert(0, new ListItem("--- Select Operator Company ---", "-1"));
                this.ddlGlobalOperatorList.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlGlobalOperatorList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                String oName = Convert.ToString(this.ddlGlobalOperatorList.SelectedItem);
                this.ancHead.InnerHtml = "<strong> " + oName + "</strong>";
                Int32 oprID = Convert.ToInt32(this.ddlGlobalOperatorList.SelectedValue);
                Dictionary<Int32, String> padList = AST.getWellPadListWithAssetCount(oprID, SysClientID, GetClientDBString);
                this.ddlWellPad.Items.Clear();
                this.ddlWellPad.DataSource = padList;
                this.ddlWellPad.DataTextField = "Value";
                this.ddlWellPad.DataValueField = "Key";
                this.ddlWellPad.DataBind();
                this.ddlWellPad.Items.Insert(0, new ListItem("--- Select Wellpad ---", "-1"));
                this.ddlWellPad.SelectedIndex = -1;
                this.divOpr.Visible = false;
                this.divPad.Visible = true;
                this.divWell.Visible = false;
                this.divSec.Visible = false;
                this.divRun.Visible = false;
                this.divManData.Visible = false;
                this.btnMetaCancel.Enabled = true;
                this.btnMetaCancel.CssClass = "btn btn-warning text-center";
                this.liStep1.Attributes["class"] = "stepper-done";
                this.liStep2.Attributes["class"] = "stepper-todo";
                this.liStep3.Attributes["class"] = "stepper-todo";
                this.liStep4.Attributes["class"] = "stepper-todo";
                this.liStep5.Attributes["class"] = "stepper-todo";
                this.liStep6.Attributes["class"] = "stepper-todo";
                this.liStep7.Attributes["class"] = "stepper-todo";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWellPad_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                String oName = Convert.ToString(this.ddlGlobalOperatorList.SelectedItem);
                String pName = Convert.ToString(this.ddlWellPad.SelectedItem);
                this.ancHead.InnerHtml = "<strong> " + oName + " > " + pName + "</strong>";
                Int32 opID = Convert.ToInt32(this.ddlGlobalOperatorList.SelectedValue);
                Int32 pdID = Convert.ToInt32(this.ddlWellPad.SelectedValue);
                System.Data.DataTable pdTable = AST.getWellsForWellPadWithRegistrationTable(opID, SysClientID, pdID, GetClientDBString);
                this.gvwWellList.DataSource = pdTable;
                this.gvwWellList.PageIndex = 0;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();
                this.divOpr.Visible = false;
                this.divPad.Visible = false;
                this.divWell.Visible = true;
                this.divSec.Visible = false;
                this.divRun.Visible = false;
                this.divManData.Visible = false;
                this.btnMetaCancel.Enabled = true;
                this.btnMetaCancel.CssClass = "btn btn-warning text-center";
                this.liStep1.Attributes["class"] = "stepper-done";
                this.liStep2.Attributes["class"] = "stepper-done";
                this.liStep3.Attributes["class"] = "stepper-todo";
                this.liStep4.Attributes["class"] = "stepper-todo";
                this.liStep5.Attributes["class"] = "stepper-todo";
                this.liStep6.Attributes["class"] = "stepper-todo";
                this.liStep7.Attributes["class"] = "stepper-todo";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = ((DataRowView)e.Row.DataItem).Row;
                    Button selBtn = e.Row.Cells[0].Controls[0] as Button;
                    String stC = Convert.ToString(dr["stClr"]);
                    String stt = Convert.ToString(dr["wstID"]);
                    Int32 sID = Convert.ToInt32(dr["srvID"]);
                    if (stt.Equals("In Process") && sID.Equals(33))
                    {
                        selBtn.Enabled = true;
                        e.Row.Cells[2].BackColor = Color.FromName(stC.ToString());
                        e.Row.Cells[3].BackColor = Color.FromName(stC.ToString());
                    }
                    else
                    {
                        selBtn.Enabled = false;
                        selBtn.CssClass = "btn btn-default text-center";                        
                    }                                       
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 OperatorID = Convert.ToInt32(this.ddlGlobalOperatorList.SelectedValue);
                Int32 WellpadID = Convert.ToInt32(this.ddlWellPad.SelectedValue);
                Int32 WellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                GridViewRow dRow = this.gvwWellList.SelectedRow;
                Decimal welTarget = AST.getClientWellPlannedMaxMDepth(WellID, GetClientDBString);
                String welStatus = AST.getWellRegistrationStatusName(OperatorID, WellpadID, WellID, GetClientDBString);
                Int32 checkPlan = WPN.checkWellPlanForSelectedWell(WellID, GetClientDBString);

                if (welStatus.Equals("Complete"))
                {
                    this.wellEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                    this.iWell.Attributes["class"] = "icon fa fa-warning";
                    this.spnWell.InnerText = " Warning!";
                    this.lblWell.Text = "Well/Lateral processing Completed. Cannot Audit/QC Surveys for selected Well/Lateral";
                    this.wellEnclosure.Visible = true;
                }
                else
                {
                    Dictionary<Int32, String> sectionList = OPR.getWellSectionToWellList(WellID, GetClientDBString);
                    this.ddlSectionList.Items.Clear();
                    this.ddlSectionList.DataSource = sectionList;
                    this.ddlSectionList.DataTextField = "Value";
                    this.ddlSectionList.DataValueField = "Key";
                    this.ddlSectionList.DataBind();
                    this.ddlSectionList.Items.Insert(0, new ListItem("--- Select Well Section ---", "-1"));
                    this.ddlSectionList.SelectedIndex = -1;
                    this.divOpr.Visible = false;
                    this.divPad.Visible = false;
                    this.divWell.Visible = false;
                    this.divSec.Visible = true;
                    this.divRun.Visible = false;
                    this.divManData.Visible = false;
                    String oName = Convert.ToString(this.ddlGlobalOperatorList.SelectedItem);
                    String pName = Convert.ToString(this.ddlWellPad.SelectedItem);
                    String wName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                    this.ancHead.InnerHtml = "<strong> " + oName + " > " + pName + " > " + wName + "</strong>";
                    this.liStep1.Attributes["class"] = "stepper-done";
                    this.liStep2.Attributes["class"] = "stepper-done";
                    this.liStep3.Attributes["class"] = "stepper-done";
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlSectionList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 oprID = -99, padID = -99, wellID = -99, sectionID = -99;
                oprID = Convert.ToInt32(this.ddlGlobalOperatorList.SelectedValue);
                padID = Convert.ToInt32(this.ddlWellPad.SelectedValue);
                wellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                sectionID = Convert.ToInt32(this.ddlSectionList.SelectedValue);                
                Dictionary<Int32, String> runList = OPR.getRunForWellSectionDictionaryList(oprID, padID, wellID, sectionID, GetClientDBString);                
                this.ddlRunList.Items.Clear();
                this.ddlRunList.DataSource = runList;
                this.ddlRunList.DataTextField = "Value";
                this.ddlRunList.DataValueField = "Key";
                this.ddlRunList.DataBind();
                this.ddlRunList.Items.Insert(0, new ListItem("--- Select Run ---", "-1"));
                this.ddlRunList.SelectedIndex = -1;
                this.divOpr.Visible = false;
                this.divPad.Visible = false;
                this.divWell.Visible = false;
                this.divSec.Visible = false;
                this.divRun.Visible = true;
                this.divManData.Visible = false;
                String oName = Convert.ToString(this.ddlGlobalOperatorList.SelectedItem);
                String pName = Convert.ToString(this.ddlWellPad.SelectedItem);
                GridViewRow dRow = this.gvwWellList.SelectedRow;
                String wName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                String sName = Convert.ToString(this.ddlSectionList.SelectedItem);
                this.ancHead.InnerHtml = "<strong> " + oName + " > " + pName + " > " + wName + " > " + sName + "</strong>";
                this.liStep1.Attributes["class"] = "stepper-done";
                this.liStep2.Attributes["class"] = "stepper-done";
                this.liStep3.Attributes["class"] = "stepper-done";
                this.liStep4.Attributes["class"] = "stepper-done";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlRunList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 findRTI = -99, findBHA = -99, runID = -99, secID = -99, wellID = -99, padID = -99, optrID = -99;
                this.divOpr.Visible = false;
                this.divPad.Visible = false;
                this.divWell.Visible = false;
                this.divSec.Visible = false;
                optrID = Convert.ToInt32(this.ddlGlobalOperatorList.SelectedValue);
                padID = Convert.ToInt32(this.ddlWellPad.SelectedValue);
                wellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                secID = Convert.ToInt32(this.ddlSectionList.SelectedValue);
                runID = Convert.ToInt32(this.ddlRunList.SelectedValue);
                findRTI = AST.findTIForRun(runID, secID, wellID, padID, SysClientID, optrID, GetClientDBString);
                findBHA = AST.findBHAForRun(runID, secID, wellID, padID, SysClientID, optrID, GetClientDBString);
                if (!findRTI.Equals(1))
                {
                    this.runEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                    this.iRun.Attributes["class"] = "icon fa fa-warning";
                    this.spnRun.InnerText = " Warning!";
                    this.lblRun.Text = "Missing Tie-In Survey for selected Run";
                    this.runEnclosure.Visible = true;
                }
                else if (!findBHA.Equals(1))
                {
                    this.runEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                    this.iRun.Attributes["class"] = "icon fa fa-warning";
                    this.spnRun.InnerText = " Warning!";
                    this.lblRun.Text = "Missing BHA-Signature for selected Run";
                    this.runEnclosure.Visible = true;
                }
                else
                {
                    this.divManData.Visible = true;
                    this.divRun.Visible = false;
                }
                String oName = Convert.ToString(this.ddlGlobalOperatorList.SelectedItem);
                String pName = Convert.ToString(this.ddlWellPad.SelectedItem);
                GridViewRow wRow = this.gvwWellList.SelectedRow;
                String wName = Convert.ToString(Server.HtmlDecode(wRow.Cells[2].Text));
                String sName = Convert.ToString(this.ddlSectionList.SelectedItem);
                String rName = Convert.ToString(this.ddlRunList.SelectedItem);
                this.ancHead.InnerHtml = "<strong> " + oName + " > " + pName + " > " + wName + " > " + sName + " > " + rName + "</strong>";
                this.liStep1.Attributes["class"] = "stepper-done";
                this.liStep2.Attributes["class"] = "stepper-done";
                this.liStep3.Attributes["class"] = "stepper-done";
                this.liStep4.Attributes["class"] = "stepper-done";
                this.liStep5.Attributes["class"] = "stepper-done";
                this.btnMetaCancel.Visible = false;
                this.btnQCManualUpload.Visible = true;
                this.btnCancelManualUpload.Visible = true;
                this.btnDetailCancel.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 pdID = Convert.ToInt32(this.ddlWellPad.SelectedValue);
                System.Data.DataTable pdTable = AST.getWellForWellPadTable(pdID, GetClientDBString);
                this.gvwWellList.DataSource = pdTable;
                this.gvwWellList.PageIndex = e.NewPageIndex;
                this.gvwWellList.DataBind();
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnMetaCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> operatorList = CLNT.getOperatorCoList(ClientType);
                this.ddlGlobalOperatorList.Items.Clear();
                this.ddlGlobalOperatorList.DataSource = operatorList;
                this.ddlGlobalOperatorList.DataTextField = "Value";
                this.ddlGlobalOperatorList.DataValueField = "Key";
                this.ddlGlobalOperatorList.DataBind();
                this.ddlGlobalOperatorList.Items.Insert(0, new ListItem("--- Select Operator Company ---", "-1"));
                this.ddlGlobalOperatorList.SelectedIndex = -1;
                this.divOpr.Visible = true;
                this.divPad.Visible = false;
                this.divWell.Visible = false;
                this.divSec.Visible = false;
                this.divRun.Visible = false;
                this.divManData.Visible = false;
                this.ancHead.InnerHtml = "Operator Company";
                this.liStep1.Attributes["class"] = "stepper-todo";
                this.liStep2.Attributes["class"] = "stepper-todo";
                this.liStep3.Attributes["class"] = "stepper-todo";
                this.liStep4.Attributes["class"] = "stepper-todo";
                this.liStep5.Attributes["class"] = "stepper-todo";
                this.liStep6.Attributes["class"] = "stepper-todo";
                this.liStep7.Attributes["class"] = "stepper-todo";
                this.rdbCMS.Checked = false;
                this.rdbFSC.Checked = false;
                this.rdbG.Checked = false;
                this.rdbMSC.Checked = false;
                this.rdbMG.Checked = false;
                this.rdbGss.Checked = false;
                this.rdbT.Checked = false;
                this.rdbMLIT.Checked = false;
                this.rdbMCT.Checked = false;
                this.rdbNT.Checked = false;
                this.wellEnclosure.Visible = false;
                this.lblWell.Text = "";
                this.runEnclosure.Visible = false;
                this.lblRun.Text = "";
                this.txtDataEnclosure.Visible = false;
                this.lblManual.Text = "";
                this.btnMetaCancel.Enabled = false;
                this.btnMetaCancel.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwViewRawDisplay_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                List<System.Data.DataTable> dataTables = new List<System.Data.DataTable>();
                dataTables = (List<System.Data.DataTable>)HttpContext.Current.Session["rawDataTables"];
                System.Data.DataTable rawTable = dataTables[1];
                this.gvwViewRawDisplay.DataSource = rawTable;
                this.gvwViewRawDisplay.PageIndex = e.NewPageIndex;
                this.gvwViewRawDisplay.DataBind();
                this.liStep5.Attributes["class"] = "stepper-todo";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnQCManualUpload_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable iResult = new System.Data.DataTable(), headTable = new System.Data.DataTable(), bodyTable = new System.Data.DataTable();
                List<System.Data.DataTable> dataTables = new List<System.Data.DataTable>();
                Int32 wD = -99, scD = -99, RunID = -99, ServiceID = -99, demID = -99;
                String dataRowText = String.Empty, selections = String.Empty;
                wD = Convert.ToInt32(this.gvwWellList.SelectedValue);
                ServiceID = AST.getWellRegisteredServiceID(wD, GetClientDBString);
                scD = Convert.ToInt32(this.ddlSectionList.SelectedValue);
                RunID = Convert.ToInt32(this.ddlRunList.SelectedValue);
                dataRowText = Convert.ToString(this.txtQCManualProcess.Text);

                GridViewRow dRow = this.gvwWellList.SelectedRow;
                String wName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                logger.Info("User: " + username + " Raw Survey Uploaded for Well/Lateral: " + wName);

                if (String.IsNullOrEmpty(dataRowText))
                {
                    this.txtDataEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                    this.iTXTDMessage.Attributes["class"] = "icon fa fa-warning";
                    this.txtDHeader.InnerText = " Warning!";
                    this.lblManual.Text = " Please enter Survey before clicking Upload Survey button. ";
                    this.txtDataEnclosure.Visible = true;
                    this.txtQCManualProcess.Text = "";
                }
                else
                {
                    iResult = DL.uploadManualRawDataRow(ServiceID, wD, scD, RunID, demID, dataType, dataRowText, LoginName, domain, GetClientDBString);
                    dataTables.Add(headTable);
                    dataTables.Add(iResult);
                    HttpContext.Current.Session["rawUploadedDataTables"] = dataTables;
                    String[] str = dataRowText.Split(',');
                    HttpContext.Current.Session["rDepth"] = Convert.ToDecimal(str[1]);
                    this.gvwViewRawDisplay.DataSource = iResult;
                    this.gvwViewRawDisplay.PageIndex = 0;
                    this.gvwViewRawDisplay.SelectedIndex = -1;
                    this.gvwViewRawDisplay.DataBind();
                    this.gvwViewRawDisplay.Visible = true;
                    this.liStep1.Attributes["class"] = "stepper-done";
                    this.liStep2.Attributes["class"] = "stepper-done";
                    this.liStep3.Attributes["class"] = "stepper-done";
                    this.liStep4.Attributes["class"] = "stepper-done";
                    this.liStep5.Attributes["class"] = "stepper-done";
                    this.liStep6.Attributes["class"] = "stepper-todo";
                    this.liStep7.Attributes["class"] = "stepper-todo";
                    selections = Convert.ToString(HttpContext.Current.Session["selectionsDone"]);
                    if (String.IsNullOrEmpty(selections))
                    {
                        this.rdbCMS.Checked = false;
                        this.rdbFSC.Checked = false;
                        this.rdbG.Checked = false;
                        this.rdbMSC.Checked = false;
                        this.rdbMG.Checked = false;
                        this.rdbGss.Checked = false;
                        this.rdbT.Checked = false;
                        this.rdbMLIT.Checked = false;
                        this.rdbMCT.Checked = false;
                        this.rdbNT.Checked = false;
                    }
                    this.txtQCManualProcess.Text = "";
                    this.btnQCManualUpload.Visible = false;
                    this.btnCancelManualUpload.Visible = false;
                    this.lblDepthU.CssClass = "control-label label-info";
                    this.lblAclU.CssClass = "control-label label-info";
                    this.lblMagU.CssClass = "control-label label-info";
                    this.lbl_Flip.CssClass = "control-label label-info";
                    this.divManData.Visible = false;
                    this.divUploadedData.Visible = true;
                    this.divTransformData.Visible = false;
                    this.btnProcessRawData.Visible = true;
                    this.btnProcessSrvyCancel.Visible = true;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCancelManualUpload_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtQCManualProcess.Text = "";
                this.txtDataEnclosure.Visible = false;
                this.lblManual.Text = "";
                this.liStep5.Attributes["class"] = "stepper-todo";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProcessRawData_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable iResult = new System.Data.DataTable(), selectedRawTable = new System.Data.DataTable(), selectedResultsTable = new System.Data.DataTable();
                DataColumn chk = selectedRawTable.Columns.Add("chk", typeof(Int32));
                DataColumn drID = selectedRawTable.Columns.Add("drID", typeof(Int32));
                DataColumn Depth = selectedRawTable.Columns.Add("Depth", typeof(Decimal));
                DataColumn rGx = selectedRawTable.Columns.Add("rGx", typeof(Decimal));
                DataColumn rGy = selectedRawTable.Columns.Add("rGy", typeof(Decimal));
                DataColumn rGz = selectedRawTable.Columns.Add("rGz", typeof(Decimal));
                DataColumn rBx = selectedRawTable.Columns.Add("rBx", typeof(Decimal));
                DataColumn rBy = selectedRawTable.Columns.Add("rBy", typeof(Decimal));
                DataColumn rBz = selectedRawTable.Columns.Add("rBz", typeof(Decimal));
                DataColumn rtimestamp = selectedRawTable.Columns.Add("rtimestamp", typeof(String));
                DataColumn rsChk = selectedResultsTable.Columns.Add("chk", typeof(Int32));
                DataColumn rsID = selectedResultsTable.Columns.Add("drID", typeof(Int32));
                DataColumn rsDepth = selectedResultsTable.Columns.Add("resDepth", typeof(Decimal));
                DataColumn rsInc = selectedResultsTable.Columns.Add("resInc", typeof(Decimal));
                DataColumn rsAzm = selectedResultsTable.Columns.Add("resAzm", typeof(Decimal));
                DataColumn rsDip = selectedResultsTable.Columns.Add("resDip", typeof(Decimal));
                DataColumn rsBTotal = selectedResultsTable.Columns.Add("resBTotal", typeof(Decimal));
                DataColumn rsGTotal = selectedResultsTable.Columns.Add("resGTotal", typeof(Decimal));
                DataColumn rsGTF = selectedResultsTable.Columns.Add("resGTF", typeof(Decimal));
                Int32 wD = -99, wS = -99, RunID = -99, chk_FlipGx = -99, chk_FlipGy = -99, chk_FlipGz = -99, chk_FlipBx = -99, chk_FlipBy = -99, chk_FlipBz = -99, selectedService = -99;
                Int32 rawID = -99, rawChecked = -99, lenU = -99, magU = -99, aclU = -99;
                Decimal rawDepth = -99.00M, rawGx = -99.000000000000M, rawGy = -99.000000000000M, rawGz = -99.000000000000M, rawBx = -99.000000000000M, rawBy = -99.000000000000M, rawBz = -99.000000000000M, resDepth = -99.00M, resInc = -99.000000000000M, resAzm = -99.000000000000M, resDip = -99.000000000000M, resBTotal = -99.000000000000M, resGTotal = -99.000000000000M, resGTF = -99.000000000000M;
                String rawDepthVal = String.Empty, rawGxVal = String.Empty, rawGyVal = String.Empty, rawGzVal = String.Empty, rawBxVal = String.Empty, rawByVal = String.Empty, rawBzVal = String.Empty, resDepthVal = String.Empty, resIncVal = String.Empty, resAzmVal = String.Empty, resDipVal = String.Empty, resBTotalVal = String.Empty, resGTotalVal = String.Empty, resGTFVal = String.Empty, rTime = String.Empty;
                Decimal depthResult = -99.00M, gxResult = -99.000000000000M, gyResult = -99.000000000000M, gzResult = -99.000000000000M, bxResult = -99.000000000000M, byResult = -99.000000000000M, bzResult = -99.000000000000M, incResult = -99.000000000000M, azmResult = -99.000000000000M, dipResult = -99.000000000000M, btResult = -99.000000000000M, gtResult = -99.000000000000M, gtfResult = -99.000000000000M;
                wD = Convert.ToInt32(this.gvwWellList.SelectedValue);
                wS = Convert.ToInt32(this.ddlSectionList.SelectedValue);
                RunID = Convert.ToInt32(this.ddlRunList.SelectedValue);
                selectedService = AST.getWellRegisteredServiceID(wD, GetClientDBString);

                List<System.Data.DataTable> dataTables = new List<System.Data.DataTable>(), readyTables = new List<System.Data.DataTable>();
                if (dataType.Equals(1)) //Raw Table
                {
                    dataTables = (List<System.Data.DataTable>)HttpContext.Current.Session["rawUploadedDataTables"];
                    System.Data.DataTable rawdataTable = new System.Data.DataTable(), fileHead = new System.Data.DataTable(), fileBody = new System.Data.DataTable();
                    fileBody = (System.Data.DataTable)dataTables[0];
                    rawdataTable = (System.Data.DataTable)dataTables[1];
                    foreach (System.Data.DataRow dRow in rawdataTable.Rows)
                    {
                        rawID = Convert.ToInt32(dRow["drID"]);
                        rawDepthVal = Convert.ToString(dRow["depth"]);
                        if (Decimal.TryParse(rawDepthVal, out depthResult))
                        {
                            rawDepth = Convert.ToDecimal(rawDepthVal);
                            if (lenU.Equals(4))
                            {
                                rawDepth = UNT.convFeetToMeters(rawDepth);
                            }
                        }
                        else
                        {
                            rawDepth = -99.00M;
                        }
                        rawGxVal = Convert.ToString(dRow["gx"]);
                        if (Decimal.TryParse(rawGxVal, out gxResult))
                        {
                            rawGx = Convert.ToDecimal(rawGxVal);
                            rawGx = rawGx * Convert.ToDecimal(chk_FlipGx);
                            rawGx = UNT.convAccelerometerToAccelerationDueToGravity(rawGx, aclU);
                        }
                        else
                        {
                            rawGx = -99.00M;
                        }
                        rawGyVal = Convert.ToString(dRow["gy"]);
                        if (Decimal.TryParse(rawGyVal, out gyResult))
                        {
                            rawGy = Convert.ToDecimal(rawGyVal);
                            rawGy = rawGy * Convert.ToDecimal(chk_FlipGy);
                            rawGy = UNT.convAccelerometerToAccelerationDueToGravity(rawGy, aclU);
                        }
                        else
                        {
                            rawGy = -99.00M;
                        }
                        rawGzVal = Convert.ToString(dRow["gz"]);
                        if (Decimal.TryParse(rawGzVal, out gzResult))
                        {
                            rawGz = Convert.ToDecimal(rawGzVal);
                            rawGz = rawGz * Convert.ToDecimal(chk_FlipGz);
                            rawGz = UNT.convAccelerometerToAccelerationDueToGravity(rawGz, aclU);
                        }
                        else
                        {
                            rawGz = -99.00M;
                        }
                        rawBxVal = Convert.ToString(dRow["bx"]);
                        if (Decimal.TryParse(rawBxVal, out bxResult))
                        {
                            rawBx = Convert.ToDecimal(rawBxVal);
                            rawBx = rawBx * Convert.ToDecimal(chk_FlipBx);
                            rawBx = UNT.convMagnetometerToNanoTesla(rawBx, magU);
                        }
                        else
                        {
                            rawBx = -99.00M;
                        }
                        rawByVal = Convert.ToString(dRow["by"]);
                        if (Decimal.TryParse(rawByVal, out byResult))
                        {
                            rawBy = Convert.ToDecimal(rawByVal);
                            rawBy = rawBy * Convert.ToDecimal(chk_FlipBy);
                            rawBy = UNT.convMagnetometerToNanoTesla(rawBy, magU);
                        }
                        else
                        {
                            rawBy = -99.00M;
                        }
                        rawBzVal = Convert.ToString(dRow["bz"]);
                        if (Decimal.TryParse(rawBzVal, out bzResult))
                        {
                            rawBz = Convert.ToDecimal(rawBzVal);
                            rawBz = rawBz * Convert.ToDecimal(chk_FlipBz);
                            rawBz = UNT.convMagnetometerToNanoTesla(rawBz, magU);
                        }
                        else
                        {
                            rawBz = -99.00M;
                        }
                        if ((!rawDepth.Equals(-99.00) && (!rawGx.Equals(-99.00)) && (!rawGy.Equals(-99.00)) && (!rawGz.Equals(-99.00))))
                        {
                            rawChecked = 1;
                        }
                        else
                        {
                            rawChecked = -1;
                        }
                        rTime = Convert.ToString(dRow["Timestamp"]);
                        if (String.IsNullOrEmpty(rTime)) { rTime = "---"; }
                        selectedRawTable.Rows.Add(rawChecked, rawID, rawDepth, rawGx, rawGy, rawGz, rawBx, rawBy, rawBz, rTime);
                        selectedRawTable.AcceptChanges();
                        HttpContext.Current.Session["selectedRawValues"] = selectedRawTable;
                        this.gvwSelectedRaw.DataSource = selectedRawTable;
                        this.gvwSelectedRaw.PageIndex = 0;
                        this.gvwSelectedRaw.SelectedIndex = -1;
                        this.gvwSelectedRaw.DataBind();
                        this.gvwSelectedRaw.Visible = true;
                        this.divTransformData.Visible = true;
                        this.divUploadedData.Visible = false;
                        GridViewRow wellNameRow = this.gvwWellList.SelectedRow;
                        String wName = Convert.ToString(Server.HtmlDecode(wellNameRow.Cells[2].Text));
                        logger.Info("User: " + username + " QC_Processing Initiated for Well/Lateral: " + wName);
                    }
                }
                this.liStep1.Attributes["class"] = "stepper-done";
                this.liStep2.Attributes["class"] = "stepper-done";
                this.liStep3.Attributes["class"] = "stepper-done";
                this.liStep4.Attributes["class"] = "stepper-done";
                this.liStep5.Attributes["class"] = "stepper-done";
                this.liStep6.Attributes["class"] = "stepper-done";
                this.liStep7.Attributes["class"] = "stepper-todo";
                this.btnProcessRawData.Visible = false;
                this.btnProcessSrvyCancel.Visible = false;
                this.btnProcessReadyData.Visible = true;
                this.btnCancelReadyData.Visible = true;
                this.txtDataEnclosure.Visible = false;
                this.lblManual.Text = "";
                this.enWDInfo.Visible = false;
                this.lblWDInfo.Text = "";
                this.enRDInfo.Visible = false;
                this.lblRDInfo.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProcessSrvyCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.liStep5.Attributes["class"] = "stepper-todo";
                this.chk_NoSignTransformation.Checked = false;
                this.chk_NoSignTransformation.Enabled = true;
                this.chk_FlipBx.Checked = false;
                this.chk_FlipBx.Enabled = false;
                this.chk_FlipBy.Checked = false;
                this.chk_FlipBy.Enabled = false;
                this.chk_FlipBz.Checked = false;
                this.chk_FlipBz.Enabled = false;
                this.chk_FlipGx.Checked = false;
                this.chk_FlipGx.Enabled = false;
                this.chk_FlipGy.Checked = false;
                this.chk_FlipGy.Enabled = false;
                this.chk_FlipGz.Checked = false;
                this.chk_FlipGz.Enabled = false;
                this.rdbMT.Checked = false;
                this.rdbFT.Checked = false;
                this.rdbCMS.Checked = false;
                this.rdbFSC.Checked = false;
                this.rdbG.Checked = false;
                this.rdbMSC.Checked = false;
                this.rdbMT.Checked = false;
                this.rdbT.Checked = false;
                this.rdbGss.Checked = false;
                this.rdbNT.Checked = false;
                this.rdbMLIT.Checked = false;
                this.rdbMCT.Checked = false;
                HttpContext.Current.Session["rdbFT"] = null;
                HttpContext.Current.Session["rdbMT"] = null;
                HttpContext.Current.Session["rdbCMS"] = null;
                HttpContext.Current.Session["rdbFSC"] = null;
                HttpContext.Current.Session["rdbG"] = null;
                HttpContext.Current.Session["rdbMSC"] = null;
                HttpContext.Current.Session["rdbMG"] = null;
                HttpContext.Current.Session["rdbGss"] = null;
                HttpContext.Current.Session["rdbT"] = null;
                HttpContext.Current.Session["rdbMLIT"] = null;
                HttpContext.Current.Session["rdbMCT"] = null;
                HttpContext.Current.Session["rdbNT"] = null;
                HttpContext.Current.Session["chk_NoSignTransformation"] = null;
                HttpContext.Current.Session["chk_FlipGx"] = null;
                HttpContext.Current.Session["chk_FlipGy"] = null;
                HttpContext.Current.Session["chk_FlipGz"] = null;
                HttpContext.Current.Session["chk_FlipBx"] = null;
                HttpContext.Current.Session["chk_FlipBy"] = null;
                HttpContext.Current.Session["chk_FlipBz"] = null;
                this.enWDInfo.Visible = false;
                this.lblWDInfo.Text = "";
                this.enRDInfo.Visible = false;
                this.lblRDInfo.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDetailCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.lblManual.Text = "";
                this.lblManual.Visible = false;
                this.txtQCManualProcess.Text = "";
                this.chk_NoSignTransformation.Checked = false;
                this.chk_FlipBx.Checked = false;
                this.chk_FlipBy.Checked = false;
                this.chk_FlipBz.Checked = false;
                this.chk_FlipGx.Checked = false;
                this.chk_FlipGy.Checked = false;
                this.chk_FlipGz.Checked = false;
                this.btnMetaCancel_Click(null, null);
                this.btnMetaCancel.Visible = true;
                this.btnQCManualUpload.Visible = false;
                this.btnCancelManualUpload.Visible = false;
                this.btnProcessRawData.Visible = false;
                this.btnProcessSrvyCancel.Visible = false;
                this.btnDetailCancel.Visible = false;
                this.btnNextSurvey.Visible = false;
                Dictionary<Int32, String> operatorsList = CLNT.getOperatorCoList(ClientType);
                this.ddlGlobalOperatorList.Items.Clear();
                this.ddlGlobalOperatorList.DataSource = operatorsList;
                this.ddlGlobalOperatorList.DataTextField = "Value";
                this.ddlGlobalOperatorList.DataValueField = "Key";
                this.ddlGlobalOperatorList.DataBind();
                this.ddlGlobalOperatorList.Items.Insert(0, new ListItem("--- Select Operator Company ---", "-1"));
                this.ddlGlobalOperatorList.SelectedIndex = -1;
                this.ddlWellPad.Items.Clear();
                this.ddlWellPad.DataSource = null;
                this.ddlWellPad.DataBind();
                this.ddlWellPad.Items.Insert(0, new ListItem("--- Select Wellpad ---", "-1"));
                this.ddlWellPad.SelectedIndex = -1;
                this.gvwWellList.DataSource = null;
                this.gvwWellList.DataBind();
                this.gvwRQCResults.DataSource = null;
                this.gvwRQCResults.DataBind();
                this.gvwRQCAzimuths.DataSource = null;
                this.gvwRQCAzimuths.DataBind();
                this.gvwPlacement.DataSource = null;
                this.gvwPlacement.DataBind();
                this.ddlSectionList.Items.Clear();
                this.ddlSectionList.DataSource = null;
                this.ddlSectionList.DataBind();
                this.ddlSectionList.Items.Insert(0, new ListItem("--- Select Well Section ---", "-1"));
                this.ddlSectionList.SelectedIndex = -1;
                this.ddlRunList.Items.Clear();
                this.ddlRunList.DataSource = null;
                this.ddlRunList.DataBind();
                this.ddlRunList.Items.Insert(0, new ListItem("--- Select Run ---", "-1"));
                this.ddlRunList.SelectedIndex = -1;
                this.ancHead.InnerHtml = "Operator Company";
                this.divOpr.Visible = true;
                this.divPad.Visible = false;
                this.divWell.Visible = false;
                this.divSec.Visible = false;
                this.divRun.Visible = false;
                this.divManData.Visible = false;
                this.divUploadedData.Visible = false;
                this.divTransformData.Visible = false;
                this.liStep1.Attributes["class"] = "stepper-todo";
                this.liStep2.Attributes["class"] = "stepper-todo";
                this.liStep3.Attributes["class"] = "stepper-todo";
                this.liStep4.Attributes["class"] = "stepper-todo";
                this.liStep5.Attributes["class"] = "stepper-todo";
                this.liStep6.Attributes["class"] = "stepper-todo";
                this.liStep7.Attributes["class"] = "stepper-todo";
                HttpContext.Current.Session["selectionsDone"] = null;
                HttpContext.Current.Session["rdbFT"] = null;
                HttpContext.Current.Session["rdbMT"] = null;
                HttpContext.Current.Session["rdbCMS"] = null;
                HttpContext.Current.Session["rdbFSC"] = null;
                HttpContext.Current.Session["rdbG"] = null;
                HttpContext.Current.Session["rdbMSC"] = null;
                HttpContext.Current.Session["rdbMG"] = null;
                HttpContext.Current.Session["rdbGss"] = null;
                HttpContext.Current.Session["rdbT"] = null;
                HttpContext.Current.Session["rdbMLIT"] = null;
                HttpContext.Current.Session["rdbMCT"] = null;
                HttpContext.Current.Session["rdbNT"] = null;
                HttpContext.Current.Session["chk_NoSignTransformation"] = null;
                HttpContext.Current.Session["chk_FlipGx"] = null;
                HttpContext.Current.Session["chk_FlipGy"] = null;
                HttpContext.Current.Session["chk_FlipGz"] = null;
                HttpContext.Current.Session["chk_FlipBx"] = null;
                HttpContext.Current.Session["chk_FlipBy"] = null;
                HttpContext.Current.Session["chk_FlipBz"] = null;
                this.enRDInfo.Visible = false;
                this.lblRDInfo.Text = "";
                this.enWDInfo.Visible = false;
                this.lblWDInfo.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void chk_NoSignTransformation_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.chk_NoSignTransformation.Checked = true;
                this.chk_FlipGx.Checked = false;
                this.chk_FlipGy.Checked = false;
                this.chk_FlipGz.Checked = false;
                this.chk_FlipBx.Checked = false;
                this.chk_FlipBy.Checked = false;
                this.chk_FlipBz.Checked = false;
                this.btnProcessRawData.Enabled = true;
                this.btnProcessRawData.CssClass = "btn btn-info text-center";
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void chk_FlipGx_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.chk_NoSignTransformation.Checked = false;
                this.btnProcessRawData.Enabled = true;
                this.btnProcessRawData.CssClass = "btn btn-info text-center";
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void chk_FlipGy_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.chk_NoSignTransformation.Checked = false;
                this.btnProcessRawData.Enabled = true;
                this.btnProcessRawData.CssClass = "btn btn-info text-center";
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void chk_FlipGz_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.chk_NoSignTransformation.Checked = false;
                this.btnProcessRawData.Enabled = true;
                this.btnProcessRawData.CssClass = "btn btn-info text-center";
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void chk_FlipBx_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.chk_NoSignTransformation.Checked = false;
                this.btnProcessRawData.Enabled = true;
                this.btnProcessRawData.CssClass = "btn btn-info text-center";
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void chk_FlipBy_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.chk_NoSignTransformation.Checked = false;
                this.btnProcessRawData.Enabled = true;
                this.btnProcessRawData.CssClass = "btn btn-info text-center";
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void chk_FlipBz_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.chk_NoSignTransformation.Checked = false;
                this.btnProcessRawData.Enabled = true;
                this.btnProcessRawData.CssClass = "btn btn-info text-center";
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void resetPage()
        {
            Dictionary<Int32, String> operatorsList = CLNT.getOperatorCoList(ClientType);
            this.ddlGlobalOperatorList.Items.Clear();
            this.ddlGlobalOperatorList.DataSource = operatorsList;
            this.ddlGlobalOperatorList.DataTextField = "Value";
            this.ddlGlobalOperatorList.DataValueField = "Key";
            this.ddlGlobalOperatorList.DataBind();
            this.ddlGlobalOperatorList.Items.Insert(0, new ListItem("--- Select Operator Company ---", "-1"));
            this.ddlGlobalOperatorList.SelectedIndex = -1;
            this.gvwWellList.SelectedIndex = -1;
            this.gvwWellList.DataBind();
            this.ddlSectionList.Items.Clear();
            this.ddlSectionList.DataSource = null;
            this.ddlSectionList.DataBind();
            this.ddlSectionList.Items.Insert(0, new ListItem("--- Select Well Section ---", "-1"));
            this.ddlSectionList.SelectedIndex = -1;
            this.ddlRunList.Items.Clear();
            this.ddlRunList.DataSource = null;
            this.ddlRunList.DataBind();
            this.ddlRunList.Items.Insert(0, new ListItem("--- Select Run ---", "-1"));
            this.ddlRunList.SelectedIndex = -1;
            this.btnProcessRawData.Enabled = false;
        }

        protected void gvwRQCQualifiers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = ((DataRowView)e.Row.DataItem).Row;
                    Int32 dC = Convert.ToInt32(dr["DWClr"]);
                    Int32 bC = Convert.ToInt32(dr["DYClr"]);
                    Int32 gC = Convert.ToInt32(dr["EAClr"]);
                    Int32 nG = Convert.ToInt32(dr["HVClr"]);

                    if (dC.Equals(1))
                    {
                        e.Row.Cells[1].BackColor = Color.LightGreen;
                    }
                    else
                    {
                        e.Row.Cells[1].BackColor = Color.Red;
                    }

                    if (bC.Equals(1))
                    {
                        e.Row.Cells[3].BackColor = Color.LightGreen;
                    }
                    else
                    {
                        e.Row.Cells[3].BackColor = Color.Red;
                    }

                    if (gC.Equals(1))
                    {
                        e.Row.Cells[5].BackColor = Color.LightGreen;
                    }
                    else
                    {
                        e.Row.Cells[5].BackColor = Color.Red;
                    }

                    if (nG.Equals(2))
                    {
                        e.Row.Cells[11].BackColor = Color.Red;
                    }
                    else
                    {
                        e.Row.Cells[11].BackColor = Color.LightGreen;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRQCBHA_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = ((DataRowView)e.Row.DataItem).Row;
                    Int32 ek = Convert.ToInt32(dr["EKClr"]);
                    Int32 eo = Convert.ToInt32(dr["EOClr"]);

                    if (ek.Equals(2) || ek.Equals(4) || ek.Equals(7))
                    {
                        e.Row.Cells[4].BackColor = Color.LightGreen;
                    }
                    else
                    {
                        e.Row.Cells[4].BackColor = Color.Red;
                    }

                    if (eo.Equals(1))
                    {
                        e.Row.Cells[8].BackColor = Color.LightGreen;
                    }
                    else
                    {
                        e.Row.Cells[8].BackColor = Color.Red;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected string loadGraphs()
        {
            try
            {
                StringBuilder strScript = new StringBuilder();

                strScript.Append("<script>");
                strScript.Append(" window.onload = function() { ");
                strScript.Append(" var multiline = document.getElementById('chrtQCChart').getContext(\"2d\", {alpha: false}); ");
                strScript.Append(" window.myLine = new Chart(multiline, configQCChart);  ");

                strScript.Append("    }; ");
                strScript.Append(" var colorNames = Object.keys(window.chartColors); ");

                strScript.Append("</script>");

                return strScript.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProcessReadyData_Click(object sender, EventArgs e)
        {
            try
            {
                ArrayList iResult = new ArrayList();
                List<System.Data.DataTable> uploadedData = new List<System.Data.DataTable>();
                System.Data.DataTable selectedValues = new System.Data.DataTable();
                String wellName = String.Empty, runName = String.Empty, selections = String.Empty;
                Int32 optrID = 0, srvID = 0, padID = 0, wellID = 0, sectionID = 0, runNameID = 0, RunID = 0, counter = 0;
                Int32 lenUnit = -99, acelUnit = -99, magUnit = -99, lenU = -99, aclU = -99, magU = -99;
                Int32 chk_FlipGx = 0, chk_FlipGy = 0, chk_FlipGz = 0, chk_FlipBx = 0, chk_FlipBy = 0, chk_FlipBz = 0;
                Int32 wellFlag = -99, runFlag = -99;
                selections = Convert.ToString(HttpContext.Current.Session["selectionsDone"]);
                if (String.IsNullOrEmpty(selections))
                {
                    if (this.chk_FlipGx.Checked) { chk_FlipGx = -1; } else { chk_FlipGx = 1; }
                    if (this.chk_FlipGy.Checked) { chk_FlipGy = -1; } else { chk_FlipGy = 1; }
                    if (this.chk_FlipGz.Checked) { chk_FlipGz = -1; } else { chk_FlipGz = 1; }
                    if (this.chk_FlipBx.Checked) { chk_FlipBx = -1; } else { chk_FlipBx = 1; }
                    if (this.chk_FlipBy.Checked) { chk_FlipBy = -1; } else { chk_FlipBy = 1; }
                    if (this.chk_FlipBz.Checked) { chk_FlipBz = -1; } else { chk_FlipBz = 1; }
                    if (this.rdbMT.Checked) { lenU = 5; } else { lenU = 4; }
                    if (this.rdbCMS.Checked) { aclU = 1; } else if (this.rdbFSC.Checked) { aclU = 2; } else if (this.rdbG.Checked) { aclU = 3; } else if (this.rdbMSC.Checked) { aclU = 4; } else { aclU = 5; }
                    if (this.rdbGss.Checked) { magU = 1; } else if (this.rdbT.Checked) { magU = 2; } else if (this.rdbMLIT.Checked) { magU = 3; } else if (this.rdbMCT.Checked) { magU = 4; } else { magU = 5; }
                }
                else if (Convert.ToInt32(selections).Equals(1))
                {
                    if (HttpContext.Current.Session["rdbFT"].Equals(1)) { this.rdbFT.Checked = true; }
                    if (HttpContext.Current.Session["rdbMT"].Equals(1)) { this.rdbMT.Checked = true; }
                    if (HttpContext.Current.Session["rdbCMS"].Equals(1)) { this.rdbCMS.Checked = true; }
                    if (HttpContext.Current.Session["rdbFSC"].Equals(1)) { this.rdbFSC.Checked = true; }
                    if (HttpContext.Current.Session["rdbG"].Equals(1)) { this.rdbG.Checked = true; }
                    if (HttpContext.Current.Session["rdbMSC"].Equals(1)) { this.rdbMSC.Checked = true; }
                    if (HttpContext.Current.Session["rdbMG"].Equals(1)) { this.rdbMG.Checked = true; }
                    if (HttpContext.Current.Session["rdbGss"].Equals(1)) { this.rdbGss.Checked = true; }
                    if (HttpContext.Current.Session["rdbT"].Equals(1)) { this.rdbT.Checked = true; }
                    if (HttpContext.Current.Session["rdbMLIT"].Equals(1)) { this.rdbMLIT.Checked = true; }
                    if (HttpContext.Current.Session["rdbMCT"].Equals(1)) { this.rdbMCT.Checked = true; }
                    if (HttpContext.Current.Session["rdbNT"].Equals(1)) { this.rdbNT.Checked = true; }
                    if (HttpContext.Current.Session["chk_NoSignTransformation"].Equals(1)) { this.chk_NoSignTransformation.Checked = true; }
                    if (HttpContext.Current.Session["chk_FlipGx"].Equals(1)) { this.chk_FlipGx.Checked = true; }
                    if (HttpContext.Current.Session["chk_FlipGy"].Equals(1)) { this.chk_FlipGy.Checked = true; }
                    if (HttpContext.Current.Session["chk_FlipGz"].Equals(1)) { this.chk_FlipGz.Checked = true; }
                    if (HttpContext.Current.Session["chk_FlipBx"].Equals(1)) { this.chk_FlipBx.Checked = true; }
                    if (HttpContext.Current.Session["chk_FlipBy"].Equals(1)) { this.chk_FlipBy.Checked = true; }
                    if (HttpContext.Current.Session["chk_FlipBz"].Equals(1)) { this.chk_FlipBz.Checked = true; }

                    if (this.chk_FlipGx.Checked) { chk_FlipGx = -1; } else { chk_FlipGx = 1; }
                    if (this.chk_FlipGy.Checked) { chk_FlipGy = -1; } else { chk_FlipGy = 1; }
                    if (this.chk_FlipGz.Checked) { chk_FlipGz = -1; } else { chk_FlipGz = 1; }
                    if (this.chk_FlipBx.Checked) { chk_FlipBx = -1; } else { chk_FlipBx = 1; }
                    if (this.chk_FlipBy.Checked) { chk_FlipBy = -1; } else { chk_FlipBy = 1; }
                    if (this.chk_FlipBz.Checked) { chk_FlipBz = -1; } else { chk_FlipBz = 1; }
                    if (this.rdbMT.Checked) { lenU = 5; } else { lenU = 4; }
                    if (this.rdbCMS.Checked) { aclU = 1; } else if (this.rdbFSC.Checked) { aclU = 2; } else if (this.rdbG.Checked) { aclU = 3; } else if (this.rdbMSC.Checked) { aclU = 4; } else { aclU = 5; }
                    if (this.rdbGss.Checked) { magU = 1; } else if (this.rdbT.Checked) { magU = 2; } else if (this.rdbMLIT.Checked) { magU = 3; } else if (this.rdbMCT.Checked) { magU = 4; } else { magU = 5; }
                }

                if (this.rdbFT.Checked) { lenUnit = 4; } else { lenUnit = 5; }
                if (this.rdbCMS.Checked) { acelUnit = 1; } else if (this.rdbFSC.Checked) { acelUnit = 2; } else if (this.rdbG.Checked) { acelUnit = 3; } else if (this.rdbMSC.Checked) { acelUnit = 4; } else { acelUnit = 5; }
                if (this.rdbGss.Checked) { magUnit = 1; } else if (this.rdbT.Checked) { magUnit = 2; } else if (this.rdbMLIT.Checked) { magUnit = 3; } else if (this.rdbMCT.Checked) { magUnit = 4; } else { magUnit = 5; }
                optrID = Convert.ToInt32(this.ddlGlobalOperatorList.SelectedValue);
                padID = Convert.ToInt32(this.ddlWellPad.SelectedValue);
                wellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                srvID = AST.getWellRegisteredServiceID(wellID, GetClientDBString);
                wellName = AST.getWellName(wellID, GetClientDBString);
                sectionID = Convert.ToInt32(this.ddlSectionList.SelectedValue);
                RunID = Convert.ToInt32(this.ddlRunList.SelectedValue);

                uploadedData = (List<System.Data.DataTable>)HttpContext.Current.Session["rawUploadedDataTables"];
                selectedValues = (System.Data.DataTable)HttpContext.Current.Session["selectedRawValues"];

                runNameID = AST.getRunNameIDFromRunID(RunID, GetClientDBString);
                runName = AST.getRunName(runNameID);
                counter = QC.getRunCount(RunID, sectionID, wellID, padID, SysClientID, optrID, GetClientDBString);
                iResult = QC.ProcessRawData(optrID, SysClientID, srvID, padID, wellID, sectionID, RunID, dataType, dataEntryMethod, lenUnit, acelUnit, magUnit, dataFileName, uploadedData, selectedValues, LoginName, domain, GetClientDBString);
                wellFlag = Convert.ToInt32(iResult[1]);
                runFlag = Convert.ToInt32(iResult[2]);
                this.liStep7.Attributes["class"] = "stepper-done";
                this.gvwSelectedRaw.Visible = false;
                //this.divTransformData.Visible = true;
                System.Data.DataTable resultsData = QC.getLimitedQCResults(optrID, SysClientID, padID, wellID, sectionID, RunID, Convert.ToDecimal(HttpContext.Current.Session["rDepth"]), GetClientDBString);
                Decimal Depth = -99.00M;
                Depth = Convert.ToDecimal(resultsData.Rows[0]["Depth"]);
                Int32 resID = Convert.ToInt32(resultsData.Rows[0]["resID"]);
                Int32 acID = QC.getAzimuthCriteriaIDForResultRow(resID, GetClientDBString);
                DataTable placeTable = QC.getPlacementTableforSelectedAzimuthCriteria(optrID, padID, wellID, Depth, acID, GetClientDBString);
                System.Data.DataTable azimuthData = QC.getLimitedQCAzimuths(wellID, sectionID, RunID, Convert.ToDecimal(HttpContext.Current.Session["rDepth"]), GetClientDBString);
                this.gvwRQCResults.DataSource = resultsData;
                this.gvwRQCResults.DataBind();
                this.gvwRQCResults.Visible = true;                
                this.gvwRQCAzimuths.DataSource = azimuthData;
                this.gvwRQCAzimuths.DataBind();
                this.gvwRQCAzimuths.Visible = true;                
                this.gvwPlacement.DataSource = placeTable;
                this.gvwPlacement.DataBind();
                this.gvwPlacement.Visible = true;
                GridViewRow wellNameRow = this.gvwWellList.SelectedRow;
                String wName = Convert.ToString(Server.HtmlDecode(wellNameRow.Cells[2].Text));
                this.btnMetaCancel.Visible = false;
                this.btnProcessReadyData.Visible = false;
                this.btnCancelReadyData.Visible = false;

                if (runFlag.Equals(1))
                {
                    this.enRDInfo.Visible = true;
                    this.enRDInfo.Attributes["class"] = "alert alert-info alert-dismissable";
                    this.iRDInfo.Attributes["class"] = "icon fa fa-info-circle";
                    this.spnRDInfo.InnerText = " !!! Alert !!!";
                    this.lblRDInfo.Text = "Reached Run End Depth";
                }
                else if (runFlag.Equals(2))
                {
                    this.enRDInfo.Visible = true;
                    this.enRDInfo.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iRDInfo.Attributes["class"] = "icon fa fa-warning";
                    this.spnRDInfo.InnerText = " !!! Caution !!!";
                    this.lblRDInfo.Text = "Exceeded Run End Depth";
                }
                else
                {
                    this.enRDInfo.Visible = false;
                    this.lblRDInfo.Text = "";
                }
                if (wellFlag.Equals(1))
                {
                    this.enWDInfo.Visible = true;
                    this.enWDInfo.Attributes["class"] = "alert alert-info alert-dismissable";
                    this.iWDInfo.Attributes["class"] = "icon fa fa-info-circle";
                    this.spnWDInfo.InnerText = " !!! Alert !!!";
                    this.lblWDInfo.Text = "Reached Plan Target Depth";
                }
                else if (wellFlag.Equals(2))
                {
                    this.enWDInfo.Visible = true;
                    this.enWDInfo.Attributes["class"] = "alert alert-info alert-dismissable";
                    this.iWDInfo.Attributes["class"] = "icon fa fa-info-circle";
                    this.spnWDInfo.InnerText = " !!! Alert !!!";
                    this.lblWDInfo.Text = "Exceeded Planned Target Depth. (Working within In-Process threshold)";
                }
                else if (wellFlag.Equals(3))
                {
                    this.enWDInfo.Visible = true;
                    this.enWDInfo.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iWDInfo.Attributes["class"] = "icon fa fa-warning";
                    this.spnWDInfo.InnerText = " !!! Alert !!!";
                    this.lblWDInfo.Text = "Exceeded Planned Target Depth and In-Process threshold. Additional Data Processing is restricted";
                    AST.updateWellStatus(optrID, wellID, 1, GetClientDBString); //Demog Well Status
                    Helpers.DemogHelpers.Demographics.updateWellProcessRegistration(optrID, SysClientID, padID, wellID, serviceGroupID, srvID, 1, "SMARTs Platform", GetClientDBString);
                }
                else
                {
                    this.enWDInfo.Visible = false;
                    this.lblWDInfo.Text = "";
                }



                this.btnNextSurvey.Visible = true;
                logger.Info("User: " + username + " QC_Processed for Well/Lateral: " + wName);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCancelReadyData_Click(object sender, EventArgs e)
        {
            try
            {
                this.gvwSelectedRaw.DataSource = null;
                this.gvwSelectedRaw.DataBind();
                this.gvwSelectedRaw.Visible = false;
                this.liStep5.Attributes["class"] = "stepper-todo";
                this.enWDInfo.Visible = false;
                this.lblWDInfo.Text = "";
                this.enRDInfo.Visible = false;
                this.lblRDInfo.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnNextSurvey_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtQCManualProcess.Text = "";
                this.divManData.Visible = true;
                this.divUploadedData.Visible = false;
                this.divTransformData.Visible = false;
                this.gvwRQCResults.Visible = false;
                this.gvwRQCAzimuths.Visible = false;
                this.gvwPlacement.Visible = false;
                this.btnQCManualUpload.Visible = true;
                this.btnCancelManualUpload.Visible = true;
                this.btnNextSurvey.Visible = false;
                HttpContext.Current.Session["selectionsDone"] = 1;
                if (this.rdbFT.Checked) { HttpContext.Current.Session["rdbFT"] = 1; } else { HttpContext.Current.Session["rdbFT"] = 0; }
                if (this.rdbMT.Checked) { HttpContext.Current.Session["rdbMT"] = 1; } else { HttpContext.Current.Session["rdbMT"] = 0; }
                if (this.rdbCMS.Checked) { HttpContext.Current.Session["rdbCMS"] = 1; } else { HttpContext.Current.Session["rdbCMS"] = 0; }
                if (this.rdbFSC.Checked) { HttpContext.Current.Session["rdbFSC"] = 1; } else { HttpContext.Current.Session["rdbFSC"] = 0; }
                if (this.rdbG.Checked) { HttpContext.Current.Session["rdbG"] = 1; } else { HttpContext.Current.Session["rdbG"] = 0; }
                if (this.rdbMSC.Checked) { HttpContext.Current.Session["rdbMSC"] = 1; } else { HttpContext.Current.Session["rdbMSC"] = 0; }
                if (this.rdbMG.Checked) { HttpContext.Current.Session["rdbMG"] = 1; } else { HttpContext.Current.Session["rdbMG"] = 0; }
                if (this.rdbGss.Checked) { HttpContext.Current.Session["rdbGss"] = 1; } else { HttpContext.Current.Session["rdbGss"] = 0; }
                if (this.rdbT.Checked) { HttpContext.Current.Session["rdbT"] = 1; } else { HttpContext.Current.Session["rdbT"] = 0; }
                if (this.rdbMLIT.Checked) { HttpContext.Current.Session["rdbMLIT"] = 1; } else { HttpContext.Current.Session["rdbMLIT"] = 0; }
                if (this.rdbMCT.Checked) { HttpContext.Current.Session["rdbMCT"] = 1; } else { HttpContext.Current.Session["rdbMCT"] = 0; }
                if (this.rdbNT.Checked) { HttpContext.Current.Session["rdbNT"] = 1; } else { HttpContext.Current.Session["rdbNT"] = 0; }
                if (this.chk_NoSignTransformation.Checked) { HttpContext.Current.Session["chk_NoSignTransformation"] = 1; } else { HttpContext.Current.Session["chk_NoSignTransformation"] = 0; }
                if (this.chk_FlipGx.Checked) { HttpContext.Current.Session["chk_FlipGx"] = 1; } else { HttpContext.Current.Session["chk_FlipGx"] = 0; }
                if (this.chk_FlipGy.Checked) { HttpContext.Current.Session["chk_FlipGy"] = 1; } else { HttpContext.Current.Session["chk_FlipGy"] = 0; }
                if (this.chk_FlipGz.Checked) { HttpContext.Current.Session["chk_FlipGz"] = 1; } else { HttpContext.Current.Session["chk_FlipGz"] = 0; }
                if (this.chk_FlipBx.Checked) { HttpContext.Current.Session["chk_FlipBx"] = 1; } else { HttpContext.Current.Session["chk_FlipBx"] = 0; }
                if (this.chk_FlipBy.Checked) { HttpContext.Current.Session["chk_FlipBy"] = 1; } else { HttpContext.Current.Session["chk_FlipBy"] = 0; }
                if (this.chk_FlipBz.Checked) { HttpContext.Current.Session["chk_FlipBz"] = 1; } else { HttpContext.Current.Session["chk_FlipBz"] = 0; }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwReadyRawData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable rawTable = new System.Data.DataTable();
                List<System.Data.DataTable> selTables = new List<System.Data.DataTable>();
                selTables = (List<System.Data.DataTable>)HttpContext.Current.Session["rawDataTables"];
                rawTable = (System.Data.DataTable)selTables[3];
                this.gvwReadyRawData.DataSource = rawTable;
                this.gvwReadyRawData.PageIndex = e.NewPageIndex;
                this.gvwReadyRawData.SelectedIndex = -1;
                this.gvwReadyRawData.DataBind();
                this.liStep5.Attributes["class"] = "stepper-done";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSelectedRaw_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable selectedRawTable = new System.Data.DataTable();
                selectedRawTable = (System.Data.DataTable)HttpContext.Current.Session["selectedRawValues"];
                this.gvwViewRawDisplay.Visible = false;
                this.gvwSelectedRaw.DataSource = selectedRawTable;
                this.gvwSelectedRaw.PageIndex = e.NewPageIndex;
                this.gvwSelectedRaw.SelectedIndex = -1;
                this.gvwSelectedRaw.DataBind();
                this.gvwSelectedRaw.Visible = true;
                this.liStep5.Attributes["class"] = "stepper-todo";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSelectedRaw_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = ((DataRowView)e.Row.DataItem).Row;
                    CheckBox chkSelected = e.Row.FindControl("srvyCheck") as CheckBox;
                    if (dr["chk"].Equals(1))
                    {
                        e.Row.BackColor = Color.LightGreen;
                        chkSelected.Checked = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbFT_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
                this.rdbMT.Checked = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbMT_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
                this.rdbFT.Checked = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbCMS_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
                this.rdbFSC.Checked = false;
                this.rdbG.Checked = false;
                this.rdbMSC.Checked = false;
                this.rdbMG.Checked = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbFSC_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
                this.rdbCMS.Checked = false;
                this.rdbG.Checked = false;
                this.rdbMSC.Checked = false;
                this.rdbMG.Checked = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbG_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
                this.rdbCMS.Checked = false;
                this.rdbFSC.Checked = false;
                this.rdbMSC.Checked = false;
                this.rdbMG.Checked = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbMSC_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
                this.rdbCMS.Checked = false;
                this.rdbFSC.Checked = false;
                this.rdbG.Checked = false;
                this.rdbMG.Checked = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbMG_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
                this.rdbCMS.Checked = false;
                this.rdbFSC.Checked = false;
                this.rdbG.Checked = false;
                this.rdbMSC.Checked = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbGss_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
                this.rdbT.Checked = false;
                this.rdbMLIT.Checked = false;
                this.rdbMCT.Checked = false;
                this.rdbNT.Checked = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbT_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
                this.rdbGss.Checked = false;
                this.rdbMLIT.Checked = false;
                this.rdbMCT.Checked = false;
                this.rdbNT.Checked = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbMLIT_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
                this.rdbGss.Checked = false;
                this.rdbT.Checked = false;
                this.rdbMCT.Checked = false;
                this.rdbNT.Checked = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbMCT_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
                this.rdbGss.Checked = false;
                this.rdbT.Checked = false;
                this.rdbMLIT.Checked = false;
                this.rdbNT.Checked = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbNT_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnProcessSrvyCancel.Enabled = true;
                this.btnProcessSrvyCancel.CssClass = "btn btn-warning text-center";
                this.rdbGss.Checked = false;
                this.rdbT.Checked = false;
                this.rdbMLIT.Checked = false;
                this.rdbMCT.Checked = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRQCResults_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = ((DataRowView)e.Row.DataItem).Row;
                    e.Row.Cells[5].BackColor = Color.FromName(Convert.ToString(dr["dipQ"]));
                    e.Row.Cells[6].BackColor = Color.FromName(Convert.ToString(dr["btQ"]));
                    e.Row.Cells[7].BackColor = Color.FromName(Convert.ToString(dr["gtQ"]));
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}