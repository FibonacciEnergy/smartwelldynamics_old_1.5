﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlSMARTs_2A.ascx.cs" Inherits="SmartsVer1.Control.Services.ctrlSMARTs_2A" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>

<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript for ref lib -->
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>
<!--- local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }

    .rotate {
        -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
        -moz-transform: rotate(90deg); /* Firefox */
        -ms-transform: rotate(90deg); /* IE 9 */
        transform: rotate(90deg); /* Standard syntax */
    }
    /*custom anchor defi for menu items*/
    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }
</style>
<!-- Javascript for accordion -->
<script type="text/javascript">
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("rotate");
        });
        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").addClass("rotate");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("rotate");
        });
    });
</script>
<!--- Page Menu -->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!-- Breadcrumb -->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../Viewer/fesViewer.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>SMARTs</li>
                <li><a href="../../Services/fesDRServices_2A.aspx">SMARTs Services</a></li>
            </ol>            
        </div>
    </div>
</div>
<!--- Page Content --->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div id="outerAcrd" runat="server" class="panel-group">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancSvcs" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlSMARTs_2A_outerAcrd"
                                href="#BodyContent_ctrlSMARTs_2A_divSrvcs"><span class="glyphicon glyphicon-menu-right"></span> SMARTs Service</a>
                        </h5>
                    </div>
                    <div id="divSrvcs" runat="server" class="panel-collapse collapse in">
                        <div class="panel panel-body">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <asp:UpdateProgress ID="upgrsSrvs" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlSrvs">
                                    <ProgressTemplate>
                                        <div id="divQCSrvsProgress" runat="server" class="updateProgress">                                    
                                        <asp:Image ID="imgQCSrvsProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:UpdatePanel ID="upnlSrvs" runat="server">
                                    <ContentTemplate>
                                        <div class="input-group" style="width: 100%;">
                                            <asp:LinkButton ID="btnNewSrv" runat="server" CssClass="btn btn-info text-center" OnClick="btnNewSrv_Click"><i class="fa fa-plus"> Service</i></asp:LinkButton>
                                            <asp:LinkButton ID="btnSrvClear" runat="server" Enabled="false" CssClass="btn btn-default text-center" OnClick="btnSrvClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                        </div>
                                        <div id="acrdServices" runat="server" class="panel-group">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h5 class="panel-title" style="text-align: left">
                                                        <a id="a1" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlSMARTs_2A_acrdServices"
                                                            href="#BodyContent_ctrlSMARTs_2A_divSrvcsList">SMARTs Service(s) List</a>
                                                    </h5>
                                                </div>
                                                <div id="divSrvcsList" runat="server" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <asp:GridView ID="gvwSrvGrp" runat="server" DataKeyNames="srvGrpID" AllowPaging="True" AutoGenerateColumns="false"
                                                                CssClass="mydatagrid" OnPageIndexChanging="gvwSrvGrp_PageIndexChanging"
                                                                AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                RowStyle-CssClass="rows" OnSelectedIndexChanged="gvwSrvGrp_SelectedIndexChanged">
                                                                <Columns>
                                                                    <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                        <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                    </asp:ButtonField>
                                                                    <asp:BoundField DataField="srvGrpID" Visible="False" />
                                                                    <asp:BoundField DataField="srvGroupLabel" HeaderText="SMARTs Service Group Title" />
                                                                    <asp:BoundField DataField="srvGrpCount" HeaderText="Associated Services" />
                                                                </Columns>
                                                            </asp:GridView>
                                                            <asp:GridView ID="gvwSrvGrpService" runat="server" DataKeyNames="srvID" AllowPaging="True" AutoGenerateColumns="false"
                                                                CssClass="mydatagrid" OnPageIndexChanging="gvwSrvGrpService_PageIndexChanging"
                                                                AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                RowStyle-CssClass="rows" Visible="false">
                                                                <Columns>
                                                                    <asp:BoundField DataField="srvID" HeaderText="srvID" Visible="False" />
                                                                    <asp:BoundField DataField="srvName" HeaderText="SMARTs Service Name" />
                                                                    <asp:BoundField DataField="srvStatID" HeaderText="Status" />
                                                                    <asp:BoundField DataField="srvVersion" HeaderText="Version" />
                                                                    <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-success">
                                                <div class="panel-heading">
                                                    <h5 class="panel-title" style="text-align: left">
                                                        <a id="a2" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlSMARTs_2A_acrdServices"
                                                            href="#BodyContent_ctrlSMARTs_2A_divAddSrvcs">Add SMARTs Service</a>
                                                    </h5>
                                                </div>
                                                <div id="divAddSrvcs" runat="server" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <asp:GridView ID="gvwSrvGrpAdd" runat="server" DataKeyNames="srvGrpID" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="false"
                                                                CssClass="mydatagrid" OnPageIndexChanging="gvwSrvGrpAdd_PageIndexChanging"
                                                                AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                RowStyle-CssClass="rows" OnSelectedIndexChanged="gvwSrvGrpAdd_SelectedIndexChanged">
                                                                <Columns>
                                                                    <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                        <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                    </asp:ButtonField>
                                                                    <asp:BoundField DataField="srvGrpID" Visible="False" />
                                                                    <asp:BoundField DataField="srvGroupLabel" HeaderText="SMARTs Service Group Title" />
                                                                    <asp:BoundField DataField="srvGrpCount" HeaderText="Associated Services" />
                                                                </Columns>
                                                            </asp:GridView>
                                                            <div id="divGrpParam" runat="server" visible="false">
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <asp:TextBox runat="server" ID="txtSASrvName" CssClass="form-control" Text="" placeholder="Service Name" />
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <asp:TextBox runat="server" ID="txtSASrvVersion" CssClass="form-control" Text="" placeholder="Version #" />
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <asp:DropDownList ID="ddlSASrvStatus" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                                        OnSelectedIndexChanged="ddlSASrvStatus_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select SMARTs Service Status ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div id="gpEnclosure" runat="server" visible="false">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                    <h4><i runat="server" id="iGP"><span runat="server" id="spnGP"></span></i></h4>
                                                                    <asp:Label runat="server" ID="lblSrvSuccess" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="input-group" style="width: 100%;">
                                                                    <asp:LinkButton runat="server" ID="btnAddNewSrv" CssClass="btn btn-default text-center" Enabled="false"
                                                                        OnClick="btnAddNewSrv_Click"><i class="fa fa-plus"> Add Service</i></asp:LinkButton>
                                                                    <asp:LinkButton runat="server" ID="btnAddNewClear" CssClass="btn btn-default text-center" Enabled="false"
                                                                        OnClick="btnAddNewClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                    <asp:LinkButton runat="server" ID="btnSrvDone" CssClass="btn btn-danger text-center" OnClick="btnSrvDone_Click"><i
                                                                        class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancStat" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlSMARTs_2A_outerAcrd"
                                href="#BodyContent_ctrlSMARTs_2A_divSrvStat"><span class="glyphicon glyphicon-menu-right"></span> Service Status</a>
                        </h5>
                    </div>
                    <div id="divSrvStat" runat="server" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <asp:UpdateProgress ID="uprgrsSrvsStatus" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlSrvsStatus">
                                    <ProgressTemplate>
                                        <div id="divQCStatProgress" runat="server" class="updateProgress">                                    
                                        <asp:Image ID="imgQCStatProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:UpdatePanel ID="upnlSrvsStatus" runat="server">
                                    <ContentTemplate>
                                        <div id="acrdStat" runat="server" class="panel-group">
                                            <div class="input-group">
                                                <asp:LinkButton ID="btnStatAdd" runat="server" CssClass="btn btn-info text-center" OnClick="btnStatAdd_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h5 class="panel-title" style="text-align: left">
                                                        <a id="ancStatList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlSMARTs_2A_acrdStat"
                                                            href="#BodyContent_ctrlSMARTs_2A_divSrvcsStat"> SMARTs Status List</a>
                                                    </h5>
                                                </div>
                                                <div id="divSrvcsStat" runat="server" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        <asp:GridView ID="gvwSrvStatList" runat="server" DataKeyNames="srvstatID" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="false"
                                                            CssClass="mydatagrid" OnPageIndexChanging="gvwSrvStatList_PageIndexChanging"
                                                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                            RowStyle-CssClass="rows">
                                                            <Columns>
                                                                <asp:BoundField DataField="srvstatID" Visible="False" />
                                                                <asp:BoundField DataField="srvStatus" HeaderText="SMARTs Service Status" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-success">
                                                <div class="panel-heading">
                                                    <h5 class="panel-title" style="text-align: left">
                                                        <a id="a4" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlSMARTs_2A_acrdStat"
                                                            href="#BodyContent_ctrlSMARTs_2A_divSrvcsStatAdd">Add SMARTs Service Status</a>
                                                    </h5>
                                                </div>
                                                <div id="divSrvcsStatAdd" runat="server" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                            <asp:Label ID="lblStatAdd" runat="server" CssClass="control-label" for="txtStatAdd" Text="SMART Service Status" />
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                                                            <asp:TextBox ID="txtStatAdd" runat="server" CssClass="form-control" Text="" placeholder="SMART Service Status" />
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div runat="server" id="enSrvStat" visible="false">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                <h4><i runat="server" id="iSrvStat"><span runat="server" id="spnSrvStat"></span></i></h4>
                                                                <asp:Label ID="lblSrvStat" runat="server" Text="" />
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="input-group">
                                                                <asp:LinkButton ID="btnAddSrvStat" runat="server" CssClass="btn btn-success text-center" OnClick="btnAddSrvStat_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnAddSrvStatCancel" runat="server" CssClass="btn btn-warning text-center" OnClick="btnAddSrvStatCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnAddSrvStatDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnAddSrvStatDone_Click"><i class="fa fa-times-rectangle"> Done</i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancCUnit" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlSMARTs_2A_outerAcrd"
                                href="#BodyContent_ctrlSMARTs_2A_divChargeUnit"><span class="glyphicon glyphicon-menu-right"></span> Service Charge Units</a>
                        </h5>
                    </div>
                    <div id="divChargeUnit" runat="server" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <asp:UpdateProgress ID="uprgrsCharge" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlCharge">
                                    <ProgressTemplate>
                                        <div id="divChargeProgress" runat="server" class="updateProgress">                                    
                                        <asp:Image ID="imgChargeProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:UpdatePanel ID="upnlCharge" runat="server">
                                    <ContentTemplate>
                                        <div class="input-group">
                                            <asp:LinkButton ID="btnSrvChg" runat="server" CssClass="btn btn-info text-center" OnClick="btnSrvChg_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                        </div>
                                        <div id="acrdCharge" runat="server" class="panel-group">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h5 class="panel-title" style="text-align: left">
                                                        <a id="ancChgList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlSMARTs_2A_acrdCharge"
                                                            href="#BodyContent_ctrlSMARTs_2A_divChargeList"> SMARTs Service Charge Units List</a>
                                                    </h5>
                                                </div>
                                                <div id="divChargeList" runat="server" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        <asp:GridView ID="gvwSrvChgUnit" runat="server" DataKeyNames="scuID" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="false"
                                                            CssClass="mydatagrid" OnPageIndexChanging="gvwSrvChgUnit_PageIndexChanging"
                                                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                            RowStyle-CssClass="rows">
                                                            <Columns>
                                                                <asp:BoundField DataField="scuID" Visible="False" />
                                                                <asp:BoundField DataField="scuName" HeaderText="SMARTs Service Status" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-success">
                                                <div class="panel-heading">
                                                    <h5 class="panel-title" style="text-align: left">
                                                        <a id="ancChgAdd" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlSMARTs_2A_acrdCharge"
                                                            href="#BodyContent_ctrlSMARTs_2A_divChargeAdd">Add Service Charge Unit</a>
                                                    </h5>
                                                </div>
                                                <div id="divChargeAdd" runat="server" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                            <asp:Label ID="lblChgUnit" runat="server" CssClass="control-label" for="txtChgUnit" Text="Service Charge Unit Value" />
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                                                            <asp:TextBox ID="txtChgUnit" runat="server" CssClass="form-control" Text="" placeholder="Service Charge Unit" />
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div runat="server" id="enSrvChg" visible="false">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                <h4><i runat="server" id="iSrvChg"><span runat="server" id="spnSrvChg"></span></i></h4>
                                                                <asp:Label ID="lblSrvChgSuccess" runat="server" Text="" />
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="input-group">
                                                                <asp:LinkButton ID="btnAddCharge" runat="server" CssClass="btn btn-success text-center" OnClick="btnAddCharge_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnAddChargeCancel" runat="server" CssClass="btn btn-warning text-center" OnClick="btnAddChargeCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnAddChargeDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnAddChargeDone_Click"><i class="fa fa-times-rectangle"> Done</i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancPrice" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlSMARTs_2A_outerAcrd"
                                href="#BodyContent_ctrlSMARTs_2A_divPrices"><span class="glyphicon glyphicon-menu-right"></span> Standard Service Prices</a>
                        </h5>
                    </div>
                    <div id="divPrices" runat="server" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <asp:UpdateProgress ID="uprgrsPrice" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlPrice">
                                    <ProgressTemplate>
                                        <div id="divPriceProgress" runat="server" class="updateProgress">                                    
                                        <asp:Image ID="imgPriceProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:UpdatePanel ID="upnlPrice" runat="server">
                                    <ContentTemplate>
                                        <div id="acrdPrice" runat="server" class="panel-group">
                                            <div class="input-group">
                                                <asp:LinkButton ID="btnAddSrvPrice" runat="server" CssClass="btn btn-info text-center" OnClick="btnAddSrvPrice_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h5 class="panel-title" style="text-align: left">
                                                        <a id="ancPList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlSMARTs_2A_acrdPrice"
                                                            href="#BodyContent_ctrlSMARTs_2A_divPriceList"> SMARTs Service Price List</a>
                                                    </h5>
                                                </div>
                                                <div id="divPriceList" runat="server" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        <asp:GridView ID="gvwStdPriceList" runat="server" DataKeyNames="stpID" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="false"
                                                            CssClass="mydatagrid" OnPageIndexChanging="gvwStdPriceList_PageIndexChanging"
                                                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                            RowStyle-CssClass="rows">
                                                            <Columns>
                                                                <asp:BoundField DataField="stpID" Visible="False" />
                                                                <asp:BoundField DataField="srvGrpID" HeaderText="Service Group" />
                                                                <asp:BoundField DataField="srvID" HeaderText="Service" />
                                                                <asp:BoundField DataField="stpPrice" HeaderText="Standard Price" />
                                                                <asp:BoundField DataField="scuID" HeaderText="Price Unit" />                                                             
                                                                <asp:BoundField DataField="stpStart" HeaderText="Start Date" />
                                                                <asp:BoundField DataField="stpEnd" HeaderText="End Date" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-success">
                                                <div class="panel-heading">
                                                    <h5 class="panel-title" style="text-align: left">
                                                        <a id="ancPAdd" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlSMARTs_2A_acrdPrice"
                                                            href="#BodyContent_ctrlSMARTs_2A_divPriceAdd">Add Service Price</a>
                                                    </h5>
                                                </div>
                                                <div id="divPriceAdd" runat="server" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="text-align: right;">
                                                                <asp:Label ID="lblPriceGroup" runat="server" CssClass="control-label" Font-Size="Large" for="txtPrice" Text="Service Group" />
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                                <asp:DropDownList ID="ddlPriceGroup" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlPriceGroup_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0" Text="--- Select Service Group ---"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="text-align: right;">
                                                                <asp:Label ID="lblPriceService" runat="server" CssClass="control-label" Font-Size="Large" for="txtPrice" Text="Service" />
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                                <asp:DropDownList ID="ddlPriceService" runat="server" CssClass="form-control" AutoPostBack="true" Enabled="false" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlPriceService_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0" Text="--- Select Service ---"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="text-align: right;">
                                                                <asp:Label ID="lblPrice" runat="server" CssClass="control-label" Font-Size="Large" for="txtPrice" Text="Service Price" />
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                                <div class="input-group" style="width: 100%;">
                                                                    <span class="input-group-addon">US$</span>
                                                                    <asp:TextBox ID="txtPrice" runat="server" Text="" Enabled="false" Font-Size="Large" TextMode="Number" class="form-control"
                                                                        Width="100%" placeholder="(US$ XXXX.XX)" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="text-align: right;">
                                                                <asp:Label ID="lblPUnit" runat="server" CssClass="control-label" Font-Size="Large" for="txtPrice" Text="Service Price Unit" />
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                                <asp:DropDownList ID="ddlPriceUnit" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true" Enabled="false" 
                                                                    OnSelectedIndexChanged="ddlPriceUnit_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0" Text="--- Select Price Unit ---" />
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="text-align: right;">
                                                            <asp:Label ID="lblPriceStart" runat="server" CssClass="control-label" Font-Size="Large" for="txtPriceStart" Text="Price Start Date" />
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="text-align: right;">
                                                            <div class="input-group" style="width: 100%;">
                                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                <asp:TextBox ID="txtPriceStart" runat="server" Text="" Enabled="false" Font-Size="Large" TextMode="DateTime" class="form-control" Width="100%" />
                                                                <ajaxToolkit:CalendarExtender ID="clndrStart" runat="server" Animated="true" TargetControlID="txtPriceStart" />
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="text-align: right;">
                                                            <asp:Label ID="lblPriceEnd" runat="server" CssClass="control-label" Font-Size="Large" for="txtPriceEnd" Text="Price End Date" />
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="text-align: right;">
                                                            <div class="input-group" style="width: 100%;">
                                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                <asp:TextBox ID="txtPriceEnd" runat="server" Text="" Enabled="false" Font-Size="Large" TextMode="DateTime" class="form-control" Width="100%" AutoPostBack="true" OnTextChanged="clndrEnd_SelectedDateChanged" />
                                                                <ajaxToolkit:CalendarExtender ID="clndrEnd" runat="server" Animated="true" TargetControlID="txtPriceEnd" />
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div runat="server" id="enPrice" visible="false">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                <h4><i runat="server" id="iPrice"><span runat="server" id="spnPrice"></span></i></h4>
                                                                <asp:Label ID="lblPriceSuccess" runat="server" Text="" />
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="input-group">
                                                                <asp:LinkButton ID="btnPriceAdd" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnPriceAdd_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnPriceAddCancel" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnPriceAddCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnPriceDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnPriceDone_Click"><i class="fa fa-times-rectangle"> Done</i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancMD" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlSMARTs_2A_outerAcrd"
                                href="#BodyContent_ctrlSMARTs_2A_divMDLimits"><span class="glyphicon glyphicon-menu-right"></span> Audit/QC Measured Depth Processing Limits</a>
                        </h5>
                    </div>
                    <div id="divMDLimits" runat="server" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <asp:UpdateProgress ID="uprgrsMD" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlMD">
                                    <ProgressTemplate>
                                        <div id="divMDProgress" runat="server" class="updateProgress">                                    
                                        <asp:Image ID="imgMDProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:UpdatePanel ID="upnlMD" runat="server">
                                    <ContentTemplate>
                                        <div id="acrdMD" runat="server" class="panel-group">
                                            <div class="input-group">
                                                <asp:LinkButton ID="btnLimitNew" runat="server" CssClass="btn btn-info text-center" OnClick="btnLimitNew_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h5 class="panel-title" style="text-align: left">
                                                        <a id="ancMDList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlSMARTs_2A_acrdMD"
                                                            href="#BodyContent_ctrlSMARTs_2A_divMDList"> Audit/QC Measured Depth Limits</a>
                                                    </h5>
                                                </div>
                                                <div id="divMDList" runat="server" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        <asp:GridView ID="gvwMDLimits" runat="server" DataKeyNames="mdpID" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="false"
                                                            CssClass="mydatagrid" OnPageIndexChanging="gvwMDLimits_PageIndexChanging"
                                                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                            RowStyle-CssClass="rows" EmptyDataText="No Measured Depth Processing Limit(s) defined" ShowHeaderWhenEmpty="true">
                                                            <Columns>
                                                                <asp:BoundField DataField="mdpID" Visible="False" />
                                                                <asp:BoundField DataField="mdpValue" HeaderText="MD Processing Limit (%)" ItemStyle-HorizontalAlign="Center" ControlStyle-Font-Bold="true" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-success">
                                                <div class="panel-heading">
                                                    <h5 class="panel-title" style="text-align: left">
                                                        <a id="ancMDAdd" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlSMARTs_2A_acrdMD"
                                                            href="#BodyContent_ctrlSMARTs_2A_divMDAdd"> Add Audit/QC Measured Depth Limit</a>
                                                    </h5>
                                                </div>
                                                <div id="divMDAdd" runat="server" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                            <asp:Label ID="lblMDLim" runat="server" CssClass="control-label" for="txtMDLim" Text="MD Processing Limit" />
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                                                            <asp:TextBox ID="txtMDLim" runat="server" CssClass="form-control" Text="" TextMode="Number" placeholder="Measured Depth Processing Limit" />
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div runat="server" id="enLim" visible="false">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                <h4><i runat="server" id="iLim"><span runat="server" id="spnLim"></span></i></h4>
                                                                <asp:Label ID="lblLimSuccess" runat="server" Text="" />
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="input-group">
                                                                <asp:LinkButton ID="btnLimAdd" runat="server" CssClass="btn btn-success text-center" OnClick="btnLimAdd_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnLimAddCancel" runat="server" CssClass="btn btn-warning text-center" OnClick="btnLimAddCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnLimDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnLimDone_Click"><i class="fa fa-times-rectangle"> Done</i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>                                