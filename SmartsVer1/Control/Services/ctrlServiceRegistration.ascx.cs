﻿using log4net;
using System;
using System.Data;
using System.Net.Mail;
using System.Collections.Generic;
using System.Web.Security;
using System.Web.UI.WebControls;
using CLR = System.Drawing.Color;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using CTRT = SmartsVer1.Helpers.AccountHelpers.CreateContract;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;

namespace SmartsVer1.Control.Services
{
    public partial class ctrlServiceRegistration : System.Web.UI.UserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ctrlServiceRegistration));
        String LoginName, username, domain, GetClientDBString;
        Int32 SysClientID = -99;        

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                SysClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(SysClientID, LoginName);
                //DataSource for page Controls
                System.Data.DataTable rrTable = DMG.getGlobalWellRegistrationRequest();
                this.gvwRegReqList.DataSource = rrTable;
                this.gvwRegReqList.PageIndex = 0;
                this.gvwRegReqList.SelectedIndex = -1;
                this.gvwRegReqList.DataBind();                
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {   }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRegReqList_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable rrTable = DMG.getGlobalWellRegistrationRequest();
                this.gvwRegReqList.DataSource = rrTable;
                this.gvwRegReqList.PageIndex = e.NewPageIndex;
                this.gvwRegReqList.SelectedIndex = -1;
                this.gvwRegReqList.DataBind();
                this.btnRRListClear.Enabled = true;
                this.btnRRListClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRegReqList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 wsrID = Convert.ToInt32(this.gvwRegReqList.SelectedValue);
                System.Data.DataTable rrTable = DMG.getGlobalWellRegistrationRequesterDetail(wsrID);
                this.gvwRegReqDetail.DataSource = rrTable;
                this.gvwRegReqDetail.PageIndex = 0;
                this.gvwRegReqDetail.SelectedIndex = -1;
                this.gvwRegReqDetail.DataBind();
                this.gvwRegReqDetail.Visible = true;
                this.lblRegReqDetail.Visible = true;
                System.Data.DataTable apTable = DMG.getGlobalWellRegistrationApproverDetail(wsrID);
                this.gvwRegAppDetail.DataSource = apTable;
                this.gvwRegAppDetail.PageIndex = 0;
                this.gvwRegAppDetail.SelectedIndex = -1;
                this.gvwRegAppDetail.DataBind();
                this.gvwRegAppDetail.Visible = true;
                this.lblRegAppDetail.Visible = true;
                GridViewRow dRow = this.gvwRegReqList.SelectedRow;
                Int32 ClientID = Convert.ToInt32(Server.HtmlDecode(dRow.Cells[2].Text));
                System.Data.DataTable cttTable = CTRT.getClientContractTable(ClientID);
                this.gvwContractsList.DataSource = cttTable;
                this.gvwContractsList.PageIndex = 0;
                this.gvwContractsList.DataBind();
                this.gvwContractsList.Visible = true;
                this.lblRRContract.Visible = true;
                this.btnRRListClear.Enabled = true;
                this.btnRRListClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRegReqList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {                    
                    DataRow dr = ((DataRowView)e.Row.DataItem).Row;
                    String cClr = Convert.ToString(dr["cApp"]);
                    e.Row.Cells[8].BackColor = CLR.FromName(cClr);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRegAppDetail_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = ((DataRowView)e.Row.DataItem).Row;
                    String cClr = Convert.ToString(dr["appClr"]);
                    e.Row.Cells[4].BackColor = CLR.FromName(cClr);                    
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRRListClear_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable rrTable = DMG.getGlobalWellRegistrationRequest();
                this.gvwRegReqList.DataSource = rrTable;
                this.gvwRegReqList.PageIndex = 0;
                this.gvwRegReqList.SelectedIndex = -1;
                this.gvwRegReqList.DataBind();
                this.lblRRContract.Visible = false;
                this.gvwContractsList.DataSource = null;
                this.gvwContractsList.PageIndex = 0;
                this.gvwContractsList.DataBind();
                this.gvwContractsList.Visible = false;
                this.lblRegReqDetail.Visible = false;
                this.gvwRegReqDetail.DataSource = null;
                this.gvwRegReqDetail.DataBind();
                this.gvwRegReqDetail.Visible = false;
                this.lblRegAppDetail.Visible = false;
                this.gvwRegAppDetail.DataSource = null;
                this.gvwRegAppDetail.DataBind();
                this.gvwRegAppDetail.Visible = false;                
                this.divLReqList.Attributes["class"] = "panel-collapse collapse in";
                this.divReqProc.Attributes["class"] = "panel-collapse collapse";
                this.btnRRListClear.Enabled = false;
                this.btnRRListClear.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwContractsList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow drRes = ((DataRowView)e.Row.DataItem).Row;

                    DateTime sD = Convert.ToDateTime(drRes["ctrtStart"]);
                    DateTime eD = Convert.ToDateTime(drRes["ctrtEnd"]);
                    Double ttlDays = -99;
                    Double rmgDays = -99;
                    ttlDays = (eD - sD).TotalDays;
                    rmgDays = (eD - DateTime.Now).TotalDays;
                    if (rmgDays < 0)
                    {
                        rmgDays = 0;
                    }

                    Double difference = Convert.ToDouble((eD - DateTime.Now).TotalDays);

                    Label lblTDays = e.Row.Cells[7].FindControl("lblDttl") as Label;
                    lblTDays.Text = ttlDays.ToString();
                    Label lblRDays = e.Row.Cells[8].FindControl("lblDrm") as Label;
                    lblRDays.Text = Math.Round(rmgDays, 2).ToString();

                    if (eD < DateTime.Now)
                    {
                        e.Row.BackColor = CLR.Red;
                        e.Row.ForeColor = CLR.White;
                        lblTDays.ForeColor = CLR.White;
                        lblRDays.ForeColor = CLR.White;
                    }
                    else if (difference < 60)
                    {
                        e.Row.BackColor = CLR.LightYellow;
                    }
                    else
                    {
                        e.Row.BackColor = CLR.LightGreen;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRRProcess_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnRRListClear_Click(null, null);
                this.divLReqList.Attributes["class"] = "panel-collapse collapse";
                this.divReqProc.Attributes["class"] = "panel-collapse collapse in";
                List<Int32> RequestingClients = DMG.getRegReqClientsList();
                Dictionary<Int32, String> clntList = CLNT.getRegReqClientList(RequestingClients); //Client
                this.ddlClientList.Items.Clear();
                this.ddlClientList.DataSource = clntList;
                this.ddlClientList.DataTextField = "Value";
                this.ddlClientList.DataValueField = "Key";
                this.ddlClientList.DataBind();
                this.ddlClientList.Items.Insert(0, new ListItem("--- Select SMARTs Client Company ---", "-1"));
                this.ddlClientList.SelectedIndex = -1;
                this.btnRRListClear.Enabled = true;
                this.btnRRListClear.CssClass = "btn btn-warning text-center";                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlClientList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 clntID = Convert.ToInt32(this.ddlClientList.SelectedValue);
                Dictionary<Int32, String> rrqList = DMG.getWellRegReqList(clntID);
                this.ddlRegReqList.Items.Clear();
                this.ddlRegReqList.DataSource = rrqList;
                this.ddlRegReqList.DataTextField = "Value";
                this.ddlRegReqList.DataValueField = "Key";
                this.ddlRegReqList.DataBind();
                this.ddlRegReqList.Items.Insert(0, new ListItem("--- Select Registration Request ---", "-1"));
                this.ddlRegReqList.SelectedIndex = -1;
                this.ddlRegReqList.Enabled = true;
                this.btnRegWellReset.Enabled = true;
                this.btnRegWellReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlRegReqList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 clntID = Convert.ToInt32(this.ddlClientList.SelectedValue);
                Dictionary<Int32, String> ctpList = CTRT.getContractTypeList();
                this.ddlRegReqContractType.Items.Clear();
                this.ddlRegReqContractType.DataSource = ctpList;
                this.ddlRegReqContractType.DataTextField = "Value";
                this.ddlRegReqContractType.DataValueField = "Key";
                this.ddlRegReqContractType.DataBind();
                this.ddlRegReqContractType.Items.Insert(0, new ListItem("--- Select Client Contract Type ---", "-1"));
                this.ddlRegReqContractType.SelectedIndex = -1;
                this.ddlRegReqContractType.Enabled = true;
                this.btnRegWellReset.Enabled = true;
                this.btnRegWellReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlRegReqContractType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 clntID = Convert.ToInt32(this.ddlClientList.SelectedValue);
                Int32 ctypID = Convert.ToInt32(this.ddlRegReqContractType.SelectedValue);
                Dictionary<Int32, String> cttList = CTRT.getClientContractList(ctypID, clntID);
                this.ddlRegReqContract.Items.Clear();
                this.ddlRegReqContract.DataSource = cttList;
                this.ddlRegReqContract.DataTextField = "Value";
                this.ddlRegReqContract.DataValueField = "Key";
                this.ddlRegReqContract.DataBind();
                this.ddlRegReqContract.Items.Insert(0, new ListItem("--- Select Client Contract ---", "-1"));
                this.ddlRegReqContract.SelectedIndex = -1;
                this.ddlRegReqContract.Enabled = true;
                this.btnRegWellReset.Enabled = true;
                this.btnRegWellReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlRegReqContract_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 wsrID = Convert.ToInt32(this.ddlRegReqList.SelectedValue);
                Int32 ctrtID = Convert.ToInt32(this.ddlRegReqContract.SelectedValue);
                Dictionary<Int32, String> pList = DMG.getRegReqPriceList(wsrID, ctrtID);
                this.ddlRegReqPrice.Items.Clear();
                this.ddlRegReqPrice.DataSource = pList;
                this.ddlRegReqPrice.DataTextField = "Value";
                this.ddlRegReqPrice.DataValueField = "Key";
                this.ddlRegReqPrice.DataBind();
                this.ddlRegReqPrice.Items.Insert(0, new ListItem("--- Select Contracted Price ---", "-1"));
                this.ddlRegReqPrice.SelectedIndex = -1;
                this.ddlRegReqPrice.Enabled = true;
                this.btnRegWellReset.Enabled = true;
                this.btnRegWellReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlRegReqPrice_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtRegReqApproval.Text = "";
                this.txtRegReqApproval.Enabled = true;
                Dictionary<Int32, String> sttList = DMG.getWellStatusList();
                this.ddlRegReqApproval.Items.Clear();
                this.ddlRegReqApproval.DataSource = sttList;
                this.ddlRegReqApproval.DataTextField = "Value";
                this.ddlRegReqApproval.DataValueField = "Key";
                this.ddlRegReqApproval.DataBind();
                this.ddlRegReqApproval.Items.Insert(0, new ListItem("--- Select Approval Status ---", "-1"));
                this.ddlRegReqApproval.SelectedIndex = -1;
                this.ddlRegReqApproval.Enabled = true;
                this.btnRegWellReset.Enabled = true;
                this.btnRegWellReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlRegReqApproval_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnRegWell.Enabled = true;
                this.btnRegWell.CssClass = "btn btn-success text-center";
                this.btnRegWellReset.Enabled = true;
                this.btnRegWellReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRegWell_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, RequestID = -99, ClientID = -99, ApprovedStatus = -99, ContractTypeID = -99, ContractID = -99, ServicePriceID = -99;
                String confirmationCode = String.Empty;
                RequestID = Convert.ToInt32(this.ddlRegReqList.SelectedValue);
                ClientID = Convert.ToInt32(this.ddlClientList.SelectedValue);
                ContractTypeID = Convert.ToInt32(this.ddlRegReqContractType.SelectedValue);
                ContractID = Convert.ToInt32(this.ddlRegReqContract.SelectedValue);
                ServicePriceID = Convert.ToInt32(this.ddlRegReqPrice.SelectedValue);
                confirmationCode = Convert.ToString(this.txtRegReqApproval.Text);
                ApprovedStatus = Convert.ToInt32(this.ddlRegReqApproval.SelectedValue);
                if (String.IsNullOrEmpty(confirmationCode))
                {
                    this.enReg.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iReg.Attributes["class"] = "icon fa fa-warning";
                    this.spnReg.InnerText = " Missing information!";
                    this.lblRegSuccess.Text = "!!! Error !!! Null values detected. Please provide all values";
                    this.enReg.Visible = true;
                }
                else
                {
                    iResult = DMG.updateGlobalWellRegistrationRequest(RequestID, ClientID, ContractTypeID, ContractID, ApprovedStatus, ServicePriceID, confirmationCode, LoginName, domain);
                    if (iResult.Equals(1))
                    {
                        this.btnRegWellReset_Click(null, null);
                        this.enReg.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iReg.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnReg.InnerText = " Success!";
                        this.lblRegSuccess.Text = "Successfully updated Registration Request";
                        this.enReg.Visible = true;
                        this.btnRegWellReset.Enabled = true;
                        this.btnRegWellReset.CssClass = "btn btn-warning text-center";
                        this.btnRegWell.Enabled = false;
                        this.btnRegWell.CssClass = "btn btn-default text-center";
                    }
                    else
                    {
                        this.btnRegWellReset_Click(null, null);
                        this.enReg.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iReg.Attributes["class"] = "icon fa fa-warning";
                        this.spnReg.InnerText = " Missing information!";
                        this.lblRegSuccess.Text = "!!! Error !!! Please try again";
                        this.enReg.Visible = true;
                        this.btnRegWellReset.Enabled = true;
                        this.btnRegWellReset.CssClass = "btn btn-warning text-center";
                        this.btnRegWell.Enabled = false;
                        this.btnRegWell.CssClass = "btn btn-default text-center";
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRegWellReset_Click(object sender, EventArgs e)
        {
            try
            {
                List<Int32> RequestingClients = DMG.getRegReqClientsList();
                Dictionary<Int32, String> clntList = CLNT.getRegReqClientList(RequestingClients); //Client
                this.ddlClientList.Items.Clear();
                this.ddlClientList.DataSource = clntList;
                this.ddlClientList.DataTextField = "Value";
                this.ddlClientList.DataValueField = "Key";
                this.ddlClientList.DataBind();
                this.ddlClientList.Items.Insert(0, new ListItem("--- Select SMARTs Client Company ---", "-1"));
                this.ddlClientList.SelectedIndex = -1;
                this.ddlRegReqList.Items.Clear();
                this.ddlRegReqList.DataBind();
                this.ddlRegReqList.Items.Insert(0, new ListItem("--- Select Registration Request ---", "-1"));
                this.ddlRegReqList.SelectedIndex = -1;
                this.ddlRegReqList.Enabled = false;
                this.ddlRegReqContractType.Items.Clear();
                this.ddlRegReqContractType.DataBind();
                this.ddlRegReqContractType.Items.Insert(0, new ListItem("--- Select Client Contract Type ---", "-1"));
                this.ddlRegReqContractType.SelectedIndex = -1;
                this.ddlRegReqContractType.Enabled = false;
                this.ddlRegReqContract.Items.Clear();
                this.ddlRegReqContract.DataBind();
                this.ddlRegReqContract.Items.Insert(0, new ListItem("--- Select Client Contract ---", "-1"));
                this.ddlRegReqContract.SelectedIndex = -1;
                this.ddlRegReqContract.Enabled = false;
                this.ddlRegReqPrice.Items.Clear();
                this.ddlRegReqPrice.DataBind();
                this.ddlRegReqPrice.Items.Insert(0, new ListItem("--- Select Contracted Price ---", "-1"));
                this.ddlRegReqPrice.SelectedIndex = -1;
                this.ddlRegReqPrice.Enabled = false;
                this.txtRegReqApproval.Text = "";
                this.txtRegReqApproval.Enabled = false;
                this.ddlRegReqApproval.Items.Clear();
                this.ddlRegReqApproval.DataBind();
                this.ddlRegReqApproval.Items.Insert(0, new ListItem("--- Select Approval Status ---", "-1"));
                this.ddlRegReqApproval.SelectedIndex = -1;
                this.ddlRegReqApproval.Enabled = false;
                this.enReg.Visible = false;
                this.lblRegSuccess.Text = "";
                this.btnRegWell.Enabled = false;
                this.btnRegWell.CssClass = "btn btn-default text-center";
                this.btnRegWellReset.Enabled = false;
                this.btnRegWellReset.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRegWellDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.divLReqList.Attributes["class"] = "panel-collapse collapse in";
                this.divReqProc.Attributes["class"] = "panel-collapse collapse";
                System.Data.DataTable rrTable = DMG.getGlobalWellRegistrationRequest();
                this.gvwRegReqList.DataSource = rrTable;
                this.gvwRegReqList.PageIndex = 0;
                this.gvwRegReqList.SelectedIndex = -1;
                this.gvwRegReqList.DataBind();
                this.btnRegWellReset_Click(null, null);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }                                                                      
    }
}