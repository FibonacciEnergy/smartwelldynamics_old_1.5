﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using OPR = SmartsVer1.Helpers.QCHelpers.Opr;
using SRV = SmartsVer1.Helpers.DemogHelpers.ServicesConfig;
using RMV = SmartsVer1.Helpers.QCHelpers.RemoveData;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using Color = System.Drawing.Color;
using log4net;

namespace SmartsVer1.Control.Services
{
    public partial class ctrlRemoveData_2A : System.Web.UI.UserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ctrlRemoveData_2A));
        String LoginName, username, domain, GetClientDBString;
        Int32 ClientID = -99, ServiceGroupID = 2, OperatorType = 4;

        protected void Page_Init(object sender, EventArgs e)
        {

            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);

                //Populating dropdown lists on the page
                Dictionary<Int32, String> srvList = SRV.getServiceList(ServiceGroupID);
                this.ddlSrvType.Items.Clear();
                this.ddlSrvType.DataSource = srvList;
                this.ddlSrvType.DataTextField = "Value";
                this.ddlSrvType.DataValueField = "Key";
                this.ddlSrvType.DataBind();
                this.ddlSrvType.Items.Insert(0, new ListItem("--- Select Service Type ---", "-1"));
                this.ddlSrvType.SelectedIndex = -1;
                this.ddlSrvType.BorderColor = Color.Green;
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) // If page loads for first time 
            {
                try
                {
                    Page.Form.Attributes.Add("enctype", "multipart/form-data");
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
            }
        }

        protected void ddlSrvType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 serviceID = Convert.ToInt32(this.ddlSrvType.SelectedValue);
                Int32 myKeys = 0, kID = 0;
                Dictionary<Int32, String> oprList = CLNT.getGlobalOperatorCoListWithAssetCount(OperatorType, ClientID, GetClientDBString);
                myKeys = oprList.Keys.Count;
                if (myKeys >= 1)
                {
                    foreach (KeyValuePair<Int32, String> item in oprList)
                    {
                        kID = item.Key;
                    }

                    if (kID.Equals(-99))
                    {
                        this.ddlOpr.BorderColor = Color.Maroon;
                        this.ddlOpr.Items.Clear();
                        this.ddlOpr.DataBind();
                        this.ddlOpr.Items.Insert(0, new ListItem("--- Select Global Operator ---", "-1"));
                        this.ddlOpr.SelectedIndex = -1;
                        this.ddlOpr.ToolTip = "Could not find any Job Definition";
                    }
                    else
                    {
                        this.ddlOpr.BorderColor = Color.Green;
                        this.ddlOpr.Items.Clear();
                        this.ddlOpr.DataSource = oprList;
                        this.ddlOpr.DataTextField = "Value";
                        this.ddlOpr.DataValueField = "Key";
                        this.ddlOpr.DataBind();
                        this.ddlOpr.Items.Insert(0, new ListItem("--- Select Global Operator ---", "-1"));
                        this.ddlOpr.SelectedIndex = -1;
                        this.ddlOpr.Enabled = true;

                        this.ddlOpr.Focus();
                    }
                }
                else
                {
                    this.ddlOpr.BorderColor = Color.Maroon;
                    this.ddlOpr.Items.Clear();
                    this.ddlOpr.DataBind();
                    this.ddlOpr.Items.Insert(0, new ListItem("--- Select Global Operator ---", "-1"));
                    this.ddlOpr.SelectedIndex = -1;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlOpr_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 opID = Convert.ToInt32(this.ddlOpr.SelectedValue);
                Int32 srvID = Convert.ToInt32(this.ddlSrvType.SelectedValue);
                if (opID.Equals(-99))
                {
                    this.ddlOpr.Focus();
                }
                else
                {
                    Int32 myKeys = 0, kID = 0;
                    Dictionary<Int32, String> wpdList = AST.getOprWellPadList(opID, srvID, GetClientDBString);
                    myKeys = wpdList.Keys.Count;
                    if (myKeys >= 1)
                    {
                        foreach (KeyValuePair<Int32, String> item in wpdList)
                        {
                            kID = item.Key;
                        }
                    }

                    if (kID.Equals(-99))
                    {
                        this.ddlPads.BorderColor = Color.Maroon;
                        this.ddlPads.Items.Clear();
                        this.ddlPads.DataBind();
                        this.ddlPads.Items.Insert(0, new ListItem("--- Select Well Pad ---", "-1"));
                        this.ddlPads.SelectedIndex = -1;
                        this.ddlPads.ToolTip = "No Well Pad(s) found";
                    }
                    else
                    {
                        this.ddlPads.BorderColor = Color.Green;
                        this.ddlPads.Items.Clear();
                        this.ddlPads.DataSource = wpdList;
                        this.ddlPads.DataValueField = "Key";
                        this.ddlPads.DataTextField = "Value";
                        this.ddlPads.DataBind();
                        this.ddlPads.Items.Insert(0, new ListItem("--- Select Well Pad ---", "-1"));
                        this.ddlPads.SelectedIndex = -1;
                        this.ddlPads.Enabled = true;                                                
                    }
                }
                this.ddlWell.Items.Clear();
                this.ddlWell.DataBind();
                this.ddlWell.Items.Insert(0, new ListItem("--- Select Well / Lateral ---", "-1"));
                this.ddlWell.SelectedIndex = -1;
                this.ddlWell.Enabled = false;
                this.ddlRun.Items.Clear();
                this.ddlRun.DataBind();
                this.ddlRun.Items.Insert(0, new ListItem("--- Select Run ---", "-1"));
                this.ddlRun.SelectedIndex = -1;
                this.ddlRun.Enabled = false;
                this.btnDetailCancel.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWell_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 wsKeys = -99, wskID = -99, wlID = Convert.ToInt32(this.ddlWell.SelectedValue);
                Dictionary<Int32, String> wsList = OPR.WellSectionNameToWellList(wlID, GetClientDBString);
                wsKeys = wsList.Keys.Count;
                if (wsKeys >= (1))
                {
                    foreach (KeyValuePair<Int32, String> item in wsList)
                    {
                        wskID = item.Key;
                    }
                    if (wskID.Equals(-99))
                    {
                        this.ddlWellSection.BorderColor = Color.Maroon;
                        this.ddlWellSection.Items.Clear();
                        this.ddlWellSection.DataBind();
                        this.ddlWellSection.Items.Insert(0, new ListItem("--- Select Well Section ---", "-1"));
                        this.ddlWellSection.SelectedIndex = -1;
                        this.ddlWellSection.ToolTip = "Unable to load Well Section list";
                    }
                    else
                    {
                        this.ddlWellSection.BorderColor = Color.Green;
                        this.ddlWellSection.Items.Clear();
                        this.ddlWellSection.DataSource = wsList;
                        this.ddlWellSection.DataTextField = "Value";
                        this.ddlWellSection.DataValueField = "Key";
                        this.ddlWellSection.DataBind();
                        this.ddlWellSection.Items.Insert(0, new ListItem("--- Select Well Section ---", "-1"));
                        this.ddlWellSection.SelectedIndex = -1;
                        this.ddlWellSection.Enabled = true;
                        this.ddlWellSection.Focus();
                    }
                }
                else
                {
                    this.ddlWellSection.BorderColor = Color.Maroon;
                    this.ddlWellSection.Items.Clear();
                    this.ddlWellSection.DataBind();
                    this.ddlWellSection.Items.Insert(0, new ListItem("--- Select Well Section ---", "-1"));
                    this.ddlWellSection.SelectedIndex = -1;
                    this.ddlWellSection.ToolTip = "Unable to load Data";
                }
                this.ddlRun.Items.Clear();
                this.ddlRun.DataBind();
                this.ddlRun.Items.Insert(0, new ListItem("--- Select Run ---", "-1"));
                this.ddlRun.SelectedIndex = -1;
                this.ddlRun.Enabled = false;
                this.gvwData.DataBind();
                this.gvwData.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlPads_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srvID = Convert.ToInt32(this.ddlSrvType.SelectedValue);
                Int32 opID = Convert.ToInt32(this.ddlOpr.SelectedValue);
                Int32 pdID = Convert.ToInt32(this.ddlPads.SelectedValue);
                
                    Int32 myKeys = 0, kID = 0;
                    Dictionary<Int32, String> welList = AST.getOperatorWellList(srvID, opID, pdID, GetClientDBString);
                    myKeys = welList.Keys.Count;
                    if (myKeys >= 1)
                    {
                        foreach (KeyValuePair<Int32, String> item in welList)
                        {
                            kID = item.Key;
                        }
                    }

                    if (kID.Equals(-99))
                    {
                        this.ddlWell.BorderColor = Color.Maroon;
                        this.ddlWell.Items.Clear();
                        this.ddlWell.DataBind();
                        this.ddlWell.Items.Insert(0, new ListItem("--- Select Well / Lateral ---", "-1"));
                        this.ddlWell.SelectedIndex = -1;
                        this.ddlWell.ToolTip = "No Well / Lateral found";
                    }
                    else
                    {
                        this.ddlWell.BorderColor = Color.Green;
                        this.ddlWell.Items.Clear();
                        this.ddlWell.DataSource = welList;
                        this.ddlWell.DataValueField = "Key";
                        this.ddlWell.DataTextField = "Value";
                        this.ddlWell.DataBind();
                        this.ddlWell.Items.Insert(0, new ListItem("--- Select Well / Lateral ---", "-1"));
                        this.ddlWell.SelectedIndex = -1;
                        this.ddlWell.Enabled = true;
                    }
                
                this.ddlRun.Items.Clear();
                this.ddlRun.DataBind();
                this.ddlRun.Items.Insert(0, new ListItem("--- Select Run ---", "-1"));
                this.ddlRun.SelectedIndex = -1;
                this.ddlRun.Enabled = false;
                this.btnDetailCancel.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWellSection_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 rnKeys = -99, rnkID = -99, welID = Convert.ToInt32(this.ddlWell.SelectedValue), wsID = Convert.ToInt32(this.ddlWellSection.SelectedValue);
                Dictionary<Int32, String> rnList = OPR.getRunList(welID, wsID, GetClientDBString);
                rnKeys = rnList.Keys.Count;
                if (rnKeys >= (1))
                {
                    foreach (KeyValuePair<Int32, String> item in rnList)
                    {
                        rnkID = item.Key;
                    }
                    if (rnkID.Equals(-99))
                    {
                        this.ddlRun.BorderColor = Color.Maroon;
                        this.ddlRun.Items.Clear();
                        this.ddlRun.DataBind();
                        this.ddlRun.Items.Insert(0, new ListItem("--- Select Run ---", "-1"));
                        this.ddlRun.SelectedIndex = -1;
                        this.ddlRun.ToolTip = "Unable to load Run list";
                    }
                    else
                    {
                        this.ddlRun.BorderColor = Color.Green;
                        this.ddlRun.Items.Clear();
                        this.ddlRun.DataSource = rnList;
                        this.ddlRun.DataTextField = "Value";
                        this.ddlRun.DataValueField = "Key";
                        this.ddlRun.DataBind();
                        this.ddlRun.Items.Insert(0, new ListItem("--- Select Run ---", "-1"));
                        this.ddlRun.SelectedIndex = -1;
                        this.ddlRun.Enabled = true;
                        this.ddlRun.Focus();
                    }
                }
                else
                {
                    this.ddlRun.BorderColor = Color.Maroon;
                    this.ddlRun.Items.Clear();
                    this.ddlRun.DataBind();
                    this.ddlRun.Items.Insert(0, new ListItem("--- Select Run ---", "-1"));
                    this.ddlRun.SelectedIndex = -1;
                    this.ddlRun.ToolTip = "Unable to load Data";
                }
                this.gvwData.DataBind();
                this.gvwData.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlRun_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable resTable = new System.Data.DataTable();
                Int32 srvID = Convert.ToInt32(this.ddlSrvType.SelectedValue);
                Int32 oprID = Convert.ToInt32(this.ddlOpr.SelectedValue);
                Int32 rID = Convert.ToInt32(this.ddlRun.SelectedValue);
                Int32 wsID = Convert.ToInt32(this.ddlWellSection.SelectedValue);
                Int32 wlID = Convert.ToInt32(this.ddlWell.SelectedValue);
                switch (srvID)
                {
                    case 33: { resTable = RMV.getRawQCData(srvID, rID, wsID, wlID, GetClientDBString);
                                break; }
                    case 34: { resTable = RMV.getResQCData(srvID, oprID, rID, wsID, wlID, GetClientDBString);   
                                break; }
                    case 37: { resTable = RMV.get3rdRawQCData(srvID, oprID, rID, wsID, wlID, GetClientDBString);   
                                break; }
                    case 38: { break; }
                    case 39: { break; }
                    case 40: { break; }
                    case 46: { break; }
                    default: { break; }
                }
                
                this.gvwData.DataSource = resTable;
                this.gvwData.DataBind();
                this.gvwData.Visible = true;
                this.btnDetailCancel.Visible = true;
                this.btnDeleteAll.Enabled = true;
                this.btnDeleteAll.CssClass = "btn btn-danger text-center";
                this.gvwData.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnMetaCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 jcKeys = -99, jckID = -99;
                Dictionary<Int32, String> svList = SRV.getServiceList(ServiceGroupID);
                jcKeys = svList.Keys.Count;
                if (jcKeys >= (1))
                {
                    foreach (KeyValuePair<Int32, String> item in svList)
                    {
                        jckID = item.Key;
                    }
                    if (jckID.Equals(-99))
                    {
                        this.ddlSrvType.BorderColor = Color.Maroon;
                        this.ddlSrvType.Items.Clear();
                        this.ddlSrvType.DataBind();
                        this.ddlSrvType.Items.Insert(0, new ListItem("--- Select Service Type ---", "-1"));
                        this.ddlSrvType.SelectedIndex = -1;
                        this.ddlSrvType.ToolTip = "Unable to load Job Class list";
                    }
                    else
                    {
                        this.ddlSrvType.BorderColor = Color.Green;
                        this.ddlSrvType.Items.Clear();
                        this.ddlSrvType.DataSource = svList;
                        this.ddlSrvType.DataTextField = "Value";
                        this.ddlSrvType.DataValueField = "Key";
                        this.ddlSrvType.DataBind();
                        this.ddlSrvType.Items.Insert(0, new ListItem("--- Select Service Type ---", "-1"));
                        this.ddlSrvType.SelectedIndex = -1;
                        this.ddlSrvType.Focus();
                    }
                }
                else
                {
                    this.ddlSrvType.BorderColor = Color.Maroon;
                    this.ddlSrvType.Items.Clear();
                    this.ddlSrvType.DataBind();
                    this.ddlSrvType.Items.Insert(0, new ListItem("--- Select Service Type ---", "-1"));
                    this.ddlSrvType.SelectedIndex = -1;
                    this.ddlSrvType.ToolTip = "Unable to load Data";
                }
                this.ddlOpr.Items.Clear();
                this.ddlOpr.DataBind();
                this.ddlOpr.Items.Insert(0, new ListItem("--- Select Global Operator ---", "-1"));
                this.ddlOpr.SelectedIndex = -1;
                this.ddlOpr.Enabled = false;
                this.ddlOpr.BorderColor = Color.Empty;
                this.ddlPads.Items.Clear();
                this.ddlPads.DataBind();
                this.ddlPads.Items.Insert(0, new ListItem("--- Select Well Pad ---", "-1"));
                this.ddlPads.SelectedIndex = -1;
                this.ddlPads.Enabled = false;
                this.ddlWell.Items.Clear();
                this.ddlWell.DataBind();
                this.ddlWell.Items.Insert(0, new ListItem("--- Select Well ---", "-1"));
                this.ddlWell.SelectedIndex = -1;
                this.ddlWell.Enabled = false;
                this.ddlWell.BorderColor = Color.Empty;
                this.ddlWellSection.BorderColor = Color.Empty;
                this.ddlWellSection.Items.Clear();
                this.ddlWellSection.DataBind();
                this.ddlWellSection.Items.Insert(0, new ListItem("--- Select Well Section ---", "-1"));
                this.ddlWellSection.SelectedIndex = -1;
                this.ddlWellSection.Enabled = false;
                this.ddlRun.Items.Clear();
                this.ddlRun.DataBind();
                this.ddlRun.Items.Insert(0, new ListItem("--- Select Run ---", "-1"));
                this.ddlRun.SelectedIndex = -1;
                this.ddlRun.Enabled = false;
                this.ddlRun.BorderColor = Color.Empty;
                System.Data.DataTable emptyData = new System.Data.DataTable();
                this.gvwData.DataSource = emptyData;
                this.gvwData.DataBind();
                this.gvwData.Visible = false;
                this.delEnclosure.Visible = false;
                this.btnMetaCancel.Visible = false;
                this.btnDetailCancel.Visible = false;
                this.btnDeleteAll.Enabled = false;
                this.btnDeleteAll.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 srvID = Convert.ToInt32(this.ddlSrvType.SelectedValue);
                Int32 opID = Convert.ToInt32(this.ddlOpr.SelectedValue);
                Int32 rID = Convert.ToInt32(this.ddlRun.SelectedValue);
                Int32 wsID = Convert.ToInt32(this.ddlWellSection.SelectedValue);
                Int32 wlID = Convert.ToInt32(this.ddlWell.SelectedValue);
                DataTable resTable = RMV.getRawQCData(srvID, rID, wsID, wlID, GetClientDBString);
                this.gvwData.PageIndex = e.NewPageIndex;
                this.gvwData.DataSource = resTable;
                this.gvwData.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                Decimal sDepth = -99.00M;
                Int32 operatorID = -99, wellPadID = -99, runID = -99, wellSectionID = -99, wellID = -99;
                Int32 selID = -99, serviceID = -99, rawQCD = -99, resQCD = -99, rqcD = -99, rqcP = -99, crrD = -99, crrAnaD = -99, rowID = -99;
                String welName = String.Empty, serviceName = String.Empty, SectionName = String.Empty, runName = String.Empty;
                operatorID = Convert.ToInt32(this.ddlOpr.SelectedValue);
                serviceID = Convert.ToInt32(this.ddlSrvType.SelectedValue);
                serviceName = Convert.ToString(this.ddlSrvType.SelectedItem);
                runID = Convert.ToInt32(this.ddlRun.SelectedValue);
                runName = Convert.ToString(this.ddlRun.SelectedItem);
                wellSectionID = Convert.ToInt32(this.ddlWellSection.SelectedValue);
                SectionName = Convert.ToString(this.ddlWellSection.SelectedItem);
                wellID = Convert.ToInt32(this.ddlWell.SelectedValue);
                wellPadID = Convert.ToInt32(this.ddlPads.SelectedValue);
                welName = Convert.ToString(this.ddlWell.SelectedItem);
                selID = Convert.ToInt32(gvwData.DataKeys[e.RowIndex].Value);
                rowID = Convert.ToInt32(e.Values[0]);
                sDepth = Convert.ToDecimal(e.Values[1]);
                switch (serviceID)
                {
                    case 33: //Raw QC
                        {
                            rawQCD = RMV.deleteSurveyRawFormattedData(sDepth, runID, wellSectionID, wellID, serviceID, GetClientDBString);
                            rqcD = RMV.deleteRawQCResults(rowID, sDepth, runID, wellSectionID, wellID, serviceID, GetClientDBString);
                            rqcP = RMV.deleteWellPlacementData(rowID, sDepth, wellID, wellPadID, ClientID, operatorID, GetClientDBString);
                            if (rawQCD >= (1) && rqcD >= (0) && rqcP >= (0))
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-check-circle";
                                this.spnDel.InnerText = " Deleted!";
                                this.lblSuccess.Text = "Selected data row deleted";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(serviceID, runID, wellSectionID, wellID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Info(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Survey_Depth: " + sDepth + " Action: " + " DELETED_Survey_Row ");
                            }
                            else
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-warning";
                                this.spnDel.InnerText = " Not Deleted!";
                                this.lblSuccess.Text = "There was a problem deleting Survey Row from database. Please try again";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(serviceID, runID, wellSectionID, wellID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Error(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Survey_Depth: " + sDepth + " Action: " + " COULDN'T_DELETE_Survey_Row ");
                            }
                            break; 
                        }
                    case 34: //Results QC
                        {
                            resQCD = RMV.deleteSurveyResFormattedData(sDepth, runID, wellSectionID, wellID, serviceID, GetClientDBString);
                            rawQCD = RMV.deleteSurveyRawFormattedData(sDepth, runID, wellSectionID, wellID, serviceID, GetClientDBString);
                            rqcD = RMV.deleteRawQCResults(rowID, sDepth, runID, wellSectionID, wellID, serviceID, GetClientDBString);
                            rqcP = RMV.deleteWellPlacementData(rowID, sDepth, wellID, wellPadID, ClientID, operatorID, GetClientDBString);
                            if (resQCD >= (1) && rawQCD >= (0) && rqcD >= (0) && rqcP >= (0))
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-check-circle";
                                this.spnDel.InnerText = " Deleted!";
                                this.lblSuccess.Text = "Selected data row deleted";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(serviceID, runID, wellSectionID, wellID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Info(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Survey_Depth: " + sDepth + " Action: " + " DELETED_Survey_Row ");
                            }
                            else
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-warning";
                                this.spnDel.InnerText = " Not Deleted!";
                                this.lblSuccess.Text = "There was a problem deleting Survey Row from database. Please try again";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(serviceID, runID, wellSectionID, wellID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Error(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Survey_Depth: " + sDepth + " Action: " + " COULDN'T_DELETE_Survey_Row ");
                            }
                            break; 
                        }
                    case 37: //3rd Raw QC
                        {
                            rawQCD = RMV.delete3rdSurveyRawFormattedData(sDepth, runID, wellSectionID, wellID, serviceID, GetClientDBString);
                            rqcD = RMV.deleteRawQCResults(rowID, sDepth, runID, wellSectionID, wellID, serviceID, GetClientDBString);
                            rqcP = RMV.deleteWellPlacementData(rowID, sDepth, wellID, wellPadID, ClientID, operatorID, GetClientDBString);
                            if (resQCD >= (1) && rawQCD >= (0) && rqcD >= (0) && rqcP >= (0))
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-check-circle";
                                this.spnDel.InnerText = " Deleted!";
                                this.lblSuccess.Text = "Selected data row deleted";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(serviceID, runID, wellSectionID, wellID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Info(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Survey_Depth: " + sDepth + " Action: " + " DELETED_Survey_Row ");
                            }
                            else
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-warning";
                                this.spnDel.InnerText = " Not Deleted!";
                                this.lblSuccess.Text = "There was a problem deleting Survey Row from database. Please try again";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(serviceID, runID, wellSectionID, wellID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Error(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Survey_Depth: " + sDepth + " Action: " + " COULDN'T_DELETE_Survey_Row ");
                            }
                        break; 
                        }
                    case 38: //3rd Results QC
                        {
                            resQCD = RMV.delete3rdSurveyResFormattedData(sDepth, runID, wellSectionID, wellID, serviceID, GetClientDBString);
                            rawQCD = RMV.delete3rdSurveyRawFormattedData(sDepth, runID, wellSectionID, wellID, serviceID, GetClientDBString);
                            rqcD = RMV.deleteRawQCResults(rowID, sDepth, runID, wellSectionID, wellID, serviceID, GetClientDBString);
                            rqcP = RMV.deleteWellPlacementData(rowID, sDepth, wellID, wellPadID, ClientID, operatorID, GetClientDBString);
                            if (resQCD >= (1) && rawQCD >= (0) && rqcD >= (0) && rqcP >= (0))
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-check-circle";
                                this.spnDel.InnerText = " Deleted!";
                                this.lblSuccess.Text = "Selected data row deleted";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(serviceID, runID, wellSectionID, wellID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Info(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Survey_Depth: " + sDepth + " Action: " + " DELETED_Survey_Row ");
                            }
                            else
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-warning";
                                this.spnDel.InnerText = " Not Deleted!";
                                this.lblSuccess.Text = "There was a problem deleting Survey Row from database. Please try again";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(serviceID, runID, wellSectionID, wellID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Error(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Survey_Depth: " + sDepth + " Action: " + " COULDN'T_DELETE_Survey_Row ");
                            }
                            break; 
                        }
                    case 39: //Hist Raw QC
                        {
                            rawQCD = RMV.deleteHistSurveyRawFormattedData(sDepth, runID, wellSectionID, wellID, serviceID, GetClientDBString);
                            rqcD = RMV.deleteRawQCResults(rowID, sDepth, runID, wellSectionID, wellID, serviceID, GetClientDBString);
                            rqcP = RMV.deleteWellPlacementData(rowID, sDepth, wellID, wellPadID, ClientID, operatorID, GetClientDBString);
                            if (resQCD >= (1) && rawQCD >= (0) && rqcD >= (0) && rqcP >= (0))
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-check-circle";
                                this.spnDel.InnerText = " Deleted!";
                                this.lblSuccess.Text = "Selected data row deleted";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(serviceID, runID, wellSectionID, wellID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Info(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Survey_Depth: " + sDepth + " Action: " + " DELETED_Survey_Row ");
                            }
                            else
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-warning";
                                this.spnDel.InnerText = " Not Deleted!";
                                this.lblSuccess.Text = "There was a problem deleting Survey Row from database. Please try again";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(serviceID, runID, wellSectionID, wellID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Error(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Survey_Depth: " + sDepth + " Action: " + " COULDN'T_DELETE_Survey_Row ");
                            }
                            break; }
                    case 40: //Hist Results QC
                        {
                            resQCD = RMV.deleteHistSurveyResFormattedData(sDepth, runID, wellSectionID, wellID, serviceID, GetClientDBString);
                            rawQCD = RMV.deleteHistSurveyRawFormattedData(sDepth, runID, wellSectionID, wellID, serviceID, GetClientDBString);
                            rqcD = RMV.deleteRawQCResults(rowID, sDepth, runID, wellSectionID, wellID, serviceID, GetClientDBString);
                            rqcP = RMV.deleteWellPlacementData(rowID, sDepth, wellID, wellPadID, ClientID, operatorID, GetClientDBString);
                            if (resQCD >= (1) && rawQCD >= (0) && rqcD >= (0) && rqcP >= (0))
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-check-circle";
                                this.spnDel.InnerText = " Deleted!";
                                this.lblSuccess.Text = "Selected data row deleted";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(serviceID, runID, wellSectionID, wellID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Info(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Survey_Depth: " + sDepth + " Action: " + " DELETED_Survey_Row ");
                            }
                            else
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-warning";
                                this.spnDel.InnerText = " Not Deleted!";
                                this.lblSuccess.Text = "There was a problem deleting Survey Row from database. Please try again";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(serviceID, runID, wellSectionID, wellID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Error(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Survey_Depth: " + sDepth + " Action: " + " COULDN'T_DELETE_Survey_Row ");
                            }
                            break; }
                    case 41: //Well Profile
                        {
                            resQCD = RMV.deleteWPSurveyResFormattedData(sDepth, runID, wellSectionID, wellID, serviceID, GetClientDBString);
                            rawQCD = RMV.deleteSurveyRawFormattedData(sDepth, runID, wellSectionID, wellID, serviceID, GetClientDBString);
                            rqcD = RMV.deleteRawQCResults(rowID, sDepth, runID, wellSectionID, wellID, serviceID, GetClientDBString);
                            rqcP = RMV.deleteWellPlacementData(rowID, sDepth, wellID, wellPadID, ClientID, operatorID, GetClientDBString);
                            if (resQCD >= (1) && rawQCD >= (0) && rqcD >= (0) && rqcP >= (0))
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-check-circle";
                                this.spnDel.InnerText = " Deleted!";
                                this.lblSuccess.Text = "Selected data row deleted";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(serviceID, runID, wellSectionID, wellID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Info(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Survey_Depth: " + sDepth + " Action: " + " DELETED_Survey_Row ");
                            }
                            else
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-warning";
                                this.spnDel.InnerText = " Not Deleted!";
                                this.lblSuccess.Text = "There was a problem deleting Survey Row from database. Please try again";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(serviceID, runID, wellSectionID, wellID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Error(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Survey_Depth: " + sDepth + " Action: " + " COULDN'T_DELETE_Survey_Row ");
                            }
                            break; }
                    case 46: //MSA Correction
                        {
                            rawQCD = RMV.deleteSurveyRawFormattedData(sDepth, runID, wellSectionID, wellID, serviceID, GetClientDBString);
                            crrD = RMV.deleteSurveyCorrFormattedData(sDepth, runID, wellSectionID, wellID, serviceID, GetClientDBString);
                            crrAnaD = RMV.deleteSorveyCorrAnalysisData(sDepth, runID, wellSectionID, wellID, GetClientDBString);
                            rqcD = RMV.deleteRawQCResults(rowID, sDepth, runID, wellSectionID, wellID, serviceID, GetClientDBString);
                            rqcP = RMV.deleteWellPlacementData(rowID, sDepth, wellID, wellPadID, ClientID, operatorID, GetClientDBString);
                            if (resQCD >= (1) && rawQCD >= (0) && rqcD >= (0) && rqcP >= (0))
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-check-circle";
                                this.spnDel.InnerText = " Deleted!";
                                this.lblSuccess.Text = "Selected data row deleted";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(serviceID, runID, wellSectionID, wellID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Info(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Survey_Depth: " + sDepth + " Action: " + " DELETED_Survey_Row ");
                            }
                            else
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-warning";
                                this.spnDel.InnerText = " Not Deleted!";
                                this.lblSuccess.Text = "There was a problem deleting Survey Row from database. Please try again";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(serviceID, runID, wellSectionID, wellID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Error(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Survey_Depth: " + sDepth + " Action: " + " COULDN'T_DELETE_Survey_Row ");
                            }
                            break; }
                    default: //Azimuth Correction
                        {
                            resQCD = RMV.deleteIncDipCorrFormattedData(sDepth, runID, wellSectionID, wellID, serviceID, GetClientDBString);
                            rawQCD = RMV.deleteIncDipCorrInputToAnalysis(sDepth, runID, wellSectionID, wellID, serviceID, GetClientDBString);
                            rqcD = RMV.deleteRawQCResults(rowID, sDepth, runID, wellSectionID, wellID, serviceID, GetClientDBString);
                            rqcP = RMV.deleteWellPlacementData(rowID, sDepth, wellID, wellPadID, ClientID, operatorID, GetClientDBString);
                            if (resQCD >= (1) && rawQCD >= (0) && rqcD >= (0) && rqcP >= (0))
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-check-circle";
                                this.spnDel.InnerText = " Deleted!";
                                this.lblSuccess.Text = "Selected data row deleted";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(serviceID, runID, wellSectionID, wellID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Info(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Survey_Depth: " + sDepth + " Action: " + " DELETED_Survey_Row ");
                            }
                            else
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-warning";
                                this.spnDel.InnerText = " Not Deleted!";
                                this.lblSuccess.Text = "There was a problem deleting Survey Row from database. Please try again";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(serviceID, runID, wellSectionID, wellID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Error(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Survey_Depth: " + sDepth + " Action: " + " COULDN'T_DELETE_Survey_Row ");
                            }
                            break; 
                        }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDetailCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnMetaCancel_Click(null, null);
                this.btnDetailCancel.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            try
            {
                Decimal sDepth = -99.00M;
                Int32 runID = -99, wsID = -99, welID = -99, srvID = -99, rawD = -99, resD = -99, rqcD = -99, padID = -99, optrID = -99, crrD = -99, crrAnaD = -99, prD = -99;
                String serviceName = String.Empty, welName = String.Empty, SectionName = String.Empty, runName = String.Empty;
                srvID = Convert.ToInt32(this.ddlSrvType.SelectedValue);
                serviceName = Convert.ToString(this.ddlSrvType.SelectedItem);
                runID = Convert.ToInt32(this.ddlRun.SelectedValue);
                runName = Convert.ToString(this.ddlRun.SelectedItem);
                wsID = Convert.ToInt32(this.ddlWellSection.SelectedValue);
                SectionName = Convert.ToString(this.ddlWellSection.SelectedItem);
                padID = Convert.ToInt32(this.ddlPads.SelectedValue);
                welID = Convert.ToInt32(this.ddlWell.SelectedValue);
                welName = Convert.ToString(this.ddlWell.SelectedItem);
                optrID = Convert.ToInt32(this.ddlOpr.SelectedValue);
                switch (srvID)
                {
                    case 33: //Raw QC
                        {
                            rawD = RMV.deleteAllSurveyRawFormattedData(runID, wsID, welID, srvID, GetClientDBString);
                            rqcD = RMV.deleteAllRawQCResults(runID, wsID, welID, srvID, GetClientDBString);
                            prD = RMV.deleteAllWellPlacementData(welID, padID, ClientID, optrID, GetClientDBString);
                            if (rawD >= (1) && rqcD >= (0))
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-check-circle";
                                this.spnDel.InnerText = " Deleted!";
                                this.lblSuccess.Text = "Selected data row deleted";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(srvID, runID, wsID, welID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Info(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Action: " + " DELETED_All_Survey_Row ");
                            }
                            else
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-warning";
                                this.spnDel.InnerText = " Not Deleted!";
                                this.lblSuccess.Text = "There was a problem deleting Survey Row from database. Please try again";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(srvID, runID, wsID, welID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Error(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Action: " + " COULDN'T_DELETE_All_Survey_Row ");
                            }
                            break;
                        }
                    case 34: //Results QC
                        {
                            resD = RMV.deleteAllSurveyResFormattedData(runID, wsID, welID, srvID, GetClientDBString);
                            rawD = RMV.deleteAllSurveyRawFormattedData(runID, wsID, welID, srvID, GetClientDBString);
                            rqcD = RMV.deleteAllRawQCResults(runID, wsID, welID, srvID, GetClientDBString);
                            prD = RMV.deleteAllWellPlacementData(welID, padID, ClientID, optrID, GetClientDBString);
                            if (resD >= (1) && rawD >= (0) && rqcD >= (0))
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-check-circle";
                                this.spnDel.InnerText = " Deleted!";
                                this.lblSuccess.Text = "Selected data row deleted";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(srvID, runID, wsID, welID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Info(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Action: " + " DELETED_All_Survey_Row ");
                            }
                            else
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-warning";
                                this.spnDel.InnerText = " Not Deleted!";
                                this.lblSuccess.Text = "There was a problem deleting Survey Row from database. Please try again";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(srvID, runID, wsID, welID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Error(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Action: " + " COULDN'T_DELETE_All_Survey_Row ");
                            }
                            break;
                        }
                    case 37: //3rd Raw QC
                        {
                            rawD = RMV.deleteAll3rdSurveyRawFormattedData(runID, wsID, welID, srvID, GetClientDBString);
                            rqcD = RMV.deleteAllRawQCResults(runID, wsID, welID, srvID, GetClientDBString);
                            prD = RMV.deleteAllWellPlacementData(welID, padID, ClientID, optrID, GetClientDBString);
                            if (resD >= (1) && rawD >= (0) && rqcD >= (0))
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-check-circle";
                                this.spnDel.InnerText = " Deleted!";
                                this.lblSuccess.Text = "Selected data row deleted";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(srvID, runID, wsID, welID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Info(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Action: " + " DELETED_All_Survey_Row ");
                            }
                            else
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-warning";
                                this.spnDel.InnerText = " Not Deleted!";
                                this.lblSuccess.Text = "There was a problem deleting Survey Row from database. Please try again";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(srvID, runID, wsID, welID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Error(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Action: " + " COULDN'T_DELETE_All_Survey_Row ");
                            }
                            break;
                        }
                    case 38: //3rd Results QC
                        {
                            resD = RMV.deleteAll3rdSurveyResFormattedData(runID, wsID, welID, srvID, GetClientDBString);
                            rawD = RMV.deleteAll3rdSurveyRawFormattedData(runID, wsID, welID, srvID, GetClientDBString);
                            rqcD = RMV.deleteAllRawQCResults(runID, wsID, welID, srvID, GetClientDBString);
                            prD = RMV.deleteAllWellPlacementData(welID, padID, ClientID, optrID, GetClientDBString);
                            if (resD >= (1) && rawD >= (0) && rqcD >= (0))
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-check-circle";
                                this.spnDel.InnerText = " Deleted!";
                                this.lblSuccess.Text = "Selected data row deleted";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(srvID, runID, wsID, welID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Info(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Survey_Depth: " + sDepth + " Action: " + " DELETED_Survey_Row ");
                            }
                            else
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-warning";
                                this.spnDel.InnerText = " Not Deleted!";
                                this.lblSuccess.Text = "There was a problem deleting Survey Row from database. Please try again";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(srvID, runID, wsID, welID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Error(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Action: " + " COULDN'T_All_DELETE_Survey_Row ");
                            }
                            break;
                        }
                    case 39: //Hist Raw QC
                        {
                            rawD = RMV.deleteAllHistSurveyRawFormattedData(runID, wsID, welID, srvID, GetClientDBString);
                            rqcD = RMV.deleteAllRawQCResults(runID, wsID, welID, srvID, GetClientDBString);
                            prD = RMV.deleteAllWellPlacementData(welID, padID, ClientID, optrID, GetClientDBString);
                            if (resD >= (1) && rawD >= (0) && rqcD >= (0))
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-check-circle";
                                this.spnDel.InnerText = " Deleted!";
                                this.lblSuccess.Text = "Selected data row deleted";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(srvID, runID, wsID, welID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Info(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Action: " + " DELETED_All_Survey_Row ");
                            }
                            else
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-warning";
                                this.spnDel.InnerText = " Not Deleted!";
                                this.lblSuccess.Text = "There was a problem deleting Survey Row from database. Please try again";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(srvID, runID, wsID, welID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Error(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Action: " + " COULDN'T_DELETE_All_Survey_Row ");
                            }
                            break;
                        }
                    case 40: //Hist Results QC
                        {
                            resD = RMV.deleteHistSurveyResFormattedData(sDepth, runID, wsID, welID, srvID, GetClientDBString);
                            rawD = RMV.deleteAllHistSurveyResFormattedData(runID, wsID, welID, srvID, GetClientDBString);
                            rqcD = RMV.deleteAllRawQCResults(runID, wsID, welID, srvID, GetClientDBString);
                            prD = RMV.deleteAllWellPlacementData(welID, padID, ClientID, optrID, GetClientDBString);
                            if (resD >= (1) && rawD >= (0) && rqcD >= (0))
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-check-circle";
                                this.spnDel.InnerText = " Deleted!";
                                this.lblSuccess.Text = "Selected data row deleted";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(srvID, runID, wsID, welID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Info(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Action: " + " DELETED_All_Survey_Row ");
                            }
                            else
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-warning";
                                this.spnDel.InnerText = " Not Deleted!";
                                this.lblSuccess.Text = "There was a problem deleting Survey Row from database. Please try again";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(srvID, runID, wsID, welID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Error(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Action: " + " COULDN'T_DELETE_All_Survey_Row ");
                            }
                            break;
                        }
                    case 41: //Well Profile
                        {
                            resD = RMV.deleteAllWPSurveyResFormattedData(runID, wsID, welID, srvID, GetClientDBString);
                            rawD = RMV.deleteAllSurveyRawFormattedData(runID, wsID, welID, srvID, GetClientDBString);
                            rqcD = RMV.deleteAllRawQCResults(runID, wsID, welID, srvID, GetClientDBString);
                            prD = RMV.deleteAllWellPlacementData(welID, padID, ClientID, optrID, GetClientDBString);
                            if (resD >= (1) && rawD >= (0) && rqcD >= (0))
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-check-circle";
                                this.spnDel.InnerText = " Deleted!";
                                this.lblSuccess.Text = "Selected data row deleted";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(srvID, runID, wsID, welID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Info(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Action: " + " DELETED_All_Survey_Row ");
                            }
                            else
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-warning";
                                this.spnDel.InnerText = " Not Deleted!";
                                this.lblSuccess.Text = "There was a problem deleting Survey Row from database. Please try again";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(srvID, runID, wsID, welID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Error(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Action: " + " COULDN'T_DELETE_All_Survey_Row ");
                            }
                            break;
                        }
                    case 46: //MSA Correction
                        {
                            rawD = RMV.deleteAllSurveyRawFormattedData(runID, wsID, welID, srvID, GetClientDBString);
                            crrD = RMV.deleteAllSurveyCorrFormattedData(runID, wsID, welID, srvID, GetClientDBString);
                            crrAnaD = RMV.deleteAllSurveyCorrAnalysisData(runID, wsID, welID, GetClientDBString);
                            rqcD = RMV.deleteAllRawQCResults(runID, wsID, welID, srvID, GetClientDBString);
                            prD = RMV.deleteAllWellPlacementData(welID, padID, ClientID, optrID, GetClientDBString);
                            if (resD >= (1) && rawD >= (0) && rqcD >= (0))
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-check-circle";
                                this.spnDel.InnerText = " Deleted!";
                                this.lblSuccess.Text = "Selected data row deleted";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(srvID, runID, wsID, welID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Info(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Action: " + " DELETED_All_Survey_Row ");
                            }
                            else
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-warning";
                                this.spnDel.InnerText = " Not Deleted!";
                                this.lblSuccess.Text = "There was a problem deleting Survey Row from database. Please try again";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(srvID, runID, wsID, welID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Error(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Action: " + " COULDN'T_DELETE_All_Survey_Row ");
                            }
                            break;
                        }
                    default: //Azimuth Correction
                        {
                            resD = RMV.deleteAllIncDipCorrFormattedData(runID, wsID, welID, srvID, GetClientDBString);
                            rawD = RMV.deleteAllIncDipCorrInputToAnalysis(runID, wsID, welID, srvID, GetClientDBString);
                            rqcD = RMV.deleteAllRawQCResults(runID, wsID, welID, srvID, GetClientDBString);
                            prD = RMV.deleteAllWellPlacementData(welID, padID, ClientID, optrID, GetClientDBString);
                            if (resD >= (1) && rawD >= (0) && rqcD >= (0))
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-check-circle";
                                this.spnDel.InnerText = " Deleted!";
                                this.lblSuccess.Text = "Selected data row deleted";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(srvID, runID, wsID, welID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Info(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Action: " + " DELETED_All_Survey_Row ");
                            }
                            else
                            {
                                this.delEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iDel.Attributes["class"] = "icon fa fa-warning";
                                this.spnDel.InnerText = " Not Deleted!";
                                this.lblSuccess.Text = "There was a problem deleting Survey Row from database. Please try again";
                                this.delEnclosure.Visible = true;
                                System.Data.DataTable resTable = RMV.getRawQCData(srvID, runID, wsID, welID, GetClientDBString);
                                this.gvwData.DataSource = resTable;
                                this.gvwData.DataBind();
                                logger.Error(" User: " + LoginName + " Realm: " + domain + " Service_Name: " + serviceName + " Well_Name: " + welName + " Section_Name: " + SectionName + " Run_Name: " + runName + " Action: " + " COULDN'T_DELETE_All_Survey_Row ");
                            }
                            break;
                        }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        
    }
}