﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlServiceRegistration.ascx.cs" Inherits="SmartsVer1.Control.Services.ctrlServiceRegistration" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<!-- Stylesheet -->
<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript for ref lib -->
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>
<!--- local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }
    /*custom anchor defi for menu items*/
    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }
</style>
<!-- Javascript for accordion -->
<script type="text/javascript">
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("rotate");
        });
        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").addClass("rotate");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("rotate");
        });
    });
</script>
<!--- Page Menu -->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!-- Breadcrumb -->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../Viewer/fesViewer.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>SMARTs</li>
                <li><a href="../../Services/fesDRServiceRegistration.aspx"> SMARTs Service Registration</a></li>
            </ol>            
        </div>
    </div>
</div>
<!--- Page Content --->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div id="acrdRegReqOuter" class="panel-group" runat="server">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancRegReqMain" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlServiceRegistration_acrdRegReqOuter" href="#BodyContent_ctrlServiceRegistration_divRegReq">
                                 Service Registration Requests</a>
                        </h5>
                    </div>
                    <div id="divRegReqOuter" runat="server" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="uprgrsRegReq" runat="server" DisplayAfter="0" DynamicLayout="true" AssociatedUpdatePanelID="upnlRegReq">
                                <ProgressTemplate>
                                    <div id="divRegReqProcessing" runat="server" class="updateProgress">
                                        <asp:Image ID="imgRegReqProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel runat="server" ID="upnlRegReq">
                                <ContentTemplate>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="panel-group" runat="server" id="acrdReqList">
                                            <div class="panel panel-danger panel-body">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="input-group">
                                                        <div class="box-tools">
                                                            <asp:LinkButton ID="btnRRProcess" runat="server" CssClass="btn btn-info text-center" Font-color="Black" OnClick="btnRRProcess_Click"><i class="fa fa-plus"> Process</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnRRListClear" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnRRListClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                    <div class="panel-group" id="acrdReg">
                                                        <div class="panel panel-primary">
                                                            <div class="panel-heading">
                                                                <h5 class="panel-title" style="text-align: left">
                                                                    <a id="ancReqList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlServiceRegistration_acrdReqList" href="#BodyContent_ctrlServiceRegistration_divLReqList">
                                                                        Service Registration Request by System Clients</a>
                                                                </h5>
                                                            </div>
                                                            <div id="divLReqList" runat="server" class="panel-collapse collapse in">
                                                                <div class="panel-body">
                                                                    <asp:GridView ID="gvwRegReqList" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                                        AllowPaging="True" DataKeyNames="wsrID" OnPageIndexChanging="gvwRegReqList_PageIndexChanging"
                                                                        AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header"
                                                                        PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnSelectedIndexChanged="gvwRegReqList_SelectedIndexChanged" OnRowDataBound="gvwRegReqList_RowDataBound">
                                                                        <Columns>
                                                                            <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                                <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                            </asp:ButtonField>
                                                                            <asp:BoundField DataField="wsrID" Visible="False" />
                                                                            <asp:BoundField DataField="clntID" HeaderText="SMART ID" />
                                                                            <asp:BoundField DataField="clntNm" HeaderText="Client Company" />
                                                                            <asp:BoundField DataField="wpdID" HeaderText="Well Pad" />
                                                                            <asp:BoundField DataField="welID" HeaderText="Well / Lateral" />
                                                                            <asp:BoundField DataField="srvGrpID" HeaderText="Service Group" />
                                                                            <asp:BoundField DataField="srvID" HeaderText="SMARTs Service" />                                                                            
                                                                            <asp:BoundField DataField="isApproved" HeaderText="Request Status" />
                                                                            <asp:BoundField DataField="cTime" HeaderText="Created On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                    <asp:Label ID="lblRRContract" runat="server" CssClass="label label-info text-center" Text="Client Contract(s)" AssociatedControlID="gvwContractsList" Font-Size="Large" Visible="false" />
                                                                    <asp:GridView ID="gvwContractsList" runat="server" CssClass="mydatagrid" Width="100%" AutoGenerateColumns="False" DataKeyNames="ctrtID"
                                                                        AllowPaging="True" PageSize="15" PagerStyle-CssClass="pager" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                        OnRowDataBound="gvwContractsList_RowDataBound" EmptyDataText="No Contracts found in the System." ShowHeaderWhenEmpty="True"
                                                                        SelectedRowStyle-CssClass="selectedrow" Visible="false">
                                                                        <Columns>
                                                                            <asp:BoundField DataField="ctrtID" HeaderText="ID" />
                                                                            <asp:BoundField DataField="ctID" HeaderText="Type" />
                                                                            <asp:BoundField DataField="ctrtName" HeaderText="Name" ItemStyle-HorizontalAlign="Center" />
                                                                            <asp:BoundField DataField="cstatID" HeaderText="Status" ItemStyle-HorizontalAlign="Center" />
                                                                            <asp:BoundField DataField="ctrtStart" HeaderText="Contract Start Date" SortExpression="ctrtStart" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}"
                                                                                ItemStyle-HorizontalAlign="Center" />
                                                                            <asp:BoundField DataField="ctrtEnd" HeaderText="Contract End Date" SortExpression="ctrtEnd" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}"
                                                                                ItemStyle-HorizontalAlign="Center" />
                                                                            <asp:TemplateField HeaderText="Total Contract Days" ItemStyle-HorizontalAlign="Center">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblDttl" runat="server" Text="" BackColor="Transparent" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Contract Days Remaining" ItemStyle-HorizontalAlign="Center">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblDrm" runat="server" Text="" BackColor="Transparent" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="uTime" HeaderText="Last Updated" SortExpression="uTime" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}"
                                                                                ItemStyle-HorizontalAlign="Center" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                    <asp:Label ID="lblRegReqDetail" runat="server" CssClass="label label-info text-center" Text="Registration Request By" AssociatedControlID="gvwRegReqDetail" Font-Size="Large" Visible="false" />
                                                                    <asp:GridView ID="gvwRegReqDetail" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                                        AllowPaging="True" DataKeyNames="wsrID" AlternatingRowStyle-CssClass="AlternatingRowStyle" HeaderStyle-CssClass="header"
                                                                        PagerStyle-CssClass="pager" RowStyle-CssClass="rows" Visible="false">
                                                                        <Columns>
                                                                           <asp:BoundField DataField="wsrID" Visible="False" />
                                                                            <asp:BoundField DataField="lastName" HeaderText="Last Name" />
                                                                            <asp:BoundField DataField="firstName" HeaderText="First Name" />
                                                                            <asp:BoundField DataField="designation" HeaderText="Designation" />
                                                                            <asp:BoundField DataField="userRole" HeaderText="SMARTs Role" />
                                                                            <asp:BoundField DataField="realm" HeaderText="Account Realm" />                                                                                                                                                        
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                    <asp:Label ID="lblRegAppDetail" runat="server" CssClass="label label-info text-center" Text="Registration Approved By" AssociatedControlID="gvwRegAppDetail" Font-Size="Large" Visible="false" />
                                                                    <asp:GridView ID="gvwRegAppDetail" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                                        AllowPaging="True" DataKeyNames="wsrID" AlternatingRowStyle-CssClass="AlternatingRowStyle" HeaderStyle-CssClass="header"
                                                                        PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnRowDataBound="gvwRegAppDetail_RowDataBound" Visible="false">
                                                                        <Columns>
                                                                           <asp:BoundField DataField="wsrID" Visible="False" />
                                                                            <asp:BoundField DataField="appLast" HeaderText="Last Name" />
                                                                            <asp:BoundField DataField="appFirst" HeaderText="First Name" />
                                                                            <asp:BoundField DataField="appRole" HeaderText="SMARTs Role" />                                                                            
                                                                            <asp:BoundField DataField="appStat" HeaderText="Registration Status" />                                                                            
                                                                            <asp:BoundField DataField="appTime" HeaderText="Approved At" />
                                                                            <asp:BoundField DataField="appWait" HeaderText="Registration Timespan" />                                                                                                                                                        
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-primary">
                                                            <div class="panel-heading">
                                                                <h5 class="panel-title" style="text-align: left">
                                                                    <a id="ancReqProc" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlServiceRegistration_acrdReqList" href="#BodyContent_ctrlServiceRegistration_divReqProc">
                                                                        Process Service Registration Request</a>
                                                                </h5>
                                                            </div>
                                                            <div id="divReqProc" runat="server" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                        <asp:DropDownList ID="ddlClientList" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                            OnSelectedIndexChanged="ddlClientList_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select SMARTs Client Company ---" />
                                                                        </asp:DropDownList>                                                                    
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                        <asp:DropDownList ID="ddlRegReqList" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true" Enabled="false"
                                                                            OnSelectedIndexChanged="ddlRegReqList_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select Registration Request ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                                        <asp:DropDownList ID="ddlRegReqContractType" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                            Enabled="false" OnSelectedIndexChanged="ddlRegReqContractType_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select Client Contract Type ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                                        <asp:DropDownList ID="ddlRegReqContract" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                            Enabled="false"
                                                                            OnSelectedIndexChanged="ddlRegReqContract_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select Client Contract ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                                        <asp:DropDownList ID="ddlRegReqPrice" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                            Enabled="false"
                                                                            OnSelectedIndexChanged="ddlRegReqPrice_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select Contracted Price ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                                        <asp:TextBox ID="txtRegReqApproval" runat="server" CssClass="form-control" Enabled="false" Text="" placeholder="Registration Request Approval Status" />
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                                        <asp:DropDownList ID="ddlRegReqApproval" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true" Enabled="false"
                                                                            OnSelectedIndexChanged="ddlRegReqApproval_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select Approval Status ---" />
                                                                        </asp:DropDownList>
                                                                    </div>                                               
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <div runat="server" id="enReg" visible="false">
                                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                                <h4><i runat="server" id="iReg"><span runat="server" id="spnReg"></span></i></h4>
                                                                                <asp:Label ID="lblRegSuccess" runat="server" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <asp:LinkButton ID="btnRegWell" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnRegWell_Click"><i class="fa fa-plus"> Update</i></asp:LinkButton>
                                                                            <asp:LinkButton ID="btnRegWellReset" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnRegWellReset_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                            <asp:LinkButton ID="btnRegWellDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnRegWellDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
