﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlRawQC_2A.ascx.cs" Inherits="SmartsVer1.Control.Services.ctrlRawQC_2A" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<!-- Stylesheet -->
<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dataTables.bootstrap.min.css" rel="stylesheet" />
<!-- Javascript for ref lib -->
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>
<!--- local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }
    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }
</style>
<asp:UpdateProgress ID="uprgrsMeta" runat="server" DisplayAfter="0" DynamicLayout="true" AssociatedUpdatePanelID="upnlQC">
    <ProgressTemplate>
        <div id="divMetaProcessing" runat="server" class="updateProgress">
            <asp:Image ID="imgMetaProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<asp:UpdatePanel runat="server" ID="upnlQC">
    <ContentTemplate>
        <!--- Page Menu -->
        <div class="row bg-black">
            <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom: 2px; padding-left: 2px; padding-top: 2px">
                <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
            </div>
            <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
                <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
            </div>
        </div>
        <div class="row bg-black">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
                <asp:LoginView ID='LoginView1' runat='server'>
                    <AnonymousTemplate>
                        <li style="vertical-align: middle; display: table-cell;">
                            <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                                ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                        </li>
                    </AnonymousTemplate>
                    <LoggedInTemplate>
                        <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">(
                            <asp:LoginName ID="LoginName1" runat="server" />
                            )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                        </li>
                    </LoggedInTemplate>
                </asp:LoginView>
            </div>
        </div>
        <!-- Breadcrumb -->
        <div class="row bg-white">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box-tools pull-right">
                    <ol class="breadcrumb">
                        <li><a href="../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                        <li>SMARTs</li>
                        <li><a href="../../Services/fecRawSMART.aspx">SMARTs Audit/QC</a></li>
                    </ol>
                </div>
            </div>
        </div>
        <!--- Page Content --->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row bg-white">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-danger">
                        <div class="panel-body">
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div id="divSolar" class="bg-danger">
                                    <div class="panel-body">
                                        <div class="col-xs-3 text-left">
                                            <asp:Image ID="imgSolar" runat="server" ImageUrl="~/Images/SolarRays.png" ImageAlign="Middle" />
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div>
                                                <h4>Solar X-Rays Activity</h4>
                                            </div>
                                            <div id="divSolarLnk" runat="server">
                                                <asp:ImageButton ID="imgbSolarLink" runat="server" Width="40%" ImageUrl="http://www.n3kl.org/sun/images/status.gif?"
                                                    ImageAlign="AbsMiddle" OnClick="imgbtnSolar_Click" CausesValidation="False" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div id="divMag" class="bg-danger">
                                    <div class="panel-body">
                                        <div class="col-xs-3">
                                            <asp:Image ID="imgMag" runat="server" ImageUrl="~/Images/MagField.png" ImageAlign="Middle" Height="90px" />
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div>
                                                <h4>Geomagnetic Field Activity</h4>
                                            </div>
                                            <div id="divMagLnk" runat="server">
                                                <asp:ImageButton ID="imgbMag" runat="server" Width="40%" ImageUrl="http://www.n3kl.org/sun/images/kpstatus.gif?"
                                                    ImageAlign="AbsMiddle" OnClick="imgbtnGeoMag_Click" CausesValidation="False" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div id="divGlobe" class="bg-danger">
                                    <div class="panel-body">
                                        <div class="col-xs-3">
                                            <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/MagField_2.png" ImageAlign="Middle" Height="90px" />
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div>
                                                <h4>Regional Magnetic Activity</h4>
                                            </div>
                                            <div id="divGlobeLnk" runat="server">
                                                <a href="http://geomag.usgs.gov/realtime/" target="_blank" style="color: black;">USA</a> |
                                                    <a href="http://www.spaceweather.ca/current-actuelle/short-court/sfst-5-eng.php" target="_blank" style="color: black;">Canada</a>
                                                |
                                                    <a href="http://www.intermagnet.org/activitymap/activitymap-eng.php" target="_blank" style="color: black;">Global</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row bg-white">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div id="panelGages" class="panel panel-danger">
                        <div class="panel-body">
                            <div id="divGuages" runat="server" class="col-xs-12 col-sm-12 col-md-offset-1 col-lg-offset-1">
                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                    <div class="small-box bg-blue">
                                        <div id="divGuageJob" runat="server" class="inner" />
                                        <div class="icon">
                                            <i class="fa fa-list" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                    <div class="small-box bg-orange-active">
                                        <div id="divGuageWell" runat="server" class="inner" />
                                        <div class="icon">
                                            <i class="fa fa-tasks" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                    <div class="small-box bg-olive">
                                        <div id="divAscWell" runat="server" class="inner" />
                                        <div class="icon">
                                            <i class="fa fa-chain-broken" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                    <div class="small-box bg-olive">
                                        <div id="divMRM" runat="server" class="inner" />
                                        <div class="icon">
                                            <i class="fa fa-heartbeat" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                    <div class="small-box bg-olive">
                                        <div id="divMsngBHA" runat="server" class="inner" />
                                        <div class="icon">
                                            <i class="fa fa-chain-broken" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="acrdQCOuter" runat="server" class="panel panel-danger">
                        <div id="panelOuterHeading" class="panel-heading">
                            <h5 class="panel-title" style="text-align: left">
                                <a id="ancQCOuter" runat="server" class="disabled" href="#">SMARTs Raw Data Audit/QC</a>
                            </h5>
                        </div>
                        <div id="panelOuterBody" runat="server" class="panel-body">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div id="divProgress" class="panel panel-danger">
                                    <div class="panel-body">
                                        <ol id="olStepper" runat="server" class="stepper" data-stepper-steps="7">
                                            <li id="liStep1" runat="server" class="stepper-todo">Operator Company</li>
                                            <li id="liStep2" runat="server" class="stepper-todo">Well Pad</li>
                                            <li id="liStep3" runat="server" class="stepper-todo">Well/Lateral</li>
                                            <li id="liStep4" runat="server" class="stepper-todo">Section</li>
                                            <li id="liStep5" runat="server" class="stepper-todo">Run</li>
                                            <li id="liStep6" runat="server" class="stepper-todo">Data</li>
                                            <li id="liStep7" runat="server" class="stepper-todo">Audit/QC</li>
                                        </ol>
                                    </div>
                                </div>
                                <div class="panel panel-danger">
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group" style="width: 100%;">
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:LinkButton ID="btnMetaCancel" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnMetaCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                </div>
                                            </div>
                                            <div class="panel panel-info">
                                                <div class="panel-heading">
                                                    <h5 class="panel-title" style="text-align: left">
                                                        <a id="ancHead" runat="server" class="disabled" href="#">Operator Company</a>
                                                    </h5>
                                                </div>
                                                <div id="divOpr" runat="server" class="panel-body">
                                                    <div class="table-responsive">
                                                        <asp:GridView runat="server" ID="gvwGlobalOptr" DataKeyNames="clntID" CssClass="mydatagrid" AllowPaging="True" AllowSorting="True"
                                                            AutoGenerateColumns="False" EmptyDataText="No Operator Company found."
                                                            OnPageIndexChanging="gvwGlobalOptr_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15"
                                                            HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow"
                                                            OnSelectedIndexChanged="gvwGlobalOptr_SelectedIndexChanged">
                                                            <Columns>
                                                                <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                    <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                </asp:ButtonField>
                                                                <asp:BoundField DataField="clntID" Visible="False" />
                                                                <asp:BoundField DataField="clntName" HeaderText="Operator Company" />
                                                                <asp:BoundField DataField="wpdCount" HeaderText="Global Well Pad Count">
                                                                    <ItemStyle CssClass="padRight" HorizontalAlign="Right" VerticalAlign="Middle" Font-Bold="True"></ItemStyle>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="welCount" HeaderText="Global Well Count">
                                                                    <ItemStyle CssClass="padRight" HorizontalAlign="Right" VerticalAlign="Middle" Font-Bold="True"></ItemStyle>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="welGlobal" HeaderText="SMARTs Well Count">
                                                                    <ItemStyle CssClass="padRight" HorizontalAlign="Right" VerticalAlign="Middle" Font-Bold="True"></ItemStyle>
                                                                </asp:BoundField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                                <div id="divPad" runat="server" visible="false" class="panel-body">
                                                    <div class="table-responsive">
                                                        <asp:GridView ID="gvwWellPads" runat="server" EmptyDataText="No Well Pad(s) found for selected Operator"
                                                            ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="wpdID"
                                                            AllowPaging="True" CssClass="mydatagrid" OnPageIndexChanging="gvwWellPads_PageIndexChanging" OnSelectedIndexChanged="gvwWellPads_SelectedIndexChanged"
                                                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                            RowStyle-CssClass="rows">
                                                            <Columns>
                                                                <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                    <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                </asp:ButtonField>
                                                                <asp:BoundField DataField="wpdID" HeaderText="wpd" ReadOnly="True" Visible="false" />
                                                                <asp:BoundField DataField="welPadName" HeaderText="Well Pad" />
                                                                <asp:BoundField DataField="field" HeaderText="O&G Field" />
                                                                <asp:BoundField DataField="county" HeaderText="County/District/Division/Municipality" />
                                                                <asp:BoundField DataField="state" HeaderText="State/Province" />
                                                                <asp:BoundField DataField="country" HeaderText="Country/Nation" />
                                                                <asp:BoundField DataField="sreg" HeaderText="Sub-Region" />
                                                                <asp:BoundField DataField="reg" HeaderText="Major Region" />
                                                                <asp:BoundField DataField="swlCount" HeaderText="SMARTs Well Count">
                                                                    <ItemStyle CssClass="padRight" HorizontalAlign="Right" VerticalAlign="Middle" Font-Bold="True"></ItemStyle>
                                                                </asp:BoundField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                                <div id="divWell" runat="server" visible="false" class="panel-body">
                                                    <div class="table-responsive">
                                                        <asp:GridView ID="gvwWellList" runat="server" CssClass="mydatagrid" EmptyDataText="No Wells associated with the selected Service Order #"
                                                            AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="welID" OnSelectedIndexChanged="gvwWellList_SelectedIndexChanged"
                                                            ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwWellList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                            PageSize="15" PagerSettings-Mode="Numeric" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Center" PagerStyle-VerticalAlign="Middle"
                                                            HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow"
                                                            OnRowDataBound="gvwWellList_RowDataBound">
                                                            <Columns>
                                                                <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                    <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                </asp:ButtonField>
                                                                <asp:BoundField DataField="welID" Visible="false" />
                                                                <asp:BoundField DataField="welName" HeaderText="Well / Lateral" />
                                                                <asp:BoundField DataField="field" HeaderText="Oil & Gas Field" />
                                                                <asp:BoundField DataField="county" HeaderText="County / District" />
                                                                <asp:BoundField DataField="state" HeaderText="State / Province" />
                                                                <asp:BoundField DataField="spud" HeaderText="Spud Date" />
                                                                <asp:BoundField DataField="wstID" HeaderText="Status" />
                                                            </Columns>
                                                        </asp:GridView>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div runat="server" id="wellEnclosure" visible="false">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                <h4><i runat="server" id="iWell"><span runat="server" id="spnWell"></span></i></h4>
                                                                <asp:Label ID="lblWell" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divSec" runat="server" visible="false" class="panel-body">
                                                    <div class="table-responsive">
                                                        <asp:GridView ID="gvwSectionList" runat="server" CssClass="mydatagrid" EmptyDataText="No Well/Lateral Section associated with the selected Well"
                                                            AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="wswID" OnSelectedIndexChanged="gvwSectionList_SelectedIndexChanged"
                                                            ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwSectionList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                            PageSize="15" PagerSettings-Mode="Numeric" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Center" PagerStyle-VerticalAlign="Middle"
                                                            HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                                            <Columns>
                                                                <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                    <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                </asp:ButtonField>
                                                                <asp:BoundField DataField="wswID" Visible="false" />
                                                                <asp:BoundField DataField="wsName" HeaderText="Well/Lateral Section Name" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                                <asp:BoundField DataField="rnCount" HeaderText="Run Count" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                                <div id="divRun" runat="server" visible="false" class="panel-body">
                                                    <div class="table-responsive">
                                                        <asp:GridView ID="gvwRunList" runat="server" CssClass="mydatagrid" EmptyDataText="No Run associated with the selected Section"
                                                            AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="runID" OnSelectedIndexChanged="gvwRunList_SelectedIndexChanged"
                                                            ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwRunList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                            PageSize="15" PagerSettings-Mode="Numeric" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Center" PagerStyle-VerticalAlign="Middle"
                                                            HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                                            <Columns>
                                                                <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                    <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                </asp:ButtonField>
                                                                <asp:BoundField DataField="runID" Visible="false" />
                                                                <asp:BoundField DataField="rnName" HeaderText="Run Name" />
                                                                <asp:BoundField DataField="rnStatus" HeaderText="Status" />
                                                                <asp:BoundField DataField="rnBHA" HeaderText="BHA Signature ID" />
                                                                <asp:BoundField DataField="rnToolcode" HeaderText="Toolcode" />
                                                                <asp:BoundField DataField="rnSQC" HeaderText="Survey Qualification Criteria" />
                                                                <asp:BoundField DataField="rnStartD" HeaderText="Start Measured Depth(m)" ItemStyle-HorizontalAlign="Right" />
                                                                <asp:BoundField DataField="rnEndD" HeaderText="End Measured Depth(m)" ItemStyle-HorizontalAlign="Right" />
                                                            </Columns>
                                                        </asp:GridView>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div runat="server" id="runEnclosure" visible="false">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                <h4><i runat="server" id="iRun"><span runat="server" id="spnRun"></span></i></h4>
                                                                <asp:Label ID="lblRun" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divDataEntry" runat="server" visible="false" class="panel-body">
                                                    <div class="table-responsive">
                                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                            <div class="input-group">
                                                                <span id="spnDataType" class="input-group-addon bg-light-blue">Survey Row Type</span>
                                                                <asp:DropDownList ID="ddlDataType" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlDataType_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0" Text="--- Select Survey Row Type ---"></asp:ListItem>
                                                                    <asp:ListItem Value="1" Text="Raw Survey Row"></asp:ListItem>
                                                                    <asp:ListItem Value="2" Text="Results Survey Row"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                            <div class="input-group">
                                                                <span id="spnUploadMethod" class="input-group-addon bg-light-blue">Upload Method</span>
                                                                <asp:DropDownList ID="ddlUploadMethod" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlUploadMethod_SelectedIndexChanged"
                                                                    Enabled="false">
                                                                    <asp:ListItem Value="0" Text="--- Select Survey Row Upload Method ---"></asp:ListItem>
                                                                    <asp:ListItem Value="1" Text="Copy/Paste single Survey Row"></asp:ListItem>
                                                                    <asp:ListItem Value="2" Text="Upload Survey Row File"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divManData" runat="server" visible="false" class="panel-body">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon bg-light-blue">Input Survey</span>
                                                            <asp:TextBox ID="txtQCManualProcess" runat="server" CssClass="form-control" Visible="false" placeholder="Copy/Paste Raw Data Survey line. Format: Timestamp, Depth (m)*, Gx (g)*, Gy (g)*, Gz (g)*, Bx (nT)*, By (nT)*, Bz (nT)* ...  ( * required )" />
                                                            <asp:TextBox ID="txtQCResultProcess" runat="server" CssClass="form-control" Visible="false" placeholder="Copy/Paste Result Data Survey line. Format: Depth (m)*, Inc (°)*, Azm (°)*, Dip (°)*, BTotal (nT), GTotal (g), GTF (°)* ...  ( * required )" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div runat="server" id="txtDataEnclosure" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iTXTDMessage"><span runat="server" id="txtDHeader"></span></i></h4>
                                                            <asp:Label runat="server" ID="lblManual" Text="" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group" style="width: 100%;">
                                                            <asp:LinkButton ID="btnQCManualUpload" runat="server" CssClass="btn btn-success text-center" OnClick="btnQCManualUpload_Click"><i class="fa fa-upload"> Upload Survey</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnCancelManualUpload" runat="server" CssClass="btn btn-warning text-center" OnClick="btnCancelManualUpload_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divFileData" runat="server" visible="false" class="panel-body">
                                                    <div class="col-xs-12 col-sm-12 col-md-offset-1 col-lg-offset-1">
                                                        <div class="input-group">
                                                            <span id="spnFile" runat="server" class="input-group-addon bg-light-blue">Select Survey File</span>
                                                            <asp:FileUpload ID="FileUploadControl" runat="server" Font-Size="Large" Width="60%" placeholder="Select Survey file to Upload" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div runat="server" id="filDEnclosure" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="ifilDMessage"><span runat="server" id="spnFILED"></span></i></h4>
                                                            <asp:Label ID="FileUploadStatusLabel" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group" style="width: 100%;">
                                                            <asp:LinkButton ID="btn_FileUpload" runat="server" CssClass="btn btn-success text-center" OnClick="btn_FileUpload_Click"><i class="fa fa-upload"> Upload File</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnCancelFileUpload" runat="server" CssClass="btn btn-warning text-center" OnClick="btnCancelFileUpload_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divUploadedData" runat="server" visible="false" class="panel-body">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div id="divSrvCheck" runat="server">
                                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                                <asp:Label ID="lblTotalRows" runat="server" CssClass="control-label label-default" Font-Bold="true" Font-Size="Larger" Text="" />
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                                <asp:Label ID="lblTotalRaw" runat="server" CssClass="control-label label-default" Visible="false" Font-Bold="true" Font-Size="Larger"
                                                                    Text="" />
                                                                <asp:Label ID="lblTotalResult" runat="server" CssClass="control-label label-default" Visible="false" Font-Bold="true" Font-Size="Larger"
                                                                    Text="" />
                                                            </div>
                                                            <asp:GridView ID="gvwViewRawDisplay" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="drID"
                                                                SelectedRowStyle-CssClass="selectedrow" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                                EmptyDataText="No Data Uploaded" AllowPaging="True" AllowSorting="True" PagerStyle-CssClass="pager" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                OnPageIndexChanging="gvwViewRawDisplay_PageIndexChanging" Visible="false">
                                                                <Columns>
                                                                    <asp:BoundField DataField="drID" Visible="False" />
                                                                    <asp:BoundField DataField="Timestamp" HeaderText="Timestamp" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" />
                                                                    <asp:BoundField DataField="Depth" HeaderText="Depth" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="Gx" HeaderText="Ax / Gx" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="Gy" HeaderText="Ay / Gy" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="Gz" HeaderText="Az / Gz" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="Bx" HeaderText="Mx / Bx" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="By" HeaderText="My / By" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="Bz" HeaderText="Mz / Bz" ItemStyle-HorizontalAlign="Right" />
                                                                </Columns>
                                                            </asp:GridView>
                                                            <asp:GridView ID="gvwViewResultsDispaly" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="drID"
                                                                SelectedRowStyle-CssClass="selectedrow" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                                EmptyDataText="No Data Uploaded" AllowPaging="True" AllowSorting="True" PagerStyle-CssClass="pager" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                OnPageIndexChanging="gvwViewResultsDispaly_PageIndexChanging" Visible="false">
                                                                <Columns>
                                                                    <asp:BoundField DataField="drID" Visible="False" />
                                                                    <asp:BoundField DataField="Depth" HeaderText="Depth (m)" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="ResInc" HeaderText="Inclination (°)" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="ResAzm" HeaderText="Azimuth (°)" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="ResDip" HeaderText="Dip (°)" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="ResBT" HeaderText="B-Total (nT)" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="ResGT" HeaderText="G-Total (g)" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="ResGTF" HeaderText="GTF (°)" ItemStyle-HorizontalAlign="Right" />
                                                                </Columns>
                                                            </asp:GridView>
                                                            <asp:GridView ID="gvwReadyRawData" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="drID"
                                                                SelectedRowStyle-CssClass="selectedrow" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                                EmptyDataText="No Data Uploaded" AllowPaging="True" AllowSorting="True" PagerStyle-CssClass="pager" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                OnPageIndexChanging="gvwReadyRawData_PageIndexChanging" Visible="false">
                                                                <Columns>
                                                                    <asp:BoundField DataField="drID" Visible="False" />
                                                                    <asp:BoundField DataField="Depth" HeaderText="Depth (m)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                    <asp:BoundField DataField="Gx" HeaderText="Ax / Gx (g)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.000000000}" />
                                                                    <asp:BoundField DataField="Gy" HeaderText="Ay / Gy (g)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.000000000}" />
                                                                    <asp:BoundField DataField="Gz" HeaderText="Az / Gz (g)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.000000000}" />
                                                                    <asp:BoundField DataField="Bx" HeaderText="Mx / Bx (nT)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                    <asp:BoundField DataField="By" HeaderText="My / By (nT)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                    <asp:BoundField DataField="Bz" HeaderText="Mz / Bz (nT)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                </Columns>
                                                            </asp:GridView>
                                                            <asp:GridView ID="gvwReadyResultsData" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="drID"
                                                                SelectedRowStyle-CssClass="selectedrow" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                                EmptyDataText="No Data Uploaded" AllowPaging="True" AllowSorting="True" PagerStyle-CssClass="pager" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                OnPageIndexChanging="gvwReadyResultsData_PageIndexChanging" Visible="false">
                                                                <Columns>
                                                                    <asp:BoundField DataField="drID" Visible="False" />
                                                                    <asp:BoundField DataField="Depth" HeaderText="Depth (m)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                    <asp:BoundField DataField="Inc" HeaderText="Inclination (°)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                    <asp:BoundField DataField="Azm" HeaderText="Azimuth (°)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.0000}" />
                                                                    <asp:BoundField DataField="Dip" HeaderText="Dip (°)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                    <asp:BoundField DataField="BTotal" HeaderText="B-Total (nT)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                    <asp:BoundField DataField="GTotal" HeaderText="G-Total (g)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.000000000}" />
                                                                    <asp:BoundField DataField="GTF" HeaderText="GTF (°)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                    <div id="divLenU" runat="server" class="form-group">
                                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="text-align: right; align-content: center; vertical-align: middle;">
                                                            <asp:Label ID="Label1" runat="server" CssClass="control-label label-default" Font-Bold="true" Font-Size="Larger" Text="Input Depth Units" />
                                                        </div>
                                                        <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                                            <div id="divRDLenU" runat="server">
                                                                <asp:RadioButton ID="rdbFT" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Feet" OnCheckedChanged="rdbFT_CheckedChanged" />
                                                                <asp:RadioButton ID="rdbMT" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Meter" OnCheckedChanged="rdbMT_CheckedChanged" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="divAclU" runat="server" class="form-group">
                                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="text-align: right; align-content: center; vertical-align: middle;">
                                                            <asp:Label ID="lblAclU" runat="server" CssClass="control-label label-default" Font-Bold="true" Font-Size="Larger" Text="Input Accelerometer Units" />
                                                        </div>
                                                        <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                                            <div id="divRDAclU" runat="server">
                                                                <asp:RadioButton ID="rdbCMS" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Accl. due to Gravity (cm/sec. sq)"
                                                                    OnCheckedChanged="rdbCMS_CheckedChanged" />
                                                                <asp:RadioButton ID="rdbFSC" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Accl. due to Gravity (ft/sec. sq)"
                                                                    OnCheckedChanged="rdbFSC_CheckedChanged" />
                                                                <asp:RadioButton ID="rdbG" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Accl. due to Gravity (g)"
                                                                    OnCheckedChanged="rdbG_CheckedChanged" />
                                                                <asp:RadioButton ID="rdbMSC" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Accl. due to Gravity (m/sec. sq)"
                                                                    OnCheckedChanged="rdbMSC_CheckedChanged" />
                                                                <asp:RadioButton ID="rdbMG" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Accl. due to Gravity (mg)"
                                                                    OnCheckedChanged="rdbMG_CheckedChanged" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="divMagU" runat="server" class="form-group">
                                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="text-align: right; align-content: center; vertical-align: middle;">
                                                            <asp:Label ID="lblMagU" runat="server" CssClass="control-label label-default" Font-Bold="true" Font-Size="Larger" Text="Input Magnetometer Units" />
                                                        </div>
                                                        <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                                            <div id="divRDMagU" runat="server">
                                                                <asp:RadioButton ID="rdbGss" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Gauss (G)"
                                                                    OnCheckedChanged="rdbGss_CheckedChanged" />
                                                                <asp:RadioButton ID="rdbT" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Tesla (T)" OnCheckedChanged="rdbT_CheckedChanged" />
                                                                <asp:RadioButton ID="rdbMLIT" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="Milli Tesla (mT)"
                                                                    OnCheckedChanged="rdbMLIT_CheckedChanged" />
                                                                <asp:RadioButton ID="rdbMCT" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="MicroTesla (µT)"
                                                                    OnCheckedChanged="rdbMCT_CheckedChanged" />
                                                                <asp:RadioButton ID="rdbNT" runat="server" CssClass="radio-inline" AutoPostBack="true" Checked="false" Text="NanoTesla (nT)"
                                                                    OnCheckedChanged="rdbNT_CheckedChanged" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="divSensors" runat="server" class="form-group">
                                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="text-align: right; align-content: center; vertical-align: middle;">
                                                            <asp:Label ID="lbl_Flip" runat="server" CssClass="control-label label-default" Font-Bold="true" Font-Size="Larger" Text="Select Sensor(s) for data transformation" />
                                                        </div>
                                                        <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                                            <div id="divChecks" runat="server">
                                                                <fieldset>
                                                                    <asp:CheckBox ID="chk_NoSignTransformation" runat="server" Text="None" CssClass="checkbox-inline" CausesValidation="True"
                                                                        AutoPostBack="True" OnCheckedChanged="chk_NoSignTransformation_CheckedChanged" />
                                                                    <asp:CheckBox ID="chk_FlipGx" runat="server" Text="Gx" CssClass="checkbox-inline" Checked="false" AutoPostBack="true"
                                                                        OnCheckedChanged="chk_FlipGx_CheckedChanged" />
                                                                    <asp:CheckBox ID="chk_FlipGy" runat="server" Text="Gy" CssClass="checkbox-inline" Checked="false" AutoPostBack="true"
                                                                        OnCheckedChanged="chk_FlipGy_CheckedChanged" />
                                                                    <asp:CheckBox ID="chk_FlipGz" runat="server" Text="Gz" CssClass="checkbox-inline" Checked="false" AutoPostBack="true"
                                                                        OnCheckedChanged="chk_FlipGz_CheckedChanged" />
                                                                    <asp:CheckBox ID="chk_FlipBx" runat="server" Text="Bx" CssClass="checkbox-inline" Checked="false" AutoPostBack="true"
                                                                        OnCheckedChanged="chk_FlipBx_CheckedChanged" />
                                                                    <asp:CheckBox ID="chk_FlipBy" runat="server" Text="By" CssClass="checkbox-inline" Checked="false" AutoPostBack="true"
                                                                        OnCheckedChanged="chk_FlipBy_CheckedChanged" />
                                                                    <asp:CheckBox ID="chk_FlipBz" runat="server" Text="Bz" CssClass="checkbox-inline" Checked="false" AutoPostBack="true"
                                                                        OnCheckedChanged="chk_FlipBz_CheckedChanged" />
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group" style="width: 100%;">
                                                            <asp:LinkButton ID="btnProcessRawData" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnProcessRawData_Click"><i class="fa fa-gears"> Process Raw Survey(s)</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnProcessSrvyCancel" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnProcessSrvyCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divTransformData" runat="server" visible="false" class="panel-body">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="panel-body">
                                                            <asp:GridView ID="gvwSelectedRaw" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="drID"
                                                                SelectedRowStyle-CssClass="selectedrow" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                                EmptyDataText="No Data Uploaded" AllowPaging="True" AllowSorting="True" PagerStyle-CssClass="pager" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                OnPageIndexChanging="gvwSelectedRaw_PageIndexChanging" OnRowDataBound="gvwSelectedRaw_RowDataBound" Visible="false">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Selected" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="srvyCheck" runat="server" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="drID" Visible="False" />
                                                                    <asp:BoundField DataField="Depth" HeaderText="Depth (m)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                    <asp:BoundField DataField="rGx" HeaderText="Ax / Gx (g)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: 0.000000000}" />
                                                                    <asp:BoundField DataField="rGy" HeaderText="Ay / Gy (g)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: 0.000000000}" />
                                                                    <asp:BoundField DataField="rGz" HeaderText="Az / Gz (g)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: 0.000000000}" />
                                                                    <asp:BoundField DataField="rBx" HeaderText="Mx / Bx (nT)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                    <asp:BoundField DataField="rBy" HeaderText="My / By (nT)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                    <asp:BoundField DataField="rBz" HeaderText="Mz / Bz (nT)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #.00}" />
                                                                </Columns>
                                                            </asp:GridView>
                                                            <asp:GridView ID="gvwSelectedResult" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="drID"
                                                                SelectedRowStyle-CssClass="selectedrow" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                                EmptyDataText="No Data Uploaded" AllowPaging="True" AllowSorting="True" PagerStyle-CssClass="pager" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                OnPageIndexChanging="gvwSelectedResult_PageIndexChanging" Visible="false" OnRowDataBound="gvwSelectedResult_RowDataBound">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Selected" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="srvyCheck" runat="server" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="drID" Visible="False" />
                                                                    <asp:BoundField DataField="resDepth" HeaderText="Depth (m)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #0.00}" />
                                                                    <asp:BoundField DataField="resInc" HeaderText="Inclination" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #0.00}" />
                                                                    <asp:BoundField DataField="resAzm" HeaderText="Azimuth" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #0.0000}" />
                                                                    <asp:BoundField DataField="resDip" HeaderText="Dip" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #0.00}" />
                                                                    <asp:BoundField DataField="resBTotal" HeaderText="B-Total" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #0.00}" />
                                                                    <asp:BoundField DataField="resGTotal" HeaderText="G-Total" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #0.000000000}" />
                                                                    <asp:BoundField DataField="resGTF" HeaderText="GTF" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: #0.00}" />
                                                                </Columns>
                                                            </asp:GridView>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="input-group" style="width: 100%;">
                                                                    <asp:LinkButton ID="btnProcessReadyData" runat="server" CssClass="btn btn-success text-center" OnClick="btnProcessReadyData_Click"><i class="fa fa-life-ring"> Audit/QC</i></asp:LinkButton>
                                                                    <asp:LinkButton ID="btnCancelReadyData" runat="server" CssClass="btn btn-warning text-center" OnClick="btnCancelReadyData_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="divQC" runat="server" class="panel panel-success" visible="false">
                                                <div class="panel-heading">
                                                    <h5 class="panel-title" style="text-align: left">
                                                        <asp:Label ID="lblQC" runat="server" CssClass="label" Text="Audit/QC Results" ForeColor="Black"></asp:Label>
                                                    </h5>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div id="divAuditedData" runat="server" class="panel-heading"></div>
                                                        <div class="panel-body">
                                                            <div id="charts" runat="server">
                                                                <h3>Analysis Chart </h3>
                                                                <div id="divECharts" runat="server" style="height: 700px;"></div>
                                                                <h3>Azimuth Analysis Chart </h3>
                                                                <div id="divEChartsAzim" runat="server" style="height: 450px;"></div>
                                                                <h3>Analysis Chart - 3D </h3>
                                                                <div id="divECharts3D" runat="server" style="height: 700px;"></div>
                                                                <asp:Literal ID="ltrECHarts" runat="server"></asp:Literal>
                                                                <asp:Literal ID="ltrECHartsAzim" runat="server"></asp:Literal>
                                                                <asp:Literal ID="ltrECHarts3D" runat="server"></asp:Literal>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div id="divResultsGridRow" class="row">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <asp:GridView ID="gvwRQCResults" runat="server" CssClass="mydatagrid" AllowPaging="True"
                                                                    AllowSorting="True" PageSize="15" AutoGenerateColumns="False" DataKeyNames="resID" Visible="False" SelectedRowStyle-CssClass="selectedrow"
                                                                    PagerStyle-CssClass="pager" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                    OnSelectedIndexChanged="gvwRQCResults_SelectedIndexChanged" OnPageIndexChanging="gvwRQCResults_PageIndexChanging">
                                                                    <Columns>
                                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                            <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                        </asp:ButtonField>
                                                                        <asp:BoundField DataField="resID" HeaderText="resID" Visible="False" />
                                                                        <asp:BoundField DataField="srvyID" HeaderText="Survey ID" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%" />
                                                                        <asp:BoundField DataField="Depth" HeaderText="Survey Depth" ItemStyle-Width="5%" />
                                                                        <asp:BoundField DataField="acID" HeaderText="Selected Azimuth" ItemStyle-Width="10%" />
                                                                        <asp:BoundField DataField="taSt" HeaderText="SMARTs Analysis" ItemStyle-Width="80%" ItemStyle-HorizontalAlign="Left" />
                                                                        <asp:BoundField DataField="chSt" Visible="false" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                                <asp:Label ID="lblMaxRunTieIn" runat="server" Text="End-Depth Tie-In" BackColor="LightBlue" Width="99.6%"
                                                                    ForeColor="Purple" Font-Size="Large" />
                                                                <asp:GridView ID="gvwMaxRunTieIn" runat="server" CssClass="mydatagrid" AllowPaging="True"
                                                                    AutoGenerateColumns="False" DataKeyNames="tisID" ShowHeaderWhenEmpty="True" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                    HeaderStyle-CssClass="header">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="tisID" Visible="false" />
                                                                        <asp:BoundField DataField="title" />
                                                                        <asp:BoundField HeaderText="M. Depth" DataField="md" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField HeaderText="Inclincation" DataField="inc" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField HeaderText="Azimuth" DataField="azm" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField HeaderText="TVD" DataField="tvd" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField HeaderText="North" DataField="north" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.0000}" />
                                                                        <asp:BoundField HeaderText="East" DataField="east" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.0000}" />
                                                                        <asp:BoundField HeaderText="Vertical Section" DataField="vs" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField HeaderText="Subsea Elev." DataField="subsea" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                        <div id="divSQCGridRow" class="row">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <asp:Label ID="lblQCResSQC" runat="server" Text="Survey Qualification Criterion" BackColor="LightBlue"
                                                                    ForeColor="Purple" Visible="False" Font-Size="Large" class="form-control" />
                                                                <asp:GridView ID="gvwResSQC" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="sqcID"
                                                                    Visible="False" SelectedRowStyle-BackColor="#CCFFCC" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="sqcID" Visible="false" />
                                                                        <asp:BoundField DataField="sqcName" HeaderText="Criterion Name" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="24%" />
                                                                        <asp:BoundField DataField="sqcDip" HeaderText="Dip Tolerance" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="24%" />
                                                                        <asp:BoundField DataField="sqcBT" HeaderText="B-Total Tolerance" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="24%" />
                                                                        <asp:BoundField DataField="sqcGT" HeaderText="G-Total Tolerance" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="24%" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                        <div id="divMetaDataGridRow" class="row">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <asp:Label ID="lblQCResMetaData" runat="server" Text="Survey Reference Values" BackColor="LightBlue" class="form-control"
                                                                    ForeColor="Purple" Visible="False" Font-Size="Large" />
                                                                <asp:GridView ID="gvwRefVals" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="rfmgID"
                                                                    Visible="False" SelectedRowStyle-BackColor="#CCFFCC" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="rfmgID" HeaderText="rfmgID" Visible="False" />
                                                                        <asp:BoundField DataField="ifrID" HeaderText="IFR/IIFR" ItemStyle-HorizontalAlign="Center" />
                                                                        <asp:BoundField DataField="mdec" HeaderText="Magnetic Declination°" ItemStyle-HorizontalAlign="Center" />
                                                                        <asp:BoundField DataField="dip" HeaderText="Dip°" ItemStyle-HorizontalAlign="Center" />
                                                                        <asp:BoundField DataField="bTotal" HeaderText="B-Total" ItemStyle-HorizontalAlign="Center" />
                                                                        <asp:BoundField DataField="gTotal" HeaderText="G-Total" ItemStyle-HorizontalAlign="Center" />
                                                                        <asp:BoundField DataField="jcID" HeaderText="Convergence" ItemStyle-HorizontalAlign="Center" />
                                                                        <asp:BoundField DataField="gconv" HeaderText="Grid Convergence" ItemStyle-HorizontalAlign="Center" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                        <div id="divParametersGridRow" class="row">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <asp:Label ID="lblQCResParameters" runat="server" Text="Survey Parameters" BackColor="LightBlue" class="form-control"
                                                                    ForeColor="Purple" Visible="False" Font-Size="Large" />
                                                                <asp:GridView ID="gvwQCResParameters" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                                    DataKeyNames="resID"
                                                                    SelectedRowStyle-BackColor="#CCFFCC" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle">
                                                                    <Columns>
                                                                        <asp:BoundField HeaderText="resID" Visible="false" DataField="resID" />
                                                                        <asp:BoundField DataField="inc" HeaderText="Inclination°" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="14%" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField DataField="azm" HeaderText="Azimuth° (Grid)" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="14%" DataFormatString="{0: #0.00}" ControlStyle-BackColor="#999966" />
                                                                        <asp:BoundField DataField="dip" HeaderText="Dip°" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="14%" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField DataField="bTotal" HeaderText="B-Total (nT)" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="14%" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField DataField="gTotal" HeaderText="G-Total (g)" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="14%" DataFormatString="{0: #0.00000}" />
                                                                        <asp:BoundField DataField="gtf" HeaderText="GTF°" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="14%" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField DataField="mtf" HeaderText="MTF°" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="14%" DataFormatString="{0: #0.00}" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                        <div id="divQualifierGridRow" class="row">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <asp:Label ID="lblRQCQualifiers" runat="server" Text="Survey Qualifiers" BackColor="LightBlue" class="form-control"
                                                                    ForeColor="Purple" Visible="False" Font-Size="Large" />
                                                                <asp:GridView ID="gvwRQCQualifiers" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                                    DataKeyNames="resID"
                                                                    SelectedRowStyle-BackColor="#CCFFCC" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                    OnRowDataBound="gvwRQCQualifiers_RowDataBound">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="resID" HeaderText="resID" Visible="false" />
                                                                        <asp:BoundField DataField="DW" HeaderText="Dip Qualifier" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" />
                                                                        <asp:BoundField DataField="BC" HeaderText="ΔDip°" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField DataField="DY" HeaderText="B-Total Qualifier" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" />
                                                                        <asp:BoundField DataField="CD" HeaderText="ΔB-Total (nT)" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField DataField="EA" HeaderText="G-Total Qualifier" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" />
                                                                        <asp:BoundField DataField="CG" HeaderText="ΔG-Total (g)" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField DataField="AO" HeaderText="Corrected Azimuth° (Grid)" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%"
                                                                            DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField DataField="AP" HeaderText="ΔAzimuth° Error" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField DataField="DD" HeaderText="Short Collar Azimuth°" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%"
                                                                            DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField DataField="DE" HeaderText="Δ( SCAZ - LCAZ )°" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField DataField="HV" HeaderText="No Go Zone" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="9%" DataFormatString="{0: #0.00}" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                        <div id="divBHAGridRow" class="row">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <asp:Label ID="lblRQCBHA" runat="server" Text="BHA Analysis" BackColor="LightBlue" class="form-control"
                                                                    ForeColor="Purple" Visible="False" Font-Size="Large" />
                                                                <asp:GridView ID="gvwRQCBHA" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                                    DataKeyNames="resID"
                                                                    SelectedRowStyle-BackColor="#CCFFCC" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                    OnRowDataBound="gvwRQCBHA_RowDataBound">
                                                                    <Columns>
                                                                        <asp:BoundField HeaderText="resID" Visible="false" DataField="resID" />
                                                                        <asp:BoundField HeaderText="Azimuth°" DataField="azmBHA" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField HeaderText="Azimuth° BHA" DataField="azmBHA" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField HeaderText="ΔAzimuth° BHA" DataField="colGE" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField HeaderText="ΔAzimuth° Motor" DataField="colEI" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField HeaderText="ΔAzimuth° Drill Collar" DataField="colEJ" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField DataField="colEK" HeaderText="BHA Non-Mag Space" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField HeaderText="ΔAzimuth° Drill Collar (Downhole)" DataField="colEN" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                        <div id="divNMSCalcGridRow" class="row">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <asp:Label ID="lblNMSCalc" runat="server" Text="Non-Mag Space Calculations" BackColor="LightBlue" class="form-control"
                                                                    ForeColor="Purple" Visible="False" Font-Size="Large" />
                                                                <asp:GridView ID="gvwNMSCalc" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                                    DataKeyNames="resID"
                                                                    SelectedRowStyle-BackColor="#CCFFCC" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle">
                                                                    <Columns>
                                                                        <asp:BoundField HeaderText="resID" Visible="false" DataField="resID" />
                                                                        <asp:BoundField HeaderText="Std. DC Interference" DataField="D34" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField HeaderText="Std. Mtr. Interference" DataField="D35" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField HeaderText="Total BHA Interference" DataField="D37" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField HeaderText="ΔLCAZ Error° (BHA)" DataField="D38" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField HeaderText="Bit-To-Sensor Distance" DataField="D39" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                        <div id="divMinMaxGridRow" class="row">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <asp:Label ID="lblRQCMinMax" runat="server" Text="Min-Max" BackColor="LightBlue" class="form-control"
                                                                    ForeColor="Purple" Visible="False" Font-Size="Large" />
                                                                <asp:GridView ID="gvwRQCMinMax" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                                    DataKeyNames="resID"
                                                                    SelectedRowStyle-BackColor="#CCFFCC" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle">
                                                                    <Columns>
                                                                        <asp:BoundField HeaderText="resID" Visible="false" DataField="resID" />
                                                                        <asp:BoundField HeaderText="Min. Dip°" DataField="EU" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                                        <asp:BoundField HeaderText="Max. Dip°" DataField="EV" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                                        <asp:BoundField HeaderText="Dip Spread" DataField="EW" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                                        <asp:BoundField HeaderText="Min. B-Total" DataField="EX" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                                        <asp:BoundField HeaderText="Max. B-Total" DataField="EY" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                                        <asp:BoundField HeaderText="B-Total Spread" DataField="EZ" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                                        <asp:BoundField HeaderText="Min. G-Total" DataField="FA" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                                        <asp:BoundField HeaderText="Max. G-Total" DataField="FB" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                                        <asp:BoundField HeaderText="G-Total Spread" DataField="FC" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <asp:Label ID="lblPlacement" runat="server" Text="Placement Values" CssClass="form-control" BackColor="LightBlue" ForeColor="Purple"
                                                                Visible="False" Font-Size="Large" />
                                                            <asp:GridView ID="gvwPlacement" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                                DataKeyNames="srvID" SelectedRowStyle-BackColor="#CCFFCC" HeaderStyle-CssClass="header" ShowHeaderWhenEmpty="true"
                                                                RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle" EmptyDataText="No Placement values found for selected depth">
                                                                <Columns>
                                                                    <asp:BoundField HeaderText="srvID" Visible="false" DataField="srvID" />
                                                                    <asp:BoundField HeaderText="TVD" DataField="tvd" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                    <asp:BoundField HeaderText="North" DataField="north" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                    <asp:BoundField HeaderText="East" DataField="east" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #.00}" />
                                                                    <asp:BoundField HeaderText="Vertical Section" DataField="vs" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                    <asp:BoundField HeaderText="Subsea Elevation" DataField="subsea" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.0000}" />
                                                                    <asp:BoundField HeaderText="Dogleg" DataField="dls" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.0000}" />
                                                                    <asp:BoundField HeaderText="Northing" DataField="northing" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                    <asp:BoundField HeaderText="Easting" DataField="easting" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                        <div id="divTISRow" class="row">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <asp:Label ID="lblTieInSurvey" runat="server" Text="Tie-In Survey" BackColor="LightBlue" class="form-control"
                                                                    ForeColor="Purple" Visible="False" Font-Size="Large" />
                                                                <asp:GridView ID="gvwTieInSurvey" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                                    DataKeyNames="tisID" SelectedRowStyle-BackColor="#CCFFCC" HeaderStyle-CssClass="header" ShowHeaderWhenEmpty="true"
                                                                    RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle" EmptyDataText="No Tie-In Surveys found for selected depth">
                                                                    <Columns>
                                                                        <asp:BoundField HeaderText="tisID" Visible="false" DataField="tisID" />
                                                                        <asp:BoundField HeaderText="" DataField="title" />
                                                                        <asp:BoundField HeaderText="M. Depth" DataField="md" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField HeaderText="Inclination" DataField="inc" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField HeaderText="Azimuth" DataField="azm" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField HeaderText="TVD" DataField="tvd" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField HeaderText="North" DataField="north" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.0000}" />
                                                                        <asp:BoundField HeaderText="East" DataField="east" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.0000}" />
                                                                        <asp:BoundField HeaderText="VS" DataField="vs" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                        <asp:BoundField HeaderText="Subsea" DataField="subsea" ItemStyle-HorizontalAlign="Center" DataFormatString="{0: #0.00}" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                        <div id="divWellDepthInfo" class="row">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div runat="server" id="enWDInfo" visible="false">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                    <h4><i runat="server" id="iWDInfo"><span runat="server" id="spnWDInfo"></span></i></h4>
                                                                    <asp:Label runat="server" ID="lblWDInfo" Text="" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="divRunDepthInfo" class="row">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div runat="server" id="enRDInfo" visible="false">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                    <h4><i runat="server" id="iRDInfo"><span runat="server" id="spnRDInfo"></span></i></h4>
                                                                    <asp:Label runat="server" ID="lblRDInfo" Text="" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="divClearResBtnRow" class="row">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="input-group" style="width: 100%;">
                                                                    <asp:LinkButton ID="btnClearResDetail" runat="server" Text="Clear Detail" CssClass="btn btn-warning text-center" OnClick="btnClearResDetail_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                    <asp:LinkButton ID="btnDetailCancel" runat="server" CssClass="btn btn-danger text-center" OnClick="btnDetailCancel_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btn_FileUpload" />
        <asp:PostBackTrigger ControlID="btnProcessReadyData" />
        <asp:PostBackTrigger ControlID="gvwRQCResults" />
        <asp:PostBackTrigger ControlID="btnClearResDetail" />
    </Triggers>
</asp:UpdatePanel>
