﻿using log4net;
using System;
using System.Web;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using CHRT = SmartsVer1.Helpers.ChartHelpers.Gauges;
using CHRTRes = SmartsVer1.Helpers.ChartHelpers.ChartResources;
using Color = System.Drawing.Color;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using OPR = SmartsVer1.Helpers.QCHelpers.Opr;
using QC = SmartsVer1.QCHelpers.sdRawDataQC;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using SRV = SmartsVer1.Helpers.DemogHelpers.ServicesConfig;

namespace SmartsVer1.Control.Services
{
    public partial class ctrlFECDataReview_2A : System.Web.UI.UserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ctrlFECDataReview_2A));
        String LoginName, username, domain, GetClientDBString, wellName;
        Int32 ClientID = -99, jobCount = 0, wellCount = 0, runCount = 0, _24Count = 0, _14Count = 0;
        const Int32 serviceGroupID = 2;
        const Int32 ClientType = 4;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientType);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = 0;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");
                if (Page.IsPostBack)
                {
                    this.createAssetCharts(null, null);
                }
                else
                {
                    this.createAssetCharts(null, null);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }

        }

        protected void btnDetailCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnClear_Click(null, null);
                this.divResultsOuter.Visible = false;
                this.btnMetaInfoReset_Click(null, null);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.gvwResSQC.DataBind();
                this.gvwResSQC.Visible = false;
                this.gvwRefVals.DataBind();
                this.gvwRefVals.Visible = false;
                this.gvwQCResParameters.DataBind();
                this.gvwQCResParameters.Visible = false;
                this.gvwRQCQualifiers.DataBind();
                this.gvwRQCQualifiers.Visible = false;
                this.gvwRQCBHA.DataBind();
                this.gvwRQCBHA.Visible = false;
                this.gvwNMSCalc.DataBind();
                this.gvwNMSCalc.Visible = false;
                this.gvwRQCMinMax.DataBind();
                this.gvwRQCMinMax.Visible = false;
                this.gvwPlacement.DataSource = null;
                this.gvwPlacement.DataBind();
                this.gvwPlacement.Visible = false;
                this.gvwTieInSurvey.DataSource = null;
                this.gvwTieInSurvey.DataBind();
                this.gvwTieInSurvey.Visible = false;
                this.lblDepth.Visible = false;
                this.lblQCResSQC.Visible = false;
                this.lblQCResMetaData.Visible = false;
                this.lblQCResParameters.Visible = false;
                this.lblRQCQualifiers.Visible = false;
                this.lblRQCBHA.Visible = false;
                this.lblNMSCalc.Visible = false;
                this.lblRQCMinMax.Visible = false;
                this.lblTieInSurvey.Visible = false;
                this.lblPlacement.Visible = false;
                this.btnClear.Enabled = false;
                this.btnClear.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwGlobalOptr_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientType);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = e.NewPageIndex;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
                this.btnMetaInfoReset.CssClass = "btn btn-warning text-center";
                this.btnMetaInfoReset.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwGlobalOptr_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow dRow = this.gvwGlobalOptr.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.ancHead.InnerHtml = "<strong> " + oName + "</strong>";
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                System.Data.DataTable padTable = AST.getClientWellPadTableForOperator(oprID, ClientID);
                this.gvwWellPads.DataSource = padTable;
                this.gvwWellPads.PageIndex = 0;
                this.gvwWellPads.SelectedIndex = -1;
                this.gvwWellPads.DataBind();
                this.divOpr.Visible = false;
                this.divPad.Visible = true;
                this.divWell.Visible = false;
                this.divSec.Visible = false;
                this.divRun.Visible = false;
                this.btnMetaInfoReset.Enabled = true;
                this.btnMetaInfoReset.CssClass = "btn btn-warning text-center";
                this.liStep1.Attributes["class"] = "stepper-done";
                this.liStep2.Attributes["class"] = "stepper-todo";
                this.liStep3.Attributes["class"] = "stepper-todo";
                this.liStep4.Attributes["class"] = "stepper-todo";
                this.liStep5.Attributes["class"] = "stepper-todo";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnClearResDetail_Click(object sender, EventArgs e)
        {
            try
            {
                this.gvwResSQC.DataBind();
                this.gvwResSQC.Visible = false;
                this.gvwRefVals.DataBind();
                this.gvwRefVals.Visible = false;
                this.gvwQCResParameters.DataBind();
                this.gvwQCResParameters.Visible = false;
                this.gvwRQCQualifiers.DataBind();
                this.gvwRQCQualifiers.Visible = false;
                this.gvwRQCBHA.DataBind();
                this.gvwRQCBHA.Visible = false;
                this.gvwNMSCalc.DataBind();
                this.gvwNMSCalc.Visible = false;
                this.gvwRQCMinMax.DataBind();
                this.gvwRQCMinMax.Visible = false;
                this.lblDepth.Visible = false;
                this.lblQCResSQC.Visible = false;
                this.lblQCResMetaData.Visible = false;
                this.lblQCResParameters.Visible = false;
                this.lblRQCQualifiers.Visible = false;
                this.lblRQCBHA.Visible = false;
                this.lblNMSCalc.Visible = false;
                this.lblRQCMinMax.Visible = false;

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void createChartQCTotal()
        {
            String result = String.Empty;
            String echarts = String.Empty;
            String echartsAzim = String.Empty;
            String echarts3D = String.Empty;
            String echartsPolarIncAzim = String.Empty;
            String echartsPolarToolFace = String.Empty;
            String echartsPerfQS1 = String.Empty;
            String echartsPerfQS2 = String.Empty;

            String wellName = Convert.ToString(HttpContext.Current.Session["wellName"]);
            decimal maxTVD = 0;

            System.Data.DataTable dsChartData = new System.Data.DataTable();
            System.Data.DataTable dsChartData_3DPlacement = new System.Data.DataTable();
            System.Data.DataTable dsChartDataRawBased = new System.Data.DataTable();
            System.Data.DataTable dsChartDataBHABased = new System.Data.DataTable();
            System.Data.DataTable dsChartDataSCBased = new System.Data.DataTable();
            System.Data.DataTable dsChartDataAzim = new System.Data.DataTable();
            System.Data.DataTable dsChartDataPolarIncAzimToolFace = new System.Data.DataTable();
            System.Data.DataTable dsChartDataPerfQS = new System.Data.DataTable();
            System.Data.DataTable dsChartDataPerfQS2 = new System.Data.DataTable();
            System.Data.DataTable getScalerEastingNorthing = new System.Data.DataTable();

            Int32 wsD = Convert.ToInt32(this.gvwSectionList.SelectedValue);
            if (wsD.Equals(-2))
            {
                Int32 wD = Convert.ToInt32(this.gvwWellList.SelectedValue);
                dsChartData = CHRTRes.getDataTableforQCWellGraph(wD, GetClientDBString);
                dsChartData_3DPlacement = CHRTRes.getDataTableforQCWellGraph_3DPlacement(wD, GetClientDBString);
                dsChartDataRawBased = CHRTRes.getDataTableforQCWellGraphRawBased(wD, GetClientDBString);
                dsChartDataBHABased = CHRTRes.getDataTableforQCWellGraphBHABased(wD, GetClientDBString);
                dsChartDataSCBased = CHRTRes.getDataTableforQCWellGraphSCBased(wD, GetClientDBString);

                dsChartDataAzim = QC.getDataTableforQCWellGraphAZIM(wD, GetClientDBString);
                dsChartDataPolarIncAzimToolFace = CHRTRes.getDataTableforQCWellGraphIncAZIMToolFace(wD, GetClientDBString);
                maxTVD = CHRTRes.getMaxTVDforWell(wD, GetClientDBString);
                dsChartDataPerfQS = CHRTRes.getDataTableforPerfQualityScore(wD, GetClientDBString);
                dsChartDataPerfQS2 = CHRTRes.getDataTableforPerfQualityScore2(wD, GetClientDBString);
                getScalerEastingNorthing = CHRTRes.getScallingFactorChartInfo_WPlan(wD, GetClientDBString);
            }
            else
            {                
                Int32 wellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 srvID = AST.getWellRegisteredServiceID(wellID, GetClientDBString);
                Int32 RunID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                dsChartData = CHRTRes.getDataTableforQCGraph_1(wellID, wsD, srvID, RunID, GetClientDBString);
                dsChartData_3DPlacement = CHRTRes.getDataTableforQCWellGraph_3DPlacement(wellID, GetClientDBString);
                dsChartDataRawBased = CHRTRes.getDataTableforQCGraph_1_RawBased(wellID, wsD, srvID, RunID, GetClientDBString);
                dsChartDataBHABased = CHRTRes.getDataTableforQCGraph_1_BHABased(wellID, wsD, srvID, RunID, GetClientDBString);
                dsChartDataSCBased = CHRTRes.getDataTableforQCGraph_1_SCBased(wellID, wsD, srvID, RunID, GetClientDBString);

                dsChartDataAzim = CHRTRes.getDataTableforQCWelGraphAZIM_1(wellID, wsD, srvID, RunID, GetClientDBString);
                dsChartDataPolarIncAzimToolFace = CHRTRes.getDataTableforQCWelGraphIncAZIMToolFace_1(wellID, wsD, srvID, RunID, GetClientDBString);
                maxTVD = CHRTRes.getMaxTVDforWell(wellID, GetClientDBString);
                dsChartDataPerfQS = CHRTRes.getDataTableforPerfQualityScore(wellID, GetClientDBString);
                dsChartDataPerfQS2 = CHRTRes.getDataTableforPerfQualityScore2(wellID, GetClientDBString);
                getScalerEastingNorthing = CHRTRes.getScallingFactorChartInfo_WPlan(wellID, GetClientDBString);
            }

            try
            {

                echarts = CHRT.getChartsQCResultsECHRT01("BodyContent_ctrlFECDataReview_2A_divECharts", dsChartData);
                this.ltrECHarts.Text = echarts;

                echartsAzim = CHRT.getChartsQCResultsECHRT_AZM("BodyContent_ctrlFECDataReview_2A_divEChartsAzim", dsChartDataAzim);
                this.ltrECHartsAzim.Text = echartsAzim;

                echarts3D = CHRT.getChartsQCResultsECHRT_3D("BodyContent_ctrlFECDataReview_2A_divECharts3D", wellName, dsChartData_3DPlacement, dsChartDataRawBased, dsChartDataBHABased, dsChartDataSCBased, getScalerEastingNorthing, maxTVD);
                this.ltrECHarts3D.Text = echarts3D;

                echartsPolarIncAzim = CHRT.getChartsQCResultsECHRTPolar_INCAZM("BodyContent_ctrlFECDataReview_2A_divPolar1", dsChartDataPolarIncAzimToolFace);
                this.ltrECHartsPolar1.Text = echartsPolarIncAzim;

                echartsPolarToolFace = CHRT.getChartsQCResultsECHRTPolar_ToolFaces("BodyContent_ctrlFECDataReview_2A_divPolar2", dsChartDataPolarIncAzimToolFace);
                this.ltrECHartsPolar2.Text = echartsPolarToolFace;

                echartsPerfQS1 = CHRT.getChartsPerformanceQS01("BodyContent_ctrlFECDataReview_2A_divPerfHeatMapQS1", dsChartDataPerfQS);
                this.ltrECHartsPerfHeatMapQS1.Text = echartsPerfQS1;

                echartsPerfQS2 = CHRT.getChartsPerformanceQS02("BodyContent_ctrlFECDataReview_2A_divPerfQS2", dsChartDataPerfQS2);
                this.ltrECHartsPerfQS2.Text = echartsPerfQS2;

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
            finally
            {
                dsChartData.Dispose();
                result = String.Empty;
            }
        }

        protected void createChartQCTotalwithPlan()
        {
            String result = String.Empty;
            String echarts = String.Empty;
            String echartsAzim = String.Empty;
            String echarts3D = String.Empty;
            String echartsPerfQS1 = String.Empty;
            String echartsPerfQS2 = String.Empty;

            GridViewRow dRow = this.gvwWellList.SelectedRow;
            String wellName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
            //String wellName = Convert.ToString(this.Session["wellName"]);
            Decimal maxTVD = 0;
            System.Data.DataTable dsChartData = new System.Data.DataTable();
            System.Data.DataTable dsChartData_3DPlacement = new System.Data.DataTable();
            System.Data.DataTable dsChartDataRawBased = new System.Data.DataTable();
            System.Data.DataTable dsChartDataBHABased = new System.Data.DataTable();
            System.Data.DataTable dsChartDataSCBased = new System.Data.DataTable();
            System.Data.DataTable dsChartDatawithPlan = new System.Data.DataTable();
            System.Data.DataTable dsChartDataAzim = new System.Data.DataTable();
            System.Data.DataTable dsChartDataPerfQS = new System.Data.DataTable();
            System.Data.DataTable dsChartDataPerfQS2 = new System.Data.DataTable();
            System.Data.DataTable getScalerEastingNorthing = new System.Data.DataTable();

            Int32 wsD = Convert.ToInt32(this.gvwSectionList.SelectedValue);
            if (wsD.Equals(-2))
            {
                Int32 wD = Convert.ToInt32(this.gvwWellList.SelectedValue);
                dsChartData = CHRTRes.getDataTableforQCWellGraph(wD, GetClientDBString);
                dsChartData_3DPlacement = CHRTRes.getDataTableforQCWellGraph_3DPlacement(wD, GetClientDBString);
                dsChartDataRawBased = CHRTRes.getDataTableforQCWellGraphRawBased(wD, GetClientDBString);
                dsChartDataBHABased = CHRTRes.getDataTableforQCWellGraphBHABased(wD, GetClientDBString);
                dsChartDataSCBased = CHRTRes.getDataTableforQCWellGraphSCBased(wD, GetClientDBString);

                dsChartDatawithPlan = CHRTRes.getDataTableforQCWellGraphWithPlan(wD, GetClientDBString);
                dsChartDataAzim = QC.getDataTableforQCWellGraphAZIM(wD, GetClientDBString);
                maxTVD = CHRTRes.getMaxTVDforWell(wD, GetClientDBString);
                dsChartDataPerfQS = CHRTRes.getDataTableforPerfQualityScore(wD, GetClientDBString);
                dsChartDataPerfQS2 = CHRTRes.getDataTableforPerfQualityScore2(wD, GetClientDBString);
                getScalerEastingNorthing = CHRTRes.getScallingFactorChartInfo_WPlan(wD, GetClientDBString);
            }
            else
            {
                Int32 wellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 srvID = AST.getWellRegisteredServiceID(wellID, GetClientDBString);
                Int32 RunID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                dsChartData = CHRTRes.getDataTableforQCGraph_1(wellID, wsD, srvID, RunID, GetClientDBString);
                dsChartData_3DPlacement = CHRTRes.getDataTableforQCWellGraph_3DPlacement(wellID, GetClientDBString);
                dsChartDataRawBased = CHRTRes.getDataTableforQCGraph_1_RawBased(wellID, wsD, srvID, RunID, GetClientDBString);
                dsChartDataBHABased = CHRTRes.getDataTableforQCGraph_1_BHABased(wellID, wsD, srvID, RunID, GetClientDBString);
                dsChartDataSCBased = CHRTRes.getDataTableforQCGraph_1_SCBased(wellID, wsD, srvID, RunID, GetClientDBString);

                Int32 wD = Convert.ToInt32(this.gvwWellList.SelectedValue);
                dsChartDatawithPlan = CHRTRes.getDataTableforQCWellGraphWithPlan(wD, GetClientDBString);
                dsChartDataAzim = CHRTRes.getDataTableforQCWelGraphAZIM_1(wellID, wsD, srvID, RunID, GetClientDBString);
                maxTVD = CHRTRes.getMaxTVDforWell(wD, GetClientDBString);
                dsChartDataPerfQS = CHRTRes.getDataTableforPerfQualityScore(wellID, GetClientDBString);
                dsChartDataPerfQS2 = CHRTRes.getDataTableforPerfQualityScore2(wellID, GetClientDBString);
                getScalerEastingNorthing = CHRTRes.getScallingFactorChartInfo_WPlan(wellID, GetClientDBString);
            }

            try
            {

                echarts = CHRT.getChartsQCResultsECHRTWithPlan("BodyContent_ctrlFECDataReview_2A_divEChartsWithPlan", dsChartData, dsChartDatawithPlan);
                this.ltrECHartswithPlan.Text = echarts;

                echartsAzim = CHRT.getChartsQCResultsECHRT_AZMWithPlan("BodyContent_ctrlFECDataReview_2A_divEChartsAzimWithPlan", dsChartDataAzim, dsChartDatawithPlan);
                this.ltrECHartsAzimwithPlan.Text = echartsAzim;

                echarts3D = CHRT.getChartsQCResultsECHRT_3DWithPlan("BodyContent_ctrlFECDataReview_2A_divECharts3DWithPlan", wellName, dsChartData_3DPlacement, dsChartDataRawBased, dsChartDataBHABased, dsChartDataSCBased, dsChartDatawithPlan, getScalerEastingNorthing, maxTVD);
                this.ltrECHarts3DwithPlan.Text = echarts3D;

                echartsPerfQS1 = CHRT.getChartsPerformanceQS01("BodyContent_ctrlFECDataReview_2A_divPerfHeatMapQS1wPlan", dsChartDataPerfQS);
                this.ltrECHartsPerfHeatMapQS1wPan.Text = echartsPerfQS1;

                echartsPerfQS2 = CHRT.getChartsPerformanceQS02("BodyContent_ctrlFECDataReview_2A_divPerfQS2wPlan", dsChartDataPerfQS2);
                this.ltrECHartsPerfQS2wPlan.Text = echartsPerfQS2;


            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
            finally
            {
                dsChartData.Dispose();
                dsChartData_3DPlacement.Dispose();
                result = String.Empty;
            }
        }

        protected void btnChart_Click(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.createChartQCTotal(); // Creating Graph
            }
            else
            {
                this.createChartQCTotal(); // Creating Graph
            }
        }

        protected void resetPage()
        {
        }

        protected void gvwRQCQualifiers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = ((DataRowView)e.Row.DataItem).Row;
                    Int32 dC = Convert.ToInt32(dr["DWClr"]);
                    Int32 bC = Convert.ToInt32(dr["DYClr"]);
                    Int32 gC = Convert.ToInt32(dr["EAClr"]);
                    Int32 hC = Convert.ToInt32(dr["HVClr"]);

                    if (dC.Equals(1))
                    {
                        e.Row.Cells[1].BackColor = Color.LightGreen;
                    }
                    else
                    {
                        e.Row.Cells[1].BackColor = Color.Red;
                    }

                    if (bC.Equals(1))
                    {
                        e.Row.Cells[3].BackColor = Color.LightGreen;
                    }
                    else
                    {
                        e.Row.Cells[3].BackColor = Color.Red;
                    }

                    if (gC.Equals(1))
                    {
                        e.Row.Cells[5].BackColor = Color.LightGreen;
                    }
                    else
                    {
                        e.Row.Cells[5].BackColor = Color.Red;
                    }

                    if (hC.Equals(1))
                    {
                        e.Row.Cells[11].BackColor = Color.LightGreen;
                    }
                    else
                    {
                        e.Row.Cells[11].BackColor = Color.Red;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRQCBHA_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = ((DataRowView)e.Row.DataItem).Row;
                    Int32 ek = Convert.ToInt32(dr["EKClr"]);

                    if (ek.Equals(2) || ek.Equals(4) || ek.Equals(7))
                    {
                        e.Row.Cells[4].BackColor = Color.LightGreen;
                    }
                    else
                    {
                        e.Row.Cells[4].BackColor = Color.Red;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void createAssetCharts(object sender, EventArgs e)
        {
            try
            {
                List<Int32> summaryDetails = CHRT.getClientDataReviewSummary(ClientID, GetClientDBString);
                jobCount = summaryDetails[0];
                wellCount = summaryDetails[1];
                runCount = summaryDetails[2];
                _24Count = summaryDetails[3];
                _14Count = summaryDetails[4];

                this.divBTGuageJob.InnerHtml = "<h3>" + jobCount.ToString() + "</h3><p>In Process Well(s)/Lateral(s)</p>";
                this.divBTGuageWell.InnerHtml = "<h3>" + wellCount.ToString() + "</h3><p>Total Wells</p>";
                this.divBTGuageTotalRun.InnerHtml = "<h3>" + runCount.ToString() + "</h3><p>Total Run</p>";
                this.divBTGuageAud_12.InnerHtml = "<h3>" + _24Count.ToString() + "</h3><p>Audit/QC 24hr</p>";
                this.divBTGuageAud_14Days.InnerHtml = "<h3>" + _14Count.ToString() + "</h3><p>Audit/QC 14days</p>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnMetaInfoReset_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientType);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = 0;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
                this.gvwWellPads.DataSource = null;
                this.gvwWellPads.DataBind();
                this.gvwWellList.DataSource = null;
                this.gvwWellList.DataBind();
                this.gvwSectionList.DataSource = null;
                this.gvwSectionList.DataBind();
                this.gvwRunList.DataSource = null;
                this.gvwRunList.DataBind();
                this.gvwRQCWellResults.DataSource = null;
                this.gvwRQCWellResults.DataBind();
                this.gvwRQCRunResults.DataSource = null;
                this.gvwRQCRunResults.DataBind();
                this.gvwMaxRunTieIn.DataSource = null;
                this.gvwMaxRunTieIn.DataBind();
                this.gvwResSQC.DataSource = null;
                this.gvwResSQC.DataBind();
                this.gvwRefVals.DataSource = null;
                this.gvwRefVals.DataBind();
                this.gvwQCResParameters.DataSource = null;
                this.gvwQCResParameters.DataBind();
                this.gvwRQCQualifiers.DataSource = null;
                this.gvwRQCQualifiers.DataBind();
                this.gvwRQCBHA.DataSource = null;
                this.gvwRQCBHA.DataBind();
                this.gvwRQCMinMax.DataSource = null;
                this.gvwRQCMinMax.DataBind();
                this.gvwTieInSurvey.DataSource = null;
                this.gvwTieInSurvey.DataBind();
                this.divOpr.Visible = true;
                this.divPad.Visible = false;
                this.divWell.Visible = false;
                this.divSec.Visible = false;
                this.divRun.Visible = false;
                this.liStep1.Attributes["class"] = "stepper-todo";
                this.liStep2.Attributes["class"] = "stepper-todo";
                this.liStep3.Attributes["class"] = "stepper-todo";
                this.liStep4.Attributes["class"] = "stepper-todo";
                this.liStep5.Attributes["class"] = "stepper-todo";
                this.ancHead.InnerHtml = "Operator Company";
                this.btnMetaInfoReset.Enabled = false;
                this.btnMetaInfoReset.CssClass = "btn btn-default text-center";
                this.btnClear.Enabled = false;
                this.btnClear.CssClass = "btn btn-default text-center";

                this.btnClear_Click(null, null);
                this.divResultsOuter.Visible = false;
                //this.btnMetaInfoReset_Click(null, null);

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
        
        protected void gvwWellPads_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                System.Data.DataTable padTable = AST.getClientWellPadTableForOperator(oprID, ClientID);
                this.gvwWellPads.DataSource = padTable;
                this.gvwWellPads.PageIndex = e.NewPageIndex;
                this.gvwWellPads.SelectedIndex = -1;
                this.gvwWellPads.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellPads_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow oRow = this.gvwGlobalOptr.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                GridViewRow pRow = this.gvwWellPads.SelectedRow;
                String pName = Convert.ToString(Server.HtmlDecode(pRow.Cells[2].Text));
                this.ancHead.InnerHtml = "<strong> " + oName + " > " + pName + "</strong>";
                Int32 pdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                System.Data.DataTable pdTable = AST.getWellForWellPadTable(pdID, GetClientDBString);
                this.gvwWellList.DataSource = pdTable;
                this.gvwWellList.PageIndex = 0;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();
                this.divOpr.Visible = false;
                this.divPad.Visible = false;
                this.divWell.Visible = true;
                this.divSec.Visible = false;
                this.divRun.Visible = false;
                this.liStep1.Attributes["class"] = "stepper-done";
                this.liStep2.Attributes["class"] = "stepper-done";
                this.liStep3.Attributes["class"] = "stepper-todo";
                this.liStep4.Attributes["class"] = "stepper-todo";
                this.liStep5.Attributes["class"] = "stepper-todo";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                System.Data.DataTable wsTable = OPR.WellSectionNameToWellTable(wlID, GetClientDBString);
                this.gvwSectionList.DataSource = wsTable;
                this.gvwSectionList.PageIndex = 0;
                this.gvwSectionList.SelectedIndex = -1;
                this.gvwSectionList.DataBind();
                this.gvwRunList.PageIndex = 0;
                this.gvwRunList.SelectedIndex = -1;
                this.gvwRunList.DataBind();
                this.divOpr.Visible = false;
                this.divPad.Visible = false;
                this.divWell.Visible = false;
                this.divSec.Visible = true;
                this.divRun.Visible = false;
                this.liStep1.Attributes["class"] = "stepper-done";
                this.liStep2.Attributes["class"] = "stepper-done";
                this.liStep3.Attributes["class"] = "stepper-done";
                this.liStep4.Attributes["class"] = "stepper-todo";
                this.liStep5.Attributes["class"] = "stepper-todo";
                GridViewRow oRow = this.gvwGlobalOptr.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                GridViewRow pRow = this.gvwWellPads.SelectedRow;
                String pName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                GridViewRow wRow = this.gvwWellList.SelectedRow;
                String wName = Convert.ToString(Server.HtmlDecode(wRow.Cells[2].Text));
                // Storing Well Name for chart label use
                HttpContext.Current.Session["wellName"] = wName;
                this.ancHead.InnerHtml = "<strong> " + oName + " > " + pName + " > " + wName + "</strong>";                
                this.btnMetaInfoReset.Enabled = true;
                this.btnMetaInfoReset.CssClass = "btn btn-warning text-center";
                this.gvwRQCWellResults.DataBind();
                this.gvwRQCWellResults.SelectedIndex = -1;
                this.gvwRQCRunResults.DataBind();
                this.gvwRQCRunResults.SelectedIndex = -1;
                this.btnDetailCancel.Visible = true;                    
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 pdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                System.Data.DataTable pdTable = AST.getWellForWellPadTable(pdID, GetClientDBString);
                this.gvwWellList.DataSource = pdTable;
                this.gvwWellList.PageIndex = e.NewPageIndex;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();               
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = ((DataRowView)e.Row.DataItem).Row;
                    Button selBtn = e.Row.Cells[0].Controls[0] as Button;
                    if (dr["wstID"].Equals("In Process"))
                    {
                        String stC = Convert.ToString(dr["stClr"]);
                        e.Row.Cells[7].BackColor = Color.FromName(stC.ToString());
                    }
                    else
                    {
                        e.Row.Cells[7].BackColor = Color.LightBlue;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSectionList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 padID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                Int32 welID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 wSecID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                String wellName = Convert.ToString(HttpContext.Current.Session["wellName"]);
                if (wSecID.Equals(-2)) //Complete well selected to show results
                {
                    //Int32 chkD = OPR.findData(wlID, wsID, GetClientDBString);
                    //if (chkD < 1)
                    //{
                    //    this.divNRes.Visible = true;
                    //    this.charts.Visible = false;
                    //    this.chartsWplan.Visible = false;
                    //    this.divResView.Visible = false;
                    //    this.divResultsOuter.Visible = true;
                    //    this.divNRes.Visible = true;
                    //}
                    //else
                    //{
                        System.Data.DataTable resTable = QC.getMetaResultsForWell(oprID, ClientID, padID, welID, GetClientDBString);
                        this.gvwRQCWellResults.DataSource = resTable;
                        this.gvwRQCWellResults.DataBind();
                        this.gvwRQCWellResults.SelectedIndex = -1;
                        this.gvwRQCWellResults.Visible = true;

                        Decimal maxDepthTI = -99.00M;
                        maxDepthTI = QC.getMaxWellDepthForTieInSurveys(oprID, ClientID, padID, welID, GetClientDBString);
                        System.Data.DataTable wellPlansTable = QC.getWellTieInSurveysTable(maxDepthTI, welID, GetClientDBString);
                        this.gvwMaxRunTieIn.DataSource = wellPlansTable;
                        this.gvwMaxRunTieIn.DataBind();
                        this.gvwRQCRunResults.DataBind();
                        this.gvwRQCRunResults.SelectedIndex = -1;
                        this.btnChart_Click(this, EventArgs.Empty);
                        this.divResView.Visible = true;
                        GridViewRow oRow = this.gvwGlobalOptr.SelectedRow;
                        String oName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                        GridViewRow pRow = this.gvwWellPads.SelectedRow;
                        String pName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                        GridViewRow wRow = this.gvwWellList.SelectedRow;
                        String wName = Convert.ToString(Server.HtmlDecode(wRow.Cells[2].Text));
                        GridViewRow sRow = this.gvwSectionList.SelectedRow;
                        String wsName = Convert.ToString(Server.HtmlDecode(sRow.Cells[2].Text));
                        this.ancHead.InnerHtml = "<strong> " + oName + " > " + pName + " > " + wName + " > " + wsName + "</strong>";                         
                                                                       
                        logger.Info("User: " + username + " Data_Review: Display Charts for Well/Later: " + wellName + " Well Section: " + wsName + " Selected");

                        this.btnMetaInfoReset.Enabled = true;
                        this.btnMetaInfoReset.CssClass = "btn btn-warning text-center";
                        this.divResultsOuter.Visible = true;
                        this.divOpr.Visible = false;
                        this.divPad.Visible = false;
                        this.divWell.Visible = false;
                        this.divSec.Visible = false;
                        this.divRun.Visible = false;
                        this.liStep1.Attributes["class"] = "stepper-done";
                        this.liStep2.Attributes["class"] = "stepper-done";
                        this.liStep3.Attributes["class"] = "stepper-done";
                        this.liStep4.Attributes["class"] = "stepper-done";
                        this.liStep5.Attributes["class"] = "stepper-todo";
                    //}
                }
                else
                {
                    System.Data.DataTable runList = OPR.getRunForSelectedWellSectionTable(welID, wSecID, GetClientDBString);
                    this.gvwRunList.DataSource = runList;
                    this.gvwRunList.PageIndex = 0;
                    this.gvwRunList.SelectedIndex = -1;
                    this.gvwRunList.DataBind();
                    this.gvwRQCWellResults.DataBind();
                    this.gvwRQCWellResults.SelectedIndex = -1;
                    this.gvwRQCRunResults.DataBind();
                    this.gvwRQCRunResults.SelectedIndex = -1;

                        GridViewRow oRow = this.gvwGlobalOptr.SelectedRow;
                        String oName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                        GridViewRow pRow = this.gvwWellPads.SelectedRow;
                        String pName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                        GridViewRow wRow = this.gvwWellList.SelectedRow;
                        String wName = Convert.ToString(Server.HtmlDecode(wRow.Cells[2].Text));
                        GridViewRow sRow = this.gvwSectionList.SelectedRow;
                        String wsName = Convert.ToString(Server.HtmlDecode(sRow.Cells[2].Text));

                        this.ancHead.InnerHtml = "<strong> " + oName + " > " + pName + " > " + wName + " > " + wsName + "</strong>";                         

                    logger.Info("User: " + username + " Data_Review: Display Charts for Well/Later: " + wellName + " Well Section: " + wsName + " Selected");

                    this.btnMetaInfoReset.Enabled = true;
                    this.btnMetaInfoReset.CssClass = "btn btn-warning text-center";
                    this.divOpr.Visible = false;
                    this.divPad.Visible = false;
                    this.divWell.Visible = false;
                    this.divSec.Visible = false;
                    this.divRun.Visible = true;
                    this.liStep1.Attributes["class"] = "stepper-done";
                    this.liStep2.Attributes["class"] = "stepper-done";
                    this.liStep3.Attributes["class"] = "stepper-done";
                    this.liStep4.Attributes["class"] = "stepper-done";
                    this.liStep5.Attributes["class"] = "stepper-todo";
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSectionList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                System.Data.DataTable wsTable = OPR.WellSectionNameToWellTable(wlID, GetClientDBString);
                this.gvwSectionList.DataSource = wsTable;
                this.gvwSectionList.PageIndex = e.NewPageIndex;
                this.gvwSectionList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRunList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 padID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 wsID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                Int32 rID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                String wellName = Convert.ToString(HttpContext.Current.Session["wellName"]);

                Int32 result = CHRT.checkRunData(rID, GetClientDBString);
                if (result < 1)
                {
                    this.divNRes.Visible = true;
                    this.charts.Visible = false;
                    this.chartsWplan.Visible = false;
                    this.divResultsOuter.Visible = true;
                    this.divNRes.Visible = true;
                }
                else
                {
                    System.Data.DataTable resTable = QC.getMetaResults(oprID, padID, wlID, wsID, rID, GetClientDBString);
                    this.gvwRQCRunResults.DataSource = resTable;
                    this.gvwRQCRunResults.DataBind();
                    this.gvwRQCRunResults.Visible = true;
                    Decimal maxDepthTI = -99.00M;
                    maxDepthTI = QC.getMaxDepthForTieInSurveys(rID, wsID, wlID, padID, ClientID, oprID, GetClientDBString);
                    System.Data.DataTable plansTable = QC.getTieInSurveysTable(maxDepthTI, rID, wsID, wlID, GetClientDBString);
                    this.gvwMaxRunTieIn.DataSource = plansTable;
                    this.gvwMaxRunTieIn.DataBind();
                    this.btnChart_Click(null, null); // Generate the Graphs
                    GridViewRow oRow = this.gvwGlobalOptr.SelectedRow;
                    String oName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                    GridViewRow pRow = this.gvwWellPads.SelectedRow;
                    String pName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                    GridViewRow wRow = this.gvwWellList.SelectedRow;
                    String wName = Convert.ToString(Server.HtmlDecode(wRow.Cells[2].Text));
                    GridViewRow sRow = this.gvwSectionList.SelectedRow;
                    String wsName = Convert.ToString(Server.HtmlDecode(sRow.Cells[2].Text)); 
                    GridViewRow rRow = this.gvwRunList.SelectedRow;
                    String rnName = Convert.ToString(Server.HtmlDecode(rRow.Cells[2].Text));
                    this.ancHead.InnerHtml = "<strong> " + oName + " > " + pName + " > " + wName + " > " + wsName + " > " + rnName + "</strong>";                         
                    
                    logger.Info("User: " + username + " Data_Review: Display Charts for Well/Later: " + wellName + " Run: " + rnName + " Selected");

                    this.btnMetaInfoReset.Enabled = true;
                    this.btnMetaInfoReset.CssClass = "btn btn-warning text-center";

                    this.divOpr.Visible = false;
                    this.divPad.Visible = false;
                    this.divWell.Visible = false;
                    this.divSec.Visible = false;
                    this.divRun.Visible = false;
                    this.liStep1.Attributes["class"] = "stepper-done";
                    this.liStep2.Attributes["class"] = "stepper-done";
                    this.liStep3.Attributes["class"] = "stepper-done";
                    this.liStep4.Attributes["class"] = "stepper-done";
                    this.liStep5.Attributes["class"] = "stepper-done";
                    this.divResultsOuter.Visible = true;
                    this.divNRes.Visible = false;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRunList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 wsID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                System.Data.DataTable runList = OPR.getRunForSelectedWellSectionTable(wlID, wsID, GetClientDBString);
                this.gvwRunList.DataSource = runList;
                this.gvwRunList.PageIndex = e.NewPageIndex;
                this.gvwRunList.DataBind();
                this.gvwRunList.Visible = true;
                this.gvwRunList.SelectedIndex = -1;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRQCWellResults_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 resID = -99;
                Decimal Depth = -99.00M;
                GridViewRow dRow = this.gvwRQCWellResults.SelectedRow;
                Int32 rowIndex = this.gvwRQCWellResults.SelectedRow.RowIndex;
                Label lblDepth = gvwRQCWellResults.Rows[rowIndex].FindControl("lblDepthItem") as Label;
                Depth = Convert.ToDecimal(lblDepth.Text);
                resID = Convert.ToInt32(this.gvwRQCWellResults.SelectedValue);
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 padID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 wsID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                Int32 rnID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                Int32 acID = QC.getAzimuthCriteriaIDForResultRow(resID, GetClientDBString);
                DataTable placeTable = QC.getPlacementTableforSelectedAzimuthCriteria(oprID, padID, wlID, Depth, acID, GetClientDBString);
                System.Data.DataTable selSQC = QC.getResSQCTable(resID, GetClientDBString);
                System.Data.DataTable refMagTable = OPR.getReferenceMagneticsTable(oprID, ClientID, padID, wlID, GetClientDBString);
                System.Data.DataTable resParam = QC.getResParameters(resID, GetClientDBString);
                System.Data.DataTable qualfParam = QC.getResQualifiersTable(resID, GetClientDBString);
                System.Data.DataTable BHATable = QC.getResBHATable(resID, GetClientDBString);
                System.Data.DataTable NMCTable = QC.getNMCalcTable(resID, GetClientDBString);
                System.Data.DataTable MinMaxTable = QC.getMinMaxTable(resID, GetClientDBString);
                System.Data.DataTable tieInTable = QC.getTieInSurveysTable(Depth, rnID, wsID, wlID, GetClientDBString);
                System.Data.DataTable placementTable = QC.getPlacementValuesTable(Depth, rnID, wsID, wlID, GetClientDBString);
                this.lblQCResMetaData.Visible = true;
                this.lblQCResParameters.Visible = true;
                this.lblRQCQualifiers.Visible = true;
                this.lblRQCBHA.Visible = true;
                this.lblNMSCalc.Visible = true;
                this.lblRQCMinMax.Visible = true;
                this.lblDepth.Visible = true;
                this.lblPlacement.Visible = true;
                this.lblQCResSQC.Visible = true;
                this.lblTieInSurvey.Visible = true;
                this.gvwResSQC.DataSource = selSQC;
                this.gvwResSQC.DataBind();
                this.gvwResSQC.Visible = true;
                this.gvwRefVals.DataSource = refMagTable;
                this.gvwRefVals.DataBind();
                this.gvwRefVals.Visible = true;
                this.gvwQCResParameters.DataSource = resParam;
                this.gvwQCResParameters.DataBind();
                this.gvwQCResParameters.Visible = true;
                this.gvwRQCQualifiers.DataSource = qualfParam;
                this.gvwRQCQualifiers.DataBind();
                this.gvwRQCQualifiers.Visible = true;
                this.gvwRQCBHA.DataSource = BHATable;
                this.gvwRQCBHA.DataBind();
                this.gvwRQCBHA.Visible = true;
                this.gvwNMSCalc.DataSource = NMCTable;
                this.gvwNMSCalc.DataBind();
                this.gvwNMSCalc.Visible = true;
                this.gvwRQCMinMax.DataSource = MinMaxTable;
                this.gvwRQCMinMax.DataBind();
                this.gvwRQCMinMax.Visible = true;
                this.gvwPlacement.DataSource = placeTable;
                this.gvwPlacement.DataBind();
                this.gvwPlacement.Visible = true;
                this.gvwTieInSurvey.DataSource = tieInTable;
                this.gvwTieInSurvey.DataBind();
                this.gvwTieInSurvey.Visible = true;
                this.createChartQCTotal();
                this.btnClear.Enabled = true;
                this.btnClear.CssClass = "btn btn-warning text-center";
                this.lblDepth.Text = " Results for Measured Depth: " + Depth;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRQCWellResults_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 OprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 PadID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                Int32 WellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                System.Data.DataTable resTable = QC.getMetaResultsForWell(OprID, ClientID, PadID, WellID, GetClientDBString);
                this.gvwRQCWellResults.DataSource = resTable;
                this.gvwRQCWellResults.PageIndex = e.NewPageIndex;
                this.gvwRQCWellResults.SelectedIndex = -1;
                this.gvwRQCWellResults.DataBind();
                this.lblDepth.Visible = false;
                this.lblQCResSQC.Visible = false;
                this.gvwResSQC.DataBind();
                this.gvwResSQC.Visible = false;
                this.lblQCResMetaData.Visible = false;
                this.gvwRefVals.DataBind();
                this.gvwRefVals.Visible = false;
                this.lblQCResParameters.Visible = false;
                this.gvwQCResParameters.DataBind();
                this.gvwQCResParameters.Visible = false;
                this.lblRQCQualifiers.Visible = false;
                this.gvwRQCQualifiers.DataBind();
                this.gvwRQCQualifiers.Visible = false;
                this.lblRQCBHA.Visible = false;
                this.gvwRQCBHA.DataBind();
                this.gvwRQCBHA.Visible = false;
                this.lblNMSCalc.Visible = false;
                this.gvwNMSCalc.DataBind();
                this.gvwNMSCalc.Visible = false;
                this.lblRQCMinMax.Visible = false;
                this.gvwRQCMinMax.DataBind();
                this.gvwRQCMinMax.Visible = false;
                this.btnChart_Click(null, null);
                this.btnClear.Enabled = true;
                this.btnClear.CssClass = "btn btn-warning text-center";
                this.btnChart_Click(null, null); // Generate the Graphs
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRQCWellResults_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                this.gvwRQCWellResults.EditIndex = e.NewEditIndex;                
                Int32 OprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 PadID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                Int32 WellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 srvyID = -99;
                Int32 eIndex = e.NewEditIndex;
                System.Data.DataTable resTable = QC.getMetaResultsForWell(OprID, ClientID, PadID, WellID, GetClientDBString);
                this.gvwRQCWellResults.DataSource = resTable;
                this.gvwRQCWellResults.DataBind();
                Label lblSrvy = gvwRQCWellResults.Rows[eIndex].FindControl("lblSrvyIDEdit") as Label;
                srvyID = Convert.ToInt32(lblSrvy.Text);
                HttpContext.Current.Session["selectedSurvey"] = srvyID;
                Int32 acID = QC.getAzimuthCriteriaIDForWell(OprID, PadID, WellID, srvyID, GetClientDBString);
                DropDownList ddlAzm = gvwRQCWellResults.Rows[e.NewEditIndex].FindControl("ddlEditAzmCriteria") as DropDownList;
                Dictionary<Int32, String> azmList = Helpers.DemogHelpers.Demographics.getAzimuthCriteriaList();
                ddlAzm.DataSource = azmList;
                ddlAzm.DataTextField = "Value";
                ddlAzm.DataValueField = "Key";
                ddlAzm.DataBind();
                ddlAzm.Items.FindByValue(acID.ToString()).Selected = true;
                gvwRQCWellResults.SelectedRowStyle.CssClass = null;
                this.btnChart_Click(null, null); // Generate the Graphs
                ddlAzm.Focus();                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRQCWellResults_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                gvwRQCWellResults.EditIndex = -1;                
                Int32 OprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 PadID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                Int32 WellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                System.Data.DataTable resTable = QC.getMetaResultsForWell(OprID, ClientID, PadID, WellID, GetClientDBString);
                this.gvwRQCWellResults.DataSource = resTable;
                this.gvwRQCWellResults.DataBind();
                this.gvwRQCWellResults.Focus();
                this.btnChart_Click(null, null); // Generate the Graphs
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRQCWellResults_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {                
                Int32 OprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 PadID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                Int32 WellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 srvyID = Convert.ToInt32(HttpContext.Current.Session["selectedSurvey"]);
                DropDownList ddlAzm = gvwRQCWellResults.Rows[e.RowIndex].FindControl("ddlEditAzmCriteria") as DropDownList;
                Int32 acID = Convert.ToInt32(ddlAzm.SelectedValue);
                Int32 iResult = QC.updateAzimuthCriteriaID(OprID, PadID, WellID, srvyID, acID, GetClientDBString);
                if (iResult.Equals(1))
                {
                    gvwRQCWellResults.EditIndex = -1;
                    System.Data.DataTable resTable = QC.getMetaResultsForWell(OprID, ClientID, PadID, WellID, GetClientDBString);
                    this.gvwRQCWellResults.DataSource = resTable;
                    this.gvwRQCWellResults.DataBind();
                    this.gvwRQCWellResults.Focus();
                }
                else
                {
                    ddlAzm.Focus();
                }
                this.btnChart_Click(null, null); // Generate the Graphs
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRQCRunResults_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 resID = -99;
                Decimal Depth = -99.00M;
                GridViewRow dRow = this.gvwRQCRunResults.SelectedRow;
                Int32 rowIndex = this.gvwRQCRunResults.SelectedRow.RowIndex;
                Label lblDepth = gvwRQCRunResults.Rows[rowIndex].FindControl("lblDepthItem") as Label;
                Depth = Convert.ToDecimal(lblDepth.Text);
                resID = Convert.ToInt32(this.gvwRQCRunResults.SelectedValue);
                Int32 opID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 pdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 wsID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                Int32 rnID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                DataTable selSQC = QC.getResSQCTable(resID, GetClientDBString);
                DataTable refMagTable = OPR.getReferenceMagneticsTable(opID, ClientID, pdID, wlID, GetClientDBString);
                DataTable resParam = QC.getResParameters(resID, GetClientDBString);
                DataTable qualfParam = QC.getResQualifiersTable(resID, GetClientDBString);
                DataTable BHATable = QC.getResBHATable(resID, GetClientDBString);
                DataTable NMCTable = QC.getNMCalcTable(resID, GetClientDBString);
                DataTable MinMaxTable = QC.getMinMaxTable(resID, GetClientDBString);
                Int32 acID = QC.getAzimuthCriteriaIDForResultRow(resID, GetClientDBString);
                DataTable placeTable = QC.getPlacementTableforSelectedAzimuthCriteria(opID, pdID, wlID, Depth, acID, GetClientDBString);
                DataTable plansTable = QC.getTieInSurveysTable(Depth, rnID, wsID, wlID, GetClientDBString);
                this.lblQCResMetaData.Visible = true;
                this.lblQCResParameters.Visible = true;
                this.lblRQCQualifiers.Visible = true;
                this.lblRQCBHA.Visible = true;
                this.lblNMSCalc.Visible = true;
                this.lblRQCMinMax.Visible = true;
                this.lblQCResSQC.Visible = true;
                this.lblDepth.Visible = true;
                this.lblPlacement.Visible = true;
                this.lblTieInSurvey.Visible = true;
                this.gvwResSQC.DataSource = selSQC;
                this.gvwResSQC.DataBind();
                this.gvwResSQC.Visible = true;
                this.gvwRefVals.DataSource = refMagTable;
                this.gvwRefVals.DataBind();
                this.gvwRefVals.Visible = true;
                this.gvwQCResParameters.DataSource = resParam;
                this.gvwQCResParameters.DataBind();
                this.gvwQCResParameters.Visible = true;
                this.gvwRQCQualifiers.DataSource = qualfParam;
                this.gvwRQCQualifiers.DataBind();
                this.gvwRQCQualifiers.Visible = true;
                this.gvwRQCBHA.DataSource = BHATable;
                this.gvwRQCBHA.DataBind();
                this.gvwRQCBHA.Visible = true;
                this.gvwNMSCalc.DataSource = NMCTable;
                this.gvwNMSCalc.DataBind();
                this.gvwNMSCalc.Visible = true;
                this.gvwRQCMinMax.DataSource = MinMaxTable;
                this.gvwRQCMinMax.DataBind();
                this.gvwRQCMinMax.Visible = true;
                this.gvwPlacement.DataSource = placeTable;
                this.gvwPlacement.DataBind();
                this.gvwPlacement.Visible = true;
                this.gvwTieInSurvey.DataSource = plansTable;
                this.gvwTieInSurvey.DataBind();
                this.gvwTieInSurvey.Visible = true;
                this.createChartQCTotal();
                this.btnClear.Enabled = true;
                this.btnClear.CssClass = "btn btn-warning text-center";
                this.lblDepth.Text = " Results for Measured Depth: " + Depth;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRQCRunResults_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 optrID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 wpdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                Int32 wID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 sID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                Int32 rID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                System.Data.DataTable resTable = QC.getMetaResults(optrID, wpdID, wID, sID, rID, GetClientDBString);
                this.gvwRQCRunResults.DataSource = resTable;
                this.gvwRQCRunResults.PageIndex = e.NewPageIndex;
                this.gvwRQCRunResults.SelectedIndex = -1;
                this.gvwRQCRunResults.DataBind();
                this.lblQCResSQC.Visible = false;
                this.gvwResSQC.DataBind();
                this.gvwResSQC.Visible = false;
                this.lblQCResMetaData.Visible = false;
                this.gvwRefVals.DataBind();
                this.gvwRefVals.Visible = false;
                this.lblQCResParameters.Visible = false;
                this.gvwQCResParameters.DataBind();
                this.gvwQCResParameters.Visible = false;
                this.lblRQCQualifiers.Visible = false;
                this.gvwRQCQualifiers.DataBind();
                this.gvwRQCQualifiers.Visible = false;
                this.lblRQCBHA.Visible = false;
                this.gvwRQCBHA.DataBind();
                this.gvwRQCBHA.Visible = false;
                this.lblNMSCalc.Visible = false;
                this.gvwNMSCalc.DataBind();
                this.gvwNMSCalc.Visible = false;
                this.lblRQCMinMax.Visible = false;
                this.gvwRQCMinMax.DataBind();
                this.gvwRQCMinMax.Visible = false;
                this.btnClear.Enabled = true;
                this.btnClear.CssClass = "btn btn-warning text-center";
                this.btnChart_Click(null, null); // Generate the Graphs
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRQCRunResults_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                this.gvwRQCRunResults.EditIndex = e.NewEditIndex;
                this.createChartQCTotal();
                Int32 OprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 PadID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                Int32 WellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 SectionID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                Int32 RunID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                Int32 srvyID = -99;
                Int32 eIndex = e.NewEditIndex;
                System.Data.DataTable resTable = QC.getMetaResults(OprID, PadID, WellID, SectionID, RunID, GetClientDBString);
                this.gvwRQCRunResults.DataSource = resTable;
                this.gvwRQCRunResults.DataBind();
                Label lblSrvy = gvwRQCRunResults.Rows[eIndex].FindControl("lblSrvyIDEdit") as Label;
                srvyID = Convert.ToInt32(lblSrvy.Text);
                HttpContext.Current.Session["selectedSurvey"] = srvyID;                
                Int32 acID = QC.getAzimuthCriteriaID(OprID, PadID, WellID, SectionID, RunID, srvyID, GetClientDBString);
                DropDownList ddlAzm = gvwRQCRunResults.Rows[e.NewEditIndex].FindControl("ddlEditRunAzmCriteria") as DropDownList;
                Dictionary<Int32, String> azmList = Helpers.DemogHelpers.Demographics.getAzimuthCriteriaList();
                ddlAzm.DataSource = azmList;
                ddlAzm.DataTextField = "Value";
                ddlAzm.DataValueField = "Key";
                ddlAzm.DataBind();
                ddlAzm.Items.FindByValue(acID.ToString()).Selected = true;
                gvwRQCRunResults.SelectedRowStyle.CssClass = null;
                ddlAzm.Focus();                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRQCRunResults_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                gvwRQCRunResults.EditIndex = -1;
                this.createChartQCTotal();
                Int32 OprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 PadID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                Int32 WellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 SectionID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                Int32 RunID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                System.Data.DataTable resTable = QC.getMetaResults(OprID, PadID, WellID, SectionID, RunID, GetClientDBString);
                this.gvwRQCRunResults.DataSource = resTable;
                this.gvwRQCRunResults.DataBind();
                this.gvwRQCRunResults.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRQCRunResults_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                this.createChartQCTotal();
                Int32 OprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 PadID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                Int32 WellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 SectionID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                Int32 RunID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                Int32 srvyID = Convert.ToInt32(HttpContext.Current.Session["selectedSurvey"]);
                DropDownList ddlAzm = gvwRQCRunResults.Rows[e.RowIndex].FindControl("ddlEditRunAzmCriteria") as DropDownList;
                Int32 acID = Convert.ToInt32(ddlAzm.SelectedValue);
                Int32 iResult = QC.updateAzimuthCriteriaID(OprID, PadID, WellID, srvyID, acID, GetClientDBString);
                if (iResult.Equals(1))
                {
                    gvwRQCRunResults.EditIndex = -1;
                    System.Data.DataTable resTable = QC.getMetaResults(OprID, PadID, WellID, SectionID, RunID, GetClientDBString);
                    this.gvwRQCRunResults.DataSource = resTable;
                    this.gvwRQCRunResults.DataBind();
                    this.gvwRQCRunResults.Focus();
                }
                else
                {
                    ddlAzm.Focus();
                }         
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnClearMetaInfo_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientType);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = 0;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
                this.gvwWellList.DataBind();
                this.gvwWellList.SelectedIndex = -1;
                this.gvwSectionList.DataBind();
                this.gvwSectionList.SelectedIndex = -1;
                this.gvwRunList.DataBind();
                this.gvwRunList.SelectedIndex = -1;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDisplayChartsDataNoPlan_Click(object sender, EventArgs e)
        {
            try
            {
                //this.btnChart_Click(null, null);
                this.createChartQCTotal();
                this.charts.Visible = true;
                this.chartsWplan.Visible = false;
                logger.Info("User: " + username + " Data_Review: Display Charts Button clicked");
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDisplayChartsDataPlan_Click(object sender, EventArgs e)
        {
            try
            {
                this.createChartQCTotalwithPlan();
                this.charts.Visible = false;
                this.chartsWplan.Visible = true;
                logger.Info("User: " + username + " Data_Review: Display Charts with Well Plan Button clicked");
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlEditAzmCriteria_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.gvwRQCWellResults.Focus();
        }

        protected void ddlEditRunAzmCriteria_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.gvwRQCRunResults.Focus();
        }                                        
    }
}