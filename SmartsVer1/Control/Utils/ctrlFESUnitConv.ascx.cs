﻿using System;
using log4net;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Net.Mail;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using QC = SmartsVer1.QCHelpers.sdRawDataQC;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using CHRT = SmartsVer1.Helpers.ChartHelpers.Gauges;
using WPN = SmartsVer1.Helpers.QCHelpers.WellPlan;
using SRV = SmartsVer1.Helpers.DemogHelpers.ServicesConfig;
using CLR = System.Drawing.Color;


namespace SmartsVer1.Control.Utils
{
    public partial class ctrlFESUnitConv : System.Web.UI.UserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ctrlFESUnitConv));
        String LoginName, username, domain, GetClientDBString;
        Int32 ClientID = -99;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}