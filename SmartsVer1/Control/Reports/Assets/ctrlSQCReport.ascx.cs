﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI.WebControls;
using MSRpt = Microsoft.Reporting.WebForms;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using RPT = SmartsVer1.Helpers.ReportHelpers.Reports;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;

namespace SmartsVer1.Control.Reports.Assets
{
    public partial class ctrlSQCReport : System.Web.UI.UserControl
    {
        String LoginName, username, domain, client, GetClientDBString;
        Int32 disclaimerID = -99, jobClass = 1, ClientID = -99;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
                //Loading Page Report parameters to DataTables
                disclaimerID = RPT.getDisclaimerID("AssetReports");
                System.Data.DataTable tblUser = new System.Data.DataTable();
                System.Data.DataTable tblDisc = new System.Data.DataTable();
                System.Data.DataTable tblSQC = new System.Data.DataTable();
                tblUser = RPT.getUserInfo(LoginName, domain);
                tblDisc = RPT.getDisclaimer(disclaimerID);
                tblSQC = RPT.getSQCAssetList(GetClientDBString);
                //Loading DataSources
                MSRpt.ReportDataSource usrInfo = new MSRpt.ReportDataSource("UserProfile", tblUser);
                MSRpt.ReportDataSource dscInfo = new MSRpt.ReportDataSource("AssetDisclaimer", tblDisc);
                MSRpt.ReportDataSource sqcInfo = new MSRpt.ReportDataSource("SQCList", tblSQC);
                //Loading datasources to local report
                this.rvwrSQC.LocalReport.DataSources.Clear();
                this.rvwrSQC.LocalReport.DataSources.Add(usrInfo);
                this.rvwrSQC.LocalReport.DataSources.Add(sqcInfo);
                this.rvwrSQC.LocalReport.DataSources.Add(dscInfo);
                this.rvwrSQC.LocalReport.Refresh();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}