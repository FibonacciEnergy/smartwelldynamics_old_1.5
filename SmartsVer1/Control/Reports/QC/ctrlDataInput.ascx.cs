﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using log4net;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using OPR = SmartsVer1.Helpers.QCHelpers.Opr;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using SRV = SmartsVer1.Helpers.DemogHelpers.ServicesConfig;

namespace SmartsVer1.Control.Reports.QC
{
    public partial class ctrlDataInput : System.Web.UI.UserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ctrlDataInput));
        const Int32 JobClassID = 1;
        const Int32 serviceGroupID = 2;
        const Int32 ClientTypID = 4;
        String LoginName, username, domain, GetClientDBString;
        Int32 SysClientID = -99;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                SysClientID = Convert.ToInt32(usrP.clntID);

                System.Data.DataTable srvTable = SRV.getServicesTable(serviceGroupID, GetClientDBString);
                this.gvwSrvGrpService.DataSource = srvTable;
                this.gvwSrvGrpService.SelectedIndex = -1;
                this.gvwSrvGrpService.DataBind();
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSrvGrpService_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable srvTable = SRV.getServicesTable(serviceGroupID, GetClientDBString);
                this.gvwSrvGrpService.DataSource = srvTable;
                this.gvwSrvGrpService.PageIndex = e.NewPageIndex;
                this.gvwSrvGrpService.SelectedIndex = -1;
                this.gvwSrvGrpService.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSrvGrpService_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srvID = Convert.ToInt32(this.gvwSrvGrpService.SelectedValue);
                this.divSrvType.Attributes["class"] = "panel-collapse collapse";
                this.divJobClassList.Attributes["class"] = "panel-collapse collapse in";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }     

        protected void gvwWellList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.divReportType.Visible = true;
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                HttpContext.Current.Session["wellName"] = Convert.ToString(this.gvwWellList.SelectedRow.Cells[2].Text);
                System.Data.DataTable rtTable = DMG.getReportTypeTable();
                this.divReportType.Visible = true;
                this.gvwReportType.DataSource = rtTable;
                this.gvwReportType.SelectedIndex = -1;
                this.gvwReportType.DataBind();
                this.divSectionList.Visible = false;
                this.gvwSectionList.DataBind();
                this.gvwSectionList.SelectedIndex = -1;
                this.divRunList.Visible = false;
                this.gvwRunList.DataBind();
                this.gvwRunList.SelectedIndex = -1;                
                GridViewRow dRow = this.gvwWellList.SelectedRow;
                String wName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.divWellList.Attributes["class"] = "panel-collapse collapse";
                this.divReportType.Attributes["class"] = "panel-collapse collapse in";
                this.ancWellList.InnerHtml = "Well :<span style='text-decoration: underline; font-weight: bold'>" + wName + "</span>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwReportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 rtID = Convert.ToInt32(this.gvwReportType.SelectedValue);
                if (rtID.Equals(1))
                {
                    this.divReportType.Visible = false;
                    GridViewRow dRow = this.gvwReportType.SelectedRow;
                    String rName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                    this.divReportType.Attributes["class"] = "panel-collapse collapse";
                    this.ancRType.InnerHtml = "Report Type :<span style='text-decoration: underline; font-weight: bold'>" + rName + "</span>";
                    this.divReports.Visible = true;
                    this.divCompleteReport.Visible = true;
                    this.divSectionReport.Visible = false;
                    this.divRunReport.Visible = false;
                    this.btnMetaCancel.Visible = false;
                    this.btnReportDone.Visible = true;
                }
                else
                {
                    Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                    HttpContext.Current.Session["reportType"] = Convert.ToString(this.gvwReportType.SelectedRow.Cells[2].Text);
                    System.Data.DataTable rtTable = DMG.getReportTypeTable();
                    this.divSectionList.Visible = true;
                    System.Data.DataTable wsTable = OPR.getWellSectionToWellTable(wlID, GetClientDBString);
                    this.divSectionList.Visible = true;
                    this.gvwSectionList.DataSource = wsTable;
                    this.gvwSectionList.DataBind();
                    this.gvwSectionList.SelectedIndex = -1;
                    this.divRunList.Visible = false;
                    this.gvwRunList.DataBind();
                    this.gvwRunList.SelectedIndex = -1;
                    GridViewRow dRow = this.gvwReportType.SelectedRow;
                    String rName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                    this.divReportType.Attributes["class"] = "panel-collapse collapse";
                    this.divSectionList.Attributes["class"] = "panel-collapse collapse in";
                    this.ancRType.InnerHtml = "Report Type :<span style='text-decoration: underline; font-weight: bold'>" + rName + "</span>";
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSectionList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                System.Data.DataTable wsTable = OPR.WellSectionNameToWellTable(wlID, GetClientDBString);
                this.gvwSectionList.DataSource = wsTable;
                this.gvwSectionList.PageIndex = e.NewPageIndex;
                this.gvwSectionList.DataBind();
                this.gvwSectionList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSectionList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 wsID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                Int32 rtID = Convert.ToInt32(this.gvwReportType.SelectedValue);
                if (rtID.Equals(2))
                {
                    this.divRunList.Visible = false;
                    this.gvwRunList.DataBind();
                    this.gvwRunList.SelectedIndex = -1;
                    GridViewRow dRow = this.gvwSectionList.SelectedRow;
                    String sName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                    this.divSectionList.Attributes["class"] = "panel-collapse collapse";                    
                    this.ancSection.InnerHtml = "Well Section :<span style='text-decoration: underline; font-weight: bold'>" + sName + "</span>";
                    this.divReports.Visible = true;
                    this.divCompleteReport.Visible = false;
                    this.divSectionReport.Visible = true;
                    this.divRunReport.Visible = false;
                    this.btnMetaCancel.Visible = false;
                    this.btnReportDone.Visible = true;
                }
                else
                {
                    this.divRunList.Visible = true;
                    System.Data.DataTable runList = OPR.getRunForSelectedWellSectionTable(wlID, wsID, GetClientDBString);
                    this.gvwRunList.DataSource = runList;
                    this.gvwRunList.DataBind();
                    this.gvwRunList.SelectedIndex = -1;
                    GridViewRow dRow = this.gvwSectionList.SelectedRow;
                    String sName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                    this.divSectionList.Attributes["class"] = "panel-collapse collapse";
                    this.divRunList.Attributes["class"] = "panel-collapse collapse in";
                    this.ancSection.InnerHtml = "Well Section :<span style='text-decoration: underline; font-weight: bold'>" + sName + "</span>";
                    this.divCompleteReport.Visible = false;
                    this.divSectionReport.Visible = false;
                    this.divRunReport.Visible = false;
                }                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRunList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 wsID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                System.Data.DataTable runList = OPR.getRunForSelectedWellSectionTable(wlID, wsID, GetClientDBString);
                this.gvwRunList.DataSource = runList;
                this.gvwRunList.PageIndex = e.NewPageIndex;
                this.gvwRunList.DataBind();
                this.gvwRunList.Visible = true;
                this.gvwRunList.SelectedIndex = -1;
                this.gvwRunList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRunList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 rID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                GridViewRow dRow = this.gvwRunList.SelectedRow;
                String rName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.divRunList.Attributes["class"] = "panel-collapse collapse";
                this.ancRun.InnerHtml = "Run :<span style='text-decoration: underline; font-weight: bold'>" + rName + "</span>";
                this.divReports.Visible = true;
                this.divCompleteReport.Visible = false;
                this.divSectionReport.Visible = false;
                this.divRunReport.Visible = true;
                this.rvwrRun.LocalReport.Refresh();
                this.btnMetaCancel.Visible = false;
                this.btnReportDone.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnMetaCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.divWellList.Visible = false;
                this.gvwWellList.DataBind();
                this.gvwWellList.SelectedIndex = -1;
                this.divReportType.Visible = false;
                this.gvwReportType.DataBind();
                this.gvwReportType.SelectedIndex = -1;
                this.divSectionList.Visible = false;
                this.gvwSectionList.DataBind();
                this.gvwSectionList.SelectedIndex = -1;
                this.divRunList.Visible = false;
                this.gvwRunList.DataBind();
                this.gvwRunList.SelectedIndex = -1;
                this.divJobClassList.Attributes["class"] = "panel-collapse collapse in";
                this.ancJobClass.InnerHtml = "Job Class";
                this.ancJobList.InnerHtml = " Job Name/Work Order #";
                this.ancWellList.InnerHtml = "Well";
                this.ancRType.InnerHtml = "Report Type";
                this.ancSection.InnerHtml = "Well Section";
                this.ancRun.InnerHtml = "Run";
                this.btnMetaCancel.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnReportDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnMetaCancel_Click(null, null);
                this.divReports.Visible = false;
                this.btnReportDone.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rvwrComplete_Load(object sender, EventArgs e)
        {
            try
            {
                String exportOption = "PDF";

                RenderingExtension extension = rvwrComplete.LocalReport.ListRenderingExtensions().ToList().Find(x => x.Name.Equals(exportOption, StringComparison.CurrentCultureIgnoreCase));
                if (extension != null)
                {
                    System.Reflection.FieldInfo fInfo = extension.GetType().GetField("m_isVisible", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
                    fInfo.SetValue(extension, false);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rvwrSection_Load(object sender, EventArgs e)
        {
            try
            {
                String exportOption = "PDF";

                RenderingExtension extension = rvwrSection.LocalReport.ListRenderingExtensions().ToList().Find(x => x.Name.Equals(exportOption, StringComparison.CurrentCultureIgnoreCase));
                if (extension != null)
                {
                    System.Reflection.FieldInfo fInfo = extension.GetType().GetField("m_isVisible", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
                    fInfo.SetValue(extension, false);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rvwrRun_Load(object sender, EventArgs e)
        {
            try
            {
                String exportOption = "PDF";

                RenderingExtension extension = rvwrRun.LocalReport.ListRenderingExtensions().ToList().Find(x => x.Name.Equals(exportOption, StringComparison.CurrentCultureIgnoreCase));
                if (extension != null)
                {
                    System.Reflection.FieldInfo fInfo = extension.GetType().GetField("m_isVisible", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
                    fInfo.SetValue(extension, false);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }                        
    }
}