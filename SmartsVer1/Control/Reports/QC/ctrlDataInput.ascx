﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlDataInput.ascx.cs" Inherits="SmartsVer1.Control.Reports.QC.ctrlDataInput" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" ></asp:ScriptManager>

<link href="../../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">        
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../../Viewer/crdViewer.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li> Reports</li>
                <li> Data Audit/QC Reports</li>
                <li><a href="../../../Reports/QC/DataInputReport_2A.aspx"> Data Input Report</a></li>
            </ol>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h1 class="box-title">SMARTs Input Data Report</h1>
            </div>
            <div class="box-body">                
                <asp:UpdateProgress ID="upgrsMeta" runat="server" AssociatedUpdatePanelID="upnlMeta" DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="divDataReviewProgressbar" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center; z-index: 100;">
                            <asp:Image ID="upgrsMetaImage" runat="server" CssClass="center" AlternateText="Processing ...." ForeColor="Purple" ImageAlign="Middle"
                                ImageUrl="~/Images/Progress.gif" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <asp:UpdatePanel runat="server" ID="upnlMeta">
                    <ContentTemplate>
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h5 class="panel-title" style="text-align: left">
                                        <a id="ancServiceType" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlAssets_2A_acrdSOList" href="#BodyContent_ctrlAssets_2A_divSrvType">
                                            Service Type</a>
                                    </h5>
                                </div>
                                <div id="divSrvType" runat="server" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <asp:GridView ID="gvwSrvGrpService" runat="server" DataKeyNames="srvID" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="false"
                                            CssClass="mydatagrid" OnPageIndexChanging="gvwSrvGrpService_PageIndexChanging"
                                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                            RowStyle-CssClass="rows" OnSelectedIndexChanged="gvwSrvGrpService_SelectedIndexChanged">
                                            <Columns>
                                                <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                    <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                </asp:ButtonField>
                                                <asp:BoundField DataField="srvID" HeaderText="srvID" Visible="False" />
                                                <asp:BoundField DataField="srvName" HeaderText="SMARTs Service Name" />
                                                <asp:BoundField DataField="srvStatID" HeaderText="Status" />
                                                <asp:BoundField DataField="srvVersion" HeaderText="Version" />
                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" Visible="false" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h5 class="panel-title" style="text-align: left">
                                        <a id="ancJobClass" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlDataInput_accordion" href="#BodyContent_ctrlDataInput_divJobClass"> Job Class</a>
                                    </h5>
                                </div>
                                <div id="divJobClassList" runat="server" class="panel-collapse collapse">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h5 class="panel-title" style="text-align: left">
                                        <a id="ancJobList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlDataInput_accordion" href="#BodyContent_ctrlDataInput_divJobClass"> Job Name/Work Order #</a>
                                    </h5>
                                </div>
                                <div id="divJobList" runat="server" class="panel-collapse collapse">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h5 class="panel-title" style="text-align: left">
                                        <a id="ancWellList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlDataInput_accordion" href="#BodyContent_ctrlDataInput_divWellList"> Well </a>
                                    </h5>
                                </div>
                                <div id="divWellList" runat="server" visible="false" class="panel-collapse collapse">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:GridView ID="gvwWellList" runat="server" CssClass="mydatagrid" EmptyDataText="No Wells associated with the selected Job/Worker Order #"
                                                AllowPaging="True" Width="99.6%" AutoGenerateColumns="False" DataKeyNames="welID" OnSelectedIndexChanged="gvwWellList_SelectedIndexChanged"
                                                ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwWellList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                PageSize="15" PagerSettings-Mode="Numeric" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Center" PagerStyle-VerticalAlign="Middle"
                                                HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                                <Columns>
                                                    <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                        <ControlStyle CssClass="btn btn-primary text-center"></ControlStyle>
                                                    </asp:ButtonField>
                                                    <asp:BoundField DataField="welID" Visible="false" />
                                                    <asp:BoundField DataField="welName" HeaderText="Well Name" />
                                                    <asp:BoundField DataField="oprName" HeaderText="Operator Company" />
                                                    <asp:BoundField DataField="fldName" HeaderText="Field" />
                                                    <asp:BoundField DataField="cnyName" HeaderText="County/District" />
                                                    <asp:BoundField DataField="apiuwi" HeaderText="API/UWI" />
                                                    <asp:BoundField DataField="spudDate" HeaderText="Spud Date" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h5 class="panel-title" style="text-align: left">
                                        <a id="ancRType" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlDataInput_accordion" href="#BodyContent_ctrlDataInput_divReportType"> Report Type </a>
                                    </h5>
                                </div>
                                <div id="divReportType" runat="server" visible="false" class="panel-collapse collapse">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:GridView ID="gvwReportType" runat="server" CssClass="mydatagrid" EmptyDataText="No Well Section associated with the selected Well"
                                                AutoGenerateColumns="False" DataKeyNames="rtID" OnSelectedIndexChanged="gvwReportType_SelectedIndexChanged"
                                                ShowHeaderWhenEmpty="True" AlternatingRowStyle-CssClass="AlternatingRowStyle" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                                <Columns>
                                                    <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                        <ControlStyle CssClass="btn btn-primary text-center"></ControlStyle>
                                                    </asp:ButtonField>
                                                    <asp:BoundField DataField="rtID" Visible="false" />
                                                    <asp:BoundField DataField="rtValue" HeaderText="Report Type" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h5 class="panel-title" style="text-align: left">
                                        <a id="ancSection" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlDataInput_accordion" href="#BodyContent_ctrlDataInput_divSectionList"> Well Section </a>
                                    </h5>
                                </div>
                                <div id="divSectionList" runat="server" visible="false" class="panel-collapse collapse">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:GridView ID="gvwSectionList" runat="server" CssClass="mydatagrid" EmptyDataText="No Well Section associated with the selected Well"
                                                AllowPaging="True" Width="99.6%" AutoGenerateColumns="False" DataKeyNames="wswID" OnSelectedIndexChanged="gvwSectionList_SelectedIndexChanged"
                                                ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwSectionList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                PageSize="15" PagerSettings-Mode="Numeric" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Center" PagerStyle-VerticalAlign="Middle"
                                                HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                                <Columns>
                                                    <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                        <ControlStyle CssClass="btn btn-primary text-center"></ControlStyle>
                                                    </asp:ButtonField>
                                                    <asp:BoundField DataField="wswID" Visible="false" />
                                                    <asp:BoundField DataField="wsName" HeaderText="Well Section Name" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="rnCount" HeaderText="Run Count" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h5 class="panel-title" style="text-align: left">
                                        <a id="ancRun" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlDataInput_accordion" href="#BodyContent_ctrlDataInput_divRunList"> Run</a>
                                    </h5>
                                </div>
                                <div id="divRunList" runat="server" visible="false" class="panel-collapse collapse">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:GridView ID="gvwRunList" runat="server" CssClass="mydatagrid" EmptyDataText="No Run associated with the selected Section"
                                                AllowPaging="True" Width="99.6%" AutoGenerateColumns="False" DataKeyNames="runID" OnSelectedIndexChanged="gvwRunList_SelectedIndexChanged"
                                                ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwRunList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                PageSize="15" PagerSettings-Mode="Numeric" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Center" PagerStyle-VerticalAlign="Middle"
                                                HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                                <Columns>
                                                    <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                        <ControlStyle CssClass="btn btn-primary text-center"></ControlStyle>
                                                    </asp:ButtonField>
                                                    <asp:BoundField DataField="runID" Visible="false" />
                                                    <asp:BoundField DataField="rnName" HeaderText="Run Name" />
                                                    <asp:BoundField DataField="rnStatus" HeaderText="Status" />
                                                    <asp:BoundField DataField="rnBHA" HeaderText="BHA Signature ID" />
                                                    <asp:BoundField DataField="rnToolcode" HeaderText="Toolcode" />
                                                    <asp:BoundField DataField="rnSQC" HeaderText="Survey Qualification Criteria" />
                                                    <asp:BoundField DataField="rnStartD" HeaderText="Start Measured Depth(m)" ItemStyle-HorizontalAlign="Right" />
                                                    <asp:BoundField DataField="rnEndD" HeaderText="End Measured Depth(m)" ItemStyle-HorizontalAlign="Right" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Accordian -->
                        <div id="divCancelButtonRow" class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="input-group" style="width: 100%;">
                                    <asp:Button ID="btnMetaCancel" runat="server" Text="Clear Values" CssClass="btn btn-warning text-center" Visible="false" Width="15%"
                                        OnClick="btnMetaCancel_Click" />
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="gvwReportType" />
                        <asp:PostBackTrigger ControlID="gvwSectionList" />
                        <asp:PostBackTrigger ControlID="gvwRunList" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div id="divReports" runat="server" visible="false">
            <asp:UpdateProgress ID="upgrsReports" runat="server" AssociatedUpdatePanelID="upnlReports" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="divDataReviewProgressbar" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center; z-index: 100;">
                        <asp:Image ID="upgrsReportsImage" runat="server" CssClass="center" AlternateText="Processing ...." ForeColor="Purple" ImageAlign="Middle"
                            ImageUrl="~/Images/Progress.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel runat="server" ID="upnlReports">
                <ContentTemplate>
                    <div class="box box-primary">
                        <div id="divCompleteReport" runat="server" visible="false">
                            <div class="box-header with-border">
                                <h1 class="box-title">SMARTs Input Data Report for Well</h1>
                            </div>
                            <div class="box-body">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <rsweb:ReportViewer ID="rvwrComplete" runat="server" Font-Names="Verdana" Font-Size="8pt" Height=""
                                        WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" SizeToReportContent="True"
                                        ShowExportControls="True" ShowFindControls="False" ShowZoomControl="False" ShowRefreshButton="False"
                                        DocumentMapWidth="100%" OnLoad="rvwrComplete_Load">
                                        <LocalReport ReportPath="Control\Reports\QC\WellDataInput.rdlc"></LocalReport>
                                    </rsweb:ReportViewer>
                                </div>
                            </div>
                        </div>
                        <div id="divSectionReport" runat="server" visible="false">
                            <div class="box-header with-border">
                                <h1 class="box-title">SMARTs Input Data Report for Section</h1>
                            </div>
                            <div class="box-body">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <rsweb:ReportViewer ID="rvwrSection" runat="server" Font-Names="Verdana" Font-Size="8pt" Height=""
                                        WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" SizeToReportContent="True"
                                        ShowExportControls="True" ShowFindControls="False" ShowZoomControl="False" ShowRefreshButton="False"
                                        DocumentMapWidth="100%" OnLoad="rvwrSection_Load">
                                        <LocalReport ReportPath="Control\Reports\QC\SectionDataInput.rdlc"></LocalReport>
                                    </rsweb:ReportViewer>
                                </div>
                            </div>
                        </div>
                        <div id="divRunReport" runat="server" visible="false">
                            <div class="box-header with-border">
                                <h1 class="box-title">SMARTs Input Data Report for Run</h1>
                            </div>
                            <div class="box-body">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <rsweb:ReportViewer ID="rvwrRun" runat="server" Font-Names="Verdana" Font-Size="8pt" Height=""
                                        WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" SizeToReportContent="True"
                                        ShowExportControls="True" ShowFindControls="False" ShowZoomControl="False" ShowRefreshButton="False"
                                        DocumentMapWidth="100%" OnLoad="rvwrRun_Load">
                                        <LocalReport ReportPath="Control\Reports\QC\RunDataInput.rdlc"></LocalReport>
                                    </rsweb:ReportViewer>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="input-group" style="width: 100%;">
                                <asp:Button ID="btnReportDone" runat="server" CssClass="btn btn-primary text-center" Text="Done" Visible="false" Width="15%"
                                    OnClick="btnReportDone_Click" />
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnReportDone" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</div>


<!-- JavaScripts -->
<%--<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>--%>