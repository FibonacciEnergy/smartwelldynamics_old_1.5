﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using log4net;
using Color = System.Drawing.Color;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using MSRpt = Microsoft.Reporting.WebForms.ReportDataSource;
using RPT = SmartsVer1.Helpers.ReportHelpers.Reports;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using OPR = SmartsVer1.Helpers.QCHelpers.Opr;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using SRV = SmartsVer1.Helpers.DemogHelpers.ServicesConfig;

namespace SmartsVer1.Control.Reports.QC
{
    public partial class ctrlDataAudit : System.Web.UI.UserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ctrlDataInput));
        const Int32 JobClassID = 1;
        const Int32 serviceGroupID = 2;
        const Int32 ClientTypID = 4;
        String LoginName, username, domain, GetClientDBString;
        Int32 ClientID = -99;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();
                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);
                username = addr.User;
                domain = addr.Host;
                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
                //DataSource
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientTypID);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = 0;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwGlobalOptr_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientTypID);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = e.NewPageIndex;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
                this.btnMetaCancel.CssClass = "btn btn-warning text-center";
                this.btnMetaCancel.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwGlobalOptr_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow dRow = this.gvwGlobalOptr.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.ancHead.InnerHtml = "<strong> " + oName + "</strong>";
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                System.Data.DataTable padTable = AST.getClientWellPadTableForOperator(oprID, ClientID);
                this.gvwWellPads.DataSource = padTable;
                this.gvwWellPads.PageIndex = 0;
                this.gvwWellPads.SelectedIndex = -1;
                this.gvwWellPads.DataBind();
                this.divOpr.Visible = false;
                this.divPad.Visible = true;
                this.divWell.Visible = false;
                this.divSec.Visible = false;
                this.divRun.Visible = false;
                this.btnMetaCancel.Enabled = true;
                this.btnMetaCancel.CssClass = "btn btn-warning text-center";
                this.liStep1.Attributes["class"] = "stepper-done";
                this.liStep2.Attributes["class"] = "stepper-todo";
                this.liStep3.Attributes["class"] = "stepper-todo";
                this.liStep4.Attributes["class"] = "stepper-todo";
                this.liStep5.Attributes["class"] = "stepper-todo";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellPads_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                System.Data.DataTable padTable = AST.getClientWellPadTableForOperator(oprID, ClientID);
                this.gvwWellPads.DataSource = padTable;
                this.gvwWellPads.PageIndex = e.NewPageIndex;
                this.gvwWellPads.SelectedIndex = -1;
                this.gvwWellPads.DataBind();
                this.btnMetaCancel.CssClass = "btn btn-warning text-center";
                this.btnMetaCancel.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellPads_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow oRow = this.gvwGlobalOptr.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                GridViewRow pRow = this.gvwWellPads.SelectedRow;
                String pName = Convert.ToString(Server.HtmlDecode(pRow.Cells[2].Text));
                this.ancHead.InnerHtml = "<strong> " + oName + " > " + pName + "</strong>";
                Int32 pdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                System.Data.DataTable pdTable = AST.getWellForWellPadTable(pdID, GetClientDBString);
                this.gvwWellList.DataSource = pdTable;
                this.gvwWellList.PageIndex = 0;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();
                System.Data.DataTable padInfo = RPT.getPadInfoTable(pdID, GetClientDBString);
                HttpContext.Current.Session["padInfoTable"] = padInfo;
                this.divOpr.Visible = false;
                this.divPad.Visible = false;
                this.divWell.Visible = true;
                this.divSec.Visible = false;
                this.divRun.Visible = false;
                this.btnMetaCancel.Enabled = true;
                this.btnMetaCancel.CssClass = "btn btn-warning text-center";
                this.liStep1.Attributes["class"] = "stepper-done";
                this.liStep2.Attributes["class"] = "stepper-done";
                this.liStep3.Attributes["class"] = "stepper-todo";
                this.liStep4.Attributes["class"] = "stepper-todo";
                this.liStep5.Attributes["class"] = "stepper-todo";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        protected void gvwWellList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 pdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                System.Data.DataTable pdTable = AST.getWellForWellPadTable(pdID, GetClientDBString);
                this.gvwWellList.DataSource = pdTable;
                this.gvwWellList.PageIndex = e.NewPageIndex;
                this.gvwWellList.DataBind();
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 welID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                GridViewRow dRow = this.gvwWellList.SelectedRow;
                Decimal welTarget = AST.getClientWellPlannedMaxMDepth(welID, GetClientDBString);
                String welStatus = Convert.ToString(Server.HtmlDecode(dRow.Cells[7].Text));
                System.Data.DataTable wellInfo = RPT.getWellInfoTable(welID, GetClientDBString);
                System.Data.DataTable refMags = RPT.getWellRefMagsTable(welID, GetClientDBString);                
                HttpContext.Current.Session["wellInfoTable"] = wellInfo;
                HttpContext.Current.Session["refMagsTable"] = refMags;
                {
                    System.Data.DataTable wsTable = OPR.getWellSectionToWellTable(welID, GetClientDBString);
                    this.gvwSectionList.DataSource = wsTable;
                    this.gvwSectionList.DataBind();
                    this.gvwSectionList.SelectedIndex = -1;
                    this.divOpr.Visible = false;
                    this.divPad.Visible = false;
                    this.divWell.Visible = false;
                    this.divSec.Visible = true;
                    this.divRun.Visible = false;
                    GridViewRow oRow = this.gvwGlobalOptr.SelectedRow;
                    String oName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                    GridViewRow pRow = this.gvwWellPads.SelectedRow;
                    String pName = Convert.ToString(Server.HtmlDecode(pRow.Cells[2].Text));
                    String wName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                    this.ancHead.InnerHtml = "<strong> " + oName + " > " + pName + " > " + wName + "</strong>";
                    this.liStep1.Attributes["class"] = "stepper-done";
                    this.liStep2.Attributes["class"] = "stepper-done";
                    this.liStep3.Attributes["class"] = "stepper-done";
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = ((DataRowView)e.Row.DataItem).Row;
                    String stC = Convert.ToString(dr["stClr"]);
                    e.Row.Cells[7].BackColor = Color.FromName(stC.ToString());
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSectionList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                System.Data.DataTable wsTable = OPR.WellSectionNameToWellTable(wlID, GetClientDBString);
                this.gvwSectionList.DataSource = wsTable;
                this.gvwSectionList.PageIndex = e.NewPageIndex;
                this.gvwSectionList.DataBind();
                this.gvwSectionList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSectionList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 wsID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                System.Data.DataTable runList = OPR.getRunForSelectedWellSectionTable(wlID, wsID, GetClientDBString);
                this.gvwRunList.DataSource = runList;
                this.gvwRunList.DataBind();
                this.gvwRunList.SelectedIndex = -1;
                this.gvwRunList.DataBind();
                this.gvwRunList.Visible = true;
                System.Data.DataTable secTable = RPT.getWellSectionTable(wlID, wsID, GetClientDBString);
                HttpContext.Current.Session["sectionTable"] = secTable;
                this.divOpr.Visible = false;
                this.divPad.Visible = false;
                this.divWell.Visible = false;
                this.divSec.Visible = false;
                this.divRun.Visible = true;
                GridViewRow oRow = this.gvwGlobalOptr.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                GridViewRow pRow = this.gvwWellPads.SelectedRow;
                String pName = Convert.ToString(Server.HtmlDecode(pRow.Cells[2].Text));
                GridViewRow wRow = this.gvwWellList.SelectedRow;
                String wName = Convert.ToString(Server.HtmlDecode(wRow.Cells[2].Text));
                GridViewRow sRow = this.gvwSectionList.SelectedRow;
                String sName = Convert.ToString(Server.HtmlDecode(sRow.Cells[2].Text));
                this.ancHead.InnerHtml = "<strong> " + oName + " > " + pName + " > " + wName + " > " + sName + "</strong>";
                this.liStep1.Attributes["class"] = "stepper-done";
                this.liStep2.Attributes["class"] = "stepper-done";
                this.liStep3.Attributes["class"] = "stepper-done";
                this.liStep4.Attributes["class"] = "stepper-done";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRunList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 wsID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                System.Data.DataTable runList = OPR.getRunForSelectedWellSectionTable(wlID, wsID, GetClientDBString);
                this.gvwRunList.DataSource = runList;
                this.gvwRunList.PageIndex = e.NewPageIndex;
                this.gvwRunList.DataBind();
                this.gvwRunList.Visible = true;
                this.gvwRunList.SelectedIndex = -1;
                this.gvwRunList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRunList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 pdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                Int32 wlID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 scID = Convert.ToInt32(this.gvwSectionList.SelectedValue);                
                Int32 rID = Convert.ToInt32(this.gvwRunList.SelectedValue);
                GridViewRow oRow = this.gvwGlobalOptr.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                GridViewRow pRow = this.gvwWellPads.SelectedRow;
                String pName = Convert.ToString(Server.HtmlDecode(pRow.Cells[2].Text));
                GridViewRow wRow = this.gvwWellList.SelectedRow;
                String wName = Convert.ToString(Server.HtmlDecode(wRow.Cells[2].Text));
                GridViewRow sRow = this.gvwSectionList.SelectedRow;
                String sName = Convert.ToString(Server.HtmlDecode(sRow.Cells[2].Text));                
                GridViewRow rRow = this.gvwRunList.SelectedRow;
                String rName = Convert.ToString(Server.HtmlDecode(rRow.Cells[2].Text));
                this.ancHead.InnerHtml = "<strong> " + oName + " > " + pName + " > " + wName + " > " + sName + " > " + rName + "</strong>";
                this.gvwRunList.Visible = false;
                this.divReport.Visible = true;
                this.rptComplete.Visible = false;
                this.rptSection.Visible = false;                
                this.rptRun.Visible = true;
                this.btnMetaCancel.Visible = false;
                this.btnReportDone.Visible = true;
                this.liStep5.Attributes["class"] = "stepper-done";                
                //Loading Page Report parameters to DataTables
                Int32 disclaimerID = RPT.getDisclaimerID("Report");
                System.Data.DataTable tblUser = new System.Data.DataTable();
                System.Data.DataTable tblDisclaimer = new System.Data.DataTable();
                System.Data.DataTable tblWellPad = (System.Data.DataTable)HttpContext.Current.Session["padInfoTable"];
                System.Data.DataTable tblWell = (System.Data.DataTable)HttpContext.Current.Session["wellInfoTable"];
                System.Data.DataTable tblRefMag = (System.Data.DataTable)HttpContext.Current.Session["refMagsTable"];
                System.Data.DataTable tblSection = (System.Data.DataTable)HttpContext.Current.Session["sectionTable"];
                System.Data.DataTable tblRun = new System.Data.DataTable();
                System.Data.DataTable tblSQC = new System.Data.DataTable();
                System.Data.DataTable tblResults = new System.Data.DataTable();
                tblUser = RPT.getUserInfo(LoginName, domain);
                tblDisclaimer = RPT.getDisclaimer(disclaimerID);
                tblRun = RPT.getRunTable(rID, GetClientDBString);
                tblResults = RPT.getQCResults(wlID, scID, rID, GetClientDBString);
                //Loading DataSources
                MSRpt usrInfo = new MSRpt("UserProfile", tblUser);
                MSRpt padInfo = new MSRpt("PadInfo", tblWellPad);
                MSRpt wellInfo = new MSRpt("WellInfo", tblWell);
                MSRpt rmInfo = new MSRpt("RefMagInfo", tblRefMag);
                MSRpt secInfo = new MSRpt("SectionInfo", tblSection);
                MSRpt runInfo = new MSRpt("RunInfo", tblRun);
                MSRpt resultsInfo = new MSRpt("QCResults", tblResults);
                MSRpt dscInfo = new MSRpt("RunDisclaimer", tblDisclaimer);
                //Loading datasources to local report
                this.rvwrRun.LocalReport.DataSources.Clear();
                this.rvwrRun.LocalReport.DataSources.Add(usrInfo);
                this.rvwrRun.LocalReport.DataSources.Add(padInfo);
                this.rvwrRun.LocalReport.DataSources.Add(wellInfo);
                this.rvwrRun.LocalReport.DataSources.Add(rmInfo);
                this.rvwrRun.LocalReport.DataSources.Add(secInfo);
                this.rvwrRun.LocalReport.DataSources.Add(runInfo);
                this.rvwrRun.LocalReport.DataSources.Add(resultsInfo);
                this.rvwrRun.LocalReport.DataSources.Add(dscInfo);
                this.rvwrRun.LocalReport.Refresh();
                this.divReport.Visible = true;
                this.rptRun.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnMetaCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.divOpr.Visible = true;
                this.divPad.Visible = false;
                this.divWell.Visible = false;
                this.divSec.Visible = false;
                this.divRun.Visible = false;
                this.divReport.Visible = false;
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientTypID);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = 0;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
                this.gvwWellList.DataSource = null;
                this.gvwWellList.DataBind();
                this.gvwSectionList.DataSource = null;
                this.gvwSectionList.DataBind();
                this.gvwRunList.DataSource = null;
                this.gvwRunList.DataBind();
                this.gvwRunList.SelectedIndex = -1;
                this.btnMetaCancel.Enabled = false;
                this.btnMetaCancel.CssClass = "btn btn-default text-center";
                this.ancHead.InnerHtml = "Operator Company";
                HttpContext.Current.Session["soInfoTable"] = null;
                HttpContext.Current.Session["padInfoTable"] = null;
                HttpContext.Current.Session["wellInfoTable"] = null;
                HttpContext.Current.Session["refMagsTable"] = null;
                HttpContext.Current.Session["sectionTable"] = null;
                this.liStep1.Attributes["class"] = "stepper-todo";
                this.liStep2.Attributes["class"] = "stepper-todo";
                this.liStep3.Attributes["class"] = "stepper-todo";
                this.liStep4.Attributes["class"] = "stepper-todo";
                this.liStep5.Attributes["class"] = "stepper-todo";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnReportDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnMetaCancel_Click(null, null);                
                this.rptComplete.Visible = false;
                this.rptSection.Visible = false;
                this.rptRun.Visible = false;                
                this.rvwrComplete.LocalReport.DataSources.Clear();
                this.rvwrComplete.LocalReport.Refresh();
                this.rvwrSection.LocalReport.DataSources.Clear();
                this.rvwrSection.LocalReport.Refresh();
                this.rvwrRun.LocalReport.DataSources.Clear();
                this.rvwrRun.LocalReport.Refresh();
                this.btnMetaCancel.Visible = true;
                this.btnMetaCancel.Enabled = false;
                this.btnMetaCancel.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rvwrComplete_Load(object sender, EventArgs e)
        {
            try
            {
                String exportOption = "PDF";

                RenderingExtension extension = rvwrComplete.LocalReport.ListRenderingExtensions().ToList().Find(x => x.Name.Equals(exportOption, StringComparison.CurrentCultureIgnoreCase));
                if (extension != null)
                {
                    System.Reflection.FieldInfo fInfo = extension.GetType().GetField("m_isVisible", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
                    fInfo.SetValue(extension, false);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rvwrSection_Load(object sender, EventArgs e)
        {
            try
            {
                String exportOption = "PDF";

                RenderingExtension extension = rvwrSection.LocalReport.ListRenderingExtensions().ToList().Find(x => x.Name.Equals(exportOption, StringComparison.CurrentCultureIgnoreCase));
                if (extension != null)
                {
                    System.Reflection.FieldInfo fInfo = extension.GetType().GetField("m_isVisible", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
                    fInfo.SetValue(extension, false);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rvwrRun_Load(object sender, EventArgs e)
        {
            try
            {
                String exportOption1 = "PDF", exportOption2 = "Word";

                RenderingExtension extension = rvwrRun.LocalReport.ListRenderingExtensions().ToList().Find(x => x.Name.Equals(exportOption1, StringComparison.CurrentCultureIgnoreCase));
                if (extension != null)
                {
                    System.Reflection.FieldInfo fInfo = extension.GetType().GetField("m_isVisible", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
                    fInfo.SetValue(extension, false);
                }
                extension = rvwrRun.LocalReport.ListRenderingExtensions().ToList().Find(x => x.Name.Equals(exportOption2, StringComparison.CurrentCultureIgnoreCase));
                if (extension != null)
                {
                    System.Reflection.FieldInfo fInfo = extension.GetType().GetField("m_isVisible", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
                    fInfo.SetValue(extension, false);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }                   
    }
}