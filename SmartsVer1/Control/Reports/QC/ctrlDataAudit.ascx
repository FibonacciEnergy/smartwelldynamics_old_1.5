﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlDataAudit.ascx.cs" Inherits="SmartsVer1.Control.Reports.QC.ctrlDataAudit" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" ></asp:ScriptManager>

<link href="../../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- JavaScripts -->
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<!-- Javascript for Footer -->
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>
<!--- Page local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }
    .swdMenu {
        color: #ffffff;
    }   
    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }
</style>
<asp:UpdateProgress ID="upgrsMeta" runat="server" AssociatedUpdatePanelID="upnlMeta" DisplayAfter="0">
    <ProgressTemplate>
        <div id="divDataAuditProgressbar" runat="server" class="updateProgress">
            <asp:Image ID="imgDataAuditProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<asp:UpdatePanel runat="server" ID="upnlMeta">
    <ContentTemplate>
        <!--- Page Menu --->
        <div class="row bg-black">
            <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom: 2px; padding-left: 2px; padding-top: 2px">
                <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
            </div>
            <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
                <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
            </div>
        </div>
        <div class="row bg-black">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
                <asp:LoginView ID='LoginView1' runat='server'>
                    <AnonymousTemplate>
                        <li style="vertical-align: middle; display: table-cell;">
                            <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                                ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                        </li>
                    </AnonymousTemplate>
                    <LoggedInTemplate>
                        <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">(
                            <asp:LoginName ID="LoginName1" runat="server" />
                            )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                        </li>
                    </LoggedInTemplate>
                </asp:LoginView>
            </div>
        </div>
        <!--- Page Breadcrumb --->
        <div class="row bg-white">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box-tools pull-right">
                    <ol class="breadcrumb">
                        <li><a href="../../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i>SMARTs Panel</a></li>
                        <li>Reports</li>
                        <li><a href="../../../Reports/QC/QCReport_2A.aspx">Audit/QC Reports</a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row bg-white">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel-group" runat="server" id="acrdRptMain">
                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                <h5 class="panel-title" style="text-align: left">
                                    <a id="ancAscMain" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlDataAudit_acrdRptMain" href="#BodyContent_ctrlDataAudit_divRptOuter">
                                        Audit / QC Reports</a>
                                </h5>
                            </div>
                            <div id="divRptOuter" runat="server">
                                <div class="panel-body">
                                    <div id="divProgress" class="panel panel-danger">
                                        <div class="panel-body">
                                            <ol id="olStepper" runat="server" class="stepper" data-stepper-steps="5">
                                                <li id="liStep1" runat="server" class="stepper-todo">Operator Company</li>
                                                <li id="liStep2" runat="server" class="stepper-todo">Well Pad</li>
                                                <li id="liStep3" runat="server" class="stepper-todo">Well/Lateral</li>
                                                <li id="liStep4" runat="server" class="stepper-todo">Section</li>
                                                <li id="liStep5" runat="server" class="stepper-todo">Run</li>
                                            </ol>
                                        </div>
                                    </div>
                                    <div class="panel panel-danger">
                                        <div class="panel-body">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group" style="width: 100%;">
                                                    <div class="box-tools">
                                                        <asp:LinkButton ID="btnMetaCancel" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnMetaCancel_Click"><i class="fa fa-refresh"> Refresh</i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="panel panel-info">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancHead" runat="server" class="disabled" href="#">Operator Company</a>
                                                        </h5>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div id="divOpr" runat="server">
                                                            <div class="table-responsive">
                                                                <asp:GridView runat="server" ID="gvwGlobalOptr" DataKeyNames="clntID" CssClass="mydatagrid" AllowPaging="True" AllowSorting="True"
                                                                    AutoGenerateColumns="False" EmptyDataText="No Operator Company found."
                                                                    OnPageIndexChanging="gvwGlobalOptr_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15"
                                                                    HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow"
                                                                    OnSelectedIndexChanged="gvwGlobalOptr_SelectedIndexChanged">
                                                                    <Columns>
                                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                            <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                        </asp:ButtonField>
                                                                        <asp:BoundField DataField="clntID" Visible="False" />
                                                                        <asp:BoundField DataField="clntName" HeaderText="Operator Company" />
                                                                        <asp:BoundField DataField="wpdCount" HeaderText="Global Well Pad Count">
                                                                            <ItemStyle CssClass="padRight" HorizontalAlign="Right" VerticalAlign="Middle" Font-Bold="True"></ItemStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="welCount" HeaderText="Global Well Count">
                                                                            <ItemStyle CssClass="padRight" HorizontalAlign="Right" VerticalAlign="Middle" Font-Bold="True"></ItemStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="welGlobal" HeaderText="SMARTs Well Count">
                                                                            <ItemStyle CssClass="padRight" HorizontalAlign="Right" VerticalAlign="Middle" Font-Bold="True"></ItemStyle>
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                        <div id="divPad" runat="server">
                                                            <div class="table-responsive">
                                                                <asp:GridView ID="gvwWellPads" runat="server" EmptyDataText="No Well Pad(s) found for selected Operator"
                                                                    ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="wpdID"
                                                                    AllowPaging="True" CssClass="mydatagrid" OnPageIndexChanging="gvwWellPads_PageIndexChanging" OnSelectedIndexChanged="gvwWellPads_SelectedIndexChanged"
                                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                    RowStyle-CssClass="rows">
                                                                    <Columns>
                                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                            <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                        </asp:ButtonField>
                                                                        <asp:BoundField DataField="wpdID" HeaderText="wpd" ReadOnly="True" Visible="false" />
                                                                        <asp:BoundField DataField="welPadName" HeaderText="Well Pad" />
                                                                        <asp:BoundField DataField="field" HeaderText="O&G Field" />
                                                                        <asp:BoundField DataField="county" HeaderText="County/District/Division/Municipality" />
                                                                        <asp:BoundField DataField="state" HeaderText="State/Province" />
                                                                        <asp:BoundField DataField="country" HeaderText="Country/Nation" />
                                                                        <asp:BoundField DataField="sreg" HeaderText="Sub-Region" />
                                                                        <asp:BoundField DataField="reg" HeaderText="Major Region" />
                                                                        <asp:BoundField DataField="swlCount" HeaderText="SMARTs Well Count">
                                                                            <ItemStyle CssClass="padRight" HorizontalAlign="Right" VerticalAlign="Middle" Font-Bold="True"></ItemStyle>
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                        <div id="divWell" runat="server">
                                                            <div class="table-responsive">
                                                                <asp:GridView ID="gvwWellList" runat="server" CssClass="mydatagrid" EmptyDataText="No Wells associated with the selected Service Order #"
                                                                    AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="welID" OnSelectedIndexChanged="gvwWellList_SelectedIndexChanged"
                                                                    ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwWellList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                    PageSize="15" PagerSettings-Mode="Numeric" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Center" PagerStyle-VerticalAlign="Middle"
                                                                    HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow"
                                                                    OnRowDataBound="gvwWellList_RowDataBound">
                                                                    <Columns>
                                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                            <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                        </asp:ButtonField>
                                                                        <asp:BoundField DataField="welID" Visible="false" />
                                                                        <asp:BoundField DataField="welName" HeaderText="Well / Lateral" />
                                                                        <asp:BoundField DataField="field" HeaderText="Oil & Gas Field" />
                                                                        <asp:BoundField DataField="county" HeaderText="County / District" />
                                                                        <asp:BoundField DataField="state" HeaderText="State / Province" />
                                                                        <asp:BoundField DataField="spud" HeaderText="Spud Date" />
                                                                        <asp:BoundField DataField="wstID" HeaderText="Status" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                    <div runat="server" id="wellEnclosure" visible="false">
                                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                        <h4><i runat="server" id="iWell"><span runat="server" id="spnWell"></span></i></h4>
                                                                        <asp:Label ID="lblWell" runat="server" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="divSec" runat="server">
                                                            <div class="table-responsive">
                                                                <asp:GridView ID="gvwSectionList" runat="server" CssClass="mydatagrid" EmptyDataText="No Well/Lateral Section associated with the selected Well"
                                                                    AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="wswID" OnSelectedIndexChanged="gvwSectionList_SelectedIndexChanged"
                                                                    ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwSectionList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                    PageSize="15" PagerSettings-Mode="Numeric" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Center" PagerStyle-VerticalAlign="Middle"
                                                                    HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                                                    <Columns>
                                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                            <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                        </asp:ButtonField>
                                                                        <asp:BoundField DataField="wswID" Visible="false" />
                                                                        <asp:BoundField DataField="wsName" HeaderText="Well/Lateral Section Name" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                                        <asp:BoundField DataField="rnCount" HeaderText="Run Count" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                        <div id="divRun" runat="server">
                                                            <div class="table-responsive">
                                                                <asp:GridView ID="gvwRunList" runat="server" CssClass="mydatagrid" EmptyDataText="No Run associated with the selected Section"
                                                                    AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="runID" OnSelectedIndexChanged="gvwRunList_SelectedIndexChanged"
                                                                    ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwRunList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                    PageSize="15" PagerSettings-Mode="Numeric" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Center" PagerStyle-VerticalAlign="Middle"
                                                                    HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                                                    <Columns>
                                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                            <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                        </asp:ButtonField>
                                                                        <asp:BoundField DataField="runID" Visible="false" />
                                                                        <asp:BoundField DataField="rnName" HeaderText="Run Name" />
                                                                        <asp:BoundField DataField="rnStatus" HeaderText="Status" />
                                                                        <asp:BoundField DataField="rnBHA" HeaderText="BHA Signature ID" />
                                                                        <asp:BoundField DataField="rnToolcode" HeaderText="Toolcode" />
                                                                        <asp:BoundField DataField="rnSQC" HeaderText="Survey Qualification Criteria" />
                                                                        <asp:BoundField DataField="rnStartD" HeaderText="Start Measured Depth(m)" ItemStyle-HorizontalAlign="Right" />
                                                                        <asp:BoundField DataField="rnEndD" HeaderText="End Measured Depth(m)" ItemStyle-HorizontalAlign="Right" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                    <div runat="server" id="runEnclosure" visible="false">
                                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                        <h4><i runat="server" id="iRun"><span runat="server" id="spnRun"></span></i></h4>
                                                                        <asp:Label ID="lblRun" runat="server" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divReport" runat="server" visible="false">
                                                    <div class="panel panel-success">
                                                        <div class="panel-heading">
                                                            <h5 class="panel-title" style="text-align: left">
                                                                <a id="ancQCReport" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlDataAudit_acrdQC" href="#BodyContent_ctrlDataAudit_divRpt">
                                                                    Audit / QC Report</a>
                                                            </h5>
                                                        </div>

                                                        <div class="panel-body">
                                                            <div id="rptComplete" runat="server" visible="false">
                                                                <rsweb:ReportViewer ID="rvwrComplete" runat="server" Font-Names="Verdana" Font-Size="8pt" Height=""
                                                                    WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" SizeToReportContent="True"
                                                                    ShowExportControls="True" ShowFindControls="False" ShowZoomControl="False" ShowRefreshButton="False"
                                                                    DocumentMapWidth="100%" OnLoad="rvwrComplete_Load">
                                                                    <LocalReport ReportPath="Control\Reports\QC\WellDataInput.rdlc"></LocalReport>
                                                                </rsweb:ReportViewer>
                                                            </div>
                                                            <div id="rptSection" runat="server" visible="false">
                                                                <rsweb:ReportViewer ID="rvwrSection" runat="server" Font-Names="Verdana" Font-Size="8pt" Height=""
                                                                    WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" SizeToReportContent="True"
                                                                    ShowExportControls="True" ShowFindControls="False" ShowZoomControl="False" ShowRefreshButton="False"
                                                                    DocumentMapWidth="100%" OnLoad="rvwrSection_Load">
                                                                    <LocalReport ReportPath="Control\Reports\QC\SectionDataInput.rdlc"></LocalReport>
                                                                </rsweb:ReportViewer>
                                                            </div>
                                                            <div id="rptRun" runat="server" visible="false">
                                                                <rsweb:ReportViewer ID="rvwrRun" runat="server" Font-Names="Verdana" Font-Size="8pt" Height=""
                                                                    WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" SizeToReportContent="True"
                                                                    ShowExportControls="True" ShowFindControls="False" ShowZoomControl="False" ShowRefreshButton="False"
                                                                    DocumentMapWidth="100%" OnLoad="rvwrRun_Load">
                                                                    <LocalReport ReportPath="Control\Reports\QC\RunDataAudit.rdlc" EnableHyperlinks="true"></LocalReport>
                                                                </rsweb:ReportViewer>
                                                            </div>
                                                        </div>
                                                        <div>&nbsp;</div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="input-group" style="width: 100%;">
                                                                <asp:LinkButton ID="btnReportDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnReportDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="gvwRunList" />
        <asp:PostBackTrigger ControlID="btnReportDone" />
    </Triggers>
</asp:UpdatePanel>
