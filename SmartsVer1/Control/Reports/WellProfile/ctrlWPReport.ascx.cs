﻿using System;
using log4net;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Net.Mail;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using QC = SmartsVer1.QCHelpers.sdRawDataQC;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using CHRT = SmartsVer1.Helpers.ChartHelpers.Gauges;
using WPN = SmartsVer1.Helpers.QCHelpers.WellPlan;
using SRV = SmartsVer1.Helpers.DemogHelpers.ServicesConfig;
using CLR = System.Drawing.Color;

namespace SmartsVer1.Control.Reports.WellProfile
{
    public partial class ctrlWPReport : System.Web.UI.UserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ctrlWPReport));
        String LoginName, username, domain, GetClientDBString;
        const Int32 ClientType = 4, siloWSt = 2, globeWSt = 1, serviceGroupID = 2;
        Int32 ClientID = -99, qcjCount = 0, actCount = 0, ifrCount = 0, ttljCount = 0, grdCount = 0, truCount = 0;
        Int32 pdCount = 0, gwCount = 0, awCount = 0, opCount = 0, ascCount = 0, msngCount = 0, planCount = 0;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}