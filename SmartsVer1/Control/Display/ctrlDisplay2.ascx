﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlDisplay2.ascx.cs" Inherits="SmartsVer1.Control.Display.ctrlDisplay2" %>

<!--- Stylesheets --->
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<!--- Script Libraries --->
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<script src="../../Scripts/jquery.slimscroll.min.js"></script>
<!--- Local Style --->
<style type="text/css">
    a {
    color: #ffffff;
}

    a:hover, a:active, a:focus {
        outline: none;
        text-decoration: none;
        color: white;
    }
</style>

<div class="row bg-white" style="background-image: url('../../../Images/OilRig_3D.png'); opacity: .98; background-repeat: no-repeat; background-color: white; background-size:100% 100%; width: 100%;">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding: 5px;" >
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>

    </div>
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="padding:15px">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Large" Font-Names="Segoe UI"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none">
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' />
                </li>
            </LoggedInTemplate>
        </asp:LoginView>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="container" style="clear: both; margin-left: 15px">
                <div class="row align-items-center justify-content-center text-center">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 align ">
                        <br />
                        <br />
                        <h1 class="text-uppercase font-weight-bold" style="font-family: Cambria; color: white; text-align: left">Survey Management and
                            Audit in Real-Time (SMART)</h1>
                        <hr class="divider">
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 align-self-baseline">
                        <p style="font-size: x-large; font-family: Arial; text-align: left; color: white">
                            SMARTs Survey Management and Audit in Real-Time Services platform can help manage people, 
                        assets and opertions in Real-Time. Our stream-lined services help you and your team to make better decisions - faster and
                        enable you to have full visibility into your operations.
                        </p>
                        <br />
                        <p style="font-size: x-large; font-family: Arial; text-align: left; color: white">
                            SMARTs platform comprises a suite of Real-Time Data Audit Services and SMART Utilities that help
                        in maintaining continous operations, increase operations safety while reducing costs. The platform operates in the cloud
                        and as such
                        is highly scalable - remaining cost efficient at the same time
                        </p>
                        <br />
                        <a class="btn btn-danger btn-xl js-scroll-trigger" href="#services">Find Out More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="divCarousel" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" runat="server" style="background-color: white; margin-left: 5px;
    margin-right: 5px">
    <div id="cslSWD" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#cslSWD" data-slide-to="0" class="active"></li>
            <li data-target="#cslSWD" data-slide-to="1" class=""></li>
            <li data-target="#cslSWD" data-slide-to="2" class=""></li>
            <li data-target="#cslSWD" data-slide-to="3" class=""></li>
        </ol>
        <div class="carousel-inner">
            <div class="item active">
                <img src="../../Images/1.png" alt="SMARTs Data Audit/QC">
            </div>
            <div class="item">
                <img src="../../Images/2.png" alt="Well Logs Management & Printing">
            </div>
            <div class="item">
                <img src="../../Images/3.png" alt="Magnetic Storm Survey Rectifier (MSSR)">
            </div>
            <div class="item">
                <img src="../../Images/4.png" alt="Non-Magnetic Space Calculator">
            </div>
        </div>
        <a class="left carousel-control" href="#cslSWD" data-slide="prev" style="color: #CC3300">
            <span class="fa fa-4x fa-arrow-circle-left" style="left: 15px"></span>
        </a>
        <a class="right carousel-control" href="#cslSWD" data-slide="next" style="color: #CC3300">
            <span class="fa fa-4x fa-arrow-circle-right" style="right: 15px"></span>
        </a>
    </div>
</div>
<div id="divServices" runat="server" style="background-color: white;">
    <section class="page-section" id="services">
        <div class="container">
            <h2 class="text-center">SMARTs At Your Service</h2>
            <hr class="divider">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-center">
                    <i class="fa fa-diamond fa-4x text-danger"></i>
                    <h3 class="h4 mb-2">Real-Time Survey Audit/QC</h3>
                    <p class="text-muted mb-0">
                        Data Audit/Quality Control is used to highlight good practices and implement them company wide, thus
                        increasing operational efficiencies, reducing costs and increasing service safety and highlighting slowly deteriorating
                        conditions - contributing to continual improvements.
                    </p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-center">
                    <i class="fa fa-compass fa-4x text-danger"></i>
                    <h3 class="h4 mb-2">Multi-Station Analysis (MSA)</h3>
                    <p class="text-muted mb-0">
                        Multi-Station Analysis (MSA) is the technique to identify & reduce magnetic interference effects
                        of BHA thereby improving accuracy of MWD Surveys. MSA utilizes multiply MWD surveys to calculate magnetic biases & scaling
                        on tool axes to ensure survey data is in agreement with expected back-ground field. MSA then applies these calculations
                        as corrections to each individual survey
                    </p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-center">
                    <i class="fa fa-globe fa-4x text-danger"></i>
                    <h3 class="h4 mb-2">Well Logs Management & Print</h3>
                    <p class="text-muted mb-0">
                        Deploy a user friendly, role-based, secure access interface - enabling Real-Time data uploads from
                        field, Real-Time Log Analysis and Real-Time Monitoring & Tracking of Well log Audit, Printing & Submission process. SMARTs
                        Well Log Management & Printing Service is capable of printing and submitting multiple paper and electronic copies to user
                        identified entities.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">
                        <div class="mt-5">
                            <i class="fa fa-sun-o fa-4x text-danger"></i>
                            <h3 class="h4 mb-2">Magnetic Storm Survey Rectifier</h3>
                            <p class="text-muted mb-0">
                                A Geomagnetic Storm is a temporary disturbance casused by Solar winds and/or cloud of magnetic field.
                            SMARTs Mag-Storm Survey Rectifier (MSSR) is a passive solution that allows subterranean exploration by removing effects
                            of Solar Storms, so that drilling activities can continue during an active storm - reducing operational costs and increasing
                                safety
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">
                        <div class="mt-5">
                            <i class="fa fa-calculator fa-4x text-danger"></i>
                            <h3 class="h4 mb-2">BHA Non-Magnetic Space Calculator</h3>
                            <p class="text-muted mb-0">
                                By adding proper non-mag collars above and below, a balance is achieved by keeping sensors away from
                                    highly magnetized components. SMARTs BHA Calculator is a utility that helps to identify the correct signature to be
                                used
                                    for a defined Run - before drilling operations start
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Call to Action Section -->
<div runat="server" id="divDemo" style="background-color: white;">
    <section class="page-section bg-white">
        <div class="container text-center">
            <h2>Gives SMARTs a Try</h2>
            <a class="btn btn-danger btn-xl" href="../../Support/ContactUs2.aspx">Request Demo!</a>
        </div>
        <br />
    </section>
</div>
<!-- Contact Section -->
<div runat="server" id="divContact" style="background-color: white;">
    <section class="page-section bg-danger" id="contact">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row justify-content-center">
                <div class="text-center">
                    <h2 class="text-gray">Let's Get In Touch!</h2>
                    <hr class="divider ">
                    <p class="text-gray">
                        Ready to drill your next Well with SMARTs? Give us a call or send us an email and we will get back
                            to you ASAP!
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md -offset-2 col-lg-offset-2">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="text-center text-gray">
                            <i class="fa fa-phone fa-3x"></i>
                            <p>+1 (469) 251-2486</p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="text-center text-gray">
                            <i class="fa fa-envelope text-gray fa-3x"></i>
                            <!-- Make sure to change the email address in anchor text AND the link below! -->
                            <p><a href="mailto:info@smartwelldynamics.com" class="text-gray">info@smartwelldynamics.com</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>