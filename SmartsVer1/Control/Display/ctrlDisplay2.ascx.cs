﻿using System;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;

namespace SmartsVer1.Control.Display
{
    public partial class ctrlDisplay2 : System.Web.UI.UserControl
    {        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //this.pageMenuSource.SiteMapProvider = "default";
                this.ltrMenuLiteral.Text = MNL.getDefaultMenu();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Account/SMARTAccess2.aspx", false);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}