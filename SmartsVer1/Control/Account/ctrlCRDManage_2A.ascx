﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlCRDManage_2A.ascx.cs" Inherits="SmartsVer1.Control.Account.ctrlCRDManage_2A" %>

<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" ></asp:ScriptManager>
<!--  Stylesheet -->
<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript -->
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>

<!--- Page local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }
    .swdMenu {
        color: #ffffff;
    }
    .spnLabel {
        min-width: 125px;
        text-align: right;
    }
</style>
<!-- Javascript for Footer -->
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>
<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<!--- Login --->
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">        
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>Configuration</li>
                <li><a href="../../Account/fecCRDManage_2A.aspx"> Account Profile</a></li>
            </ol>
        </div>
    </div>    
</div>
<!-- Page Content -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul class="nav nav-pills nav-justified">
                <li class="active"><a data-toggle="pill" href="#pswd">Account Password</a></li>
                <li><a data-toggle="pill" href="#name">Full Name</a></li>
                <li><a data-toggle="pill" href="#ph">Phone</a></li>
                <li><a data-toggle="pill" href="#eml">Email</a></li>
                <li><a data-toggle="pill" href="#add">Address</a></li>
            </ul>
            <div class="tab-content">
                <div id="pswd" class="tab-pane fade in active">
                    <asp:UpdateProgress ID="uprgrsPassword" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlPassword">
                        <ProgressTemplate>
                            <div id="divPasswordProcessing" runat="server" class="updateProgress">
                                <asp:Image ID="imgPasswordProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlPassword">
                        <ContentTemplate>
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <h5 class="panel-title"><span>Account Password</span></h5>
                                </div>
                                <div class="panel-body">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="min-height: 400px;">
                                        <asp:ChangePassword ID="chgPwrd" runat="server" ContinueButtonStyle-CssClass="btn btn-success text-center"
                                            Font-Names="Segoi UI" ForeColor="Gray" Font-Size="X-Large" OnContinueButtonClick="chgPwrd_ContinueButtonClick"
                                            Width="100%">
                                            <CancelButtonStyle CssClass="btn btn-warning text-center" />
                                            <ChangePasswordButtonStyle CssClass="btn btn-primary text-center" />
                                            <ChangePasswordTemplate>
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="CurrentPassword" runat="server" CssClass="form-control" TextMode="Password"
                                                                placeholder="Current Password"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword"
                                                                ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="ctl00$chgPwrd">*</asp:RequiredFieldValidator>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="NewPassword" runat="server" CssClass="form-control" TextMode="Password"
                                                                placeholder="New Password"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword"
                                                                ErrorMessage="New Password is required." ToolTip="New Password is required." ValidationGroup="ctl00$chgPwrd">*</asp:RequiredFieldValidator>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="ConfirmNewPassword" runat="server" CssClass="form-control" TextMode="Password"
                                                                placeholder="Confirm New Password"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword"
                                                                ErrorMessage="Confirm New Password is required." ToolTip="Confirm New Password is required."
                                                                ValidationGroup="ctl00$chgPwrd">*</asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center;" colspan="3">
                                                            <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword"
                                                                ControlToValidate="ConfirmNewPassword" Display="Dynamic" CssClass="form-control"
                                                                ErrorMessage="The Confirm New Password must match the New Password entry." ValidationGroup="ctl00$chgPwrd"></asp:CompareValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center; color: red;" colspan="3">
                                                            <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <asp:LinkButton ID="ChangePasswordPushButton" runat="server" CommandName="ChangePassword" CssClass="btn btn-success text-center"
                                                                ValidationGroup="ctl00$chgPwrd"><i class="fa fa-thumbs-up"> Change Password</i></asp:LinkButton>
                                                            <asp:LinkButton ID="CancelPushButton" runat="server" CommandName="Cancel" CssClass="btn btn-warning text-center"><i class="fa fa-thumbs-down"> Cancel</i></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ChangePasswordTemplate>
                                            <ContinueButtonStyle CssClass="btn btn-success text-center" Width="45%" />
                                            <SuccessTemplate>
                                                <table>
                                                    <tr>
                                                        <td colspan="2" style="text-align: center; color: green;">Change Password Complete</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center; text-decoration: underline; color: green;">Your password has been changed!</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center" colspan="2">
                                                            <asp:LinkButton ID="ContinuePushButton" runat="server" CommandName="Continue" CssClass="btn btn-success text-center"><i class="fa fa-check-circle">Continue</i></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </SuccessTemplate>
                                            <TextBoxStyle CssClass="form-control" />
                                        </asp:ChangePassword>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div id="name" class="tab-pane fade">
                    <asp:UpdateProgress ID="uprgrsName" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlFullName">
                        <ProgressTemplate>
                            <div id="divNameProcessing" runat="server" class="updateProgress">
                                <asp:Image ID="imgNameProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlFullName">
                        <ContentTemplate>
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <h5 class="panel-title"><span>Full Name and Biographic Info.</span></h5>
                                </div>
                                <div class="panel-body">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="min-height: 400px;">
                                        <div class="input-group" style="width: 100%;">
                                            <asp:LinkButton runat="server" ID="btnUpdatePersonal" CssClass="btn btn-info" OnClick="btnUpdatePersonal_Click"><i class="fa fa-edit"> Full Name</i></asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="btnUpdateNameCancel" CssClass="btn btn-default" Enabled="false" OnClick="btnUpdateNameCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                        </div>
                                        <div id="divName" runat="server">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <asp:Label ID="lblFName" runat="server" Text="First Name" CssClass="text-bold text-olive" />
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <asp:Label ID="lblMName" runat="server" Text="Middle Name" CssClass="text-bold text-olive" />
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <asp:Label ID="lblLName" runat="server" Text="Last Name" CssClass="text-bold text-olive" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <asp:TextBox ID="txtProfileFName" runat="server" CssClass="form-control" Enabled="false" />
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <asp:TextBox ID="txtProfileMName" runat="server" CssClass="form-control" Enabled="false" />
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <asp:TextBox ID="txtProfileLName" runat="server" CssClass="form-control" Enabled="false" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <asp:Label ID="lblGender" runat="server" Text="Gender" CssClass="text-bold text-olive" />
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <asp:Label ID="lblRole" runat="server" Text="Assigned User Role" CssClass="text-bold text-olive" />
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <asp:Label ID="lblUsrNm" runat="server" Text="Current Username" CssClass="text-bold text-olive" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <asp:TextBox ID="txtGender" runat="server" CssClass="form-control" Enabled="false" />
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <asp:TextBox ID="txtRole" runat="server" CssClass="form-control" Enabled="false" />
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <asp:TextBox ID="txtUserName" runat="server" CssClass="form-control" Enabled="false" />
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divNameEdit" runat="server" visible="false">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue">First Name</span>
                                                        <asp:TextBox ID="txtFName" runat="server" CssClass="form-control" placeholder="First Name (required)" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue">Middle Name</span>
                                                        <asp:TextBox ID="txtMName" runat="server" CssClass="form-control" placeholder="Middle Name (optional)" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue">Last Name</span>
                                                        <asp:TextBox ID="txtLName" runat="server" CssClass="form-control" placeholder="Last Name (required)" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue">Gender</span>
                                                        <asp:DropDownList ID="ddlUpdGender" runat="server" AutoPostBack="True" CssClass="form-control" OnSelectedIndexChanged="ddlUpdGender_SelectedIndexChanged">
                                                            <asp:ListItem Value="0" Text="--- Select Gender ---" />
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4"></div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div runat="server" id="nameEnclosure" visible="false">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <h4><i runat="server" id="iName"><span runat="server" id="spnName"></span></i></h4>
                                                        <asp:Label ID="lblProfileInfo" runat="server" Text="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="input-group" style="width: 100%;">
                                                        <asp:LinkButton ID="btnConfirmProfileUpdate" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnConfirmProfileUpdate_Click"><i class="fa fa-thumbs-up"> Full Name</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnClearProfileUpdate" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnClearProfileUpdate_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancelProfile" runat="server" CssClass="btn btn-danger text-center" OnClick="btnCancelProfile_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div id="ph" class="tab-pane fade">
                    <asp:UpdateProgress ID="uprgrsPhone" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlPhone">
                        <ProgressTemplate>
                            <div id="divPhoneProcessing" runat="server" class="updateProgress">
                                <asp:Image ID="imgPhoneProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlPhone">
                        <ContentTemplate>
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <h5 class="panel-title"><span>Phone Number</span></h5>
                                </div>
                                <div class="panel-body">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="min-height: 400px;">
                                        <div id="divPhoneList" runat="server">
                                            <div class="input-group" style="width: 100%;">
                                                <asp:LinkButton runat="server" ID="btnPhoneUpdate" CssClass="btn btn-info text-center" OnClick="btnPhoneUpdate_Click"><i class="fa fa-plus"> Phone Number</i></asp:LinkButton>
                                                <asp:LinkButton runat="server" ID="btnPhoneReset" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnPhoneReset_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                            </div>
                                            <asp:TextBox ID="txtPhone" runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                        <div id="divPhoneEdit" runat="server" visible="false">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-light-blue">Phone Type</span>
                                                <asp:DropDownList ID="ddlPhoneType" runat="server" AutoPostBack="True" CssClass="form-control" OnSelectedIndexChanged="ddlPhoneType_SelectedIndexChanged">
                                                    <asp:ListItem Value="0" Text="--- Select Phone Type ---" />
                                                </asp:DropDownList>
                                            </div>
                                            <asp:TextBox ID="txtUPDPhoneArea" runat="server" Text="" CssClass="form-control" TextMode="Number" placeholder="Area Code" />
                                            <asp:TextBox ID="txtUPDPhonePrefix" runat="server" Text="" CssClass="form-control" TextMode="Number" placeholder="Prefix" />
                                            <asp:TextBox ID="txtUPDPhoneNumber" runat="server" Text="" CssClass="form-control" TextMode="Number" placeholder="Phone Number" />
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div runat="server" id="phEnclosure" visible="false">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <h4><i runat="server" id="iPhone"><span runat="server" id="spnPhone"></span></i></h4>
                                                        <asp:Label ID="lblPhoneSuccess" runat="server" Text="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="input-group" style="width: 100%;">
                                                        <asp:LinkButton runat="server" ID="btnPhoneAdd" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnPhoneAdd_Click"><i class="fa fa-plus"> Phone Number</i></asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="btnPhoneAddClear" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnPhoneAddClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="btnPhoneDone" CssClass="btn btn-danger text-center" OnClick="btnPhoneDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div id="eml" class="tab-pane fade">
                    <asp:UpdateProgress ID="uprgrsEmail" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlEmail">
                        <ProgressTemplate>
                            <div id="divEmailProcessing" runat="server" class="updateProgress">
                                <asp:Image ID="imgEmailProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlEmail">
                        <ContentTemplate>
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <h5 class="panel-title"><span>Email</span></h5>
                                </div>
                                <div class="panel-body">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="min-height: 400px;">
                                        <div id="divEmailList" runat="server">
                                            <div class="input-group" style="width: 100%;">
                                                <asp:LinkButton runat="server" ID="btnUpdateEmail" CssClass="btn btn-info text-center" OnClick="btnUpdateEmail_Click"><i class="fa fa-edit"> Email</i></asp:LinkButton>
                                                <asp:LinkButton runat="server" ID="btnUpdateEmailReset" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnUpdateEmailReset_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                            </div>
                                            <asp:TextBox ID="txtProfileEmail" runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                        <div id="divEmailAdd" runat="server" visible="false">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <asp:TextBox ID="txtUPDProfileEmail" runat="server" CssClass="form-control" placeholder="Email Address" TextMode="Email"
                                                        Text="" />
                                                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtUPDProfileEmail" Display="Dynamic" ErrorMessage="*"
                                                        SetFocusOnError="true" ForeColor="Maroon" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div runat="server" id="emlEnclosure" visible="false">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <h4><i runat="server" id="iEmail"><span runat="server" id="spnEmail"></span></i></h4>
                                                        <asp:Label ID="lblEmailSuccess" runat="server" Text="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="input-group" style="width: 100%;">
                                                        <asp:LinkButton runat="server" ID="btnDoUpdateEmail" CssClass="btn btn-success text-center" OnClick="btnDoUpdateEmail_Click"><i class="fa fa-edit"> Email</i></asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="btnEmailReset" CssClass="btn btn-warning text-center" OnClick="btnEmailReset_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="btnEmailDone" CssClass="btn btn-danger text-center" OnClick="btnEmailDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div id="add" class="tab-pane fade">
                    <asp:UpdateProgress ID="upgrsAddress" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlAddress">
                        <ProgressTemplate>
                            <div id="divAddressProcessing" runat="server" class="updateProgress">
                                <asp:Image ID="imgAddressProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlAddress">
                        <ContentTemplate>
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <h5 class="panel-title"><span>Address</span></h5>
                                </div>
                                <div class="panel-body">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="min-height: 400px;">
                                        <div id="divAddList" runat="server">
                                            <div class="input-group" style="width: 100%;">
                                                <asp:LinkButton runat="server" ID="btnProfileAddress" CssClass="btn btn-info" OnClick="btnProfileAddress_Click"><i class="fa fa-edit"> Profile Address</i></asp:LinkButton>
                                                <asp:LinkButton runat="server" ID="btnProfileAddressReset" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnProfileAddressReset_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Add. Type</span>
                                                        <asp:TextBox ID="txtPType" runat="server" CssClass="form-control" Enabled="false" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Street 1</span>
                                                        <asp:TextBox ID="txtPStreet1" runat="server" CssClass="form-control" Enabled="false" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Street 2</span>
                                                        <asp:TextBox ID="txtPStreet2" runat="server" CssClass="form-control" Enabled="false" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Street 3</span>
                                                        <asp:TextBox ID="txtPStreet3" runat="server" CssClass="form-control" Enabled="false" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">City</span>
                                                        <asp:TextBox ID="txtPCityName" runat="server" CssClass="form-control" Enabled="false" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Zip/Postal Code</span>
                                                        <asp:TextBox ID="txtPZip" runat="server" CssClass="form-control" Enabled="false" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">County</span>
                                                        <asp:TextBox ID="txtPCounty" runat="server" CssClass="form-control" Enabled="false" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">State</span>
                                                        <asp:TextBox ID="txtPState" runat="server" CssClass="form-control" Enabled="false" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Country</span>
                                                        <asp:TextBox ID="txtPCountry" runat="server" CssClass="form-control" Enabled="false" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Sub-Region</span>
                                                        <asp:TextBox ID="txtPSubRegion" runat="server" CssClass="form-control" Enabled="false" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Region</span>
                                                        <asp:TextBox ID="txtPRegion" runat="server" CssClass="form-control" Enabled="false" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divAddEdit" runat="server" visible="false">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Geo-Region</span>
                                                        <asp:DropDownList ID="ddlProfileReg" runat="server" CssClass="form-control" AutoPostBack="True"
                                                            AppendDataBoundItems="True" OnSelectedIndexChanged="ddlProfileReg_SelectedIndexChanged">
                                                            <asp:ListItem Value="0" Text="--- Select Major Geographic Region ---"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Sub-Region</span>
                                                        <asp:DropDownList ID="ddlProfileSReg" runat="server" CssClass="form-control" AutoPostBack="True"
                                                            AppendDataBoundItems="True" OnSelectedIndexChanged="ddlProfileSReg_SelectedIndexChanged" Enabled="false">
                                                            <asp:ListItem Value="0" Text="--- Select Geographic Sub-Region ---"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Country/Nation</span>
                                                        <asp:DropDownList ID="ddlProfileCountry" runat="server" CssClass="form-control" AutoPostBack="True"
                                                            AppendDataBoundItems="True" OnSelectedIndexChanged="ddlProfileCountry_SelectedIndexChanged" Enabled="false">
                                                            <asp:ListItem Value="0" Text="--- Select Country/Nation ---"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">State/Province</span>
                                                        <asp:DropDownList ID="ddlProfileState" runat="server" CssClass="form-control" AutoPostBack="True"
                                                            AppendDataBoundItems="True" OnSelectedIndexChanged="ddlProfileState_SelectedIndexChanged" Enabled="false">
                                                            <asp:ListItem Value="0" Text="--- Select State/Province ---"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">County/District</span>
                                                        <asp:DropDownList ID="ddlProfileCounty" runat="server" CssClass="form-control" Enabled="False" AutoPostBack="True"
                                                            AppendDataBoundItems="True" OnSelectedIndexChanged="ddlProfileCounty_SelectedIndexChanged">
                                                            <asp:ListItem Value="0" Text="--- Select County/District ---"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">City/Town</span>
                                                        <asp:DropDownList ID="ddlProfileCity" runat="server" CssClass="form-control" Enabled="False" AutoPostBack="True"
                                                            AppendDataBoundItems="True" OnSelectedIndexChanged="ddlProfileCity_SelectedIndexChanged">
                                                            <asp:ListItem Value="0" Text="--- Select City/Town ---"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Zip/Postal Code</span>
                                                        <asp:DropDownList ID="ddlProfileZip" runat="server" CssClass="form-control" Enabled="False" AutoPostBack="True"
                                                            AppendDataBoundItems="True" OnSelectedIndexChanged="ddlProfileZip_SelectedIndexChanged">
                                                            <asp:ListItem Value="0" Text="--- Select Zip/Postal Code ---"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Street 1</span>
                                                        <asp:TextBox ID="txtSt1" runat="server" Text="" CssClass="form-control" Enabled="false" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Street 2</span>
                                                        <asp:TextBox ID="txtSt2" runat="server" Text="" CssClass="form-control" Enabled="false" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Street 3</span>
                                                        <asp:TextBox ID="txtSt3" runat="server" Text="" CssClass="form-control" Enabled="false" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Address Type</span>
                                                        <asp:DropDownList ID="ddlAddressType" runat="server" CssClass="form-control" AutoPostBack="True"
                                                            OnSelectedIndexChanged="ddlAddressType_SelectedIndexChanged" AppendDataBoundItems="True" Enabled="false">
                                                            <asp:ListItem Value="0" Text="--- Select Address Type ---"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div runat="server" id="addEnclosure" visible="false">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <h4><i runat="server" id="iAddress"><span runat="server" id="spnAddress"></span></i></h4>
                                                        <asp:Label ID="lblAddressInfo" runat="server" Text="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="input-group" style="width: 100%;">
                                                        <asp:LinkButton ID="btnAddressUpdate" runat="server" CssClass="btn btn-default" Enabled="false" OnClick="btnAddressUpdate_Click"><i class="fa fa-edit"> Profile Address</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnAddressClear" runat="server" CssClass="btn btn-default" Enabled="false" OnClick="btnAddressClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnAddressCancel" runat="server" CssClass="btn btn-danger" OnClick="btnAddressCancel_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</div>