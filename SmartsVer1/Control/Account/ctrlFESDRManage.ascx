﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlFESDRManage.ascx.cs" Inherits="SmartsVer1.Control.Account.ctrlFESDRManage" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript -->
<script src="../../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>

<!--- Page local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }

    .rotate {
        -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
        -moz-transform: rotate(90deg); /* Firefox */
        -ms-transform: rotate(90deg); /* IE 9 */
        transform: rotate(90deg); /* Standard syntax */
    }

    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper[data-stepper-steps="8"] li {
            width: 12%;
        }

        ol.stepper[data-stepper-steps="9"] li {
            width: 11%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }    
</style>
<!--- Page local script --->
<script type="text/javascript">
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("rotate");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").addClass("rotate");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("rotate");
        });
    });
</script>

<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">        
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>Human Resource</li>
                <li><a href="../../Account/fesDRManage_2A.aspx"> User Profile</a></li>
            </ol>
        </div>
    </div>    
</div>

<!-- Page Content -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <asp:UpdatePanel runat="server" ID="upnlPassword">
            <ContentTemplate>
                <div class="box box-info">
                    <div class="box-body">
                        <div id="divUserList" runat="server">
                            <div class="box-header with-border">
                                <h1 class="box-title">Change/Update Password</h1>
                            </div>
                            <asp:ChangePassword ID="chgPwrd" runat="server" ContinueButtonStyle-CssClass="btn btn-primary"
                                Font-Names="Segoi UI" ForeColor="Gray" Font-Size="X-Large" OnContinueButtonClick="chgPwrd_ContinueButtonClick"
                                CancelDestinationPageUrl="~/Account/fesDRManage.aspx" ContinueDestinationPageUrl="~/Account/fesDRManage.aspx"
                                CssClass="col-xs-12 col-sm-12 col-md-offset-2 col-lg-offset-2">
                                <CancelButtonStyle CssClass="btn btn-warning" Width="30%" />
                                <ChangePasswordButtonStyle CssClass="btn btn-success" Width="50%" />
                                <ChangePasswordTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <table style="width: 1200px;">
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="CurrentPassword" runat="server" CssClass="form-control" TextMode="Password" placeholder="Current Password"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword"
                                                                ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="ctl00$chgPwrd">*</asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="NewPassword" runat="server" CssClass="form-control" TextMode="Password" placeholder="New Password"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword"
                                                                ErrorMessage="New Password is required." ToolTip="New Password is required." ValidationGroup="ctl00$chgPwrd">*</asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="ConfirmNewPassword" runat="server" CssClass="form-control" TextMode="Password" placeholder="Confirm New Password"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword"
                                                                ErrorMessage="Confirm New Password is required." ToolTip="Confirm New Password is required."
                                                                ValidationGroup="ctl00$chgPwrd">*</asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="text-align: center;">
                                                            <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword"
                                                                ControlToValidate="ConfirmNewPassword" Display="Dynamic"
                                                                ErrorMessage="The Confirm New Password must match the New Password entry." ValidationGroup="ctl00$chgPwrd"></asp:CompareValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="text-align: center; color: red;">
                                                            <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right;">
                                                            <asp:Button ID="ChangePasswordPushButton" runat="server" CommandName="ChangePassword" CssClass="btn btn-primary"
                                                                Text="Change Password" ValidationGroup="ctl00$chgPwrd" Width="20%" />
                                                        </td>
                                                        <td style="text-align: left;">
                                                            <asp:Button ID="CancelPushButton" runat="server" CausesValidation="False" CommandName="Cancel" CssClass="btn btn-warning"
                                                                Text="Cancel" Width="20%" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ChangePasswordTemplate>
                                <ContinueButtonStyle CssClass="btn btn-primary" Width="50%" />
                                <SuccessTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td colspan="2" style="text-align: center; color: green;">Change Password Complete</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center; text-decoration: underline; color: green;">Your password has been changed!</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center" colspan="2">
                                                            <asp:Button ID="ContinuePushButton" runat="server" CausesValidation="False" CommandName="Continue" CssClass="btn btn-primary"
                                                                Text="Continue" Width="30%" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </SuccessTemplate>
                                <TextBoxStyle CssClass="form-control" />
                            </asp:ChangePassword>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <asp:UpdatePanel runat="server" ID="upnlProfileName">
                <ContentTemplate>
                    <div class="box box-info">
                        <div class="box-body">
                            <div id="divNameList" runat="server">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="box-header with-border">
                                        <h1 runat="server" id="nameTile" class="box-title">Logged-In Profile Full Name/Gender/Designation</h1>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                            <asp:Label ID="lblFName" runat="server" Text="First" CssClass="text-bold text-olive" />
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                            <asp:Label ID="lblMName" runat="server" Text="Middle" CssClass="text-bold text-olive" />
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                            <asp:Label ID="lblLName" runat="server" Text="Last" CssClass="text-bold text-olive" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                            <asp:TextBox ID="txtFName" runat="server" CssClass="form-control" Enabled="false" />
                                            <asp:TextBox ID="txtUpdFName" runat="server" CssClass="form-control" Visible="false" placeholder="First Name (Required)" />
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                            <asp:TextBox ID="txtMName" runat="server" CssClass="form-control" Enabled="false" />
                                            <asp:TextBox ID="txtUpdMName" runat="server" CssClass="form-control" Visible="false" placeholder="Middle Name" />
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                            <asp:TextBox ID="txtLName" runat="server" CssClass="form-control" Enabled="false" />
                                            <asp:TextBox ID="txtUpdLName" runat="server" CssClass="form-control" Visible="false" placeholder="Last Name (Required)" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                            <asp:Label ID="lblGender" runat="server" Text="Gender" CssClass="text-bold text-olive" />
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                            <asp:Label ID="lblDesig" runat="server" Text="Designation/Job Role" CssClass="text-bold text-olive" />
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                            <asp:TextBox ID="txtGender" runat="server" CssClass="form-control" Enabled="false" />
                                            <asp:DropDownList ID="ddlGender" runat="server" AutoPostBack="true" AppendDataBoundItems="true" CssClass="form-control" Visible="false">
                                                <asp:ListItem Value="0" Text="--- Select Gender ---" />
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                            <asp:TextBox ID="txtDesig" runat="server" CssClass="form-control" Enabled="false" />
                                            <asp:TextBox ID="txtUpdDesig" runat="server" CssClass="form-control" Visible="false" placeholder="Designation/Job Role (Required)" />
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4"></div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div runat="server" id="nameEnclosure" visible="false">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <h4><i runat="server" id="iName"><span runat="server" id="spnName"></span></i></h4>
                                        <asp:Label ID="lblNameSuccess" runat="server" Text="" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="input-group" style="width: 100%;">
                                        <asp:Button ID="btnUpdateFullName" runat="server" Text="Update Name" CssClass="btn btn-info text-center" Width="15%" OnClick="btnUpdateFullName_Click" />
                                        <asp:Button ID="btnDoUpdateFN" runat="server" Text="Update Name" CssClass="btn btn-success text-center" Width="15%" Visible="false"
                                            OnClick="btnDoUpdateFN_Click" />
                                        <asp:Button ID="btnUpdateReset" runat="server" Text="Reset Values" CssClass="btn btn-warning text-center" Width="15%" Visible="false"
                                            OnClick="btnUpdateReset_Click" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="input-group" style="width: 100%;">
                                        <asp:Button ID="btnUpdateNameDone" runat="server" Text="Done" CssClass="btn btn-primary text-center" Width="15%" Visible="false"
                                            OnClick="btnUpdateNameDone_Click" />
                                    </div>
                                </div>
                            </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <asp:UpdatePanel runat="server" ID="UpdatePanel2">
                <ContentTemplate>
                    <div class="box box-info">
                        <div class="box-body">
                            <div id="divPhoneList" runat="server">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="box-header with-border">
                                        <h1 class="box-title">Logged-In Profile Phone</h1>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="row">
                                            <asp:Label ID="lblPhone" runat="server" Text="Phone Number" CssClass="text-bold text-olive" />
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="row">
                                            <asp:TextBox ID="txtPhone" runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                        <asp:DropDownList ID="ddlUpdPhoneType" runat="server" AutoPostBack="true" AppendDataBoundItems="true" CssClass="form-control"
                                            Visible="false" OnSelectedIndexChanged="ddlUpdPhoneType_SelectedIndexChanged">
                                            <asp:ListItem Value="0" Text="--- Select Phone Type ---"></asp:ListItem>
                                        </asp:DropDownList>
                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                            <asp:TextBox ID="txtUpdPhoneArea" runat="server" CssClass="form-control" Enabled="false" Visible="false" TextMode="Number"
                                                placeholder="Phone Area Code" />
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                            <asp:TextBox ID="txtUpdPhonePre" runat="server" CssClass="form-control" Enabled="false" Visible="false" TextMode="Number"
                                                placeholder="Phone Prefix" />
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                            <asp:TextBox ID="txtUpdPhoneNumber" runat="server" CssClass="form-control" Enabled="false" Visible="false" TextMode="Number"
                                                placeholder="Phone Number" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div runat="server" id="phEnclosure" visible="false">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <h4><i runat="server" id="iPhone"><span runat="server" id="spnPhone"></span></i></h4>
                                        <asp:Label ID="lblPhoneSuccess" runat="server" Text="" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="input-group" style="width: 100%;">
                                        <asp:Button ID="btnUpdatePhone" runat="server" Text="Update Phone" CssClass="btn btn-info text-center" Width="15%" OnClick="btnUpdatePhone_Click" />
                                        <asp:Button ID="btnDoUpdatePhone" runat="server" Text="Update Phone" CssClass="btn btn-success text-center" Width="15%" Visible="false"
                                            OnClick="btnDoUpdatePhone_Click" />
                                        <asp:Button ID="btnUpdatePhoneReset" runat="server" Text="Reset Values" CssClass="btn btn-warning text-center" Width="15%"
                                            Visible="false" OnClick="btnUpdatePhoneReset_Click" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="input-group" style="width: 100%;">
                                        <asp:Button ID="btnUpdatePhoneDone" runat="server" Text="Done" CssClass="btn btn-primary text-center" Width="15%" Visible="false"
                                            OnClick="btnUpdatePhoneDone_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <asp:UpdatePanel runat="server" ID="UpdatePanel3">
                <ContentTemplate>
                    <div class="box box-info">
                        <div class="box-body">
                            <div id="divEmailList" runat="server">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="box-header with-border">
                                        <h1 class="box-title">Logged-In Profile Email</h1>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="row">
                                            <asp:Label ID="lblEmail" runat="server" Text="Email Address" CssClass="text-bold text-olive" />
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="row">
                                            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" Enabled="false" />
                                            <asp:TextBox ID="txtUpdEmail" runat="server" CssClass="form-control" Visible="false" placeholder="New Email Address" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div runat="server" id="emlEnclosure" visible="false">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                    <h4><i runat="server" id="iEmail"><span runat="server" id="spnEmail"></span></i></h4>
                                    <asp:Label ID="lblEmailSuccess" runat="server" Text="" />
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="input-group" style="width: 100%;">
                                    <asp:Button ID="btnUpdateEmail" runat="server" Text="Update Email" CssClass="btn btn-info text-center" Width="15%" OnClick="btnUpdateEmail_Click" />
                                    <asp:Button ID="btnDoUpdateEmail" runat="server" Text="Update Email" CssClass="btn btn-success text-center" Width="15%" Visible="false"
                                        OnClick="btnDoUpdateEmail_Click" />
                                    <asp:Button ID="btnResetEmail" runat="server" Text="Reset Values" CssClass="btn btn-warning text-center" Width="15%" Visible="false"
                                        OnClick="btnResetEmail_Click" />
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="input-group" style="width: 100%;">
                                    <asp:Button ID="btnDoneEmail" runat="server" Text="Done" CssClass="btn btn-primary text-center" Width="15%" Visible="false"
                                        OnClick="btnDoneEmail_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnDoUpdateEmail" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
