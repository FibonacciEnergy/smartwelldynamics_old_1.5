﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlFESSPManage.ascx.cs" Inherits="SmartsVer1.Control.Account.ctrlFESSPManage" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript -->
<script src="../../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>

<!--- Page local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }

    .rotate {
        -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
        -moz-transform: rotate(90deg); /* Firefox */
        -ms-transform: rotate(90deg); /* IE 9 */
        transform: rotate(90deg); /* Standard syntax */
    }

    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper[data-stepper-steps="8"] li {
            width: 12%;
        }

        ol.stepper[data-stepper-steps="9"] li {
            width: 11%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }    
</style>
<!--- Page local script --->
<script type="text/javascript">
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("rotate");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").addClass("rotate");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("rotate");
        });
    });
</script>

<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>Human Resource</li>
                <li><a href="../../Account/fesSPManage_2A.aspx"> User Profile</a></li>
            </ol>
        </div>
    </div>
</div>

<!-- Page Content -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <asp:Panel ID="pnlMain" runat="server" ScrollBars="Auto" Width="100%">


            <div class="content-wrapper" style="width: 100%;">

                <asp:UpdatePanel ID="upnlLogin" runat="server">
                    <ContentTemplate>

                        <div style="-ms-align-content: center; -webkit-align-content: center; align-content: center; width: 100%;">

                            <asp:ChangePassword ID="chgPwrd" runat="server" Height="100%" Width="900 px" ContinueButtonStyle-CssClass="btn red"
                                Font-Names="Segoi UI" ForeColor="Gray" Font-Size="X-Large" OnContinueButtonClick="chgPwrd_ContinueButtonClick" ContinueDestinationPageUrl="~/Account/fesSPManage.aspx">
                                <CancelButtonStyle CssClass="btn red" Width="30%" />
                                <ChangePasswordButtonStyle CssClass="btn red" Width="50%" />
                                <ChangePasswordTemplate>
                                    <table style="padding: 4px; border-spacing: 4px; border-collapse: collapse; width: 100%;">
                                        <tr>
                                            <td>
                                                <table style="padding: 1px; height: 100%; width: 1200px;">
                                                    <tr>
                                                        <td style="text-align: center" colspan="2">Change Your Password</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right">
                                                            <asp:Label ID="CurrentPasswordLabel" runat="server" AssociatedControlID="CurrentPassword">Password</asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="CurrentPassword" runat="server" CssClass="TextBox" Height="40%" TextMode="Password" Width="50%"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword"
                                                                ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="ctl00$chgPwrd">*</asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right">
                                                            <asp:Label ID="NewPasswordLabel" runat="server" AssociatedControlID="NewPassword">New Password</asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="NewPassword" runat="server" CssClass="TextBox" Height="40%" TextMode="Password" Width="50%"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword"
                                                                ErrorMessage="New Password is required." ToolTip="New Password is required." ValidationGroup="ctl00$chgPwrd">*</asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right">
                                                            <asp:Label ID="ConfirmNewPasswordLabel" runat="server" AssociatedControlID="ConfirmNewPassword">Confirm New Password</asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="ConfirmNewPassword" runat="server" CssClass="TextBox" Height="40%" TextMode="Password" Width="50%"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword"
                                                                ErrorMessage="Confirm New Password is required." ToolTip="Confirm New Password is required."
                                                                ValidationGroup="ctl00$chgPwrd">*</asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center;" colspan="2">
                                                            <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword"
                                                                ControlToValidate="ConfirmNewPassword" Display="Dynamic"
                                                                ErrorMessage="The Confirm New Password must match the New Password entry." ValidationGroup="ctl00$chgPwrd"></asp:CompareValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center; color: red;" colspan="2">
                                                            <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right;">
                                                            <asp:Button ID="ChangePasswordPushButton" runat="server" CommandName="ChangePassword" CssClass="btn red"
                                                                Text="Change Password" ValidationGroup="ctl00$chgPwrd" Width="35%" />
                                                        </td>
                                                        <td style="text-align: left;">
                                                            <asp:Button ID="CancelPushButton" runat="server" CausesValidation="False" CommandName="Cancel" CssClass="btn red"
                                                                Text="Cancel" Width="30%" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ChangePasswordTemplate>
                                <ContinueButtonStyle CssClass="btn red" Width="50%" />
                                <SuccessTemplate>
                                    <table style="padding: 1px; border-collapse: collapse;">
                                        <tr>
                                            <td>
                                                <table style="padding: 1px; height: 100%; width: 900px;">
                                                    <tr>
                                                        <td colspan="2" style="text-align: center; color: green;">Change Password Complete</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center; text-decoration: underline; color: green;">Your password has been changed!</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center" colspan="2">
                                                            <asp:Button ID="ContinuePushButton" runat="server" CausesValidation="False" CommandName="Continue" CssClass="btn red"
                                                                Text="Continue" Width="30%" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </SuccessTemplate>
                                <TextBoxStyle CssClass="TextBox" Width="80%" Height="40%" />
                            </asp:ChangePassword>

                        </div>
                        <div style="-ms-align-content: center; -webkit-align-content: center; align-content: center; width: 100%;">

                            <asp:Label ID="lblFName" runat="server" Text="First Name" CssClass="genLabel" Width="25%" />
                            <asp:TextBox ID="txtFName" runat="server" CssClass="genTextBox" Width="65%" />
                            <asp:Label ID="lblMName" runat="server" Text="Middle Name" CssClass="genLabel" Width="25%" />
                            <asp:TextBox ID="txtMName" runat="server" CssClass="genTextBox" Width="65%" />
                            <asp:Label ID="lblLName" runat="server" Text="Last Name" CssClass="genLabel" Width="25%" />
                            <asp:TextBox ID="txtLName" runat="server" CssClass="genTextBox" Width="65%" />
                            <asp:Label ID="lblPhone" runat="server" Text="Phone Name" CssClass="genLabel" Width="25%" />
                            <asp:TextBox ID="txtPhone" runat="server" CssClass="genTextBox" Width="65%" />
                            <asp:Label ID="lblEmail" runat="server" Text="Email Name" CssClass="genLabel" Width="25%" />
                            <asp:TextBox ID="txtEmail" runat="server" CssClass="genTextBox" Width="65%" />

                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>

        </asp:Panel>
    </div>
</div>
