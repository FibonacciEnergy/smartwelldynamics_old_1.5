﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Net.Mail;
using System.Web.Profile;
using System.Web.Security;
using System.Web.UI.WebControls;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using USR = SmartsVer1.Helpers.AccountHelpers.CreateUser;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;

namespace SmartsVer1.Control.Account
{
    public partial class ctrlFESDRManage : System.Web.UI.UserControl
    {
        String LoginName, username, domain;
        Int32 ClientID = -99;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                String gender;
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                this.txtFName.Text = usrP.FirstName;
                this.txtMName.Text = usrP.MiddleName;
                this.txtLName.Text = usrP.LastName;
                System.Guid uID = new Guid(usrP.UserId);
                gender = USR.getUserGender(uID);
                this.txtGender.Text = gender;
                this.txtDesig.Text = usrP.designation;
                String uPhone = Convert.ToString(usrP.upPhone);
                Int32 pType = Convert.ToInt32(usrP.upPhoneType);
                String ptName = DMG.getDemogTypeName(pType);
                this.txtPhone.Text = "(" + uPhone.Substring(0, 3) + ") " + uPhone.Substring(3, 3) + "-" + uPhone.Substring(6, 4) + " (" + ptName + ")";                
                this.txtEmail.Text = Membership.GetUser().Email;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void chgPwrd_ContinueButtonClick(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Account/fesDRManage.aspx", true);                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnUpdateFullName_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtUpdFName.Visible = true;
                this.txtUpdMName.Visible = true;
                this.txtUpdLName.Visible = true;
                this.txtGender.Visible = false;
                Dictionary<Int32, String> gndList = DMG.getGenderList();
                this.ddlGender.Items.Clear();
                this.ddlGender.DataSource = gndList;
                this.ddlGender.DataTextField = "Value";
                this.ddlGender.DataValueField = "Key";                
                this.ddlGender.DataBind();
                this.ddlGender.Items.Insert(0, new ListItem("--- Select Gender ---", "-1"));
                this.ddlGender.SelectedIndex = -1;
                this.ddlGender.Visible = true;
                this.txtUpdDesig.Visible = true;
                this.btnUpdateFullName.Visible = false;
                this.btnDoUpdateFN.Visible = true;
                this.btnUpdateReset.Visible = true;
                this.nameEnclosure.Visible = false;
                this.btnUpdateNameDone.Visible = true;
                this.nameTile.InnerText = "Update Full Name/Gender/Designation";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }                

        protected void btnDoUpdateFN_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 gnd = -99, pgnd = -99, result1 = -99, result2 = -99;
                String fN = String.Empty, mN = String.Empty, lN = String.Empty, dsg = String.Empty, gndr = String.Empty, pgndr = String.Empty;
                fN = Convert.ToString(this.txtUpdFName.Text);
                mN = Convert.ToString(this.txtUpdMName.Text);
                lN = Convert.ToString(this.txtUpdLName.Text);
                gndr = Convert.ToString(this.txtGender.Text);                                
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                System.Guid uID = new Guid(usrP.UserId);
                pgndr = USR.getUserGender(uID);
                gnd = Convert.ToInt32(this.ddlGender.SelectedValue);
                pgnd = DMG.getGenderIDFromValue(gndr);
                dsg = Convert.ToString(this.txtUpdDesig.Text);
                if (fN.Equals(usrP.FirstName) && mN.Equals(usrP.MiddleName) && lN.Equals(usrP.LastName) && gnd.Equals(-1) && gnd.Equals(pgnd) && dsg.Equals(usrP.designation))
                {
                    this.nameEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iName.Attributes["class"] = "icon fa fa-warning";
                    this.spnName.InnerText = " Information Not Updated";
                    this.lblNameSuccess.Text = "Submitted information is not different than Profile. No updates were done";
                    this.nameEnclosure.Visible = true;
                }
                else
                {
                    result1 = USR.updatePersonalProfile(LoginName, fN, mN, lN, gnd);
                    result2 = USR.updateProfileDesignation(LoginName, dsg);
                    if (result1.Equals(1) && result2.Equals(1))
                    {
                        this.nameEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iName.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnName.InnerText = " Success";
                        this.lblNameSuccess.Text = "Successfully updated Profile information for logged-in user";
                        this.nameEnclosure.Visible = true;
                        String gender;                        
                        this.txtFName.Text = usrP.FirstName;
                        this.txtMName.Text = usrP.MiddleName;
                        this.txtLName.Text = usrP.LastName;
                        gender = USR.getUserGender(uID);
                        this.txtGender.Text = gender;
                        this.txtDesig.Text = usrP.designation;
                        this.txtUpdFName.Text = "";
                        this.txtUpdFName.Visible = false;
                        this.txtUpdMName.Text = "";
                        this.txtUpdMName.Visible = false;
                        this.txtUpdLName.Text = "";
                        this.txtUpdLName.Visible = false;
                        this.ddlGender.Visible = false;
                        this.txtUpdDesig.Text = "";
                        this.txtUpdDesig.Visible = false;
                    }
                    else
                    {
                        this.nameEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iName.Attributes["class"] = "icon fa fa-warning";
                        this.spnName.InnerText = " Information Not Updated";
                        this.lblNameSuccess.Text = "Submitted information is not different than Profile. No updates were done";
                        this.nameEnclosure.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnUpdateReset_Click(object sender, EventArgs e)
        {
            try
            {
                this.nameEnclosure.Visible = false;
                String gender;
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                this.txtFName.Text = usrP.FirstName;
                this.txtMName.Text = usrP.MiddleName;
                this.txtLName.Text = usrP.LastName;
                System.Guid uID = new Guid(usrP.UserId);
                gender = USR.getUserGender(uID);
                this.txtGender.Text = gender;
                this.txtDesig.Text = usrP.designation;
                this.txtUpdFName.Text = "";
                this.txtUpdMName.Text = "";
                this.txtUpdLName.Text = "";
                Dictionary<Int32, String> gndList = DMG.getGenderList();
                this.ddlGender.Items.Clear();
                this.ddlGender.DataSource = gndList;
                this.ddlGender.DataTextField = "Value";
                this.ddlGender.DataValueField = "Key";
                this.ddlGender.DataBind();
                this.ddlGender.Items.Insert(0, new ListItem("--- Select Gender ---", "-1"));
                this.ddlGender.SelectedIndex = -1;
                this.ddlGender.Visible = true;
                this.txtUpdDesig.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnUpdateNameDone_Click(object sender, EventArgs e)
        {
            try
            {
                String gender;
                this.btnUpdateReset_Click(null, null);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                this.txtFName.Text = usrP.FirstName;
                this.txtMName.Text = usrP.MiddleName;
                this.txtLName.Text = usrP.LastName;
                System.Guid uID = new Guid(usrP.UserId);
                gender = USR.getUserGender(uID);
                this.txtGender.Text = gender;
                this.txtGender.Visible = true;
                this.txtGender.Enabled = false;
                this.txtDesig.Text = usrP.designation;
                this.txtDesig.Enabled = false;
                this.ddlGender.Items.Clear();
                this.ddlGender.DataBind();
                this.ddlGender.Items.Insert(0, new ListItem("--- Select Gender ---", "-1"));
                this.ddlGender.SelectedIndex = -1;
                this.ddlGender.Visible = false;
                this.nameTile.InnerText = "Logged-In Profile Full Name/Gender/Designation";
                this.txtUpdFName.Visible = false;
                this.txtUpdMName.Visible = false;
                this.txtUpdLName.Visible = false;
                this.txtUpdDesig.Visible = false;
                this.btnUpdateFullName.Visible = true;
                this.btnDoUpdateFN.Visible = false;
                this.btnUpdateReset.Visible = false;
                this.btnUpdateNameDone.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnUpdateEmail_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtUpdEmail.Visible = true;
                this.btnUpdateEmail.Visible = false;
                this.btnDoUpdateEmail.Visible = true;
                this.btnResetEmail.Visible = true;
                this.btnDoneEmail.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDoUpdateEmail_Click(object sender, EventArgs e)
        {
            try            
            {
                Int32 iResult = -99;
                String eml = String.Empty;
                eml = Convert.ToString(this.txtUpdEmail.Text);
                if (String.IsNullOrEmpty(eml))
                {
                    this.emlEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iEmail.Attributes["class"] = "icon fa fa-warning";
                    this.spnEmail.InnerText = " Warning";
                    this.lblEmailSuccess.Text = "Please provide a valid Email address";
                    this.emlEnclosure.Visible = true;
                    this.txtUpdEmail.Text = "";
                }
                else
                {
                    iResult = USR.updateContactEmail(LoginName, eml);
                    if (iResult.Equals(1))
                    {
                        this.emlEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iEmail.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnEmail.InnerText = " Success";
                        this.lblEmailSuccess.Text = "Successfully updated Profile Email address";
                        this.emlEnclosure.Visible = true;
                        UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                        this.txtEmail.Text = Membership.GetUser().Email;
                        this.txtUpdEmail.Text = "";
                        this.txtUpdEmail.Visible = false;
                    }
                    else
                    {
                        this.emlEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iEmail.Attributes["class"] = "icon fa fa-warning";
                        this.spnEmail.InnerText = " Warning";
                        this.lblEmailSuccess.Text = "Unable to update Profile Email";
                        this.emlEnclosure.Visible = true;
                        this.txtUpdEmail.Text = "";
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnResetEmail_Click(object sender, EventArgs e)
        {
            try
            {
                this.emlEnclosure.Visible = false;
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                this.txtEmail.Text = Membership.GetUser().Email;                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDoneEmail_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnResetEmail_Click(null, null);
                this.txtEmail.Enabled = false;
                this.btnUpdateEmail.Visible = true;
                this.btnDoUpdateEmail.Visible = false;
                this.btnResetEmail.Visible = false;
                this.btnDoneEmail.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnUpdatePhone_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> phTypes = DMG.getDemogTypeList();
                this.ddlUpdPhoneType.Items.Clear();
                this.ddlUpdPhoneType.DataSource = phTypes;
                this.ddlUpdPhoneType.DataTextField = "Value";
                this.ddlUpdPhoneType.DataValueField = "Key";
                this.ddlUpdPhoneType.DataBind();
                this.ddlUpdPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlUpdPhoneType.SelectedIndex = -1;
                this.ddlUpdPhoneType.Visible = true;
                this.txtUpdPhoneArea.Visible = true;
                this.txtUpdPhonePre.Visible = true;
                this.txtUpdPhoneNumber.Visible = true;
                this.btnUpdatePhone.Visible = false;
                this.btnDoUpdatePhone.Visible = true;
                this.btnUpdatePhoneReset.Visible = true;
                this.btnUpdatePhoneDone.Visible = true;
                this.phEnclosure.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlUpdPhoneType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtUpdPhoneArea.Enabled = true;
                this.txtUpdPhonePre.Enabled = true;
                this.txtUpdPhoneNumber.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDoUpdatePhone_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 result = -99;
                String phArea = String.Empty, phPre = String.Empty, phNumb = String.Empty, phone = String.Empty, phType = String.Empty;

                phType = this.ddlUpdPhoneType.SelectedValue;
                phArea = this.txtUpdPhoneArea.Text;
                phPre = this.txtUpdPhonePre.Text;
                phNumb = this.txtUpdPhoneNumber.Text;

                if (String.IsNullOrEmpty(phType) || String.IsNullOrEmpty(phArea) || String.IsNullOrEmpty(phPre) || String.IsNullOrEmpty(phNumb))
                {
                    this.phEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iPhone.Attributes["class"] = "icon fa fa-warning";
                    this.spnPhone.InnerText = " Warning";
                    this.lblPhoneSuccess.Text = "Please provide all values!";
                    this.phEnclosure.Visible = true;                    
                }
                else
                {
                    phone = phArea + phPre + phNumb;

                    result = USR.updateProfilePhone(LoginName, phType, phone);
                    if (result.Equals(1))
                    {
                        UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                        String uPhone = Convert.ToString(usrP.upPhone);
                        Int32 pType = Convert.ToInt32(usrP.upPhoneType);
                        String ptName = DMG.getDemogTypeName(pType);
                        this.txtPhone.Text = "(" + uPhone.Substring(0, 3) + ") " + uPhone.Substring(3, 3) + "-" + uPhone.Substring(6, 4) + " (" + ptName + ")";
                        this.btnUpdatePhoneReset_Click(null, null);
                        this.phEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iPhone.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnPhone.InnerText = " Success";
                        this.lblPhoneSuccess.Text = "Successfully updated Profile's Phone Number.";
                        this.phEnclosure.Visible = true;                         
                    }
                    else
                    {
                        this.btnUpdatePhoneReset_Click(null, null);
                        this.phEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iPhone.Attributes["class"] = "icon fa fa-warning";
                        this.spnPhone.InnerText = " Warning";
                        this.lblPhoneSuccess.Text = "Problem updating Profile's Phone Number";
                        this.phEnclosure.Visible = true;   
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnUpdatePhoneReset_Click(object sender, EventArgs e)
        {
            try
            {
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                String uPhone = Convert.ToString(usrP.upPhone);
                Int32 pType = Convert.ToInt32(usrP.upPhoneType);
                String ptName = DMG.getDemogTypeName(pType);
                this.txtPhone.Text = "(" + uPhone.Substring(0, 3) + ") " + uPhone.Substring(3, 3) + "-" + uPhone.Substring(6, 4) + " (" + ptName + ")";
                Dictionary<Int32, String> phTypes = DMG.getDemogTypeList();
                this.ddlUpdPhoneType.Items.Clear();
                this.ddlUpdPhoneType.DataSource = phTypes;
                this.ddlUpdPhoneType.DataTextField = "Value";
                this.ddlUpdPhoneType.DataValueField = "Key";
                this.ddlUpdPhoneType.DataBind();
                this.ddlUpdPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlUpdPhoneType.SelectedIndex = -1;
                this.txtUpdPhoneArea.Text = "";
                this.txtUpdPhoneArea.Enabled = false;
                this.txtUpdPhonePre.Text = "";
                this.txtUpdPhonePre.Enabled = false;
                this.txtUpdPhoneNumber.Text = "";
                this.txtUpdPhoneNumber.Enabled = false;
                this.phEnclosure.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnUpdatePhoneDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnUpdatePhone_Click(null, null);
                this.ddlUpdPhoneType.Visible = false;
                this.txtUpdPhoneArea.Visible = false;
                this.txtUpdPhonePre.Visible = false;
                this.txtUpdPhoneNumber.Visible = false;
                this.btnUpdatePhone.Visible = true;
                this.btnDoUpdatePhone.Visible = false;
                this.btnUpdatePhoneReset.Visible = false;
                this.btnUpdatePhoneDone.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }                
    }
}