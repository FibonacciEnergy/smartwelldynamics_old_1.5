﻿using System;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using TRK = SmartsVer1.Helpers.AccountHelpers.Tracker;

namespace SmartsVer1.Control.Account
{
    public partial class ctrlSMARTAccess2 : System.Web.UI.UserControl
    {
        String LoginName = String.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Menu Loading
                this.ltrMenuLiteral.Text = MNL.getDefaultMenu();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Login_LoggingIn(object sender, LoginCancelEventArgs e)
        {
            try
            {
                Int32 usrTyp = 0;
                DateTime start = DateTime.Now, end = DateTime.Now;
                String name = Convert.ToString(Login.UserName);
                List<String> checkValidContract = new List<String>();
                MembershipUser usrN = Membership.GetUser(name, false);
                if (String.IsNullOrEmpty(name))
                {
                    Login.FailureText = "User Account not found";
                    FormsAuthentication.SetAuthCookie(Login.UserName, false);
                }
                else if (Membership.FindUsersByName(name).Count.Equals(0))
                {
                    Login.FailureText = "User Account not found";
                    FormsAuthentication.SetAuthCookie(Login.UserName, false);
                }
                else
                {
                    UP usrProf = UP.GetUserProfile(name);
                    //find username exists here first
                    if (!usrProf.ctypeID.Equals(null))
                    {
                        usrTyp = Convert.ToInt32(usrProf.ctypeID);
                    }
                    else
                    {
                        usrTyp = -99;
                    }
                    Int32 ClientID = Convert.ToInt32(usrProf.clntID);
                    if (!usrTyp.Equals(4))
                    {
                        //List<String> checkValidContract = TRK.checkContractStatus(ClientID);
                        checkValidContract = TRK.checkContractStatus(ClientID);
                        LoginName = Convert.ToString(checkValidContract[0]);
                        start = Convert.ToDateTime(checkValidContract[1]);
                        end = Convert.ToDateTime(checkValidContract[2]);
                    }
                    else
                    {
                        end = Convert.ToDateTime(DateTime.Now.AddHours(6));
                    }
                    if (Membership.ValidateUser(Login.UserName, Login.Password) && Membership.GetUser(Login.UserName).IsApproved && !Membership.GetUser(Login.UserName).IsLockedOut && (end > DateTime.Now))
                    {
                        if (usrTyp.Equals(3)) //3 = Owners
                        {
                            if (Roles.IsUserInRole(Login.UserName, "feDirector"))
                            {
                                FormsAuthentication.SetAuthCookie(Login.UserName, true);
                                FormsAuthentication.RedirectFromLoginPage(Login.UserName, true);
                                Login.DestinationPageUrl = "/Viewer/fesViewer.aspx";
                            }
                            else
                            {
                                FormsAuthentication.SetAuthCookie(Login.UserName, true);
                                FormsAuthentication.RedirectFromLoginPage(Login.UserName, true);
                                Login.DestinationPageUrl = "/Viewer/fesViewer.aspx";
                            }
                        }
                        else if (usrTyp.Equals(4)) //4 = Operators
                        {
                            if (Roles.IsUserInRole(Login.UserName, "Reviewer"))
                            {
                                FormsAuthentication.SetAuthCookie(Login.UserName, true);
                                FormsAuthentication.RedirectFromLoginPage(Login.UserName, true);
                                Login.DestinationPageUrl = "/Viewer/rwrViewer_2A.aspx";
                            }
                        }
                        else if (usrTyp.Equals(1)) //1 = Clients
                        {
                            LoginName = Convert.ToString(checkValidContract[0]);
                            start = Convert.ToDateTime(checkValidContract[1]);
                            end = Convert.ToDateTime(checkValidContract[2]);
                            if (end > (DateTime.Now))
                            {
                                if (Roles.IsUserInRole(Login.UserName, "Director"))
                                {
                                    FormsAuthentication.SetAuthCookie(Login.UserName, true);
                                    FormsAuthentication.RedirectFromLoginPage(Login.UserName, true);
                                    Login.DestinationPageUrl = "/Viewer/crdViewer2.aspx";
                                }
                                else if (Roles.IsUserInRole(Login.UserName, "Coordinator"))
                                {
                                    FormsAuthentication.SetAuthCookie(Login.UserName, true);
                                    FormsAuthentication.RedirectFromLoginPage(Login.UserName, true);
                                    Login.DestinationPageUrl = "/Viewer/crdViewer2.aspx";
                                }
                                else if (Roles.IsUserInRole(Login.UserName, "Fieldhand"))
                                {
                                    FormsAuthentication.SetAuthCookie(Login.UserName, true);
                                    FormsAuthentication.RedirectFromLoginPage(Login.UserName, true);
                                    Login.DestinationPageUrl = "/Viewer/crdViewer2.aspx";
                                }
                                else if (Roles.IsUserInRole(Login.UserName, "LabTech"))
                                {
                                    FormsAuthentication.SetAuthCookie(Login.UserName, true);
                                    FormsAuthentication.RedirectFromLoginPage(Login.UserName, true);
                                    Login.DestinationPageUrl = "/Viewer/crdViewer2.aspx";
                                }
                                else if (Roles.IsUserInRole(Login.UserName, "feSupport"))
                                {
                                    FormsAuthentication.SetAuthCookie(Login.UserName, true);
                                    FormsAuthentication.RedirectFromLoginPage(Login.UserName, true);
                                    Login.DestinationPageUrl = "/Viewer/fesViewer.aspx";
                                }
                                else if (Roles.IsUserInRole(Login.UserName, "MWD_1"))
                                {
                                    FormsAuthentication.SetAuthCookie(Login.UserName, true);
                                    FormsAuthentication.RedirectFromLoginPage(Login.UserName, true);
                                    Login.DestinationPageUrl = "/Services/fecQCMin.aspx";
                                }
                            }
                            else
                            {
                                FormsAuthentication.SetAuthCookie(Login.UserName, false);
                                String value = String.Format("Your Contract, {0} with start Date, {1} and end Date, {2}, has expired.", LoginName, start, end);
                                this.Login.FailureText = value;
                                Response.Redirect("/SWDHome.aspx", false);
                            }
                        }
                        else if (usrTyp.Equals(2)) //2 = Partners
                        {
                            LoginName = Convert.ToString(checkValidContract[0]);
                            start = Convert.ToDateTime(checkValidContract[1]);
                            end = Convert.ToDateTime(checkValidContract[2]);
                            if (end < (DateTime.Now))
                            {
                                if (Roles.IsUserInRole(Login.UserName, "Director"))
                                {
                                    FormsAuthentication.SetAuthCookie(Login.UserName, true);
                                    FormsAuthentication.RedirectFromLoginPage(Login.UserName, true);
                                    Login.DestinationPageUrl = "/Viewer/FESDRViewer.aspx";
                                }
                                else if (Roles.IsUserInRole(Login.UserName, "Log Reviewer"))
                                {
                                    FormsAuthentication.SetAuthCookie(Login.UserName, true);
                                    FormsAuthentication.RedirectFromLoginPage(Login.UserName, true);
                                    Login.DestinationPageUrl = "/Viewer/rwrViewer.aspx";
                                }
                                else
                                {
                                    FormsAuthentication.SetAuthCookie(Login.UserName, true);
                                    FormsAuthentication.RedirectFromLoginPage(Login.UserName, true);
                                    Login.DestinationPageUrl = "/Viewer/FESSPViewer.aspx";
                                }
                            }
                            else
                            {
                                FormsAuthentication.SetAuthCookie(Login.UserName, false);
                                Login.DestinationPageUrl = "/Account/SMARTAccess.aspx";
                            }
                        }
                        else
                        {
                            FormsAuthentication.SetAuthCookie(Login.UserName, false);
                            Response.Redirect("/SWDHome.aspx", false);
                        }
                    }
                    else
                    {
                        String value = String.Format("Your Contract, {0} with start Date, {1} and end Date, {2}, has expired.", LoginName, start, end);
                        this.Login.FailureText = value;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Login_LoginError(object sender, EventArgs e)
        {
            try
            {
                Login.FailureText = "!!! Username or Password do not match your registered account information !!!";

                MembershipUser usrInfo = Membership.GetUser(Login.UserName);
                if (usrInfo != null)
                {
                    if (usrInfo.IsLockedOut)
                    {
                        Login.FailureText = "!!! Too many invalid Login attempts. Please contact Platform Administrator to have your account unlocked !!!";
                    }
                    else if (!usrInfo.IsApproved)
                    {
                        Login.FailureText = "!!! Your account is not approved. You cannot login until your account is approved. !!!";
                    }
                }
                else if (Membership.FindUsersByName(Login.UserName).Count.Equals(0))
                {
                    Login.FailureText = "!!! Invalid User name !!!";
                }
                else
                {
                    Login.FailureText = "!!! Username not found !!!";
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}