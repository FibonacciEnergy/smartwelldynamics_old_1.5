﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Net.Mail;
using System.Web.Profile;
using System.Web.Security;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using System.Web.UI.WebControls;

namespace SmartsVer1.Control.Account
{
    public partial class ctrlFESSPManage : System.Web.UI.UserControl
    {
        String LoginName, username, domain;
        Int32 ClientID = -99;
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                this.txtFName.Text = usrP.FirstName;
                this.txtMName.Text = usrP.MiddleName;
                this.txtLName.Text = usrP.LastName;
                this.txtPhone.Text = usrP.upPhone;
                this.txtEmail.Text = Membership.GetUser().Email;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void chgPwrd_ContinueButtonClick(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Account/fesSPManage_2A.aspx");
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}