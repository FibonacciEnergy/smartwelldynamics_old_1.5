﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI.WebControls;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using MNU = SmartsVer1.Helpers.Literals.MenuItems;
using USR = SmartsVer1.Helpers.AccountHelpers.CreateUser;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;

namespace SmartsVer1.Control.Account
{
    public partial class ctrlCRDManage_2A : System.Web.UI.UserControl
    {
        String LoginName, username, domain, GetClientDBString;
        Int32 ClientID = -99;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
                //Datasources for page dropdown lists
                Dictionary<Int32, String> phTypes = DMG.getDemogTypeList();
                Dictionary<Int32, String> gndList = DMG.getGenderList();
                //Datasource assignmens to controls
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataSource = phTypes;
                this.ddlPhoneType.DataTextField = "Value";
                this.ddlPhoneType.DataValueField = "Key";
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlAddressType.Items.Clear();
                this.ddlAddressType.DataSource = phTypes;
                this.ddlAddressType.DataTextField = "Value";
                this.ddlAddressType.DataValueField = "Key";
                this.ddlAddressType.DataBind();
                this.ddlAddressType.Items.Insert(0, new ListItem("--- Select Address Type ---", "-1"));
                this.ddlAddressType.SelectedIndex = -1;
                this.ddlUpdGender.Items.Clear();
                this.ddlUpdGender.DataSource = gndList;
                this.ddlUpdGender.DataTextField = "Value";
                this.ddlUpdGender.DataValueField = "Key";
                this.ddlUpdGender.DataBind();
                this.ddlUpdGender.Items.Insert(0, new ListItem("--- Select Gender ---", "-1"));
                this.ddlUpdGender.SelectedIndex = -1;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Int32 city = 0, county = 0, state = 0, country = 0, zip = 0, sreg = 0, reg = 0, dmg = 0, phDmg = 0;
                String type = String.Empty, pT = String.Empty, phone = String.Empty, phTypeName = String.Empty, rolename = String.Empty, addType = String.Empty;
                String uRegion = String.Empty, uSRegion = String.Empty, uCountry = String.Empty, uState = String.Empty, uCounty = String.Empty;
                String uCity = String.Empty, uZip = String.Empty, gender = String.Empty;
                String[] roles = Roles.GetRolesForUser(LoginName);
                rolename = Convert.ToString(roles.GetValue(0));

                UP usrP = UP.GetUserProfile(LoginName);
                System.Guid uID = new Guid(usrP.UserId);
                type = Convert.ToString(usrP.addType);
                if (!String.IsNullOrEmpty(type))
                {
                    dmg = Convert.ToInt32(usrP.addType);
                    addType = DMG.getDemogTypeName(dmg);
                }
                else
                {
                    addType = "---";
                }
                pT = Convert.ToString(usrP.upPhoneType);
                if (!String.IsNullOrEmpty(pT))
                {
                    phDmg = Convert.ToInt32(usrP.upPhoneType);
                    phTypeName = DMG.getDemogTypeName(phDmg);
                }
                else
                {
                    phTypeName = "---";
                }
                city = Convert.ToInt32(usrP.city);
                uCity = DMG.getCityName(city);
                county = Convert.ToInt32(usrP.county);
                uCounty = DMG.getCountyName(county);
                state = Convert.ToInt32(usrP.state);
                uState = DMG.getStateName(state);
                country = Convert.ToInt32(usrP.country);
                uCountry = DMG.getCountryName(country);
                zip = Convert.ToInt32(usrP.zip);
                uZip = DMG.getZipCode(zip);
                sreg = Convert.ToInt32(usrP.sreg);
                uSRegion = DMG.getSubRegionName(sreg);
                reg = Convert.ToInt32(usrP.reg);
                uRegion = DMG.getRegionName(reg);
                phone = Convert.ToString(usrP.upPhone);
                gender = USR.getUserGender(uID);
                if (String.IsNullOrEmpty(gender)) { gender = "---"; }
                //Edit Profile Info Text
                this.txtProfileFName.Text = usrP.FirstName;
                this.txtProfileMName.Text = usrP.MiddleName;
                this.txtProfileLName.Text = usrP.LastName;
                this.txtGender.Text = gender;

                this.txtPhone.Text = "(" + phone.Substring(0, 3) + ") " + phone.Substring(3, 3) + "-" + phone.Substring(6, 4) + " (" + phTypeName + ")";
                this.txtProfileEmail.Text = Membership.GetUser().Email;
                this.txtRole.Text = rolename;
                this.txtPType.Text = addType;
                this.txtPStreet1.Text = usrP.st1;
                this.txtPStreet2.Text = usrP.st2;
                this.txtPStreet3.Text = usrP.st3;
                this.txtPCityName.Text = uCity;
                this.txtPZip.Text = uZip;
                this.txtPCounty.Text = uCounty;
                this.txtPState.Text = uState;
                this.txtPCountry.Text = uCountry;
                this.txtPSubRegion.Text = uSRegion;
                this.txtPRegion.Text = uRegion;
                this.txtUserName.Text = System.Web.HttpContext.Current.User.Identity.Name;

                //Render Profile Address Map
                //this.ltrLandingMap.Text = MAP.getProfileLandingMap(usrP, "BodyContent_ctrlClientDRManage_divMap", GetClientDBString);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void chgPwrd_ContinueButtonClick(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnUpdatePersonal_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnUpdateNameCancel_Click(null, null);
                this.btnUpdateNameCancel.Enabled = true;
                this.btnUpdateNameCancel.CssClass = "btn btn-warning text-center";
                this.divName.Visible = false;
                this.divNameEdit.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnUpdateNameCancel_Click(object sender, EventArgs e)
        {
            try
            {
                UP usrP = UP.GetUserProfile(LoginName);
                System.Guid uID = new Guid(usrP.UserId);
                
                String gender = USR.getUserGender(uID);
                if (String.IsNullOrEmpty(gender)) { gender = "---"; }
                //Edit Profile Info Text
                this.txtProfileFName.Text = usrP.FirstName;
                this.txtProfileMName.Text = usrP.MiddleName;
                this.txtProfileLName.Text = usrP.LastName;
                this.txtGender.Text = gender;
                this.divName.Visible = true;
                this.divNameEdit.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnConfirmProfileUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 result = -99, gID;
                String FN = String.Empty, MN = String.Empty, LN = String.Empty;

                FN = Convert.ToString(this.txtFName.Text);
                MN = Convert.ToString(this.txtMName.Text);
                LN = Convert.ToString(this.txtLName.Text);
                gID = Convert.ToInt32(this.ddlUpdGender.SelectedValue);
                if (String.IsNullOrEmpty(FN) || String.IsNullOrEmpty(LN))
                {
                    this.nameEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iName.Attributes["class"] = "icon fa fa-warning";
                    this.spnName.InnerText = " Warning";
                    this.lblProfileInfo.Text = "Please provide all required values";
                    this.nameEnclosure.Visible = true;
                }
                else
                {
                    result = USR.updatePersonalProfile(LoginName, FN, MN, LN, gID);
                    if (result.Equals(1))
                    {
                        this.nameEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iName.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnName.InnerText = " Success";
                        this.lblProfileInfo.Text = "Successfully updated Full Name";
                        this.nameEnclosure.Visible = true;
                    }
                    else
                    {
                        this.nameEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iName.Attributes["class"] = "icon fa fa-warning";
                        this.spnName.InnerText = " Warning";
                        this.lblProfileInfo.Text = "Unable to process request.";
                        this.nameEnclosure.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnClearProfileUpdate_Click(Object sender, EventArgs e)
        {
            try
            {
                this.txtFName.Text = "";
                this.txtMName.Text = "";
                this.txtLName.Text = "";
                Dictionary<Int32, String> gndList = DMG.getGenderList();
                this.ddlUpdGender.Items.Clear();
                this.ddlUpdGender.DataSource = gndList;
                this.ddlUpdGender.DataTextField = "Value";
                this.ddlUpdGender.DataValueField = "Key";
                this.ddlUpdGender.DataBind();
                this.ddlUpdGender.Items.Insert(0, new ListItem("--- Select Gender ---", "-1"));
                this.ddlUpdGender.SelectedIndex = -1;
                this.nameEnclosure.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCancelProfile_Click(Object sender, EventArgs e)
        {
            try
            {
                this.btnClearProfileUpdate_Click(null, null);
                this.btnUpdateNameCancel_Click(null, null);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnPhoneUpdate_Click(object sender, EventArgs e)
        {
            try
            {                
                this.btnPhoneReset_Click(null, null);
                this.btnPhoneReset.Enabled = true;
                this.btnPhoneReset.CssClass = "btn btn-warning text-center";
                this.phEnclosure.Visible = false;
                this.divPhoneList.Visible = false;
                this.divPhoneEdit.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnUpdateEmail_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnUpdateEmailReset.Enabled = true;
                this.btnUpdateEmailReset.CssClass = "btn btn-warning text-center";
                this.divEmailList.Visible = false;
                this.divEmailAdd.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnUpdateEmailReset_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtProfileEmail.Text = Membership.GetUser().Email;
                this.btnUpdateEmailReset.Enabled = false;
                this.btnUpdateEmailReset.CssClass = "btn btn-default text-center";
                this.divEmailList.Visible = true;
                this.divEmailAdd.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDoUpdateEmail_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                String eml = String.Empty;
                eml = Convert.ToString(this.txtUPDProfileEmail.Text);
                if (String.IsNullOrEmpty(eml))
                {
                    this.emlEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iEmail.Attributes["class"] = "icon fa fa-warning";
                    this.spnEmail.InnerText = " Warning";
                    this.lblEmailSuccess.Text = "Please provide a valid Email address";
                    this.emlEnclosure.Visible = true;
                }
                else
                {
                    iResult = USR.updateContactEmail(LoginName, eml);
                    if (iResult.Equals(1))
                    {
                        this.txtUPDProfileEmail.Text = "";
                        this.emlEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iEmail.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnEmail.InnerText = " Success";
                        this.lblEmailSuccess.Text = "Successfully updated Email address";
                        this.emlEnclosure.Visible = true;
                    }
                    else
                    {
                        this.emlEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iEmail.Attributes["class"] = "icon fa fa-warning";
                        this.spnEmail.InnerText = " Warning";
                        this.lblEmailSuccess.Text = "Unable to update Email address. Please try again";
                        this.emlEnclosure.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnEmailReset_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtUPDProfileEmail.Text = "";
                this.emlEnclosure.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnEmailDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnEmailReset_Click(null, null);
                this.btnUpdateEmailReset_Click(null, null);             
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProfileAddress_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnProfileAddressReset.Enabled = true;
                this.btnProfileAddressReset.CssClass = "btn btn-warning text-center";
                this.divAddList.Visible = false;
                this.divAddEdit.Visible = true;
                Dictionary<Int32, String> regList = DMG.getRegionList();
                this.ddlProfileReg.Items.Clear();
                this.ddlProfileReg.DataSource = regList;
                this.ddlProfileReg.DataTextField = "Value";
                this.ddlProfileReg.DataValueField = "Key";
                this.ddlProfileReg.DataBind();
                this.ddlProfileReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlProfileReg.SelectedIndex = -1;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProfileAddressReset_Click(object sender, EventArgs e)
        {
            Int32 city = 0, county = 0, state = 0, country = 0, zip = 0, sreg = 0, reg = 0, dmg = 0;
            String type = String.Empty, addType = String.Empty;
            String uRegion = String.Empty, uSRegion = String.Empty, uCountry = String.Empty, uState = String.Empty, uCounty = String.Empty;
            String uCity = String.Empty, uZip = String.Empty;            

            UP usrP = UP.GetUserProfile(LoginName);
            type = Convert.ToString(usrP.addType);
            if (!String.IsNullOrEmpty(type))
            {
                dmg = Convert.ToInt32(usrP.addType);
                addType = DMG.getDemogTypeName(dmg);
            }
            else
            {
                addType = "---";
            }
            city = Convert.ToInt32(usrP.city);
            uCity = DMG.getCityName(city);
            county = Convert.ToInt32(usrP.county);
            uCounty = DMG.getCountyName(county);
            state = Convert.ToInt32(usrP.state);
            uState = DMG.getStateName(state);
            country = Convert.ToInt32(usrP.country);
            uCountry = DMG.getCountryName(country);
            zip = Convert.ToInt32(usrP.zip);
            uZip = DMG.getZipCode(zip);
            sreg = Convert.ToInt32(usrP.sreg);
            uSRegion = DMG.getSubRegionName(sreg);
            reg = Convert.ToInt32(usrP.reg);
            uRegion = DMG.getRegionName(reg);

            //Edit Profile Info Text
            this.txtPStreet1.Text = usrP.st1;
            this.txtPStreet2.Text = usrP.st2;
            this.txtPStreet3.Text = usrP.st3;
            this.txtPCityName.Text = uCity;
            this.txtPZip.Text = uZip;
            this.txtPCounty.Text = uCounty;
            this.txtPState.Text = uState;
            this.txtPCountry.Text = uCountry;
            this.txtPSubRegion.Text = uSRegion;
            this.txtPRegion.Text = uRegion;
            this.btnProfileAddressReset.Enabled = false;
            this.btnProfileAddressReset.CssClass = "btn btn-default text-center";
            this.divAddList.Visible = true;
            this.divAddEdit.Visible = false;
        }  


        protected void btnPhoneReset_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 phDmg = 0;
                String phone = String.Empty, pT = String.Empty, phTypeName = String.Empty;
                UP usrP = UP.GetUserProfile(LoginName);
                System.Guid uID = new Guid(usrP.UserId);
                
                pT = Convert.ToString(usrP.upPhoneType);
                if (!String.IsNullOrEmpty(pT))
                {
                    phDmg = Convert.ToInt32(usrP.upPhoneType);
                    phTypeName = DMG.getDemogTypeName(phDmg);
                }
                else
                {
                    phTypeName = "---";
                }
                phone = Convert.ToString(usrP.upPhone);
                this.txtPhone.Text = "(" + phone.Substring(0, 3) + ") " + phone.Substring(3, 3) + "-" + phone.Substring(6, 4) + " (" + phTypeName + ")";
                this.divPhoneList.Visible = true;
                this.divPhoneEdit.Visible = false;
                this.phEnclosure.Visible = false;
                this.btnPhoneReset.Enabled = false;
                this.btnPhoneReset.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnPhoneAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 result = -99;
                String phArea = String.Empty, phPre = String.Empty, phNumb = String.Empty, phone = String.Empty, phType = String.Empty;
                phType = this.ddlPhoneType.SelectedValue;
                phArea = this.txtUPDPhoneArea.Text;
                phPre = this.txtUPDPhonePrefix.Text;
                phNumb = this.txtUPDPhoneNumber.Text;
                if (String.IsNullOrEmpty(phType) || String.IsNullOrEmpty(phArea) || String.IsNullOrEmpty(phPre) || String.IsNullOrEmpty(phNumb))
                {
                    this.phEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iPhone.Attributes["class"] = "icon fa fa-warning";
                    this.spnPhone.InnerText = " Warning";
                    this.lblPhoneSuccess.Text = "Please provide all values!";
                    this.phEnclosure.Visible = true;
                }
                else
                {
                    phone = phArea + phPre + phNumb;

                    result = USR.updateProfilePhone(LoginName, phType, phone);
                    if (result.Equals(1))
                    {
                        this.phEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iPhone.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnPhone.InnerText = " Success";
                        this.lblPhoneSuccess.Text = "Successfully updated Profile's Phone Number.";
                        this.phEnclosure.Visible = true;
                        String phTypeName = String.Empty;
                        UP usrP = UP.GetUserProfile(LoginName);
                        String pT = Convert.ToString(usrP.upPhoneType);
                        if (!String.IsNullOrEmpty(pT))
                        {
                            Int32 phDmg = Convert.ToInt32(usrP.upPhoneType);
                            phTypeName = DMG.getDemogTypeName(phDmg);
                        }
                        else
                        {
                            phTypeName = "---";
                        }
                        phone = Convert.ToString(usrP.upPhone);
                        this.txtPhone.Text = "(" + phone.Substring(0, 3) + ") " + phone.Substring(3, 3) + "-" + phone.Substring(6, 4) + " (" + phTypeName + ")";
                    }
                    else
                    {
                        this.phEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iPhone.Attributes["class"] = "icon fa fa-warning";
                        this.spnPhone.InnerText = " Warning";
                        this.lblPhoneSuccess.Text = "Unable to update Profile's Phone number. Please try again";
                        this.phEnclosure.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnPhoneAddClear_Click(Object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> phTypes = DMG.getDemogTypeList();
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataSource = phTypes;
                this.ddlPhoneType.DataTextField = "Value";
                this.ddlPhoneType.DataValueField = "Key";
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.txtUPDPhoneArea.Text = "";
                this.txtUPDPhoneArea.Enabled = false;
                this.txtUPDPhonePrefix.Text = "";
                this.txtUPDPhonePrefix.Enabled = false;
                this.txtUPDPhoneNumber.Text = "";
                this.txtUPDPhoneNumber.Enabled = false;
                this.phEnclosure.Visible = false;
                this.btnPhoneAdd.Enabled = false;
                this.btnPhoneAdd.CssClass = "btn btn-default";
                this.btnPhoneAddClear.Enabled = false;
                this.btnPhoneAddClear.CssClass = "btn btn-default";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnPhoneDone_Click(Object sender, EventArgs e)
        {
            try
            {
                this.btnPhoneAddClear_Click(null, null);
                this.btnPhoneReset_Click(null, null);                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlPhoneType_SelectedIndexChanged(Object sender, EventArgs e)
        {
            try
            {
                this.txtUPDPhoneArea.Text = "";
                this.txtUPDPhoneArea.Enabled = true;
                this.txtUPDPhonePrefix.Text = "";
                this.txtUPDPhonePrefix.Enabled = true;
                this.txtUPDPhoneNumber.Text = "";
                this.txtUPDPhoneNumber.Enabled = true;
                this.txtUPDPhoneArea.Focus();
                this.btnPhoneAdd.Enabled = true;
                this.btnPhoneAdd.CssClass = "btn btn-success";
                this.btnPhoneAddClear.Enabled = true;
                this.btnPhoneAddClear.CssClass = "btn btn-warning";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAddressType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnAddressUpdate.Enabled = true;
                this.btnAddressUpdate.CssClass = "btn btn-success";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlProfileReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 rg = Convert.ToInt32(this.ddlProfileReg.SelectedValue);
                Dictionary<Int32, String> srList = DMG.getSubRegionList(rg);
                this.addEnclosure.Visible = false;
                this.ddlProfileSReg.Items.Clear();
                this.ddlProfileSReg.DataSource = srList;
                this.ddlProfileSReg.DataTextField = "Value";
                this.ddlProfileSReg.DataValueField = "key";
                this.ddlProfileSReg.DataBind();
                this.ddlProfileSReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlProfileSReg.SelectedIndex = -1;
                this.ddlProfileSReg.Enabled = true;
                this.ddlProfileCountry.Items.Clear();
                this.ddlProfileCountry.DataBind();
                this.ddlProfileCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlProfileCountry.SelectedIndex = -1;
                this.ddlProfileCountry.Enabled = false;
                this.ddlProfileState.Items.Clear();
                this.ddlProfileState.DataBind();
                this.ddlProfileState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlProfileState.SelectedIndex = -1;
                this.ddlProfileState.Enabled = false;
                this.ddlProfileCounty.Items.Clear();
                this.ddlProfileCounty.DataBind();
                this.ddlProfileCounty.Items.Insert(0, new ListItem("--- Select County/District ---", "-1"));
                this.ddlProfileCounty.SelectedIndex = -1;
                this.ddlProfileCounty.Enabled = false;
                this.ddlProfileCity.Items.Clear();
                this.ddlProfileCity.DataBind();
                this.ddlProfileCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlProfileCity.SelectedIndex = -1;
                this.ddlProfileCity.Enabled = false;
                this.ddlProfileZip.Items.Clear();
                this.ddlProfileZip.DataBind();
                this.ddlProfileZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlProfileZip.SelectedIndex = -1;
                this.ddlProfileZip.Enabled = false;
                this.txtSt1.Text = "";
                this.txtSt1.Enabled = false;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = false;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = false;
                this.ddlProfileSReg.Focus();
                this.btnAddressClear.Enabled = true;
                this.btnAddressClear.CssClass = "btn btn-warning";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlProfileSReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlProfileSReg.SelectedValue);
                Dictionary<Int32, String> ctyList = DMG.getCountryList(srID);
                this.addEnclosure.Visible = false;
                this.ddlProfileCountry.Items.Clear();
                this.ddlProfileCountry.DataSource = ctyList;
                this.ddlProfileCountry.DataTextField = "Value";
                this.ddlProfileCountry.DataValueField = "Key";
                this.ddlProfileCountry.DataBind();
                this.ddlProfileCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlProfileCountry.SelectedIndex = -1;
                this.ddlProfileCountry.Enabled = true;
                this.ddlProfileState.Items.Clear();
                this.ddlProfileState.DataBind();
                this.ddlProfileState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlProfileState.SelectedIndex = -1;
                this.ddlProfileState.Enabled = false;
                this.ddlProfileCounty.Items.Clear();
                this.ddlProfileCounty.DataBind();
                this.ddlProfileCounty.Items.Insert(0, new ListItem("--- Select County/District ---", "-1"));
                this.ddlProfileCounty.SelectedIndex = -1;
                this.ddlProfileCounty.Enabled = false;
                this.ddlProfileCity.Items.Clear();
                this.ddlProfileCity.DataBind();
                this.ddlProfileCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlProfileCity.SelectedIndex = -1;
                this.ddlProfileCity.Enabled = false;
                this.ddlProfileZip.Items.Clear();
                this.ddlProfileZip.DataBind();
                this.ddlProfileZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlProfileZip.SelectedIndex = -1;
                this.ddlProfileZip.Enabled = false;
                this.txtSt1.Text = "";
                this.txtSt1.Enabled = false;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = false;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = false;

                this.ddlProfileCountry.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlProfileCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctyID = Convert.ToInt32(this.ddlProfileCountry.SelectedValue);
                Dictionary<Int32, String> stList = DMG.getStateList(ctyID);
                this.addEnclosure.Visible = false;
                this.ddlProfileState.Items.Clear();
                this.ddlProfileState.DataSource = stList;
                this.ddlProfileState.DataTextField = "Value";
                this.ddlProfileState.DataValueField = "Key";
                this.ddlProfileState.DataBind();
                this.ddlProfileState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlProfileState.SelectedIndex = -1;
                this.ddlProfileState.Enabled = true;
                this.ddlProfileCounty.Items.Clear();
                this.ddlProfileCounty.DataBind();
                this.ddlProfileCounty.Items.Insert(0, new ListItem("--- Select County/District ---", "-1"));
                this.ddlProfileCounty.SelectedIndex = -1;
                this.ddlProfileCounty.Enabled = false;
                this.ddlProfileCity.Items.Clear();
                this.ddlProfileCity.DataBind();
                this.ddlProfileCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlProfileCity.SelectedIndex = -1;
                this.ddlProfileCity.Enabled = false;
                this.ddlProfileZip.Items.Clear();
                this.ddlProfileZip.DataBind();
                this.ddlProfileZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlProfileZip.SelectedIndex = -1;
                this.ddlProfileZip.Enabled = false;
                this.txtSt1.Text = "";
                this.txtSt1.Enabled = false;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = false;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = false;

                this.ddlProfileState.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlProfileState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 stID = Convert.ToInt32(this.ddlProfileState.SelectedValue);
                Dictionary<Int32, String> ctyList = DMG.getCountyList(stID);
                this.addEnclosure.Visible = false;
                this.ddlProfileCounty.Items.Clear();
                this.ddlProfileCounty.DataSource = ctyList;
                this.ddlProfileCounty.DataTextField = "Value";
                this.ddlProfileCounty.DataValueField = "Key";
                this.ddlProfileCounty.DataBind();
                this.ddlProfileCounty.Items.Insert(0, new ListItem("--- Select County/District ---", "-1"));
                this.ddlProfileCounty.SelectedIndex = -1;
                this.ddlProfileCounty.Enabled = true;
                this.ddlProfileCity.Items.Clear();
                this.ddlProfileCity.DataBind();
                this.ddlProfileCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlProfileCity.SelectedIndex = -1;
                this.ddlProfileCity.Enabled = false;
                this.ddlProfileZip.Items.Clear();
                this.ddlProfileZip.DataBind();
                this.ddlProfileZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlProfileZip.SelectedIndex = -1;
                this.ddlProfileZip.Enabled = false;
                this.txtSt1.Text = "";
                this.txtSt1.Enabled = false;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = false;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = false;

                this.ddlProfileCounty.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlProfileCounty_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctyID = Convert.ToInt32(this.ddlProfileCounty.SelectedValue);
                Dictionary<Int32, String> citList = DMG.getCityList(ctyID);
                this.addEnclosure.Visible = false;
                this.ddlProfileCity.Items.Clear();
                this.ddlProfileCity.DataSource = citList;
                this.ddlProfileCity.DataTextField = "Value";
                this.ddlProfileCity.DataValueField = "Key";
                this.ddlProfileCity.DataBind();
                this.ddlProfileCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlProfileCity.SelectedIndex = -1;
                this.ddlProfileCity.Enabled = true;
                this.ddlProfileZip.Items.Clear();
                this.ddlProfileZip.DataBind();
                this.ddlProfileZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlProfileZip.SelectedIndex = -1;
                this.ddlProfileZip.Enabled = false;
                this.txtSt1.Text = "";
                this.txtSt1.Enabled = false;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = false;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = false;

                this.ddlProfileCity.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlProfileCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctyID = Convert.ToInt32(this.ddlProfileCounty.SelectedValue);
                Dictionary<Int32, String> zipList = DMG.getZipList(ctyID);
                this.addEnclosure.Visible = false;
                this.ddlProfileZip.Items.Clear();
                this.ddlProfileZip.DataSource = zipList;
                this.ddlProfileZip.DataTextField = "Value";
                this.ddlProfileZip.DataValueField = "Key";
                this.ddlProfileZip.DataBind();
                this.ddlProfileZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlProfileZip.SelectedIndex = -1;
                this.ddlProfileZip.Enabled = true;
                this.txtSt1.Text = "";
                this.txtSt1.Enabled = false;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = false;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = false;

                this.ddlProfileZip.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlProfileZip_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.addEnclosure.Visible = false;
                this.txtSt1.Text = "";
                this.txtSt1.Enabled = true;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = true;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = true;
                this.txtSt1.Focus();
                Dictionary<Int32, String> phTypes = DMG.getDemogTypeList();
                this.ddlAddressType.Items.Clear();
                this.ddlAddressType.DataSource = phTypes;
                this.ddlAddressType.DataTextField = "Value";
                this.ddlAddressType.DataValueField = "Key";
                this.ddlAddressType.DataBind();
                this.ddlAddressType.Items.Insert(0, new ListItem("--- Select Address Type ---", "-1"));
                this.ddlAddressType.SelectedIndex = -1;
                this.ddlAddressType.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddressUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 result = -99;
                String type = String.Empty, region = String.Empty, subRegion = String.Empty, country = String.Empty, state = String.Empty, county = String.Empty, city = String.Empty;
                String zip = String.Empty, st1 = String.Empty, st2 = String.Empty, st3 = String.Empty;
                type = this.ddlAddressType.SelectedValue;
                region = this.ddlProfileReg.SelectedValue;
                subRegion = this.ddlProfileSReg.SelectedValue;
                country = this.ddlProfileCountry.SelectedValue;
                state = this.ddlProfileState.SelectedValue;
                county = this.ddlProfileCounty.SelectedValue;
                city = this.ddlProfileCity.SelectedValue;
                zip = this.ddlProfileZip.SelectedValue;
                st1 = this.txtSt1.Text;
                st2 = this.txtSt2.Text;
                st3 = this.txtSt3.Text;
                if (String.IsNullOrEmpty(st1))
                {
                    this.addEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iAddress.Attributes["class"] = "icon fa fa-warning";
                    this.spnAddress.InnerText = " Warning";
                    this.lblAddressInfo.Text = "Please Provide all values!";
                    this.addEnclosure.Visible = true;
                }
                else
                {
                    result = USR.updateAddressProfile(LoginName, region, subRegion, country, state, county, city, zip, st1, st2, st3, type);
                    if (result.Equals(1))
                    {
                        this.addEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iAddress.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnAddress.InnerText = " Warning";
                        this.lblAddressInfo.Text = "Successfully edited/updated Profile address.";
                        this.addEnclosure.Visible = true;
                    }
                    else
                    {
                        this.addEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iAddress.Attributes["class"] = "icon fa fa-warning";
                        this.spnAddress.InnerText = " Warning";
                        this.lblAddressInfo.Text = "Problem updating User's Contact info. <br />Please contact support for Updating Contact Information.";
                        this.addEnclosure.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddressClear_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> regList = DMG.getRegionList();
                this.addEnclosure.Visible = false;
                this.ddlProfileReg.Items.Clear();
                this.ddlProfileReg.DataSource = regList;
                this.ddlProfileReg.DataTextField = "Value";
                this.ddlProfileReg.DataValueField = "Key";
                this.ddlProfileReg.DataBind();
                this.ddlProfileReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlProfileReg.SelectedIndex = -1;
                this.ddlProfileReg.Enabled = true;
                this.ddlProfileSReg.Items.Clear();
                this.ddlProfileSReg.DataBind();
                this.ddlProfileSReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlProfileSReg.SelectedIndex = -1;
                this.ddlProfileSReg.Enabled = false;
                this.ddlProfileCountry.Items.Clear();
                this.ddlProfileCountry.DataBind();
                this.ddlProfileCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlProfileCountry.SelectedIndex = -1;
                this.ddlProfileCountry.Enabled = false;
                this.ddlProfileState.Items.Clear();
                this.ddlProfileState.DataBind();
                this.ddlProfileState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlProfileState.SelectedIndex = -1;
                this.ddlProfileState.Enabled = false;
                this.ddlProfileCounty.Items.Clear();
                this.ddlProfileCounty.DataBind();
                this.ddlProfileCounty.Items.Insert(0, new ListItem("--- Select County/District ---", "-1"));
                this.ddlProfileCounty.SelectedIndex = -1;
                this.ddlProfileCounty.Enabled = false;
                this.ddlProfileCity.Items.Clear();
                this.ddlProfileCity.DataBind();
                this.ddlProfileCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlProfileCity.SelectedIndex = -1;
                this.ddlProfileCity.Enabled = false;
                this.ddlProfileZip.Items.Clear();
                this.ddlProfileZip.DataBind();
                this.ddlProfileZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlProfileZip.SelectedIndex = -1;
                this.ddlProfileZip.Enabled = false;
                this.txtSt1.Text = "";
                this.txtSt1.Enabled = false;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = false;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = false;
                this.ddlAddressType.Items.Clear();
                this.ddlAddressType.DataBind();
                this.ddlAddressType.Items.Insert(0, new ListItem("--- Select Address Type ---", "-1"));
                this.ddlAddressType.SelectedIndex = -1;
                this.ddlAddressType.Enabled = false;
                this.btnAddressUpdate.Enabled = false;
                this.btnAddressUpdate.CssClass = "btn btn-default";
                this.btnAddressClear.Enabled = false;
                this.btnAddressClear.CssClass = "btn btn-default";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddressCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnAddressClear_Click(null, null);
                this.btnProfileAddressReset_Click(null, null);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlUpdGender_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnConfirmProfileUpdate.Enabled = true;
                this.btnConfirmProfileUpdate.CssClass = "btn btn-success";
                this.btnClearProfileUpdate.Enabled = true;
                this.btnClearProfileUpdate.CssClass = "btn btn-warning";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }                              
    }
}