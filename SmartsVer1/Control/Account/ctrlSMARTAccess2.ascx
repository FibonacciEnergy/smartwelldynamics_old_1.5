﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlSMARTAccess2.ascx.cs" Inherits="SmartsVer1.Control.Account.ctrlSMARTAccess2" %>
<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
<!-- Stylesheet -->
<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<!-- Javascript Libraries -->
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<script src="../../Scripts/jquery-3.3.1.min.js"></script>
<!-- local page style -->
<style type="text/css">
    a {
        color: #ffffff;
    }

        a:hover, a:active, a:focus {
            outline: none;
            text-decoration: none;
            color: #ffffff;
        }
</style>
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>

<asp:UpdateProgress ID="upgrsAccess" runat="server" AssociatedUpdatePanelID="upnlAccess" DisplayAfter="0">
    <ProgressTemplate>
        <div id="divLogInProgresss" runat="server" class="updateProgress">
            <asp:Image ID="imgLogProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<asp:UpdatePanel runat="server" ID="upnlAccess">
    <ContentTemplate>
        <div class="row bg-black">
            <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
                <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 align-left">
                <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
            </div>
        </div>
        <div class="row bg-white">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"  style="margin-top:150px;">
                <div class="panel panel-default">
                    <div class="col-sm-5 col-sm-offset-5">
                        <div class="panel-body">
                            <div runat="server" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <asp:Login ID="Login" runat="server" UserNameLabelText="Email:" OnLoggingIn="Login_LoggingIn"
                                    OnLoginError="Login_LoginError" CreateUserText="Not Registered yet? Please Contact Us to Sign up!" CreateUserUrl="~/Support/ContactUs2.aspx">
                                    <LayoutTemplate>
                                        <table class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <tr>
                                                <td>
                                                    <div class="form-group has-feedback">
                                                        <asp:TextBox ID="UserName" runat="server" CssClass="form-control input-lg" placeholder="Username"></asp:TextBox><span
                                                            class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                                            ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="ctl00$Login">*</asp:RequiredFieldValidator>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="form-group has-feedback">
                                                        <asp:TextBox ID="Password" runat="server" CssClass="form-control input-lg" TextMode="Password" placeholder="Password"></asp:TextBox><span
                                                            class="glyphicon glyphicon-lock form-control-feedback"></span>
                                                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                                            ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="ctl00$Login">*</asp:RequiredFieldValidator>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <asp:Literal ID="FailureText" runat="server"></asp:Literal>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="checkbox">
                                                            <asp:CheckBox ID="RememberMe" runat="server" Text="Remember me next time." />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <asp:Button ID="LoginButton" runat="server" CommandName="Login" CssClass="btn btn-primary btn-block btn-flat text-center"
                                                            Text="Log In" ValidationGroup="ctl00$Login" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="fa-2x text-center">
                                                        <asp:Label ID="Label1" runat="server" Text="Not Registered yet?" ForeColor="CadetBlue"></asp:Label><br />
                                                        <a id="A1" runat="server" href="~/Support/ContactUs2.aspx" style="color: cadetblue">Contact Us to Sign-up</a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </LayoutTemplate>
                                </asp:Login>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>

