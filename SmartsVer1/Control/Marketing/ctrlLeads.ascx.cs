﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Net.Mail;
using System.Web.Configuration;
using System.Web.Security;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using USR = SmartsVer1.Helpers.AccountHelpers.CreateUser;
using LD = SmartsVer1.Helpers.SalesHelpers.Leads;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using MAP = SmartsVer1.Helpers.ChartHelpers.MapBox;
using CLR = System.Drawing.Color;
using System.Web.UI.WebControls;

namespace SmartsVer1.Control.Marketing
{
    public partial class ctrlLeads : System.Web.UI.UserControl
    {
        Int32 ClientID = -99, feID = 1;
        String LoginName, username, domain, GetClientDBString;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);
                UP usrP = UP.GetUserProfile(LoginName);
                ClientID = Convert.ToInt32(usrP.clntID);
                username = addr.User;
                domain = addr.Host;
                GetClientDBString = clntConn.getClientDBConnection(domain);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
                //Assigning Datasources
                System.Data.DataTable compTable = LD.getCompaniesTable();
                this.gvwCoList.DataSource = compTable;
                this.gvwCoList.PageIndex = 0;
                this.gvwCoList.SelectedIndex = -1;
                this.gvwCoList.DataBind();
                this.gvwLeadCompany.DataSource = compTable;
                this.gvwLeadCompany.PageIndex = 0;
                this.gvwLeadCompany.SelectedIndex = -1;
                this.gvwLeadCompany.DataBind();
                this.gvwCommCompany.DataSource = compTable;
                this.gvwCommCompany.PageIndex = 0;
                this.gvwCommCompany.SelectedIndex = -1;
                this.gvwCommCompany.DataBind();
                Dictionary<Int32, String> compList = LD.getCompaniesListWithCount();
                this.ddlLPACompany.Items.Clear();
                this.ddlLPACompany.DataSource = compList;
                this.ddlLPACompany.DataTextField = "Value";
                this.ddlLPACompany.DataValueField = "Key";
                this.ddlLPACompany.DataBind();
                this.ddlLPACompany.Items.Insert(0, new ListItem("--- Select Company Name ---", "-1"));
                this.ddlLPACompany.SelectedIndex = -1;
                this.ddlNEmlCompany.DataSource = compList;
                this.ddlNEmlCompany.DataTextField = "Value";
                this.ddlNEmlCompany.DataValueField = "Key";
                this.ddlNEmlCompany.DataBind();
                this.ddlNEmlCompany.SelectedIndex = -1;
                this.ddlNPhCompany.DataSource = compList;
                this.ddlNPhCompany.DataTextField = "Value";
                this.ddlNPhCompany.DataValueField = "Key";
                this.ddlNPhCompany.DataBind();
                this.ddlNPhCompany.SelectedIndex = -1;
                Dictionary<Int32, String> companyList = LD.getCompaniesList();
                this.ddlCACompany.Items.Clear();
                this.ddlCACompany.DataSource = companyList;
                this.ddlCACompany.DataTextField = "Value";
                this.ddlCACompany.DataValueField = "Key";
                this.ddlCACompany.DataBind();
                this.ddlCACompany.Items.Insert(0, new ListItem("--- Select Company Name ---", "-1"));
                this.ddlCACompany.SelectedIndex = -1;                
                Dictionary<Int32, String> rgList = DMG.getRegionList();
                this.clndrTimeStamp.SelectedDate = DateTime.Now;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        /// <summary>
        /// Lead Companies
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvwCoList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {                
                System.Data.DataTable coList = LD.getCompaniesTable();
                this.gvwCoList.PageIndex = e.NewPageIndex;
                this.gvwCoList.DataSource = coList;
                this.gvwCoList.DataBind();
                this.gvwCoList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwCoList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 coID = Convert.ToInt32(this.gvwCoList.SelectedValue);
                System.Data.DataTable addTable = LD.getCompaniesAddressTable(coID);
                this.gvwCoAddress.DataSource = addTable;
                this.gvwCoAddress.PageIndex = 0;
                this.gvwCoAddress.SelectedIndex = -1;
                this.gvwCoAddress.DataBind();
                this.gvwCoAddress.Visible = true;
                this.btnLeadReset.CssClass = "btn btn-warning text-center";
                this.btnLeadReset.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnLeadNew_Click(object sender, EventArgs e)
        {
            try
            {
                this.divCoList.Visible = false;
                this.divLCoAdd.Visible = true;
                this.divAddCoAddress.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnLeadCancel_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable compTable = LD.getCompaniesTable();
                this.gvwCoList.DataSource = compTable;
                this.gvwCoList.PageIndex = 0;
                this.gvwCoList.SelectedIndex = -1;
                this.gvwCoList.DataBind();
                this.gvwCoAddress.DataSource = null;
                this.gvwCoAddress.DataBind();
                this.gvwCoAddress.Visible = false;
                this.btnLeadReset.CssClass = "btn btn-default text-center";
                this.btnLeadReset.Enabled = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewCo_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, ifol = -99, duplicate = -99;
                String name = String.Empty, wU = String.Empty, lU = String.Empty;
                name = Convert.ToString(this.txtCoName.Text);
                wU = Convert.ToString(this.txtWUrl.Text);
                lU = Convert.ToString(this.txtLUrl.Text);
                ifol = Convert.ToInt32(this.ddlFollow.SelectedValue);
                if (String.IsNullOrEmpty(name))
                {
                    this.txtCoName.Text = "";
                    this.txtWUrl.Text = "";
                    this.txtLUrl.Text = "";
                    Dictionary<Int32, String> fsList = LD.getInFollowList();
                    this.ddlFollow.Items.Clear();
                    this.ddlFollow.DataSource = fsList;
                    this.ddlFollow.DataTextField = "Value";
                    this.ddlFollow.DataValueField = "Key";
                    this.ddlFollow.DataBind();
                    this.ddlFollow.Items.Insert(0, new ListItem("--- Select LinkedIn Follow Status ---", "-1"));
                    this.ddlFollow.SelectedIndex = -1;
                    this.enLeads.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iLead.Attributes["class"] = "icon fa fa-warning";
                    this.spnLead.InnerText = " Warning!";
                    this.lblCoAddSuccess.Text = "!!! Error !!! Null values detected. Please provide all values";
                    this.enLeads.Visible = true;
                }
                else
                {
                    if (String.IsNullOrEmpty(wU))
                    {
                        wU = "---";
                    }
                    if (String.IsNullOrEmpty(lU))
                    {
                        lU = "---";
                    }
                    duplicate = LD.findDuplicateLeadCompany(name, wU);
                    if (duplicate.Equals(0))
                    {
                        iResult = LD.insertNewSMLeadCompany(name, wU, lU, ifol, LoginName, domain);
                        if (iResult.Equals(1))
                        {
                            this.txtCoName.Text = "";
                            this.txtWUrl.Text = "";
                            this.txtLUrl.Text = "";
                            Dictionary<Int32, String> fsList = LD.getInFollowList();
                            this.ddlFollow.Items.Clear();
                            this.ddlFollow.DataSource = fsList;
                            this.ddlFollow.DataTextField = "Value";
                            this.ddlFollow.DataValueField = "Key";
                            this.ddlFollow.DataBind();
                            this.ddlFollow.Items.Insert(0, new ListItem("--- Select LinkedIn Follow Status ---", "-1"));
                            this.ddlFollow.SelectedIndex = -1;
                            this.enLeads.Attributes["class"] = "alert alert-success alert-dismissable";
                            this.iLead.Attributes["class"] = "fa fa-check-circle";
                            this.spnLead.InnerText = " Success!";
                            this.lblCoAddSuccess.Text = "Successfully inserted Company Name into database";
                            this.enLeads.Visible = true;
                        }
                        else
                        {
                            this.txtCoName.Text = "";
                            this.txtWUrl.Text = "";
                            this.txtLUrl.Text = "";
                            Dictionary<Int32, String> fsList = LD.getInFollowList();
                            this.ddlFollow.Items.Clear();
                            this.ddlFollow.DataSource = fsList;
                            this.ddlFollow.DataTextField = "Value";
                            this.ddlFollow.DataValueField = "Key";
                            this.ddlFollow.DataBind();
                            this.ddlFollow.Items.Insert(0, new ListItem("--- Select LinkedIn Follow Status ---", "-1"));
                            this.ddlFollow.SelectedIndex = -1;
                            this.enLeads.Attributes["class"] = "alert alert-warning alert-dismissable";
                            this.iLead.Attributes["class"] = "fa fa-warning";
                            this.spnLead.InnerText = " Warning!";
                            this.lblCoAddSuccess.Text = "!!! Error !!! Unable to insert Company. Please provide a valid Company Name";
                            this.enLeads.Visible = true;
                        }
                    }
                    else if (duplicate.Equals(-1))
                    {
                        this.txtCoName.Text = "";
                        this.txtWUrl.Text = "";
                        this.txtLUrl.Text = "";
                        Dictionary<Int32, String> fsList = LD.getInFollowList();
                        this.ddlFollow.Items.Clear();
                        this.ddlFollow.DataSource = fsList;
                        this.ddlFollow.DataTextField = "Value";
                        this.ddlFollow.DataValueField = "Key";
                        this.ddlFollow.DataBind();
                        this.ddlFollow.Items.Insert(0, new ListItem("--- Select LinkedIn Follow Status ---", "-1"));
                        this.ddlFollow.SelectedIndex = -1;
                        this.enLeads.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iLead.Attributes["class"] = "icon fa fa-warning";
                        this.spnLead.InnerText = " Warning!";
                        this.lblCoAddSuccess.Text = "!!! Error !!! Found Duplicate Web URL";
                        this.enLeads.Visible = true;
                    }
                    else if (duplicate.Equals(-2))
                    {
                        this.txtCoName.Text = "";
                        this.txtWUrl.Text = "";
                        this.txtLUrl.Text = "";
                        Dictionary<Int32, String> fsList = LD.getInFollowList();
                        this.ddlFollow.Items.Clear();
                        this.ddlFollow.DataSource = fsList;
                        this.ddlFollow.DataTextField = "Value";
                        this.ddlFollow.DataValueField = "Key";
                        this.ddlFollow.DataBind();
                        this.ddlFollow.Items.Insert(0, new ListItem("--- Select LinkedIn Follow Status ---", "-1"));
                        this.ddlFollow.SelectedIndex = -1;
                        this.enLeads.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iLead.Attributes["class"] = "icon fa fa-warning";
                        this.spnLead.InnerText = " Warning!";
                        this.lblCoAddSuccess.Text = "!!! Error !!! Found Duplicate Lead Company";
                        this.enLeads.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtCoName.Text = "";
                this.txtWUrl.Text = "";
                this.txtLUrl.Text = "";
                Dictionary<Int32, String> fsList = LD.getInFollowList();
                this.ddlFollow.Items.Clear();
                this.ddlFollow.DataSource = fsList;
                this.ddlFollow.DataTextField = "Value";
                this.ddlFollow.DataValueField = "Key";
                this.ddlFollow.DataBind();
                this.ddlFollow.Items.Insert(0, new ListItem("--- Select LinkedIn Follow Status ---", "-1"));
                this.ddlFollow.SelectedIndex = -1;
                this.lblCoAddSuccess.Text = "";
                this.enLeads.Visible = false;
                this.btnAddNewCo.Enabled = false;
                this.btnAddNewCo.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewDone_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable coList = LD.getCompaniesTable();
                this.gvwCoList.DataSource = coList;
                this.gvwCoList.PageIndex = 0;
                this.gvwCoList.SelectedIndex = -1;
                this.gvwCoList.DataBind();
                this.gvwCoList.Visible = true;
                this.btnAddNewClear_Click(null, null);
                this.divCoList.Visible = true;
                this.divLCoAdd.Visible = false;
                this.divAddCoAddress.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        /// <summary>
        /// Lead Company Address
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>                

        protected void gvwCoAddress_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 coID = Convert.ToInt32(this.gvwCoList.SelectedValue);
                System.Data.DataTable addTable = LD.getCompaniesAddressTable(coID);
                this.gvwCoAddress.DataSource = addTable;
                this.gvwCoAddress.PageIndex = e.NewPageIndex;
                this.gvwCoAddress.SelectedIndex = -1;
                this.gvwCoAddress.DataBind();
                this.gvwCoAddress.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddCo_Click(object sender, EventArgs e)
        {
            try
            {
                this.divCoList.Visible = false;
                this.divLCoAdd.Visible = false;
                this.divAddCoAddress.Visible = true;
            }            
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }               

        protected void ddlFollow_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnAddNewCo.Enabled = true;
                this.btnAddNewCo.CssClass = "btn btn-success text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        protected void btnAddNP_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnCancelNP_Click(null, null);
                    
                this.divAddNewAcc.Visible = true;
                this.divLeadEmail.Visible = false;
                this.divLeadPhone.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNPEmail_Click(object sender, EventArgs e)
        {
            try
            {
                btnCancelNP_Click(null, null);
                this.divAccList.Visible = false;
                this.divAddNewAcc.Visible = false;
                this.divLeadEmail.Visible = true;
                this.divLeadPhone.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNPPhone_Click(object sender, EventArgs e)
        {
            try
            {
                btnCancelNP_Click(null, null);
                this.divAccList.Visible = false;
                this.divAddNewAcc.Visible = false;
                this.divLeadEmail.Visible = false;
                this.divLeadPhone.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCancelNP_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable coTable = LD.getCompaniesTable();
                this.gvwLeadCompany.DataSource = coTable;
                this.gvwLeadCompany.PageIndex = 0;
                this.gvwLeadCompany.SelectedIndex = -1;
                this.gvwLeadCompany.DataBind();
                this.gvwLeadPerson.DataSource = null;
                this.gvwLeadPerson.DataBind();
                this.gvwLeadPerson.Visible = false;
                this.gvwPersonAddress.DataSource = null;
                this.gvwPersonAddress.DataBind();
                this.gvwPersonAddress.Visible = false;
                this.gvwLeadEmail.DataSource = null;
                this.gvwLeadEmail.DataBind();
                this.gvwLeadEmail.Visible = false;
                this.gvwLeadPhone.DataSource = null;
                this.gvwLeadPhone.DataBind();
                this.gvwLeadPhone.Visible = false;
                this.btnCancelNP.Enabled = false;
                this.btnCancelNP.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewLead_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, coID = -99, cnID = -99;
                String fN = String.Empty, mN = String.Empty, lN = String.Empty, desig = String.Empty, lnkProf = String.Empty;
                fN = Convert.ToString(this.txtLPAFName.Text);
                mN = Convert.ToString(this.txtLPAMName.Text);
                lN = Convert.ToString(this.txtLPALName.Text);
                desig = Convert.ToString(this.txtLPADesig.Text);
                coID = Convert.ToInt32(this.ddlLPACompany.SelectedValue);
                lnkProf = Convert.ToString(this.txtLPAIn.Text);
                cnID = Convert.ToInt32(this.ddlLPAInConnected.SelectedValue);
                if (String.IsNullOrEmpty(fN) || String.IsNullOrEmpty(lN))
                {
                    this.enNL.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iNL.Attributes["class"] = "icon fa fa-warning";
                    this.spnNL.InnerText = " Warning!";
                    this.lblNLSuccess.Text = "!!! Error !!! Missing values detected. Please provide all values";
                    this.enNL.Visible = true;
                    this.btnAddNewLead.Enabled = false;
                    this.btnAddNewLead.CssClass = "btn btn-default text-center";
                }
                else
                {
                    if (String.IsNullOrEmpty(desig))
                    {
                        desig = "---";
                    }
                    if (String.IsNullOrEmpty(lnkProf))
                    {
                        lnkProf = "---";
                        cnID = 8; //Profile not found on LinkedIn
                    }
                    iResult = LD.insertNewLeadPerson(fN, mN, lN, desig, coID, lnkProf, cnID, LoginName, domain);
                    if (iResult.Equals(1))
                    {
                        btnAddNewLeadClear_Click(null, null);
                        this.enNL.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iNL.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnNL.InnerText = " Success!";
                        this.lblNLSuccess.Text = "Successfully inserted new Lead in SMARTs";
                        this.enNL.Visible = true;
                        this.btnAddNewLead.Enabled = false;
                        this.btnAddNewLead.CssClass = "btn btn-default text-center";
                    }
                    else
                    {
                        btnAddNewLeadClear_Click(null, null);
                        this.enNL.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iNL.Attributes["class"] = "icon fa fa-warning";
                        this.spnNL.InnerText = " Warning!";
                        this.lblNLSuccess.Text = "!!! Error !!! Please try again";
                        this.enNL.Visible = true;
                        this.btnAddNewLead.Enabled = false;
                        this.btnAddNewLead.CssClass = "btn btn-default text-center";
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewLeadClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtLPAFName.Text = "";
                this.txtLPAMName.Text = "";
                this.txtLPALName.Text = "";
                this.txtLPADesig.Text = "";
                Dictionary<Int32, String> compList = LD.getCompaniesList();
                this.ddlLPACompany.Items.Clear();
                this.ddlLPACompany.DataSource = compList;
                this.ddlLPACompany.DataTextField = "Value";
                this.ddlLPACompany.DataValueField = "Key";
                this.ddlLPACompany.DataBind();
                this.ddlLPACompany.Items.Insert(0, new ListItem("--- Select Company Name ---", "-1"));
                this.ddlLPACompany.SelectedIndex = -1;
                this.txtLPAIn.Text = "";
                this.txtLPAIn.Enabled = false;
                this.ddlLPAInConnected.Items.Clear();
                this.ddlLPAInConnected.DataBind();
                this.ddlLPAInConnected.Items.Insert(0, new ListItem("--- Select LinkedIn Connection Status ---", "-1"));
                this.ddlLPAInConnected.SelectedIndex = -1;
                this.ddlLPAInConnected.Enabled = false;
                this.enNL.Visible = false;
                this.lblNLSuccess.Text = "";
                this.btnAddNewLeadClear.Enabled = false;
                this.btnAddNewLeadClear.CssClass = "btn btn-default text-center";
                this.btnAddNewLead.Enabled = false;
                this.btnAddNewLead.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewLeadDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnAddNewLeadClear_Click(null, null);
                this.divAccList.Visible = true;
                this.divAddNewAcc.Visible = false;
                this.divLeadEmail.Visible = false;
                this.divLeadPhone.Visible = false;
                System.Data.DataTable coTable = LD.getCompaniesTable();
                this.gvwLeadCompany.DataSource = coTable;
                this.gvwLeadCompany.PageIndex = 0;
                this.gvwLeadCompany.SelectedIndex = -1;
                this.gvwLeadCompany.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlLPAInConnected_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnAddNewLead.Enabled = true;
                this.btnAddNewLead.CssClass = "btn btn-success text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlLPACompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtLPAIn.Text = "";
                this.txtLPAIn.Enabled = true;
                Dictionary<Int32, String> stt = LD.getLinkedConnectionStatusList();
                this.ddlLPAInConnected.Items.Clear();
                this.ddlLPAInConnected.DataSource = stt;
                this.ddlLPAInConnected.DataTextField = "Value";
                this.ddlLPAInConnected.DataValueField = "Key";
                this.ddlLPAInConnected.DataBind();
                this.ddlLPAInConnected.Items.Insert(0, new ListItem("--- Select LinkedIn Connection Status ---", "-1"));
                this.ddlLPAInConnected.SelectedIndex = -1;
                this.ddlLPAInConnected.Enabled = true;
                this.btnAddNewLeadClear.Enabled = true;
                this.btnAddNewLeadClear.CssClass = "btn btn-warning text-center";
                this.txtLPAIn.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwPAddList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCACompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> dmgList = DMG.getDemogTypeList();
                this.ddlCAType.Items.Clear();
                this.ddlCAType.DataSource = dmgList;
                this.ddlCAType.DataTextField = "Value";
                this.ddlCAType.DataValueField = "Key";
                this.ddlCAType.DataBind();
                this.ddlCAType.Items.Insert(0, new ListItem("--- Select Address Type ---", "-1"));
                this.ddlCAType.SelectedIndex = -1;
                this.ddlCAType.Enabled = true;
                this.ddlCARegion.Items.Clear();
                this.ddlCARegion.DataBind();
                this.ddlCARegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlCARegion.SelectedIndex = -1;
                this.ddlCARegion.Enabled = false;
                this.ddlCASRegion.Items.Clear();
                this.ddlCASRegion.DataBind();
                this.ddlCASRegion.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlCASRegion.SelectedIndex = -1;
                this.ddlCASRegion.Enabled = false;
                this.ddlCACountry.Items.Clear();
                this.ddlCACountry.DataBind();
                this.ddlCACountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlCACountry.SelectedIndex = -1;
                this.ddlCACountry.Enabled = false;
                this.ddlCAState.Items.Clear();
                this.ddlCAState.DataBind();
                this.ddlCAState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlCAState.SelectedIndex = -1;
                this.ddlCAState.Enabled = false;
                this.ddlCACounty.Items.Clear();
                this.ddlCACounty.DataBind();
                this.ddlCACounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlCACounty.SelectedIndex = -1;
                this.ddlCACounty.Enabled = false;
                this.ddlCACity.Items.Clear();
                this.ddlCACity.DataBind();
                this.ddlCACity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCACity.SelectedIndex = -1;
                this.ddlCACity.Enabled = false;
                this.ddlCAZip.Items.Clear();
                this.ddlCAZip.DataBind();
                this.ddlCAZip.Items.Insert(0, new ListItem("--- Select Zip/Postalcode ---", "-1"));
                this.ddlCAZip.SelectedIndex = -1;
                this.ddlCAZip.Enabled = false;
                this.txtCASt1.Text = "";
                this.txtCASt1.Enabled = false;
                this.txtCASt2.Text = "";
                this.txtCASt2.Enabled = false;
                this.txtCASt3.Text = "";
                this.txtCASt3.Enabled = false;
                this.txtCATitle.Text = "";
                this.txtCATitle.Enabled = false;
                this.ddlCAType.Focus();
                this.btnCAAClear.Enabled = true;
                this.btnCAAClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCAType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> rgList = DMG.getRegionList();
                this.ddlCARegion.Items.Clear();
                this.ddlCARegion.DataSource = rgList;
                this.ddlCARegion.DataTextField = "Value";
                this.ddlCARegion.DataValueField = "Key";
                this.ddlCARegion.DataBind();
                this.ddlCARegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlCARegion.SelectedIndex = -1;
                this.ddlCARegion.Enabled = true;
                this.ddlCASRegion.Items.Clear();
                this.ddlCASRegion.DataBind();
                this.ddlCASRegion.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlCASRegion.SelectedIndex = -1;
                this.ddlCASRegion.Enabled = false;
                this.ddlCACountry.Items.Clear();
                this.ddlCACountry.DataBind();
                this.ddlCACountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlCACountry.SelectedIndex = -1;
                this.ddlCACountry.Enabled = false;
                this.ddlCAState.Items.Clear();
                this.ddlCAState.DataBind();
                this.ddlCAState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlCAState.SelectedIndex = -1;
                this.ddlCAState.Enabled = false;
                this.ddlCACounty.Items.Clear();
                this.ddlCACounty.DataBind();
                this.ddlCACounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlCACounty.SelectedIndex = -1;
                this.ddlCACounty.Enabled = false;
                this.ddlCACity.Items.Clear();
                this.ddlCACity.DataBind();
                this.ddlCACity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCACity.SelectedIndex = -1;
                this.ddlCACity.Enabled = false;
                this.ddlCAZip.Items.Clear();
                this.ddlCAZip.DataBind();
                this.ddlCAZip.Items.Insert(0, new ListItem("--- Select Zip/Postalcode ---", "-1"));
                this.ddlCAZip.SelectedIndex = -1;
                this.ddlCAZip.Enabled = false;
                this.txtCASt1.Text = "";
                this.txtCASt1.Enabled = false;
                this.txtCASt2.Text = "";
                this.txtCASt2.Enabled = false;
                this.txtCASt3.Text = "";
                this.txtCASt3.Enabled = false;
                this.txtCATitle.Text = "";
                this.txtCATitle.Enabled = false;

                this.ddlCARegion.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCARegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 rID = Convert.ToInt32(this.ddlCARegion.SelectedValue);
                Dictionary<Int32, String> srList = DMG.getSubRegionList(rID);
                this.ddlCASRegion.Items.Clear();
                this.ddlCASRegion.DataSource = srList;
                this.ddlCASRegion.DataTextField = "Value";
                this.ddlCASRegion.DataValueField = "Key";
                this.ddlCASRegion.DataBind();
                this.ddlCASRegion.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlCASRegion.SelectedIndex = -1;
                this.ddlCASRegion.Enabled = true;
                this.ddlCACountry.Items.Clear();
                this.ddlCACountry.DataBind();
                this.ddlCACountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlCACountry.SelectedIndex = -1;
                this.ddlCACountry.Enabled = false;
                this.ddlCAState.Items.Clear();
                this.ddlCAState.DataBind();
                this.ddlCAState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlCAState.SelectedIndex = -1;
                this.ddlCAState.Enabled = false;
                this.ddlCACounty.Items.Clear();
                this.ddlCACounty.DataBind();
                this.ddlCACounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlCACounty.SelectedIndex = -1;
                this.ddlCACounty.Enabled = false;
                this.ddlCACity.Items.Clear();
                this.ddlCACity.DataBind();
                this.ddlCACity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCACity.SelectedIndex = -1;
                this.ddlCACity.Enabled = false;
                this.ddlCAZip.Items.Clear();
                this.ddlCAZip.DataBind();
                this.ddlCAZip.Items.Insert(0, new ListItem("--- Select Zip/Postalcode ---", "-1"));
                this.ddlCAZip.SelectedIndex = -1;
                this.ddlCAZip.Enabled = false;
                this.txtCASt1.Text = "";
                this.txtCASt1.Enabled = false;
                this.txtCASt2.Text = "";
                this.txtCASt2.Enabled = false;
                this.txtCASt3.Text = "";
                this.txtCASt3.Enabled = false;
                this.txtCATitle.Text = "";
                this.txtCATitle.Enabled = false;

                this.ddlCASRegion.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCASRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlCASRegion.SelectedValue);
                Dictionary<Int32, String> ctyList = DMG.getCountryList(srID);
                this.ddlCACountry.Items.Clear();
                this.ddlCACountry.DataSource = ctyList;
                this.ddlCACountry.DataTextField = "Value";
                this.ddlCACountry.DataValueField = "Key";
                this.ddlCACountry.DataBind();
                this.ddlCACountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlCACountry.SelectedIndex = -1;
                this.ddlCACountry.Enabled = true;
                this.ddlCAState.Items.Clear();
                this.ddlCAState.DataBind();
                this.ddlCAState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlCAState.SelectedIndex = -1;
                this.ddlCAState.Enabled = false;
                this.ddlCACounty.Items.Clear();
                this.ddlCACounty.DataBind();
                this.ddlCACounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlCACounty.SelectedIndex = -1;
                this.ddlCACounty.Enabled = false;
                this.ddlCACity.Items.Clear();
                this.ddlCACity.DataBind();
                this.ddlCACity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCACity.SelectedIndex = -1;
                this.ddlCACity.Enabled = false;
                this.ddlCAZip.Items.Clear();
                this.ddlCAZip.DataBind();
                this.ddlCAZip.Items.Insert(0, new ListItem("--- Select Zip/Postalcode ---", "-1"));
                this.ddlCAZip.SelectedIndex = -1;
                this.ddlCAZip.Enabled = false;
                this.txtCASt1.Text = "";
                this.txtCASt1.Enabled = false;
                this.txtCASt2.Text = "";
                this.txtCASt2.Enabled = false;
                this.txtCASt3.Text = "";
                this.txtCASt3.Enabled = false;
                this.txtCATitle.Text = "";
                this.txtCATitle.Enabled = false;

                this.ddlCACountry.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCACountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctyID = Convert.ToInt32(this.ddlCACountry.SelectedValue);
                Dictionary<Int32, String> stList = DMG.getStateList(ctyID);
                this.ddlCAState.Items.Clear();
                this.ddlCAState.DataSource = stList;
                this.ddlCAState.DataTextField = "Value";
                this.ddlCAState.DataValueField = "Key";
                this.ddlCAState.DataBind();
                this.ddlCAState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlCAState.SelectedIndex = -1;
                this.ddlCAState.Enabled = true;
                this.ddlCACounty.Items.Clear();
                this.ddlCACounty.DataBind();
                this.ddlCACounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlCACounty.SelectedIndex = -1;
                this.ddlCACounty.Enabled = false;
                this.ddlCACity.Items.Clear();
                this.ddlCACity.DataBind();
                this.ddlCACity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCACity.SelectedIndex = -1;
                this.ddlCACity.Enabled = false;
                this.ddlCAZip.Items.Clear();
                this.ddlCAZip.DataBind();
                this.ddlCAZip.Items.Insert(0, new ListItem("--- Select Zip/Postalcode ---", "-1"));
                this.ddlCAZip.SelectedIndex = -1;
                this.ddlCAZip.Enabled = false;
                this.txtCASt1.Text = "";
                this.txtCASt1.Enabled = false;
                this.txtCASt2.Text = "";
                this.txtCASt2.Enabled = false;
                this.txtCASt3.Text = "";
                this.txtCASt3.Enabled = false;
                this.txtCATitle.Text = "";
                this.txtCATitle.Enabled = false;

                this.ddlCAState.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCAState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 stID = Convert.ToInt32(this.ddlCAState.SelectedValue);
                Dictionary<Int32, String> cnList = DMG.getCountyList(stID);
                this.ddlCACounty.Items.Clear();
                this.ddlCACounty.DataSource = cnList;
                this.ddlCACounty.DataTextField = "Value";
                this.ddlCACounty.DataValueField = "Key";
                this.ddlCACounty.DataBind();
                this.ddlCACounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlCACounty.SelectedIndex = -1;
                this.ddlCACounty.Enabled = true;
                this.ddlCACity.Items.Clear();
                this.ddlCACity.DataBind();
                this.ddlCACity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCACity.SelectedIndex = -1;
                this.ddlCACity.Enabled = false;
                this.ddlCAZip.Items.Clear();
                this.ddlCAZip.DataBind();
                this.ddlCAZip.Items.Insert(0, new ListItem("--- Select Zip/Postalcode ---", "-1"));
                this.ddlCAZip.SelectedIndex = -1;
                this.ddlCAZip.Enabled = false;
                this.txtCASt1.Text = "";
                this.txtCASt1.Enabled = false;
                this.txtCASt2.Text = "";
                this.txtCASt2.Enabled = false;
                this.txtCASt3.Text = "";
                this.txtCASt3.Enabled = false;
                this.txtCATitle.Text = "";
                this.txtCATitle.Enabled = false;

                this.ddlCACounty.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCACounty_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cnID = Convert.ToInt32(this.ddlCACounty.SelectedValue);
                Dictionary<Int32, String> cityList = DMG.getCityList(cnID);
                this.ddlCACity.Items.Clear();
                this.ddlCACity.DataSource = cityList;
                this.ddlCACity.DataTextField = "Value";
                this.ddlCACity.DataValueField = "Key";
                this.ddlCACity.DataBind();
                this.ddlCACity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCACity.SelectedIndex = -1;
                this.ddlCACity.Enabled = true;
                this.ddlCAZip.Items.Clear();
                this.ddlCAZip.DataBind();
                this.ddlCAZip.Items.Insert(0, new ListItem("--- Select Zip/Postalcode ---", "-1"));
                this.ddlCAZip.SelectedIndex = -1;
                this.ddlCAZip.Enabled = false;
                this.txtCASt1.Text = "";
                this.txtCASt1.Enabled = false;
                this.txtCASt2.Text = "";
                this.txtCASt2.Enabled = false;
                this.txtCASt3.Text = "";
                this.txtCASt3.Enabled = false;
                this.txtCATitle.Text = "";
                this.txtCATitle.Enabled = false;

                this.ddlCACity.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCACity_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cnID = Convert.ToInt32(this.ddlCACounty.SelectedValue);
                Dictionary<Int32, String> zipList = DMG.getZipList(cnID);
                this.ddlCAZip.Items.Clear();
                this.ddlCAZip.DataSource = zipList;
                this.ddlCAZip.DataTextField = "Value";
                this.ddlCAZip.DataValueField = "Key";
                this.ddlCAZip.DataBind();
                this.ddlCAZip.Items.Insert(0, new ListItem("--- Select Zip/Postalcode ---", "-1"));
                this.ddlCAZip.SelectedIndex = -1;
                this.ddlCAZip.Enabled = true;
                this.txtCASt1.Text = "";
                this.txtCASt1.Enabled = false;
                this.txtCASt2.Text = "";
                this.txtCASt2.Enabled = false;
                this.txtCASt3.Text = "";
                this.txtCASt3.Enabled = false;
                this.txtCATitle.Text = "";
                this.txtCATitle.Enabled = false;

                this.ddlCAZip.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCAZip_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtCASt1.Text = "";
                this.txtCASt1.Enabled = true;
                this.txtCASt2.Text = "";
                this.txtCASt2.Enabled = true;
                this.txtCASt3.Text = "";
                this.txtCASt3.Enabled = true;
                this.txtCATitle.Text = "";
                this.txtCATitle.Enabled = true;
                this.btnCAANew.Enabled = true;
                this.btnCAANew.CssClass = "btn btn-success text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCAANew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, dmgID = -99, rID = -99, srID = -99, cyID = -99, stID = -99, cnID = -99, ctID = -99, zpID = -99, coID = -99;
                String s1 = String.Empty, s2 = String.Empty, s3 = String.Empty, aT = String.Empty;
                coID = Convert.ToInt32(this.ddlCACompany.SelectedValue);
                dmgID = Convert.ToInt32(this.ddlCAType.SelectedValue);
                rID = Convert.ToInt32(this.ddlCARegion.SelectedValue);
                srID = Convert.ToInt32(this.ddlCASRegion.SelectedValue);
                cyID = Convert.ToInt32(this.ddlCACountry.SelectedValue);
                stID = Convert.ToInt32(this.ddlCAState.SelectedValue);
                cnID = Convert.ToInt32(this.ddlCACounty.SelectedValue);
                ctID = Convert.ToInt32(this.ddlCACity.SelectedValue);
                zpID = Convert.ToInt32(this.ddlCAZip.SelectedValue);
                s1 = Convert.ToString(this.txtCASt1.Text);
                s2 = Convert.ToString(this.txtCASt2.Text);
                s3 = Convert.ToString(this.txtCASt3.Text);
                aT = Convert.ToString(this.txtCATitle.Text);
                if (string.IsNullOrEmpty(s1))
                {
                    this.enCoAdd.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iCoAdd.Attributes["class"] = "icon fa fa-warning";
                    this.spnCoAdd.InnerText = " Warning!";
                    this.lblCoAddressSuccess.Text = "!!! Error !!! Null Values detected. Please provide all values";
                    this.enCoAdd.Visible = true;
                    this.txtCASt1.Focus();
                }
                else
                {
                    iResult = LD.insertLeadCompanyAddress(coID, dmgID, rID, srID, cyID, stID, cnID, ctID, zpID, s1, s2, s3, aT, LoginName, domain);
                    if (iResult.Equals(1))
                    {
                        btnCAAClear_Click(null, null);
                        this.enCoAdd.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iCoAdd.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnCoAdd.InnerText = " Success!";
                        this.lblCoAddressSuccess.Text = "Successfully inserted new address into SMARTs";
                        this.enCoAdd.Visible = true;
                    }
                    else
                    {
                        btnCAAClear_Click(null, null);
                        this.enCoAdd.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iCoAdd.Attributes["class"] = "icon fa fa-warning";
                        this.spnCoAdd.InnerText = " Warning!";
                        this.lblCoAddressSuccess.Text = "!!! Error !!! Please try again";
                        this.enCoAdd.Visible = true;   
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCAAClear_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> companyList = LD.getCompaniesList();
                this.ddlCACompany.Items.Clear();
                this.ddlCACompany.DataSource = companyList;
                this.ddlCACompany.DataTextField = "Value";
                this.ddlCACompany.DataValueField = "Key";
                this.ddlCACompany.DataBind();
                this.ddlCACompany.Items.Insert(0, new ListItem("--- Select Company Name ---", "-1"));
                this.ddlCACompany.SelectedIndex = -1;
                this.ddlCAType.Items.Clear();
                this.ddlCAType.DataBind();
                this.ddlCAType.Items.Insert(0, new ListItem("--- Select Address Type ---", "-1"));
                this.ddlCAType.SelectedIndex = -1;
                this.ddlCAType.Enabled = false;
                this.ddlCARegion.Items.Clear();
                this.ddlCARegion.DataBind();
                this.ddlCARegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlCARegion.SelectedIndex = -1;
                this.ddlCARegion.Enabled = false;
                this.ddlCASRegion.Items.Clear();
                this.ddlCASRegion.DataBind();
                this.ddlCASRegion.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlCASRegion.SelectedIndex = -1;
                this.ddlCASRegion.Enabled = false;
                this.ddlCACountry.Items.Clear();
                this.ddlCACountry.DataBind();
                this.ddlCACountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlCACountry.SelectedIndex = -1;
                this.ddlCACountry.Enabled = false;
                this.ddlCAState.Items.Clear();
                this.ddlCAState.DataBind();
                this.ddlCAState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlCAState.SelectedIndex = -1;
                this.ddlCAState.Enabled = false;
                this.ddlCACounty.Items.Clear();
                this.ddlCACounty.DataBind();
                this.ddlCACounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlCACounty.SelectedIndex = -1;
                this.ddlCACounty.Enabled = false;
                this.ddlCACity.Items.Clear();
                this.ddlCACity.DataBind();
                this.ddlCACity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCACity.SelectedIndex = -1;
                this.ddlCACity.Enabled = false;
                this.ddlCAZip.Items.Clear();
                this.ddlCAZip.DataBind();
                this.ddlCAZip.Items.Insert(0, new ListItem("--- Select Zip/Postalcode ---", "-1"));
                this.ddlCAZip.SelectedIndex = -1;
                this.ddlCAZip.Enabled = false;
                this.txtCASt1.Text = "";
                this.txtCASt1.Enabled = false;
                this.txtCASt2.Text = "";
                this.txtCASt2.Enabled = false;
                this.txtCASt3.Text = "";
                this.txtCASt3.Enabled = false;
                this.txtCATitle.Text = "";
                this.txtCATitle.Enabled = false;
                this.btnCAAClear.Enabled = false;
                this.enCoAdd.Visible = false;
                this.lblCoAddressSuccess.Text = "";
                this.btnCAAClear.CssClass = "btn btn-default text-center";
                this.btnCAANew.Enabled = false;
                this.btnCAANew.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCAADone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnCAAClear_Click(null, null);
                this.divCoList.Visible = true;
                this.divLCoAdd.Visible = false;
                this.divAddCoAddress.Visible = false;
                System.Data.DataTable coTable = LD.getCompaniesTable();
                this.gvwCoList.DataSource = coTable;
                this.gvwCoList.PageIndex = 0;
                this.gvwCoList.SelectedIndex = -1;
                this.gvwCoList.DataBind();
                this.gvwCoList.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        /// <summary>
        /// Lead Person List
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void gvwLeadCompany_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable compTable = LD.getCompaniesTable();
                this.gvwLeadCompany.DataSource = compTable;
                this.gvwLeadCompany.PageIndex = e.NewPageIndex;
                this.gvwLeadCompany.SelectedIndex = -1;
                this.gvwLeadCompany.DataBind();
                this.btnCancelNP.Enabled = true;
                this.btnCancelNP.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwLeadCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 coID = Convert.ToInt32(this.gvwLeadCompany.SelectedValue);
                System.Data.DataTable ldTable = LD.getLeadPersonTable(coID);
                this.gvwLeadPerson.DataSource = ldTable;
                this.gvwLeadPerson.PageIndex = 0;
                this.gvwLeadPerson.SelectedIndex = -1;
                this.gvwLeadPerson.DataBind();
                this.gvwLeadPerson.Visible = true;
                this.btnCancelNP.Enabled = true;
                this.btnCancelNP.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwLeadPerson_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 coID = Convert.ToInt32(this.gvwLeadCompany.SelectedValue);
                System.Data.DataTable lpTable = LD.getLeadPersonTable(coID);
                this.gvwLeadPerson.PageIndex = e.NewPageIndex;
                this.gvwLeadPerson.DataSource = lpTable;
                this.gvwLeadPerson.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwLeadPerson_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 lpID = Convert.ToInt32(this.gvwLeadPerson.SelectedValue);
                System.Data.DataTable addTable = LD.getPersonAddressTable(lpID);
                this.gvwPersonAddress.DataSource = addTable;
                this.gvwPersonAddress.PageIndex = 0;
                this.gvwPersonAddress.SelectedIndex = -1;
                this.gvwPersonAddress.DataBind();
                System.Data.DataTable phTable = LD.getPersonPhoneTable(lpID);
                this.gvwLeadPhone.DataSource = phTable;
                this.gvwLeadPhone.PageIndex = 0;
                this.gvwLeadPhone.SelectedIndex = -1;
                this.gvwLeadPhone.DataBind();
                System.Data.DataTable emlTable = LD.getPersonEmailTable(lpID);
                this.gvwLeadEmail.DataSource = emlTable;
                this.gvwLeadEmail.PageIndex = 0;
                this.gvwLeadEmail.SelectedIndex = -1;
                this.gvwLeadEmail.DataBind();
                this.btnCancelNP.Enabled = true;
                this.btnCancelNP.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwPersonAddress_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 lpID = Convert.ToInt32(this.gvwLeadPerson.SelectedValue);
                System.Data.DataTable addTable = LD.getPersonAddressTable(lpID);
                this.gvwPersonAddress.DataSource = addTable;
                this.gvwPersonAddress.PageIndex = e.NewPageIndex;
                this.gvwPersonAddress.SelectedIndex = -1;
                this.gvwPersonAddress.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwLeadEmail_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {          
                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwLeadPhone_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlNEmlCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 coID = Convert.ToInt32(this.ddlNEmlCompany.SelectedValue);
                Dictionary<Int32, String> ldList = LD.getLeadPersonList(coID);
                this.ddlNEmlLeadAcc.Items.Clear();
                this.ddlNEmlLeadAcc.DataSource = ldList;
                this.ddlNEmlLeadAcc.DataTextField = "Value";
                this.ddlNEmlLeadAcc.DataValueField = "Key";
                this.ddlNEmlLeadAcc.DataBind();
                this.ddlNEmlLeadAcc.Items.Insert(0, new ListItem("--- Select Lead Account ---", "-1"));
                this.ddlNEmlLeadAcc.SelectedIndex = -1;
                this.ddlNEmlLeadAcc.Enabled = true;
                this.txtLeadEmail.Text = "";
                this.txtLeadEmail.Enabled = false;
                this.enNEml.Visible = false;
                this.lblNEmlSuccess.Text = "";
                this.btnNewEmailClear.Enabled = true;
                this.btnNewEmailClear.CssClass = "btn btn-warning text-center";
                this.btnNewEmailAdd.Enabled = false;
                this.btnNewEmailAdd.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlNEmlLeadAcc_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtLeadEmail.Text = "";
                this.txtLeadEmail.Enabled = true;
                Dictionary<Int32, String> typList = DMG.getDemogTypeList();
                this.ddlNEmlType.Items.Clear();
                this.ddlNEmlType.DataSource = typList;
                this.ddlNEmlType.DataTextField = "Value";
                this.ddlNEmlType.DataValueField = "Key";
                this.ddlNEmlType.DataBind();
                this.ddlNEmlType.Items.Insert(0, new ListItem("--- Select Email Type ---", "-1"));
                this.ddlNEmlType.SelectedIndex = -1;
                this.ddlNEmlType.Enabled = true;
                this.btnNewEmailAdd.Enabled = false;
                this.btnNewEmailAdd.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlNEmlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {                
                this.btnNewEmailAdd.Enabled = true;
                this.btnNewEmailAdd.CssClass = "btn btn-success text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnNewEmailAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, coID = -99, lpID = -99, dmgID = -99;
                String email = String.Empty;
                coID = Convert.ToInt32(this.ddlNEmlCompany.SelectedValue);
                lpID = Convert.ToInt32(this.ddlNEmlLeadAcc.SelectedValue);
                dmgID = Convert.ToInt32(this.ddlNEmlType.SelectedValue);
                email = Convert.ToString(this.txtLeadEmail.Text);
                if (String.IsNullOrEmpty(email))
                {
                    this.btnNewEmailClear_Click(null, null);
                    this.enNEml.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iNEml.Attributes["class"] = "icon fa fa-warning";
                    this.spnNEml.InnerText = " Warning!";
                    this.lblNEmlSuccess.Text = "!!! Error !!! Null value detected. Please provide all values";
                    this.enNEml.Visible = true;
                    this.btnNewEmailClear.Enabled = true;
                    this.btnNewEmailClear.CssClass = "btn btn-warning text-center";
                }
                else
                {
                    iResult = LD.insertNewLeadPersonEmail(coID, lpID, dmgID, email);
                    if (iResult.Equals(1))
                    {
                        this.btnNewEmailClear_Click(null, null);
                        this.enNEml.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iNEml.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnNEml.InnerText = " Success!";
                        this.lblNEmlSuccess.Text = "Successfully inserted Email to SMARTs";
                        this.enNEml.Visible = true;
                    }
                    else if(iResult.Equals(-1))
                    {
                        this.btnNewEmailClear_Click(null, null);
                        this.enNEml.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iNEml.Attributes["class"] = "icon fa fa-warning";
                        this.spnNEml.InnerText = " Warning!";
                        this.lblNEmlSuccess.Text = "!!! Error !!! Found Duplicate. Please provide a unique Email Address";
                        this.enNEml.Visible = true;
                        this.btnNewEmailClear.Enabled = true;
                        this.btnNewEmailClear.CssClass = "btn btn-warning text-center";
                    }
                    else
                    {
                        this.btnNewEmailClear_Click(null, null);
                        this.enNEml.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iNEml.Attributes["class"] = "icon fa fa-warning";
                        this.spnNEml.InnerText = " Warning!";
                        this.lblNEmlSuccess.Text = "!!! Error !!! Please try again";
                        this.enNEml.Visible = true;
                        this.btnNewEmailClear.Enabled = true;
                        this.btnNewEmailClear.CssClass = "btn btn-warning text-center";
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnNewEmailClear_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> coList = LD.getCompaniesListWithCount();
                this.ddlNEmlLeadAcc.Items.Clear();
                this.ddlNEmlCompany.DataSource = coList;
                this.ddlNEmlCompany.DataTextField = "Value";
                this.ddlNEmlCompany.DataValueField = "Key";
                this.ddlNEmlCompany.DataBind();
                this.ddlNEmlCompany.Items.Insert(0, new ListItem("--- Select Company Name ---", "-1"));
                this.ddlNEmlCompany.SelectedIndex = -1;
                this.ddlNEmlLeadAcc.Items.Clear();
                this.ddlNEmlLeadAcc.DataBind();
                this.ddlNEmlLeadAcc.Items.Insert(0, new ListItem("--- Select Lead Account ---", "-1"));
                this.ddlNEmlLeadAcc.SelectedIndex = -1;
                this.ddlNEmlLeadAcc.Enabled = false;
                this.ddlNEmlType.Items.Clear();
                this.ddlNEmlType.DataBind();
                this.ddlNEmlType.Items.Insert(0, new ListItem("--- Select Email Type ---", "-1"));
                this.ddlNEmlType.SelectedIndex = -1;
                this.ddlNEmlType.Enabled = false;
                this.txtLeadEmail.Text = "";
                this.txtLeadEmail.Enabled = false;
                this.lblNEmlSuccess.Text = "";
                this.enNEml.Visible = false;
                this.btnNewEmailClear.Enabled = false;
                this.btnNewEmailClear.CssClass = "btn btn-default text-center";
                this.btnNewEmailAdd.Enabled = false;
                this.btnNewEmailAdd.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnNewEmailDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnNewEmailClear_Click(null, null);
                this.divAccList.Visible = true;
                this.divAddNewAcc.Visible = false;
                this.divLeadEmail.Visible = false;
                this.divLeadPhone.Visible = false;
                System.Data.DataTable coTable = LD.getCompaniesTable();
                this.gvwLeadCompany.DataSource = coTable;
                this.gvwLeadCompany.PageIndex = 0;
                this.gvwLeadCompany.SelectedIndex = -1;
                this.gvwLeadCompany.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlNPhCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 coID = Convert.ToInt32(this.ddlNPhCompany.SelectedValue);
                Dictionary<Int32, String> ldList = LD.getLeadPersonList(coID);
                this.ddlNPhLead.Items.Clear();
                this.ddlNPhLead.DataSource = ldList;
                this.ddlNPhLead.DataTextField = "Value";
                this.ddlNPhLead.DataValueField = "Key";
                this.ddlNPhLead.DataBind();
                this.ddlNPhLead.Items.Insert(0, new ListItem("--- Select Lead Account ---", "-1"));
                this.ddlNPhLead.SelectedIndex = -1;
                this.ddlNPhLead.Enabled = true;
                this.txtNPhNumber.Text = "";
                this.txtNPhNumber.Enabled = false;
                this.txtNPhExtension.Text = "";
                this.txtNPhExtension.Enabled = false;
                this.ddlNPhType.Items.Clear();
                this.ddlNPhType.DataBind();
                this.ddlNPhType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlNPhType.SelectedIndex = -1;
                this.ddlNPhType.Enabled = false;
                this.enNPh.Visible = false;
                this.lblNPhSuccess.Text = "";
                this.btnNewPhoneAddClear.Enabled = true;
                this.btnNewPhoneAddClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlNPhLead_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtNPhNumber.Text = "";
                this.txtNPhNumber.Enabled = true;
                this.txtNPhExtension.Text = "";
                this.txtNPhExtension.Enabled = true;
                Dictionary<Int32, String> typList = DMG.getDemogTypeList();
                this.ddlNPhType.Items.Clear();
                this.ddlNPhType.DataSource = typList;
                this.ddlNPhType.DataTextField = "Value";
                this.ddlNPhType.DataValueField = "Key";
                this.ddlNPhType.DataBind();
                this.ddlNPhType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlNPhType.SelectedIndex = -1;
                this.ddlNPhType.Enabled = true;
                this.enNPh.Visible = false;
                this.lblNPhSuccess.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlNPhType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnNewPhoneAdd.Enabled = true;
                this.btnNewPhoneAdd.CssClass = "btn btn-success text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnNewPhoneAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, coID = -99, lpID = -99, dType = -99;
                String phone = String.Empty, ext = String.Empty;
                coID = Convert.ToInt32(this.ddlNPhCompany.SelectedValue);
                lpID = Convert.ToInt32(this.ddlNPhLead.SelectedValue);
                phone = Convert.ToString(this.txtNPhNumber.Text);
                ext = Convert.ToString(this.txtNPhExtension.Text);
                dType = Convert.ToInt32(this.ddlNPhType.SelectedValue);
                if(String.IsNullOrEmpty(phone))
                {
                    this.btnNewPhoneAddClear_Click(null, null);
                    this.enNPh.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iNPh.Attributes["class"] = "icon fa fa-warning";
                    this.spnNPh.InnerText = " Warning!";
                    this.lblNPhSuccess.Text = "!!! Error !!! Null value detected. Please provide all values";
                    this.enNPh.Visible = true;
                    this.btnNewPhoneAddClear.Enabled = true;
                    this.btnNewPhoneAddClear.CssClass = "btn btn-warning text-center";
                }
                else
                {
                    iResult = LD.insertNewLeadPersonPhone(phone, ext, dType, lpID);
                    if (iResult.Equals(1))
                    {
                        this.btnNewPhoneAddClear_Click(null, null);
                        this.enNPh.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iNPh.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnNPh.InnerText = " Success!";
                        this.lblNPhSuccess.Text = "Successfully inserted Phone/Fax Number to SMARTs";
                        this.enNPh.Visible = true;
                    }
                    else if (iResult.Equals(-1))
                    {
                        this.btnNewPhoneAddClear_Click(null, null);
                        this.enNPh.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iNPh.Attributes["class"] = "icon fa fa-warning";
                        this.spnNPh.InnerText = " Warning!";
                        this.lblNPhSuccess.Text = "!!! Error !!! Found Duplicate Number";
                        this.enNPh.Visible = true;
                        this.btnNewPhoneAddClear.Enabled = true;
                        this.btnNewPhoneAddClear.CssClass = "btn btn-warning text-center";
                    }
                    else
                    {
                        this.btnNewPhoneAddClear_Click(null, null);
                        this.enNPh.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iNPh.Attributes["class"] = "icon fa fa-warning";
                        this.spnNPh.InnerText = " Warning!";
                        this.lblNPhSuccess.Text = "!!! Error !!! Please try again";
                        this.enNPh.Visible = true;
                        this.btnNewPhoneAddClear.Enabled = true;
                        this.btnNewPhoneAddClear.CssClass = "btn btn-warning text-center";
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnNewPhoneAddClear_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> compList = LD.getCompaniesListWithCount();
                this.ddlNPhCompany.Items.Clear();
                this.ddlNPhCompany.DataSource = compList;
                this.ddlNPhCompany.DataTextField = "Value";
                this.ddlNPhCompany.DataValueField = "Key";
                this.ddlNPhCompany.DataBind();
                this.ddlNPhCompany.Items.Insert(0, new ListItem("--- Select Company Name ---", "-1"));
                this.ddlNPhCompany.SelectedIndex = -1;
                this.ddlNPhLead.Items.Clear();
                this.ddlNPhLead.DataBind();
                this.ddlNPhLead.Items.Insert(0, new ListItem("--- Select Lead Account ---", "-1"));
                this.ddlNPhLead.SelectedIndex = -1;
                this.ddlNPhLead.Enabled = false;
                this.txtNPhNumber.Text = "";
                this.txtNPhNumber.Enabled = false;
                this.txtNPhExtension.Text = "";
                this.txtNPhExtension.Enabled = false;
                this.ddlNPhType.Items.Clear();
                this.ddlNPhType.DataBind();
                this.ddlNPhType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlNPhType.SelectedIndex = -1;
                this.ddlNPhType.Enabled = false;
                this.enNPh.Visible = false;
                this.lblNPhSuccess.Text = "";
                this.btnNewPhoneAdd.Enabled = false;
                this.btnNewPhoneAdd.CssClass = "btn btn-default text-center";
                this.btnNewPhoneAddClear.Enabled = false;
                this.btnNewPhoneAddClear.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnNewPhoneAddDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnNewPhoneAddClear_Click(null, null);
                this.divAccList.Visible = true;
                this.divLeadsList.Visible = false;
                this.divLeadEmail.Visible = false;
                this.divLeadPhone.Visible = false;
                System.Data.DataTable coList = LD.getCompaniesTable();
                this.gvwLeadCompany.DataSource = coList;
                this.gvwLeadCompany.PageIndex = 0;
                this.gvwLeadCompany.SelectedIndex = -1;
                this.gvwLeadCompany.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        /// <summary>
        /// Leads Communications
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvwCommCompany_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable compTable = LD.getCompaniesTable();
                this.gvwCommCompany.DataSource = compTable;
                this.gvwCommCompany.PageIndex = 0;
                this.gvwCommCompany.SelectedIndex = -1;
                this.gvwCommCompany.DataBind();
                this.btnClearNValues.Enabled = true;
                this.btnClearNValues.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwCommCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 coID = Convert.ToInt32(this.gvwCommCompany.SelectedValue);
                this.gvwCommCompany.Visible = false;
                System.Data.DataTable lpTable = LD.getLeadPersonTable(coID);
                this.gvwCommLA.DataSource = lpTable;
                this.gvwCommLA.PageIndex = 0;
                this.gvwCommLA.SelectedIndex = -1;
                this.gvwCommLA.DataBind();
                this.gvwCommLA.Visible = true;
                GridViewRow dRow = this.gvwCommCompany.SelectedRow;
                String coName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.btnClearNValues.Enabled = true;
                this.btnClearNValues.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwCommLA_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 coID = Convert.ToInt32(this.gvwCommCompany.SelectedValue);
                System.Data.DataTable lpTable = LD.getLeadPersonTable(coID);
                this.gvwCommLA.DataSource = lpTable;
                this.gvwCommLA.PageIndex = e.NewPageIndex;
                this.gvwCommLA.SelectedIndex = -1;
                this.gvwCommLA.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwCommLA_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 coID = Convert.ToInt32(this.gvwCommCompany.SelectedValue);
                Int32 lpID = Convert.ToInt32(this.gvwCommLA.SelectedValue);
                System.Data.DataTable commTable = LD.getLeadNurtureTable(coID, lpID);
                this.gvwCommList.DataSource = commTable;
                this.gvwCommList.PageIndex = 0;
                this.gvwCommList.SelectedIndex = -1;
                this.gvwCommList.DataBind();
                this.gvwCommList.Visible = true;
                GridViewRow dRow = this.gvwCommLA.SelectedRow;
                String lpName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwCommList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 coID = Convert.ToInt32(this.gvwCommCompany.SelectedValue);
                Int32 lpID = Convert.ToInt32(this.gvwCommLA.SelectedValue);
                System.Data.DataTable cTable = LD.getLeadNurtureTable(coID, lpID);
                this.gvwCommList.DataSource = cTable;
                this.gvwCommList.PageIndex = e.NewPageIndex;
                this.gvwCommList.SelectedIndex = -1;
                this.gvwCommList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwCommList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow dRow = this.gvwCommList.SelectedRow;
                String txtDetail = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.txtCommDetail.Text = txtDetail;
                this.txtCommDetail.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnNewComment_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnClearNValues_Click(null, null);
                this.divCommList.Visible = false;
                this.divCommAdd.Visible = true;
                Dictionary<Int32, String> compList = LD.getCompaniesListWithCount();
                this.ddlCommAddCompany.Items.Clear();
                this.ddlCommAddCompany.DataSource = compList;
                this.ddlCommAddCompany.DataTextField = "Value";
                this.ddlCommAddCompany.DataValueField = "Key";
                this.ddlCommAddCompany.DataBind();
                this.ddlCommAddCompany.Items.Insert(0, new ListItem("--- Select Company Name ---", "-1"));
                this.ddlCommAddCompany.SelectedIndex = -1;                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnClearNValues_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable compTable = LD.getCompaniesTable();
                this.gvwCommCompany.DataSource = compTable;
                this.gvwCommCompany.PageIndex = 0;
                this.gvwCommCompany.SelectedIndex = -1;
                this.gvwCommCompany.DataBind();
                this.gvwCommCompany.Visible = true;
                this.gvwCommLA.DataSource = null;
                this.gvwCommLA.PageIndex = 0;
                this.gvwCommLA.SelectedIndex = -1;
                this.gvwCommLA.DataBind();
                this.gvwCommLA.Visible = false;
                this.gvwCommList.DataSource = null;
                this.gvwCommList.PageIndex = 0;
                this.gvwCommList.SelectedIndex = -1;
                this.gvwCommList.DataBind();
                this.gvwCommList.Visible = false;
                this.txtCommDetail.Text = "";
                this.txtCommDetail.Visible = false;
                this.btnClearNValues.Enabled = false;
                this.btnClearNValues.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCommAddCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 coID = Convert.ToInt32(this.ddlCommAddCompany.SelectedValue);
                Dictionary<Int32, String> lpList = LD.getLeadPersonList(coID);
                this.ddlCommAddLead.Items.Clear();
                this.ddlCommAddLead.DataSource = lpList;
                this.ddlCommAddLead.DataTextField = "Value";
                this.ddlCommAddLead.DataValueField = "Key";
                this.ddlCommAddLead.DataBind();
                this.ddlCommAddLead.Items.Insert(0, new ListItem("--- Select Lead Name ---", "-1"));
                this.ddlCommAddLead.SelectedIndex = -1;
                this.ddlCommAddLead.Enabled = true;
                this.txtComms.Text = "";
                this.txtComms.Enabled = false;
                this.ddlCommFEPerson.Items.Clear();
                this.ddlCommFEPerson.DataBind();
                this.ddlCommFEPerson.Items.Insert(0, new ListItem("--- Select FE Contact Person ---", "-1"));
                this.ddlCommFEPerson.SelectedIndex = -1;
                this.ddlCommFEPerson.Enabled = false;
                this.clndrTimeStamp.SelectedDate = DateTime.Now;
                this.txtCommTimeStamp.Enabled = false;
                Dictionary<Int32, String> ctList = DMG.getContactTypeList();
                this.ddlContactType.Items.Clear();
                this.ddlContactType.DataSource = ctList;
                this.ddlContactType.DataTextField = "Value";
                this.ddlContactType.DataValueField = "Key";
                this.ddlContactType.DataBind();
                this.ddlContactType.Items.Insert(0, new ListItem("--- Select Contact Type ---", "-1"));
                this.ddlContactType.SelectedIndex = -1;
                this.ddlContactType.Enabled = false;
                this.enComm.Visible = false;
                this.lblCommentSuccess.Text = "";
                this.btnCommentAdd.Enabled = false;
                this.btnCommentAdd.CssClass = "btn btn-default text-center";
                this.btnCommentClear.Enabled = true;
                this.btnCommentClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCommAddLead_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<System.Guid, String> userList = USR.getClientUsersList(feID);
                this.txtComms.Text = "";
                this.txtComms.Enabled = true;
                this.ddlCommFEPerson.Items.Clear();
                this.ddlCommFEPerson.DataSource = userList;
                this.ddlCommFEPerson.DataTextField = "Value";
                this.ddlCommFEPerson.DataValueField = "Key";
                this.ddlCommFEPerson.DataBind();
                this.ddlCommFEPerson.Items.Insert(0, new ListItem("--- Select FE Contact Person ---", "-1"));
                this.ddlCommFEPerson.SelectedIndex = -1;
                this.ddlCommFEPerson.Enabled = true;
                this.clndrTimeStamp.SelectedDate = DateTime.Now;
                this.txtCommTimeStamp.Enabled = false;
                Dictionary<Int32, String> ctList = DMG.getContactTypeList();
                this.ddlContactType.Items.Clear();
                this.ddlContactType.DataSource = ctList;
                this.ddlContactType.DataTextField = "Value";
                this.ddlContactType.DataValueField = "Key";
                this.ddlContactType.DataBind();
                this.ddlContactType.Items.Insert(0, new ListItem("--- Select Contact Type ---", "-1"));
                this.ddlContactType.SelectedIndex = -1;
                this.ddlContactType.Enabled = false;
                this.enComm.Visible = false;
                this.lblCommentSuccess.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCommFEPerson_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.clndrTimeStamp.SelectedDate = DateTime.Now;
                this.txtCommTimeStamp.Enabled = true;
                Dictionary<Int32, String> ctList = DMG.getContactTypeList();
                this.ddlContactType.Items.Clear();
                this.ddlContactType.DataSource = ctList;
                this.ddlContactType.DataTextField = "Value";
                this.ddlContactType.DataValueField = "Key";
                this.ddlContactType.DataBind();
                this.ddlContactType.Items.Insert(0, new ListItem("--- Select Contact Type ---", "-1"));
                this.ddlContactType.SelectedIndex = -1;
                this.ddlContactType.Enabled = true;
                this.enComm.Visible = false;
                this.lblCommentSuccess.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlContactType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnCommentAdd.Enabled = true;
                this.btnCommentAdd.CssClass = "btn btn-success text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCommentAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, cID = -99, pID = -99, ctID = -99;
                String comments = String.Empty;
                System.Guid user = new System.Guid();
                DateTime comTime = DateTime.Now;
                cID = Convert.ToInt32(this.ddlCommAddCompany.SelectedValue);
                pID = Convert.ToInt32(this.ddlCommAddLead.SelectedValue);
                comments = Convert.ToString(this.txtComms.Text);
                user = new Guid(this.ddlCommFEPerson.SelectedValue);
                comTime = Convert.ToDateTime(this.txtCommTimeStamp.Text);
                ctID = Convert.ToInt32(this.ddlContactType.SelectedValue);
                if (String.IsNullOrEmpty(comments))
                {                    
                    this.enComm.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iComm.Attributes["class"] = "icon fa fa-warning";
                    this.spnComm.InnerText = " Warning!";
                    this.lblCommentSuccess.Text = "!!! Error !!! Null Values detected. Please provide all values";
                    this.enComm.Visible = true;
                }
                else
                {
                    iResult = LD.insertLeadComments(cID, pID, comments, user, comTime, ctID, LoginName, domain);
                    if (iResult.Equals(1))
                    {
                        this.btnCommentClear_Click(null, null);
                        this.enComm.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iComm.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnComm.InnerText = " Success!";
                        this.lblCommentSuccess.Text = "Successfully inserted Comments";
                        this.enComm.Visible = true;
                        this.btnCommentAdd.Enabled = false;
                        this.btnCommentAdd.CssClass = "btn btn-default text-center";
                        this.btnCommentClear.Enabled = true;
                        this.btnCommentClear.CssClass = "btn btn-warning text-center";
                    }
                    else
                    {
                        this.btnCommentClear_Click(null, null);
                        this.enComm.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iComm.Attributes["class"] = "icon fa fa-warning";
                        this.spnComm.InnerText = " Warning!";
                        this.lblCommentSuccess.Text = "!!! Error !!! Please try again";
                        this.enComm.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCommentClear_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> compList = LD.getCompaniesListWithCount();
                this.ddlCommAddCompany.Items.Clear();
                this.ddlCommAddCompany.DataSource = compList;
                this.ddlCommAddCompany.DataTextField = "Value";
                this.ddlCommAddCompany.DataValueField = "Key";
                this.ddlCommAddCompany.DataBind();
                this.ddlCommAddCompany.Items.Insert(0, new ListItem("--- Select Company Name ---", "-1"));
                this.ddlCommAddCompany.SelectedIndex = -1;
                this.ddlCommAddLead.Items.Clear();
                this.ddlCommAddLead.DataBind();
                this.ddlCommAddLead.Items.Insert(0, new ListItem("--- Select Lead Name ---", "-1"));
                this.ddlCommAddLead.SelectedIndex = -1;
                this.ddlCommAddLead.Enabled = false;
                this.txtComms.Text = "";
                this.txtComms.Enabled = false;
                this.ddlCommFEPerson.Items.Clear();
                this.ddlCommFEPerson.DataBind();
                this.ddlCommFEPerson.Items.Insert(0, new ListItem("--- Select FE Contact Person ---", "-1"));
                this.ddlCommFEPerson.SelectedIndex = -1;
                this.ddlCommFEPerson.Enabled = false;
                this.txtCommTimeStamp.Text = "";
                this.txtCommTimeStamp.Enabled = false;
                this.clndrTimeStamp.SelectedDate = DateTime.Now;
                Dictionary<Int32, String> ctList = DMG.getContactTypeList();
                this.ddlContactType.Items.Clear();
                this.ddlContactType.DataSource = ctList;
                this.ddlContactType.DataTextField = "Value";
                this.ddlContactType.DataValueField = "Key";
                this.ddlContactType.DataBind();
                this.ddlContactType.Items.Insert(0, new ListItem("--- Select Contact Type ---", "-1"));
                this.ddlContactType.SelectedIndex = -1;
                this.ddlContactType.Enabled = false;
                this.enComm.Visible = false;
                this.lblCommentSuccess.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCommentDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnCommentClear_Click(null, null);
                this.divCommList.Visible = true;
                this.divCommAdd.Visible = false;
                System.Data.DataTable compTable = LD.getCompaniesTable();
                this.gvwCommCompany.DataSource = compTable;
                this.gvwCommCompany.PageIndex = 0;
                this.gvwCommCompany.SelectedIndex = -1;
                this.gvwCommCompany.DataBind();                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }                                                                                                                                                       
    }
}