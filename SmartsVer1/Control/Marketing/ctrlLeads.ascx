﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlLeads.ascx.cs" Inherits="SmartsVer1.Control.Marketing.ctrlLeads" %>
<!-- Content Header (Page header) -->
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<!-- Stylesheet -->
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<!-- Javascript -->
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<!-- Javascript for Footer -->
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>
<!--- Page local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }
    .swdMenu {
        color: #ffffff;
    }
    a.disabled {
        pointer-events: none;
        cursor: default;
    }
    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper[data-stepper-steps="8"] li {
            width: 12%;
        }

        ol.stepper[data-stepper-steps="9"] li {
            width: 11%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }
    
        .lblHeader {
            min-width: 120px;
            text-align: right;
        } 
</style>
<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<!--- Login --->
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">        
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>Sales/Marketing</li>
                <li><a href="../../Marketing/Leads_2A.aspx"> S&M Leads</a></li>
            </ol>
        </div>
    </div>    
</div>
<!-- Page Content -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul class="nav nav-pills nav-justified">
                <li class="active"><a data-toggle="pill" href="#leads">Leads</a></li>
                <li><a data-toggle="pill" href="#acctt">Lead Account</a></li>
                <li><a data-toggle="pill" href="#comm">Lead Nurture Cycle</a></li>
            </ul>
            <div class="tab-content">
                <div id="leads" class="tab-pane fade in active">
                    <asp:UpdateProgress ID="uprgrsLeadsListing" runat="server" DisplayAfter="0" DynamicLayout="true" AssociatedUpdatePanelID="upnlLeadsList">
                        <ProgressTemplate>
                            <div id="divLeadsListProcessing" runat="server" class="updateProgress">
                                <asp:Image ID="imgLeadsListProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlLeadsList">
                        <ContentTemplate>
                            <div id="divLeadsList" runat="server">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h5 class="panel-title"><span>Sales & Marketing Leads</span></h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group">
                                                    <asp:LinkButton ID="btnLeadCoNew" runat="server" CssClass="btn btn-info text-center" OnClick="btnLeadNew_Click"><i class="fa fa-plus"> S&M Lead Company</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnLeadAdd" runat="server" CssClass="btn btn-info text-center" OnClick="btnAddCo_Click"><i class="fa fa-plus"> S&M Lead Address</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnLeadEmail" runat="server" CssClass="btn btn-info text-center"><i class="fa fa-plus"> S&M Lead Email</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnLeadPhone" runat="server" CssClass="btn btn-info text-center"><i class="fa fa-plus"> S&M Lead Phone</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnLeadReset" runat="server" CssClass="btn btn-warning text-center" OnClick="btnLeadCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div id="divCoList" runat="server">
                                                    <asp:GridView ID="gvwCoList" runat="server" AutoGenerateColumns="False" DataKeyNames="coID" AllowPaging="True" AllowSorting="True"
                                                        CssClass="mydatagrid" EmptyDataText="No Companies Found." ShowHeaderWhenEmpty="true" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                        PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnPageIndexChanging="gvwCoList_PageIndexChanging"
                                                         OnSelectedIndexChanged ="gvwCoList_SelectedIndexChanged">
                                                        <Columns>
                                                            <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                            </asp:ButtonField>
                                                            <asp:BoundField DataField="coID" Visible="false" />
                                                            <asp:BoundField DataField="coName" HeaderText="Company Name" />
                                                            <asp:BoundField DataField="coWebURL" HeaderText="Website" />
                                                            <asp:BoundField DataField="coLnkURL" HeaderText="LinkedIn Company Page" />
                                                            <asp:BoundField DataField="infID" HeaderText="Following on In?" />
                                                            <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                                                        </Columns>
                                                    </asp:GridView>
                                                    <asp:GridView ID="gvwCoAddress" runat="server" AutoGenerateColumns="False" DataKeyNames="addID" AllowPaging="True" AllowSorting="True"
                                                        CssClass="mydatagrid" EmptyDataText="No Companies Found." ShowHeaderWhenEmpty="true" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                        PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnPageIndexChanging="gvwCoAddress_PageIndexChanging"
                                                        Visible="false">
                                                        <Columns>
                                                            <asp:BoundField DataField="addID" Visible="false" />
                                                            <asp:BoundField DataField="title" HeaderText="Title" />
                                                            <asp:BoundField DataField="st1" HeaderText="Street 1" />
                                                            <asp:BoundField DataField="st2" HeaderText="Street 2" />
                                                            <asp:BoundField DataField="st3" HeaderText="Street 3" />
                                                            <asp:BoundField DataField="cityID" HeaderText="City/Town" />
                                                            <asp:BoundField DataField="stID" HeaderText="State/Province" />
                                                            <asp:BoundField DataField="zipID" HeaderText="Zip/Postcode" />
                                                            <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div id="divLCoAdd" runat="server" visible="false">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                            <div class="input-group">
                                                                <span class="input-group-addon lblHeader bg-light-blue">Company</span>
                                                                <asp:TextBox ID="txtCoName" runat="server" CssClass="form-control" Text="" AutoComplete="off" placeholder="Company (required)" />
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                            <div class="input-group">
                                                                <span class="input-group-addon lblHeader bg-light-blue">Website</span>
                                                                <asp:TextBox ID="txtWUrl" runat="server" CssClass="form-control" Text="" AutoComplete="off" placeholder="Website URL (optional)" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                            <div class="input-group">
                                                                <span class="input-group-addon lblHeader bg-light-blue">LinkedIn</span>
                                                                <asp:TextBox ID="txtLUrl" runat="server" CssClass="form-control" Text="" AutoComplete="off" placeholder="LinkedIN URL (optional)" />
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                            <div class="input-group">
                                                                <span class="input-group-addon lblHeader bg-light-blue">Following In?</span>
                                                                <asp:DropDownList ID="ddlFollow" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                    OnSelectedIndexChanged="ddlFollow_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0">--- Select LinkedIn Follow Status ---</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div runat="server" id="enLeads" visible="false">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                <h4><i runat="server" id="iLead"><span runat="server" id="spnLead"></span></i></h4>
                                                                <asp:Label ID="lblCoAddSuccess" runat="server" Text="" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="input-group">
                                                                <asp:LinkButton ID="btnAddNewCo" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnAddNewCo_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnAddNewClear" runat="server" CssClass="btn btn-warning text-center" OnClick="btnAddNewClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnAddNewDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnAddNewDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div id="divAddCoAddress" runat="server" visible="false">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon lblHeader bg-light-blue">Company</span>
                                                                <asp:DropDownList ID="ddlCACompany" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                    OnSelectedIndexChanged="ddlCACompany_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0">--- Select Company Name ---</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon lblHeader bg-light-blue">Address Type</span>
                                                                <asp:DropDownList ID="ddlCAType" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                    Enabled="false" OnSelectedIndexChanged="ddlCAType_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0">--- Select Address Type ---</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon lblHeader bg-light-blue">Major Region</span>
                                                                <asp:DropDownList ID="ddlCARegion" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                    Enabled="false" OnSelectedIndexChanged="ddlCARegion_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0">--- Select Major Geographic Region ---</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon lblHeader bg-light-blue">Sub-Region</span>
                                                                <asp:DropDownList ID="ddlCASRegion" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                    Enabled="false" OnSelectedIndexChanged="ddlCASRegion_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0">--- Select Geographic Sub-Region ---</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon lblHeader bg-light-blue">Country/Nation</span>
                                                                <asp:DropDownList ID="ddlCACountry" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                    Enabled="false" OnSelectedIndexChanged="ddlCACountry_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0">--- Select Country/Nation ---</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon lblHeader bg-light-blue">State/Province</span>
                                                                <asp:DropDownList ID="ddlCAState" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                    Enabled="false" OnSelectedIndexChanged="ddlCAState_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0">--- Select State/Province ---</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon lblHeader bg-light-blue">County/District</span>
                                                                <asp:DropDownList ID="ddlCACounty" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                    Enabled="false" OnSelectedIndexChanged="ddlCACounty_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0">--- Select County/District/Municipality ---</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon lblHeader bg-light-blue">City/Town</span>
                                                                <asp:DropDownList ID="ddlCACity" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                    Enabled="false" OnSelectedIndexChanged="ddlCACity_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0">--- Select City/Town ---</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon lblHeader bg-light-blue">Zip/Postalcode</span>
                                                                <asp:DropDownList ID="ddlCAZip" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                    Enabled="false" OnSelectedIndexChanged="ddlCAZip_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0">--- Select Zip/Postalcode ---</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue lblHeader">Street 1</span>
                                                                <asp:TextBox ID="txtCASt1" runat="server" CssClass="form-control" Text="" Enabled="false" placeholder="Street 1 (required)" />
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue lblHeader">Street 2</span>
                                                                <asp:TextBox ID="txtCASt2" runat="server" CssClass="form-control" Text="" Enabled="false" placeholder="Street 2 (optional)" />
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue lblHeader">Street 3</span>
                                                                <asp:TextBox ID="txtCASt3" runat="server" CssClass="form-control" Text="" Enabled="false" placeholder="Street 3 (optional)" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue lblHeader">Title</span>
                                                                <asp:TextBox ID="txtCATitle" runat="server" CssClass="form-control" Text="" Enabled="false" placeholder="Title (required)" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div runat="server" id="enCoAdd" visible="false">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                <h4><i runat="server" id="iCoAdd"><span runat="server" id="spnCoAdd"></span></i></h4>
                                                                <asp:Label ID="lblCoAddressSuccess" runat="server" Text="" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="input-group">
                                                                <asp:LinkButton ID="btnCAANew" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnCAANew_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnCAAClear" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnCAAClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnCAADone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnCAADone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div id="acctt" class="tab-pane fade">
                    <asp:UpdateProgress ID="uprgrsAccount" runat="server" DisplayAfter="0" DynamicLayout="true" AssociatedUpdatePanelID="upnlAccount">
                        <ProgressTemplate>
                            <div id="divAccountProcessing" runat="server" class="updateProgress">
                                <asp:Image ID="imgAccountProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlAccount">
                        <ContentTemplate>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h5 class="panel-title"><span>Sales & Marketing Leads Accounts</span></h5>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group">
                                                <asp:LinkButton runat="server" ID="btnAddNP" CssClass="btn btn-info text-center" OnClick="btnAddNP_Click"><i class="fa fa-plus"> New Lead</i></asp:LinkButton>
                                                <asp:LinkButton runat="server" ID="btnAddNPEmail" CssClass="btn btn-info text-center" OnClick="btnAddNPEmail_Click"><i class="fa fa-plus"> Email</i></asp:LinkButton>
                                                <asp:LinkButton runat="server" ID="btnAddNPPhone" CssClass="btn btn-info text-center" OnClick="btnAddNPPhone_Click"><i class="fa fa-plus"> Phone</i></asp:LinkButton>
                                                <asp:LinkButton runat="server" ID="btnCancelNP" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnCancelNP_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="divAccList" runat="server">
                                                <asp:GridView ID="gvwLeadCompany" runat="server" AutoGenerateColumns="False" DataKeyNames="coID"
                                                    AllowPaging="True" AllowSorting="True"
                                                    CssClass="mydatagrid" EmptyDataText="No Companies Found." ShowHeaderWhenEmpty="true" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                    PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnPageIndexChanging="gvwLeadCompany_PageIndexChanging"
                                                    OnSelectedIndexChanged="gvwLeadCompany_SelectedIndexChanged">
                                                    <Columns>
                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                            <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                        </asp:ButtonField>
                                                        <asp:BoundField DataField="coID" Visible="false" />
                                                        <asp:BoundField DataField="coName" HeaderText="Company Name" />
                                                        <asp:BoundField DataField="coWebURL" HeaderText="Website" />
                                                        <asp:BoundField DataField="coLnkURL" HeaderText="LinkedIn Company Page" />
                                                        <asp:BoundField DataField="infID" HeaderText="Following on In?" />
                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                                                    </Columns>
                                                </asp:GridView>
                                                <asp:GridView ID="gvwLeadPerson" runat="server" AutoGenerateColumns="False" DataKeyNames="lpID"
                                                    AllowPaging="True" AllowSorting="True"
                                                    CssClass="mydatagrid" EmptyDataText="No Leads Found." ShowHeaderWhenEmpty="true" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                    PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnPageIndexChanging="gvwLeadPerson_PageIndexChanging"
                                                    OnSelectedIndexChanged="gvwLeadPerson_SelectedIndexChanged" Visible="false">
                                                    <Columns>
                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                            <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                        </asp:ButtonField>
                                                        <asp:BoundField DataField="lpID" Visible="false" />
                                                        <asp:BoundField DataField="lastName" HeaderText="Last Name" />
                                                        <asp:BoundField DataField="firstName" HeaderText="First Name" />
                                                        <asp:BoundField DataField="middleName" HeaderText="Middle Name" />
                                                        <asp:BoundField DataField="lpDesignation" HeaderText="Designation" />
                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                                                    </Columns>
                                                </asp:GridView>
                                                <asp:GridView ID="gvwPersonAddress" runat="server" AutoGenerateColumns="False" DataKeyNames="addID"
                                                    AllowPaging="True" AllowSorting="True"
                                                    CssClass="mydatagrid" EmptyDataText="No Addresses found" ShowHeaderWhenEmpty="true"
                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                    PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows"
                                                    OnPageIndexChanging="gvwPersonAddress_PageIndexChanging">
                                                    <Columns>
                                                        <asp:BoundField DataField="addID" Visible="false" />
                                                        <asp:BoundField DataField="title" HeaderText="Title" />
                                                        <asp:BoundField DataField="st1" HeaderText="Street 1" />
                                                        <asp:BoundField DataField="st2" HeaderText="Street 2" />
                                                        <asp:BoundField DataField="st3" HeaderText="Street3" />
                                                        <asp:BoundField DataField="cityID" HeaderText="City / Town" />
                                                        <asp:BoundField DataField="stID" HeaderText="State / Province" />
                                                        <asp:BoundField DataField="zipID" HeaderText="Zip / Postalcode" />
                                                        <asp:BoundField DataField="ctyID" HeaderText="Country / Nation" />
                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                                                    </Columns>
                                                </asp:GridView>
                                                <asp:GridView ID="gvwLeadEmail" runat="server" AutoGenerateColumns="False" DataKeyNames="lemlID"
                                                    AllowPaging="True" AllowSorting="True"
                                                    CssClass="mydatagrid" EmptyDataText="No Email found" ShowHeaderWhenEmpty="true" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                    PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnPageIndexChanging="gvwLeadEmail_PageIndexChanging">
                                                    <Columns>
                                                        <asp:BoundField DataField="lemlID" Visible="false" />
                                                        <asp:BoundField DataField="leadEmail" HeaderText="Email" />
                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                                                    </Columns>
                                                </asp:GridView>
                                                <asp:GridView ID="gvwLeadPhone" runat="server" AutoGenerateColumns="False" DataKeyNames="phID"
                                                    AllowPaging="True" AllowSorting="True"
                                                    CssClass="mydatagrid" EmptyDataText="No Phone Number found" ShowHeaderWhenEmpty="true"
                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                    PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows"
                                                    OnPageIndexChanging="gvwLeadPhone_PageIndexChanging">
                                                    <Columns>
                                                        <asp:BoundField DataField="phID" Visible="false" />
                                                        <asp:BoundField DataField="phValue" HeaderText="Phone Number" />
                                                        <asp:BoundField DataField="phExtension" HeaderText="Extension" />
                                                        <asp:BoundField DataField="dmgID" HeaderText="Type" />
                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="divAddNewAcc" runat="server" visible="false">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                        <div class="input-group">
                                                            <span class="input-group-addon lblHeader bg-light-blue">First Name</span>
                                                            <asp:TextBox ID="txtLPAFName" runat="server" CssClass="form-control" Text=""
                                                                placeholder="First Name (required)" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                        <div class="input-group">
                                                            <span class="input-group-addon lblHeader bg-light-blue">Middle Name</span>
                                                            <asp:TextBox ID="txtLPAMName" runat="server" CssClass="form-control" Text=""
                                                                placeholder="Middle Name (optional)" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                        <div class="input-group">
                                                            <span class="input-group-addon lblHeader bg-light-blue">Last Name</span>
                                                            <asp:TextBox ID="txtLPALName" runat="server" CssClass="form-control" Text=""
                                                                placeholder="Last Name (required)" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">                                                    
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon lblHeader bg-light-blue">Designation</span>
                                                            <asp:TextBox ID="txtLPADesig" runat="server" CssClass="form-control" Text=""
                                                                placeholder="Designation (required)" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon lblHeader bg-light-blue">Company</span>
                                                            <asp:DropDownList ID="ddlLPACompany" runat="server" CssClass="form-control"
                                                                AutoPostBack="true" AppendDataBoundItems="true"
                                                                OnSelectedIndexChanged="ddlLPACompany_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Company Name ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">                                                    
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon lblHeader bg-light-blue">IN Profile</span>
                                                            <asp:TextBox ID="txtLPAIn" runat="server" CssClass="form-control" Enabled="false"
                                                                Text="" placeholder="LinkedIn Profile (optional)" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon lblHeader bg-light-blue">IN Connected</span>
                                                            <asp:DropDownList ID="ddlLPAInConnected" runat="server" CssClass="form-control"
                                                                AutoPostBack="true" AppendDataBoundItems="true"
                                                                Enabled="false" OnSelectedIndexChanged="ddlLPAInConnected_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select LinkedIn Connection Status ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div runat="server" id="enNL" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iNL"><span runat="server" id="spnNL"></span></i></h4>
                                                            <asp:Label ID="lblNLSuccess" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <asp:LinkButton ID="btnAddNewLead" runat="server" CssClass="btn btn-default text-center"
                                                                Enabled="false" OnClick="btnAddNewLead_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnAddNewLeadClear" runat="server" CssClass="btn btn-default text-center"
                                                                Enabled="false" OnClick="btnAddNewLeadClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnAddNewLeadDone" runat="server" CssClass="btn btn-danger text-center"
                                                                OnClick="btnAddNewLeadDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="divLeadEmail" runat="server" visible="false">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon lblHeader bg-light-blue">Company</span>
                                                            <asp:DropDownList ID="ddlNEmlCompany" runat="server" CssClass="form-control" AutoPostBack="true"
                                                                AppendDataBoundItems="true"
                                                                OnSelectedIndexChanged="ddlNEmlCompany_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Company Name ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon lblHeader bg-light-blue">Lead</span>
                                                            <asp:DropDownList ID="ddlNEmlLeadAcc" runat="server" CssClass="form-control" AutoPostBack="true"
                                                                AppendDataBoundItems="true" Enabled="false"
                                                                OnSelectedIndexChanged="ddlNEmlLeadAcc_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Lead Account ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon lblHeader bg-light-blue">Email</span>
                                                            <asp:TextBox ID="txtLeadEmail" runat="server" CssClass="form-control" Text="" TextMode="Email"
                                                                Enabled="false" placeholder="Lead Account Email Address (required)" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon lblHeader bg-light-blue">Type</span>
                                                            <asp:DropDownList ID="ddlNEmlType" runat="server" CssClass="form-control" AutoPostBack="true"
                                                                AppendDataBoundItems="true" Enabled="false"
                                                                OnSelectedIndexChanged="ddlNEmlType_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Email Type ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div runat="server" id="enNEml" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iNEml"><span runat="server" id="spnNEml"></span></i></h4>
                                                            <asp:Label ID="lblNEmlSuccess" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <asp:LinkButton ID="btnNewEmailAdd" runat="server" CssClass="btn btn-default text-center"
                                                                Enabled="false" OnClick="btnNewEmailAdd_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnNewEmailClear" runat="server" CssClass="btn btn-default text-center"
                                                                Enabled="false" OnClick="btnNewEmailClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnNewEmailDone" runat="server" CssClass="btn btn-danger text-center"
                                                                OnClick="btnNewEmailDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="divLeadPhone" runat="server" visible="false">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon lblHeader bg-light-blue">Company</span>
                                                            <asp:DropDownList ID="ddlNPhCompany" runat="server" CssClass="form-control" AutoPostBack="true"
                                                                AppendDataBoundItems="true"
                                                                OnSelectedIndexChanged="ddlNPhCompany_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Company Name ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon lblHeader bg-light-blue">Lead</span>
                                                            <asp:DropDownList ID="ddlNPhLead" runat="server" CssClass="form-control" AutoPostBack="true"
                                                                AppendDataBoundItems="true"
                                                                Enabled="false" OnSelectedIndexChanged="ddlNPhLead_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Lead Account ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                        <div class="input-group">
                                                            <span class="input-group-addon lblHeader bg-light-blue">Phone</span>
                                                            <asp:TextBox ID="txtNPhNumber" runat="server" CssClass="form-control" Text="" TextMode="Phone"
                                                                Enabled="false" placeholder="Lead Account Phone Number (required)" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                        <div class="input-group">
                                                            <span class="input-group-addon lblHeader bg-light-blue">Extension</span>
                                                            <asp:TextBox ID="txtNPhExtension" runat="server" CssClass="form-control" Text="" Enabled="false"
                                                                placeholder="Extension (optional)" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon lblHeader bg-light-blue">Type</span>
                                                            <asp:DropDownList ID="ddlNPhType" runat="server" CssClass="form-control" AutoPostBack="true"
                                                                AppendDataBoundItems="true"
                                                                Enabled="false" OnSelectedIndexChanged="ddlNPhType_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Phone Type ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div runat="server" id="enNPh" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iNPh"><span runat="server" id="spnNPh"></span></i></h4>
                                                            <asp:Label ID="lblNPhSuccess" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <asp:LinkButton ID="btnNewPhoneAdd" runat="server" CssClass="btn btn-default text-center"
                                                                Enabled="false" OnClick="btnNewPhoneAdd_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnNewPhoneAddClear" runat="server" CssClass="btn btn-default text-center"
                                                                Enabled="false" OnClick="btnNewPhoneAddClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnNewPhoneAddDone" runat="server" CssClass="btn btn-danger text-center"
                                                                OnClick="btnNewPhoneAddDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div id="comm" class="tab-pane fade">
                    <asp:UpdateProgress ID="uprgrsCycle" runat="server" DisplayAfter="0" DynamicLayout="true" AssociatedUpdatePanelID="upnlCycle">
                        <ProgressTemplate>
                            <div id="divCycleProcessing" runat="server" class="updateProgress">
                                <asp:Image ID="imgCycleProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlCycle">
                        <ContentTemplate>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h5 class="panel-title"><span>Lead Nurture Cycle</span></h5>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group">
                                                <asp:LinkButton runat="server" ID="btnNewComment" CssClass="btn btn-info text-center" OnClick="btnNewComment_Click"><i class="fa fa-plus"> New Comment</i></asp:LinkButton>
                                                <asp:LinkButton runat="server" ID="btnClearNValues" CssClass="btn btn-default text-center" OnClick="btnClearNValues_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="divCommList" runat="server">
                                                <asp:GridView ID="gvwCommCompany" runat="server" AutoGenerateColumns="False" DataKeyNames="coID" AllowPaging="True" AllowSorting="True"
                                                    CssClass="mydatagrid" EmptyDataText="No Companies Found." ShowHeaderWhenEmpty="true" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                    PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnPageIndexChanging="gvwCommCompany_PageIndexChanging"
                                                    OnSelectedIndexChanged="gvwCommCompany_SelectedIndexChanged">
                                                    <Columns>
                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                            <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                        </asp:ButtonField>
                                                        <asp:BoundField DataField="coID" Visible="false" />
                                                        <asp:BoundField DataField="coName" HeaderText="Company Name" />
                                                        <asp:BoundField DataField="coWebURL" HeaderText="Website" />
                                                        <asp:BoundField DataField="coLnkURL" HeaderText="LinkedIn Company Page" />
                                                        <asp:BoundField DataField="infID" HeaderText="Following on In?" />
                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                                                    </Columns>
                                                </asp:GridView>
                                                <asp:GridView ID="gvwCommLA" runat="server" AutoGenerateColumns="False" DataKeyNames="lpID" AllowPaging="True" AllowSorting="True"
                                                    CssClass="mydatagrid" EmptyDataText="No Leads Found." ShowHeaderWhenEmpty="true" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                    PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnPageIndexChanging="gvwCommLA_PageIndexChanging"
                                                    OnSelectedIndexChanged="gvwCommLA_SelectedIndexChanged">
                                                    <Columns>
                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                            <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                        </asp:ButtonField>
                                                        <asp:BoundField DataField="lpID" Visible="false" />
                                                        <asp:BoundField DataField="lastName" HeaderText="Last Name" />
                                                        <asp:BoundField DataField="firstName" HeaderText="First Name" />
                                                        <asp:BoundField DataField="middleName" HeaderText="Middle Name" />
                                                        <asp:BoundField DataField="lpDesignation" HeaderText="Designation" />
                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                                                    </Columns>
                                                </asp:GridView>
                                                <asp:GridView ID="gvwCommList" runat="server" AutoGenerateColumns="False" DataKeyNames="lnID" AllowPaging="True" AllowSorting="True"
                                                    CssClass="mydatagrid" EmptyDataText="No Lead Nurturing process found for selected Lead/Person." ShowHeaderWhenEmpty="true"
                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                    RowStyle-CssClass="rows" OnPageIndexChanging="gvwCommList_PageIndexChanging" OnSelectedIndexChanged="gvwCommList_SelectedIndexChanged">
                                                    <Columns>
                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                            <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                        </asp:ButtonField>
                                                        <asp:BoundField DataField="lnID" Visible="false" />
                                                        <asp:BoundField DataField="lnComment" HeaderText="Comment" />
                                                        <asp:BoundField DataField="UserId" HeaderText="FE Contact Person" />
                                                        <asp:BoundField DataField="lnTimeStamp" HeaderText="Contact Timestamp" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                                                        <asp:BoundField DataField="ctID" HeaderText="Contact Type" />
                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                                                    </Columns>
                                                </asp:GridView>
                                                <div>
                                                    <asp:TextBox runat="server" ID="txtCommDetail" Text="" TextMode="MultiLine" CssClass="form-control" Height="250px" Width="100%"
                                                        Font-Size="Large" ReadOnly="True" Visible="False"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="divCommAdd" runat="server" visible="false">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon lblHeader bg-light-blue">Lead Company</span>
                                                            <asp:DropDownList ID="ddlCommAddCompany" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                OnSelectedIndexChanged="ddlCommAddCompany_SelectedIndexChanged">
                                                                <asp:ListItem Value="0" Text="--- Select Company Name ---" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon lblHeader bg-light-blue">Lead Account</span>
                                                            <asp:DropDownList ID="ddlCommAddLead" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                Enabled="false" OnSelectedIndexChanged="ddlCommAddLead_SelectedIndexChanged">
                                                                <asp:ListItem Value="0" Text="--- Select Lead Name ---" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon lblHeader bg-light-blue">Comments</span>
                                                            <asp:TextBox ID="txtComms" runat="server" CssClass="form-control" Text="" TextMode="MultiLine" Enabled="false" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon lblHeader bg-light-blue">FE Rep.</span>
                                                            <asp:DropDownList ID="ddlCommFEPerson" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                Enabled="false" OnSelectedIndexChanged="ddlCommFEPerson_SelectedIndexChanged">
                                                                <asp:ListItem Value="0" Text="--- Select FE Contact Person ---" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon lblHeader bg-light-blue">Contact Timestamp</span>
                                                            <asp:TextBox runat="server" ID="txtCommTimeStamp" CssClass="form-control" Text="" Enabled="false"></asp:TextBox>
                                                            <ajaxToolkit:CalendarExtender ID="clndrTimeStamp" runat="server" TargetControlID="txtCommTimeStamp" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon lblHeader bg-light-blue">Contact Type</span>
                                                            <asp:DropDownList ID="ddlContactType" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                Enabled="false" OnSelectedIndexChanged="ddlContactType_SelectedIndexChanged">
                                                                <asp:ListItem Value="0" Text="--- Select Contact Type ---" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div runat="server" id="enComm" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iComm"><span runat="server" id="spnComm"></span></i></h4>
                                                            <asp:Label ID="lblCommentSuccess" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <asp:LinkButton ID="btnCommentAdd" runat="server" CssClass="btn btn-default text-center" OnClick="btnCommentAdd_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnCommentClear" runat="server" CssClass="btn btn-default text-center" OnClick="btnCommentClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnCommentDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnCommentDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</div>
