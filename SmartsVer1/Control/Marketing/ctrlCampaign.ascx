﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlCampaign.ascx.cs" Inherits="SmartsVer1.Control.Marketing.ctrlCampaign" %>
<!-- Content Header (Page header) -->
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<!-- Stylesheet -->
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<!-- Javascript -->
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>

<!--- Page local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }

    .rotate {
        -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
        -moz-transform: rotate(90deg); /* Firefox */
        -ms-transform: rotate(90deg); /* IE 9 */
        transform: rotate(90deg); /* Standard syntax */
    }

    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper[data-stepper-steps="8"] li {
            width: 12%;
        }

        ol.stepper[data-stepper-steps="9"] li {
            width: 11%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }    
</style>
<!--- Page local script --->
<script type="text/javascript">
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("rotate");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").addClass("rotate");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("rotate");
        });
    });
</script>

<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">        
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>Sales/Marketing</li>
                <li><a href="../../Marketing/Campaign_2A.aspx"> S&M Campaigns</a></li>
            </ol>
        </div>
    </div>    
</div>

<!-- Page Content -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box box-primary">
            <div class="nav-tabs-custom">
                <ul class="nav nav-pills">
                    <li class="nav-item active"><a data-toggle="pill" data-target="#Camp">Campaign List</a></li>
                    <li class="nav-item"><a data-toggle="pill" data-target="#Price">Campaign Prices</a></li>
                </ul>
                <div class="tab-content">
                    <div id="Camp" class="tab-pane fade in active">
                        <div class="box box-body">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div id="boxCampaignTitle" class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="bg-light-blue">
                                            <div class="text-center">
                                                <h1 class="box-title">Campaign List</h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <asp:UpdateProgress ID="upgrsCampaign" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlCampaign">
                                    <ProgressTemplate>
                                        <div id="divCampaignProcessing" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle;
                                            text-align: center; z-index: 100;">
                                            <asp:Image ID="imgCampaignProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:UpdatePanel ID="upnlCampaign" runat="server">
                                    <ContentTemplate>
                                        <div id="divCampaignList" runat="server">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <asp:GridView ID="gvwCampaign" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                    AllowPaging="True" AllowSorting="True" DataKeyNames="scID" OnPageIndexChanging="gvwCampaign_PageIndexChanging"
                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                    RowStyle-CssClass="rows" EmptyDataText="No Sales Campaign found" ShowHeaderWhenEmpty="true" OnRowDataBound="gvwCampaign_RowDataBound">
                                                    <Columns>
                                                        <asp:BoundField DataField="scID" Visible="False" />
                                                        <asp:BoundField DataField="scName" HeaderText="Campaign Name" />
                                                        <asp:BoundField DataField="minContract" HeaderText="Min. Contract Length (Months)" />
                                                        <asp:BoundField DataField="minWell" HeaderText="Min. Well Count" />
                                                        <asp:BoundField DataField="cStart" HeaderText="Campaign Start" DataFormatString="{0:MMM d , yyyy}" />
                                                        <asp:BoundField DataField="cEnd" HeaderText="Campaign End" DataFormatString="{0:MMM d , yyyy}" />
                                                        <asp:BoundField DataField="cPromotion" HeaderText="Promotion Period" />
                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group">
                                                    <asp:Button ID="btnCampAdd" runat="server" CssClass="btn btn-primary" Text="Add New Campaign" OnClick="btnCampAdd_Click" />
                                                    <asp:Button ID="btnCampCancel" runat="server" CssClass="btn btn-warning" Text="Cancel" Visible="false" OnClick="btnCampCancel_Click" />
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divCampaignAdd" runat="server" visible="false">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon bg-light-blue">Sales Campaign Name</span>
                                                            <asp:TextBox ID="txtCName" runat="server" CssClass="form-control" Text="" />
                                                            <asp:RequiredFieldValidator ID="rfvCName" runat="server" ControlToValidate="txtCName" Display="Dynamic" ForeColor="Maroon"
                                                                ErrorMessage="*" SetFocusOnError="true" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon bg-light-blue">Min. Contract Length (Months)</span>
                                                            <asp:TextBox ID="txtMinCtrt" runat="server" CssClass="form-control" Text="" placeholder="Enter Zero (0), if not required" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon bg-light-blue">Min. Well Count</span>
                                                            <asp:TextBox ID="txtMinWell" runat="server" CssClass="form-control" Text="" placeholder="Enter Zero (0), if not required" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon bg-light-blue">Start Date</span>
                                                            <asp:TextBox ID="txtCStart" runat="server" CssClass="form-control" Text="" />
                                                            <ajaxToolkit:CalendarExtender ID="clndrCStart" runat="server" TargetControlID="txtCStart" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon bg-light-blue">End Date</span>
                                                            <asp:TextBox ID="txtCEnd" runat="server" CssClass="form-control" Text="" />
                                                            <ajaxToolkit:CalendarExtender ID="clndrCEnd" runat="server" TargetControlID="txtCEnd" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon bg-light-blue">Promotion Months</span>
                                                            <asp:TextBox ID="txtCPromo" runat="server" CssClass="form-control" Text="" placeholder="Promotion Months Count" />
                                                            <asp:RequiredFieldValidator ID="rfvCPromo" runat="server" ControlToValidate="txtCPromo" Display="Dynamic" ForeColor="Maroon"
                                                                ErrorMessage="*" SetFocusOnError="true" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <asp:Label ID="lblCSuccess" runat="server" Text="" />
                                                <br />
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group">
                                                    <asp:Button ID="btnAddCamp" runat="server" CssClass="btn btn-success" Text="Add New Campaign" OnClick="btnAddCamp_Click" />
                                                    <asp:Button ID="btnAddCampClear" runat="server" CssClass="btn btn-warning" Text="Clear Values" OnClick="btnAddCampClear_Click"
                                                        CausesValidation="false" />
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group">
                                                    <asp:Button ID="btnAddCampDone" runat="server" CssClass="btn btn-primary" Text="Done" Visible="false" OnClick="btnAddCampDone_Click"
                                                        CausesValidation="false" />
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                    <div id="Price" class="tab-pane fade">
                        <div class="box box-body">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div id="boxPriceTitle" class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="bg-light-blue">
                                            <div class="text-center">
                                                <h1 class="box-title">Campaign Prices List</h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:UpdateProgress ID="uprgrsPrices" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlPrices">
                                    <ProgressTemplate>
                                        <div id="divPricesProcessing" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                            z-index: 100;">
                                            <asp:Image ID="imgPricesProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:UpdatePanel ID="upnlPrices" runat="server">
                                    <ContentTemplate>
                                        <div id="divPriceList" runat="server">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <asp:GridView ID="gvwPCampaign" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                    AllowPaging="True" AllowSorting="True" DataKeyNames="scID" OnPageIndexChanging="gvwPCampaign_PageIndexChanging"
                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                    RowStyle-CssClass="rows" EmptyDataText="No Sales Campaign found" ShowHeaderWhenEmpty="true" OnRowDataBound="gvwPCampaign_RowDataBound"
                                                    OnSelectedIndexChanged="gvwPCampaign_SelectedIndexChanged">
                                                    <Columns>
                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                            <ControlStyle CssClass="btn btn-primary text-center"></ControlStyle>
                                                        </asp:ButtonField>
                                                        <asp:BoundField DataField="scID" Visible="False" />
                                                        <asp:BoundField DataField="scName" HeaderText="Campaign Name" />
                                                        <asp:BoundField DataField="minContract" HeaderText="Min. Contract Length (Months)" />
                                                        <asp:BoundField DataField="minWell" HeaderText="Min. Well Count" />
                                                        <asp:BoundField DataField="cStart" HeaderText="Campaign Start" DataFormatString="{0:MMM d , yyyy}" />
                                                        <asp:BoundField DataField="cEnd" HeaderText="Campaign End" DataFormatString="{0:MMM d , yyyy}" />
                                                        <asp:BoundField DataField="cPromotion" HeaderText="Promotion Period" />
                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                    </Columns>
                                                </asp:GridView>
                                                <asp:GridView ID="gvwPrices" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                    AllowPaging="True" AllowSorting="True" DataKeyNames="cpID" OnPageIndexChanging="gvwPrices_PageIndexChanging"
                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                    RowStyle-CssClass="rows" EmptyDataText="No Prices found" ShowHeaderWhenEmpty="true" Visible="false" OnRowDeleting="gvwPrices_RowDeleting">
                                                    <Columns>
                                                        <asp:BoundField DataField="cpID" Visible="False" />
                                                        <asp:BoundField DataField="srvGrpID" HeaderText="SMARTs Service Group" />
                                                        <asp:BoundField DataField="srvID" HeaderText="SMARTs Service" />
                                                        <asp:BoundField DataField="cpReducer" HeaderText="Price Reducer" />
                                                        <asp:BoundField DataField="stdPrice" HeaderText="Standard Price ( US$ )" />
                                                        <asp:BoundField DataField="proPrice" HeaderText="Campaign Price ( US$ )" />
                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                        <asp:CommandField ShowDeleteButton="True" DeleteText="Remove" ItemStyle-ForeColor="Blue"></asp:CommandField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div runat="server" id="enDPrice" visible="false">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                    <h4><i runat="server" id="iDPrice"><span runat="server" id="spnDPrice"></span></i></h4>
                                                    <asp:Label ID="lblDeletePrice" runat="server" />
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group">
                                                    <asp:Button ID="btnPricesNew" runat="server" CssClass="btn btn-primary" Text="Add New Price" OnClick="btnPricesNew_Click" />
                                                    <asp:Button ID="btnPricesCancel" runat="server" CssClass="btn btn-warning" Text="Cancel" Visible="false" OnClick="btnPricesCancel_Click" />
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divPriceAdd" runat="server" visible="false">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon bg-light-blue" style="width: 13%;">Select Sales Campaign</span>
                                                            <asp:DropDownList ID="ddlPACampaign" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                OnSelectedIndexChanged="ddlPACampaign_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Sales & Marketing Campaign ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon bg-light-blue" style="width: 13%;">Select Service Group</span>
                                                            <asp:DropDownList ID="ddlCSrvGrp" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                Enabled="false" OnSelectedIndexChanged="ddlCSrvGrp_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select SMARTs Service Group ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon bg-light-blue" style="width: 13%;">Select SMARTs Service</span>
                                                            <asp:DropDownList ID="ddlCSrv" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                Enabled="false" OnSelectedIndexChanged="ddlCSrv_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select SMARTs Service ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon bg-light-blue" style="width: 13%;">Reduce Price by (% Value)</span>
                                                            <asp:TextBox ID="txtCReducer" runat="server" CssClass="form-control" Enabled="false" Text="" />
                                                            <asp:RequiredFieldValidator ID="rfvCReducer" runat="server" ForeColor="Maroon" Display="Dynamic" SetFocusOnError="true" ErrorMessage="*"
                                                                ControlToValidate="txtCReducer" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon bg-light-blue" style="width: 13%;">Standard Service Price</span>
                                                            <asp:Label ID="lblStandardPrice" runat="server" CssClass="form-control" Visible="false" Text="" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon bg-light-blue" style="width: 13%;">Promotion Service Price</span>
                                                            <asp:Label ID="lblPromotionPrice" runat="server" CssClass="form-control" Visible="false" Text="" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div runat="server" id="divPAEnclosure" visible="false">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                    <h4><i runat="server" id="iPA"><span runat="server" id="spnPA"></span></i></h4>
                                                    <asp:Label ID="lblPASuccess" runat="server" />
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group">
                                                    <asp:Button ID="btnPACampCheck" runat="server" CssClass="btn btn-info" Text="Check New Value" Enabled="false" OnClick="btnAddCampCheck_Click" />
                                                    <asp:Button ID="btnPAdd" runat="server" CssClass="btn btn-success" Text="Add New Price" Visible="false" OnClick="btnPAdd_Click" />
                                                    <asp:Button ID="btnPClear" runat="server" CssClass="btn btn-warning" Text="Clear Values" Visible="false" OnClick="btnPClear_Click"
                                                        CausesValidation="false" />
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group">
                                                    <asp:Button ID="btnPADone" runat="server" CssClass="btn btn-primary" Text="Done" Visible="false" OnClick="btnPADone_Click"
                                                        CausesValidation="false" />
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- JavaScripts -->
<%--<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>--%>