﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI.WebControls;
using CLR = System.Drawing.Color;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using SRV = SmartsVer1.Helpers.DemogHelpers.ServicesConfig;
using SL = SmartsVer1.Helpers.SalesHelpers.Sales;
using log4net;

namespace SmartsVer1.Control.Marketing
{
    public partial class ctrlCampaign : System.Web.UI.UserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ctrlCampaign));
        String LoginName, username, domain, GetClientDBString;
        Int32 ClientID = -99;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
                //DataSources
                System.Data.DataTable cmpTable = SL.getCampaignTable();
                this.gvwCampaign.DataSource = cmpTable;
                this.gvwCampaign.DataBind();
                this.gvwPCampaign.DataSource = cmpTable;
                this.gvwPCampaign.DataBind();
                Dictionary<Int32, String> scList = SL.getCampaignList();
                this.ddlPACampaign.Items.Clear();
                this.ddlPACampaign.DataSource = scList;
                this.ddlPACampaign.DataTextField = "Value";
                this.ddlPACampaign.DataValueField = "Key";
                this.ddlPACampaign.DataBind();
                this.ddlPACampaign.Items.Insert(0, new ListItem("--- Select Sales & Marketing Campaign ---", "-1"));
                this.ddlPACampaign.SelectedIndex = -1;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.clndrCStart.SelectedDate = DateTime.Now;
                this.clndrCEnd.SelectedDate = DateTime.Now;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwCampaign_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable cmpTable = SL.getCampaignTable();
                this.gvwCampaign.PageIndex = e.NewPageIndex;
                this.gvwCampaign.DataSource = cmpTable;
                this.gvwCampaign.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCampAdd_Click(object sender, EventArgs e)
        {
            try
            {
                btnCampCancel_Click(null, null);
                this.divCampaignList.Visible = false;
                this.divCampaignAdd.Visible = true;
                this.btnAddCampDone.Visible = true;

                this.txtCName.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCampCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.gvwCampaign.SelectedIndex = -1;
                this.btnCampCancel.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddCampCheck_Click(object sender, EventArgs e)
        {
            try
            {
                Decimal redValue = -99.00M;
                redValue = Convert.ToDecimal(this.txtCReducer.Text);
                if (redValue < 0.00M)
                {
                    this.divPAEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iPA.Attributes["class"] = "icon fa fa-warning";
                    this.spnPA.InnerText = " Incorrect information!";
                    this.lblPASuccess.Text = "Please provide a positive price reducer";
                    this.divPAEnclosure.Visible = true;
                    this.txtCReducer.Text = "";
                    this.txtCReducer.Focus();
                }
                else if (redValue.Equals(100.00M))
                {
                    this.divPAEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iPA.Attributes["class"] = "icon fa fa-warning";
                    this.spnPA.InnerText = " Incorrect information!";
                    this.lblPASuccess.Text = "This will make the Price $0.00. Price can't be reduced to $0.00";
                    this.divPAEnclosure.Visible = true;
                    this.txtCReducer.Text = "";
                    this.txtCReducer.Focus();
                }
                else if (redValue > 100.00M)
                {
                    this.divPAEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iPA.Attributes["class"] = "icon fa fa-warning";
                    this.spnPA.InnerText = " Incorrect information!";
                    this.lblPASuccess.Text = "Please provide a positive price reducer (Price can't be reduced by more than 100%)";
                    this.divPAEnclosure.Visible = true;
                    this.txtCReducer.Text = "";
                    this.txtCReducer.Focus();
                }
                else
                {
                    Int32 sgID = Convert.ToInt32(this.ddlCSrvGrp.SelectedValue);
                    Int32 svID = Convert.ToInt32(this.ddlCSrv.SelectedValue);
                    List<String> sp = SRV.getStandardPriceWithUnitName(sgID, svID);
                    Decimal stdP = Convert.ToDecimal(sp[0]);
                    Decimal proP = Decimal.Subtract(stdP, Decimal.Multiply(stdP, Decimal.Divide(redValue, 100.00M)));
                    this.divPAEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                    this.iPA.Attributes["class"] = "icon fa fa-check-circle";
                    this.spnPA.InnerText = " New Price accepted";
                    this.lblPASuccess.Text = String.Format("{0} {1}", Convert.ToString(proP), Convert.ToString(sp[1]));
                    this.divPAEnclosure.Visible = true;
                    this.btnPAdd.Visible = true;
                    this.btnPAdd.Focus();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddCamp_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, mC = -99, mW = -99, pro = -99;
                String name = String.Empty;
                DateTime start = DateTime.Now, end = DateTime.Now;
                name = Convert.ToString(this.txtCName.Text);
                mC = Convert.ToInt32(this.txtMinCtrt.Text);
                mW = Convert.ToInt32(this.txtMinWell.Text);
                start = Convert.ToDateTime(this.txtCStart.Text);
                end = Convert.ToDateTime(this.txtCEnd.Text);
                pro = Convert.ToInt32(this.txtCPromo.Text);
                if ((end < start) || (end.Equals(start)))
                {
                    this.lblCSuccess.ForeColor = CLR.Maroon;
                    this.lblCSuccess.Text = "Campaign End Date needs to be after Start Date";
                    this.lblCSuccess.Visible = true;

                    this.txtCStart.Focus();
                }
                else
                {
                    iResult = SL.insertSalesCampaign(name, mC, mW, start, end, pro, username, domain);
                    if (iResult.Equals(1))
                    {
                        btnAddCampClear_Click(null, null);
                        this.lblCSuccess.ForeColor = CLR.Green;
                        this.lblCSuccess.Text = "Successfully inserted New Campaign into Database";
                        this.lblCSuccess.Visible = true;

                        this.btnAddCampDone.Focus();
                    }
                    else
                    {
                        btnAddCampClear_Click(null, null);
                        this.lblCSuccess.ForeColor = CLR.Maroon;
                        this.lblCSuccess.Text = "Unable to insert New Campaign into Database";
                        this.lblCSuccess.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddCampClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtCName.Text = "";
                this.txtMinCtrt.Text = "";
                this.txtMinWell.Text = "";
                this.txtCStart.Text = "";
                this.txtCEnd.Text = "";
                this.txtCPromo.Text = "";
                this.lblCSuccess.Text = "";
                this.lblCSuccess.Visible = false;

                this.txtCName.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCSrvGrp_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 sgID = Convert.ToInt32(this.ddlCSrvGrp.SelectedValue);
                Dictionary<Int32, String> sList = SRV.getServiceList(sgID);
                this.ddlCSrv.Items.Clear();
                this.ddlCSrv.DataSource = sList;
                this.ddlCSrv.DataTextField = "Value";
                this.ddlCSrv.DataValueField = "Key";
                this.ddlCSrv.DataBind();
                this.ddlCSrv.Items.Insert(0, new ListItem("--- Select SMARTs Service ---", "-1"));
                this.ddlCSrv.SelectedIndex = -1;
                this.ddlCSrv.Enabled = true;
                this.txtCReducer.Text = "";
                this.txtCReducer.Enabled = false;
                this.lblStandardPrice.Text = "";
                this.lblStandardPrice.Visible = false;
                this.lblPromotionPrice.Text = "";
                this.lblPromotionPrice.Visible = false;
                this.btnPACampCheck.Enabled = false;
                this.btnAddCamp.Visible = false;
                this.btnAddCampClear.Visible = true;

                this.ddlCSrv.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCSrv_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 sgID = Convert.ToInt32(this.ddlCSrvGrp.SelectedValue);
                Int32 svID = Convert.ToInt32(this.ddlCSrv.SelectedValue);
                this.txtCReducer.Text = "";
                this.txtCReducer.Enabled = true;
                List<String> sP = SRV.getStandardPriceWithUnitName(sgID, svID);
                if (sP[0].Equals("-1"))
                {
                    this.txtCReducer.Enabled = false;
                    this.lblStandardPrice.Text = "---";
                    this.lblStandardPrice.Visible = true;
                    this.lblCSuccess.ForeColor = CLR.Maroon;
                    this.lblCSuccess.Text = "No Standard Price defined for selected Service. Unable to proceed";
                    this.lblCSuccess.Visible = true;
                    this.lblPromotionPrice.Text = "";
                    this.lblPromotionPrice.Visible = false;
                    this.lblStandardPrice.Text = "";
                    this.lblStandardPrice.Visible = false;
                    this.btnPACampCheck.Enabled = false;
                    this.btnAddCamp.Visible = false;
                    this.btnAddCampClear.Visible = true;

                    this.ddlCSrv.Focus();
                }
                else
                {
                    this.lblStandardPrice.Text = String.Format("{0} {1}", Convert.ToString(sP[0]), Convert.ToString(sP[1]));
                    this.lblStandardPrice.Visible = true;
                    this.lblPromotionPrice.Text = "";
                    this.lblPromotionPrice.Visible = false;
                    this.btnPACampCheck.Enabled = true;
                    this.btnAddCamp.Visible = false;
                    this.btnAddCampClear.Visible = true;
                    this.lblCSuccess.Text = "";
                    this.lblCSuccess.Visible = false;
                    this.txtCReducer.Enabled = true;
                    this.txtCReducer.Text = "";

                    this.txtCReducer.Focus();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddCampDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnAddCampClear_Click(null, null);
                this.divCampaignList.Visible = true;
                this.divCampaignAdd.Visible = false;
                this.btnAddCampDone.Visible = false;
                System.Data.DataTable cmpTable = SL.getCampaignTable();
                this.gvwCampaign.DataSource = cmpTable;
                this.gvwCampaign.DataBind();

                this.gvwCampaign.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwPrices_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Int32 pcID = Convert.ToInt32(this.gvwPCampaign.SelectedValue);
            System.Data.DataTable pcTable = SL.getPriceTable(pcID);
            this.gvwPrices.PageIndex = e.NewPageIndex;
            this.gvwPrices.DataSource = pcTable;
            this.gvwPrices.DataBind();
        }

        protected void ddlPACampaign_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> sgList = SRV.getSrvGroupList();
                this.ddlCSrvGrp.Items.Clear();
                this.ddlCSrvGrp.DataSource = sgList;
                this.ddlCSrvGrp.DataTextField = "Value";
                this.ddlCSrvGrp.DataValueField = "Key";
                this.ddlCSrvGrp.DataBind();
                this.ddlCSrvGrp.Items.Insert(0, new ListItem("--- Select SMARTs Service Group ---", "-1"));
                this.ddlCSrvGrp.SelectedIndex = -1;
                this.ddlCSrvGrp.Enabled = true;
                this.ddlCSrv.Items.Clear();
                this.ddlCSrv.DataBind();
                this.ddlCSrv.Items.Insert(0, new ListItem("--- Select SMARTs Service ---", "-1"));
                this.ddlCSrv.SelectedIndex = -1;
                this.ddlCSrv.Enabled = false;
                this.txtCReducer.Text = "";
                this.txtCReducer.Enabled = false;
                this.lblStandardPrice.Text = "";
                this.lblStandardPrice.Visible = false;
                this.lblPromotionPrice.Text = "";
                this.lblPromotionPrice.Visible = false;
                this.divPAEnclosure.Visible = false;
                this.btnPACampCheck.Enabled = false;
                this.btnPAdd.Visible = false;
                this.btnPClear.Visible = true;

                this.ddlCSrvGrp.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnPricesNew_Click(object sender, EventArgs e)
        {
            try
            {
                btnPricesCancel_Click(null, null);
                this.divPriceList.Visible = false;
                this.divPriceAdd.Visible = true;
                this.btnPADone.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnPricesCancel_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable cmpTable = SL.getCampaignTable();
                this.gvwPCampaign.DataSource = cmpTable;
                this.gvwPCampaign.SelectedIndex = -1;
                this.gvwPCampaign.DataBind();                
                this.gvwPCampaign.Visible = true;
                this.gvwPrices.PageIndex = 0;
                this.gvwPrices.DataBind();
                this.gvwPrices.Visible = false;
                this.btnPricesCancel.Visible = false;
                this.enDPrice.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnPAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, id = -99, grp = -99, srv = -99;
                Decimal red = -99.00M;
                id = Convert.ToInt32(this.ddlPACampaign.SelectedValue);
                grp = Convert.ToInt32(this.ddlCSrvGrp.SelectedValue);
                srv = Convert.ToInt32(this.ddlCSrv.SelectedValue);
                red = Convert.ToDecimal(this.txtCReducer.Text);
                if (red < 0.00M)
                {
                    this.divPAEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iPA.Attributes["class"] = "icon fa fa-warning";
                    this.spnPA.InnerText = " Missing information!";
                    this.lblPASuccess.Text = "Incorrect Price Reducer value";
                    this.divPAEnclosure.Visible = true;
                    this.txtCReducer.Text = "";
                }
                else
                {
                    iResult = SL.insertSalesCampaignPrice(id, grp, srv, red, username, domain);
                    if (iResult.Equals(1))
                    {
                        btnPClear_Click(null, null);
                        this.divPAEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iPA.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnPA.InnerText = " Success";
                        this.lblPASuccess.Text = "Successfully inserted selected Campaign Price to database";
                        this.divPAEnclosure.Visible = true;

                        this.btnPADone.Focus();
                    }
                    else
                    {
                        btnPClear_Click(null, null);
                        this.divPAEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iPA.Attributes["class"] = "icon fa fa-warning";
                        this.spnPA.InnerText = " Missing information!";
                        this.lblPASuccess.Text = "Unable to insert selected Campaign Price to database";
                        this.divPAEnclosure.Visible = true;
                        this.btnPADone.Focus();
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnPClear_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> scList = SL.getCampaignList();
                this.ddlPACampaign.Items.Clear();
                this.ddlPACampaign.DataSource = scList;
                this.ddlPACampaign.DataTextField = "Value";
                this.ddlPACampaign.DataValueField = "Key";
                this.ddlPACampaign.DataBind();
                this.ddlPACampaign.Items.Insert(0, new ListItem("--- Select Sales & Marketing Campaign ---", "-1"));
                this.ddlPACampaign.SelectedIndex = -1;
                this.ddlCSrvGrp.Items.Clear();
                this.ddlCSrvGrp.DataBind();
                this.ddlCSrvGrp.Items.Insert(0, new ListItem("--- Select SMARTs Service Group ---", "-1"));
                this.ddlCSrvGrp.SelectedIndex = -1;
                this.ddlCSrvGrp.Enabled = false;
                this.ddlCSrv.Items.Clear();
                this.ddlCSrv.DataBind();
                this.ddlCSrv.Items.Insert(0, new ListItem("--- Select SMARTs Service ---", "-1"));
                this.ddlCSrv.SelectedIndex = -1;
                this.ddlCSrv.Enabled = false;
                this.txtCReducer.Text = "";
                this.txtCReducer.Enabled = false;
                this.lblStandardPrice.Text = "";
                this.lblStandardPrice.Visible = false;
                this.lblPromotionPrice.Text = "";
                this.lblPromotionPrice.Visible = false;
                this.divPAEnclosure.Visible = false;
                this.btnPACampCheck.Enabled = false;
                this.btnPAdd.Visible = false;
                this.btnPClear.Visible = false;

                this.ddlPACampaign.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnPADone_Click(object sender, EventArgs e)
        {
            try
            {
                btnPClear_Click(null, null);
                this.divPriceAdd.Visible = false;
                this.divPriceList.Visible = true;
                this.btnPADone.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwCampaign_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow drRes = ((DataRowView)e.Row.DataItem).Row;
                    String rowColor = Convert.ToString(drRes[6]);
                    if (rowColor.Equals("Yellow"))
                    {
                        e.Row.Cells[5].BackColor = CLR.LightYellow;
                    }
                    else if (rowColor.Equals("Green"))
                    {
                        e.Row.Cells[5].BackColor = CLR.LightGreen;
                    }
                    else
                    {
                        e.Row.Cells[5].BackColor = CLR.Empty;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwPrices_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                Int32 id = -99, iResult = -99;
                id = Convert.ToInt32(gvwPrices.DataKeys[e.RowIndex].Value);
                iResult = SL.deleteSalesCampaignPrice(id);
                if (iResult.Equals(1))
                {
                    this.enDPrice.Attributes["class"] = "alert alert-success alert-dismissable";
                    this.iDPrice.Attributes["class"] = "icon fa fa-check-circle";
                    this.spnDPrice.InnerText = " Success!";
                    this.lblDeletePrice.Text = "Successfully removed Campaign Price from database";
                    this.enDPrice.Visible = true;
                    Int32 pcID = Convert.ToInt32(this.gvwPCampaign.SelectedValue);
                    System.Data.DataTable pcTable = SL.getPriceTable(pcID);
                    this.gvwPrices.DataSource = pcTable;
                    this.gvwPrices.DataBind();
                    this.gvwPrices.Focus();
                }
                else
                {
                    this.enDPrice.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iDPrice.Attributes["class"] = "icon fa fa-warning";
                    this.spnDPrice.InnerText = " Warning!";
                    this.lblDeletePrice.Text = "Unable to remove Campaign Price from database";
                    this.enDPrice.Visible = true;
                    Int32 pcID = Convert.ToInt32(this.gvwPCampaign.SelectedValue);
                    System.Data.DataTable pcTable = SL.getPriceTable(pcID);
                    this.gvwPrices.DataSource = pcTable;
                    this.gvwPrices.DataBind();
                    this.gvwPrices.Focus();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwPCampaign_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable cmpTable = SL.getCampaignTable();
                this.gvwPCampaign.PageIndex = e.NewPageIndex;
                this.gvwPCampaign.DataSource = cmpTable;
                this.gvwPCampaign.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwPCampaign_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow drRes = ((DataRowView)e.Row.DataItem).Row;
                    String rowColor = Convert.ToString(drRes[6]);
                    if (rowColor.Equals("Yellow"))
                    {
                        e.Row.Cells[5].BackColor = CLR.LightYellow;
                    }
                    else if (rowColor.Equals("Green"))
                    {
                        e.Row.Cells[5].BackColor = CLR.LightGreen;
                    }
                    else
                    {
                        e.Row.Cells[5].BackColor = CLR.Empty;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwPCampaign_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 pcID = Convert.ToInt32(this.gvwPCampaign.SelectedValue);
                System.Data.DataTable pcTable = SL.getPriceTable(pcID);
                this.gvwPrices.DataSource = pcTable;
                this.gvwPrices.PageIndex = 0;
                this.gvwPrices.DataBind();
                this.gvwPrices.Visible = true;
                this.btnPricesCancel.Visible = true;
                this.gvwPCampaign.Visible = false;
                this.gvwPrices.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}