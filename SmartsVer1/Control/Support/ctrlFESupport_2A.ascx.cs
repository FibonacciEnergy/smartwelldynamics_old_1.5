﻿using System;
using System.Web;
using System.Data;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using USR = SmartsVer1.Helpers.AccountHelpers.CreateUser;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using SVP = SmartsVer1.Helpers.SupportHelpers.SParameters;
using SPR = SmartsVer1.Helpers.SupportHelpers.SRequest;

namespace SmartsVer1.Control.Support
{
    public partial class ctrlFESupport_2A : System.Web.UI.UserControl
    {
        String LoginName, username, domain;
        Int32 ClientID;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
                //Datasource for Controls
                System.Data.DataTable stTable = SPR.getGlobalSupportTicketTable();
                this.gvwSupportTicket.DataSource = stTable;
                this.gvwSupportTicket.PageIndex = 0;
                this.gvwSupportTicket.SelectedIndex = -1;
                this.gvwSupportTicket.DataBind();
                Dictionary<Int32, String> depttList = SVP.getSupportDepartmentList();
                this.ddlSDeptt.Items.Clear();
                this.ddlSDeptt.DataSource = depttList;
                this.ddlSDeptt.DataTextField = "Value";
                this.ddlSDeptt.DataValueField = "Key";
                this.ddlSDeptt.DataBind();
                this.ddlSDeptt.Items.Insert(0, new ListItem("--- Select Support Department ---", "-1"));
                this.ddlSDeptt.SelectedIndex = -1;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAssign_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> clntList = CLNT.getGlobalClientList();
                this.ddlTicketClient.Items.Clear();
                this.ddlTicketClient.DataSource = clntList;
                this.ddlTicketClient.DataTextField = "Value";
                this.ddlTicketClient.DataValueField = "Key";
                this.ddlTicketClient.DataBind();
                this.ddlTicketClient.Items.Insert(0, new ListItem("--- Select System Client ---", "-1"));
                this.ddlTicketClient.SelectedIndex = -1;
                this.divTktList.Attributes["class"] = "panel-collapse collapse";
                this.divAssign.Attributes["class"] = "panel-collapse collapse in";
                this.divReplyTo.Attributes["class"] = "panel-collapse collapse";
                this.divTktAdd.Attributes["class"] = "panel-collapse collapse";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnReplyTo_Click(object sender, EventArgs e)
        {
            try
            {
                System.Guid UserId = USR.getUserIdFromUserName(LoginName);
                Dictionary<Int32, String> tktList = SPR.getAssignedSupportTickets(UserId);
                this.ddlReplyTicket.Items.Clear();
                this.ddlReplyTicket.DataSource = tktList;
                this.ddlReplyTicket.DataTextField = "Value";
                this.ddlReplyTicket.DataValueField = "Key";
                this.ddlReplyTicket.DataBind();
                this.ddlReplyTicket.Items.Insert(0, new ListItem("--- Select Support Ticket ---", "-1"));
                this.ddlReplyTicket.SelectedIndex = -1;
                this.divTktList.Attributes["class"] = "panel-collapse collapse";
                this.divAssign.Attributes["class"] = "panel-collapse collapse";
                this.divReplyTo.Attributes["class"] = "panel-collapse collapse in";
                this.divTktAdd.Attributes["class"] = "panel-collapse collapse";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            try
            {                
                this.divTktList.Attributes["class"] = "panel-collapse collapse";
                this.divAssign.Attributes["class"] = "panel-collapse collapse";
                this.divReplyTo.Attributes["class"] = "panel-collapse collapse";
                this.divTktAdd.Attributes["class"] = "panel-collapse collapse in";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnTopReset_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable tktList = SPR.getGlobalSupportTicketTable();
                this.gvwSupportTicket.DataSource = tktList;
                this.gvwSupportTicket.PageIndex = 0;
                this.gvwSupportTicket.SelectedIndex = -1;
                this.gvwSupportTicket.DataBind();
                this.gvwSupportTicketDetail.DataSource = null;
                this.gvwSupportTicketDetail.DataBind();
                this.gvwSupportTicketDetail.Visible = false;
                this.gvwSupportTicketReply.DataSource = null;
                this.gvwSupportTicketReply.DataBind();
                this.gvwSupportTicketReply.Visible = false;
                this.divTktList.Attributes["class"] = "panel-collapse collapse in";
                this.divAssign.Attributes["class"] = "panel-collapse collapse";
                this.divReplyTo.Attributes["class"] = "panel-collapse collapse";
                this.divTktAdd.Attributes["class"] = "panel-collapse collapse";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSupportTicket_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable stTable = SPR.getSupportTicketTable(ClientID);
                this.gvwSupportTicket.DataSource = stTable;
                this.gvwSupportTicket.PageIndex = e.NewPageIndex;
                this.gvwSupportTicket.SelectedIndex = -1;
                this.gvwSupportTicket.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSupportTicketDetail_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 tkID = Convert.ToInt32(this.gvwSupportTicket.SelectedValue);
                System.Data.DataTable detTable = SPR.getSupportTicketDetailTable(tkID);
                this.gvwSupportTicket.DataSource = detTable;
                this.gvwSupportTicket.PageIndex = e.NewPageIndex;
                this.gvwSupportTicket.SelectedIndex = -1;
                this.gvwSupportTicket.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSupportTicket_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 tkID = Convert.ToInt32(this.gvwSupportTicket.SelectedValue);
                System.Data.DataTable detTable = SPR.getSupportTicketDetailTable(tkID);
                this.gvwSupportTicketDetail.DataSource = detTable;
                this.gvwSupportTicketDetail.PageIndex = 0;
                this.gvwSupportTicketDetail.SelectedIndex = -1;
                this.gvwSupportTicketDetail.DataBind();
                this.gvwSupportTicketDetail.Visible = true;
                System.Data.DataTable repTable = SPR.getSupportTicketResponseTable(tkID);
                this.gvwSupportTicketReply.DataSource = repTable;
                this.gvwSupportTicketReply.PageIndex = 0;
                this.gvwSupportTicketReply.SelectedIndex = -1;
                this.gvwSupportTicketReply.DataBind();
                this.gvwSupportTicketReply.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlTicketClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 clntID = Convert.ToInt32(this.ddlTicketClient.SelectedValue);
                Dictionary<Int32, String> tktList = SPR.getSupportTicketListForClient(clntID);
                this.ddlTicketList.Items.Clear();
                this.ddlTicketList.DataSource = tktList;
                this.ddlTicketList.DataTextField = "Value";
                this.ddlTicketList.DataValueField = "Key";
                this.ddlTicketList.DataBind();
                this.ddlTicketList.Items.Insert(0, new ListItem("--- Select Support Ticket ---", "-1"));
                this.ddlTicketList.SelectedIndex = -1;
                this.ddlTicketList.Enabled = true;
                this.btnAsgUserReset.Enabled = true;
                this.btnAsgUserReset.CssClass = "btn btn-warning text-center";
                this.enAsg.Visible = false;
                this.lblAssign.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlTicketList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Guid, String> repList = USR.getFESupportRepresentatives();
                this.ddlSRep.Items.Clear();
                this.ddlSRep.DataSource = repList;
                this.ddlSRep.DataTextField = "Value";
                this.ddlSRep.DataValueField = "Key";
                this.ddlSRep.DataBind();
                this.ddlSRep.Items.Insert(0, new ListItem("--- Select Support Representative ---", "-1"));
                this.ddlSRep.SelectedIndex = -1;
                this.ddlSRep.Enabled = true;
                this.btnAsgUserReset.Enabled = true;
                this.btnAsgUserReset.CssClass = "btn btn-warning text-center";
                this.enAsg.Visible = false;
                this.lblAssign.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlSRep_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnAsgUser.Enabled = true;
                this.btnAsgUser.CssClass = "btn btn-success text-center";
                this.btnAsgUserReset.Enabled = true;
                this.btnAsgUserReset.CssClass = "btn btn-warning text-center";
                this.enAsg.Visible = false;
                this.lblAssign.Text = "";   
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAsgUser_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, ticketID = -99;
                System.Guid usrID = System.Guid.Empty;
                ticketID = Convert.ToInt32(this.ddlTicketList.SelectedValue);
                usrID = System.Guid.Parse(this.ddlSRep.SelectedValue);
                iResult = SPR.updateSupportTicketAssignment(ticketID, usrID, LoginName, domain);
                if (iResult.Equals(1))
                {
                    this.btnAsgUserReset_Click(null, null);
                    this.enAsg.Attributes["class"] = "alert alert-success alert-dismissable";
                    this.iAsg.Attributes["class"] = "icon fa fa-check-circle";
                    this.spnAsg.InnerText = " Success!";
                    this.lblAssign.Text = "Successfully assigned Support Ticket to User";
                    this.enAsg.Visible = true;
                }
                else
                {
                    this.btnAsgUserReset_Click(null, null);
                    this.enAsg.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iAsg.Attributes["class"] = "icon fa fa-warning";
                    this.spnAsg.InnerText = " Warning!";
                    this.lblAssign.Text = "!!! Error !!! Please try again";
                    this.enAsg.Visible = true;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAsgUserReset_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> clntList = CLNT.getGlobalClientList();
                this.ddlTicketClient.Items.Clear();
                this.ddlTicketClient.DataSource = clntList;
                this.ddlTicketClient.DataTextField = "Value";
                this.ddlTicketClient.DataValueField = "Key";
                this.ddlTicketClient.DataBind();
                this.ddlTicketClient.Items.Insert(0, new ListItem("--- Select System Client ---", "-1"));
                this.ddlTicketClient.SelectedIndex = -1;
                this.ddlTicketList.Items.Clear();
                this.ddlTicketList.DataBind();
                this.ddlTicketList.Items.Insert(0, new ListItem("--- Select Support Ticket ---", "-1"));
                this.ddlTicketList.SelectedIndex = -1;
                this.ddlTicketList.Enabled = false;
                this.ddlSRep.Items.Clear();
                this.ddlSRep.DataBind();
                this.ddlSRep.Items.Insert(0, new ListItem("--- Select Support Representative ---", "-1"));
                this.ddlSRep.SelectedIndex = -1;
                this.ddlSRep.Enabled = false;
                this.btnAsgUser.Enabled = false;
                this.btnAsgUser.CssClass = "btn btn-default text-center";
                this.btnAsgUserReset.Enabled = false;
                this.btnAsgUserReset.CssClass = "btn btn-default text-center";
                this.enAsg.Visible = false;
                this.lblAssign.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAsgUserDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnAsgUserReset_Click(null, null);
                this.btnTopReset_Click(null, null);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlReplyTicket_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtReplyTicket.Text = "";
                this.txtReplyTicket.Enabled = true;
                Dictionary<Int32, String> sttList = SVP.getSupportStatusList();
                this.ddlReplyStatus.Items.Clear();
                this.ddlReplyStatus.DataSource = sttList;
                this.ddlReplyStatus.DataTextField = "Value";
                this.ddlReplyStatus.DataValueField = "Key";
                this.ddlReplyStatus.DataBind();
                this.ddlReplyStatus.Items.Insert(0, new ListItem("--- Select Support Ticket Status ---", "-1"));
                this.ddlReplyStatus.SelectedIndex = -1;
                this.ddlReplyStatus.Enabled = true;
                this.btnReply.Enabled = false;
                this.btnReply.CssClass = "btn btn-default text-center";
                this.btnReplyReset.Enabled = true;
                this.btnReplyReset.CssClass = "btn btn-warning text-center";
                this.enReply.Visible = false;
                this.lblReply.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlReplyStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtReplyTicket.Text = "";
                this.txtReplyTicket.Enabled = true;
                this.btnReply.Enabled = true;
                this.btnReply.CssClass = "btn btn-success text-center";
                this.btnReplyReset.Enabled = true;
                this.btnReplyReset.CssClass = "btn btn-warning text-center";
                this.enReply.Visible = false;
                this.lblReply.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnReply_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, tktID = -99, sttID = -99;
                String response = String.Empty;
                tktID = Convert.ToInt32(this.ddlReplyTicket.SelectedValue);
                response = Convert.ToString(this.txtReplyTicket.Text);
                sttID = Convert.ToInt32(this.ddlReplyStatus.SelectedValue);
                if (String.IsNullOrEmpty(response))
                {                    
                        this.enReply.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iReply.Attributes["class"] = "icon fa fa-warning";
                        this.spnReply.InnerText = " Warning!";
                        this.lblReply.Text = "!!! Error !!! Null/Empty Response statement";
                        this.enReply.Visible = true;
                }
                else
                {
                    iResult = SPR.submitTicketResponse(tktID, response, sttID, LoginName);
                    if(iResult.Equals(1))
                    {
                        this.enReply.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iReply.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnReply.InnerText = " Success!";
                        this.lblReply.Text = "Response Submitted";
                        this.enReply.Visible = true;
                    }
                    else
                    {
                        this.btnReplyReset_Click(null, null);
                        this.enReply.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iReply.Attributes["class"] = "icon fa fa-warning";
                        this.spnReply.InnerText = " Warning!";
                        this.lblReply.Text = "!!! Error !!! Please try again";
                        this.enReply.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnReplyReset_Click(object sender, EventArgs e)
        {
            try
            {
                System.Guid UserId = USR.getUserIdFromUserName(LoginName);
                Dictionary<Int32, String> tktList = SPR.getAssignedSupportTickets(UserId);
                this.ddlReplyTicket.Items.Clear();
                this.ddlReplyTicket.DataTextField = "Value";
                this.ddlReplyTicket.DataValueField = "Key";
                this.ddlReplyTicket.DataBind();
                this.ddlReplyTicket.Items.Insert(0, new ListItem("--- Select Support Ticket ---", "-1"));
                this.ddlReplyTicket.SelectedIndex = -1;                
                this.txtReplyTicket.Text = "";
                this.txtReplyTicket.Enabled = false;
                this.ddlReplyStatus.Items.Clear();
                this.ddlReplyStatus.DataBind();
                this.ddlReplyStatus.Items.Insert(0, new ListItem("--- Select Support Ticket Status ---", "-1"));
                this.ddlReplyStatus.SelectedIndex = -1;
                this.ddlReplyStatus.Enabled = false;
                this.enReply.Visible = false;
                this.lblReply.Text = "";
                this.btnReply.Enabled = false;
                this.btnReply.CssClass = "btn btn-default text-center";
                this.btnReplyReset.Enabled = false;
                this.btnReplyReset.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnReplyDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnReplyReset_Click(null, null);
                this.btnTopReset_Click(null, null);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlSDeptt_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtSubject.Text = "";
                this.txtSubject.Enabled = true;
                this.txtSupportReq.Text = "";
                this.txtSupportReq.Enabled = true;
                this.enSup.Visible = false;
                this.lblSupport.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, deptID = -99;
                String subject = String.Empty, request = String.Empty, dptName = String.Empty;
                deptID = Convert.ToInt32(this.ddlSDeptt.SelectedValue);
                dptName = Convert.ToString(this.ddlSDeptt.SelectedItem);
                subject = Convert.ToString(this.txtSubject.Text);
                request = Convert.ToString(this.txtSupportReq.Text);
                if (String.IsNullOrEmpty(subject) || String.IsNullOrEmpty(request))
                {
                    this.enSup.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iSup.Attributes["class"] = "icon fa fa-warning";
                    this.spnSup.InnerText = " Missing information!";
                    this.lblSupport.Text = "!!! Error !!! Null values detected. Please provide all values.";
                    this.enSup.Visible = true;
                }
                else
                {
                    iResult = SPR.sendClientSupportRequest(LoginName, domain, ClientID, deptID, dptName, subject, request);
                    if (iResult.Equals(1))
                    {
                        this.btnSubmitClear_Click(null, null);
                        this.enSup.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iSup.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnSup.InnerText = " Success!";
                        this.lblSupport.Text = "Support Request submitted.";
                        this.enSup.Visible = true;
                        System.Data.DataTable stTable = SPR.getSupportTicketTable(ClientID);
                        this.gvwSupportTicket.DataSource = stTable;
                        this.gvwSupportTicket.PageIndex = 0;
                        this.gvwSupportTicket.SelectedIndex = -1;
                        this.gvwSupportTicket.DataBind();
                    }
                    else
                    {
                        this.enSup.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iSup.Attributes["class"] = "icon fa fa-warning";
                        this.spnSup.InnerText = " Missing information!";
                        this.lblSupport.Text = "!!! Error !!! Please try again";
                        this.enSup.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSubmitClear_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> depttList = SVP.getSupportDepartmentList();
                this.ddlSDeptt.Items.Clear();
                this.ddlSDeptt.DataSource = depttList;
                this.ddlSDeptt.DataTextField = "Value";
                this.ddlSDeptt.DataValueField = "Key";
                this.ddlSDeptt.DataBind();
                this.ddlSDeptt.Items.Insert(0, new ListItem("--- Select Support Department ---", "-1"));
                this.ddlSDeptt.SelectedIndex = -1;
                System.Data.DataTable stTable = SPR.getSupportTicketTable(ClientID);
                this.gvwSupportTicket.DataSource = stTable;
                this.gvwSupportTicket.PageIndex = 0;
                this.gvwSupportTicket.SelectedIndex = -1;
                this.gvwSupportTicket.DataBind();
                this.gvwSupportTicketDetail.DataSource = null;
                this.gvwSupportTicketDetail.PageIndex = 0;
                this.gvwSupportTicketDetail.SelectedIndex = -1;
                this.gvwSupportTicketDetail.DataBind();
                this.gvwSupportTicketDetail.Visible = false;
                this.txtSubject.Text = "";
                this.txtSubject.Enabled = false;
                this.txtSupportReq.Text = "";
                this.txtSupportReq.Enabled = false;
                this.enSup.Visible = false;
                this.lblSupport.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }                                                                
    }
}