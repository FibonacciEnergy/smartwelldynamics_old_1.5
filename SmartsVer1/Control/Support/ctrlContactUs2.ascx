﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlContactUs2.ascx.cs" Inherits="SmartsVer1.Control.Support.ctrlContactUs2" %>
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />

<style type="text/css">
    a {
    color: #ffffff;
    }

    a:hover, a:active, a:focus {
        outline: none;
        text-decoration: none;
        color: black;
    }
</style>
<!--- Page Menu --->
<div class="row bg-white" style="background-image: url('../../../Images/ContactOilRig.png'); background-repeat: no-repeat; background-color: white; background-size:100% 100%; width: 100%; height: 275px;">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding: 5px;" >
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>

    </div>
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="padding:15px">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Large" Font-Names="Segoe UI"><span class='glyphicon glyphicon-log-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none">
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='glyphicon glyphicon-log-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' />
                </li>
            </LoggedInTemplate>
        </asp:LoginView>
    </div>
</div>
<!--- Page Content --->
<div class="row" style="background-color: white;">
    <div class="row text-center" style="padding-top: 10px">
        <p class="fa-2x" style="color: Gray;">Contact us or ask questions about SMARTs</p>
    </div>
    <div class="text-center">
        <div class="col-md-offset-3">
            <div class="col-md-8">                
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <asp:TextBox ID="txtFName" runat="server" CssClass="form-control has-feedback" Text="" placeholder="First Name" />
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <asp:TextBox ID="txtLName" runat="server" CssClass="form-control has-feedback" Text="" placeholder="Last Name" />
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control has-feedback" Text="" placeholder="Email" TextMode="Email" />
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <asp:TextBox ID="txtPhone" runat="server" CssClass="form-control has-feedback" Text="" placeholder="Phone" TextMode="Phone" />
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <asp:TextBox ID="txtCompany" runat="server" CssClass="form-control has-feedback" Text="" placeholder="Company" />
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <asp:TextBox ID="txtComments" runat="server" CssClass="form-control has-feedback" Text="" placeholder="Comments ...." TextMode="MultiLine"
                        Height="350px" />
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div id="contactEnclosure" runat="server" visible="false">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        <h4><i runat="server" id="iMessage"><span runat="server" id="spnHeader"></span></i></h4>
                        <asp:Label ID="lblSent" runat="server" />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-bottom: 15px">
                    <div class="input-group">
                        <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-success" Text="Submit" OnClick="btnSubmit_Click" />
                        <asp:Button ID="btnReset" runat="server" CssClass="btn btn-warning" Text="Reset" OnClick="btnReset_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
          
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<script src="../../Scripts/Bootstrap/jQuery/jquery.slim.min.js"></script>
