﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlAnalysisCharts2.ascx.cs" Inherits="SmartsVer1.Control.Support.ctrlAnalysisCharts2" %>
<link href="../../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../../Content/Skins/css/skin-blue.css" rel="stylesheet" />
<link href="../../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />

<style type="text/css">
    a {
    color: #ffffff;
}

    a:hover, a:active, a:focus {
        outline: none;
        text-decoration: none;
        color: black;
    }
</style>


<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<div class="wrapper bg-white">
    <div id="boxMessage" class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="box-body">
                <div class="col-xs-12 col-sm-12 col-md-offset-2 col-lg-offset-2">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                            <p class="text-dark-gray fa-2x" style="text-align: center">
                                <b>SMART Data Visualisation</b> helps gain immediate knowledge of Live operations which helps in understanding
                                <b><u>Real-Time operations</u></b>, take corrective measures if required and increase operational safety
                            </p>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <ul class="text-dark-gray" style="font-size: medium">
                                <li>Enhance Decision Making - Since Data is processed in Real-Time, decisions help Live exploration operations
                                </li>
                                <li>Real-Time Data Visualization greatly improves Response-Time during Drilling and Correction / Analysis
                                </li>
                                <li>SMARTs Analysis and Visualizations helps in profiling Data Trends and Identify Errors in Real-Time
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <ul class="text-dark-gray" style="font-size: medium">
                                <li>SMARTs Visualizations can determine data patterns which can be incorporated into Business Intelligence
                                </li>
                                <li>SMARTs Visualizations are readily shareable between different teams, and devices, located in different geographic regions
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="divDisplayButtons" class="row" runat="server" style="padding-bottom: 20px; padding-top: 15px">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <asp:Button ID="btn2D" runat="server" Text="2D Representation" CssClass="btn btn-danger text-center" Font-Bold="true" Font-Size="Large"
                            Width="90%" Height="25%" OnClick="btn2D_Click" />
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <asp:Button ID="btn3D" runat="server" Text="3D Representation" CssClass="btn btn-danger text-center" Font-Bold="true" Font-Size="Large"
                            Width="90%" Height="25%" OnClick="btn3D_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row bg-white" runat="server" id="TEST">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
            </div>
            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                <div class="bg-navy disabled">
                    <div class="text-center">
                        <h1 class="box-title">SMARTs 2D Visualizations</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div id="divSampleCharts2D" runat="server" style="height: 700px; padding-left: 5px; padding-right: 5px"></div>
                                    <%--<asp:Literal ID="ltrSampleCharts2D" runat="server"></asp:Literal> --%>                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box-body">
                    <div class="row">
                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    </div>
                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                            <div class="bg-navy disabled">
                                <div class="text-center">
                                    <h1 class="box-title">SMARTs POLAR Visualizations</h1>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div id="divPolar1" runat="server" style="height: 700px; padding-left: 5px; padding-right: 5px"></div>
                                    <%--<asp:Literal ID="ltrSamplePolar1" runat="server"></asp:Literal>--%>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div id="divPolar2" runat="server" style="height: 700px; padding-left: 5px; padding-right: 5px"></div>
                                    <%--<asp:Literal ID="ltrSamplePolar2" runat="server"></asp:Literal>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box-body">
                    <div class="row">
                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    </div>
                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                            <div class="bg-navy disabled">
                                <div class="text-center">
                                    <h1 class="box-title">SMARTs Performance Quality Score Report</h1>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div id="divPerfHeatMapQS1" runat="server" style="height: 700px; padding-left: 5px; padding-right: 5px"></div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div id="divPerfHeatMapQS2" runat="server" style="height: 700px; padding-left: 5px; padding-right: 5px"></div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

                <div>
                <asp:Literal ID="ltrSampleCharts2D" runat="server"></asp:Literal>
                <asp:Literal ID="ltrSamplePolar1" runat="server"></asp:Literal> 
                <asp:Literal ID="ltrSamplePolar2" runat="server"></asp:Literal>
                <asp:Literal ID="ltrPerfHeatMapQS1" runat="server"></asp:Literal>
                <asp:Literal ID="ltrPerfHeatMapQS2" runat="server"></asp:Literal>


                    </div>
        </div>
        

    </div>


<div class="row bg-white" runat="server" id="TEST2">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
            </div>
            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                <div class="bg-navy disabled">
                    <div class="text-center">
                        <h1 class="box-title">SMARTs 3D Visualizations</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div id="divSampleCharts3D" runat="server" style="height: 780px; padding-left: 5px; padding-right: 5px"></div>
                                    <asp:Literal ID="ltrSampleCharts3D" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="../../Scripts/echarts.min.js"></script>
<script src="../../Scripts/echarts3D.min.js"></script>
<script src="../../Scripts/echarts-gl.min.js"></script>
