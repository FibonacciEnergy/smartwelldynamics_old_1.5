﻿using System;
using System.Web;
using System.Data;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using SVP = SmartsVer1.Helpers.SupportHelpers.SParameters;
using SPR = SmartsVer1.Helpers.SupportHelpers.SRequest;

namespace SmartsVer1.Control.Support
{
    public partial class ctrlFRequest_2A : System.Web.UI.UserControl
    {
        String LoginName, username, domain;
        Int32 ClientID;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);     
                //Datasource for Controls
                System.Data.DataTable stTable = SPR.getSupportTicketTable(ClientID);
                this.gvwSupportTicket.DataSource = stTable;
                this.gvwSupportTicket.PageIndex = 0;
                this.gvwSupportTicket.SelectedIndex = -1;
                this.gvwSupportTicket.DataBind();     
                Dictionary<Int32, String> depttList = SVP.getSupportDepartmentList();
                this.ddlSDeptt.Items.Clear();
                this.ddlSDeptt.DataSource = depttList;
                this.ddlSDeptt.DataTextField = "Value";
                this.ddlSDeptt.DataValueField = "Key";
                this.ddlSDeptt.DataBind();
                this.ddlSDeptt.Items.Insert(0, new ListItem("--- Select Support Department ---", "-1"));
                this.ddlSDeptt.SelectedIndex = -1;              
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {}
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSupportTicket_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable stTable = SPR.getSupportTicketTable(ClientID);
                this.gvwSupportTicket.DataSource = stTable;
                this.gvwSupportTicket.PageIndex = e.NewPageIndex;
                this.gvwSupportTicket.SelectedIndex = -1;
                this.gvwSupportTicket.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSupportTicketDetail_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 tkID = Convert.ToInt32(this.gvwSupportTicket.SelectedValue);
                System.Data.DataTable detTable = SPR.getSupportTicketDetailTable(tkID);
                this.gvwSupportTicket.DataSource = detTable;
                this.gvwSupportTicket.PageIndex = e.NewPageIndex;
                this.gvwSupportTicket.SelectedIndex = -1;
                this.gvwSupportTicket.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSupportTicket_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 tkID = Convert.ToInt32(this.gvwSupportTicket.SelectedValue);
                System.Data.DataTable detTable = SPR.getSupportTicketDetailTable(tkID);
                this.gvwSupportTicketDetail.DataSource = detTable;
                this.gvwSupportTicketDetail.PageIndex = 0;
                this.gvwSupportTicketDetail.SelectedIndex = -1;
                this.gvwSupportTicketDetail.DataBind();
                this.gvwSupportTicketDetail.Visible = true;
                System.Data.DataTable repTable = SPR.getSupportTicketResponseTable(tkID);
                this.gvwSupportTicketReply.DataSource = repTable;
                this.gvwSupportTicketReply.PageIndex = 0;
                this.gvwSupportTicketReply.SelectedIndex = -1;
                this.gvwSupportTicketReply.DataBind();
                this.gvwSupportTicketReply.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
        
        protected void ddlSDeptt_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtSubject.Text = "";
                this.txtSubject.Enabled = true;
                this.txtSupportReq.Text = "";
                this.txtSupportReq.Enabled = true;
                this.enSup.Visible = false;
                this.lblSupport.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, deptID = -99;
                String subject = String.Empty, request = String.Empty, dptName = String.Empty;
                deptID = Convert.ToInt32(this.ddlSDeptt.SelectedValue);
                dptName = Convert.ToString(this.ddlSDeptt.SelectedItem);
                subject = Convert.ToString(this.txtSubject.Text);
                request = Convert.ToString(this.txtSupportReq.Text);
                if (String.IsNullOrEmpty(subject) || String.IsNullOrEmpty(request))
                {
                    this.enSup.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iSup.Attributes["class"] = "icon fa fa-warning";
                    this.spnSup.InnerText = " Missing information!";
                    this.lblSupport.Text = "!!! Error !!! Null values detected. Please provide all values.";
                    this.enSup.Visible = true;
                }
                else
                {
                    iResult = SPR.sendClientSupportRequest(LoginName, domain, ClientID, deptID, dptName, subject, request);
                    if (iResult.Equals(1))
                    {
                        this.btnSubmitClear_Click(null, null);
                        this.enSup.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iSup.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnSup.InnerText = " Success!";
                        this.lblSupport.Text = "Support Request submitted.";
                        this.enSup.Visible = true;
                        System.Data.DataTable stTable = SPR.getSupportTicketTable(ClientID);
                        this.gvwSupportTicket.DataSource = stTable;
                        this.gvwSupportTicket.PageIndex = 0;
                        this.gvwSupportTicket.SelectedIndex = -1;
                        this.gvwSupportTicket.DataBind();     
                    }
                    else
                    {
                        this.enSup.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iSup.Attributes["class"] = "icon fa fa-warning";
                        this.spnSup.InnerText = " Missing information!";
                        this.lblSupport.Text = "!!! Error !!! Please try again";
                        this.enSup.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSubmitClear_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> depttList = SVP.getSupportDepartmentList();
                this.ddlSDeptt.Items.Clear();
                this.ddlSDeptt.DataSource = depttList;
                this.ddlSDeptt.DataTextField = "Value";
                this.ddlSDeptt.DataValueField = "Key";
                this.ddlSDeptt.DataBind();
                this.ddlSDeptt.Items.Insert(0, new ListItem("--- Select Support Department ---", "-1"));
                this.ddlSDeptt.SelectedIndex = -1;
                System.Data.DataTable stTable = SPR.getSupportTicketTable(ClientID);
                this.gvwSupportTicket.DataSource = stTable;
                this.gvwSupportTicket.PageIndex = 0;
                this.gvwSupportTicket.SelectedIndex = -1;
                this.gvwSupportTicket.DataBind();
                this.gvwSupportTicketDetail.DataSource = null;
                this.gvwSupportTicketDetail.PageIndex = 0;
                this.gvwSupportTicketDetail.SelectedIndex = -1;
                this.gvwSupportTicketDetail.DataBind();
                this.gvwSupportTicketDetail.Visible = false;
                this.gvwSupportTicketReply.DataSource = null;
                this.gvwSupportTicketReply.DataBind();
                this.gvwSupportTicketReply.Visible = false;
                this.txtSubject.Text = "";
                this.txtSubject.Enabled = false;
                this.txtSupportReq.Text = "";
                this.txtSupportReq.Enabled = false;
                this.enSup.Visible = false;
                this.lblSupport.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }                
    }
}