﻿using System;
using System.Web.UI;
using log4net;
using System.Web.UI.WebControls;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using SM = SmartsVer1.Helpers.SupportHelpers.SRequest;

namespace SmartsVer1.Control.Support
{
    public partial class ctrlDemo_2A : System.Web.UI.UserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ctrlDemo_2A));

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //this.pageMenuSource.SiteMapProvider = MNU.getStandardMenu();
                // Menu Loading
                this.ltrMenuLiteral.Text = MNL.getDefaultMenu();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRequest_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    Int32 iReply = -99;
                    String fName = this.txtFN.Text;
                    String LName = this.txtLN.Text;
                    String title = this.txtTitle.Text;
                    String cName = this.txtCompany.Text;
                    String wPhon = this.txtWPh.Text;
                    String Email = this.txtWEml.Text;
                    Int32 sMd = Convert.ToInt32(this.ddlPreferred.SelectedValue);

                    if (String.IsNullOrEmpty(fName) || String.IsNullOrEmpty(LName) || String.IsNullOrEmpty(title) || String.IsNullOrEmpty(wPhon) || String.IsNullOrEmpty(Email) || String.IsNullOrEmpty(cName))
                    {
                        this.divAlertEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iMessage.Attributes["class"] = "icon fa fa-warning";
                        this.spnHeader.InnerText = " Missing information!";
                        this.lblSent.Text = "Please provide all values before sending a Message.";
                        this.divAlertEnclosure.Visible = true;
                    }
                    else
                    {
                        iReply = Convert.ToInt32(SM.sendDemoRequest(fName, LName, title, cName, wPhon, Email, sMd));
                        logger.Error(" Request For Demo. Sent by: " + fName + " " + LName + ". Company: " + cName + ". Email: " + Email + ". Phone: " + wPhon );
                        if (iReply.Equals(1))
                        {
                            this.divAlertEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                            this.iMessage.Attributes["class"] = "icon fa fa-success";
                            this.spnHeader.InnerText = " Success!";
                            this.lblSent.Text = "Successfully sent your message. You will hear from us shortly. Thank you for your message/comments.";
                            this.divAlertEnclosure.Visible = true;
                            this.txtFN.Text = String.Empty;
                            this.txtLN.Text = String.Empty;
                            this.txtTitle.Text = String.Empty;
                            this.txtCompany.Text = String.Empty;
                            this.txtWPh.Text = String.Empty;
                            this.txtWEml.Text = String.Empty;
                            this.ddlPreferred.Items.Clear();
                            this.ddlPreferred.DataBind();
                            this.ddlPreferred.Items.Insert(0, new ListItem("--- Select Preferred Contact Method ---", "-1"));
                            this.ddlPreferred.Items.Insert(1, new ListItem("Email", "1"));
                            this.ddlPreferred.Items.Insert(2, new ListItem("Phone", "2"));
                            this.ddlPreferred.SelectedIndex = -1;
                        }
                        else
                        {
                            this.divAlertEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                            this.iMessage.Attributes["class"] = "icon fa fa-danger";
                            this.spnHeader.InnerText = " !!! Error !!!";
                            this.lblSent.Text = "Unable to send message at this time. Please try again. If problem persists please send an email with your message to info@fibonaccienergy.com";
                            this.divAlertEnclosure.Visible = true;
                            this.txtFN.Text = String.Empty;
                            this.txtLN.Text = String.Empty;
                            this.txtTitle.Text = String.Empty;
                            this.txtCompany.Text = String.Empty;
                            this.txtWPh.Text = String.Empty;
                            this.txtWEml.Text = String.Empty;
                            this.ddlPreferred.Items.Clear();
                            this.ddlPreferred.DataBind();
                            this.ddlPreferred.Items.Insert(0, new ListItem("--- Select Preferred Contact Method ---", "-1"));
                            this.ddlPreferred.Items.Insert(1, new ListItem("Email", "1"));
                            this.ddlPreferred.Items.Insert(2, new ListItem("Phone", "2"));
                            this.ddlPreferred.SelectedIndex = -1;
                        }
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}