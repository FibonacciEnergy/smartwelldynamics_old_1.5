﻿using System;
using System.Web.UI;
using log4net;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using SM = SmartsVer1.Helpers.SupportHelpers.SRequest;

namespace SmartsVer1.Control.Support
{
    public partial class ctrlContactUs2 : System.Web.UI.UserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ctrlContactUs2));

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //this.pageMenuSource.SiteMapProvider = MNU.getStandardMenu();
                // Menu Loading
                this.ltrMenuLiteral.Text = MNL.getDefaultMenu();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    Int32 iReply = -99;
                    String fName = Convert.ToString(this.txtFName.Text);
                    String LName = Convert.ToString(this.txtLName.Text);
                    String cName = Convert.ToString(this.txtCompany.Text);
                    String wPhon = Convert.ToString(this.txtPhone.Text);
                    String Email = Convert.ToString(this.txtEmail.Text);
                    String Comm = Convert.ToString(this.txtComments.Text);

                    if (String.IsNullOrEmpty(fName) || String.IsNullOrEmpty(LName) || String.IsNullOrEmpty(wPhon) || String.IsNullOrEmpty(Email) || String.IsNullOrEmpty(Comm))
                    {
                        this.contactEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iMessage.Attributes["class"] = "icon fa fa-warning";
                        this.spnHeader.InnerText = " Missing information!";
                        this.lblSent.Text = "Please provide all values before sending a Message.";
                        this.contactEnclosure.Visible = true;
                    }
                    else
                    {
                        iReply = Convert.ToInt32(SM.sendContactMessage(fName, LName, cName, wPhon, Email, Comm));
                        logger.Error(" Request For Contact.  Sent by: " + fName + " " + LName + ". Company:  " + cName + ". Email: " + Email + ". Phone: " + wPhon + ". Comments: " + Comm);
                        if (iReply.Equals(1))
                        {
                            this.contactEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                            this.iMessage.Attributes["class"] = "icon fa fa-check-circle";
                            this.spnHeader.InnerText = " Success!";
                            this.lblSent.Text = "Successfully sent your message. You will hear from us shortly. Thank you for your message/comments.";
                            this.contactEnclosure.Visible = true;
                        }
                        else
                        {
                            this.contactEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                            this.iMessage.Attributes["class"] = "icon fa fa-danger";
                            this.spnHeader.InnerText = " !!! Error !!!";
                            this.lblSent.Text = "Unable to send message at this time. Please try again. If problem persists please send an email with your message to info@fibonaccienergy.com";
                            this.contactEnclosure.Visible = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtFName.Text = null;
                this.txtLName.Text = null;
                this.txtPhone.Text = null;
                this.txtEmail.Text = null;
                this.txtComments.Text = null;
                this.lblSent.Text = null;
                this.contactEnclosure.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}