﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlTerms.ascx.cs" Inherits="SmartsVer1.Control.Support.ctrlTerms" %>

<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!--- Page local style --->
<style type="text/css">    
    .swdMenu {
        color: #ffffff;
    }    
</style>
<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">        
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../SWDHome.aspx"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="../../Support/Terms_2A.aspx"> Terms of Service</a></li>
            </ol>
        </div>
    </div>    
</div>
<!-- Page Content -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-info">
                <div class="box panel-body">
                    This Privacy Policy tells you how we use personal information collected at this 
                                Site. Please read this privacy policy before using the Site or submitting any 
                                personal information. By using the Site, you are accepting the practices 
                                described in this privacy policy. You are encouraged to review the privacy 
                                policy whenever you visit the Site to make sure that you understand how any 
                                personal information you provided will be treated.<br />
                    <br />
                    Note: Our Site may contain links to other websites of interest. However, once 
                                you have used these links to leave our Site, you should note that we do not have 
                                any control over that other website. Therefore, we cannot be responsible for the 
                                protection and privacy of any information which you provide whilst visiting such 
                                sites and such sites are not governed by this privacy statement. You should 
                                exercise caution and look at the privacy statement applicable to the website in 
                                question.<br />

                    <h5>Collection of Information</h5>
                    We collect personally identifiable information, like names, postal addresses, 
                                email addresses, telephone numbers, IP Addresses etc. The information you 
                                provide is used to fulfill your specific requests. This information is only used 
                                to fulfill you specific request, unless you give us permission to use it in 
                                another manner, for example to add you to one of our mailing lists. We also use 
                                this information for internal record keeping, use this information to improve 
                                our products and services.<br />
                    From time to time, we may also use your information to contact 
                                you for market research purposes. We may contact you by email, phone, fax or 
                                mail. We may use the information to customize the website according to your 
                                interests/preferences.<br />
                    <br />
                    <h5>Cookies/Tracking Technologies</h5>
                    The Site may use cookie and tracking technology depending on the features offered. Cookie and tracking technology is 
                                useful for gathering information such as browser type and operating system, 
                                tracking the number of visitors to the Site, and understanding how visitors use 
                                the Site. Cookies can also help customize the Site for visitors. Personal 
                                information cannot be collected via cookies and other tracking technology, 
                                however, if you previously provided identifiable information, cookies may be 
                                tied to such information. Overall, cookies help us provide you with a 
                                better Site, by enabling us to monitor which pages you find useful and which you 
                                do not. A cookie in no way gives us access to your computer.<br />
                    <br />
                    <h5>Distribution of Information</h5>
                    We may share information with governmental 
                                agencies or other companies assisting us in fraud prevention or investigation. 
                                We may do so when: (1) permitted or required by law; or, (2) trying to protect 
                                against or prevent actual or potential fraud or unauthorized transactions; or, 
                                (3) investigating fraud which has already taken place. The information is not 
                                provided to these companies for marketing purposes.<br />

                    <h5>Commitment to Data Security</h5>
                    Your personally identifiable information is 
                                kept secure. Only authorized employees, agents and contractors (who have agreed 
                                to keep information secure and confidential) have access to this information.
                                <br />
                    <br />
                    <h5>Privacy Contact Information</h5>
                    If you have any questions, concerns or 
                                comments about our privacy policy you may contact us using the information 
                                below:<br />
                    <br />
                    By email: <a href="mailto:info@smartwelldynamics.com">info@smartwelldynamics.com</a><br />
                    <br />
                    We reserve the right to make changes to this privacy policy. Any changes to this 
                                policy will be posted.
                </div>
            </div>
        </div>
    </div>
</div>

<!-- JavaScripts -->
<%--<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>--%>