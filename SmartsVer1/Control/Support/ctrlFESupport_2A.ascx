﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlFESupport_2A.ascx.cs" Inherits="SmartsVer1.Control.Support.ctrlFESupport_2A" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<!-- Stylesheet -->
<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!--- Page local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }
    .rotate{
		-webkit-transform: rotate(90deg);  /* Chrome, Safari, Opera */
			-moz-transform: rotate(90deg);  /* Firefox */
			-ms-transform: rotate(90deg);  /* IE 9 */
				transform: rotate(90deg);  /* Standard syntax */    
    }
    .swdMenu {
        color: #ffffff;
    }
</style>

<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!-- Breadcrumb -->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>Support</li>
                <li><a href="../../Support/FRequest_2A.aspx">Support Request</a></li>
            </ol>            
        </div>
    </div>
</div>
<!-- Page Content -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div id="acrdSupportOuter" class="panel-group" runat="server">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancSPMain" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFESupport_2A_acrdSupportOuter" href="#BodyContent_ctrlFESupport_2A_divSPOuter">
                                SMARTs Support Request</a>
                        </h5>
                    </div>
                    <div id="divSPOuter" runat="server" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="upgrsMeta" runat="server" AssociatedUpdatePanelID="upnlMeta" DisplayAfter="0">
                                <ProgressTemplate>
                                    <div id="divProgSurround" runat="server" class="updateProgress">
                                        <asp:Image ID="imgJob1Processing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel runat="server" ID="upnlMeta">
                                <ContentTemplate>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div id="acrdSupport" class="panel-group">
                                            <div class="panel panel-info">
                                                <div class="panel-body">
                                                    <div id="divJGages" runat="server" class="panel-collapse collapse in">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="input-group">
                                                <asp:LinkButton ID="btnAssign" runat="server" CssClass="btn btn-info text-center" OnClick="btnAssign_Click"><i class="fa fa-plus"> Assign Ticket</i></asp:LinkButton>
                                                <asp:LinkButton ID="btnReplyTo" runat="server" CssClass="btn btn-info text-center" OnClick="btnReplyTo_Click"><i class="fa fa-reply"> Reply To Ticket</i></asp:LinkButton>
                                                <asp:LinkButton ID="btnCreate" runat="server" CssClass="btn btn-info text-center" OnClick="btnCreate_Click"><i class="fa fa-plus"> Create Ticket</i></asp:LinkButton>
                                                <asp:LinkButton ID="btnTopReset" runat="server" CssClass="btn btn-warning text-center" OnClick="btnTopReset_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                            </div>
                                            <div class="panel panel-info">
                                                <div class="panel-heading">
                                                    <h5 class="panel-title" style="text-align: left">
                                                        <a id="ancSTList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFESupport_2A_acrdSupport" href="#BodyContent_ctrlFESupport_2A_divTktList">
                                                            Support Tickets</a>
                                                    </h5>
                                                </div>
                                                <div id="divTktList" runat="server" class="panel-collapse collapse in">
                                                    <div class="panel-body">                                                                                                                
                                                        <asp:GridView ID="gvwSupportTicket" runat="server" DataKeyNames="stktID" AllowPaging="True" AutoGenerateColumns="false"
                                                            CssClass="mydatagrid" OnPageIndexChanging="gvwSupportTicket_PageIndexChanging" EmptyDataText="No Support Tickets found" ShowHeaderWhenEmpty="true"  
                                                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                            RowStyle-CssClass="rows" OnSelectedIndexChanged="gvwSupportTicket_SelectedIndexChanged">
                                                            <Columns>
                                                                <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                    <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                </asp:ButtonField>
                                                                <asp:BoundField DataField="stktID" HeaderText="stktID" Visible="false" />
                                                                <asp:BoundField DataField="stktNumber" HeaderText="Ticket #" ItemStyle-HorizontalAlign="Right"></asp:BoundField>                                                            
                                                                <asp:BoundField DataField="sdptID" HeaderText="Department" />
                                                                <asp:BoundField DataField="stktSubject" HeaderText="Subject" />
                                                                <asp:BoundField DataField="sstID" HeaderText="Status" />
                                                                <asp:BoundField DataField="stktAssigned" HeaderText="Assigned To" />
                                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                            </Columns>
                                                        </asp:GridView>
                                                        <asp:GridView ID="gvwSupportTicketDetail" runat="server" DataKeyNames="stktID" AllowPaging="True" AutoGenerateColumns="false"
                                                            CssClass="mydatagrid" OnPageIndexChanging="gvwSupportTicketDetail_PageIndexChanging" EmptyDataText="No Support Tickets found" ShowHeaderWhenEmpty="true"  
                                                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                            RowStyle-CssClass="rows" Visible="false">
                                                            <Columns>
                                                                <asp:BoundField DataField="stktID" HeaderText="stktID" Visible="false" />
                                                                <asp:BoundField DataField="stktRequest" />
                                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                            </Columns>
                                                        </asp:GridView>
                                                        <asp:GridView ID="gvwSupportTicketReply" runat="server" DataKeyNames="strpID" AllowPaging="True" AutoGenerateColumns="false"
                                                            CssClass="mydatagrid" OnPageIndexChanging="gvwSupportTicketDetail_PageIndexChanging" EmptyDataText="No Support Tickets found" ShowHeaderWhenEmpty="true"  
                                                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                            RowStyle-CssClass="rows" Visible="false">
                                                            <Columns>
                                                                <asp:BoundField DataField="strpID" HeaderText="strpID" Visible="false" />
                                                                <asp:BoundField DataField="strpText" />
                                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-info">
                                                <div class="panel-heading">
                                                    <h5 class="panel-title" style="text-align: left">
                                                        <a id="ancAssign" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFESupport_2A_acrdSupport" href="#BodyContent_ctrlFESupport_2A_divAssign">
                                                            Assign Support Ticket</a>
                                                    </h5>
                                                </div>
                                                <div id="divAssign" runat="server" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue">Client</span>
                                                                <asp:DropDownList ID="ddlTicketClient" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                    OnSelectedIndexChanged="ddlTicketClient_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0" Text="--- Select System Client ---" />
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue">Ticket</span>
                                                                <asp:DropDownList ID="ddlTicketList" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true" Enabled="false"
                                                                    OnSelectedIndexChanged="ddlTicketList_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0" Text="--- Select Support Ticket ---" />
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue">Representative</span>
                                                                <asp:DropDownList ID="ddlSRep" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true" Enabled="false"
                                                                    OnSelectedIndexChanged="ddlSRep_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0" Text="--- Select Support Representative ---" />
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div runat="server" id="enAsg" visible="false">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                <h4><i runat="server" id="iAsg"><span runat="server" id="spnAsg"></span></i></h4>
                                                                <asp:Label ID="lblAssign" runat="server" />
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="input-group">
                                                                <asp:LinkButton ID="btnAsgUser" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnAsgUser_Click"><i class="fa fa-plus"> Assign User</i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnAsgUserReset" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnAsgUserReset_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnAsgUserDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnAsgUserDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-info">
                                                <div class="panel-heading">
                                                    <h5 class="panel-title" style="text-align: left">
                                                        <a id="ancReplyTo" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFESupport_2A_acrdSupport" href="#BodyContent_ctrlFESupport_2A_divReplyTo">
                                                            Reply To Support Ticket</a>
                                                    </h5>
                                                </div>
                                                <div id="divReplyTo" runat="server" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue">Ticket</span>
                                                                <asp:DropDownList ID="ddlReplyTicket" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true" 
                                                                    OnSelectedIndexChanged="ddlReplyTicket_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0" Text="--- Select Support Ticket ---" />
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <asp:TextBox ID="txtReplyTicket" runat="server" CssClass="form-control" Enabled="false" Text="" placeholder="Please type in your response here" TextMode="MultiLine" Height="250px"></asp:TextBox>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue">Status</span>
                                                                <asp:DropDownList ID="ddlReplyStatus" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true" Enabled="false" 
                                                                    OnSelectedIndexChanged="ddlReplyStatus_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0" Text="--- Select Support Ticket Status ---" />
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div runat="server" id="enReply" visible="false">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                <h4><i runat="server" id="iReply"><span runat="server" id="spnReply"></span></i></h4>
                                                                <asp:Label ID="lblReply" runat="server" />
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="input-group">
                                                                <asp:LinkButton ID="btnReply" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnReply_Click"><i class="fa fa-plus"> Submit Response</i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnReplyReset" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnReplyReset_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnReplyDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnReplyDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-info">
                                                <div class="panel-heading">
                                                    <h5 class="panel-title" style="text-align: left">
                                                        <a id="ancSTAdd" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFESupport_2A_acrdSupport" href="#BodyContent_ctrlFESupport_2A_divTktAdd">
                                                            New Support Ticket</a>
                                                    </h5>
                                                </div>
                                                <div id="divTktAdd" runat="server" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue">Department</span>
                                                                <asp:DropDownList ID="ddlSDeptt" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                    OnSelectedIndexChanged="ddlSDeptt_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0" Text="--- Select Support Department ---" />
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue">Subject</span>
                                                                <asp:TextBox ID="txtSubject" runat="server" CssClass="form-control" Text="" Enabled="false" TextMode="SingleLine" placeholder="Support Email Subject" />
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <asp:TextBox ID="txtSupportReq" runat="server" CssClass="form-control" Text="" Enabled="false" Height="350px" TextMode="Multiline" placeholder="Support Request Description" />
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div runat="server" id="enSup" visible="false">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                <h4><i runat="server" id="iSup"><span runat="server" id="spnSup"></span></i></h4>
                                                                <asp:Label ID="lblSupport" runat="server" />
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="input-group">
                                                                <asp:LinkButton ID="btnSubmit" runat="server" CssClass="btn btn-success text-center" Text="Submit" OnClick="btnSubmit_Click" />
                                                                <asp:LinkButton ID="btnSubmitClear" runat="server" CssClass="btn btn-warning text-center" Text="Reset" OnClick="btnSubmitClear_Click" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Script -->
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/jquery.slimscroll.min.js"></script>