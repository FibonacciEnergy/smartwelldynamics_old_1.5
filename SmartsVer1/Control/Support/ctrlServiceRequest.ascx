﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlServiceRequest.ascx.cs" Inherits="SmartsVer1.Control.Support.ctrlServiceRequest" %>


<!--- Stylesheets --->
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<!--- Script Libraries --->
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<script src="../../Scripts/jquery.slimscroll.min.js"></script>
<!--- Local Style --->
<style type="text/css">
    a {
    color: #ffffff;
}

    a:hover, a:active, a:focus {
        outline: none;
        text-decoration: none;
        color: white;
    }
</style>

<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>

<div class="row bg-white" runat="server" id="TEST">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                <div class="bg-navy disabled">
                    <div class="text-center" >
                        <h1 class="box-title">Welcome to SMARTs Service Request From</h1>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>


<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div id="boxMessage" class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <div class="box box-default">
                    <div class="box-body" style="height:700px">
                        <div class="row" style="padding-top:20px">
                            <div class="col-md-2 col-lg-2"></div>
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon bg-light-blue" style="width: 20%;">First Name</span>
                                    <asp:TextBox ID="txtFN" runat="server" CssClass="form-control" placeholder="First Name" />
                                    <span class="input-group-addon"><i class="fa fa-user" style="width:20px"></i></span>
                                </div>
                            </div>
                            <div class="col-md-2 col-lg-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 col-lg-2"></div>
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon bg-light-blue" style="width: 20%;">Last Name</span>
                                    <asp:TextBox ID="txtLN" runat="server" CssClass="form-control" placeholder="Last Name" />
                                    <span class="input-group-addon"><i class="fa fa-user" style="width:20px"></i></span>
                                </div>
                            </div>
                            <div class="col-md-2 col-lg-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 col-lg-2"></div>
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon bg-light-blue" style="width: 20%;">Title/Role</span>
                                    <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control" placeholder="Title/Role" />
                                    <span class="input-group-addon"><i class="fa fa-users" style="width:20px"></i></span>
                                </div>
                            </div>
                            <div class="col-md-2 col-lg-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 col-lg-2"></div>
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon bg-light-blue" style="width: 20%;">Company</span>
                                    <asp:TextBox ID="txtCompany" runat="server" CssClass="form-control" placeholder="Company" />
                                    <span class="input-group-addon"><i class="fa fa-institution" style="width:20px"></i></span>
                                </div>
                            </div>
                            <div class="col-md-2 col-lg-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 col-lg-2"></div>
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon bg-light-blue" style="width: 20%;">Email</span>
                                    <asp:TextBox ID="txtWEml" runat="server" CssClass="form-control" TextMode="Email" placeholder="Email" />
                                    <span class="input-group-addon"><i class=" fa fa-envelope" style="width:20px"></i></span>
                                </div>
                            </div>
                            <div class="col-md-2 col-lg-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 col-lg-2"></div>
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon bg-light-blue" style="width: 20%;">Phone</span>
                                    <asp:TextBox ID="txtWPh" runat="server" CssClass="form-control" TextMode="Phone" placeholder="Phone" />
                                    <span class="input-group-addon"><i class="fa fa-phone" style="width:20px"></i></span>
                                </div>
                            </div>
                            <div class="col-md-2 col-lg-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 col-lg-2"></div>
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon bg-light-blue" style="width: 20%;">Operator Company Name</span>
                                    <asp:TextBox ID="txtOperator" runat="server" CssClass="form-control" placeholder="Operator Company" />
                                    <span class="input-group-addon"><i class="fa fa-institution" style="width: 20px"></i></span>
                                </div>
                            </div>
                            <div class="col-md-2 col-lg-2"></div>
                        </div>
<%--                        <div class="row">
                            <div class="col-md-2 col-lg-2"></div>
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon bg-light-blue" style="width: 20%;">Service Request Type</span>
                                    <asp:DropDownList ID="ddlPreferred" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true">
                                        <asp:ListItem Value="0" Text="--- Select Service Type ---" />
                                        <asp:ListItem Value="1" Text="SMARTs Raw Data Audit/QC" />
                                        <asp:ListItem Value="2" Text="SMARTs Historical Data Audit/QC" />
                                        <asp:ListItem Value="3" Text="SMARTs 3rd Party Data Audit/QC" />
                                    </asp:DropDownList>
                                    <span class="input-group-addon"><i class="fa fa-check-circle" style="width:20px"></i></span>
                                </div>
                            </div>
                            <div class="col-md-2 col-lg-2"></div>
                        </div>--%>
                        <br />
                        <div class="row">
                            <div class="col-md-2 col-lg-2"></div>
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                <%--<div class="input-group">--%>
                                    <span class="input-group-addon bg-light-blue" style="height:40px;font-size:medium;" >Additional Comments</span>
                                    <asp:TextBox ID="txtComments" runat="server" CssClass="form-control has-feedback" Font-Bold="true" Text="" placeholder="Additional Comments ...." TextMode="MultiLine"
                                        Height="150px" />
                               <%-- </div>--%>
                            </div>
                            <div class="col-md-2 col-lg-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-sm-12 col-md-4 col-lg-4"></div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="text-center">
                                    <div runat="server" id="divAlertEnclosure" visible="false">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <h4><i runat="server" id="iMessage"><span runat="server" id="spnHeader"></span></i></h4>
                                        <asp:Label ID="lblSent" runat="server" CssClass="lblContact" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-sm-12 col-md-4 col-lg-4"></div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-sm-12 col-sm-12 col-md-4 col-lg-4"></div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="text-center " >                                    
                                    <asp:Button runat="server" Font-Bold="true" Font-Size="Medium" ID="btnRequest" class="btn btn-primary text-center" Width="350px" Text="Request Service" OnClick="btnRequest_Click" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-sm-12 col-md-4 col-lg-4"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

