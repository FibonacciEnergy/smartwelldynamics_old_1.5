﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlDemo_2A.ascx.cs" Inherits="SmartsVer1.Control.Support.ctrlDemo_2A" %>

<!--- Stylesheets --->
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<!--- Script Libraries --->
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<script src="../../Scripts/jquery.slimscroll.min.js"></script>
<!--- Local Style --->
<style type="text/css">
    a {
    color: #ffffff;
}

    a:hover, a:active, a:focus {
        outline: none;
        text-decoration: none;
        color: white;
    }
</style>
<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding: 5px;">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="padding:15px">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Large" Font-Names="Segoe UI"><span class='glyphicon glyphicon-log-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none">
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='glyphicon glyphicon-log-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' />
                </li>
            </LoggedInTemplate>
        </asp:LoginView>
    </div>
</div>
<!--- Page Content --->
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div id="boxMessage" class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <div class="box box-default">
                    <div class="box-body" style="height:500px">
                        <div class="row" style="padding-top:20px">
                            <div class="col-md-2 col-lg-2"></div>
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon bg-light-blue" style="width: 20%;">First Name</span>
                                    <asp:TextBox ID="txtFN" runat="server" CssClass="form-control" placeholder="First Name" />
                                    <span class="input-group-addon"><i class="fa fa-user" style="width:20px"></i></span>
                                </div>
                            </div>
                            <div class="col-md-2 col-lg-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 col-lg-2"></div>
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon bg-light-blue" style="width: 20%;">Last Name</span>
                                    <asp:TextBox ID="txtLN" runat="server" CssClass="form-control" placeholder="Last Name" />
                                    <span class="input-group-addon"><i class="fa fa-user" style="width:20px"></i></span>
                                </div>
                            </div>
                            <div class="col-md-2 col-lg-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 col-lg-2"></div>
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon bg-light-blue" style="width: 20%;">Title/Role</span>
                                    <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control" placeholder="Title/Role" />
                                    <span class="input-group-addon"><i class="fa fa-users" style="width:20px"></i></span>
                                </div>
                            </div>
                            <div class="col-md-2 col-lg-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 col-lg-2"></div>
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon bg-light-blue" style="width: 20%;">Company</span>
                                    <asp:TextBox ID="txtCompany" runat="server" CssClass="form-control" placeholder="Company" />
                                    <span class="input-group-addon"><i class="fa fa-institution" style="width:20px"></i></span>
                                </div>
                            </div>
                            <div class="col-md-2 col-lg-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 col-lg-2"></div>
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon bg-light-blue" style="width: 20%;">Email</span>
                                    <asp:TextBox ID="txtWEml" runat="server" CssClass="form-control" TextMode="Email" placeholder="Email" />
                                    <span class="input-group-addon"><i class=" fa fa-envelope" style="width:20px"></i></span>
                                </div>
                            </div>
                            <div class="col-md-2 col-lg-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 col-lg-2"></div>
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon bg-light-blue" style="width: 20%;">Phone</span>
                                    <asp:TextBox ID="txtWPh" runat="server" CssClass="form-control" TextMode="Phone" placeholder="Phone" />
                                    <span class="input-group-addon"><i class="fa fa-phone" style="width:20px"></i></span>
                                </div>
                            </div>
                            <div class="col-md-2 col-lg-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 col-lg-2"></div>
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon bg-light-blue" style="width: 20%;">Preferred</span>
                                    <asp:DropDownList ID="ddlPreferred" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true">
                                        <asp:ListItem Value="0" Text="--- Select Preferred Contact Method ---" />
                                        <asp:ListItem Value="1" Text="Email" />
                                        <asp:ListItem Value="2" Text="Phone" />
                                    </asp:DropDownList>
                                    <span class="input-group-addon"><i class="fa fa-check-circle" style="width:20px"></i></span>
                                </div>
                            </div>
                            <div class="col-md-2 col-lg-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-sm-12 col-md-4 col-lg-4"></div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="text-center">
                                    <div runat="server" id="divAlertEnclosure" visible="false">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <h4><i runat="server" id="iMessage"><span runat="server" id="spnHeader"></span></i></h4>
                                        <asp:Label ID="lblSent" runat="server" CssClass="lblContact" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-sm-12 col-md-4 col-lg-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-sm-12 col-md-4 col-lg-4"></div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="text-center">
                                    <asp:Button runat="server" ID="btnRequest" class="btn btn-primary text-center" Text="Request Demo" OnClick="btnRequest_Click" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-sm-12 col-md-4 col-lg-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="box-footer">
                                    <div class="input-group">
                                        <div class="text-center">
                                            <span style="color: red;"><i class="fa fa-shield"></i></span>
                                            <u>
                                                <asp:Label ID="lblNotice" runat="server" CssClass="form-text text-muted" Text=" Your information remains secure and unshared" /></u>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
