﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using SM = SmartsVer1.Helpers.SupportHelpers.SRequest;

namespace SmartsVer1.Control.Support
{
    public partial class ctrlServiceRequest : System.Web.UI.UserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ctrlServiceRequest));

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //this.pageMenuSource.SiteMapProvider = MNU.getStandardMenu();
                // Menu Loading
                this.ltrMenuLiteral.Text = MNL.getDefaultMenu();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRequest_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    Int32 iReply = -99;
                    String fName = this.txtFN.Text;
                    String LName = this.txtLN.Text;
                    String title = this.txtTitle.Text;
                    String cName = this.txtCompany.Text;
                    String wPhon = this.txtWPh.Text;
                    String Email = this.txtWEml.Text;
                    String Operator = this.txtOperator.Text;
                    String Comments = this.txtComments.Text;

                    if (String.IsNullOrEmpty(fName) || String.IsNullOrEmpty(LName) || String.IsNullOrEmpty(title) || String.IsNullOrEmpty(wPhon) || String.IsNullOrEmpty(Email) || String.IsNullOrEmpty(cName)) 
                    {
                        this.divAlertEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iMessage.Attributes["class"] = "icon fa fa-warning";
                        this.spnHeader.InnerText = " Missing information!";
                        this.lblSent.Text = "Please provide all values before sending a Message.";
                        this.divAlertEnclosure.Visible = true;
                    }
                    else
                    {
                        iReply = Convert.ToInt32(SM.sendServiceRequest(fName, LName, title, cName, wPhon, Email , Operator , Comments));
                        logger.Error(" Request For Service Request. Sent by: " + fName + " " + LName + ". Company  " + cName + ". Email: " + Email + ". Phone: " + wPhon + ". Comments: " + Comments);

                        if (iReply.Equals(1))
                        {
                            this.divAlertEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                            this.iMessage.Attributes["class"] = "icon fa fa-success";
                            this.spnHeader.InnerText = " Success!";
                            this.lblSent.Text = "Successfully sent your message. You will hear from us shortly. Thank you for your message/comments.";
                            this.divAlertEnclosure.Visible = true;
                            this.txtFN.Text = String.Empty;
                            this.txtLN.Text = String.Empty;
                            this.txtTitle.Text = String.Empty;
                            this.txtCompany.Text = String.Empty;
                            this.txtWPh.Text = String.Empty;
                            this.txtWEml.Text = String.Empty;
                            this.txtOperator.Text = String.Empty;
                            this.txtComments.Text = String.Empty;

                        }
                        else
                        {
                            this.divAlertEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                            this.iMessage.Attributes["class"] = "icon fa fa-danger";
                            this.spnHeader.InnerText = " !!! Error !!!";
                            this.lblSent.Text = "Unable to send message at this time. Please try again. If problem persists please send an email with your message to info@fibonaccienergy.com";
                            this.divAlertEnclosure.Visible = true;
                            this.txtFN.Text = String.Empty;
                            this.txtLN.Text = String.Empty;
                            this.txtTitle.Text = String.Empty;
                            this.txtCompany.Text = String.Empty;
                            this.txtWPh.Text = String.Empty;
                            this.txtWEml.Text = String.Empty;
                            this.txtOperator.Text = String.Empty;
                            this.txtComments.Text = String.Empty;

                        }
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}