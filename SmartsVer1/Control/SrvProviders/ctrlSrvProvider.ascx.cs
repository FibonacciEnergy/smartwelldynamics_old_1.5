﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI.WebControls;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;

namespace SmartsVer1.Control.SrvProviders
{
    public partial class ctrlSrvProvider : System.Web.UI.UserControl
    {
        String LoginName, username, domain, GetClientDBString;
        const Int32 ServiceClientType = 1, siloWSt = 2, globeWSt = 1;
        Int32 ClientID = -99, ClientType = 4, qcjCount = 0, crjCount = 0, wpjCount = 0, ifrCount = 0, ttljCount = 0;
        Int32 gwCount = 0, awCount = 0, opCount = 0, ascCount = 0, msngCount = 0;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
                //Datasources for page controls
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientType);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = 0;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
                String ClientName = CLNT.getClientName(ClientID);
                this.ancSProvider.InnerHtml = String.Format("<span class='glyphicon glyphicon-menu-right'></span> SMARTs Service Providers for <span style='text-decoration: underline; font-weight: bold'>{0}</span>", ClientName);
                System.Data.DataTable spList = CLNT.getOperatorCoServiceProviderTableForReviewer(ClientID);
                this.gvwServiceProviderList.DataSource = spList;
                this.gvwServiceProviderList.SelectedIndex = -1;
                this.gvwServiceProviderList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Int32 padCount = -99, welCount = -99, spCount = -99, opCount = -99, spadCount = -99, swelCount = -99, sopCount = -99, sspCount = -99;
                List<Int32> assetCounts = CLNT.getReviewerAssetGlobalCounts();
                opCount = assetCounts[0];
                spCount = assetCounts[1];
                padCount = assetCounts[2];
                welCount = assetCounts[3];
                this.divOperators.InnerHtml = "<h3>" + opCount.ToString() + "</h3><span>Global Operator Companies</span>";
                this.divProviders.InnerHtml = "<h3>" + spCount.ToString() + "</h3><span>Service Provider Companies</span>";
                this.divPads.InnerHtml = "<h3>" + padCount.ToString() + "</h3><span>Total Global Well Pads</span>";
                this.divWells.InnerHtml = "<h3>" + spCount.ToString() + "</h3><span>Total Global Well(s)/Lateral(s)</span>";
                //List<Int32> siloAssetsCounts = CLNT.getReviewerSiloAssetCounts(ClientID);
                //sopCount = assetCounts[0];
                //sspCount = assetCounts[1];
                //spadCount = assetCounts[2];
                //swelCount = assetCounts[3];
                //this.divSiloOperators.InnerHtml = "<h3>" + sopCount.ToString() + "</h3><span>Global Operator Companies</span>";
                //this.divSiloProviders.InnerHtml = "<h3>" + sspCount.ToString() + "</h3><span>Service Provider Companies</span>";
                //this.divSiloPads.InnerHtml = "<h3>" + spadCount.ToString() + "</h3><span>Total Well Pads</span>";
                //this.divSiloWells.InnerHtml = "<h3>" + sspCount.ToString() + "</h3><span>Total Well(s)/Lateral(s)</span>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwGlobalOptr_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientType);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = e.NewPageIndex;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
                this.btnGlobalClear.Enabled = true;
                this.btnGlobalClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwGlobalOptr_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 clntID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                GridViewRow dRow = this.gvwGlobalOptr.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.divOP.Attributes["class"] = "panel-collapse collapse";
                this.divMRList.Attributes["class"] = "panel-collapse collapse in";
                this.divWellList.Attributes["class"] = "panel-collapse collapse";
                this.ancOP.InnerHtml = "Well Operator Company :<span style='text-decoration: underline; font-weight: bold'>" + oName + "</span>";
                Int32 OperatorID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                System.Data.DataTable regTable = DMG.getRegionsWithCountTableForReviewers(OperatorID);
                this.gvwWPadMRList.DataSource = regTable;
                this.gvwWPadMRList.SelectedIndex = -1;
                this.gvwWPadMRList.DataBind();
                this.gvwWell.DataSource = null;
                this.gvwWell.SelectedIndex = -1;
                this.gvwWell.DataBind();
                this.btnGlobalClear.Enabled = true;
                this.btnGlobalClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWPadMRList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 OperatorID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                System.Data.DataTable regTable = DMG.getRegionsWithCountTableForReviewers(OperatorID);
                this.gvwWPadMRList.DataSource = regTable;
                this.gvwWPadMRList.PageIndex = e.NewPageIndex;
                this.gvwWPadMRList.SelectedIndex = -1;
                this.gvwWPadMRList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWPadMRList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 rgID = Convert.ToInt32(this.gvwWPadMRList.SelectedValue);
                System.Data.DataTable sregList = DMG.getSubRegionTableForOperatorBySubRegionForReviewer(rgID, oprID);
                this.gvwSRList.DataSource = sregList;
                this.gvwSRList.SelectedIndex = -1;
                this.gvwSRList.DataBind();
                GridViewRow dRow = this.gvwWPadMRList.SelectedRow;
                String mrName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.divOP.Attributes["class"] = "panel-collapse collapse";
                this.divMRList.Attributes["class"] = "panel-collapse collapse";
                this.divSRList.Attributes["class"] = "panel-collapse collapse in";
                this.ancWellReg.InnerHtml = "Major Geographic Region: <span style='text-decoration: underline; font-weight: bold'>" + mrName + "</span>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSRList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 rgID = Convert.ToInt32(this.gvwWPadMRList.SelectedValue);
                System.Data.DataTable sregList = DMG.getSubRegionTableForOperatorBySubRegionForReviewer(rgID, oprID);
                this.gvwSRList.DataSource = sregList;
                this.gvwSRList.PageIndex = e.NewPageIndex;
                this.gvwSRList.SelectedIndex = -1;
                this.gvwSRList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSRList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 srID = Convert.ToInt32(this.gvwSRList.SelectedValue);
                System.Data.DataTable ctyList = DMG.getCountryTableForOperatorBySubRegionForReviewer(srID, oprID);
                this.gvwCountryList.DataSource = ctyList;
                this.gvwCountryList.SelectedIndex = -1;
                this.gvwCountryList.DataBind();
                this.divOP.Attributes["class"] = "panel-collapse collapse";
                this.divMRList.Attributes["class"] = "panel-collapse collapse";
                this.divSRList.Attributes["class"] = "panel-collapse collapse";
                this.divCountryList.Attributes["class"] = "panel-collapse collapse in";
                GridViewRow dRow = this.gvwSRList.SelectedRow;
                String srName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.ancWellSReg.InnerHtml = "Sub-Region: <span style='text-decoration: underline; font-weight: bold'>" + srName + "</span>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwCountryList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 srID = Convert.ToInt32(this.gvwSRList.SelectedValue);
                System.Data.DataTable ctyList = DMG.getCountryTableForOperatorBySubRegionForReviewer(srID, oprID);
                this.gvwCountryList.DataSource = ctyList;
                this.gvwCountryList.PageIndex = e.NewPageIndex;
                this.gvwCountryList.SelectedIndex = -1;
                this.gvwCountryList.DataBind();
                this.divMRList.Attributes["class"] = "panel-collapse collapse";
                this.divSRList.Attributes["class"] = "panel-collapse collapse";
                this.divCountryList.Attributes["class"] = "panel-collapse collapse in";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwCountryList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 ctyID = Convert.ToInt32(this.gvwCountryList.SelectedValue);
                System.Data.DataTable stTable = DMG.getStateTableForOperatorByCountryForReviewer(ctyID, oprID);
                this.gvwStateList.DataSource = stTable;
                this.gvwStateList.SelectedIndex = -1;
                this.gvwStateList.DataBind();
                GridViewRow dRow = this.gvwCountryList.SelectedRow;
                String ctyName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.ancWellCountry.InnerHtml = "Country / Nation: <span style='text-decoration: underline; font-weight: bold'>" + ctyName + "</span>";
                this.divOP.Attributes["class"] = "panel-collapse collapse";
                this.divMRList.Attributes["class"] = "panel-collapse collapse";
                this.divSRList.Attributes["class"] = "panel-collapse collapse";
                this.divCountryList.Attributes["class"] = "panel-collapse collapse";
                this.divStateList.Attributes["class"] = "panel-collapse collapse in";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwStateList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 ctyID = Convert.ToInt32(this.gvwCountryList.SelectedValue);
                System.Data.DataTable stTable = DMG.getStateTableForOperatorByCountryForReviewer(ctyID, oprID);
                this.gvwStateList.DataSource = stTable;
                this.gvwStateList.PageIndex = e.NewPageIndex;
                this.gvwStateList.SelectedIndex = -1;
                this.gvwStateList.DataBind();
                GridViewRow dRow = this.gvwCountryList.SelectedRow;
                String ctyName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.ancWellCountry.InnerHtml = "Country / Nation: <span style='text-decoration: underline; font-weight: bold'>" + ctyName + "</span>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwStateList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 stID = Convert.ToInt32(this.gvwStateList.SelectedValue);
                System.Data.DataTable cnyList = DMG.getCountyTableForOperatorByStateForReviewer(stID, oprID);
                this.gvwCountyList.DataSource = cnyList;
                this.gvwCountyList.SelectedIndex = -1;
                this.gvwCountyList.DataBind();
                this.divOP.Attributes["class"] = "panel-collapse collapse";
                this.divMRList.Attributes["class"] = "panel-collapse collapse";
                this.divSRList.Attributes["class"] = "panel-collapse collapse";
                this.divCountryList.Attributes["class"] = "panel-collapse collapse";
                this.divStateList.Attributes["class"] = "panel-collapse collapse";
                this.divCountyList.Attributes["class"] = "panel-collapse collapse in";
                GridViewRow dRow = this.gvwStateList.SelectedRow;
                String stName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.ancWellState.InnerHtml = "State / Province: <span style='text-decoration: underline; font-weight: bold'>" + stName + "</span>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwCountyList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 stID = Convert.ToInt32(this.gvwStateList.SelectedValue);
                System.Data.DataTable cnyList = DMG.getCountyTableForOperatorByStateForReviewer(stID, oprID);
                this.gvwCountyList.DataSource = cnyList;
                this.gvwCountryList.PageIndex = e.NewPageIndex;
                this.gvwCountyList.SelectedIndex = -1;
                this.gvwCountyList.DataBind();
                this.divOP.Attributes["class"] = "panel-collapse collapse";
                this.divMRList.Attributes["class"] = "panel-collapse collapse";
                this.divSRList.Attributes["class"] = "panel-collapse collapse";
                this.divCountryList.Attributes["class"] = "panel-collapse collapse";
                this.divStateList.Attributes["class"] = "panel-collapse collapse";
                this.divCountyList.Attributes["class"] = "panel-collapse collapse in";
                GridViewRow dRow = this.gvwStateList.SelectedRow;
                String stName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.ancWellState.InnerHtml = "State / Province: <span style='text-decoration: underline; font-weight: bold'>" + stName + "</span>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwCountyList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 cnID = Convert.ToInt32(this.gvwCountyList.SelectedValue);
                System.Data.DataTable fldList = DMG.getFieldTableForOperatorByCountyForReviewer(cnID, oprID);
                this.gvwFldList.DataSource = fldList;
                this.gvwFldList.SelectedIndex = -1;
                this.gvwFldList.DataBind();
                this.divOP.Attributes["class"] = "panel-collapse collapse";
                this.divMRList.Attributes["class"] = "panel-collapse collapse";
                this.divSRList.Attributes["class"] = "panel-collapse collapse";
                this.divCountryList.Attributes["class"] = "panel-collapse collapse";
                this.divStateList.Attributes["class"] = "panel-collapse collapse";
                this.divCountyList.Attributes["class"] = "panel-collapse collapse";
                this.divWellField.Attributes["class"] = "panel-collapse collapse in";
                GridViewRow dRow = this.gvwCountyList.SelectedRow;
                String cnName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.ancWellCounty.InnerHtml = "County / District / Division / Municipality: <span style='text-decoration: underline; font-weight: bold'>" + cnName + "</span>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwFldList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 cnID = Convert.ToInt32(this.gvwCountyList.SelectedValue);
                System.Data.DataTable fldList = DMG.getFieldTableForOperatorByCountyForReviewer(cnID, oprID);
                this.gvwFldList.DataSource = fldList;
                this.gvwFldList.PageIndex = e.NewPageIndex;
                this.gvwFldList.SelectedIndex = -1;
                this.gvwFldList.DataBind();
                this.divOP.Attributes["class"] = "panel-collapse collapse";
                this.divMRList.Attributes["class"] = "panel-collapse collapse";
                this.divSRList.Attributes["class"] = "panel-collapse collapse";
                this.divCountryList.Attributes["class"] = "panel-collapse collapse";
                this.divStateList.Attributes["class"] = "panel-collapse collapse";
                this.divCountyList.Attributes["class"] = "panel-collapse collapse";
                this.divWellField.Attributes["class"] = "panel-collapse collapse in";
                GridViewRow dRow = this.gvwCountyList.SelectedRow;
                String cnName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.ancWellCounty.InnerHtml = "County / District / Division / Municipality: <span style='text-decoration: underline; font-weight: bold'>" + cnName + "</span>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwFldList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {                
                Int32 opID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 fldID = Convert.ToInt32(this.gvwFldList.SelectedValue);
                System.Data.DataTable wpdTable = AST.getWellPadTableForOperatorForReviewer(opID, fldID);
                this.gvwWellPads.DataSource = wpdTable;
                this.gvwWellPads.SelectedIndex = -1;
                this.gvwWellPads.DataBind();
                this.divOP.Attributes["class"] = "panel-collapse collapse";
                this.divMRList.Attributes["class"] = "panel-collapse collapse";
                this.divSRList.Attributes["class"] = "panel-collapse collapse";
                this.divCountryList.Attributes["class"] = "panel-collapse collapse";
                this.divStateList.Attributes["class"] = "panel-collapse collapse";
                this.divCountyList.Attributes["class"] = "panel-collapse collapse";
                this.divWellField.Attributes["class"] = "panel-collapse collapse";
                this.divWellPadList.Attributes["class"] = "panel-collapse collapse in";
                GridViewRow dRow = this.gvwFldList.SelectedRow;
                String fdName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.ancWellField.InnerHtml = "O&G Fields: <span style='text-decoration: underline; font-weight: bold'>" + fdName + "</span>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellPads_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 opID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 fldID = Convert.ToInt32(this.gvwFldList.SelectedValue);
                System.Data.DataTable wpdTable = AST.getWellPadTableForOperatorForReviewer(opID, fldID);
                this.gvwWellPads.DataSource = wpdTable;
                this.gvwWellPads.PageIndex = e.NewPageIndex;
                this.gvwWellPads.SelectedIndex = -1;
                this.gvwWellPads.DataBind();
                this.divOP.Attributes["class"] = "panel-collapse collapse";
                this.divMRList.Attributes["class"] = "panel-collapse collapse";
                this.divSRList.Attributes["class"] = "panel-collapse collapse";
                this.divCountryList.Attributes["class"] = "panel-collapse collapse";
                this.divStateList.Attributes["class"] = "panel-collapse collapse";
                this.divCountyList.Attributes["class"] = "panel-collapse collapse";
                this.divWellField.Attributes["class"] = "panel-collapse collapse";
                this.divWellPadList.Attributes["class"] = "panel-collapse collapse in";
                GridViewRow dRow = this.gvwFldList.SelectedRow;
                String fdName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.ancWellField.InnerHtml = "O&G Fields: <span style='text-decoration: underline; font-weight: bold'>" + fdName + "</span>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellPads_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 pdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                System.Data.DataTable pdTable = DMG.getWellForWellPadTableForReviewer(pdID);
                this.gvwWell.DataSource = pdTable;
                this.gvwWell.SelectedIndex = -1;
                this.gvwWell.DataBind();
                this.divOP.Attributes["class"] = "panel-collapse collapse";
                this.divMRList.Attributes["class"] = "panel-collapse collapse";
                this.divSRList.Attributes["class"] = "panel-collapse collapse";
                this.divCountryList.Attributes["class"] = "panel-collapse collapse";
                this.divStateList.Attributes["class"] = "panel-collapse collapse";
                this.divCountyList.Attributes["class"] = "panel-collapse collapse";
                this.divWellField.Attributes["class"] = "panel-collapse collapse";
                this.divWellPadList.Attributes["class"] = "panel-collapse collapse";
                this.divWellList.Attributes["class"] = "panel-collapse collapse in";
                GridViewRow dRow = this.gvwWellPads.SelectedRow;
                String pdName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.ancWellPadList.InnerHtml = "Well Pad: <span style='text-decoration: underline; font-weight: bold'>" + pdName + "</span>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWell_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                System.Data.DataTable wlTable = DMG.getWellForWellPadTableForReviewer(oprID);
                this.gvwWell.PageIndex = e.NewPageIndex;
                this.gvwWell.DataSource = wlTable;
                this.gvwWell.DataBind();
                this.gvwWell.SelectedIndex = -1;
                this.gvwWellDetail.DataBind();
                this.gvwWellDetail.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWell_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 wID = Convert.ToInt32(this.gvwWell.SelectedValue);
                System.Data.DataTable detailTable = DMG.getWellDetailTableForReviewer(wID);
                this.gvwWellDetail.DataSource = detailTable;
                this.gvwWellDetail.DataBind();
                this.gvwWellDetail.Visible = true;                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnGlobalClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.divOP.Attributes["class"] = "panel-collapse collapse in";
                this.divMRList.Attributes["class"] = "panel-collapse collapse";
                this.divSRList.Attributes["class"] = "panel-collapse collapse";
                this.divCountryList.Attributes["class"] = "panel-collapse collapse";
                this.divStateList.Attributes["class"] = "panel-collapse collapse";
                this.divCountyList.Attributes["class"] = "panel-collapse collapse";
                this.divWellField.Attributes["class"] = "panel-collapse collapse";
                this.divWellPadList.Attributes["class"] = "panel-collapse collapse";
                this.divWellList.Attributes["class"] = "panel-collapse collapse";
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientType);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = 0;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
                this.ancOP.InnerHtml = "Global Operator Companies";
                this.ancWellReg.InnerHtml = "Major Geographic Region";
                this.ancWellSReg.InnerHtml = "Sub-Region";
                this.ancWellCountry.InnerHtml = "Country / Nation";
                this.ancWellState.InnerHtml = "State / Province";
                this.ancWellCounty.InnerHtml = "County / District / Division / Municipality";
                this.ancWellField.InnerHtml = "O&G Fields";
                this.ancWellPadList.InnerHtml = "Well Pad";
                this.ancWellList.InnerHtml = "Well(s) / Lateral(s)";
                this.btnGlobalClear.Enabled = false;
                this.btnGlobalClear.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwServiceProviderList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable spList = CLNT.getOperatorCoServiceProviderTable(ClientID);
                this.gvwServiceProviderList.DataSource = spList;
                this.gvwServiceProviderList.PageIndex = e.NewPageIndex;
                this.gvwServiceProviderList.SelectedIndex = -1;
                this.gvwServiceProviderList.DataBind();
                this.btnSrvPClear.Enabled = true;
                this.btnSrvPClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwServiceProviderList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 clntID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                GridViewRow dRow = this.gvwServiceProviderList.SelectedRow;
                String cName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.divSrvPList.Attributes["class"] = "panel-collapse collapse";
                this.divSPReg.Attributes["class"] = "panel-collapse collapse in";
                this.divSPSReg.Attributes["class"] = "panel-collapse collapse";
                this.divSPCountry.Attributes["class"] = "panel-collapse collapse";
                this.divSPState.Attributes["class"] = "panel-collapse collapse";
                this.divSPCounty.Attributes["class"] = "panel-collapse collapse";
                this.divSPField.Attributes["class"] = "panel-collapse collapse";
                this.divSPPad.Attributes["class"] = "panel-collapse collapse";
                this.divSPWell.Attributes["class"] = "panel-collapse collapse";
                this.ancOptrList.InnerHtml = "Service Provider(s) :<span style='text-decoration: underline; font-weight: bold'>" + cName + "</span>";
                Int32 ProviderID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                System.Data.DataTable regTable = DMG.getProviderRegionsWithCountTableForReviewers(ProviderID);
                this.gvwSPReg.DataSource = regTable;
                this.gvwSPReg.PageIndex = 0;
                this.gvwSPReg.SelectedIndex = -1;
                this.gvwSPReg.DataBind();
                this.btnSrvPClear.Enabled = true;
                this.btnSrvPClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSPReg_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 ProviderID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                System.Data.DataTable regTable = DMG.getProviderRegionsWithCountTableForReviewers(ProviderID);
                this.gvwSPReg.DataSource = regTable;
                this.gvwSPReg.PageIndex = e.NewPageIndex;
                this.gvwSPReg.SelectedIndex = -1;
                this.gvwSPReg.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSPReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 clntID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                GridViewRow dRow = this.gvwSPReg.SelectedRow;
                String rName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.divSrvPList.Attributes["class"] = "panel-collapse collapse";
                this.divSPReg.Attributes["class"] = "panel-collapse collapse";
                this.divSPSReg.Attributes["class"] = "panel-collapse collapse in";
                this.divSPCountry.Attributes["class"] = "panel-collapse collapse";
                this.divSPState.Attributes["class"] = "panel-collapse collapse";
                this.divSPCounty.Attributes["class"] = "panel-collapse collapse";
                this.divSPField.Attributes["class"] = "panel-collapse collapse";
                this.divSPPad.Attributes["class"] = "panel-collapse collapse";
                this.divSPWell.Attributes["class"] = "panel-collapse collapse";
                this.ancSPReg.InnerHtml = "Major Geographic Region :<span style='text-decoration: underline; font-weight: bold'>" + rName + "</span>";
                Int32 ProviderID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                Int32 RegionID = Convert.ToInt32(this.gvwSPReg.SelectedValue);
                System.Data.DataTable regTable = DMG.getProviderSubRegionTableForOperatorBySubRegionForReviewer(RegionID, ProviderID);
                this.gvwSPSReg.DataSource = regTable;
                this.gvwSPSReg.PageIndex = 0;
                this.gvwSPSReg.SelectedIndex = -1;
                this.gvwSPSReg.DataBind();
                this.btnSrvPClear.Enabled = true;
                this.btnSrvPClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSPSReg_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 ProviderID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                Int32 RegionID = Convert.ToInt32(this.gvwSPReg.SelectedValue);
                System.Data.DataTable regTable = DMG.getProviderSubRegionTableForOperatorBySubRegionForReviewer(RegionID, ProviderID);
                this.gvwSPSReg.DataSource = regTable;
                this.gvwSPSReg.PageIndex = e.NewPageIndex;
                this.gvwSPSReg.SelectedIndex = -1;
                this.gvwSPSReg.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSPSReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 clntID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                GridViewRow dRow = this.gvwSPSReg.SelectedRow;
                String srName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.divSrvPList.Attributes["class"] = "panel-collapse collapse";
                this.divSPReg.Attributes["class"] = "panel-collapse collapse";
                this.divSPSReg.Attributes["class"] = "panel-collapse collapse";
                this.divSPCountry.Attributes["class"] = "panel-collapse collapse in";
                this.divSPState.Attributes["class"] = "panel-collapse collapse";
                this.divSPCounty.Attributes["class"] = "panel-collapse collapse";
                this.divSPField.Attributes["class"] = "panel-collapse collapse";
                this.divSPPad.Attributes["class"] = "panel-collapse collapse";
                this.divSPWell.Attributes["class"] = "panel-collapse collapse";
                this.ancSPSReg.InnerHtml = "Geographic Sub-Region :<span style='text-decoration: underline; font-weight: bold'>" + srName + "</span>";
                Int32 ProviderID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                Int32 SRegionID = Convert.ToInt32(this.gvwSPSReg.SelectedValue);
                System.Data.DataTable regTable = DMG.getProviderCountryTableForOperatorBySubRegionForReviewer(SRegionID, ProviderID);
                this.gvwSPCountry.DataSource = regTable;
                this.gvwSPCountry.PageIndex = 0;
                this.gvwSPCountry.SelectedIndex = -1;
                this.gvwSPCountry.DataBind();
                this.btnSrvPClear.Enabled = true;
                this.btnSrvPClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSPCountry_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 ProviderID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                Int32 SRegionID = Convert.ToInt32(this.gvwSPSReg.SelectedValue);
                System.Data.DataTable regTable = DMG.getProviderCountryTableForOperatorBySubRegionForReviewer(SRegionID, ProviderID);
                this.gvwSPCountry.DataSource = regTable;
                this.gvwSPCountry.PageIndex = e.NewPageIndex;
                this.gvwSPCountry.SelectedIndex = -1;
                this.gvwSPCountry.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSPCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 clntID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                GridViewRow dRow = this.gvwSPCountry.SelectedRow;
                String ctyName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.divSrvPList.Attributes["class"] = "panel-collapse collapse";
                this.divSPReg.Attributes["class"] = "panel-collapse collapse";
                this.divSPSReg.Attributes["class"] = "panel-collapse collapse";
                this.divSPCountry.Attributes["class"] = "panel-collapse collapse";
                this.divSPState.Attributes["class"] = "panel-collapse collapse in";
                this.divSPCounty.Attributes["class"] = "panel-collapse collapse";
                this.divSPField.Attributes["class"] = "panel-collapse collapse";
                this.divSPPad.Attributes["class"] = "panel-collapse collapse";
                this.divSPWell.Attributes["class"] = "panel-collapse collapse";
                this.ancSPCountry.InnerHtml = "Country / Nation :<span style='text-decoration: underline; font-weight: bold'>" + ctyName + "</span>";
                Int32 ProviderID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                Int32 CountryID = Convert.ToInt32(this.gvwSPCountry.SelectedValue);
                System.Data.DataTable regTable = DMG.getProviderStateTableForOperatorByCountryForReviewer(CountryID, ProviderID);
                this.gvwSPState.DataSource = regTable;
                this.gvwSPState.PageIndex = 0;
                this.gvwSPState.SelectedIndex = -1;
                this.gvwSPState.DataBind();
                this.btnSrvPClear.Enabled = true;
                this.btnSrvPClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSPState_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 ProviderID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                Int32 CountryID = Convert.ToInt32(this.gvwSPCountry.SelectedValue);
                System.Data.DataTable regTable = DMG.getProviderStateTableForOperatorByCountryForReviewer(CountryID, ProviderID);
                this.gvwSPState.DataSource = regTable;
                this.gvwSPState.PageIndex = e.NewPageIndex;
                this.gvwSPState.SelectedIndex = -1;
                this.gvwSPState.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSPState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 clntID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                GridViewRow dRow = this.gvwSPState.SelectedRow;
                String stName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.divSrvPList.Attributes["class"] = "panel-collapse collapse";
                this.divSPReg.Attributes["class"] = "panel-collapse collapse";
                this.divSPSReg.Attributes["class"] = "panel-collapse collapse";
                this.divSPCountry.Attributes["class"] = "panel-collapse collapse";
                this.divSPState.Attributes["class"] = "panel-collapse collapse";
                this.divSPCounty.Attributes["class"] = "panel-collapse collapse in";
                this.divSPField.Attributes["class"] = "panel-collapse collapse";
                this.divSPPad.Attributes["class"] = "panel-collapse collapse";
                this.divSPWell.Attributes["class"] = "panel-collapse collapse";
                this.ancSPState.InnerHtml = "State / Province :<span style='text-decoration: underline; font-weight: bold'>" + stName + "</span>";
                Int32 ProviderID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                Int32 StateID = Convert.ToInt32(this.gvwSPState.SelectedValue);
                System.Data.DataTable cntyTable = DMG.getProviderCountyTableForOperatorByStateForReviewer(StateID, ProviderID);
                this.gvwSPCounty.DataSource = cntyTable;
                this.gvwSPCounty.PageIndex = 0;
                this.gvwSPCounty.SelectedIndex = -1;
                this.gvwSPCounty.DataBind();
                this.btnSrvPClear.Enabled = true;
                this.btnSrvPClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSPCounty_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 ProviderID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                Int32 StateID = Convert.ToInt32(this.gvwSPState.SelectedValue);
                System.Data.DataTable cntyTable = DMG.getProviderCountyTableForOperatorByStateForReviewer(StateID, ProviderID);
                this.gvwSPCounty.DataSource = cntyTable;
                this.gvwSPCounty.PageIndex = e.NewPageIndex;
                this.gvwSPCounty.SelectedIndex = -1;
                this.gvwSPCounty.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSPCounty_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow dRow = this.gvwSPCounty.SelectedRow;
                String cntyName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.divSrvPList.Attributes["class"] = "panel-collapse collapse";
                this.divSPReg.Attributes["class"] = "panel-collapse collapse";
                this.divSPSReg.Attributes["class"] = "panel-collapse collapse";
                this.divSPCountry.Attributes["class"] = "panel-collapse collapse";
                this.divSPState.Attributes["class"] = "panel-collapse collapse";
                this.divSPCounty.Attributes["class"] = "panel-collapse collapse";
                this.divSPField.Attributes["class"] = "panel-collapse collapse in";
                this.divSPPad.Attributes["class"] = "panel-collapse collapse";
                this.divSPWell.Attributes["class"] = "panel-collapse collapse";
                this.ancSPCounty.InnerHtml = "County / District / Division / Municipality :<span style='text-decoration: underline; font-weight: bold'>" + cntyName + "</span>";
                Int32 ProviderID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                Int32 CountyID = Convert.ToInt32(this.gvwSPCounty.SelectedValue);
                System.Data.DataTable cntyTable = DMG.getProviderFieldTableForOperatorByCountyForReviewer(CountyID, ProviderID);
                this.gvwSPField.DataSource = cntyTable;
                this.gvwSPField.PageIndex = 0;
                this.gvwSPField.SelectedIndex = -1;
                this.gvwSPField.DataBind();
                this.btnSrvPClear.Enabled = true;
                this.btnSrvPClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSPField_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 ProviderID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                Int32 CountyID = Convert.ToInt32(this.gvwSPCounty.SelectedValue);
                System.Data.DataTable cntyTable = DMG.getProviderFieldTableForOperatorByCountyForReviewer(CountyID, ProviderID);
                this.gvwSPField.DataSource = cntyTable;
                this.gvwSPField.PageIndex = e.NewPageIndex;
                this.gvwSPField.SelectedIndex = -1;
                this.gvwSPField.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSPField_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow dRow = this.gvwSPField.SelectedRow;
                String fldName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.divSrvPList.Attributes["class"] = "panel-collapse collapse";
                this.divSPReg.Attributes["class"] = "panel-collapse collapse";
                this.divSPSReg.Attributes["class"] = "panel-collapse collapse";
                this.divSPCountry.Attributes["class"] = "panel-collapse collapse";
                this.divSPState.Attributes["class"] = "panel-collapse collapse";
                this.divSPCounty.Attributes["class"] = "panel-collapse collapse";
                this.divSPField.Attributes["class"] = "panel-collapse collapse";
                this.divSPPad.Attributes["class"] = "panel-collapse collapse in";
                this.divSPWell.Attributes["class"] = "panel-collapse collapse";
                this.ancSPField.InnerHtml = "O&G Fields :<span style='text-decoration: underline; font-weight: bold'>" + fldName + "</span>";
                Int32 ProviderID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                Int32 FieldID = Convert.ToInt32(this.gvwSPField.SelectedValue);
                System.Data.DataTable cntyTable = AST.getProviderWellPadTableForOperatorForReviewer(ProviderID, FieldID);
                this.gvwSPPad.DataSource = cntyTable;
                this.gvwSPPad.PageIndex = 0;
                this.gvwSPPad.SelectedIndex = -1;
                this.gvwSPPad.DataBind();
                this.btnSrvPClear.Enabled = true;
                this.btnSrvPClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSPPad_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 ProviderID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                Int32 FieldID = Convert.ToInt32(this.gvwSPField.SelectedValue);
                System.Data.DataTable cntyTable = AST.getProviderWellPadTableForOperatorForReviewer(ProviderID, FieldID);
                this.gvwSPPad.DataSource = cntyTable;
                this.gvwSPPad.PageIndex = e.NewPageIndex;
                this.gvwSPPad.SelectedIndex = -1;
                this.gvwSPPad.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSPPad_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow dRow = this.gvwSPPad.SelectedRow;
                String pdName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.divSrvPList.Attributes["class"] = "panel-collapse collapse";
                this.divSPReg.Attributes["class"] = "panel-collapse collapse";
                this.divSPSReg.Attributes["class"] = "panel-collapse collapse";
                this.divSPCountry.Attributes["class"] = "panel-collapse collapse";
                this.divSPState.Attributes["class"] = "panel-collapse collapse";
                this.divSPCounty.Attributes["class"] = "panel-collapse collapse";
                this.divSPField.Attributes["class"] = "panel-collapse collapse";
                this.divSPPad.Attributes["class"] = "panel-collapse collapse";
                this.divSPWell.Attributes["class"] = "panel-collapse collapse in";
                this.ancSPPad.InnerHtml = "Well Pad :<span style='text-decoration: underline; font-weight: bold'>" + pdName + "</span>";
                Int32 ProviderID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                Int32 PadID = Convert.ToInt32(this.gvwSPPad.SelectedValue);
                System.Data.DataTable wellTable = AST.getProviderWellForWellPadTable(PadID, ProviderID);
                this.gvwSPWell.DataSource = wellTable;
                this.gvwSPWell.PageIndex = 0;
                this.gvwSPWell.SelectedIndex = -1;
                this.gvwSPWell.DataBind();
                this.btnSrvPClear.Enabled = true;
                this.btnSrvPClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSPWell_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 ProviderID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                Int32 PadID = Convert.ToInt32(this.gvwSPPad.SelectedValue);
                System.Data.DataTable wellTable = AST.getProviderWellForWellPadTable(PadID, ProviderID);
                this.gvwSPWell.DataSource = wellTable;
                this.gvwSPWell.PageIndex = e.NewPageIndex;
                this.gvwSPWell.SelectedIndex = -1;
                this.gvwSPWell.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSPWell_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ProviderID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                Int32 WellID = Convert.ToInt32(this.gvwSPWell.SelectedValue);
                System.Data.DataTable detTable = AST.getProviderWellDetailTableForReviewer(WellID, ProviderID);
                this.gvwSPWellDetail.DataSource = detTable;
                this.gvwSPWellDetail.PageIndex = 0;
                this.gvwSPWellDetail.SelectedIndex = -1;
                this.gvwSPWellDetail.DataBind();
                this.gvwSPWellDetail.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSrvPClear_Click(object sender, EventArgs e)
        {
            try
            {
                String ClientName = CLNT.getClientName(ClientID);
                this.ancSProvider.InnerHtml = String.Format("<span class='glyphicon glyphicon-menu-right'></span> SMARTs Service Providers for <span style='text-decoration: underline; font-weight: bold'>{0}</span>", ClientName);
                this.ancOptrList.InnerHtml = "Service Provider(s)";
                this.ancSPReg.InnerHtml = "Major Geographic Regions";
                this.ancSPSReg.InnerHtml = "Geographic Sub-Regions";
                this.ancSPCountry.InnerHtml = "Country / Nation";
                this.ancSPState.InnerHtml = "State / Province";
                this.ancSPCounty.InnerHtml = "County / District / Division / Municipality";
                this.ancSPField.InnerHtml = "O&G Fields";
                this.ancSPPad.InnerHtml = "Well Pad";
                this.ancSPWell.InnerHtml = "Well(s) / Lateral(s)";
                System.Data.DataTable spList = CLNT.getOperatorCoServiceProviderTableForReviewer(ClientID);
                this.gvwServiceProviderList.DataSource = spList;
                this.gvwServiceProviderList.SelectedIndex = -1;
                this.gvwServiceProviderList.DataBind();
                this.divSrvPList.Attributes["class"] = "panel-collapse collapse in";
                this.divSPReg.Attributes["class"] = "panel-collapse collapse";
                this.divSPSReg.Attributes["class"] = "panel-collapse collapse";
                this.divSPCountry.Attributes["class"] = "panel-collapse collapse";
                this.divSPState.Attributes["class"] = "panel-collapse collapse";
                this.divSPCounty.Attributes["class"] = "panel-collapse collapse";
                this.divSPField.Attributes["class"] = "panel-collapse collapse";
                this.divSPPad.Attributes["class"] = "panel-collapse collapse";
                this.divSPWell.Attributes["class"] = "panel-collapse collapse";
                this.btnSrvPClear.Enabled = false;
                this.btnSrvPClear.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }                
    }
}