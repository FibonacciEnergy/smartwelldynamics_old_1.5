﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlSrvProvider.ascx.cs" Inherits="SmartsVer1.Control.SrvProviders.ctrlSrvProvider" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />

<!-- JavaScripts -->
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>

<!-- Local Page Style -->
<style type="text/css">
    .swdMenu {
        color: #ffffff;
    }

    .rotate {
        -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
        -moz-transform: rotate(90deg); /* Firefox */
        -ms-transform: rotate(90deg); /* IE 9 */
        transform: rotate(90deg); /* Standard syntax */
    }
</style>
<!--- Page local script --->
<script type="text/javascript">
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("rotate");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").addClass("rotate");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("rotate");
        });
    });
</script>

<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">        
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../Viewer/rwrViewer_2A.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li><a href="../../SrvProviders/rwrSrvProvider_2A.aspx"> Service Providers</a></li>
            </ol>
        </div>
    </div>    
</div>

<!-- Page Content -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div id="acrdProvidersOuter" class="panel-group" runat="server">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancGOptr" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlSrvProvider_acrdProvidersOuter" href="#BodyContent_ctrlSrvProvider_divGProviderOuter">
                                <span class="glyphicon glyphicon-menu-right"></span> Global Operators</a>
                        </h5>
                    </div>
                    <div id="divGProviderOuter" runat="server" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="uprgrsGO" runat="server" DisplayAfter="0" DynamicLayout="true" AssociatedUpdatePanelID="upnlGO">
                                <ProgressTemplate>
                                    <div id="divGOProcessing" runat="server" class="updateProgress">
                                        <asp:Image ID="imgGOProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel runat="server" ID="upnlGO">
                                <ContentTemplate>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div id="acrdGOList" class="panel-group">
                                            <div class="panel panel-danger panel-body">
                                                <div id="divJGages" runat="server" class="panel-collapse collapse in">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                            <div class="small-box bg-blue">
                                                                <div id="divOperators" class="inner" runat="server">
                                                                </div>
                                                                <div class="icon">
                                                                    <i class="fa fa-bank" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                            <div class="small-box bg-green">
                                                                <div id="divProviders" class="inner" runat="server">
                                                                </div>
                                                                <div class="icon">
                                                                    <i class="fa fa-group" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                            <div class="small-box bg-orange">
                                                                <div id="divPads" class="inner" runat="server">
                                                                </div>
                                                                <div class="icon">
                                                                    <i class="fa fa-th" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                            <div class="small-box bg-red">
                                                                <div id="divWells" class="inner" runat="server">
                                                                </div>
                                                                <div class="icon">
                                                                    <i class="fa fa-tint" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-danger panel-body">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="input-group">
                                                        <div class="box-tools">
                                                            <asp:LinkButton ID="btnGlobalClear" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnGlobalClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                    <div class="panel-group" id="acrdOPWellList">
                                                        <div class="panel panel-info">
                                                            <div class="panel-heading">
                                                                <h5 class="panel-title" style="text-align: left">
                                                                    <a id="ancOP" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlSrvProvider_acrdGOList" href="#BodyContent_ctrlSrvProvider_divOP">
                                                                        Global Operator Companies</a>
                                                                </h5>
                                                            </div>
                                                            <div id="divOP" runat="server" class="panel-collapse collapse in">
                                                                <div class="panel-body">
                                                                    <asp:GridView runat="server" ID="gvwGlobalOptr" DataKeyNames="clntID" CssClass="mydatagrid" AllowPaging="True" AllowSorting="True"
                                                                        AutoGenerateColumns="False" EmptyDataText="No Operator Company found."
                                                                        OnPageIndexChanging="gvwGlobalOptr_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15"
                                                                        HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow"
                                                                        OnSelectedIndexChanged="gvwGlobalOptr_SelectedIndexChanged">
                                                                        <Columns>
                                                                            <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                                <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                            </asp:ButtonField>
                                                                            <asp:BoundField DataField="clntID" Visible="False" />
                                                                            <asp:BoundField DataField="clntName" HeaderText="Operator Company" />
                                                                            <asp:BoundField DataField="wpdCount" HeaderText="Global Well Pad Count" />
                                                                            <asp:BoundField DataField="welCount" HeaderText="Global Well Count" />
                                                                            <asp:BoundField DataField="welGlobal" HeaderText="SMARTs Well Count" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-info">
                                                                <div class="panel-heading">
                                                                    <h5 class="panel-title" style="text-align: left">
                                                                        <a id="ancWellReg" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlAssets_2A_acrdGOList" href="#BodyContent_ctrlAssets_2A_divMRList">
                                                                            Major Geographic Region</a>
                                                                    </h5>
                                                                </div>
                                                                <div id="divMRList" runat="server" class="panel-collapse collapse">
                                                                    <div class="panel-body">
                                                                        <asp:GridView ID="gvwWPadMRList" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                                            AllowPaging="True" DataKeyNames="grID" OnPageIndexChanging="gvwWPadMRList_PageIndexChanging"
                                                                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header"
                                                                            PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnSelectedIndexChanged="gvwWPadMRList_SelectedIndexChanged">
                                                                            <Columns>
                                                                                <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                                    <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                                </asp:ButtonField>
                                                                                <asp:BoundField DataField="grID" HeaderText="grID" ReadOnly="True" Visible="False" />
                                                                                <asp:BoundField DataField="grName" HeaderText="Geographic Region Name" />
                                                                                <asp:BoundField DataField="padCount" HeaderText="Global Well Pad Count">
                                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="welCount" HeaderText="Global Well/Lateral Count">
                                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-info">
                                                                <div class="panel-heading">
                                                                    <h5 class="panel-title" style="text-align: left">
                                                                        <a id="ancWellSReg" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlAssets_2A_acrdGOList" href="#BodyContent_ctrlAssets_2A_divSRList">
                                                                            Sub-Region</a>
                                                                    </h5>
                                                                </div>
                                                                <div id="divSRList" runat="server" class="panel-collapse collapse">
                                                                    <div class="panel-body">
                                                                        <asp:GridView ID="gvwSRList" runat="server" AutoGenerateColumns="False" DataKeyNames="gsrID"
                                                                            AllowPaging="True" AllowSorting="True" CssClass="mydatagrid" OnPageIndexChanging="gvwSRList_PageIndexChanging"
                                                                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                            RowStyle-CssClass="rows" OnSelectedIndexChanged="gvwSRList_SelectedIndexChanged">
                                                                            <Columns>
                                                                                <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                                    <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                                </asp:ButtonField>
                                                                                <asp:BoundField DataField="gsrID" HeaderText="gsrID" Visible="False"></asp:BoundField>
                                                                                <asp:BoundField DataField="gsrName" HeaderText="Geographic Sub-Region" SortExpression="gsrName"></asp:BoundField>
                                                                                <asp:BoundField DataField="padCount" HeaderText="Global Well Pad Count">
                                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="welCount" HeaderText="Global Well/Lateral Count">
                                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-info">
                                                                <div class="panel-heading">
                                                                    <h5 class="panel-title" style="text-align: left">
                                                                        <a id="ancWellCountry" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlAssets_2A_acrdGOList" href="#BodyContent_ctrlAssets_2A_divCountryList">
                                                                            Country / Nation</a>
                                                                    </h5>
                                                                </div>
                                                                <div id="divCountryList" runat="server" class="panel-collapse collapse">
                                                                    <div class="panel-body">
                                                                        <asp:GridView ID="gvwCountryList" runat="server" AutoGenerateColumns="False" DataKeyNames="ctyID"
                                                                            AllowPaging="True" AllowSorting="True" CssClass="mydatagrid" EmptyDataText="No Country/Nation found for selected Sub-Region"
                                                                            ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwCountryList_PageIndexChanging" OnSelectedIndexChanged="gvwCountryList_SelectedIndexChanged"
                                                                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                            RowStyle-CssClass="rows">
                                                                            <Columns>
                                                                                <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                                    <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                                </asp:ButtonField>
                                                                                <asp:BoundField DataField="ctyID" Visible="False"></asp:BoundField>
                                                                                <asp:BoundField DataField="ctyName" HeaderText="Country/Nation" SortExpression="ctyName"></asp:BoundField>
                                                                                <asp:BoundField DataField="ctyCode" HeaderText="Letter Code" SortExpression="ctyCode"></asp:BoundField>
                                                                                <asp:BoundField DataField="ctyDialCode" HeaderText="Dial Code" SortExpression="ctyDialCode"></asp:BoundField>
                                                                                <asp:BoundField DataField="padCount" HeaderText="Global Well Pad Count">
                                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="welCount" HeaderText="Global Well/Lateral Count">
                                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-info">
                                                                <div class="panel-heading">
                                                                    <h5 class="panel-title" style="text-align: left">
                                                                        <a id="ancWellState" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlAssets_2A_acrdGOList" href="#BodyContent_ctrlAssets_2A_divStateList">
                                                                            State / Province</a>
                                                                    </h5>
                                                                </div>
                                                                <div id="divStateList" runat="server" class="panel-collapse collapse">
                                                                    <div class="panel-body">
                                                                        <asp:GridView ID="gvwStateList" runat="server" AutoGenerateColumns="False" DataKeyNames="stID" AllowPaging="True"
                                                                            CssClass="mydatagrid" EmptyDataText="No State/Province found for selected Country/Nation" ShowHeaderWhenEmpty="true"
                                                                            OnPageIndexChanging="gvwStateList_PageIndexChanging" OnSelectedIndexChanged="gvwStateList_SelectedIndexChanged"
                                                                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                            RowStyle-CssClass="rows">
                                                                            <Columns>
                                                                                <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                                    <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                                </asp:ButtonField>
                                                                                <asp:BoundField DataField="stID" HeaderText="stID" ReadOnly="True" InsertVisible="False" SortExpression="stID" Visible="False" />
                                                                                <asp:BoundField DataField="stName" HeaderText="State/Province" />
                                                                                <asp:BoundField DataField="stCode" HeaderText="Letter Code" />
                                                                                <asp:BoundField DataField="stAPI" HeaderText="API Code" />
                                                                                <asp:BoundField DataField="padCount" HeaderText="Global Well Pad Count">
                                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="welCount" HeaderText="Global Well/Lateral Count">
                                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-info">
                                                                <div class="panel-heading">
                                                                    <h5 class="panel-title" style="text-align: left">
                                                                        <a id="ancWellCounty" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlAssets_2A_acrdGOList" href="#BodyContent_ctrlAssets_2A_divCountyList">
                                                                            County / District / Division / Municipality</a>
                                                                    </h5>
                                                                </div>
                                                                <div id="divCountyList" runat="server" class="panel-collapse collapse">
                                                                    <div class="panel-body">
                                                                        <asp:GridView runat="server" ID="gvwCountyList" AutoGenerateColumns="False" DataKeyNames="cntyID" AllowPaging="True"
                                                                            CssClass="mydatagrid" ShowHeaderWhenEmpty="True" EmptyDataText="No data found for selected State/Province"
                                                                            OnPageIndexChanging="gvwCountyList_PageIndexChanging" OnSelectedIndexChanged="gvwCountyList_SelectedIndexChanged"
                                                                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                            RowStyle-CssClass="rows">
                                                                            <Columns>
                                                                                <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                                    <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                                </asp:ButtonField>
                                                                                <asp:BoundField DataField="cntyID" HeaderText="cntyID" ReadOnly="True" InsertVisible="False"
                                                                                    SortExpression="cntyID" Visible="False" />
                                                                                <asp:BoundField DataField="cntyName" HeaderText="County/District/Division" />
                                                                                <asp:BoundField DataField="cntyCode" HeaderText="County Code" />
                                                                                <asp:BoundField DataField="cntyFIPS" HeaderText="FIPS Code" />
                                                                                <asp:BoundField DataField="padCount" HeaderText="Global Well Pad Count">
                                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="welCount" HeaderText="Global Well/Lateral Count">
                                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-info">
                                                                <div class="panel-heading">
                                                                    <h5 class="panel-title" style="text-align: left">
                                                                        <a id="ancWellField" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlAssets_2A_acrdGOList" href="#BodyContent_ctrlAssets_2A_divWellField">
                                                                            O&G Fields</a>
                                                                    </h5>
                                                                </div>
                                                                <div id="divWellField" runat="server" class="panel-collapse collapse">
                                                                    <div class="panel-body">
                                                                        <asp:GridView ID="gvwFldList" runat="server" EmptyDataText="No Field found for selected County/District/Division"
                                                                            ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="fldID"
                                                                            AllowPaging="True" CssClass="mydatagrid" OnPageIndexChanging="gvwFldList_PageIndexChanging" OnSelectedIndexChanged="gvwFldList_SelectedIndexChanged"
                                                                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                            RowStyle-CssClass="rows">
                                                                            <Columns>
                                                                                <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                                    <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                                </asp:ButtonField>
                                                                                <asp:BoundField DataField="fldID" HeaderText="fldID" ReadOnly="True" Visible="false" />
                                                                                <asp:BoundField DataField="fldName" HeaderText="Name" />
                                                                                <asp:BoundField DataField="fldCode" HeaderText="Code" />
                                                                                <asp:BoundField DataField="hcOil" HeaderText="Oil" />
                                                                                <asp:BoundField DataField="hcGas" HeaderText="Gas" />
                                                                                <asp:BoundField DataField="hcAssoc" HeaderText="Associated" />
                                                                                <asp:BoundField DataField="hcUnknown" HeaderText="Unknown" />
                                                                                <asp:BoundField DataField="Year" HeaderText="Year Discovered" />
                                                                                <asp:BoundField DataField="padCount" HeaderText="Global Well Pad Count">
                                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="welCount" HeaderText="Global Well/Lateral Count">
                                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-info">
                                                                <div class="panel-heading">
                                                                    <h5 class="panel-title" style="text-align: left">
                                                                        <a id="ancWellPadList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlAssets_2A_acrdGOList" href="#BodyContent_ctrlAssets_2A_divWellPadList">
                                                                            Well Pad</a>
                                                                    </h5>
                                                                </div>
                                                                <div id="divWellPadList" runat="server" class="panel-collapse collapse">
                                                                    <div class="panel-body">
                                                                        <asp:GridView ID="gvwWellPads" runat="server" EmptyDataText="No Well Pad(s) found for selected Operator"
                                                                            ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="clientWellPadID"
                                                                            AllowPaging="True" CssClass="mydatagrid" OnPageIndexChanging="gvwWellPads_PageIndexChanging" OnSelectedIndexChanged="gvwWellPads_SelectedIndexChanged"
                                                                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                            RowStyle-CssClass="rows">
                                                                            <Columns>
                                                                                <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                                    <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                                </asp:ButtonField>
                                                                                <asp:BoundField DataField="clientWellPadID" HeaderText="wpd" ReadOnly="True" Visible="false" />
                                                                                <asp:BoundField DataField="welPadName" HeaderText="Well Pad" />
                                                                                <asp:BoundField DataField="field" HeaderText="O&G Field" />
                                                                                <asp:BoundField DataField="county" HeaderText="County/District/Division/Municipality" />
                                                                                <asp:BoundField DataField="state" HeaderText="State/Province" />
                                                                                <asp:BoundField DataField="welCount" HeaderText="Global Well/Lateral Count">
                                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-success">
                                                                <div class="panel-heading">
                                                                    <h5 class="panel-title" style="text-align: left">
                                                                        <a id="ancWellList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlAssets_2A_acrdGOList" href="#BodyContent_ctrlAssets_2A_divWellList">
                                                                            Well(s) / Lateral(s)</a>
                                                                    </h5>
                                                                </div>
                                                                <div id="divWellList" runat="server" class="panel-collapse collapse">
                                                                    <div class="panel-body">
                                                                        <asp:GridView runat="server" ID="gvwWell" DataKeyNames="welID" CssClass="mydatagrid" AllowPaging="True" AllowSorting="True"
                                                                            AutoGenerateColumns="False" EmptyDataText="No Well(s)/Lateral(s) found for selected Operator Company."
                                                                            OnPageIndexChanging="gvwWell_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header"
                                                                            PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow" OnSelectedIndexChanged="gvwWell_SelectedIndexChanged">
                                                                            <Columns>
                                                                                <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                                    <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                                </asp:ButtonField>
                                                                                <asp:BoundField DataField="welID" HeaderText="welID" Visible="False" />
                                                                                <asp:BoundField DataField="welName" HeaderText="Well Name" />
                                                                                <asp:BoundField DataField="field" HeaderText="Field" />
                                                                                <asp:BoundField DataField="county" HeaderText="County / District / Municipality" />
                                                                                <asp:BoundField DataField="state" HeaderText="State / Province" />
                                                                                <asp:BoundField DataField="spud" HeaderText="Spud Date" DataFormatString="{0:MMM d , yyyy}" />
                                                                                <asp:BoundField DataField="wstID" HeaderText="SMARTs Status" />
                                                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                        <asp:GridView runat="server" ID="gvwWellDetail" DataKeyNames="welID" CssClass="mydatagrid" AllowPaging="True" AllowSorting="True"
                                                                            Enabled="false" Visible="false" AutoGenerateColumns="False" HeaderStyle-CssClass="header" RowStyle-CssClass="rows"
                                                                            SelectedRowStyle-CssClass="selectedrow">
                                                                            <Columns>
                                                                                <asp:BoundField DataField="welID" HeaderText="welID" Visible="False" />
                                                                                <asp:BoundField DataField="welPermit" HeaderText="Well Permit" />
                                                                                <asp:BoundField DataField="welAPI" HeaderText="American Petroleum Institude ID (API)" />
                                                                                <asp:BoundField DataField="welUWI" HeaderText="Unique Well Identifier (UWI)" />
                                                                                <asp:BoundField DataField="welLongitude" HeaderText="Longitude" />
                                                                                <asp:BoundField DataField="welLatitude" HeaderText="Latitude" />
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancSProvider" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlSrvProvider_acrdProvidersOuter" href="#BodyContent_ctrlSrvProvider_divSProvider">
                                <span class="glyphicon glyphicon-menu-right"></span> SMARTs Service Providers</a>
                        </h5>
                    </div>
                    <div id="divSProvider" runat="server" class="panel-collapse collapse">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="uprgrsSProv" runat="server" DisplayAfter="0" DynamicLayout="true" AssociatedUpdatePanelID="upnlSProv">
                                <ProgressTemplate>
                                    <div id="divSProvProcessing" runat="server" class="updateProgress">
                                        <asp:Image ID="imgSProvProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel runat="server" ID="upnlSProv">
                                <ContentTemplate>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div id="acrdWellList" class="panel-group">
                                            <div class="panel panel-danger panel-body">
                                                <div id="divWellGages" runat="server" class="panel-collapse collapse in">
                                                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                        <div class="small-box bg-blue">
                                                            <div id="divSiloOperators" class="inner" runat="server">
                                                            </div>
                                                            <div class="icon">
                                                                <i class="fa fa-bank" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                        <div class="small-box bg-green">
                                                            <div id="divSiloProviders" class="inner" runat="server">
                                                            </div>
                                                            <div class="icon">
                                                                <i class="fa fa-group" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                        <div class="small-box bg-orange">
                                                            <div id="divSiloPads" class="inner" runat="server">
                                                            </div>
                                                            <div class="icon">
                                                                <i class="fa fa-th" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                        <div class="small-box bg-red">
                                                            <div id="divSiloWells" class="inner" runat="server">
                                                            </div>
                                                            <div class="icon">
                                                                <i class="fa fa-tint" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-danger panel-body">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">                                                    
                                                    <div class="input-group" style="width: 100%;">
                                                        <asp:LinkButton ID="btnSrvPClear" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnSrvPClear_Click"><i class="fa fa-refresh"> Refresh</i></asp:LinkButton>
                                                    </div>                                                
                                                    <div class="panel panel-info">
                                                        <div class="panel-heading">
                                                            <h5 class="panel-title" style="text-align: left">
                                                                <a id="ancOptrList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlSrvProvider_acrdWellList" href="#BodyContent_ctrlSrvProvider_divSrvPList">
                                                                    Service Provider(s)</a>
                                                            </h5>
                                                        </div>
                                                        <div id="divSrvPList" runat="server" class="panel-collapse collapse in">
                                                            <div class="panel-body">
                                                                <asp:GridView ID="gvwServiceProviderList" runat="server" CssClass="mydatagrid" EmptyDataText="No Service Provider(s) found."
                                                                    AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="clntID" OnSelectedIndexChanged="gvwServiceProviderList_SelectedIndexChanged"
                                                                    ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwServiceProviderList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                    PageSize="8" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                                                    <Columns>
                                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                            <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                        </asp:ButtonField>
                                                                        <asp:BoundField DataField="clntID" Visible="false" />
                                                                        <asp:BoundField DataField="clntName" HeaderText="Service Provider Name" />
                                                                        <asp:BoundField DataField="clntRealm" HeaderText="Realm/Domain" />
                                                                        <asp:BoundField DataField="pdCount" HeaderText="Well Pad(s) Count">
                                                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="wlCount" HeaderText="Wells(s)/Lateral(s) Count">
                                                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>                                                
                                                    <div class="panel panel-info">
                                                        <div class="panel-heading">
                                                            <h5 class="panel-title" style="text-align: left">
                                                                <a id="ancSPReg" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlAssets_2A_acrdWellList" href="#BodyContent_ctrlAssets_2A_divSPReg">
                                                                    Major Geographic Region</a>
                                                            </h5>
                                                        </div>
                                                        <div id="divSPReg" runat="server" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <asp:GridView ID="gvwSPReg" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                                    AllowPaging="True" DataKeyNames="grID" OnPageIndexChanging="gvwSPReg_PageIndexChanging"
                                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header"
                                                                    PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnSelectedIndexChanged="gvwSPReg_SelectedIndexChanged">
                                                                    <Columns>
                                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                            <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                        </asp:ButtonField>
                                                                        <asp:BoundField DataField="grID" HeaderText="grID" ReadOnly="True" Visible="False" />
                                                                        <asp:BoundField DataField="grName" HeaderText="Geographic Region Name" />
                                                                        <asp:BoundField DataField="padCount" HeaderText="Global Well Pad Count">
                                                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="welCount" HeaderText="Global Well/Lateral Count">
                                                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-info">
                                                        <div class="panel-heading">
                                                            <h5 class="panel-title" style="text-align: left">
                                                                <a id="ancSPSReg" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlAssets_2A_acrdWellList" href="#BodyContent_ctrlAssets_2A_divSPSReg">
                                                                    Geographic Sub-Regions</a>
                                                            </h5>
                                                        </div>
                                                        <div id="divSPSReg" runat="server" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <asp:GridView ID="gvwSPSReg" runat="server" AutoGenerateColumns="False" DataKeyNames="gsrID"
                                                                    AllowPaging="True" AllowSorting="True" CssClass="mydatagrid" OnPageIndexChanging="gvwSPSReg_PageIndexChanging"
                                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                    RowStyle-CssClass="rows" OnSelectedIndexChanged="gvwSPSReg_SelectedIndexChanged">
                                                                    <Columns>
                                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                            <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                        </asp:ButtonField>
                                                                        <asp:BoundField DataField="gsrID" HeaderText="gsrID" Visible="False"></asp:BoundField>
                                                                        <asp:BoundField DataField="gsrName" HeaderText="Geographic Sub-Region" SortExpression="gsrName"></asp:BoundField>
                                                                        <asp:BoundField DataField="padCount" HeaderText="Global Well Pad Count">
                                                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="welCount" HeaderText="Global Well/Lateral Count">
                                                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-info">
                                                        <div class="panel-heading">
                                                            <h5 class="panel-title" style="text-align: left">
                                                                <a id="ancSPCountry" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlAssets_2A_acrdWellList" href="#BodyContent_ctrlAssets_2A_divSPCountry">
                                                                    Country / Nation</a>
                                                            </h5>
                                                        </div>
                                                        <div id="divSPCountry" runat="server" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <asp:GridView ID="gvwSPCountry" runat="server" AutoGenerateColumns="False" DataKeyNames="ctyID"
                                                                    AllowPaging="True" AllowSorting="True" CssClass="mydatagrid" EmptyDataText="No Country/Nation found for selected Sub-Region"
                                                                    ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwSPCountry_PageIndexChanging" OnSelectedIndexChanged="gvwSPCountry_SelectedIndexChanged"
                                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                    RowStyle-CssClass="rows">
                                                                    <Columns>
                                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                            <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                        </asp:ButtonField>
                                                                        <asp:BoundField DataField="ctyID" Visible="False"></asp:BoundField>
                                                                        <asp:BoundField DataField="ctyName" HeaderText="Country/Nation" SortExpression="ctyName"></asp:BoundField>
                                                                        <asp:BoundField DataField="ctyCode" HeaderText="Letter Code" SortExpression="ctyCode"></asp:BoundField>
                                                                        <asp:BoundField DataField="ctyDialCode" HeaderText="Dial Code" SortExpression="ctyDialCode"></asp:BoundField>
                                                                        <asp:BoundField DataField="padCount" HeaderText="Global Well Pad Count">
                                                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="welCount" HeaderText="Global Well/Lateral Count">
                                                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-info">
                                                        <div class="panel-heading">
                                                            <h5 class="panel-title" style="text-align: left">
                                                                <a id="ancSPState" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlAssets_2A_acrdWellList" href="#BodyContent_ctrlAssets_2A_divSPState">
                                                                    State / Province</a>
                                                            </h5>
                                                        </div>
                                                        <div id="divSPState" runat="server" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <asp:GridView ID="gvwSPState" runat="server" AutoGenerateColumns="False" DataKeyNames="stID" AllowPaging="True"
                                                                    CssClass="mydatagrid" EmptyDataText="No State/Province found for selected Country/Nation" ShowHeaderWhenEmpty="true"
                                                                    OnPageIndexChanging="gvwSPState_PageIndexChanging" OnSelectedIndexChanged="gvwSPState_SelectedIndexChanged"
                                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                    RowStyle-CssClass="rows">
                                                                    <Columns>
                                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                            <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                        </asp:ButtonField>
                                                                        <asp:BoundField DataField="stID" HeaderText="stID" ReadOnly="True" InsertVisible="False" SortExpression="stID" Visible="False" />
                                                                        <asp:BoundField DataField="stName" HeaderText="State/Province" />
                                                                        <asp:BoundField DataField="stCode" HeaderText="Letter Code" />
                                                                        <asp:BoundField DataField="stAPI" HeaderText="API Code" />
                                                                        <asp:BoundField DataField="padCount" HeaderText="Global Well Pad Count">
                                                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="welCount" HeaderText="Global Well/Lateral Count">
                                                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-info">
                                                        <div class="panel-heading">
                                                            <h5 class="panel-title" style="text-align: left">
                                                                <a id="ancSPCounty" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlAssets_2A_acrdWellList" href="#BodyContent_ctrlAssets_2A_divSPCounty">
                                                                    County / District / Division / Municipality</a>
                                                            </h5>
                                                        </div>
                                                        <div id="divSPCounty" runat="server" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <asp:GridView ID="gvwSPCounty" runat="server" AutoGenerateColumns="False" DataKeyNames="cntyID" AllowPaging="True"
                                                                    CssClass="mydatagrid" ShowHeaderWhenEmpty="True" EmptyDataText="No data found for selected State/Province"
                                                                    OnPageIndexChanging="gvwSPCounty_PageIndexChanging" OnSelectedIndexChanged="gvwSPCounty_SelectedIndexChanged"
                                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                    RowStyle-CssClass="rows">
                                                                    <Columns>
                                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                            <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                        </asp:ButtonField>
                                                                        <asp:BoundField DataField="cntyID" HeaderText="cntyID" ReadOnly="True" InsertVisible="False"
                                                                            SortExpression="cntyID" Visible="False" />
                                                                        <asp:BoundField DataField="cntyName" HeaderText="County/District/Division" />
                                                                        <asp:BoundField DataField="cntyCode" HeaderText="County Code" />
                                                                        <asp:BoundField DataField="cntyFIPS" HeaderText="FIPS Code" />
                                                                        <asp:BoundField DataField="padCount" HeaderText="Global Well Pad Count">
                                                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="welCount" HeaderText="Global Well/Lateral Count">
                                                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-info">
                                                        <div class="panel-heading">
                                                            <h5 class="panel-title" style="text-align: left">
                                                                <a id="ancSPField" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlAssets_2A_acrdWellList" href="#BodyContent_ctrlAssets_2A_divSPField">
                                                                    O&G Fields</a>
                                                            </h5>
                                                        </div>
                                                        <div id="divSPField" runat="server" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <asp:GridView ID="gvwSPField" runat="server" EmptyDataText="No Field found for selected County/District/Division"
                                                                    ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="fldID"
                                                                    AllowPaging="True" CssClass="mydatagrid" OnPageIndexChanging="gvwSPField_PageIndexChanging" OnSelectedIndexChanged="gvwSPField_SelectedIndexChanged"
                                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                    RowStyle-CssClass="rows">
                                                                    <Columns>
                                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                            <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                        </asp:ButtonField>
                                                                        <asp:BoundField DataField="fldID" HeaderText="fldID" ReadOnly="True" Visible="false" />
                                                                        <asp:BoundField DataField="fldName" HeaderText="Name" />
                                                                        <asp:BoundField DataField="fldCode" HeaderText="Code" />
                                                                        <asp:BoundField DataField="hcOil" HeaderText="Oil" />
                                                                        <asp:BoundField DataField="hcGas" HeaderText="Gas" />
                                                                        <asp:BoundField DataField="hcAssoc" HeaderText="Associated" />
                                                                        <asp:BoundField DataField="hcUnknown" HeaderText="Unknown" />
                                                                        <asp:BoundField DataField="Year" HeaderText="Year Discovered" />
                                                                        <asp:BoundField DataField="padCount" HeaderText="Global Well Pad Count">
                                                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="welCount" HeaderText="Global Well/Lateral Count">
                                                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-info">
                                                        <div class="panel-heading">
                                                            <h5 class="panel-title" style="text-align: left">
                                                                <a id="ancSPPad" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlAssets_2A_acrdWellList" href="#BodyContent_ctrlAssets_2A_divSPPad">
                                                                    Well Pad</a>
                                                            </h5>
                                                        </div>
                                                        <div id="divSPPad" runat="server" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <asp:GridView ID="gvwSPPad" runat="server" EmptyDataText="No Well Pad(s) found for selected Operator"
                                                                    ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="clientWellPadID"
                                                                    AllowPaging="True" CssClass="mydatagrid" OnPageIndexChanging="gvwSPPad_PageIndexChanging" OnSelectedIndexChanged="gvwSPPad_SelectedIndexChanged"
                                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                    RowStyle-CssClass="rows">
                                                                    <Columns>
                                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                            <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                        </asp:ButtonField>
                                                                        <asp:BoundField DataField="clientWellPadID" HeaderText="wpd" ReadOnly="True" Visible="false" />
                                                                        <asp:BoundField DataField="welPadName" HeaderText="Well Pad" />
                                                                        <asp:BoundField DataField="field" HeaderText="O&G Field" />
                                                                        <asp:BoundField DataField="county" HeaderText="County/District/Division/Municipality" />
                                                                        <asp:BoundField DataField="state" HeaderText="State/Province" />
                                                                        <asp:BoundField DataField="welCount" HeaderText="Global Well/Lateral Count">
                                                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-success">
                                                        <div class="panel-heading">
                                                            <h5 class="panel-title" style="text-align: left">
                                                                <a id="ancSPWell" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlAssets_2A_acrdWellList" href="#BodyContent_ctrlAssets_2A_divSPWell">
                                                                    Well(s) / Lateral(s)</a>
                                                            </h5>
                                                        </div>
                                                        <div id="divSPWell" runat="server" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <asp:GridView ID="gvwSPWell" runat="server" DataKeyNames="welID" CssClass="mydatagrid" AllowPaging="True" AllowSorting="True"
                                                                    AutoGenerateColumns="False" EmptyDataText="No Well(s)/Lateral(s) found for selected Operator Company."
                                                                    OnPageIndexChanging="gvwSPWell_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header"
                                                                    PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow" OnSelectedIndexChanged="gvwSPWell_SelectedIndexChanged">
                                                                    <Columns>
                                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                            <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                        </asp:ButtonField>
                                                                        <asp:BoundField DataField="welID" HeaderText="welID" Visible="False" />
                                                                        <asp:BoundField DataField="welName" HeaderText="Well Name" />
                                                                        <asp:BoundField DataField="field" HeaderText="Field" />
                                                                        <asp:BoundField DataField="county" HeaderText="County / District / Municipality" />
                                                                        <asp:BoundField DataField="state" HeaderText="State / Province" />
                                                                        <asp:BoundField DataField="spud" HeaderText="Spud Date" DataFormatString="{0:MMM d , yyyy}" />
                                                                        <asp:BoundField DataField="wstID" HeaderText="SMARTs Status" />
                                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                                <asp:GridView runat="server" ID="gvwSPWellDetail" DataKeyNames="welID" CssClass="mydatagrid" AllowPaging="True" AllowSorting="True"
                                                                    Enabled="false" Visible="false" AutoGenerateColumns="False" HeaderStyle-CssClass="header" RowStyle-CssClass="rows"
                                                                    SelectedRowStyle-CssClass="selectedrow">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="welID" HeaderText="welID" Visible="False" />
                                                                        <asp:BoundField DataField="welPermit" HeaderText="Well Permit" />
                                                                        <asp:BoundField DataField="welAPI" HeaderText="American Petroleum Institude ID (API)" />
                                                                        <asp:BoundField DataField="welUWI" HeaderText="Unique Well Identifier (UWI)" />
                                                                        <asp:BoundField DataField="welLongitude" HeaderText="Longitude" />
                                                                        <asp:BoundField DataField="welLatitude" HeaderText="Latitude" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
