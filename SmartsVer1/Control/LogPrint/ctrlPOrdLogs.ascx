﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlPOrdLogs.ascx.cs" Inherits="SmartsVer1.Control.LogPrint.ctrlPOrdLogs" %>

<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>


<style type="text/css">
    .panelBkg {
        background-color: #C1CFDD;
    }
</style>
<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />    

<!-- page content -->
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box-tools pull-right">            
            <div class="box-tools pull-right">
                <ol class="breadcrumb">
                    <li><a href="../../Viewer/rwrViewer_2A.aspx"><i class="fa fa-dashboard"></i> SMARTs Home</a></li>
                    <li>Logs Management</li>
                    <li><a href="~/LogPrint/fePrintOrderLogs.aspx"> Order Log Print(s)</a></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box box-primary">
            <div class="box-header">
                <h1 class="box-title">Order Summary</h1>
            </div>
            <div class="box box-body with-border">
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="small-box bg-green">
                        <div id="divTotalOrders" class="inner" runat="server"></div>
                        <div class="icon"><i class="fa fa-first-order"></i></div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="small-box bg-orange-active">
                        <div id="divSubmittedOrders" class="inner" runat="server"></div>
                        <div class="icon"><i class="fa fa-list"></i></div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="small-box bg-light-blue">
                        <div id="divInprogressOrders" class="inner" runat="server"></div>
                        <div class="icon"><i class="fa fa-arrow-right"></i></div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="small-box bg-blue">
                        <div id="divCompleteOrders" class="inner" runat="server"></div>
                        <div class="icon"><i class="fa fa-check-circle"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box box-primary">
            <div class="box-header">
                <h1 class="box-title">Order Log Print(s)</h1>
            </div>
            <div class="box box-body">
                <asp:UpdateProgress ID="upgrsMeta" runat="server" AssociatedUpdatePanelID="upnlMeta" DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="divMetaProgress" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                            z-index: 100;">
                            <asp:Image ID="upgrsMetaImage" runat="server" CssClass="center" AlternateText="Processing ...." ForeColor="Purple" ImageAlign="Middle"
                                ImageUrl="~/Images/Progress.gif" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <asp:UpdatePanel runat="server" ID="upnlMeta">
                    <ContentTemplate>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="panel-group" id="accordion" runat="server">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title" style="text-align: left">
                                            <a id="ancOperator" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlPOrdLogs_accordion" href="#BodyContent_ctrlPOrdLogs_divOperatorClass">
                                                Select Operator</a>
                                        </h5>
                                    </div>
                                    <div id="divOperatorClass" runat="server" class="panel-collapse collapse in">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <asp:GridView ID="gvwOperatorList" runat="server" CssClass="mydatagrid" EmptyDataText="No Operator found."
                                                    AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="clntID" ShowHeaderWhenEmpty="True" OnSelectedIndexChanged="gvwOperatorList_SelectedIndexChanged"
                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                    RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow" OnPageIndexChanging="gvwOperatorList_PageIndexChanging">
                                                    <Columns>
                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                            <ControlStyle CssClass="btn btn-primary text-center"></ControlStyle>
                                                        </asp:ButtonField>
                                                        <asp:BoundField DataField="clntID" Visible="false" />
                                                        <asp:BoundField DataField="clntName" HeaderText="Operator Name" />
                                                        <asp:BoundField DataField="clntRealm" HeaderText="Operator Realm/Domain" />
                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title" style="text-align: left">
                                            <a id="ancWellList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlPOrdLogs_accordion" href="#BodyContent_ctrlPOrdLogs_divWellClass">
                                                Select Well</a>
                                        </h5>
                                    </div>
                                    <div id="divWellClass" runat="server" class="panel-collapse collapse">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <asp:GridView ID="gvwWellList" runat="server" CssClass="mydatagrid" EmptyDataText="No Wells found."
                                                    AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="welID" ShowHeaderWhenEmpty="True" OnSelectedIndexChanged="gvwWellList_SelectedIndexChanged"
                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                    OnPageIndexChanging="gvwWellList_PageIndexChanging" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                                    <Columns>
                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                            <ControlStyle CssClass="btn btn-primary text-center"></ControlStyle>
                                                        </asp:ButtonField>
                                                        <asp:BoundField DataField="welID" Visible="false" />
                                                        <asp:BoundField DataField="welName" HeaderText="Well Name" />
                                                        <asp:BoundField DataField="field" HeaderText="Field" />
                                                        <asp:BoundField DataField="county" HeaderText="County/District" />
                                                        <asp:BoundField DataField="state" HeaderText="State/Province" />
                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title" style="text-align: left">
                                            <a id="ancLF" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlPOrdLogs_accordion" href="#BodyContent_ctrlPOrdLogs_divLogFiles">
                                                Select Log Files</a>
                                        </h5>
                                    </div>
                                    <div id="divLogFiles" runat="server" class="panel-collapse collapse">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <asp:GridView ID="gvwFileExplorer" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="50"
                                                    CssClass="mydatagrid" PagerStyle-CssClass="pager" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                    EmptyDataText="No Wells Logs available for selected Well" ShowHeaderWhenEmpty="True">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Select">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkRow" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="File" HeaderText="Log Files" ItemStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField DataField="CreationTime" HeaderText="Creation Time" />
                                                        <asp:BoundField DataField="Size" HeaderText="Size in KB" />
                                                        <asp:BoundField DataField="Type" HeaderText="Extension" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div runat="server" id="testEnclosure" visible="false">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                    <h4><i runat="server" id="iTest"><span runat="server" id="spnTest"></span></i></h4>
                                    <asp:Label ID="lblTest" runat="server" Text="" />
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="input-group" style="width: 100%;">
                                    <asp:Button ID="btnPromoteFiles" runat="server" CssClass="btn btn-success text-center" Text="Promote Files for Printing"
                                        Width="15%" Visible="false" OnClick="btnPromoteFiles_Click" />
                                    <asp:Button ID="btnMetaClear" runat="server" CssClass="btn btn-warning text-center" BackColor="MediumPurple" Text="Clear Selection" Width="15%" Visible="false"
                                        OnClick="btnMetaClear_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div id="divOrder" runat="server" visible="false">
                                <div class="box box-primary">
                                    <div class="box-header">
                                        <h1 class="box-title">Order Log Print(s)</h1>
                                    </div>
                                    <div class="box box-body with-border">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" style="vertical-align: middle; text-align: right;">
                                                <asp:Label ID="lblPrintCopy" runat="server" CssClass="label label-default" Text="Type In/Select Printed Copy Count" Font-Size="Large"
                                                    Width="100%" />
                                            </div>
                                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                <asp:TextBox ID="txtPrintCopy" runat="server" CssClass="form-control" TextMode="Number" placeholder="Type In/Select Print Copy Count" Text="1" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtPrintCopy" runat="server" ErrorMessage="Only Numbers allowed" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                            </div>
                                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" style="vertical-align: middle; text-align: right;">
                                                <asp:Label ID="lblDVDCopy" runat="server" CssClass="label label-default" Text="Type In/Select CD/DVD Count" Font-Size="Large"
                                                    Width="100%" />
                                            </div>
                                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                <asp:TextBox ID="txtDVDCopy" runat="server" CssClass="form-control" TextMode="Number" placeholder="Type In/Select CD/DVD Count" Text="0" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtDVDCopy" runat="server" ErrorMessage="Only Numbers allowed" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:GridView ID="gvwAddress" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="20"
                                                CssClass="mydatagrid" DataKeyNames="clntAddID" PagerStyle-CssClass="pager" HeaderStyle-CssClass="header" RowStyle-CssClass="rows"
                                                AlternatingRowStyle-CssClass="AlternatingRowStyle" EmptyDataText="No Address avaiable" ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwAddress_PageIndexChanging">
                                                <Columns>
<%--                                                    <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                        <ControlStyle CssClass="btn btn-primary text-center"></ControlStyle>
                                                    </asp:ButtonField>--%>
                                                    <asp:TemplateField HeaderText="Select">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkRow" runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="clntAddID" HeaderText="clntAddID" />
                                                    <asp:BoundField DataField="title" HeaderText="Title" />
                                                    <asp:BoundField DataField="dmgID" HeaderText="Type" />
                                                    <asp:BoundField DataField="st1" HeaderText="Street 1" />
                                                    <asp:BoundField DataField="st2" HeaderText="Street 2" />
                                                    <asp:BoundField DataField="st3" HeaderText="Street 3" />
                                                    <asp:BoundField DataField="cityID" HeaderText="City/Town" />
                                                    <asp:BoundField DataField="cntyID" HeaderText="County/District" />
                                                    <asp:BoundField DataField="stID" HeaderText="State/Province" />
                                                    <asp:BoundField DataField="ctyID" HeaderText="Country/Nation" />
                                                    <asp:BoundField DataField="zipID" HeaderText="Zipcode/Postal Code" />
                                                    <asp:BoundField DataField="sregID" HeaderText="Sub-Region" />
                                                    <asp:BoundField DataField="regID" HeaderText="Geographic Region" />
                                                    <asp:BoundField DataField="uTime" HeaderText="Last Updated On" SortExpression="uTime" ReadOnly="True" DataFormatString="{0:MMM d , yyyy hh:mm tt}" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="divPromote" class="input-group" style="width: 100%;margin-bottom:5px">
                                                <asp:Button ID="btnPromoteLogs" runat="server" CssClass="btn btn-success" Text="Promote Order" Width="15%" OnClick="btnPromoteLogs_Click" />
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 10px;margin-bottom: 10px">                                            
                                            <asp:TextBox runat="server" ID="txtbxComments" TextMode="MultiLine" Height="100px" MaxLength="2500" BackColor="#F9F3FD"
                                                BorderWidth="3px" CssClass="form-control" Enabled="false" />
                                        </div>

                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group" style="width: 100%;margin-bottom:5px">
                                                <asp:Button ID="btnProceedFinal" runat="server" CssClass="btn btn-success" Visible="false" Text="Place Order" Width="15%" OnClick="btnProceedFinal_Click" />
                                                <asp:Button ID="btnClearAll" runat="server" Text="Clear / Done" CssClass="btn btn-warning text-center" Visible="false" Width="15%" OnClick="btnClearAll_Click" />
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                            <div runat="server" id="orsEnclosure" visible="false" style="margin-top:10px">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                <h4><i runat="server" id="iORS"><span runat="server" id="spnORS"></span></i></h4>
                                                <asp:Label ID="lblOrderStatus" runat="server" Text="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</div>
<!-- Scripts -->
<%--<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>--%>