﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Data;
using System.Net.Mail;
using System.Globalization;
using System.Web.Security;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using Ionic.Zip;
using System.Net;
using System.Threading;
using SmartsVer1.Helpers.LogPrint;
using log4net;

namespace SmartsVer1.Control.LogPrint
{
    public partial class ctrlReviewLogs : System.Web.UI.UserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ctrlReviewLogs));
        String LoginName, username, domain, GetClientDBString;
        Int32 ClientID = -99;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);
                username = addr.User;
                domain = addr.Host;
                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
                //Populating dropdown lists on the page 
                System.Data.DataTable spList = CLNT.getOperatorCoServiceProviderTableForReviewer(ClientID);
                this.gvwServiceProviderList.DataSource = spList;
                this.gvwServiceProviderList.PageIndex = 0;
                this.gvwServiceProviderList.SelectedIndex = -1;
                this.gvwServiceProviderList.DataBind();
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");
                Int32 padCount = -99, welCount = -99, spCount = -99, opCount = -99;
                List<Int32> assetCounts = CLNT.getReviewerAssetGlobalCounts();
                opCount = assetCounts[0];
                spCount = assetCounts[1];
                padCount = assetCounts[2];
                welCount = assetCounts[3];
                this.divOperators.InnerHtml = "<h3>" + opCount.ToString() + "</h3><span>Global Operator Companies</span>";
                this.divProviders.InnerHtml = "<h3>" + spCount.ToString() + "</h3><span>Service Provider Companies</span>";
                this.divPads.InnerHtml = "<h3>" + padCount.ToString() + "</h3><span>Total Global Well Pads</span>";
                this.divWells.InnerHtml = "<h3>" + spCount.ToString() + "</h3><span>Total Global Well(s)/Lateral(s)</span>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwServiceProviderList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable spList = CLNT.getOperatorCoServiceProviderTableForReviewer(ClientID);
                this.gvwServiceProviderList.DataSource = spList;
                this.gvwServiceProviderList.PageIndex = e.NewPageIndex;
                this.gvwServiceProviderList.SelectedIndex = -1;
                this.gvwServiceProviderList.DataBind();
                this.btnSrvPClear.Enabled = true;
                this.btnSrvPClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwServiceProviderList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 clntID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                GridViewRow pRow = this.gvwServiceProviderList.SelectedRow;
                String pName = Convert.ToString(Server.HtmlDecode(pRow.Cells[2].Text));
                this.ancHead.InnerHtml = "<strong> " + pName + "</strong>";
                Int32 ProviderID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                System.Data.DataTable padTable = AST.getClientWellPadTableForOperator(ClientID, ProviderID);
                this.gvwSPPad.DataSource = padTable;
                this.gvwSPPad.PageIndex = 0;
                this.gvwSPPad.SelectedIndex = -1;
                this.gvwSPPad.DataBind();
                this.divSrvPList.Visible = false;
                this.divSPPad.Visible = true;
                this.divSPWell.Visible = false;
                this.divLogs.Visible = false;
                this.btnSrvPClear.Enabled = true;
                this.btnSrvPClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSPPad_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 ProviderID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                System.Data.DataTable padTable = AST.getClientWellPadTableForOperator(ClientID, ProviderID);
                this.gvwSPPad.DataSource = padTable;
                this.gvwSPPad.PageIndex = e.NewPageIndex;
                this.gvwSPPad.SelectedIndex = -1;
                this.gvwSPPad.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSPPad_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow pRow = this.gvwServiceProviderList.SelectedRow;
                String pName = Convert.ToString(Server.HtmlDecode(pRow.Cells[2].Text));
                GridViewRow pdRow = this.gvwSPPad.SelectedRow;
                String pdName = Convert.ToString(Server.HtmlDecode(pdRow.Cells[2].Text));
                this.ancHead.InnerHtml = "<span style='font-weight: bold'>" + pName + " > " + pdName + "</span>";
                this.divSrvPList.Visible = false;
                this.divSPPad.Visible = false;
                this.divSPWell.Visible = true;
                this.divLogs.Visible = false;                
                Int32 ProviderID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                Int32 PadID = Convert.ToInt32(this.gvwSPPad.SelectedValue);
                System.Data.DataTable wellTable = AST.getProviderWellForWellPadTable(PadID, ProviderID);
                this.gvwSPWell.DataSource = wellTable;
                this.gvwSPWell.PageIndex = 0;
                this.gvwSPWell.SelectedIndex = -1;
                this.gvwSPWell.DataBind();
                this.btnSrvPClear.Enabled = true;
                this.btnSrvPClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSPWell_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 ProviderID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                Int32 PadID = Convert.ToInt32(this.gvwSPPad.SelectedValue);
                System.Data.DataTable wellTable = AST.getProviderWellForWellPadTable(PadID, ProviderID);
                this.gvwSPWell.DataSource = wellTable;
                this.gvwSPWell.PageIndex = e.NewPageIndex;
                this.gvwSPWell.SelectedIndex = -1;
                this.gvwSPWell.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSPWell_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow pRow = this.gvwServiceProviderList.SelectedRow;
                String pName = Convert.ToString(Server.HtmlDecode(pRow.Cells[2].Text));
                GridViewRow pdRow = this.gvwSPPad.SelectedRow;
                String pdName = Convert.ToString(Server.HtmlDecode(pdRow.Cells[2].Text));
                GridViewRow wRow = this.gvwSPWell.SelectedRow;
                String wName = Convert.ToString(Server.HtmlDecode(wRow.Cells[2].Text));
                this.ancHead.InnerHtml = "<span style='font-weight: bold'>" + pName + " > " + pdName + " > " + wName + "</span>";
                String uploadLogsDirectory, uploadCompletePath, providerID, wellPad, wellID, comments;
                providerID = Convert.ToString(this.gvwServiceProviderList.SelectedValue);
                wellPad = Convert.ToString(this.gvwSPPad.SelectedValue);
                wellID = Convert.ToString(this.gvwSPWell.SelectedValue);
                uploadLogsDirectory = WebConfigurationManager.AppSettings["LogsPath"].ToString();
                uploadCompletePath = (String.Format("{0}\\Operator_{1}\\SrvProvider_{2}\\WellPad_{3}\\Well_{4}\\", uploadLogsDirectory, ClientID, providerID, wellPad, wellID));
                if (!Directory.Exists(uploadCompletePath))
                {
                    this.div2Enclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.i2.Attributes["class"] = "icon fa fa-warning";
                    this.spn2.InnerText = " Missing information!";
                    this.lblTest2.Text = "No Logs found for Selected Well/Lateral";
                    this.div2Enclosure.Visible = true;
                    this.divSrvPList.Visible = false;
                    this.divSPPad.Visible = false;
                    this.divSPWell.Visible = true;
                    this.divLogs.Visible = false;
                }
                else
                {
                    if (Directory.GetFiles(uploadCompletePath).Length > 0)
                    {
                        HttpContext.Current.Session["uploadCompletePath"] = uploadCompletePath;
                        this.RefreshClientFileExplorerGrid();
                        comments = populateComments();
                        this.txtbxComments.Enabled = true;
                        this.txtbxComments.Text = comments;
                        this.divSrvPList.Visible = false;
                        this.divSPPad.Visible = false;
                        this.divSPWell.Visible = false;
                        this.divLogs.Visible = true;
                        this.lblTest2.Text = "";
                        this.div2Enclosure.Visible = false;
                        logger.Info("User: " + username + " Reviewed Logs Path: " + uploadCompletePath);
                    }
                    else
                    {
                        this.div2Enclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.i2.Attributes["class"] = "icon fa fa-warning";
                        this.spn2.InnerText = " Missing information!";
                        this.lblTest2.Text = "No Logs found for Selected Well/Lateral";
                        this.div2Enclosure.Visible = true;
                        this.divSrvPList.Visible = false;
                        this.divSPPad.Visible = false;
                        this.divSPWell.Visible = true;
                        this.divLogs.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSrvPClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.divSrvPList.Visible = true;
                this.divSPPad.Visible = false;
                this.divSPWell.Visible = false;
                this.divLogs.Visible = false;
                System.Data.DataTable spList = CLNT.getOperatorCoServiceProviderTableForReviewer(ClientID);
                this.gvwServiceProviderList.DataSource = spList;
                this.gvwServiceProviderList.PageIndex = 0;
                this.gvwServiceProviderList.SelectedIndex = -1;
                this.gvwServiceProviderList.DataBind();
                this.ancHead.InnerHtml = "Service Provider(s)";
                this.btnSrvPClear.Enabled = false;
                this.btnSrvPClear.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private void RefreshClientFileExplorerGrid()
        {
            try
            {
                String uploadCompletePath = Convert.ToString(HttpContext.Current.Session["uploadCompletePath"]);
                DataTable dt = new DataTable();
                dt.Columns.Add("File", typeof(String));
                dt.Columns.Add("CreationTime", typeof(String));
                dt.Columns.Add("Size", typeof(String));
                dt.Columns.Add("Type", typeof(String));
                Int32 noPDFFiles = 0;


                foreach (string strFile in Directory.GetFiles(uploadCompletePath))
                {
                    FileInfo fi = new FileInfo(strFile);
                    String lengthKB = GetFileSizeInKB(strFile);
                    if (fi.Extension.ToString().Contains("pdf"))
                    {
                        dt.Rows.Add(fi.Name, fi.CreationTime, lengthKB, fi.Extension);
                        noPDFFiles = 1;
                    }
                }

                if (noPDFFiles == 1)
                {
                    this.grdFileExplorer.DataSource = dt;
                    this.grdFileExplorer.PageIndex = 0;
                    this.grdFileExplorer.DataBind();
                }
                if (noPDFFiles == 0)
                {
                    this.grdFileExplorer.DataSource = null;
                    this.grdFileExplorer.DataBind();
                    this.updEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iUPD.Attributes["class"] = "icon fa fa-warning";
                    this.spnUPD.InnerText = " Missing information!";
                    this.lblCommentsUpdate.Text = "No Well Logs found for Selected Well/Lateral";
                    this.updEnclosure.Visible = true;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public String GetFileSizeInKB(String FileName)
        {
            try
            {
                FileInfo fs = new FileInfo(FileName);
                long filesize = fs.Length / 1024;

                return System.Convert.ToString(filesize);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void grdFileExplorer_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                this.grdFileExplorer.PageIndex = e.NewPageIndex;
                RefreshClientFileExplorerGrid();
                this.grdFileExplorer.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDownloadFile_Click(object sender, EventArgs e)
        {
            try
            {
                int ifFileSelected = 0;
                String uploadCompletePath, providerID, wellID;

                uploadCompletePath = Convert.ToString(HttpContext.Current.Session["uploadCompletePath"]);
                using (ZipFile zip = new ZipFile())
                {
                    zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                    zip.AddDirectoryByName("Files");

                    foreach (GridViewRow row in grdFileExplorer.Rows)
                    {
                        CheckBox chkRow = (row.Cells[0].FindControl("chkRow") as CheckBox);

                        if (chkRow.Checked)
                        {

                            String filename = row.Cells[1].Text;
                            String filePath = Path.Combine(uploadCompletePath, filename);
                            zip.AddFile(filePath, "Files");
                            ifFileSelected = 1;
                        }
                        chkRow.Checked = false;
                    }
                    if (ifFileSelected == 1)
                    {
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BufferOutput = false;
                        String zipName = String.Format("ZipFiles_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
                        HttpContext.Current.Response.ContentType = "application/zip";
                        HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                        zip.Save(Response.OutputStream);
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        //HttpContext.Current.Response.End();
                        logger.Info("User: " + username + " Reviewed Well Logs Downloaded: " + zipName);
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnUncheckAll_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (GridViewRow row in grdFileExplorer.Rows)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("chkRow") as CheckBox);
                    if (chkRow.Checked)
                    {
                        chkRow.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnEditComments_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtbxComments.Enabled = true;
                this.btnSaveComments.Enabled = true;
                this.updEnclosure.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSaveComments_Click(object sender, EventArgs e)
        {
            try
            {
                String uploadCompletePath = Convert.ToString(this.Session["uploadCompletePath"]);
                String comments = this.txtbxComments.Text;
                if (String.IsNullOrEmpty(comments))
                {
                    this.updEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iUPD.Attributes["class"] = "icon fa fa-warning";
                    this.spnUPD.InnerText = " Warning!";
                    this.lblCommentsUpdate.Text = "Null comments can't be saved. Please provide a valid comment";
                    this.updEnclosure.Visible = true;
                    this.txtbxComments.Focus();
                }
                else
                {
                    this.updateComments(this.txtbxComments.Text);
                    this.updEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                    this.iUPD.Attributes["class"] = "icon fa fa-check-circle";
                    this.spnUPD.InnerText = " Warning!";
                    this.lblCommentsUpdate.Text = "The Comments have been saved";
                    this.updEnclosure.Visible = true;
                    this.btnSaveComments.Enabled = false;
                    this.txtbxComments.Enabled = false;
                    logger.Info("User: " + username + " Comments Updated for Reviewed Log Path: " + uploadCompletePath);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected String populateComments()
        {
            try
            {
                String output, time;
                output = time = null;
                Int32 providerID, wellID;

                providerID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                wellID = Convert.ToInt32(this.gvwSPWell.SelectedValue);

                LogComments Comments = new LogComments();
                output = Comments.getLogComments(ClientID, providerID, wellID, out time);
                this.timEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                this.iTime.Attributes["class"] = "icon fa fa-check-circle";
                this.spnTime.InnerText = " Comments Updated!";
                this.lblCommentsTime.Text = "Comments update Time: " + time;
                this.timEnclosure.Visible = true;

                return output;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void updateComments(String comment)
        {
            try
            {
                String output, time;
                output = time = null;
                Int32 providerID, wellID;

                providerID = Convert.ToInt32(this.gvwServiceProviderList.SelectedValue);
                wellID = Convert.ToInt32(this.gvwSPWell.SelectedValue);

                LogComments Comments = new LogComments();
                Comments.updateLogComments(ClientID, providerID, wellID, comment, out time);
                this.timEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                this.iTime.Attributes["class"] = "icon fa fa-success";
                this.spnTime.InnerText = " Comments Updated!";
                this.lblCommentsTime.Text = "Comments update Time: " + time;
                this.timEnclosure.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}