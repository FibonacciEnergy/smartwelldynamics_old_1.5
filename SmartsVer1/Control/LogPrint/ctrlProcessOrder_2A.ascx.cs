﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Net;
using System.Net.Mail;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Threading;
using Microsoft.SqlServer.Types;
using SmartsVer1.Helpers.QCHelpers;
using SmartsVer1.Helpers.LogPrint;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using ACC = SmartsVer1.Helpers.QCHelpers.AccountingQC;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using LGP = SmartsVer1.Helpers.LogPrint.LogComments;
using CHRT = SmartsVer1.Helpers.ChartHelpers.Gauges;
using Ionic.Zip;
using log4net;

namespace SmartsVer1.Control.LogPrint
{
    public partial class ctrlProcessOrder_2A : System.Web.UI.UserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ctrlProcessOrder_2A));
        String LoginName, username, clientDomain, clientDomainID, operatorDomain, GetClientDBString, reviewOrder;
        Int32 ClientID = -99;


        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                clientDomain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(clientDomain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                HttpContext.Current.Session["clntID"] = ClientID;
                HttpContext.Current.Session["clientDomain"] = clientDomain;
                this.txtbxComments.Text = "";
                this.txtbxComments.Visible = false;
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");
                GenerateOrderGraphs();
                RefreshClientOrderGird();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }


        protected void RefreshClientOrderGird()
        {
            try
            {
                System.Data.DataTable dt = new DataTable();
                dt = LGP.getAllClientsOrders();

                this.grdClientOrder.DataSource = dt;
                this.grdClientOrder.DataBind();

                this.grdClientOrder.Columns[1].Visible = false;
                this.grdClientOrder.Columns[2].Visible = false;
                this.grdClientOrder.Columns[5].Visible = false;
                this.grdClientOrder.Columns[7].Visible = false;
                this.grdClientOrder.Columns[8].Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void GenerateOrderGraphs()
        {
            try
            {
                Int32 totalcount, submitted, inprogress, complete;
                totalcount = submitted = inprogress = complete = 0;

                System.Data.DataTable dt = new DataTable();
                dt = LGP.getAllOrderCounts();

                foreach (DataRow row in dt.Rows)
                {
                    totalcount = Convert.ToInt32(row[0].ToString());
                    submitted = Convert.ToInt32(row[1].ToString());
                    inprogress = Convert.ToInt32(row[2].ToString());
                    complete = Convert.ToInt32(row[3].ToString());
                }

                this.divGuagetotalOrders.InnerHtml = "<h3>" + totalcount.ToString() + "</h3><span>Total Orders</span>";
                this.divGuagesubmittedOrders.InnerHtml = "<h3>" + submitted.ToString() + "</h3><span>In-Queue Orders</span>";
                this.divGuageinprogressOrders.InnerHtml = "<h3>" + inprogress.ToString() + "</h3><span>In Progress</span>";
                this.divGuagecompleteOrders.InnerHtml = "<h3>" + complete.ToString() + "</h3><span>Completed</span>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void grdClientOrder_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                String order = this.grdClientOrder.SelectedRow.Cells[3].Text;
                String filePath = this.grdClientOrder.SelectedRow.Cells[4].Text;
                String status = this.grdClientOrder.SelectedRow.Cells[6].Text;

                HttpContext.Current.Session["filePath"] = filePath;
                HttpContext.Current.Session["order"] = order;

                string orderDetails = LGP.getOrderComments(order);

                // PreSetting the dropdown list to the status
                this.ddlStatus.SelectedIndex = this.ddlStatus.Items.IndexOf(this.ddlStatus.Items.FindByText(status));

                this.txtbxComments.Text = orderDetails;
                this.txtbxComments.Visible = true;
                this.divOrderUpdate.Visible = true;
                this.btnDownloadFile.Enabled = true;
                this.enableDeleteDiv();
                this.btnDownloadFile.Visible = true;
                this.btnDeleteOrder.Visible = true;
                this.btnDone.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnUpdateStatus_Click(object sender, EventArgs e)
        {
            try
            {
                String status = this.ddlStatus.SelectedItem.Text;
                String order = Convert.ToString(this.Session["order"]);

                LogComments updateStatus = new LogComments();
                updateStatus.updatePrintStatus(order, status);

                // PreSetting the dropdown list to the status
                this.ddlStatus.SelectedIndex = this.ddlStatus.Items.IndexOf(this.ddlStatus.Items.FindByText(status));

                RefreshClientOrderGird();
                this.btnDownloadFile.Enabled = false;
                logger.Info("User: " + username + " Status updated for Well Logs. Order: " + order + " New Status: " + status);
                GenerateOrderGraphs();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDownloadFile_Click(object sender, EventArgs e)
        {
            try
            {
                String downloadPrintLogsDirectory = System.Web.Configuration.WebConfigurationManager.AppSettings["LogPrintPath"].ToString();
                //String downloadPrintLogsDirectory = WebConfigurationManager.AppSettings["LogsPath"].ToString();
                String order = Convert.ToString(this.Session["order"]);
                String zipName = order + ".zip";

                String filePath = Path.Combine(downloadPrintLogsDirectory, zipName);


                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.BufferOutput = false;

                HttpContext.Current.Response.ContentType = "application/zip";
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + zipName);

                HttpContext.Current.Response.WriteFile(filePath);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                //        HttpContext.Current.Response.End();                                

                logger.Info("User: " + username + " Process Well Logs for Order: " + order);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }


        protected void grdClientOrder_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable dt = new DataTable();
                dt = LGP.getAllClientsOrders();

                this.grdClientOrder.SelectedIndex = -1;
                this.grdClientOrder.PageIndex = e.NewPageIndex;
                this.grdClientOrder.DataSource = dt;
                this.grdClientOrder.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void enableDeleteDiv()
        {
            try
            {
                if (LoginName.ToUpper().Contains("ADMIN"))
                {
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDeleteOrder_Click(object sender, EventArgs e)
        {
            try
            {
                String status = "DELETED";
                int accountDelete = -99;
                string order = Convert.ToString(this.Session["order"]);
                LogComments deleteOrder = new LogComments();
                deleteOrder.updateDeleteStatus(order);

                LogComments updateStatus = new LogComments();
                updateStatus.updatePrintStatus(order, status);

                accountDelete = ACC.accDeleteLogPrint(order);
                logger.Info("********************  DELETED ORDER  *****************************");
                logger.Info("User: " + username + " Order: " + order + " DELETED!. New STATUS is :" + status);
                logger.Info("******************************************************************");
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.grdClientOrder.SelectedIndex = -1;
                this.txtbxComments.Text = "";
                this.txtbxComments.Visible = false;
                this.btnDownloadFile.Visible = false;
                this.btnDeleteOrder.Visible = false;
                this.btnDone.Visible = false;
                this.divOrderUpdate.Visible = false;
                this.ddlStatus.Items.Clear();
                this.ddlStatus.DataBind();
                this.ddlStatus.Items.Insert(0, new ListItem("--- Select New Status ---", "-1"));
                this.ddlStatus.Items.Insert(1, new ListItem("--- Select New Status ---", "1"));
                this.ddlStatus.Items.Insert(2, new ListItem("--- Select New Status ---", "2"));
                this.ddlStatus.Items.Insert(3, new ListItem("--- Select New Status ---", "3"));
                this.ddlStatus.SelectedIndex = -1;
                this.GenerateOrderGraphs();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }    
    }
}