﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlManageLogs.ascx.cs" Inherits="SmartsVer1.Control.LogPrint.ctrlManageLogs" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<!-- Stylesheet -->
<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript for ref lib -->
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<!-- Javascript for Footer -->
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>
<!--- Page local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }
    .panelBkg {
        background-color: #C1CFDD;
    }    
    .swdMenu {
        color: #ffffff;
    }
    a.disabled {
        pointer-events: none;
        cursor: default;
    }
    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }
</style>
<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>Logs Management</li>
                <li><a href="../../LogPrint/feManageLogs_2A.aspx">Manage Well/Lateral Logs</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- Page Content -->
<asp:UpdateProgress ID="upgrsMLogs" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlMLogs">
    <ProgressTemplate>
        <div id="divMLogsProgressbar" runat="server" class="updateProgress">
            <asp:Image ID="imgJob1Processing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<asp:UpdatePanel ID="upnlMLogs" runat="server">
    <ContentTemplate>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row bg-white">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bg-white">
                    <div id="acrdMgtOuter" class="panel-group" runat="server">
                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                <h5 class="panel-title" style="text-align: left">
                                    <a id="ancMgtMain" runat="server" class="disabled" href="#">Manage Well/Lateral Logs</a>
                                </h5>
                            </div>
                            <div id="divMgtOuter" runat="server" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div id="divProgress" class="panel panel-danger">
                                            <div class="panel-body">
                                                <ol id="olStepper" runat="server" class="stepper" data-stepper-steps="7">
                                                    <li id="liStep1" runat="server" class="stepper-todo">Operator Company</li>
                                                    <li id="liStep2" runat="server" class="stepper-todo">Well Pad</li>
                                                    <li id="liStep3" runat="server" class="stepper-todo">Well/Lateral</li>
                                                    <li id="liStep4" runat="server" class="stepper-todo">Upload Well/Lateral Log(s)</li>
                                                    <li id="liStep5" runat="server" class="stepper-todo">Well/Lateral Log(s)</li>
                                                </ol>
                                            </div>
                                        </div>
                                        <div class="panel panel-danger">
                                            <div class="panel-body">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div id="acrdMgtList" class="panel-group">
                                                        <div class="input-group" style="width: 100%;">
                                                            <asp:LinkButton ID="btnSelClear" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnSelClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                        </div>
                                                        <div class="panel panel-info">
                                                            <div class="panel-heading">
                                                                <h5 class="panel-title" style="text-align: left">
                                                                    <a id="ancHead" runat="server" class="disabled" href="#">Operator Company</a>
                                                                </h5>
                                                            </div>
                                                            <div id="divOPList" runat="server" class="panel-body">
                                                                <div class="panel-body">
                                                                    <asp:GridView runat="server" ID="gvwOperatorList" DataKeyNames="clntID" CssClass="mydatagrid" AllowPaging="True" AllowSorting="True"
                                                                        AutoGenerateColumns="False" EmptyDataText="No Operator Company found"
                                                                        OnPageIndexChanging="gvwOperatorList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15"
                                                                        HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow"
                                                                        OnSelectedIndexChanged="gvwOperatorList_SelectedIndexChanged">
                                                                        <Columns>
                                                                            <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                                <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                            </asp:ButtonField>
                                                                            <asp:BoundField DataField="clntID" Visible="False" />
                                                                            <asp:BoundField DataField="clntName" HeaderText="Operator Company" />
                                                                            <asp:BoundField DataField="welGlobal" HeaderText="SMARTs Well Count" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                            <div id="divPadList" runat="server" class="panel-body" visible="false">
                                                                <div class="table-responsive">
                                                                    <asp:GridView ID="gvwWellPads" runat="server" EmptyDataText="No Well Pad(s) found for selected Operator"
                                                                        ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="wpdID"
                                                                        AllowPaging="True" CssClass="mydatagrid" OnPageIndexChanging="gvwWellPads_PageIndexChanging" OnSelectedIndexChanged="gvwWellPads_SelectedIndexChanged"
                                                                        AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                        RowStyle-CssClass="rows">
                                                                        <Columns>
                                                                            <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                                <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                            </asp:ButtonField>
                                                                            <asp:BoundField DataField="wpdID" HeaderText="wpd" ReadOnly="True" Visible="false" />
                                                                            <asp:BoundField DataField="welPadName" HeaderText="Well Pad" />
                                                                            <asp:BoundField DataField="field" HeaderText="O&G Field" />
                                                                            <asp:BoundField DataField="county" HeaderText="County/District/Division/Municipality" />
                                                                            <asp:BoundField DataField="state" HeaderText="State/Province" />
                                                                            <asp:BoundField DataField="welCount" HeaderText="Global Well/Lateral Count">
                                                                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="swlCount" HeaderText="SMARTs Well Count">
                                                                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                            <div id="divWellList" runat="server" class="panel-body" visible="false">
                                                                <div class="table-responsive">
                                                                    <asp:GridView ID="gvwWellList" runat="server" CssClass="mydatagrid" EmptyDataText="No Well(s)/Lateral(s) found."
                                                                        AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="welID" ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwWellList_PageIndexChanging"
                                                                        AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                        OnSelectedIndexChanged="gvwWellList_SelectedIndexChanged" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                                                        <Columns>
                                                                            <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                                <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                            </asp:ButtonField>
                                                                            <asp:BoundField DataField="welID" Visible="false" />
                                                                            <asp:BoundField DataField="welName" HeaderText="Well / Lateral" />
                                                                            <asp:BoundField DataField="field" HeaderText="Oil & Gas Field" />
                                                                            <asp:BoundField DataField="county" HeaderText="County / District" />
                                                                            <asp:BoundField DataField="state" HeaderText="State / Province" />
                                                                            <asp:BoundField DataField="spud" HeaderText="Spud Date" />
                                                                            <asp:BoundField DataField="wstID" HeaderText="Status" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                    <div>&nbsp;</div>
                                                                    <div runat="server" id="txtEnclosure" visible="false" class="panel panel-danger panel-body">
                                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                        <h4><i runat="server" id="iTest"><span runat="server" id="spnTest"></span></i></h4>
                                                                        <asp:Label ID="lblTest" runat="server" Text="" />
                                                                    </div>                                                                    
                                                                </div>
                                                            </div>
                                                            <div id="divUpload" runat="server" class="panel-body" visible="false">
                                                                <div class="table-responsive">
                                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                        <div class="input-group">
                                                                            <span id="spnLogFile" runat="server" class="input-group-addon bg-light-blue">Select Log File</span>
                                                                            <asp:FileUpload ID="FileUploadControl" runat="server" CssClass="form-control" Font-Size="Medium" Width="80%" />
                                                                        </div>
                                                                        <div runat="server" id="fileEnclosure" visible="false">
                                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                            <h4><i runat="server" id="iFile"><span runat="server" id="spnFile"></span></i></h4>
                                                                            <asp:Label ID="FileUploadStatusLabel" runat="server" Text="" />
                                                                        </div>                                                                    
                                                                        <div class="input-group" style="width: 100%;">
                                                                            <asp:LinkButton ID="btn_FileUpload" runat="server" CssClass="btn btn-success text-center" OnClick="btn_FileUpload_Click"><i class="fa fa-upload"> Upload Well Log(s)</i></asp:LinkButton>
                                                                            <asp:LinkButton ID="btnRefreshFileUpload" runat="server" CssClass="btn btn-warning text-center" OnClick="btnRefreshFileUpload_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="divFileComments" runat="server" class="panel-body" visible="false">
                                                                <div class="table-responsive">
                                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                        <asp:GridView ID="grdFileExplorer" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="15"
                                                                            CssClass="mydatagrid" PagerStyle-CssClass="pager" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                            OnPageIndexChanging="grdFileExplorer_PageIndexChanging" EmptyDataText="No Wells Log(s) available for selected Well/Lateral"
                                                                            ShowHeaderWhenEmpty="True">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="Select">
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="chkRow" runat="server" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="File" HeaderText="Log Files" ItemStyle-Width="65%" ItemStyle-HorizontalAlign="Left" SortExpression="File" />
                                                                                <asp:BoundField DataField="CreationTime" HeaderText="Creation Time" />
                                                                                <asp:BoundField DataField="Size" HeaderText="Size in KB" />
                                                                                <asp:BoundField DataField="Type" HeaderText="Extension" />
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                    <div>&nbsp;</div>
                                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                        <asp:TextBox runat="server" ID="txtbxComments" CssClass="form-control" TextMode="MultiLine" Height="100px" MaxLength="2500"
                                                                            BackColor="#F9F3FD" Enabled="false" />
                                                                        <asp:Label runat="server" ID="lblCommentsTime" CssClass="label" Text="" Font-Bold="true" ForeColor="Black" Font-Size="Small" />
                                                                    </div>
                                                                    <div>&nbsp;</div>
                                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                        <div class="input-group" style="width: 100%;">
                                                                            <asp:LinkButton ID="btnDownloadFile" runat="server" CssClass="btn btn-success text-center" OnClick="btnDownloadFile_Click"><i class="fa fa-download"> Download Selected Files</i></asp:LinkButton>
                                                                            <asp:LinkButton ID="btnDeleteFile" runat="server" CssClass="btn btn-danger text-center" OnClick="btnDeleteFile_Click"><i class="fa fa-trash"> Delete Selected Files</i></asp:LinkButton>
                                                                            <asp:LinkButton ID="btnUncheckAll" runat="server" CssClass="btn btn-warning" OnClick="btnUncheckAll_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                        <div runat="server" id="delEnclosure" visible="false">
                                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                            <h4><i runat="server" id="iDel"><span runat="server" id="spnDel"></span></i></h4>
                                                                            <asp:Label ID="lblDeleteResults" runat="server" Text="" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                        <div class="input-group" style="width: 100%; margin-top: 10px">
                                                                            <asp:LinkButton ID="btnDone" runat="server" CssClass="btn btn-danger text-center" Visible="false" OnClick="btnDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="gvwWellList" />
        <asp:PostBackTrigger ControlID="btn_FileUpload" />
        <asp:PostBackTrigger ControlID="btnDownloadFile" />
        <asp:PostBackTrigger ControlID="btnUncheckAll" />
        <asp:PostBackTrigger ControlID="btnDone" />
    </Triggers>
</asp:UpdatePanel>
