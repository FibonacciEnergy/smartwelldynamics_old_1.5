﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlOrderTracker.ascx.cs" Inherits="SmartsVer1.Control.LogPrint.ctrlOrderTracker" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<!-- Stylesheet -->
<link href="../../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript for ref lib -->
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<!-- Javascript for Footer -->
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>
<!--- Page local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }

    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }
</style>
<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>Logs Management</li>
                <li><a href="../../LogPrint/feOrderTracker_2A.aspx">Track Log Print Orders</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- Page Content -->
<asp:UpdateProgress ID="upgrsOR" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlOR">
    <ProgressTemplate>
        <div id="divORProcessing" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
            z-index: 100;">
            <asp:Image ID="imgORProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<asp:UpdatePanel ID="upnlOR" runat="server">
    <ContentTemplate>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row bg-white">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div id="acrdOrderOuter" class="panel-group" runat="server">
                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                <h5 class="panel-title" style="text-align: left">
                                    <a id="ancMain" runat="server" class="disabled">Track Log Print Orders</a>
                                </h5>
                            </div>
                            <div id="divTOOuter" runat="server">
                                <div class="panel-body">
                                    <div id="acrdOrders" class="panel-group">
                                        <div class="panel panel-danger panel-body">
                                            <div id="divGages" runat="server" class="panel-collapse collapse in">
                                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                    <div class="small-box bg-green">
                                                        <div id="divTotalOrders" class="inner" runat="server"></div>
                                                        <div class="icon"><i class="fa fa-first-order"></i></div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                    <div class="small-box bg-orange-active">
                                                        <div id="divSubmittedOrders" class="inner" runat="server"></div>
                                                        <div class="icon"><i class="fa fa-list"></i></div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                    <div class="small-box bg-light-blue">
                                                        <div id="divInprogressOrders" class="inner" runat="server"></div>
                                                        <div class="icon"><i class="fa fa-arrow-right"></i></div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                    <div class="small-box bg-blue">
                                                        <div id="divCompleteOrders" class="inner" runat="server"></div>
                                                        <div class="icon"><i class="fa fa-check-circle"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h5 class="panel-title" style="text-align: left">
                                                    <a id="ancHead" runat="server" class="disabled">Log Print Orders List</a>
                                                </h5>
                                            </div>
                                            <div class="panel-body">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="input-group" style="width: 100%;">
                                                        <asp:LinkButton ID="btnClear" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div id="divOrderList" runat="server" class="panel-body">                                                
                                                    <asp:GridView ID="grdClientOrder" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="20"
                                                        CssClass="mydatagrid" PagerStyle-CssClass="pager" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                        EmptyDataText="No Log Print Submission Orders found" ShowHeaderWhenEmpty="True" OnSelectedIndexChanged="grdClientOrder_SelectedIndexChanged"
                                                        OnPageIndexChanging="grdClientOrder_PageIndexChanging" SelectedRowStyle-CssClass="selectedrow">
                                                        <Columns>
                                                            <asp:ButtonField CommandName="Select" Text="Order Details" ButtonType="Button">
                                                                <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                            </asp:ButtonField>
                                                            <asp:BoundField DataField="clientID" HeaderText="clientID" />
                                                            <asp:BoundField DataField="wellID" HeaderText="WellID" />
                                                            <asp:BoundField DataField="orderName" HeaderText="Print Order Name" />
                                                            <asp:BoundField DataField="fileSystemPath" HeaderText="File Path" />
                                                            <asp:BoundField DataField="filesToPrint" HeaderText="Files to Print" />
                                                            <asp:BoundField DataField="printStatus" HeaderText="Order Status" />
                                                            <asp:BoundField DataField="printComments" HeaderText="Comments" />
                                                            <asp:BoundField DataField="deliveryAddress" HeaderText="Address to deliver" />
                                                            <asp:BoundField DataField="orderBy" HeaderText="Ordered by User" />
                                                            <asp:BoundField DataField="datetime" HeaderText="Order Date/Time" ReadOnly="True" DataFormatString="{0:MMM d , yyyy hh:mm tt}" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                                <div id="divOrderDetails" runat="server" visible="false">
                                                    <div class="panel-body">
                                                            <div id="divTextBox" runat="server">
                                                                <asp:TextBox runat="server" ID="txtbxComments" TextMode="MultiLine" CssClass="form-control" Height="300px" MaxLength="2500"
                                                                    BackColor="#F9F3FD" Enabled="false" />
                                                            </div>                                                    
                                                    </div>
                                                </div>  
                                            </div>                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
