﻿using System;
using System.IO;
using System.Data;
using System.Net.Mail;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using LGP = SmartsVer1.Helpers.LogPrint.LogComments;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using Ionic.Zip;
using log4net;

namespace SmartsVer1.Control.LogPrint
{
    public partial class ctrlPrintOrderLogs : System.Web.UI.UserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ctrlPrintOrderLogs));
        String LoginName, username, clientDomain, GetClientDBString, reviewOrder;
        Int32 ClientID = -99;
        Int32 numPrint, numDVD = 0;
        const Int32 ClientType = 4;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                clientDomain = addr.Host;

                HttpContext.Current.Session["clientDomain"] = clientDomain;

                GetClientDBString = clntConn.getClientDBConnection(clientDomain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientType);
                this.gvwOperatorList.DataSource = operatorCoTable;
                this.gvwOperatorList.PageIndex = 0;
                this.gvwOperatorList.SelectedIndex = -1;
                this.gvwOperatorList.DataBind();
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");
                GenerateOrderGraphs();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwOperatorList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientType);
                this.gvwOperatorList.DataSource = operatorCoTable;
                this.gvwOperatorList.PageIndex = e.NewPageIndex;
                this.gvwOperatorList.SelectedIndex = -1;
                this.gvwOperatorList.DataBind();
                this.btnFileExpClear.Enabled = true;
                this.btnFileExpClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwOperatorList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow dRow = this.gvwOperatorList.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.ancHead.InnerHtml = "<span style='text-decoration: font-weight: bold'>" + oName + "</span>";
                Int32 oprID = Convert.ToInt32(this.gvwOperatorList.SelectedValue);
                System.Data.DataTable wpdTable = AST.getClientWellPadTableForOperator(oprID, ClientID);
                this.gvwWellPads.DataSource = wpdTable;
                this.gvwWellPads.PageIndex = 0;
                this.gvwWellPads.SelectedIndex = -1;
                this.gvwWellPads.DataBind();
                RefreshClientAddressGrid();
                this.divOPList.Visible = false;
                this.divPadList.Visible = true;
                this.divWellList.Visible = false;
                this.divFD.Visible = false;
                this.divFinalOrder.Visible = false;
                this.liStep1.Attributes["class"] = "stepper-done";
                this.liStep2.Attributes["class"] = "stepper-todo";
                this.liStep3.Attributes["class"] = "stepper-todo";
                this.liStep4.Attributes["class"] = "stepper-todo";
                this.liStep5.Attributes["class"] = "stepper-todo";
                this.btnFileExpClear.Enabled = true;
                this.btnFileExpClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellPads_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 opID = Convert.ToInt32(this.gvwOperatorList.SelectedValue);
                System.Data.DataTable wpdTable = AST.getClientWellPadTableForOperator(opID, ClientID);
                this.gvwWellPads.DataSource = wpdTable;
                this.gvwWellPads.PageIndex = e.NewPageIndex;
                this.gvwWellPads.SelectedIndex = -1;
                this.gvwWellPads.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellPads_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 pdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                System.Data.DataTable pdTable = AST.getWellForWellPadTable(pdID, GetClientDBString);
                this.gvwWellList.DataSource = pdTable;
                this.gvwWellList.PageIndex = 0;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();
                GridViewRow oRow = this.gvwOperatorList.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                GridViewRow pRow = this.gvwWellPads.SelectedRow;
                String pdName = Convert.ToString(Server.HtmlDecode(pRow.Cells[2].Text));
                this.ancHead.InnerHtml = "<span style='text-decoration: font-weight: bold'>" + oName + " > " + pdName + "</span>";
                this.divOPList.Visible = false;
                this.divPadList.Visible = false;
                this.divWellList.Visible = true;
                this.divFD.Visible = false;
                this.divFinalOrder.Visible = false;
                this.liStep1.Attributes["class"] = "stepper-done";
                this.liStep2.Attributes["class"] = "stepper-done";
                this.liStep3.Attributes["class"] = "stepper-todo";
                this.liStep4.Attributes["class"] = "stepper-todo";
                this.liStep5.Attributes["class"] = "stepper-todo";
                this.btnFileExpClear.Enabled = true;
                this.btnFileExpClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 pdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                System.Data.DataTable pdTable = AST.getWellForWellPadTable(pdID, GetClientDBString);
                this.gvwWellList.DataSource = pdTable;
                this.gvwWellList.PageIndex = e.NewPageIndex;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                String uploadLogsDirectory, uploadCompletePath, operatorDomainID, padID, wellName;
                Int32 wellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                operatorDomainID = Convert.ToString(this.gvwOperatorList.SelectedValue);
                this.testEnclosure.Visible = false;
                this.txtbxComments.Text = "";
                this.orsEnclosure.Visible = false;
                GridViewRow oRow = this.gvwOperatorList.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                GridViewRow pRow = this.gvwWellPads.SelectedRow;
                String pdName = Convert.ToString(Server.HtmlDecode(pRow.Cells[2].Text));                
                GridViewRow wRow = this.gvwWellList.SelectedRow;
                String wName = Convert.ToString(Server.HtmlDecode(wRow.Cells[2].Text));
                this.ancHead.InnerHtml = "<span style='text-decoration: font-weight: bold'>" + oName + " > " + pdName + " > " + wName + "</span>";
                padID = Convert.ToString(this.gvwWellPads.SelectedValue);
                wellName = Convert.ToString(this.gvwWellList.SelectedValue);
                uploadLogsDirectory = System.Web.Configuration.WebConfigurationManager.AppSettings["LogsPath"].ToString();
                uploadCompletePath = (String.Format("{0}\\Operator_{1}\\SrvProvider_{2}\\WellPad_{3}\\Well_{4}\\", uploadLogsDirectory, operatorDomainID, ClientID.ToString(), padID, wellName));
                if (!Directory.Exists(uploadCompletePath))
                {
                    this.testEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iTest.Attributes["class"] = "icon fa fa-warning";
                    this.spnTest.InnerText = " Warning!";
                    this.lblTest.Text = "Well Logs not found for selected Well/Lateral. Please Upload Well/Lateral Logs";
                    this.testEnclosure.Visible = true;
                }
                else
                {
                    if (Directory.GetFiles(uploadCompletePath).Length > 0)
                    {
                        HttpContext.Current.Session["uploadCompletePath"] = uploadCompletePath;
                        RefreshClientFileExplorerGrid();
                        this.divOPList.Visible = false;
                        this.divPadList.Visible = false;
                        this.divWellList.Visible = false;
                        this.divFD.Visible=true;
                        this.divFinalOrder.Visible = false;
                        this.liStep1.Attributes["class"] = "stepper-done";
                        this.liStep2.Attributes["class"] = "stepper-done";
                        this.liStep3.Attributes["class"] = "stepper-done";
                        this.liStep4.Attributes["class"] = "stepper-todo";
                        this.liStep5.Attributes["class"] = "stepper-todo";
                    }
                    else
                    {
                        this.testEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iTest.Attributes["class"] = "icon fa fa-warning";
                        this.spnTest.InnerText = " Warning!";
                        this.lblTest.Text = "Note: Logs for the Well/Lateral does not exist. Please Upload Well/Lateral Logs";
                        this.testEnclosure.Visible = true;
                        HttpContext.Current.Session["uploadCompletePath"] = uploadCompletePath;
                        RefreshClientFileExplorerGrid();
                        this.divOPList.Visible = false;
                        this.divPadList.Visible = false;
                        this.divWellList.Visible = true;
                        this.divFD.Visible = false;
                        this.divFinalOrder.Visible = false;
                        this.liStep1.Attributes["class"] = "stepper-done";
                        this.liStep2.Attributes["class"] = "stepper-done";
                        this.liStep3.Attributes["class"] = "stepper-todo";
                        this.liStep4.Attributes["class"] = "stepper-todo";
                        this.liStep5.Attributes["class"] = "stepper-todo";
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }


        protected void refreshFolder()
        {
            try
            {
                string uploadCompletePath = Convert.ToString(this.Session["uploadCompletePath"]);
                RefreshClientFileExplorerGrid();
                this.divFD.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void GenerateOrderGraphs()
        {
            try
            {
                Int32 totalcount = 0, submitted = 0, inprogress = 0, complete = 0;
                System.Data.DataTable dt = new DataTable();
                dt = LGP.getOrderCounts(ClientID);
                foreach (DataRow row in dt.Rows)
                {
                    totalcount = Convert.ToInt32(row[0].ToString());
                    submitted = Convert.ToInt32(row[1].ToString());
                    inprogress = Convert.ToInt32(row[2].ToString());
                    complete = Convert.ToInt32(row[3].ToString());
                }
                this.divTotalOrders.InnerHtml = "<h3>" + totalcount.ToString() + "</h3><p>Total Print Orders</p>";
                this.divSubmittedOrders.InnerHtml = "<h3>" + submitted.ToString() + "</h3><p>Orders Submitted</p>";
                this.divInprogressOrders.InnerHtml = "<h3>" + inprogress.ToString() + "</h3><p>Orders InProgress</p>";
                this.divCompleteOrders.InnerHtml = "<h3>" + complete.ToString() + "</h3><p>Completed Orders</p>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private void RefreshClientFileExplorerGrid()
        {
            try
            {
                String uploadCompletePath = Convert.ToString(this.Session["uploadCompletePath"]);
                DataTable dt = new DataTable();
                dt.Columns.Add("File", typeof(String));
                dt.Columns.Add("CreationTime", typeof(String));
                dt.Columns.Add("Size", typeof(String));
                dt.Columns.Add("Type", typeof(String));

                foreach (String strFile in Directory.GetFiles(uploadCompletePath))
                {
                    FileInfo fi = new FileInfo(strFile);
                    String lengthKB = GetFileSizeInKB(strFile);
                    dt.Rows.Add(fi.Name, fi.CreationTime, lengthKB, fi.Extension);
                }

                this.grdFileExplorer.DataSource = dt;
                this.grdFileExplorer.PageIndex = 0;
                this.grdFileExplorer.SelectedIndex = -1;
                this.grdFileExplorer.DataBind();
                this.divFD.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private void RefreshClientAddressGrid()
        {
            try
            {
                Int32 operatorDomainID = Convert.ToInt32(this.gvwOperatorList.SelectedValue);
                System.Data.DataTable dt = new DataTable();
                dt = CLNT.getAddressTable(operatorDomainID);
                this.grdAddress.DataSource = dt;
                this.grdAddress.PageIndex = 0;
                this.grdAddress.DataBind();
                this.grdAddress.Columns[1].Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void grdAddress_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 operatorDomainID = Convert.ToInt32(this.gvwOperatorList.SelectedValue);
                System.Data.DataTable dt = new DataTable();
                dt = CLNT.getAddressTable(operatorDomainID);
                this.grdAddress.DataSource = dt;
                this.grdAddress.PageIndex = e.NewPageIndex;
                this.grdAddress.DataBind();
                this.grdAddress.Columns[1].Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public string GetFileSizeInKB(String FileName)
        {
            try
            {
                FileInfo fs = new FileInfo(FileName);
                long filesize = fs.Length / 1024;

                return System.Convert.ToString(filesize);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void grdFileExplorer_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                this.grdFileExplorer.PageIndex = e.NewPageIndex;
                RefreshClientFileExplorerGrid();
                this.grdFileExplorer.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }


        protected void btnUncheckAll_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (GridViewRow row in grdFileExplorer.Rows)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("chkRow") as CheckBox);
                    if (chkRow.Checked)
                    {
                        chkRow.Checked = false;
                    }
                }
                foreach (GridViewRow row in grdAddress.Rows)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("chkRow") as CheckBox);
                    if (chkRow.Checked)
                    {
                        chkRow.Checked = false;
                    }

                }
                clearDropDownPrintList();
                this.btnFileExpClear_Click(null, null);
                this.txtbxComments.Text = "";
                this.lblOrderStatus.Text = "";                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnPromoteFiles_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 isFilechecked = 0;
                foreach (GridViewRow row in grdFileExplorer.Rows)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("chkRow") as CheckBox);
                    if (chkRow.Checked)
                    {
                        isFilechecked = 1;
                        this.cEnclosure.Visible = false;
                        this.divOPList.Visible = false;
                        this.divPadList.Visible = false;
                        this.divWellList.Visible = false;
                        this.divFD.Visible = false;
                        this.divFinalOrder.Visible = true;
                        this.liStep1.Attributes["class"] = "stepper-done";
                        this.liStep2.Attributes["class"] = "stepper-done";
                        this.liStep3.Attributes["class"] = "stepper-done";
                        this.liStep4.Attributes["class"] = "stepper-done";
                        this.liStep5.Attributes["class"] = "stepper-todo";
                    }
                }
                if (isFilechecked.Equals(0))
                {
                    this.divPF.Visible = false;
                    this.cEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iC.Attributes["class"] = "icon fa fa-warning";
                    this.spnC.InnerText = " Warning!";
                    this.lblCaution1.Text = " Select Log File(s) for Promotion";
                    this.cEnclosure.Visible = true;
                    this.divOPList.Visible = false;
                    this.divPadList.Visible = false;
                    this.divWellList.Visible = false;
                    this.divFD.Visible = true;
                    this.divFinalOrder.Visible = false;
                    this.liStep1.Attributes["class"] = "stepper-done";
                    this.liStep2.Attributes["class"] = "stepper-done";
                    this.liStep3.Attributes["class"] = "stepper-todo";
                    this.liStep4.Attributes["class"] = "stepper-todo";
                    this.liStep5.Attributes["class"] = "stepper-todo";
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProceedFinal_Click(object sender, EventArgs e)
        {
            try
            {
                String getAddress, deliveryAddress, filesToPrint;
                Int32 operatorDomainID = Convert.ToInt32(this.gvwOperatorList.SelectedValue);
                Int32 addressID = -99;
                Int32 isValidAddress = 0;
                deliveryAddress = filesToPrint = "";
                reviewOrder += "Order Detail:" + Environment.NewLine;
                reviewOrder += "Files to Print:" + Environment.NewLine;
                foreach (GridViewRow row in grdFileExplorer.Rows)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("chkRow") as CheckBox);

                    if (chkRow.Checked)
                    {
                        string filename = row.Cells[1].Text;
                        filesToPrint += filename + " |" + Environment.NewLine;
                        reviewOrder += filename + Environment.NewLine;
                        this.divPF.Visible = true;
                    }
                }
                HttpContext.Current.Session["filesToPrint"] = filesToPrint;
                reviewOrder += Environment.NewLine;
                reviewOrder += "Address:" + Environment.NewLine;
                foreach (GridViewRow row in this.grdAddress.Rows)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("chkRow") as CheckBox);
                    if (chkRow.Checked)
                    {
                        addressID = int.Parse(row.Cells[1].Text);
                        getAddress = CLNT.getAddressLineWithPhone(operatorDomainID, addressID);
                        reviewOrder += getAddress + Environment.NewLine;
                        deliveryAddress += getAddress + " |" + Environment.NewLine;
                        if ((Convert.ToInt32(this.Session["printcount"]).Equals(0)) && (Convert.ToInt32(this.Session["dvdcount"]).Equals(0)))
                        {
                            isValidAddress = 0;
                        }
                        else
                        {
                            isValidAddress = 1;
                        }
                    }
                }
                HttpContext.Current.Session["deliveryAddress"] = deliveryAddress;
                reviewOrder += Environment.NewLine;
                reviewOrder += "Number of Prints Ordered: " + Convert.ToString(this.Session["printcount"]) + Environment.NewLine;
                //reviewOrder += Environment.NewLine;
                reviewOrder += "Number of DVD Copies: " + Convert.ToString(this.Session["dvdcount"]) + Environment.NewLine;
                if (isValidAddress == 1)
                {
                    this.txtbxComments.Text = reviewOrder;
                    this.txtbxComments.Enabled = true;
                    this.divFO.Visible = true;
                }
                else
                {
                    //this.lblProceedAlert.Text = "";
                }
                this.btnProceedFinal.Visible = false;
                this.btnProceedWithPrint.Enabled = true;
                this.btnProceedWithPrint.CssClass = "btn btn-success text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlPrintCount_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(this.ddlPrintCount.SelectedValue).Equals(0) || Convert.ToInt32(this.ddlPrintCount.SelectedValue).Equals(-1))
                {
                    HttpContext.Current.Session["printcount"] = 0;
                }
                else
                {
                    Int32 printcount = Convert.ToInt32(this.ddlPrintCount.SelectedValue);
                    HttpContext.Current.Session["printcount"] = printcount;
                }

                foreach (GridViewRow row in grdAddress.Rows)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("chkRow") as CheckBox);
                    if (chkRow.Checked)
                    {
                        chkRow.Checked = false;
                    }
                }
                this.divFO.Visible = false;
                this.btnProceedFinal.Visible = true;
                this.txtbxComments.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlPrintCDs_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(this.ddlPrintCDs.SelectedValue).Equals(0) || Convert.ToInt32(this.ddlPrintCDs.SelectedValue).Equals(-1))
                {
                    HttpContext.Current.Session["dvdcount"] = 0;
                }
                else
                {
                    Int32 dvdcount = Convert.ToInt32(this.ddlPrintCDs.SelectedValue);
                    HttpContext.Current.Session["dvdcount"] = dvdcount;
                }
                foreach (GridViewRow row in grdAddress.Rows)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("chkRow") as CheckBox);
                    if (chkRow.Checked)
                    {
                        chkRow.Checked = false;
                    }
                }
                this.divFO.Visible = false;
                this.btnProceedFinal.Visible = true;
                this.txtbxComments.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void clearDropDownPrintList()
        {
            try
            {
                this.ddlPrintCount.SelectedIndex = -1;
                this.ddlPrintCDs.SelectedIndex = -1;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProceedWithPrint_Click(object sender, EventArgs e)
        {
            try
            {
                String user, uploadCompletePath, clientDomainID, clientDomain, printLogsOrderDirectory, filetoSaveWithFullPath, filesToPrint, deliveryAddress, dvdPrintCount, logsPrintCount;
                Int32 welID = -99;
                // If Number of Prints orders is Zero
                if (Convert.ToInt32(this.ddlPrintCount.SelectedValue).Equals(0) || Convert.ToInt32(this.ddlPrintCount.SelectedValue).Equals(-1))
                {
                    HttpContext.Current.Session["printcount"] = 0;
                }
                // if Number of DVD orders is Zero
                if (Convert.ToInt32(this.ddlPrintCDs.SelectedValue).Equals(0) || Convert.ToInt32(this.ddlPrintCDs.SelectedValue).Equals(-1))
                {
                    HttpContext.Current.Session["dvdcount"] = 0;
                }
                dvdPrintCount = Convert.ToString(HttpContext.Current.Session["dvdcount"]);
                logsPrintCount = Convert.ToString(HttpContext.Current.Session["printcount"]);

                uploadCompletePath = Convert.ToString(HttpContext.Current.Session["uploadCompletePath"]);
                clientDomainID = Convert.ToString(HttpContext.Current.Session["clntID"]);
                welID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                clientDomain = Convert.ToString(HttpContext.Current.Session["clientDomain"]);
                user = Convert.ToString(LoginName);
                Int32 accountOrder = -99, delFlag = 0;
                try
                {
                    using (ZipFile zip = new ZipFile())
                    {
                        zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                        zip.AddDirectoryByName("Files");
                        logger.Info(" ######################################################## ");
                        foreach (GridViewRow row in grdFileExplorer.Rows)
                        {
                            CheckBox chkRow = (row.Cells[0].FindControl("chkRow") as CheckBox);
                            if (chkRow.Checked)
                            {
                                String filename = row.Cells[1].Text;
                                String filePath = Path.Combine(uploadCompletePath, filename);
                                zip.AddFile(filePath, "Files");
                                logger.Info("User: " + username + " Print Order. Files to Print: " + filename);
                            }
                        }

                        String zipName = String.Format("Order_" + clientDomain + "_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
                        String OrderName = String.Format("Order_" + clientDomain + "_{0}", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));

                        printLogsOrderDirectory = System.Web.Configuration.WebConfigurationManager.AppSettings["LogPrintPath"].ToString();
                        //printLogsOrderDirectory = System.Web.Configuration.WebConfigurationManager.AppSettings["LogsPath"].ToString();
                        // Create Upload Directory if it doesnt Exist
                        DirectoryInfo di = Directory.CreateDirectory(printLogsOrderDirectory);

                        //zip.Save("C:\\Temp\\"+ zipName);
                        filetoSaveWithFullPath = printLogsOrderDirectory + "\\" + zipName;
                        zip.Save(filetoSaveWithFullPath);

                        try
                        {
                            filesToPrint = Convert.ToString(HttpContext.Current.Session["filesToPrint"]);
                            deliveryAddress = Convert.ToString(HttpContext.Current.Session["deliveryAddress"]);

                            LGP Comments = new LGP();
                            Comments.insertPrintOrder(ClientID, welID, OrderName, filetoSaveWithFullPath, filesToPrint, "SUBMITTED", this.txtbxComments.Text, deliveryAddress, user, clientDomain, delFlag);
                            this.orsEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                            this.iORS.Attributes["class"] = "icon fa fa-check-circle";
                            this.spnORS.InnerText = " Success!";
                            this.lblOrderStatus.Text = "Order submitted successfully. Order ID: " + OrderName;
                            this.orsEnclosure.Visible = true;
                            logger.Info("User: " + username + " Print Order. Order Name SUBMITTED: " + OrderName);
                            logger.Info("User: " + username + " Print Order. Order File Name: " + zipName);
                            logger.Info("User: " + username + " Print Order. Order Count. Logs: " + logsPrintCount + ", DvD: " + dvdPrintCount);
                            logger.Info("User: " + username + " Print Order. Order File Path: " + filetoSaveWithFullPath);
                            logger.Info(" ######################################################## ");
                            //accountOrder = ACC.accLogPrint(OrderName, LoginName, clientDomain, Convert.ToInt32(logsPrintCount), Convert.ToInt32(dvdPrintCount));                            
                            HttpContext.Current.Session["filesToPrint"] = null;
                            HttpContext.Current.Session["deliveryAddress"] = null;
                        }
                        catch (Exception ex)
                        {
                            this.orsEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                            this.iORS.Attributes["class"] = "icon fa fa-warning";
                            this.spnORS.InnerText = " Warning!";
                            this.lblOrderStatus.Text = "Unable to submit request Order. Please contact Support. Error: " + ex.Message;
                            this.orsEnclosure.Visible = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.orsEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iORS.Attributes["class"] = "icon fa fa-warning";
                    this.spnORS.InnerText = " Warning!";
                    this.lblOrderStatus.Text = "Unable to submit request Order. Please contact Support. Error: " + ex.Message;
                    this.orsEnclosure.Visible = false;
                }
                this.btnProceedWithPrint.Enabled = false;
                this.btnProceedWithPrint.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnClearAll_Click(object sender, EventArgs e)
        {
            try
            {
                RefreshClientFileExplorerGrid();
                this.cEnclosure.Visible = false;
                this.lblCaution1.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnFileExpClear_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientType);
                this.gvwOperatorList.DataSource = operatorCoTable;
                this.gvwOperatorList.PageIndex = 0;
                this.gvwOperatorList.SelectedIndex = -1;
                this.gvwOperatorList.DataBind();
                this.gvwWellPads.DataSource = null;
                this.gvwWellPads.DataBind();
                this.gvwWellList.DataSource = null;
                this.gvwWellList.DataBind();
                this.divOPList.Visible = true;
                this.divPadList.Visible = false;
                this.divWellList.Visible = false;
                this.divFD.Visible = false;
                this.divFinalOrder.Visible = false;
                this.liStep1.Attributes["class"] = "stepper-todo";
                this.liStep2.Attributes["class"] = "stepper-todo";
                this.liStep3.Attributes["class"] = "stepper-todo";
                this.liStep4.Attributes["class"] = "stepper-todo";
                this.liStep5.Attributes["class"] = "stepper-todo";
                this.testEnclosure.Visible = false;
                this.lblTest.Text = "";
                this.cEnclosure.Visible = false;
                this.lblCaution1.Text = "";
                this.ancHead.InnerHtml = "Operator Company";
                this.btnFileExpClear.Enabled = false;
                this.btnFileExpClear.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }   
    }
}