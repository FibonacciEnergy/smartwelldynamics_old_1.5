﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlReviewLogs.ascx.cs" Inherits="SmartsVer1.Control.LogPrint.ctrlReviewLogs" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<!-- Stylesheet -->
<link href="../../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript for ref lib -->
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<!-- Javascript for Footer -->
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>
<!-- Local Page Style -->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }

    .panelBkg {
        background-color: #C1CFDD;
    }

    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }
</style>
<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">        
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../Viewer/rwrViewer_2A.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li><a href="../../LogPrint/feReviewLogs_2A.aspx"> Logs Management</a></li>
            </ol>
        </div>
    </div>    
</div>
<!-- Page Content -->
<asp:UpdateProgress ID="upgrsRWRProvider" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlRWRProvider">
    <ProgressTemplate>
        <div id="divRWRProviderProgressbar" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle;
            text-align: center; z-index: 100;">
            <asp:Image ID="imgRWRProviderProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<asp:UpdatePanel ID="upnlRWRProvider" runat="server">
    <ContentTemplate>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row bg-white">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-danger">
                        <div class="panel-body">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div id="acrdWellList" class="panel-group">
                                    <div class="panel panel-danger panel-body">
                                        <div id="divWellGages" runat="server">
                                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                <div class="small-box bg-blue">
                                                    <div id="divOperators" class="inner" runat="server">
                                                    </div>
                                                    <div class="icon">
                                                        <i class="fa fa-bank" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                <div class="small-box bg-green">
                                                    <div id="divProviders" class="inner" runat="server">
                                                    </div>
                                                    <div class="icon">
                                                        <i class="fa fa-group" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                <div class="small-box bg-orange">
                                                    <div id="divPads" class="inner" runat="server">
                                                    </div>
                                                    <div class="icon">
                                                        <i class="fa fa-th" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                <div class="small-box bg-red">
                                                    <div id="divWells" class="inner" runat="server">
                                                    </div>
                                                    <div class="icon">
                                                        <i class="fa fa-tint" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-danger panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group" style="width: 100%;">
                                                <asp:LinkButton ID="btnSrvPClear" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnSrvPClear_Click"><i class="fa fa-refresh"> Refresh</i></asp:LinkButton>
                                            </div>
                                            <div class="panel panel-info">
                                                <div class="panel-heading">
                                                    <h5 class="panel-title" style="text-align: left">
                                                        <a id="ancHead" runat="server" class="disabled">Service Provider(s)</a>
                                                    </h5>
                                                </div>
                                                <div id="divSrvPList" runat="server">
                                                    <div class="panel-body">
                                                        <asp:GridView ID="gvwServiceProviderList" runat="server" CssClass="mydatagrid" EmptyDataText="No Service Provider(s) found."
                                                            AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="clntID" OnSelectedIndexChanged="gvwServiceProviderList_SelectedIndexChanged"
                                                            ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwServiceProviderList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                            PageSize="8" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                                            <Columns>
                                                                <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                    <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                </asp:ButtonField>
                                                                <asp:BoundField DataField="clntID" Visible="false" />
                                                                <asp:BoundField DataField="clntName" HeaderText="Service Provider Name" />
                                                                <asp:BoundField DataField="clntRealm" HeaderText="Realm/Domain" />
                                                                <asp:BoundField DataField="pdCount" HeaderText="Well Pad(s) Count">
                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="wlCount" HeaderText="Wells(s)/Lateral(s) Count">
                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                </asp:BoundField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                                <div id="divSPPad" runat="server" visible="false">
                                                    <div class="panel-body">
                                                        <asp:GridView ID="gvwSPPad" runat="server" EmptyDataText="No Well Pad(s) found for selected Operator"
                                                            ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="wpdID"
                                                            AllowPaging="True" CssClass="mydatagrid" OnPageIndexChanging="gvwSPPad_PageIndexChanging" OnSelectedIndexChanged="gvwSPPad_SelectedIndexChanged"
                                                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                            RowStyle-CssClass="rows">
                                                            <Columns>
                                                                <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                    <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                </asp:ButtonField>
                                                                <asp:BoundField DataField="wpdID" HeaderText="wpd" ReadOnly="True" Visible="false" />
                                                                <asp:BoundField DataField="welPadName" HeaderText="Well Pad" />
                                                                <asp:BoundField DataField="field" HeaderText="O&G Field" />
                                                                <asp:BoundField DataField="county" HeaderText="County/District/Division/Municipality" />
                                                                <asp:BoundField DataField="state" HeaderText="State/Province" />
                                                                <asp:BoundField DataField="welCount" HeaderText="Global Well/Lateral Count">
                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                </asp:BoundField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                                <div id="divSPWell" runat="server" visible="false">
                                                    <div class="panel-body">
                                                        <asp:GridView ID="gvwSPWell" runat="server" DataKeyNames="welID" CssClass="mydatagrid" AllowPaging="True" AllowSorting="True"
                                                            AutoGenerateColumns="False" EmptyDataText="No Well(s)/Lateral(s) found for selected Operator Company."
                                                            OnPageIndexChanging="gvwSPWell_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header"
                                                            PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow" OnSelectedIndexChanged="gvwSPWell_SelectedIndexChanged">
                                                            <Columns>
                                                                <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                    <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                </asp:ButtonField>
                                                                <asp:BoundField DataField="welID" HeaderText="welID" Visible="False" />
                                                                <asp:BoundField DataField="welName" HeaderText="Well Name" />
                                                                <asp:BoundField DataField="field" HeaderText="Field" />
                                                                <asp:BoundField DataField="county" HeaderText="County / District / Municipality" />
                                                                <asp:BoundField DataField="state" HeaderText="State / Province" />
                                                                <asp:BoundField DataField="spud" HeaderText="Spud Date" DataFormatString="{0:MMM d , yyyy}" />
                                                                <asp:BoundField DataField="wstID" HeaderText="SMARTs Status" />
                                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                                                            </Columns>
                                                        </asp:GridView>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div runat="server" id="div2Enclosure" visible="false">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                <h4><i runat="server" id="i2"><span runat="server" id="spn2"></span></i></h4>
                                                                <asp:Label ID="lblTest2" runat="server" CssClass="lblContact" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divLogs" runat="server" visible="false">
                                                    <div class="panel-body">
                                                        <asp:GridView ID="grdFileExplorer" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="15"
                                                            CssClass="mydatagrid" PagerStyle-CssClass="pager" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                            OnPageIndexChanging="grdFileExplorer_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Select">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkRow" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="File" HeaderText="Log Files" ItemStyle-Width="65%" ItemStyle-HorizontalAlign="Left" />
                                                                <asp:BoundField DataField="CreationTime" HeaderText="Creation Time" ItemStyle-Width="25%" />
                                                                <asp:BoundField DataField="Size" HeaderText="Size in KB" ItemStyle-Width="15%" />
                                                                <asp:BoundField DataField="Type" HeaderText="Extension" ItemStyle-Width="15%" />
                                                            </Columns>
                                                        </asp:GridView>
                                                        <div class="input-group" style="width: 100%;">
                                                            <asp:LinkButton ID="btnDownloadFile" runat="server" CssClass="btn btn-success text-center" OnClick="btnDownloadFile_Click"><i class="fa fa-download"> Download Selected File(s)</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnUncheckAll" runat="server" CssClass="btn btn-warning text-center" OnClick="btnUncheckAll_Click"><i class="fa fa-refresh"> UnSelect File(s)</i></asp:LinkButton>
                                                        </div>
                                                        <div>&nbsp;</div>
                                                        <asp:Label runat="server" ID="lblComments" CssClass="label label-info" Font-Size="Large" Text="Comments" />
                                                        <asp:TextBox runat="server" ID="txtbxComments" TextMode="MultiLine" Enabled="false" CssClass="form-control with-border" placeholder="Enter your comments here" />
                                                        <asp:RegularExpressionValidator ID="RegValidatortxtbxComments" runat="server"
                                                            ControlToValidate="txtbxComments" ErrorMessage="Comments limit of 2500 charactors exceeded" ValidationExpression="^[\s\S]{0,2500}$"
                                                            Font-Size="Medium" ForeColor="Maroon" />
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div runat="server" id="updEnclosure" visible="false">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                <h4><i runat="server" id="iUPD"><span runat="server" id="spnUPD"></span></i></h4>
                                                                <asp:Label runat="server" ID="lblCommentsUpdate" Text="" />
                                                            </div>
                                                            <div runat="server" id="timEnclosure" visible="false">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                <h4><i runat="server" id="iTime"><span runat="server" id="spnTime"></span></i></h4>
                                                                <asp:Label runat="server" ID="lblCommentsTime" Text="" />
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="input-group" style="width: 100%;">
                                                                <asp:LinkButton ID="btnEditComments" runat="server" CssClass="btn btn-primary text-center" OnClick="btnEditComments_Click"><i class="fa fa-plus"> Add/Edit Comment(s)</i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnSaveComments" runat="server" CssClass="btn btn-success text-center" OnClick="btnSaveComments_Click"><i class="fa fa-save"> Save Comments</i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="gvwSPWell" />
        <asp:PostBackTrigger ControlID="btnDownloadFile" />
        <asp:PostBackTrigger ControlID="btnUncheckAll" />
    </Triggers>
</asp:UpdatePanel>                
                