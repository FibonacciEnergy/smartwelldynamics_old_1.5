﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlProcessOrder_2A.ascx.cs" Inherits="SmartsVer1.Control.LogPrint.ctrlProcessOrder_2A" %>

<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>

<link href="../../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />

<!-- Scripts -->
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>

<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box box-primary">
            <div class="box box-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="small-box bg-green">
                            <div id="divGuagetotalOrders" class="inner" runat="server"></div>
                            <div class="icon"><i class="fa fa-first-order"></i></div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="small-box bg-orange-active">
                            <div id="divGuagesubmittedOrders" class="inner" runat="server"></div>
                            <div class="icon"><i class="fa fa-list"></i></div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="small-box bg-light-blue">
                            <div id="divGuageinprogressOrders" class="inner" runat="server"></div>
                            <div class="icon"><i class="fa fa-arrow-right"></i></div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="small-box bg-blue">
                            <div id="divGuagecompleteOrders" class="inner" runat="server"></div>
                            <div class="icon"><i class="fa fa-check-circle"></i></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <asp:UpdateProgress ID="upgrsPO" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlProcessOrder">
            <ProgressTemplate>
                <div id="divPOProcessing" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center; z-index: 100;">
                    <asp:Image ID="imgPOProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="upnlProcessOrder" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="box box-primary">
                    <div class="box-header">
                        <h1 class="box-title">SMARTs Order Summary</h1>
                    </div>
                    <div class="box box-body">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <asp:GridView ID="grdClientOrder" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="20"
                                CssClass="mydatagrid" PagerStyle-CssClass="pager" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                EmptyDataText="No Address avaiable" ShowHeaderWhenEmpty="True" OnSelectedIndexChanged="grdClientOrder_SelectedIndexChanged"
                                OnPageIndexChanging="grdClientOrder_PageIndexChanging" SelectedRowStyle-CssClass="selectedrow">
                                <Columns>
                                    <asp:ButtonField CommandName="Select" Text="Process Order" ButtonType="Button">
                                        <ControlStyle CssClass="btn btn-primary text-center"></ControlStyle>
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="clientID" HeaderText="clientID" />
                                    <asp:BoundField DataField="wellID" HeaderText="WellID" />
                                    <asp:BoundField DataField="orderName" HeaderText="Print Order Name" />
                                    <asp:BoundField DataField="fileSystemPath" HeaderText="File Path" />
                                    <asp:BoundField DataField="filesToPrint" HeaderText="Files to Print" />
                                    <asp:BoundField DataField="printStatus" HeaderText="Order Status" />
                                    <asp:BoundField DataField="printComments" HeaderText="Comments" />
                                    <asp:BoundField DataField="deliveryAddress" HeaderText="Address to deliver" />
                                    <asp:BoundField DataField="orderBy" HeaderText="Ordered by User" />
                                    <asp:BoundField DataField="clientDomain" HeaderText="Client Domain" />
                                    <asp:BoundField DataField="datetime" HeaderText="Order Date/Time" SortExpression="datetime" ReadOnly="True" DataFormatString="{0:MMM d , yyyy hh:mm tt}" />
                                </Columns>
                            </asp:GridView>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <asp:TextBox runat="server" ID="txtbxComments" TextMode="MultiLine" CssClass="form-control" Height="200px" MaxLength="2500"
                                Visible="false" />
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="input-group" style="width: 100%;">
                                <asp:Button ID="btnDownloadFile" runat="server" Text="Download Order for Processing" CssClass="btn btn-info text-center"
                                    Width="15%" Visible="false" OnClick="btnDownloadFile_Click" />
                                <asp:Button ID="btnDeleteOrder" runat="server" Text="Delete Order" CssClass="btn btn-danger text-center" Width="15%" Visible="false"
                                    OnClick="btnDeleteOrder_Click" />
                                <asp:Button ID="btnDone" runat="server" Text="Done" CssClass="btn btn-primary text-center" Width="15%" Visible="false" OnClick="btnDone_Click" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div id="divOrderUpdate" runat="server" class="box-body" visible="false">
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <asp:Label ID="lblUpdate" runat="server" CssClass="label label-default text-right" Text="Update Order Status" Font-Bold="true"
                                        Width="100%" Font-Size="Large" />
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="True" AppendDataBoundItems="True" Enabled="true" CssClass="form-control">
                                        <asp:ListItem Selected="True" Value="0"> ---  Select New Status --- </asp:ListItem>
                                        <asp:ListItem Value="1">SUBMITTED</asp:ListItem>
                                        <asp:ListItem Value="2">IN PROGRESS</asp:ListItem>
                                        <asp:ListItem Value="3">COMPLETE</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <asp:Button ID="btnUpdateStatus" runat="server" Text="Update Order Status " CssClass="btn btn-success text-center" Width="30%"
                                        OnClick="btnUpdateStatus_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnDownloadFile" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</div>