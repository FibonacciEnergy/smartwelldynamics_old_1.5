﻿using System;
using System.Data;
using System.Net.Mail;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using LGP = SmartsVer1.Helpers.LogPrint.LogComments;

namespace SmartsVer1.Control.LogPrint
{
    public partial class ctrlOrderTracker : System.Web.UI.UserControl
    {
        String LoginName, username, clientDomain, clientDomainID, operatorDomain, GetClientDBString, reviewOrder;
        Int32 ClientID = -99;
        Int32 numPrint, numDVD = 0;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {

                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                clientDomain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(clientDomain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                this.txtbxComments.Text = "";
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");
                GenerateOrderGraphs();
                RefreshClientOrderGird();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }

        }


        protected void RefreshClientOrderGird()
        {
            try
            {
                System.Data.DataTable dt = new DataTable();
                dt = LGP.getClientsOrders(ClientID);

                this.grdClientOrder.DataSource = dt;
                this.grdClientOrder.PageIndex = 0;
                this.grdClientOrder.SelectedIndex = -1;
                this.grdClientOrder.DataBind();
                this.grdClientOrder.Columns[1].Visible = false;
                this.grdClientOrder.Columns[2].Visible = false;
                this.grdClientOrder.Columns[4].Visible = false;
                this.grdClientOrder.Columns[5].Visible = false;
                this.grdClientOrder.Columns[7].Visible = false;
                this.grdClientOrder.Columns[8].Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }


        protected void GenerateOrderGraphs()
        {
            try
            {
                Int32 totalcount, submitted, inprogress, complete;
                totalcount = submitted = inprogress = complete = 0;

                System.Data.DataTable dt = new DataTable();
                dt = LGP.getOrderCounts(ClientID);

                foreach (DataRow row in dt.Rows)
                {
                    totalcount = Convert.ToInt32(row[0].ToString());
                    submitted = Convert.ToInt32(row[1].ToString());
                    inprogress = Convert.ToInt32(row[2].ToString());
                    complete = Convert.ToInt32(row[3].ToString());
                }

                this.divTotalOrders.InnerHtml = "<h3>" + totalcount.ToString() + "</h3><p>Total Print Orders</p>";
                this.divSubmittedOrders.InnerHtml = "<h3>" + submitted.ToString() + "</h3><p>Orders Submitted</p>";
                this.divInprogressOrders.InnerHtml = "<h3>" + inprogress.ToString() + "</h3><p>Orders InProgress</p>";
                this.divCompleteOrders.InnerHtml = "<h3>" + complete.ToString() + "</h3><p>Completed Orders</p>";


            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void grdClientOrder_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow dRow = this.grdClientOrder.SelectedRow;
                String order = Convert.ToString(Server.HtmlDecode(dRow.Cells[3].Text));
                String orderDetails = LGP.getOrderComments(order);
                this.ancHead.InnerHtml = "Order Name : <span style='text-decoration: underline; font-weight: bold'>" + order + "</span>";
                this.txtbxComments.Text = orderDetails;
                this.txtbxComments.Visible = true;
                this.divOrderList.Visible = false;
                this.divOrderDetails.Visible = true;
                this.btnClear.Enabled = true;
                this.btnClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void grdClientOrder_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable dt = new DataTable();
                dt = LGP.getClientsOrders(ClientID);
                this.grdClientOrder.DataSource = dt;
                this.grdClientOrder.PageIndex = e.NewPageIndex;
                this.grdClientOrder.SelectedIndex = -1;
                this.grdClientOrder.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable dt = new DataTable();
                dt = LGP.getClientsOrders(ClientID);

                this.grdClientOrder.DataSource = dt;
                this.grdClientOrder.PageIndex = 0;
                this.grdClientOrder.SelectedIndex = -1;
                this.grdClientOrder.DataBind();
                this.grdClientOrder.Columns[1].Visible = false;
                this.grdClientOrder.Columns[2].Visible = false;
                this.grdClientOrder.Columns[4].Visible = false;
                this.grdClientOrder.Columns[5].Visible = false;
                this.grdClientOrder.Columns[7].Visible = false;
                this.grdClientOrder.Columns[8].Visible = false;
                this.txtbxComments.Text = "";
                this.btnClear.Enabled = false;
                this.btnClear.CssClass = "btn btn-default text-center";
                this.divOrderList.Visible = true;
                this.divOrderDetails.Visible = false;
                this.ancHead.InnerHtml = "Log Print Orders List";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}