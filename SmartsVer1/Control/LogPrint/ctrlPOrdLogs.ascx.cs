﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Net;
using System.Net.Mail;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Threading;
using Microsoft.SqlServer.Types;
using SmartsVer1.Helpers.QCHelpers;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using ACC = SmartsVer1.Helpers.QCHelpers.AccountingQC;
using LGP = SmartsVer1.Helpers.LogPrint.LogComments;
using Ionic.Zip;
using SmartsVer1.Helpers.LogPrint;
using log4net;

namespace SmartsVer1.Control.LogPrint
{
    public partial class ctrlPOrdLogs : System.Web.UI.UserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ctrlPOrdLogs));
        String LoginName, username, domain, reviewOrder, GetClientDBString;
        const Int32 ClientType = 4;
        Int32 ClientID = -99;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                System.Data.DataTable opTable = CLNT.getOperatorCoTable(ClientType);
                this.gvwOperatorList.DataSource = opTable;
                this.gvwOperatorList.DataBind();

                HttpContext.Current.Session["clientDomain"] = domain;
                HttpContext.Current.Session["user"] = username;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");
                this.GenerateOrderGraphs();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void GenerateOrderGraphs()
        {
            try
            {
                Int32 totalcount = 0, submitted = 0, inprogress = 0, complete = 0;
                System.Data.DataTable dt = new DataTable();
                dt = LGP.getOrderCounts(ClientID);
                foreach (DataRow row in dt.Rows)
                {
                    totalcount = Convert.ToInt32(row[0].ToString());
                    submitted = Convert.ToInt32(row[1].ToString());
                    inprogress = Convert.ToInt32(row[2].ToString());
                    complete = Convert.ToInt32(row[3].ToString());
                }
                this.divTotalOrders.InnerHtml = "<h3>" + totalcount.ToString() + "</h3><p>Total Print Orders</p>";
                this.divSubmittedOrders.InnerHtml = "<h3>" + submitted.ToString() + "</h3><p>Orders Submitted</p>";
                this.divInprogressOrders.InnerHtml = "<h3>" + inprogress.ToString() + "</h3><p>Orders InProgress</p>";
                this.divCompleteOrders.InnerHtml = "<h3>" + complete.ToString() + "</h3><p>Completed Orders</p>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnMetaClear_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable opTable = CLNT.getOperatorCoTable(ClientType);
                this.gvwOperatorList.DataSource = opTable;
                this.gvwOperatorList.SelectedIndex = -1;
                this.gvwOperatorList.DataBind();
                this.gvwWellList.DataSource = null;
                this.gvwWellList.DataBind();
                this.ancOperator.InnerHtml = "Select Operator";
                this.ancWellList.InnerHtml = "Select Well";
                this.btnMetaClear.Visible = false;
                this.btnPromoteFiles.Visible = false;
                this.testEnclosure.Visible = false;
                this.gvwWellList.DataSource = null;
                this.gvwWellList.DataBind();
                this.divOrder.Visible = false;
                this.divOperatorClass.Attributes["class"] = "panel-collapse collapse in";
                this.divWellClass.Attributes["class"] = "panel-collapse collapse";
                this.divLogFiles.Attributes["class"] = "panel-collapse collapse";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwOperatorList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable opTable = CLNT.getOperatorCoTable(ClientType);
                this.gvwOperatorList.DataSource = opTable;
                this.gvwOperatorList.PageIndex = e.NewPageIndex;
                this.gvwOperatorList.SelectedIndex = -1;
                this.gvwOperatorList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwOperatorList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 opID = Convert.ToInt32(this.gvwOperatorList.SelectedValue);
                System.Data.DataTable wlTable = AST.getWellTable(opID, GetClientDBString);
                this.gvwWellList.DataSource = wlTable;                
                this.gvwWellList.DataBind();
                this.gvwWellList.SelectedIndex = -1;
                this.btnMetaClear.Visible = true;
                GridViewRow dRow = this.gvwOperatorList.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.divOperatorClass.Attributes["class"] = "panel-collapse collapse";
                this.divWellClass.Attributes["class"] = "panel-collapse collapse in";
                this.divLogFiles.Attributes["class"] = "panel-collapse collapse";
                this.ancOperator.InnerHtml = "Select Operator : <span style='text-decoration: underline; font-weight: bold'>" + oName + "</span>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 opID = Convert.ToInt32(this.gvwOperatorList.SelectedValue);
                System.Data.DataTable wlTable = AST.getWellTable(opID, GetClientDBString);
                this.gvwWellList.DataSource = wlTable;
                this.gvwWellList.PageIndex = e.NewPageIndex;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                String uploadLogsDirectory, uploadCompletePath, wellName;
                Int32 wellID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                Int32 oprID = Convert.ToInt32(this.gvwOperatorList.SelectedValue);
                this.testEnclosure.Visible = false;
                GridViewRow dRow = this.gvwWellList.SelectedRow;
                String wName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.divWellClass.Attributes["class"] = "panel-collapse collapse";
                this.divLogFiles.Attributes["class"] = "panel-collapse collapse in";
                this.ancWellList.InnerHtml = "Select Well : <span style='text-decoration: underline; font-weight: bold'>" + wName + "</span>";
                wellName = Convert.ToString(this.gvwWellList.SelectedValue);
                uploadLogsDirectory = System.Web.Configuration.WebConfigurationManager.AppSettings["LogsPath"].ToString();
                uploadCompletePath = String.Format("{0}\\{1}\\{2}\\{3}\\", uploadLogsDirectory, oprID, ClientID, wellID);
                if (!Directory.Exists(uploadCompletePath))
                {
                    this.gvwFileExplorer.DataSource = null;
                    this.gvwFileExplorer.DataBind();
                    this.testEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iTest.Attributes["class"] = "icon fa fa-warning";
                    this.spnTest.InnerText = " Warning!";
                    this.lblTest.Text = "Note: Logs for the Well does not exist. Use 'Upload Well Logs' to Upload the well logs.";
                    this.testEnclosure.Visible = true;
                    this.btnPromoteFiles.Visible = false;
                    this.btnMetaClear.Visible = true;
                }
                else
                {
                    if (Directory.GetFiles(uploadCompletePath).Length > 0)
                    {
                        HttpContext.Current.Session["uploadCompletePath"] = uploadCompletePath;
                        DataTable dt = new DataTable();
                        dt.Columns.Add("File", typeof(String));
                        dt.Columns.Add("CreationTime", typeof(String));
                        dt.Columns.Add("Size", typeof(String));
                        dt.Columns.Add("Type", typeof(String));
                        foreach (String strFile in Directory.GetFiles(uploadCompletePath))
                        {
                            FileInfo fi = new FileInfo(strFile);
                            String lengthKB = GetFileSizeInKB(strFile);
                            dt.Rows.Add(fi.Name, fi.CreationTime, lengthKB, fi.Extension);
                        }
                        this.gvwFileExplorer.DataSource = dt;
                        this.gvwFileExplorer.DataBind();
                        this.btnPromoteFiles.Visible = true;
                    }
                    else
                    {
                        this.gvwFileExplorer.DataSource = null;
                        this.gvwFileExplorer.DataBind();
                        this.testEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iTest.Attributes["class"] = "icon fa fa-warning";
                        this.spnTest.InnerText = " Warning!";
                        this.lblTest.Text = "Note: Logs for the Well does not exist. Use 'Upload Well Logs' to Upload the well logs.";
                        this.testEnclosure.Visible = true;
                        HttpContext.Current.Session["uploadCompletePath"] = uploadCompletePath;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public String GetFileSizeInKB(String FileName)
        {
            try
            {
                FileInfo fs = new FileInfo(FileName);
                long filesize = fs.Length / 1024;
                return System.Convert.ToString(filesize);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }



        protected void gvwAddress_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 oprID = Convert.ToInt32(this.gvwOperatorList.SelectedValue);
                System.Data.DataTable adrTable = CLNT.getAddressTable(oprID);
                this.gvwAddress.DataSource = adrTable;
                this.gvwAddress.PageIndex = e.NewPageIndex;
                this.gvwAddress.SelectedIndex = -1;
                this.gvwAddress.DataBind();
            }
            catch(Exception ex)
            {throw new System.Exception(ex.ToString());}
        }

        protected void btnProceedFinal_Click(object sender, EventArgs e)
        {
            try
            {
                String user, uploadCompletePath, login_name, clientDomain, printLogsOrderDirectory, filetoSaveWithFullPath, filesToPrint, deliveryAddress, dvdPrintCount, logsPrintCount;
                Int32 welID = -99;
                // If Number of Prints orders is Zero

                    HttpContext.Current.Session["printcount"] = Convert.ToInt32(this.txtPrintCopy.Text);
                    HttpContext.Current.Session["dvdcount"] = Convert.ToInt32(this.txtDVDCopy.Text);
                
                dvdPrintCount = Convert.ToString(this.Session["dvdcount"]);
                logsPrintCount = Convert.ToString(this.Session["printcount"]);

                uploadCompletePath = Convert.ToString(this.Session["uploadCompletePath"]);
                //clientDomainID = Convert.ToString(this.Session["clntID"]);
                welID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                clientDomain = Convert.ToString(this.Session["clientDomain"]);
                user = Convert.ToString(this.Session["user"]);
                login_name = user+ "@"+ clientDomain;
                Int32 delFlag = 0;
                try
                {
                    using (ZipFile zip = new ZipFile())
                    {
                        zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                        zip.AddDirectoryByName("Files");
                        logger.Info(" ######################################################## ");
                        foreach (GridViewRow row in gvwFileExplorer.Rows)
                        {
                            CheckBox chkRow = (row.Cells[0].FindControl("chkRow") as CheckBox);
                            if (chkRow.Checked)
                            {
                                String filename = row.Cells[1].Text;
                                String filePath = Path.Combine(uploadCompletePath, filename);
                                zip.AddFile(filePath, "Files");
                                logger.Info("User: " + login_name + " Print Order. Files to Print: " + filename);
                            }
                        }

                        String zipName = String.Format("Order_" + clientDomain + "_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
                        String OrderName = String.Format("Order_" + clientDomain + "_{0}", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));

                        printLogsOrderDirectory = System.Web.Configuration.WebConfigurationManager.AppSettings["LogPrintPath"].ToString();
                        //printLogsOrderDirectory = System.Web.Configuration.WebConfigurationManager.AppSettings["LogsPath"].ToString();
                        // Create Upload Directory if it doesnt Exist
                        DirectoryInfo di = Directory.CreateDirectory(printLogsOrderDirectory);

                        //zip.Save("C:\\Temp\\"+ zipName);
                        filetoSaveWithFullPath = printLogsOrderDirectory + "\\" + zipName;
                        zip.Save(filetoSaveWithFullPath);

                        try
                        {
                            filesToPrint = Convert.ToString(this.Session["filesToPrint"]);
                            deliveryAddress = Convert.ToString(this.Session["deliveryAddress"]);

                            LogComments Comments = new LogComments();
                            Comments.insertPrintOrder(ClientID, welID, OrderName, filetoSaveWithFullPath, filesToPrint, "SUBMITTED", this.txtbxComments.Text, deliveryAddress, login_name, clientDomain, delFlag);
                            this.orsEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                            this.iORS.Attributes["class"] = "icon fa fa-check-circle";
                            this.spnORS.InnerText = " Success!";
                            this.lblOrderStatus.Text = "Order submitted successfully. Order ID: " + OrderName;
                            this.orsEnclosure.Visible = true;
                            logger.Info("User: " + login_name + " Print Order. Order Name SUBMITTED: " + OrderName);
                            logger.Info("User: " + login_name + " Print Order. Order File Name: " + zipName);
                            logger.Info("User: " + login_name + " Print Order. Order Count. Logs: " + logsPrintCount + ", DvD: " + dvdPrintCount);
                            logger.Info("User: " + login_name + " Print Order. Order File Path: " + filetoSaveWithFullPath);
                            logger.Info(" ######################################################## ");
                            //accountOrder = ACC.accLogPrint(OrderName, login_name, clientDomain, Int32.Parse(logsPrintCount), Int32.Parse(dvdPrintCount));
                            ACC.accLogPrint(OrderName, login_name, clientDomain, Int32.Parse(logsPrintCount), Int32.Parse(dvdPrintCount));
                            HttpContext.Current.Session["filesToPrint"] = null;
                            HttpContext.Current.Session["deliveryAddress"] = null;
                            this.btnProceedFinal.Visible = false;
                        }
                        catch (Exception ex)
                        {
                            this.orsEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                            this.iORS.Attributes["class"] = "icon fa fa-warning";
                            this.spnORS.InnerText = " Warning!";
                            this.lblOrderStatus.Text = "Unable to submit request Order. Please contact Support. Error: " + ex.Message;
                            this.orsEnclosure.Visible = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.orsEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iORS.Attributes["class"] = "icon fa fa-warning";
                    this.spnORS.InnerText = " Warning!";
                    this.lblOrderStatus.Text = "Unable to submit request Order. Please contact Support. Error: " + ex.Message;
                    this.orsEnclosure.Visible = true;
                }
                this.GenerateOrderGraphs();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }


        protected void btnPromoteFiles_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 isFilechecked = 0;
                Int32 oprID = Convert.ToInt32(this.gvwOperatorList.SelectedValue);
                foreach (GridViewRow row in gvwFileExplorer.Rows)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("chkRow") as CheckBox);
                    if (chkRow.Checked)
                    {
                        //this.divFD.Visible = true;
                        isFilechecked = 1;
                        System.Data.DataTable adrTable = CLNT.getAddressTable(oprID);
                        this.gvwAddress.DataSource = adrTable;
                        this.gvwAddress.SelectedIndex = -1;
                        this.gvwAddress.DataBind();
                        this.divOrder.Visible = true;
                    }
                }
                if (isFilechecked == 0)
                {
                    //this.divtblPF.Visible = false;
                    this.testEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iTest.Attributes["class"] = "icon fa fa-warning";
                    this.spnTest.InnerText = " Warning!";
                    this.lblTest.Text = " Select the Files for Promotion";
                    this.testEnclosure.Visible = true;
                    this.btnMetaClear.Visible = true;
                }
                else
                {
                    this.btnPromoteFiles.Visible = false;
                    this.btnMetaClear.Visible = false;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwAddress_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                String getAddress, deliveryAddress, filesToPrint;
                Int32 operatorDomainID = Convert.ToInt32(this.gvwOperatorList.SelectedValue);
                Int32 addressID = -99, printCount = -99, dvdCount = -99;
                Int32 isValidAddress = 0;
                deliveryAddress = filesToPrint = "";
                reviewOrder += "Order Detail:" + Environment.NewLine;
                reviewOrder += "Files to Print:" + Environment.NewLine;
                printCount = Convert.ToInt32(this.txtPrintCopy.Text);
                dvdCount = Convert.ToInt32(this.txtDVDCopy.Text);
                addressID = Convert.ToInt32(this.gvwAddress.SelectedValue);
                foreach (GridViewRow row in gvwFileExplorer.Rows)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("chkRow") as CheckBox);

                    if (chkRow.Checked)
                    {
                        String filename = row.Cells[1].Text;
                        filesToPrint += filename + " |" + Environment.NewLine;
                        reviewOrder += filename + Environment.NewLine;
                    }
                }
                HttpContext.Current.Session["filesToPrint"] = filesToPrint;
                reviewOrder += Environment.NewLine;
                reviewOrder += "Address:" + Environment.NewLine;
                getAddress = CLNT.getAddressLineWithPhone(operatorDomainID, addressID);
                reviewOrder += getAddress + Environment.NewLine;
                deliveryAddress += getAddress + " |" + Environment.NewLine;
                if ((printCount.Equals(0)) && (dvdCount.Equals(0)))
                {
                    isValidAddress = 0;
                }
                else
                {
                    isValidAddress = 1;
                }
                HttpContext.Current.Session["deliveryAddress"] = deliveryAddress;
                reviewOrder += Environment.NewLine;
                reviewOrder += "Number of Prints Ordered: " + Convert.ToString(printCount) + Environment.NewLine;
                //reviewOrder += Environment.NewLine;
                reviewOrder += "Number of DVD Copies: " + Convert.ToString(dvdCount) + Environment.NewLine;
                if (isValidAddress.Equals(1))
                {
                    this.txtbxComments.Text = reviewOrder;
                    this.txtbxComments.Enabled = true;
                }
                else
                {
                    this.orsEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iORS.Attributes["class"] = "icon fa fa-warning";
                    this.spnORS.InnerText = " Warning!";
                    this.lblOrderStatus.Text = "Unable to create Order. Please try again";
                    this.orsEnclosure.Visible = true;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnClearAll_Click(object sender, EventArgs e)
        {
            try
            {
                this.divOrder.Visible = false;
                this.txtPrintCopy.Text = "";
                this.txtDVDCopy.Text = "";                
                this.gvwAddress.DataSource = null;
                this.gvwAddress.DataBind();
                this.txtbxComments.Text = "";
                this.orsEnclosure.Visible = false;
                this.testEnclosure.Visible = false;
                this.btnMetaClear_Click(null, null);
                this.txtPrintCopy.Text = "1";
                this.txtDVDCopy.Text = "0";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnPromoteLogs_Click(object sender, EventArgs e)
        {
            try
            {
                String getAddress, deliveryAddress, filesToPrint;
                Int32 operatorDomainID = Convert.ToInt32(this.gvwOperatorList.SelectedValue);
                Int32 addressID = -99, printCount = -99, dvdCount = -99;
                Int32 isValidAddress = 0;
                deliveryAddress = filesToPrint = "";
                reviewOrder += "Order Detail:" + Environment.NewLine;
                reviewOrder += "Files to Print:" + Environment.NewLine;


                //addressID = Convert.ToInt32(this.gvwAddress.SelectedValue);
                foreach (GridViewRow row in gvwFileExplorer.Rows)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("chkRow") as CheckBox);

                    if (chkRow.Checked)
                    {
                        String filename = row.Cells[1].Text;
                        filesToPrint += filename + " |" + Environment.NewLine;
                        reviewOrder += filename + Environment.NewLine;
                    }
                }
                HttpContext.Current.Session["filesToPrint"] = filesToPrint;
                reviewOrder += Environment.NewLine;
                reviewOrder += "Address:" + Environment.NewLine;
                foreach (GridViewRow row in this.gvwAddress.Rows)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("chkRow") as CheckBox);
                    if (chkRow.Checked)
                    {
                        addressID = int.Parse(row.Cells[1].Text);
                        getAddress = CLNT.getAddressLineWithPhone(operatorDomainID, addressID);
                        reviewOrder += getAddress + Environment.NewLine;
                        deliveryAddress += getAddress + " |" + Environment.NewLine;

                        if (string.IsNullOrWhiteSpace(this.txtPrintCopy.Text) || string.IsNullOrWhiteSpace(this.txtDVDCopy.Text))
                        {
                            isValidAddress = 0;                        
                        }
                        else
                        {
                            printCount = Convert.ToInt32(this.txtPrintCopy.Text);
                            dvdCount = Convert.ToInt32(this.txtDVDCopy.Text);

                            if ((printCount.Equals(0)) && (dvdCount.Equals(0)))
                            {
                                isValidAddress = 0;
                            }
                            else
                            {
                                isValidAddress = 1;
                            }
                        }
                    }
                }

                //getAddress = CLNT.getAddressLineWithPhone(operatorDomainID, addressID);
                //reviewOrder += getAddress + Environment.NewLine;
                //deliveryAddress += getAddress + " |" + Environment.NewLine;
                //if ((printCount.Equals(0)) && (dvdCount.Equals(0)))
                //{
                //    isValidAddress = 0;
                //}
                //else
                //{
                //    isValidAddress = 1;
                //}
                HttpContext.Current.Session["deliveryAddress"] = deliveryAddress;
                reviewOrder += Environment.NewLine;
                reviewOrder += "Number of Prints Ordered: " + Convert.ToString(printCount) + Environment.NewLine;
                //reviewOrder += Environment.NewLine;
                reviewOrder += "Number of DVD Copies: " + Convert.ToString(dvdCount) + Environment.NewLine;
                if (isValidAddress.Equals(1))
                {
                    this.txtbxComments.Text = reviewOrder;
                    this.txtbxComments.Enabled = true;
                    this.btnProceedFinal.Visible = true;
                    this.btnClearAll.Visible = true;
                    this.btnPromoteLogs.Visible = false;
                }
                else
                {
                    this.orsEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iORS.Attributes["class"] = "icon fa fa-warning";
                    this.spnORS.InnerText = " Warning!";
                    this.lblOrderStatus.Text = "Unable to create Order. Please try again";
                    this.orsEnclosure.Visible = true;
                    this.btnClearAll.Visible = true;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}