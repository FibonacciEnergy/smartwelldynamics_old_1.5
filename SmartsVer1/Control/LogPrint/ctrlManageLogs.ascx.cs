﻿using System;
using System.IO;
using System.Data;
using System.Net.Mail;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartsVer1.Helpers.LogPrint;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using log4net;
using Ionic.Zip;

namespace SmartsVer1.Control.LogPrint
{
    public partial class ctrlManageLogs : System.Web.UI.UserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ctrlManageLogs));
        String LoginName, username, clientDomain, clientDomainID, operatorDomain, GetClientDBString;
        Int32 ClientID = -99;
        const Int32 ClientType = 4;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                clientDomain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(clientDomain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientType);
                this.gvwOperatorList.DataSource = operatorCoTable;
                this.gvwOperatorList.PageIndex = 0;
                this.gvwOperatorList.SelectedIndex = -1;
                this.gvwOperatorList.DataBind();
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwOperatorList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientType);
                this.gvwOperatorList.DataSource = operatorCoTable;
                this.gvwOperatorList.PageIndex = e.NewPageIndex;
                this.gvwOperatorList.SelectedIndex = -1;
                this.gvwOperatorList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwOperatorList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow dRow = this.gvwOperatorList.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.ancHead.InnerHtml = "<strong> " + oName + "</strong>";
                Int32 oprID = Convert.ToInt32(this.gvwOperatorList.SelectedValue);
                System.Data.DataTable padTable = AST.getClientWellPadTableForOperator(oprID, ClientID);
                this.gvwWellPads.DataSource = padTable;
                this.gvwWellPads.PageIndex = 0;
                this.gvwWellPads.SelectedIndex = -1;
                this.gvwWellPads.DataBind();
                this.divOPList.Visible = false;
                this.divPadList.Visible = true;
                this.divWellList.Visible = false;
                this.divUpload.Visible = false;
                this.divFileComments.Visible = false;
                this.liStep1.Attributes["class"] = "stepper-done";
                this.liStep2.Attributes["class"] = "stepper-todo";
                this.liStep3.Attributes["class"] = "stepper-todo";
                this.liStep4.Attributes["class"] = "stepper-todo";
                this.liStep5.Attributes["class"] = "stepper-todo";
                this.btnSelClear.Enabled = true;
                this.btnSelClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellPads_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 oprID = Convert.ToInt32(this.gvwOperatorList.SelectedValue);
                System.Data.DataTable padTable = AST.getClientWellPadTableForOperator(oprID, ClientID);
                this.gvwWellPads.DataSource = padTable;
                this.gvwWellPads.PageIndex = e.NewPageIndex;
                this.gvwWellPads.SelectedIndex = -1;
                this.gvwWellPads.DataBind();
                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellPads_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow oRow = this.gvwOperatorList.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                GridViewRow pRow = this.gvwWellPads.SelectedRow;
                String pName = Convert.ToString(Server.HtmlDecode(pRow.Cells[2].Text));
                this.ancHead.InnerHtml = "<strong> " + oName + " > " + pName + "</strong>";
                Int32 pdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                System.Data.DataTable pdTable = AST.getWellForWellPadTable(pdID, GetClientDBString);
                this.gvwWellList.DataSource = pdTable;
                this.gvwWellList.PageIndex = 0;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();
                this.divOPList.Visible = false;
                this.divPadList.Visible = false;
                this.divWellList.Visible = true;
                this.divUpload.Visible = false;
                this.divFileComments.Visible = false;
                this.liStep1.Attributes["class"] = "stepper-done";
                this.liStep2.Attributes["class"] = "stepper-done";
                this.liStep3.Attributes["class"] = "stepper-todo";
                this.liStep4.Attributes["class"] = "stepper-todo";
                this.liStep5.Attributes["class"] = "stepper-todo";
                this.btnSelClear.Enabled = true;
                this.btnSelClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                String uploadLogsDirectory, uploadCompletePath;
                Int32 operatorID = Convert.ToInt32(this.gvwOperatorList.SelectedValue);
                Int32 padID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                Int32 wellID = Convert.ToInt32(this.gvwWellList.SelectedValue);

                uploadLogsDirectory = System.Web.Configuration.WebConfigurationManager.AppSettings["LogsPath"].ToString();
                uploadCompletePath = (String.Format("{0}\\Operator_{1}\\SrvProvider_{2}\\WellPad_{3}\\Well_{4}\\", uploadLogsDirectory, operatorID, ClientID, padID, wellID));
                if (!Directory.Exists(uploadCompletePath))
                {
                    this.txtEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iTest.Attributes["class"] = "icon fa fa-warning";
                    this.spnTest.InnerText = " Missing Well/Lateral Log(s)!";
                    this.lblTest.Text = "Note: Logs for the Well/Lateral not found. Upload Well/Lateral Logs.";
                    this.txtEnclosure.Visible = true;
                    this.fileEnclosure.Visible = false;
                    this.grdFileExplorer.DataSource = null;
                    this.grdFileExplorer.DataBind();
                    this.delEnclosure.Visible = false;
                    this.divOPList.Visible = false;
                    this.divPadList.Visible = false;
                    this.divWellList.Visible = false;
                    this.divUpload.Visible = true;
                    this.divFileComments.Visible = true;
                    this.btnRefreshFileUpload.Visible = false;
                }
                else
                {
                    if (Directory.GetFiles(uploadCompletePath).Length > 0)                    
                    {
                        GridViewRow oRow = this.gvwOperatorList.SelectedRow;
                        String oName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                        GridViewRow pRow = this.gvwWellPads.SelectedRow;
                        String pName = Convert.ToString(Server.HtmlDecode(pRow.Cells[2].Text));
                        GridViewRow wRow = this.gvwWellList.SelectedRow;
                        String wName = Convert.ToString(Server.HtmlDecode(wRow.Cells[2].Text));
                        this.ancHead.InnerHtml = "<strong> " + oName + " > " + pName + " > " + wName + "</strong>";
                        HttpContext.Current.Session["uploadCompletePath"] = uploadCompletePath;
                        this.RefreshClientFileExplorerGrid();
                        this.FileUploadStatusLabel.Text = "";
                        this.txtbxComments.Text = populateComments();
                        this.txtbxComments.Visible = true;
                        this.divOPList.Visible = false;
                        this.divPadList.Visible = false;
                        this.divWellList.Visible = false;
                        this.divUpload.Visible = true;
                        this.divFileComments.Visible = true;
                        this.liStep1.Attributes["class"] = "stepper-done";
                        this.liStep2.Attributes["class"] = "stepper-done";
                        this.liStep3.Attributes["class"] = "stepper-done";
                        this.liStep4.Attributes["class"] = "stepper-done";
                        this.liStep5.Attributes["class"] = "stepper-done";
                        this.btnRefreshFileUpload.Visible = true;
                        this.lblTest.Text = null;
                        this.txtEnclosure.Visible = false;
                        logger.Info("User: " + username + " Well/Lateral Logs Explored Path: " + uploadCompletePath);

                    }
                    else
                    {
                        this.txtEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iTest.Attributes["class"] = "icon fa fa-warning";
                        this.spnTest.InnerText = " Missing Well/Lateral Log(s)!";
                        this.lblTest.Text = "Note: Logs for the Well/Lateral does not exist";
                        this.txtEnclosure.Visible = true;                        
                        this.FileUploadStatusLabel.Text = "";
                        this.lblDeleteResults.Text = "";
                        this.btnDone.Visible = true;
                        this.btnRefreshFileUpload.Visible = false;
                        this.divOPList.Visible = false;
                        this.divPadList.Visible = false;
                        this.divWellList.Visible = false;
                        this.divUpload.Visible = true;
                        this.divFileComments.Visible = true;
                        this.liStep1.Attributes["class"] = "stepper-done";
                        this.liStep2.Attributes["class"] = "stepper-done";
                        this.liStep3.Attributes["class"] = "stepper-done";
                        this.liStep4.Attributes["class"] = "stepper-todo";
                        this.liStep5.Attributes["class"] = "stepper-todo";
                    }
                }
                this.btnDone.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }


        protected void gvwWellList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 pdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                System.Data.DataTable pdTable = AST.getWellForWellPadTable(pdID, GetClientDBString);
                this.gvwWellList.DataSource = pdTable;
                this.gvwWellList.PageIndex = e.NewPageIndex;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btn_FileUpload_Click(object sender, EventArgs e)
        {
            try
            {
                String fileName, uploadLogsDirectory, uploadCompletePath, operatorDomainID, padID, wellID;
                if (FileUploadControl.HasFile)
                {
                    try
                    {
                        this.txtEnclosure.Visible = false;
                        operatorDomainID = Convert.ToString(this.gvwOperatorList.SelectedValue);
                        padID = Convert.ToString(this.gvwWellPads.SelectedValue);
                        wellID = this.gvwWellList.SelectedValue.ToString();
                        uploadLogsDirectory = System.Web.Configuration.WebConfigurationManager.AppSettings["LogsPath"].ToString();
                        uploadCompletePath = (String.Format("{0}\\Operator_{1}\\SrvProvider_{2}\\WellPad_{3}\\Well_{4}\\", uploadLogsDirectory, operatorDomainID, ClientID, padID, wellID));
                        HttpContext.Current.Session["uploadCompletePath"] = uploadCompletePath;
                        // Create Upload Directory if it doesnt Exist
                        DirectoryInfo di = Directory.CreateDirectory(uploadCompletePath);
                        fileName = Path.Combine(uploadCompletePath, FileUploadControl.FileName);
                        HttpContext.Current.Session["uploadedLogFile"] = fileName;
                        FileUploadControl.SaveAs(fileName);
                        if (File.Exists(fileName))
                        {
                            this.fileEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                            this.iFile.Attributes["class"] = "icon fa fa-check-circle";
                            this.spnFile.InnerText = " Success!";
                            FileUploadStatusLabel.Text = "Successfully uploaded Well/Lateral Log File";
                            this.fileEnclosure.Visible = true;
                            HttpContext.Current.Session["uploadCompletePath"] = uploadCompletePath;
                            FileUploadControl.Dispose();
                            this.btnRefreshFileUpload.Visible = true;
                            this.delEnclosure.Visible = false;
                            this.txtEnclosure.Visible = false;
                            this.RefreshClientFileExplorerGrid();
                            logger.Info("User: " + username + " Well Logs Uploaded : " + fileName);
                        }
                        else
                        {
                            this.fileEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                            this.iFile.Attributes["class"] = "icon fa fa-danger";
                            this.spnFile.InnerText = " Warning!";
                            FileUploadStatusLabel.Text = "Upload status: Failure ... Please try again";
                            this.fileEnclosure.Visible = true;
                            FileUploadControl.Dispose();
                        }
                    }
                    catch (Exception ex)
                    {
                        this.fileEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                        this.iFile.Attributes["class"] = "icon fa fa-danger";
                        this.spnFile.InnerText = " Warning!";
                        FileUploadStatusLabel.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
                        this.fileEnclosure.Visible = true;
                        FileUploadControl.Dispose();
                    }
                }
                else
                {
                    this.fileEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iFile.Attributes["class"] = "icon fa fa-warning";
                    this.spnFile.InnerText = " Warning!";
                    FileUploadStatusLabel.Text = "Upload status: Specify the file to be uploaded ";
                    this.fileEnclosure.Visible = true;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRefreshFileUpload_Click(object sender, EventArgs e)
        {
            try
            {
                String uploadCompletePath = Convert.ToString(HttpContext.Current.Session["uploadCompletePath"]);
                RefreshClientFileExplorerGrid();
                this.FileUploadStatusLabel.Text = "";
                this.txtbxComments.Text = populateComments();
                this.fileEnclosure.Visible = false;

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected string populateComments()
        {
            try
            {
                String output, time;
                output = time = null;
                Int32 wellID = -99, clientDomainID = -99;
                clientDomainID = Convert.ToInt32(this.gvwOperatorList.SelectedValue);
                wellID = Convert.ToInt32(this.gvwWellList.SelectedValue);

                LogComments Comments = new LogComments();
                output = Comments.getLogComments(clientDomainID, ClientID, wellID, out time);
                this.lblCommentsTime.Text = "Comments update Time: " + time;

                return output;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }


        private void RefreshClientFileExplorerGrid()
        {
            try
            {
                String uploadCompletePath = Convert.ToString(HttpContext.Current.Session["uploadCompletePath"]);
                DataTable dt = new DataTable();
                dt.Columns.Add("File", typeof(String));
                dt.Columns.Add("CreationTime", typeof(String));
                dt.Columns.Add("Size", typeof(String));
                dt.Columns.Add("Type", typeof(String));

                foreach (String strFile in Directory.GetFiles(uploadCompletePath))
                {
                    FileInfo fi = new FileInfo(strFile);
                    String lengthKB = GetFileSizeInKB(strFile);
                    dt.Rows.Add(fi.Name, fi.CreationTime, lengthKB, fi.Extension);
                }
                this.grdFileExplorer.DataSource = dt;
                this.grdFileExplorer.PageIndex = 0;
                this.grdFileExplorer.SelectedIndex = -1;
                this.grdFileExplorer.DataBind();
                this.delEnclosure.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public string GetFileSizeInKB(String FileName)
        {
            try
            {
                FileInfo fs = new FileInfo(FileName);
                long filesize = fs.Length / 1024;
                return System.Convert.ToString(filesize);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void grdFileExplorer_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                this.grdFileExplorer.PageIndex = e.NewPageIndex;
                RefreshClientFileExplorerGrid();
                this.grdFileExplorer.SelectedIndex = -1;
                this.grdFileExplorer.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDeleteFile_Click(object sender, EventArgs e)
        {
            try
            {
                this.lblDeleteResults.Text = "";
                String uploadCompletePath = Convert.ToString(HttpContext.Current.Session["uploadCompletePath"]);
                DataTable dt = new DataTable();
                foreach (GridViewRow row in this.grdFileExplorer.Rows)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("chkRow") as CheckBox);
                    if (chkRow.Checked)
                    {
                        String filename = row.Cells[1].Text;
                        try
                        {
                            String destFile = Path.Combine(uploadCompletePath, filename);
                            this.lblDeleteResults.Text += String.Format(" Log Files Deleted {0}<br />", filename);
                            File.Delete(destFile);
                            logger.Info("User: " + username + " Well Logs Deleted: " + destFile);
                            this.lblDeleteResults.Visible = true;
                        }
                        catch (Exception ex)
                        {
                            this.lblDeleteResults.Text = "Log File could not be deleted" + ex.Message;
                            this.lblDeleteResults.Visible = true;

                        }
                    }
                }
                RefreshClientFileExplorerGrid();
                this.lblDeleteResults.Visible = true;
                if (Directory.GetFiles(uploadCompletePath).Length < 1)
                {
                    this.lblDeleteResults.Text = "";
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDownloadFile_Click(object sender, EventArgs e)
        {
            try
            {
                int ifFileSelected = 0;
                String uploadCompletePath = Convert.ToString(HttpContext.Current.Session["uploadCompletePath"]);
                using (ZipFile zip = new ZipFile())
                {
                    zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                    zip.AddDirectoryByName("Files");

                    foreach (GridViewRow row in grdFileExplorer.Rows)
                    {
                        CheckBox chkRow = (row.Cells[0].FindControl("chkRow") as CheckBox);

                        if (chkRow.Checked)
                        {

                            String filename = row.Cells[1].Text;
                            String filePath = Path.Combine(uploadCompletePath, filename);
                            zip.AddFile(filePath, "Files");
                            ifFileSelected = 1;
                        }
                        chkRow.Checked = false;
                    }
                    if (ifFileSelected.Equals(1))
                    {
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BufferOutput = false;
                        String zipName = String.Format("ZipFiles_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
                        HttpContext.Current.Response.ContentType = "application/zip";
                        HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                        zip.Save(Response.OutputStream);

                        HttpContext.Current.Response.Flush();
                        HttpContext.Current.Response.SuppressContent = true;
                        HttpContext.Current.ApplicationInstance.CompleteRequest();

                        //HttpContext.Current.Response.End();
                        logger.Info("User: " + username + " Managed Well/Lateral Logs Downloaded: " + zipName);

                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSelClear_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientType);
                this.gvwOperatorList.DataSource = operatorCoTable;
                this.gvwOperatorList.PageIndex = 0;
                this.gvwOperatorList.SelectedIndex = -1;
                this.gvwOperatorList.DataBind();
                this.gvwWellPads.DataSource = null;
                this.gvwWellPads.DataBind();
                this.gvwWellList.DataSource = null;
                this.gvwWellList.DataBind();
                this.divOPList.Visible = true;
                this.divPadList.Visible = false;
                this.divWellList.Visible = false;
                this.divUpload.Visible = false;
                this.divFileComments.Visible = false;
                this.liStep1.Attributes["class"] = "stepper-todo";
                this.liStep2.Attributes["class"] = "stepper-todo";
                this.liStep3.Attributes["class"] = "stepper-todo";
                this.liStep4.Attributes["class"] = "stepper-todo";
                this.liStep5.Attributes["class"] = "stepper-todo";
                this.ancHead.InnerHtml = "Operator Company";
                this.btnSelClear.Enabled = false;
                this.btnSelClear.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnSelClear_Click(null, null);
                this.btnDone.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnUncheckAll_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (GridViewRow row in grdFileExplorer.Rows)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("chkRow") as CheckBox);
                    if (chkRow.Checked)
                    {
                        chkRow.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}