﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlPrintOrderLogs.ascx.cs" Inherits="SmartsVer1.Control.LogPrint.ctrlPrintOrderLogs" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<!-- Stylesheet -->
<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript for ref lib -->
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<!-- Javascript for Footer -->
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>
<!--- Page local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }

    .panelBkg {
        background-color: #C1CFDD;
    }

    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }
</style>
<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>Logs Management</li>
                <li><a href="../../LogPrint/fePrintOrderLogs_2A.aspx">Order Log Prints</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- Page Content -->
<asp:UpdateProgress ID="upgrsMeta" runat="server" AssociatedUpdatePanelID="upnlMeta" DisplayAfter="0">
    <ProgressTemplate>
        <div id="divMetaProgress" runat="server" class="updateProgress">
            <asp:Image ID="imgJob1Processing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<asp:UpdatePanel runat="server" ID="upnlMeta">
    <ContentTemplate>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row bg-white">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bg-white">
                    <div id="acrdOrderOuter" class="panel-group" runat="server">
                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                <h5 class="panel-title" style="text-align: left">
                                    <a id="ancMgtMain" runat="server" class="disabled" href="#">Log Print Order(s)</a>
                                </h5>
                            </div>
                            <div id="divOrdersOuter" runat="server" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div id="divGages" runat="server" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div class="panel panel-danger panel-body">
                                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                    <div class="small-box bg-green">
                                                        <div id="divTotalOrders" class="inner" runat="server"></div>
                                                        <div class="icon"><i class="fa fa-first-order"></i></div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                    <div class="small-box bg-orange-active">
                                                        <div id="divSubmittedOrders" class="inner" runat="server"></div>
                                                        <div class="icon"><i class="fa fa-list"></i></div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                    <div class="small-box bg-light-blue">
                                                        <div id="divInprogressOrders" class="inner" runat="server"></div>
                                                        <div class="icon"><i class="fa fa-arrow-right"></i></div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                    <div class="small-box bg-blue">
                                                        <div id="divCompleteOrders" class="inner" runat="server"></div>
                                                        <div class="icon"><i class="fa fa-check-circle"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div id="divProgress" class="panel panel-danger">
                                            <div class="panel-body">
                                                <ol id="olStepper" runat="server" class="stepper" data-stepper-steps="7">
                                                    <li id="liStep1" runat="server" class="stepper-todo">Operator Company</li>
                                                    <li id="liStep2" runat="server" class="stepper-todo">Well Pad</li>
                                                    <li id="liStep3" runat="server" class="stepper-todo">Well/Lateral</li>
                                                    <li id="liStep4" runat="server" class="stepper-todo">Well/Lateral Log(s)</li>
                                                    <li id="liStep5" runat="server" class="stepper-todo">Order Details</li>
                                                </ol>
                                            </div>
                                        </div>
                                        <div id="div1" class="panel panel-danger">
                                            <div class="panel-body">
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:LinkButton ID="btnFileExpClear" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnFileExpClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                </div>
                                                <div class="panel panel-info">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancHead" runat="server" class="disabled" href="#">Operator Company</a>
                                                        </h5>
                                                    </div>
                                                    <div id="divOPList" runat="server" class="panel-body">
                                                        <div class="table-responsive">
                                                            <asp:GridView runat="server" ID="gvwOperatorList" DataKeyNames="clntID" CssClass="mydatagrid" AllowPaging="True" AllowSorting="True"
                                                                AutoGenerateColumns="False" EmptyDataText="No Operator Company found"
                                                                OnPageIndexChanging="gvwOperatorList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15"
                                                                HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow"
                                                                OnSelectedIndexChanged="gvwOperatorList_SelectedIndexChanged">
                                                                <Columns>
                                                                    <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                        <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                    </asp:ButtonField>
                                                                    <asp:BoundField DataField="clntID" Visible="False" />
                                                                    <asp:BoundField DataField="clntName" HeaderText="Operator Company" />
                                                                    <asp:BoundField DataField="wpdCount" HeaderText="Global Well Pad Count" />
                                                                    <asp:BoundField DataField="welCount" HeaderText="Global Well Count" />
                                                                    <asp:BoundField DataField="welGlobal" HeaderText="SMARTs Well Count" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                    <div id="divPadList" runat="server" class="panel-body" visible="false">
                                                        <div class="table-responsive">
                                                            <asp:GridView ID="gvwWellPads" runat="server" EmptyDataText="No Well Pad(s) found for selected Operator"
                                                                ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="wpdID"
                                                                AllowPaging="True" CssClass="mydatagrid" OnPageIndexChanging="gvwWellPads_PageIndexChanging" OnSelectedIndexChanged="gvwWellPads_SelectedIndexChanged"
                                                                AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                RowStyle-CssClass="rows">
                                                                <Columns>
                                                                    <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                        <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                    </asp:ButtonField>
                                                                    <asp:BoundField DataField="wpdID" HeaderText="wpd" ReadOnly="True" Visible="false" />
                                                                    <asp:BoundField DataField="welPadName" HeaderText="Well Pad" />
                                                                    <asp:BoundField DataField="field" HeaderText="O&G Field" />
                                                                    <asp:BoundField DataField="county" HeaderText="County/District/Division/Municipality" />
                                                                    <asp:BoundField DataField="state" HeaderText="State/Province" />
                                                                    <asp:BoundField DataField="welCount" HeaderText="Global Well/Lateral Count">
                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="swlCount" HeaderText="SMARTs Well Count">
                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                    </asp:BoundField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                    <div id="divWellList" runat="server" class="panel-body" visible="false">
                                                        <div class="table-responsive">
                                                            <asp:GridView ID="gvwWellList" runat="server" CssClass="mydatagrid" EmptyDataText="No Well(s)/Lateral(s) found."
                                                                AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="welID" ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwWellList_PageIndexChanging"
                                                                AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                OnSelectedIndexChanged="gvwWellList_SelectedIndexChanged" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                                                <Columns>
                                                                    <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                        <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                    </asp:ButtonField>
                                                                    <asp:BoundField DataField="welID" Visible="false" />
                                                                    <asp:BoundField DataField="welName" HeaderText="Well / Lateral" />
                                                                    <asp:BoundField DataField="field" HeaderText="Oil & Gas Field" />
                                                                    <asp:BoundField DataField="county" HeaderText="County / District" />
                                                                    <asp:BoundField DataField="state" HeaderText="State / Province" />
                                                                    <asp:BoundField DataField="spud" HeaderText="Spud Date" />
                                                                    <asp:BoundField DataField="wstID" HeaderText="Status" />
                                                                </Columns>
                                                            </asp:GridView>
                                                            <div runat="server" id="testEnclosure" visible="false">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                <h4><i runat="server" id="iTest"><span runat="server" id="spnTest"></span></i></h4>
                                                                <asp:Label ID="lblTest" runat="server" Text="" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="divFD" runat="server" class="panel-body" visible="false">
                                                        <div class="table-responsive">
                                                            <asp:GridView ID="grdFileExplorer" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="15"
                                                                CssClass="mydatagrid" PagerStyle-CssClass="pager" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                OnPageIndexChanging="grdFileExplorer_PageIndexChanging" EmptyDataText="No Wells Logs available for selected Well/Lateral"
                                                                ShowHeaderWhenEmpty="True">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Select">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkRow" runat="server" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="File" HeaderText="Log Files" ItemStyle-HorizontalAlign="Left" />
                                                                    <asp:BoundField DataField="CreationTime" HeaderText="Creation Time" />
                                                                    <asp:BoundField DataField="Size" HeaderText="Size in KB" />
                                                                    <asp:BoundField DataField="Type" HeaderText="Extension" />
                                                                </Columns>
                                                            </asp:GridView>
                                                            <div runat="server" id="cEnclosure" visible="false">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                    <h4><i runat="server" id="iC"><span runat="server" id="spnC"></span></i></h4>
                                                                    <asp:Label ID="lblCaution1" runat="server" Text="" />
                                                                </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="input-group" style="width: 100%;">
                                                                    <asp:LinkButton ID="btnPromoteFiles" runat="server" CssClass="btn btn-success text-center" OnClick="btnPromoteFiles_Click"><i class="fa fa-upload"> Promote Files to Print</i></asp:LinkButton>
                                                                    <asp:LinkButton ID="btnClearAll" runat="server" CssClass="btn btn-warning text-center" OnClick="btnClearAll_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="divFinalOrder" runat="server" class="panel-body" visible="false">
                                                        <div class="table-responsive">
                                                            <div id="divPF" runat="server">
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                                            <asp:Label ID="Label1" runat="server" CssClass="label label-default" Text="Select/Type Print Copy Count" ForeColor="Black"
                                                                                Font-Size="Large" />
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                                            <asp:Label ID="Label2" runat="server" CssClass="label label-default" Text="Select/Type CD/DVD Count" ForeColor="Black" Font-Size="Large" />
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                                            <asp:DropDownList ID="ddlPrintCount" runat="server" AutoPostBack="True" AppendDataBoundItems="True" Enabled="true" CssClass="form-control"
                                                                                Style="text-align: center; font-size: small"
                                                                                ForeColor="White" BackColor="Gray" OnSelectedIndexChanged="ddlPrintCount_SelectedIndexChanged">
                                                                                <asp:ListItem Selected="True" Value="0" Text="--- Number of Prints to Order ---"></asp:ListItem>
                                                                                <asp:ListItem Value="1">1</asp:ListItem>
                                                                                <asp:ListItem Value="2">2</asp:ListItem>
                                                                                <asp:ListItem Value="3">3</asp:ListItem>
                                                                                <asp:ListItem Value="4">4</asp:ListItem>
                                                                                <asp:ListItem Value="5">5</asp:ListItem>
                                                                                <asp:ListItem Value="6">6</asp:ListItem>
                                                                                <asp:ListItem Value="7">7</asp:ListItem>
                                                                                <asp:ListItem Value="8">8</asp:ListItem>
                                                                                <asp:ListItem Value="9">9</asp:ListItem>
                                                                                <asp:ListItem Value="10">10</asp:ListItem>
                                                                                <asp:ListItem Value="11">11</asp:ListItem>
                                                                                <asp:ListItem Value="12">12</asp:ListItem>
                                                                                <asp:ListItem Value="13">13</asp:ListItem>
                                                                                <asp:ListItem Value="14">14</asp:ListItem>
                                                                                <asp:ListItem Value="15">15</asp:ListItem>
                                                                                <asp:ListItem Value="16">16</asp:ListItem>
                                                                                <asp:ListItem Value="17">17</asp:ListItem>
                                                                                <asp:ListItem Value="18">18</asp:ListItem>
                                                                                <asp:ListItem Value="19">19</asp:ListItem>
                                                                                <asp:ListItem Value="20">20</asp:ListItem>
                                                                                <asp:ListItem Value="21">21</asp:ListItem>
                                                                                <asp:ListItem Value="22">22</asp:ListItem>
                                                                                <asp:ListItem Value="23">23</asp:ListItem>
                                                                                <asp:ListItem Value="24">24</asp:ListItem>
                                                                                <asp:ListItem Value="25">25</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                                            <asp:DropDownList ID="ddlPrintCDs" runat="server" AutoPostBack="True" AppendDataBoundItems="True" Enabled="true" CssClass="form-control"
                                                                                Style="text-align: center; font-size: small"
                                                                                ForeColor="White" BackColor="Gray" OnSelectedIndexChanged="ddlPrintCDs_SelectedIndexChanged">
                                                                                <asp:ListItem Selected="True" Value="0" Text="--- Number of DVDs to Order ---"></asp:ListItem>
                                                                                <asp:ListItem Value="1">1</asp:ListItem>
                                                                                <asp:ListItem Value="2">2</asp:ListItem>
                                                                                <asp:ListItem Value="3">3</asp:ListItem>
                                                                                <asp:ListItem Value="4">4</asp:ListItem>
                                                                                <asp:ListItem Value="5">5</asp:ListItem>
                                                                                <asp:ListItem Value="6">6</asp:ListItem>
                                                                                <asp:ListItem Value="7">7</asp:ListItem>
                                                                                <asp:ListItem Value="8">8</asp:ListItem>
                                                                                <asp:ListItem Value="9">9</asp:ListItem>
                                                                                <asp:ListItem Value="10">10</asp:ListItem>
                                                                                <asp:ListItem Value="11">11</asp:ListItem>
                                                                                <asp:ListItem Value="12">12</asp:ListItem>
                                                                                <asp:ListItem Value="13">13</asp:ListItem>
                                                                                <asp:ListItem Value="14">14</asp:ListItem>
                                                                                <asp:ListItem Value="15">15</asp:ListItem>
                                                                                <asp:ListItem Value="16">16</asp:ListItem>
                                                                                <asp:ListItem Value="17">17</asp:ListItem>
                                                                                <asp:ListItem Value="18">18</asp:ListItem>
                                                                                <asp:ListItem Value="19">19</asp:ListItem>
                                                                                <asp:ListItem Value="20">20</asp:ListItem>
                                                                                <asp:ListItem Value="21">21</asp:ListItem>
                                                                                <asp:ListItem Value="22">22</asp:ListItem>
                                                                                <asp:ListItem Value="23">23</asp:ListItem>
                                                                                <asp:ListItem Value="24">24</asp:ListItem>
                                                                                <asp:ListItem Value="25">25</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <div>&nbsp;</div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <asp:Label ID="lblAdress" runat="server" CssClass="label label-default" Text="Select Mail-To Address" ForeColor="Black" Font-Size="Large" />
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <asp:GridView ID="grdAddress" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="20"
                                                                                CssClass="mydatagrid" PagerStyle-CssClass="pager" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                                EmptyDataText="No Address avaiable" ShowHeaderWhenEmpty="True" OnPageIndexChanging="grdAddress_PageIndexChanging">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Select">
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="chkRow" runat="server" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField DataField="clntAddID" HeaderText="clntAddID" />
                                                                                    <asp:BoundField DataField="title" HeaderText="Title" />
                                                                                    <asp:BoundField DataField="dmgID" HeaderText="Type" />
                                                                                    <asp:BoundField DataField="st1" HeaderText="Street 1" />
                                                                                    <asp:BoundField DataField="st2" HeaderText="Street 2" />
                                                                                    <asp:BoundField DataField="st3" HeaderText="Street 3" />
                                                                                    <asp:BoundField DataField="cityID" HeaderText="City/Town" />
                                                                                    <asp:BoundField DataField="cntyID" HeaderText="County/District" />
                                                                                    <asp:BoundField DataField="stID" HeaderText="State/Province" />
                                                                                    <asp:BoundField DataField="ctyID" HeaderText="Country/Nation" />
                                                                                    <asp:BoundField DataField="zipID" HeaderText="Zipcode/Postal Code" />
                                                                                    <asp:BoundField DataField="sregID" HeaderText="Sub-Region" Visible="False" />
                                                                                    <asp:BoundField DataField="regID" HeaderText="Geographic Region" />
                                                                                    <asp:BoundField DataField="uTime" HeaderText="Last Updated On" SortExpression="uTime" ReadOnly="True" DataFormatString="{0:MMM d , yyyy hh:mm tt}" />
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                            <div runat="server" id="orsEnclosure" visible="false">
                                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                                <h4><i runat="server" id="iORS"><span runat="server" id="spnORS"></span></i></h4>
                                                                                <asp:Label ID="lblOrderStatus" runat="server" Text="" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <div class="input-group" style="width: 100%;">
                                                                                <asp:LinkButton ID="btnProceedFinal" runat="server" CssClass="btn btn-success text-center" Visible="false" OnClick="btnProceedFinal_Click"><i class="fa fa-arrow-circle-right"></i> Next</asp:LinkButton>
                                                                            </div>
                                                                        </div>
                                                                        <div>&nbsp;</div>
                                                                    </div>
                                                                    <div id="divFO" runat="server" visible="false">
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <asp:Label ID="lblOrderReview" runat="server" CssClass="label label-default" Text="Review Order
                                                                                Details:"
                                                                                Font-Bold="true" ForeColor="Blue" Font-Size="Large" />
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <asp:TextBox runat="server" ID="txtbxComments" TextMode="MultiLine" Height="250px" MaxLength="2500"
                                                                                BackColor="#F9F3FD" BorderWidth="3px" CssClass="form-control" Enabled="false" Font-Size="Large" />
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <div class="input-group" style="width: 100%;">
                                                                                <asp:LinkButton ID="btnProceedWithPrint" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnProceedWithPrint_Click"><i
                                                                    class="fa fa-check-circle"> Confirm Order</i></asp:LinkButton>
                                                                                <asp:LinkButton ID="btnUncheckAll" runat="server" CssClass="btn btn-danger text-center" OnClick="btnUncheckAll_Click"><i
                                                                    class=" fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnUncheckAll" />
    </Triggers>
</asp:UpdatePanel>
                    