﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Net.Mail;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;

namespace SmartsVer1.Control.Config.Demogs
{
    public partial class ctrlFECDemogs_2A : System.Web.UI.UserControl
    {
        String LoginName, username, domain, GetClientDBString;
        Int32 ClientID = -99, globWSt = 1;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();
                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);
                UP usrP = UP.GetUserProfile(LoginName);
                username = addr.User;
                domain = addr.Host;
                ClientID = Convert.ToInt32(usrP.clntID);
                GetClientDBString = clntConn.getClientDBConnection(domain);

                //Configuring page dropdownlists
                System.Data.DataTable wsTable = DMG.getClientWellSectionTable(GetClientDBString);
                this.gvwWSList.DataSource = wsTable;
                this.gvwWSList.DataBind();
                Dictionary<Int32, String> rgList = DMG.getRegionList();
                this.ddlWRegion.Items.Clear();
                this.ddlWRegion.DataSource = rgList;
                this.ddlWRegion.DataTextField = "Value";
                this.ddlWRegion.DataValueField = "Key";
                this.ddlWRegion.DataBind();
                this.ddlWRegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlWRegion.SelectedIndex = -1;
                Dictionary<Int32, String> opList = CLNT.getOpCoList();
                this.ddlWAOPC.Items.Clear();
                this.ddlWAOPC.DataSource = opList;
                this.ddlWAOPC.DataTextField = "Value";
                this.ddlWAOPC.DataValueField = "Key";
                this.ddlWAOPC.DataBind();
                this.ddlWAOPC.Items.Insert(0, new ListItem("--- Select Well Operator Company ---", "-1"));
                this.ddlWAOPC.SelectedIndex = -1;
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");
                //this.clndrSpudDate.SelectedDate = DateTime.Now;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWellClear_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> rgList = DMG.getRegionList();
                this.ddlWRegion.Items.Clear();
                this.ddlWRegion.DataSource = rgList;
                this.ddlWRegion.DataTextField = "Value";
                this.ddlWRegion.DataValueField = "Key";
                this.ddlWRegion.DataBind();
                this.ddlWRegion.Items.Insert(0, new ListItem("--- Select Major Geogrpahic Region ---", "-1"));
                this.ddlWRegion.SelectedIndex = -1;
                this.ddlWSRegion.Items.Clear();
                this.ddlWSRegion.DataBind();
                this.ddlWSRegion.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlWSRegion.SelectedIndex = -1;
                this.ddlWSRegion.Enabled = false;
                this.ddlWCountry.Items.Clear();
                this.ddlWCountry.DataBind();
                this.ddlWCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlWCountry.SelectedIndex = -1;
                this.ddlWCountry.Enabled = false;
                this.ddlWState.Items.Clear();
                this.ddlWState.DataBind();
                this.ddlWState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlWState.SelectedIndex = -1;
                this.ddlWState.Enabled = false;
                this.ddlWCounty.Items.Clear();
                this.ddlWCounty.DataBind();
                this.ddlWCounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlWCounty.SelectedIndex = -1;
                this.ddlWCounty.Enabled = false;
                this.ddlWField.Items.Clear();
                this.ddlWField.DataBind();
                this.ddlWField.Items.Insert(0, new ListItem("--- Select Oil & Gas Field ---", "-1"));
                this.ddlWField.SelectedIndex = -1;
                this.ddlWField.Enabled = false;
                this.btnWellClear.CssClass = "btn btn-default";
                this.btnWellClear.Enabled = false;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();
                this.gvwWellList.Visible = false;
                this.gvwWellDetailList.DataBind();
                this.gvwWellDetailList.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 selID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                System.Data.DataTable detTable = AST.getDemogWellDetailTable(selID);
                this.gvwWellDetailList.DataSource = detTable;
                this.gvwWellDetailList.DataBind();
                this.gvwWellDetailList.Visible = true;
                this.btnWellClear.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srg = Convert.ToInt32(this.ddlWRegion.SelectedValue);
                Dictionary<Int32, String> srgList = DMG.getSubRegionList(srg);
                this.ddlWSRegion.Items.Clear();
                this.ddlWSRegion.DataSource = srgList;
                this.ddlWSRegion.DataTextField = "Value";
                this.ddlWSRegion.DataValueField = "Key";
                this.ddlWSRegion.DataBind();
                this.ddlWSRegion.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlWSRegion.SelectedIndex = -1;
                this.ddlWSRegion.Enabled = true;
                this.ddlWCountry.Items.Clear();
                this.ddlWCountry.DataBind();
                this.ddlWCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlWCountry.SelectedIndex = -1;
                this.ddlWCountry.Enabled = false;
                this.ddlWState.Items.Clear();
                this.ddlWState.DataBind();
                this.ddlWState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlWState.SelectedIndex = -1;
                this.ddlWState.Enabled = false;
                this.ddlWCounty.Items.Clear();
                this.ddlWCounty.DataBind();
                this.ddlWCounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlWCounty.SelectedIndex = -1;
                this.ddlWCounty.Enabled = false;
                this.ddlWField.Items.Clear();
                this.ddlWField.DataBind();
                this.ddlWField.Items.Insert(0, new ListItem("--- Select Oil & Gas Field ---", "-1"));
                this.ddlWField.SelectedIndex = -1;
                this.ddlWField.Enabled = false;
                this.btnWellClear.Visible = true;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();
                this.gvwWellDetailList.DataBind();
                this.gvwWellDetailList.Visible = false;
                this.btnWellClear.CssClass = "btn btn-warning";
                this.btnWellClear.Enabled = true;
                this.ddlWSRegion.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWSRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlWSRegion.SelectedValue);
                Dictionary<Int32, String> srList = DMG.getCountryList(srID);
                this.ddlWCountry.Items.Clear();
                this.ddlWCountry.DataSource = srList;
                this.ddlWCountry.DataTextField = "Value";
                this.ddlWCountry.DataValueField = "Key";
                this.ddlWCountry.DataBind();
                this.ddlWCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlWCountry.SelectedIndex = -1;
                this.ddlWCountry.Enabled = true;
                this.ddlWState.Items.Clear();
                this.ddlWState.DataBind();
                this.ddlWState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlWState.SelectedIndex = -1;
                this.ddlWState.Enabled = false;
                this.ddlWCounty.Items.Clear();
                this.ddlWCounty.DataBind();
                this.ddlWCounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlWCounty.SelectedIndex = -1;
                this.ddlWCounty.Enabled = false;
                this.ddlWField.Items.Clear();
                this.ddlWField.DataBind();
                this.ddlWField.Items.Insert(0, new ListItem("--- Select Oil & Gas Field ---", "-1"));
                this.ddlWField.SelectedIndex = -1;
                this.ddlWField.Enabled = false;
                this.btnWellClear.Visible = true;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();
                this.gvwWellDetailList.DataBind();
                this.gvwWellDetailList.Visible = false;

                this.ddlWCountry.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cID = Convert.ToInt32(this.ddlWCountry.SelectedValue);
                Dictionary<Int32, String> stList = DMG.getStateList(cID);
                this.ddlWState.Items.Clear();
                this.ddlWState.DataSource = stList;
                this.ddlWState.DataTextField = "Value";
                this.ddlWState.DataValueField = "Key";
                this.ddlWState.DataBind();
                this.ddlWState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlWState.SelectedIndex = -1;
                this.ddlWState.Enabled = true;
                this.ddlWCounty.Items.Clear();
                this.ddlWCounty.DataBind();
                this.ddlWCounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlWCounty.SelectedIndex = -1;
                this.ddlWCounty.Enabled = false;
                this.ddlWField.Items.Clear();
                this.ddlWField.DataBind();
                this.ddlWField.Items.Insert(0, new ListItem("--- Select Oil & Gas Field ---", "-1"));
                this.ddlWField.SelectedIndex = -1;
                this.ddlWField.Enabled = false;
                this.btnWellClear.Visible = true;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();
                this.gvwWellDetailList.DataBind();
                this.gvwWellDetailList.Visible = false;

                this.ddlWState.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 sID = Convert.ToInt32(this.ddlWState.SelectedValue);
                Dictionary<Int32, String> ctList = DMG.getCountyWithFieldCountList(sID);
                this.ddlWCounty.Items.Clear();
                this.ddlWCounty.DataSource = ctList;
                this.ddlWCounty.DataTextField = "Value";
                this.ddlWCounty.DataValueField = "Key";
                this.ddlWCounty.DataBind();
                this.ddlWCounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlWCounty.SelectedIndex = -1;
                this.ddlWCounty.Enabled = true;
                this.ddlWField.Items.Clear();
                this.ddlWField.DataBind();
                this.ddlWField.Items.Insert(0, new ListItem("--- Select Oil & Gas Field ---", "-1"));
                this.ddlWField.SelectedIndex = -1;
                this.ddlWField.Enabled = false;
                this.btnWellClear.Visible = true;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();
                this.gvwWellDetailList.DataBind();
                this.gvwWellDetailList.Visible = false;

                this.ddlWCounty.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWCounty_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cID = Convert.ToInt32(this.ddlWCounty.SelectedValue);
                Dictionary<Int32, String> fList = DMG.getFieldWithWellCountList(cID);
                this.ddlWField.Items.Clear();
                this.ddlWField.DataSource = fList;
                this.ddlWField.DataTextField = "Value";
                this.ddlWField.DataValueField = "Key";
                this.ddlWField.DataBind();
                this.ddlWField.Items.Insert(0, new ListItem("--- Select Oil & Gas Field ---", "-1"));
                this.ddlWField.SelectedIndex = -1;
                this.ddlWField.Enabled = true;
                this.btnWellClear.Visible = true;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();
                this.gvwWellDetailList.DataBind();
                this.gvwWellDetailList.Visible = false;

                this.ddlWField.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWField_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 fID = Convert.ToInt32(this.ddlWField.SelectedValue);
                System.Data.DataTable wTable = DMG.getGlobalFieldWellTable(fID);
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataSource = wTable;
                this.gvwWellList.DataBind();
                this.gvwWellList.Visible = true;
                this.gvwWellDetailList.DataBind();
                this.gvwWellDetailList.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWAOPC_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtWAName.Text = "";
                this.txtWAName.Enabled = true;
                this.txtWAAPI.Text = "";
                this.txtWAAPI.Enabled = true;
                this.txtWAUWI.Text = "";
                this.txtWAUWI.Enabled = true;
                this.txtWAPermit.Text = "";
                this.txtWAPermit.Enabled = true;
                this.datepicker.Text = "";
                this.datepicker.Enabled = true;
                //this.txtWADate.Text = "";
                //this.txtWADate.Enabled = true;
                this.txtWALat.Text = "";
                this.txtWALat.Enabled = true;
                this.txtWALon.Text = "";
                this.txtWALon.Enabled = true;
                Dictionary<Int32, String> latO = DMG.getLatCardinals();
                this.ddlWellAddLatO.Items.Clear();
                this.ddlWellAddLatO.DataSource = latO;
                this.ddlWellAddLatO.DataTextField = "Value";
                this.ddlWellAddLatO.DataValueField = "Key";
                this.ddlWellAddLatO.DataBind();
                this.ddlWellAddLatO.Items.Insert(0, new ListItem("--- Select Direction ---", "-1"));
                this.ddlWellAddLatO.SelectedIndex = -1;
                this.ddlWellAddLatO.Enabled = true;
                Dictionary<Int32, String> lonO = DMG.getLngCardinals();
                this.ddlWellAddLonO.Items.Clear();
                this.ddlWellAddLonO.DataSource = lonO;
                this.ddlWellAddLonO.DataTextField = "Value";
                this.ddlWellAddLonO.DataValueField = "Key";
                this.ddlWellAddLonO.DataBind();
                this.ddlWellAddLonO.Items.Insert(0, new ListItem("--- Select Direction ---", "-1"));
                this.ddlWellAddLonO.SelectedIndex = -1;
                this.ddlWellAddLonO.Enabled = true;
                this.txtWAName.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWARegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 rgID = Convert.ToInt32(this.ddlWARegion.SelectedValue);
                Dictionary<Int32, String> srList = DMG.getSubRegionList(rgID);
                this.ddlWASRegion.Items.Clear();
                this.ddlWASRegion.DataSource = srList;
                this.ddlWASRegion.DataTextField = "Value";
                this.ddlWASRegion.DataValueField = "Key";
                this.ddlWASRegion.DataBind();
                this.ddlWASRegion.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlWASRegion.SelectedIndex = -1;
                this.ddlWASRegion.Enabled = true;
                this.ddlWACountry.Items.Clear();
                this.ddlWACountry.DataBind();
                this.ddlWACountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlWACountry.SelectedIndex = -1;
                this.ddlWACountry.Enabled = false;
                this.ddlWAState.Items.Clear();
                this.ddlWAState.DataBind();
                this.ddlWAState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlWAState.SelectedIndex = -1;
                this.ddlWAState.Enabled = false;
                this.ddlWACounty.Items.Clear();
                this.ddlWACounty.DataBind();
                this.ddlWACounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlWACounty.SelectedIndex = -1;
                this.ddlWACounty.Enabled = false;
                this.ddlWAField.Items.Clear();
                this.ddlWAField.DataBind();
                this.ddlWAField.Items.Insert(0, new ListItem("--- Select Oil & Gas Field ---", "-1"));
                this.ddlWAField.SelectedIndex = -1;
                this.ddlWAField.Enabled = false;
                this.txtWAName.Text = "";
                this.txtWAName.Enabled = false;
                this.txtWAAPI.Text = "";
                this.txtWAAPI.Enabled = false;
                this.txtWAUWI.Text = "";
                this.txtWAUWI.Enabled = false;
                this.txtWAPermit.Text = "";
                this.txtWAPermit.Enabled = false;
                this.datepicker.Text = "";
                this.datepicker.Enabled = false;
                this.txtWALat.Text = "";
                this.txtWALat.Enabled = false;
                this.txtWALon.Text = "";
                this.txtWALon.Enabled = false;
                this.btnWellNewCancel.Enabled = true;
                this.btnWellNewCancel.CssClass = "btn btn-warning";
                this.ddlWASRegion.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWASRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srgID = Convert.ToInt32(this.ddlWASRegion.SelectedValue);
                Dictionary<Int32, String> ctyList = DMG.getCountryList(srgID);
                this.ddlWACountry.Items.Clear();
                this.ddlWACountry.DataSource = ctyList;
                this.ddlWACountry.DataTextField = "Value";
                this.ddlWACountry.DataValueField = "Key";
                this.ddlWACountry.DataBind();
                this.ddlWACountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlWACountry.SelectedIndex = -1;
                this.ddlWACountry.Enabled = true;
                this.ddlWAState.Items.Clear();
                this.ddlWAState.DataBind();
                this.ddlWAState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlWAState.SelectedIndex = -1;
                this.ddlWAState.Enabled = false;
                this.ddlWACounty.Items.Clear();
                this.ddlWACounty.DataBind();
                this.ddlWACounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlWACounty.SelectedIndex = -1;
                this.ddlWACounty.Enabled = false;
                this.ddlWAField.Items.Clear();
                this.ddlWAField.DataBind();
                this.ddlWAField.Items.Insert(0, new ListItem("--- Select Oil & Gas Field ---", "-1"));
                this.ddlWAField.SelectedIndex = -1;
                this.ddlWAField.Enabled = false;
                this.txtWAName.Text = "";
                this.txtWAName.Enabled = false;
                this.txtWAAPI.Text = "";
                this.txtWAAPI.Enabled = false;
                this.txtWAUWI.Text = "";
                this.txtWAUWI.Enabled = false;
                this.txtWAPermit.Text = "";
                this.txtWAPermit.Enabled = false;
                this.datepicker.Text = "";
                this.datepicker.Enabled = false;
                this.txtWALat.Text = "";
                this.txtWALat.Enabled = false;
                this.txtWALon.Text = "";
                this.txtWALon.Enabled = false;

                this.ddlWACountry.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWACountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctyID = Convert.ToInt32(this.ddlWACountry.SelectedValue);
                Dictionary<Int32, String> stList = DMG.getStateList(ctyID);
                this.ddlWAState.Items.Clear();
                this.ddlWAState.DataSource = stList;
                this.ddlWAState.DataTextField = "Value";
                this.ddlWAState.DataValueField = "Key";
                this.ddlWAState.DataBind();
                this.ddlWAState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlWAState.SelectedIndex = -1;
                this.ddlWAState.Enabled = true;
                this.ddlWACounty.Items.Clear();
                this.ddlWACounty.DataBind();
                this.ddlWACounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlWACounty.SelectedIndex = -1;
                this.ddlWACounty.Enabled = false;
                this.ddlWAField.Items.Clear();
                this.ddlWAField.DataBind();
                this.ddlWAField.Items.Insert(0, new ListItem("--- Select Oil & Gas Field ---", "-1"));
                this.ddlWAField.SelectedIndex = -1;
                this.ddlWAField.Enabled = false;
                this.txtWAName.Text = "";
                this.txtWAName.Enabled = false;
                this.txtWAAPI.Text = "";
                this.txtWAAPI.Enabled = false;
                this.txtWAUWI.Text = "";
                this.txtWAUWI.Enabled = false;
                this.txtWAPermit.Text = "";
                this.txtWAPermit.Enabled = false;
                this.datepicker.Text = "";
                this.datepicker.Enabled = false;
                this.txtWALat.Text = "";
                this.txtWALat.Enabled = false;
                this.txtWALon.Text = "";
                this.txtWALon.Enabled = false;

                this.ddlWAState.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWAState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 sID = Convert.ToInt32(this.ddlWAState.SelectedValue);
                Dictionary<Int32, String> cnyList = DMG.getCountyWithFieldCountList(sID);
                this.ddlWACounty.Items.Clear();
                this.ddlWACounty.DataSource = cnyList;
                this.ddlWACounty.DataTextField = "Value";
                this.ddlWACounty.DataValueField = "Key";
                this.ddlWACounty.DataBind();
                this.ddlWACounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlWACounty.SelectedIndex = -1;
                this.ddlWACounty.Enabled = true;
                this.ddlWAField.Items.Clear();
                this.ddlWAField.DataBind();
                this.ddlWAField.Items.Insert(0, new ListItem("--- Select Oil & Gas Field ---", "-1"));
                this.ddlWAField.SelectedIndex = -1;
                this.ddlWAField.Enabled = false;
                this.txtWAName.Text = "";
                this.txtWAName.Enabled = false;
                this.txtWAAPI.Text = "";
                this.txtWAAPI.Enabled = false;
                this.txtWAUWI.Text = "";
                this.txtWAUWI.Enabled = false;
                this.txtWAPermit.Text = "";
                this.txtWAPermit.Enabled = false;
                this.datepicker.Text = "";
                this.datepicker.Enabled = false;
                this.txtWALat.Text = "";
                this.txtWALat.Enabled = false;
                this.txtWALon.Text = "";
                this.txtWALon.Enabled = false;

                this.ddlWACounty.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWACounty_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cID = Convert.ToInt32(this.ddlWACounty.SelectedValue);
                Dictionary<Int32, String> fList = DMG.getFieldNameListForCounty(cID);
                this.ddlWAField.Items.Clear();
                this.ddlWAField.DataSource = fList;
                this.ddlWAField.DataTextField = "Value";
                this.ddlWAField.DataValueField = "Key";
                this.ddlWAField.DataBind();
                this.ddlWAField.Items.Insert(0, new ListItem("--- Select Oil & Gas Field ---", "-1"));
                this.ddlWAField.SelectedIndex = -1;
                this.ddlWAField.Enabled = true;
                this.txtWAName.Text = "";
                this.txtWAName.Enabled = false;
                this.txtWAAPI.Text = "";
                this.txtWAAPI.Enabled = false;
                this.txtWAUWI.Text = "";
                this.txtWAUWI.Enabled = false;
                this.txtWAPermit.Text = "";
                this.txtWAPermit.Enabled = false;
                this.datepicker.Text = "";
                this.datepicker.Enabled = false;
                this.txtWALat.Text = "";
                this.txtWALat.Enabled = false;
                this.txtWALon.Text = "";
                this.txtWALon.Enabled = false;

                this.ddlWAField.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWAField_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> opList = CLNT.getOpCoList();
                this.ddlWAOPC.Items.Clear();
                this.ddlWAOPC.DataSource = opList;
                this.ddlWAOPC.DataTextField = "Value";
                this.ddlWAOPC.DataValueField = "Key";
                this.ddlWAOPC.DataBind();
                this.ddlWAOPC.Items.Insert(0, new ListItem("--- Select Well Operator Company ---", "-1"));
                this.ddlWAOPC.SelectedIndex = -1;
                this.ddlWAOPC.Enabled = true;
                this.txtWAName.Text = "";
                this.txtWAName.Enabled = false;
                this.txtWAAPI.Text = "";
                this.txtWAAPI.Enabled = false;
                this.txtWAUWI.Text = "";
                this.txtWAUWI.Enabled = false;
                this.txtWAPermit.Text = "";
                this.txtWAPermit.Enabled = false;
                this.datepicker.Text = "";
                this.datepicker.Enabled = false;
                this.txtWALat.Text = "";
                this.txtWALat.Enabled = false;
                this.ddlWellAddLatO.Items.Clear();
                this.ddlWellAddLatO.DataBind();
                this.ddlWellAddLatO.Items.Insert(0, new ListItem("--- Select Direction ---", "-1"));
                this.ddlWellAddLatO.SelectedIndex = -1;
                this.ddlWellAddLatO.Enabled = false;
                this.txtWALon.Text = "";
                this.txtWALon.Enabled = false;
                this.ddlWellAddLonO.Items.Clear();
                this.ddlWellAddLonO.DataBind();
                this.ddlWellAddLonO.Items.Insert(0, new ListItem("--- Select Direction ---", "-1"));
                this.ddlWellAddLonO.SelectedIndex = -1;
                this.ddlWellAddLonO.Enabled = false;
                this.ddlWAOPC.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWellAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnWellClear_Click(null, null);
                this.divWList.Visible = false;
                this.divWEdit.Visible = true;
                Dictionary<Int32, String> regList = DMG.getRegionList();
                this.ddlWARegion.Items.Clear();
                this.ddlWARegion.DataSource = regList;
                this.ddlWARegion.DataTextField = "Value";
                this.ddlWARegion.DataValueField = "Key";
                this.ddlWARegion.DataBind();
                this.ddlWARegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlWARegion.SelectedIndex = -1;
                this.ddlWARegion.Enabled = true;
                this.ddlWARegion.Focus();
                this.btnWellClear.CssClass = "btn btn-warning";
                this.btnWellClear.Enabled = true;
                this.divWList.Attributes["class"] = "panel-collapse collapse";
                this.divWEdit.Attributes["class"] = "panel-collapse collapse in";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWellNew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, oprID = -99, rgID = -99, srgID = -99, ctyID = -99, stID = -99, cnyID = -99, fldID = -99, oLat = -99, oLon = -99, clientID = 1; //Using ClientID for FESMARTs
                String wName = String.Empty, wAPI = String.Empty, wUWI = String.Empty, wPermit = String.Empty;
                DateTime wSpud = DateTime.Now;
                Decimal wLat = -99.000000M, wLon = -99.000000M;
                oprID = Convert.ToInt32(this.ddlWAOPC.SelectedValue);
                rgID = Convert.ToInt32(this.ddlWARegion.SelectedValue);
                srgID = Convert.ToInt32(this.ddlWASRegion.SelectedValue);
                ctyID = Convert.ToInt32(this.ddlWACountry.SelectedValue);
                stID = Convert.ToInt32(this.ddlWAState.SelectedValue);
                cnyID = Convert.ToInt32(this.ddlWACounty.SelectedValue);
                fldID = Convert.ToInt32(this.ddlWAField.SelectedValue);
                wName = Convert.ToString(this.txtWAName.Text);
                wAPI = Convert.ToString(this.txtWAAPI.Text);
                if (String.IsNullOrEmpty(wAPI))
                {
                    wAPI = "---";
                }
                wUWI = Convert.ToString(this.txtWAUWI.Text);
                if (String.IsNullOrEmpty(wUWI))
                {
                    wUWI = "---";
                }
                wPermit = Convert.ToString(this.txtWAPermit.Text);
                wSpud = Convert.ToDateTime(this.datepicker.Text);
                wLat = Convert.ToDecimal(this.txtWALat.Text);
                oLat = Convert.ToInt32(this.ddlWellAddLatO.SelectedValue);
                wLon = Convert.ToDecimal(this.txtWALon.Text);
                oLon = Convert.ToInt32(this.ddlWellAddLonO.SelectedValue);
                if (String.IsNullOrEmpty(wName) || wLon.Equals(null) || wLat.Equals(null))
                {
                    if (String.IsNullOrEmpty(wName))
                    {
                        this.divWellEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iWellMessage.Attributes["class"] = "icon fa fa-warning";
                        this.spnWellAdd.InnerText = " Missing information!";
                        this.lblWASuccess.Text = "Well Name can not be null/empty. Please provide all values";
                        this.divAlertEnclosure.Visible = true;
                    }
                    else if (wLon.Equals(null) && wLat.Equals(null))
                    {
                        this.divWellEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iWellMessage.Attributes["class"] = "icon fa fa-warning";
                        this.spnWellAdd.InnerText = " Missing information!";
                        this.lblWASuccess.Text = "Well Location, as indicated by Latitude and Longitude, values are required. Please provide all values";
                        this.divAlertEnclosure.Visible = true;
                    }
                    else if (wLon.Equals(null))
                    {
                        this.divWellEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iWellMessage.Attributes["class"] = "icon fa fa-warning";
                        this.spnWellAdd.InnerText = " Missing information!";
                        this.lblWASuccess.Text = "Well Longitude is a required field. Please provide all values";
                        this.divAlertEnclosure.Visible = true;
                    }
                    else if (wLat.Equals(null))
                    {
                        this.divWellEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iWellMessage.Attributes["class"] = "icon fa fa-warning";
                        this.spnWellAdd.InnerText = " Missing information!";
                        this.lblWASuccess.Text = "Well Latitude is a required field. Please provide all values";
                        this.divAlertEnclosure.Visible = true;
                    }
                }
                else
                {
                    //iResult = AST.addWellDemog(clientID, Convert.ToString(oprID), wName, wLon, oLon, wLat, oLat, rgID, srgID, ctyID, cnyID, fldID, stID, wAPI, wUWI, wPermit, wSpud, globWSt, domain, LoginName, domain); //Using FESmarts domain for both client domain and user domain
                    //if (iResult.Equals(1))
                    //{
                    //    this.btnWellNewCancel_Click(null, null);
                    //    this.divWellEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                    //    this.iWellMessage.Attributes["class"] = "icon fa fa-check-circle";
                    //    this.spnWellAdd.InnerText = " Success!";
                    //    this.lblWASuccess.Text = "Successfully inserted New Global Well";
                    //    this.divAlertEnclosure.Visible = true;
                    //}
                    //else
                    //{
                    //    this.btnWellNewCancel_Click(null, null);
                    //    this.divWellEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                    //    this.iWellMessage.Attributes["class"] = "icon fa fa-warning";
                    //    this.spnWellAdd.InnerText = " Internal Error!";
                    //    this.lblWASuccess.Text = "Unable to insert New Global Well";
                    //    this.divAlertEnclosure.Visible = true;
                    //}
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWellNewCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> rgList = DMG.getRegionList();
                this.ddlWARegion.Items.Clear();
                this.ddlWARegion.DataSource = rgList;
                this.ddlWARegion.DataTextField = "Value";
                this.ddlWARegion.DataValueField = "Key";
                this.ddlWARegion.DataBind();
                this.ddlWARegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlWARegion.SelectedIndex = -1;
                this.ddlWARegion.Enabled = true;
                this.ddlWAOPC.Items.Clear();
                this.ddlWAOPC.DataBind();
                this.ddlWAOPC.Items.Insert(0, new ListItem("--- Select Well Operator Company ---", "-1"));
                this.ddlWAOPC.SelectedIndex = -1;
                this.ddlWAOPC.Enabled = false;
                this.ddlWASRegion.Items.Clear();
                this.ddlWASRegion.DataBind();
                this.ddlWASRegion.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlWASRegion.SelectedIndex = -1;
                this.ddlWASRegion.Enabled = false;
                this.ddlWACountry.Items.Clear();
                this.ddlWACountry.DataBind();
                this.ddlWACountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlWACountry.SelectedIndex = -1;
                this.ddlWACountry.Enabled = false;
                this.ddlWAState.Items.Clear();
                this.ddlWAState.DataBind();
                this.ddlWAState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlWAState.SelectedIndex = -1;
                this.ddlWAState.Enabled = false;
                this.ddlWACounty.Items.Clear();
                this.ddlWACounty.DataBind();
                this.ddlWACounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlWACounty.SelectedIndex = -1;
                this.ddlWACounty.Enabled = false;
                this.ddlWAField.Items.Clear();
                this.ddlWAField.DataBind();
                this.ddlWAField.Items.Insert(0, new ListItem("--- Select Oil & Gas Field ---", "-1"));
                this.ddlWAField.SelectedIndex = -1;
                this.ddlWAField.Enabled = false;
                this.txtWAName.Text = "";
                this.txtWAName.Enabled = false;
                this.txtWAAPI.Text = "";
                this.txtWAAPI.Enabled = false;
                this.txtWAUWI.Text = "";
                this.txtWAUWI.Enabled = false;
                this.txtWAPermit.Text = "";
                this.txtWAPermit.Enabled = false;
                this.datepicker.Text = "";
                this.datepicker.Enabled = false;
                this.txtWALat.Text = "";
                this.txtWALat.Enabled = false;
                this.ddlWellAddLatO.Items.Clear();
                this.ddlWellAddLatO.DataBind();
                this.ddlWellAddLatO.Items.Insert(0, new ListItem("--- Select Direction ---", "-1"));
                this.ddlWellAddLatO.SelectedIndex = -1;
                this.ddlWellAddLatO.Enabled = false;
                this.txtWALon.Text = "";
                this.txtWALon.Enabled = false;
                this.ddlWellAddLonO.Items.Clear();
                this.ddlWellAddLonO.DataBind();
                this.ddlWellAddLonO.Items.Insert(0, new ListItem("--- Select Direction ---", "-1"));
                this.ddlWellAddLonO.SelectedIndex = -1;
                this.ddlWellAddLonO.Enabled = false;
                this.divAlertEnclosure.Visible = false;
                this.ddlWARegion.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWellAddDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnWellNewCancel_Click(null, null);                                                
                this.divWList.Attributes["class"] = "panel-collapse collapse in";
                this.divWList.Visible = true;
                this.divWEdit.Attributes["class"] = "panel-collapse collapse";
                this.divWEdit.Visible = false;
                Dictionary<Int32, String> rgList = DMG.getRegionList();
                this.ddlWRegion.Items.Clear();
                this.ddlWRegion.DataSource = rgList;
                this.ddlWRegion.DataTextField = "Value";
                this.ddlWRegion.DataValueField = "Key";
                this.ddlWRegion.DataBind();
                this.ddlWRegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlWRegion.SelectedIndex = -1;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable tableWL = DMG.getGlobalWellTable();
                this.gvwWellList.PageIndex = e.NewPageIndex;
                this.gvwWellList.DataSource = tableWL;
                this.gvwWellList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWSAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnWSClear_Click(null, null);
                this.txtWSName.Text = "";
                this.divWSList.Attributes["class"] = "panel-collapse collapse";
                this.divWSectionList.Visible = false;
                this.divAddWS.Attributes["class"] = "panel-collapse collapse in";
                this.divWSectionEdit.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWSClear_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable wsTable = DMG.getClientWellSectionTable(GetClientDBString);
                this.gvwWSList.DataSource = wsTable;
                this.gvwWSList.DataBind();
                this.gvwWSList.SelectedIndex = -1;

                this.gvwWSList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWSAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                String value = Convert.ToString(this.txtWSName.Text);
                if (!String.IsNullOrEmpty(value))
                {
                    iResult = DMG.addWellSection(value, LoginName, domain, GetClientDBString);
                    if (iResult.Equals(1))
                    {
                        this.txtWSName.Text = "";
                        this.divAlertEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iMessage.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnHeader.InnerText = " Success";
                        this.lblWSSuccess.Text = "Successfully inserted new Well Section name";
                        this.divAlertEnclosure.Visible = true;
                        this.btnWSDone.Focus();
                    }
                    else if (iResult.Equals(-1))
                    {
                        this.txtWSName.Text = "";
                        this.divAlertEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iMessage.Attributes["class"] = "icon fa fa-warning";
                        this.spnHeader.InnerText = " Warning!";
                        this.lblWSSuccess.Text = "Unable to insert duplicate value. Please provide a unique Well Section name";
                        this.divAlertEnclosure.Visible = true;
                        this.txtWSName.Focus();
                    }
                    else
                    {
                        this.txtWSName.Text = "";
                        this.divAlertEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                        this.iMessage.Attributes["class"] = "icon fa fa-warning";
                        this.spnHeader.InnerText = " Warning!";
                        this.lblWSSuccess.Text = "Unable to insert value. Please try again";
                        this.divAlertEnclosure.Visible = true;
                        this.txtWSName.Focus();
                    }
                }
                else
                {
                    this.txtWSName.Text = "";
                    this.divAlertEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iMessage.Attributes["class"] = "icon fa fa-warning";
                    this.spnHeader.InnerText = " Warning!";
                    this.lblWSSuccess.Text = "Unable to insert Null values. Please provide a valid Well Section name";
                    this.divAlertEnclosure.Visible = true;
                    this.txtWSName.Focus();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWSAddClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtWSName.Text = "";
                this.divAlertEnclosure.Visible = false;
                this.txtWSName.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWSDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnWSAddClear_Click(null, null);
                this.divWSectionList.Visible = true;
                this.divWSectionEdit.Visible = false;
                this.btnWSClear_Click(null, null);
                this.divWSList.Attributes["class"] = "panel-collapse collapse in";
                this.divAddWS.Attributes["class"] = "panel-collapse collapse";
                System.Data.DataTable wsTable = DMG.getClientWellSectionTable(GetClientDBString);
                this.gvwWSList.DataSource = wsTable;
                this.gvwWSList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWSList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable wsTable = DMG.getClientWellSectionTable(GetClientDBString);
                this.gvwWSList.PageIndex = e.NewPageIndex;
                this.gvwWSList.DataSource = wsTable;
                this.gvwWSList.DataBind();
                this.btnWSClear.CssClass = "btn btn-warning";
                this.btnWSClear.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWellAddLonO_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnWellNew.Enabled = true;
                this.btnWellNew.CssClass = "btn btn-success";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}