﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlFECDemogs_2A.ascx.cs" Inherits="SmartsVer1.Control.Config.Demogs.ctrlFECDemogs_2A" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

<link href="../../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />

<!-- JavaScripts -->
<script src="../../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>

<!--- Page local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }
    .rotate{
		-webkit-transform: rotate(90deg);  /* Chrome, Safari, Opera */
			-moz-transform: rotate(90deg);  /* Firefox */
			-ms-transform: rotate(90deg);  /* IE 9 */
				transform: rotate(90deg);  /* Standard syntax */    
    }
    /*custom anchor defi for menu items*/
    .swdMenu {
        color: #ffffff;
    }
</style>

<!--- Page local script --->
<script type="text/javascript">
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("rotate");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").addClass("rotate");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("rotate");
        });
    });
</script>

<!--- Page Menu -->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li>
                    <asp:HyperLink ID="hlnkPanel" runat="server" NavigateUrl="~/Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> System Panel</asp:HyperLink>
                </li>
                <li>Configuration</li>
                <li><a href="../../../Config/Demogs/fecCRDDemogs_2A.aspx">Demograhpics</a></li>
            </ol>
        </div>
    </div>
</div>
<!--- Page Content --->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div id="acrdMain" runat="server" class="panel-group">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left;">
                            <a id="ancWSecMain" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFECDemogs_2A_acrdMain" href="#BodyContent_ctrlFECDemogs_2A_divWSecMain">
                                <span class="glyphicon glyphicon-menu-right"></span> Well Section</a>
                        </h5>
                    </div>
                    <div id="divWSecMain" runat="server" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="upgrsWSec" runat="server" AssociatedUpdatePanelID="upnlWSec" DisplayAfter="0">
                                <ProgressTemplate>
                                    <div id="divProgSurround" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                        z-index: 100;">
                                        <asp:Image ID="upgrsWSecImage" runat="server" CssClass="center" AlternateText="Processing ...." ForeColor="Purple" ImageAlign="Middle"
                                            ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel runat="server" ID="upnlWSec">
                                <ContentTemplate>
                                    <div class="panel panel-danger panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="acrdWSection" runat="server" class="panel-group">
                                                <div class="input-group">
                                                    <div class="box-tools" style="width: 100%;">
                                                        <asp:LinkButton ID="btnWSAddNew" runat="server" CssClass="btn btn-info text-center" OnClick="btnWSAddNew_Click"><i class="fa fa-plus"> Well Section</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnWSClear" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnWSClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div class="panel panel-info">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancWSList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlAssets2_acrdWSec" href="#BodyContent_ctrlAssets2_divWSList">
                                                                Well Section List</a>
                                                        </h5>
                                                    </div>
                                                    <div id="divWSList" runat="server" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <div runat="server" id="divWSectionList">
                                                                <asp:GridView ID="gvwWSList" runat="server" EmptyDataText="No Well Sections defined"
                                                                    ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="wsID" AllowPaging="True" AllowSorting="True" CssClass="mydatagrid"
                                                                    OnPageIndexChanging="gvwWSList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15"
                                                                    HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="wsID" HeaderText="welID" Visible="false" />
                                                                        <asp:BoundField DataField="wsName" HeaderText="Well Section Name" />
                                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" ReadOnly="True" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-success">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancWSAdd" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFECDemogs_2A_acrdWSec" href="#BodyContent_ctrlFECDemogs_2A_divAddWS">
                                                                Add Well Section</a>
                                                        </h5>
                                                    </div>
                                                    <div id="divAddWS" runat="server" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <div runat="server" id="divWSectionEdit" visible="false">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Section Name</span>
                                                                    <asp:TextBox ID="txtWSName" runat="server" CssClass="form-control" CausesValidation="True" AutoComplete="off"
                                                                        placeholder="Well Section Name (required)" />
                                                                </div>
                                                                <div runat="server" id="divAlertEnclosure" visible="false">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                    <h4><i runat="server" id="iMessage"><span runat="server" id="spnHeader"></span></i></h4>
                                                                    <asp:Label ID="lblWSSuccess" runat="server" CssClass="lblContact" />
                                                                </div>
                                                                <div class="input-group" style="width: 100%;">
                                                                    <asp:LinkButton ID="btnWSAdd" runat="server" CssClass="btn btn-success text-center" OnClick="btnWSAdd_Click"><i class="fa fa-plus"> Well Section</i></asp:LinkButton>
                                                                    <asp:LinkButton ID="btnWSAddClear" runat="server" CssClass="btn btn-warning text-center" OnClick="btnWSAddClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                    <asp:LinkButton ID="btnWSDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnWSDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left;">
                            <a id="ancWell" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFECDemogs_2A_acrdMain" href="#BodyContent_ctrlFECDemogs_2A_divWellMain">
                                <span class="glyphicon glyphicon-menu-right"></span> Well / Lateral</a>
                        </h5>
                    </div>
                    <div id="divWellMain" runat="server" class="panel-collapse collapse">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="upgrsDmgWell" runat="server" AssociatedUpdatePanelID="upnlDmgWell" DisplayAfter="0">
                                <ProgressTemplate>
                                    <div id="divDmgWellSurround" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                        z-index: 100;">
                                        <asp:Image ID="upgrsDmgWellImage" runat="server" CssClass="center" AlternateText="Processing ...." ForeColor="Purple" ImageAlign="Middle"
                                            ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel runat="server" ID="upnlDmgWell">
                                <ContentTemplate>
                                    <div class="panel panel-danger panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="acrdWellList" runat="server" class="panel-group">
                                                <div class="input-group">
                                                    <div class="box-tools" style="width: 100%;">
                                                        <asp:LinkButton ID="btnWellAdd" runat="server" CssClass="btn btn-info text-center" OnClick="btnWellAdd_Click"><i class="fa fa-plus"> Well</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnWellClear" runat="server" CssClass="btn btn-default text-center" OnClick="btnWellClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div class="panel panel-info">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancWList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFECDemogs_2A_acrdWellList" href="#BodyContent_ctrlFECDemogs_2A_divWList">
                                                                Well(s) List</a>
                                                        </h5>
                                                    </div>
                                                    <div id="divWList" runat="server" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">Geographic Region</span>
                                                                        <asp:DropDownList ID="ddlWRegion" runat="server" CssClass="form-control select2" AutoPostBack="true" AppendDataBoundItems="true"
                                                                            OnSelectedIndexChanged="ddlWRegion_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select Major Geographic Region ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">Sub-Region</span>
                                                                        <asp:DropDownList ID="ddlWSRegion" runat="server" CssClass="form-control select2" AutoPostBack="true" AppendDataBoundItems="true"
                                                                            Enabled="false"
                                                                            OnSelectedIndexChanged="ddlWSRegion_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select Geographic Sub-Region ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">Country/Nation</span>
                                                                        <asp:DropDownList ID="ddlWCountry" runat="server" CssClass="form-control select2" AutoPostBack="true" AppendDataBoundItems="true"
                                                                            Enabled="false"
                                                                            OnSelectedIndexChanged="ddlWCountry_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select Country/Nation ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">State/Province</span>
                                                                        <asp:DropDownList ID="ddlWState" runat="server" CssClass="form-control select2" AutoPostBack="true" AppendDataBoundItems="true"
                                                                            Enabled="false"
                                                                            OnSelectedIndexChanged="ddlWState_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select State/Province ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">County/District</span>
                                                                        <asp:DropDownList ID="ddlWCounty" runat="server" CssClass="form-control select2" AutoPostBack="true" AppendDataBoundItems="true"
                                                                            Enabled="false"
                                                                            OnSelectedIndexChanged="ddlWCounty_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select County/District/Municipality ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">O&G Field</span>
                                                                        <asp:DropDownList ID="ddlWField" runat="server" CssClass="form-control select2" AutoPostBack="true" AppendDataBoundItems="true"
                                                                            Enabled="false"
                                                                            OnSelectedIndexChanged="ddlWField_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select Oil & Gas Field ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <asp:GridView ID="gvwWellList" runat="server" EmptyDataText="No Well found." ShowHeaderWhenEmpty="true" AutoGenerateColumns="False"
                                                                    DataKeyNames="welID" AllowPaging="True" AllowSorting="True" CssClass="mydatagrid"
                                                                    OnPageIndexChanging="gvwWellList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15"
                                                                    HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnSelectedIndexChanged="gvwWellList_SelectedIndexChanged"
                                                                    Visible="false">
                                                                    <Columns>
                                                                        <asp:CommandField ShowSelectButton="True" SelectText="Select" ShowCancelButton="True" ItemStyle-ForeColor="Blue"></asp:CommandField>
                                                                        <asp:BoundField DataField="welID" HeaderText="welID" Visible="false" />
                                                                        <asp:BoundField DataField="welName" HeaderText="Well Name" />
                                                                        <asp:BoundField DataField="welClient" HeaderText="Service Provider" />
                                                                        <asp:BoundField DataField="WelOptr" HeaderText="Well Owner/Operator" />
                                                                        <asp:BoundField DataField="welCnty" HeaderText="County" />
                                                                        <asp:BoundField DataField="welSt" HeaderText="State" />
                                                                        <asp:BoundField DataField="welCty" HeaderText="Country" />
                                                                        <asp:BoundField DataField="welSR" HeaderText="Sub Region" />
                                                                        <asp:BoundField DataField="welR" HeaderText="Region" />
                                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" ReadOnly="True" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                                <asp:GridView ID="gvwWellDetailList" runat="server" EmptyDataText="No Well Details available" ShowHeaderWhenEmpty="true"
                                                                    AutoGenerateColumns="false" DataKeyNames="welID" CssClass="mydatagrid" HeaderStyle-CssClass="header" RowStyle-CssClass="rows"
                                                                    Visible="false">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="welID" Visible="false" />
                                                                        <asp:BoundField DataField="welLat" HeaderText="Decimal Latitude" />
                                                                        <asp:BoundField DataField="welLng" HeaderText="Decimal Longitude" />
                                                                        <asp:BoundField DataField="welLatDMS" HeaderText="Degrees Minutes Seconds (DMS) Latitude" />
                                                                        <asp:BoundField DataField="welLngDMS" HeaderText="Degrees Minutes Seconds (DMS) Longitude" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-success">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancWEdit" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFECDemogs_2A_acrdWellList" href="#BodyContent_ctrlFECDemogs_2A_divWEdit">
                                                                Add New Well / Lateral</a>
                                                        </h5>
                                                    </div>
                                                    <div runat="server" id="divWEdit" visible="false">
                                                        <div class="panel-body">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">Geographic Region</span>
                                                                        <asp:DropDownList ID="ddlWARegion" runat="server" AutoPostBack="true" AppendDataBoundItems="true" CssClass="form-control"
                                                                            OnSelectedIndexChanged="ddlWARegion_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select Major Geographic Region ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">Sub-Region</span>
                                                                        <asp:DropDownList ID="ddlWASRegion" runat="server" AutoPostBack="true" AppendDataBoundItems="true" Enabled="false" CssClass="form-control"
                                                                            OnSelectedIndexChanged="ddlWASRegion_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select Geographic Sub-Region ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">Country/Nation</span>
                                                                        <asp:DropDownList ID="ddlWACountry" runat="server" AutoPostBack="true" AppendDataBoundItems="true" Enabled="false" CssClass="form-control"
                                                                            OnSelectedIndexChanged="ddlWACountry_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select Country/Nation ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">State/Province</span>
                                                                        <asp:DropDownList ID="ddlWAState" runat="server" AutoPostBack="true" AppendDataBoundItems="true" Enabled="false" CssClass="form-control"
                                                                            OnSelectedIndexChanged="ddlWAState_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select State/Province ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">County/District</span>
                                                                        <asp:DropDownList ID="ddlWACounty" runat="server" AutoPostBack="true" AppendDataBoundItems="true" Enabled="false" CssClass="form-control"
                                                                            OnSelectedIndexChanged="ddlWACounty_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select County/District/Municipality ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">O&G Field</span>
                                                                        <asp:DropDownList ID="ddlWAField" runat="server" AutoPostBack="true" AppendDataBoundItems="true" Enabled="false" CssClass="form-control"
                                                                            OnSelectedIndexChanged="ddlWAField_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select Oil & Gas Field ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">Operator</span>
                                                                        <asp:DropDownList ID="ddlWAOPC" runat="server" AppendDataBoundItems="True" AutoPostBack="True" Enabled="false" CssClass="form-control"
                                                                            OnSelectedIndexChanged="ddlWAOPC_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select Well Operator Company ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">Well Name</span>
                                                                        <asp:TextBox ID="txtWAName" runat="server" CssClass="form-control" Enabled="False" CausesValidation="True"
                                                                            AutoComplete="off" placeholder="Well Name (required)" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">UWI</span>
                                                                        <asp:TextBox ID="txtWAUWI" runat="server" CssClass="form-control" Enabled="False" AutoComplete="off" placeholder="Universal Well Identifier (UWI)" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">API</span>
                                                                        <asp:TextBox ID="txtWAAPI" runat="server" CssClass="form-control" Enabled="False" AutoComplete="off" placeholder="American Petroleum Insitutue Identifier (API)" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">Well Permit</span>
                                                                        <asp:TextBox ID="txtWAPermit" runat="server" CssClass="form-control" Enabled="False" CausesValidation="True"
                                                                            AutoComplete="off" placeholder="Well Permit Number" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                    <div class="input-group date">
                                                                        <span class="input-group-addon bg-light-blue">Spud Date</span>
                                                                        <asp:TextBox runat="server" ID="datepicker" CssClass="date-picker form-control" data-date-format="mm/dd/yyy" Enabled="false"
                                                                            AutoComplete="off" TextMode="Date" placeholder="Spud Date" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">Latitude</span>
                                                                        <asp:TextBox ID="txtWALat" runat="server" CssClass="form-control" CausesValidation="True" Enabled="false" Width="50%"
                                                                            AutoComplete="off" placeholder="Latitude (99.999999) - Range -90 and 90 degrees" />
                                                                        <asp:RangeValidator ID="rngLatitude" runat="server" ControlToValidate="txtWALat" MaximumValue="90.00" MinimumValue="-90.00"
                                                                            Type="Double" SetFocusOnError="True" ErrorMessage="*" Font-Bold="True" Display="Dynamic" />
                                                                        <asp:DropDownList ID="ddlWellAddLatO" runat="server" AppendDataBoundItems="false" AutoPostBack="true" CssClass="form-control"
                                                                            Enabled="false" Width="50%">
                                                                            <asp:ListItem Value="0" Text="--- Select Direction ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">Longitude</span>
                                                                        <asp:TextBox ID="txtWALon" runat="server" CssClass="form-control" CausesValidation="True" Enabled="false" Width="50%"
                                                                            AutoComplete="off" placeholder="Longitude (99.999999) - Range -180 and 180 degrees" />
                                                                        <asp:RangeValidator ID="rngLongitude" runat="server" ControlToValidate="txtWALon" MaximumValue="180.00" MinimumValue="-180.00"
                                                                            Type="Double" SetFocusOnError="True" ErrorMessage="*" Font-Bold="True" Display="Dynamic" />
                                                                        <asp:DropDownList ID="ddlWellAddLonO" runat="server" AppendDataBoundItems="false" AutoPostBack="true" CssClass="form-control"
                                                                            Width="50%" Enabled="false" OnSelectedIndexChanged="ddlWellAddLonO_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select Direction ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="text-center" style="width: 100%;">
                                                                    <div runat="server" id="divWellEnclosure" visible="false">
                                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                        <h4><i runat="server" id="iWellMessage"><span runat="server" id="spnWellAdd"></span></i></h4>
                                                                        <asp:Label ID="lblWASuccess" runat="server" CssClass="lblContact" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                    <div class="input-group" style="width: 100%;">
                                                                        <asp:LinkButton ID="btnWellNew" runat="server" CssClass="btn btn-default text-center" OnClick="btnWellNew_Click"><i class="fa fa-add"> Well</i></asp:LinkButton>
                                                                        <asp:LinkButton ID="btnWellNewCancel" runat="server" CssClass="btn btn-default text-center" OnClick="btnWellNewCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                        <asp:LinkButton ID="btnWellAddDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnWellAddDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>