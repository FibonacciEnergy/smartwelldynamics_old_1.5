﻿using System;
using System.Data;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI.WebControls;
using CLR = System.Drawing.Color;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;

namespace SmartsVer1.Control.Config.Demogs
{
    public partial class ctrlFEDRDropDowns : System.Web.UI.UserControl
    {
        String LoginName, username, domain;
        Int32 ClientID = -99;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);
                UP usrP = UP.GetUserProfile(LoginName);
                username = addr.User;
                domain = addr.Host;
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
                //Configuring page dropdownlists
                DataTable tableBTQ = DMG.getBTQualifierTable();
                DataTable tableGTQ = DMG.getGTQualifierTable();
                DataTable tableDipQ = DMG.getDipQualifierTable();
                DataTable tableColorType = DMG.getColorTypeTable();
                DataTable tableDataEntry = DMG.getDataEntryTable();
                DataTable tableDemogType = DMG.getDemogTypeTable();
                DataTable tableIFR = DMG.getIFRValueTable();
                DataTable tableOPR = DMG.getOprStatusTable();
                DataTable tableSCApp = DMG.getShortCollarAppTable();
                DataTable tableDStat = DMG.getStatusTable();
                DataTable wpiTable = DMG.getWellProfileIterationTable();
                DataTable csqcTable = DMG.getCorrSQCTable();

                //Loading GridViews on page
                if (!this.IsPostBack)
                {
                    this.gvwBTQList.DataSource = tableBTQ;
                    this.gvwBTQList.PageIndex = 0;
                    this.gvwBTQList.SelectedIndex = -1;
                    this.gvwBTQList.DataBind();
                    this.gvwGTQList.DataSource = tableGTQ;
                    this.gvwGTQList.PageIndex = 0;
                    this.gvwGTQList.SelectedIndex = -1;
                    this.gvwGTQList.DataBind();
                    this.gvwDipQList.DataSource = tableDipQ;
                    this.gvwDipQList.PageIndex = 0;
                    this.gvwDipQList.SelectedIndex = -1;
                    this.gvwDipQList.DataBind();
                    this.gvwColorTypeList.DataSource = tableColorType;
                    this.gvwColorTypeList.PageIndex = 0;
                    this.gvwColorTypeList.SelectedIndex = -1;
                    this.gvwColorTypeList.DataBind();
                    this.gvwDataEntryList.DataSource = tableDataEntry;
                    this.gvwDataEntryList.PageIndex = 0;
                    this.gvwDataEntryList.SelectedIndex = -1;
                    this.gvwDataEntryList.DataBind();
                    this.gvwDemogTypeList.DataSource = tableDemogType;
                    this.gvwDemogTypeList.PageIndex = 0;
                    this.gvwDemogTypeList.SelectedIndex = -1;
                    this.gvwDemogTypeList.DataBind();
                    this.gvwIFRList.DataSource = tableIFR;
                    this.gvwIFRList.PageIndex = 0;
                    this.gvwIFRList.SelectedIndex = -1;
                    this.gvwIFRList.DataBind();                    
                    this.gvwOprList.DataSource = tableOPR;
                    this.gvwOprList.PageIndex = 0;
                    this.gvwOprList.SelectedIndex = -1;
                    this.gvwOprList.DataBind();
                    this.gvwSCAppList.DataSource = tableSCApp;
                    this.gvwSCAppList.PageIndex = 0;
                    this.gvwSCAppList.SelectedIndex = -1;
                    this.gvwSCAppList.DataBind();
                    this.gvwStatList.DataSource = tableDStat;
                    this.gvwStatList.PageIndex = 0;
                    this.gvwStatList.SelectedIndex = -1;
                    this.gvwStatList.DataBind();
                    this.gvwWPIList.DataSource = wpiTable;
                    this.gvwWPIList.PageIndex = 0;
                    this.gvwWPIList.SelectedIndex = -1;
                    this.gvwWPIList.DataBind();
                    this.gvwCSQC.DataSource = csqcTable;
                    this.gvwCSQC.PageIndex = 0;
                    this.gvwCSQC.SelectedIndex = -1;
                    this.gvwCSQC.DataBind();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {}
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        /// <summary>
        /// BT Qualifier List
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvwBTQList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable btqTable = DMG.getBTQualifierTable();
                this.gvwBTQList.DataSource = btqTable;
                this.gvwBTQList.PageIndex = e.NewPageIndex;
                this.gvwBTQList.SelectedIndex = -1;
                this.gvwBTQList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnBTQAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnAddBTQCancel_Click(null, null);
                this.divBTQList.Attributes["class"] = "panel-collapse collapse";
                this.divBTQAdd.Attributes["class"] = "panel-collapse collapse in";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddBTQ_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                String value = String.Empty;
                value = Convert.ToString(this.txtAddBTQ.Text);
                if (String.IsNullOrEmpty(value))
                {
                    this.txtAddBTQ.Text = "";
                    this.enBTQ.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iBTQ.Attributes["class"] = "icon fa fa-warning";
                    this.spnBTQ.InnerText = " Missing information!";
                    this.lblBTQAdd.Text = "Empty or Null value detected. Please provide a value";
                    this.enBTQ.Visible = true;                    
                }
                else
                {
                    iResult = DMG.addBTQ(value, LoginName, domain);
                    if (iResult.Equals(1))
                    {
                        this.txtAddBTQ.Text = "";
                        this.enBTQ.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iBTQ.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnBTQ.InnerText = " Success!";
                        this.lblBTQAdd.Text = "Successfully inserted new statement, " + value + ", to SMARTs";
                        this.enBTQ.Visible = true;
                    }
                    else
                    {
                        this.txtAddBTQ.Text = "";
                        this.enBTQ.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iBTQ.Attributes["class"] = "icon fa fa-warning";
                        this.spnBTQ.InnerText = " Missing information!";
                        this.lblBTQAdd.Text = "Empty or Null value detected. Please provide a value";
                        this.enBTQ.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddBTQCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtAddBTQ.Text = "";
                this.enBTQ.Visible = false;
                this.lblBTQAdd.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btndivBTQDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnAddBTQCancel_Click(null, null);                
                this.divBTQList.Attributes["class"] = "panel-collapse collapse in";
                this.divBTQAdd.Attributes["class"] = "panel-collapse collapse";
                DataTable btqList = DMG.getBTQualifierTable();
                this.gvwBTQList.DataSource = btqList;
                this.gvwBTQList.PageIndex = 0;
                this.gvwBTQList.SelectedIndex = -1;
                this.gvwBTQList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        /// <summary>
        /// GT Qualifier List
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvwGTQList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable gtqTable = DMG.getGTQualifierTable();
                this.gvwGTQList.DataSource = gtqTable;
                this.gvwGTQList.PageIndex = e.NewPageIndex;
                this.gvwGTQList.SelectedIndex = -1;
                this.gvwGTQList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnGTQAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.divGTQList.Attributes["class"] = "panel-collapse collapse";
                this.divGTQAdd.Attributes["class"] = "panel-collapse collapse in";
                this.btnGTQAddCancel_Click(null, null);                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnGTQAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                String value = String.Empty;
                value = Convert.ToString(this.txtAddGTQ.Text);
                if (String.IsNullOrEmpty(value))
                {
                    this.txtAddGTQ.Text = "";
                    this.enGTQ.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iGTQ.Attributes["class"] = "icon fa fa-warning";
                    this.spnGTQ.InnerText = " Missing information!";
                    this.lblGTQAdd.Text = "Empty or Null value detected. Please provide a value.";
                    this.enGTQ.Visible = true;

                    this.txtAddGTQ.Focus();
                }
                else
                {
                    iResult = DMG.addGTQ(value, LoginName, domain);
                    if (iResult.Equals(1))
                    {
                        this.txtAddGTQ.Text = "";
                        this.enGTQ.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iGTQ.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnGTQ.InnerText = " Success!";
                        this.lblGTQAdd.Text = "Successfully inserted statment to SMARTs";
                        this.enGTQ.Visible = true;
                    }
                    else
                    {
                        this.txtAddBTQ.Text = "";
                        this.enGTQ.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iGTQ.Attributes["class"] = "icon fa fa-warning";
                        this.spnGTQ.InnerText = " Warning!";
                        this.lblGTQAdd.Text = "!!! Error !!! Please try again";
                        this.enGTQ.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnGTQAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtAddGTQ.Text = "";
                this.lblGTQAdd.Text = "";
                this.enGTQ.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnGTQDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnGTQAddCancel_Click(null, null);
                this.divGTQList.Attributes["class"] = "panel-collapse collpase in";
                this.divGTQAdd.Attributes["class"] = "panel-collapse collapse";
                DataTable gtqList = DMG.getGTQualifierTable();
                this.gvwGTQList.DataSource = gtqList;
                this.gvwGTQList.PageIndex = 0;
                this.gvwGTQList.SelectedIndex = -1;
                this.gvwGTQList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        /// <summary>
        /// Dip Qualifier Statement
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvwDipQList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable dqTable = DMG.getDipQualifierTable();
                this.gvwDipQList.PageIndex = e.NewPageIndex;
                this.gvwDipQList.DataSource = dqTable;
                this.gvwDipQList.SelectedIndex = -1;
                this.gvwDipQList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDipQAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnDipQAddCancel_Click(null, null);
                this.divDipQList.Attributes["class"] = "panel-collapse collapse";
                this.divDQAdd.Attributes["class"] = "panel-collapse collapse in";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDipQAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                String value = String.Empty;
                value = Convert.ToString(this.txtAddDipQ.Text);
                if (String.IsNullOrEmpty(value))
                {
                    this.txtAddDipQ.Text = "";
                    this.encDipQ.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iDipQ.Attributes["class"] = "icon fa fa-warning";
                    this.spnDipQ.InnerText = " Warning!";
                    this.lblDipQ.Text = "Empty or Null value detected. Please provide a value";
                    this.encDipQ.Visible = true;
                }
                else
                {
                    iResult = DMG.addDipQ(value, LoginName, domain);
                    if (iResult.Equals(1))
                    {
                        this.txtAddDipQ.Text = "";
                        this.encDipQ.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iDipQ.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnDipQ.InnerText = " Success!";
                        this.lblDipQ.Text = "Successfully inserted new Value, " + value + ", to SMARTs";
                        this.encDipQ.Visible = true;
                    }
                    else
                    {
                        this.txtAddDipQ.Text = "";
                        this.encDipQ.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iDipQ.Attributes["class"] = "icon fa fa-warning";
                        this.spnDipQ.InnerText = " Warning!";
                        this.lblDipQ.Text = "!!! Error !!! Please try again";
                        this.encDipQ.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDipQAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtAddDipQ.Text = "";
                this.lblDipQ.Text = "";
                this.encDipQ.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDipQDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnDipQAddCancel_Click(null, null);
                this.divDipQList.Attributes["class"] = "panel-collapse collapse in";
                this.divDQAdd.Attributes["class"] = "panel-collapse collapse";
                DataTable dpqTable = DMG.getDipQualifierTable();
                this.gvwDipQList.DataSource = dpqTable;
                this.gvwDipQList.SelectedIndex = -1;
                this.gvwDipQList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        /// <summary>
        /// Color Type 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvwColorTypeList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable ctypTable = DMG.getColorTypeTable();                
                this.gvwColorTypeList.DataSource = ctypTable;
                this.gvwColorTypeList.PageIndex = e.NewPageIndex;
                this.gvwColorTypeList.SelectedIndex = -1;
                this.gvwColorTypeList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnColorTypeAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnColorTypeAddCancel_Click(null, null);
                this.divClrList.Attributes["class"] = "panel-collapse collapse";
                this.divClrAdd.Attributes["class"] = "panel-collapse collapse in";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnColorTypeAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                String value = String.Empty;
                value = Convert.ToString(this.txtColorTypeAdd.Text);
                if (String.IsNullOrEmpty(value))
                {
                    this.txtColorTypeAdd.Text = "";
                    this.encClr.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iCLR.Attributes["class"] = "icon fa fa-warning";
                    this.spnCLR.InnerText = " Warning!";
                    this.lblColorTypeSuccess.Text = "Empty or Null value detected. Please provide a value.";
                    this.encClr.Visible = true;

                    this.txtColorTypeAdd.Focus();
                }
                else
                {
                    iResult = DMG.addColorType(value, LoginName, domain);
                    if (iResult.Equals(1))
                    {
                        this.txtColorTypeAdd.Text = "";
                        this.encClr.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iCLR.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnCLR.InnerText = " Success!";
                        this.lblColorTypeSuccess.Text = "Successfully inserted new Value, " + value + ", to SMARTs";
                        this.encClr.Visible = true;
                    }
                    else
                    {
                        this.txtColorTypeAdd.Text = "";
                        this.encClr.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iCLR.Attributes["class"] = "icon fa fa-warning";
                        this.spnCLR.InnerText = " Warning!";
                        this.lblColorTypeSuccess.Text = "!!! Error !!! Please try again";
                        this.encClr.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnColorTypeAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtColorTypeAdd.Text = "";
                this.lblColorTypeSuccess.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnColorTypeDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnColorTypeAddCancel_Click(null, null);
                this.divClrList.Attributes["class"] = "panel-collapse collapse in";
                this.divClrAdd.Attributes["class"] = "panel-collapse collapse";
                DataTable clrTTable = DMG.getColorTypeTable();
                this.gvwColorTypeList.DataSource = clrTTable;
                this.gvwColorTypeList.PageIndex = 0;
                this.gvwColorTypeList.SelectedIndex = -1;
                this.gvwColorTypeList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        /// <summary>
        /// Data Entry Types
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvwDataEntryList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable delTable = DMG.getDataEntryTable();
                this.gvwDataEntryList.DataSource = delTable;
                this.gvwDataEntryList.PageIndex = e.NewPageIndex;
                this.gvwDataEntryList.SelectedIndex = -1;
                this.gvwDataEntryList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDataEntryAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnDataEntryAddCancel_Click(null, null);
                this.divDEList.Attributes["class"] = "panel-collapse collapse";
                this.divDEAdd.Attributes["class"] = "panel-collapse collapse in";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDataEntryAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                String value = String.Empty;
                value = Convert.ToString(this.txtDataEntryAdd.Text);
                if (String.IsNullOrEmpty(value))
                {
                    this.txtDataEntryAdd.Text = "";
                    this.encDEType.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iDE.Attributes["class"] = "icon fa fa-warning";
                    this.spnDE.InnerText = " Warning!";
                    this.lblDataEntrySuccess.Text = "Empty or Null value detected. Please provide a value";
                    this.encDEType.Visible = true;
                }
                else
                {
                    iResult = DMG.addDataEntryType(value, LoginName, domain);
                    if (iResult.Equals(1))
                    {
                        this.txtDataEntryAdd.Text = "";
                        this.encDEType.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iDE.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnDE.InnerText = " Success!";
                        this.lblDataEntrySuccess.Text = "Successfully inserted new Value, " + value + ", to SMARTs";
                        this.encDEType.Visible = true;
                    }
                    else
                    {
                        this.txtDataEntryAdd.Text = "";
                        this.encDEType.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iDE.Attributes["class"] = "icon fa fa-warning";
                        this.spnDE.InnerText = " Warning!";
                        this.lblDataEntrySuccess.Text = "!!! Error !!! Please try again";
                        this.encDEType.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDataEntryAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtDataEntryAdd.Text = "";
                this.lblDataEntrySuccess.Text = "";
                this.encDEType.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDataEntryAddDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnDataEntryAddCancel_Click(null, null);
                DataTable deTable = DMG.getDataEntryTable();
                this.gvwDataEntryList.DataSource = deTable;
                this.gvwDataEntryList.PageIndex = 0;
                this.gvwDataEntryList.SelectedIndex = -1;
                this.gvwDataEntryList.DataBind();
                this.divDEList.Attributes["class"] = "panel-collapse collapse in";
                this.divDEAdd.Attributes["class"] = "panel-collapse collapse";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        /// <summary>
        /// Demographic Type List
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>        

        protected void gvwStatList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable statTable = DMG.getDemogTypeTable();
                this.gvwStatList.DataSource = statTable;
                this.gvwStatList.PageIndex = e.NewPageIndex;
                this.gvwStatList.SelectedIndex = -1;
                this.gvwStatList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewStat_Click(object sender, EventArgs e)
        {
            try
            {
                btnStatCancel_Click(null, null);
                this.divDList.Attributes["class"] = "panel-collapse collapse";
                this.divDAdd.Attributes["class"] = "panel-collapse collapse in";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnStatAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                String value = String.Empty;
                value = Convert.ToString(this.txtStatAdd.Text);
                if (String.IsNullOrEmpty(value))
                {
                    this.txtStatAdd.Text = "";
                    this.encDStatus.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iDStat.Attributes["class"] = "icon fa fa-warning";
                    this.spnDStat.InnerText = " Warning!";
                    this.lblStatSuccess.Text = "Empty or Null value detected. Please provide a value.";
                    this.encDStatus.Visible = true;
                }
                else
                {
                    iResult = DMG.addStatus(value, LoginName, domain);
                    if (iResult.Equals(1))
                    {
                        this.txtStatAdd.Text = "";
                        this.encDStatus.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iDStat.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnDStat.InnerText = " Success!";
                        this.lblStatSuccess.Text = "Successfully inserted new Value, " + value + ", to SMARTs";
                        this.encDStatus.Visible = true;
                    }
                    else
                    {
                        this.txtStatAdd.Text = "";
                        this.encDStatus.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iDStat.Attributes["class"] = "icon fa fa-warning";
                        this.spnDStat.InnerText = " Warning!";
                        this.lblStatSuccess.Text = "!!! Error !!! Please try again";
                        this.encDStatus.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnStatCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtStatAdd.Text = "";
                this.encDStatus.Visible = false;
                this.lblStatSuccess.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnStatDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnStatCancel_Click(null, null);
                DataTable sttTable = DMG.getStatusTable();
                this.gvwStatList.DataSource = sttTable;
                this.gvwStatList.PageIndex = 0;
                this.gvwStatList.SelectedIndex = -1;
                this.gvwStatList.DataBind();
                this.divDList.Attributes["class"] = "panel-collapse collapse in";
                this.divDAdd.Attributes["class"] = "panel-collapse collapse";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        /// <summary>
        /// Demographic Location Types
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvwDemogTypeList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable dmgTable = DMG.getDemogTypeTable();
                this.gvwDemogTypeList.DataSource = dmgTable;
                this.gvwDemogTypeList.PageIndex = e.NewPageIndex;
                this.gvwDemogTypeList.SelectedIndex = -1;
                this.gvwDemogTypeList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDemogTypeAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnDemogTypeAddCancel_Click(null, null);
                this.divDMGList.Attributes["class"] = "panel-collapse collapse";
                this.divDTypeAdd.Attributes["class"] = "panel-collapse collapse in";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
        
        protected void btnDemogTypeAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                String value = String.Empty;
                value = Convert.ToString(this.txtDemogTypeAdd.Text);
                if (String.IsNullOrEmpty(value))
                {
                    this.txtDemogTypeAdd.Text = "";
                    this.enDmgType.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iDType.Attributes["class"] = "icon fa fa-warning";
                    this.spnDType.InnerText = " Warning!";
                    this.lblDemogTypeSuccess.Text = "Empty or Null value detected. Please provide a value.";
                    this.enDmgType.Visible = true;
                    
                }
                else
                {
                    iResult = DMG.addDemogType(value, LoginName, domain);
                    if (iResult.Equals(1))
                    {
                        this.txtDemogTypeAdd.Text = "";
                        this.enDmgType.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iDType.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnDType.InnerText = " Success!";
                        this.lblDemogTypeSuccess.Text = "Successfully inserted new Value, " + value + ", to SMARTs";
                        this.enDmgType.Visible = true;
                    }
                    else
                    {
                        this.txtDemogTypeAdd.Text = "";
                        this.enDmgType.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iDType.Attributes["class"] = "icon fa fa-warning";
                        this.spnDType.InnerText = " Warning!";
                        this.lblDemogTypeSuccess.Text = "!!! Error !!! Please try again";
                        this.enDmgType.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDemogTypeAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtDemogTypeAdd.Text = "";
                this.lblDemogTypeSuccess.Text = "";
                this.enDmgType.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDemogTypeDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnDemogTypeAddCancel_Click(null, null);
                DataTable dmgTable = DMG.getDemogTypeTable();
                this.gvwDemogTypeList.DataSource = dmgTable;
                this.gvwDemogTypeList.PageIndex = 0;
                this.gvwDemogTypeList.SelectedIndex = -1;
                this.gvwDemogTypeList.DataBind();
                this.divDMGList.Attributes["class"] = "panel-collapse collapse in";
                this.divDTypeAdd.Attributes["class"] = "panel-collapse collapse";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        /// <summary>
        /// Operating Status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvwOprList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable oprTable = DMG.getDemogTypeTable();
                this.gvwOprList.DataSource = oprTable;
                this.gvwOprList.PageIndex = e.NewPageIndex;
                this.gvwOprList.SelectedIndex = -1;
                this.gvwOprList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddOpr_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnOprCancel_Click(null, null);
                this.divOPStatList.Attributes["class"] = "panel-collapse collapse";
                this.divOPStatAdd.Attributes["class"] = "panel-collapse collapse in";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnOprAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                String value = String.Empty;
                value = Convert.ToString(this.txtOprAdd.Text);
                if (String.IsNullOrEmpty(value))
                {
                    this.txtOprAdd.Text = "";
                    this.encOpr.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iOPSTT.Attributes["class"] = "icon fa fa-warning";
                    this.spnOPSTT.InnerText = " Warning!";
                    this.lblOprSuccess.Text = "Empty or Null value detected. Please provide a value.";
                    this.encOpr.Visible = true;

                    this.txtOprAdd.Focus();
                }
                else
                {
                    iResult = DMG.addOprStatus(value, LoginName, domain);
                    if (iResult.Equals(1))
                    {
                        this.txtOprAdd.Text = "";
                        this.encOpr.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iOPSTT.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnOPSTT.InnerText = " Success!";
                        this.lblOprSuccess.Text = "Successfully inserted new Value, " + value + ", to SMARTs"; 
                        this.encOpr.Visible = true;
                    }
                    else
                    {
                        this.txtOprAdd.Text = "";
                        this.encOpr.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iOPSTT.Attributes["class"] = "icon fa fa-warning";
                        this.spnOPSTT.InnerText = " Warning!";
                        this.lblOprSuccess.Text = "!!! Error !!! Please try again";
                        this.encOpr.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnOprCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtOprAdd.Text = "";
                this.lblOprSuccess.Text = "";
                this.encOpr.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnOprDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnOprCancel_Click(null, null);
                DataTable oprTable = DMG.getOprStatusTable();
                this.gvwOprList.DataSource = oprTable;
                this.gvwOprList.PageIndex = 0;
                this.gvwOprList.SelectedIndex = -1;
                this.gvwOprList.DataBind();
                this.divOPStatList.Attributes["class"] = "panel-collapse collapse in";
                this.divOPStatAdd.Attributes["class"] = "panel-collapse collapse";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        /// <summary>
        /// IFR List
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvwIFRList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable ifrTable = DMG.getDemogTypeTable();                
                this.gvwIFRList.DataSource = ifrTable;
                this.gvwIFRList.PageIndex = e.NewPageIndex;
                this.gvwIFRList.SelectedIndex = -1;
                this.gvwIFRList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }              
        
        /// <summary>
        /// Short-Collar Application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvwSCAppList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable scaTable = DMG.getDemogTypeTable();
                this.gvwSCAppList.PageIndex = e.NewPageIndex;
                this.gvwSCAppList.DataSource = scaTable;
                this.gvwSCAppList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
                
        /// <summary>
        /// Correction Survey Qualification Criteria
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void gvwCSQC_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable csqcTable = DMG.getCorrSQCTable();
                this.gvwCSQC.DataSource = csqcTable;
                this.gvwCSQC.PageIndex = e.NewPageIndex;
                this.gvwCSQC.SelectedIndex = -1;
                this.gvwCSQC.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewCSQC_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnClearSQC_Click(null, null);
                this.divCSQCList.Attributes["class"] = "panel-collapse collapse";
                this.divCSQCAdd.Attributes["class"] = "panel-collapse collapse in";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddSQC_Click(object sender, EventArgs e)
        {
            try
            {
                this.enCSQC.Attributes["class"] = "alert alert-warning alert-dismissable";
                this.iCSQC.Attributes["class"] = "icon fa fa-warning";
                this.spnCSQC.InnerText = " Warning!";
                this.lblCSQC.Text = "!!! Error !!! Not yet implemented";
                this.enCSQC.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnClearSQC_Click(object sender, EventArgs e)
        {
            try
            {
                this.enCSQC.Visible = false;
                this.lblCSQC.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDoneSQC_Click(object sender, EventArgs e)
        {
            try
            {
                this.divCSQCList.Attributes["class"] = "panel-collapse collapse in";
                this.divCSQCAdd.Attributes["class"] = "panel-collapse collapse";
                System.Data.DataTable csqcTable = DMG.getCorrSQCTable();
                this.gvwCSQC.DataSource = csqcTable;
                this.gvwCSQC.PageIndex = 0;
                this.gvwCSQC.SelectedIndex = -1;
                this.gvwCSQC.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        
    }
}