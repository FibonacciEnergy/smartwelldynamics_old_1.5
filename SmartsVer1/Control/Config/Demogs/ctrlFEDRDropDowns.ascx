﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlFEDRDropDowns.ascx.cs" Inherits="SmartsVer1.Control.Config.Demogs.ctrlFEDRDropDowns" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<!-- Stylesheet -->
<link href="../../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript -->
<script src="../../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<!--- Page local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }

    .rotate {
        -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
        -moz-transform: rotate(90deg); /* Firefox */
        -ms-transform: rotate(90deg); /* IE 9 */
        transform: rotate(90deg); /* Standard syntax */
    }

    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper[data-stepper-steps="8"] li {
            width: 12%;
        }

        ol.stepper[data-stepper-steps="9"] li {
            width: 11%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }    
</style>
<!--- Page local script --->
<script type="text/javascript">
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("rotate");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").addClass("rotate");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("rotate");
        });
    });
</script>
<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">        
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>System Configuration</li>
                <li><a href="ctrlFEDRDropDowns.ascx"> DropDowns Parameters</a></li>
            </ol>
        </div>
    </div>    
</div>
<!-- Page Content -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div id="acrdMain" runat="server" class="panel-group">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancBTQual" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdMain" href="#BodyContent_ctrlFEDRDropDowns_divBTQual">
                                <span class="glyphicon glyphicon-menu-right"></span> B-Total Qualifier</a>
                        </h5>
                    </div>
                    <div id="divBTQual" runat="server" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="uprgrsBTQ" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlBTQ">
                                <ProgressTemplate>
                                    <div id="divBTQPrgrss" runat="server" class="updateProgress">
                                        <asp:Image ID="imgBTQProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel ID="upnlBTQ" runat="server">
                                <ContentTemplate>
                                    <div class="input-group">
                                        <asp:LinkButton ID="btnBTQAdd" runat="server" CssClass="btn btn-info" OnClick="btnBTQAdd_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                    </div>
                                    <div class="panel-group" runat="server" id="acrdBTQ">
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h5 class="panel-title" style="text-align: left">
                                                    <a id="ancBTQList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdBTQ" href="#BodyContent_ctrlFEDRDropDowns_divBTQList">
                                                        B-Total Qualifier List</a>
                                                </h5>
                                            </div>
                                            <div id="divBTQList" runat="server" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <asp:GridView ID="gvwBTQList" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                        AllowPaging="True" AllowSorting="True" DataKeyNames="btqID" OnPageIndexChanging="gvwBTQList_PageIndexChanging"
                                                        AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                        RowStyle-CssClass="rows">
                                                        <Columns>
                                                            <asp:BoundField DataField="btqID" HeaderText="btqID" Visible="False" />
                                                            <asp:BoundField DataField="btqValue" HeaderText="B-Total Qualifier" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-success">
                                            <div class="panel-heading">
                                                <h5 class="panel-title" style="text-align: left">
                                                    <a id="ancBTQAdd" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdBTQ" href="#BodyContent_ctrlFEDRDropDowns_divBTQAdd">
                                                        Add B-Total Qualifier</a>
                                                </h5>
                                            </div>
                                            <div id="divBTQAdd" runat="server" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div>
                                                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                            <asp:Label ID="lblAddBTQ" runat="server" CssClass="control-label" for="txtAddBTQ" Font-size="Large" Text="B-Total Qualifier Statement" />
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                                                            <asp:TextBox ID="txtAddBTQ" runat="server" CssClass="form-control" Font-Size="Large" Text="" placeholder="B-Total Qualifier Statement" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div id="enBTQ" runat="server" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iBTQ"><span runat="server" id="spnBTQ"></span></i></h4>
                                                            <asp:Label ID="lblBTQAdd" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <asp:LinkButton ID="btnAddBTQ" runat="server" CssClass="btn btn-success text-center" OnClick="btnAddBTQ_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnAddBTQCancel" runat="server" CssClass="btn btn-warning text-center" OnClick="btnAddBTQCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btndivBTQDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btndivBTQDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancGTQual" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdMain" href="#BodyContent_ctrlFEDRDropDowns_divGTQ">
                                <span class="glyphicon glyphicon-menu-right"></span> G-Total Qualifier</a>
                        </h5>
                    </div>
                    <div id="divGTQ" runat="server" class="panel-collapse collapse">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="uprgrsGTQ" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlGTQ">
                                <ProgressTemplate>
                                    <div id="divGTQPrgrss" runat="server" class="updateProgress">
                                        <asp:Image ID="imgGTProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel ID="upnlGTQ" runat="server">
                                <ContentTemplate>
                                    <div class="panel-group" runat="server" id="acrdGTQ">
                                        <div class="input-group">
                                            <asp:LinkButton ID="btnGTQAdd" runat="server" CssClass="btn btn-info text-center" OnClick="btnGTQAdd_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                        </div>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h5 class="panel-title" style="text-align: left">
                                                    <a id="ancGTList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdGTQ" href="#BodyContent_ctrlFEDRDropDowns_divGTQList">
                                                        G-Total Qualifier List</a>
                                                </h5>
                                            </div>
                                            <div id="divGTQList" runat="server" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <asp:GridView ID="gvwGTQList" runat="server" AutoGenerateColumns="False" DataKeyNames="gtqID"
                                                        AllowPaging="True" AllowSorting="True" CssClass="mydatagrid" OnPageIndexChanging="gvwGTQList_PageIndexChanging"
                                                        AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                        RowStyle-CssClass="rows">
                                                        <Columns>
                                                            <asp:BoundField DataField="gtqID" HeaderText="gtqID" Visible="False" />
                                                            <asp:BoundField DataField="gtqValue" HeaderText="G-Total Qualifier" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-success">
                                            <div class="panel-heading">
                                                <h5 class="panel-title" style="text-align: left">
                                                    <a id="ancGTQAdd" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdGTQ" href="#BodyContent_ctrlFEDRDropDowns_divGTQAdd">
                                                        Add G-Total Qualifier</a>
                                                </h5>
                                            </div>
                                            <div id="divGTQAdd" runat="server" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                        <asp:Label ID="lblAddGTQ" runat="server" CssClass="control-label" Font-Size="Large" for="txtAddGTQ" Text="G-Total Qualifier Statement" />
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                                                        <asp:TextBox ID="txtAddGTQ" runat="server" CssClass="form-control" Font-Size="Large" Text="" placeholder="G-Total Qualifier Statement" />
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div id="enGTQ" runat="server" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iGTQ"><span runat="server" id="spnGTQ"></span></i></h4>
                                                            <asp:Label ID="lblGTQAdd" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <asp:LinkButton ID="btnGTQAddNew" runat="server" CssClass="btn btn-success text-center" OnClick="btnGTQAddNew_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnGTQAddCancel" runat="server" CssClass="btn btn-warning text-center" OnClick="btnGTQAddCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnGTQDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnGTQDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancDipQ" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdMain" href="#BodyContent_ctrlFEDRDropDowns_divDipQ">
                                <span class="glyphicon glyphicon-menu-right"></span> Dip Qualifier</a>
                        </h5>
                    </div>
                    <div id="divDipQ" runat="server" class="panel-collapse collapse">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="uprgrsDipQ" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlDipQ">
                                <ProgressTemplate>
                                    <div id="divDipQPrgrss" runat="server" class="updateProgress">
                                        <asp:Image ID="imgDipQProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel ID="upnlDipQ" runat="server">
                                <ContentTemplate>
                                    <div class="panel-group" runat="server" id="acrdDipQ">
                                        <div class="input-group">
                                            <asp:LinkButton ID="btnDipQAdd" runat="server" CssClass="btn btn-info text-center" OnClick="btnDipQAdd_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                        </div>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h5 class="panel-title" style="text-align: left">
                                                    <a id="ancDQList" runat="server" data-toggle="collapse" class="disabled" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdDipQ"
                                                        href="#BodyContent_ctrlFEDRDropDowns_divDipQList">Dip Qualifiers List</a>
                                                </h5>
                                            </div>
                                            <div id="divDipQList" runat="server" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <asp:GridView ID="gvwDipQList" runat="server" AutoGenerateColumns="False" DataKeyNames="dpqID"
                                                        AllowPaging="True" AllowSorting="True" CssClass="mydatagrid" EmptyDataText="No Dip Qualifier description found."
                                                        ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwDipQList_PageIndexChanging"
                                                        AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                        RowStyle-CssClass="rows">
                                                        <Columns>
                                                            <asp:BoundField DataField="dpqID" HeaderText="dpqID" Visible="False" />
                                                            <asp:BoundField DataField="dpqValue" HeaderText="Dip Qualifier" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-success">
                                            <div class="panel-heading">
                                                <h5 class="panel-title" style="text-align: left">
                                                    <a id="ancDQAdd" runat="server" data-toggle="collapse" class="disabled" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdDipQ"
                                                        href="#BodyContent_ctrlFEDRDropDowns_divDipQAdd">Add Dip Qualifier</a>
                                                </h5>
                                            </div>
                                            <div id="divDQAdd" runat="server" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                        <asp:Label ID="lblAddDipQ" runat="server" CssClass="control-label" Font-Size="Large" for="txtAddDipQ" Text="Dip Qualifier Statement" />
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                                                        <asp:TextBox ID="txtAddDipQ" runat="server" CssClass="form-control" Font-Size="Large" Text="" placeholder="Dip Qualifier Statement" />
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div id="encDipQ" runat="server" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iDipQ"><span runat="server" id="spnDipQ"></span></i></h4>
                                                            <asp:Label ID="lblDipQ" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <asp:LinkButton ID="btnDipQAddNew" runat="server" CssClass="btn btn-success text-center" OnClick="btnDipQAddNew_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnDipQAddCancel" runat="server" CssClass="btn btn-warning text-center" OnClick="btnDipQAddCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnDipQDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnDipQDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancClrType" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdMain" href="#BodyContent_ctrlFEDRDropDowns_divClrType">
                                <span class="glyphicon glyphicon-menu-right"></span> Logo Color Type</a>
                        </h5>
                    </div>
                    <div id="divClrType" runat="server" class="panel-collapse collapse">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="uprgrsClr" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlClr">
                                <ProgressTemplate>
                                    <div id="divClrPrgrss" runat="server" class="updateProgress">
                                        <asp:Image ID="imgClrProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel ID="upnlClr" runat="server">
                                <ContentTemplate>
                                    <div class="panel-group" runat="server" id="acrdClr">
                                        <div class="input-group">
                                            <asp:LinkButton ID="btnColorTypeAdd" runat="server" CssClass="btn btn-info text-center" OnClick="btnColorTypeAdd_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                        </div>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h5 class="panel-title" style="text-align: left">
                                                    <a id="ancClrList" runat="server" data-toggle="collapse" class="disabled" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdClr"
                                                        href="#BodyContent_ctrlFEDRDropDowns_divClrList">Log Color Type List</a>
                                                </h5>
                                            </div>
                                            <div id="divClrList" runat="server" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <asp:GridView ID="gvwColorTypeList" runat="server" AutoGenerateColumns="False" DataKeyNames="lclrID" AllowPaging="True"
                                                        AllowSorting="True" CssClass="mydatagrid" EmptyDataText="No Image Types found" ShowHeaderWhenEmpty="true" OnPageIndexChanging="gvwColorTypeList_PageIndexChanging"
                                                        AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                        RowStyle-CssClass="rows">
                                                        <Columns>
                                                            <asp:BoundField DataField="lclrID" HeaderText="lclrID" Visible="False" />
                                                            <asp:BoundField DataField="lclrValue" HeaderText="Color Type" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-success">
                                            <div class="panel-heading">
                                                <h5 class="panel-title" style="text-align: left">
                                                    <a id="ancClrAdd" runat="server" data-toggle="collapse" class="disabled" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdClr"
                                                        href="#BodyContent_ctrlFEDRDropDowns_divClrAdd">Add Log Color Type</a>
                                                </h5>
                                            </div>
                                            <div id="divClrAdd" runat="server" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                        <asp:Label ID="lblColorTypeHeading" runat="server" CssClass="control-label" Font-Size="Large" for="txtColorTypeAdd" Text="Logo Color Type" />
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                                                        <asp:TextBox ID="txtColorTypeAdd" runat="server" CssClass="form-control" Font-Size="Large" Text="" placeholder="Logo Color Type" />
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div id="encClr" runat="server" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iCLR"><span runat="server" id="spnCLR"></span></i></h4>
                                                            <asp:Label ID="lblColorTypeSuccess" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <asp:LinkButton ID="btnColorTypeAddNew" runat="server" CssClass="btn btn-success text-center" OnClick="btnColorTypeAddNew_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnColorTypeAddCancel" runat="server" CssClass="btn btn-warning text-center" OnClick="btnColorTypeAddCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnColorTypeDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnColorTypeDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancDE" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdMain" href="#BodyContent_ctrlFEDRDropDowns_divDE">
                                <span class="glyphicon glyphicon-menu-right"></span> Data Entry Type</a>
                        </h5>
                    </div>
                    <div id="divDE" runat="server" class="panel-collapse collapse">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="uprgrsDE" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlDE">
                                <ProgressTemplate>
                                    <div id="divDEPrgrss" runat="server" class="updateProgress">
                                        <asp:Image ID="imgDEProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel ID="upnlDE" runat="server">
                                <ContentTemplate>
                                    <div class="panel-group" runat="server" id="acrdDE">
                                        <div class="input-group">
                                            <asp:LinkButton ID="btnDataEntryAdd" runat="server" CssClass="btn btn-info text-center" OnClick="btnDataEntryAdd_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                        </div>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h5 class="panel-title" style="text-align: left">
                                                    <a id="ancDEList" runat="server" data-toggle="collapse" class="disabled" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdDE"
                                                        href="#BodyContent_ctrlFEDRDropDowns_divDEList">Data Entry Type List</a>
                                                </h5>
                                            </div>
                                            <div id="divDEList" runat="server" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <asp:GridView runat="server" ID="gvwDataEntryList" AutoGenerateColumns="False" DataKeyNames="demID" AllowPaging="True" AllowSorting="True"
                                                        CssClass="mydatagrid" ShowHeaderWhenEmpty="True" EmptyDataText="No Data Entry description found." OnPageIndexChanging="gvwDataEntryList_PageIndexChanging"
                                                        AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                        RowStyle-CssClass="rows">
                                                        <Columns>
                                                            <asp:BoundField DataField="demID" HeaderText="demID" Visible="False" />
                                                            <asp:BoundField DataField="demName" HeaderText="Data Entry Method" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-success">
                                            <div class="panel-heading">
                                                <h5 class="panel-title" style="text-align: left">
                                                    <a id="ancDEAdd" runat="server" data-toggle="collapse" class="disabled" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdDE"
                                                        href="#BodyContent_ctrlFEDRDropDowns_divDEAdd">Add Data Entry Type</a>
                                                </h5>
                                            </div>
                                            <div id="divDEAdd" runat="server" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                        <asp:Label ID="lblDataEntryAdd" runat="server" CssClass="control-label" For="txtDataEntryAdd" Font-Size="Large" Text="Data Entry Method" />
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                                                        <asp:TextBox ID="txtDataEntryAdd" runat="server" CssClass="form-control" Text="" Font-Size="Large" placeholder="Data Entry Type" />
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div id="encDEType" runat="server" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iDE"><span runat="server" id="spnDE"></span></i></h4>
                                                            <asp:Label ID="lblDataEntrySuccess" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <asp:LinkButton ID="btnDataEntryAddNew" runat="server" CssClass="btn btn-success text-center" OnClick="btnDataEntryAddNew_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnDataEntryAddCancel" runat="server" CssClass="btn btn-warning text-center" OnClick="btnDataEntryAddCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnDataEntryAddDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnDataEntryAddDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancDmgStatOuter" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdMain" href="#BodyContent_ctrlFEDRDropDowns_divDmgStat">
                                <span class="glyphicon glyphicon-menu-right"></span> Demographic Status</a>
                        </h5>
                    </div>
                    <div id="divDmgStat" runat="server" class="panel-collapse collapse">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="uprgrsDmgStat" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlDmgStat">
                                <ProgressTemplate>
                                    <div id="divDmgStatPrgrss" runat="server" class="updateProgress">
                                        <asp:Image ID="imgDmgStatProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel ID="upnlDmgStat" runat="server">
                                <ContentTemplate>
                                    <div class="panel-group" runat="server" id="acrdDStat">
                                        <div class="input-group">
                                            <asp:LinkButton ID="btnAddNewStat" runat="server" CssClass="btn btn-info text-center" OnClick="btnAddNewStat_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                        </div>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h5 class="panel-title" style="text-align: left">
                                                    <a id="ancDStat" runat="server" data-toggle="collapse" class="disabled" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdDStat"
                                                        href="#BodyContent_ctrlFEDRDropDowns_divDList">Demographic Status List</a>
                                                </h5>
                                            </div>
                                            <div id="divDList" runat="server" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <asp:GridView ID="gvwStatList" runat="server" EmptyDataText="No Demographic Status List found."
                                                        ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="dmgstID" AllowPaging="True" AllowSorting="True"
                                                        CssClass="mydatagrid" OnPageIndexChanging="gvwStatList_PageIndexChanging"
                                                        AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                        RowStyle-CssClass="rows">
                                                        <Columns>
                                                            <asp:BoundField DataField="dmgstID" HeaderText="dmgstID" Visible="false" />
                                                            <asp:BoundField DataField="dmgStatus" HeaderText="Demographic Status" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-success">
                                            <div class="panel-heading">
                                                <h5 class="panel-title" style="text-align: left">
                                                    <a id="ancDAdd" runat="server" data-toggle="collapse" class="disabled" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdDStat"
                                                        href="#BodyContent_ctrlFEDRDropDowns_divDAdd">Add Demographic Status</a>
                                                </h5>
                                            </div>
                                            <div id="divDAdd" runat="server" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                        <asp:Label ID="lblStatAdd" runat="server" CssClass="control-label" for="txtStatAdd" Font-Size="Large" Text="Demographic Status" />
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                                                        <asp:TextBox ID="txtStatAdd" runat="server" CssClass="form-control" Font-Size="Large" Text="" placeholder="Demographic Status" />
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div id="encDStatus" runat="server" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iDStat"><span runat="server" id="spnDStat"></span></i></h4>
                                                            <asp:Label ID="lblStatSuccess" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <asp:LinkButton ID="btnStatAdd" runat="server" CssClass="btn btn-success text-center" OnClick="btnStatAdd_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnStatCancel" runat="server" CssClass="btn btn-warning text-center" OnClick="btnStatCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnStatDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnStatDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancLocTyp" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdMain" href="#BodyContent_ctrlFEDRDropDowns_divLocTyp">
                                <span class="glyphicon glyphicon-menu-right"></span> Demographic Location Types</a>
                        </h5>
                    </div>
                    <div id="divLocTyp" runat="server" class="panel-collapse collapse">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="uprgrsDType" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlDType">
                                <ProgressTemplate>
                                    <div id="divDmgTypePrgrss" runat="server" class="updateProgress">
                                        <asp:Image ID="imgDmgTypeProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel ID="upnlDType" runat="server">
                                <ContentTemplate>
                                    <div class="panel-group" runat="server" id="acrdDmgType">
                                        <div class="input-group">
                                            <asp:LinkButton ID="btnDemogTypeAdd" runat="server" CssClass="btn btn-info text-center" OnClick="btnDemogTypeAdd_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                        </div>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h5 class="panel-title" style="text-align: left">
                                                    <a id="ancDmgType" runat="server" data-toggle="collapse" class="disabled" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdDmgType"
                                                        href="#BodyContent_ctrlFEDRDropDowns_divDMGList">Demographic Location Type List</a>
                                                </h5>
                                            </div>
                                            <div id="divDMGList" runat="server" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <asp:GridView ID="gvwDemogTypeList" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                                        AllowSorting="True" CssClass="mydatagrid" DataKeyNames="dmgID" OnPageIndexChanging="gvwDemogTypeList_PageIndexChanging"
                                                        AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                        RowStyle-CssClass="rows">
                                                        <Columns>
                                                            <asp:BoundField DataField="dmgID" HeaderText="dmgID" Visible="False" />
                                                            <asp:BoundField DataField="dmgName" HeaderText="Demographic Type" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-success">
                                            <div class="panel-heading">
                                                <h5 class="panel-title" style="text-align: left">
                                                    <a id="ancDTypeAdd" runat="server" data-toggle="collapse" class="disabled" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdDmgType"
                                                        href="#BodyContent_ctrlFEDRDropDowns_divDTypeAdd">Add Demographic Location Type</a>
                                                </h5>
                                            </div>
                                            <div id="divDTypeAdd" runat="server" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                        <asp:Label ID="lblDemogType" runat="server" CssClass="control-label" for="txtDemogTypeAdd" Font-Size="Large" Text="Demographic Location Type" />
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                                                        <asp:TextBox ID="txtDemogTypeAdd" runat="server" CssClass="form-control" Text="" placeholder="Demographic Location Type" />
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div id="enDmgType" runat="server" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iDType"><span runat="server" id="spnDType"></span></i></h4>
                                                            <asp:Label ID="lblDemogTypeSuccess" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <asp:LinkButton ID="btnDemogTypeAddNew" runat="server" CssClass="btn btn-success text-center" OnClick="btnDemogTypeAddNew_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnDemogTypeAddCancel" runat="server" CssClass="btn btn-warning text-center" OnClick="btnDemogTypeAddCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnDemogTypeDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnDemogTypeDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancOPStat" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdMain" href="#BodyContent_ctrlFEDRDropDowns_divOPStat">
                                <span class="glyphicon glyphicon-menu-right"></span> Operating Status</a>
                        </h5>
                    </div>
                    <div id="divOPStat" runat="server" class="panel-collapse collapse">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="uprgrsOPStat" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlOPStat">
                                <ProgressTemplate>
                                    <div id="divOPStatPrgrss" runat="server" class="updateProgress">
                                        <asp:Image ID="imgOPStatProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel ID="upnlOPStat" runat="server">
                                <ContentTemplate>
                                    <div class="panel-group" runat="server" id="acrdOPStat">
                                        <div class="input-group">
                                            <asp:LinkButton ID="btnAddOpr" runat="server" CssClass="btn btn-info text-center" OnClick="btnAddOpr_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                        </div>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h5 class="panel-title" style="text-align: left">
                                                    <a id="ancOPSTList" runat="server" data-toggle="collapse" class="disabled" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdOPStat"
                                                        href="#BodyContent_ctrlFEDRDropDowns_divOPStatList">Operating Status List</a>
                                                </h5>
                                            </div>
                                            <div id="divOPStatList" runat="server" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <asp:GridView ID="gvwOprList" runat="server" EmptyDataText="No Operating Status description found."
                                                        ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="osID" AllowPaging="True" AllowSorting="True" CssClass="mydatagrid"
                                                        OnPageIndexChanging="gvwOprList_PageIndexChanging"
                                                        AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                        RowStyle-CssClass="rows">
                                                        <Columns>
                                                            <asp:BoundField DataField="osID" HeaderText="osID" Visible="false" />
                                                            <asp:BoundField DataField="osValue" HeaderText="Operating Status" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-success">
                                            <div class="panel-heading">
                                                <h5 class="panel-title" style="text-align: left">
                                                    <a id="ancOPStatAdd" runat="server" data-toggle="collapse" class="disabled" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdOPStat"
                                                        href="#BodyContent_ctrlFEDRDropDowns_divOPStatAdd">Add Operating Status</a>
                                                </h5>
                                            </div>
                                            <div id="divOPStatAdd" runat="server" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                        <asp:Label ID="lblOprAdd" runat="server" CssClass="control-label" for="txtOprAdd" Font-Size="Large" Text="Operating Status" />
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                                                        <asp:TextBox ID="txtOprAdd" runat="server" CssClass="form-control" Text="" placeholder="Operating Status" />
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div id="encOpr" runat="server" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iOPSTT"><span runat="server" id="spnOPSTT"></span></i></h4>
                                                            <asp:Label ID="lblOprSuccess" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <asp:LinkButton ID="btnOprAdd" runat="server" CssClass="btn btn-success text-center" OnClick="btnOprAdd_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnOprCancel" runat="server" CssClass="btn btn-warning text-center" OnClick="btnOprCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnOprDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnOprDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancIFR" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdMain" href="#BodyContent_ctrlFEDRDropDowns_divIFR">
                                <span class="glyphicon glyphicon-menu-right"></span> IFR Values</a>
                        </h5>
                    </div>
                    <div id="divIFR" runat="server" class="panel-collapse collapse">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="uprgrsIFR" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlIFR">
                                <ProgressTemplate>
                                    <div id="divIFRPrgrss" runat="server" class="updateProgress">
                                        <asp:Image ID="imgIFRProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel ID="upnlIFR" runat="server">
                                <ContentTemplate>
                                    <div class="panel-group" runat="server" id="acrdIFR">                                        
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h5 class="panel-title" style="text-align: left">
                                                    <a id="ancIFRList" runat="server" data-toggle="collapse" class="disabled" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdIFR"
                                                        href="#BodyContent_ctrlFEDRDropDowns_divIFRList">IFR List</a>
                                                </h5>
                                            </div>
                                            <div id="divIFRList" runat="server" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <asp:GridView ID="gvwIFRList" runat="server" AutoGenerateColumns="False" DataKeyNames="ifrValID" AllowPaging="True"
                                                        AllowSorting="True" CssClass="mydatagrid" OnPageIndexChanging="gvwIFRList_PageIndexChanging"
                                                        AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                        RowStyle-CssClass="rows">
                                                        <Columns>
                                                            <asp:BoundField DataField="ifrValID" HeaderText="ifrValID" Visible="False" />
                                                            <asp:BoundField DataField="ifrValue" HeaderText="IFR Value" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancSC" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdMain" href="#BodyContent_ctrlFEDRDropDowns_divSC">
                                <span class="glyphicon glyphicon-menu-right"></span> Short-Collar Application Criteria</a>
                        </h5>
                    </div>
                    <div id="divSC" runat="server" class="panel-collapse collapse">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="uprgrsSC" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlSC">
                                <ProgressTemplate>
                                    <div id="divSCPrgrss" runat="server" class="updateProgress">
                                        <asp:Image ID="imgSCProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel ID="upnlSC" runat="server">
                                <ContentTemplate>
                                    <div class="panel-group" runat="server" id="acrdSC">
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h5 class="panel-title" style="text-align: left">
                                                    <a id="ancSCList" runat="server" data-toggle="collapse" class="disabled" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdMain"
                                                        href="#BodyContent_ctrlFEDRDropDowns_divSCList">Short-Collar Application Criterion List</a>
                                                </h5>
                                            </div>
                                            <div id="divSCList" runat="server" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <asp:GridView ID="gvwSCAppList" runat="server" EmptyDataText="No Well Section description found."
                                                        ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="scappID" AllowPaging="True" AllowSorting="True"
                                                        CssClass="mydatagrid" OnPageIndexChanging="gvwSCAppList_PageIndexChanging"
                                                        AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                        RowStyle-CssClass="rows">
                                                        <Columns>
                                                            <asp:BoundField DataField="scappID" HeaderText="scappID" Visible="false" />
                                                            <asp:BoundField DataField="scappValue" HeaderText="Short Colloar Application Crieria" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancCSQC" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdMain" href="#BodyContent_ctrlFEDRDropDowns_divCSQC">
                                <span class="glyphicon glyphicon-menu-right"></span> Data Correction Survey Selection Criterion</a>
                        </h5>
                    </div>
                    <div id="divCSQC" runat="server" class="panel-collapse collapse">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="uprgrsCSQC" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlCSQC">
                                <ProgressTemplate>
                                    <div id="divCSQCPrgrss" runat="server" class="updateProgress">
                                        <asp:Image ID="imgCSQCProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel ID="upnlCSQC" runat="server">
                                <ContentTemplate>
                                    <div class="panel-group" runat="server" id="acrdCSQC">
                                        <div class="input-group">
                                            <asp:LinkButton ID="btnAddNewCSQC" runat="server" CssClass="btn btn-info text-center" OnClick="btnAddNewCSQC_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                        </div>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h5 class="panel-title" style="text-align: left">
                                                    <a id="ancCSQCList" runat="server" data-toggle="collapse" class="disabled" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdCSQC"
                                                        href="#BodyContent_ctrlFEDRDropDowns_divCSQC">Data Correction Survey Selection Criteria List</a>
                                                </h5>
                                            </div>
                                            <div id="divCSQCList" runat="server" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <asp:GridView ID="gvwCSQC" runat="server" EmptyDataText="No Criteria definition found."
                                                        ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="csqcID" AllowPaging="True" AllowSorting="True" CssClass="mydatagrid"
                                                        OnPageIndexChanging="gvwCSQC_PageIndexChanging"
                                                        AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                        RowStyle-CssClass="rows">
                                                        <Columns>
                                                            <asp:BoundField DataField="csqcID" Visible="false" />
                                                            <asp:BoundField DataField="csqcBTotal" HeaderText="Total Field (B-Total) - nT">
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="csqcDip" HeaderText="Dip">
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="csqcGTotal" HeaderText="Total Gravity (G-Total) - g">
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="csqcName" HeaderText="Survey Check Color" ItemStyle-HorizontalAlign="Center" />
                                                            <asp:BoundField DataField="csqcDesc" HeaderText="Description" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-success">
                                            <div class="panel-heading">
                                                <h5 class="panel-title" style="text-align: left">
                                                    <a id="ancCSQCAdd" runat="server" data-toggle="collapse" class="disabled" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdCSQC"
                                                        href="#BodyContent_ctrlFEDRDropDowns_divCSQCAdd">Add Data Correction Survey Selection Criterion</a>
                                                </h5>
                                            </div>
                                            <div id="divCSQCAdd" runat="server" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div id="enCSQC" runat="server" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iCSQC"><span runat="server" id="spnCSQC"></span></i></h4>
                                                            <asp:Label ID="lblCSQC" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <asp:LinkButton ID="btnAddSQC" runat="server" CssClass="btn btn-success text-center" OnClick="btnAddSQC_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnClearSQC" runat="server" CssClass="btn btn-warning text-center" OnClick="btnClearSQC_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnDoneSQC" runat="server" CssClass="btn btn-danger text-center" OnClick="btnDoneSQC_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancWPIT" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdMain" href="#BodyContent_ctrlFEDRDropDowns_divWPIT">
                                <span class="glyphicon glyphicon-menu-right"></span> Well Profile Iteration Limits</a>
                        </h5>
                    </div>
                    <div id="divWPIT" runat="server" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="panel-group" runat="server" id="acrdWPI">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h5 class="panel-title" style="text-align: left">
                                            <a id="ancWPIList" runat="server" data-toggle="collapse" class="disabled" data-parent="#BodyContent_ctrlFEDRDropDowns_acrdWPI" href="#BodyContent_ctrlFEDRDropDowns_divWPIList">
                                                Well Profile Iteration Limits List</a>
                                        </h5>
                                    </div>
                                    <div class="panel-body">
                                        <asp:GridView ID="gvwWPIList" runat="server" EmptyDataText="No Well Profile Iteration Limits configured."
                                            ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="wpiID" AllowPaging="True" AllowSorting="True" CssClass="mydatagrid"
                                            OnPageIndexChanging="gvwStatList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15"
                                            HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                            RowStyle-CssClass="rows">
                                            <Columns>
                                                <asp:BoundField DataField="wpiID" HeaderText="dmgstID" Visible="false" />
                                                <asp:BoundField DataField="wpiMin" HeaderText="Minimum" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField DataField="wpiMed" HeaderText="Threshold" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField DataField="wpiMax" HeaderText="Maximum" ItemStyle-HorizontalAlign="Center" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>