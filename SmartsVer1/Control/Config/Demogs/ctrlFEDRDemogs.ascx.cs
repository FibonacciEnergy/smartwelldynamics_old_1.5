﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Net.Mail;
using System.Web.UI;
using System.Data.SqlTypes;
using System.Web.Security;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using SQ = Microsoft.SqlServer.Types;
using CLR = System.Drawing.Color;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;

namespace SmartsVer1.Control.Config.Demogs
{
    public partial class ctrlFEDRDemogs : System.Web.UI.UserControl
    {
        String LoginName, username, domain;
        Int32 ClientID = -99, globWSt = 1;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);
                UP usrP = UP.GetUserProfile(LoginName);
                username = addr.User;
                domain = addr.Host;
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
                //Configuring page dropdownlists
                System.Data.DataTable tableDisclaimers = DMG.getDisclaimerTable();
                System.Data.DataTable tableDisclaimersType = DMG.getDisclaimerTypeTable();
                System.Data.DataTable tableImagePurpose = DMG.getImagePurposeTable();
                System.Data.DataTable tableImageType = DMG.getImageTypeTable();
                System.Data.DataTable tableLogType = DMG.getLogTypeTable();
                System.Data.DataTable tableNGZ = DMG.getNGZTable();
                System.Data.DataTable tableRunName = DMG.getRunNameTable();
                System.Data.DataTable tableRunSection = DMG.getRunSectionTable();
                System.Data.DataTable tableSrvyFileType = DMG.getSFileTypeTable();
                System.Data.DataTable tableWellSection = DMG.getWellSectionTable();
                System.Data.DataTable tableCard = DMG.getCardinals();
                Dictionary<Int32, String> rgList = DMG.getRegionList();
                this.ddlWRegion.Items.Clear();
                this.ddlWRegion.DataSource = rgList;
                this.ddlWRegion.DataTextField = "Value";
                this.ddlWRegion.DataValueField = "Key";
                this.ddlWRegion.DataBind();
                this.ddlWRegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlWRegion.SelectedIndex = -1;
                Dictionary<Int32, String> opList = CLNT.getOpCoList();
                this.ddlWAOPC.Items.Clear();
                this.ddlWAOPC.DataSource = opList;
                this.ddlWAOPC.DataTextField = "Value";
                this.ddlWAOPC.DataValueField = "Key";
                this.ddlWAOPC.DataBind();
                this.ddlWAOPC.Items.Insert(0, new ListItem("--- Select Well Operator Company ---", "-1"));
                this.ddlWAOPC.SelectedIndex = -1;

                //Loading GridViews on page
                if (!this.IsPostBack)
                {
                    this.gvwDisclaimer.DataSource = tableDisclaimers;
                    this.gvwDisclaimer.DataBind();
                    this.gvwDscTypeList.DataSource = tableDisclaimersType;
                    this.gvwDscTypeList.DataBind();
                    this.gvwImgPurposeList.DataSource = tableImagePurpose;
                    this.gvwImgPurposeList.DataBind();
                    this.gvwImgTypeList.DataSource = tableImageType;
                    this.gvwImgTypeList.DataBind();
                    this.gvwLogTypeList.DataSource = tableLogType;
                    this.gvwLogTypeList.DataBind();
                    this.gvwNGZList.DataSource = tableNGZ;
                    this.gvwNGZList.DataBind();
                    this.gvwRunNameList.DataSource = tableRunName;
                    this.gvwRunNameList.DataBind();
                    this.gvwFileTypeList.DataSource = tableSrvyFileType;
                    this.gvwFileTypeList.DataBind();
                    this.gvwWSecList.DataSource = tableWellSection;
                    this.gvwWSecList.DataBind();
                    this.gvwCardinals.DataSource = tableCard;
                    this.gvwCardinals.DataBind();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");
                this.clndrWellSpudDate.SelectedDate = DateTime.Now;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwDisclaimer_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable tableD = DMG.getDisclaimerTable();
                this.gvwDisclaimer.PageIndex = e.NewPageIndex;
                this.gvwDisclaimer.DataSource = tableD;
                this.gvwDisclaimer.DataBind();
                this.btnDsclmrReset.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDsclmrAdd_Click(object sender, EventArgs e)
        {
            try
            {
                btnDscTypeAddCancel_Click(null, null);
                this.divDiscList.Visible = false;
                this.divDiscAdd.Visible = true;
                this.btnDsclmrDone.Visible = true;
                this.ddlDscType.Items.Clear();
                Dictionary<Int32, String> dtList = DMG.getDisclaimerTypeList();
                this.ddlDscType.DataSource = dtList;
                this.ddlDscType.DataTextField = "Value";
                this.ddlDscType.DataValueField = "Key";
                this.ddlDscType.DataBind();
                this.ddlDscType.Items.Insert(0, new ListItem("--- Select Disclaimer Type ---", "-1"));
                this.ddlDscType.SelectedIndex = -1;
                this.txtAddDsclmr.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddDsclmr_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, type = -99;
                String textValue = String.Empty;
                type = Convert.ToInt32(this.ddlDscType.SelectedValue);
                textValue = Convert.ToString(this.txtAddDsclmr.Text);
                if(String.IsNullOrEmpty(textValue))
                {
                    this.dscAddEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iDscAdd.Attributes["class"] = "icon fa fa-warning";
                    this.spnDscAdd.InnerText = " Missing information!";
                    this.lblDsclmrSuccess.Text = "Null values can't be inserted for Disclaimer Text/Statement";
                    this.dscAddEnclosure.Visible = true;
                }
                else
                {
                    iResult = DMG.addDisclaimerText(textValue, type);
                    if (iResult.Equals(1))
                    {
                        this.dscAddEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iDscAdd.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnDscAdd.InnerText = " Success!";
                        this.lblDsclmrSuccess.Text = "Successfully inserted Disclaimer Text/Statement";
                        this.dscAddEnclosure.Visible = true;
                        System.Data.DataTable dscTable = DMG.getDisclaimerTable();
                        this.gvwDisclaimer.DataSource = dscTable;
                        this.gvwDisclaimer.SelectedIndex = -1;
                        this.gvwDisclaimer.PageIndex = 0;
                        this.gvwDisclaimer.DataBind();
                        this.txtAddDsclmr.Text = String.Empty;
                    }
                    else
                    {
                        this.dscAddEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iDscAdd.Attributes["class"] = "icon fa fa-warning";
                        this.spnDscAdd.InnerText = " Missing information!";
                        this.lblDsclmrSuccess.Text = "Problem inserting Disclaimer Text/Statement. Please try again";
                        this.dscAddEnclosure.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddDsclmrCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtAddDsclmr.Text = "";
                this.txtAddDsclmr.Enabled = false;
                this.dscAddEnclosure.Visible = false;
                this.lblDsclmrSuccess.Text = "";
                Dictionary<Int32, String> dtList = DMG.getDisclaimerTypeList();
                this.ddlDscType.Items.Clear();
                this.ddlDscType.DataSource = dtList;
                this.ddlDscType.DataTextField = "Value";
                this.ddlDscType.DataValueField = "Key";
                this.ddlDscType.DataBind();
                this.ddlDscType.Items.Insert(0, new ListItem("--- Select Disclaimer Type ---", "-1"));
                this.ddlDscType.SelectedIndex = -1;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDsclmrDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnAddDsclmrCancel_Click(null, null);
                this.divDiscList.Visible = true;
                this.divDiscAdd.Visible = false;
                this.btnDsclmrDone.Visible = false;
                DataTable tableD = DMG.getDisclaimerTable();
                this.gvwDisclaimer.DataSource = tableD;
                this.gvwDisclaimer.DataBind();
                this.gvwDisclaimer.SelectedIndex = -1;
                this.gvwDisclaimer.PageIndex = 0;
                this.gvwDisclaimer.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwDscTypeList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable tableDT = DMG.getDisclaimerTable();
                this.gvwDscTypeList.PageIndex = e.NewPageIndex;
                this.gvwDscTypeList.DataSource = tableDT;
                this.gvwDscTypeList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDscTypeAdd_Click(object sender, EventArgs e)
        {
            try
            {
                btnDscTypeAddCancel_Click(null, null);
                this.divDTypeList.Visible = false;
                this.divDTypeAdd.Visible = true;
                this.btnDscTypeDone.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDscTypeAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                String value = String.Empty;
                value = Convert.ToString(this.txtAddDscTypeName.Text);
                if (String.IsNullOrEmpty(value))
                {
                    this.dtypeEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iDType.Attributes["class"] = "icon fa fa-warning";
                    this.spnDType.InnerText = " Missing information!";
                    this.lblDscTypeSuccess.Text = "Null values can't be inserted for Disclaimer Type";
                    this.dtypeEnclosure.Visible = true;
                }
                else
                {
                    iResult = DMG.addDisclaimerType(value);
                    if (iResult.Equals(1))
                    {
                        this.dtypeEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iDType.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnDType.InnerText = " Success!";
                        this.lblDscTypeSuccess.Text = "Successfully inserted Disclaimer Type";
                        this.dtypeEnclosure.Visible = true;
                        System.Data.DataTable dtTable = DMG.getDisclaimerTypeTable();
                        this.gvwDscTypeList.DataSource = dtTable;
                        this.gvwDscTypeList.SelectedIndex = -1;
                        this.gvwDscTypeList.PageIndex = 0;
                        this.gvwDscTypeList.DataBind();
                    }
                    else
                    {
                        this.dtypeEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iDType.Attributes["class"] = "icon fa fa-warning";
                        this.spnDType.InnerText = " Missing information!";
                        this.lblDscTypeSuccess.Text = "Unable to insert value for Disclaimer Type";
                        this.dtypeEnclosure.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDscTypeAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtAddDscTypeName.Text = "";
                this.dtypeEnclosure.Visible = false;
                this.lblDscTypeSuccess.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDscTypeDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnDscTypeAddCancel_Click(null, null);
                this.divDTypeList.Visible = true;
                this.divDTypeAdd.Visible = false;
                this.btnDscTypeDone.Visible = false;
                DataTable tableDT = DMG.getDisclaimerTypeTable();
                this.gvwDscTypeList.DataSource = tableDT;
                this.gvwDscTypeList.SelectedIndex = -1;
                this.gvwDscTypeList.PageIndex = 0;
                this.gvwDscTypeList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwImgPurposeList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable tableIP = DMG.getImagePurposeTable();
                this.gvwImgPurposeList.PageIndex = e.NewPageIndex;
                this.gvwImgPurposeList.DataSource = tableIP;
                this.gvwImgPurposeList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnImgPurposeAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.divImgPurposeList.Visible = false;
                this.divImgPurposeAdd.Visible = true;
                btnImgPurposeAddCancel_Click(null, null);
                this.btnImgPurposeDone.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnImgPurposeAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                String value = String.Empty;
                value = Convert.ToString(this.txtAddImgPurpose.Text);
                if (String.IsNullOrEmpty(value))
                {
                    this.imgEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iImgP.Attributes["class"] = "icon fa fa-warning";
                    this.spnImgP.InnerText = " Missing information!";
                    this.lblImgPurposeSuccess.Text = "Unable to insert Null values for Image Purpose";
                    this.imgEnclosure.Visible = true;
                }
                else
                {
                    iResult = DMG.addImagePurpose(value);
                    if (iResult.Equals(1))
                    {
                        this.imgEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iImgP.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnImgP.InnerText = " Success!";
                        this.lblImgPurposeSuccess.Text = "Successfully inserted Image Purpose";
                        this.imgEnclosure.Visible = true;
                    }
                    else
                    {
                        this.imgEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iImgP.Attributes["class"] = "icon fa fa-warning";
                        this.spnImgP.InnerText = " Warning!";
                        this.lblImgPurposeSuccess.Text = "Unable to insert Image Purpose";
                        this.imgEnclosure.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnImgPurposeAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtAddImgPurpose.Text = "";
                this.lblImgPurposeSuccess.Text = "";
                this.imgEnclosure.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnImgPurposeDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnImgPurposeAddCancel_Click(null, null);
                this.divImgPurposeAdd.Visible = false;
                this.btnImgPurposeDone.Visible = false;
                this.divImgPurposeList.Visible = true;
                DataTable tableIP = DMG.getImagePurposeTable();
                this.gvwImgPurposeList.DataSource = tableIP;
                this.gvwImgPurposeList.DataBind();

                this.gvwImgPurposeList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwImgTypeList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable tableIT = DMG.getImageTypeTable();
                this.gvwImgTypeList.PageIndex = e.NewPageIndex;
                this.gvwImgTypeList.DataSource = tableIT;
                this.gvwImgTypeList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnImgTypeAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.divImgTypeList.Visible = false;
                this.divImgTypeAdd.Visible = true;
                btnImgTypeAddCancel_Click(null, null);
                this.btnImgTypeDone.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnImgTypeAddNew_Click(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnImgTypeAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtImgTypeAdd.Text = "";
                this.lblImgTypeSuccess.Text = "";
                this.itypEnclosure.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnImgTypeDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.divImgTypeAdd.Visible = false;
                btnImgTypeAddCancel_Click(null, null);
                this.btnImgTypeDone.Visible = false;
                DataTable tableIT = DMG.getImageTypeTable();
                this.gvwImgTypeList.DataSource = tableIT;
                this.gvwImgTypeList.DataBind();
                this.divImgTypeList.Visible = true;

                this.gvwImgTypeList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwLogTypeList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable tableLT = DMG.getLogTypeTable();
                this.gvwLogTypeList.PageIndex = e.NewPageIndex;
                this.gvwLogTypeList.DataSource = tableLT;
                this.gvwLogTypeList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnLogTypeAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.divLogTypeList.Visible = false;
                this.divLogTypeAdd.Visible = true;
                btnLogTypeAddCancel_Click(null, null);
                this.btnLogTypeAddDone.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnLogTypeAddNew_Click(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnLogTypeAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtLogTypeAdd.Text = "";
                this.lblLogTypeSuccess.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnLogTypeAddDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnLogTypeAddCancel_Click(null, null);
                this.divLogTypeAdd.Visible = false;
                this.btnLogTypeAddDone.Visible = false;
                DataTable tableLT = DMG.getLogTypeTable();
                this.gvwLogTypeList.DataSource = tableLT;
                this.gvwLogTypeList.DataBind();
                this.divLogTypeList.Visible = true;

                this.gvwLogTypeList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwNGZList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable tableLNGZ = DMG.getNGZTable();
                this.gvwNGZList.PageIndex = e.NewPageIndex;
                this.gvwNGZList.DataSource = tableLNGZ;
                this.gvwNGZList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnNGZAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.divNGZList.Visible = false;
                this.divNGZAdd.Visible = true;
                btnNGZAddCancel_Click(null, null);
                this.btnNGZDone.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnNGZAddNew_Click(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnNGZAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtNGZCAdd.Text = "";
                this.txtNGZVAdd.Text = "";
                this.lblNGZSuccess.Text = "";
                this.ngzEnclosure.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnNGZDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnNGZAddCancel_Click(null, null);
                this.divNGZAdd.Visible = false;
                this.btnNGZDone.Visible = false;
                DataTable tableLNGZ = DMG.getNGZTable();
                this.gvwNGZList.DataSource = tableLNGZ;
                this.gvwNGZList.DataBind();
                this.divNGZList.Visible = true;

                this.gvwNGZList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRunNameList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable tableRN = DMG.getRunNameTable();
                this.gvwRunNameList.PageIndex = e.NewPageIndex;
                this.gvwRunNameList.DataSource = tableRN;
                this.gvwRunNameList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRunNameAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.divRunNameList.Visible = false;
                this.divRunNameAdd.Visible = true;
                btnRunNameAddCancel_Click(null, null);
                this.btnRunNameDone.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRunNameAddNew_Click(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRunNameAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtRunNameAdd.Text = "";
                this.lblRunNameSuccess.Text = "";
                this.rnnEnclosure.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRunNameDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnRunNameAddCancel_Click(null, null);
                this.divRunNameAdd.Visible = false;
                this.btnRunNameDone.Visible = false;
                this.divRunNameList.Visible = true;
                DataTable tableRN = DMG.getRunNameTable();
                this.gvwRunNameList.DataSource = tableRN;
                this.gvwRunNameList.DataBind();

                this.gvwRunNameList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwFileTypeList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable tableFT = DMG.getSFileTypeTable();
                this.gvwFileTypeList.PageIndex = e.NewPageIndex;
                this.gvwFileTypeList.DataSource = tableFT;
                this.gvwFileTypeList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewFileType_Click(object sender, EventArgs e)
        {
            try
            {
                this.divFileTypeList.Visible = false;
                this.divFileTypeAdd.Visible = true;
                btnFileTypeCancel_Click(null, null);
                this.btnFileTypeDone.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnFileTypeAdd_Click(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnFileTypeCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtFileTypeAdd.Text = "";
                this.txtFileDescAdd.Text = "";
                this.lblFileTypeSuccess.Text = "";
                this.lblFileTypeSuccess.CssClass = "lblSuccess";
                this.lblFileTypeSuccess.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnFileTypeDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnFileTypeCancel_Click(null, null);
                this.divFileTypeAdd.Visible = false;
                this.btnFileTypeDone.Visible = false;
                DataTable tableFT = DMG.getSFileTypeTable();
                this.gvwFileTypeList.DataSource = tableFT;
                this.gvwFileTypeList.DataBind();
                this.divFileTypeList.Visible = true;

                this.gvwFileTypeList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWSecList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable tableWSec = DMG.getWellSectionTable();
                this.gvwWSecList.PageIndex = e.NewPageIndex;
                this.gvwWSecList.DataSource = tableWSec;
                this.gvwWSecList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWSecAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                this.divWSecList.Visible = false;
                this.divWSecAdd.Visible = true;
                btnWSecCancel_Click(null, null);
                this.btnWSecDone.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWSecAdd_Click(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWSecCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtWSecAdd.Text = "";
                this.lblWSecSuccess.Text = "";
                this.wsEnclosure.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWSecDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnWSecCancel_Click(null, null);
                this.divWSecAdd.Visible = false;
                this.btnWSecDone.Visible = false;
                DataTable tableWSec = DMG.getWellSectionTable();
                this.gvwWSecList.DataSource = tableWSec;
                this.gvwWSecList.DataBind();
                this.divWSecList.Visible = true;

                this.gvwWSecList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable tableWL = DMG.getGlobalWellTable();
                this.gvwWellList.PageIndex = e.NewPageIndex;
                this.gvwWellList.DataSource = tableWL;
                this.gvwWellList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwCardinals_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable tableCard = DMG.getCardinals();
                this.gvwCardinals.PageIndex = e.NewPageIndex;
                this.gvwCardinals.DataSource = tableCard;
                this.gvwCardinals.DataBind();
                this.btnCardReset.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWellClear_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> rgList = DMG.getRegionList();
                this.ddlWRegion.Items.Clear();
                this.ddlWRegion.DataSource = rgList;
                this.ddlWRegion.DataTextField = "Value";
                this.ddlWRegion.DataValueField = "Key";
                this.ddlWRegion.DataBind();
                this.ddlWRegion.Items.Insert(0, new ListItem("--- Select Major Geogrpahic Region ---", "-1"));
                this.ddlWRegion.SelectedIndex = -1;
                this.ddlWSRegion.Items.Clear();
                this.ddlWSRegion.DataBind();
                this.ddlWSRegion.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlWSRegion.SelectedIndex = -1;
                this.ddlWSRegion.Enabled = false;
                this.ddlWCountry.Items.Clear();
                this.ddlWCountry.DataBind();
                this.ddlWCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlWCountry.SelectedIndex = -1;
                this.ddlWCountry.Enabled = false;
                this.ddlWState.Items.Clear();
                this.ddlWState.DataBind();
                this.ddlWState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlWState.SelectedIndex = -1;
                this.ddlWState.Enabled = false;
                this.ddlWCounty.Items.Clear();
                this.ddlWCounty.DataBind();
                this.ddlWCounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlWCounty.SelectedIndex = -1;
                this.ddlWCounty.Enabled = false;
                this.ddlWField.Items.Clear();
                this.ddlWField.DataBind();
                this.ddlWField.Items.Insert(0, new ListItem("--- Select Oil & Gas Field ---", "-1"));
                this.ddlWField.SelectedIndex = -1;
                this.ddlWField.Enabled = false;
                this.btnWellClear.Visible = false;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();
                this.gvwWellList.Visible = false;
                this.gvwWellDetailList.DataBind();
                this.gvwWellDetailList.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 selID = Convert.ToInt32(this.gvwWellList.SelectedValue);
                System.Data.DataTable detTable = AST.getDemogWellDetailTable(selID);
                this.gvwWellDetailList.DataSource = detTable;
                this.gvwWellDetailList.DataBind();
                this.gvwWellDetailList.Visible = true;
                this.btnWellClear.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srg = Convert.ToInt32(this.ddlWRegion.SelectedValue);
                Dictionary<Int32, String> srgList = DMG.getSubRegionList(srg);
                this.ddlWSRegion.Items.Clear();
                this.ddlWSRegion.DataSource = srgList;
                this.ddlWSRegion.DataTextField = "Value";
                this.ddlWSRegion.DataValueField = "Key";
                this.ddlWSRegion.DataBind();
                this.ddlWSRegion.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlWSRegion.SelectedIndex = -1;
                this.ddlWSRegion.Enabled = true;
                this.ddlWCountry.Items.Clear();
                this.ddlWCountry.DataBind();
                this.ddlWCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlWCountry.SelectedIndex = -1;
                this.ddlWCountry.Enabled = false;
                this.ddlWState.Items.Clear();
                this.ddlWState.DataBind();
                this.ddlWState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlWState.SelectedIndex = -1;
                this.ddlWState.Enabled = false;
                this.ddlWCounty.Items.Clear();
                this.ddlWCounty.DataBind();
                this.ddlWCounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlWCounty.SelectedIndex = -1;
                this.ddlWCounty.Enabled = false;
                this.ddlWField.Items.Clear();
                this.ddlWField.DataBind();
                this.ddlWField.Items.Insert(0, new ListItem("--- Select Oil & Gas Field ---", "-1"));
                this.ddlWField.SelectedIndex = -1;
                this.ddlWField.Enabled = false;
                this.btnWellClear.Visible = true;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();
                this.gvwWellDetailList.DataBind();
                this.gvwWellDetailList.Visible = false;

                this.ddlWSRegion.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWSRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlWSRegion.SelectedValue);
                Dictionary<Int32, String> srList = DMG.getCountryList(srID);
                this.ddlWCountry.Items.Clear();
                this.ddlWCountry.DataSource = srList;
                this.ddlWCountry.DataTextField = "Value";
                this.ddlWCountry.DataValueField = "Key";
                this.ddlWCountry.DataBind();
                this.ddlWCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlWCountry.SelectedIndex = -1;
                this.ddlWCountry.Enabled = true;
                this.ddlWState.Items.Clear();
                this.ddlWState.DataBind();
                this.ddlWState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlWState.SelectedIndex = -1;
                this.ddlWState.Enabled = false;
                this.ddlWCounty.Items.Clear();
                this.ddlWCounty.DataBind();
                this.ddlWCounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlWCounty.SelectedIndex = -1;
                this.ddlWCounty.Enabled = false;
                this.ddlWField.Items.Clear();
                this.ddlWField.DataBind();
                this.ddlWField.Items.Insert(0, new ListItem("--- Select Oil & Gas Field ---", "-1"));
                this.ddlWField.SelectedIndex = -1;
                this.ddlWField.Enabled = false;
                this.btnWellClear.Visible = true;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();
                this.gvwWellDetailList.DataBind();
                this.gvwWellDetailList.Visible = false;

                this.ddlWCountry.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cID = Convert.ToInt32(this.ddlWCountry.SelectedValue);
                Dictionary<Int32, String> stList = DMG.getStateList(cID);
                this.ddlWState.Items.Clear();
                this.ddlWState.DataSource = stList;
                this.ddlWState.DataTextField = "Value";
                this.ddlWState.DataValueField = "Key";
                this.ddlWState.DataBind();
                this.ddlWState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlWState.SelectedIndex = -1;
                this.ddlWState.Enabled = true;
                this.ddlWCounty.Items.Clear();
                this.ddlWCounty.DataBind();
                this.ddlWCounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlWCounty.SelectedIndex = -1;
                this.ddlWCounty.Enabled = false;
                this.ddlWField.Items.Clear();
                this.ddlWField.DataBind();
                this.ddlWField.Items.Insert(0, new ListItem("--- Select Oil & Gas Field ---", "-1"));
                this.ddlWField.SelectedIndex = -1;
                this.ddlWField.Enabled = false;
                this.btnWellClear.Visible = true;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();
                this.gvwWellDetailList.DataBind();
                this.gvwWellDetailList.Visible = false;

                this.ddlWState.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 sID = Convert.ToInt32(this.ddlWState.SelectedValue);
                Dictionary<Int32, String> ctList = DMG.getCountyWithFieldCountList(sID);
                this.ddlWCounty.Items.Clear();
                this.ddlWCounty.DataSource = ctList;
                this.ddlWCounty.DataTextField = "Value";
                this.ddlWCounty.DataValueField = "Key";
                this.ddlWCounty.DataBind();
                this.ddlWCounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlWCounty.SelectedIndex = -1;
                this.ddlWCounty.Enabled = true;
                this.ddlWField.Items.Clear();
                this.ddlWField.DataBind();
                this.ddlWField.Items.Insert(0, new ListItem("--- Select Oil & Gas Field ---", "-1"));
                this.ddlWField.SelectedIndex = -1;
                this.ddlWField.Enabled = false;
                this.btnWellClear.Visible = true;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();
                this.gvwWellDetailList.DataBind();
                this.gvwWellDetailList.Visible = false;

                this.ddlWCounty.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWCounty_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cID = Convert.ToInt32(this.ddlWCounty.SelectedValue);
                Dictionary<Int32, String> fList = DMG.getFieldWithWellCountList(cID);
                this.ddlWField.Items.Clear();
                this.ddlWField.DataSource = fList;
                this.ddlWField.DataTextField = "Value";
                this.ddlWField.DataValueField = "Key";
                this.ddlWField.DataBind();
                this.ddlWField.Items.Insert(0, new ListItem("--- Select Oil & Gas Field ---", "-1"));
                this.ddlWField.SelectedIndex = -1;
                this.ddlWField.Enabled = true;
                this.btnWellClear.Visible = true;
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataBind();
                this.gvwWellDetailList.DataBind();
                this.gvwWellDetailList.Visible = false;

                this.ddlWField.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWField_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 fID = Convert.ToInt32(this.ddlWField.SelectedValue);
                System.Data.DataTable wTable = DMG.getGlobalFieldWellTable(fID);
                this.gvwWellList.SelectedIndex = -1;
                this.gvwWellList.DataSource = wTable;
                this.gvwWellList.DataBind();
                this.gvwWellList.Visible = true;
                this.gvwWellDetailList.DataBind();
                this.gvwWellDetailList.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWAOPC_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> rgList = DMG.getRegionList();
                this.ddlWARegion.Items.Clear();
                this.ddlWARegion.DataSource = rgList;
                this.ddlWARegion.DataTextField = "Value";
                this.ddlWARegion.DataValueField = "Key";
                this.ddlWARegion.DataBind();
                this.ddlWARegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlWARegion.SelectedIndex = -1;
                this.ddlWARegion.Enabled = true;
                this.ddlWASRegion.Items.Clear();
                this.ddlWASRegion.DataBind();
                this.ddlWASRegion.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlWASRegion.SelectedIndex = -1;
                this.ddlWASRegion.Enabled = false;
                this.ddlWACountry.Items.Clear();
                this.ddlWACountry.DataBind();
                this.ddlWACountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlWACountry.SelectedIndex = -1;
                this.ddlWACountry.Enabled = false;
                this.ddlWAState.Items.Clear();
                this.ddlWAState.DataBind();
                this.ddlWAState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlWAState.SelectedIndex = -1;
                this.ddlWAState.Enabled = false;
                this.ddlWACounty.Items.Clear();
                this.ddlWACounty.DataBind();
                this.ddlWACounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlWACounty.SelectedIndex = -1;
                this.ddlWACounty.Enabled = false;
                this.ddlWAField.Items.Clear();
                this.ddlWAField.DataBind();
                this.ddlWAField.Items.Insert(0, new ListItem("--- Select Oil & Gas Field ---", "-1"));
                this.ddlWAField.SelectedIndex = -1;
                this.ddlWAField.Enabled = false;
                this.txtWAName.Text = "";
                this.txtWAName.Enabled = false;
                this.txtWAAPI.Text = "";
                this.txtWAAPI.Enabled = false;
                this.txtWAUWI.Text = "";
                this.txtWAUWI.Enabled = false;
                this.txtWAPermit.Text = "";
                this.txtWAPermit.Enabled = false;
                this.txtWADate.Text = "";
                this.txtWADate.Enabled = false;
                this.txtWALat.Text = "";
                this.txtWALat.Enabled = false;
                this.txtWALon.Text = "";
                this.txtWALon.Enabled = false;
                this.wlEnclosure.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWARegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 rgID = Convert.ToInt32(this.ddlWARegion.SelectedValue);
                Dictionary<Int32, String> srList = DMG.getSubRegionList(rgID);
                this.ddlWASRegion.Items.Clear();
                this.ddlWASRegion.DataSource = srList;
                this.ddlWASRegion.DataTextField = "Value";
                this.ddlWASRegion.DataValueField = "Key";
                this.ddlWASRegion.DataBind();
                this.ddlWASRegion.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlWASRegion.SelectedIndex = -1;
                this.ddlWASRegion.Enabled = true;
                this.ddlWACountry.Items.Clear();
                this.ddlWACountry.DataBind();
                this.ddlWACountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlWACountry.SelectedIndex = -1;
                this.ddlWACountry.Enabled = false;
                this.ddlWAState.Items.Clear();
                this.ddlWAState.DataBind();
                this.ddlWAState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlWAState.SelectedIndex = -1;
                this.ddlWAState.Enabled = false;
                this.ddlWACounty.Items.Clear();
                this.ddlWACounty.DataBind();
                this.ddlWACounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlWACounty.SelectedIndex = -1;
                this.ddlWACounty.Enabled = false;
                this.ddlWAField.Items.Clear();
                this.ddlWAField.DataBind();
                this.ddlWAField.Items.Insert(0, new ListItem("--- Select Oil & Gas Field ---", "-1"));
                this.ddlWAField.SelectedIndex = -1;
                this.ddlWAField.Enabled = false;
                this.txtWAName.Text = "";
                this.txtWAName.Enabled = false;
                this.txtWAAPI.Text = "";
                this.txtWAAPI.Enabled = false;
                this.txtWAUWI.Text = "";
                this.txtWAUWI.Enabled = false;
                this.txtWAPermit.Text = "";
                this.txtWAPermit.Enabled = false;
                this.txtWADate.Text = "";
                this.txtWADate.Enabled = false;
                this.txtWALat.Text = "";
                this.txtWALat.Enabled = false;
                this.txtWALon.Text = "";
                this.txtWALon.Enabled = false;
                this.wlEnclosure.Visible = false;

                this.ddlWASRegion.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWASRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srgID = Convert.ToInt32(this.ddlWASRegion.SelectedValue);
                Dictionary<Int32, String> ctyList = DMG.getCountryList(srgID);
                this.ddlWACountry.Items.Clear();
                this.ddlWACountry.DataSource = ctyList;
                this.ddlWACountry.DataTextField = "Value";
                this.ddlWACountry.DataValueField = "Key";
                this.ddlWACountry.DataBind();
                this.ddlWACountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlWACountry.SelectedIndex = -1;
                this.ddlWACountry.Enabled = true;
                this.ddlWAState.Items.Clear();
                this.ddlWAState.DataBind();
                this.ddlWAState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlWAState.SelectedIndex = -1;
                this.ddlWAState.Enabled = false;
                this.ddlWACounty.Items.Clear();
                this.ddlWACounty.DataBind();
                this.ddlWACounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlWACounty.SelectedIndex = -1;
                this.ddlWACounty.Enabled = false;
                this.ddlWAField.Items.Clear();
                this.ddlWAField.DataBind();
                this.ddlWAField.Items.Insert(0, new ListItem("--- Select Oil & Gas Field ---", "-1"));
                this.ddlWAField.SelectedIndex = -1;
                this.ddlWAField.Enabled = false;
                this.txtWAName.Text = "";
                this.txtWAName.Enabled = false;
                this.txtWAAPI.Text = "";
                this.txtWAAPI.Enabled = false;
                this.txtWAUWI.Text = "";
                this.txtWAUWI.Enabled = false;
                this.txtWAPermit.Text = "";
                this.txtWAPermit.Enabled = false;
                this.txtWADate.Text = "";
                this.txtWADate.Enabled = false;
                this.txtWALat.Text = "";
                this.txtWALat.Enabled = false;
                this.txtWALon.Text = "";
                this.txtWALon.Enabled = false;
                this.wlEnclosure.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWACountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctyID = Convert.ToInt32(this.ddlWACountry.SelectedValue);
                Dictionary<Int32, String> stList = DMG.getStateList(ctyID);
                this.ddlWAState.Items.Clear();
                this.ddlWAState.DataSource = stList;
                this.ddlWAState.DataTextField = "Value";
                this.ddlWAState.DataValueField = "Key";
                this.ddlWAState.DataBind();
                this.ddlWAState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlWAState.SelectedIndex = -1;
                this.ddlWAState.Enabled = true;
                this.ddlWACounty.Items.Clear();
                this.ddlWACounty.DataBind();
                this.ddlWACounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlWACounty.SelectedIndex = -1;
                this.ddlWACounty.Enabled = false;
                this.ddlWAField.Items.Clear();
                this.ddlWAField.DataBind();
                this.ddlWAField.Items.Insert(0, new ListItem("--- Select Oil & Gas Field ---", "-1"));
                this.ddlWAField.SelectedIndex = -1;
                this.ddlWAField.Enabled = false;
                this.txtWAName.Text = "";
                this.txtWAName.Enabled = false;
                this.txtWAAPI.Text = "";
                this.txtWAAPI.Enabled = false;
                this.txtWAUWI.Text = "";
                this.txtWAUWI.Enabled = false;
                this.txtWAPermit.Text = "";
                this.txtWAPermit.Enabled = false;
                this.txtWADate.Text = "";
                this.txtWADate.Enabled = false;
                this.txtWALat.Text = "";
                this.txtWALat.Enabled = false;
                this.txtWALon.Text = "";
                this.txtWALon.Enabled = false;
                this.wlEnclosure.Visible = false;
                this.ddlWAState.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWAState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 sID = Convert.ToInt32(this.ddlWAState.SelectedValue);
                Dictionary<Int32, String> cnyList = DMG.getCountyWithFieldCountList(sID);
                this.ddlWACounty.Items.Clear();
                this.ddlWACounty.DataSource = cnyList;
                this.ddlWACounty.DataTextField = "Value";
                this.ddlWACounty.DataValueField = "Key";
                this.ddlWACounty.DataBind();
                this.ddlWACounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlWACounty.SelectedIndex = -1;
                this.ddlWACounty.Enabled = true;
                this.ddlWAField.Items.Clear();
                this.ddlWAField.DataBind();
                this.ddlWAField.Items.Insert(0, new ListItem("--- Select Oil & Gas Field ---", "-1"));
                this.ddlWAField.SelectedIndex = -1;
                this.ddlWAField.Enabled = false;
                this.txtWAName.Text = "";
                this.txtWAName.Enabled = false;
                this.txtWAAPI.Text = "";
                this.txtWAAPI.Enabled = false;
                this.txtWAUWI.Text = "";
                this.txtWAUWI.Enabled = false;
                this.txtWAPermit.Text = "";
                this.txtWAPermit.Enabled = false;
                this.txtWADate.Text = "";
                this.txtWADate.Enabled = false;
                this.txtWALat.Text = "";
                this.txtWALat.Enabled = false;
                this.txtWALon.Text = "";
                this.txtWALon.Enabled = false;
                this.wlEnclosure.Visible = false;
                this.ddlWACounty.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWACounty_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cID = Convert.ToInt32(this.ddlWACounty.SelectedValue);
                Dictionary<Int32, String> fList = DMG.getFieldWithWellCountList(cID);
                this.ddlWAField.Items.Clear();
                this.ddlWAField.DataSource = fList;
                this.ddlWAField.DataTextField = "Value";
                this.ddlWAField.DataValueField = "Key";
                this.ddlWAField.DataBind();
                this.ddlWAField.Items.Insert(0, new ListItem("--- Select Oil & Gas Field ---", "-1"));
                this.ddlWAField.SelectedIndex = -1;
                this.ddlWAField.Enabled = true;
                this.txtWAName.Text = "";
                this.txtWAName.Enabled = false;
                this.txtWAAPI.Text = "";
                this.txtWAAPI.Enabled = false;
                this.txtWAUWI.Text = "";
                this.txtWAUWI.Enabled = false;
                this.txtWAPermit.Text = "";
                this.txtWAPermit.Enabled = false;
                this.txtWADate.Text = "";
                this.txtWADate.Enabled = false;
                this.txtWALat.Text = "";
                this.txtWALat.Enabled = false;
                this.txtWALon.Text = "";
                this.txtWALon.Enabled = false;
                this.wlEnclosure.Visible = false;
                this.ddlWAField.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWAField_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtWAName.Text = "";
                this.txtWAName.Enabled = true;
                this.txtWAAPI.Text = "";
                this.txtWAAPI.Enabled = true;
                this.txtWAUWI.Text = "";
                this.txtWAUWI.Enabled = true;
                this.txtWAPermit.Text = "";
                this.txtWAPermit.Enabled = true;
                this.txtWADate.Text = "";
                this.txtWADate.Enabled = true;
                this.clndrWellSpudDate.SelectedDate = DateTime.Now;
                this.txtWALat.Text = "";
                this.txtWALat.Enabled = true;
                Dictionary<Int32, String> latOList = DMG.getLatCardinals();
                this.ddlWellAddLatO.Items.Clear();
                this.ddlWellAddLatO.DataSource = latOList;
                this.ddlWellAddLatO.DataValueField = "Key";
                this.ddlWellAddLatO.DataTextField = "Value";
                this.ddlWellAddLatO.DataBind();
                this.ddlWellAddLatO.Items.Insert(0, new ListItem("--- Select Direction ---", "-1"));
                this.ddlWellAddLatO.SelectedIndex = -1;
                this.ddlWellAddLatO.Enabled = true;
                this.txtWALon.Text = "";
                this.txtWALon.Enabled = true;
                Dictionary<Int32, String> lngOList = DMG.getLngCardinals();
                this.ddlWellAddLonO.Items.Clear();
                this.ddlWellAddLonO.DataSource = lngOList;
                this.ddlWellAddLonO.DataValueField = "Key";
                this.ddlWellAddLonO.DataTextField = "Value";
                this.ddlWellAddLonO.DataBind();
                this.ddlWellAddLonO.Items.Insert(0, new ListItem("--- Select Direction ---", "-1"));
                this.ddlWellAddLonO.SelectedIndex = -1;
                this.ddlWellAddLonO.Enabled = true;
                this.wlEnclosure.Visible = false;
                this.txtWAName.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWellAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnWellClear_Click(null, null);
                this.divWellList.Visible = false;
                this.divWellAdd.Visible = true;
                this.btnWellAddDone.Visible = true;
                Dictionary<Int32, String> opList = CLNT.getOpCoList();
                this.ddlWAOPC.Items.Clear();
                this.ddlWAOPC.DataSource = opList;
                this.ddlWAOPC.DataTextField = "Value";
                this.ddlWAOPC.DataValueField = "Key";
                this.ddlWAOPC.DataBind();
                this.ddlWAOPC.Items.Insert(0, new ListItem("--- Select Well Operator Company ---", "-1"));
                this.ddlWAOPC.SelectedIndex = -1;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWellNew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, oprID = -99, rgID = -99, srgID = -99, ctyID = -99, stID = -99, cnyID = -99, fldID = -99, oLon = -99, oLat = -99, clientID = 1; //Using ClientID for FESMARTs
                String wName = String.Empty, wAPI = String.Empty, wUWI = String.Empty, wPermit = String.Empty, wLat = String.Empty, wLon = String.Empty;
                DateTime wSpud = DateTime.Now;
                Decimal dLat = -99.000000000M, dLon = -99.000000000M, latitude = -99.000000000M, longitude = -99.000000000M;
                oprID = Convert.ToInt32(this.ddlWAOPC.SelectedValue);
                rgID = Convert.ToInt32(this.ddlWARegion.SelectedValue);
                srgID = Convert.ToInt32(this.ddlWASRegion.SelectedValue);
                ctyID = Convert.ToInt32(this.ddlWACountry.SelectedValue);
                stID = Convert.ToInt32(this.ddlWAState.SelectedValue);
                cnyID = Convert.ToInt32(this.ddlWACounty.SelectedValue);
                fldID = Convert.ToInt32(this.ddlWAField.SelectedValue);
                wName = Convert.ToString(this.txtWAName.Text);
                wAPI = Convert.ToString(this.txtWAAPI.Text);
                if (String.IsNullOrEmpty(wAPI))
                {
                    wAPI = "---";
                }
                wUWI = Convert.ToString(this.txtWAUWI.Text);
                if (String.IsNullOrEmpty(wUWI))
                {
                    wUWI = "---";
                }
                wPermit = Convert.ToString(this.txtWAPermit.Text);
                wSpud = Convert.ToDateTime(this.txtWADate.Text);
                wLat = Convert.ToString(this.txtWALat.Text);
                oLat = Convert.ToInt32(this.ddlWellAddLatO.SelectedValue);
                wLon = Convert.ToString(this.txtWALon.Text);
                oLon = Convert.ToInt32(this.ddlWellAddLonO.SelectedValue);
                if (String.IsNullOrEmpty(wName) || String.IsNullOrEmpty(wLon) || String.IsNullOrEmpty(wLat))
                {
                    if (String.IsNullOrEmpty(wName))
                    {
                        this.wlEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iWell.Attributes["class"] = "icon fa fa-warning";
                        this.spnWell.InnerText = " Warning!";
                        this.lblWASuccess.Text = "Well Name can not be null/empty. Please provide all values";
                        this.wlEnclosure.Visible = true;                                               
                    }
                    else if (wLon.Equals(null) && wLat.Equals(null))
                    {
                        this.wlEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iWell.Attributes["class"] = "icon fa fa-warning";
                        this.spnWell.InnerText = " Warning!";
                        this.lblWASuccess.Text = "Well Location, as indicated by Latitude and Longitude, values are required. Please provide all values";
                        this.wlEnclosure.Visible = true;
                    }
                    else if (wLon.Equals(null))
                    {
                        this.wlEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iWell.Attributes["class"] = "icon fa fa-warning";
                        this.spnWell.InnerText = " Warning!";
                        this.lblWASuccess.Text = "Well Longitude is a required field. Please provide all values";
                        this.wlEnclosure.Visible = true;
                    }
                    else if (wLat.Equals(null))
                    {
                        this.wlEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iWell.Attributes["class"] = "icon fa fa-warning";
                        this.spnWell.InnerText = " Warning!";
                        this.lblWASuccess.Text = "Well Latitude is a required field. Please provide all values";
                        this.wlEnclosure.Visible = true;
                    }
                }
                else
                {
                    dLat = Convert.ToDecimal(wLat);
                    dLon = Convert.ToDecimal(wLon);
                    //iResult = AST.addWellDemog(clientID, Convert.ToString(oprID), wName, dLon, oLon, dLat, oLat, rgID, srgID, ctyID, cnyID, fldID, stID, wAPI, wUWI, wPermit, wSpud, globWSt, domain, LoginName, domain); //Using FESmarts domain for both client domain and user domain
                    //if (iResult.Equals(1))
                    //{
                    //    this.btnWellNewCancel_Click(null, null);
                    //    this.wlEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                    //    this.iWell.Attributes["class"] = "icon fa fa-check-circle";
                    //    this.spnWell.InnerText = " Success!"; ;
                    //    this.lblWASuccess.Text = "Successfully inserted New Global Well";
                    //    this.wlEnclosure.Visible = true;
                    //}
                    //else
                    //{
                    //    this.btnWellNewCancel_Click(null, null);
                    //    this.wlEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    //    this.iWell.Attributes["class"] = "icon fa fa-warning";
                    //    this.spnWell.InnerText = " Warning!";
                    //    this.lblWASuccess.Text = "Unable to insert New Global Well";
                    //    this.wlEnclosure.Visible = true;
                    //}
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWellNewCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> opList = CLNT.getOpCoList();
                this.ddlWAOPC.Items.Clear();
                this.ddlWAOPC.DataSource = opList;
                this.ddlWAOPC.DataTextField = "Value";
                this.ddlWAOPC.DataValueField = "Key";
                this.ddlWAOPC.DataBind();
                this.ddlWAOPC.Items.Insert(0, new ListItem("--- Select Well Operator Company ---", "-1"));
                this.ddlWAOPC.SelectedIndex = -1;
                this.ddlWARegion.Items.Clear();
                this.ddlWARegion.DataBind();
                this.ddlWARegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlWARegion.SelectedIndex = -1;
                this.ddlWARegion.Enabled = false;
                this.ddlWASRegion.Items.Clear();
                this.ddlWASRegion.DataBind();
                this.ddlWASRegion.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlWASRegion.SelectedIndex = -1;
                this.ddlWASRegion.Enabled = false;
                this.ddlWACountry.Items.Clear();
                this.ddlWACountry.DataBind();
                this.ddlWACountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlWACountry.SelectedIndex = -1;
                this.ddlWACountry.Enabled = false;
                this.ddlWAState.Items.Clear();
                this.ddlWAState.DataBind();
                this.ddlWAState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlWAState.SelectedIndex = -1;
                this.ddlWAState.Enabled = false;
                this.ddlWACounty.Items.Clear();
                this.ddlWACounty.DataBind();
                this.ddlWACounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlWACounty.SelectedIndex = -1;
                this.ddlWACounty.Enabled = false;
                this.ddlWAField.Items.Clear();
                this.ddlWAField.DataBind();
                this.ddlWAField.Items.Insert(0, new ListItem("--- Select Oil & Gas Field ---", "-1"));
                this.ddlWAField.SelectedIndex = -1;
                this.ddlWAField.Enabled = false;
                this.txtWAName.Text = "";
                this.txtWAName.Enabled = false;
                this.txtWAAPI.Text = "";
                this.txtWAAPI.Enabled = false;
                this.txtWAUWI.Text = "";
                this.txtWAUWI.Enabled = false;
                this.txtWAPermit.Text = "";
                this.txtWAPermit.Enabled = false;
                this.txtWADate.Text = "";
                this.txtWADate.Enabled = false;
                this.clndrWellSpudDate.SelectedDate = DateTime.Now;
                this.txtWALat.Text = "";
                this.txtWALat.Enabled = false;
                this.ddlWellAddLatO.Items.Clear();
                this.ddlWellAddLatO.DataBind();
                this.ddlWellAddLatO.Items.Insert(0, new ListItem("--- Select Direction ---", "-1"));
                this.ddlWellAddLatO.SelectedIndex = -1;
                this.ddlWellAddLatO.Enabled = false;
                this.txtWALon.Text = "";
                this.txtWALon.Enabled = false;
                this.ddlWellAddLonO.Items.Clear();
                this.ddlWellAddLonO.DataBind();
                this.ddlWellAddLonO.Items.Insert(0, new ListItem("--- Select Direction ---", "-1"));
                this.ddlWellAddLonO.SelectedIndex = -1;
                this.ddlWellAddLonO.Enabled = false;
                this.lblWASuccess.Text = "";
                this.wlEnclosure.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWellAddDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnWellNewCancel_Click(null, null);
                this.divWellList.Visible = true;
                this.divWellAdd.Visible = false;
                Dictionary<Int32, String> rgList = DMG.getRegionList();
                this.ddlWRegion.Items.Clear();
                this.ddlWRegion.DataSource = rgList;
                this.ddlWRegion.DataTextField = "Value";
                this.ddlWRegion.DataValueField = "Key";
                this.ddlWRegion.DataBind();
                this.ddlWRegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlWRegion.SelectedIndex = -1;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlDscType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtAddDsclmr.Text = "";
                this.txtAddDsclmr.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDsclmrReset_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable dscTable = DMG.getDisclaimerTable();
                this.gvwDisclaimer.DataSource = dscTable;
                this.gvwDisclaimer.SelectedIndex = -1;
                this.gvwDisclaimer.PageIndex = 0;
                this.gvwDisclaimer.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDscTypeReset_Click(object sender, EventArgs e)
        {
            try
            {
                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCardReset_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable tableCard = DMG.getCardinals();
                this.gvwCardinals.DataSource = tableCard;
                this.gvwCardinals.PageIndex = 0;
                this.gvwCardinals.SelectedIndex = -1;
                this.gvwCardinals.DataBind();
                this.btnCardReset.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }            
        }
    }
}