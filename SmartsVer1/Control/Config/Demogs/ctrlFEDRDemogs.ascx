﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlFEDRDemogs.ascx.cs" Inherits="SmartsVer1.Control.Config.Demogs.ctrlFEDRDemogs" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

<link href="../../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript -->
<script src="../../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>

<!--- Page local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }

    .rotate {
        -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
        -moz-transform: rotate(90deg); /* Firefox */
        -ms-transform: rotate(90deg); /* IE 9 */
        transform: rotate(90deg); /* Standard syntax */
    }

    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper[data-stepper-steps="8"] li {
            width: 12%;
        }

        ol.stepper[data-stepper-steps="9"] li {
            width: 11%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }    
</style>
<!--- Page local script --->
<script type="text/javascript">
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("rotate");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").addClass("rotate");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("rotate");
        });
    });
</script>

<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">        
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>Configuration</li>
                <li><a href="../../../Config/Demogs/feDRDemogs_2A.aspx"> Demographics</a></li>
            </ol>
        </div>
    </div>    
</div>

<!-- Page Content -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-pills">
                            <li class="nav-item active"><a data-target="#Disc" data-toggle="tab">System Disclaimers</a></li>
                            <li class="nav-item"><a data-target="#DiscType" data-toggle="tab">Disclaimer Types</a></li>
                            <li class="nav-item"><a data-target="#ImgPrp" data-toggle="tab">Image Purpose</a></li>
                            <li class="nav-item"><a data-target="#ImgType" data-toggle="tab">Image Type</a></li>
                            <li class="nav-item"><a data-target="#LogType" data-toggle="tab">Log Type</a></li>
                            <li class="nav-item"><a data-target="#NGZ" data-toggle="tab">No Go Zone</a></li>
                            <li class="nav-item"><a data-target="#RunN" data-toggle="tab">Run Name</a></li>
                            <li class="nav-item"><a data-target="#FType" data-toggle="tab">File Type</a></li>
                            <li class="nav-item"><a data-target="#WSec" data-toggle="tab">Well Section</a></li>
                            <li class="nav-item"><a data-target="#Card" data-toggle="tab">Cardinals</a></li>
                            <li class="nav-item"><a data-target="#GWell" data-toggle="tab">Global Wells</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="Disc">
                                <asp:UpdateProgress ID="upgrsDisclaimer" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlDisclaimer">
                                    <ProgressTemplate>
                                        <div id="divDiscs" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                            z-index: 100;">
                                            <asp:Image ID="imgDisclaimerProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:UpdatePanel ID="upnlDisclaimer" runat="server">
                                    <ContentTemplate>
                                        <div id="divDiscTitle" class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <hr style="margin: auto;" />
                                                <div class="bg-light-blue">
                                                    <div class="text-center">
                                                        <h1 class="box-title">System Disclaimers Text</h1>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divDiscList" runat="server" class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <asp:GridView ID="gvwDisclaimer" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                    AllowPaging="True" AllowSorting="True" DataKeyNames="dscID" OnPageIndexChanging="gvwDisclaimer_PageIndexChanging"
                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="5" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                    RowStyle-CssClass="rows">
                                                    <Columns>
                                                        <asp:BoundField DataField="dscID" HeaderText="dscID" Visible="False" />
                                                        <asp:BoundField DataField="dsctID" HeaderText="Type" />
                                                        <asp:BoundField DataField="dscValue" HeaderText="Disclaimer Text" />
                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" SortExpression="uTime" ReadOnly="True" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                    </Columns>
                                                </asp:GridView>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnDsclmrAdd" runat="server" CssClass="btn btn-info" Text="Add New Disclaimer" Width="10%" OnClick="btnDsclmrAdd_Click" />
                                                    <asp:Button ID="btnDsclmrReset" runat="server" CssClass="btn btn-warning" Width="10%" Text="Reset" Visible="false" OnClick="btnDsclmrReset_Click" />
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divDiscAdd" runat="server" visible="false" class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon bg-light-blue">Type</span>
                                                    <asp:DropDownList ID="ddlDscType" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                        OnSelectedIndexChanged="ddlDscType_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select Disclaimer Type ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group">
                                                    <span class="input-group-addon bg-light-blue">Text</span>
                                                    <asp:TextBox ID="txtAddDsclmr" runat="server" CssClass="form-control" Enabled="false" Text="" CausesValidation="True" TextMode="MultiLine"
                                                        Height="50px" placeholder="Disclaimer Statement" />
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div runat="server" id="dscAddEnclosure" visible="false">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <h4><i runat="server" id="iDscAdd"><span runat="server" id="spnDscAdd"></span></i></h4>
                                                        <asp:Label ID="lblDsclmrSuccess" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnAddDsclmr" runat="server" CssClass="btn btn-success" Width="10%" Text="Add Disclaimer" OnClick="btnAddDsclmr_Click" />
                                                    <asp:Button ID="btnAddDsclmrCancel" runat="server" CssClass="btn btn-warning" Width="10%" Text="Clear Values" OnClick="btnAddDsclmrCancel_Click" />
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnDsclmrDone" runat="server" CssClass="btn btn-primary" Width="10%" Text="Done" OnClick="btnDsclmrDone_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="tab-pane fade" id="DiscType">
                                <asp:UpdateProgress ID="upgrsDscType" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlDscType">
                                    <ProgressTemplate>
                                        <div id="divDscType" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                            z-index: 100;">
                                            <asp:Image ID="imgDscTypeProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:UpdatePanel ID="upnlDscType" runat="server">
                                    <ContentTemplate>
                                        <div id="divDTypeTitle" class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <hr style="margin: auto;" />
                                                <div class="bg-light-blue">
                                                    <div class="text-center">
                                                        <h1 class="box-title">System Disclaimers Types</h1>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divDTypeList" runat="server" class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <asp:GridView ID="gvwDscTypeList" runat="server" AutoGenerateColumns="False" DataKeyNames="dsctID"
                                                    AllowPaging="True" AllowSorting="True" CssClass="mydatagrid" OnPageIndexChanging="gvwDscTypeList_PageIndexChanging"
                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                    RowStyle-CssClass="rows">
                                                    <Columns>
                                                        <asp:BoundField DataField="dsctID" HeaderText="dsctID" Visible="False" />
                                                        <asp:BoundField DataField="dsctValue" HeaderText="Disclaimer Type" SortExpression="dsctValue" />
                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" SortExpression="uTime" ReadOnly="True" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                    </Columns>
                                                </asp:GridView>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnDscTypeAdd" runat="server" CssClass="btn btn-info" Text="Add New Type" Width="10%" OnClick="btnDscTypeAdd_Click" />
                                                    <asp:Button ID="btnDscTypeReset" runat="server" CssClass="btn btn-warning" Text="Reset" Width="10%" OnClick="btnDscTypeReset_Click" />
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divDTypeAdd" runat="server" visible="false" class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon bg-light-blue">Disclaimer Type</span>
                                                    <asp:TextBox ID="txtAddDscTypeName" runat="server" CssClass="form-control" Text="" CausesValidation="true" placeholder="Disclaimer Type" />
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div runat="server" id="dtypeEnclosure" visible="false">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <h4><i runat="server" id="iDType"><span runat="server" id="spnDType"></span></i></h4>
                                                        <asp:Label ID="lblDscTypeSuccess" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnDscTypeAddNew" runat="server" CssClass="btn btn-success" Text="Add New Type" Width="10%" OnClick="btnDscTypeAddNew_Click" />
                                                    <asp:Button ID="btnDscTypeAddCancel" runat="server" CssClass="btn btn-warning" Text="Clear Values" CausesValidation="false"
                                                        OnClick="btnDscTypeAddCancel_Click" Width="10%" />
                                                </div>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnDscTypeDone" runat="server" CssClass="btn btn-primary" Text="Done" Width="10%" CausesValidation="false"
                                                        OnClick="btnDscTypeDone_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="tab-pane fade" id="ImgPrp">
                                <asp:UpdateProgress ID="upgrsImgPurpose" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlImgPurpose">
                                    <ProgressTemplate>
                                        <div id="divIP" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                            z-index: 100;">
                                            <asp:Image ID="imgIPProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:UpdatePanel ID="upnlImgPurpose" runat="server">
                                    <ContentTemplate>
                                        <div id="divImgPurpTitle" class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <hr style="margin: auto;" />
                                                <div class="bg-light-blue">
                                                    <div class="text-center">
                                                        <h1 class="box-title">Image Purpose Configuration</h1>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divImgPurposeList" runat="server" class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <asp:GridView ID="gvwImgPurposeList" runat="server" AutoGenerateColumns="False" DataKeyNames="imgpID"
                                                    AllowPaging="True" AllowSorting="True" CssClass="mydatagrid" EmptyDataText="No Image Purpose description found."
                                                    ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwImgPurposeList_PageIndexChanging"
                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                    RowStyle-CssClass="rows">
                                                    <Columns>
                                                        <asp:BoundField DataField="imgpID" HeaderText="imgpID" Visible="False" />
                                                        <asp:BoundField DataField="imgpValue" HeaderText="Image Purpose" SortExpression="imgpValue" />
                                                    </Columns>
                                                </asp:GridView>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnImgPurposeAdd" runat="server" CssClass="btn btn-info" Text="Add New Image Purpose" Width="10%" OnClick="btnImgPurposeAdd_Click" />
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divImgPurposeAdd" runat="server" visible="false" class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon bg-light-blue">Image Purpose</span>
                                                    <asp:TextBox ID="txtAddImgPurpose" runat="server" CssClass="form-control" Text="" CausesValidation="true" placeholder="Image Purpose (required)" />
                                                    <%--<asp:RequiredFieldValidator ID="rfvImgPurpose" runat="server" ControlToValidate="txtAddImgPurpose" ErrorMessage="*" Display="Dynamic"
                                                        ForeColor="Maroon" SetFocusOnError="true" />--%>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div runat="server" id="imgEnclosure" visible="false">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <h4><i runat="server" id="iImgP"><span runat="server" id="spnImgP"></span></i></h4>
                                                        <asp:Label ID="lblImgPurposeSuccess" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnImgPurposeAddNew" runat="server" CssClass="btn btn-success" Text="Add New Image Purpose" Width="10%" OnClick="btnImgPurposeAddNew_Click"
                                                        CausesValidation="True" />
                                                    <asp:Button ID="btnImgPurposeAddCancel" runat="server" CssClass="btn btn-warning" Text="Clear Values" Width="10%" CausesValidation="false"
                                                        OnClick="btnImgPurposeAddCancel_Click" />
                                                </div>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnImgPurposeDone" runat="server" CssClass="btn btn-primary" Text="Done" Visible="false" CausesValidation="false"
                                                        OnClick="btnImgPurposeDone_Click" Width="10%" />
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="tab-pane fade" id="ImgType">
                                <asp:UpdateProgress ID="upgrsImgType" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlImgType">
                                    <ProgressTemplate>
                                        <div id="divImgType" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                            z-index: 100;">
                                            <asp:Image ID="imgImgTypeProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:UpdatePanel ID="upnlImgType" runat="server">
                                    <ContentTemplate>
                                        <div id="divImgTypeTitle" class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <hr style="margin: auto;" />
                                                <div class="bg-light-blue">
                                                    <div class="text-center">
                                                        <h1 class="box-title">Image Purpose Configuration</h1>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divImgTypeList" runat="server" class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <asp:GridView ID="gvwImgTypeList" runat="server" AutoGenerateColumns="False" DataKeyNames="imgtypID" AllowPaging="True"
                                                    AllowSorting="True" CssClass="mydatagrid" EmptyDataText="No Image Types found" ShowHeaderWhenEmpty="true" OnPageIndexChanging="gvwImgTypeList_PageIndexChanging"
                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                    RowStyle-CssClass="rows">
                                                    <Columns>
                                                        <asp:BoundField DataField="imgtypID" HeaderText="imgtypID" Visible="False" />
                                                        <asp:BoundField DataField="imgtypValue" HeaderText="Image Type" SortExpression="stName" />
                                                    </Columns>
                                                </asp:GridView>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnImgTypeAdd" runat="server" CssClass="btn btn-info" Text="Add New Image Type" Width="10%" OnClick="btnImgTypeAdd_Click" />
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divImgTypeAdd" runat="server" class="row" visible="false">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group" style="width: 100%;">
                                                    <span class="input-group-addon bg-light-blue">Image Type</span>
                                                    <asp:TextBox ID="txtImgTypeAdd" runat="server" CssClass="form-control" Text="" CausesValidation="true" placeholder="Image Type (required)" />
                                                    <asp:RequiredFieldValidator ID="rfvImgTypeAdd" runat="server" ControlToValidate="txtImgTypeAdd" Display="Dynamic" ErrorMessage="*"
                                                        ForeColor="Maroon" SetFocusOnError="True" />
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div runat="server" id="itypEnclosure" visible="false">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <h4><i runat="server" id="iIType"><span runat="server" id="spnIType"></span></i></h4>
                                                        <asp:Label ID="lblImgTypeSuccess" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnImgTypeAddNew" runat="server" CssClass="btn btn-success" Width="10%" Text="Add New Type" OnClick="btnImgTypeAddNew_Click" />
                                                    <asp:Button ID="btnImgTypeAddCancel" runat="server" CssClass="btn btn-warning" Width="10%" Text="Clear Values" OnClick="btnImgTypeAddCancel_Click"
                                                        CausesValidation="False" />
                                                </div>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnImgTypeDone" runat="server" CssClass="btn btn-primary" Width="10%" Text="Done" Visible="false" OnClick="btnImgTypeDone_Click"
                                                        CausesValidation="False" />
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="tab-pane fade" id="LogType">
                                <asp:UpdateProgress ID="upgrsLogType" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlLogType">
                                    <ProgressTemplate>
                                        <div id="divLT" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                            z-index: 100;">
                                            <asp:Image ID="imgLogTypeProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:UpdatePanel ID="upnlLogType" runat="server">
                                    <ContentTemplate>
                                        <div id="divLogTitle" class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <hr style="margin: auto;" />
                                                <div class="bg-light-blue">
                                                    <div class="text-center">
                                                        <h1 class="box-title">Well Logs Type Configuration</h1>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divLogTypeList" runat="server" class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <asp:GridView runat="server" ID="gvwLogTypeList" AutoGenerateColumns="False" DataKeyNames="logID" AllowPaging="True" AllowSorting="True"
                                                    CssClass="mydatagrid" ShowHeaderWhenEmpty="True" EmptyDataText="No Log Type description found." OnPageIndexChanging="gvwLogTypeList_PageIndexChanging"
                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                    RowStyle-CssClass="rows">
                                                    <Columns>
                                                        <asp:BoundField DataField="logID" HeaderText="logID" Visible="False" />
                                                        <asp:BoundField DataField="logType" HeaderText="Log Type" SortExpression="logType" />
                                                    </Columns>
                                                </asp:GridView>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnLogTypeAdd" runat="server" CssClass="btn btn-info" Text="Add New Log Type" Width="10%" OnClick="btnLogTypeAdd_Click" />
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divLogTypeAdd" runat="server" class="row" visible="false">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group" style="width: 100%;">
                                                    <span class="input-group-addon bg-light-blue">Log Type</span>
                                                    <asp:TextBox ID="txtLogTypeAdd" runat="server" CssClass="form-control" Text="" CausesValidation="true" placeholder="Well Log Type (required)" />
                                                    <asp:RequiredFieldValidator ID="rfvLogTypeAdd" runat="server" ControlToValidate="txtLogTypeAdd" Display="Dynamic" ErrorMessage="*"
                                                        SetFocusOnError="True" />
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div runat="server" id="ltypEnclosure" visible="false">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <h4><i runat="server" id="iLType"><span runat="server" id="spnLType"></span></i></h4>
                                                        <asp:Label ID="lblLogTypeSuccess" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnLogTypeAddNew" runat="server" CssClass="btn btn-success" Text="Add New Type" Width="10%" OnClick="btnLogTypeAddNew_Click" />
                                                    <asp:Button ID="btnLogTypeAddCancel" runat="server" CssClass="btn btn-warning" Text="Clear Values" Width="10%" OnClick="btnLogTypeAddCancel_Click"
                                                        CausesValidation="False" />
                                                </div>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnLogTypeAddDone" runat="server" CssClass="btn btn-primary" Text="Done" Width="10%" OnClick="btnLogTypeAddDone_Click"
                                                        CausesValidation="False" Visible="False" />
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="tab-pane fade" id="NGZ">
                                <asp:UpdateProgress ID="upgrsNGZ" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlNGZ">
                                    <ProgressTemplate>
                                        <div id="divNGZPrg" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                            z-index: 100;">
                                            <asp:Image ID="imgNGZProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:UpdatePanel ID="upnlNGZ" runat="server">
                                    <ContentTemplate>
                                        <div id="divNGZTitle" class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <hr style="margin: auto;" />
                                                <div class="bg-light-blue">
                                                    <div class="text-center">
                                                        <h1 class="box-title">No-Go-Zone Configuration</h1>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divNGZList" runat="server" class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <asp:GridView ID="gvwNGZList" runat="server" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True"
                                                    CssClass="mydatagrid" DataKeyNames="ngzID" OnPageIndexChanging="gvwNGZList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                    PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows">
                                                    <Columns>
                                                        <asp:BoundField DataField="ngzID" HeaderText="ngzID" Visible="False" />
                                                        <asp:BoundField DataField="ngzCriteria" HeaderText="No Go Zone Criterion" />
                                                        <asp:BoundField DataField="ngzValue" HeaderText="No Go Zone Criterion Value" />
                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" SortExpression="uTime" ReadOnly="True" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                    </Columns>
                                                </asp:GridView>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnNGZAdd" runat="server" CssClass="btn btn-info" Text="Add New Criterion" Width="10%" OnClick="btnNGZAdd_Click"
                                                        CausesValidation="false" />
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divNGZAdd" runat="server" class="row" visible="false">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group" style="width: 100%;">
                                                    <span class="input-group-addon bg-light-blue">Criterion</span>
                                                    <asp:TextBox ID="txtNGZCAdd" runat="server" CssClass="form-control" Text="" CausesValidation="true" placeholder="Criterion Name (required)" />
                                                    <asp:RequiredFieldValidator ID="rfvNGZCAdd" runat="server" ControlToValidate="txtNGZCAdd" Display="Dynamic" ErrorMessage="*"
                                                        SetFocusOnError="true" ForeColor="Maroon" />
                                                </div>
                                                <div class="input-group" style="width: 100%;">
                                                    <span class="input-group-addon bg-light-blue">Value</span>
                                                    <asp:TextBox ID="txtNGZVAdd" runat="server" CssClass="form-control" Text="" CausesValidation="true" placeholder="Criterion Value (required)" />
                                                    <asp:RequiredFieldValidator ID="rfvNGZVAdd" runat="server" ControlToValidate="txtNGZVAdd" Display="Dynamic" ErrorMessage="*"
                                                        SetFocusOnError="true" ForeColor="Maroon" />
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div runat="server" id="ngzEnclosure" visible="false">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <h4><i runat="server" id="iNGZ"><span runat="server" id="spnNGZ"></span></i></h4>
                                                        <asp:Label ID="lblNGZSuccess" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnNGZAddNew" runat="server" CssClass="btn btn-success" Width="10%" Text="Add New Criterion" OnClick="btnNGZAddNew_Click" />
                                                    <asp:Button ID="btnNGZAddCancel" runat="server" CssClass="btn btn-warning" Text="Clear Values" OnClick="btnNGZAddCancel_Click"
                                                        CausesValidation="False"
                                                        Width="10%" />
                                                </div>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnNGZDone" runat="server" CssClass="btn btn-primary" Width="10%" Text="Done" OnClick="btnNGZDone_Click"
                                                        Visible="False"
                                                        CausesValidation="False" />
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="tab-pane fade" id="RunN">
                                <asp:UpdateProgress ID="upgrsRunName" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlRunName">
                                    <ProgressTemplate>
                                        <div id="divRN" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                            z-index: 100;">
                                            <asp:Image ID="imgRNProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:UpdatePanel ID="upnlRunName" runat="server">
                                    <ContentTemplate>
                                        <div id="divRunNameTitle" class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <hr style="margin: auto;" />
                                                <div class="bg-light-blue">
                                                    <div class="text-center">
                                                        <h1 class="box-title">Run Name Configuration</h1>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divRunNameList" runat="server" class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <asp:GridView ID="gvwRunNameList" runat="server" AutoGenerateColumns="False" DataKeyNames="rnamID" AllowPaging="True"
                                                    AllowSorting="True" CssClass="mydatagrid" OnPageIndexChanging="gvwRunNameList_PageIndexChanging"
                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                    RowStyle-CssClass="rows">
                                                    <Columns>
                                                        <asp:BoundField DataField="rnamID" HeaderText="rnamID" Visible="False" />
                                                        <asp:BoundField DataField="rnamValue" HeaderText="Run Name" />
                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" SortExpression="uTime" ReadOnly="True" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                    </Columns>
                                                </asp:GridView>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnRunNameAdd" runat="server" CssClass="btn btn-info" Text="Add New Run Name" Width="10%" OnClick="btnRunNameAdd_Click"
                                                        CausesValidation="false" />
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divRunNameAdd" runat="server" class="row" visible="false">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group" style="width: 100%;">
                                                    <span class="input-group-addon bg-light-blue">Run Name</span>
                                                    <asp:TextBox ID="txtRunNameAdd" runat="server" CssClass="form-control" Text="" CausesValidation="true" placeholder="Run Name (required)" />
                                                    <asp:RequiredFieldValidator ID="rfvRunNameAdd" runat="server" ControlToValidate="txtRunNameAdd" Display="Dynamic" ErrorMessage="*"
                                                        SetFocusOnError="true" ForeColor="Maroon" />
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div runat="server" id="rnnEnclosure" visible="false">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <h4><i runat="server" id="iRnn"><span runat="server" id="spnRnn"></span></i></h4>
                                                        <asp:Label ID="lblRunNameSuccess" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnRunNameAddNew" runat="server" CssClass="btn btn-success" Width="10%" Text="Add New Name" OnClick="btnRunNameAddNew_Click" />
                                                    <asp:Button ID="btnRunNameAddCancel" runat="server" CssClass="btn btn-warning" Width="10%" Text="Clear Values" OnClick="btnRunNameAddCancel_Click"
                                                        CausesValidation="False" />
                                                </div>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnRunNameDone" runat="server" CssClass="btn btn-primary" Width="10%" Visible="false" Text="Done" OnClick="btnRunNameDone_Click"
                                                        CausesValidation="False" />
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="tab-pane fade" id="FType">
                                <asp:UpdateProgress ID="uprgrsFileType" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlFileType">
                                    <ProgressTemplate>
                                        <div id="divFileTypePrgrss" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                            z-index: 100;">
                                            <asp:Image ID="imgFileTypeProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:UpdatePanel ID="upnlFileType" runat="server">
                                    <ContentTemplate>
                                        <div id="divFTypeTitle" class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <hr style="margin: auto;" />
                                                <div class="bg-light-blue">
                                                    <div class="text-center">
                                                        <h1 class="box-title">File Type Configuration</h1>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divFileTypeList" runat="server" class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <asp:GridView ID="gvwFileTypeList" runat="server" EmptyDataText="No Survey File Type description found."
                                                    ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="sftID" AllowPaging="True" AllowSorting="True" CssClass="mydatagrid"
                                                    OnPageIndexChanging="gvwFileTypeList_PageIndexChanging"
                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                    RowStyle-CssClass="rows">
                                                    <Columns>
                                                        <asp:BoundField DataField="sftID" HeaderText="rnsID" Visible="false" />
                                                        <asp:BoundField DataField="sftName" HeaderText="Survey File Type" />
                                                        <asp:BoundField DataField="sftDesc" HeaderText="Description" />
                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" SortExpression="uTime" ReadOnly="True" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                    </Columns>
                                                </asp:GridView>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnAddNewFileType" runat="server" CssClass="btn btn-info" Text="Add New File Type" Width="10%" OnClick="btnAddNewFileType_Click" />
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divFileTypeAdd" runat="server" class="row" visible="false">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group" style="width: 100%;">
                                                    <span class="input-group-addon bg-light-blue">File Type</span>
                                                    <asp:TextBox ID="txtFileTypeAdd" runat="server" CssClass="form-control" Text="" CausesValidation="True" placeholder="File Type (required)" />
                                                    <asp:RequiredFieldValidator ID="rfvFileTypeAdd" runat="server" Display="Dynamic" ControlToValidate="txtFileTypeAdd" ErrorMessage="*"
                                                        ForeColor="Maroon" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="input-group" style="width: 100%;">
                                                    <span class="input-group-addon bg-light-blue">Description</span>
                                                    <asp:TextBox ID="txtFileDescAdd" runat="server" CssClass="form-control" Text="" CausesValidation="True" placeholder="Description (required)" />
                                                    <asp:RequiredFieldValidator ID="rfvFileDescAdd" runat="server" Display="Dynamic" ControlToValidate="txtFileDescAdd" ErrorMessage="*"
                                                        ForeColor="Maroon" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div runat="server" id="ftypEnclosure" visible="false">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <h4><i runat="server" id="iFType"><span runat="server" id="spnFType"></span></i></h4>
                                                        <asp:Label ID="lblFileTypeSuccess" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnFileTypeAdd" runat="server" CssClass="btn btn-success" Width="10%" Text="Add New Type" OnClick="btnFileTypeAdd_Click" />
                                                    <asp:Button ID="btnFileTypeCancel" runat="server" CssClass="btn btn-warning" Text="Clear Values" Width="10%" CausesValidation="False"
                                                        OnClick="btnFileTypeCancel_Click" />
                                                </div>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnFileTypeDone" runat="server" CssClass="btn btn-primary" Width="10%" Visible="false" Text="Done" CausesValidation="False"
                                                        OnClick="btnFileTypeDone_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="tab-pane fade" id="WSec">
                                <asp:UpdateProgress ID="uprgrsWSec" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlWSec">
                                    <ProgressTemplate>
                                        <div id="divWSecPrgrss" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                            z-index: 100;">
                                            <asp:Image ID="imgWSecProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:UpdatePanel ID="upnlWSec" runat="server">
                                    <ContentTemplate>
                                        <div id="divWSecTitle" class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <hr style="margin: auto;" />
                                                <div class="bg-light-blue">
                                                    <div class="text-center">
                                                        <h1 class="box-title">Well Section Configuration</h1>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divWSecList" runat="server" class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <asp:GridView ID="gvwWSecList" runat="server" EmptyDataText="No Well Section description found."
                                                    ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="wsID" AllowPaging="True" AllowSorting="True" CssClass="mydatagrid"
                                                    OnPageIndexChanging="gvwWSecList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15"
                                                    HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows">
                                                    <Columns>
                                                        <asp:BoundField DataField="wsID" HeaderText="rnsID" Visible="false" />
                                                        <asp:BoundField DataField="wsName" HeaderText="Survey File Type" />
                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" SortExpression="uTime" ReadOnly="True" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                    </Columns>
                                                </asp:GridView>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnWSecAddNew" runat="server" CssClass="btn btn-info" Text="Add New Well Section" Width="10%" OnClick="btnWSecAddNew_Click" />
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divWSecAdd" runat="server" class="row" visible="false">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group" style="width: 100%;">
                                                    <span class="input-group-addon bg-light-blue">Well Section</span>
                                                    <asp:TextBox ID="txtWSecAdd" runat="server" CssClass="form-control" Text="" CausesValidation="True" placeholder="Well Section (required)" />
                                                    <asp:RequiredFieldValidator ID="rfvWSecAdd" runat="server" Display="Dynamic" ControlToValidate="txtWSecAdd" ErrorMessage="*"
                                                        ForeColor="Maroon" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div runat="server" id="wsEnclosure" visible="false">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <h4><i runat="server" id="iWS"><span runat="server" id="spnWS"></span></i></h4>
                                                        <asp:Label ID="lblWSecSuccess" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnWSecAdd" runat="server" CssClass="btn btn-success" Width="10%" Text="Add Section" OnClick="btnWSecAdd_Click" />
                                                    <asp:Button ID="btnWSecCancel" runat="server" CssClass="btn btn-warning" Text="Clear Values" Width="10%" CausesValidation="False"
                                                        OnClick="btnWSecCancel_Click" />
                                                </div>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnWSecDone" runat="server" CssClass="btn btn-primary" Width="10%" Visible="false" Text="Done" CausesValidation="False"
                                                        OnClick="btnWSecDone_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="tab-pane fade" id="Card">
                                <asp:UpdateProgress ID="uprgrsCardinal" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlCardinal">
                                    <ProgressTemplate>
                                        <div id="divCardPrgrss" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                            z-index: 100;">
                                            <asp:Image ID="imgCardProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:UpdatePanel ID="upnlCardinal" runat="server">
                                    <ContentTemplate>
                                        <div id="divCardTitle" class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <hr style="margin: auto;" />
                                                <div class="bg-light-blue">
                                                    <div class="text-center">
                                                        <h1 class="box-title">Cardinals Configuration</h1>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divCardList" runat="server" class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <asp:GridView ID="gvwCardinals" runat="server" EmptyDataText="No Data found."
                                                    ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="cID" AllowPaging="True" AllowSorting="True" CssClass="mydatagrid"
                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                    RowStyle-CssClass="rows" OnPageIndexChanging="gvwCardinals_PageIndexChanging">
                                                    <Columns>
                                                        <asp:BoundField DataField="cID" Visible="false" />
                                                        <asp:BoundField DataField="compassPoint" HeaderText="Compass Point" />
                                                        <asp:BoundField DataField="abbreviation" HeaderText="Abbreviation" />
                                                        <asp:BoundField DataField="traditionalName" HeaderText="Traditional Name" />
                                                        <asp:BoundField DataField="minAzimuth" HeaderText="Minimum Azimuth" />
                                                        <asp:BoundField DataField="midAzimuth" HeaderText="Middle Azimuth" />
                                                        <asp:BoundField DataField="maxAzimuth" HeaderText="Maximum Azimuth" />
                                                    </Columns>
                                                </asp:GridView>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnCardReset" runat="server" CssClass="btn btn-warning" Text="Reset" Width="10%" Visible="false" OnClick="btnCardReset_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="tab-pane fade" id="GWell">
                                <asp:UpdateProgress ID="uprgrsWell" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlWell">
                                    <ProgressTemplate>
                                        <div id="divWellPrgrss" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                            z-index: 100;">
                                            <asp:Image ID="imgWellProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:UpdatePanel ID="upnlWell" runat="server">
                                    <ContentTemplate>
                                        <div id="divWellTitle" class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <hr style="margin: auto;" />
                                                <div class="bg-light-blue">
                                                    <div class="text-center">
                                                        <h1 class="box-title">Global SMARTs Well Configuration</h1>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divWellList" runat="server" class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group" style="width: 100%;">
                                                        <span class="input-group-addon bg-light-blue">Geographic Region</span>
                                                        <asp:DropDownList ID="ddlWRegion" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                            OnSelectedIndexChanged="ddlWRegion_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- Select Major Geographic Region ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group" style="width: 100%;">
                                                        <span class="input-group-addon bg-light-blue">Sub-Region</span>
                                                        <asp:DropDownList ID="ddlWSRegion" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                            Enabled="false"
                                                            OnSelectedIndexChanged="ddlWSRegion_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- Select Geographic Sub-Region ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group" style="width: 100%;">
                                                        <span class="input-group-addon bg-light-blue">Country/Nation</span>
                                                        <asp:DropDownList ID="ddlWCountry" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                            Enabled="false"
                                                            OnSelectedIndexChanged="ddlWCountry_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- Select Country/Nation ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group" style="width: 100%;">
                                                        <span class="input-group-addon bg-light-blue">State/Province</span>
                                                        <asp:DropDownList ID="ddlWState" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true" Enabled="false"
                                                            OnSelectedIndexChanged="ddlWState_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- Select State/Province ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group" style="width: 100%;">
                                                        <span class="input-group-addon bg-light-blue">County/District</span>
                                                        <asp:DropDownList ID="ddlWCounty" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                            Enabled="false"
                                                            OnSelectedIndexChanged="ddlWCounty_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- Select County/District/Municipality ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group" style="width: 100%;">
                                                        <span class="input-group-addon bg-light-blue">Field</span>
                                                        <asp:DropDownList ID="ddlWField" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true" Enabled="false"
                                                            OnSelectedIndexChanged="ddlWField_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- Select Oil & Gas Field ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <asp:GridView ID="gvwWellList" runat="server" EmptyDataText="No Well found."
                                                    ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="welID" AllowPaging="True" AllowSorting="True" CssClass="mydatagrid"
                                                    OnPageIndexChanging="gvwWellList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15"
                                                    HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnSelectedIndexChanged="gvwWellList_SelectedIndexChanged"
                                                    Visible="false">
                                                    <Columns>
                                                        <asp:CommandField ShowSelectButton="True" SelectText="View Detail" ShowCancelButton="True" ItemStyle-ForeColor="Blue"></asp:CommandField>
                                                        <asp:BoundField DataField="welID" HeaderText="welID" Visible="false" />
                                                        <asp:BoundField DataField="welName" HeaderText="Well Name" />
                                                        <asp:BoundField DataField="welClient" HeaderText="Service Provider" />
                                                        <asp:BoundField DataField="WelOptr" HeaderText="Well Owner/Operator" />
                                                        <asp:BoundField DataField="welCnty" HeaderText="County/District" />
                                                        <asp:BoundField DataField="welSt" HeaderText="State/Province" />
                                                        <asp:BoundField DataField="welCty" HeaderText="Country/Nation" />
                                                        <asp:BoundField DataField="welSR" HeaderText="Sub Region" />
                                                        <asp:BoundField DataField="welR" HeaderText="Region" />
                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" ReadOnly="True" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                    </Columns>
                                                </asp:GridView>
                                                <asp:GridView ID="gvwWellDetailList" runat="server" EmptyDataText="No Well Details available" ShowHeaderWhenEmpty="true"
                                                    AutoGenerateColumns="false" DataKeyNames="welID" CssClass="mydatagrid" HeaderStyle-CssClass="header" RowStyle-CssClass="rows"
                                                    Visible="false">
                                                    <Columns>
                                                        <asp:BoundField DataField="welID" Visible="false" />
                                                        <asp:BoundField DataField="welLat" HeaderText="Decimal Latitude" />
                                                        <asp:BoundField DataField="welLng" HeaderText="Decimal Longitude" />
                                                        <asp:BoundField DataField="welLatDMS" HeaderText="Degrees Minutes Seconds (DMS) Latitude" />
                                                        <asp:BoundField DataField="welLngDMS" HeaderText="Degrees Minutes Seconds (DMS) Longitude" />
                                                    </Columns>
                                                </asp:GridView>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnWellAdd" runat="server" Width="10%" CssClass="btn btn-info" Text="Add New Well" OnClick="btnWellAdd_Click" />
                                                    <asp:Button ID="btnWellClear" runat="server" Width="10%" CssClass="btn btn-warning" Text="Clear Selection" Visible="false"
                                                        OnClick="btnWellClear_Click" />
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divWellAdd" runat="server" class="row" visible="false">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="input-group" style="width: 100%;">
                                                        <span class="input-group-addon bg-light-blue">Operator</span>
                                                        <asp:DropDownList ID="ddlWAOPC" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control"
                                                            OnSelectedIndexChanged="ddlWAOPC_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- Select Well Operator Company ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group" style="width: 100%;">
                                                        <span class="input-group-addon bg-light-blue">Geographic Region</span>
                                                        <asp:DropDownList ID="ddlWARegion" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                            OnSelectedIndexChanged="ddlWARegion_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- Select Major Geographic Region ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group" style="width: 100%;">
                                                        <span class="input-group-addon bg-light-blue">Sub-Region</span>
                                                        <asp:DropDownList ID="ddlWASRegion" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                            Enabled="false"
                                                            OnSelectedIndexChanged="ddlWASRegion_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- Select Geographic Sub-Region ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group" style="width: 100%;">
                                                        <span class="input-group-addon bg-light-blue">Country/Nation</span>
                                                        <asp:DropDownList ID="ddlWACountry" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                            Enabled="false"
                                                            OnSelectedIndexChanged="ddlWACountry_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- Select Country/Nation ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group" style="width: 100%;">
                                                        <span class="input-group-addon bg-light-blue">State/Province</span>
                                                        <asp:DropDownList ID="ddlWAState" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                            Enabled="false"
                                                            OnSelectedIndexChanged="ddlWAState_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- Select State/Province ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group" style="width: 100%;">
                                                        <span class="input-group-addon bg-light-blue">County/District</span>
                                                        <asp:DropDownList ID="ddlWACounty" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                            Enabled="false"
                                                            OnSelectedIndexChanged="ddlWACounty_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- Select County/District/Municipality ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group" style="width: 100%;">
                                                        <span class="input-group-addon bg-light-blue">Field</span>
                                                        <asp:DropDownList ID="ddlWAField" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                            Enabled="false"
                                                            OnSelectedIndexChanged="ddlWAField_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- Select Oil & Gas Field ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group" style="width: 100%;">
                                                        <span class="input-group-addon bg-light-blue">Name</span>
                                                        <asp:TextBox ID="txtWAName" runat="server" CssClass="form-control" Enabled="False" CausesValidation="True"
                                                            AutoComplete="off" placeholder="Well Name (required)" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group" style="width: 100%;">
                                                        <span class="input-group-addon bg-light-blue">UWI</span>
                                                        <asp:TextBox ID="txtWAUWI" runat="server" CssClass="form-control" Enabled="False" AutoComplete="off" placeholder="Universal Well Identifier (UWI)" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group" style="width: 100%;">
                                                        <span class="input-group-addon bg-light-blue">API</span>
                                                        <asp:TextBox ID="txtWAAPI" runat="server" CssClass="form-control" Enabled="False" AutoComplete="off" placeholder="America Petroleum Institude Identifier (API)" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group" style="width: 100%;">
                                                        <span class="input-group-addon bg-light-blue">Permit</span>
                                                        <asp:TextBox ID="txtWAPermit" runat="server" CssClass="form-control" Enabled="False" CausesValidation="True"
                                                            AutoComplete="off" placeholder="Well Permit (required)" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group" style="width: 100%;">
                                                        <span class="input-group-addon bg-light-blue">Spud</span>
                                                        <asp:TextBox runat="server" ID="txtWADate" CssClass="form-control" Enabled="false" AutoComplete="off" />
                                                        <ajaxToolkit:CalendarExtender ID="clndrWellSpudDate" runat="server" TargetControlID="txtWADate" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                    <div class="input-group" style="width: 100%;">
                                                        <span class="input-group-addon bg-light-blue">Latitude</span>
                                                        <asp:TextBox ID="txtWALat" runat="server" CssClass="form-control" CausesValidation="True" Enabled="false"
                                                            AutoComplete="off" placeholder="Latitude (required) (Range -90.00 to 90.00)" />
                                                        <asp:RangeValidator ID="rngLatitude" runat="server" ControlToValidate="txtWALat" MaximumValue="90.00" MinimumValue="-90.00"
                                                            Type="Double" SetFocusOnError="True" ErrorMessage="*" Font-Bold="True" Display="Dynamic" />
                                                        <asp:DropDownList ID="ddlWellAddLatO" runat="server" AppendDataBoundItems="false" AutoPostBack="true" CssClass="form-control"
                                                            Enabled="false">
                                                            <asp:ListItem Value="0">--- Select Direction ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                    <div class="input-group" style="width: 100%;">
                                                        <span class="input-group-addon bg-light-blue">Longitude</span>
                                                        <asp:TextBox ID="txtWALon" runat="server" CssClass="form-control" CausesValidation="True" Enabled="false"
                                                            AutoComplete="off" placeholder="Longitude (required) (Range -180.00 to 180.00)" />
                                                        <asp:RangeValidator ID="rngLongitude" runat="server" ControlToValidate="txtWALon" MaximumValue="180.00" MinimumValue="-180.00"
                                                            Type="Double" SetFocusOnError="True" ErrorMessage="*" Font-Bold="True" Display="Dynamic" />
                                                        <asp:DropDownList ID="ddlWellAddLonO" runat="server" AppendDataBoundItems="false" AutoPostBack="true" CssClass="form-control"
                                                            Enabled="false">
                                                            <asp:ListItem Value="0">--- Select Direction ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div runat="server" id="wlEnclosure" visible="false">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <h4><i runat="server" id="iWell"><span runat="server" id="spnWell"></span></i></h4>
                                                        <asp:Label runat="server" ID="lblWASuccess" />
                                                    </div>
                                                </div>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnWellNew" runat="server" Width="10%" CssClass="btn btn-success" Text="Add Well" OnClick="btnWellNew_Click" />
                                                    <asp:Button ID="btnWellNewCancel" runat="server" Width="10%" CssClass="btn btn-warning" Text="Clear Values" OnClick="btnWellNewCancel_Click"
                                                        CausesValidation="False" />
                                                </div>
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnWellAddDone" runat="server" Width="10%" CssClass="btn btn-primary" Text="Done" Visible="false" OnClick="btnWellAddDone_Click"
                                                        CausesValidation="False" />
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>