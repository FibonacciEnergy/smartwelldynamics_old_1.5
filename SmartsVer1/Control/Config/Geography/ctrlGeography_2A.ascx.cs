﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Net.Mail;
using System.Data.SqlTypes;
using System.Web.Security;
using System.Web.UI.WebControls;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;

namespace SmartsVer1.Control.Config.Geography
{
    public partial class ctrlGeography_2A : System.Web.UI.UserControl
    {
        String LoginName, username, domain;
        Int32 ClientID = -99;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;
                UP usrP = UP.GetUserProfile(LoginName);
                ClientID = Convert.ToInt32(usrP.clntID);

                //Configuring page dropdownlists
                Dictionary<Int32, String> regionList = DMG.getRegionList();
                Dictionary<Int32, String> hcOptlist = DMG.getHCOptionList();

                this.ddlSRRegion.Items.Clear();
                this.ddlSRRegion.DataSource = regionList;
                this.ddlSRRegion.DataTextField = "Value";
                this.ddlSRRegion.DataValueField = "Key";
                this.ddlSRRegion.DataBind();
                this.ddlSRRegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlSRRegion.SelectedIndex = -1;
                this.ddlAddSRMReg.Items.Clear();
                this.ddlAddSRMReg.DataSource = regionList;
                this.ddlAddSRMReg.DataTextField = "Value";
                this.ddlAddSRMReg.DataValueField = "Key";
                this.ddlAddSRMReg.DataBind();
                this.ddlAddSRMReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlAddSRMReg.SelectedIndex = -1;
                this.ddlCountryReg.Items.Clear();
                this.ddlCountryReg.DataSource = regionList;
                this.ddlCountryReg.DataTextField = "Value";
                this.ddlCountryReg.DataValueField = "Key";
                this.ddlCountryReg.DataBind();
                this.ddlCountryReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlCountryReg.SelectedIndex = -1;
                this.ddlCAddRegion.Items.Clear();
                this.ddlCAddRegion.DataSource = regionList;
                this.ddlCAddRegion.DataTextField = "Value";
                this.ddlCAddRegion.DataValueField = "Key";
                this.ddlCAddRegion.DataBind();
                this.ddlCAddRegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlCAddRegion.SelectedIndex = -1;
                this.ddlSTReg.Items.Clear();
                this.ddlSTReg.DataSource = regionList;
                this.ddlSTReg.DataTextField = "Value";
                this.ddlSTReg.DataValueField = "Key";
                this.ddlSTReg.DataBind();
                this.ddlSTReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlSTReg.SelectedIndex = -1;
                this.ddlAStReg.Items.Clear();
                this.ddlAStReg.DataSource = regionList;
                this.ddlAStReg.DataTextField = "Value";
                this.ddlAStReg.DataValueField = "Key";
                this.ddlAStReg.DataBind();
                this.ddlAStReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlAStReg.SelectedIndex = -1;
                this.ddlCntyReg.Items.Clear();
                this.ddlCntyReg.DataSource = regionList;
                this.ddlCntyReg.DataTextField = "Value";
                this.ddlCntyReg.DataValueField = "Key";
                this.ddlCntyReg.DataBind();
                this.ddlCntyReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlCntyReg.SelectedIndex = -1;
                this.ddlACntyReg.Items.Clear();
                this.ddlACntyReg.DataSource = regionList;
                this.ddlACntyReg.DataTextField = "Value";
                this.ddlACntyReg.DataValueField = "Key";
                this.ddlACntyReg.DataBind();
                this.ddlACntyReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlACntyReg.SelectedIndex = -1;
                this.ddlCityReg.Items.Clear();
                this.ddlCityReg.DataSource = regionList;
                this.ddlCityReg.DataTextField = "Value";
                this.ddlCityReg.DataValueField = "Key";
                this.ddlCityReg.DataBind();
                this.ddlCityReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlCityReg.SelectedIndex = -1;
                this.ddlACityReg.Items.Clear();
                this.ddlACityReg.DataSource = regionList;
                this.ddlACityReg.DataTextField = "Value";
                this.ddlACityReg.DataValueField = "Key";
                this.ddlACityReg.DataBind();
                this.ddlACityReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlACityReg.SelectedIndex = -1;
                this.ddlZipReg.Items.Clear();
                this.ddlZipReg.DataSource = regionList;
                this.ddlZipReg.DataTextField = "Value";
                this.ddlZipReg.DataValueField = "Key";
                this.ddlZipReg.DataBind();
                this.ddlZipReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlZipReg.SelectedIndex = -1;
                this.ddlAZipReg.Items.Clear();
                this.ddlAZipReg.DataSource = regionList;
                this.ddlAZipReg.DataTextField = "Value";
                this.ddlAZipReg.DataValueField = "Key";
                this.ddlAZipReg.DataBind();
                this.ddlAZipReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlAZipReg.SelectedIndex = -1;
                this.ddlFldReg.Items.Clear();
                this.ddlFldReg.DataSource = regionList;
                this.ddlFldReg.DataTextField = "Value";
                this.ddlFldReg.DataValueField = "Key";
                this.ddlFldReg.DataBind();
                this.ddlFldReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlFldReg.SelectedIndex = -1;
                this.ddlFAddReg.Items.Clear();
                this.ddlFAddReg.DataSource = regionList;
                this.ddlFAddReg.DataTextField = "Value";
                this.ddlFAddReg.DataValueField = "Key";
                this.ddlFAddReg.DataBind();
                this.ddlFAddReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlFAddReg.SelectedIndex = -1;
                this.ddlFAddOil.DataSource = hcOptlist;
                this.ddlFAddOil.DataTextField = "Value";
                this.ddlFAddOil.DataValueField = "Key";
                this.ddlFAddOil.DataBind();
                this.ddlFAddGas.DataSource = hcOptlist;
                this.ddlFAddGas.DataTextField = "Value";
                this.ddlFAddGas.DataValueField = "Key";
                this.ddlFAddGas.DataBind();
                this.ddlFAddAssoc.DataSource = hcOptlist;
                this.ddlFAddAssoc.DataTextField = "Value";
                this.ddlFAddAssoc.DataValueField = "Key";
                this.ddlFAddAssoc.DataBind();
                this.ddlFAddUnknown.DataSource = hcOptlist;
                this.ddlFAddUnknown.DataTextField = "Value";
                this.ddlFAddUnknown.DataValueField = "Key";
                this.ddlFAddUnknown.DataBind();

                //Loading GridViews on page
                if (!this.IsPostBack)
                {
                    DataTable tableRegions = DMG.getRegionsTable();
                    this.gvwMRList.DataSource = tableRegions;
                    this.gvwMRList.DataBind();

                    DataTable tableHCO = DMG.getHCOptionsTable();
                    this.gvwHCO.DataSource = tableHCO;
                    this.gvwHCO.DataBind();
                }
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        /// Major Region Routines
        
        protected void gvwMRList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable RegionTable = DMG.getRegionsTable();
                gvwMRList.PageIndex = e.NewPageIndex;
                gvwMRList.DataSource = RegionTable;
                gvwMRList.DataBind();
                this.btnMRCancel.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnMRAdd_Click(object sender, EventArgs e)
        {
            try
            {
                btnMRCancel_Click(null, null);
                this.grEnclosure.Visible = false;
                this.btnMRCancel.Enabled = true;
                this.btnMRCancel.CssClass = "btn btn-warning";
                this.divGRList.Attributes["class"] = "panel-collapse collapse";
                this.divGRAdd.Attributes["class"] = "panel-collapse collapse in";                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnMRCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.divGRList.Attributes["class"] = "panel-collapse collapse in";
                this.divGRAdd.Attributes["class"] = "panel-collapse collapse";
                this.grEnclosure.Visible = false;
                System.Data.DataTable grTable = DMG.getRegionsTable();
                this.gvwMRList.DataSource = grTable;
                this.gvwMRList.PageIndex = 0;
                this.gvwMRList.SelectedIndex = -1;
                this.gvwMRList.DataBind();
                this.gvwMRList.Focus();
                this.gvwMRList.Visible = true;
                this.btnMRCancel.Enabled = false;
                this.btnMRCancel.CssClass = "btn btn-default";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddMReg_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iReply = -99;

                String rName = null;
                rName = this.txtAddMReg.Text;
                if (!String.IsNullOrEmpty(rName))
                {
                    iReply = DMG.addMajorGRegion(rName, LoginName, domain);
                    if (iReply.Equals(1))
                    {
                        btnAddMRegCancel_Click(null, null);
                        this.grEnclosure.Visible = true;
                        this.grEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iMRMessage.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnMR.InnerText = " Success!";
                        this.lblMRegAddSuccess.Text = "Successfully inserted Region Name!";
                        this.gvwMRList.DataBind();

                        this.txtAddMReg.Focus();
                    }
                    else
                    {
                        btnAddMRegCancel_Click(null, null);
                        this.grEnclosure.Visible = true;
                        this.grEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iMRMessage.Attributes["class"] = "icon fa fa-warning";
                        this.spnMR.InnerText = " Warning!";
                        this.lblMRegAddSuccess.Text = "Unknown Error. Please try inserting Region Name again.";
                        this.lblMRegAddSuccess.Visible = true;

                        this.txtAddMReg.Focus();
                    }
                }
                else
                {
                    this.grEnclosure.Visible = true;
                    this.grEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iMRMessage.Attributes["class"] = "icon fa fa-warning";
                    this.spnMR.InnerText = " Warning!";
                    this.lblMRegAddSuccess.Text = "Unable to insert Null value for Region Name. Please provide a valid Region Name.";
                    this.lblMRegAddSuccess.Visible = true;

                    this.txtAddMReg.Focus();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddMRegCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtAddMReg.Text = "";
                this.grEnclosure.Visible = false;
                this.txtAddMReg.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnMRegDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnAddMRegCancel_Click(null, null);
                btnMRCancel_Click(null, null);                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        /// Major Region Routine Ends
        /// Sub Region Routines Begin

        protected void gvwSRList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 grID = Convert.ToInt32(this.ddlSRRegion.SelectedValue);
                DataTable subRegionTable = DMG.getSubRegionTable(grID);
                gvwSRList.PageIndex = e.NewPageIndex;
                gvwSRList.DataSource = subRegionTable;
                gvwSRList.DataBind();
                this.btnSRCancel.Enabled = true;
                this.btnSRCancel.CssClass = "btn btn-warning";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSRAdd_Click(object sender, EventArgs e)
        {
            try
            {
                btnSRCancel_Click(null, null);
                this.divSRList.Visible = false;
                this.divSRAdd.Visible = true;
                this.srEnclosure.Visible = false;
                this.btnSRDone.Visible = true;
                this.btnSRCancel.Enabled = true;
                this.btnSRCancel.CssClass = "btn btn-warning";
                this.divSRList.Attributes["class"] = "panel-collapse collapse";
                this.divSRAdd.Attributes["class"] = "panel-collapse collapse in";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSRCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> regionList = DMG.getRegionList();
                this.ddlSRRegion.Items.Clear();
                this.ddlSRRegion.DataSource = regionList;
                this.ddlSRRegion.DataTextField = "Value";
                this.ddlSRRegion.DataValueField = "Key";
                this.ddlSRRegion.DataBind();
                this.ddlSRRegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlSRRegion.SelectedIndex = -1;
                this.gvwSRList.DataSource = null;
                this.gvwSRList.DataBind();
                this.gvwSRList.Visible = false;
                this.btnSRCancel.Enabled = false;
                this.btnSRCancel.CssClass = "btn btn-default";
                this.divSRList.Attributes["class"] = "panel-collapse collapse in";
                this.divSRAdd.Attributes["class"] = "panel-collapse collapse";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSRAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.ddlAddSRMReg.Items.Clear();
                this.ddlAddSRMReg.DataBind();
                this.ddlAddSRMReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlAddSRMReg.SelectedIndex = -1;
                this.txtAddSRName.Text = "";
                this.txtAddSRName.Enabled = false;
                this.srEnclosure.Visible = false;
                this.btnSRAddNew.Enabled = false;

                this.ddlAddSRMReg.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSRAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iReply = -99, selR = -99;
                String srName = null;
                selR = Convert.ToInt32(this.ddlAddSRMReg.SelectedValue);
                srName = this.txtAddSRName.Text;
                if (!String.IsNullOrEmpty(srName))
                {
                    iReply = DMG.addGSRegion(selR, srName, LoginName, domain);
                    if (iReply.Equals(1))
                    {
                        btnSRAddCancel_Click(null, null);
                        this.srEnclosure.Visible = true;
                        this.srEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iSR.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnSR.InnerText = " Success!";
                        this.lblSRSuccess.Text = "Successfully inserted Sub-Region!";
                    }
                    else
                    {
                        btnSRAddCancel_Click(null, null);
                        this.srEnclosure.Visible = true;
                        this.srEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iSR.Attributes["class"] = "icon fa fa-warning";
                        this.spnSR.InnerText = " Warning!";
                        this.lblSRSuccess.Text = "Unknown Error. Please try inserting Sub-Region again";
                    }
                }
                else
                {
                    btnSRAddCancel_Click(null, null);
                    this.srEnclosure.Visible = true;
                    this.srEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iSR.Attributes["class"] = "icon fa fa-warning";
                    this.spnSR.InnerText = " Warning!";
                    this.lblSRSuccess.Text = "Unable to insert Null value for Sub-Region Name. Please provide a valid Sub-Region Name";

                    this.ddlAddSRMReg.Focus();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSRDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnSRAddCancel_Click(null, null);
                this.divSRList.Visible = true;
                this.divSRAdd.Visible = false;
                this.btnSRDone.Visible = false;

                btnSRCancel_Click(null, null);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAddSRMReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtAddSRName.Text = "";
                this.txtAddSRName.Enabled = true;
                this.btnSRAddNew.Enabled = true;
                this.srEnclosure.Visible = false;
                this.btnSRAddNew.Enabled = true;
                this.btnSRAddNew.CssClass = "btn btn-success";
                this.btnSRAddCancel.Enabled = true;
                this.btnSRAddCancel.CssClass = "btn btn-warning";
                this.txtAddSRName.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlSRRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 id = Convert.ToInt32(this.ddlSRRegion.SelectedValue);
                DataTable tableSubRegion = DMG.getSubRegionTable(id);
                this.gvwSRList.DataSource = tableSubRegion;
                this.gvwSRList.SelectedIndex = -1;
                this.gvwSRList.DataBind();
                this.gvwSRList.Visible = true;
                this.btnSRCancel.Enabled = true;
                this.btnSRCancel.CssClass = "btn btn-warning";                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        /// Sub Region Ends ///
        /// Country/Nation Starts ///

        protected void gvwCountryList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 gsrID = Convert.ToInt32(this.ddlCountrySR.SelectedValue);
                gvwCountryList.PageIndex = e.NewPageIndex;
                DataTable countryTable = DMG.getCountryTable(gsrID);
                gvwCountryList.DataSource = countryTable;
                gvwCountryList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCountryReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlCountryReg.SelectedValue);
                Dictionary<Int32, String> subregList = DMG.getSubRegionList(srID);

                this.ddlCountrySR.Items.Clear();
                this.ddlCountrySR.DataSource = subregList;
                this.ddlCountrySR.DataTextField = "Value";
                this.ddlCountrySR.DataValueField = "Key";
                this.ddlCountrySR.DataBind();
                this.ddlCountrySR.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlCountrySR.SelectedIndex = -1;
                this.ddlCountrySR.Enabled = true;
                this.gvwCountryList.DataBind();
                this.gvwCountryList.Visible = false;
                this.btnCountryCancel.Enabled = true;
                this.btnCountryCancel.CssClass = "btn btn-warning";
                this.ddlCountrySR.Focus();

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCountrySR_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 gsrID = Convert.ToInt32(this.ddlCountrySR.SelectedValue);
                DataTable tableCountry = DMG.getCountryTable(gsrID);
                this.gvwCountryList.DataSource = tableCountry;
                this.gvwCountryList.DataBind();
                this.gvwCountryList.Visible = true;

                this.gvwCountryList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCountryAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iReply = -99, ctySR = -99;
                String ctyName = String.Empty, ctyCode = String.Empty, ctyDCode = String.Empty;
                ctySR = Convert.ToInt32(this.ddlCAddSRegion.SelectedValue);
                ctyName = this.txtCAddName.Text;
                ctyCode = this.txtCAddCode.Text;
                ctyDCode = this.txtCAddDCode.Text;

                if (!String.IsNullOrEmpty(ctyName))
                {
                    iReply = DMG.addCountry(ctyName, ctyCode, ctyDCode, ctySR, LoginName, domain);
                    if (iReply.Equals(1))
                    {                        
                        this.ctyEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iCTY.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnCTY.InnerText = " Success!";
                        this.lblCountrySuccess.Text = "Successfully inserted Country to selected Geographic Sub-Region!";
                        this.ctyEnclosure.Visible = true;
                    }
                    else
                    {                        
                        this.ctyEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iCTY.Attributes["class"] = "icon fa fa-warning";
                        this.spnCTY.InnerText = " Warning!";
                        this.lblCountrySuccess.Text = "Unknown Error occured. Please try again.";
                        this.ctyEnclosure.Visible = true;
                    }
                }
                else
                {
                    this.ctyEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iCTY.Attributes["class"] = "icon fa fa-warning";
                    this.spnCTY.InnerText = " Warning!";
                    this.lblCountrySuccess.Text = "Unable to insert Null Country Name. Please provide a valid Country Name.";
                    this.ctyEnclosure.Visible = true;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCountryDone_Click(object sender, EventArgs e)
        {
            try
            {                
                btnCountryAddCancel_Click(null, null);
                btnCountryCancel_Click(null, null);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCAddRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlCAddRegion.SelectedValue);
                Dictionary<Int32, String> subregList = DMG.getSubRegionList(srID);

                this.ddlCAddSRegion.Items.Clear();
                this.ddlCAddSRegion.DataSource = subregList;
                this.ddlCAddSRegion.DataTextField = "Value";
                this.ddlCAddSRegion.DataValueField = "Key";
                this.ddlCAddSRegion.DataBind();
                this.ddlCAddSRegion.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlCAddSRegion.SelectedIndex = -1;
                this.ddlCAddSRegion.Enabled = true;
                this.txtCAddName.Text = "";
                this.txtCAddName.Enabled = false;

                this.ddlCAddSRegion.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCAddSRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtCAddName.Text = "";
                this.txtCAddName.Enabled = true;
                this.txtCAddCode.Text = "";
                this.txtCAddCode.Enabled = true;
                this.txtCAddDCode.Text = "";
                this.txtCAddDCode.Enabled = true;
                this.btnCountryAddNew.Enabled = true;
                this.btnCountryAddNew.CssClass = "btn btn-success";
                this.btnCountryAddCancel.Enabled = true;
                this.btnCountryAddCancel.CssClass = "btn btn-warning";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCountryAdd_Click(object sender, EventArgs e)
        {
            try
            {
                btnCountryCancel_Click(null, null);
                this.btnCountryCancel.Enabled = true;
                this.btnCountryCancel.CssClass = "btn btn-warning";
                this.divCTYList.Attributes["class"] = "panel-collapse collapse";
                this.divCTYAdd.Attributes["class"] = "panel-collapse collapse in";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCountryCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> regionList = DMG.getRegionList();
                this.ddlCountryReg.Items.Clear();
                this.ddlCountryReg.DataSource = regionList;
                this.ddlCountryReg.DataTextField = "Value";
                this.ddlCountryReg.DataValueField = "Key";
                this.ddlCountryReg.DataBind();
                this.ddlCountryReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlCountryReg.SelectedIndex = -1;
                this.ddlCountrySR.Items.Clear();
                this.ddlCountrySR.DataBind();
                this.ddlCountrySR.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlCountrySR.SelectedIndex = -1;
                this.ddlCountrySR.Enabled = false;
                this.gvwCountryList.DataBind();
                this.gvwCountryList.Visible = false;
                this.btnCountryCancel.Enabled = false;
                this.btnCountryCancel.CssClass = "btn btn-default";
                this.divCTYList.Attributes["class"] = "panel-collapse collapse in";
                this.divCTYAdd.Attributes["class"] = "panel-collapse collapse";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCountryAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> cregionList = DMG.getRegionList();
                this.ddlCAddRegion.Items.Clear();
                this.ddlCAddRegion.DataSource = cregionList;
                this.ddlCAddRegion.DataValueField = "key";
                this.ddlCAddRegion.DataTextField = "Value";
                this.ddlCAddRegion.DataBind();
                this.ddlCAddRegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlCAddRegion.SelectedIndex = -1;
                this.ddlCAddSRegion.Items.Clear();
                this.ddlCAddSRegion.DataBind();
                this.ddlCAddSRegion.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlCAddSRegion.SelectedIndex = -1;
                this.ddlCAddSRegion.Enabled = false;
                this.txtCAddName.Text = "";
                this.txtCAddName.Enabled = false;
                this.txtCAddCode.Text = "";
                this.txtCAddCode.Enabled = false;
                this.txtCAddDCode.Text = "";
                this.txtCAddDCode.Enabled = false;
                this.ctyEnclosure.Visible = false;
                this.btnCountryAddNew.Enabled = false;

                this.ddlCAddRegion.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
        /// Country Routine Ended
        /// State Routine

        protected void gvwSTList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 ctyID = Convert.ToInt32(this.ddlSTCountry.SelectedValue);
                DataTable StateTable = DMG.getStateTable(ctyID);
                gvwSTList.PageIndex = e.NewPageIndex;
                gvwSTList.DataSource = StateTable;
                gvwSTList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnStateAdd_Click(object sender, EventArgs e)
        {
            try
            {
                btnStateCancel_Click(null, null);
                btnStateAddCancel_Click(null, null);
                this.stEnclosure.Visible = false;
                this.btnStateCancel.Enabled = true;
                this.btnStateCancel.CssClass = "btn btn-warning";
                this.divSTList.Attributes["class"] = "panel-collapse collapse";
                this.divSTAdd.Attributes["class"] = "panel-collapse collapse in";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnStateCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> regList = DMG.getRegionList();
                this.ddlSTReg.Items.Clear();
                this.ddlSTReg.DataSource = regList;
                this.ddlSTReg.DataTextField = "Value";
                this.ddlSTReg.DataValueField = "Key";
                this.ddlSTReg.DataBind();
                this.ddlSTReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlSTReg.SelectedIndex = -1;
                this.ddlSTSR.Items.Clear();
                this.ddlSTSR.DataBind();
                this.ddlSTSR.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlSTSR.SelectedIndex = -1;
                this.ddlSTSR.Enabled = false;
                this.ddlSTCountry.Items.Clear();
                this.ddlSTCountry.DataBind();
                this.ddlSTCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlSTCountry.SelectedIndex = -1;
                this.ddlSTCountry.Enabled = false;
                this.gvwSTList.DataBind();
                this.gvwSTList.Visible = false;
                this.btnStateCancel.Enabled = false;
                this.btnStateCancel.CssClass = "btn btn-default";
                this.divSTList.Attributes["class"] = "panel-collapse collapse in";
                this.divSTAdd.Attributes["class"] = "panel-collapse collapse";
                this.ddlSTReg.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnStateAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.ddlAStReg.Items.Clear();
                this.ddlAStReg.DataBind();
                this.ddlAStReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlAStReg.SelectedIndex = -1;
                this.ddlAStSReg.Items.Clear();
                this.ddlAStSReg.DataBind();
                this.ddlAStSReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlAStSReg.SelectedIndex = -1;
                this.ddlAStSReg.Enabled = false;
                this.ddlAStCountry.Items.Clear();
                this.ddlAStCountry.DataBind();
                this.ddlAStCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlAStCountry.SelectedIndex = -1;
                this.ddlAStCountry.Enabled = false;
                this.txtAStName.Text = "";
                this.txtAStName.Enabled = false;
                this.txtAStCode.Text = "";
                this.txtAStCode.Enabled = false;
                this.txtAStAPI.Text = "";
                this.txtAStAPI.Enabled = false;
                this.txtAStANSI.Text = "";
                this.txtAStANSI.Enabled = false;
                this.txtAStFIPS.Text = "";
                this.txtAStFIPS.Enabled = false;
                this.stEnclosure.Visible = false;
                this.btnStateAddNew.Enabled = false;
                this.btnStateAddNew.CssClass = "btn btn-default";
                this.btnStateAddCancel.Enabled = false;
                this.btnStateAddCancel.CssClass = "btn btn-default";
                Dictionary<Int32, String> rgList = DMG.getRegionList();
                this.ddlSRRegion.Items.Clear();
                this.ddlSRRegion.DataSource = rgList;
                this.ddlSRRegion.DataTextField = "Value";
                this.ddlSRRegion.DataValueField = "Key";
                this.ddlSRRegion.DataBind();
                this.ddlSRRegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlSRRegion.SelectedIndex = -1;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnStateDone_Click(object sender, EventArgs e)
        {
            try
            {                
                btnStateAddCancel_Click(null, null);
                btnStateCancel_Click(null, null);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnStateAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iReply = -99, ctyID = -99;
                String stName = String.Empty, stCode = String.Empty, stAPI = String.Empty, stANSI = String.Empty, stFIPS = String.Empty;
                ctyID = Convert.ToInt32(this.ddlAStCountry.SelectedValue);
                stName = Convert.ToString(this.txtAStName.Text);
                stCode = Convert.ToString(this.txtAStCode.Text);
                stAPI = Convert.ToString(this.txtAStAPI.Text);
                stANSI = Convert.ToString(this.txtAStANSI.Text);
                stFIPS = Convert.ToString(this.txtAStFIPS.Text);

                if (String.IsNullOrEmpty(stName) || String.IsNullOrEmpty(stCode))
                {
                    this.stEnclosure.Visible = true;
                    this.stEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iST.Attributes["class"] = "icon fa fa-warning";
                    this.spnST.InnerText = " Warning!";
                    this.lblStateSuccess.Text = "Unable to insert Null values for State Name or State Code. Please provide valid Values.";
                }
                else
                {
                    if (String.IsNullOrEmpty(stAPI)) { stAPI = "---"; };
                    if (String.IsNullOrEmpty(stANSI)) { stANSI = "---"; };
                    if (String.IsNullOrEmpty(stFIPS)) { stFIPS = "---"; };
                    iReply = DMG.addState(stName, stCode, stAPI, stANSI, stFIPS, ctyID, LoginName, domain);
                    if (iReply.Equals(1))
                    {
                        this.stEnclosure.Visible = true;
                        this.stEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iST.Attributes["class"] = "icon fa fa-success";
                        this.spnST.InnerText = " Success!";
                        this.lblStateSuccess.Text = "Successfully inserted State into selected Country/Nation!";
                    }
                    else
                    {
                        this.stEnclosure.Visible = true;
                        this.stEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iST.Attributes["class"] = "icon fa fa-warning";
                        this.spnST.InnerText = " Warning!";
                        this.lblStateSuccess.Text = "Unknown Error. Please try again.";
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlSTReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlSTReg.SelectedValue);
                Dictionary<Int32, String> subregList = DMG.getSubRegionList(srID);

                this.ddlSTSR.Items.Clear();
                this.ddlSTSR.DataSource = subregList;
                this.ddlSTSR.DataTextField = "Value";
                this.ddlSTSR.DataValueField = "Key";
                this.ddlSTSR.DataBind();
                this.ddlSTSR.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlSTSR.SelectedIndex = -1;
                this.ddlSTSR.Enabled = true;
                this.ddlSTCountry.Items.Clear();
                this.ddlSTCountry.DataBind();
                this.ddlSTCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlSTCountry.SelectedIndex = -1;
                this.ddlSTCountry.Enabled = false;
                this.gvwSTList.DataBind();
                this.gvwSTList.Visible = false;
                this.btnStateCancel.Enabled = true;
                this.btnStateCancel.CssClass = "btn btn-warning";
                this.ddlSTSR.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlSTSR_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlSTSR.SelectedValue);
                Dictionary<Int32, String> countryList = DMG.getCountryList(srID);

                this.ddlSTCountry.Items.Clear();
                this.ddlSTCountry.DataSource = countryList;
                this.ddlSTCountry.DataTextField = "Value";
                this.ddlSTCountry.DataValueField = "Key";
                this.ddlSTCountry.DataBind();
                this.ddlSTCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlSTCountry.SelectedIndex = -1;
                this.ddlSTCountry.Enabled = true;
                this.gvwSTList.DataBind();
                this.gvwSTList.Visible = false;

                this.ddlSTCountry.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlSTCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctyID = Convert.ToInt32(this.ddlSTCountry.SelectedValue);
                DataTable tableState = DMG.getStateTable(ctyID);
                this.gvwSTList.DataSource = tableState;
                this.gvwSTList.DataBind();
                this.gvwSTList.Visible = true;

                this.gvwSTList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAStReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srgID = Convert.ToInt32(this.ddlAStReg.SelectedValue);
                Dictionary<Int32, String> subregList = DMG.getSubRegionList(srgID);

                this.ddlAStSReg.Items.Clear();
                this.ddlAStSReg.DataSource = subregList;
                this.ddlAStSReg.DataTextField = "Value";
                this.ddlAStSReg.DataValueField = "Key";
                this.ddlAStSReg.DataBind();
                this.ddlAStSReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlAStSReg.SelectedIndex = -1;
                this.ddlAStSReg.Enabled = true;
                this.ddlAStCountry.Items.Clear();
                this.ddlAStCountry.DataBind();
                this.ddlAStCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlAStCountry.SelectedIndex = -1;
                this.ddlAStCountry.Enabled = false;
                this.txtAStName.Text = "";
                this.txtAStName.Enabled = false;
                this.txtAStCode.Text = "";
                this.txtAStCode.Enabled = false;
                this.txtAStAPI.Text = "";
                this.txtAStAPI.Enabled = false;
                this.txtAStANSI.Text = "";
                this.txtAStANSI.Enabled = false;
                this.txtAStFIPS.Text = "";
                this.txtAStFIPS.Enabled = false;
                this.stEnclosure.Visible = false;
                this.btnStateAddNew.Enabled = false;
                this.btnStateAddCancel.Enabled = true;
                this.btnStateAddCancel.CssClass = "btn btn-warning";
                this.ddlAStSReg.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAStSReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srgID = Convert.ToInt32(this.ddlAStSReg.SelectedValue);
                Dictionary<Int32, String> countryList = DMG.getCountryList(srgID);

                this.ddlAStCountry.Items.Clear();
                this.ddlAStCountry.DataSource = countryList;
                this.ddlAStCountry.DataTextField = "Value";
                this.ddlAStCountry.DataValueField = "Key";
                this.ddlAStCountry.DataBind();
                this.ddlAStCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlAStCountry.SelectedIndex = -1;
                this.ddlAStCountry.Enabled = true;
                this.txtAStName.Text = "";
                this.txtAStName.Enabled = false;
                this.txtAStCode.Text = "";
                this.txtAStCode.Enabled = false;
                this.txtAStAPI.Text = "";
                this.txtAStAPI.Enabled = false;
                this.txtAStANSI.Text = "";
                this.txtAStANSI.Enabled = false;
                this.txtAStFIPS.Text = "";
                this.txtAStFIPS.Enabled = false;
                this.stEnclosure.Visible = false;
                this.btnStateAddNew.Enabled = false;
                this.ddlAStCountry.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAStCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtAStName.Text = "";
                this.txtAStName.Enabled = true;
                this.txtAStCode.Text = "";
                this.txtAStCode.Enabled = true;
                this.txtAStAPI.Text = "";
                this.txtAStAPI.Enabled = true;
                this.txtAStANSI.Text = "";
                this.txtAStANSI.Enabled = true;
                this.txtAStFIPS.Text = "";
                this.txtAStFIPS.Enabled = true;
                this.stEnclosure.Visible = false;
                this.btnStateAddNew.Enabled = true;
                this.btnStateAddNew.CssClass = "btn btn-success";
                this.txtAStName.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //State Routine Ended
        // County Routine Started

        protected void gvwCountyList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 stID = Convert.ToInt32(this.ddlCntyState.SelectedValue);
                DataTable CountyTable = DMG.getCountyTable(stID);
                this.gvwCountyList.PageIndex = e.NewPageIndex;
                this.gvwCountyList.DataSource = CountyTable;
                this.gvwCountyList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCountyAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnCountyCancel_Click(null, null);
                this.btnCountyCancel.Enabled = true;
                this.btnCountyCancel.CssClass = "btn btn-warning";
                this.cnyEnclosure.Visible = false;
                this.divCNYList.Attributes["class"] = "panel-collapse collapse";
                this.divCNYAdd.Attributes["class"] = "panel-collapse collapse in";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCountyCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> regList = DMG.getRegionList();
                this.ddlCntyReg.Items.Clear();
                this.ddlCntyReg.DataSource = regList;
                this.ddlCntyReg.DataTextField = "Value";
                this.ddlCntyReg.DataValueField = "Key";
                this.ddlCntyReg.DataBind();
                this.ddlCntyReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlCntyReg.SelectedIndex = -1;
                this.ddlCntyReg.Focus();
                this.ddlCntySR.Items.Clear();
                this.ddlCntySR.DataBind();
                this.ddlCntySR.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlCntySR.SelectedIndex = -1;
                this.ddlCntySR.Enabled = false;
                this.ddlCntyCountry.Items.Clear();
                this.ddlCntyCountry.DataBind();
                this.ddlCntyCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlCntyCountry.SelectedIndex = -1;
                this.ddlCntyCountry.Enabled = false;
                this.ddlCntyState.Items.Clear();
                this.ddlCntyState.DataBind();
                this.ddlCntyState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlCntyState.SelectedIndex = -1;
                this.ddlCntyState.Enabled = false;
                this.gvwCountyList.DataBind();
                this.gvwCountyList.Visible = false;
                this.btnCountyCancel.Enabled = false;
                this.btnCountyCancel.CssClass = "btn btn-default";                
                this.divCNYList.Attributes["class"] = "panel-collapse collapse in";
                this.divCNYAdd.Attributes["class"] = "panel-collapse collapse";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCountyAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> regList = DMG.getRegionList();
                this.ddlACntyReg.Items.Clear();
                this.ddlACntyReg.DataSource = regList;
                this.ddlACntyReg.DataTextField = "Value";
                this.ddlACntyReg.DataValueField = "Key";
                this.ddlACntyReg.DataBind();
                this.ddlACntyReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlACntyReg.SelectedIndex = -1;
                this.ddlACntySReg.Items.Clear();
                this.ddlACntySReg.DataBind();
                this.ddlACntySReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlACntySReg.SelectedIndex = -1;
                this.ddlACntySReg.Enabled = false;
                this.ddlACntyCountry.Items.Clear();
                this.ddlACntyCountry.DataBind();
                this.ddlACntyCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlACntyCountry.SelectedIndex = -1;
                this.ddlACntyCountry.Enabled = false;
                this.ddlACntyState.Items.Clear();
                this.ddlACntyState.DataBind();
                this.ddlACntyState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlACntyState.SelectedIndex = -1;
                this.ddlACntyState.Enabled = false;
                this.txtACntyName.Text = null;
                this.txtACntyName.Enabled = false;
                this.txtACntyCode.Text = null;
                this.txtACntyCode.Enabled = false;
                this.txtACntyFIPS.Text = null;
                this.txtACntyFIPS.Enabled = false;
                this.cnyEnclosure.Visible = false;
                this.ddlACntyReg.Focus();
                this.btnCountyAddNew.Enabled=false;
                this.btnCountyAddNew.CssClass = "btn btn-default";
                this.btnCountyAddCancel.Enabled = false;
                this.btnCountyAddCancel.CssClass = "btn btn-default";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCntyReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlCntyReg.SelectedValue);
                Dictionary<Int32, String> subregList = DMG.getSubRegionList(srID);

                this.ddlCntySR.Items.Clear();
                this.ddlCntySR.DataSource = subregList;
                this.ddlCntySR.DataTextField = "Value";
                this.ddlCntySR.DataValueField = "Key";
                this.ddlCntySR.DataBind();
                this.ddlCntySR.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlCntySR.SelectedIndex = -1;
                this.ddlCntySR.Enabled = true;
                this.ddlCntyCountry.Items.Clear();
                this.ddlCntyCountry.DataBind();
                this.ddlCntyCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlCntyCountry.SelectedIndex = -1;
                this.ddlCntyCountry.Enabled = false;
                this.ddlCntyState.Items.Clear();
                this.ddlCntyState.DataBind();
                this.ddlCntyState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlCntyState.SelectedIndex = -1;
                this.ddlCntyState.Enabled = false;
                this.gvwCountyList.DataBind();
                this.gvwCountyList.Visible = false;
                this.btnCountyCancel.Enabled = true;
                this.btnCountyCancel.CssClass = "btn btn-warning";
                this.ddlCountrySR.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCntySR_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlCntySR.SelectedValue);
                Dictionary<Int32, String> countryList = DMG.getCountryList(srID);

                this.ddlCntyCountry.Items.Clear();
                this.ddlCntyCountry.DataSource = countryList;
                this.ddlCntyCountry.DataTextField = "Value";
                this.ddlCntyCountry.DataValueField = "Key";
                this.ddlCntyCountry.DataBind();
                this.ddlCntyCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlCntyCountry.SelectedIndex = -1;
                this.ddlCntyCountry.Enabled = true;
                this.ddlCntyState.Items.Clear();
                this.ddlCntyState.DataBind();
                this.ddlCntyState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlCntyState.SelectedIndex = -1;
                this.ddlCntyState.Enabled = false;
                this.gvwCountyList.DataBind();
                this.gvwCountyList.Visible = false;
                this.cnyEnclosure.Visible = false;
                this.ddlCntyCountry.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCntyCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctID = Convert.ToInt32(this.ddlCntyCountry.SelectedValue);
                Dictionary<int, string> stateList = DMG.getStateList(ctID);

                this.ddlCntyState.Items.Clear();
                this.ddlCntyState.DataSource = stateList;
                this.ddlCntyState.DataTextField = "Value";
                this.ddlCntyState.DataValueField = "Key";
                this.ddlCntyState.DataBind();
                this.ddlCntyState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlCntyState.SelectedIndex = -1;
                this.ddlCntyState.Enabled = true;
                this.gvwCountyList.DataBind();
                this.gvwCountyList.Visible = false;
                this.cnyEnclosure.Visible = false;
                this.ddlCntyState.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCntyState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 stID = Convert.ToInt32(this.ddlCntyState.SelectedValue);
                DataTable cntyTable = DMG.getCountyTable(stID);
                this.gvwCountyList.DataSource = cntyTable;
                this.gvwCountyList.DataBind();
                this.gvwCountyList.Visible = true;
                this.gvwCountyList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwCountyList_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                gvwCountyList.EditIndex = e.NewEditIndex;
                gvwCountyList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlACntyReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 rID = Convert.ToInt32(this.ddlACntyReg.SelectedValue);
                Dictionary<Int32, String> subregList = DMG.getSubRegionList(rID);

                this.ddlACntySReg.Items.Clear();
                this.ddlACntySReg.DataSource = subregList;
                this.ddlACntySReg.DataTextField = "Value";
                this.ddlACntySReg.DataValueField = "Key";
                this.ddlACntySReg.Items.Clear();
                this.ddlACntySReg.DataBind();
                this.ddlACntySReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlACntySReg.SelectedIndex = -1;
                this.ddlACntySReg.Enabled = true;
                this.ddlACntyCountry.Items.Clear();
                this.ddlACntyCountry.DataBind();
                this.ddlACntyCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlACntyCountry.SelectedIndex = -1;
                this.ddlACntyCountry.Enabled = false;
                this.ddlACntyState.Items.Clear();
                this.ddlACntyState.DataBind();
                this.ddlACntyState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlACntyState.SelectedIndex = -1;
                this.ddlACntyState.Enabled = false;
                this.txtACntyName.Text = null;
                this.txtACntyName.Enabled = false;
                this.txtACntyCode.Text = null;
                this.txtACntyCode.Enabled = false;
                this.txtACntyFIPS.Text = null;
                this.txtACntyFIPS.Enabled = false;                
                this.ddlACntySReg.Focus();
                this.btnCountyAddNew.Enabled = false;
                this.btnCountyAddNew.CssClass = "btn btn-default";
                this.btnCountyAddCancel.Enabled = true;
                this.btnCountyAddCancel.CssClass = "btn btn-warning";
                this.cnyEnclosure.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlACntySReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlACntySReg.SelectedValue);
                Dictionary<Int32, String> countryList = DMG.getCountryList(srID);

                this.ddlACntyCountry.Items.Clear();
                this.ddlACntyCountry.DataSource = countryList;
                this.ddlACntyCountry.DataTextField = "Value";
                this.ddlACntyCountry.DataValueField = "Key";
                this.ddlACntyCountry.DataBind();
                this.ddlACntyCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlACntyCountry.SelectedIndex = -1;
                this.ddlACntyCountry.Enabled = true;
                this.ddlACntyState.Items.Clear();
                this.ddlACntyState.DataBind();
                this.ddlACntyState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlACntyState.SelectedIndex = -1;
                this.ddlACntyState.Enabled = false;
                this.txtACntyName.Text = null;
                this.txtACntyName.Enabled = false;
                this.txtACntyCode.Text = null;
                this.txtACntyCode.Enabled = false;
                this.txtACntyFIPS.Text = null;
                this.txtACntyFIPS.Enabled = false;
                this.cnyEnclosure.Visible = false;
                this.ddlACntyCountry.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlACntyCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctID = Convert.ToInt32(this.ddlACntyCountry.SelectedValue);
                Dictionary<int, string> stateList = DMG.getStateList(ctID);

                this.ddlACntyState.Items.Clear();
                this.ddlACntyState.DataSource = stateList;
                this.ddlACntyState.DataTextField = "Value";
                this.ddlACntyState.DataValueField = "Key";
                this.ddlACntyState.DataBind();
                this.ddlACntyState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlACntyState.SelectedIndex = -1;
                this.ddlACntyState.Enabled = true;
                this.txtACntyName.Text = null;
                this.txtACntyName.Enabled = false;
                this.txtACntyCode.Text = null;
                this.txtACntyCode.Enabled = false;
                this.txtACntyFIPS.Text = null;
                this.txtACntyFIPS.Enabled = false;
                this.cnyEnclosure.Visible = false;
                this.ddlACntyState.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlACntyState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtACntyName.Text = null;
                this.txtACntyName.Enabled = true;
                this.txtACntyCode.Text = null;
                this.txtACntyCode.Enabled = true;
                this.txtACntyFIPS.Text = null;
                this.txtACntyFIPS.Enabled = true;
                this.cnyEnclosure.Visible = false;
                this.txtACntyName.Focus();
                this.btnCountyAddNew.Enabled = true;
                this.btnCountyAddNew.CssClass = "btn btn-success";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCountyAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iReply = -99, stID = -99;
                String cName = String.Empty, cCode = String.Empty, cFIPS = String.Empty;
                stID = Convert.ToInt32(this.ddlACntyState.SelectedValue);
                cName = this.txtACntyName.Text;
                cCode = this.txtACntyCode.Text;
                cFIPS = this.txtACntyFIPS.Text;

                if (!String.IsNullOrEmpty(cName))
                {
                    iReply = DMG.addCounty(cName, cCode, cFIPS, stID, LoginName, domain);
                    if (iReply.Equals(1))
                    {
                        this.cnyEnclosure.Visible = true;
                        this.cnyEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iCNY.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnCNY.InnerText = " Success!";
                        this.lblCountySuccess.Text = "Successfully inserted County/District/Division!";
                    }
                    else
                    {
                        this.cnyEnclosure.Visible = true;
                        this.cnyEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iCNY.Attributes["class"] = "icon fa fa-warning";
                        this.spnCNY.InnerText = " Warning!";
                        this.lblCountySuccess.Text = "Unknown Error. Please try again.";
                    }
                }
                else
                {
                    this.cnyEnclosure.Visible = true;
                    this.cnyEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iCNY.Attributes["class"] = "icon fa fa-warning";
                    this.spnCNY.InnerText = " Warning!";
                    this.lblCountySuccess.Text = "Unable to insert Null values for County/District/Division name. Please provide a valid County/District/Division Name.";
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCountyAddDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnCountyAddCancel_Click(null, null);
                btnCountyCancel_Click(null, null);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //County Routines Ended
        //City Routine Start

        protected void gvwCityList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 cntyID = Convert.ToInt32(this.ddlCityCounty.SelectedValue);
                DataTable cityTable = DMG.getCityTable(cntyID);
                this.gvwCityList.PageIndex = e.NewPageIndex;
                this.gvwCityList.DataSource = cityTable;
                this.gvwCityList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCityAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnCityAddCancel_Click(null, null);
                this.btnCityCancel.Enabled = true;
                this.btnCityCancel.CssClass = "btn btn-warning";
                this.cityEnclosure.Visible = false;
                this.divCITYList.Attributes["class"] = "panel-collapse collapse";
                this.divCITYAdd.Attributes["class"] = "panel-collapse collapse in";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCityCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> regList = DMG.getRegionList();
                this.ddlCityReg.Items.Clear();
                this.ddlCityReg.DataSource = regList;
                this.ddlCityReg.DataTextField = "Value";
                this.ddlCityReg.DataValueField = "Key";
                this.ddlCityReg.DataBind();
                this.ddlCityReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlCityReg.SelectedIndex = -1;
                this.ddlCitySR.Items.Clear();
                this.ddlCitySR.DataBind();
                this.ddlCitySR.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlCitySR.SelectedIndex = -1;
                this.ddlCitySR.Enabled = false;
                this.ddlCityCountry.Items.Clear();
                this.ddlCityCountry.DataBind();
                this.ddlCityCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlCityCountry.SelectedIndex = -1;
                this.ddlCityCountry.Enabled = false;
                this.ddlCityState.Items.Clear();
                this.ddlCityState.DataBind();
                this.ddlCityState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlCityState.SelectedIndex = -1;
                this.ddlCityState.Enabled = false;
                this.ddlCityCounty.Items.Clear();
                this.ddlCityCounty.DataBind();
                this.ddlCityCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlCityCounty.SelectedIndex = -1;
                this.ddlCityCounty.Enabled = false;
                this.gvwCityList.DataBind();
                this.gvwCityList.Visible = false;
                this.cityEnclosure.Visible = false;
                this.ddlCityReg.Focus();
                this.btnCityCancel.Enabled = false;
                this.btnCityCancel.CssClass = "btn btn-default";
                this.divCITYList.Attributes["class"] = "panel-collapse collapse in";
                this.divCITYAdd.Attributes["class"] = "panel-collapse collapse";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCityAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> regList = DMG.getRegionList();
                this.ddlACityReg.Items.Clear();
                this.ddlACityReg.DataSource = regList;
                this.ddlACityReg.DataTextField = "Value";
                this.ddlACityReg.DataValueField = "Key";
                this.ddlACityReg.DataBind();
                this.ddlACityReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlACityReg.SelectedIndex = -1;
                this.ddlACitySReg.Items.Clear();
                this.ddlACitySReg.DataBind();
                this.ddlACitySReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlACitySReg.SelectedIndex = -1;
                this.ddlACitySReg.Enabled = false;
                this.ddlACityCountry.Items.Clear();
                this.ddlACityCountry.DataBind();
                this.ddlACityCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlACityCountry.SelectedIndex = -1;
                this.ddlACityCountry.Enabled = false;
                this.ddlACityState.Items.Clear();
                this.ddlACityState.DataBind();
                this.ddlACityState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlACityState.SelectedIndex = -1;
                this.ddlACityState.Enabled = false;
                this.ddlACityCounty.Items.Clear();
                this.ddlACityCounty.DataBind();
                this.ddlACityCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlACityCounty.SelectedIndex = -1;
                this.ddlACityCounty.Enabled = false;
                this.txtACityName.Text = null;
                this.txtACityName.Enabled = false;
                this.cityEnclosure.Visible = false;
                this.ddlACityReg.Focus();
                this.btnCityAddNew.Enabled = false;
                this.btnCityAddNew.CssClass = "btn btn-default";
                this.btnCityAddCancel.Enabled = false;
                this.btnCityAddCancel.CssClass = "btn btn-default";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCityReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlCityReg.SelectedValue);
                Dictionary<Int32, String> subregList = DMG.getSubRegionList(srID);

                this.ddlCitySR.Items.Clear();
                this.ddlCitySR.DataSource = subregList;
                this.ddlCitySR.DataTextField = "Value";
                this.ddlCitySR.DataValueField = "Key";
                this.ddlCitySR.DataBind();
                this.ddlCitySR.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlCitySR.SelectedIndex = -1;
                this.ddlCitySR.Enabled = true;
                this.ddlCityCountry.Items.Clear();
                this.ddlCityCountry.DataBind();
                this.ddlCityCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlCityCountry.SelectedIndex = -1;
                this.ddlCityCountry.Enabled = false;
                this.ddlCityState.Items.Clear();
                this.ddlCityState.DataBind();
                this.ddlCityState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlCityState.SelectedIndex = -1;
                this.ddlCityState.Enabled = false;
                this.ddlCityCounty.Items.Clear();
                this.ddlCityCounty.DataBind();
                this.ddlCityCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlCityCounty.SelectedIndex = -1;
                this.ddlCityCounty.Enabled = false;
                this.gvwCityList.DataBind();
                this.gvwCityList.Visible = false;
                this.cityEnclosure.Visible = false;
                this.ddlCitySR.Focus();
                this.btnCityCancel.Enabled = true;
                this.btnCityCancel.CssClass = "btn btn-warning";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCitySR_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlCitySR.SelectedValue);
                Dictionary<Int32, String> countryList = DMG.getCountryList(srID);

                this.ddlCityCountry.Items.Clear();
                this.ddlCityCountry.DataSource = countryList;
                this.ddlCityCountry.DataTextField = "Value";
                this.ddlCityCountry.DataValueField = "Key";
                this.ddlCityCountry.DataBind();
                this.ddlCityCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlCityCountry.SelectedIndex = -1;
                this.ddlCityCountry.Enabled = true;
                this.ddlCityState.Items.Clear();
                this.ddlCityState.DataBind();
                this.ddlCityState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlCityState.SelectedIndex = -1;
                this.ddlCityState.Enabled = false;
                this.ddlCityCounty.Items.Clear();
                this.ddlCityCounty.DataBind();
                this.ddlCityCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlCityCounty.SelectedIndex = -1;
                this.ddlCityCounty.Enabled = false;
                this.gvwCityList.DataBind();
                this.gvwCityList.Visible = false;
                this.cityEnclosure.Visible = false;
                this.ddlCityCountry.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCityCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctID = Convert.ToInt32(this.ddlCityCountry.SelectedValue);
                Dictionary<int, string> stateList = DMG.getStateList(ctID);

                this.ddlCityState.Items.Clear();
                this.ddlCityState.DataSource = stateList;
                this.ddlCityState.DataTextField = "Value";
                this.ddlCityState.DataValueField = "Key";
                this.ddlCityState.DataBind();
                this.ddlCityState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlCityState.SelectedIndex = -1;
                this.ddlCityState.Enabled = true;
                this.ddlCityCounty.Items.Clear();
                this.ddlCityCounty.DataBind();
                this.ddlCityCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlCityCounty.SelectedIndex = -1;
                this.ddlCityCounty.Enabled = false;
                this.gvwCityList.DataBind();
                this.gvwCityList.Visible = false;
                this.cityEnclosure.Visible = false;
                this.ddlCityState.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCityState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 sID = Convert.ToInt32(this.ddlCityState.SelectedValue);
                Dictionary<Int32, String> countyList = DMG.getCountyList(sID);

                this.ddlCityCounty.Items.Clear();
                this.ddlCityCounty.DataSource = countyList;
                this.ddlCityCounty.DataTextField = "Value";
                this.ddlCityCounty.DataValueField = "Key";
                this.ddlCityCounty.DataBind();
                this.ddlCityCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlCityCounty.SelectedIndex = -1;
                this.ddlCityCounty.Enabled = true;
                this.gvwCityList.DataBind();
                this.gvwCityList.Visible = false;
                this.ddlCityCounty.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCityCounty_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cntyID = Convert.ToInt32(this.ddlCityCounty.SelectedValue);
                DataTable CityTable = DMG.getCityTable(cntyID);
                this.gvwCityList.DataSource = CityTable;
                this.gvwCityList.DataBind();
                this.gvwCityList.Visible = true;
                this.gvwCityList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCityDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnCityAddCancel_Click(null, null);
                this.btnCityCancel_Click(null, null);                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCityAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iReply = -99, cID = -99;
                String cName = String.Empty;
                cID = Convert.ToInt32(this.ddlACityCounty.SelectedValue);
                cName = this.txtACityName.Text;

                if (String.IsNullOrEmpty(cName))
                {
                    this.cityEnclosure.Visible = true;
                    this.cityEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iCITY.Attributes["class"] = "icon fa fa-warning";
                    this.spnCITY.InnerText = " Warning!";
                    this.lblCitySuccess.Text = "Unable to insert null value as City Name. Please provide a valid City name.";
                }
                else
                {
                    iReply = DMG.addCity(cName, cID, LoginName, domain);
                    if (iReply.Equals(1))
                    {
                        this.cityEnclosure.Visible = true;
                        this.cityEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iCITY.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnCITY.InnerText = " Success!";
                        this.lblCitySuccess.Text = "Successfully inserted City/Town Name!";
                    }
                    else
                    {
                        this.cityEnclosure.Visible = true;
                        this.cityEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iCITY.Attributes["class"] = "icon fa fa-warning";
                        this.spnCITY.InnerText = " Warning!";
                        this.lblCitySuccess.Text = "Unknown Error. Please try again.";
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlACityReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlACityReg.SelectedValue);
                Dictionary<Int32, String> subregList = DMG.getSubRegionList(srID);

                this.ddlACitySReg.Items.Clear();
                this.ddlACitySReg.DataSource = subregList;
                this.ddlACitySReg.DataTextField = "Value";
                this.ddlACitySReg.DataValueField = "Key";
                this.ddlACitySReg.DataBind();
                this.ddlACitySReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlACitySReg.SelectedIndex = -1;
                this.ddlACitySReg.Enabled = true;
                this.ddlACityCountry.Items.Clear();
                this.ddlACityCountry.DataBind();
                this.ddlACityCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlACityCountry.SelectedIndex = -1;
                this.ddlACityCountry.Enabled = false;
                this.ddlACityState.Items.Clear();
                this.ddlACityState.DataBind();
                this.ddlACityState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlACityState.SelectedIndex = -1;
                this.ddlACityState.Enabled = false;
                this.ddlACityCounty.Items.Clear();
                this.ddlACityCounty.DataBind();
                this.ddlACityCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlACityCounty.SelectedIndex = -1;
                this.ddlACityCounty.Enabled = false;
                this.txtACityName.Text = null;
                this.txtACityName.Enabled = false;
                this.cityEnclosure.Visible = false;
                this.ddlACitySReg.Focus();
                this.btnCityAddCancel.Enabled = true;
                this.btnCityAddCancel.CssClass = "btn btn-warning";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlACitySReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlACitySReg.SelectedValue);
                Dictionary<Int32, String> countryList = DMG.getCountryList(srID);

                this.ddlACityCountry.Items.Clear();
                this.ddlACityCountry.DataSource = countryList;
                this.ddlACityCountry.DataTextField = "Value";
                this.ddlACityCountry.DataValueField = "Key";
                this.ddlACityCountry.DataBind();
                this.ddlACityCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlACityCountry.SelectedIndex = -1;
                this.ddlACityCountry.Enabled = true;
                this.ddlACityState.Items.Clear();
                this.ddlACityState.DataBind();
                this.ddlACityState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlACityState.SelectedIndex = -1;
                this.ddlACityState.Enabled = false;
                this.ddlACityCounty.Items.Clear();
                this.ddlACityCounty.DataBind();
                this.ddlACityCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlACityCounty.SelectedIndex = -1;
                this.ddlACityCounty.Enabled = false;
                this.txtACityName.Text = null;
                this.txtACityName.Enabled = false;
                this.cityEnclosure.Visible = false;
                this.ddlACityCountry.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlACityCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctID = Convert.ToInt32(this.ddlACityCountry.SelectedValue);
                Dictionary<int, string> stateList = DMG.getStateList(ctID);

                this.ddlACityState.Items.Clear();
                this.ddlACityState.DataSource = stateList;
                this.ddlACityState.DataTextField = "Value";
                this.ddlACityState.DataValueField = "Key";
                this.ddlACityState.DataBind();
                this.ddlACityState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlACityState.SelectedIndex = -1;
                this.ddlACityState.Enabled = true;
                this.ddlACityCounty.Items.Clear();
                this.ddlACityCounty.DataBind();
                this.ddlACityCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlACityCounty.SelectedIndex = -1;
                this.ddlACityCounty.Enabled = false;
                this.txtACityName.Text = null;
                this.txtACityName.Enabled = false;
                this.cityEnclosure.Visible = false;
                this.ddlACityState.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlACityState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 sID = Convert.ToInt32(this.ddlACityState.SelectedValue);
                Dictionary<Int32, String> countyList = DMG.getCountyList(sID);

                this.ddlACityCounty.Items.Clear();
                this.ddlACityCounty.DataSource = countyList;
                this.ddlACityCounty.DataTextField = "Value";
                this.ddlACityCounty.DataValueField = "Key";
                this.ddlACityCounty.DataBind();
                this.ddlACityCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlACityCounty.SelectedIndex = -1;
                this.ddlACityCounty.Enabled = true;
                this.txtACityName.Text = null;
                this.txtACityName.Enabled = false;
                this.cityEnclosure.Visible = false;
                this.ddlACityCounty.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlACityCounty_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtACityName.Text = null;
                this.txtACityName.Enabled = true;
                this.cityEnclosure.Visible = false;
                this.btnCityAddNew.Enabled = true;
                this.btnCityAddNew.CssClass = "btn btn-success";
                this.txtACityName.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //City Routine Ended
        //Zip Routine started
        protected void gvwZipList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 cntyID = Convert.ToInt32(this.ddlZipCounty.SelectedValue);
                DataTable ZipTable = DMG.getZipTable(cntyID);
                this.gvwZipList.PageIndex = e.NewPageIndex;
                this.gvwZipList.DataSource = ZipTable;
                this.gvwZipList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnZipAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.divZIPList.Attributes["class"] = "panel-collapse collapse";
                this.divZIPAdd.Attributes["class"] = "panel-collapse collapse in";
                this.btnZipCancel.Enabled = true;
                this.btnZipCancel.CssClass = "btn btn-warning";
                this.zipEnclosure.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnZipCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> regList = DMG.getRegionList();
                this.ddlZipReg.Items.Clear();
                this.ddlZipReg.DataSource = regList;
                this.ddlZipReg.DataTextField = "Value";
                this.ddlZipReg.DataValueField = "Key";
                this.ddlZipReg.DataBind();
                this.ddlZipReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlZipReg.SelectedIndex = -1;
                this.ddlZipSR.Items.Clear();
                this.ddlZipSR.DataBind();
                this.ddlZipSR.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlZipSR.SelectedIndex = -1;
                this.ddlZipSR.Enabled = false;
                this.ddlZipCountry.Items.Clear();
                this.ddlZipCountry.DataBind();
                this.ddlZipCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlZipCountry.SelectedIndex = -1;
                this.ddlZipCountry.Enabled = false;
                this.ddlZipState.Items.Clear();
                this.ddlZipState.DataBind();
                this.ddlZipState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlZipState.SelectedIndex = -1;
                this.ddlZipState.Enabled = false;
                this.ddlZipCounty.Items.Clear();
                this.ddlZipCounty.DataBind();
                this.ddlZipCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlZipCounty.SelectedIndex = -1;
                this.ddlZipCounty.Enabled = false;
                this.gvwZipList.DataBind();
                this.gvwZipList.Visible = false;
                this.zipEnclosure.Visible = false;
                this.ddlZipReg.Focus();
                this.btnZipCancel.Enabled = false;
                this.btnZipCancel.CssClass = "btn btn-default";
                this.divZIPList.Attributes["class"] = "panel-collapse collapse in";
                this.divZIPAdd.Attributes["class"] = "panel-collapse collapse";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnZipAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> regList = DMG.getRegionList();
                this.ddlAZipReg.Items.Clear();
                this.ddlAZipReg.DataSource = regList;
                this.ddlAZipReg.DataTextField = "Value";
                this.ddlAZipReg.DataValueField = "Key";
                this.ddlAZipReg.DataBind();
                this.ddlAZipReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlAZipReg.SelectedIndex = -1;
                this.ddlAZipSReg.Items.Clear();
                this.ddlAZipSReg.DataBind();
                this.ddlAZipSReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlAZipSReg.SelectedIndex = -1;
                this.ddlAZipSReg.Enabled = false;
                this.ddlAZipCountry.Items.Clear();
                this.ddlAZipCountry.DataBind();
                this.ddlAZipCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlAZipCountry.SelectedIndex = -1;
                this.ddlAZipCountry.Enabled = false;
                this.ddlAZipState.Items.Clear();
                this.ddlAZipState.DataBind();
                this.ddlAZipState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlAZipState.SelectedIndex = -1;
                this.ddlAZipState.Enabled = false;
                this.ddlAZipCounty.Items.Clear();
                this.ddlAZipCounty.DataBind();
                this.ddlAZipCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlAZipCounty.SelectedIndex = -1;
                this.ddlAZipCounty.Enabled = false;
                this.ddlAZipDatum.Items.Clear();
                this.ddlAZipDatum.DataBind();
                this.ddlAZipDatum.Items.Insert(0, new ListItem("--- Select Datum ---", "-1"));
                this.ddlAZipDatum.SelectedIndex = -1;
                this.ddlAZipDatum.Enabled = false;
                this.txtAZipLatitude.Text = null;
                this.txtAZipLatitude.Enabled = false;
                this.txtAZipLongitude.Text = null;
                this.txtAZipLongitude.Enabled = false;
                this.txtAZipValue.Text = null;
                this.txtAZipValue.Enabled = false;
                this.zipEnclosure.Visible = false;
                this.ddlAZipReg.Focus();
                this.btnZipAddNew.Enabled = false;
                this.btnZipAddNew.CssClass = "btn btn-default";
                this.btnZipAddCancel.Enabled = false;
                this.btnZipAddCancel.CssClass = "btn btn-default";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwZipList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlZipReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlZipReg.SelectedValue);
                Dictionary<Int32, String> subregList = DMG.getSubRegionList(srID);

                this.ddlZipSR.Items.Clear();
                this.ddlZipSR.DataSource = subregList;
                this.ddlZipSR.DataTextField = "Value";
                this.ddlZipSR.DataValueField = "Key";
                this.ddlZipSR.DataBind();
                this.ddlZipSR.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlZipSR.SelectedIndex = -1;
                this.ddlZipSR.Enabled = true;
                this.ddlZipCountry.Items.Clear();
                this.ddlZipCountry.DataBind();
                this.ddlZipCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlZipCountry.SelectedIndex = -1;
                this.ddlZipCountry.Enabled = false;
                this.ddlZipState.Items.Clear();
                this.ddlZipState.DataBind();
                this.ddlZipState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlZipState.SelectedIndex = -1;
                this.ddlZipState.Enabled = false;
                this.ddlZipCounty.Items.Clear();
                this.ddlZipCounty.DataBind();
                this.ddlZipCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlZipCounty.SelectedIndex = -1;
                this.ddlZipCounty.Enabled = false;
                this.gvwZipList.DataBind();
                this.gvwZipList.Visible = false;
                this.zipEnclosure.Visible = false;
                this.ddlZipSR.Focus();
                this.btnZipCancel.Enabled = true;
                this.btnZipCancel.CssClass = "btn btn-warning";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlZipSR_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlZipSR.SelectedValue);
                Dictionary<Int32, String> countryList = DMG.getCountryList(srID);

                this.ddlZipCountry.Items.Clear();
                this.ddlZipCountry.DataSource = countryList;
                this.ddlZipCountry.DataTextField = "Value";
                this.ddlZipCountry.DataValueField = "Key";
                this.ddlZipCountry.DataBind();
                this.ddlZipCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlZipCountry.SelectedIndex = -1;
                this.ddlZipCountry.Enabled = true;
                this.ddlZipState.Items.Clear();
                this.ddlZipState.DataBind();
                this.ddlZipState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlZipState.SelectedIndex = -1;
                this.ddlZipState.Enabled = false;
                this.ddlZipCounty.Items.Clear();
                this.ddlZipCounty.DataBind();
                this.ddlZipCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlZipCounty.SelectedIndex = -1;
                this.ddlZipCounty.Enabled = false;
                this.gvwZipList.DataBind();
                this.gvwZipList.Visible = false;
                this.zipEnclosure.Visible = false;
                this.ddlZipCountry.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlZipCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctID = Convert.ToInt32(this.ddlZipCountry.SelectedValue);
                Dictionary<int, string> stateList = DMG.getStateList(ctID);

                this.ddlZipState.Items.Clear();
                this.ddlZipState.DataSource = stateList;
                this.ddlZipState.DataTextField = "Value";
                this.ddlZipState.DataValueField = "Key";
                this.ddlZipState.DataBind();
                this.ddlZipState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlZipState.SelectedIndex = -1;
                this.ddlZipState.Enabled = true;
                this.ddlZipCounty.Items.Clear();
                this.ddlZipCounty.DataBind();
                this.ddlZipCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlZipCounty.SelectedIndex = -1;
                this.ddlZipCounty.Enabled = false;
                this.gvwZipList.DataBind();
                this.gvwZipList.Visible = false;
                this.zipEnclosure.Visible = false;
                this.ddlZipState.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlZipState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 sID = Convert.ToInt32(this.ddlZipState.SelectedValue);
                Dictionary<Int32, String> countyList = DMG.getCountyList(sID);

                this.ddlZipCounty.Items.Clear();
                this.ddlZipCounty.DataSource = countyList;
                this.ddlZipCounty.DataTextField = "Value";
                this.ddlZipCounty.DataValueField = "Key";
                this.ddlZipCounty.DataBind();
                this.ddlZipCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlZipCounty.SelectedIndex = -1;
                this.ddlZipCounty.Enabled = true;
                this.gvwZipList.DataBind();
                this.gvwZipList.Visible = false;
                this.zipEnclosure.Visible = false;
                this.ddlZipCounty.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlZipCounty_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cntyID = Convert.ToInt32(this.ddlZipCounty.SelectedValue);
                DataTable ZipTable = DMG.getZipTable(cntyID);
                this.gvwZipList.DataSource = ZipTable;
                this.gvwZipList.DataBind();
                this.gvwZipList.Visible = true;
                this.zipEnclosure.Visible = false;
                this.gvwZipList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnZipAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iReply = -99,  zID = -99;
                String zLat = String.Empty, zLon = String.Empty, zValue = String.Empty;
                Decimal zLatD = -99.00000000M, zLonD = -99.00000000M;
                Int32 zDat = -99, latPrecision = 9, latScale = 6, lonPrecision = 8, lonScale = 6;

                zID = Convert.ToInt32(this.ddlAZipCounty.SelectedValue);
                zLat = this.txtAZipLatitude.Text;
                zLon = this.txtAZipLongitude.Text;
                zValue = this.txtAZipValue.Text;
                zDat = Convert.ToInt32(this.ddlAZipDatum.SelectedValue);
                if (!String.IsNullOrEmpty(zLat) || !String.IsNullOrEmpty(zLon) || !String.IsNullOrEmpty(zValue))
                {
                    if (Decimal.TryParse(zLat, out zLatD) && Decimal.TryParse(zLon, out zLonD))
                    {
                        SqlDecimal latDecimal = new SqlDecimal(zLatD);
                        SqlDecimal lonDecimal = new SqlDecimal(zLonD);
                        latPrecision = (Int32)latDecimal.Precision;
                        latScale = (Int32)latDecimal.Scale;
                        lonPrecision = (Int32)lonDecimal.Precision;
                        lonScale = (Int32)lonDecimal.Scale;
                        if (latPrecision <= 9 && latScale.Equals(6) && lonPrecision <= 9 && lonScale.Equals(6))
                        {
                            if (zLatD > 90.000000M || zLatD < -90.000000M || zLonD > 180.000000M || zLonD < -180.000000M)
                            {
                                this.zipEnclosure.Visible = true;
                                this.zipEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iZip.Attributes["class"] = "icon fa fa-warning";
                                this.spnZip.InnerText = " Warning!";
                                this.lblZipSuccess.Text = "Invalid Latitude and Longitude values. Please provide valid values upto 6-Decimal places.";

                                this.txtAZipLatitude.Text = null;
                                this.txtAZipLongitude.Text = null;
                                this.txtAZipValue.Text = null;

                                this.txtAZipLatitude.Focus();
                            }
                            else
                            {
                                iReply = DMG.addZip(zValue, zID, zDat, zLat, zLon, LoginName, domain);
                                if (iReply.Equals(1))
                                {
                                    this.lblZipSuccess.Text = "Successfully inserted Zip/Postal code!";
                                    this.zipEnclosure.Visible = true;
                                    this.zipEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                                    this.iZip.Attributes["class"] = "icon fa fa-check-circle";
                                    this.spnZip.InnerText = " Success!";
                                }
                                else
                                {
                                    this.lblZipSuccess.Text = "Unknown Error. Please try inserting Zip/Postal code again.";
                                    this.zipEnclosure.Visible = true;
                                    this.zipEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                    this.iZip.Attributes["class"] = "icon fa fa-warning";
                                    this.spnZip.InnerText = " Warning!";
                                }
                            }
                        }
                        else
                        {
                            this.lblZipSuccess.Text = "Invalid Latitude and Longitude values. Please provide valid values upto 6-Decimal places.";
                            this.zipEnclosure.Visible = true;
                            this.zipEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                            this.iZip.Attributes["class"] = "icon fa fa-warning";
                            this.spnZip.InnerText = " Warning!";
                            this.txtAZipLatitude.Text = null;
                            this.txtAZipLongitude.Text = null;
                            this.txtAZipValue.Text = null;

                            this.txtAZipLatitude.Focus();
                        }
                    }
                    else
                    {
                        this.lblZipSuccess.Text = "Invalid Latitude and Longitude values. Please provide valid values upto 6-Decimal places.";
                        this.zipEnclosure.Visible = true;
                        this.zipEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iZip.Attributes["class"] = "icon fa fa-warning";
                        this.spnZip.InnerText = " Warning!";
                        this.txtAZipLatitude.Text = null;
                        this.txtAZipLongitude.Text = null;
                        this.txtAZipValue.Text = null;

                        this.txtAZipLatitude.Focus();
                    }
                }
                else
                {
                    this.lblZipSuccess.Text = "Unable to insert Null values for Latitude, Longitude or Zip/Postal Code. Please provide valid values.";
                    this.zipEnclosure.Visible = true;
                    this.zipEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iZip.Attributes["class"] = "icon fa fa-warning";
                    this.spnZip.InnerText = " Warning!";
                    this.txtAZipLatitude.Text = null;
                    this.txtAZipLongitude.Text = null;
                    this.txtAZipValue.Text = null;

                    this.txtAZipLatitude.Focus();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnZipDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnZipAddCancel_Click(null, null);
                this.btnZipCancel_Click(null, null);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAZipReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlAZipReg.SelectedValue);
                Dictionary<Int32, String> subregList = DMG.getSubRegionList(srID);

                this.ddlAZipSReg.Items.Clear();
                this.ddlAZipSReg.DataSource = subregList;
                this.ddlAZipSReg.DataTextField = "Value";
                this.ddlAZipSReg.DataValueField = "Key";
                this.ddlAZipSReg.DataBind();
                this.ddlAZipSReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlAZipSReg.SelectedIndex = -1;
                this.ddlAZipSReg.Enabled = true;
                this.ddlAZipCountry.Items.Clear();
                this.ddlAZipCountry.DataBind();
                this.ddlAZipCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlAZipCountry.SelectedIndex = -1;
                this.ddlAZipCountry.Enabled = false;
                this.ddlAZipState.Items.Clear();
                this.ddlAZipState.DataBind();
                this.ddlAZipState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlAZipState.SelectedIndex = -1;
                this.ddlAZipState.Enabled = false;
                this.ddlAZipCounty.Items.Clear();
                this.ddlAZipCounty.DataBind();
                this.ddlAZipCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlAZipCounty.SelectedIndex = -1;
                this.ddlAZipCounty.Enabled = false;
                this.ddlAZipDatum.Items.Clear();
                this.ddlAZipDatum.DataBind();
                this.ddlAZipDatum.Items.Insert(0, new ListItem("--- Select Datum ---", "-1"));
                this.ddlAZipDatum.SelectedIndex = -1;
                this.ddlAZipDatum.Enabled = false;
                this.txtAZipLatitude.Text = null;
                this.txtAZipLatitude.Enabled = false;
                this.txtAZipLongitude.Text = null;
                this.txtAZipLongitude.Enabled = false;
                this.txtAZipValue.Text = null;
                this.txtAZipValue.Enabled = false;
                this.zipEnclosure.Visible = false;
                this.ddlAZipSReg.Focus();
                this.btnZipAddCancel.Enabled = true;
                this.btnZipAddCancel.CssClass = "btn btn-warning";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAZipSReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlAZipSReg.SelectedValue);
                Dictionary<Int32, String> countryList = DMG.getCountryList(srID);

                this.ddlAZipCountry.Items.Clear();
                this.ddlAZipCountry.DataSource = countryList;
                this.ddlAZipCountry.DataTextField = "Value";
                this.ddlAZipCountry.DataValueField = "Key";
                this.ddlAZipCountry.DataBind();
                this.ddlAZipCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlAZipCountry.SelectedIndex = -1;
                this.ddlAZipCountry.Enabled = true;
                this.ddlAZipState.Items.Clear();
                this.ddlAZipState.DataBind();
                this.ddlAZipState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlAZipState.SelectedIndex = -1;
                this.ddlAZipState.Enabled = false;
                this.ddlAZipCounty.Items.Clear();
                this.ddlAZipCounty.DataBind();
                this.ddlAZipCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlAZipCounty.SelectedIndex = -1;
                this.ddlAZipCounty.Enabled = false;
                this.ddlAZipDatum.Items.Clear();
                this.ddlAZipDatum.DataBind();
                this.ddlAZipDatum.Items.Insert(0, new ListItem("--- Select Datum ---", "-1"));
                this.ddlAZipDatum.SelectedIndex = -1;
                this.ddlAZipDatum.Enabled = false;
                this.txtAZipLatitude.Text = null;
                this.txtAZipLatitude.Enabled = false;
                this.txtAZipLongitude.Text = null;
                this.txtAZipLongitude.Enabled = false;
                this.txtAZipValue.Text = null;
                this.txtAZipValue.Enabled = false;
                this.zipEnclosure.Visible = false;
                this.ddlAZipCountry.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAZipCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctID = Convert.ToInt32(this.ddlAZipCountry.SelectedValue);
                Dictionary<int, string> stateList = DMG.getStateList(ctID);

                this.ddlAZipState.Items.Clear();
                this.ddlAZipState.DataSource = stateList;
                this.ddlAZipState.DataTextField = "Value";
                this.ddlAZipState.DataValueField = "Key";
                this.ddlAZipState.DataBind();
                this.ddlAZipState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlAZipState.SelectedIndex = -1;
                this.ddlAZipState.Enabled = true;
                this.ddlAZipCounty.Items.Clear();
                this.ddlAZipCounty.DataBind();
                this.ddlAZipCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlAZipCounty.SelectedIndex = -1;
                this.ddlAZipCounty.Enabled = false;
                this.ddlAZipDatum.Items.Clear();
                this.ddlAZipDatum.DataBind();
                this.ddlAZipDatum.Items.Insert(0, new ListItem("--- Select Datum ---", "-1"));
                this.ddlAZipDatum.SelectedIndex = -1;
                this.ddlAZipDatum.Enabled = false;
                this.txtAZipLatitude.Text = null;
                this.txtAZipLatitude.Enabled = false;
                this.txtAZipLongitude.Text = null;
                this.txtAZipLongitude.Enabled = false;
                this.txtAZipValue.Text = null;
                this.txtAZipValue.Enabled = false;
                this.zipEnclosure.Visible = false;
                this.ddlAZipState.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAZipState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 sID = Convert.ToInt32(this.ddlAZipState.SelectedValue);
                Dictionary<Int32, String> countyList = DMG.getCountyList(sID);

                this.ddlAZipCounty.Items.Clear();
                this.ddlAZipCounty.DataSource = countyList;
                this.ddlAZipCounty.DataTextField = "Value";
                this.ddlAZipCounty.DataValueField = "Key";
                this.ddlAZipCounty.DataBind();
                this.ddlAZipCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlAZipCounty.SelectedIndex = -1;
                this.ddlAZipCounty.Enabled = true;
                this.ddlAZipDatum.Items.Clear();
                this.ddlAZipDatum.DataBind();
                this.ddlAZipDatum.Items.Insert(0, new ListItem("--- Select Datum ---", "-1"));
                this.ddlAZipDatum.SelectedIndex = -1;
                this.ddlAZipDatum.Enabled = false;
                this.txtAZipLatitude.Text = null;
                this.txtAZipLatitude.Enabled = false;
                this.txtAZipLongitude.Text = null;
                this.txtAZipLongitude.Enabled = false;
                this.txtAZipValue.Text = null;
                this.txtAZipValue.Enabled = false;
                this.zipEnclosure.Visible = false;
                this.ddlAZipCounty.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAZipCounty_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> datumList = AST.getCordSysList();                
                this.ddlAZipDatum.Items.Clear();
                this.ddlAZipDatum.DataSource = datumList;
                this.ddlAZipDatum.DataTextField = "Value";
                this.ddlAZipDatum.DataValueField = "Key";
                this.ddlAZipDatum.DataBind();
                this.ddlAZipDatum.SelectedIndex = ddlAZipDatum.Items.IndexOf(ddlAZipDatum.Items.FindByText("WGS84"));
                this.ddlAZipDatum.Enabled = false;
                this.txtAZipLatitude.Text = null;
                this.txtAZipLatitude.Enabled = true;
                this.txtAZipLongitude.Text = null;
                this.txtAZipLongitude.Enabled = true;
                this.txtAZipValue.Text = null;
                this.txtAZipValue.Enabled = true;
                this.zipEnclosure.Visible = false;
                this.btnZipAddNew.Enabled = true;
                this.txtAZipLatitude.Focus();
                this.btnZipAddNew.Enabled = true;
                this.btnZipAddNew.CssClass = "btn btn-success";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAZipDatum_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnZipAddNew.Enabled = true;
                this.btnZipAddNew.CssClass = "btn btn-success";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Zip Routine ended
        //Field Routines Started

        protected void gvwFldList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 cntyID = Convert.ToInt32(this.ddlFldCounty.SelectedValue);
                DataTable FieldTable = DMG.getFieldTable(cntyID);
                this.gvwFldList.PageIndex = e.NewPageIndex;
                this.gvwFldList.DataSource = FieldTable;
                this.gvwFldList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlFldReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 rID = Convert.ToInt32(this.ddlFldReg.SelectedValue);
                Dictionary<Int32, String> subregList = DMG.getSubRegionList(rID);

                this.ddlFldSReg.Items.Clear();
                this.ddlFldSReg.DataSource = subregList;
                this.ddlFldSReg.DataTextField = "Value";
                this.ddlFldSReg.DataValueField = "Key";
                this.ddlFldSReg.DataBind();
                this.ddlFldSReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlFldSReg.SelectedIndex = -1;
                this.ddlFldSReg.Enabled = true;
                this.ddlFldCountry.Items.Clear();
                this.ddlFldCountry.DataBind();
                this.ddlFldCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlFldCountry.SelectedIndex = -1;
                this.ddlFldCountry.Enabled = false;
                this.ddlFldState.Items.Clear();
                this.ddlFldState.DataBind();
                this.ddlFldState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlFldState.SelectedIndex = -1;
                this.ddlFldState.Enabled = false;
                this.ddlFldCounty.Items.Clear();
                this.ddlFldCounty.DataBind();
                this.ddlFldCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlFldCounty.SelectedIndex = -1;
                this.ddlFldCounty.Enabled = false;
                this.gvwFldList.DataBind();
                this.gvwFldList.Visible = false;
                this.btnFieldCancel.Visible = true;
                this.ddlFldSReg.Focus();
                this.btnFieldCancel.Enabled = true;
                this.btnFieldCancel.CssClass = "btn btn-warning";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlFldSReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlFldSReg.SelectedValue);
                Dictionary<Int32, String> countryList = DMG.getCountryList(srID);
                this.ddlFldCountry.Items.Clear();
                this.ddlFldCountry.DataSource = countryList;
                this.ddlFldCountry.DataTextField = "Value";
                this.ddlFldCountry.DataValueField = "Key";
                this.ddlFldCountry.DataBind();
                this.ddlFldCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlFldCountry.SelectedIndex = -1;
                this.ddlFldCountry.Enabled = true;
                this.ddlFldState.Items.Clear();
                this.ddlFldState.DataBind();
                this.ddlFldState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlFldState.SelectedIndex = -1;
                this.ddlFldState.Enabled = false;
                this.ddlFldCounty.Items.Clear();
                this.ddlFldCounty.DataBind();
                this.ddlFldCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlFldCounty.SelectedIndex = -1;
                this.ddlFldCounty.Enabled = false;
                this.gvwFldList.DataBind();
                this.gvwFldList.Visible = false;

                this.ddlFldCountry.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlFldCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctID = Convert.ToInt32(this.ddlFldCountry.SelectedValue);
                Dictionary<Int32, String> stateList = DMG.getStateList(ctID);

                this.ddlFldState.Items.Clear();
                this.ddlFldState.DataSource = stateList;
                this.ddlFldState.DataTextField = "Value";
                this.ddlFldState.DataValueField = "Key";
                this.ddlFldState.DataBind();
                this.ddlFldState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlFldState.SelectedIndex = -1;
                this.ddlFldState.Enabled = true;
                this.ddlFldCounty.Items.Clear();
                this.ddlFldCounty.DataBind();
                this.ddlFldCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlFldCounty.SelectedIndex = -1;
                this.ddlFldCounty.Enabled = false;
                this.gvwFldList.DataBind();
                this.gvwFldList.Visible = false;

                this.ddlFldState.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlFldState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 stateID = Convert.ToInt32(this.ddlFldState.SelectedValue);
                Dictionary<Int32, String> countyList = DMG.getCountyFieldsList(stateID);

                this.ddlFldCounty.Items.Clear();
                this.ddlFldCounty.DataSource = countyList;
                this.ddlFldCounty.DataTextField = "Value";
                this.ddlFldCounty.DataValueField = "Key";
                this.ddlFldCounty.DataBind();
                this.ddlFldCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlFldCounty.SelectedIndex = -1;
                this.ddlFldCounty.Enabled = true;
                this.gvwFldList.DataBind();
                this.gvwFldList.Visible = false;

                this.ddlFldCounty.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlFldCounty_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cntyID = Convert.ToInt32(this.ddlFldCounty.SelectedValue);
                DataTable FieldTable = DMG.getFieldTable(cntyID);
                this.gvwFldList.DataSource = FieldTable;
                this.gvwFldList.DataBind();
                this.gvwFldList.SelectedIndex = -1;
                this.gvwFldList.Visible = true;

                this.gvwFldList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwFldList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {}
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnFieldAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnFieldCancel_Click(null, null);
                this.fldEnclosure.Visible = false;
                this.divFLDList.Attributes["class"] = "panel-collapse collapse";
                this.divFLDAdd.Attributes["class"] = "panel-collapse collapse in";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnFieldCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> regList = DMG.getRegionList();
                this.ddlFldReg.Items.Clear();
                this.ddlFldReg.DataSource = regList;
                this.ddlFldReg.DataTextField = "Value";
                this.ddlFldReg.DataValueField = "Key";
                this.ddlFldReg.DataBind();
                this.ddlFldReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlFldReg.SelectedIndex = -1;
                this.ddlFldSReg.Items.Clear();
                this.ddlFldSReg.DataBind();
                this.ddlFldSReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlFldSReg.SelectedIndex = -1;
                this.ddlFldSReg.Enabled = false;
                this.ddlFldCountry.Items.Clear();
                this.ddlFldCountry.DataBind();
                this.ddlFldCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlFldCountry.SelectedIndex = -1;
                this.ddlFldCountry.Enabled = false;
                this.ddlFldState.Items.Clear();
                this.ddlFldState.DataBind();
                this.ddlFldState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlFldState.SelectedIndex = -1;
                this.ddlFldState.Enabled = false;
                this.ddlFldCounty.Items.Clear();
                this.ddlFldCounty.DataBind();
                this.ddlFldCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlFldCounty.SelectedIndex = -1;
                this.ddlFldCounty.Enabled = false;
                this.gvwFldList.DataBind();
                this.gvwFldList.Visible = false;
                this.gvwFldList.SelectedIndex = -1;
                this.ddlFldReg.Focus();
                this.btnFieldCancel.Enabled = false;
                this.btnFieldCancel.CssClass = "btn btn-warning";
                this.divFLDList.Attributes["class"] = "panel-collapse collapse in";
                this.divFLDAdd.Attributes["class"] = "panel-collapse collapse";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlFAddReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlFAddReg.SelectedValue);
                Dictionary<Int32, String> subregList = DMG.getSubRegionList(srID);

                this.ddlFAddSReg.Items.Clear();
                this.ddlFAddSReg.DataSource = subregList;
                this.ddlFAddSReg.DataTextField = "Value";
                this.ddlFAddSReg.DataValueField = "Key";
                this.ddlFAddSReg.DataBind();
                this.ddlFAddSReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlFAddSReg.SelectedIndex = -1;
                this.ddlFAddSReg.Enabled = true;
                this.ddlFAddCountry.Items.Clear();
                this.ddlFAddCountry.DataBind();
                this.ddlFAddCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlFAddCountry.SelectedIndex = -1;
                this.ddlFAddCountry.Enabled = false;
                this.ddlFAddState.Items.Clear();
                this.ddlFAddState.DataBind();
                this.ddlFAddState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlFAddState.SelectedIndex = -1;
                this.ddlFAddState.Enabled = false;
                this.ddlFAddCounty.Items.Clear();
                this.ddlFAddCounty.DataBind();
                this.ddlFAddCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlFAddCounty.SelectedIndex = -1;
                this.ddlFAddCounty.Enabled = false;
                this.ddlFAddOil.Items.Clear();
                this.ddlFAddOil.DataBind();
                this.ddlFAddOil.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddOil.SelectedIndex = -1;
                this.ddlFAddOil.Enabled = false;
                this.ddlFAddGas.Items.Clear();
                this.ddlFAddGas.DataBind();
                this.ddlFAddGas.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddGas.SelectedIndex = -1;
                this.ddlFAddGas.Enabled = false;
                this.ddlFAddAssoc.Items.Clear();
                this.ddlFAddAssoc.DataBind();
                this.ddlFAddAssoc.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddAssoc.SelectedIndex = -1;
                this.ddlFAddAssoc.Enabled = false;
                this.ddlFAddUnknown.Items.Clear();
                this.ddlFAddUnknown.DataBind();
                this.ddlFAddUnknown.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddUnknown.SelectedIndex = -1;
                this.ddlFAddUnknown.Enabled = false;
                this.txtFAddName.Text = null;
                this.txtFAddName.Enabled = false;
                this.txtFAddCode.Text = null;
                this.txtFAddCode.Enabled = false;
                this.txtFAddYear.Text = null;
                this.txtFAddYear.Enabled = false;
                this.fldEnclosure.Visible = false;
                this.btnFieldAddCancel.Enabled = true;
                this.btnFieldAddCancel.CssClass = "btn btn-warning";
                this.ddlFAddSReg.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlFAddSReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlFAddSReg.SelectedValue);
                Dictionary<int, string> countryList = DMG.getCountryList(srID);

                this.ddlFAddCountry.Items.Clear();
                this.ddlFAddCountry.DataSource = countryList;
                this.ddlFAddCountry.DataTextField = "Value";
                this.ddlFAddCountry.DataValueField = "Key";
                this.ddlFAddCountry.DataBind();
                this.ddlFAddCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlFAddCountry.SelectedIndex = -1;
                this.ddlFAddCountry.Enabled = true;
                this.ddlFAddState.Items.Clear();
                this.ddlFAddState.DataBind();
                this.ddlFAddState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlFAddState.SelectedIndex = -1;
                this.ddlFAddState.Enabled = false;
                this.ddlFAddCounty.Items.Clear();
                this.ddlFAddCounty.DataBind();
                this.ddlFAddCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlFAddCounty.SelectedIndex = -1;
                this.ddlFAddCounty.Enabled = false;
                this.ddlFAddOil.Items.Clear();
                this.ddlFAddOil.DataBind();
                this.ddlFAddOil.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddOil.SelectedIndex = -1;
                this.ddlFAddOil.Enabled = false;
                this.ddlFAddGas.Items.Clear();
                this.ddlFAddGas.DataBind();
                this.ddlFAddGas.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddGas.SelectedIndex = -1;
                this.ddlFAddGas.Enabled = false;
                this.ddlFAddAssoc.Items.Clear();
                this.ddlFAddAssoc.DataBind();
                this.ddlFAddAssoc.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddAssoc.SelectedIndex = -1;
                this.ddlFAddAssoc.Enabled = false;
                this.ddlFAddUnknown.Items.Clear();
                this.ddlFAddUnknown.DataBind();
                this.ddlFAddUnknown.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddUnknown.SelectedIndex = -1;
                this.ddlFAddUnknown.Enabled = false;
                this.txtFAddName.Text = null;
                this.txtFAddName.Enabled = false;
                this.txtFAddCode.Text = null;
                this.txtFAddCode.Enabled = false;
                this.txtFAddYear.Text = null;
                this.txtFAddYear.Enabled = false;
                this.fldEnclosure.Visible = false;
                this.btnFAddNew.Enabled = false;

                this.ddlFAddCountry.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlFAddCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctID = Convert.ToInt32(this.ddlFAddCountry.SelectedValue);
                Dictionary<Int32, String> stateList = DMG.getStateList(ctID);

                this.ddlFAddState.Items.Clear();
                this.ddlFAddState.DataSource = stateList;
                this.ddlFAddState.DataTextField = "Value";
                this.ddlFAddState.DataValueField = "Key";
                this.ddlFAddState.DataBind();
                this.ddlFAddState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlFAddState.SelectedIndex = -1;
                this.ddlFAddState.Enabled = true;
                this.ddlFAddCounty.Items.Clear();
                this.ddlFAddCounty.DataBind();
                this.ddlFAddCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlFAddCounty.SelectedIndex = -1;
                this.ddlFAddCounty.Enabled = false;
                this.ddlFAddOil.Items.Clear();
                this.ddlFAddOil.DataBind();
                this.ddlFAddOil.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddOil.SelectedIndex = -1;
                this.ddlFAddOil.Enabled = false;
                this.ddlFAddGas.Items.Clear();
                this.ddlFAddGas.DataBind();
                this.ddlFAddGas.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddGas.SelectedIndex = -1;
                this.ddlFAddGas.Enabled = false;
                this.ddlFAddAssoc.Items.Clear();
                this.ddlFAddAssoc.DataBind();
                this.ddlFAddAssoc.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddAssoc.SelectedIndex = -1;
                this.ddlFAddAssoc.Enabled = false;
                this.ddlFAddUnknown.Items.Clear();
                this.ddlFAddUnknown.DataBind();
                this.ddlFAddUnknown.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddUnknown.SelectedIndex = -1;
                this.ddlFAddUnknown.Enabled = false;
                this.txtFAddName.Text = null;
                this.txtFAddName.Enabled = false;
                this.txtFAddCode.Text = null;
                this.txtFAddCode.Enabled = false;
                this.txtFAddYear.Text = null;
                this.txtFAddYear.Enabled = false;
                this.fldEnclosure.Visible = false;
                this.btnFAddNew.Enabled = false;

                this.ddlFAddState.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlFAddState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 sID = Convert.ToInt32(this.ddlFAddState.SelectedValue);
                Dictionary<Int32, String> countyList = DMG.getCountyList(sID);

                this.ddlFAddCounty.Items.Clear();
                this.ddlFAddCounty.DataSource = countyList;
                this.ddlFAddCounty.DataTextField = "Value";
                this.ddlFAddCounty.DataValueField = "Key";
                this.ddlFAddCounty.DataBind();
                this.ddlFAddCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlFAddCounty.SelectedIndex = -1;
                this.ddlFAddCounty.Enabled = true;
                this.ddlFAddOil.Items.Clear();
                this.ddlFAddOil.DataBind();
                this.ddlFAddOil.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddOil.SelectedIndex = -1;
                this.ddlFAddOil.Enabled = false;
                this.ddlFAddGas.Items.Clear();
                this.ddlFAddGas.DataBind();
                this.ddlFAddGas.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddGas.SelectedIndex = -1;
                this.ddlFAddGas.Enabled = false;
                this.ddlFAddAssoc.Items.Clear();
                this.ddlFAddAssoc.DataBind();
                this.ddlFAddAssoc.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddAssoc.SelectedIndex = -1;
                this.ddlFAddAssoc.Enabled = false;
                this.ddlFAddUnknown.Items.Clear();
                this.ddlFAddUnknown.DataBind();
                this.ddlFAddUnknown.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddUnknown.SelectedIndex = -1;
                this.ddlFAddUnknown.Enabled = false;
                this.txtFAddName.Text = null;
                this.txtFAddName.Enabled = false;
                this.txtFAddCode.Text = null;
                this.txtFAddCode.Enabled = false;
                this.txtFAddYear.Text = null;
                this.txtFAddYear.Enabled = false;
                this.fldEnclosure.Visible = false;
                this.btnFAddNew.Enabled = false;

                this.ddlFAddCounty.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlFAddCounty_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.ddlFAddOil.Items.Clear();
                this.ddlFAddOil.DataBind();
                this.ddlFAddOil.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddOil.SelectedIndex = -1;
                this.ddlFAddOil.Enabled = true;
                this.ddlFAddGas.Items.Clear();
                this.ddlFAddGas.DataBind();
                this.ddlFAddGas.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddGas.SelectedIndex = -1;
                this.ddlFAddGas.Enabled = false;
                this.ddlFAddAssoc.Items.Clear();
                this.ddlFAddAssoc.DataBind();
                this.ddlFAddAssoc.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddAssoc.SelectedIndex = -1;
                this.ddlFAddAssoc.Enabled = false;
                this.ddlFAddUnknown.Items.Clear();
                this.ddlFAddUnknown.DataBind();
                this.ddlFAddUnknown.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddUnknown.SelectedIndex = -1;
                this.ddlFAddUnknown.Enabled = false;
                this.txtFAddName.Text = null;
                this.txtFAddName.Enabled = false;
                this.txtFAddCode.Text = null;
                this.txtFAddCode.Enabled = false;
                this.txtFAddYear.Text = null;
                this.txtFAddYear.Enabled = false;
                this.fldEnclosure.Visible = false;
                this.btnFAddNew.Enabled = false;

                this.ddlFAddOil.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlFAddOil_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.ddlFAddGas.Items.Clear();
                this.ddlFAddGas.DataBind();
                this.ddlFAddGas.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddGas.SelectedIndex = -1;
                this.ddlFAddGas.Enabled = true;
                this.ddlFAddAssoc.Items.Clear();
                this.ddlFAddAssoc.DataBind();
                this.ddlFAddAssoc.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddAssoc.SelectedIndex = -1;
                this.ddlFAddAssoc.Enabled = false;
                this.ddlFAddUnknown.Items.Clear();
                this.ddlFAddUnknown.DataBind();
                this.ddlFAddUnknown.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddUnknown.SelectedIndex = -1;
                this.ddlFAddUnknown.Enabled = false;
                this.txtFAddName.Text = null;
                this.txtFAddName.Enabled = false;
                this.txtFAddCode.Text = null;
                this.txtFAddCode.Enabled = false;
                this.txtFAddYear.Text = null;
                this.txtFAddYear.Enabled = false;
                this.fldEnclosure.Visible = false;
                this.btnFAddNew.Enabled = false;

                this.ddlFAddGas.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlFAddGas_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.ddlFAddAssoc.Items.Clear();
                this.ddlFAddAssoc.DataBind();
                this.ddlFAddAssoc.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddAssoc.SelectedIndex = -1;
                this.ddlFAddAssoc.Enabled = true;
                this.ddlFAddUnknown.Items.Clear();
                this.ddlFAddUnknown.DataBind();
                this.ddlFAddUnknown.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddUnknown.SelectedIndex = -1;
                this.ddlFAddUnknown.Enabled = false;
                this.txtFAddName.Text = null;
                this.txtFAddName.Enabled = false;
                this.txtFAddCode.Text = null;
                this.txtFAddCode.Enabled = false;
                this.txtFAddYear.Text = null;
                this.txtFAddYear.Enabled = false;
                this.fldEnclosure.Visible = false;
                this.btnFAddNew.Enabled = false;

                this.ddlFAddAssoc.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlFAddAssoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.ddlFAddUnknown.Items.Clear();
                this.ddlFAddUnknown.DataBind();
                this.ddlFAddUnknown.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddUnknown.SelectedIndex = -1;
                this.ddlFAddUnknown.Enabled = true;
                this.txtFAddName.Text = null;
                this.txtFAddName.Enabled = false;
                this.txtFAddCode.Text = null;
                this.txtFAddCode.Enabled = false;
                this.txtFAddYear.Text = null;
                this.txtFAddYear.Enabled = false;
                this.fldEnclosure.Visible = false;
                this.btnFAddNew.Enabled = false;

                this.ddlFAddUnknown.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlFAddUnknown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtFAddName.Text = null;
                this.txtFAddName.Enabled = true;
                this.txtFAddCode.Text = null;
                this.txtFAddCode.Enabled = true;
                this.txtFAddYear.Text = null;
                this.txtFAddYear.Enabled = true;
                this.fldEnclosure.Visible = false;
                this.btnFAddNew.Enabled = true;
                this.btnFAddNew.CssClass = "btn btn-success";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnFAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iReply = -99;
                Int32 stID = -99;
                Int32 cnyID = -99;
                String fName = null;
                String fCode = null;
                Int32 fOil = -99;
                Int32 fGas = -99;
                Int32 fAsc = -99;
                Int32 fUnk = -99;
                String Year = null;

                stID = Convert.ToInt32(this.ddlFAddState.SelectedValue);
                cnyID = Convert.ToInt32(this.ddlFAddCounty.SelectedValue);
                fName = this.txtFAddName.Text;
                fCode = this.txtFAddCode.Text;
                Year = this.txtFAddYear.Text;
                fOil = Convert.ToInt32(this.ddlFAddOil.SelectedValue);
                fGas = Convert.ToInt32(this.ddlFAddGas.SelectedValue);
                fAsc = Convert.ToInt32(this.ddlFAddAssoc.SelectedValue);
                fUnk = Convert.ToInt32(this.ddlFAddUnknown.SelectedValue);

                if (String.IsNullOrEmpty(fName) || String.IsNullOrEmpty(fCode) || String.IsNullOrEmpty(Year))
                {
                    this.lblFieldSuccess.Text = " Unable to insert Null values. Please provide all valid values.";
                    this.fldEnclosure.Visible = true;
                    this.fldEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iFLD.Attributes["class"] = "icon fa fa-warning";
                    this.spnFLD.InnerText = " Warning!";
                }
                else
                {
                    iReply = DMG.addField(stID, cnyID, fName, fCode, Year, fOil, fGas, fAsc, fUnk, LoginName, domain);
                    if (iReply.Equals(1))
                    {
                        this.lblFieldSuccess.Text = " Successfully inserted Field!";
                        this.fldEnclosure.Visible = true;
                        this.fldEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iFLD.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnFLD.InnerText = " Success!";
                    }
                    else
                    {
                        this.lblFieldSuccess.Text = " Unknown error. Please try again.";
                        this.fldEnclosure.Visible = true;
                        this.fldEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iFLD.Attributes["class"] = "icon fa fa-warning";
                        this.spnFLD.InnerText = " Warning!";
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnFieldAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> regList = DMG.getRegionList();
                this.ddlFAddReg.Items.Clear();
                this.ddlFAddReg.DataSource = regList;
                this.ddlFAddReg.DataTextField = "Value";
                this.ddlFAddReg.DataValueField = "Key";
                this.ddlFAddReg.DataBind();
                this.ddlFAddReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlFAddReg.SelectedIndex = -1;
                this.ddlFAddSReg.Items.Clear();
                this.ddlFAddSReg.DataBind();
                this.ddlFAddSReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlFAddSReg.SelectedIndex = -1;
                this.ddlFAddSReg.Enabled = false;
                this.ddlFAddCountry.Items.Clear();
                this.ddlFAddCountry.DataBind();
                this.ddlFAddCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlFAddCountry.SelectedIndex = -1;
                this.ddlFAddCountry.Enabled = false;
                this.ddlFAddState.Items.Clear();
                this.ddlFAddState.DataBind();
                this.ddlFAddState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlFAddState.SelectedIndex = -1;
                this.ddlFAddState.Enabled = false;
                this.ddlFAddCounty.Items.Clear();
                this.ddlFAddCounty.DataBind();
                this.ddlFAddCounty.Items.Insert(0, new ListItem("--- Select County/Municipality/Division/District ---", "-1"));
                this.ddlFAddCounty.SelectedIndex = -1;
                this.ddlFAddCounty.Enabled = false;
                this.ddlFAddOil.Items.Clear();
                this.ddlFAddOil.DataBind();
                this.ddlFAddOil.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddOil.SelectedIndex = -1;
                this.ddlFAddOil.Enabled = false;
                this.ddlFAddGas.Items.Clear();
                this.ddlFAddGas.DataBind();
                this.ddlFAddGas.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddGas.SelectedIndex = -1;
                this.ddlFAddGas.Enabled = false;
                this.ddlFAddAssoc.Items.Clear();
                this.ddlFAddAssoc.DataBind();
                this.ddlFAddAssoc.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddAssoc.SelectedIndex = -1;
                this.ddlFAddAssoc.Enabled = false;
                this.ddlFAddUnknown.Items.Clear();
                this.ddlFAddUnknown.DataBind();
                this.ddlFAddUnknown.Items.Insert(0, new ListItem("--- Select Field Hydrocarbon Option ---", "-1"));
                this.ddlFAddUnknown.SelectedIndex = -1;
                this.ddlFAddUnknown.Enabled = false;
                this.txtFAddName.Text = null;
                this.txtFAddName.Enabled = false;
                this.txtFAddCode.Text = null;
                this.txtFAddCode.Enabled = false;
                this.txtFAddYear.Text = null;
                this.txtFAddYear.Enabled = false;
                this.fldEnclosure.Visible = false;
                this.btnFAddNew.Enabled = false;
                this.btnFAddNew.CssClass = "btn btn-default";
                this.btnFieldAddCancel.Enabled = false;
                this.btnFieldAddCancel.CssClass = "btn btn-default";
                this.ddlFAddReg.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnFDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnFieldAddCancel_Click(null, null);
                this.btnFieldCancel_Click(null, null);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        
    }
}