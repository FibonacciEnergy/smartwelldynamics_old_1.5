﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlGeography_2A.ascx.cs" Inherits="SmartsVer1.Control.Config.Geography.ctrlGeography_2A" %>
<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" ></asp:ScriptManager>
<!--  Stylesheet -->
<link href="../../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript -->
<script src="../../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>

<!--- Page local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }
    .rotate{
		-webkit-transform: rotate(90deg);  /* Chrome, Safari, Opera */
			-moz-transform: rotate(90deg);  /* Firefox */
			-ms-transform: rotate(90deg);  /* IE 9 */
				transform: rotate(90deg);  /* Standard syntax */    
    }
    /*custom anchor defi for menu items*/
    .swdMenu {
        color: #ffffff;
    }
</style>
<!--- Page local script --->
<script type="text/javascript">
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("rotate");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").addClass("rotate");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("rotate");
        });
    });
</script>
<!--- Page Menu -->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">        
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>Configuration</li>
                <li><a href="../../../Config/Geography/fecDRGeography_2A.aspx"> Geography</a></li>
            </ol>
        </div>
    </div>    
</div>

<!-- Page Content -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div id="acrdGeoMain" class="panel-group" runat="server">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancMRegOuter" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlGeography_2A_acrdGeoMain" href="#BodyContent_ctrlGeography_2A_divMGOuter">
                                <span class="glyphicon glyphicon-menu-right"></span> Major Geographic Regions</a>
                        </h5>
                    </div>
                    <div id="divMGOuter" runat="server" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="uprgsMajR" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlMajR">
                                <ProgressTemplate>
                                    <div id="divMajRProcessing" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                        z-index: 100;">
                                        <asp:Image ID="imgMajRProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel runat="server" ID="upnlMajR">
                                <ContentTemplate>
                                    <div class="panel panel-danger panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="acrdMReg" runat="server" class="panel-group">
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:LinkButton ID="btnMRAdd" runat="server" CssClass="btn btn-info text-center" OnClick="btnMRAdd_Click"><i
                                                                    class="fa fa-plus"> Maj. Geographic Region</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnMRCancel" runat="server" CssClass="btn btn-default text-center" Enabled="false"
                                                        OnClick="btnMRCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                </div>
                                                <div class="panel panel-info">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancWSList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlGeography_2A_acrdMReg" href="#BodyContent_ctrlGeography_2A_divGRList">
                                                                Major Geographic Regions</a>
                                                        </h5>
                                                    </div>
                                                    <div id="divGRList" runat="server" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <asp:GridView ID="gvwMRList" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                            AllowPaging="True" DataKeyNames="grID" OnPageIndexChanging="gvwMRList_PageIndexChanging"
                                                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header"
                                                            PagerStyle-CssClass="pager" RowStyle-CssClass="rows">
                                                            <Columns>
                                                                <asp:BoundField DataField="grID" HeaderText="grID" ReadOnly="True" Visible="False" />
                                                                <asp:BoundField DataField="grName" HeaderText="Geographic Region Name" />
                                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" ReadOnly="True" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                            </Columns>
                                                        </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-success">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancWSAdd" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlGeography_2A_acrdWSec" href="#BodyContent_ctrlGeography_2A_divGRAdd">
                                                                Add Major Geographic Region</a>
                                                        </h5>
                                                    </div>
                                                    <div id="divGRAdd" runat="server" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Geographic Region</span>
                                                                    <asp:TextBox ID="txtAddMReg" runat="server" CssClass="form-control" Text="" CausesValidation="True" placeholder="Geographic Region Name" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div id="grEnclosure" runat="server" visible="false">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                    <h4><i runat="server" id="iMRMessage"><span runat="server" id="spnMR"></span></i></h4>
                                                                    <asp:Label ID="lblMRegAddSuccess" runat="server" Text="" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="input-group" style="width: 100%;">
                                                                    <asp:LinkButton ID="btnAddMReg" runat="server" CssClass="btn btn-success text-center" OnClick="btnAddMReg_Click"><i class="fa fa-plus"> Maj. Geographic Region</i></asp:LinkButton>
                                                                    <asp:LinkButton ID="btnAddMRegCancel" runat="server" CssClass="btn btn-warning text-center" OnClick="btnAddMRegCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                    <asp:LinkButton ID="btnMRegDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnMRegDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                           </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancSRegOuter" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlGeography_2A_acrdGeoMain" href="#BodyContent_ctrlGeography_2A_divSGOuter">
                                <span class="glyphicon glyphicon-menu-right"></span> Sub-Regions</a>
                        </h5>
                    </div>
                    <div id="divSGOuter" runat="server" class="panel-collapse collapse">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="uprgrsSubR" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlSubR">
                                <ProgressTemplate>
                                    <div id="divSubRProcessing" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                        z-index: 100;">
                                        <asp:Image ID="imgSubRProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel runat="server" ID="upnlSubR">
                                <ContentTemplate>
                                    <div class="panel panel-danger panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="acrdSReg" runat="server" class="panel-group">
                                                <div class="input-group" runat="server" style="width: 100%;">
                                                    <asp:LinkButton ID="btnSRAdd" runat="server" CssClass="btn btn-info text-center" OnClick="btnSRAdd_Click"><i class="fa fa-plus"> Sub-Region</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnSRCancel" runat="server" CssClass="btn btn-default text-center" OnClick="btnSRCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                </div>
                                                <div class="panel panel-info">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancSRList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlGeography_2A_acrdSReg" href="#BodyContent_ctrlGeography_2A_divSRList">
                                                                Geographic Sub-Region</a>
                                                        </h5>
                                                    </div>
                                                    <div id="divSRList" runat="server" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue">Major Region</span>
                                                                <asp:DropDownList ID="ddlSRRegion" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control"
                                                                    OnSelectedIndexChanged="ddlSRRegion_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0" Text="--- Select Major Geographic Region ---" />
                                                                </asp:DropDownList>
                                                            </div>
                                                            <asp:GridView ID="gvwSRList" runat="server" AutoGenerateColumns="False" DataKeyNames="gsrID"
                                                                AllowPaging="True" AllowSorting="True" CssClass="mydatagrid" Visible="False" OnPageIndexChanging="gvwSRList_PageIndexChanging"
                                                                AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                RowStyle-CssClass="rows">
                                                                <Columns>
                                                                    <asp:BoundField DataField="gsrID" HeaderText="gsrID" ReadOnly="True" InsertVisible="False" SortExpression="gsrID" Visible="False">
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="gsrName" HeaderText="Geographic Sub-Region" SortExpression="gsrName"></asp:BoundField>
                                                                    <asp:BoundField DataField="uTime" HeaderText="Last Updated On" SortExpression="uTime" ReadOnly="True" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}">
                                                                    </asp:BoundField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-success">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancSRAdd" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlGeography_2A_acrdSReg" href="#BodyContent_ctrlGeography_2A_divSRAdd">
                                                                Add Geographic Sub-Region</a>
                                                        </h5>
                                                    </div>
                                                    <div id="divSRAdd" runat="server" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue">Major Region</span>
                                                                <asp:DropDownList ID="ddlAddSRMReg" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control"
                                                                    OnSelectedIndexChanged="ddlAddSRMReg_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0" Text="--- Select Major Geographic Region ---" />
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue">Sub-Region</span>
                                                                <asp:TextBox ID="txtAddSRName" runat="server" CssClass="form-control" Text="" Enabled="false" CausesValidation="true"
                                                                    placeholder="Sub-Region Name (required)" />
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div id="srEnclosure" runat="server" visible="false">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                    <h4><i runat="server" id="iSR"><span runat="server" id="spnSR"></span></i></h4>
                                                                    <asp:Label ID="lblSRSuccess" runat="server" CssClass="lblSuccess" Visible="false" />
                                                                </div>
                                                            </div>
                                                            <div class="input-group" style="width: 100%;">
                                                                <asp:LinkButton ID="btnSRAddNew" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnSRAddNew_Click"><i class="fa fa-plus"> Sub-Region</i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnSRAddCancel" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnSRAddCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnSRDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnSRDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancCountryOuter" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlGeography_2A_acrdGeoMain" href="#BodyContent_ctrlGeography_2A_divCountryOuter">
                                <span class="glyphicon glyphicon-menu-right"></span> Country / Nation</a>
                        </h5>
                    </div>
                    <div id="divCountryOuter" runat="server" class="panel-collapse collapse">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="uprgrsCty" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlCty">
                                <ProgressTemplate>
                                    <div id="divCtyProcessing" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                        z-index: 100;">
                                        <asp:Image ID="imgCtyProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel runat="server" ID="upnlCty">
                                <ContentTemplate>
                                    <div class="panel panel-danger panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="acrdCountry" runat="server" class="panel-group">
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:LinkButton ID="btnCountryAdd" runat="server" CssClass="btn btn-info text-center" OnClick="btnCountryAdd_Click"><i class="fa fa-plus"> Country/Nation</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnCountryCancel" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnCountryCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                </div>
                                                <div class="panel panel-info">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancCountryList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlGeography_2A_acrdCountry" href="#BodyContent_ctrlGeography_2A_divCTYList">
                                                                Country / Nation</a>
                                                        </h5>
                                                    </div>
                                                    <div id="divCTYList" runat="server" class="row">
                                                        <div class="panel-body">
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Geographic Region</span>
                                                                    <asp:DropDownList ID="ddlCountryReg" runat="server" AppendDataBoundItems="True" AutoPostBack="True"
                                                                        CssClass="form-control" OnSelectedIndexChanged="ddlCountryReg_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Major Geographic Region ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Sub-Region</span>
                                                                    <asp:DropDownList ID="ddlCountrySR" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        Width="100%"
                                                                        OnSelectedIndexChanged="ddlCountrySR_SelectedIndexChanged" Enabled="False">
                                                                        <asp:ListItem Value="0" Text="--- Select Geographic Sub-Region ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <asp:GridView ID="gvwCountryList" runat="server" AutoGenerateColumns="False" DataKeyNames="ctyID"
                                                                    AllowPaging="True" AllowSorting="True" CssClass="mydatagrid" Visible="False" EmptyDataText="No Country/Nation found for selected Sub-Region"
                                                                    ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwCountryList_PageIndexChanging"
                                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                    RowStyle-CssClass="rows">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="ctyID" HeaderText="ctyID" ReadOnly="True" InsertVisible="False" SortExpression="ctyID"
                                                                            Visible="False"></asp:BoundField>
                                                                        <asp:BoundField DataField="ctyName" HeaderText="Country/Nation" SortExpression="ctyName"></asp:BoundField>
                                                                        <asp:BoundField DataField="ctyCode" HeaderText="Letter Code" SortExpression="ctyCode"></asp:BoundField>
                                                                        <asp:BoundField DataField="ctyDialCode" HeaderText="Dial Code" SortExpression="ctyDialCode"></asp:BoundField>
                                                                        <asp:BoundField DataField="cTime" HeaderText="cTime" SortExpression="cTime" Visible="False"></asp:BoundField>
                                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" SortExpression="uTime" ReadOnly="True"
                                                                            DataFormatString="{0:MMM d , yyyy --- hh:mm tt}"></asp:BoundField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-success">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancAddCountry" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlGeography_2A_acrdCountry" href="#BodyContent_ctrlGeography_2A_divCTYAdd">
                                                                Add Country / Nation</a>
                                                        </h5>
                                                    </div>
                                                    <div id="divCTYAdd" runat="server" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Geographic Region</span>
                                                                    <asp:DropDownList ID="ddlCAddRegion" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control"
                                                                        OnSelectedIndexChanged="ddlCAddRegion_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Major Geographic Region ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Sub-Region</span>
                                                                    <asp:DropDownList ID="ddlCAddSRegion" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        OnSelectedIndexChanged="ddlCAddSRegion_SelectedIndexChanged" Enabled="False">
                                                                        <asp:ListItem Value="0" Text="--- Select Geographic Sub-Region ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Country/Nation</span>
                                                                    <asp:TextBox ID="txtCAddName" runat="server" CssClass="form-control has-feedback" Text="" Enabled="false" CausesValidation="true"
                                                                        placeholder="Country/Nation Name (required)" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Country Code</span>
                                                                    <asp:TextBox ID="txtCAddCode" runat="server" CssClass="form-control has-feedback" Text="" Enabled="false" CausesValidation="true"
                                                                        placeholder="Country Code (required)" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Dial Code</span>
                                                                    <asp:TextBox ID="txtCAddDCode" runat="server" CssClass="form-control has-feedback" Text="" Enabled="false" CausesValidation="true"
                                                                        placeholder="Country Dial Code (required)" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div id="ctyEnclosure" runat="server" visible="false">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                    <h4><i runat="server" id="iCTY"><span runat="server" id="spnCTY"></span></i></h4>
                                                                    <asp:Label ID="lblCountrySuccess" runat="server" CssClass="lblSuccess" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="input-group" style="width: 100%;">
                                                                    <asp:LinkButton ID="btnCountryAddNew" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnCountryAddNew_Click"><i class="fa fa-plus"> Country/Nation</i></asp:LinkButton>
                                                                    <asp:LinkButton ID="btnCountryAddCancel" runat="server" CssClass="btn btn-default text-center" OnClick="btnCountryAddCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                    <asp:LinkButton ID="btnCountryDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnCountryDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancStateOuter" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlGeography_2A_acrdGeoMain" href="#BodyContent_ctrlGeography_2A_divStateOuter">
                                <span class="glyphicon glyphicon-menu-right"></span> State / Province</a>
                        </h5>
                    </div>
                    <div id="divStateOuter" runat="server" class="panel-collapse collapse">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="uprgrsST" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlST">
                                <ProgressTemplate>
                                    <div id="divSTProcessing" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                        z-index: 100;">
                                        <asp:Image ID="imgSTProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel runat="server" ID="upnlST">
                                <ContentTemplate>
                                    <div class="panel panel-danger panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="acrdState" runat="server" class="panel-group">
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:LinkButton ID="btnStateAdd" runat="server" CssClass="btn btn-info text-center" OnClick="btnStateAdd_Click"><i class="fa fa-plus"> State/Province</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnStateCancel" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnStateCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                </div>
                                                <div class="panel panel-danger">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancStateList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlGeography_2A_acrdState" href="#BodyContent_ctrlGeography_2A_divSTList">
                                                                State / Province</a>
                                                        </h5>
                                                    </div>
                                                    <div id="divSTList" runat="server" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Geographic Region</span>
                                                                    <asp:DropDownList ID="ddlSTReg" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control"
                                                                        OnSelectedIndexChanged="ddlSTReg_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Major Geographic Region ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Sub-Region</span>
                                                                    <asp:DropDownList ID="ddlSTSR" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        OnSelectedIndexChanged="ddlSTSR_SelectedIndexChanged" Enabled="False">
                                                                        <asp:ListItem Value="0" Text="--- Select Geographic Sub-Region ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Country/Nation</span>
                                                                    <asp:DropDownList ID="ddlSTCountry" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        OnSelectedIndexChanged="ddlSTCountry_SelectedIndexChanged" Enabled="False">
                                                                        <asp:ListItem Value="0" Text="--- Select Country/Nation ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <asp:GridView ID="gvwSTList" runat="server" AutoGenerateColumns="False" DataKeyNames="stID" AllowPaging="True"
                                                                    CssClass="mydatagrid" Visible="False" EmptyDataText="No State/Province found for selected Country/Nation" ShowHeaderWhenEmpty="true"
                                                                    OnPageIndexChanging="gvwSTList_PageIndexChanging"
                                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                    RowStyle-CssClass="rows">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="stID" HeaderText="stID" ReadOnly="True" InsertVisible="False" SortExpression="stID" Visible="False" />
                                                                        <asp:BoundField DataField="stName" HeaderText="State/Province" />
                                                                        <asp:BoundField DataField="stCode" HeaderText="Letter Code" />
                                                                        <asp:BoundField DataField="stAPI" HeaderText="API Code" />
                                                                        <asp:BoundField DataField="cTime" HeaderText="cTime" SortExpression="cTime" Visible="False"></asp:BoundField>
                                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" SortExpression="uTime" ReadOnly="True" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-success">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancStateAdd" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlGeography_2A_acrdState" href="#BodyContent_ctrlGeography_2A_divSTAdd">
                                                                Add State / Province</a>
                                                        </h5>
                                                    </div>
                                                    <div id="divSTAdd" runat="server" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Geographic Region</span>
                                                                    <asp:DropDownList ID="ddlAStReg" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control"
                                                                        OnSelectedIndexChanged="ddlAStReg_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Major Geographic Region ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Sub-Region</span>
                                                                    <asp:DropDownList ID="ddlAStSReg" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        OnSelectedIndexChanged="ddlAStSReg_SelectedIndexChanged" Enabled="False">
                                                                        <asp:ListItem Value="0" Text="--- Select Geographic Sub-Region ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Country/Nation</span>
                                                                    <asp:DropDownList ID="ddlAStCountry" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        OnSelectedIndexChanged="ddlAStCountry_SelectedIndexChanged" Enabled="False">
                                                                        <asp:ListItem Value="0" Text="--- Select Country/Nation ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">State/Province</span>
                                                                    <asp:TextBox ID="txtAStName" runat="server" CssClass="form-control" Text="" Enabled="false" CausesValidation="true"
                                                                        placeholder="State/Province Name (required)" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">State Code</span>
                                                                    <asp:TextBox ID="txtAStCode" runat="server" CssClass="form-control" Text="" Enabled="false" CausesValidation="true"
                                                                        placeholder="State Letter code" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">State API</span>
                                                                    <asp:TextBox ID="txtAStAPI" runat="server" CssClass="form-control" Text="" Enabled="false" CausesValidation="true"
                                                                        placeholder="State API" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">State ANSI</span>
                                                                    <asp:TextBox ID="txtAStANSI" runat="server" CssClass="form-control" Text="" Enabled="false" CausesValidation="true"
                                                                        placeholder="State ANSI" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">State FIPS</span>
                                                                    <asp:TextBox ID="txtAStFIPS" runat="server" CssClass="form-control" Text="" Enabled="false" CausesValidation="true"
                                                                        placeholder="State FIPS" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div id="stEnclosure" runat="server" visible="false">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                    <h4><i runat="server" id="iST"><span runat="server" id="spnST"></span></i></h4>
                                                                    <asp:Label ID="lblStateSuccess" runat="server" Text="" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="input-group" style="width: 100%;">
                                                                    <asp:LinkButton ID="btnStateAddNew" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnStateAddNew_Click"><i class="fa fa-plus"> State/Province</i></asp:LinkButton>
                                                                    <asp:LinkButton ID="btnStateAddCancel" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnStateAddCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                    <asp:LinkButton ID="btnStateDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnStateDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancCountyOuter" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlGeography_2A_acrdGeoMain" href="#BodyContent_ctrlGeography_2A_divCountyOuter">
                                <span class="glyphicon glyphicon-menu-right"></span> County / District / Division / Municipality</a>
                        </h5>
                    </div>
                    <div id="divCountyOuter" runat="server" class="panel-collapse collapse">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="uprgrsCnty" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlCnty">
                                <ProgressTemplate>
                                    <div id="divCntyProcessing" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                        z-index: 100;">
                                        <asp:Image ID="imgCntyProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel runat="server" ID="upnlCnty">
                                <ContentTemplate>
                                    <div class="panel panel-danger panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="acrdCounty" runat="server" class="panel-group">
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:LinkButton ID="btnCountyAdd" runat="server" CssClass="btn btn-info text-center" OnClick="btnCountyAdd_Click"><i class="fa fa-plus"> County/District/Municipality</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnCountyCancel" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnCountyCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                </div>
                                                <div class="panel panel-info">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancCountyList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlGeography_2A_acrdCounty"
                                                                href="#BodyContent_ctrlGeography_2A_divCNYList">County / District / Division / Municipality</a>
                                                        </h5>
                                                    </div>
                                                    <div id="divCNYList" runat="server" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Geographic Region</span>
                                                                    <asp:DropDownList ID="ddlCntyReg" runat="server" AppendDataBoundItems="True" AutoPostBack="True"
                                                                        CssClass="form-control"
                                                                        OnSelectedIndexChanged="ddlCntyReg_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Major Geographic Region ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Sub-Region</span>
                                                                    <asp:DropDownList ID="ddlCntySR" runat="server" AppendDataBoundItems="true" AutoPostBack="true"
                                                                        CssClass="form-control"
                                                                        Enabled="False" OnSelectedIndexChanged="ddlCntySR_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Geographic Sub-Region ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Country/Nation</span>
                                                                    <asp:DropDownList ID="ddlCntyCountry" runat="server" AppendDataBoundItems="true" AutoPostBack="true"
                                                                        CssClass="form-control"
                                                                        Enabled="False" OnSelectedIndexChanged="ddlCntyCountry_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Country/Nation ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">State/Province</span>
                                                                    <asp:DropDownList ID="ddlCntyState" runat="server" AppendDataBoundItems="true" AutoPostBack="true"
                                                                        CssClass="form-control"
                                                                        Enabled="False" OnSelectedIndexChanged="ddlCntyState_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select State/Province ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <asp:GridView runat="server" ID="gvwCountyList" AutoGenerateColumns="False" DataKeyNames="cntyID" AllowPaging="True"
                                                                    CssClass="mydatagrid"
                                                                    Visible="False" ShowHeaderWhenEmpty="True" EmptyDataText="No data found for selected State/Province"
                                                                    OnPageIndexChanging="gvwCountyList_PageIndexChanging"
                                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                    RowStyle-CssClass="rows">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="cntyID" HeaderText="cntyID" ReadOnly="True" InsertVisible="False"
                                                                            SortExpression="cntyID" Visible="False" />
                                                                        <asp:BoundField DataField="cntyName" HeaderText="County/District/Division" />
                                                                        <asp:BoundField DataField="cntyCode" HeaderText="County Code" />
                                                                        <asp:BoundField DataField="cntyFIPS" HeaderText="FIPS Code" />
                                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-success">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancCNYAdd" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlGeography_2A_acrdCounty"
                                                                href="#BodyContent_ctrlGeography_2A_divCNYAdd">Add County / District / Division / Municipality</a>
                                                        </h5>
                                                    </div>
                                                    <div id="divCNYAdd" runat="server" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Geographic Region</span>
                                                                    <asp:DropDownList ID="ddlACntyReg" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control"
                                                                        OnSelectedIndexChanged="ddlACntyReg_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Major Geographic Region ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Sub-Region</span>
                                                                    <asp:DropDownList ID="ddlACntySReg" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        Enabled="False" OnSelectedIndexChanged="ddlACntySReg_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Geographic Sub-Region ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Country/Nation</span>
                                                                    <asp:DropDownList ID="ddlACntyCountry" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        Enabled="false" OnSelectedIndexChanged="ddlACntyCountry_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Country/Nation ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">State/Province</span>
                                                                    <asp:DropDownList ID="ddlACntyState" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        Enabled="False" OnSelectedIndexChanged="ddlACntyState_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select State/Province ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Name</span>
                                                                    <asp:TextBox ID="txtACntyName" runat="server" CssClass="form-control has-feedback" Text="" Enabled="false" CausesValidation="true"
                                                                        placeholder="Name (required)" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Code</span>
                                                                    <asp:TextBox ID="txtACntyCode" runat="server" CssClass="form-control has-feedback" Text="" Enabled="false" CausesValidation="true"
                                                                        placeholder="County/District Code" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">FIPS Code</span>
                                                                    <asp:TextBox ID="txtACntyFIPS" runat="server" CssClass="form-control has-feedback" Text="" Enabled="false" CausesValidation="true"
                                                                        placeholder="County/District FIPS Code" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div id="cnyEnclosure" runat="server" visible="false">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                    <h4><i runat="server" id="iCNY"><span runat="server" id="spnCNY"></span></i></h4>
                                                                    <asp:Label ID="lblCountySuccess" runat="server" CssClass="lblSuccess" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="input-group" style="width: 100%;">
                                                                    <asp:LinkButton ID="btnCountyAddNew" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnCountyAddNew_Click"><i class="fa fa-plus"> County/District/Municipality</i></asp:LinkButton>
                                                                    <asp:LinkButton ID="btnCountyAddCancel" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnCountyAddCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                    <asp:LinkButton ID="btnCountyAddDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnCountyAddDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancCityOuter" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlGeography_2A_acrdGeoMain" href="#BodyContent_ctrlGeography_2A_divCityOuter">
                                <span class="glyphicon glyphicon-menu-right"></span> City / Town</a>
                        </h5>
                    </div>
                    <div id="divCityOuter" runat="server" class="panel-collapse collapse">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="uprgrsCity" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlCity">
                                <ProgressTemplate>
                                    <div id="divCityProcessing" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                        z-index: 100;">
                                        <asp:Image ID="imgCityProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel runat="server" ID="upnlCity">
                                <ContentTemplate>
                                    <div class="panel panel-danger panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="acrdCity" runat="server" class="panel-group">
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:LinkButton ID="btnCityAdd" runat="server" CssClass="btn btn-info text-center" OnClick="btnCityAdd_Click"><i class="fa fa-plus"> City/Town</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnCityCancel" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnCityCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                </div>
                                                <div class="panel panel-info">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancCityList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlGeography_2A_acrdCity"
                                                                href="#BodyContent_ctrlGeography_2A_divCITYList">City / Town</a>
                                                        </h5>
                                                    </div>
                                                    <div id="divCITYList" runat="server" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Geographic Region</span>
                                                                    <asp:DropDownList ID="ddlCityReg" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control"
                                                                        OnSelectedIndexChanged="ddlCityReg_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Major Geographic Region ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Sub-Region</span>
                                                                    <asp:DropDownList ID="ddlCitySR" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        Enabled="False" OnSelectedIndexChanged="ddlCitySR_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Geographic Sub-Region ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Country/Nation</span>
                                                                    <asp:DropDownList ID="ddlCityCountry" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        Enabled="False" OnSelectedIndexChanged="ddlCityCountry_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Country/Nation ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">State/Province</span>
                                                                    <asp:DropDownList ID="ddlCityState" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        Enabled="False" OnSelectedIndexChanged="ddlCityState_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select State/Province ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">County/District</span>
                                                                    <asp:DropDownList ID="ddlCityCounty" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        Enabled="False" OnSelectedIndexChanged="ddlCityCounty_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select County/Municipality/Division/District ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6"></div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <asp:GridView ID="gvwCityList" runat="server" AutoGenerateColumns="False" AllowPaging="True" CssClass="mydatagrid" DataKeyNames="cityID"
                                                                    Visible="False" OnPageIndexChanging="gvwCityList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                    PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="cityID" HeaderText="cityID" Visible="False" />
                                                                        <asp:BoundField DataField="cityName" HeaderText="City/Town" />
                                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" ReadOnly="True" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-success">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancCityAdd" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlGeography_2A_acrdCity"
                                                                href="#BodyContent_ctrlGeography_2A_divCITYList">Add City / Town</a>
                                                        </h5>
                                                    </div>
                                                    <div id="divCITYAdd" runat="server" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Geographic Region</span>
                                                                    <asp:DropDownList ID="ddlACityReg" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control"
                                                                        OnSelectedIndexChanged="ddlACityReg_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Major Geographic Region ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Sub-Region</span>
                                                                    <asp:DropDownList ID="ddlACitySReg" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        Enabled="False" OnSelectedIndexChanged="ddlACitySReg_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Geographic Sub-Region ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Country/Nation</span>
                                                                    <asp:DropDownList ID="ddlACityCountry" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        Enabled="False" OnSelectedIndexChanged="ddlACityCountry_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Country/Nation ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">State/Province</span>
                                                                    <asp:DropDownList ID="ddlACityState" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        Enabled="False" OnSelectedIndexChanged="ddlACityState_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select State/Province ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">County/District</span>
                                                                    <asp:DropDownList ID="ddlACityCounty" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        Enabled="False" OnSelectedIndexChanged="ddlACityCounty_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select County/Municipality/Division/District ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Name</span>
                                                                    <asp:TextBox ID="txtACityName" runat="server" CssClass="form-control has-feedback" Text="" Enabled="false" CausesValidation="true" placeholder="Name (required)" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div id="cityEnclosure" runat="server" visible="false">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                    <h4><i runat="server" id="iCITY"><span runat="server" id="spnCITY"></span></i></h4>
                                                                    <asp:Label ID="lblCitySuccess" runat="server" CssClass="lblSuccess" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="input-group" style="width: 100%;">
                                                                    <asp:LinkButton ID="btnCityAddNew" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnCityAddNew_Click"><i class="fa fa-plus"> City/Town</i></asp:LinkButton>
                                                                    <asp:LinkButton ID="btnCityAddCancel" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnCityAddCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                    <asp:LinkButton ID="btnCityDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnCityDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancZipOuter" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlGeography_2A_acrdGeoMain" href="#BodyContent_ctrlGeography_2A_divZipOuter">
                                <span class="glyphicon glyphicon-menu-right"></span> Zip / Postal Codes</a>
                        </h5>
                    </div>
                    <div id="divZipOuter" runat="server" class="panel-collapse collapse">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="uprgrsZip" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlZip">
                                <ProgressTemplate>
                                    <div id="divZipProcessing" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                        z-index: 100;">
                                        <asp:Image ID="imgZipProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel runat="server" ID="upnlZip">
                                <ContentTemplate>
                                    <div class="panel panel-danger panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="ancZip" runat="server" class="panel-group">
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:LinkButton ID="btnZipAdd" runat="server" CssClass="btn btn-info text-center" OnClick="btnZipAdd_Click"><i class="fa fa-plus"> Zip/Postal Code</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnZipCancel" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnZipCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                </div>
                                                <div class="panel panel-info">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancZipList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlGeography_2A_acrdZip"
                                                                href="#BodyContent_ctrlGeography_2A_divZIPList">Zip / Postal code</a>
                                                        </h5>
                                                    </div>
                                                    <div id="divZIPList" runat="server" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Geographic Region</span>
                                                                    <asp:DropDownList ID="ddlZipReg" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control"
                                                                        OnSelectedIndexChanged="ddlZipReg_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Major Geographic Region ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Sub-Region</span>
                                                                    <asp:DropDownList ID="ddlZipSR" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        Enabled="false" OnSelectedIndexChanged="ddlZipSR_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Geographic Sub-Region ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Country/Nation</span>
                                                                    <asp:DropDownList ID="ddlZipCountry" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        Enabled="false" OnSelectedIndexChanged="ddlZipCountry_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Country/Nation ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">State/Province</span>
                                                                    <asp:DropDownList ID="ddlZipState" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        Enabled="false" OnSelectedIndexChanged="ddlZipState_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select State/Province ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">County/District</span>
                                                                    <asp:DropDownList ID="ddlZipCounty" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        Enabled="false" OnSelectedIndexChanged="ddlZipCounty_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select County/Municipality/Division/District ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <asp:GridView ID="gvwZipList" runat="server" AutoGenerateColumns="False" DataKeyNames="zipID" AllowPaging="True"
                                                                    CssClass="mydatagrid" OnRowDataBound="gvwZipList_RowDataBound" Visible="false" OnPageIndexChanging="gvwZipList_PageIndexChanging"
                                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                    RowStyle-CssClass="rows">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="zipID" HeaderText="zipID" InsertVisible="False" Visible="False" />
                                                                        <asp:BoundField DataField="zipCode" HeaderText="Zip/Postal Code" />
                                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" ReadOnly="True" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-success">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="ancZipAdd" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlGeography_2A_acrdZip"
                                                                href="#BodyContent_ctrlGeography_2A_divZIPAdd">Add Zip / Postal code</a>
                                                        </h5>
                                                    </div>
                                                    <div id="divZIPAdd" runat="server" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Geographic Region</span>
                                                                    <asp:DropDownList ID="ddlAZipReg" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control"
                                                                        OnSelectedIndexChanged="ddlAZipReg_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Major Geographic Region ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Sub-Region</span>
                                                                    <asp:DropDownList ID="ddlAZipSReg" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        Enabled="false" OnSelectedIndexChanged="ddlAZipSReg_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Geographic Sub-Region ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Country/Nation</span>
                                                                    <asp:DropDownList ID="ddlAZipCountry" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        Enabled="false" OnSelectedIndexChanged="ddlAZipCountry_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Country/Nation ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">State/Province</span>
                                                                    <asp:DropDownList ID="ddlAZipState" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        Enabled="false" OnSelectedIndexChanged="ddlAZipState_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select State/Province ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">County/District</span>
                                                                    <asp:DropDownList ID="ddlAZipCounty" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        Enabled="false" OnSelectedIndexChanged="ddlAZipCounty_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select County/Municipality/Division/District ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Datum</span>
                                                                    <asp:DropDownList ID="ddlAZipDatum" runat="server" CssClass="form-control" DataTextField="datName"
                                                                        DataValueField="datID" AutoPostBack="True" AppendDataBoundItems="True" Enabled="False" OnSelectedIndexChanged="ddlAZipDatum_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Datum ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Latitude</span>
                                                                    <asp:TextBox ID="txtAZipLatitude" runat="server" CssClass="form-control has-feedback" Text="" Enabled="false"
                                                                        CausesValidation="true" placeholder="Latitude (required) (Range -90 to +90)" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Longitude</span>
                                                                    <asp:TextBox ID="txtAZipLongitude" runat="server" CssClass="form-control has-feedback" Text="" Enabled="false"
                                                                        CausesValidation="true" placeholder="Longitude (required) (Range -180 to +180)" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Zip/Postalcode</span>
                                                                    <asp:TextBox ID="txtAZipValue" runat="server" CssClass="form-control has-feedback" Text="" Enabled="false" CausesValidation="true"
                                                                        placeholder="Zip/Postalcode (Required)" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div id="zipEnclosure" runat="server" visible="false">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                    <h4><i runat="server" id="iZip"><span runat="server" id="spnZip"></span></i></h4>
                                                                    <asp:Label ID="lblZipSuccess" runat="server" Text="" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="input-group" style="width: 100%;">
                                                                    <asp:LinkButton ID="btnZipAddNew" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnZipAddNew_Click"><i class="fa fa-plus"> Zip/Postal Code</i></asp:LinkButton>
                                                                    <asp:LinkButton ID="btnZipAddCancel" runat="server" CssClass="btn btn-default text-center" OnClick="btnZipAddCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                    <asp:LinkButton ID="btnZipDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnZipDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="aFieldOuter" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlGeography_2A_acrdGeoMain" href="#BodyContent_ctrlGeography_2A_divFieldOuter">
                                <span class="glyphicon glyphicon-menu-right"></span> Oil & Gas Fields</a>
                        </h5>
                    </div>
                    <div id="divFieldOuter" runat="server" class="panel-collapse collapse">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="uprgrsField" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlField">
                                <ProgressTemplate>
                                    <div id="divFieldProcessing" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                        z-index: 100;">
                                        <asp:Image ID="imgFieldProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel runat="server" ID="upnlField">
                                <ContentTemplate>
                                    <div class="panel panel-danger panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="acrdField" runat="server" class="panel-group">
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:LinkButton ID="btnFieldAdd" runat="server" CssClass="btn btn-info text-center" OnClick="btnFieldAdd_Click"><i class="fa fa-plus"> Field</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnFieldCancel" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnFieldCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                </div>
                                                <div class="panel panel-info">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="a1" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlGeography_2A_acrdField"
                                                                href="#BodyContent_ctrlGeography_2A_divFLDList">Oil & Gas Field</a>
                                                        </h5>
                                                    </div>
                                                    <div id="divFLDList" runat="server" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Geographic Region</span>
                                                                    <asp:DropDownList ID="ddlFldReg" runat="server" AppendDataBoundItems="True" AutoPostBack="True"
                                                                        CssClass="form-control"
                                                                        OnSelectedIndexChanged="ddlFldReg_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Major Geographic Region ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Sub-Region</span>
                                                                    <asp:DropDownList ID="ddlFldSReg" runat="server" AppendDataBoundItems="true" AutoPostBack="true"
                                                                        CssClass="form-control"
                                                                        Enabled="false" OnSelectedIndexChanged="ddlFldSReg_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Geographic Sub-Region ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Country/Nation</span>
                                                                    <asp:DropDownList ID="ddlFldCountry" runat="server" AppendDataBoundItems="true" AutoPostBack="true"
                                                                        CssClass="form-control"
                                                                        Enabled="false" OnSelectedIndexChanged="ddlFldCountry_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Country/Nation ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">State/Province</span>
                                                                    <asp:DropDownList ID="ddlFldState" runat="server" AppendDataBoundItems="true" AutoPostBack="true"
                                                                        CssClass="form-control"
                                                                        Enabled="false" OnSelectedIndexChanged="ddlFldState_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select State/Province ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">County/District</span>
                                                                    <asp:DropDownList ID="ddlFldCounty" runat="server" AppendDataBoundItems="true" AutoPostBack="true"
                                                                        CssClass="form-control"
                                                                        Enabled="false" OnSelectedIndexChanged="ddlFldCounty_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select County/Municipality/Division/District ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <asp:GridView ID="gvwFldList" runat="server" EmptyDataText="No Field found for selected County/District/Division"
                                                                    ShowHeaderWhenEmpty="true" OnRowDataBound="gvwFldList_RowDataBound" AutoGenerateColumns="False" DataKeyNames="fldID"
                                                                    AllowPaging="True" CssClass="mydatagrid" Visible="false" OnPageIndexChanging="gvwFldList_PageIndexChanging"
                                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                    RowStyle-CssClass="rows">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="fldID" HeaderText="fldID" ReadOnly="True" Visible="false" />
                                                                        <asp:BoundField DataField="fldName" HeaderText="Name" />
                                                                        <asp:BoundField DataField="fldCode" HeaderText="Code" />
                                                                        <asp:BoundField DataField="hcOil" HeaderText="Oil" />
                                                                        <asp:BoundField DataField="hcGas" HeaderText="Gas" />
                                                                        <asp:BoundField DataField="hcAssoc" HeaderText="Associated" />
                                                                        <asp:BoundField DataField="hcUnknown" HeaderText="Unknown" />
                                                                        <asp:BoundField DataField="Year" HeaderText="Year Discovered" />
                                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" ReadOnly="True" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-success">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title" style="text-align: left">
                                                            <a id="a2" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlGeography_2A_acrdField"
                                                                href="#BodyContent_ctrlGeography_2A_divFLDAdd">Add Oil & Gas Field</a>
                                                        </h5>
                                                    </div>
                                                    <div id="divFLDAdd" runat="server" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Geographic Region</span>
                                                                    <asp:DropDownList ID="ddlFAddReg" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control"
                                                                        OnSelectedIndexChanged="ddlFAddReg_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Major Geographic Region ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Sub-Region</span>
                                                                    <asp:DropDownList ID="ddlFAddSReg" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        Enabled="false" OnSelectedIndexChanged="ddlFAddSReg_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Geographic Sub-Region ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Country/Nation</span>
                                                                    <asp:DropDownList ID="ddlFAddCountry" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        Enabled="false" OnSelectedIndexChanged="ddlFAddCountry_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="--- Select Country/Nation ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">State/Province</span>
                                                                    <asp:DropDownList ID="ddlFAddState" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        Enabled="false" OnSelectedIndexChanged="ddlFAddState_SelectedIndexChanged" CausesValidation="False">
                                                                        <asp:ListItem Value="0" Text="--- Select State/Province ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">County/District</span>
                                                                    <asp:DropDownList ID="ddlFAddCounty" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
                                                                        Enabled="false" OnSelectedIndexChanged="ddlFAddCounty_SelectedIndexChanged" CausesValidation="False">
                                                                        <asp:ListItem Value="0" Text="--- Select County/Municipality/Division/District ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Hydrocarbon (Oil)</span>
                                                                    <asp:DropDownList ID="ddlFAddOil" runat="server" CssClass="form-control" Enabled="False"
                                                                        AutoPostBack="True" AppendDataBoundItems="True" OnSelectedIndexChanged="ddlFAddOil_SelectedIndexChanged" CausesValidation="False">
                                                                        <asp:ListItem Value="0" Text="--- Select Field Hydrocarbon (Oil) Option ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Hydrocarbon (Gas)</span>
                                                                    <asp:DropDownList ID="ddlFAddGas" runat="server" CssClass="form-control" Enabled="False" AutoPostBack="True"
                                                                        AppendDataBoundItems="True"
                                                                        OnSelectedIndexChanged="ddlFAddGas_SelectedIndexChanged" CausesValidation="False">
                                                                        <asp:ListItem Value="0" Text="--- Select Field Hydrocarbon (Gas) Option ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Hydrocarbon (Assoc.)</span>
                                                                    <asp:DropDownList ID="ddlFAddAssoc" runat="server" CssClass="form-control" Enabled="False" AutoPostBack="True"
                                                                        AppendDataBoundItems="True"
                                                                        OnSelectedIndexChanged="ddlFAddAssoc_SelectedIndexChanged" CausesValidation="False">
                                                                        <asp:ListItem Value="0" Text="--- Select Field Hydrocarbon (Associated) Option ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Hydrocarbon (Unknown)</span>
                                                                    <asp:DropDownList ID="ddlFAddUnknown" runat="server" CssClass="form-control" Enabled="False" AutoPostBack="True"
                                                                        AppendDataBoundItems="True"
                                                                        OnSelectedIndexChanged="ddlFAddUnknown_SelectedIndexChanged" CausesValidation="False">
                                                                        <asp:ListItem Value="0" Text="--- Select Field Hydrocarbon (Unknown) Option ---" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Name</span>
                                                                    <asp:TextBox ID="txtFAddName" runat="server" CssClass="form-control has-feedback" Text="" CausesValidation="True" Enabled="False"
                                                                        placeholder="Field Name (required)" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Code</span>
                                                                    <asp:TextBox ID="txtFAddCode" runat="server" CssClass="form-control has-feedback" Text="" CausesValidation="True" Enabled="False"
                                                                        placeholder="Field code (required)" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon bg-light-blue">Year</span>
                                                                    <asp:TextBox ID="txtFAddYear" runat="server" CssClass="form-control has-feedback" Text="" CausesValidation="True" Enabled="False"
                                                                        placeholder="Discovery Year" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div id="fldEnclosure" runat="server" visible="false">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                    <h4><i runat="server" id="iFLD"><span runat="server" id="spnFLD"></span></i></h4>
                                                                    <asp:Label ID="lblFieldSuccess" runat="server" Text="" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="input-group" style="width: 100%;">
                                                                    <asp:LinkButton ID="btnFAddNew" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnFAddNew_Click"><i class="fa fa-plus"> Field</i></asp:LinkButton>
                                                                    <asp:LinkButton ID="btnFieldAddCancel" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnFieldAddCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                    <asp:LinkButton ID="btnFDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnFDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancHydOuter" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlGeography_2A_acrdGeoMain" href="#BodyContent_ctrlGeography_2A_divHydOuter">
                                <span class="glyphicon glyphicon-menu-right"></span> Oil & Gas Field Hydrocarbon Options</a>
                        </h5>
                    </div>
                    <div id="divHydOuter" runat="server" class="panel-collapse collapse">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="uprgrsHCO" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlHCO">
                                <ProgressTemplate>
                                    <div id="divHCOProcessing" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                        z-index: 100;">
                                        <asp:Image ID="imgHCOProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel runat="server" ID="upnlHCO">
                                <ContentTemplate>
                                    <div class="panel panel-danger panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:GridView ID="gvwHCO" runat="server" AutoGenerateColumns="False" DataKeyNames="hcoID" AllowPaging="True"
                                                AllowSorting="True" CssClass="mydatagrid" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header"
                                                PagerStyle-CssClass="pager" RowStyle-CssClass="rows">
                                                <Columns>
                                                    <asp:BoundField DataField="hcoID" HeaderText="hcoID" Visible="False" />
                                                    <asp:BoundField DataField="hcoName" HeaderText="HydroCarbon Option Code" />
                                                    <asp:BoundField DataField="hcoDesc" HeaderText="HydroCarbon Option Code Description" />
                                                    <asp:BoundField DataField="uTime" HeaderText="Last Updated On" ReadOnly="True" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>