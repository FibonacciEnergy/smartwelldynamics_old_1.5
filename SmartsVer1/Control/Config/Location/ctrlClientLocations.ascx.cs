﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Net.Mail;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Microsoft.SqlServer.Types;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using CLR = System.Drawing.Color;
using PH = PhoneNumbers;

namespace SmartsVer1.Control.Config.Location
{
    public partial class ctrlClientLocations : System.Web.UI.UserControl
    {
        String LoginName, username, domain, GetClientDBString;
        Int32 ClientID = -99;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();
                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);
                username = addr.User;
                domain = addr.Host;
                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
                //Datasources for controls on page
                System.Data.DataTable locTable = CLNT.getAddressTable(ClientID);
                System.Data.DataTable phTable = CLNT.getClientPhoneTable(ClientID);
                Dictionary<Int32, String> dmgType = DMG.getDemogTypeList();

                //Assigning datasource to controls
                this.gvwAddressList.DataSource = locTable;
                this.gvwAddressList.DataBind();
                this.ddlALType.Items.Clear();
                this.ddlALType.DataSource = dmgType;
                this.ddlALType.DataTextField = "Value";
                this.ddlALType.DataValueField = "Key";
                this.ddlALType.DataBind();
                this.ddlALType.Items.Insert(0, new ListItem("--- Select Location Type ---", "-1"));
                this.ddlALType.SelectedIndex = -1;
                this.ddlAPLocType.Items.Clear();
                this.ddlAPLocType.DataSource = dmgType;
                this.ddlAPLocType.DataTextField = "Value";
                this.ddlAPLocType.DataValueField = "Key";
                this.ddlAPLocType.DataBind();
                this.ddlAPLocType.Items.Insert(0, new ListItem("--- Select Location Type ---", "-1"));
                this.ddlAPLocType.SelectedIndex = -1;
                this.gvwPhoneList.DataSource = phTable;
                this.gvwPhoneList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                String cName = CLNT.getClientName(ClientID);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewLocation_Click(object sender, EventArgs e)
        {
            try
            {
                btnAddLocationClear_Click(null, null);
                this.divLOCList.Visible = false;
                this.divLOCAdd.Visible = true;
                this.btnAddAddressDone.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddLocationClear_Click(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlALType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> regList = DMG.getRegionList();
                this.txtALTitle.Text = null;
                this.txtALTitle.Enabled = true;
                this.ddlALRegion.Items.Clear();
                this.ddlALRegion.DataSource = regList;
                this.ddlALRegion.DataTextField = "Value";
                this.ddlALRegion.DataValueField = "Key";
                this.ddlALRegion.DataBind();
                this.ddlALRegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlALRegion.SelectedIndex = -1;
                this.ddlALRegion.Enabled = true;
                this.ddlALSRegion.Items.Clear();
                this.ddlALSRegion.DataBind();
                this.ddlALSRegion.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlALSRegion.SelectedIndex = -1;
                this.ddlALSRegion.Enabled = false;
                this.ddlALCountry.Items.Clear();
                this.ddlALCountry.DataBind();
                this.ddlALCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlALCountry.SelectedIndex = -1;
                this.ddlALCountry.Enabled = false;
                this.ddlALState.Items.Clear();
                this.ddlALState.DataBind();
                this.ddlALState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlALState.SelectedIndex = -1;
                this.ddlALState.Enabled = false;
                this.ddlALCounty.Items.Clear();
                this.ddlALCounty.DataBind();
                this.ddlALCounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlALCounty.SelectedIndex = -1;
                this.ddlALCounty.Enabled = false;
                this.ddlALCity.Items.Clear();
                this.ddlALCity.DataBind();
                this.ddlALCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlALCity.SelectedIndex = -1;
                this.ddlALCity.Enabled = false;
                this.ddlALZip.Items.Clear();
                this.ddlALZip.DataBind();
                this.ddlALZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlALZip.SelectedIndex = -1;
                this.ddlALZip.Enabled = false;
                this.txtALSt1.Text = null;
                this.txtALSt1.Enabled = false;
                this.txtALSt2.Text = null;
                this.txtALSt2.Enabled = false;
                this.txtALSt3.Text = null;
                this.txtALSt3.Enabled = false;
                this.lblAddressSuccess.Text = null;
                this.lblAddressSuccess.CssClass = "lblSuccess";
                this.lblAddressSuccess.Visible = false;
                this.btnAddAddress.Enabled = false;

                this.txtALTitle.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlALRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 rID = Convert.ToInt32(this.ddlALRegion.SelectedValue);
                Dictionary<Int32, String> srList = DMG.getSubRegionList(rID);
                this.ddlALSRegion.Items.Clear();
                this.ddlALSRegion.DataSource = srList;
                this.ddlALSRegion.DataTextField = "Value";
                this.ddlALSRegion.DataValueField = "Key";
                this.ddlALSRegion.DataBind();
                this.ddlALSRegion.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlALSRegion.SelectedIndex = -1;
                this.ddlALSRegion.Enabled = true;
                this.ddlALCountry.Items.Clear();
                this.ddlALCountry.DataBind();
                this.ddlALCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlALCountry.SelectedIndex = -1;
                this.ddlALCountry.Enabled = false;
                this.ddlALState.Items.Clear();
                this.ddlALState.DataBind();
                this.ddlALState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlALState.SelectedIndex = -1;
                this.ddlALState.Enabled = false;
                this.ddlALCounty.Items.Clear();
                this.ddlALCounty.DataBind();
                this.ddlALCounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlALCounty.SelectedIndex = -1;
                this.ddlALCounty.Enabled = false;
                this.ddlALCity.Items.Clear();
                this.ddlALCity.DataBind();
                this.ddlALCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlALCity.SelectedIndex = -1;
                this.ddlALCity.Enabled = false;
                this.ddlALZip.Items.Clear();
                this.ddlALZip.DataBind();
                this.ddlALZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlALZip.SelectedIndex = -1;
                this.ddlALZip.Enabled = false;
                this.txtALSt1.Text = null;
                this.txtALSt1.Enabled = false;
                this.txtALSt2.Text = null;
                this.txtALSt2.Enabled = false;
                this.txtALSt3.Text = null;
                this.txtALSt3.Enabled = false;
                this.lblAddressSuccess.Text = null;
                this.lblAddressSuccess.CssClass = "lblSuccess";
                this.lblAddressSuccess.Visible = false;
                this.btnAddAddress.Enabled = false;

                this.ddlALSRegion.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlALSRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlALSRegion.SelectedValue);
                Dictionary<Int32, String> ctyList = DMG.getCountryList(srID);
                this.ddlALCountry.Items.Clear();
                this.ddlALCountry.DataSource = ctyList;
                this.ddlALCountry.DataTextField = "Value";
                this.ddlALCountry.DataValueField = "Key";
                this.ddlALCountry.DataBind();
                this.ddlALCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlALCountry.SelectedIndex = -1;
                this.ddlALCountry.Enabled = true;
                this.ddlALState.Items.Clear();
                this.ddlALState.DataBind();
                this.ddlALState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlALState.SelectedIndex = -1;
                this.ddlALState.Enabled = false;
                this.ddlALCounty.Items.Clear();
                this.ddlALCounty.DataBind();
                this.ddlALCounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlALCounty.SelectedIndex = -1;
                this.ddlALCounty.Enabled = false;
                this.ddlALCity.Items.Clear();
                this.ddlALCity.DataBind();
                this.ddlALCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlALCity.SelectedIndex = -1;
                this.ddlALCity.Enabled = false;
                this.ddlALZip.Items.Clear();
                this.ddlALZip.DataBind();
                this.ddlALZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlALZip.SelectedIndex = -1;
                this.ddlALZip.Enabled = false;
                this.txtALSt1.Text = null;
                this.txtALSt1.Enabled = false;
                this.txtALSt2.Text = null;
                this.txtALSt2.Enabled = false;
                this.txtALSt3.Text = null;
                this.txtALSt3.Enabled = false;
                this.lblAddressSuccess.Text = null;
                this.lblAddressSuccess.CssClass = "lblSuccess";
                this.lblAddressSuccess.Visible = false;
                this.btnAddAddress.Enabled = false;

                this.ddlALCountry.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlALCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctyID = Convert.ToInt32(this.ddlALCountry.SelectedValue);
                Dictionary<Int32, String> stList = DMG.getStateList(ctyID);
                this.ddlALState.Items.Clear();
                this.ddlALState.DataSource = stList;
                this.ddlALState.DataTextField = "Value";
                this.ddlALState.DataValueField = "Key";
                this.ddlALState.DataBind();
                this.ddlALState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlALState.SelectedIndex = -1;
                this.ddlALState.Enabled = true;
                this.ddlALCounty.Items.Clear();
                this.ddlALCounty.DataBind();
                this.ddlALCounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlALCounty.SelectedIndex = -1;
                this.ddlALCounty.Enabled = false;
                this.ddlALCity.Items.Clear();
                this.ddlALCity.DataBind();
                this.ddlALCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlALCity.SelectedIndex = -1;
                this.ddlALCity.Enabled = false;
                this.ddlALZip.Items.Clear();
                this.ddlALZip.DataBind();
                this.ddlALZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlALZip.SelectedIndex = -1;
                this.ddlALZip.Enabled = false;
                this.txtALSt1.Text = null;
                this.txtALSt1.Enabled = false;
                this.txtALSt2.Text = null;
                this.txtALSt2.Enabled = false;
                this.txtALSt3.Text = null;
                this.txtALSt3.Enabled = false;
                this.lblAddressSuccess.Text = null;
                this.lblAddressSuccess.CssClass = "lblSuccess";
                this.lblAddressSuccess.Visible = false;
                this.btnAddAddress.Enabled = false;

                this.ddlALState.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlALState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 stID = Convert.ToInt32(this.ddlALState.SelectedValue);
                Dictionary<Int32, String> cnList = DMG.getCountyList(stID);
                this.ddlALCounty.Items.Clear();
                this.ddlALCounty.DataSource = cnList;
                this.ddlALCounty.DataTextField = "Value";
                this.ddlALCounty.DataValueField = "Key";
                this.ddlALCounty.DataBind();
                this.ddlALCounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlALCounty.SelectedIndex = -1;
                this.ddlALCounty.Enabled = true;
                this.ddlALCity.Items.Clear();
                this.ddlALCity.DataBind();
                this.ddlALCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlALCity.SelectedIndex = -1;
                this.ddlALCity.Enabled = false;
                this.ddlALZip.Items.Clear();
                this.ddlALZip.DataBind();
                this.ddlALZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlALZip.SelectedIndex = -1;
                this.ddlALZip.Enabled = false;
                this.txtALSt1.Text = null;
                this.txtALSt1.Enabled = false;
                this.txtALSt2.Text = null;
                this.txtALSt2.Enabled = false;
                this.txtALSt3.Text = null;
                this.txtALSt3.Enabled = false;
                this.lblAddressSuccess.Text = null;
                this.lblAddressSuccess.CssClass = "lblSuccess";
                this.lblAddressSuccess.Visible = false;
                this.btnAddAddress.Enabled = false;

                this.ddlALCounty.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlALCounty_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cnID = Convert.ToInt32(this.ddlALCounty.SelectedValue);
                Dictionary<Int32, String> citList = DMG.getCityList(cnID);
                this.ddlALCity.Items.Clear();
                this.ddlALCity.DataSource = citList;
                this.ddlALCity.DataTextField = "Value";
                this.ddlALCity.DataValueField = "Key";
                this.ddlALCity.DataBind();
                this.ddlALCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlALCity.SelectedIndex = -1;
                this.ddlALCity.Enabled = true;
                this.ddlALZip.Items.Clear();
                this.ddlALZip.DataBind();
                this.ddlALZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlALZip.SelectedIndex = -1;
                this.ddlALZip.Enabled = false;
                this.txtALSt1.Text = null;
                this.txtALSt1.Enabled = false;
                this.txtALSt2.Text = null;
                this.txtALSt2.Enabled = false;
                this.txtALSt3.Text = null;
                this.txtALSt3.Enabled = false;
                this.lblAddressSuccess.Text = null;
                this.lblAddressSuccess.CssClass = "lblSuccess";
                this.lblAddressSuccess.Visible = false;
                this.btnAddAddress.Enabled = false;

                this.ddlALCity.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlALCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cnID = Convert.ToInt32(this.ddlALCounty.SelectedValue);
                Dictionary<Int32, String> zpList = DMG.getZipList(cnID);
                this.ddlALZip.Items.Clear();
                this.ddlALZip.DataSource = zpList;
                this.ddlALZip.DataTextField = "Value";
                this.ddlALZip.DataValueField = "Key";
                this.ddlALZip.DataBind();
                this.ddlALZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlALZip.SelectedIndex = -1;
                this.ddlALZip.Enabled = true;
                this.txtALSt1.Text = null;
                this.txtALSt1.Enabled = false;
                this.txtALSt2.Text = null;
                this.txtALSt2.Enabled = false;
                this.txtALSt3.Text = null;
                this.txtALSt3.Enabled = false;
                this.lblAddressSuccess.Text = null;
                this.lblAddressSuccess.CssClass = "lblSuccess";
                this.lblAddressSuccess.Visible = false;
                this.btnAddAddress.Enabled = false;

                this.ddlALZip.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlALZip_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtALSt1.Text = null;
                this.txtALSt1.Enabled = true;
                this.txtALSt2.Text = null;
                this.txtALSt2.Enabled = true;
                this.txtALSt3.Text = null;
                this.txtALSt3.Enabled = true;
                this.lblAddressSuccess.Text = null;
                this.lblAddressSuccess.CssClass = "lblSuccess";
                this.lblAddressSuccess.Visible = false;
                this.btnAddAddress.Enabled = true;

                this.txtALSt1.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddAddress_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iReply = -99, dmgID = -99, regID = -99, sregID = -99, ctyID = -99, stID = -99, cntyID = -99, cityID = -99, zipID = -99;
                String lTitle = String.Empty, s1 = String.Empty, s2 = String.Empty, s3 = String.Empty;

                dmgID = Convert.ToInt32(this.ddlALType.SelectedValue);
                lTitle = Convert.ToString(this.txtALTitle.Text);
                regID = Convert.ToInt32(this.ddlALRegion.SelectedValue);
                sregID = Convert.ToInt32(this.ddlALSRegion.SelectedValue);
                ctyID = Convert.ToInt32(this.ddlALCountry.SelectedValue);
                stID = Convert.ToInt32(this.ddlALState.SelectedValue);
                cntyID = Convert.ToInt32(this.ddlALCounty.SelectedValue);
                cityID = Convert.ToInt32(this.ddlALCity.SelectedValue);
                zipID = Convert.ToInt32(this.ddlALZip.SelectedValue);
                s1 = Convert.ToString(this.txtALSt1.Text);
                s2 = Convert.ToString(this.txtALSt2.Text);
                s3 = Convert.ToString(this.txtALSt3.Text);

                if (String.IsNullOrEmpty(lTitle) || String.IsNullOrEmpty(s1))
                {
                    this.lblAddressSuccess.Text = "Missing values detected. Please provide all values before inserting new location into SMARTs.";
                    this.lblAddressSuccess.ForeColor = CLR.Red;
                    this.lblAddressSuccess.Visible = true;
                }
                else
                {
                    iReply = CLNT.createAddress(ClientID, dmgID, regID, sregID, ctyID, stID, cntyID, cityID, zipID, s1, s2, s3, lTitle, LoginName, domain);
                    if (iReply.Equals(1))
                    {
                        btnAddAddressClear_Click(null, null);
                        this.lblAddressSuccess.Text = "Successfully inserted new location " + "" + lTitle + "" + " into SMARTs.";
                        this.lblAddressSuccess.ForeColor = CLR.Green;
                        this.lblAddressSuccess.Visible = true;
                    }
                    else
                    {
                        btnAddAddressClear_Click(null, null);
                        this.lblAddressSuccess.Text = "Unable to insert new location " + lTitle + " into SMARTs. Please try again.";
                        this.lblAddressSuccess.ForeColor = CLR.Red;
                        this.lblAddressSuccess.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddAddressClear_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> dmgType = DMG.getDemogTypeList();

                this.ddlALType.Items.Clear();
                this.ddlALType.DataSource = dmgType;
                this.ddlALType.DataTextField = "Value";
                this.ddlALType.DataValueField = "Key";
                this.ddlALType.DataBind();
                this.ddlALType.Items.Insert(0, new ListItem("--- Select Location Type ---", "-1"));
                this.ddlALType.SelectedIndex = -1;
                this.txtALTitle.Text = null;
                this.txtALTitle.Enabled = false;
                this.ddlALRegion.Items.Clear();
                this.ddlALRegion.DataBind();
                this.ddlALRegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlALRegion.SelectedIndex = -1;
                this.ddlALRegion.Enabled = false;
                this.ddlALSRegion.Items.Clear();
                this.ddlALSRegion.DataBind();
                this.ddlALSRegion.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlALSRegion.SelectedIndex = -1;
                this.ddlALSRegion.Enabled = false;
                this.ddlALCountry.Items.Clear();
                this.ddlALCountry.DataBind();
                this.ddlALCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlALCountry.SelectedIndex = -1;
                this.ddlALCountry.Enabled = false;
                this.ddlALState.Items.Clear();
                this.ddlALState.DataBind();
                this.ddlALState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlALState.SelectedIndex = -1;
                this.ddlALState.Enabled = false;
                this.ddlALCounty.Items.Clear();
                this.ddlALCounty.DataBind();
                this.ddlALCounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality ---", "-1"));
                this.ddlALCounty.SelectedIndex = -1;
                this.ddlALCounty.Enabled = false;
                this.ddlALCity.Items.Clear();
                this.ddlALCity.DataBind();
                this.ddlALCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlALCity.SelectedIndex = -1;
                this.ddlALCity.Enabled = false;
                this.ddlALZip.Items.Clear();
                this.ddlALZip.DataBind();
                this.ddlALZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlALZip.SelectedIndex = -1;
                this.ddlALZip.Enabled = false;
                this.txtALSt1.Text = null;
                this.txtALSt1.Enabled = false;
                this.txtALSt2.Text = null;
                this.txtALSt2.Enabled = false;
                this.txtALSt3.Text = null;
                this.txtALSt3.Enabled = false;
                this.lblAddressSuccess.Text = null;
                this.lblAddressSuccess.CssClass = "lblSuccess";
                this.lblAddressSuccess.Visible = false;
                this.btnAddAddress.Enabled = false;

                this.ddlALType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddAddressDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnAddAddressClear_Click(null, null);
                System.Data.DataTable locTable = CLNT.getAddressTable(ClientID);
                this.gvwAddressList.DataSource = locTable;
                this.gvwAddressList.DataBind();
                this.divLOCAdd.Visible = false;
                this.divLOCList.Visible = true;
                this.btnAddAddressDone.Visible = false;

                this.gvwAddressList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAPLocType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 dmID = Convert.ToInt32(this.ddlAPLocType.SelectedValue);
                Dictionary<Int32, String> ttList = CLNT.getClientLocationTitleList(dmID, ClientID);
                this.ddlAPLocTitle.Items.Clear();
                this.ddlAPLocTitle.DataSource = ttList;
                this.ddlAPLocTitle.DataTextField = "Value";
                this.ddlAPLocTitle.DataValueField = "Key";
                this.ddlAPLocTitle.DataBind();
                this.ddlAPLocTitle.Items.Insert(0, new ListItem("--- Select Location ---", "-1"));
                this.ddlAPLocTitle.SelectedIndex = -1;
                this.ddlAPLocTitle.Enabled = true;
                this.ddlAPRegion.Items.Clear();
                this.ddlAPRegion.DataBind();
                this.ddlAPRegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlAPRegion.SelectedIndex = -1;
                this.ddlAPRegion.Enabled = false;
                this.ddlAPSubReg.Items.Clear();
                this.ddlAPSubReg.DataBind();
                this.ddlAPSubReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlAPSubReg.SelectedIndex = -1;
                this.ddlAPSubReg.Enabled = false;
                this.ddlAPCountry.Items.Clear();
                this.ddlAPCountry.DataBind();
                this.ddlAPCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlAPCountry.SelectedIndex = -1;
                this.ddlAPCountry.Enabled = false;
                this.ddlAPType.Items.Clear();
                this.ddlAPType.DataBind();
                this.ddlAPType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlAPType.SelectedIndex = -1;
                this.ddlAPType.Enabled = false;
                this.txtAPPhone.Text = "";
                this.txtAPPhone.Enabled = false;
                this.txtAPExtension.Text = "";
                this.txtAPExtension.Enabled = false;
                this.lblAPSuccess.Text = "";
                this.lblAPSuccess.CssClass = "lblSuccess";
                this.lblAPSuccess.Visible = false;
                this.ddlAPLocType.Enabled = true;
                this.btnAddPhone.Enabled = false;

                this.ddlAPLocTitle.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAPLocTitle_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> rgList = DMG.getRegionList();
                this.ddlAPRegion.Items.Clear();
                this.ddlAPRegion.DataSource = rgList;
                this.ddlAPRegion.DataTextField = "Value";
                this.ddlAPRegion.DataValueField = "Key";
                this.ddlAPRegion.DataBind();
                this.ddlAPRegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlAPRegion.SelectedIndex = -1;
                this.ddlAPRegion.Enabled = true;
                this.ddlAPSubReg.Items.Clear();
                this.ddlAPSubReg.DataBind();
                this.ddlAPSubReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlAPSubReg.SelectedIndex = -1;
                this.ddlAPSubReg.Enabled = false;
                this.ddlAPCountry.Items.Clear();
                this.ddlAPCountry.DataBind();
                this.ddlAPCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlAPCountry.SelectedIndex = -1;
                this.ddlAPCountry.Enabled = false;
                this.ddlAPType.Items.Clear();
                this.ddlAPType.DataBind();
                this.ddlAPType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlAPType.SelectedIndex = -1;
                this.ddlAPType.Enabled = false;
                this.txtAPPhone.Text = "";
                this.txtAPPhone.Enabled = false;
                this.txtAPExtension.Text = "";
                this.txtAPExtension.Enabled = false;
                this.lblAPSuccess.Text = "";
                this.lblAPSuccess.CssClass = "lblSuccess";
                this.lblAPSuccess.Visible = false;
                this.ddlAPLocType.Enabled = true;
                this.btnAddPhone.Enabled = false;

                this.ddlAPRegion.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAPRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 rID = Convert.ToInt32(this.ddlAPRegion.SelectedValue);
                Dictionary<Int32, String> srList = DMG.getSubRegionList(rID);
                this.ddlAPSubReg.Items.Clear();
                this.ddlAPSubReg.DataSource = srList;
                this.ddlAPSubReg.DataTextField = "Value";
                this.ddlAPSubReg.DataValueField = "Key";
                this.ddlAPSubReg.DataBind();
                this.ddlAPSubReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlAPSubReg.SelectedIndex = -1;
                this.ddlAPSubReg.Enabled = true;
                this.ddlAPCountry.Items.Clear();
                this.ddlAPCountry.DataBind();
                this.ddlAPCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlAPCountry.SelectedIndex = -1;
                this.ddlAPCountry.Enabled = false;
                this.ddlAPType.Items.Clear();
                this.ddlAPType.DataBind();
                this.ddlAPType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlAPType.SelectedIndex = -1;
                this.ddlAPType.Enabled = false;
                this.txtAPPhone.Text = "";
                this.txtAPPhone.Enabled = false;
                this.txtAPExtension.Text = "";
                this.txtAPExtension.Enabled = false;
                this.lblAPSuccess.Text = "";
                this.lblAPSuccess.CssClass = "lblSuccess";
                this.lblAPSuccess.Visible = false;
                this.ddlAPLocType.Enabled = true;
                this.ddlAPLocType.Focus();
                this.btnAddPhone.Enabled = false;

                this.ddlAPSubReg.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAPSubReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlAPSubReg.SelectedValue);
                Dictionary<Int32, String> ctyList = DMG.getCountryList(srID);
                this.ddlAPCountry.Items.Clear();
                this.ddlAPCountry.DataSource = ctyList;
                this.ddlAPCountry.DataTextField = "Value";
                this.ddlAPCountry.DataValueField = "Key";
                this.ddlAPCountry.DataBind();
                this.ddlAPCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlAPCountry.SelectedIndex = -1;
                this.ddlAPCountry.Enabled = true;
                this.ddlAPType.Items.Clear();
                this.ddlAPType.DataBind();
                this.ddlAPType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlAPType.SelectedIndex = -1;
                this.ddlAPType.Enabled = false;
                this.txtAPPhone.Text = "";
                this.txtAPPhone.Enabled = false;
                this.txtAPExtension.Text = "";
                this.txtAPExtension.Enabled = false;
                this.lblAPSuccess.Text = "";
                this.lblAPSuccess.CssClass = "lblSuccess";
                this.lblAPSuccess.Visible = false;
                this.ddlAPLocType.Enabled = true;
                this.ddlAPLocType.Focus();
                this.btnAddPhone.Enabled = false;

                this.ddlAPCountry.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAPCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> dmgType = DMG.getDemogTypeList();
                this.ddlAPType.Items.Clear();
                this.ddlAPType.DataSource = dmgType;
                this.ddlAPType.DataTextField = "Value";
                this.ddlAPType.DataValueField = "Key";
                this.ddlAPType.DataBind();
                this.ddlAPType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlAPType.SelectedIndex = -1;
                this.ddlAPType.Enabled = true;
                this.txtAPPhone.Text = "";
                this.txtAPPhone.Enabled = false;
                this.txtAPExtension.Text = "";
                this.txtAPExtension.Enabled = false;
                this.lblAPSuccess.Text = "";
                this.lblAPSuccess.CssClass = "lblSuccess";
                this.lblAPSuccess.Visible = false;
                this.btnAddPhone.Enabled = false;

                this.ddlAPType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAPType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtAPPhone.Text = "";
                this.txtAPPhone.Enabled = true;
                this.txtAPExtension.Text = "";
                this.txtAPExtension.Enabled = true;
                this.lblAPSuccess.Text = "";
                this.lblAPSuccess.CssClass = "lblSuccess";
                this.lblAPSuccess.Visible = false;
                this.ddlAPLocType.Enabled = true;
                this.ddlAPLocType.Focus();
                this.btnAddPhone.Enabled = true;

                this.txtAPPhone.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddPhone_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iReply = 0, clAdd = 0, ctyID = 0, ctypID = 0, ctyDCode = 0;
                String phone = String.Empty, phExt = String.Empty, ctyCode = String.Empty, vPhNum = String.Empty;
                clAdd = Convert.ToInt32(this.ddlAPLocTitle.SelectedValue);
                ctyID = Convert.ToInt32(this.ddlAPCountry.SelectedValue);
                ctypID = Convert.ToInt32(this.ddlAPType.SelectedIndex);
                phone = Convert.ToString(this.txtAPPhone.Text);
                phExt = Convert.ToString(this.txtAPExtension.Text);
                ctyCode = DMG.getCountryLetterCode(ctyID);
                ctyDCode = DMG.getCountryDialCode(ctyID);
                if (String.IsNullOrEmpty(phone))
                {
                    btnAddPhoneCancel_Click(null, null);
                    this.lblAPSuccess.Text = "Phone Number can not be empty. Please provide a valid Phone Number";
                    this.phEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                    this.iPHMessage.Attributes["class"] = "icon fa fa-warning";
                    this.spnPH.InnerText = " Warning!";
                    this.phEnclosure.Visible = true;
                }
                else
                {
                    PH.PhoneNumberUtil phUtil = PH.PhoneNumberUtil.GetInstance();
                    try
                    {
                        phone = String.Format("{0}{1}", ctyDCode, phone);
                        PH.PhoneNumber ctyNumberProto = phUtil.Parse(phone, "US");
                        Boolean isValid = phUtil.IsValidNumber(ctyNumberProto);
                        vPhNum = phUtil.Format(ctyNumberProto, PH.PhoneNumberFormat.INTERNATIONAL);
                        if (isValid)
                        {
                            iReply = DMG.addClientPhoneToAddress(clAdd, vPhNum, ctypID, LoginName, domain);
                            if (iReply.Equals(1))
                            {
                                this.lblAPSuccess.Text = "Successfully inserted Phone Number for selected Location";
                                this.phEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                                this.iPHMessage.Attributes["class"] = "icon fa fa-check-circle";
                                this.spnPH.InnerText = " Success!";
                                this.phEnclosure.Visible = true;
                            }
                            else
                            {
                                this.lblAPSuccess.Text = "Unable to insert Phone Number for selected Location. Please try again.";
                                this.phEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                                this.iPHMessage.Attributes["class"] = "icon fa fa-warning";
                                this.spnPH.InnerText = " Warning!";
                                this.phEnclosure.Visible = true;
                            }
                        }
                        else
                        {
                            this.lblAPSuccess.Text = "Successfully inserted Phone Number for selected Location";
                            this.phEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                            this.iPHMessage.Attributes["class"] = "icon fa fa-check-circle";
                            this.spnPH.InnerText = " Success!";
                            this.phEnclosure.Visible = true;
                        }
                    }
                    catch (PH.NumberParseException ex)
                    { throw new System.Exception(ex.ToString()); }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddPhoneCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> dmgType = DMG.getDemogTypeList();
                this.ddlAPLocType.Items.Clear();
                this.ddlAPLocType.DataSource = dmgType;
                this.ddlAPLocType.DataTextField = "Value";
                this.ddlAPLocType.DataValueField = "Key";
                this.ddlAPLocType.DataBind();
                this.ddlAPLocType.Items.Insert(0, new ListItem("--- Select Location Type ---", "-1"));
                this.ddlAPLocType.SelectedIndex = -1;
                this.ddlAPLocTitle.Items.Clear();
                this.ddlAPLocTitle.DataBind();
                this.ddlAPLocTitle.Items.Insert(0, new ListItem("--- Select Location ---", "-1"));
                this.ddlAPLocTitle.SelectedIndex = -1;
                this.ddlAPLocTitle.Enabled = false;
                this.ddlAPRegion.Items.Clear();
                this.ddlAPRegion.DataBind();
                this.ddlAPRegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlAPRegion.SelectedIndex = -1;
                this.ddlAPRegion.Enabled = false;
                this.ddlAPSubReg.Items.Clear();
                this.ddlAPSubReg.DataBind();
                this.ddlAPSubReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlAPSubReg.SelectedIndex = -1;
                this.ddlAPSubReg.Enabled = false;
                this.ddlAPCountry.Items.Clear();
                this.ddlAPCountry.DataBind();
                this.ddlAPCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlAPCountry.SelectedIndex = -1;
                this.ddlAPCountry.Enabled = false;
                this.ddlAPType.Items.Clear();
                this.ddlAPType.DataBind();
                this.ddlAPType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlAPType.SelectedIndex = -1;
                this.ddlAPType.Enabled = false;
                this.txtAPPhone.Text = "";
                this.txtAPPhone.Enabled = false;
                this.txtAPExtension.Text = "";
                this.txtAPExtension.Enabled = false;
                this.phEnclosure.Visible = false;
                this.btnAddPhone.Enabled = false;

                this.ddlAPLocType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddPhoneDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnAddPhoneCancel_Click(null, null);
                System.Data.DataTable phTable = CLNT.getClientPhoneTable(ClientID);
                this.gvwPhoneList.DataSource = phTable;
                this.gvwPhoneList.DataBind();
                this.divPHList.Visible = true;
                this.divPHAdd.Visible = false;
                this.btnAddPhoneDone.Visible = false;

                this.gvwPhoneList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnClearPhone_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable phTable = CLNT.getClientPhoneTable(ClientID);
                this.gvwPhoneList.DataSource = phTable;
                this.gvwPhoneList.PageIndex = 0;
                this.gvwPhoneList.SelectedIndex = -1;
                this.gvwPhoneList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewPhone_Click(object sender, EventArgs e)
        {
            try
            {
                btnClearPhone_Click(null, null);
                this.divPHList.Visible = false;
                this.divPHAdd.Visible = true;
                this.btnClearPhone.Visible = false;
                this.btnAddPhoneDone.Visible = true;

                this.ddlAPLocType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwAddressList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable locTable = CLNT.getAddressTable(ClientID);
                this.gvwAddressList.SelectedIndex = -1;
                this.gvwAddressList.PageIndex = e.NewPageIndex;
                this.gvwAddressList.DataSource = locTable;
                this.gvwAddressList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}