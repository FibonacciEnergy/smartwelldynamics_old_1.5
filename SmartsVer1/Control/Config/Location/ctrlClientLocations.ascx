﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlClientLocations.ascx.cs" Inherits="SmartsVer1.Control.Config.Location.ctrlClientLocations" %>
<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>

<!-- Stylesheets -->
<link href="../../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../../Content/Bootstrap/Bootstrap/CSS/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="../../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript -->
<script src="../../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>

<!--- Page local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }

    .rotate {
        -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
        -moz-transform: rotate(90deg); /* Firefox */
        -ms-transform: rotate(90deg); /* IE 9 */
        transform: rotate(90deg); /* Standard syntax */
    }

    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper[data-stepper-steps="8"] li {
            width: 12%;
        }

        ol.stepper[data-stepper-steps="9"] li {
            width: 11%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }    
</style>
<!--- Page local script --->
<script type="text/javascript">
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("rotate");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").addClass("rotate");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("rotate");
        });
    });
</script>

<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">        
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>System Configuration</li>
                <li><a href="../../../Config/Locations/fecClientLoc_2A.aspx"> Client Locations</a></li>
            </ol>
        </div>
    </div>    
</div>

<!-- Page Content -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-pills">
                                <li class="nav-item active"><a data-target="#tabLoc" data-toggle="tab">Work Locations</a></li>
                                <li class="nav-item"><a data-target="#tabPh" data-toggle="tab">Phone Numbers</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tabLoc">
                                    <div id="divOPList" class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:UpdateProgress ID="uprgsLoc" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlLoc">
                                                <ProgressTemplate>
                                                    <div id="divLOCProcessing" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                                        z-index: 100;">
                                                        <asp:Image ID="imgLOCProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                                    </div>
                                                </ProgressTemplate>
                                            </asp:UpdateProgress>
                                            <asp:UpdatePanel runat="server" ID="upnlLoc">
                                                <ContentTemplate>
                                                    <div id="divJobTitle" class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <hr style="margin: auto;" />
                                                            <div class="bg-light-blue">
                                                                <div class="text-center">
                                                                    <h1 class="box-title">Work Location(s)</h1>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div id="divLOCList" runat="server">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <asp:GridView runat="server" ID="gvwAddressList" CssClass="mydatagrid" DataKeyNames="clntAddID"
                                                                    AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" EmptyDataText="No Locations available in the database!"
                                                                    ShowHeaderWhenEmpty="True" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header"
                                                                    PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnPageIndexChanging="gvwAddressList_PageIndexChanging">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="clntAddID" Visible="false" />
                                                                        <asp:BoundField DataField="dmgID" HeaderText="Type" />
                                                                        <asp:BoundField HeaderText="Title" DataField="title" />
                                                                        <asp:BoundField HeaderText="Street 1" DataField="st1" />
                                                                        <asp:BoundField HeaderText="Street 2" DataField="st2" />
                                                                        <asp:BoundField HeaderText="Street 3" DataField="st3" />
                                                                        <asp:BoundField HeaderText="City/Town" DataField="cityID" />
                                                                        <asp:BoundField HeaderText="County/District" DataField="cntyID" />
                                                                        <asp:BoundField HeaderText="State/Province" DataField="stID" />
                                                                        <asp:BoundField HeaderText="Zip/Postal Code" DataField="zipID" />
                                                                        <asp:BoundField HeaderText="Country/Nation" DataField="ctyID" />
                                                                        <asp:BoundField HeaderText="Sub-Region" DataField="sregID" />
                                                                        <asp:BoundField HeaderText="Region" DataField="regID" />
                                                                        <asp:BoundField HeaderText="Last Updated" DataField="uTime" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="input-group" runat="server" style="width: 100%;">
                                                                    <asp:Button runat="server" ID="btnAddNewLocation" CssClass="btn btn-info text-center" Width="10%" Text="Add Location" OnClick="btnAddNewLocation_Click" />
                                                                    <asp:Button runat="server" ID="btnAddLocationClear" CssClass="btn btn-warning text-center" Width="10%" Text="Clear Selection"
                                                                        Visible="false" OnClick="btnAddLocationClear_Click" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="divLOCAdd" runat="server" run="server" visible="false">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">Location Type</span>
                                                                        <asp:DropDownList runat="server" ID="ddlALType" CssClass="form-control" AutoPostBack="True" AppendDataBoundItems="True"
                                                                            OnSelectedIndexChanged="ddlALType_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select Location Type ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">Title</span>
                                                                        <asp:TextBox runat="server" ID="txtALTitle" CssClass="form-control" Text="" CausesValidation="true" Enabled="False"
                                                                            placeholder="Location Title (required)" />
                                                                        <asp:RequiredFieldValidator runat="server" ID="rfvALTitle" ControlToValidate="txtALTitle" Display="Dynamic" ErrorMessage="*"
                                                                            ForeColor="Maroon" SetFocusOnError="True" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">Major Region</span>
                                                                        <asp:DropDownList runat="server" ID="ddlALRegion" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="True"
                                                                            Enabled="False" OnSelectedIndexChanged="ddlALRegion_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select Major Geographic Region ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">Sub-Region</span>
                                                                        <asp:DropDownList runat="server" ID="ddlALSRegion" CssClass="form-control" AutoPostBack="true" Enabled="false"
                                                                            AppendDataBoundItems="True"
                                                                            OnSelectedIndexChanged="ddlALSRegion_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select Geographic Sub-Region ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">Country/Nation</span>
                                                                        <asp:DropDownList runat="server" ID="ddlALCountry" CssClass="form-control" AutoPostBack="true" Enabled="false"
                                                                            AppendDataBoundItems="true"
                                                                            OnSelectedIndexChanged="ddlALCountry_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select Country/Nation ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">State/Province</span>
                                                                        <asp:DropDownList runat="server" ID="ddlALState" CssClass="form-control" AutoPostBack="true" Enabled="false"
                                                                            AppendDataBoundItems="true"
                                                                            OnSelectedIndexChanged="ddlALState_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select State/Province ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">County/District</span>
                                                                        <asp:DropDownList runat="server" ID="ddlALCounty" CssClass="form-control" AutoPostBack="true" Enabled="false"
                                                                            AppendDataBoundItems="true"
                                                                            OnSelectedIndexChanged="ddlALCounty_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select County/District/Municipality ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">City/Town</span>
                                                                        <asp:DropDownList runat="server" ID="ddlALCity" CssClass="form-control" Enabled="false" AutoPostBack="true"
                                                                            AppendDataBoundItems="true"
                                                                            OnSelectedIndexChanged="ddlALCity_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select City/Town ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">Zip/Postalcode</span>
                                                                        <asp:DropDownList runat="server" ID="ddlALZip" CssClass="form-control" AutoPostBack="true" Enabled="false" AppendDataBoundItems="true"
                                                                            OnSelectedIndexChanged="ddlALZip_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0" Text="--- Select Zip/Postal Code ---" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">Street 1</span>
                                                                        <asp:TextBox runat="server" ID="txtALSt1" CssClass="form-control" Text="" CausesValidation="true" Enabled="False"
                                                                            placeholder="Street 1 (required)" />
                                                                        <asp:RequiredFieldValidator runat="server" ID="rfvALSt1" ControlToValidate="txtALSt1" Display="Dynamic" ErrorMessage="*"
                                                                            ForeColor="Maroon" SetFocusOnError="True" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">Street 2</span>
                                                                        <asp:TextBox runat="server" ID="txtALSt2" CssClass="form-control" Text="" Enabled="False" placeholder="Street 2 (optional)" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-light-blue">Street 3</span>
                                                                        <asp:TextBox runat="server" ID="txtALSt3" CssClass="form-control" Text="" Enabled="False" placeholder="Street 3 (optional)" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div id="locEnclosure" runat="server" visible="false">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                    <h4><i runat="server" id="iLOCMessage"><span runat="server" id="spnLOC"></span></i></h4>
                                                                    <asp:Label runat="server" ID="lblAddressSuccess" CssClass="lblSuccess" Text="" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="input-group" style="width: 100%;">
                                                                    <asp:Button runat="server" ID="btnAddAddress" CssClass="btn btn-success text-center" Width="10%" Text="Add New Location"
                                                                        Enabled="false" OnClick="btnAddAddress_Click" />
                                                                    <asp:Button runat="server" ID="btnAddAddressClear" CssClass="btn btn-warning text-center" Width="10%" Text="Clear Values"
                                                                        OnClick="btnAddAddressClear_Click" CausesValidation="False" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="input-group" style="width: 100%;">
                                                                    <asp:Button runat="server" ID="btnAddAddressDone" CssClass="btn btn-primary text-center" Width="10%" Text="Done" Visible="false"
                                                                        CausesValidation="False" OnClick="btnAddAddressDone_Click" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade box-body" id="tabPh">
                                    <div id="divPhList" class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:UpdateProgress ID="uprgrsPH" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlPhList">
                                                <ProgressTemplate>
                                                    <div id="divPHProcessing" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                                        z-index: 100;">
                                                        <asp:Image ID="imgPHProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                                    </div>
                                                </ProgressTemplate>
                                            </asp:UpdateProgress>
                                            <asp:UpdatePanel runat="server" ID="upnlPhList">
                                                <ContentTemplate>
                                                    <div id="div2" class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <hr style="margin: auto;" />
                                                            <div class="bg-light-blue">
                                                                <div class="text-center">
                                                                    <h1 class="box-title">Work Phone(s)</h1>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div id="divPHList" runat="server">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <asp:GridView runat="server" ID="gvwPhoneList" CssClass="mydatagrid" DataKeyNames="ptaID"
                                                                    AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" EmptyDataText="No Phone Numbers found."
                                                                    ShowHeaderWhenEmpty="True" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header"
                                                                    PagerStyle-CssClass="pager" RowStyle-CssClass="rows">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="ptaID" Visible="false" />
                                                                        <asp:BoundField DataField="dmgID" HeaderText="Address Type" />
                                                                        <asp:BoundField DataField="title" HeaderText="Address Title" />
                                                                        <asp:BoundField DataField="ptaValue" HeaderText="Phone Number" />
                                                                        <asp:BoundField DataField="ptaExtension" HeaderText="Extension" />
                                                                        <asp:BoundField DataField="ptaType" HeaderText="Type" />
                                                                        <asp:BoundField HeaderText="Last Updated" DataField="uTime" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="input-group" style="width: 100%;">
                                                                    <asp:Button runat="server" ID="btnAddNewPhone" CssClass="btn btn-info text-center" Width="10%" Text="Add New Phone" OnClick="btnAddNewPhone_Click" />
                                                                    <asp:Button runat="server" ID="btnClearPhone" CssClass="btn btn-warning text-center" Width="10%" Text="Clear Selection" Visible="false"
                                                                        OnClick="btnClearPhone_Click" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="divPHAdd" runat="server" visible="false">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="box-body">
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon bg-light-blue">Location Type</span>
                                                                            <asp:DropDownList runat="server" ID="ddlAPLocType" CssClass="form-control" AutoPostBack="True" AppendDataBoundItems="True"
                                                                                OnSelectedIndexChanged="ddlAPLocType_SelectedIndexChanged">
                                                                                <asp:ListItem Value="0" Text="--- Select Location Type ---" />
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon bg-light-blue">Location</span>
                                                                            <asp:DropDownList runat="server" ID="ddlAPLocTitle" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                                OnSelectedIndexChanged="ddlAPLocTitle_SelectedIndexChanged">
                                                                                <asp:ListItem Value="0" Text="--- Select Location ---" />
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon bg-light-blue">Geograhic Region</span>
                                                                            <asp:DropDownList runat="server" ID="ddlAPRegion" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="True"
                                                                                Enabled="False" OnSelectedIndexChanged="ddlAPRegion_SelectedIndexChanged">
                                                                                <asp:ListItem Value="0" Text="--- Select Major Geographic Region ---" />
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon bg-light-blue">Sub-Region</span>
                                                                            <asp:DropDownList runat="server" ID="ddlAPSubReg" CssClass="form-control" AutoPostBack="true" Enabled="false"
                                                                                AppendDataBoundItems="True"
                                                                                OnSelectedIndexChanged="ddlAPSubReg_SelectedIndexChanged">
                                                                                <asp:ListItem Value="0" Text="--- Select Geographic Sub-Region ---" />
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon bg-light-blue">Country/Nation</span>
                                                                            <asp:DropDownList runat="server" ID="ddlAPCountry" CssClass="form-control" AutoPostBack="true" Enabled="false"
                                                                                AppendDataBoundItems="true"
                                                                                OnSelectedIndexChanged="ddlAPCountry_SelectedIndexChanged">
                                                                                <asp:ListItem Value="0" Text="--- Select Country/Nation ---" />
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon bg-light-blue">Phone Type</span>
                                                                            <asp:DropDownList runat="server" ID="ddlAPType" CssClass="form-control" AutoPostBack="true" Enabled="false"
                                                                                AppendDataBoundItems="true"
                                                                                OnSelectedIndexChanged="ddlAPType_SelectedIndexChanged">
                                                                                <asp:ListItem Value="0" Text="--- Select Phone Type ---" />
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon bg-light-blue">Number</span>
                                                                            <asp:TextBox runat="server" ID="txtAPPhone" CssClass="form-control" Text="" CausesValidation="true" Enabled="False"
                                                                                placeholder="Phone Number (required)" />
                                                                            <asp:RequiredFieldValidator runat="server" ID="rfvAPPhone" ControlToValidate="txtAPPhone" Display="Dynamic" ErrorMessage="*"
                                                                                ForeColor="Maroon" SetFocusOnError="True" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon bg-light-blue">Extension</span>
                                                                            <asp:TextBox runat="server" ID="txtAPExtension" CssClass="form-control" Text="" Enabled="False" placeholder="Extension (optional)" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                        <div id="phEnclosure" runat="server" visible="false">
                                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                            <h4><i runat="server" id="iPHMessage"><span runat="server" id="spnPH"></span></i></h4>
                                                                            <asp:Label runat="server" ID="lblAPSuccess" Text="" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                        <div class="input-group" style="width: 100%;">
                                                                            <asp:Button runat="server" ID="btnAddPhone" CssClass="btn btn-success text-center" Width="10%" Text="Add Phone Number"
                                                                                Enabled="false" OnClick="btnAddPhone_Click" />
                                                                            <asp:Button runat="server" ID="btnAddPhoneCancel" CssClass="btn btn-warning text-center" Width="10%" Text="Clear Values"
                                                                                OnClick="btnAddPhoneCancel_Click" CausesValidation="False" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                        <div class="input-group" style="width: 100%;">
                                                                            <asp:Button runat="server" ID="btnAddPhoneDone" CssClass="btn btn-primary text-center" Width="10%" Text="Done" Visible="false"
                                                                                CausesValidation="False" OnClick="btnAddPhoneDone_Click" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
