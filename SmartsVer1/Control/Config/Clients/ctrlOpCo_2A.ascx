﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlOpCo_2A.ascx.cs" Inherits="SmartsVer1.Control.Config.Clients.ctrlOpCo_2A" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<!--- Stylesheet --->
<link href="../../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript -->
<script src="../../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<!-- Javascript for Footer -->
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>
<!--- Page local style --->
<style type="text/css" style="background-color: #d9534f; color: #FFFFFF">
    .rtAligned {
        text-align: right;
    }

    .swdMenu {
        color: #ffffff;
    }
</style>
<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">        
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>Configuration</li>
                <li><a href="../../../Config/Clients/fecOpCo_2A.aspx"> Operator Companies</a></li>
            </ol>
        </div>
    </div>    
</div>
<!-- Page Content -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul class="nav nav-pills nav-justified">
                <li class="active"><a data-toggle="pill" href="#opList">Operators List and Demographics</a></li>
                <li><a data-toggle="pill" href="#opUser">Operator's Reviewer Accounts</a></li>
            </ul>
            <div class="tab-content">
                <div id="opList" class="tab-pane fade in active">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h5 class="panel-title"><span>Operator Companies List</span></h5>
                        </div>
                        <div class="panel-body">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="min-height:450px;">
                                <asp:UpdateProgress ID="uprgsOpAdd" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlOpAdd">
                                    <ProgressTemplate>
                                        <div id="divOpAddProcessing" runat="server" class="updateProgress">
                                            <asp:Image ID="imgOpAddProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:UpdatePanel runat="server" ID="upnlOpAdd">
                                    <ContentTemplate>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="input-group" style="width: 100%;">
                                                        <asp:LinkButton runat="server" ID="btnOPCAddressListCancel" CssClass="btn btn-warning text-center" OnClick="btnOPCAddressListCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="divOPAddList" runat="server">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <asp:GridView runat="server" ID="gvwAddOPCList" AutoGenerateColumns="False" DataKeyNames="clntID" CssClass="mydatagrid" EmptyDataText="No Operator Companies found."
                                                            AllowPaging="True" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                            RowStyle-CssClass="rows" OnPageIndexChanging="gvwAddOPCList_PageIndexChanging" OnSelectedIndexChanged="gvwAddOPCList_SelectedIndexChanged">
                                                            <Columns>
                                                                <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                    <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                </asp:ButtonField>
                                                                <asp:BoundField DataField="clntID" Visible="False" />
                                                                <asp:BoundField DataField="clntName" HeaderText="Operator Company Name" />
                                                                <asp:BoundField DataField="clntRealm" HeaderText="Domain" />
                                                                <asp:BoundField DataField="clntNotes" HeaderText="Remarks" />
                                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <asp:GridView runat="server" ID="gvwOPCAddressList" AutoGenerateColumns="False" DataKeyNames="clntAddID" AllowPaging="True"
                                                            AllowSorting="True" CssClass="mydatagrid" EmptyDataText="No Address(es) found for selected Operator." ShowHeaderWhenEmpty="True"
                                                            Visible="False" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                            RowStyle-CssClass="rows">
                                                            <Columns>
                                                                <asp:BoundField DataField="clntAddID" Visible="false" />
                                                                <asp:BoundField DataField="dmgID" HeaderText="Type" />
                                                                <asp:BoundField DataField="title" HeaderText="Title" />
                                                                <asp:BoundField DataField="st1" HeaderText="Street 1" />
                                                                <asp:BoundField DataField="st2" HeaderText="Street 2" />
                                                                <asp:BoundField DataField="st3" HeaderText="Street 3" />
                                                                <asp:BoundField DataField="cityID" HeaderText="City/Town" />
                                                                <asp:BoundField DataField="cntyID" HeaderText="County/District" />
                                                                <asp:BoundField DataField="stID" HeaderText="State/Province" />
                                                                <asp:BoundField DataField="ctyID" HeaderText="Country/Nation" />
                                                                <asp:BoundField DataField="zipID" HeaderText="Zip/Postal Code" />
                                                                <asp:BoundField DataField="sregID" HeaderText="Sub-Region" />
                                                                <asp:BoundField DataField="regID" HeaderText="Region" />
                                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="opUser" class="tab-pane fade">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h5 class="panel-title"><span>Reviewer Accounts List</span></h5>
                        </div>
                        <div class="panel-body">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="min-height:450px;">
                                <asp:UpdateProgress ID="uprgrsOpRwr" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlOpRwr">
                                    <ProgressTemplate>
                                        <div id="divOpRwrProcessing" runat="server" class="updateProgress">
                                            <asp:Image ID="imgOpRwrProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:UpdatePanel runat="server" ID="upnlOpRwr">
                                    <ContentTemplate>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="input-group" style="width: 100%;">
                                                        <asp:LinkButton runat="server" ID="btnAddNCancel" CssClass="btn btn-warning text-center" OnClick="btnAddNCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="divUserList" runat="server">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <asp:GridView runat="server" ID="gvwUsersOPList" AutoGenerateColumns="False" DataKeyNames="clntID" CssClass="mydatagrid"
                                                            EmptyDataText="No Operator Companies found."
                                                            AllowPaging="True" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                            RowStyle-CssClass="rows" OnPageIndexChanging="gvwUsersOPList_PageIndexChanging" OnSelectedIndexChanged="gvwUsersOPList_SelectedIndexChanged">
                                                            <Columns>
                                                                <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                    <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                </asp:ButtonField>
                                                                <asp:BoundField DataField="clntID" Visible="False" />
                                                                <asp:BoundField DataField="clntName" HeaderText="Operator Company Name" />
                                                                <asp:BoundField DataField="clntRealm" HeaderText="Domain" />
                                                                <asp:BoundField DataField="clntNotes" HeaderText="Remarks" />
                                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <asp:GridView runat="server" ID="gvwOPCUsersList" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" CssClass="mydatagrid"
                                                            EmptyDataText="No Reviewer Accounts found for the selected Operator Company" ShowHeaderWhenEmpty="True" SelectedRowStyle-CssClass="selectedrow"
                                                            Enabled="False" Visible="false" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header"
                                                            PagerStyle-CssClass="pager" RowStyle-CssClass="rows" DataKeyNames="UserId" OnRowDataBound="gvwOPCUsersList_RowDataBound"
                                                            OnSelectedIndexChanged="gvwOPCUsersList_SelectedIndexChanged">
                                                            <Columns>
                                                                <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                    <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                </asp:ButtonField>
                                                                <asp:BoundField DataField="UserId" Visible="false" />
                                                                <asp:BoundField DataField="lastName" HeaderText="Last Name" />
                                                                <asp:BoundField DataField="firstName" HeaderText="First Name" />
                                                                <asp:BoundField DataField="userName" HeaderText="User Name" />
                                                                <asp:BoundField DataField="roleName" HeaderText="Role" />
                                                                <asp:TemplateField HeaderText="Enabled">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkUserApproved" runat="server" />
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Locked Out">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkDisabled" runat="server" />
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <asp:GridView runat="server" ID="gvwUserDetail" AutoGenerateColumns="False" DataKeyNames="uId" CssClass="mydatagrid"
                                                            Visible="False" AlternatingRowStyle-CssClass="AlternatingRowStyle" HeaderStyle-CssClass="header" RowStyle-CssClass="rows">
                                                            <Columns>
                                                                <asp:BoundField DataField="UserId" Visible="false" />
                                                                <asp:BoundField DataField="Email" HeaderText="Email" />
                                                                <asp:BoundField DataField="CreateDate" HeaderText="User Creation Date" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                                <asp:BoundField DataField="LastLoginDate" HeaderText="Last Login Date" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                                <asp:BoundField DataField="LastActivityDate" HeaderText="Last Activity Date" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <asp:GridView runat="server" ID="gvwUserAddressInfo" AutoGenerateColumns="False" DataKeyNames="usrID" CssClass="mydatagrid"
                                                            Visible="False" AlternatingRowStyle-CssClass="AlternatingRowStyle" HeaderStyle-CssClass="header" RowStyle-CssClass="rows">
                                                            <Columns>
                                                                <asp:BoundField DataField="usrID" Visible="false" />
                                                                <asp:BoundField DataField="addT" HeaderText="Address Type" />
                                                                <asp:BoundField DataField="dsg" HeaderText="Designation" />
                                                                <asp:BoundField DataField="Street1" HeaderText="Street 1" />
                                                                <asp:BoundField DataField="Street2" HeaderText="Street 2" />
                                                                <asp:BoundField DataField="Street3" HeaderText="Street 3" />
                                                                <asp:BoundField DataField="uCity" HeaderText="City/Town" />
                                                                <asp:BoundField DataField="uCounty" HeaderText="County/District" />
                                                                <asp:BoundField DataField="uState" HeaderText="State/Province" />
                                                                <asp:BoundField DataField="uCountry" HeaderText="Country" />
                                                                <asp:BoundField DataField="uZip" HeaderText="Zip/Postal Code" />
                                                                <asp:BoundField DataField="uSubReg" HeaderText="Sub-Region" />
                                                                <asp:BoundField DataField="uReg" HeaderText="Region" />
                                                                <asp:BoundField DataField="uPh" HeaderText="Phone" />
                                                                <asp:BoundField DataField="uPhT" HeaderText="Type" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
