﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.DirectoryServices;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Net.Mail;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using PH = PhoneNumbers;
using CLR = System.Drawing.Color;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using CTRT = SmartsVer1.Helpers.AccountHelpers.CreateContract;
using SRV = SmartsVer1.Helpers.DemogHelpers.ServicesConfig;
using SL = SmartsVer1.Helpers.SalesHelpers.Sales;

namespace SmartsVer1.Control.Config.Clients
{
    public partial class ctrlClients : System.Web.UI.UserControl
    {
        Int32 ownAcc = 0, prtAcc = 0, clnAcc = 0, opAcc = 0, ClientID = -99;
        const Double megaBytes = 0.095367431640625;
        String LoginName, username, domain, GetClientDBString;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);
                UP usrP = UP.GetUserProfile(LoginName);
                ClientID = Convert.ToInt32(usrP.clntID);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
                //DataSources for controls
                Dictionary<Int32, String> clientTypes = CLNT.getClientTypelist();
                Dictionary<Int32, String> mapClntList = CLNT.getDBMapClient();
                Dictionary<Int32, String> gclients = CLNT.getGlobalClientsList();
                Dictionary<Int32, String> ctrtType = CTRT.getContractTypeList();
                Dictionary<Int32, String> ctrtList = CTRT.getContractsList();
                this.txtStartCalendar.Text = DateTime.Now.ToString("yyyy-MM-dd");
                this.txtEndCalendar.Text = DateTime.Now.ToString("yyyy-MM-dd");
                //Assigning datasources to Controls
                this.ddlClientType.Items.Clear();
                this.ddlClientType.DataSource = clientTypes;
                this.ddlClientType.DataTextField = "Value";
                this.ddlClientType.DataValueField = "Key";
                this.ddlClientType.DataBind();
                this.ddlClientType.Items.Insert(0, new ListItem("--- Select Client Type ---", "-1"));
                this.ddlClientType.SelectedIndex = -1;
                this.ddlAddClientType.Items.Clear();
                this.ddlAddClientType.DataSource = clientTypes;
                this.ddlAddClientType.DataTextField = "Value";
                this.ddlAddClientType.DataValueField = "Key";
                this.ddlAddClientType.DataBind();
                this.ddlAddClientType.Items.Insert(0, new ListItem("--- Select Client Type ---", "-1"));
                this.ddlAddClientType.SelectedIndex = -1;
                this.ddlPClientType.Items.Clear();
                this.ddlPClientType.DataSource = clientTypes;
                this.ddlPClientType.DataTextField = "Value";
                this.ddlPClientType.DataValueField = "Key";
                this.ddlPClientType.DataBind();
                this.ddlPClientType.Items.Insert(0, new ListItem("--- Select Client Type ---", "-1"));
                this.ddlPClientType.SelectedIndex = -1;
                this.ddlAPNumberClientType.Items.Clear();
                this.ddlAPNumberClientType.DataSource = clientTypes;
                this.ddlAPNumberClientType.DataTextField = "Value";
                this.ddlAPNumberClientType.DataValueField = "Key";
                this.ddlAPNumberClientType.DataBind();
                this.ddlAPNumberClientType.Items.Insert(0, new ListItem("--- Select Client Type ---", "-1"));
                this.ddlAPNumberClientType.SelectedIndex = -1;
                this.ddlMappingClientType.Items.Clear();
                this.ddlMappingClientType.DataSource = clientTypes;
                this.ddlMappingClientType.DataTextField = "Value";
                this.ddlMappingClientType.DataValueField = "Key";
                this.ddlMappingClientType.DataBind();
                this.ddlMappingClientType.Items.Insert(0, new ListItem("--- Select Client Type ---", "-1"));
                this.ddlMappingClientType.SelectedIndex = -1;
                this.ddlAddMappingClient.Items.Clear();
                this.ddlAddMappingClient.DataSource = mapClntList;
                this.ddlAddMappingClient.DataTextField = "Value";
                this.ddlAddMappingClient.DataValueField = "Key";
                this.ddlAddMappingClient.DataBind();
                this.ddlAddMappingClient.Items.Insert(0, new ListItem("--- Select System Client ---", "-1"));
                this.ddlAddMappingClient.SelectedIndex = -1;
                this.ddlAddressClientType.Items.Clear();
                this.ddlAddressClientType.DataSource = clientTypes;
                this.ddlAddressClientType.DataTextField = "Value";
                this.ddlAddressClientType.DataValueField = "Key";
                this.ddlAddressClientType.DataBind();
                this.ddlAddressClientType.Items.Insert(0, new ListItem("--- Select Client Type ---", "-1"));
                this.ddlAddressClientType.SelectedIndex = -1;
                this.ddlAddressNewClientType.Items.Clear();
                this.ddlAddressNewClientType.DataSource = clientTypes;
                this.ddlAddressNewClientType.DataTextField = "Value";
                this.ddlAddressNewClientType.DataValueField = "Key";
                this.ddlAddressNewClientType.DataBind();
                this.ddlAddressNewClientType.Items.Insert(0, new ListItem("--- Select Client Type ---", "-1"));
                this.ddlAddressNewClientType.SelectedIndex = -1;
                this.ddlClientCo.Items.Clear();
                this.ddlClientCo.DataSource = gclients;
                this.ddlClientCo.DataTextField = "Value";
                this.ddlClientCo.DataValueField = "Key";
                this.ddlClientCo.DataBind();
                this.ddlClientCo.Items.Insert(0, new ListItem("--- Select Contract Client ---", "-1"));
                this.ddlClientCo.SelectedIndex = -1;
                this.ddlEditCttType.Items.Clear();
                this.ddlEditCttType.DataSource = ctrtType;
                this.ddlEditCttType.DataTextField = "Value";
                this.ddlEditCttType.DataValueField = "Key";
                this.ddlEditCttType.DataBind();
                this.ddlEditCttType.Items.Insert(0, new ListItem("--- Select Contract Type ---", "-1"));
                this.ddlEditCttType.SelectedIndex = -1;
                this.ddlAddContractType.Items.Clear();
                this.ddlAddContractType.DataSource = ctrtType;
                this.ddlAddContractType.DataTextField = "Value";
                this.ddlAddContractType.DataValueField = "Key";
                this.ddlAddContractType.DataBind();
                this.ddlAddContractType.Items.Insert(0, new ListItem("--- Select Contract Type ---", "-1"));
                this.ddlAddContractType.SelectedIndex = -1;
                this.ddlContractList.Items.Clear();
                this.ddlContractList.DataSource = ctrtList;
                this.ddlContractList.DataTextField = "Value";
                this.ddlContractList.DataValueField = "Key";
                this.ddlContractList.DataBind();
                this.ddlContractList.Items.Insert(0, new ListItem("--- Select Client Contract ---", "-1"));
                this.ddlContractList.SelectedIndex = -1;
                this.ddlAPContract.Items.Clear();
                this.ddlAPContract.DataSource = ctrtList;
                this.ddlAPContract.DataTextField = "Value";
                this.ddlAPContract.DataValueField = "Key";
                this.ddlAPContract.DataBind();
                this.ddlAPContract.Items.Insert(0, new ListItem("--- Select Client Contract ---", "-1"));
                this.ddlAPContract.SelectedIndex = -1;
                this.ddlImageClientType.Items.Clear();
                this.ddlImageClientType.DataSource = clientTypes;
                this.ddlImageClientType.DataTextField = "Value";
                this.ddlImageClientType.DataValueField = "Key";
                this.ddlImageClientType.DataBind();
                this.ddlImageClientType.Items.Insert(0, new ListItem("--- Select Type of System Client ---", "-1"));
                this.ddlImageClientType.SelectedIndex = -1;
                this.ddlAddLogoClientType.Items.Clear();
                this.ddlAddLogoClientType.DataSource = clientTypes;
                this.ddlAddLogoClientType.DataTextField = "Value";
                this.ddlAddLogoClientType.DataValueField = "Key";
                this.ddlAddLogoClientType.DataBind();
                this.ddlAddLogoClientType.Items.Insert(0, new ListItem("--- Select Type of System Client ---", "-1"));
                this.ddlAddLogoClientType.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnClientAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.tblClientList.Visible = false;
                this.tblClientAdd.Visible = true;
                this.ddlAddClientType.Items.Clear();
                this.ddlAddClientType.DataBind();
                this.ddlAddClientType.Items.Insert(0, new ListItem("--- Select New Client Type ---", "-1"));
                this.ddlAddClientType.SelectedIndex = -1;
                this.gvwClientList.DataBind();
                this.gvwClientList.Visible = false;
                this.btnClientDone.Visible = true;

                this.ddlAddClientType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
        protected void btnClientListCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.ddlClientType.Items.Clear();
                this.ddlClientType.DataBind();
                this.ddlClientType.Items.Insert(0, new ListItem("--- Select Type of System Client ---", "-1"));
                this.ddlClientType.SelectedIndex = -1;
                this.gvwClientList.Visible = false;
                this.btnClientListCancel.Visible = false;

                this.ddlClientType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAddClientType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.lblClientSuccess.Text = "";
                this.lblClientSuccess.Visible = false;
                this.txtClntName.Text = "";
                this.txtClntName.Enabled = true;
                this.txtClntNotes.Text = "";
                this.txtClntNotes.Enabled = true;
                this.txtclntRealm.Text = "";
                this.txtclntRealm.Enabled = true;
                this.txtclntTaxReg.Text = "";
                this.txtclntTaxReg.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewClient_Click(object sender, EventArgs e)
        {
            try
            {
                String clntName = null, clntNameLower = null, clntTax = null, clntNotes = null, clntRealm = null;
                Int32 clntType = 0;

                clntType = Convert.ToInt32(ddlAddClientType.SelectedValue);
                clntName = Convert.ToString(txtClntName.Text);
                clntNotes = Convert.ToString(txtClntNotes.Text);
                clntRealm = Convert.ToString(txtclntRealm.Text);
                clntTax = Convert.ToString(txtclntTaxReg.Text);

                TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                clntRealm = ti.ToTitleCase(clntRealm);
                TextInfo tiClnt = new CultureInfo("en-US", false).TextInfo;
                clntName = tiClnt.ToTitleCase(clntName);
                clntNameLower = tiClnt.ToLower(clntName);
                if (String.IsNullOrEmpty(clntName) || String.IsNullOrEmpty(clntRealm) || String.IsNullOrEmpty(clntTax))
                {
                    this.tblClientAdd.Visible = true;
                    this.ddlAddClientType.Items.Clear();
                    this.ddlAddClientType.DataBind();
                    this.ddlAddClientType.Items.Insert(0, new ListItem("--- Select New Client Type ---", "-1"));
                    this.ddlAddClientType.SelectedIndex = -1;
                    this.txtClntName.Text = "";
                    this.txtClntNotes.Text = "";
                    this.txtclntRealm.Text = "";
                    this.txtclntTaxReg.Text = "";
                    this.lblClientSuccess.ForeColor = CLR.Maroon;
                    this.lblClientSuccess.Font.Size = 14;
                    this.lblClientSuccess.Text = "Missing Values detected. Please provide all values before inserting Client.";
                    this.lblClientSuccess.Visible = true;

                    this.ddlAddClientType.Focus();
                }
                else
                {
                    if (clntType.Equals(1)) //Services Client
                    {
                        clnAcc = Convert.ToInt32(CLNT.clientCreate(clntName, clntTax, clntNotes, clntRealm));
                        if (clnAcc.Equals(1)) //Account created successfully
                        {
                            //this.divClientAdd.Visible = false;
                            btnAddNewClientCancel_Click(null, null);
                            this.lblClientSuccess.ForeColor = CLR.Green;
                            this.lblClientSuccess.Font.Size = 14;
                            this.lblClientSuccess.Text = "Successfully created Client Account.";
                            this.lblClientSuccess.Visible = true;
                        }
                        else
                        {
                            this.tblClientAdd.Visible = false;
                            this.lblClientSuccess.ForeColor = CLR.Maroon;
                            this.lblClientSuccess.Font.Size = 14;
                            this.lblClientSuccess.Text = "Error Creating Client Account. Please try again.";
                            this.lblClientSuccess.Visible = true;

                            this.btnClientAdd.Focus();
                        }
                    }
                    else if (clntType.Equals(2)) //Partner
                    {
                        prtAcc = Convert.ToInt32(CLNT.clientCreate(clntName, clntTax, clntNotes, clntRealm));
                        if (prtAcc.Equals(1)) //Account created successfully
                        {
                            this.tblClientAdd.Visible = false;
                            this.lblClientSuccess.ForeColor = CLR.Green;
                            this.lblClientSuccess.Font.Size = 14;
                            this.lblClientSuccess.Text = "Successfully created Partner Account.";
                            this.lblClientSuccess.Visible = true;

                            this.btnClientAdd.Focus();
                        }
                        else
                        {
                            this.tblClientAdd.Visible = false;
                            this.lblClientSuccess.ForeColor = CLR.Maroon;
                            this.lblClientSuccess.Font.Size = 14;
                            this.lblClientSuccess.Text = "Error Creating Partner Account. Please try again.";
                            this.lblClientSuccess.Visible = true;

                            this.btnClientAdd.Focus();
                        }
                    }
                    else if (clntType.Equals(3)) //Owner
                    {
                        ownAcc = Convert.ToInt32(CLNT.ownerCreate(clntName, clntTax, clntNotes, clntRealm));
                        if (ownAcc.Equals(1)) //Account created successfully
                        {
                            this.tblClientAdd.Visible = false;
                            this.lblClientSuccess.ForeColor = CLR.Green;
                            this.lblClientSuccess.Font.Size = 14;
                            this.lblClientSuccess.Text = "Successfully created Owner Account.";
                            this.lblClientSuccess.Visible = true;

                            this.btnClientAdd.Focus();
                        }
                        else
                        {
                            this.tblClientAdd.Visible = false;
                            this.lblClientSuccess.ForeColor = CLR.Maroon;
                            this.lblClientSuccess.Font.Size = 14;
                            this.lblClientSuccess.Text = "Error Creating Owner Account. Please try again.";
                            this.lblClientSuccess.Visible = true;

                            this.btnClientAdd.Focus();
                        }
                    }
                    else // Operator
                    {
                        opAcc = Convert.ToInt32(CLNT.opetrCreate(clntName, clntTax, clntNotes, clntRealm));
                        if (opAcc.Equals(1)) //Account created successfully
                        {
                            this.lblClientSuccess.ForeColor = CLR.Green;
                            this.lblClientSuccess.Font.Size = 14;
                            this.lblClientSuccess.Text = "Successfully created Operator Account.";
                            this.lblClientSuccess.Visible = true;

                            this.btnClientAdd.Focus();
                        }
                        else
                        {
                            this.lblClientSuccess.ForeColor = CLR.Maroon;
                            this.lblClientSuccess.Font.Size = 14;
                            this.lblClientSuccess.Text = "Error Creating Operator Account. Please try again.";
                            this.lblClientSuccess.Visible = true;

                            this.btnClientAdd.Focus();
                        }
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewClientCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.ddlAddClientType.Items.Clear();
                this.ddlAddClientType.DataBind();
                this.ddlAddClientType.Items.Insert(0, new ListItem("--- Select New Client Type ---", "-1"));
                this.ddlAddClientType.SelectedIndex = -1;
                this.txtClntName.Text = "";
                this.txtClntName.Enabled = false;
                this.txtClntNotes.Text = "";
                this.txtClntNotes.Enabled = false;
                this.txtclntRealm.Text = "";
                this.txtclntRealm.Enabled = false;
                this.txtclntTaxReg.Text = "";
                this.txtclntTaxReg.Enabled = false;
                this.lblClientSuccess.Text = "";
                this.lblClientSuccess.CssClass = "lblSuccess";
                this.lblClientSuccess.Visible = false;

                this.ddlAddClientType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwClientList_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlMappingClientType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 typeID = Convert.ToInt32(this.ddlMappingClientType.SelectedValue);
                Dictionary<Int32, String> rList = CLNT.getClientRealmList(typeID);
                this.ddlMappingClientRealm.Items.Clear();
                this.ddlMappingClientRealm.DataSource = rList;
                this.ddlMappingClientRealm.DataTextField = "Value";
                this.ddlMappingClientRealm.DataValueField = "Key";
                this.ddlMappingClientRealm.DataBind();
                this.ddlMappingClientRealm.Items.Insert(0, new ListItem("--- Select Client Realm ---", "-1"));
                this.ddlMappingClientRealm.SelectedIndex = -1;
                this.ddlMappingClientRealm.Enabled = true;
                this.lblMappedDB.Text = null;
                this.lblMappedDB.CssClass = "lblSuccess";
                this.lblMappedDB.Visible = false;
                this.btnMappingCancel.Visible = true;

                this.ddlMappingClientRealm.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlMappingClientRealm_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                String realm = Convert.ToString(this.ddlMappingClientRealm.SelectedItem);
                String dbName = CLNT.getMappedDBName(realm);
                if (!dbName.Equals(String.Empty))
                {
                    this.lblMappedDB.Text = dbName;
                    this.lblMappedDB.ForeColor = CLR.Green;
                    this.lblMappedDB.Visible = true;

                    this.lblMappedDB.Focus();
                }
                else
                {
                    this.lblMappedDB.Text = "!!! The above realm is currently NOT MAPPED to any database !!! ";
                    this.lblMappedDB.ForeColor = CLR.Maroon;
                    this.lblMappedDB.Visible = true;

                    this.lblMappedDB.Focus();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnMappingAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.tblClientDBList.Visible = false;
                btnMappingCancel_Click(null, null);
                this.tblClientDBAdd.Visible = true;
                this.lblMappingSuccess.Text = "";
                this.lblMappingSuccess.Visible = false;
                this.lblMappedDB.Text = "";
                this.lblMappedDB.Visible = false;
                this.btnAddNewMappingDone.Visible = true;
                this.btnMappingCancel.Visible = false;
                this.ddlAddMappingClient.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnMappingCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.ddlMappingClientType.SelectedIndex = -1;

                this.ddlMappingClientRealm.Items.Clear();
                this.ddlMappingClientRealm.DataBind();
                this.ddlMappingClientRealm.Items.Insert(0, new ListItem("--- Select Client Realm ---", "-1"));
                this.ddlMappingClientRealm.SelectedIndex = -1;
                this.ddlMappingClientRealm.Enabled = false;
                this.lblMappedDB.Text = "";
                this.lblMappedDB.Visible = false;

                this.tblClientDBAdd.Visible = false;
                this.lblMappingSuccess.Text = "";
                this.lblMappingSuccess.CssClass = "lblSuccess";
                this.lblMappingSuccess.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAddMappingClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> dbList = CLNT.getClientDBNamesList();
                Int32 cID = Convert.ToInt32(this.ddlAddMappingClient.SelectedValue);
                String clntRealm = CLNT.getClientRealm(cID);
                this.txtClientRealm.Text = clntRealm;
                this.txtClientRealm.Enabled = false;
                this.ddlDatabaseNameList.Items.Clear();
                this.ddlDatabaseNameList.DataSource = dbList;
                this.ddlDatabaseNameList.DataTextField = "Value";
                this.ddlDatabaseNameList.DataValueField = "Key";
                this.ddlDatabaseNameList.DataBind();
                this.ddlDatabaseNameList.Items.Insert(0, new ListItem("--- Select Database Name ---", "-1"));
                this.ddlDatabaseNameList.SelectedIndex = -1;
                this.ddlDatabaseNameList.Enabled = true;

                this.ddlDatabaseNameList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlDatabaseNameList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnAddNewMapping.Enabled = true;
                this.btnAddContract.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddContract_Click(object sender, EventArgs e)
        {
            try
            {
                this.tblContract.Visible = false;
                this.tblAddContract.Visible = true;
                this.btnAddNewContractDone.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnEditContract_Click(object sender, EventArgs e)
        {
            try
            {
                this.tblContract.Visible = false;
                this.tblEditContract.Visible = true;
                this.tblAddContract.Visible = false;
                this.btnAddNewContractDone.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnContractListCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.lblContractSuccess.Text = "";
                this.lblContractSuccess.CssClass = "genLabel";
                this.lblContractSuccess.Visible = false;
                this.tblAddContract.Visible = false;

                this.ddlClientCo.Items.Clear();
                this.ddlClientCo.DataBind();
                this.ddlClientCo.Items.Insert(0, new ListItem("--- Select Contract Client ---", "-1"));
                this.ddlClientCo.SelectedIndex = -1;
                this.gvwContractList.DataBind();
                this.gvwContractList.Visible = false;
                this.btnEditContract.Visible = false;
                this.btnContractListCancel.Visible = false;

                this.ddlClientCo.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnEditContractUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                Int32 providerID = Convert.ToInt32(this.ddlEditCttProvider.SelectedValue);
                Int32 partnerID = Convert.ToInt32(this.ddlEditCttPartner.SelectedValue);
                Int32 contractID = Convert.ToInt32(this.ddlEditCttList.SelectedValue);
                Int32 statusID = Convert.ToInt32(this.ddlEditCttStatus.SelectedValue);
                iResult = CTRT.updateContractStatus(providerID, partnerID, contractID, statusID, LoginName);
                if (iResult.Equals(-1))
                {
                    this.lblEditContractSuccess.ForeColor = Color.Maroon;
                    this.lblEditContractSuccess.Text = "Please select a new Contract Status";
                    this.lblEditContractSuccess.Visible = true;
                    Dictionary<Int32, String> tplist = CTRT.getGlobalContractStatus();
                    this.ddlEditCttStatus.Items.Clear();
                    this.ddlEditCttStatus.DataSource = tplist;
                    this.ddlEditCttStatus.DataTextField = "Value";
                    this.ddlEditCttStatus.DataValueField = "Key";
                    this.ddlEditCttStatus.DataBind();
                    this.ddlEditCttStatus.Items.Insert(0, new ListItem("--- Select Contract Status ---", "-1"));
                    this.ddlEditCttStatus.SelectedIndex = -1;
                    this.ddlEditCttStatus.Enabled = true;

                    this.ddlEditCttStatus.Focus();
                }
                else if (iResult.Equals(-2))
                {
                    this.btnEditContractCancel_Click(null, null);
                    this.lblEditContractSuccess.ForeColor = Color.Maroon;
                    this.lblEditContractSuccess.Text = "Not Authorized for update Contract Status";
                    this.lblEditContractSuccess.Visible = true;
                }
                else if (iResult.Equals(-3))
                {
                    this.btnEditContractCancel_Click(null, null);
                    this.lblEditContractSuccess.ForeColor = Color.Maroon;
                    this.lblEditContractSuccess.Text = "Unable to update Contract Status. Please try again";
                    this.lblEditContractSuccess.Visible = true;
                }
                else
                {
                    this.btnEditContractCancel_Click(null, null);
                    this.lblEditContractSuccess.ForeColor = Color.Green;
                    this.lblEditContractSuccess.Text = "Successfully updated Contract Status.";
                    this.lblEditContractSuccess.Visible = true;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnEditContractCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> ctrtType = CTRT.getContractTypeList();
                this.ddlEditCttType.Items.Clear();
                this.ddlEditCttType.DataSource = ctrtType;
                this.ddlEditCttType.DataTextField = "Value";
                this.ddlEditCttType.DataValueField = "Key";
                this.ddlEditCttType.DataBind();
                this.ddlEditCttType.Items.Insert(0, new ListItem("--- Select Contract Type ---", "-1"));
                this.ddlEditCttType.SelectedIndex = -1;
                this.ddlEditCttProvider.Items.Clear();
                this.ddlEditCttProvider.DataBind();
                this.ddlEditCttProvider.Items.Insert(0, new ListItem("--- Select Contract Provider ---", "-1"));
                this.ddlEditCttProvider.SelectedIndex = -1;
                this.ddlEditCttProvider.Enabled = false;
                this.ddlEditCttPartner.Items.Clear();
                this.ddlEditCttPartner.DataBind();
                this.ddlEditCttPartner.Items.Insert(0, new ListItem("--- Select Contract Partner ---", "-1"));
                this.ddlEditCttPartner.SelectedIndex = -1;
                this.ddlEditCttPartner.Enabled = false;
                this.ddlEditCttClient.Items.Clear();
                this.ddlEditCttClient.DataBind();
                this.ddlEditCttClient.Items.Insert(0, new ListItem("--- Select Contract Client ---", "-1"));
                this.ddlEditCttClient.SelectedIndex = -1;
                this.ddlEditCttClient.Enabled = false;
                this.ddlEditCttList.Items.Clear();
                this.ddlEditCttList.DataBind();
                this.ddlEditCttList.Items.Insert(0, new ListItem("--- Select Contract ---", "-1"));
                this.ddlEditCttList.SelectedIndex = -1;
                this.ddlEditCttList.Enabled = false;
                this.ddlEditCttStatus.Items.Clear();
                this.ddlEditCttStatus.DataBind();
                this.ddlEditCttStatus.Items.Insert(0, new ListItem("--- Select Contract Status ---", "-1"));
                this.ddlEditCttStatus.SelectedIndex = -1;
                this.ddlEditCttStatus.Enabled = false;
                this.lblEditContractSuccess.Text = "";
                this.lblEditContractSuccess.Visible = false;

                this.ddlEditCttType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlEditCttType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctID = Convert.ToInt32(this.ddlAddContractType.SelectedValue);
                if (ctID.Equals(1)) //Demo
                {
                    Dictionary<Int32, String> pList = CLNT.getDemoContractProviderList();
                    Dictionary<Int32, String> dcList = CLNT.getDemoContractClientList();
                    this.ddlEditCttProvider.Items.Clear();
                    this.ddlEditCttProvider.DataSource = pList;
                    this.ddlEditCttProvider.DataTextField = "Value";
                    this.ddlEditCttProvider.DataValueField = "Key";
                    this.ddlEditCttProvider.DataBind();
                    this.ddlEditCttProvider.Items.Insert(0, new ListItem("--- Select Contract Provider ---", "-1"));
                    this.ddlEditCttProvider.SelectedIndex = -1;
                    this.ddlEditCttProvider.Enabled = true;
                    this.ddlEditCttPartner.Items.Clear();
                    this.ddlEditCttPartner.DataSource = pList;
                    this.ddlEditCttPartner.DataTextField = "Value";
                    this.ddlEditCttPartner.DataValueField = "Key";
                    this.ddlEditCttPartner.DataBind();
                    this.ddlEditCttPartner.Items.Insert(0, new ListItem("--- Select Contract Provider ---", "-1"));
                    this.ddlEditCttPartner.SelectedIndex = -1;
                    this.ddlEditCttPartner.Enabled = false;
                    this.ddlEditCttClient.Items.Clear();
                    this.ddlEditCttClient.DataSource = dcList;
                    this.ddlEditCttClient.DataTextField = "Value";
                    this.ddlEditCttClient.DataValueField = "Key";
                    this.ddlEditCttClient.DataBind();
                    this.ddlEditCttClient.Items.Insert(0, new ListItem("--- Select Contract Client ---", "-1"));
                    this.ddlEditCttClient.SelectedIndex = -1;
                    this.ddlEditCttClient.Enabled = true;
                    this.ddlEditCttList.Items.Clear();
                    this.ddlEditCttList.DataBind();
                    this.ddlEditCttList.Items.Insert(0, new ListItem("--- Select Contract ---", "-1"));
                    this.ddlEditCttList.SelectedIndex = -1;
                    this.ddlEditCttList.Enabled = false;
                    this.ddlEditCttStatus.Items.Clear();
                    this.ddlEditCttStatus.DataBind();
                    this.ddlEditCttStatus.Items.Insert(0, new ListItem("--- Select Contract Status ---", "-1"));
                    this.ddlEditCttStatus.SelectedIndex = -1;
                    this.ddlEditCttStatus.Enabled = false;

                    this.ddlEditCttProvider.Focus();
                }
                else if (ctID.Equals(2)) //Partnership
                {
                    //Provider is FE and Partner Null
                    Dictionary<Int32, String> pList = CLNT.getPartnershipContractProviderList();
                    Dictionary<Int32, String> cpList = CLNT.getPartnerContractClientList();
                    this.ddlEditCttProvider.Items.Clear();
                    this.ddlEditCttProvider.DataSource = pList;
                    this.ddlEditCttProvider.DataTextField = "Value";
                    this.ddlEditCttProvider.DataValueField = "Key";
                    this.ddlEditCttProvider.DataBind();
                    this.ddlEditCttProvider.Items.Insert(0, new ListItem("--- Select Contract Provider ---", "-1"));
                    this.ddlEditCttProvider.SelectedIndex = 1;
                    this.ddlEditCttProvider.Enabled = false;
                    this.ddlEditCttPartner.Items.Clear();
                    this.ddlEditCttPartner.DataBind();
                    this.ddlEditCttPartner.Items.Insert(0, new ListItem("--- Unavailable ---", "-1"));
                    this.ddlEditCttPartner.SelectedIndex = -1;
                    this.ddlEditCttPartner.Enabled = false;
                    this.ddlEditCttClient.Items.Clear();
                    this.ddlEditCttClient.DataSource = cpList;
                    this.ddlEditCttClient.DataTextField = "Value";
                    this.ddlEditCttClient.DataValueField = "Key";
                    this.ddlEditCttClient.DataBind();
                    this.ddlEditCttClient.Items.Insert(0, new ListItem("--- Select Contract Client ---", "-1"));
                    this.ddlEditCttClient.SelectedIndex = -1;
                    this.ddlEditCttClient.Enabled = true;
                    this.ddlEditCttList.Items.Clear();
                    this.ddlEditCttList.DataBind();
                    this.ddlEditCttList.Items.Insert(0, new ListItem("--- Select Contract ---", "-1"));
                    this.ddlEditCttList.SelectedIndex = -1;
                    this.ddlEditCttList.Enabled = false;
                    this.ddlEditCttStatus.Items.Clear();
                    this.ddlEditCttStatus.DataBind();
                    this.ddlEditCttStatus.Items.Insert(0, new ListItem("--- Select Contract Status ---", "-1"));
                    this.ddlEditCttStatus.SelectedIndex = -1;
                    this.ddlEditCttStatus.Enabled = false;

                    this.ddlEditCttClient.Focus();
                }
                else //Services
                {
                    Dictionary<Int32, String> cpList = CLNT.getClientContractProviderList();
                    Dictionary<Int32, String> prtList = CLNT.getContractPartnerList();
                    this.ddlEditCttProvider.Items.Clear();
                    this.ddlEditCttProvider.DataSource = cpList;
                    this.ddlEditCttProvider.DataTextField = "Value";
                    this.ddlEditCttProvider.DataValueField = "Key";
                    this.ddlEditCttProvider.DataBind();
                    this.ddlEditCttProvider.Items.Insert(0, new ListItem("--- Select Contract Provider ---", "-1"));
                    this.ddlEditCttProvider.SelectedIndex = 1;
                    this.ddlEditCttProvider.Enabled = true;
                    this.ddlEditCttPartner.Items.Clear();
                    this.ddlEditCttPartner.DataSource = prtList;
                    this.ddlEditCttPartner.DataTextField = "Value";
                    this.ddlEditCttPartner.DataValueField = "Key";
                    this.ddlEditCttPartner.DataBind();
                    this.ddlEditCttPartner.Items.Insert(0, new ListItem("--- Select Contract Partner ---", "-1"));
                    this.ddlEditCttPartner.SelectedIndex = -1;
                    this.ddlEditCttPartner.Enabled = true;
                    this.ddlEditCttClient.Items.Clear();
                    this.ddlEditCttClient.DataBind();
                    this.ddlEditCttClient.Items.Insert(0, new ListItem("--- Select Contract Client ---", "-1"));
                    this.ddlEditCttClient.SelectedIndex = -1;
                    this.ddlEditCttClient.Enabled = false;
                    this.ddlEditCttList.Items.Clear();
                    this.ddlEditCttList.DataBind();
                    this.ddlEditCttList.Items.Insert(0, new ListItem("--- Select Contract ---", "-1"));
                    this.ddlEditCttList.SelectedIndex = -1;
                    this.ddlEditCttList.Enabled = false;
                    this.ddlEditCttStatus.Items.Clear();
                    this.ddlEditCttStatus.DataBind();
                    this.ddlEditCttStatus.Items.Insert(0, new ListItem("--- Select Contract Status ---", "-1"));
                    this.ddlEditCttStatus.SelectedIndex = -1;
                    this.ddlEditCttStatus.Enabled = false;

                    this.ddlEditCttPartner.Focus();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlEditCttProvider_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.ddlEditCttStatus.Items.Clear();
                this.ddlEditCttStatus.DataBind();
                this.ddlEditCttStatus.Items.Insert(0, new ListItem("--- Select Status ---", "-1"));
                this.ddlEditCttStatus.SelectedIndex = -1;
                this.ddlEditCttStatus.Enabled = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlEditCttPartner_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 typID = Convert.ToInt32(this.ddlEditCttType.SelectedValue);
                Dictionary<Int32, String> clntList = CLNT.getClientList(typID);
                this.ddlEditCttClient.Items.Clear();
                this.ddlEditCttClient.DataSource = clntList;
                this.ddlEditCttClient.DataTextField = "Value";
                this.ddlEditCttClient.DataValueField = "Key";
                this.ddlEditCttClient.DataBind();
                this.ddlEditCttClient.Items.Insert(0, new ListItem("-- Select Contract Client ---", "-1"));
                this.ddlEditCttClient.SelectedIndex = -1;
                this.ddlEditCttClient.Enabled = true;
                this.ddlEditCttList.Items.Clear();
                this.ddlEditCttList.DataBind();
                this.ddlEditCttList.Items.Insert(0, new ListItem("--- Select Contract ---", "-1"));
                this.ddlEditCttList.SelectedIndex = -1;
                this.ddlEditCttList.Enabled = false;
                this.ddlEditCttStatus.Items.Clear();
                this.ddlEditCttStatus.DataBind();
                this.ddlEditCttStatus.Items.Insert(0, new ListItem("--- Select Contract Status ---", "-1"));
                this.ddlEditCttStatus.SelectedIndex = -1;
                this.ddlEditCttStatus.Enabled = false;

                this.ddlEditCttClient.Focus();

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlEditCttClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 typeID = Convert.ToInt32(this.ddlEditCttType.SelectedValue);
                Int32 prtID = Convert.ToInt32(this.ddlEditCttPartner.SelectedValue);
                Int32 clntID = Convert.ToInt32(this.ddlEditCttClient.SelectedValue);
                Dictionary<Int32, String> ctList = CTRT.getClientContractList(typeID, clntID);
                this.ddlEditCttList.Items.Clear();
                this.ddlEditCttList.DataSource = ctList;
                this.ddlEditCttList.DataTextField = "Value";
                this.ddlEditCttList.DataValueField = "Key";
                this.ddlEditCttList.DataBind();
                this.ddlEditCttList.Items.Insert(0, new ListItem("--- Select Contract ---", "-1"));
                this.ddlEditCttList.SelectedIndex = -1;
                this.ddlEditCttList.Enabled = true;
                this.ddlEditCttStatus.Items.Clear();
                this.ddlEditCttStatus.DataBind();
                this.ddlEditCttStatus.Items.Insert(0, new ListItem("--- Select Contract Status ---", "-1"));
                this.ddlEditCttStatus.SelectedIndex = -1;
                this.ddlEditCttStatus.Enabled = false;

                this.ddlEditCttList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlEditCttList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> tplist = CTRT.getGlobalContractStatus();
                this.ddlEditCttStatus.Items.Clear();
                this.ddlEditCttStatus.DataSource = tplist;
                this.ddlEditCttStatus.DataTextField = "Value";
                this.ddlEditCttStatus.DataValueField = "Key";
                this.ddlEditCttStatus.DataBind();
                this.ddlEditCttStatus.Items.Insert(0, new ListItem("--- Select Contract Status ---", "-1"));
                this.ddlEditCttStatus.SelectedIndex = -1;
                this.ddlEditCttStatus.Enabled = true;

                this.ddlEditCttStatus.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlEditCttStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnEditContractUpdate.Enabled = true;
                this.lblEditContractSuccess.Text = "";
                this.lblEditContractSuccess.Visible = false;

                this.btnEditContractUpdate.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAddContractType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctID = Convert.ToInt32(this.ddlAddContractType.SelectedValue);
                if (ctID.Equals(1)) //Demo
                {
                    Dictionary<Int32, String> pList = CLNT.getDemoContractProviderList();
                    Dictionary<Int32, String> dcList = CLNT.getDemoContractClientList();
                    this.ddlAddContractProvider.Items.Clear();
                    this.ddlAddContractProvider.DataSource = pList;
                    this.ddlAddContractProvider.DataTextField = "Value";
                    this.ddlAddContractProvider.DataValueField = "Key";
                    this.ddlAddContractProvider.DataBind();
                    this.ddlAddContractProvider.Items.Insert(0, new ListItem("--- Select Contract Provider ---", "-1"));
                    this.ddlAddContractProvider.SelectedIndex = -1;
                    this.ddlAddContractProvider.Enabled = true;
                    this.ddlAddContractPartner.Items.Clear();
                    this.ddlAddContractPartner.DataSource = pList;
                    this.ddlAddContractPartner.DataTextField = "Value";
                    this.ddlAddContractPartner.DataValueField = "Key";
                    this.ddlAddContractPartner.DataBind();
                    this.ddlAddContractPartner.Items.Insert(0, new ListItem("--- Select Contract Provider ---", "-1"));
                    this.ddlAddContractPartner.SelectedIndex = -1;
                    this.ddlAddContractPartner.Enabled = false;
                    this.ddlAddContractClient.Items.Clear();
                    this.ddlAddContractClient.DataSource = dcList;
                    this.ddlAddContractClient.DataTextField = "Value";
                    this.ddlAddContractClient.DataValueField = "Key";
                    this.ddlAddContractClient.DataBind();
                    this.ddlAddContractClient.Items.Insert(0, new ListItem("--- Select Contract Client ---", "-1"));
                    this.ddlAddContractClient.SelectedIndex = -1;
                    this.ddlAddContractClient.Enabled = true;
                    this.ddlContractSC.Items.Clear();
                    this.ddlContractSC.DataBind();
                    this.ddlContractSC.Items.Insert(0, new ListItem("--- Select Sales Campaign ---", "-1"));
                    this.ddlContractSC.SelectedIndex = -1;
                    this.ddlContractSC.Enabled = false;

                    this.ddlAddContractProvider.Focus();
                }
                else if (ctID.Equals(2)) //Partnership
                {
                    //Provider is FE and Partner Null
                    Dictionary<Int32, String> pList = CLNT.getPartnershipContractProviderList();
                    Dictionary<Int32, String> cpList = CLNT.getPartnerContractClientList();
                    this.ddlAddContractProvider.Items.Clear();
                    this.ddlAddContractProvider.DataSource = pList;
                    this.ddlAddContractProvider.DataTextField = "Value";
                    this.ddlAddContractProvider.DataValueField = "Key";
                    this.ddlAddContractProvider.DataBind();
                    this.ddlAddContractProvider.Items.Insert(0, new ListItem("--- Select Contract Provider ---", "-1"));
                    this.ddlAddContractProvider.SelectedIndex = 1;
                    this.ddlAddContractProvider.Enabled = false;
                    this.ddlAddContractPartner.Items.Clear();
                    this.ddlAddContractPartner.DataBind();
                    this.ddlAddContractPartner.Items.Insert(0, new ListItem("--- Unavailable ---", "-1"));
                    this.ddlAddContractPartner.SelectedIndex = -1;
                    this.ddlAddContractPartner.Enabled = false;
                    this.ddlAddContractClient.Items.Clear();
                    this.ddlAddContractClient.DataSource = cpList;
                    this.ddlAddContractClient.DataTextField = "Value";
                    this.ddlAddContractClient.DataValueField = "Key";
                    this.ddlAddContractClient.DataBind();
                    this.ddlAddContractClient.Items.Insert(0, new ListItem("--- Select Contract Client ---", "-1"));
                    this.ddlAddContractClient.SelectedIndex = -1;
                    this.ddlAddContractClient.Enabled = true;
                    this.ddlContractSC.Items.Clear();
                    this.ddlContractSC.DataBind();
                    this.ddlContractSC.Items.Insert(0, new ListItem("--- Select Sales Campaign ---", "-1"));
                    this.ddlContractSC.SelectedIndex = -1;
                    this.ddlContractSC.Enabled = false;

                    this.ddlAddContractClient.Focus();
                }
                else //Services
                {
                    Dictionary<Int32, String> cpList = CLNT.getClientContractProviderList();
                    Dictionary<Int32, String> prtList = CLNT.getContractPartnerList();
                    this.ddlAddContractProvider.Items.Clear();
                    this.ddlAddContractProvider.DataSource = cpList;
                    this.ddlAddContractProvider.DataTextField = "Value";
                    this.ddlAddContractProvider.DataValueField = "Key";
                    this.ddlAddContractProvider.DataBind();
                    this.ddlAddContractProvider.Items.Insert(0, new ListItem("--- Select Contract Provider ---", "-1"));
                    this.ddlAddContractProvider.SelectedIndex = 1;
                    this.ddlAddContractProvider.Enabled = true;
                    this.ddlAddContractPartner.Items.Clear();
                    this.ddlAddContractPartner.DataSource = prtList;
                    this.ddlAddContractPartner.DataTextField = "Value";
                    this.ddlAddContractPartner.DataValueField = "Key";
                    this.ddlAddContractPartner.DataBind();
                    this.ddlAddContractPartner.Items.Insert(0, new ListItem("--- Select Contract Partner ---", "-1"));
                    this.ddlAddContractPartner.SelectedIndex = -1;
                    this.ddlAddContractPartner.Enabled = true;
                    this.ddlAddContractClient.Items.Clear();
                    this.ddlAddContractClient.DataBind();
                    this.ddlAddContractClient.Items.Insert(0, new ListItem("--- Select Contract Client ---", "-1"));
                    this.ddlAddContractClient.SelectedIndex = -1;
                    this.ddlAddContractClient.Enabled = false;
                    this.ddlContractSC.Items.Clear();
                    this.ddlContractSC.DataBind();
                    this.ddlContractSC.Items.Insert(0, new ListItem("--- Select Sales Campaign ---", "-1"));
                    this.ddlContractSC.SelectedIndex = -1;
                    this.ddlContractSC.Enabled = false;

                    this.ddlAddContractPartner.Focus();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAddContractProvider_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> prtList = CLNT.getContractPartnerList();
                this.ddlAddContractPartner.Items.Clear();
                this.ddlAddContractPartner.DataSource = prtList;
                this.ddlAddContractPartner.DataTextField = "Value";
                this.ddlAddContractPartner.DataValueField = "Key";
                this.ddlAddContractPartner.DataBind();
                this.ddlAddContractPartner.Items.Insert(0, new ListItem("--- Select Contract Partner ---", "-1"));
                this.ddlAddContractPartner.SelectedIndex = -1;
                this.ddlAddContractPartner.Enabled = true;
                this.ddlContractSC.Items.Clear();
                this.ddlContractSC.DataBind();
                this.ddlContractSC.Items.Insert(0, new ListItem("--- Select Sales Campaign ---", "-1"));
                this.ddlContractSC.SelectedIndex = -1;
                this.ddlContractSC.Enabled = false;

                this.ddlAddContractPartner.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAddContractPartner_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ddlTypVal = Convert.ToInt32(this.ddlAddContractType.SelectedValue);
                if (ddlTypVal.Equals(2)) //Type is Partnership
                {
                    //Int32 cnt = Convert.ToInt32(this.ddlAddContractPartner.SelectedValue);
                    Dictionary<Int32, String> csttList = CTRT.getContractStatusList();
                    this.txtContractName.Text = "";
                    this.txtContractName.Enabled = true;
                    this.ddlAddContractStatus.Items.Clear();
                    this.ddlAddContractStatus.DataSource = csttList;
                    this.ddlAddContractStatus.DataTextField = "Value";
                    this.ddlAddContractStatus.DataValueField = "Key";
                    this.ddlAddContractStatus.DataBind();
                    this.ddlAddContractStatus.Items.Insert(0, new ListItem("--- Select Contract Status ---", "-1"));
                    this.ddlAddContractStatus.SelectedIndex = -1;
                    this.ddlAddContractStatus.Enabled = true;
                    this.ddlContractSC.Items.Clear();
                    this.ddlContractSC.DataBind();
                    this.ddlContractSC.Items.Insert(0, new ListItem("--- Select Sales Campaign ---", "-1"));
                    this.ddlContractSC.SelectedIndex = -1;
                    this.ddlContractSC.Enabled = false;

                    this.txtContractName.Focus();
                }
                else //Type is Service
                {
                    Dictionary<Int32, String> clntList = CLNT.getClientList(1);
                    this.ddlAddContractClient.Items.Clear();
                    this.ddlAddContractClient.DataSource = clntList;
                    this.ddlAddContractClient.DataTextField = "Value";
                    this.ddlAddContractClient.DataValueField = "Key";
                    this.ddlAddContractClient.DataBind();
                    this.ddlAddContractClient.Items.Insert(0, new ListItem("--- Select Contract Client ---", "-1"));
                    this.ddlAddContractClient.SelectedIndex = -1;
                    this.ddlAddContractClient.Enabled = true;
                    this.ddlContractSC.Items.Clear();
                    this.ddlContractSC.DataBind();
                    this.ddlContractSC.Items.Insert(0, new ListItem("--- Select Sales Campaign ---", "-1"));
                    this.ddlContractSC.SelectedIndex = -1;
                    this.ddlContractSC.Enabled = false;

                    this.ddlAddContractClient.Focus();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAddContractClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> csttList = CTRT.getContractStatusList();
                this.txtContractName.Text = "";
                this.txtContractName.Enabled = true;
                this.ddlAddContractStatus.Items.Clear();
                this.ddlAddContractStatus.DataSource = csttList;
                this.ddlAddContractStatus.DataTextField = "Value";
                this.ddlAddContractStatus.DataValueField = "Key";
                this.ddlAddContractStatus.DataBind();
                this.ddlAddContractStatus.Items.Insert(0, new ListItem("--- Select Contract Status ---", "-1"));
                this.ddlAddContractStatus.SelectedIndex = -1;
                this.ddlAddContractStatus.Enabled = true;
                this.ddlContractSC.Items.Clear();
                this.ddlContractSC.DataBind();
                this.ddlContractSC.Items.Insert(0, new ListItem("--- Select Sales Campaign ---", "-1"));
                this.ddlContractSC.SelectedIndex = -1;
                this.ddlContractSC.Enabled = false;

                this.txtContractName.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAddContractStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                String selStatus = Convert.ToString(this.ddlAddContractStatus.SelectedItem);
                if (!(selStatus.Equals("Active")) || !(selStatus.Equals("Complete")))
                {
                    this.txtCRep.Text = "";
                    this.txtCRep.Enabled = true;
                    this.txtCRepDesig.Text = "";
                    this.txtCRepDesig.Enabled = true;
                    this.txtStartCalendar.Enabled = true;
                    this.txtEndCalendar.Enabled = true;
                    Dictionary<Int32, String> scList = SL.getCampaignList();
                    this.ddlContractSC.Items.Clear();
                    this.ddlContractSC.DataSource = scList;
                    this.ddlContractSC.DataTextField = "Value";
                    this.ddlContractSC.DataValueField = "Key";
                    this.ddlContractSC.DataBind();
                    this.ddlContractSC.Items.Insert(0, new ListItem("--- Select Sales Campaign ---", "-1"));
                    this.ddlContractSC.SelectedIndex = -1;
                    this.ddlContractSC.Enabled = true;

                    this.txtCRep.Focus();
                }
                else
                {
                    this.txtCRep.Text = "";
                    this.txtCRep.Enabled = false;
                    this.txtCRepDesig.Text = "";
                    this.txtCRepDesig.Enabled = false;
                    this.txtStartCalendar.Enabled = false;
                    this.txtEndCalendar.Enabled = false;
                    this.ddlContractSC.Items.Clear();
                    this.ddlContractSC.DataBind();
                    this.ddlContractSC.Items.Insert(0, new ListItem("--- Select Sales Campaign ---", "-1"));
                    this.ddlContractSC.SelectedIndex = -1;
                    this.ddlContractSC.Enabled = false;

                    this.ddlAddContractStatus.Focus();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void clndContractStart_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctyp = 0;
                DateTime sDate = DateTime.Now;
                DateTime eDate = DateTime.Now;

                ctyp = Convert.ToInt32(this.ddlAddContractType.SelectedValue);


                if (ctyp.Equals(1))
                {
                    //this.clndContractEnd.SelectedDate = sDate.AddDays(14);
                    //this.clndContractEnd.VisibleDate = sDate.AddDays(14);
                    //this.clndContractEnd.Focus();
                }
                else if (ctyp.Equals(2))
                {
                    //this.clndContractEnd.SelectedDate = sDate.AddDays(182);
                    //this.clndContractEnd.VisibleDate = sDate.AddDays(182);
                    //this.clndContractEnd.Focus();
                }
                else
                {
                    //this.clndContractEnd.SelectedDate = sDate.AddDays(90);
                    //this.clndContractEnd.VisibleDate = sDate.AddDays(90);
                    //this.clndContractEnd.Focus();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void clndContractEnd_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> scList = SL.getCampaignList();
                this.ddlContractSC.Items.Clear();
                this.ddlContractSC.DataSource = scList;
                this.ddlContractSC.DataTextField = "Value";
                this.ddlContractSC.DataValueField = "Key";
                this.ddlContractSC.DataBind();
                this.ddlContractSC.Items.Insert(0, new ListItem("--- Select Sales Campaign ---", "-1"));
                this.ddlContractSC.SelectedIndex = -1;
                this.ddlContractSC.Enabled = true;
                //this.btnAddNewContract.Enabled = true;

                this.ddlContractSC.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlContractSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnAddNewContract.Enabled = true;

                this.btnAddNewContract.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewContract_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 provID = 0, clntID = 0, cstat = 0, ctyp = 0, amgr = 0, cmpID = 0;
                String cName = "", clientName = "", repName = "", repDesig = "";
                DateTime sD = DateTime.Now;
                DateTime eD = DateTime.Now;
                Double contractPeriod = 0;
                Int32 pCont = 0; // Partnership Contract inserted
                Int32 dCont = 0; // Demo Contract inserted
                Int32 cCont = 0; // Client Service Contract inserted

                ctyp = Convert.ToInt32(this.ddlAddContractType.SelectedValue);
                provID = Convert.ToInt32(this.ddlAddContractProvider.SelectedValue);
                cName = Convert.ToString(this.txtContractName.Text);
                cstat = Convert.ToInt32(this.ddlAddContractStatus.SelectedValue);
                repName = Convert.ToString(this.txtCRep.Text);
                repDesig = Convert.ToString(this.txtCRepDesig.Text);
                sD = Convert.ToDateTime(this.txtStartCalendar.Text);
                eD = Convert.ToDateTime(this.txtEndCalendar.Text);
                cmpID = Convert.ToInt32(this.ddlContractSC.SelectedValue);
                if (eD < sD)
                {
                    this.lblContractSuccess.ForeColor = CLR.Maroon;
                    this.lblContractSuccess.Font.Size = 14;
                    this.lblContractSuccess.Text = "Selected Contract end date is before start date. Please fix the issue before inserting contract.";
                    this.lblContractSuccess.Visible = true;
                }
                else
                {
                    contractPeriod = (eD - sD).TotalDays;

                    if (ctyp.Equals(1))
                    {
                        amgr = Convert.ToInt32(this.ddlAddContractPartner.SelectedValue);
                        clntID = Convert.ToInt32(this.ddlAddContractClient.SelectedValue);
                        clientName = Convert.ToString(this.ddlAddContractClient.SelectedItem);

                        dCont = Convert.ToInt32(CTRT.demoContract(provID, amgr, clntID, clientName, cName, ctyp, cstat, sD, eD, cmpID, LoginName, domain));

                        if (dCont.Equals(1))
                        {
                            btnAddNewContractCancel_Click(null, null);
                            this.lblContractSuccess.ForeColor = CLR.Green;
                            this.lblContractSuccess.Font.Size = 14;
                            this.lblContractSuccess.Text = "Successfully inserted New Demo Contract into the System.";
                            this.lblContractSuccess.Visible = true;

                            this.btnAddContract.Focus();
                        }
                        else
                        {
                            btnAddNewContractCancel_Click(null, null);
                            this.lblContractSuccess.ForeColor = CLR.Red;
                            this.lblContractSuccess.Font.Size = 14;
                            this.lblContractSuccess.Text = "There was a problem inserting Demo contract. Please verify all info and try adding Demo Contract again.";
                            this.lblContractSuccess.Visible = true;

                            this.btnAddContract.Focus();
                        }
                    }
                    else if (ctyp.Equals(2))
                    {
                        clntID = Convert.ToInt32(this.ddlAddContractPartner.SelectedValue);
                        clientName = Convert.ToString(this.ddlAddContractPartner.SelectedItem);

                        pCont = Convert.ToInt32(CTRT.partnerContract(provID, clntID, clientName, cName, ctyp, cstat, sD, eD, cmpID, LoginName, domain));
                        if (pCont.Equals(1))
                        {
                            btnAddNewContractCancel_Click(null, null);
                            this.lblContractSuccess.ForeColor = CLR.Green;
                            this.lblContractSuccess.Font.Size = 14;
                            this.lblContractSuccess.Text = "Successfully inserted New Partnership Contract into the System.";
                            this.lblContractSuccess.Visible = true;
                            this.btnAddContract.Focus();
                        }
                        else
                        {
                            btnAddNewContractCancel_Click(null, null);
                            this.lblContractSuccess.ForeColor = CLR.Red;
                            this.lblContractSuccess.Font.Size = 14;
                            this.lblContractSuccess.Text = "There was a problem inserting Partnership contract. Please verify all info and try adding Demo Contract again.";
                            this.lblContractSuccess.Visible = true;
                            this.btnAddContract.Focus();
                        }
                    }
                    else if (ctyp.Equals(3))
                    {
                        amgr = Convert.ToInt32(this.ddlAddContractPartner.SelectedValue);
                        clntID = Convert.ToInt32(this.ddlAddContractClient.SelectedValue);
                        clientName = Convert.ToString(this.ddlAddContractClient.SelectedItem);

                        cCont = Convert.ToInt32(CTRT.clientContract(provID, amgr, clntID, clientName, cName, ctyp, cstat, sD, eD, cmpID, LoginName, domain));
                        if (cCont.Equals(1))
                        {
                            btnAddNewContractCancel_Click(null, null);
                            this.lblContractSuccess.ForeColor = CLR.Green;
                            this.lblContractSuccess.Font.Size = 14;
                            this.lblContractSuccess.Text = "Successfully inserted New Client Contract into the System.";
                            this.lblContractSuccess.Visible = true;
                            this.btnAddContract.Focus();
                        }
                        else
                        {
                            btnAddNewContractCancel_Click(null, null);
                            this.lblContractSuccess.ForeColor = CLR.Red;
                            this.lblContractSuccess.Font.Size = 14;
                            this.lblContractSuccess.Text = "There was a problem inserting Client contract. Please verify all info and try adding Demo Contract again.";
                            this.lblContractSuccess.Visible = true;
                            this.btnAddContract.Focus();
                        }
                    }
                    else
                    {
                        btnAddNewContractCancel_Click(null, null);
                        this.tblAddContract.Visible = true;
                        this.lblContractSuccess.ForeColor = CLR.Red;
                        this.lblContractSuccess.Font.Size = 14;
                        this.lblContractSuccess.Text = "There was a problem adding the new Contract. Please try again.";
                        this.lblContractSuccess.Visible = true;
                        this.btnAddContract.Focus();
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewContractCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.ddlAddContractType.Items.Clear();
                this.ddlAddContractType.DataBind();
                this.ddlAddContractType.Items.Insert(0, new ListItem("--- Select Contract Type ---", "-1"));
                this.ddlAddContractType.SelectedIndex = -1;
                this.ddlAddContractProvider.Items.Clear();
                this.ddlAddContractProvider.DataBind();
                this.ddlAddContractProvider.Items.Insert(0, new ListItem("--- Select Contract Provider ---", "-1"));
                this.ddlAddContractProvider.SelectedIndex = -1;
                this.ddlAddContractProvider.Enabled = false;
                this.ddlAddContractPartner.Items.Clear();
                this.ddlAddContractPartner.DataBind();
                this.ddlAddContractPartner.Items.Insert(0, new ListItem("--- Select Contract Partner ---", "-1"));
                this.ddlAddContractPartner.SelectedIndex = -1;
                this.ddlAddContractPartner.Enabled = false;
                this.ddlAddContractClient.Items.Clear();
                this.ddlAddContractClient.DataBind();
                this.ddlAddContractClient.Items.Insert(0, new ListItem("--- Select Contract Client ---", "-1"));
                this.ddlAddContractClient.SelectedIndex = -1;
                this.ddlAddContractClient.Enabled = false;
                this.txtContractName.Text = "";
                this.txtContractName.Enabled = false;
                this.ddlAddContractStatus.Items.Clear();
                this.ddlAddContractStatus.DataBind();
                this.ddlAddContractStatus.Items.Insert(0, new ListItem("--- Select Contract Status ---", "-1"));
                this.ddlAddContractStatus.SelectedIndex = -1;
                this.ddlAddContractStatus.Enabled = false;
                this.txtCRep.Text = "";
                this.txtCRep.Enabled = false;
                this.txtCRepDesig.Text = "";
                this.txtCRepDesig.Enabled = false;
                this.txtStartCalendar.Enabled = false;
                this.txtEndCalendar.Enabled = false;
                this.ddlContractSC.Items.Clear();
                this.ddlContractSC.DataBind();
                this.ddlContractSC.Items.Insert(0, new ListItem("--- Select Sales Campaign ---", "-1"));
                this.ddlContractSC.SelectedIndex = -1;
                this.ddlContractSC.Enabled = false;
                this.lblContractSuccess.Text = "";
                this.lblContractSuccess.CssClass = "lblSuccess";
                this.lblContractSuccess.Visible = false;

                this.ddlAddContractType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewContractDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnAddNewContractCancel_Click(null, null);
                this.tblContract.Visible = true;
                this.tblEditContract.Visible = false;
                this.tblAddContract.Visible = false;
                this.btnAddNewContractDone.Visible = false;
                Dictionary<Int32, String> gclients = CLNT.getGlobalClientsList();
                this.ddlClientCo.Items.Clear();
                this.ddlClientCo.DataSource = gclients;
                this.ddlClientCo.DataTextField = "Value";
                this.ddlClientCo.DataValueField = "Key";
                this.ddlClientCo.DataBind();
                this.ddlClientCo.Items.Insert(0, new ListItem("--- Select Contract Client ---", "-1"));
                this.ddlClientCo.SelectedIndex = -1;
                this.ddlClientCo.Enabled = true;
                this.gvwContractList.DataBind();
                this.gvwContractList.Visible = false;

                this.ddlClientCo.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlClientCo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cID = Convert.ToInt32(this.ddlClientCo.SelectedValue);
                System.Data.DataTable ctrtTable = CLNT.getClientContracts(cID);
                this.gvwContractList.DataSource = ctrtTable;
                this.gvwContractList.DataBind();
                this.gvwContractList.Visible = true;
                this.btnEditContract.Visible = true;
                this.gvwContractList.Focus();
                this.btnContractListCancel.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwContractList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow drRes = ((DataRowView)e.Row.DataItem).Row;
                    String stt = Convert.ToString(drRes["cstatID"]);
                    Double rmn = Convert.ToDouble(drRes["ctrtRmn"]);
                    if (stt.Equals("Complete") || stt.Equals("Terminated"))
                    {
                        rmn = 0;
                        e.Row.BackColor = CLR.Red;
                    }
                    else
                    {
                        if (rmn < 14)
                        {
                            e.Row.BackColor = CLR.LightYellow;
                        }
                        else if (rmn < 7)
                        {
                            e.Row.BackColor = CLR.Red;
                            e.Row.ForeColor = CLR.White;
                        }
                        else if (rmn < 120)
                        {
                            e.Row.BackColor = CLR.LightYellow;
                        }
                        else if (rmn < 60)
                        {
                            e.Row.BackColor = CLR.Red;
                            e.Row.ForeColor = CLR.White;
                        }
                        else
                        {
                            e.Row.BackColor = CLR.LightGreen;
                        }
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlContractList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctrtID = Convert.ToInt32(this.ddlContractList.SelectedValue);
                System.Data.DataTable pTable = CTRT.getPriceListTable(ctrtID);
                this.gvwPriceList.DataSource = pTable;
                this.gvwPriceList.DataBind();
                this.gvwPriceList.Visible = true;
                this.btnPriceCancel.Visible = true;

                this.gvwPriceList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnPriceAdd_Click(object sender, EventArgs e)
        {
            try
            {
                btnPriceCancel_Click(null, null);
                this.tblServicePrices.Visible = false;
                this.tblAddPrice.Visible = true;
                this.btnAPNewDone.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnPriceCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.ddlContractList.Items.Clear();
                this.ddlContractList.DataBind();
                this.ddlContractList.Items.Insert(0, new ListItem("--- Select Client Contract ---", "-1"));
                this.ddlContractList.SelectedIndex = -1;
                this.gvwPriceList.DataBind();
                this.gvwPriceList.Visible = false;
                this.btnPriceCancel.Visible = false;

                this.ddlContractList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAddressClientType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctID = Convert.ToInt32(this.ddlAddressClientType.SelectedValue);
                Dictionary<Int32, String> cntList = CLNT.getClientList(ctID);
                this.ddlAddressClientList.Items.Clear();
                this.ddlAddressClientList.DataSource = cntList;
                this.ddlAddressClientList.DataTextField = "Value";
                this.ddlAddressClientList.DataValueField = "Key";
                this.ddlAddressClientList.DataBind();
                this.ddlAddressClientList.Items.Insert(0, new ListItem("--- Select SMfARTs Client ---", "-1"));
                this.ddlAddressClientList.SelectedIndex = -1;
                this.ddlAddressClientList.Enabled = true;
                this.btnAddressCnacel.Visible = true;

                this.ddlAddressClientList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAddressClientList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cID = Convert.ToInt32(this.ddlAddressClientList.SelectedValue);
                System.Data.DataTable addTable = CLNT.getAddressTable(cID);
                this.gvwAddressList.DataSource = addTable;
                this.gvwAddressList.DataBind();
                this.gvwAddressList.Visible = true;
                this.gvwAddressList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddressAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.ddlAddressClientType.Items.Clear();
                this.ddlAddressClientType.DataBind();
                this.ddlAddressClientType.Items.Insert(0, new ListItem("--- Select Client Name ---", "-1"));
                this.ddlAddressClientType.SelectedIndex = -1;
                this.ddlAddressClientType.Enabled = true;
                this.ddlAddressClientList.Items.Clear();
                this.ddlAddressClientList.DataBind();
                this.ddlAddressClientList.Items.Insert(0, new ListItem("--- Select SMARTs Client ---", "-1"));
                this.ddlAddressClientList.SelectedIndex = -1;
                this.ddlAddressClientList.Enabled = false;
                this.gvwAddressList.DataBind();
                this.gvwAddressList.Visible = false;
                this.tblAddressList.Visible = false;
                this.tblAddressAdd.Visible = true;
                this.btnNADone.Visible = true;
                this.btnNADone.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddressCnacel_Click(object sender, EventArgs e)
        {
            try
            {
                this.ddlAddressClientType.Items.Clear();
                this.ddlAddressClientType.DataBind();
                this.ddlAddressClientType.Items.Insert(0, new ListItem("--- Select Client Name ---", "-1"));
                this.ddlAddressClientType.SelectedIndex = -1;
                this.ddlAddressClientType.Enabled = true;
                this.ddlAddressClientList.Items.Clear();
                this.ddlAddressClientList.DataBind();
                this.ddlAddressClientList.Items.Insert(0, new ListItem("--- Select SMARTs Client ---", "-1"));
                this.ddlAddressClientList.SelectedIndex = -1;
                this.ddlAddressClientList.Enabled = false;
                this.gvwAddressList.Visible = false;
                this.gvwAddressList.DataBind();
                this.tblAddressAdd.Visible = false;
                this.lblAddressSuccess.Text = "";
                this.lblAddressSuccess.CssClass = "lblSuccess";
                this.lblAddressSuccess.Visible = false;
                this.btnAddressCnacel.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAddressNewClientType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctypID = Convert.ToInt32(this.ddlAddressNewClientType.SelectedValue);
                Dictionary<Int32, String> cList = CLNT.getClientList(ctypID);
                this.ddlAddressNewClientList.Items.Clear();
                this.ddlAddressNewClientList.DataSource = cList;
                this.ddlAddressNewClientList.DataTextField = "Value";
                this.ddlAddressNewClientList.DataValueField = "Key";
                this.ddlAddressNewClientList.DataBind();
                this.ddlAddressNewClientList.Items.Insert(0, new ListItem("--- Select SMARTs Client ---", "-1"));
                this.ddlAddressNewClientList.SelectedIndex = -1;
                this.ddlAddressNewClientList.Enabled = true;
                this.ddlNADemog.Items.Clear();
                this.ddlNADemog.DataBind();
                this.ddlNADemog.Items.Insert(0, new ListItem("--- Select Address Type ---", "-1"));
                this.ddlNADemog.SelectedIndex = -1;
                this.ddlNADemog.Enabled = false;
                this.txtNATitle.Text = "";
                this.txtNATitle.Enabled = false;
                this.ddlNAReg.Items.Clear();
                this.ddlNAReg.DataBind();
                this.ddlNAReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlNAReg.SelectedIndex = -1;
                this.ddlNAReg.Enabled = false;
                this.ddlNASReg.Items.Clear();
                this.ddlNASReg.DataBind();
                this.ddlNASReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlNASReg.SelectedIndex = -1;
                this.ddlNASReg.Enabled = false;
                this.ddlNACountry.Items.Clear();
                this.ddlNACountry.DataBind();
                this.ddlNACountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlNACountry.SelectedIndex = -1;
                this.ddlNACountry.Enabled = false;
                this.ddlNAState.Items.Clear();
                this.ddlNAState.DataBind();
                this.ddlNAState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlNAState.SelectedIndex = -1;
                this.ddlNAState.Enabled = false;
                this.ddlNACounty.Items.Clear();
                this.ddlNACounty.DataBind();
                this.ddlNACounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality/Division ---", "-1"));
                this.ddlNACounty.SelectedIndex = -1;
                this.ddlNACounty.Enabled = false;
                this.ddlNACity.Items.Clear();
                this.ddlNACity.DataBind();
                this.ddlNACity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlNACity.SelectedIndex = -1;
                this.ddlNACity.Enabled = false;
                this.ddlNAZip.Items.Clear();
                this.ddlNAZip.DataBind();
                this.ddlNAZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlNAZip.SelectedIndex = -1;
                this.ddlNAZip.Enabled = false;
                this.txtSt1.Text = "";
                this.txtSt1.Enabled = false;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = false;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = false;
                this.lblAddressSuccess.Text = "";
                this.lblAddressSuccess.CssClass = "lblSuccess";
                this.lblAddressSuccess.Visible = false;
                this.btnNAAdd.Enabled = true;
                this.btnNAClear.Enabled = true;

                this.ddlAddressNewClientList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAddressNewClientList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> dList = DMG.getDemogTypeList();
                this.ddlNADemog.Items.Clear();
                this.ddlNADemog.DataSource = dList;
                this.ddlNADemog.DataTextField = "Value";
                this.ddlNADemog.DataValueField = "Key";
                this.ddlNADemog.DataBind();
                this.ddlNADemog.Items.Insert(0, new ListItem("--- Select Address Type ---", "-1"));
                this.ddlNADemog.SelectedIndex = -1;
                this.ddlNADemog.Enabled = true;
                this.txtNATitle.Text = "";
                this.txtNATitle.Enabled = false;
                this.ddlNAReg.Items.Clear();
                this.ddlNAReg.DataBind();
                this.ddlNAReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlNAReg.SelectedIndex = -1;
                this.ddlNAReg.Enabled = false;
                this.ddlNASReg.Items.Clear();
                this.ddlNASReg.DataBind();
                this.ddlNASReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlNASReg.SelectedIndex = -1;
                this.ddlNASReg.Enabled = false;
                this.ddlNACountry.Items.Clear();
                this.ddlNACountry.DataBind();
                this.ddlNACountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlNACountry.SelectedIndex = -1;
                this.ddlNACountry.Enabled = false;
                this.ddlNAState.Items.Clear();
                this.ddlNAState.DataBind();
                this.ddlNAState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlNAState.SelectedIndex = -1;
                this.ddlNAState.Enabled = false;
                this.ddlNACounty.Items.Clear();
                this.ddlNACounty.DataBind();
                this.ddlNACounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality/Division ---", "-1"));
                this.ddlNACounty.SelectedIndex = -1;
                this.ddlNACounty.Enabled = false;
                this.ddlNACity.Items.Clear();
                this.ddlNACity.DataBind();
                this.ddlNACity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlNACity.SelectedIndex = -1;
                this.ddlNACity.Enabled = false;
                this.ddlNAZip.Items.Clear();
                this.ddlNAZip.DataBind();
                this.ddlNAZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlNAZip.SelectedIndex = -1;
                this.ddlNAZip.Enabled = false;
                this.txtSt1.Text = "";
                this.txtSt1.Enabled = false;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = false;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = false;
                this.lblAddressSuccess.Text = "";
                this.lblAddressSuccess.CssClass = "lblSuccess";
                this.lblAddressSuccess.Visible = false;
                this.btnNAClear.Enabled = true;

                this.ddlNADemog.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlNADemog_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> rList = DMG.getRegionList();
                this.txtNATitle.Text = "";
                this.txtNATitle.Enabled = true;
                this.ddlNAReg.Items.Clear();
                this.ddlNAReg.DataSource = rList;
                this.ddlNAReg.DataTextField = "Value";
                this.ddlNAReg.DataValueField = "Key";
                this.ddlNAReg.DataBind();
                this.ddlNAReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlNAReg.SelectedIndex = -1;
                this.ddlNAReg.Enabled = true;
                this.ddlNASReg.Items.Clear();
                this.ddlNASReg.DataBind();
                this.ddlNASReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlNASReg.SelectedIndex = -1;
                this.ddlNASReg.Enabled = false;
                this.ddlNACountry.Items.Clear();
                this.ddlNACountry.DataBind();
                this.ddlNACountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlNACountry.SelectedIndex = -1;
                this.ddlNACountry.Enabled = false;
                this.ddlNAState.Items.Clear();
                this.ddlNAState.DataBind();
                this.ddlNAState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlNAState.SelectedIndex = -1;
                this.ddlNAState.Enabled = false;
                this.ddlNACounty.Items.Clear();
                this.ddlNACounty.DataBind();
                this.ddlNACounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality/Division ---", "-1"));
                this.ddlNACounty.SelectedIndex = -1;
                this.ddlNACounty.Enabled = false;
                this.ddlNACity.Items.Clear();
                this.ddlNACity.DataBind();
                this.ddlNACity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlNACity.SelectedIndex = -1;
                this.ddlNACity.Enabled = false;
                this.ddlNAZip.Items.Clear();
                this.ddlNAZip.DataBind();
                this.ddlNAZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlNAZip.SelectedIndex = -1;
                this.ddlNAZip.Enabled = false;
                this.txtSt1.Text = "";
                this.txtSt1.Enabled = false;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = false;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = false;
                this.lblAddressSuccess.Text = "";
                this.lblAddressSuccess.CssClass = "lblSuccess";
                this.lblAddressSuccess.Visible = false;
                this.btnNAClear.Enabled = true;

                this.txtNATitle.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlNAReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 narID = Convert.ToInt32(this.ddlNAReg.SelectedValue);
                Dictionary<Int32, String> srList = DMG.getSubRegionList(narID);

                this.ddlNASReg.Items.Clear();
                this.ddlNASReg.DataSource = srList;
                this.ddlNASReg.DataTextField = "Value";
                this.ddlNASReg.DataValueField = "Key";
                this.ddlNASReg.DataBind();
                this.ddlNASReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlNASReg.SelectedIndex = -1;
                this.ddlNASReg.Enabled = true;
                this.ddlNACountry.Items.Clear();
                this.ddlNACountry.DataBind();
                this.ddlNACountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlNACountry.SelectedIndex = -1;
                this.ddlNACountry.Enabled = false;
                this.ddlNAState.Items.Clear();
                this.ddlNAState.DataBind();
                this.ddlNAState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlNAState.SelectedIndex = -1;
                this.ddlNAState.Enabled = false;
                this.ddlNACounty.Items.Clear();
                this.ddlNACounty.DataBind();
                this.ddlNACounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality/Division ---", "-1"));
                this.ddlNACounty.SelectedIndex = -1;
                this.ddlNACounty.Enabled = false;
                this.ddlNACity.Items.Clear();
                this.ddlNACity.DataBind();
                this.ddlNACity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlNACity.SelectedIndex = -1;
                this.ddlNACity.Enabled = false;
                this.ddlNAZip.Items.Clear();
                this.ddlNAZip.DataBind();
                this.ddlNAZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlNAZip.SelectedIndex = -1;
                this.ddlNAZip.Enabled = false;
                this.txtSt1.Text = "";
                this.txtSt1.Enabled = false;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = false;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = false;
                this.lblAddressSuccess.Text = "";
                this.lblAddressSuccess.CssClass = "lblSuccess";
                this.lblAddressSuccess.Visible = false;
                this.btnNAClear.Enabled = true;

                this.ddlNASReg.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlNASReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlNASReg.SelectedValue);
                Dictionary<Int32, String> ctyList = DMG.getCountryList(srID);
                this.ddlNACountry.Items.Clear();
                this.ddlNACountry.DataSource = ctyList;
                this.ddlNACountry.DataTextField = "Value";
                this.ddlNACountry.DataValueField = "Key";
                this.ddlNACountry.DataBind();
                this.ddlNACountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlNACountry.SelectedIndex = -1;
                this.ddlNACountry.Enabled = true;
                this.ddlNAState.Items.Clear();
                this.ddlNAState.DataBind();
                this.ddlNAState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlNAState.SelectedIndex = -1;
                this.ddlNAState.Enabled = false;
                this.ddlNACounty.Items.Clear();
                this.ddlNACounty.DataBind();
                this.ddlNACounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality/Division ---", "-1"));
                this.ddlNACounty.SelectedIndex = -1;
                this.ddlNACounty.Enabled = false;
                this.ddlNACity.Items.Clear();
                this.ddlNACity.DataBind();
                this.ddlNACity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlNACity.SelectedIndex = -1;
                this.ddlNACity.Enabled = false;
                this.ddlNAZip.Items.Clear();
                this.ddlNAZip.DataBind();
                this.ddlNAZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlNAZip.SelectedIndex = -1;
                this.ddlNAZip.Enabled = false;
                this.txtSt1.Text = "";
                this.txtSt1.Enabled = false;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = false;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = false;
                this.lblAddressSuccess.Text = "";
                this.lblAddressSuccess.CssClass = "lblSuccess";
                this.lblAddressSuccess.Visible = false;
                this.btnNAClear.Enabled = true;

                this.ddlNACountry.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlNACountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cID = Convert.ToInt32(this.ddlNACountry.SelectedValue);
                Dictionary<Int32, String> sList = DMG.getStateList(cID);
                this.ddlNAState.Items.Clear();
                this.ddlNAState.DataSource = sList;
                this.ddlNAState.DataTextField = "Value";
                this.ddlNAState.DataValueField = "Key";
                this.ddlNAState.DataBind();
                this.ddlNAState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlNAState.SelectedIndex = -1;
                this.ddlNAState.Enabled = true;
                this.ddlNACounty.Items.Clear();
                this.ddlNACounty.DataBind();
                this.ddlNACounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality/Division ---", "-1"));
                this.ddlNACounty.SelectedIndex = -1;
                this.ddlNACounty.Enabled = false;
                this.ddlNACity.Items.Clear();
                this.ddlNACity.DataBind();
                this.ddlNACity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlNACity.SelectedIndex = -1;
                this.ddlNACity.Enabled = false;
                this.ddlNAZip.Items.Clear();
                this.ddlNAZip.DataBind();
                this.ddlNAZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlNAZip.SelectedIndex = -1;
                this.ddlNAZip.Enabled = false;
                this.txtSt1.Text = "";
                this.txtSt1.Enabled = false;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = false;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = false;
                this.lblAddressSuccess.Text = "";
                this.lblAddressSuccess.CssClass = "lblSuccess";
                this.lblAddressSuccess.Visible = false;
                this.btnNAClear.Enabled = true;

                this.ddlNAState.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlNAState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 sID = Convert.ToInt32(this.ddlNAState.SelectedValue);
                Dictionary<Int32, String> cnList = DMG.getCountyList(sID);
                this.ddlNACounty.Items.Clear();
                this.ddlNACounty.DataSource = cnList;
                this.ddlNACounty.DataTextField = "Value";
                this.ddlNACounty.DataValueField = "Key";
                this.ddlNACounty.DataBind();
                this.ddlNACounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality/Division ---", "-1"));
                this.ddlNACounty.SelectedIndex = -1;
                this.ddlNACounty.Enabled = true;
                this.ddlNACity.Items.Clear();
                this.ddlNACity.DataBind();
                this.ddlNACity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlNACity.SelectedIndex = -1;
                this.ddlNACity.Enabled = false;
                this.ddlNAZip.Items.Clear();
                this.ddlNAZip.DataBind();
                this.ddlNAZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlNAZip.SelectedIndex = -1;
                this.ddlNAZip.Enabled = false;
                this.txtSt1.Text = "";
                this.txtSt1.Enabled = false;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = false;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = false;
                this.lblAddressSuccess.Text = "";
                this.lblAddressSuccess.CssClass = "lblSuccess";
                this.lblAddressSuccess.Visible = false;
                this.btnNAClear.Enabled = true;

                this.ddlNACounty.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void sdsNACounty_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 citID = Convert.ToInt32(this.ddlNACounty.SelectedValue);
                Dictionary<Int32, String> citList = DMG.getCityList(citID);
                this.ddlNACity.Items.Clear();
                this.ddlNACity.DataSource = citList;
                this.ddlNACity.DataTextField = "Value";
                this.ddlNACity.DataValueField = "Key";
                this.ddlNACity.DataBind();
                this.ddlNACity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlNACity.SelectedIndex = -1;
                this.ddlNACity.Enabled = true;
                this.ddlNAZip.Items.Clear();
                this.ddlNAZip.DataBind();
                this.ddlNAZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlNAZip.SelectedIndex = -1;
                this.ddlNAZip.Enabled = false;
                this.txtSt1.Text = "";
                this.txtSt1.Enabled = false;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = false;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = false;
                this.lblAddressSuccess.Text = "";
                this.lblAddressSuccess.CssClass = "lblSuccess";
                this.lblAddressSuccess.Visible = false;
                this.btnNAClear.Enabled = true;

                this.ddlNACity.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlNACity_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cntID = Convert.ToInt32(this.ddlNACounty.SelectedValue);
                Dictionary<Int32, String> zList = DMG.getZipList(cntID);
                this.ddlNAZip.Items.Clear();
                this.ddlNAZip.DataSource = zList;
                this.ddlNAZip.DataTextField = "Value";
                this.ddlNAZip.DataValueField = "Key";
                this.ddlNAZip.DataBind();
                this.ddlNAZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlNAZip.SelectedIndex = -1;
                this.ddlNAZip.Enabled = true;
                this.txtSt1.Text = "";
                this.txtSt1.Enabled = false;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = false;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = false;
                this.lblAddressSuccess.CssClass = "lblSuccess";
                this.lblAddressSuccess.Visible = false;
                this.btnNAClear.Enabled = true;

                this.ddlNAZip.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlNAZip_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtSt1.Text = "";
                this.txtSt1.Enabled = true;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = true;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = true;
                this.lblAddressSuccess.Text = "";
                this.lblAddressSuccess.CssClass = "lblSuccess";
                this.lblAddressSuccess.Visible = false;
                this.btnNAClear.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnNAAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 client = -99;
                Int32 demog = -99;
                Int32 region = -99;
                Int32 sregion = -99;
                Int32 country = -99;
                Int32 state = -99;
                Int32 county = -99;
                Int32 city = -99;
                Int32 zip = -99;
                String title = null;
                String s1 = null;
                String s2 = null;
                String s3 = null;
                Int32 addAddress = -99;

                client = Convert.ToInt32(this.ddlAddressNewClientList.SelectedValue);
                demog = Convert.ToInt32(this.ddlNADemog.SelectedValue);
                region = Convert.ToInt32(this.ddlNAReg.SelectedValue);
                sregion = Convert.ToInt32(this.ddlNASReg.SelectedValue);
                country = Convert.ToInt32(this.ddlNACountry.SelectedValue);
                state = Convert.ToInt32(this.ddlNAState.SelectedValue);
                county = Convert.ToInt32(this.ddlNACounty.SelectedValue);
                city = Convert.ToInt32(this.ddlNACity.SelectedValue);
                zip = Convert.ToInt32(this.ddlNAZip.SelectedValue);
                title = Convert.ToString(this.txtNATitle.Text);
                s1 = Convert.ToString(this.txtSt1.Text);
                s2 = Convert.ToString(this.txtSt2.Text);
                s3 = Convert.ToString(this.txtSt3.Text);

                if (String.IsNullOrEmpty(title) || String.IsNullOrEmpty(s1))
                {
                    btnNAClear_Click(null, null);
                    this.lblAddressSuccess.ForeColor = CLR.Red;
                    this.lblAddressSuccess.Font.Size = 14;
                    this.lblAddressSuccess.Text = "Missing values detected. Please provide all require values before inserting address.";
                    this.lblAddressSuccess.Visible = true;

                    this.ddlAddressNewClientType.Focus();
                }
                else
                {
                    if (String.IsNullOrEmpty(s2))
                    {
                        s2 = "-999";
                    }

                    if (String.IsNullOrEmpty(s3))
                    {
                        s3 = "-999";
                    }

                    addAddress = Convert.ToInt32(CLNT.createAddress(client, demog, region, sregion, country, state, county, city, zip, s1, s2, s3, title, LoginName, domain));

                    if (addAddress.Equals(1))
                    {
                        btnNAClear_Click(null, null);
                        this.lblAddressSuccess.ForeColor = CLR.Green;
                        this.lblAddressSuccess.Font.Size = 14;
                        this.lblAddressSuccess.Text = "Successfully added new Address for the selected Client.";
                        this.lblAddressSuccess.Visible = true;
                    }
                    else
                    {
                        btnNAClear_Click(null, null);
                        this.lblAddressSuccess.ForeColor = CLR.Red;
                        this.lblAddressSuccess.Font.Size = 14;
                        this.lblAddressSuccess.Text = "Could not create New Address for selected Client. Please try inserting New Address again.";
                        this.lblAddressSuccess.Visible = true;
                    }

                    this.ddlAddressNewClientType.Items.Clear();
                    this.ddlAddressNewClientType.DataBind();
                    this.ddlAddressNewClientType.Items.Insert(0, new ListItem("--- Select SMARTs Client Type ---", "-1"));
                    this.ddlAddressNewClientType.SelectedIndex = -1;
                    this.ddlAddressNewClientType.Enabled = true;
                    this.ddlAddressNewClientList.Items.Clear();
                    this.ddlAddressNewClientList.DataBind();
                    this.ddlAddressNewClientList.Items.Insert(0, new ListItem("--- Select SMARTs Client ---", "-1"));
                    this.ddlAddressNewClientList.SelectedIndex = -1;
                    this.ddlAddressNewClientList.Enabled = false;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnNAClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.ddlAddressNewClientType.Items.Clear();
                this.ddlAddressNewClientType.DataBind();
                this.ddlAddressNewClientType.Items.Insert(0, new ListItem("--- Select SMARTs Client Type ---", "-1"));
                this.ddlAddressNewClientType.SelectedIndex = -1;
                this.ddlAddressNewClientList.Items.Clear();
                this.ddlAddressNewClientList.DataBind();
                this.ddlAddressNewClientList.Items.Insert(0, new ListItem("--- Select SMARTs Client ---", "-1"));
                this.ddlAddressNewClientList.SelectedIndex = -1;
                this.ddlAddressNewClientList.Enabled = false;
                this.ddlNADemog.Items.Clear();
                this.ddlNADemog.DataBind();
                this.ddlNADemog.Items.Insert(0, new ListItem("--- Select Address Type ---", "-1"));
                this.ddlNADemog.SelectedIndex = -1;
                this.ddlNADemog.Enabled = false;
                this.txtNATitle.Text = "";
                this.txtNATitle.Enabled = false;
                this.ddlNAReg.Items.Clear();
                this.ddlNAReg.DataBind();
                this.ddlNAReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlNAReg.SelectedIndex = -1;
                this.ddlNAReg.Enabled = false;
                this.ddlNASReg.Items.Clear();
                this.ddlNASReg.DataBind();
                this.ddlNASReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlNASReg.SelectedIndex = -1;
                this.ddlNASReg.Enabled = false;
                this.ddlNACountry.Items.Clear();
                this.ddlNACountry.DataBind();
                this.ddlNACountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlNACountry.SelectedIndex = -1;
                this.ddlNACountry.Enabled = false;
                this.ddlNAState.Items.Clear();
                this.ddlNAState.DataBind();
                this.ddlNAState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlNAState.SelectedIndex = -1;
                this.ddlNAState.Enabled = false;
                this.ddlNACounty.Items.Clear();
                this.ddlNACounty.DataBind();
                this.ddlNACounty.Items.Insert(0, new ListItem("--- Select County/District/Municipality/Division ---", "-1"));
                this.ddlNACounty.SelectedIndex = -1;
                this.ddlNACounty.Enabled = false;
                this.ddlNACity.Items.Clear();
                this.ddlNACity.DataBind();
                this.ddlNACity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlNACity.SelectedIndex = -1;
                this.ddlNACity.Enabled = false;
                this.ddlNAZip.Items.Clear();
                this.ddlNAZip.DataBind();
                this.ddlNAZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlNAZip.SelectedIndex = -1;
                this.ddlNAZip.Enabled = false;
                this.txtSt1.Text = "";
                this.txtSt1.Enabled = false;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = false;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = false;
                this.lblAddressSuccess.Text = "";
                this.lblAddressSuccess.CssClass = "lblSuccess";
                this.lblAddressSuccess.Visible = false;
                this.btnNAAdd.Enabled = false;

                this.ddlAddressNewClientList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnNADone_Click(object sender, EventArgs e)
        {
            try
            {
                btnNAClear_Click(null, null);
                this.tblAddressAdd.Visible = false;
                this.tblAddressList.Visible = true;
                this.ddlAddressClientType.Items.Clear();
                this.ddlAddressClientType.DataBind();
                this.ddlAddressClientType.Items.Insert(0, new ListItem("--- Select Client Name ---", "-1"));
                this.ddlAddressClientType.SelectedIndex = -1;
                this.btnNADone.Visible = false;

                this.ddlAddressClientType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewImage_Click(object sender, EventArgs e)
        {
            try
            {
                btnCancelImageList_Click(null, null);
                this.tblImageList.Visible = false;
                this.tblAddImage.Visible = true;
                this.tblAddImage.Visible = true;
                btnCancelImageList_Click(null, null);
                this.btnImageAdd.Visible = true;
                this.btnImageClear.Visible = true;
                this.btnImageDone.Visible = true;
                this.btnImageDone.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCancelImageList_Click(object sender, EventArgs e)
        {
            try
            {
                this.ddlImageClientType.Items.Clear();
                this.ddlImageClientType.DataBind();
                this.ddlImageClientType.Items.Insert(0, new ListItem("--- Select Type of System Client ---", "-1"));
                this.ddlImageClientType.SelectedIndex = -1;
                this.ddlImageClientList.Items.Clear();
                this.ddlImageClientList.DataBind();
                this.ddlImageClientList.Items.Insert(0, new ListItem("--- Select System Client ---", "-1"));
                this.ddlImageClientList.SelectedIndex = -1;
                this.ddlImageClientList.Enabled = false;
                this.gvwImageList.DataBind();
                this.gvwImageList.Visible = false;
                this.btnCancelImageList.Visible = false;

                this.ddlImageClientType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnImageAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                Int32 clntID = -99;
                Int32 iTypeID = -99;
                Int32 iClrID = -99;
                Int32 iPurpID = -99;
                String fileName = null;
                String fileExtension = null;
                Double fileSize = -99.00;
                HttpPostedFile pstdFile = null;

                TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                clntID = Convert.ToInt32(this.ddlAddLogoClient.SelectedValue);
                iTypeID = Convert.ToInt32(this.ddlAddImageType.SelectedValue);
                iClrID = Convert.ToInt32(this.ddlAddImageColor.SelectedValue);
                iPurpID = Convert.ToInt32(this.ddlAddImagePurpose.SelectedValue);

                if (FileUploadControl.HasFile)
                {
                    pstdFile = FileUploadControl.PostedFile;
                    fileName = Path.GetFileName(pstdFile.FileName);
                    fileExtension = Path.GetExtension(FileUploadControl.FileName);
                    fileSize = pstdFile.ContentLength;

                    if (String.IsNullOrEmpty(fileName))
                    {
                        btnImageClear_Click(null, null);
                        this.lblImageSuccess.ForeColor = CLR.Red;
                        this.lblImageSuccess.Font.Size = 14;
                        this.lblImageSuccess.Text = "Please Select a Client and upload a Logo file before clicking Add New Image button";
                        this.lblImageSuccess.Visible = true;
                        this.FileUploadControl.Dispose();
                    }
                    else
                    {
                        if (fileSize > 5000000)
                        {
                            btnImageClear_Click(null, null);
                            this.lblImageSuccess.ForeColor = CLR.Red;
                            this.lblImageSuccess.Font.Size = 14;
                            this.lblImageSuccess.Text = "Maximum file size exceeded. Please upload Image file of less than 5 MB.";
                            this.lblImageSuccess.Visible = true;
                            this.FileUploadControl.Dispose();
                        }
                        else
                        {
                            if (fileExtension.ToLower().Equals(".jpg") || fileExtension.ToLower().Equals(".jpeg") || fileExtension.ToLower().Equals(".png") || fileExtension.ToLower().Equals(".gif"))
                            {
                                btnImageClear_Click(null, null);
                                this.tblAddImage.Visible = false;
                                Int32 newSize = ((Int32)fileSize / 1024);

                                Stream contentStream = pstdFile.InputStream;
                                BinaryReader contentReader = new BinaryReader(contentStream);
                                byte[] contentBytes = contentReader.ReadBytes((Int32)contentStream.Length);

                                iResult = CLNT.createImageFile(fileName, iTypeID, newSize, iClrID, iPurpID, contentBytes, clntID);
                                if (iResult.Equals(1))
                                {
                                    this.lblImageSuccess.ForeColor = CLR.Green;
                                    this.lblImageSuccess.Font.Size = 14;
                                    this.lblImageSuccess.Text = "Successfully uploaded Logo file for selected client.";
                                    this.lblImageSuccess.Visible = true;
                                    this.FileUploadControl.Dispose();
                                }
                                else
                                {
                                    btnImageClear_Click(null, null);
                                    this.lblImageSuccess.ForeColor = CLR.Red;
                                    this.lblImageSuccess.Font.Size = 14;
                                    this.lblImageSuccess.Text = "Unknown Error. Please try again.";
                                    this.lblImageSuccess.Visible = true;
                                    this.FileUploadControl.Dispose();
                                }
                            }
                            else
                            {
                                btnImageClear_Click(null, null);
                                this.lblImageSuccess.ForeColor = CLR.Red;
                                this.lblImageSuccess.Font.Size = 14;
                                this.lblImageSuccess.Text = "Only Image files of type jpg, jpeg, png or gif are allowed. Please upload the correct Logo file type.";
                                this.lblImageSuccess.Visible = true;
                                this.FileUploadControl.Dispose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnImageClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.ddlAddLogoClientType.Items.Clear();
                this.ddlAddLogoClientType.DataBind();
                this.ddlAddLogoClientType.Items.Insert(0, new ListItem("--- Select Type of System Client ---", "-1"));
                this.ddlAddLogoClientType.SelectedIndex = -1;
                this.ddlAddLogoClientType.Enabled = true;
                this.ddlAddLogoClient.Items.Clear();
                this.ddlAddLogoClient.DataBind();
                this.ddlAddLogoClient.Items.Insert(0, new ListItem("--- Select System Client ---", "-1"));
                this.ddlAddLogoClient.SelectedIndex = -1;
                this.ddlAddLogoClient.Enabled = false;
                this.ddlAddImagePurpose.Items.Clear();
                this.ddlAddImagePurpose.DataBind();
                this.ddlAddImagePurpose.Items.Insert(0, new ListItem("--- Select Image Purpose/Usage ---", "-1"));
                this.ddlAddImagePurpose.SelectedIndex = -1;
                this.ddlAddImagePurpose.Enabled = false;
                this.ddlAddImageColor.Items.Clear();
                this.ddlAddImageColor.DataBind();
                this.ddlAddImageColor.Items.Insert(0, new ListItem("--- Select Image Color Style ---", "-1"));
                this.ddlAddImageColor.SelectedIndex = -1;
                this.ddlAddImageColor.Enabled = false;
                this.ddlAddImageType.Items.Clear();
                this.ddlAddImageType.DataBind();
                this.ddlAddImageType.Items.Insert(0, new ListItem("--- Select Image Type ---", "-1"));
                this.ddlAddImageType.SelectedIndex = -1;
                this.ddlAddImageType.Enabled = false;
                this.ddlAddLogoClient.Enabled = false;
                this.FileUploadControl.Enabled = false;
                this.FileUploadControl.Dispose();
                this.lblImageSuccess.Text = null;
                this.lblImageSuccess.CssClass = "lblSuccess";
                this.lblImageSuccess.Visible = false;

                this.ddlAddLogoClientType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnImageDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnImageClear_Click(null, null);
                btnCancelImageList_Click(null, null);
                this.tblAddImage.Visible = false;
                this.tblImageList.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlImageClientType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctID = Convert.ToInt32(this.ddlImageClientType.SelectedValue);
                Dictionary<Int32, String> tcList = CLNT.getTypeClientList(ctID);
                this.ddlImageClientList.Items.Clear();
                this.ddlImageClientList.DataSource = tcList;
                this.ddlImageClientList.DataTextField = "Value";
                this.ddlImageClientList.DataValueField = "Key";
                this.ddlImageClientList.DataBind();
                this.ddlImageClientList.Items.Insert(0, new ListItem("--- Select System Client ---", "-1"));
                this.ddlImageClientList.SelectedIndex = -1;
                this.ddlImageClientList.Enabled = true;
                this.btnCancelImageList.Visible = true;

                this.ddlImageClientList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlImageClientList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 clID = Convert.ToInt32(this.ddlImageClientList.SelectedValue);
                System.Data.DataTable imgTable = CLNT.getClientImageTable(clID);
                this.gvwImageList.DataSource = imgTable;
                this.gvwImageList.DataBind();
                this.gvwImageList.Visible = true;
                this.gvwImageList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAddLogoClientType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctID = Convert.ToInt32(this.ddlAddLogoClientType.SelectedValue);
                Dictionary<Int32, String> tcList = CLNT.getTypeClientList(ctID);
                this.ddlAddLogoClient.Items.Clear();
                this.ddlAddLogoClient.DataSource = tcList;
                this.ddlAddLogoClient.DataTextField = "Value";
                this.ddlAddLogoClient.DataValueField = "Key";
                this.ddlAddLogoClient.DataBind();
                this.ddlAddLogoClient.Items.Insert(0, new ListItem("--- Select System Client ---", "-1"));
                this.ddlAddLogoClient.SelectedIndex = -1;
                this.ddlAddLogoClient.Enabled = true;
                this.ddlAddImagePurpose.Items.Clear();
                this.ddlAddImagePurpose.DataBind();
                this.ddlAddImagePurpose.Items.Insert(0, new ListItem("--- Select Image Purpose/Usage ---", "-1"));
                this.ddlAddImagePurpose.SelectedIndex = -1;
                this.ddlAddImagePurpose.Enabled = false;
                this.ddlAddImageColor.Items.Clear();
                this.ddlAddImageColor.DataBind();
                this.ddlAddImageColor.Items.Insert(0, new ListItem("--- Select Image Color Style ---", "-1"));
                this.ddlAddImageColor.SelectedIndex = -1;
                this.ddlAddImageColor.Enabled = false;
                this.ddlAddImageType.Items.Clear();
                this.ddlAddImageType.DataBind();
                this.ddlAddImageType.Items.Insert(0, new ListItem("--- Select Image Type ---", "-1"));
                this.ddlAddImageType.SelectedIndex = -1;
                this.ddlAddImageType.Enabled = false;
                this.FileUploadControl.Enabled = false;
                this.btnImageAdd.Enabled = false;

                this.btnImageClear.Enabled = true;

                this.ddlAddLogoClient.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAddLogoClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> pList = DMG.getImagePurposeList();
                this.ddlAddImagePurpose.Items.Clear();
                this.ddlAddImagePurpose.DataSource = pList;
                this.ddlAddImagePurpose.DataTextField = "Value";
                this.ddlAddImagePurpose.DataValueField = "Key";
                this.ddlAddImagePurpose.DataBind();
                this.ddlAddImagePurpose.Items.Insert(0, new ListItem("--- Select Image Purpose/Usage ---", "-1"));
                this.ddlAddImagePurpose.SelectedIndex = -1;
                this.ddlAddImagePurpose.Enabled = true;
                this.ddlAddImageColor.Items.Clear();
                this.ddlAddImageColor.DataBind();
                this.ddlAddImageColor.Items.Insert(0, new ListItem("--- Select Image Color Style ---", "-1"));
                this.ddlAddImageColor.SelectedIndex = -1;
                this.ddlAddImageColor.Enabled = false;
                this.ddlAddImageType.Items.Clear();
                this.ddlAddImageType.DataBind();
                this.ddlAddImageType.Items.Insert(0, new ListItem("--- Select Image Type ---", "-1"));
                this.ddlAddImageType.SelectedIndex = -1;
                this.ddlAddImageType.Enabled = false;
                this.FileUploadControl.Enabled = false;
                this.btnImageAdd.Enabled = false;

                this.ddlAddImagePurpose.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAddImagePurpose_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> clrList = DMG.getImageColorList();
                this.ddlAddImageColor.Items.Clear();
                this.ddlAddImageColor.DataSource = clrList;
                this.ddlAddImageColor.DataTextField = "Value";
                this.ddlAddImageColor.DataValueField = "Key";
                this.ddlAddImageColor.DataBind();
                this.ddlAddImageColor.Items.Insert(0, new ListItem("--- Select Image Color Style ---", "-1"));
                this.ddlAddImageColor.SelectedIndex = -1;
                this.ddlAddImageColor.Enabled = true;
                this.ddlAddImageType.Items.Clear();
                this.ddlAddImageType.DataBind();
                this.ddlAddImageType.Items.Insert(0, new ListItem("--- Select Image Type ---", "-1"));
                this.ddlAddImageType.SelectedIndex = -1;
                this.ddlAddImageType.Enabled = false;
                this.FileUploadControl.Enabled = false;
                this.btnImageAdd.Enabled = false;

                this.ddlAddImageColor.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAddImageColor_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> typList = DMG.getImageTypeList();
                this.ddlAddImageType.Items.Clear();
                this.ddlAddImageType.DataSource = typList;
                this.ddlAddImageType.DataTextField = "Value";
                this.ddlAddImageType.DataValueField = "Key";
                this.ddlAddImageType.DataBind();
                this.ddlAddImageType.Items.Insert(0, new ListItem("--- Select Image Type ---", "-1"));
                this.ddlAddImageType.SelectedIndex = -1;
                this.ddlAddImageType.Enabled = true;
                this.FileUploadControl.Enabled = false;
                this.btnImageAdd.Enabled = false;

                this.ddlAddImageType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAddImageType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.FileUploadControl.Enabled = true;
                this.btnImageAdd.Enabled = true;

                this.ddlAddImageType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlClientType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 typeIT = Convert.ToInt32(this.ddlClientType.SelectedValue);
                DataTable clntList = CLNT.getClientsTable(typeIT);
                this.gvwClientList.DataSource = clntList;
                this.gvwClientList.DataBind();
                this.gvwClientList.Visible = true;
                this.btnClientListCancel.Visible = true;

                this.gvwClientList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnClientDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnAddNewClientCancel_Click(null, null);
                this.tblClientList.Visible = true;
                this.tblClientAdd.Visible = false;
                this.btnClientDone.Visible = false;
                Dictionary<Int32, String> clientTypes = CLNT.getClientTypelist();
                this.ddlClientType.Items.Clear();
                this.ddlClientType.DataSource = clientTypes;
                this.ddlClientType.DataTextField = "Value";
                this.ddlClientType.DataValueField = "Key";
                this.ddlClientType.DataBind();
                this.ddlClientType.Items.Insert(0, new ListItem("--- Select Type of System Client ---", "-1"));
                this.ddlClientType.SelectedIndex = -1;

                this.ddlClientType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewMapping_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                Int32 cntID = -99;
                String dmn = null;
                String dbNam = null;
                String dbCon = null;

                cntID = Convert.ToInt32(this.ddlAddMappingClient.SelectedValue);
                dmn = Convert.ToString(this.txtClientRealm.Text);
                dbNam = Convert.ToString(this.ddlDatabaseNameList.SelectedItem);
                dbCon = CLNT.getDBCon(dbNam);

                iResult = CLNT.createDBMap(cntID, dmn, dbNam, dbCon, LoginName, domain);
                if (iResult.Equals(1))
                {
                    btnAddNewMappingClear_Click(null, null);
                    this.lblMappingSuccess.ForeColor = CLR.Green;
                    this.lblMappingSuccess.Font.Size = 14;
                    this.lblMappingSuccess.Text = "Successfully created Client-to-Database mapping.";
                    this.lblMappingSuccess.Visible = true;
                }
                else
                {
                    btnAddNewMappingClear_Click(null, null);
                    this.lblMappingSuccess.ForeColor = CLR.Red;
                    this.lblMappingSuccess.Font.Size = 14;
                    this.lblMappingSuccess.Text = "System Error. Please try again.";
                    this.lblMappingSuccess.Visible = true;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewMappingClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.ddlAddMappingClient.Items.Clear();
                this.ddlAddMappingClient.DataBind();
                this.ddlAddMappingClient.Items.Insert(0, new ListItem("--- Select System Client ---", "-1"));
                this.ddlAddMappingClient.SelectedIndex = -1;
                this.txtClientRealm.Text = null;
                this.txtClientRealm.Enabled = false;
                this.ddlDatabaseNameList.Items.Clear();
                this.ddlDatabaseNameList.DataBind();
                this.ddlDatabaseNameList.Items.Insert(0, new ListItem("--- Select Database Name ---", "-1"));
                this.ddlDatabaseNameList.SelectedIndex = -1;
                this.ddlDatabaseNameList.Enabled = false;
                this.btnAddNewMapping.Enabled = false;
                this.lblMappingSuccess.Text = null;
                this.lblMappingSuccess.CssClass = "lblSuccess";
                this.lblMappingSuccess.Visible = false;

                this.ddlAddMappingClient.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewMappingDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.tblClientDBList.Visible = true;
                btnAddNewMappingClear_Click(null, null);
                this.tblClientDBAdd.Visible = false;
                this.btnAddNewMappingDone.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAPNewAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                Int32 contractID = -99;
                Int32 serviceID = -99;
                Int32 chargeID = -99;
                Decimal price = -99.0000M;

                contractID = Convert.ToInt32(this.ddlAPContract.SelectedValue);
                serviceID = Convert.ToInt32(this.ddlAPService.SelectedValue);
                price = Convert.ToDecimal(this.txtAPSrvPrice.Text);
                chargeID = Convert.ToInt32(this.ddlAPChargeUnit.SelectedValue);

                iResult = CTRT.addPrice(contractID, serviceID, price, chargeID, LoginName, domain);
                if (iResult.Equals(1))
                {
                    btnAPNewClear_Click(null, null);
                    this.lblAPSuccess.ForeColor = CLR.Green;
                    this.lblAPSuccess.Text = "Successfully inserted Price for selected Service and Contract.";
                    this.lblAPSuccess.Visible = true;
                }
                else
                {
                    btnAPNewClear_Click(null, null);
                    this.lblAPSuccess.ForeColor = CLR.Red;
                    this.lblAPSuccess.Text = "Unable to insert Price for selected Service and Contract. Please try again.";
                    this.lblAPSuccess.Visible = true;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAPNewClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.ddlAPContract.Items.Clear();
                this.ddlAPContract.DataBind();
                this.ddlAPContract.Items.Insert(0, new ListItem("--- Select Client Contract ---", "-1"));
                this.ddlAPContract.SelectedIndex = -1;
                this.ddlAPServiceGroup.Items.Clear();
                this.ddlAPServiceGroup.DataBind();
                this.ddlAPServiceGroup.Items.Insert(0, new ListItem("--- Select Service Group ---", "-1"));
                this.ddlAPServiceGroup.SelectedIndex = -1;
                this.ddlAPServiceGroup.Enabled = false;
                this.ddlAPService.Items.Clear();
                this.ddlAPService.DataBind();
                this.ddlAPService.Items.Insert(0, new ListItem("--- Select Service ---", "-1"));
                this.ddlAPService.SelectedIndex = -1;
                this.ddlAPService.Enabled = false;
                this.txtAPSrvPrice.Text = null;
                this.txtAPSrvPrice.Enabled = false;
                this.ddlAPChargeUnit.Items.Clear();
                this.ddlAPChargeUnit.DataBind();
                this.ddlAPChargeUnit.Items.Insert(0, new ListItem("--- Select Service Charge Unit ---", "-1"));
                this.ddlAPChargeUnit.SelectedIndex = -1;
                this.ddlAPChargeUnit.Enabled = false;
                this.lblAPSuccess.Text = null;
                this.lblAPSuccess.CssClass = "lblSuccess";
                this.lblAPSuccess.Visible = false;
                this.btnAPNewAdd.Enabled = false;
                this.btnAPNewClear.Enabled = false;

                this.ddlAPContract.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAPNewDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnAPNewClear_Click(null, null);
                this.tblServicePrices.Visible = true;
                this.tblAddPrice.Visible = false;
                this.btnAPNewDone.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAPContract_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> grpList = SRV.getSrvGroupList();
                this.ddlAPServiceGroup.Items.Clear();
                this.ddlAPServiceGroup.DataSource = grpList;
                this.ddlAPServiceGroup.DataTextField = "Value";
                this.ddlAPServiceGroup.DataValueField = "Key";
                this.ddlAPServiceGroup.DataBind();
                this.ddlAPServiceGroup.Items.Insert(0, new ListItem("--- Select Service Group ---", "-1"));
                this.ddlAPServiceGroup.SelectedIndex = -1;
                this.ddlAPServiceGroup.Enabled = true;
                this.ddlAPService.Items.Clear();
                this.ddlAPService.DataBind();
                this.ddlAPService.Items.Insert(0, new ListItem("--- Select Service ---", "-1"));
                this.ddlAPService.SelectedIndex = -1;
                this.ddlAPService.Enabled = false;
                this.txtAPSrvPrice.Text = null;
                this.txtAPSrvPrice.Enabled = false;
                this.ddlAPChargeUnit.Items.Clear();
                this.ddlAPChargeUnit.DataBind();
                this.ddlAPChargeUnit.Items.Insert(0, new ListItem("--- Select Service Charge Unit ---", "-1"));
                this.ddlAPChargeUnit.SelectedIndex = -1;
                this.ddlAPChargeUnit.Enabled = false;
                this.lblAPSuccess.Text = null;
                this.lblAPSuccess.CssClass = "genLabel";
                this.lblAPSuccess.Visible = false;
                this.btnAPNewAdd.Enabled = false;
                this.btnAPNewClear.Enabled = true;

                this.ddlAPServiceGroup.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAPServiceGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 sgID = Convert.ToInt32(this.ddlAPServiceGroup.SelectedValue);
                Dictionary<Int32, String> svList = SRV.getServiceList(sgID);
                this.ddlAPService.Items.Clear();
                this.ddlAPService.DataSource = svList;
                this.ddlAPService.DataTextField = "Value";
                this.ddlAPService.DataValueField = "Key";
                this.ddlAPService.DataBind();
                this.ddlAPService.Items.Insert(0, new ListItem("--- Select Service ---", "-1"));
                this.ddlAPService.SelectedIndex = -1;
                this.ddlAPService.Enabled = true;
                this.txtAPSrvPrice.Text = null;
                this.txtAPSrvPrice.Enabled = false;
                this.ddlAPChargeUnit.Items.Clear();
                this.ddlAPChargeUnit.DataBind();
                this.ddlAPChargeUnit.Items.Insert(0, new ListItem("--- Select Service Charge Unit ---", "-1"));
                this.ddlAPChargeUnit.SelectedIndex = -1;
                this.ddlAPChargeUnit.Enabled = false;
                this.lblAPSuccess.Text = null;
                this.lblAPSuccess.CssClass = "genLabel";
                this.lblAPSuccess.Visible = false;
                this.btnAPNewAdd.Enabled = false;
                this.btnAPNewClear.Enabled = true;

                this.ddlAPService.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAPService_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> cuList = SRV.getServiceChargeList();
                this.txtAPSrvPrice.Text = null;
                this.txtAPSrvPrice.Enabled = true;
                this.ddlAPChargeUnit.Items.Clear();
                this.ddlAPChargeUnit.DataSource = cuList;
                this.ddlAPChargeUnit.DataTextField = "Value";
                this.ddlAPChargeUnit.DataValueField = "Key";
                this.ddlAPChargeUnit.DataBind();
                this.ddlAPChargeUnit.Items.Insert(0, new ListItem("--- Select Service Charge Unit ---", "-1"));
                this.ddlAPChargeUnit.SelectedIndex = -1;
                this.ddlAPChargeUnit.Enabled = true;
                this.lblAPSuccess.Text = null;
                this.lblAPSuccess.CssClass = "genLabel";
                this.lblAPSuccess.Visible = false;
                this.btnAPNewAdd.Enabled = false;
                this.btnAPNewClear.Enabled = true;

                this.txtAPSrvPrice.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAPChargeUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.lblAPSuccess.Text = null;
                this.lblAPSuccess.CssClass = "genLabel";
                this.lblAPSuccess.Visible = false;
                this.btnAPNewAdd.Enabled = true;
                this.btnAPNewClear.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Client Phone Section
        protected void ddlAPLocType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 dmID = Convert.ToInt32(this.ddlAPLocType.SelectedValue);
                Int32 cnID = Convert.ToInt32(this.ddlAPNumberClient.SelectedValue);
                Dictionary<Int32, String> ttList = CLNT.getClientLocationTitleList(dmID, cnID);
                this.ddlAPLocTitle.Items.Clear();
                this.ddlAPLocTitle.DataSource = ttList;
                this.ddlAPLocTitle.DataTextField = "Value";
                this.ddlAPLocTitle.DataValueField = "Key";
                this.ddlAPLocTitle.DataBind();
                this.ddlAPLocTitle.Items.Insert(0, new ListItem("--- Select Location ---", "-1"));
                this.ddlAPLocTitle.SelectedIndex = -1;
                this.ddlAPLocTitle.Enabled = true;
                this.ddlAPRegion.Items.Clear();
                this.ddlAPRegion.DataBind();
                this.ddlAPRegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlAPRegion.SelectedIndex = -1;
                this.ddlAPRegion.Enabled = false;
                this.ddlAPSubReg.Items.Clear();
                this.ddlAPSubReg.DataBind();
                this.ddlAPSubReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlAPSubReg.SelectedIndex = -1;
                this.ddlAPSubReg.Enabled = false;
                this.ddlAPCountry.Items.Clear();
                this.ddlAPCountry.DataBind();
                this.ddlAPCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlAPCountry.SelectedIndex = -1;
                this.ddlAPCountry.Enabled = false;
                this.ddlAPType.Items.Clear();
                this.ddlAPType.DataBind();
                this.ddlAPType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlAPType.SelectedIndex = -1;
                this.ddlAPType.Enabled = false;
                this.txtAPPhone.Text = "";
                this.txtAPPhone.Enabled = false;
                this.txtAPExtension.Text = "";
                this.txtAPExtension.Enabled = false;
                this.lblAPSuccess.Text = "";
                this.lblAPSuccess.CssClass = "lblSuccess";
                this.lblAPSuccess.Visible = false;
                this.ddlAPLocType.Enabled = true;
                this.btnAddPhone.Enabled = false;

                this.ddlAPLocTitle.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAPLocTitle_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> rgList = DMG.getRegionList();
                this.ddlAPRegion.Items.Clear();
                this.ddlAPRegion.DataSource = rgList;
                this.ddlAPRegion.DataTextField = "Value";
                this.ddlAPRegion.DataValueField = "Key";
                this.ddlAPRegion.DataBind();
                this.ddlAPRegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlAPRegion.SelectedIndex = -1;
                this.ddlAPRegion.Enabled = true;
                this.ddlAPSubReg.Items.Clear();
                this.ddlAPSubReg.DataBind();
                this.ddlAPSubReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlAPSubReg.SelectedIndex = -1;
                this.ddlAPSubReg.Enabled = false;
                this.ddlAPCountry.Items.Clear();
                this.ddlAPCountry.DataBind();
                this.ddlAPCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlAPCountry.SelectedIndex = -1;
                this.ddlAPCountry.Enabled = false;
                this.ddlAPType.Items.Clear();
                this.ddlAPType.DataBind();
                this.ddlAPType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlAPType.SelectedIndex = -1;
                this.ddlAPType.Enabled = false;
                this.txtAPPhone.Text = "";
                this.txtAPPhone.Enabled = false;
                this.txtAPExtension.Text = "";
                this.txtAPExtension.Enabled = false;
                this.lblAPSuccess.Text = "";
                this.lblAPSuccess.CssClass = "lblSuccess";
                this.lblAPSuccess.Visible = false;
                this.ddlAPLocType.Enabled = true;
                this.btnAddPhone.Enabled = false;

                this.ddlAPRegion.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAPRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 rID = Convert.ToInt32(this.ddlAPRegion.SelectedValue);
                Dictionary<Int32, String> srList = DMG.getSubRegionList(rID);
                this.ddlAPSubReg.Items.Clear();
                this.ddlAPSubReg.DataSource = srList;
                this.ddlAPSubReg.DataTextField = "Value";
                this.ddlAPSubReg.DataValueField = "Key";
                this.ddlAPSubReg.DataBind();
                this.ddlAPSubReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlAPSubReg.SelectedIndex = -1;
                this.ddlAPSubReg.Enabled = true;
                this.ddlAPCountry.Items.Clear();
                this.ddlAPCountry.DataBind();
                this.ddlAPCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlAPCountry.SelectedIndex = -1;
                this.ddlAPCountry.Enabled = false;
                this.ddlAPType.Items.Clear();
                this.ddlAPType.DataBind();
                this.ddlAPType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlAPType.SelectedIndex = -1;
                this.ddlAPType.Enabled = false;
                this.txtAPPhone.Text = "";
                this.txtAPPhone.Enabled = false;
                this.txtAPExtension.Text = "";
                this.txtAPExtension.Enabled = false;
                this.lblAPSuccess.Text = "";
                this.lblAPSuccess.CssClass = "lblSuccess";
                this.lblAPSuccess.Visible = false;
                this.ddlAPLocType.Enabled = true;
                this.ddlAPLocType.Focus();
                this.btnAddPhone.Enabled = false;

                this.ddlAPSubReg.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAPSubReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlAPSubReg.SelectedValue);
                Dictionary<Int32, String> ctyList = DMG.getCountryList(srID);
                this.ddlAPCountry.Items.Clear();
                this.ddlAPCountry.DataSource = ctyList;
                this.ddlAPCountry.DataTextField = "Value";
                this.ddlAPCountry.DataValueField = "Key";
                this.ddlAPCountry.DataBind();
                this.ddlAPCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlAPCountry.SelectedIndex = -1;
                this.ddlAPCountry.Enabled = true;
                this.ddlAPType.Items.Clear();
                this.ddlAPType.DataBind();
                this.ddlAPType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlAPType.SelectedIndex = -1;
                this.ddlAPType.Enabled = false;
                this.txtAPPhone.Text = "";
                this.txtAPPhone.Enabled = false;
                this.txtAPExtension.Text = "";
                this.txtAPExtension.Enabled = false;
                this.lblAPSuccess.Text = "";
                this.lblAPSuccess.CssClass = "lblSuccess";
                this.lblAPSuccess.Visible = false;
                this.ddlAPLocType.Enabled = true;
                this.ddlAPLocType.Focus();
                this.btnAddPhone.Enabled = false;

                this.ddlAPCountry.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAPCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> dmgType = DMG.getDemogTypeList();
                this.ddlAPType.Items.Clear();
                this.ddlAPType.DataSource = dmgType;
                this.ddlAPType.DataTextField = "Value";
                this.ddlAPType.DataValueField = "Key";
                this.ddlAPType.DataBind();
                this.ddlAPType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlAPType.SelectedIndex = -1;
                this.ddlAPType.Enabled = true;
                this.txtAPPhone.Text = "";
                this.txtAPPhone.Enabled = false;
                this.txtAPExtension.Text = "";
                this.txtAPExtension.Enabled = false;
                this.lblAPSuccess.Text = "";
                this.lblAPSuccess.CssClass = "lblSuccess";
                this.lblAPSuccess.Visible = false;
                this.btnAddPhone.Enabled = false;

                this.ddlAPType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAPType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtAPPhone.Text = "";
                this.txtAPPhone.Enabled = true;
                this.txtAPExtension.Text = "";
                this.txtAPExtension.Enabled = true;
                this.lblAPSuccess.Text = "";
                this.lblAPSuccess.CssClass = "lblSuccess";
                this.lblAPSuccess.Visible = false;
                this.ddlAPLocType.Enabled = true;
                this.ddlAPLocType.Focus();
                this.btnAddPhone.Enabled = true;

                this.txtAPPhone.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddPhone_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iReply = 0, clAdd = 0, ctyID = 0, ctypID = 0, ctyDCode = 0;
                String phone = String.Empty, phExt = String.Empty, ctyCode = String.Empty, vPhNum = String.Empty;
                clAdd = Convert.ToInt32(this.ddlAPLocTitle.SelectedValue);
                ctyID = Convert.ToInt32(this.ddlAPCountry.SelectedValue);
                ctypID = Convert.ToInt32(this.ddlAPType.SelectedIndex);
                phone = Convert.ToString(this.txtAPPhone.Text);
                phExt = Convert.ToString(this.txtAPExtension.Text);
                ctyCode = DMG.getCountryLetterCode(ctyID);
                ctyDCode = DMG.getCountryDialCode(ctyID);
                if (string.IsNullOrEmpty(phone))
                {
                    btnAddPhoneCancel_Click(null, null);
                    this.lblPhoneSuccess.Text = "Phone Number can not be empty. Please provide a valid Phone Number";
                    this.lblPhoneSuccess.ForeColor = CLR.Maroon;
                    this.lblPhoneSuccess.Visible = true;
                }
                else
                {                    
                    PH.PhoneNumberUtil phUtil = PH.PhoneNumberUtil.GetInstance();
                    try
                    {
                        phone = String.Format("{0}{1}", ctyDCode, phone);
                        PH.PhoneNumber ctyNumberProto = phUtil.Parse(phone, "US");
                        Boolean isValid = phUtil.IsValidNumber(ctyNumberProto);
                        vPhNum = phUtil.Format(ctyNumberProto, PH.PhoneNumberFormat.INTERNATIONAL);
                        if (isValid)
                        {
                            iReply = DMG.addClientPhoneToAddress(clAdd, vPhNum, ctypID, LoginName, domain);
                            if (iReply.Equals(1))
                            {
                                this.lblPhoneSuccess.Text = "Successfully inserted Phone Number for selected Location";
                                this.lblPhoneSuccess.ForeColor = CLR.Green;
                                this.lblPhoneSuccess.Visible = true;
                            }
                            else
                            {
                                this.lblPhoneSuccess.Text = "Unable to insert Phone Number for selected Location. Please try again.";
                                this.lblPhoneSuccess.ForeColor = CLR.Red;
                                this.lblPhoneSuccess.Visible = true;
                            }
                        }
                        else
                        {
                            this.lblPhoneSuccess.Text = "Successfully inserted Phone Number for selected Location";
                            this.lblPhoneSuccess.ForeColor = CLR.Green;
                            this.lblPhoneSuccess.Visible = true;
                        }
                    }
                    catch (PH.NumberParseException ex)
                    { throw new System.Exception(ex.ToString()); }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddPhoneCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> cTypes = CLNT.getClientTypelist();
                this.ddlAPNumberClientType.Items.Clear();
                this.ddlAPNumberClientType.DataSource = cTypes;
                this.ddlAPNumberClientType.DataTextField = "Value";
                this.ddlAPNumberClientType.DataValueField = "Key";
                this.ddlAPNumberClientType.DataBind();
                this.ddlAPNumberClientType.Items.Insert(0, new ListItem("--- Select Client Type ---", "-1"));
                this.ddlAPNumberClientType.SelectedIndex = -1;
                this.ddlAPNumberClient.Items.Clear();
                this.ddlAPNumberClient.DataBind();
                this.ddlAPNumberClient.Items.Insert(0, new ListItem("--- Select SMARTs Client ---", "-1"));
                this.ddlAPNumberClient.SelectedIndex = -1;
                this.ddlAPNumberClient.Enabled = false;
                this.ddlAPLocType.Items.Clear();
                this.ddlAPLocType.DataBind();
                this.ddlAPLocType.Items.Insert(0, new ListItem("--- Select Location Type ---", "-1"));
                this.ddlAPLocType.SelectedIndex = -1;
                this.ddlAPLocTitle.Items.Clear();
                this.ddlAPLocTitle.DataBind();
                this.ddlAPLocTitle.Items.Insert(0, new ListItem("--- Select Location ---", "-1"));
                this.ddlAPLocTitle.SelectedIndex = -1;
                this.ddlAPLocTitle.Enabled = false;
                this.ddlAPRegion.Items.Clear();
                this.ddlAPRegion.DataBind();
                this.ddlAPRegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlAPRegion.SelectedIndex = -1;
                this.ddlAPRegion.Enabled = false;
                this.ddlAPSubReg.Items.Clear();
                this.ddlAPSubReg.DataBind();
                this.ddlAPSubReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlAPSubReg.SelectedIndex = -1;
                this.ddlAPSubReg.Enabled = false;
                this.ddlAPCountry.Items.Clear();
                this.ddlAPCountry.DataBind();
                this.ddlAPCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlAPCountry.SelectedIndex = -1;
                this.ddlAPCountry.Enabled = false;
                this.ddlAPType.Items.Clear();
                this.ddlAPType.DataBind();
                this.ddlAPType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlAPType.SelectedIndex = -1;
                this.ddlAPType.Enabled = false;
                this.txtAPPhone.Text = "";
                this.txtAPPhone.Enabled = false;
                this.txtAPExtension.Text = "";
                this.txtAPExtension.Enabled = false;
                this.lblPhoneSuccess.Text = "";
                this.lblPhoneSuccess.CssClass = "lblSuccess";
                this.lblPhoneSuccess.Visible = false;
                this.btnAddPhone.Enabled = false;

                this.ddlAPNumberClientType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddPhoneDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnAddPhoneCancel_Click(null, null);
                this.tblPhoneList.Visible = true;
                this.tblPhoneAdd.Visible = false;
                this.btnAddPhoneDone.Visible = false;

                this.gvwPhoneList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnClearPhone_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> clientTypes = CLNT.getClientTypelist();
                this.ddlPClientType.Items.Clear();
                this.ddlPClientType.DataSource = clientTypes;
                this.ddlPClientType.DataTextField = "Value";
                this.ddlPClientType.DataValueField = "Key";
                this.ddlPClientType.DataBind();
                this.ddlPClientType.Items.Insert(0, new ListItem("--- Select Client Type ---", "-1"));
                this.ddlPClientType.SelectedIndex = -1;
                this.ddlPClientName.Items.Clear();
                this.ddlPClientName.DataBind();
                this.ddlPClientName.Items.Insert(0, new ListItem("--- Select SMART's Client ---", "-1"));
                this.ddlPClientName.SelectedIndex = -1;
                this.ddlPClientName.Enabled = false;
                this.ddlPLocType.Items.Clear();
                this.ddlPLocType.DataBind();
                this.ddlPLocType.Items.Insert(0, new ListItem("--- Select Location Type ---", "-1"));
                this.ddlPLocType.SelectedIndex = -1;
                this.ddlPLocType.Enabled = false;
                this.ddlPLoc.Items.Clear();
                this.ddlPLoc.DataBind();
                this.ddlPLoc.Items.Insert(0, new ListItem("--- Select Location ---", "-1"));
                this.ddlPLoc.SelectedIndex = -1;
                this.ddlPLoc.Enabled = false;
                this.gvwPhoneList.DataBind();
                this.gvwPhoneList.Visible = false;
                this.btnClearPhone.Visible = false;

                this.ddlPClientType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlPLocType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 tpID = Convert.ToInt32(this.ddlPLocType.SelectedValue);
                Int32 cnID = Convert.ToInt32(this.ddlPClientName.SelectedValue);
                Dictionary<Int32, String> ttList = CLNT.getClientLocationTitleList(tpID, cnID);
                this.ddlPLoc.Items.Clear();
                this.ddlPLoc.DataSource = ttList;
                this.ddlPLoc.DataTextField = "Value";
                this.ddlPLoc.DataValueField = "Key";
                this.ddlPLoc.DataBind();
                this.ddlPLoc.Items.Insert(0, new ListItem("--- Select Location ---", "-1"));
                this.ddlPLoc.SelectedIndex = -1;
                this.ddlPLoc.Enabled = true;
                this.gvwPhoneList.DataBind();
                this.gvwPhoneList.Visible = false;
                this.btnClearPhone.Visible = true;

                this.ddlPLoc.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlPLoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 lcID = Convert.ToInt32(this.ddlPLoc.SelectedValue);
                System.Data.DataTable phTable = DMG.getClientPhoneToAddressTable(lcID);
                this.gvwPhoneList.DataSource = phTable;
                this.gvwPhoneList.DataBind();
                this.gvwPhoneList.Visible = true;

                this.gvwPhoneList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewPhone_Click(object sender, EventArgs e)
        {
            try
            {
                btnClearPhone_Click(null, null);
                this.tblPhoneList.Visible = false;
                this.tblPhoneAdd.Visible = true;
                this.btnClearPhone.Visible = false;
                this.btnAddPhoneDone.Visible = true;

                this.ddlAPNumberClientType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlPClientType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cTyp = Convert.ToInt32(this.ddlPClientType.SelectedValue);
                Dictionary<Int32, String> cntList = CLNT.getClientList(cTyp);
                this.ddlPClientName.Items.Clear();
                this.ddlPClientName.DataSource = cntList;
                this.ddlPClientName.DataTextField = "Value";
                this.ddlPClientName.DataValueField = "Key";
                this.ddlPClientName.DataBind();
                this.ddlPClientName.Items.Insert(0, new ListItem("--- Select SMARTs Client ---", "-1"));
                this.ddlPClientName.SelectedIndex = -1;
                this.ddlPClientName.Enabled = true;
                this.ddlPLocType.Items.Clear();
                this.ddlPLocType.DataBind();
                this.ddlPLocType.Items.Insert(0, new ListItem("--- Select Location Type ---", "-1"));
                this.ddlPLocType.SelectedIndex = -1;
                this.ddlPLocType.Enabled = false;
                this.ddlPLoc.Items.Clear();
                this.ddlPLoc.DataBind();
                this.ddlPLoc.Items.Insert(0, new ListItem("--- Select Location ---", "-1"));
                this.ddlPLoc.SelectedIndex = -1;
                this.ddlPLoc.Enabled = false;
                this.gvwPhoneList.DataBind();
                this.btnClearPhone.Visible = true;

                this.btnClearPhone.Visible = true;

                this.ddlPClientName.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlPClientName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cnID = Convert.ToInt32(this.ddlPClientName.SelectedValue);
                Dictionary<Int32, String> locList = DMG.getDemogTypeList();
                this.ddlPLocType.Items.Clear();
                this.ddlPLocType.DataSource = locList;
                this.ddlPLocType.DataTextField = "Value";
                this.ddlPLocType.DataValueField = "Key";
                this.ddlPLocType.DataBind();
                this.ddlPLocType.Items.Insert(0, new ListItem("--- Select Location Type ---", "-1"));
                this.ddlPLocType.SelectedIndex = -1;
                this.ddlPLocType.Enabled = true;
                this.ddlPLoc.Items.Clear();
                this.ddlPLoc.DataBind();
                this.ddlPLoc.Items.Insert(0, new ListItem("--- Select Location ---", "-1"));
                this.ddlPLoc.SelectedIndex = -1;
                this.ddlPLoc.Enabled = false;
                this.gvwPhoneList.DataBind();
                this.ddlPLocType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAPNumberClientType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cTyp = Convert.ToInt32(this.ddlAPNumberClientType.SelectedValue);
                Dictionary<Int32, String> cntList = CLNT.getClientList(cTyp);
                this.ddlAPNumberClient.Items.Clear();
                this.ddlAPNumberClient.DataSource = cntList;
                this.ddlAPNumberClient.DataTextField = "Value";
                this.ddlAPNumberClient.DataValueField = "Key";
                this.ddlAPNumberClient.DataBind();
                this.ddlAPNumberClient.Items.Insert(0, new ListItem("--- Select SMARTs Client ---", "-1"));
                this.ddlAPNumberClient.SelectedIndex = -1;
                this.ddlAPNumberClient.Enabled = true;
                this.ddlAPLocType.Items.Clear();
                this.ddlAPLocType.DataBind();
                this.ddlAPLocType.Items.Insert(0, new ListItem("--- Select Location Type ---", "-1"));
                this.ddlAPLocType.SelectedIndex = -1;
                this.ddlAPLocType.Enabled = false;
                this.ddlAPLocTitle.Items.Clear();
                this.ddlAPLocTitle.DataBind();
                this.ddlAPLocTitle.Items.Insert(0, new ListItem("--- Select Location ---", "-1"));
                this.ddlAPLocTitle.SelectedIndex = -1;
                this.ddlAPLocTitle.Enabled = false;
                this.ddlAPRegion.Items.Clear();
                this.ddlAPRegion.DataBind();
                this.ddlAPRegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlAPRegion.SelectedIndex = -1;
                this.ddlAPRegion.Enabled = false;
                this.ddlAPSubReg.Items.Clear();
                this.ddlAPSubReg.DataBind();
                this.ddlAPSubReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlAPSubReg.SelectedIndex = -1;
                this.ddlAPSubReg.Enabled = false;
                this.ddlAPCountry.Items.Clear();
                this.ddlAPCountry.DataBind();
                this.ddlAPCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlAPCountry.SelectedIndex = -1;
                this.ddlAPCountry.Enabled = false;
                this.ddlAPType.Items.Clear();
                this.ddlAPType.DataBind();
                this.ddlAPType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlAPType.SelectedIndex = -1;
                this.ddlAPType.Enabled = false;
                this.txtAPPhone.Text = "";
                this.txtAPPhone.Enabled = false;
                this.txtAPExtension.Text = "";
                this.txtAPExtension.Enabled = false;
                this.lblAPSuccess.Text = "";
                this.lblAPSuccess.CssClass = "lblSuccess";
                this.lblAPSuccess.Visible = false;
                this.btnAddPhone.Enabled = false;

                this.ddlAPNumberClient.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAPNumberClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cnID = Convert.ToInt32(this.ddlAPNumberClient.SelectedValue);
                Dictionary<Int32, String> locList = CLNT.getClientAddressTypesList(cnID);
                this.ddlAPLocType.Items.Clear();
                this.ddlAPLocType.DataSource = locList;
                this.ddlAPLocType.DataTextField = "Value";
                this.ddlAPLocType.DataValueField = "Key";
                this.ddlAPLocType.DataBind();
                this.ddlAPLocType.Items.Insert(0, new ListItem("--- Select Location Type ---", "-1"));
                this.ddlAPLocType.SelectedIndex = -1;
                this.ddlAPLocType.Enabled = true;
                this.ddlAPLocTitle.Items.Clear();
                this.ddlAPLocTitle.DataBind();
                this.ddlAPLocTitle.Items.Insert(0, new ListItem("--- Select Location ---", "-1"));
                this.ddlAPLocTitle.SelectedIndex = -1;
                this.ddlAPLocTitle.Enabled = false;
                this.ddlAPRegion.Items.Clear();
                this.ddlAPRegion.DataBind();
                this.ddlAPRegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlAPRegion.SelectedIndex = -1;
                this.ddlAPRegion.Enabled = false;
                this.ddlAPSubReg.Items.Clear();
                this.ddlAPSubReg.DataBind();
                this.ddlAPSubReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlAPSubReg.SelectedIndex = -1;
                this.ddlAPSubReg.Enabled = false;
                this.ddlAPCountry.Items.Clear();
                this.ddlAPCountry.DataBind();
                this.ddlAPCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlAPCountry.SelectedIndex = -1;
                this.ddlAPCountry.Enabled = false;
                this.ddlAPType.Items.Clear();
                this.ddlAPType.DataBind();
                this.ddlAPType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlAPType.SelectedIndex = -1;
                this.ddlAPType.Enabled = false;
                this.txtAPPhone.Text = "";
                this.txtAPPhone.Enabled = false;
                this.txtAPExtension.Text = "";
                this.txtAPExtension.Enabled = false;
                this.lblAPSuccess.Text = "";
                this.lblAPSuccess.CssClass = "lblSuccess";
                this.lblAPSuccess.Visible = false;
                this.btnAddPhone.Enabled = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwContractList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 cID = Convert.ToInt32(this.ddlClientCo.SelectedValue);
                System.Data.DataTable ctrtTable = CLNT.getClientContracts(cID);
                this.gvwContractList.PageIndex = e.NewPageIndex;
                this.gvwContractList.DataSource = ctrtTable;
                this.gvwContractList.DataBind();
                this.gvwContractList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwPriceList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 cID = Convert.ToInt32(this.ddlContractList.SelectedValue);
                System.Data.DataTable pTable = CTRT.getPriceListTable(cID);
                this.gvwPriceList.PageIndex = e.NewPageIndex;
                this.gvwPriceList.DataSource = pTable;
                this.gvwPriceList.DataBind();
                this.gvwPriceList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwClientList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 typeIT = Convert.ToInt32(this.ddlClientType.SelectedValue);
                DataTable clntList = CLNT.getClientsTable(typeIT);
                this.gvwClientList.PageIndex = e.NewPageIndex;
                this.gvwClientList.DataSource = clntList;
                this.gvwClientList.DataBind();
                this.gvwClientList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwPhoneList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 lcID = Convert.ToInt32(this.ddlPLoc.SelectedValue);
                System.Data.DataTable phTable = DMG.getClientPhoneToAddressTable(lcID);
                this.gvwPhoneList.PageIndex = e.NewPageIndex;
                this.gvwPhoneList.DataSource = phTable;
                this.gvwPhoneList.DataBind();
                this.gvwPhoneList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwImageList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 clID = Convert.ToInt32(this.ddlImageClientList.SelectedValue);
                System.Data.DataTable imgTable = CLNT.getClientImageTable(clID);
                this.gvwImageList.PageIndex = e.NewPageIndex;
                this.gvwImageList.DataSource = imgTable;
                this.gvwImageList.DataBind();
                this.gvwImageList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwAddressList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 cID = Convert.ToInt32(this.ddlAddressClientList.SelectedValue);
                System.Data.DataTable addTable = CLNT.getAddressTable(cID);
                this.gvwAddressList.PageIndex = e.NewPageIndex;
                this.gvwAddressList.DataSource = addTable;
                this.gvwAddressList.DataBind();
                this.gvwAddressList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}