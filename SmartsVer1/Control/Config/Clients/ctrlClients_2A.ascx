﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlClients_2A.ascx.cs" Inherits="SmartsVer1.Control.Config.Clients.ctrlClients_2A" %>
<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
<!--  Stylesheet -->
<link href="../../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript -->
<script src="../../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<!-- Javascript for Footer -->
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>
<!--- Page local style --->
<style type="text/css" style="background-color: #d9534f; color: #FFFFFF">
    .rtAligned {
        text-align: right;
    }

    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    .sqcLabel {
        min-width: 135px;
        text-align: right;
    }

    .tiLabel {
        min-width: 180px;
        text-align: right;
    }

    .bhaLabel {
        min-width: 170px;
        text-align: right;
    }

    .padLabel {
        min-width: 145px;
        text-align: right;
    }

    .wellLabel {
        min-width: 150px;
        text-align: right;
    }

    .rfLabel {
        min-width: 110px;
        text-align: right;
    }

    .planLabel {
        min-width: 140px;
        text-align:right;
    }

    .padRight {
        padding-right: 25px;
    }

    .btRadio {
        margin-right: 1%;
    }

    .nav a:hover {
        background-color: gray;
    }

    .gridColumnHidden {
        display: none;
    }
</style>
<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom: 2px; padding-left: 2px; padding-top: 2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">(
                            <asp:LoginName ID="LoginName1" runat="server" />
                    )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>
        </asp:LoginView>
    </div>
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>Configuration</li>
                <li><a href="../../../Config/Assets/fecDRAssets_2A.aspx">Configuration</a></li>
            </ol>
        </div>
    </div>
</div>
<!--- Page Content --->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul class="nav nav-pills nav-justified">
                <li class="active"><a data-toggle="pill" href="#clnt">System Clients</a></li>
                <li><a data-toggle="pill" href="#add">Address(es)</a></li>
                <li><a data-toggle="pill" href="#ph">Phone/Fax Numbers</a></li>
                <li><a data-toggle="pill" href="#ctrt">Contracts</a></li>
                <li><a data-toggle="pill" href="#price">Price List</a></li>
                <li><a data-toggle="pill" href="#db">Database Mapping</a></li>
                <li><a data-toggle="pill" href="#img">Images (Logos etc)</a></li>
                <li><a data-toggle="pill" href="#dlim">Depth Limit Threshold</a></li>
            </ul>
            <div class="tab-content">
                <div id="clnt" class="tab-pane fade in active">
                    <asp:UpdateProgress ID="uprgrsClient" runat="server" DisplayAfter="0" DynamicLayout="true" AssociatedUpdatePanelID="upnlClient">
                        <ProgressTemplate>
                            <div id="divClientProcessing" runat="server" class="updateProgress">
                                <asp:Image ID="imgClientProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlClient">
                        <ContentTemplate>
                            <div id="divClientList" runat="server">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h5 class="panel-title"><span>System Client's List</span></h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group">
                                                <asp:LinkButton ID="lnkNewClient" runat="server" CssClass="btn btn-info text-center" OnClick="lnkNewClient_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                                <asp:LinkButton ID="lnkNewClientReset" runat="server" CssClass="btn btn-warning text-center" OnClick="lnkNewClientReset_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="table-responsive">
                                                <div class="input-group">
                                                    <span class="input-group-addon sqcLabel bg-light-blue">Client Type</span>
                                                    <asp:DropDownList ID="ddlClientType" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                        OnSelectedIndexChanged="ddlClientType_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select Type of System Client ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <asp:GridView ID="gvwClientList" runat="server" AutoGenerateColumns="False" DataKeyNames="clntID" AllowPaging="True"
                                                    CssClass="mydatagrid" EmptyDataText="No Clients Found." ShowHeaderWhenEmpty="true"
                                                    Visible="false" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                    RowStyle-CssClass="rows" OnPageIndexChanging="gvwClientList_PageIndexChanging">
                                                    <Columns>
                                                        <asp:BoundField DataField="clntID" HeaderText="clntID" Visible="False" />
                                                        <asp:BoundField DataField="clntName" HeaderText="Client Name" />
                                                        <asp:BoundField DataField="clntNotes" HeaderText="Notes" />
                                                        <asp:BoundField DataField="clntRealm" HeaderText="Realm/Domain" />
                                                        <asp:BoundField DataField="clntTaxReg" HeaderText="Tax ID" />
                                                        <asp:BoundField DataField="clntRealm" HeaderText="Realm/Domain" />
                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divClientAdd" runat="server" visible="false">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h5 class="panel-title"><span>Add New SMARTs Client</span></h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Client Type</span>
                                                            <asp:DropDownList ID="ddlAddClientType" runat="server" AppendDataBoundItems="True" AutoPostBack="True"
                                                                CssClass="form-control" OnSelectedIndexChanged="ddlAddClientType_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select New Client Type ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Client Name</span>
                                                            <asp:TextBox runat="server" Text="" ID="txtClntName" CssClass="form-control" Enabled="False" AutoComplete="off" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Client Realm</span>
                                                            <asp:TextBox ID="txtclntRealm" runat="server" Text="" CssClass="form-control" Enabled="False" AutoComplete="off" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Client Tax ID (EIN)</span>
                                                            <asp:TextBox ID="txtclntTaxReg" runat="server" Text="" CssClass="form-control" Enabled="False" AutoComplete="off" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Comments</span>
                                                            <asp:TextBox runat="server" Text="" ID="txtClntNotes" TextMode="MultiLine" CssClass="form-control" Enabled="False"
                                                                AutoComplete="off" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div runat="server" id="enClient" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iClient"><span runat="server" id="sqcClient"></span></i></h4>
                                                            <asp:Label ID="lblClientSuccess" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <asp:LinkButton ID="btnAddNewClient" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnAddNewClient_Click"><i class="fa fa-plus"> Client</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnAddNewClientCancel" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnAddNewClientCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnClientDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnClientDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div id="add" class="tab-pane fade">
                    <asp:UpdateProgress ID="uprgrsAdd" runat="server" DisplayAfter="0" DynamicLayout="true" AssociatedUpdatePanelID="upnlAdd">
                        <ProgressTemplate>
                            <div id="divAddProcessing" runat="server" class="updateProgress">
                                <asp:Image ID="imgAddProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlAdd">
                        <ContentTemplate>
                            <div id="divAddList" runat="server">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h5 class="panel-title"><span>Address List</span></h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group">
                                                <asp:LinkButton ID="lnkNewAddress" runat="server" CssClass="btn btn-info text-center" OnClick="lnkNewAddress_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                                <asp:LinkButton ID="lnkNewAddressReset" runat="server" CssClass="btn btn-warning text-center" OnClick="lnkNewAddressReset_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                            </div>
                                            <div class="table-responsive">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Client Type</span>
                                                            <asp:DropDownList ID="ddlAddressClientType" runat="server" CssClass="form-control" AutoPostBack="True" AppendDataBoundItems="True"
                                                                OnSelectedIndexChanged="ddlAddressClientType_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Client Type ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Client Name</span>
                                                            <asp:DropDownList ID="ddlAddressClientList" runat="server" CssClass="form-control" AutoPostBack="True" AppendDataBoundItems="True"
                                                                Enabled="False" OnSelectedIndexChanged="ddlAddressClientList_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select SMART's Client ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <asp:GridView ID="gvwAddressList" runat="server" AutoGenerateColumns="False" DataKeyNames="clntAddID" AllowPaging="True"
                                                            EmptyDataText="No Address Found." ShowHeaderWhenEmpty="True" CssClass="mydatagrid" Visible="false" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                            PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnPageIndexChanging="gvwAddressList_PageIndexChanging">
                                                            <Columns>
                                                                <asp:BoundField DataField="clntAddID" HeaderText="clntAddID" Visible="False" />
                                                                <asp:BoundField DataField="dmgID" HeaderText="Type" />
                                                                <asp:BoundField DataField="title" HeaderText="Title" />
                                                                <asp:BoundField DataField="st1" HeaderText="Street 1" />
                                                                <asp:BoundField DataField="st2" HeaderText="Street 2" />
                                                                <asp:BoundField DataField="st3" HeaderText="Street 3" />
                                                                <asp:BoundField DataField="cityID" HeaderText="City/Town" />
                                                                <asp:BoundField DataField="cntyID" HeaderText="County/District" />
                                                                <asp:BoundField DataField="stID" HeaderText="State/Province" />
                                                                <asp:BoundField DataField="ctyID" HeaderText="Country/Nation" />
                                                                <asp:BoundField DataField="zipID" HeaderText="Zip/Postal Code" />
                                                                <asp:BoundField DataField="sregID" HeaderText="Sub-Region" />
                                                                <asp:BoundField DataField="regID" HeaderText="Geographic Region" />
                                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" SortExpression="uTime" ReadOnly="True" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divAddAdd" runat="server" visible="false">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h5 class="panel-title"><span>Add New Address</span></h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Client Type</span>
                                                            <asp:DropDownList ID="ddlAddressNewClientType" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                                OnSelectedIndexChanged="ddlAddressNewClientType_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select SMARTs Client Type ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Client</span>
                                                            <asp:DropDownList ID="ddlAddressNewClientList" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                                Enabled="False" OnSelectedIndexChanged="ddlAddressNewClientList_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select SMARTs Client ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Address Type</span>
                                                            <asp:DropDownList ID="ddlNADemog" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                                Enabled="false" OnSelectedIndexChanged="ddlNADemog_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Address Type ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Title</span>
                                                            <asp:TextBox runat="server" ID="txtNATitle" CssClass="form-control" Text="" Enabled="false" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Maj. Region</span>
                                                            <asp:DropDownList ID="ddlNAReg" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control" Enabled="False"
                                                                OnSelectedIndexChanged="ddlNAReg_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Major Geographic Region ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Sub-Region</span>
                                                            <asp:DropDownList ID="ddlNASReg" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control" Enabled="False"
                                                                OnSelectedIndexChanged="ddlNASReg_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Geographic Sub-Region ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Country/Nation</span>
                                                            <asp:DropDownList ID="ddlNACountry" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                                Enabled="False" OnSelectedIndexChanged="ddlNACountry_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Country/Nation ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">State/Province</span>
                                                            <asp:DropDownList ID="ddlNAState" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                                Enabled="False"
                                                                OnSelectedIndexChanged="ddlNAState_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select State/Province ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Country/District</span>
                                                            <asp:DropDownList ID="ddlNACounty" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                                Enabled="False"
                                                                OnSelectedIndexChanged="sdsNACounty_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select County/District/Municipality/Division ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">City/Town</span>
                                                            <asp:DropDownList ID="ddlNACity" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control" Enabled="False"
                                                                OnSelectedIndexChanged="ddlNACity_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select City/Town ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Zip/Postal Code</span>
                                                            <asp:DropDownList ID="ddlNAZip" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control" Enabled="False"
                                                                OnSelectedIndexChanged="ddlNAZip_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Zip/Postal Code ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Street 1</span>
                                                            <asp:TextBox runat="server" Text="" ID="txtSt1" Enabled="false" CssClass="form-control" AutoComplete="off" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Street 2</span>
                                                            <asp:TextBox runat="server" Text="" ID="txtSt2" Enabled="false" CssClass="form-control" AutoComplete="off" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Street 3</span>
                                                            <asp:TextBox runat="server" Text="" ID="txtSt3" Enabled="false" CssClass="form-control" AutoComplete="off" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div runat="server" id="enAddress" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iAdd"><span runat="server" id="spnAdd"></span></i></h4>
                                                            <asp:Label runat="server" ID="lblAddressSuccess" Text="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <asp:LinkButton runat="server" ID="btnNAAdd" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnNAAdd_Click"><i class="fa fa-plus"> New Address</i></asp:LinkButton>
                                                            <asp:LinkButton runat="server" ID="btnNAClear" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnNAClear_Click"><i class="fa fa-waring"> Reset</i></asp:LinkButton>
                                                            <asp:LinkButton runat="server" ID="btnNADone" CssClass="btn btn-danger text-center" OnClick="btnNADone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div id="ph" class="tab-pane fade">
                    <asp:UpdateProgress ID="uprgrsPhone" runat="server" DisplayAfter="0" DynamicLayout="true" AssociatedUpdatePanelID="upnlPhone">
                        <ProgressTemplate>
                            <div id="divPhoneProcessing" runat="server" class="updateProgress">
                                <asp:Image ID="imgPhoneProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlPhone">
                        <ContentTemplate>
                            <div id="divPhList" runat="server">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h5 class="panel-title"><span>Phone/Fax Numbers List</span></h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group">
                                                <asp:LinkButton ID="lnkPhAdd" runat="server" CssClass="btn btn-info text-center" OnClick="btnAddNewPhone_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                                <asp:LinkButton ID="lnkPhReset" runat="server" CssClass="btn btn-warning text-center" OnClick="btnClearPhone_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Client Type</span>
                                                            <asp:DropDownList ID="ddlPClientType" runat="server" CssClass="form-control" AutoPostBack="True" AppendDataBoundItems="True"
                                                                OnSelectedIndexChanged="ddlPClientType_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Client Type ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Client Name</span>
                                                            <asp:DropDownList ID="ddlPClientName" runat="server" CssClass="form-control" AutoPostBack="True" AppendDataBoundItems="True"
                                                                Enabled="False" OnSelectedIndexChanged="ddlPClientName_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select SMARTs Client ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Location Type</span>
                                                            <asp:DropDownList runat="server" ID="ddlPLocType" CssClass="form-control" AutoPostBack="True" AppendDataBoundItems="True"
                                                                Enabled="false" OnSelectedIndexChanged="ddlPLocType_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Location Type ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Location</span>
                                                            <asp:DropDownList runat="server" ID="ddlPLoc" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                Enabled="false" OnSelectedIndexChanged="ddlPLoc_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Location ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <asp:GridView runat="server" ID="gvwPhoneList" CssClass="mydatagrid" DataKeyNames="ptaID"
                                                            AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" EmptyDataText="No Phone Numbers associated with selected Location"
                                                            ShowHeaderWhenEmpty="True" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header"
                                                            PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnPageIndexChanging="gvwPhoneList_PageIndexChanging">
                                                            <Columns>
                                                                <asp:BoundField DataField="ptaID" Visible="false" />
                                                                <asp:BoundField DataField="ptaValue" HeaderText="Phone Number" />
                                                                <asp:BoundField DataField="ptaType" HeaderText="Type" />
                                                                <asp:BoundField HeaderText="Last Updated" DataField="uTime" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divPhAdd" runat="server" visible="false">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h5 class="panel-title"><span>Add New Fax/Phone Number</span></h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Client Type</span>
                                                            <asp:DropDownList ID="ddlAPNumberClientType" runat="server" CssClass="form-control" AutoPostBack="True" AppendDataBoundItems="True"
                                                                OnSelectedIndexChanged="ddlAPNumberClientType_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Client Type ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Client</span>
                                                            <asp:DropDownList ID="ddlAPNumberClient" runat="server" CssClass="form-control" AutoPostBack="True" AppendDataBoundItems="True"
                                                                Enabled="False" OnSelectedIndexChanged="ddlAPNumberClient_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select SMARTs Client ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Location Type</span>
                                                            <asp:DropDownList runat="server" ID="ddlAPLocType" CssClass="form-control" AutoPostBack="True" AppendDataBoundItems="True"
                                                                OnSelectedIndexChanged="ddlAPLocType_SelectedIndexChanged" Enabled="false">
                                                                <asp:ListItem Value="0">--- Select Location Type ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Location</span>
                                                            <asp:DropDownList runat="server" ID="ddlAPLocTitle" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                OnSelectedIndexChanged="ddlAPLocTitle_SelectedIndexChanged" Enabled="false">
                                                                <asp:ListItem Value="0">--- Select Location ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Maj. Region</span>
                                                            <asp:DropDownList runat="server" ID="ddlAPRegion" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="True"
                                                                Enabled="False" OnSelectedIndexChanged="ddlAPRegion_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Major Geographic Region ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Sub-Region</span>
                                                            <asp:DropDownList runat="server" ID="ddlAPSubReg" CssClass="form-control" AutoPostBack="true" Enabled="false" AppendDataBoundItems="True"
                                                                OnSelectedIndexChanged="ddlAPSubReg_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Geographic Sub-Region ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Country/Nation</span>
                                                            <asp:DropDownList runat="server" ID="ddlAPCountry" CssClass="form-control" AutoPostBack="true" Enabled="false" AppendDataBoundItems="true"
                                                                OnSelectedIndexChanged="ddlAPCountry_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Country/Nation ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Phone Type</span>
                                                            <asp:DropDownList runat="server" ID="ddlAPType" CssClass="form-control" AutoPostBack="true" Enabled="false" AppendDataBoundItems="true"
                                                                OnSelectedIndexChanged="ddlAPType_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Phone Type ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Phone/Fax</span>
                                                            <asp:TextBox runat="server" ID="txtAPPhone" CssClass="form-control" Text="" Enabled="False" AutoComplete="off" placeholder="Phone/Fax Number ... (required)" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Extension</span>
                                                            <asp:TextBox runat="server" ID="txtAPExtension" CssClass="form-control" Text="" Enabled="False" AutoComplete="off" placeholder="Extension ... (optional)" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div runat="server" id="enPhone" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iPhone"><span runat="server" id="spnPhone"></span></i></h4>
                                                            <asp:Label runat="server" ID="lblPhoneSuccess" Text="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <asp:LinkButton runat="server" ID="btnAddPhone" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnAddPhone_Click"><i class="fa fa-plus"> Phone/Fax Number</i></asp:LinkButton>
                                                            <asp:LinkButton runat="server" ID="btnAddPhoneCancel" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnAddPhoneCancel_Click"><i class="fa fa-refresh"> Refresh</i></asp:LinkButton>
                                                            <asp:LinkButton runat="server" ID="btnAddPhoneDone" CssClass="btn btn-danger text-center" OnClick="btnAddPhoneDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div id="ctrt" class="tab-pane fade">
                    <asp:UpdateProgress ID="uprgrsContract" runat="server" DisplayAfter="0" DynamicLayout="true" AssociatedUpdatePanelID="upnlContract">
                        <ProgressTemplate>
                            <div id="divContractProcessing" runat="server" class="updateProgress">
                                <asp:Image ID="imgContractProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlContract">
                        <ContentTemplate>
                            <div id="divCtrtList" runat="server">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h5 class="panel-title"><span>Service Contracts List</span></h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group">
                                                <asp:LinkButton ID="btnAddContract" runat="server" CssClass="btn btn-info text-center" OnClick="btnAddContract_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                                <asp:LinkButton ID="btnEditContract" runat="server" CssClass="btn btn-warning text-center" OnClick="btnEditContract_Click"><i class="fa fa-edit"> Edit</i></asp:LinkButton>
                                                <asp:LinkButton ID="btnContractListCancel" runat="server" CssClass="btn btn-warning text-center" OnClick="btnContractListCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Contracted Client</span>
                                                            <asp:DropDownList ID="ddlClientCo" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                                OnSelectedIndexChanged="ddlClientCo_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Contract Client ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <asp:GridView ID="gvwContractList" runat="server" AutoGenerateColumns="False" DataKeyNames="ctrtID" AllowPaging="True" CssClass="mydatagrid"
                                                            EmptyDataText="No Contracts Found." ShowHeaderWhenEmpty="true" Visible="false" OnRowDataBound="gvwContractList_RowDataBound"
                                                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                            SelectedRowStyle-CssClass="selectedrow"
                                                            RowStyle-CssClass="rows" OnPageIndexChanging="gvwContractList_PageIndexChanging">
                                                            <Columns>
                                                                <asp:BoundField DataField="ctrtID" Visible="False" />
                                                                <asp:BoundField DataField="ctID" HeaderText="Type" />
                                                                <asp:BoundField DataField="ctrtName" HeaderText="Name" />
                                                                <asp:BoundField DataField="cstatID" HeaderText="Status" />
                                                                <asp:BoundField DataField="ctrtStart" HeaderText="Start Date" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                                <asp:BoundField DataField="ctrtEnd" HeaderText="End Date" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                                <asp:BoundField DataField="ctrtTTL" HeaderText="Total Days" />
                                                                <asp:BoundField DataField="ctrtRmn" HeaderText="Remaining Days" DataFormatString="{0:0.00}" />
                                                                <asp:BoundField DataField="scID" HeaderText="Sales Campaign" />
                                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divCtrtEdit" runat="server" visible="false">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h5 class="panel-title"><span>Edit/Update Client's Contract</span></h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Contract Type</span>
                                                            <asp:DropDownList ID="ddlEditCttType" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                                OnSelectedIndexChanged="ddlEditCttType_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Contract Type ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Contract Provider</span>
                                                            <asp:DropDownList ID="ddlEditCttProvider" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                                Enabled="false" OnSelectedIndexChanged="ddlEditCttProvider_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Contract Provider ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Contract Partner</span>
                                                            <asp:DropDownList ID="ddlEditCttPartner" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                                Enabled="false" OnSelectedIndexChanged="ddlEditCttPartner_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Contract Partner ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Contract Client</span>
                                                            <asp:DropDownList ID="ddlEditCttClient" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                                Enabled="false" OnSelectedIndexChanged="ddlEditCttClient_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Contract Client ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Contract</span>
                                                            <asp:DropDownList ID="ddlEditCttList" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                                Enabled="false" OnSelectedIndexChanged="ddlEditCttList_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Contract ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Contract Status</span>
                                                            <asp:DropDownList ID="ddlEditCttStatus" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                                Enabled="false" OnSelectedIndexChanged="ddlEditCttStatus_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Contract Status ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div runat="server" id="enContract" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iCtrt"><span runat="server" id="spnCtrt"></span></i></h4>
                                                            <asp:Label ID="lblEditContractSuccess" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <asp:LinkButton ID="btnEditContractUpdate" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnEditContractUpdate_Click"><i class="fa fa-edit"> Update</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnEditContractCancel" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnEditContractCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnEditContractExit" runat="server" CssClass="btn btn-danger text-center" OnClick="btnEditContractExit_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divCtrtAdd" runat="server" visible="false">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h5 class="panel-title"><span>Add New Services/Demo Contract</span></h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Contract Type</span>
                                                            <asp:DropDownList ID="ddlAddContractType" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                                OnSelectedIndexChanged="ddlAddContractType_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Contract Type ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Provider</span>
                                                            <asp:DropDownList ID="ddlAddContractProvider" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                                Enabled="false" OnSelectedIndexChanged="ddlAddContractProvider_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Contract Provider ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Partner</span>
                                                            <asp:DropDownList ID="ddlAddContractPartner" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                                Enabled="false" OnSelectedIndexChanged="ddlAddContractPartner_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Contract Partner ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Client</span>
                                                            <asp:DropDownList ID="ddlAddContractClient" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                                Enabled="false" OnSelectedIndexChanged="ddlAddContractClient_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Contract Client ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Title</span>
                                                            <asp:TextBox ID="txtContractName" runat="server" Text="" CssClass="form-control" Enabled="false" placeholder="Contract Title ... (required)" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Status</span>
                                                            <asp:DropDownList ID="ddlAddContractStatus" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                                Enabled="false" OnSelectedIndexChanged="ddlAddContractStatus_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Contract Status ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Client Rep.</span>
                                                            <asp:TextBox ID="txtCRep" runat="server" Text="" CssClass="form-control" Enabled="false" placeholder="Client Representative Person ... (required)" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Rep. Designation</span>
                                                            <asp:TextBox ID="txtCRepDesig" runat="server" Text="" CssClass="form-control" Enabled="false" placeholder="Client Representative Designation ... (required)" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Start Date</span>
                                                            <asp:TextBox ID="txtStartCalendar" runat="server" CssClass="form-control" Enabled="false" TextMode="Date" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">End Date</span>
                                                            <asp:TextBox ID="txtEndCalendar" runat="server" CssClass="form-control" Enabled="false" TextMode="Date" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Sales Campaign</span>
                                                            <asp:DropDownList ID="ddlContractSC" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                Enabled="false" OnSelectedIndexChanged="ddlContractSC_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Sales Campaign ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div runat="server" id="enAddCtrt" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iAddCtrt"><span runat="server" id="spnAddCtrt"></span></i></h4>
                                                            <asp:Label ID="lblContractSuccess" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="input-group">
                                                        <asp:LinkButton ID="btnAddNewContract" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnAddNewContract_Click"><i class="fa fa-plus"> Contract</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnAddNewContractCancel" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnAddNewContractCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnAddNewContractDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnAddNewContractDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div id="price" class="tab-pane fade">
                    <asp:UpdateProgress ID="uprgrsPrice" runat="server" DisplayAfter="0" DynamicLayout="true" AssociatedUpdatePanelID="upnlPrice">
                        <ProgressTemplate>
                            <div id="divPriceProcessing" runat="server" class="updateProgress">
                                <asp:Image ID="imgPriceProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlPrice">
                        <ContentTemplate>
                            <div id="divPriceList" runat="server">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h5 class="panel-title"><span>Client's Contracted Price List</span></h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group">
                                                <asp:LinkButton ID="btnPriceAdd" runat="server" CssClass="btn btn-info text-center" OnClick="btnPriceAdd_Click"><i class="fa fa-plus"> New</i></asp:LinkButton>
                                                <asp:LinkButton ID="btnPriceCancel" runat="server" CssClass="btn btn-warning text-center" Enabled="false" OnClick="btnPriceCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Contract</span>
                                                            <asp:DropDownList ID="ddlContractList" runat="server" AutoPostBack="True" AppendDataBoundItems="True"
                                                                CssClass="form-control"
                                                                OnSelectedIndexChanged="ddlContractList_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Client Contract ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div runat="server" id="enPriceDelete" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iPDelete"><span runat="server" id="spnPDelete"></span></i></h4>
                                                            <asp:Label ID="lblPDelete" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <asp:GridView ID="gvwPriceList" runat="server" AutoGenerateColumns="False" DataKeyNames="spID" EmptyDataText="No Price List Found for selected Contract"
                                                            ShowHeaderWhenEmpty="True" Visible="false" CssClass="mydatagrid" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                            HeaderStyle-CssClass="header" RowStyle-CssClass="rows" OnPageIndexChanging="gvwPriceList_PageIndexChanging" OnRowDataBound="gvwPriceList_RowDataBound" OnRowDeleting="gvwPriceList_RowDeleting">
                                                            <Columns>
                                                                <asp:BoundField DataField="spID" HeaderText="spID" Visible="False" />
                                                                <asp:BoundField DataField="srvID" HeaderText="Service Name" />
                                                                <asp:BoundField DataField="spCharge" HeaderText="Service Price" />
                                                                <asp:BoundField DataField="scuID" HeaderText="Price Unit" />
                                                                <asp:BoundField DataField="dateThru" HeaderText="Valid Through" />
                                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                                                                <asp:ButtonField CommandName="Delete" Text="Delete" ButtonType="Button" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                                    <ControlStyle CssClass="btn btn-danger text-center"></ControlStyle>
                                                                </asp:ButtonField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divPriceAdd" runat="server" visible="false">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h5 class="panel-title"><span>Add New Contract Price</span></h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Contract</span>
                                                            <asp:DropDownList ID="ddlAPContract" runat="server" AutoPostBack="True" CssClass="form-control"
                                                                AppendDataBoundItems="True" OnSelectedIndexChanged="ddlAPContract_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Client Contract ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Sales Campaign</span>
                                                            <asp:DropDownList ID="ddlSalesCampaign" runat="server" AutoPostBack="True" CssClass="form-control"
                                                                AppendDataBoundItems="True" OnSelectedIndexChanged="ddlSalesCampaign_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Sales Campaign ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <asp:GridView ID="gvwPrices" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                                            DataKeyNames="cpID" AlternatingRowStyle-CssClass="AlternatingRowStyle" HeaderStyle-CssClass="header"
                                                            RowStyle-CssClass="rows" EmptyDataText="No Prices found" ShowHeaderWhenEmpty="true" Visible="false">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Selected" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="priceCheck" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="cpID" Visible="False" />
                                                                <asp:BoundField DataField="srvGrpID" HeaderText="SMARTs Service Group" />
                                                                <asp:TemplateField HeaderStyle-CssClass="gridColumnHidden" ItemStyle-CssClass="gridColumnHidden">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSrvID" runat="server" Text='<%#Eval("srvID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="srvName" HeaderText="SMARTs Service" />
                                                                <asp:BoundField DataField="cpReducer" HeaderText="Price Reducer" />
                                                                <asp:BoundField DataField="stdPrice" HeaderText="Standard Price ( US$ )" />
                                                                <asp:BoundField DataField="proPrice" HeaderText="Campaign Price ( US$ )" ItemStyle-BackColor="LightGreen" />
                                                                <asp:TemplateField HeaderStyle-CssClass="gridColumnHidden" ItemStyle-CssClass="gridColumnHidden">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblScuID" runat="server" Text='<%#Eval("unitID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="priceUnit" HeaderText="Unit" ItemStyle-BackColor="LightGreen" />
                                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div runat="server" id="enPrice" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iPrice"><span runat="server" id="spnPrice"></span></i></h4>
                                                            <asp:Label ID="lblAPSuccess" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <asp:LinkButton ID="btnAPNewAdd" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnAPNewAdd_Click"><i class="fa fa-plus"> Price</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnAPNewClear" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnAPNewClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnAPNewDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnAPNewDone_Click"><i class="fa fa-times-rectrangle"> Exit</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div id="db" class="tab-pane fade">
                    <asp:UpdateProgress ID="uprgrsDB" runat="server" DisplayAfter="0" DynamicLayout="true" AssociatedUpdatePanelID="upnlDB">
                        <ProgressTemplate>
                            <div id="divDBProcessing" runat="server" class="updateProgress">
                                <asp:Image ID="imgDBProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlDB">
                        <ContentTemplate>
                            <div id="divDBList" runat="server">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h5 class="panel-title"><span>Client Database Mapping List</span></h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group">
                                                <asp:LinkButton runat="server" ID="btnMappingAdd" CssClass="btn btn-info text-center" OnClick="btnMappingAdd_Click"><i class="fa fa-plus"> Add</i></asp:LinkButton>
                                                <asp:LinkButton runat="server" ID="btnMappingCancel" CssClass="btn btn-warning text-center" OnClick="btnMappingCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Client</span>
                                                            <asp:DropDownList ID="ddlMappingClientType" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                                OnSelectedIndexChanged="ddlMappingClientType_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Client Type ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Realm</span>
                                                            <asp:DropDownList ID="ddlMappingClientRealm" runat="server" AutoPostBack="True" AppendDataBoundItems="True" Enabled="False"
                                                                CssClass="form-control" OnSelectedIndexChanged="ddlMappingClientRealm_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Client Realm ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div runat="server" id="enMappedDB" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iDB"><span runat="server" id="spnDB"></span></i></h4>
                                                            <asp:Label ID="lblMappedDB" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divDBAdd" runat="server" visible="false">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h5 class="panel-title"><span>Add New Client Database Mapping</span></h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Client</span>
                                                            <asp:DropDownList ID="ddlAddMappingClient" runat="server" AppendDataBoundItems="True" AutoPostBack="True"
                                                                CssClass="form-control" OnSelectedIndexChanged="ddlAddMappingClient_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select System Client ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Client Realm</span>
                                                            <asp:TextBox runat="server" Text="" ID="txtClientRealm" CssClass="form-control" Enabled="False" placeholder="Client Realm ... (required)" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">DB Name</span>
                                                            <asp:DropDownList ID="ddlDatabaseNameList" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control"
                                                                Enabled="False" OnSelectedIndexChanged="ddlDatabaseNameList_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Database Name ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div runat="server" id="enAddDB" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iAddDB"><span runat="server" id="spnAddDB"></span></i></h4>
                                                            <asp:Label runat="server" ID="lblMappingSuccess" Text="" />
                                                        </div>                                                        
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <asp:LinkButton ID="btnAddNewMapping" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnAddNewMapping_Click"><i class="fa fa-plus"> Mapping</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnAddNewMappingClear" runat="server" CssClass="btn btn-default text-center" OnClick="btnAddNewMappingClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnAddNewMappingDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnAddNewMappingDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div id="dlim" class="tab-pane fade">
                    <asp:UpdateProgress ID="uprgrsLim" runat="server" DisplayAfter="0" DynamicLayout="true" AssociatedUpdatePanelID="upnlLim">
                        <ProgressTemplate>
                            <div id="divLimProcessing" runat="server" class="updateProgress">
                                <asp:Image ID="imgLimProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlLim">
                        <ContentTemplate>
                            <div id="divLimitList" runat="server">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h5 class="panel-title"><span>Depth Threshold Limit</span></h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group">
                                                <asp:LinkButton ID="btnNewLimit" runat="server" CssClass="btn btn-info text-center" OnClick="btnNewLimit_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                                <asp:LinkButton ID="btnClearLimit" runat="server" CssClass="btn btn-warning text-center" OnClick="btnClearLimit_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Client Type</span>
                                                            <asp:DropDownList ID="ddlLimitClientType" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                                OnSelectedIndexChanged="ddlLimitClientType_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Type of System Client ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Client</span>
                                                            <asp:DropDownList ID="ddlLimitClient" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                Enabled="false" OnSelectedIndexChanged="ddlLimitClient_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select System Client ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <asp:GridView ID="gvwDepthLimit" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="dptID"
                                                            AllowPaging="True" EmptyDataText="No Images found for selected Client." ShowHeaderWhenEmpty="True" Visible="False" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                            PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnPageIndexChanging="gvwDepthLimit_PageIndexChanging">
                                                            <Columns>
                                                                <asp:BoundField DataField="dptID" HeaderText="dptID" Visible="false" />
                                                                <asp:BoundField DataField="dptLimit" HeaderText="Depth Limit" />
                                                                <asp:BoundField DataField="dptMargin" HeaderText="Target Depth Margin" />
                                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divLimitAdd" runat="server" visible="false">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h5 class="panel-title"><span>Add Depth Threshold Limit</span></h5>
                                    </div>
                                    <div class="panel-body">                                        
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Client Type</span>
                                                            <asp:DropDownList ID="ddlAddLimType" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                                OnSelectedIndexChanged="ddlAddLimType_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Type of System Client ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Client</span>
                                                            <asp:DropDownList ID="ddlAddLimClient" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                Enabled="false" OnSelectedIndexChanged="ddlAddLimClient_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select System Client ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Limit</span>
                                                            <asp:TextBox ID="txtLimit" runat="server" CssClass="form-control" placeholder="Depth Limit ... (required)" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Margin</span>
                                                            <asp:TextBox ID="txtMargin" runat="server" CssClass="form-control" placeholder="Target Margin ... (required)" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div runat="server" id="enLim" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iLim"><span runat="server" id="spnLim"></span></i></h4>
                                                            <asp:Label runat="server" ID="lblLimit" Text="" />
                                                        </div> 
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <asp:LinkButton ID="btnLimitAdd" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnLimitAdd_Click"><i class="fa fa-plus"> Depth Threshold</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnLimitReset" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnLimitReset_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnLimitDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnLimitDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div id="img" class="tab-pane fade">
                    <asp:UpdateProgress ID="uprgrsImg" runat="server" DisplayAfter="0" DynamicLayout="true" AssociatedUpdatePanelID="upnlImg">
                        <ProgressTemplate>
                            <div id="divImgProcessing" runat="server" class="updateProgress">
                                <asp:Image ID="imgImgProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlImg">
                        <ContentTemplate>
                            <div id="divImgList" runat="server">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h5 class="panel-title"><span>Client's Image(s) List</span></h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group">
                                                <asp:LinkButton ID="btnAddNewImage" runat="server" CssClass="btn btn-info text-center" OnClick="btnAddNewImage_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                                <asp:LinkButton ID="btnCancelImageList" runat="server" CssClass="btn btn-warning text-center" OnClick="btnCancelImageList_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Client Type</span>
                                                            <asp:DropDownList ID="ddlImageClientType" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                                OnSelectedIndexChanged="ddlImageClientType_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Type of System Client ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Client</span>
                                                            <asp:DropDownList ID="ddlImageClientList" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                Enabled="false" OnSelectedIndexChanged="ddlImageClientList_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select System Client ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <asp:GridView ID="gvwImageList" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False" DataKeyNames="climgID"
                                                            AllowPaging="True" EmptyDataText="No Images found for selected Client." ShowHeaderWhenEmpty="True" Visible="False" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                            PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnPageIndexChanging="gvwImageList_PageIndexChanging">
                                                            <Columns>
                                                                <asp:BoundField DataField="climgID" HeaderText="climgID" Visible="false" />
                                                                <asp:BoundField DataField="climgFileName" HeaderText="File Name" />
                                                                <asp:BoundField DataField="imgtypID" HeaderText="Type" />
                                                                <asp:BoundField DataField="climgFileSize" HeaderText="File Size (Bytes)" />
                                                                <asp:BoundField DataField="lclrID" HeaderText="Color" />
                                                                <asp:BoundField DataField="imgpID" HeaderText="Purpose" />
                                                                <asp:TemplateField HeaderText="Image" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                                    <ItemTemplate>
                                                                        <asp:Image ID="Image1" runat="server" ImageUrl='<%# "ImageHandler.ashx?ImID="+ Eval("climgID") %>' Height="8%" Width="8%" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divImgAdd" runat="server" visible="false">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h5 class="panel-title"><span>Add New Images (Logos etc)</span></h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Client Type</span>
                                                            <asp:DropDownList ID="ddlAddLogoClientType" runat="server" AutoPostBack="True" AppendDataBoundItems="True"
                                                                CssClass="form-control" OnSelectedIndexChanged="ddlAddLogoClientType_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Type of System Client ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Client</span>
                                                            <asp:DropDownList ID="ddlAddLogoClient" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                                Enabled="false" OnSelectedIndexChanged="ddlAddLogoClient_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select System Client ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Purpose</span>
                                                            <asp:DropDownList ID="ddlAddImagePurpose" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                                Enabled="False" OnSelectedIndexChanged="ddlAddImagePurpose_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Image Purpose/Usage ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Color Style</span>
                                                            <asp:DropDownList ID="ddlAddImageColor" runat="server" AutoPostBack="true" AppendDataBoundItems="true" CssClass="form-control"
                                                                Enabled="false" OnSelectedIndexChanged="ddlAddImageColor_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Image Color Style ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Type</span>
                                                            <asp:DropDownList runat="server" ID="ddlAddImageType" AutoPostBack="true" AppendDataBoundItems="true" CssClass="form-control"
                                                                Enabled="false" OnSelectedIndexChanged="ddlAddImageType_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- Select Image Type ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue">Upload File</span>
                                                            <asp:FileUpload ID="FileUploadControl" runat="server" Enabled="false" CssClass="form-control" Font-Size="Larger" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon sqcLabel bg-light-blue"></span>
                                                            <asp:Label ID="lblFileNotes" runat="server" Text="* Only Image file are allowed (jpg, jpeg, png or gif). Max. File size is 5 MB"
                                                                CssClass="form-control" Font-Size="Smaller" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div runat="server" id="enImage" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iImage"><span runat="server" id="spnImage"></span></i></h4>
                                                            <asp:Label ID="lblImageSuccess" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group">
                                                            <asp:LinkButton ID="btnImageAdd" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnImageAdd_Click"><i class="fa fa-plus"> New Image</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnImageClear" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnImageClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnImageDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnImageDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnImageAdd" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</div>
