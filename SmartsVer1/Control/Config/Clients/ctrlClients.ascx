﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlClients.ascx.cs" Inherits="SmartsVer1.Control.Config.Clients.ctrlClients" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript -->
<script src="../../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>

<!--- Page local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }

    .rotate {
        -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
        -moz-transform: rotate(90deg); /* Firefox */
        -ms-transform: rotate(90deg); /* IE 9 */
        transform: rotate(90deg); /* Standard syntax */
    }

    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper[data-stepper-steps="8"] li {
            width: 12%;
        }

        ol.stepper[data-stepper-steps="9"] li {
            width: 11%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }    
</style>
<!--- Page local script --->
<script type="text/javascript">
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("rotate");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").addClass("rotate");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("rotate");
        });
    });
</script>

<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">        
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>Configuration</li>
                <li><a href="../../../Config/Clients/fesDRClients_2A.aspx"> SMARTs Clients</a></li>
            </ol>
        </div>
    </div>    
</div>

<!-- Page Content -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <asp:Table ID="tblMainDialog" runat="server" Width="100%">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Middle">
                <div id="divClient" runat="server" class="divShadow" style="width: 100%;">
                    <asp:UpdateProgress ID="upgrsClients" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlClients">
                        <ProgressTemplate>
                            <div id="divClnts" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center; z-index: 100;">
                                <asp:Image ID="imgClntProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel ID="upnlClients" runat="server">
                        <ContentTemplate>
                            <asp:Table ID="tblClientList" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2" BackColor="#c0c0c0">
                                        <asp:Label ID="lblClients" runat="server" Text="System Clients" CssClass="genLabel" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblClientType" runat="server" Text="Client Type" CssClass="txtLabel" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:DropDownList ID="ddlClientType" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList"
                                            Width="80%" OnSelectedIndexChanged="ddlClientType_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Type of System Client ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Middle" ColumnSpan="2">
                                        <asp:GridView ID="gvwClientList" runat="server" AutoGenerateColumns="False" DataKeyNames="clntID" AllowPaging="True"
                                            CssClass="mydatagrid" EmptyDataText="No Clients Found." ShowHeaderWhenEmpty="true" Width="100%" OnRowUpdating="gvwClientList_RowUpdating"
                                            Visible="false" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                            RowStyle-CssClass="rows" OnPageIndexChanging="gvwClientList_PageIndexChanging">
                                            <Columns>
                                                <asp:BoundField DataField="clntID" HeaderText="clntID" Visible="False" />
                                                <asp:BoundField DataField="clntName" HeaderText="Client Name" />
                                                <asp:BoundField DataField="clntNotes" HeaderText="Notes" />
                                                <asp:BoundField DataField="clntRealm" HeaderText="Realm/Domain" />
                                                <asp:BoundField DataField="clntTaxReg" HeaderText="Tax ID" />
                                                <asp:BoundField DataField="clntRealm" HeaderText="Realm/Domain" />
                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                                            </Columns>
                                        </asp:GridView>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnClientAdd" runat="server" Text="Add New Client" OnClick="btnClientAdd_Click" CssClass="red btn" Width="85%" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Button ID="btnClientListCancel" runat="server" Text="Clear Selection" OnClick="btnClientListCancel_Click" CssClass="red btn"
                                            Width="25%" Visible="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <asp:Table ID="tblClientAdd" runat="server" Width="100%" Visible="false">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2" BackColor="#c0c0c0">
                                        <asp:Label ID="lblAddClientHead" runat="server" Text="Add New System Client" CssClass="genLabel" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblAddClientType" runat="server" Text="Client Type" CssClass="txtLabel" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:DropDownList ID="ddlAddClientType" runat="server" AppendDataBoundItems="True" AutoPostBack="True"
                                            CssClass="ddlList" Width="85%" OnSelectedIndexChanged="ddlAddClientType_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select New Client Type ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblClntName" runat="server" CssClass="txtLabel" Text="Client Name" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:TextBox runat="server" Text="" ID="txtClntName" CssClass="genTextBox" Width="85%" Enabled="False" AutoComplete="off" />
                                        <asp:RequiredFieldValidator runat="server" ID="rfvClntName" ControlToValidate="txtClntName" Display="Dynamic" ErrorMessage="*"
                                            Font-Bold="True" ForeColor="Maroon" SetFocusOnError="True" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblClntNotes" runat="server" CssClass="txtLabel" Text="Additional Client Notes" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:TextBox runat="server" Text="" ID="txtClntNotes" TextMode="MultiLine" CssClass="genTextBox" Width="85%" Enabled="False"
                                            AutoComplete="off" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblClntRealm" runat="server" CssClass="txtLabel" Text="Client Realm" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:TextBox ID="txtclntRealm" runat="server" Text="" CssClass="genTextBox" Width="85%" Enabled="False" AutoComplete="off" />
                                        <asp:RequiredFieldValidator runat="server" ID="rfvClntRealm" ControlToValidate="txtclntRealm" Display="Dynamic" ErrorMessage="*"
                                            Font-Bold="True" ForeColor="Maroon" SetFocusOnError="True" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblclntTaxReg" runat="server" CssClass="txtLabel" Text="Client Tax Registration" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:TextBox ID="txtclntTaxReg" runat="server" Text="" CssClass="genTextBox" Width="85%" Enabled="False" AutoComplete="off" />
                                        <asp:RequiredFieldValidator runat="server" ID="rfvclntTaxReg" ControlToValidate="txtclntTaxReg" Display="Dynamic" ErrorMessage="*"
                                            Font-Bold="True" ForeColor="Maroon" SetFocusOnError="True" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%"></asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Label ID="lblClientSuccess" runat="server" Text="" CssClass="lblSuccess" Visible="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnAddNewClient" runat="server" CssClass="red btn" Text="Add Client" Width="85%" OnClick="btnAddNewClient_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Button ID="btnAddNewClientCancel" runat="server" CssClass="red btn" Width="25%" Text="Clear Values" OnClick="btnAddNewClientCancel_Click"
                                            CausesValidation="False" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnClientDone" runat="server" CssClass="red btn" Width="85%" Visible="false" Text="Done" OnClick="btnClientDone_Click"
                                            CausesValidation="False" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%"></asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <div id="divClientDB" runat="server" class="divShadow" style="width: 100%;">
                    <asp:UpdateProgress ID="uprgsClntDB" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlClientDB">
                        <ProgressTemplate>
                            <div id="divClientsDB" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center; z-index: 100;">
                                <asp:Image ID="imgClntDBProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlClientDB">
                        <ContentTemplate>
                            <asp:Table ID="tblClientDBList" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2" BackColor="#c0c0c0">
                                        <asp:Label ID="lblClientDB" runat="server" Text="Client's Database Mapping" CssClass="genLabel" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblMappingClientType" runat="server" CssClass="txtLabel" Text="Client Type" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:DropDownList ID="ddlMappingClientType" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList"
                                            Width="85%" OnSelectedIndexChanged="ddlMappingClientType_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Client Type ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblMappingClientRealm" runat="server" CssClass="txtLabel" Text="Client Realm" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:DropDownList ID="ddlMappingClientRealm" runat="server" AutoPostBack="True" AppendDataBoundItems="True" Enabled="False"
                                            CssClass="ddlList" Width="85%" OnSelectedIndexChanged="ddlMappingClientRealm_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Client Realm ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%"></asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Label ID="lblMappedDB" runat="server" Text="" CssClass="lblSuccess" Visible="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button runat="server" ID="btnMappingAdd" CssClass="red btn" Text="Add New Mapping" Width="85%" OnClick="btnMappingAdd_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Button runat="server" ID="btnMappingCancel" CssClass="red btn" Text="Clear Selection" OnClick="btnMappingCancel_Click"
                                            Width="25%" Visible="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <asp:Table ID="tblClientDBAdd" runat="server" Width="100%" Visible="false">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2" BackColor="#c0c0c0">
                                        <asp:Label ID="lblAddMappingHead" runat="server" Text="Add New Client's Database Mapping" CssClass="genLabel" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblAddMappingClient" runat="server" CssClass="txtLabel" Text="Select Client" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:DropDownList ID="ddlAddMappingClient" runat="server" AppendDataBoundItems="True" AutoPostBack="True"
                                            CssClass="ddlList" OnSelectedIndexChanged="ddlAddMappingClient_SelectedIndexChanged" Width="85%">
                                            <asp:ListItem Value="0">--- Select System Client ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblAddMappingRealm" runat="server" CssClass="txtLabel" Text="Client Realm" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:TextBox runat="server" Text="" ID="txtClientRealm" CssClass="genTextBox" Enabled="False" Width="85%" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblAddMappingDBName" runat="server" CssClass="txtLabel" Text="Database Name" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:DropDownList ID="ddlDatabaseNameList" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="ddlList"
                                            Width="85%" Enabled="False" OnSelectedIndexChanged="ddlDatabaseNameList_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Database Name ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%"></asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Label runat="server" ID="lblMappingSuccess" Text="" CssClass="lblSuccess" Visible="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnAddNewMapping" runat="server" CssClass="red btn" Width="85%" Enabled="false" Text="Add Mapping" OnClick="btnAddNewMapping_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Button ID="btnAddNewMappingClear" runat="server" CssClass="red btn" Width="25%" Text="Clear Values" OnClick="btnAddNewMappingClear_Click" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnAddNewMappingDone" runat="server" CssClass="red btn" Width="85%" Text="Done" Visible="false" OnClick="btnAddNewMappingDone_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%"></asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <div id="divClntAddress" runat="server" class="divShadow" style="width: 100%;">
                    <asp:UpdateProgress ID="uprgsAddress" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlClientAddress">
                        <ProgressTemplate>
                            <div id="divAddPrgs" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center; z-index: 100;">
                                <asp:Image ID="imgAddressProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlClientAddress">
                        <ContentTemplate>
                            <asp:Table ID="tblAddressList" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2" BackColor="#c0c0c0">
                                        <asp:Label ID="lblClientAddressMain" runat="server" Text="System Client's Addresses" CssClass="genLabel" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblAddressClientType" runat="server" CssClass="txtLabel" Text="Client Type" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:DropDownList ID="ddlAddressClientType" runat="server" CssClass="ddlList" AutoPostBack="True" AppendDataBoundItems="True"
                                            Width="85%" OnSelectedIndexChanged="ddlAddressClientType_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Client Type ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblAddressClientList" runat="server" CssClass="txtLabel" Text="Client Name" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:DropDownList ID="ddlAddressClientList" runat="server" CssClass="ddlList" AutoPostBack="True" AppendDataBoundItems="True"
                                            Width="85%" Enabled="False" OnSelectedIndexChanged="ddlAddressClientList_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select SMARTs Client ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Middle" ColumnSpan="2">
                                        <asp:GridView ID="gvwAddressList" runat="server" AutoGenerateColumns="False" DataKeyNames="clntAddID" AllowPaging="True"
                                            EmptyDataText="No Address Found." ShowHeaderWhenEmpty="True" CssClass="mydatagrid" Visible="false" Width="100%" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                            PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnPageIndexChanging="gvwAddressList_PageIndexChanging">
                                            <Columns>
                                                <asp:BoundField DataField="clntAddID" HeaderText="clntAddID" Visible="False" />
                                                <asp:BoundField DataField="dmgID" HeaderText="Type" />
                                                <asp:BoundField DataField="title" HeaderText="Title" />
                                                <asp:BoundField DataField="st1" HeaderText="Street 1" />
                                                <asp:BoundField DataField="st2" HeaderText="Street 2" />
                                                <asp:BoundField DataField="st3" HeaderText="Street 3" />
                                                <asp:BoundField DataField="cityID" HeaderText="City/Town" />
                                                <asp:BoundField DataField="cntyID" HeaderText="County/District" />
                                                <asp:BoundField DataField="stID" HeaderText="State/Province" />
                                                <asp:BoundField DataField="ctyID" HeaderText="Country/Nation" />
                                                <asp:BoundField DataField="zipID" HeaderText="Zip/Postal Code" />
                                                <asp:BoundField DataField="sregID" HeaderText="Sub-Region" />
                                                <asp:BoundField DataField="regID" HeaderText="Geographic Region" />
                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" SortExpression="uTime" ReadOnly="True" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                            </Columns>
                                        </asp:GridView>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button runat="server" ID="btnAddressAdd" CssClass="red btn" Text="Add New Address" Width="85%" OnClick="btnAddressAdd_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Button runat="server" ID="btnAddressCnacel" CssClass="red btn" Text="Clear Selection"
                                            OnClick="btnAddressCnacel_Click" Width="25%" Visible="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <asp:Table ID="tblAddressAdd" runat="server" Width="100%" Visible="false">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="4" BackColor="#c0c0c0">
                                        <asp:Label ID="lblNAClientTypeHead" runat="server" Text="System Client's Address" CssClass="genLabel" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label runat="server" ID="lblNAClientType" CssClass="txtLabel" Text="SMARTs Client Type" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="25%">
                                        <asp:DropDownList ID="ddlAddressNewClientType" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList"
                                            Width="85%" OnSelectedIndexChanged="ddlAddressNewClientType_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select SMARTs Client Type ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label runat="server" ID="lblNAClientList" CssClass="txtLabel" Text="SMARTs Client" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="25%">
                                        <asp:DropDownList ID="ddlAddressNewClientList" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList"
                                            Enabled="False" Width="85%" OnSelectedIndexChanged="ddlAddressNewClientList_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select SMARTs Client ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label runat="server" ID="lblNADemog" CssClass="txtLabel" Text="Address Type" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="25%">
                                        <asp:DropDownList ID="ddlNADemog" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList" Enabled="false"
                                            OnSelectedIndexChanged="ddlNADemog_SelectedIndexChanged" Width="85%">
                                            <asp:ListItem Value="0">--- Select Address Type ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label runat="server" ID="lblNATitle" CssClass="txtLabel" Text="Address Title" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="25%">
                                        <asp:TextBox runat="server" ID="txtNATitle" CssClass="genTextBox" Text="" Width="85%" Enabled="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label runat="server" ID="lblNAReg" CssClass="txtLabel" Text="Major Geographic Region" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="25%">
                                        <asp:DropDownList ID="ddlNAReg" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList" Enabled="False"
                                            Width="85%" OnSelectedIndexChanged="ddlNAReg_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Major Geographic Region ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label runat="server" ID="lblNASReg" CssClass="txtLabel" Text="Geographic Sub-Region" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="25%">
                                        <asp:DropDownList ID="ddlNASReg" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList" Enabled="False"
                                            Width="85%" OnSelectedIndexChanged="ddlNASReg_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Geographic Sub-Region ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label runat="server" ID="lblNACountry" CssClass="txtLabel" Text="Country" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="25%">
                                        <asp:DropDownList ID="ddlNACountry" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList" Enabled="False"
                                            Width="85%" OnSelectedIndexChanged="ddlNACountry_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Country/Nation ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label runat="server" ID="lblNAState" CssClass="txtLabel" Text="State/Province" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="25%">
                                        <asp:DropDownList ID="ddlNAState" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList" Enabled="False"
                                            Width="85%" OnSelectedIndexChanged="ddlNAState_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select State/Province ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label runat="server" ID="lblNACounty" CssClass="txtLabel" Text="County/District" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="25%">
                                        <asp:DropDownList ID="ddlNACounty" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList" Enabled="False"
                                            Width="85%" OnSelectedIndexChanged="sdsNACounty_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select County/District/Municipality/Division ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label runat="server" ID="lblNACity" CssClass="txtLabel" Text="City/Town" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="25%">
                                        <asp:DropDownList ID="ddlNACity" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList" Enabled="False"
                                            Width="85%" OnSelectedIndexChanged="ddlNACity_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select City/Town ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label runat="server" ID="lblNAZip" CssClass="txtLabel" Text="Zip/Postal Code" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="25%">
                                        <asp:DropDownList ID="ddlNAZip" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList" Enabled="False"
                                            Width="85%" OnSelectedIndexChanged="ddlNAZip_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Zip/Postal Code ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%"></asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="25%"></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label runat="server" ID="lblNASt1" CssClass="txtLabel" Text="Street 1" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="25%">
                                        <asp:TextBox runat="server" Text="" ID="txtSt1" Enabled="false" Width="85%" CausesValidation="True" AutoComplete="off" />
                                        <asp:RequiredFieldValidator runat="server" ID="rfvSt1" ControlToValidate="txtSt1" Display="Dynamic" ErrorMessage="*" ForeColor="Maroon"
                                            SetFocusOnError="true" />
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label runat="server" ID="lblNASt2" CssClass="txtLabel" Text="Street 2" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="25%">
                                        <asp:TextBox runat="server" Text="" ID="txtSt2" Enabled="false" Width="85%" AutoComplete="off" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label runat="server" ID="lblNASt3" CssClass="txtLabel" Text="Street 3" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="25%">
                                        <asp:TextBox runat="server" Text="" ID="txtSt3" Enabled="false" Width="85%" AutoComplete="off" />
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%"></asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="25%"></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%"></asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="25%" ColumnSpan="3">
                                        <asp:Label runat="server" ID="lblAddressSuccess" Text="" CssClass="lblSuccess" Visible="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button runat="server" ID="btnNAAdd" CssClass="red btn" Width="85%" Text="Add New Address" Enabled="false" OnClick="btnNAAdd_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="25%" ColumnSpan="3">
                                        <asp:Button runat="server" ID="btnNAClear" CssClass="red btn" Text="Clear Values" Enabled="false"
                                            OnClick="btnNAClear_Click" Width="25%" CausesValidation="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button runat="server" ID="btnNADone" CssClass="red btn" Width="85%" Text="Done" Visible="false" Enabled="false" CausesValidation="false"
                                            OnClick="btnNADone_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%" ColumnSpan="3"></asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <div id="divPhone" runat="server" class="divShadow" style="width: 100%;">
                    <asp:UpdateProgress ID="prgrsPhone" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlPhone">
                        <ProgressTemplate>
                            <div id="divPhModal" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center; z-index: 100;">
                                <asp:Image ID="imgPhProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel ID="upnlPhone" runat="server">
                        <ContentTemplate>
                            <asp:Table ID="tblPhoneList" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2" BackColor="#c0c0c0">
                                        <asp:Label runat="server" ID="lblPhoneHead" Text="Phone Numbers" CssClass="genLabel" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblPClientType" runat="server" CssClass="txtLabel" Text="Client Type" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:DropDownList ID="ddlPClientType" runat="server" CssClass="ddlList" AutoPostBack="True" AppendDataBoundItems="True"
                                            Width="85%" OnSelectedIndexChanged="ddlPClientType_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Client Type ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblPClientName" runat="server" CssClass="txtLabel" Text="Client Name" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:DropDownList ID="ddlPClientName" runat="server" CssClass="ddlList" AutoPostBack="True" AppendDataBoundItems="True"
                                            Width="85%" Enabled="False" OnSelectedIndexChanged="ddlPClientName_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select SMARTs Client ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label runat="server" ID="lblPLocType" CssClass="txtLabel" Text="Location Type" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:DropDownList runat="server" ID="ddlPLocType" CssClass="ddlList" Width="85%" AutoPostBack="True" AppendDataBoundItems="True"
                                            Enabled="false" OnSelectedIndexChanged="ddlPLocType_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Location Type ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label runat="server" ID="lblPLoc" CssClass="txtLabel" Text="Location Title" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:DropDownList runat="server" ID="ddlPLoc" CssClass="ddlList" Width="85%" AutoPostBack="true" AppendDataBoundItems="true"
                                            Enabled="false" OnSelectedIndexChanged="ddlPLoc_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Location ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Middle" ColumnSpan="2">
                                        <asp:GridView runat="server" ID="gvwPhoneList" CssClass="mydatagrid" Width="99.5%" DataKeyNames="ptaID"
                                            AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" EmptyDataText="No Phone Numbers associated with selected Location"
                                            ShowHeaderWhenEmpty="True" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header"
                                            PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnPageIndexChanging="gvwPhoneList_PageIndexChanging">
                                            <Columns>
                                                <asp:BoundField DataField="ptaID" Visible="false" />
                                                <asp:BoundField DataField="ptaValue" HeaderText="Phone Number" />
                                                <asp:BoundField DataField="ptaType" HeaderText="Type" />
                                                <asp:BoundField HeaderText="Last Updated" DataField="uTime" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                            </Columns>
                                        </asp:GridView>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button runat="server" ID="btnAddNewPhone" CssClass="red btn" Width="85%" Text="Add New Phone" OnClick="btnAddNewPhone_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Button runat="server" ID="btnClearPhone" CssClass="red btn" Width="25%" Text="Clear Selection" Visible="false" OnClick="btnClearPhone_Click" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <asp:Table ID="tblPhoneAdd" runat="server" Width="100%" Visible="false">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="4" BackColor="#c0c0c0">
                                        <asp:Label runat="server" ID="lblAPNumberHead" Text="Add New Phone Number" CssClass="genLabel" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label runat="server" ID="lblAPNumberClientType" Text="Select Client Type" CssClass="txtLabel" />
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:DropDownList ID="ddlAPNumberClientType" runat="server" CssClass="ddlList" AutoPostBack="True" AppendDataBoundItems="True"
                                            Width="85%" OnSelectedIndexChanged="ddlAPNumberClientType_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Client Type ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblAPNumberClient" runat="server" CssClass="txtLabel" Text="Select Client" />
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:DropDownList ID="ddlAPNumberClient" runat="server" CssClass="ddlList" AutoPostBack="True" AppendDataBoundItems="True"
                                            Width="85%" Enabled="False" OnSelectedIndexChanged="ddlAPNumberClient_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select SMARTs Client ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label runat="server" ID="lblAPLocType" CssClass="txtLabel" Text="Location Type" />
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:DropDownList runat="server" ID="ddlAPLocType" CssClass="ddlList" Width="85%" AutoPostBack="True" AppendDataBoundItems="True"
                                            OnSelectedIndexChanged="ddlAPLocType_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Location Type ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label runat="server" ID="lblAPLocTitle" CssClass="txtLabel" Text="Location Title" />
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:DropDownList runat="server" ID="ddlAPLocTitle" CssClass="ddlList" Width="85%" AutoPostBack="true" AppendDataBoundItems="true"
                                            OnSelectedIndexChanged="ddlAPLocTitle_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Location ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label runat="server" ID="lblAPRegion" CssClass="txtLabel" Text="Major Geographic Region" />
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:DropDownList runat="server" ID="ddlAPRegion" CssClass="ddlList" Width="85%" AutoPostBack="true" AppendDataBoundItems="True"
                                            Enabled="False" OnSelectedIndexChanged="ddlAPRegion_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Major Geographic Region ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label runat="server" ID="lblAPSubReg" CssClass="txtLabel" Text="Geographic Sub-Region" />
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:DropDownList runat="server" ID="ddlAPSubReg" CssClass="ddlList" Width="85%" AutoPostBack="true" Enabled="false" AppendDataBoundItems="True"
                                            OnSelectedIndexChanged="ddlAPSubReg_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Geographic Sub-Region ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label runat="server" ID="lblAPCountry" CssClass="txtLabel" Text="Country/Nation" />
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:DropDownList runat="server" ID="ddlAPCountry" CssClass="ddlList" Width="85%" AutoPostBack="true" Enabled="false" AppendDataBoundItems="true"
                                            OnSelectedIndexChanged="ddlAPCountry_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Country/Nation ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label runat="server" ID="lblAPTypeExtension" CssClass="txtLabel" Text="Type" />
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:DropDownList runat="server" ID="ddlAPType" CssClass="ddlList" Width="85%" AutoPostBack="true" Enabled="false" AppendDataBoundItems="true"
                                            OnSelectedIndexChanged="ddlAPType_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Phone Type ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label runat="server" ID="lblAPPhone" CssClass="txtLabel" Text="Phone Number" />
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:TextBox runat="server" ID="txtAPPhone" CssClass="genTextBox" Text="" Width="85%" CausesValidation="true" Enabled="False"
                                            AutoComplete="off" />
                                        <asp:RequiredFieldValidator runat="server" ID="rfvAPPhone" ControlToValidate="txtAPPhone" Display="Dynamic" ErrorMessage="*"
                                            ForeColor="Maroon" SetFocusOnError="True" />
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label runat="server" ID="lblAPExtension" CssClass="txtLabel" Text="Extension" />
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:TextBox runat="server" ID="txtAPExtension" CssClass="genTextBox" Text="" Width="85%" Enabled="False" AutoComplete="off" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%"></asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" ColumnSpan="3">
                                        <asp:Label runat="server" ID="lblPhoneSuccess" CssClass="lblSuccess" Text="" Visible="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button runat="server" ID="btnAddPhone" CssClass="red btn" Width="85%" Text="Add New Phone Number" Enabled="false" OnClick="btnAddPhone_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" ColumnSpan="3">
                                        <asp:Button runat="server" ID="btnAddPhoneCancel" CssClass="red btn" Width="25%" Text="Clear Values" OnClick="btnAddPhoneCancel_Click"
                                            CausesValidation="False" Visible="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button runat="server" ID="btnAddPhoneDone" CssClass="red btn" Width="85%" Text="Done" Visible="false" CausesValidation="False"
                                            OnClick="btnAddPhoneDone_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" ColumnSpan="3"></asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <div id="divContractMain" runat="server" class="divShadow" style="width: 100%;">
                    <asp:UpdateProgress ID="uprgsContract" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlContract">
                        <ProgressTemplate>
                            <div id="divContractsProgressBar" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center; z-index: 100;">
                                <asp:Image ID="imgContractProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlContract">
                        <ContentTemplate>
                            <asp:Table ID="tblContractDialog" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2">
                                        <asp:Table ID="tblContract" runat="server" Width="100%">
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="3" BackColor="#c0c0c0">
                                                    <asp:Label ID="lblContractMain" runat="server" Text="System Client's Contracts" Width="99.6%" CssClass="genLabel" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                                    <asp:Label runat="server" ID="lblClientCo" CssClass="txtLabel" Text="Contract Client" />
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" ColumnSpan="2">
                                                    <asp:DropDownList ID="ddlClientCo" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList" Width="85%"
                                                        OnSelectedIndexChanged="ddlClientCo_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select Contract Client ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell VerticalAlign="Middle" ColumnSpan="3">
                                                    <asp:GridView ID="gvwContractList" runat="server" AutoGenerateColumns="False" DataKeyNames="ctrtID" AllowPaging="True" CssClass="mydatagrid"
                                                        EmptyDataText="No Contracts Found." ShowHeaderWhenEmpty="true" Width="100%" Visible="false" OnRowDataBound="gvwContractList_RowDataBound"
                                                        AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                        RowStyle-CssClass="rows" OnPageIndexChanging="gvwContractList_PageIndexChanging">
                                                        <Columns>
                                                            <asp:BoundField DataField="ctrtID" Visible="False" />
                                                            <asp:BoundField DataField="ctID" HeaderText="Type" />
                                                            <asp:BoundField DataField="ctrtName" HeaderText="Name" />
                                                            <asp:BoundField DataField="cstatID" HeaderText="Status" />
                                                            <asp:BoundField DataField="ctrtStart" HeaderText="Start Date" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                            <asp:BoundField DataField="ctrtEnd" HeaderText="End Date" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                            <asp:BoundField DataField="ctrtTTL" HeaderText="Total Days" />
                                                            <asp:BoundField DataField="ctrtRmn" HeaderText="Remaining Days" DataFormatString="{0:0.00}" />
                                                            <asp:BoundField DataField="scID" HeaderText="Sales Campaign" />
                                                            <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                                    <asp:Button ID="btnAddContract" runat="server" CssClass="red btn" Text="Add New Contract" Width="85%" OnClick="btnAddContract_Click" />
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" Width="25%">
                                                    <asp:Button ID="btnEditContract" runat="server" CssClass="red btn" Text="Change Contract" Width="85%" OnClick="btnEditContract_Click" />
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle">
                                                    <asp:Button ID="btnContractListCancel" runat="server" CssClass="red btn" Text="Clear Selection" OnClick="btnContractListCancel_Click"
                                                        Width="25%" Visible="false" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2">
                                        <asp:Table ID="tblEditContract" runat="server" Width="100%" Visible="false">
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2" BackColor="#c0c0c0">
                                                    <asp:Label ID="lblEditContractHeading" runat="server" Text="Edit/Update Client's Contract" CssClass="genLabel" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                                    <asp:Label ID="lblEditCttType" runat="server" CssClass="txtLabel" Text="Select Contract Type" />
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" Width="75%">
                                                    <asp:DropDownList ID="ddlEditCttType" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList"
                                                        Width="85%" OnSelectedIndexChanged="ddlEditCttType_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select Contract Type ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                                    <asp:Label ID="lblEditCttProvider" runat="server" CssClass="txtLabel" Text="Select Contract Provider" />
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" Width="75%">
                                                    <asp:DropDownList ID="ddlEditCttProvider" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList"
                                                        Width="85%" Enabled="false" OnSelectedIndexChanged="ddlEditCttProvider_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select Contract Provider ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                                    <asp:Label ID="lblEditCttPartner" runat="server" CssClass="txtLabel" Text="Select Contract Partner" />
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" Width="75%">
                                                    <asp:DropDownList ID="ddlEditCttPartner" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList"
                                                        Width="85%" Enabled="false" OnSelectedIndexChanged="ddlEditCttPartner_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select Contract Partner ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                                    <asp:Label ID="lblEditCttClient" runat="server" CssClass="txtLabel" Text="Select Contract Client" />
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" Width="75%">
                                                    <asp:DropDownList ID="ddlEditCttClient" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList"
                                                        Width="85%" Enabled="false" OnSelectedIndexChanged="ddlEditCttClient_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select Contract Client ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                                    <asp:Label ID="lblEditCttList" runat="server" CssClass="txtLabel" Text="Select Contract" />
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" Width="75%">
                                                    <asp:DropDownList ID="ddlEditCttList" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList"
                                                        Width="85%" Enabled="false" OnSelectedIndexChanged="ddlEditCttList_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select Contract ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                                    <asp:Label ID="lblEditCttStatus" runat="server" CssClass="txtLabel" Text="Select Status" />
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" Width="75%">
                                                    <asp:DropDownList ID="ddlEditCttStatus" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList"
                                                        Width="85%" Enabled="false" OnSelectedIndexChanged="ddlEditCttStatus_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select Contract Status ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%"></asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" Width="75%">
                                                    <asp:Label ID="lblEditContractSuccess" runat="server" Text="" CssClass="lblSuccess" Visible="false" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                                    <asp:Button ID="btnEditContractUpdate" runat="server" Text="Update Contract" CssClass="red btn" Width="85%" Enabled="false"
                                                        OnClick="btnEditContractUpdate_Click" />
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" Width="75%">
                                                    <asp:Button ID="btnEditContractCancel" runat="server" Text="Clear Values" Width="25%" CssClass="red btn"
                                                        OnClick="btnEditContractCancel_Click" CausesValidation="False" Visible="false" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2">
                                        <asp:Table ID="tblAddContract" runat="server" Width="100%" Visible="false">
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2" BackColor="#c0c0c0">
                                                    <asp:Label ID="lblAddContractTypeHead" runat="server" Text="Add System Client's Contracts" CssClass="genLabel" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                                    <asp:Label ID="lblAddContractType" runat="server" CssClass="txtLabel" Text="Select Contract Type" />
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" Width="75%">
                                                    <asp:DropDownList ID="ddlAddContractType" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList"
                                                        Width="85%" OnSelectedIndexChanged="ddlAddContractType_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select Contract Type ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                                    <asp:Label ID="lblAddContractProvider" runat="server" CssClass="txtLabel" Text="Select Contract Provider" />
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" Width="75%">
                                                    <asp:DropDownList ID="ddlAddContractProvider" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList"
                                                        Width="85%" Enabled="false" OnSelectedIndexChanged="ddlAddContractProvider_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select Contract Provider ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                                    <asp:Label ID="lblAddContractPartner" runat="server" CssClass="txtLabel" Text="Select Contract Partner" />
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" Width="75%">
                                                    <asp:DropDownList ID="ddlAddContractPartner" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList"
                                                        Width="85%" Enabled="false" OnSelectedIndexChanged="ddlAddContractPartner_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select Contract Partner ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                                    <asp:Label ID="lblAddContractClient" runat="server" CssClass="txtLabel" Text="Select Contract Client" />
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" Width="75%">
                                                    <asp:DropDownList ID="ddlAddContractClient" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList"
                                                        Width="85%" Enabled="false" OnSelectedIndexChanged="ddlAddContractClient_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select Contract Client ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                                    <asp:Label ID="txtAddContractName" runat="server" CssClass="txtLabel" Text="Contract Name" />
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" Width="75%">
                                                    <asp:TextBox ID="txtContractName" runat="server" Text="" CssClass="genTextBox" Enabled="false" Width="85%" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                                    <asp:Label ID="lblAddContractStatus" runat="server" CssClass="txtLabel" Text="Select Contract Status" />
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" Width="75%">
                                                    <asp:DropDownList ID="ddlAddContractStatus" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList"
                                                        Width="85%" Enabled="false" OnSelectedIndexChanged="ddlAddContractStatus_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select Contract Status ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                                    <asp:Label ID="lblClientRep" runat="server" CssClass="txtLabel" Text="Client Representative" />
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" Width="75%">
                                                    <asp:TextBox ID="txtCRep" runat="server" Text="" CssClass="genTextBox" Enabled="false" Width="85%" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                                    <asp:Label ID="lblRepDesignation" runat="server" CssClass="txtLabel" Text="Client Rep. Designation" />
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" Width="75%">
                                                    <asp:TextBox ID="txtCRepDesig" runat="server" Text="" CssClass="genTextBox" Enabled="false" Width="85%" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                                    <asp:Label ID="lblContractStartD" runat="server" CssClass="txtLabel" Text="Select Contract Start Date" />
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" Width="75%">
                                                    <asp:TextBox ID="txtStartCalendar" runat="server" Width="85%" CssClass="genTextbox" Enabled="false" TextMode="Date" />
                                                    <%--<ajaxToolkit:CalendarExtender ID="clndrStartContract" runat="server" TargetControlID="txtStartCalendar" DaysModeTitleFormat="MMMM, yyyy" />--%>
                                                    <asp:RangeValidator ID="rngvStartContract" runat="server" ErrorMessage="*" ControlToValidate="txtStartCalendar" MinimumValue="1900-01-01"
                                                        MaximumValue="2199-12-31" Type="Date" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                                    <asp:Label ID="lblontractEndD" runat="server" CssClass="txtLabel" Text="Select Contract End Date" />
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" Width="75%">
                                                    <asp:TextBox ID="txtEndCalendar" runat="server" Width="85%" CssClass="genTextbox" Enabled="false" TextMode="Date" />
                                                    <%--<ajaxToolkit:CalendarExtender ID="clndrEndContract" runat="server" TargetControlID="txtEndCalendar" DaysModeTitleFormat="MMMM, yyyy" />--%>
                                                    <asp:RangeValidator ID="rngvEndContract" runat="server" ErrorMessage="*" ControlToValidate="txtStartCalendar" MinimumValue="1900-01-01"
                                                        MaximumValue="2199-12-31" Type="Date" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                                    <asp:Label ID="lblContractSC" runat="server" CssClass="txtLabel" Text="Select Sales Campaign" />
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" Width="75%">
                                                    <asp:DropDownList ID="ddlContractSC" runat="server" CssClass="ddlList" Width="85%" AutoPostBack="true" AppendDataBoundItems="true"
                                                        Enabled="false" OnSelectedIndexChanged="ddlContractSC_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select Sales Campaign ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%"></asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" Width="75%">
                                                    <asp:Label ID="lblContractSuccess" runat="server" Text="" CssClass="lblSuccess" Visible="false" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                                    <asp:Button ID="btnAddNewContract" runat="server" Text="Add New Contract" CssClass="red btn" Width="85%" Enabled="false"
                                                        OnClick="btnAddNewContract_Click" />
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" Width="75%">
                                                    <asp:Button ID="btnAddNewContractCancel" runat="server" Text="Clear Values" Width="25%" CssClass="red btn"
                                                        OnClick="btnAddNewContractCancel_Click" CausesValidation="False" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnAddNewContractDone" runat="server" CssClass="red btn" Width="85%" Visible="false" Text="Done" OnClick="btnAddNewContractDone_Click"
                                            CausesValidation="False" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%"></asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <div id="divPriceMain" runat="server" class="divShadow">
                    <asp:UpdateProgress ID="uprgsPriceList" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlPriceList">
                        <ProgressTemplate>
                            <div id="divPriceProgressBar" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center; z-index: 100;">
                                <asp:Image ID="imgPriceProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlPriceList">
                        <ContentTemplate>
                            <asp:Table ID="tblServicePrices" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2" BackColor="#c0c0c0">
                                        <asp:Label ID="lblPriceList" runat="server" Text="Contract Price List" CssClass="genLabel" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblContractList" runat="server" Text="Contract Price List" CssClass="txtLabel" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:DropDownList ID="ddlContractList" runat="server" AutoPostBack="True" Width="85%" AppendDataBoundItems="True" OnSelectedIndexChanged="ddlContractList_SelectedIndexChanged"
                                            CssClass="ddlList">
                                            <asp:ListItem Value="0">--- Select Client Contract ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Middle" ColumnSpan="2">
                                        <asp:GridView ID="gvwPriceList" runat="server" AutoGenerateColumns="False" DataKeyNames="spID" Width="100%" EmptyDataText="No Price List Found for selected Contract"
                                            ShowHeaderWhenEmpty="True" AllowPaging="True" Visible="false" CssClass="mydatagrid" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                            PageSize="15"
                                            HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnPageIndexChanging="gvwPriceList_PageIndexChanging">
                                            <Columns>
                                                <asp:BoundField DataField="spID" HeaderText="spID" Visible="False" />
                                                <asp:BoundField DataField="srvID" HeaderText="Service Name" />
                                                <asp:BoundField DataField="spCharge" HeaderText="Service Price" />
                                                <asp:BoundField DataField="scuID" HeaderText="Price Unit" />
                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                                            </Columns>
                                        </asp:GridView>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnPriceAdd" runat="server" Text="Add New Price" CssClass="red btn" Width="85%" OnClick="btnPriceAdd_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Button ID="btnPriceCancel" runat="server" Text="Clear Selection" CssClass="red btn" OnClick="btnPriceCancel_Click"
                                            Width="25%" Visible="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <asp:Table ID="tblAddPrice" runat="server" Width="100%" Visible="false">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2" BackColor="#c0c0c0">
                                        <asp:Label ID="lblPriceListHead" runat="server" Text="Add Contract Price List" CssClass="genLabel" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblAPContract" runat="server" CssClass="txtLabel" Text="Select Service Contract" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:DropDownList ID="ddlAPContract" runat="server" AutoPostBack="True" CssClass="ddlList"
                                            Width="85%" AppendDataBoundItems="True" OnSelectedIndexChanged="ddlAPContract_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Client Contract ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblAPSrvGroup" runat="server" CssClass="txtLabel" Text="Select Service Group" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:DropDownList ID="ddlAPServiceGroup" runat="server" AppendDataBoundItems="True" AutoPostBack="True"
                                            CssClass="ddlList" Width="85%" Enabled="False" OnSelectedIndexChanged="ddlAPServiceGroup_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Service Group ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblAPService" runat="server" CssClass="txtLabel" Text="Select Service" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:DropDownList ID="ddlAPService" runat="server" AppendDataBoundItems="True" AutoPostBack="True"
                                            CssClass="ddlList" Width="85%" Enabled="False" OnSelectedIndexChanged="ddlAPService_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Service ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblAPSrvPrice" runat="server" CssClass="txtLabel" Text="Service Price" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:TextBox runat="server" Text="" ID="txtAPSrvPrice" CssClass="genTextBox" Enabled="False" Width="85%" CausesValidation="True"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" ID="rfvAPSrvPrice" ControlToValidate="txtAPSrvPrice" Display="Dynamic" ErrorMessage="*"
                                            ForeColor="Maroon" SetFocusOnError="true" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblAPChargeUnit" runat="server" CssClass="txtLabel" Text="Select Service Charge Unit" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:DropDownList ID="ddlAPChargeUnit" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="ddlList"
                                            Width="85%" Enabled="False" OnSelectedIndexChanged="ddlAPChargeUnit_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Service Charge Unit ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%"></asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Label ID="lblAPSuccess" runat="server" Text="" CssClass="lblSuccess" Visible="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnAPNewAdd" runat="server" CssClass="red btn" Width="85%" Enabled="false" Text="Add Price" OnClick="btnAPNewAdd_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Button ID="btnAPNewClear" runat="server" CssClass="red btn" Width="25%" Enabled="false" Text="Clear Values" OnClick="btnAPNewClear_Click"
                                            CausesValidation="False" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnAPNewDone" runat="server" CssClass="red btn" Width="85%" Visible="false" Text="Done" OnClick="btnAPNewDone_Click"
                                            CausesValidation="False" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%"></asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <div id="divClientImages" runat="server" class="divShadow" style="width: 100%;">
                    <asp:UpdateProgress ID="upgrsClientImages" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlClientImages">
                        <ProgressTemplate>
                            <div id="divClientImagesProgressBar" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center; z-index: 100;">
                                <asp:Image ID="imgClientImageProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlClientImages">
                        <ContentTemplate>
                            <asp:Table ID="tblImageList" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2" BackColor="#c0c0c0">
                                        <asp:Label ID="lblImageList" runat="server" Text="Client Images" CssClass="genLabel" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblImageClientType" runat="server" CssClass="txtLabel" Text="Client Type" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:DropDownList ID="ddlImageClientType" runat="server" AutoPostBack="True" AppendDataBoundItems="True" Width="85%" CssClass="ddlList"
                                            OnSelectedIndexChanged="ddlImageClientType_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Type of System Client ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblImageClientList" runat="server" CssClass="txtLabel" Text="Client Name" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:DropDownList ID="ddlImageClientList" runat="server" Width="85%" CssClass="ddlList" AutoPostBack="true" AppendDataBoundItems="true"
                                            Enabled="false" OnSelectedIndexChanged="ddlImageClientList_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select System Client ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Middle" ColumnSpan="2">
                                        <asp:GridView ID="gvwImageList" runat="server" CssClass="mydatagrid" Width="100%" AutoGenerateColumns="False" DataKeyNames="climgID"
                                            AllowPaging="True" EmptyDataText="No Images found for selected Client." ShowHeaderWhenEmpty="True" Visible="False" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                            PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnPageIndexChanging="gvwImageList_PageIndexChanging">
                                            <Columns>
                                                <asp:BoundField DataField="climgID" HeaderText="climgID" Visible="false" />
                                                <asp:BoundField DataField="climgFileName" HeaderText="File Name" />
                                                <asp:BoundField DataField="imgtypID" HeaderText="Type" />
                                                <asp:BoundField DataField="climgFileSize" HeaderText="File Size (Bytes)" />
                                                <asp:BoundField DataField="lclrID" HeaderText="Color" />
                                                <asp:BoundField DataField="imgpID" HeaderText="Purpose" />
                                                <asp:TemplateField HeaderText="Image" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                    <ItemTemplate>
                                                        <asp:Image ID="Image1" runat="server" ImageUrl='<%# "ImageHandler.ashx?ImID="+ Eval("climgID") %>' Height="8%" Width="8%" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                            </Columns>
                                        </asp:GridView>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnAddNewImage" runat="server" Text="Add New Image" CssClass="red btn" Width="85%" OnClick="btnAddNewImage_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Button ID="btnCancelImageList" runat="server" Text="Clear Selection" CssClass="red btn" OnClick="btnCancelImageList_Click"
                                            Width="25%" Visible="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <asp:Table ID="tblAddImage" runat="server" Width="100%" Visible="false">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2" BackColor="#c0c0c0">
                                        <asp:Label ID="lblLogoClientTypeHead" runat="server" Text="Add Client Images" CssClass="genLabel" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblLogoClientType" runat="server" CssClass="txtLabel" Text="Client Type" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:DropDownList ID="ddlAddLogoClientType" runat="server" AutoPostBack="True" AppendDataBoundItems="True" Width="85%"
                                            CssClass="ddlList" OnSelectedIndexChanged="ddlAddLogoClientType_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Type of System Client ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblLogoClient" runat="server" CssClass="txtLabel" Text="Client Name" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:DropDownList ID="ddlAddLogoClient" runat="server" CssClass="ddlList" AutoPostBack="true" AppendDataBoundItems="true"
                                            Width="85%" Enabled="false" OnSelectedIndexChanged="ddlAddLogoClient_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select System Client ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblImagePurpose" runat="server" CssClass="txtLabel" Text="Image Purpose" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:DropDownList ID="ddlAddImagePurpose" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList"
                                            Width="85%" Enabled="False" OnSelectedIndexChanged="ddlAddImagePurpose_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Image Purpose/Usage ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblImageColor" runat="server" CssClass="txtLabel" Text="Image Color" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:DropDownList ID="ddlAddImageColor" runat="server" AutoPostBack="true" AppendDataBoundItems="true" CssClass="ddlList"
                                            Width="85%" Enabled="false" OnSelectedIndexChanged="ddlAddImageColor_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Image Color Style ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblImageType" runat="server" CssClass="txtLabel" Text="Image Type" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:DropDownList runat="server" ID="ddlAddImageType" AutoPostBack="true" AppendDataBoundItems="true" CssClass="ddlList"
                                            Width="85%" Enabled="false" OnSelectedIndexChanged="ddlAddImageType_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Image Type ---</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblLogoUpload" runat="server" Text="Select Logo File to Upload:" CssClass="txtLabel" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:FileUpload ID="FileUploadControl" runat="server" Enabled="false" CssClass="custom-file-upload" Font-Size="Larger" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%"></asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Label ID="lblFileNotes" runat="server" Text="* Only Image file are allowed (jpg, jpeg, png or gif). Max. File size is 5 MB"
                                            CssClass="txtLabel" Font-Size="Smaller" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%"></asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Label ID="lblImageSuccess" runat="server" Text="" CssClass="lblSuccess" Visible="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnImageAdd" runat="server" Text="Add New Image" CssClass="red btn" Width="85%" Visible="false" Enabled="false"
                                            OnClick="btnImageAdd_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Button ID="btnImageClear" runat="server" Text="Clear Values" CssClass="red btn" Width="25%" Visible="false" Enabled="false"
                                            OnClick="btnImageClear_Click" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnImageDone" runat="server" CssClass="red btn" Width="85%" Text="Done" Visible="false" Enabled="false" OnClick="btnImageDone_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%"></asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnImageAdd" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    </div>
</div>
