﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI.WebControls;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using USR = SmartsVer1.Helpers.AccountHelpers.CreateUser;

namespace SmartsVer1.Control.Config.Clients
{
    public partial class ctrlOpCo_2A : System.Web.UI.UserControl
    {
        String LoginName, username, domain, GetClientDBString;
        Int32 ClientID = -99, ClientTypeID = 4;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);

                //DataSource for page controls
                System.Data.DataTable opList = CLNT.getOperatorCoTable(ClientTypeID);
                this.gvwAddOPCList.DataSource = opList;
                this.gvwAddOPCList.PageIndex = 0;
                this.gvwAddOPCList.SelectedIndex = -1;
                this.gvwAddOPCList.DataBind();
                this.gvwUsersOPList.DataSource = opList;
                this.gvwUsersOPList.PageIndex = 0;
                this.gvwUsersOPList.SelectedIndex = -1;
                this.gvwUsersOPList.DataBind();
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName); 
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnOPCAddressListCancel_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable opList = CLNT.getOperatorCoTable(ClientTypeID);
                this.gvwAddOPCList.DataSource = opList;
                this.gvwAddOPCList.DataBind();
                this.gvwOPCAddressList.PageIndex = 0;
                this.gvwOPCAddressList.DataBind();
                this.gvwOPCAddressList.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwAddOPCList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable opList = CLNT.getOperatorCoTable(ClientTypeID);
                this.gvwAddOPCList.DataSource = opList;
                this.gvwAddOPCList.PageIndex = e.NewPageIndex;
                this.gvwAddOPCList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwAddOPCList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 opaID = Convert.ToInt32(this.gvwAddOPCList.SelectedValue);
                System.Data.DataTable addTable = CLNT.getAddressTable(opaID);
                this.gvwOPCAddressList.DataSource = addTable;
                this.gvwOPCAddressList.DataBind();
                this.gvwOPCAddressList.Visible = true;
                this.gvwOPCAddressList.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwOPCUsersList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow drRes = ((DataRowView)e.Row.DataItem).Row;
                    Int32 userApproved = -99;
                    Int32 userLockedOut = -99;
                    userApproved = Convert.ToInt32(drRes["enabled"]);
                    userLockedOut = Convert.ToInt32(drRes["locked"]);
                    CheckBox chkUApp = e.Row.Cells[5].FindControl("chkUserApproved") as CheckBox;
                    CheckBox chkULck = e.Row.Cells[6].FindControl("chkDisabled") as CheckBox;

                    if (userApproved.Equals(1))
                    {
                        chkUApp.Checked = true;
                    }
                    else
                    {
                        chkUApp.Checked = false;
                    }

                    if (userLockedOut.Equals(1))
                    {
                        chkULck.Checked = true;
                    }
                    else
                    {
                        chkULck.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNCancel_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable opList = CLNT.getOperatorCoTable(ClientTypeID);
                this.gvwUsersOPList.DataSource = opList;
                this.gvwUsersOPList.PageIndex = 0;
                this.gvwUsersOPList.SelectedIndex = -1;
                this.gvwUsersOPList.DataBind();
                this.gvwOPCUsersList.PageIndex = 0;
                this.gvwOPCUsersList.DataBind();
                this.gvwOPCUsersList.Visible = false;
                this.gvwUserDetail.PageIndex = 0;
                this.gvwUserDetail.DataBind();
                this.gvwUserDetail.Visible = false;
                this.gvwUserAddressInfo.PageIndex = 0;
                this.gvwUserAddressInfo.DataBind();
                this.gvwUserAddressInfo.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwUsersOPList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable opList = CLNT.getOperatorCoTable(ClientTypeID);
                this.gvwUsersOPList.DataSource = opList;
                this.gvwUsersOPList.PageIndex = e.NewPageIndex;
                this.gvwUsersOPList.SelectedIndex = -1;
                this.gvwUsersOPList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwUsersOPList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cntID = Convert.ToInt32(this.gvwUsersOPList.SelectedValue);
                System.Data.DataTable clList = USR.getReviewersTable(cntID);
                this.gvwOPCUsersList.DataSource = clList;
                this.gvwOPCUsersList.DataBind();
                this.gvwOPCUsersList.Enabled = true;
                this.gvwOPCUsersList.Visible = true;
                this.btnAddNCancel.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwOPCUsersList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                System.Guid user = new System.Guid();
                user = (Guid)this.gvwOPCUsersList.SelectedValue;
                System.Data.DataTable detailTable = USR.getUserDetailTable(user);
                this.gvwUserDetail.DataSource = detailTable;
                this.gvwUserDetail.DataBind();
                this.gvwUserDetail.Visible = true;
                System.Data.DataTable usrinfoTable = USR.getUserProfileInfo(user);
                this.gvwUserAddressInfo.DataSource = usrinfoTable;
                this.gvwUserAddressInfo.DataBind();
                this.gvwUserAddressInfo.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}