﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Net.Mail;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Microsoft.SqlServer.Types;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using DB = SmartsVer1.Helpers.DatabaseHelpers.DBConfigs;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;

namespace SmartsVer1.Control.Config.Opr
{
    public partial class ctrlFEOpr_2A : System.Web.UI.UserControl
    {
        String LoginName, username, domain, GetClientDBString;
        Int32 ClientID = -99;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();
                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);
                username = addr.User;
                domain = addr.Host;
                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
                //Datasources
                System.Data.DataTable cmTable = DB.getClientConnectionStringsTable();
                this.gvwDBList.DataSource = cmTable;
                this.gvwDBList.PageIndex = 0;
                this.gvwDBList.SelectedIndex = -1;
                this.gvwDBList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwDBList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable cmTable = DB.getClientConnectionStringsTable();
                this.gvwDBList.DataSource = cmTable;
                this.gvwDBList.PageIndex = e.NewPageIndex;
                this.gvwDBList.SelectedIndex = -1;
                this.gvwDBList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnMappingAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.divDBList.Visible = false;
                this.divClientDBAdd.Visible = true;
                this.txtDBName.Text = "";
                this.enDB.Visible = false;
                this.lblMappingSuccess.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtDBName.Text = "";
                this.lblMappingSuccess.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnGetDBName_Click(object sender, EventArgs e)
        {
            try
            {
                String dbName = DB.getNextDataBaseName();
                if (!String.IsNullOrEmpty(dbName))
                {
                    this.txtDBName.Text = dbName;
                }
                else
                {
                    this.txtDBName.Text = "";
                    this.enDB.Attributes["class"] = "alert alert-danger alert-dismissable";
                    this.iDB.Attributes["class"] = "icon fa fa-danger";
                    this.spnDB.InnerText = " !!! Warning !!!";
                    this.lblMappingSuccess.Text = "Unable to insert null values. Please provide all values.";
                    this.enDB.Visible = true;
                }
                this.btnAddNewMapping.Enabled = true;
                this.btnAddNewMapping.CssClass = "btn btn-success text-center";
                this.btnAddNewMappingClear.Enabled = true;
                this.btnAddNewMappingClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewMapping_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iReply = -99;
                String dbName = null;

                dbName = this.txtDBName.Text;
                if (String.IsNullOrEmpty(dbName))
                {
                    this.enDB.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iDB.Attributes["class"] = "icon fa fa-warning";
                    this.spnDB.InnerText = " !!! Warning !!!";
                    this.lblMappingSuccess.Text = "Unable to insert null values. Please provide all values.";
                    this.enDB.Visible = true;
                    this.txtDBName.Text = null;
                    this.txtDBName.Focus();
                }
                else
                {
                    String searchDBName = "SELECT [cmName] FROM [ConMap] WHERE [cmName] = @cmName";
                    using (SqlConnection schCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                    {
                        try
                        {
                            schCon.Open();
                            SqlCommand search = new SqlCommand(searchDBName, schCon);
                            search.Parameters.AddWithValue("@cmName", SqlDbType.NVarChar).Value = dbName.ToString();
                            using (SqlDataReader readSearch = search.ExecuteReader())
                            {
                                try
                                {
                                    if (readSearch.HasRows)
                                    {
                                        this.btnAddNewMappingClear_Click(null, null);
                                        this.enDB.Attributes["class"] = "alert alert-success alert-dismissable";
                                        this.iDB.Attributes["class"] = "icon fa fa-check-circle";
                                        this.spnDB.InnerText = " Success!";
                                        this.lblMappingSuccess.Text = "Duplicate Database name found. Please provide a unique Database name.";
                                        this.enDB.Visible = true;
                                    }
                                    else
                                    {
                                        iReply = DB.insertClientConnectionStringMapping(dbName, LoginName, domain);
                                        if (iReply.Equals(1))
                                        {
                                            this.btnAddNewMappingClear_Click(null, null);
                                            this.enDB.Attributes["class"] = "alert alert-success alert-dismissable";
                                            this.iDB.Attributes["class"] = "icon fa fa-check-circle";
                                            this.spnDB.InnerText = " Success!";
                                            this.lblMappingSuccess.Text = "Successfully added Database Name!";
                                            this.enDB.Visible = true;
                                            this.txtDBName.Text = null;
                                            this.txtDBName.Focus();
                                        }
                                        else
                                        {
                                            this.btnAddNewMappingClear_Click(null, null);                                            
                                            this.enDB.Attributes["class"] = "alert alert-warning alert-dismissable";
                                            this.iDB.Attributes["class"] = "icon fa fa-warning";
                                            this.spnDB.InnerText = " !!! Warning !!!";
                                            this.lblMappingSuccess.Text = "Unknown Error. Please try again.";
                                            this.enDB.Visible = true;
                                            this.txtDBName.Text = null;
                                            this.txtDBName.Focus();
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readSearch.Close();
                                    readSearch.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            schCon.Close();
                            schCon.Dispose();
                        }
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewMappingClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtDBName.Text = null;
                this.enDB.Visible = false;
                this.lblMappingSuccess.Text = null;
                this.txtDBName.Focus();
                this.btnAddNewMapping.Enabled = false;
                this.btnAddNewMapping.CssClass = "btn btn-default text-center";
                this.btnAddNewMappingClear.Enabled = false;
                this.btnAddNewMappingClear.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewMappingDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.divClientDBAdd.Visible = false;
                this.divDBList.Visible = true;
                this.divClientDBAdd.Visible = false;
                this.btnAddNewMappingClear_Click(null, null);
                System.Data.DataTable cmTable = DB.getClientConnectionStringsTable();
                this.gvwDBList.DataSource = cmTable;
                this.gvwDBList.PageIndex = 0;
                this.gvwDBList.SelectedIndex = -1;
                this.gvwDBList.DataBind();                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        
    }
}