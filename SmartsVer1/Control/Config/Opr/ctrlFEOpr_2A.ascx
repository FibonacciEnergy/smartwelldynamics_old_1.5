﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlFEOpr_2A.ascx.cs" Inherits="SmartsVer1.Control.Config.Opr.ctrlFEOpr_2A" %>
<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
<!--  Stylesheet -->
<link href="../../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript -->
<script src="../../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<!-- Javascript for Footer -->
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>
<!--- Page local style --->
<style type="text/css" style="background-color: #d9534f; color: #FFFFFF">
    .rtAligned {
        text-align: right;
    }

    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    .sqcLabel {
        min-width: 135px;
        text-align: right;
    }

    .tiLabel {
        min-width: 180px;
        text-align: right;
    }

    .bhaLabel {
        min-width: 170px;
        text-align: right;
    }

    .padLabel {
        min-width: 145px;
        text-align: right;
    }

    .wellLabel {
        min-width: 150px;
        text-align: right;
    }

    .rfLabel {
        min-width: 110px;
        text-align: right;
    }

    .planLabel {
        min-width: 140px;
        text-align: right;
    }

    .padRight {
        padding-right: 25px;
    }

    .btRadio {
        margin-right: 1%;
    }

    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper[data-stepper-steps="8"] li {
            width: 12%;
        }

        ol.stepper[data-stepper-steps="9"] li {
            width: 11%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }
</style>
<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom: 2px; padding-left: 2px; padding-top: 2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">(
                            <asp:LoginName ID="LoginName1" runat="server" />
                    )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>
        </asp:LoginView>
    </div>
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>Configuration</li>
                <li><a href="../../../Config/Assets/fecDRAssets_2A.aspx">SMARTs Assets</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- Page Content -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <asp:UpdateProgress ID="uprgrsConfig" runat="server" DisplayAfter="0" DynamicLayout="true" AssociatedUpdatePanelID="upnlConfig">
                <ProgressTemplate>
                    <div id="divSQCProcessing" runat="server" class="updateProgress">
                        <asp:Image ID="imgSQCProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel runat="server" ID="upnlConfig">
                <ContentTemplate>
                    <div id="divDBList" runat="server">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h5 class="panel-title"><span>Client Databases List</span></h5>
                            </div>
                            <div class="panel-body">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="input-group">
                                        <asp:LinkButton ID="btnMappingAdd" runat="server" CssClass="btn btn-info text-center" OnClick="btnMappingAdd_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                        <asp:LinkButton ID="btnClear" runat="server" CssClass="btn btn-warning text-center" OnClick="btnClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <asp:GridView runat="server" ID="gvwDBList" CssClass="mydatagrid" AllowPaging="True" AutoGenerateColumns="False"
                                        DataKeyNames="cmID" OnPageIndexChanging="gvwDBList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                        PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                        <Columns>
                                            <asp:BoundField DataField="cmID" Visible="False"></asp:BoundField>
                                            <asp:BoundField DataField="cmName" HeaderText="Database Name" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                            <asp:BoundField DataField="cmString" HeaderText="Connection String" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                            <asp:BoundField DataField="dbSize" HeaderText="Database Size (MB)" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                            <asp:BoundField DataField="uTime" HeaderText="Last Updated On" ReadOnly="true" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}"
                                                ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="divClientDBAdd" runat="server" visible="false">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h5 class="panel-title"><span>Add New Client Database</span></h5>
                            </div>
                            <div class="panel-body">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="input-group">
                                        <asp:LinkButton ID="btnGetDBName" runat="server" CssClass="btn btn-info text-center" OnClick="btnGetDBName_Click"><i class="fa fa-plus-circle"> Get New Database Name</i></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-addon sqcLabel bg-light-blue">Client Database Name</span>
                                        <asp:TextBox runat="server" ID="txtDBName" CssClass="form-control" Text="" Enabled="false" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div runat="server" id="enDB" visible="false">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <h4><i runat="server" id="iDB"><span runat="server" id="spnDB"></span></i></h4>
                                        <asp:Label runat="server" ID="lblMappingSuccess" Text="" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="input-group">
                                        <asp:LinkButton ID="btnAddNewMapping" runat="server" CssClass="btn btn-success text-center" Enabled="false" OnClick="btnAddNewMapping_Click"><i class="fa fa-plus"> Add Connection</i></asp:LinkButton>
                                        <asp:LinkButton ID="btnAddNewMappingClear" runat="server" CssClass="btn btn-warning text-center" Enabled="false" OnClick="btnAddNewMappingClear_Click"><i class="fa fa-Reset"> Reset</i></asp:LinkButton>
                                        <asp:LinkButton ID="btnAddNewMappingDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnAddNewMappingDone_Click"><i class="fa fa-times-rectangle"> Done</i></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
    
