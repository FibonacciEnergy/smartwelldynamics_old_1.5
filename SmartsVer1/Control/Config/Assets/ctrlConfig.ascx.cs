﻿using System;
using log4net;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Net.Mail;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using OPR = SmartsVer1.Helpers.QCHelpers.Opr;
using QC = SmartsVer1.QCHelpers.sdRawDataQC;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using CHRT = SmartsVer1.Helpers.ChartHelpers.Gauges;
using CHRTRes = SmartsVer1.Helpers.ChartHelpers.ChartResources;
using WPN = SmartsVer1.Helpers.QCHelpers.WellPlan;
using SRV = SmartsVer1.Helpers.DemogHelpers.ServicesConfig;
using CTRT = SmartsVer1.Helpers.AccountHelpers.CreateContract;
using CLR = System.Drawing.Color;

namespace SmartsVer1.Control.Config.Assets
{
    public partial class ctrlConfig : System.Web.UI.UserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ctrlConfig));
        String LoginName, username, domain, GetClientDBString;
        const Int32 ClientType = 4, siloWSt = 3, globeWSt = 3, serviceGroupID = 2;
        Int32 ClientID = -99, pdCount = 0, gwCount = 0, awCount = 0, opCount = 0, msngCount = 0, planCount = 0, oprIndex = -1;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);                
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
                //Datasource for controls
                System.Data.DataTable sqcTable = AST.getSQCTable(GetClientDBString);
                this.gvwSQC.DataSource = sqcTable;
                this.gvwSQC.PageIndex = 0;
                this.gvwSQC.SelectedIndex = -1;
                this.gvwSQC.DataBind();
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientType);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = 0;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
                //Creating Asset Charts
                this.createAssetCharts(null, null);                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void createAssetCharts(object sender, EventArgs e)
        {
            try
            {
                List<Int32> wellDetails = CHRT.getAssetWellCount(GetClientDBString);
                pdCount = wellDetails[0];
                gwCount = wellDetails[1];
                opCount = wellDetails[2];
                awCount = wellDetails[3];
                msngCount = wellDetails[4];
                planCount = wellDetails[5];
                this.divWCount.InnerHtml = "<h3>" + pdCount.ToString() + " / " + gwCount.ToString() + "</h3><span>Well Pads / Well/Lateral</span>";
                this.divOPCount.InnerHtml = "<h3>" + opCount.ToString() + "</h3><span>SMARTs Operators</span>";
                this.divLWell.InnerHtml = "<h3>" + awCount.ToString() + "</h3><span>Active Well(s)</span>";
                this.divMWell.InnerHtml = "<h3>" + msngCount.ToString() + "</h3><span>Missing Ref. Mags</span>";
                this.divPWell.InnerHtml = "<h3>" + planCount.ToString() + "</h3><span>Missing Well Plan</span>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSQC_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable sqcTable = AST.getSQCTable(GetClientDBString);
                this.gvwSQC.DataSource = sqcTable;
                this.gvwSQC.PageIndex = e.NewPageIndex;
                this.gvwSQC.SelectedIndex = -1;
                this.gvwSQC.DataBind();
                this.btnSQCReset.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSQCNew_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnSQCReset_Click(null, null);
                this.divSQCList.Visible = false;
                this.divSQCAdd.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSQCReset_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable sqcTable = AST.getSQCTable(GetClientDBString);
                this.gvwSQC.DataSource = sqcTable;
                this.gvwSQC.PageIndex = 0;
                this.gvwSQC.SelectedIndex = -1;
                this.gvwSQC.DataBind();
                this.btnSQCReset.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddSQC_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                String dp = String.Empty, bt = String.Empty, gt = String.Empty, nm = String.Empty;
                Decimal dipD = -99.00M, btD = -99.00M, gtD = -99.00M;
                dp = Convert.ToString(this.txtSQCDip.Text);
                bt = Convert.ToString(this.txtSQCBT.Text);
                gt = Convert.ToString(this.txtSQCGT.Text);
                nm = Convert.ToString(this.txtSQCName.Text);
                if (String.IsNullOrEmpty(dp) || String.IsNullOrEmpty(bt) || String.IsNullOrEmpty(gt) || String.IsNullOrEmpty(nm))
                {
                    this.sqcEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                    this.iSQCMessage.Attributes["class"] = "icon fa fa-warning";
                    this.sqcHeader.InnerText = " Warning!";
                    this.lblSQCSuccess.Text = "Please provide all values before inserting Survey Qualification Criterion.";
                    this.sqcEnclosure.Visible = true;
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Error Adding SQC (Null Values) ");
                }
                else if(!Decimal.TryParse(dp, out dipD) || !Decimal.TryParse(bt, out btD) || !Decimal.TryParse(gt, out gtD))
                {
                    this.sqcEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                    this.iSQCMessage.Attributes["class"] = "icon fa fa-warning";
                    this.sqcHeader.InnerText = " Warning!";
                    this.lblSQCSuccess.Text = "Invalid values detected. Please provide valid values for Survey Qualification Criteria";
                    this.sqcEnclosure.Visible = true;
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Error Adding SQC (Null Values) ");
                }
                else
                {
                    iResult = Convert.ToInt32(AST.addSQC(nm, dp, bt, gt, GetClientDBString));
                    if (iResult.Equals(1))
                    {
                        this.sqcEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iSQCMessage.Attributes["class"] = "icon fa fa-check-circle";
                        this.sqcHeader.InnerText = " Success!";
                        this.lblSQCSuccess.Text = "Successfully inserted Survey Qualification Criterion, <u><b>" + nm + "</b></u> to SMARTs.";
                        this.sqcEnclosure.Visible = true;
                        logger.Info(" User: " + LoginName + " Realm: " + domain + " SQC Name: " + nm + " Added SQC ");
                    }
                    else if(iResult.Equals(-1))
                    {                        
                        this.sqcEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                        this.iSQCMessage.Attributes["class"] = "icon fa fa-warning";
                        this.sqcHeader.InnerText = " !!! Warning !!!";
                        this.lblSQCSuccess.Text = "There was a problem inserting Survey Qualification Criterion. Found Duplicate SQC Name/Identifier";
                        this.sqcEnclosure.Visible = true;
                        logger.Error(" User: " + LoginName + " Realm: " + domain + " Error Adding SQC (Other) ");
                    }
                    else
                    {
                        this.sqcEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                        this.iSQCMessage.Attributes["class"] = "icon fa fa-warning";
                        this.sqcHeader.InnerText = " !!! Warning !!!";
                        this.lblSQCSuccess.Text = "There was a problem inserting Survey Qualification Criterion. Incorrect values.";
                        this.sqcEnclosure.Visible = true;
                        logger.Error(" User: " + LoginName + " Realm: " + domain + " Error Adding SQC (Other) ");
                    }
                }                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddSQCReset_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtSQCName.Text = "";
                this.txtSQCDip.Text = "";
                this.txtSQCBT.Text = "";
                this.txtSQCGT.Text = "";
                this.sqcEnclosure.Visible = false;
                this.lblSQCSuccess.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddSQCExit_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnAddSQCReset_Click(null, null);
                this.btnSQCReset_Click(null, null);
                this.divSQCAdd.Visible = false;
                this.divSQCList.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwGlobalOptr_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientType);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = e.NewPageIndex;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
                this.btnOprReset.CssClass = "btn btn-warning text-center";
                this.btnOprReset.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwGlobalOptr_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                System.Data.DataTable padTable = AST.getClientWellPadTableForOperator(oprID, ClientID);
                this.gvwWellPads.DataSource = padTable;
                this.gvwWellPads.PageIndex = 0;
                this.gvwWellPads.SelectedIndex = -1;
                this.gvwWellPads.DataBind();
                this.gvwGlobalOptrAddressList.DataSource = null;
                this.gvwGlobalOptrAddressList.DataBind();
                this.gvwGlobalOptrAddressList.Visible = false;
                oprIndex = Convert.ToInt32(this.gvwGlobalOptr.SelectedIndex);
                GridViewRow dRow = this.gvwGlobalOptr.SelectedRow;
                String oprName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.lblPadList.Text = "Operator Company: <strong><u>" + oprName + "</u></strong> Well Pad(s) List";
                this.btnOprReset.CssClass = "btn btn-warning text-center";
                this.btnOprReset.Enabled = true;
                this.divOperators.Visible = false;
                this.divOperatorPads.Visible = true;
                this.li1.Attributes["class"] = "stepper-done";
                this.li2.Attributes["class"] = "stepper-done";
                this.li3.Attributes["class"] = "stepper-todo";
                this.li4.Attributes["class"] = "stepper-todo";
                this.li5.Attributes["class"] = "stepper-todo";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnOprDetails_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 opID = -99;                
                Button opBtn = (Button)sender;
                GridViewRow gRow = (GridViewRow)opBtn.NamingContainer;
                Int32 rowIndex = Convert.ToInt32(gRow.RowIndex);
                opID = Convert.ToInt32(gvwGlobalOptr.DataKeys[rowIndex]["clntID"]);                
                System.Data.DataTable addTable = CLNT.getAddressTable(opID);
                this.gvwGlobalOptrAddressList.DataSource = addTable;
                this.gvwGlobalOptrAddressList.PageIndex = 0;
                this.gvwGlobalOptrAddressList.SelectedIndex = -1;
                this.gvwGlobalOptrAddressList.DataBind();
                this.gvwGlobalOptrAddressList.Visible = true;
                this.btnOprReset.Enabled = true;
                this.btnOprReset.CssClass = "btn btn-warning text-center";
                this.li1.Attributes["class"] = "stepper-done";
                this.li2.Attributes["class"] = "stepper-todo";
                this.li3.Attributes["class"] = "stepper-todo";
                this.li4.Attributes["class"] = "stepper-todo";
                this.li5.Attributes["class"] = "stepper-todo";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwGlobalOptrAddressList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 opID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                System.Data.DataTable addTable = CLNT.getAddressTable(opID);
                this.gvwGlobalOptrAddressList.DataSource = addTable;
                this.gvwGlobalOptrAddressList.PageIndex = e.NewPageIndex;
                this.gvwGlobalOptrAddressList.SelectedIndex = -1;
                this.gvwGlobalOptrAddressList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnOprReset_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientType);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = 0;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
                this.gvwGlobalOptrAddressList.DataSource = null;
                this.gvwGlobalOptrAddressList.PageIndex = 0;
                this.gvwGlobalOptrAddressList.DataBind();
                this.gvwGlobalOptrAddressList.Visible = false;
                oprIndex = -1;
                this.btnOprReset.CssClass = "btn btn-default text-center";
                this.btnOprReset.Enabled = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellPads_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {                
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                System.Data.DataTable padTable = AST.getClientWellPadTableForOperator(oprID, ClientID);
                this.gvwWellPads.DataSource = padTable;
                this.gvwWellPads.PageIndex = e.NewPageIndex;
                this.gvwWellPads.SelectedIndex = -1;
                this.gvwWellPads.DataBind();
                oprIndex = Convert.ToInt32(this.gvwGlobalOptr.SelectedIndex);
                this.btnPadReset.CssClass = "btn btn-warning text-center";
                this.btnPadReset.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellPads_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 pdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                System.Data.DataTable pdTable = AST.getWellForWellPadTable(pdID, GetClientDBString);
                this.gvwWell.DataSource = pdTable;
                this.gvwWell.PageIndex = 0;
                this.gvwWell.SelectedIndex = -1;
                this.gvwWell.DataBind();
                this.gvwWellDetail.DataSource = null;
                this.gvwWellDetail.DataBind();
                this.divgvwWellDetail.Visible = false;
                this.gvwRMList.DataSource = null;
                this.gvwRMList.DataBind();
                this.divgvwRMList.Visible = false;
                this.gvwiRMList.DataSource = null;
                this.gvwiRMList.DataBind();
                this.gvwiRMList.Visible = false;
                this.gvwWellPlanInfo.DataSource = null;
                this.gvwWellPlanInfo.DataBind();
                this.divgvwWellPlanInfo.Visible = false;
                this.gvwWellPlanCalc.DataSource = null;
                this.gvwWellPlanCalc.DataBind();
                this.gvwWellPlanCalc.Visible = false;
                this.gvwWReg.DataSource = null;
                this.gvwWReg.DataBind();
                this.gvwWReg.Visible = false;
                this.divOperatorPads.Visible = false;
                this.divNewPad.Visible = false;
                this.divWellList.Visible = true;
                GridViewRow oRow = this.gvwGlobalOptr.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                GridViewRow pRow = this.gvwWellPads.SelectedRow;
                String pName = Convert.ToString(Server.HtmlDecode(pRow.Cells[2].Text));
                this.lblWellHeading.Text = "Operator Company: <strong><u>" + oName + "</u></strong> Well Pad: <strong><u> " + pName + ": " + "</u></strong> Well(s) / Lateral(s) List";
                this.li1.Attributes["class"] = "stepper-done";
                this.li2.Attributes["class"] = "stepper-done";
                this.li3.Attributes["class"] = "stepper-done";
                this.li4.Attributes["class"] = "stepper-todo";
                this.li5.Attributes["class"] = "stepper-todo";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddPad_Click(object sender, EventArgs e)
        {
            try
            {
                GridViewRow dRow = this.gvwGlobalOptr.SelectedRow;
                String oprName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.lblNewPad.Text = oprName + " Add New Well Pad";
                this.divOperatorPads.Visible = false;
                this.divNewPad.Visible = true;
                this.btnWPDReset_Click(null, null);
                Dictionary<Int32, String> regList = DMG.getRegionList();
                this.ddlWPDRegion.Items.Clear();
                this.ddlWPDRegion.DataSource = regList;
                this.ddlWPDRegion.DataTextField = "Value";
                this.ddlWPDRegion.DataValueField = "Key";
                this.ddlWPDRegion.DataBind();
                this.ddlWPDRegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlWPDRegion.SelectedIndex = -1;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnPadReset_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                System.Data.DataTable padTable = AST.getClientWellPadTableForOperator(oprID, ClientID);
                this.gvwWellPads.DataSource = padTable;
                this.gvwWellPads.PageIndex = 0;
                this.gvwWellPads.SelectedIndex = -1;
                this.gvwWellPads.DataBind();
                this.btnPadReset.CssClass = "btn btn-default text-center";
                this.btnPadReset.Enabled = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnPadStepBack_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnPadReset_Click(null, null);
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientType);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = 0;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
                this.btnOprReset.Enabled = false;
                this.btnOprReset.CssClass = "btn btn-default text-center";
                this.divOperators.Visible = true;
                this.divOperatorPads.Visible = false;
                this.li1.Attributes["class"] = "stepper-done";
                this.li2.Attributes["class"] = "stepper-todo";
                this.li3.Attributes["class"] = "stepper-todo";
                this.li4.Attributes["class"] = "stepper-todo";
                this.li5.Attributes["class"] = "stepper-todo";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        protected void ddlWPDRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 rgID = Convert.ToInt32(this.ddlWPDRegion.SelectedValue);
                Dictionary<Int32, String> srList = DMG.getSubRegionList(rgID);
                this.ddlWPDSubRegion.Items.Clear();
                this.ddlWPDSubRegion.DataSource = srList;
                this.ddlWPDSubRegion.DataTextField = "Value";
                this.ddlWPDSubRegion.DataValueField = "Key";
                this.ddlWPDSubRegion.DataBind();
                this.ddlWPDSubRegion.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlWPDSubRegion.SelectedIndex = -1;
                this.ddlWPDSubRegion.Enabled = true;
                this.ddlWPDCountry.Items.Clear();
                this.ddlWPDCountry.DataBind();
                this.ddlWPDCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlWPDCountry.SelectedIndex = -1;
                this.ddlWPDCountry.Enabled = false;
                this.ddlWPDState.Items.Clear();
                this.ddlWPDState.DataBind();
                this.ddlWPDState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlWPDState.SelectedIndex = -1;
                this.ddlWPDState.Enabled = false;
                this.ddlWPDCounty.Items.Clear();
                this.ddlWPDCounty.DataBind();
                this.ddlWPDCounty.Items.Insert(0, new ListItem("--- Select County/District/Division ---", "-1"));
                this.ddlWPDCounty.SelectedIndex = -1;
                this.ddlWPDCounty.Enabled = false;
                this.ddlWPDField.Items.Clear();
                this.ddlWPDField.DataBind();
                this.ddlWPDField.Items.Insert(0, new ListItem("--- Select Oil/Gas Field ---", "-1"));
                this.ddlWPDField.SelectedIndex = -1;
                this.ddlWPDField.Enabled = false;
                this.txtWPDName.Text = "";
                this.txtWPDName.Enabled = false;
                this.wpdEnclosure.Visible = false;
                this.lblWPD.Text = "";
                this.btnWPDAdd.Enabled = false;
                this.btnWPDAdd.CssClass = "btn btn-default text-center";
                this.btnWPDReset.Enabled = true;
                this.btnWPDReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWPDSubRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlWPDSubRegion.SelectedValue);
                Dictionary<Int32, String> ctyList = DMG.getCountryList(srID);
                this.ddlWPDCountry.Items.Clear();
                this.ddlWPDCountry.DataSource = ctyList;
                this.ddlWPDCountry.DataTextField = "Value";
                this.ddlWPDCountry.DataValueField = "Key";
                this.ddlWPDCountry.DataBind();
                this.ddlWPDCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlWPDCountry.SelectedIndex = -1;
                this.ddlWPDCountry.Enabled = true;
                this.ddlWPDState.Items.Clear();
                this.ddlWPDState.DataBind();
                this.ddlWPDState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlWPDState.SelectedIndex = -1;
                this.ddlWPDState.Enabled = false;
                this.ddlWPDCounty.Items.Clear();
                this.ddlWPDCounty.DataBind();
                this.ddlWPDCounty.Items.Insert(0, new ListItem("--- Select County/District/Division ---", "-1"));
                this.ddlWPDCounty.SelectedIndex = -1;
                this.ddlWPDCounty.Enabled = false;
                this.ddlWPDField.Items.Clear();
                this.ddlWPDField.DataBind();
                this.ddlWPDField.Items.Insert(0, new ListItem("--- Select Oil/Gas Field ---", "-1"));
                this.ddlWPDField.SelectedIndex = -1;
                this.ddlWPDField.Enabled = false;
                this.txtWPDName.Text = "";
                this.txtWPDName.Enabled = false;
                this.wpdEnclosure.Visible = false;
                this.lblWPD.Text = "";
                this.btnWPDAdd.Enabled = false;
                this.btnWPDAdd.CssClass = "btn btn-default text-center";
                this.btnWPDReset.Enabled = true;
                this.btnWPDReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWPDCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctyID = Convert.ToInt32(this.ddlWPDCountry.SelectedValue);
                Dictionary<Int32, String> stList = DMG.getStateList(ctyID);
                this.ddlWPDState.Items.Clear();
                this.ddlWPDState.DataSource = stList;
                this.ddlWPDState.DataTextField = "Value";
                this.ddlWPDState.DataValueField = "Key";
                this.ddlWPDState.DataBind();
                this.ddlWPDState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlWPDState.SelectedIndex = -1;
                this.ddlWPDState.Enabled = true;
                this.ddlWPDCounty.Items.Clear();
                this.ddlWPDCounty.DataBind();
                this.ddlWPDCounty.Items.Insert(0, new ListItem("--- Select County/District/Division ---", "-1"));
                this.ddlWPDCounty.SelectedIndex = -1;
                this.ddlWPDCounty.Enabled = false;
                this.ddlWPDField.Items.Clear();
                this.ddlWPDField.DataBind();
                this.ddlWPDField.Items.Insert(0, new ListItem("--- Select Oil/Gas Field ---", "-1"));
                this.ddlWPDField.SelectedIndex = -1;
                this.ddlWPDField.Enabled = false;
                this.txtWPDName.Text = "";
                this.txtWPDName.Enabled = false;
                this.wpdEnclosure.Visible = false;
                this.lblWPD.Text = "";
                this.btnWPDAdd.Enabled = false;
                this.btnWPDAdd.CssClass = "btn btn-default text-center";
                this.btnWPDReset.Enabled = true;
                this.btnWPDReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWPDState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 stID = Convert.ToInt32(this.ddlWPDState.SelectedValue);
                Dictionary<Int32, String> cnList = DMG.getCountyList(stID);
                this.ddlWPDCounty.Items.Clear();
                this.ddlWPDCounty.DataSource = cnList;
                this.ddlWPDCounty.DataTextField = "Value";
                this.ddlWPDCounty.DataValueField = "Key";
                this.ddlWPDCounty.DataBind();
                this.ddlWPDCounty.Items.Insert(0, new ListItem("--- Select County/District/Division ---", "-1"));
                this.ddlWPDCounty.SelectedIndex = -1;
                this.ddlWPDCounty.Enabled = true;
                this.ddlWPDField.Items.Clear();
                this.ddlWPDField.DataBind();
                this.ddlWPDField.Items.Insert(0, new ListItem("--- Select Oil/Gas Field ---", "-1"));
                this.ddlWPDField.SelectedIndex = -1;
                this.ddlWPDField.Enabled = false;
                this.txtWPDName.Text = "";
                this.txtWPDName.Enabled = false;
                this.wpdEnclosure.Visible = false;
                this.lblWPD.Text = "";
                this.btnWPDAdd.Enabled = false;
                this.btnWPDAdd.CssClass = "btn btn-default text-center";
                this.btnWPDReset.Enabled = true;
                this.btnWPDReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWPDCounty_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cnID = Convert.ToInt32(this.ddlWPDCounty.SelectedValue);
                Dictionary<Int32, String> fldList = DMG.getFieldWithWellPadCountList(cnID);
                this.ddlWPDField.Items.Clear();
                this.ddlWPDField.DataSource = fldList;
                this.ddlWPDField.DataTextField = "Value";
                this.ddlWPDField.DataValueField = "Key";
                this.ddlWPDField.DataBind();
                this.ddlWPDField.Items.Insert(0, new ListItem("--- Select Oil/Gas Field ---", "-1"));
                this.ddlWPDField.SelectedIndex = -1;
                this.ddlWPDField.Enabled = true;
                this.txtWPDName.Text = "";
                this.txtWPDName.Enabled = false;
                this.wpdEnclosure.Visible = false;
                this.lblWPD.Text = "";
                this.btnWPDAdd.Enabled = false;
                this.btnWPDAdd.CssClass = "btn btn-default text-center";
                this.btnWPDReset.Enabled = true;
                this.btnWPDReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWPDField_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtWPDName.Text = "";
                this.txtWPDName.Enabled = true;
                this.wpdEnclosure.Visible = false;
                this.lblWPD.Text = "";
                this.btnWPDAdd.Enabled = true;
                this.btnWPDAdd.CssClass = "btn btn-success text-center";
                this.btnWPDReset.Enabled = true;
                this.btnWPDReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWPDAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, regID = -99, srID = -99, ctyID = -99, stID = -99, cnyID = -99, fldID = -99, oprID = -99;
                String padName = String.Empty;
                regID = Convert.ToInt32(this.ddlWPDRegion.SelectedValue);
                srID = Convert.ToInt32(this.ddlWPDSubRegion.SelectedValue);
                ctyID = Convert.ToInt32(this.ddlWPDCountry.SelectedValue);
                stID = Convert.ToInt32(this.ddlWPDState.SelectedValue);
                cnyID = Convert.ToInt32(this.ddlWPDCounty.SelectedValue);
                fldID = Convert.ToInt32(this.ddlWPDField.SelectedValue);
                oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                padName = Convert.ToString(this.txtWPDName.Text);
                if (String.IsNullOrEmpty(padName))
                {
                    this.txtWPDName.Text = "";
                    this.txtWPDName.CssClass = "form-control has-error has-feedback";
                    this.wpdEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iWPD.Attributes["class"] = "icon fa fa-warning";
                    this.spnWPD.InnerText = " Warning!";
                    this.lblWPD.Text = "Unable to create a Well Pad. Please provide a valid Well Pad Name";
                    this.wpdEnclosure.Visible = true;
                    logger.Info(" User: " + LoginName + " Realm: " + domain + " Well Pad Name: " + padName + " Empty Well Pad Name ");
                }
                else
                {
                    iResult = AST.insertWellPad(ClientID, oprID, regID, srID, ctyID, stID, cnyID, fldID, padName, LoginName, domain, GetClientDBString);
                    if (iResult.Equals(-1))
                    {
                        this.txtWPDName.Text = "";
                        this.txtWPDName.CssClass = "form-control has-error has-feedback";
                        this.wpdEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iWPD.Attributes["class"] = "icon fa fa-warning";
                        this.spnWPD.InnerText = " Duplicate Value!";
                        this.lblWPD.Text = "Unable to create a Duplicate Well Pad. Please provide a unique Well Pad Name";
                        this.wpdEnclosure.Visible = true;
                        logger.Info(" User: " + LoginName + " Realm: " + domain + " Well Pad Name: " + padName + " Duplicate Well Pad ");
                    }
                    else
                    {
                        this.btnWPDReset_Click(null, null);
                        this.wpdEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iWPD.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnWPD.InnerText = " Success!";
                        this.lblWPD.Text = "Successfully added Well Pad to SMARTs";
                        this.wpdEnclosure.Visible = true;
                        logger.Info(" User: " + LoginName + " Realm: " + domain + " Well Pad Name: " + padName + " Inserted Well Pad ");
                        this.createAssetCharts(null, null);
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWPDReset_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> regList = DMG.getRegionList();
                this.ddlWPDRegion.Items.Clear();
                this.ddlWPDRegion.DataSource = regList;
                this.ddlWPDRegion.DataTextField = "Value";
                this.ddlWPDRegion.DataValueField = "Key";
                this.ddlWPDRegion.DataBind();
                this.ddlWPDRegion.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlWPDRegion.SelectedIndex = -1;
                this.ddlWPDSubRegion.Items.Clear();
                this.ddlWPDSubRegion.DataSource = null;
                this.ddlWPDSubRegion.DataBind();
                this.ddlWPDSubRegion.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlWPDSubRegion.SelectedIndex = -1;
                this.ddlWPDSubRegion.Enabled = false;
                this.ddlWPDCountry.Items.Clear();
                this.ddlWPDCountry.DataSource = null;
                this.ddlWPDCountry.DataBind();
                this.ddlWPDCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlWPDCountry.SelectedIndex = -1;
                this.ddlWPDCountry.Enabled = false;
                this.ddlWPDState.Items.Clear();
                this.ddlWPDState.DataSource = null;
                this.ddlWPDState.DataBind();
                this.ddlWPDState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlWPDState.SelectedIndex = -1;
                this.ddlWPDState.Enabled = false;
                this.ddlWPDCounty.Items.Clear();
                this.ddlWPDCounty.DataSource = null;
                this.ddlWPDCounty.DataBind();
                this.ddlWPDCounty.Items.Insert(0, new ListItem("--- Select County/District/Division ---", "-1"));
                this.ddlWPDCounty.SelectedIndex = -1;
                this.ddlWPDCounty.Enabled = false;
                this.ddlWPDField.Items.Clear();
                this.ddlWPDField.DataSource = null;
                this.ddlWPDField.DataBind();
                this.ddlWPDField.Items.Insert(0, new ListItem("--- Select Oil/Gas Field ---", "-1"));
                this.ddlWPDField.SelectedIndex = -1;
                this.ddlWPDField.Enabled = false;
                this.txtWPDName.Text = "";
                this.txtWPDName.Enabled = false;
                this.wpdEnclosure.Visible = false;
                this.lblWPD.Text = "";
                this.btnWPDAdd.Enabled = false;
                this.btnWPDAdd.CssClass = "btn btn-default text-center";
                this.btnWPDReset.Enabled = false;
                this.btnWPDReset.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWPDDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnWPDReset_Click(null, null);
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                System.Data.DataTable padTable = AST.getClientWellPadTableForOperator(oprID, ClientID);
                this.gvwWellPads.DataSource = padTable;
                this.gvwWellPads.PageIndex = 0;
                this.gvwWellPads.SelectedIndex = oprIndex;
                this.gvwWellPads.DataBind();
                this.divOperatorPads.Visible = true;
                this.divNewPad.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddWell_Click(object sender, EventArgs e)
        {
            try
            {
                this.divWellList.Visible = false;
                this.divWellAdd.Visible = true;
                this.divWellPlan.Visible = false;
                this.divWellStatus.Visible = false;
                this.divWRegister.Visible = false;
                GridViewRow oRow = this.gvwGlobalOptr.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                GridViewRow pRow = this.gvwWellPads.SelectedRow;
                String pName = Convert.ToString(Server.HtmlDecode(pRow.Cells[2].Text));
                this.lblWellAddHeading.Text = "Operator Company: <strong><u>" + oName + "</u></strong> Well Pad: <strong><u> " + pName + "</u></strong>: Add New Well(s) / Lateral(s)";
                this.btnAddWellCancel_Click(null, null);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        protected void btnWellReset_Click(object sender, EventArgs e)
        {
            try
            {
                this.divWellPlan.Visible = false;
                this.divWellStatus.Visible = false;
                this.divWRegister.Visible = false;
                this.divDisplayButtons.Visible = false;
                this.charts.Visible = false;
                this.ltrECHarts3DwithPlan.Text = "";
                this.divWellDetail.Visible = false;
                this.gvwWellDetail.DataSource = null;
                this.gvwWellDetail.DataBind();
                this.divgvwWellDetail.Visible = false;
                this.divlblWellPlanTI.Visible = false;
                this.divWellRefMag.Visible = false;
                this.gvwRMList.DataSource = null;
                this.gvwRMList.DataBind();
                this.divgvwRMList.Visible = false;
                this.gvwiRMList.DataSource = null;
                this.gvwiRMList.DataBind();
                this.gvwiRMList.Visible = false;
                this.gvwWellPlanInfo.DataSource = null;
                this.gvwWellPlanInfo.DataBind();
                this.divgvwWellPlanInfo.Visible = false;
                this.lblWellPlanCalc.Visible = false;
                this.gvwWellPlanCalc.DataSource = null;
                this.gvwWellPlanCalc.DataBind();
                this.gvwWellPlanCalc.Visible = false;
                this.lblWPCTieIn.Visible = false;
                this.gvwWPCTieIn.DataSource = null;
                this.gvwWPCTieIn.DataBind();
                this.gvwWPCTieIn.Visible = false;
                this.lblWellPlaceCalc.Visible = false;
                this.gvwWellPlaceCalc.DataSource = null;
                this.gvwWellPlaceCalc.DataBind();
                this.gvwWellPlaceCalc.Visible = false;
                this.divlblWellRegistrations.Visible = false;
                this.gvwWReg.DataSource = null;
                this.gvwWReg.DataBind();
                this.gvwWReg.Visible = false;
                this.btnWellReset.Enabled = false;
                this.btnWellReset.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWellStepBack_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnWellReset_Click(null, null);
                this.divOperatorPads.Visible = true;
                this.divWellList.Visible = false;
                this.lblWellName.Text = "";
                this.divWellNameDetail.Visible = false;
                this.li1.Attributes["class"] = "stepper-done";
                this.li2.Attributes["class"] = "stepper-done";
                this.li3.Attributes["class"] = "stepper-todo";
                this.li4.Attributes["class"] = "stepper-todo";
                this.li5.Attributes["class"] = "stepper-todo";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWell_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 pdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                System.Data.DataTable pdTable = AST.getWellForWellPadTable(pdID, GetClientDBString);
                this.gvwWell.DataSource = pdTable;
                this.gvwWell.PageIndex = e.NewPageIndex;
                this.gvwWell.SelectedIndex = -1;
                this.gvwWell.DataBind();
                this.btnWellReset.Enabled = true;
                this.btnWellReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWell_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 wlID = Convert.ToInt32(this.gvwWell.SelectedValue);
                System.Data.DataTable wsTable = OPR.getWellSectionToWellTable(wlID, GetClientDBString);
                this.gvwSectionList.DataSource = wsTable;
                this.gvwSectionList.PageIndex = 0;
                this.gvwSectionList.SelectedIndex = -1;
                this.gvwSectionList.DataBind();
                this.gvwSectionList.Visible = true;
                this.divDisplayButtons.Visible = false;
                this.charts.Visible = false;
                this.ltrECHarts3DwithPlan.Text = "";
                this.divWellDetail.Visible = false;
                this.divgvwWellDetail.Visible = false;
                this.divWellRefMag.Visible = false;
                this.divgvwRMList.Visible = false;
                this.gvwiRMList.Visible = false;
                this.divlblWellPlanTI.Visible = false;
                this.divgvwWellPlanInfo.Visible = false;
                this.lblWellPlanCalc.Visible = false;
                this.lblWPCTieIn.Visible = false;
                this.gvwWPCTieIn.Visible = false;
                this.gvwWellPlanCalc.Visible = false;
                this.lblWellPlaceCalc.Visible = false;
                this.gvwWellPlaceCalc.Visible = false;
                this.divlblWellRegistrations.Visible = false;
                this.gvwWReg.Visible = false;  
                GridViewRow oRow = this.gvwGlobalOptr.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                GridViewRow pRow = this.gvwWellPads.SelectedRow;
                String pName = Convert.ToString(Server.HtmlDecode(pRow.Cells[2].Text));
                GridViewRow wRow = this.gvwWell.SelectedRow;
                String wName = Convert.ToString(Server.HtmlDecode(wRow.Cells[2].Text));
                this.lblSectionList.Text = "Operator Company: <strong><u>" + oName + "</u></strong> Well Pad: <strong><u> " + pName + "</u></strong> Well/Lateral: <strong><u>" + wName + ": </u></strong> Well/Lateral Section List";
                this.divWellList.Visible = false;
                this.divWellAdd.Visible = false;
                this.divSection.Visible = true;
                this.divSectionAdd.Visible = false;
                this.li1.Attributes["class"] = "stepper-done";
                this.li2.Attributes["class"] = "stepper-done";
                this.li3.Attributes["class"] = "stepper-done";
                this.li4.Attributes["class"] = "stepper-done";
                this.li5.Attributes["class"] = "stepper-todo";
                this.divWellRF.Visible = false;
                this.divWellPlan.Visible = false;
                this.divWellPlacement.Visible = false;
                this.divWellStatus.Visible = false;
                this.divWRegister.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWellDetails_Click(object sender, EventArgs e)
        {
            try
            {
                this.divWellAdd.Visible = false;
                this.divWellRF.Visible = false;
                this.divWellPlan.Visible = false;
                this.divWellStatus.Visible = false;
                this.divWRegister.Visible = false;
                this.charts.Visible = false;
                Button welBtn = (Button)sender;
                GridViewRow gRow = (GridViewRow)welBtn.NamingContainer;
                Int32 rowIndex = Convert.ToInt32(gRow.RowIndex);
                String wellname = null;
                wellname = Convert.ToString(Server.HtmlDecode(gRow.Cells[2].Text));
                Int32 optrID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue), wpdID = Convert.ToInt32(this.gvwWellPads.SelectedValue), welID = Convert.ToInt32(gvwWell.DataKeys[rowIndex]["welID"]);
                this.hdnWell.Value = welID.ToString();
                System.Data.DataTable detailTable = AST.getWellDetailTable(welID, GetClientDBString);
                this.gvwWellDetail.DataSource = detailTable;
                this.gvwWellDetail.PageIndex = 0;
                this.gvwWellDetail.SelectedIndex = -1;
                this.gvwWellDetail.DataBind();
                this.divgvwWellDetail.Visible = true;
                //this.lblWellDetail.Visible = true;
                this.divWellDetail.Visible = true;
                System.Data.DataTable wpInfoTable = AST.getWellPlanInfoTable(wpdID, welID, GetClientDBString);
                this.gvwWellPlanInfo.DataSource = wpInfoTable;
                this.gvwWellPlanInfo.PageIndex = 0;
                this.gvwWellPlanInfo.SelectedIndex = -1;
                this.gvwWellPlanInfo.DataBind();
                this.divgvwWellPlanInfo.Visible = true;
                this.divlblWellPlanTI.Visible = true;
                System.Data.DataTable wpCalcPlan = WPN.getCalcWellPlan(welID, GetClientDBString);
                this.gvwWellPlanCalc.DataSource = wpCalcPlan;
                this.gvwWellPlanCalc.PageIndex = 0;
                this.gvwWellPlanCalc.SelectedIndex = -1;
                this.gvwWellPlanCalc.DataBind();
                this.lblWellPlaceCalc.Visible = true;
                System.Data.DataTable wpcTieIn = AST.getWellPlacementTieInTable(optrID, ClientID, wpdID, welID, GetClientDBString);
                this.gvwWPCTieIn.DataSource = wpcTieIn;
                this.gvwWPCTieIn.PageIndex = 0;
                this.gvwWPCTieIn.SelectedIndex = -1;
                this.gvwWPCTieIn.DataBind();
                this.gvwWPCTieIn.Visible = true;
                this.lblWPCTieIn.Visible = true;
                System.Data.DataTable wpPlace = WPN.getCalcWellPlacement(optrID, ClientID, wpdID, welID, GetClientDBString);
                this.gvwWellPlaceCalc.DataSource = wpPlace;
                this.gvwWellPlaceCalc.PageIndex = 0;
                this.gvwWellPlaceCalc.SelectedIndex = -1;
                this.gvwWellPlaceCalc.DataBind();
                this.gvwWellPlaceCalc.Visible = true;
                System.Data.DataTable refmagTable = OPR.getReferenceMagneticsTable(optrID, ClientID, wpdID, welID, GetClientDBString);
                Int32 ifrV = OPR.getIFR(welID, GetClientDBString);
                if (ifrV.Equals(1))
                {
                    this.gvwRMList.DataSource = refmagTable;
                    this.gvwRMList.DataBind();
                    this.gvwRMList.Visible = true;
                    this.gvwiRMList.DataSource = null;
                    this.gvwiRMList.DataBind();
                    this.gvwiRMList.Visible = false;
                    this.divgvwRMList.Visible = true;
                    this.divWellRefMag.Visible = true;
                }
                else
                {
                    this.gvwRMList.DataSource = null;
                    this.gvwRMList.DataBind();
                    this.gvwRMList.Visible = false;
                    this.gvwiRMList.DataSource = refmagTable;
                    this.gvwiRMList.DataBind();
                    this.gvwiRMList.Visible = true;
                    this.gvwiRMList.Visible = true;
                    this.divgvwRMList.Visible = true;
                    this.divWellRefMag.Visible = true;
                }
                System.Data.DataTable rgrsTable = AST.getWellServiceRegistration(optrID, ClientID, wpdID, welID, GetClientDBString);
                this.gvwWReg.DataSource = rgrsTable;
                this.gvwWReg.PageIndex = 0;
                this.gvwWReg.SelectedIndex = -1;
                this.gvwWReg.DataBind();
                this.gvwWReg.Visible = true;
                this.divlblWellRegistrations.Visible = true;
                this.gvwWellPlanCalc.Visible = true;
                this.lblWellPlanCalc.Visible = true;
                this.divDisplayButtons.Visible = true;
                this.btnWellReset.CssClass = "btn btn-warning text-center";
                this.btnWellReset.Enabled = true;
                this.charts.Visible = false;
                this.lblWellName.Text = " Details for Well: " + wellname;
                this.divWellNameDetail.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddRF_Click(object sender, EventArgs e)
        {
            try
            {                
                this.divWellAdd.Visible = false;
                this.divWellRF.Visible = true;
                this.divWellPlan.Visible = false;
                this.divWellPlacement.Visible = false;
                this.divWellStatus.Visible = false;
                this.divWRegister.Visible = false;
                this.charts.Visible = false;
                this.btnAddWell.Visible = false;
                this.btnWellReset.Visible = false;
                this.btnWellStepBack.Visible = false;
                this.btnWellExit.Visible = false;
                Button welBtn = (Button)sender;
                GridViewRow gRow = (GridViewRow)welBtn.NamingContainer;
                String wellname = null;
                wellname = Convert.ToString(Server.HtmlDecode(gRow.Cells[2].Text));
                Int32 rowIndex = Convert.ToInt32(gRow.RowIndex);
                Int32 optrID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue), wpdID = Convert.ToInt32(this.gvwWellPads.SelectedValue), welID = Convert.ToInt32(gvwWell.DataKeys[rowIndex]["welID"]);
                this.hdnWell.Value = welID.ToString();
                this.divDisplayButtons.Visible = false;
                this.ltrECHarts3DwithPlan.Text = "";
                this.divWellDetail.Visible = false;
                this.gvwWellDetail.DataSource = null;
                this.gvwWellDetail.DataBind();
                this.divgvwWellDetail.Visible = false;
                this.divWellRefMag.Visible = false;
                this.gvwRMList.DataSource = null;
                this.gvwRMList.DataBind();
                this.divgvwRMList.Visible = false;
                this.gvwiRMList.Visible = false;
                this.gvwiRMList.DataSource = null;
                this.gvwiRMList.DataBind();
                this.divgvwRMList.Visible = false;
                this.divlblWellPlanTI.Visible = false;
                this.gvwWellPlanInfo.DataSource = null;
                this.gvwWellPlanInfo.DataBind();
                this.divgvwWellPlanInfo.Visible = false;
                this.lblWellPlanCalc.Visible = false;
                this.gvwWellPlanCalc.DataSource = null;
                this.gvwWellPlanCalc.DataBind();
                this.gvwWellPlanCalc.Visible = false;
                this.lblWPCTieIn.Visible = false;
                this.gvwWPCTieIn.DataSource = null;
                this.gvwWPCTieIn.DataBind();
                this.gvwWPCTieIn.Visible = false;
                this.lblWellPlaceCalc.Visible = false;
                this.gvwWellPlaceCalc.DataSource = null;
                this.gvwWellPlaceCalc.DataBind();
                this.gvwWellPlaceCalc.Visible = false;
                this.divlblWellRegistrations.Visible = false;
                this.gvwWReg.DataSource = null;
                this.gvwWReg.DataBind();
                this.gvwWReg.Visible = false;
                this.btnWellReset.CssClass = "btn btn-warning text-center";
                this.btnWellReset.Enabled = true;
                this.rdbCTrue.Checked = false;
                this.rdbCTrue.Focus();
                this.rdbCGrid.Checked = false;
                this.charts.Visible = false;
                this.lblWellName.Text = " Selected Well: " + wellname;
                this.divWellNameDetail.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddPlan_Click(object sender, EventArgs e)
        {
            try
            {
                this.divWellAdd.Visible = false;
                this.divWellRF.Visible = false;
                this.divWellPlan.Visible = true;
                this.divWellPlacement.Visible = false;
                this.divWellStatus.Visible = false;
                this.divWRegister.Visible = false;
                this.charts.Visible = false;
                Button welBtn = (Button)sender;
                GridViewRow gRow = (GridViewRow)welBtn.NamingContainer;
                String wellname = null;
                wellname = Convert.ToString(Server.HtmlDecode(gRow.Cells[2].Text));
                Int32 rowIndex = Convert.ToInt32(gRow.RowIndex);
                Int32 optrID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue), wpdID = Convert.ToInt32(this.gvwWellPads.SelectedValue), welID = Convert.ToInt32(gvwWell.DataKeys[rowIndex]["welID"]);
                this.hdnWell.Value = welID.ToString();
                this.divDisplayButtons.Visible = false;
                this.ltrECHarts3DwithPlan.Text = "";
                this.divWellDetail.Visible = false;
                this.gvwWellDetail.DataSource = null;
                this.gvwWellDetail.DataBind();
                this.divgvwWellDetail.Visible = false;
                this.divWellRefMag.Visible = false;
                this.gvwRMList.DataSource = null;
                this.gvwRMList.DataBind();
                this.divgvwRMList.Visible = false;
                this.gvwiRMList.Visible = false;
                this.gvwiRMList.DataSource = null;
                this.gvwiRMList.DataBind();
                this.divlblWellPlanTI.Visible = false;
                this.gvwWellPlanInfo.DataSource = null;
                this.gvwWellPlanInfo.DataBind();
                this.divgvwWellPlanInfo.Visible = false;
                this.lblWellPlanCalc.Visible = false;
                this.gvwWellPlanCalc.DataSource = null;
                this.gvwWellPlanCalc.DataBind();
                this.gvwWellPlanCalc.Visible = false;
                this.lblWPCTieIn.Visible = false;
                this.gvwWPCTieIn.DataSource = null;
                this.gvwWPCTieIn.DataBind();
                this.gvwWPCTieIn.Visible = false;
                this.lblWellPlaceCalc.Visible = false;
                this.gvwWellPlaceCalc.DataSource = null;
                this.gvwWellPlaceCalc.DataBind();
                this.gvwWellPlaceCalc.Visible = false;
                this.divlblWellRegistrations.Visible = false;
                this.gvwWReg.DataSource = null;
                this.gvwWReg.DataBind();
                this.gvwWReg.Visible = false;
                this.btnWellReset.CssClass = "btn btn-warning text-center";
                this.btnWellReset.Enabled = true;
                this.charts.Visible = false;
                this.lblWellName.Text = " Selected Well: " + wellname;
                this.divWellNameDetail.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }



        protected void btnWPProcess_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 OptrID = -99, PadID = -99, WellID = -99, DataFileID = -99, permDat = -99, drefID = -99, drefUnit = -99, lenUnit = -99, nUnit = -99, eUnit = -99;
                Decimal refElev = -99.00M, vsAzim = -99.00M, northing = -99.00M, easting = -99.00M;
                Int32 mdUnit = -99, tvdUnit = -99, nOffUnit = -99, eOffUnit = -99, vsUnit = -99, dgUnit = -99, brUnit = -99, trUnit = -99, ssUnit = -99;
                Decimal tiMD = -99.00M, tiTVD = -99.00M, tiNOff = -99.00M, tiEOff = -99.00M, tiVS = -99.00M, tiDGLG = -99.00M, tiBR = -99.00M, tiTR = -99.00M, tiSS = -99.00M;
                String welName = String.Empty;
                ArrayList metainfo = new ArrayList(), tiinfo = new ArrayList();
                OptrID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                PadID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                WellID = Convert.ToInt32(this.hdnWell.Value);
                if (rdbGE.Checked) { permDat = 2; } else { permDat = 1; } if (rdbGElev.Checked) { drefID = 3; } else if (rdbKB.Checked) { drefID = 1; } else { drefID = 2; }
                if (rdbElFeet.Checked) { drefUnit = 4; } else { drefUnit = 5; } refElev = Convert.ToDecimal(this.txtWPDRElevation.Text);
                if (rdbVSecFt.Checked) { lenUnit = 4; } else { lenUnit = 5; } vsAzim = Convert.ToDecimal(this.txtWPVSAzimuth.Text);
                if (rdbNFeet.Checked) { nUnit = 4; } else { nUnit = 5; } northing = Convert.ToDecimal(this.txtWPNorthing.Text);
                if (rdbEFeet.Checked) { eUnit = 4; } else { eUnit = 5; } easting = Convert.ToDecimal(this.txtWPEasting.Text);
                metainfo.Add(permDat);
                metainfo.Add(drefID);
                metainfo.Add(drefUnit);
                metainfo.Add(refElev);
                metainfo.Add(lenUnit);
                metainfo.Add(vsAzim);
                metainfo.Add(nUnit);
                metainfo.Add(northing);
                metainfo.Add(eUnit);
                metainfo.Add(easting);
                if (rdbMDUnitFT.Checked) { mdUnit = 4; } else { mdUnit = 5; } tiMD = Convert.ToDecimal(this.txtTIMD.Text);
                if (rdbTVDFt.Checked) { tvdUnit = 4; } else { tvdUnit = 5; } tiTVD = Convert.ToDecimal(this.txtTITVD.Text);
                if (rdbNOFt.Checked) { nOffUnit = 4; } else { nOffUnit = 5; } tiNOff = Convert.ToDecimal(this.txtTINorth.Text);
                if (rdbEOFt.Checked) { eOffUnit = 4; } else { eOffUnit = 5; } tiEOff = Convert.ToDecimal(this.txtTIEast.Text);
                if (rdbVSecFt.Checked) { vsUnit = 4; } else { vsUnit = 5; } tiVS = Convert.ToDecimal(this.txtVS.Text);
                if (rdbDLSFt.Checked) { dgUnit = 4; } else { dgUnit = 5; } tiDGLG = Convert.ToDecimal(this.txtDGLG.Text);
                if (rdbSSDFt.Checked) { ssUnit = 4; } else { ssUnit = 5; } tiSS = Convert.ToDecimal(this.txtSS.Text);
                tiinfo.Add(mdUnit); tiinfo.Add(tiMD);
                tiinfo.Add(tvdUnit); tiinfo.Add(tiTVD);
                tiinfo.Add(nOffUnit); tiinfo.Add(tiNOff);
                tiinfo.Add(eOffUnit); tiinfo.Add(tiEOff);
                tiinfo.Add(vsUnit); tiinfo.Add(tiVS);
                tiinfo.Add(dgUnit); tiinfo.Add(tiDGLG);
                tiinfo.Add(brUnit); tiinfo.Add(tiBR);
                tiinfo.Add(trUnit); tiinfo.Add(tiTR);
                tiinfo.Add(ssUnit); tiinfo.Add(tiSS);
                System.Data.DataTable iResult = new System.Data.DataTable(), planSource = new System.Data.DataTable();
                planSource = (System.Data.DataTable)(HttpContext.Current.Session["RawWPlanDataTable"]);
                DataFileID = Convert.ToInt32(HttpContext.Current.Session["FileID"]);                
                iResult = WPN.processWPData(OptrID, ClientID, DataFileID, PadID, WellID, metainfo, tiinfo, planSource, LoginName, domain, GetClientDBString);
                HttpContext.Current.Session["calcPlan"] = iResult;
                this.btnWPUpload.CssClass = "btn btn-default";
                this.btnWPUpload.Enabled = false;
                this.btnWPAddClear.CssClass = "btn btn-default";
                this.btnWPAddClear.Enabled = false;
                this.btnWPProcess.CssClass = "btn btn-default";
                this.btnWPProcess.Visible = false;
                this.btnWPProcessRest.Visible = false;
                this.divPlanFile.Visible = false;
                this.gvwRawWellPlan.Visible = false;
                this.divWellPlanCalc.Visible = true;
                this.gvwWPCalc.DataSource = iResult;
                this.gvwWPCalc.PageIndex = 0;
                this.gvwWPCalc.DataBind();
                this.gvwWPCalc.Visible = true;
                this.createAssetCharts(null, null);
                this.liPlan.Attributes["class"] = "stepper-done";
                logger.Info(" User: " + LoginName + " Realm: " + domain + " Well Name: " + WellID + " Processed Well/Lateral Plan ");
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWPProcessRest_Click(object sender, EventArgs e)
        {
            try
            {
                this.processWPData.Visible = false;
                this.divMetaInfo.Visible = true;
                this.btnWPAddClear_Click(null, null);
                this.resetWellPlanRadios(null, null);
                Int32 WellID = Convert.ToInt32(this.hdnWell.Value);
                String WellName = AST.getWellName(WellID, GetClientDBString);
                Int32 rs = WPN.resetWellPlan(WellID, GetClientDBString);
                if (rs.Equals(1))
                {
                    this.wellEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                    this.iWellMessage.Attributes["class"] = "icon fa fa-success";
                    this.spnWellHeader.InnerText = " Reset!";
                    this.lblWellSuccess.Text = "Successfully reset Well Plan.";
                    this.wellEnclosure.Visible = true;
                    this.liMI.Attributes["class"] = "stepper-todo";
                    this.liTI.Attributes["class"] = "stepper-todo";
                    this.liFile.Attributes["class"] = "stepper-todo";
                    this.liPlan.Attributes["class"] = "stepper-todo";
                    logger.Info(" User: " + LoginName + " Realm: " + domain + " Well Name: " + WellName + " Deleted Well Plan ");
                }
                else
                {
                    this.wellEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iWellMessage.Attributes["class"] = "icon fa fa-warning";
                    this.spnWellHeader.InnerText = " Warning!";
                    this.lblWellSuccess.Text = "!!! Error !!! Couldn't reset Well Plan.";
                    this.wellEnclosure.Visible = true;
                    logger.Info(" User: " + LoginName + " Realm: " + domain + " Well Name: " + WellName + " Delete Well Plan Failed ");
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellPlanCalc_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 welID = Convert.ToInt32(this.hdnWell.Value);
                System.Data.DataTable wpCalcPlan = WPN.getCalcWellPlan(welID, GetClientDBString);
                this.gvwWellPlanCalc.DataSource = wpCalcPlan;
                this.gvwWellPlanCalc.PageIndex = e.NewPageIndex;
                this.gvwWellPlanCalc.SelectedIndex = -1;
                this.gvwWellPlanCalc.DataBind();
                this.divDisplayButtons.Visible = true;
                this.charts.Visible = false;                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWellPlaceCalc_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 wpdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                Int32 welID = Convert.ToInt32(this.hdnWell.Value);
                System.Data.DataTable wpCalcPlace = WPN.getCalcWellPlacement(oprID, ClientID, wpdID, welID, GetClientDBString);
                this.gvwWellPlaceCalc.DataSource = wpCalcPlace;
                this.gvwWellPlaceCalc.PageIndex = e.NewPageIndex;
                this.gvwWellPlaceCalc.SelectedIndex = -1;
                this.gvwWellPlaceCalc.DataBind();
                this.divDisplayButtons.Visible = true;
                this.charts.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRawWellPlan_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable rdTable = (System.Data.DataTable)HttpContext.Current.Session["RawWPlanDataTable"];
                this.gvwRawWellPlan.DataSource = rdTable;
                this.gvwRawWellPlan.PageIndex = e.NewPageIndex;
                this.gvwRawWellPlan.SelectedIndex = -1;
                this.gvwRawWellPlan.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbDLSFt_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbDLSM.Checked = false;
                this.txtDGLG.Text = "";
                this.txtDGLG.Enabled = true;
                this.txtDGLG.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbDLSM_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbDLSFt.Checked = false;
                this.txtDGLG.Text = "";
                this.txtDGLG.Enabled = true;
                this.txtDGLG.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWPCalc_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 wellID = Convert.ToInt32(this.hdnWell.Value);
                System.Data.DataTable cdTable = WPN.getCalcWellPlan(wellID, GetClientDBString);
                this.gvwWPCalc.DataSource = cdTable;
                this.gvwWPCalc.PageIndex = e.NewPageIndex;
                this.gvwWPCalc.SelectedIndex = -1;
                this.gvwWPCalc.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWPUpload_Click(object sender, EventArgs e)
        {
            try
            {
                ArrayList iResult = new ArrayList();
                List<String> fileD = new List<String>();
                String[] valuesRead = new String[] { };
                Int32 optrID = -99, wellPadID = -99, wellID = -99, datumID = -99, depthRefID = -99, elevationRefUnitID = -99, lengthUnitID = -99, northUnitID = -99, eastUnitID = -99;
                Decimal elevation = -99.00M, azimuth = -99.00M, northing = -99.00M, easting = -99.00M;
                String dataFile = String.Empty, fileName = String.Empty, fileExtension = String.Empty;
                String elevationValue = String.Empty, azimuthValue = String.Empty, northValue = String.Empty, eastValue = String.Empty;
                optrID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                wellPadID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                wellID = Convert.ToInt32(this.hdnWell.Value);
                elevationValue = Convert.ToString(this.txtWPDRElevation.Text);
                azimuthValue = Convert.ToString(this.txtWPVSAzimuth.Text);
                northValue = Convert.ToString(this.txtWPNorthing.Text);
                eastValue = Convert.ToString(this.txtWPEasting.Text);
                if (String.IsNullOrEmpty(elevationValue) || String.IsNullOrEmpty(azimuthValue) || String.IsNullOrEmpty(northValue) || String.IsNullOrEmpty(eastValue) || (!FileUploadControl.HasFile))
                {
                    this.planEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iPlan.Attributes["class"] = "icon fa fa-warning";
                    this.spnPlan.InnerText = " Warning!";
                    this.lblPlanSuccess.Text = "Missing Values. Please provide all values";
                    this.planEnclosure.Visible = true;
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wellID + " Added Well/Lateral Plan (Null Values) ");
                }
                else
                {
                    if (FileUploadControl.HasFile)
                    {
                        fileName = Path.GetFileName(FileUploadControl.PostedFile.FileName);
                        fileExtension = Path.GetExtension(FileUploadControl.PostedFile.FileName);
                        StreamReader readD = new StreamReader(FileUploadControl.FileContent);
                        do
                        {
                            String dLine = readD.ReadLine();
                            valuesRead = dLine.Split(',');
                            if (String.IsNullOrEmpty(valuesRead[0].Trim()))
                            {
                                continue;
                            }
                            else
                            {
                                fileD.Add(dLine);
                            }
                        }
                        while (readD.Peek() != -1);
                        readD.Close();
                    }
                    if (rdbGE.Checked) { datumID = 2; } else { datumID = 1; }
                    if (rdbGElev.Checked) { depthRefID = 3; } else if (rdbKB.Checked) { depthRefID = 1; } else { depthRefID = 2; }
                    if (rdbElFeet.Checked) { elevationRefUnitID = 4; } else { elevationRefUnitID = 5; }
                    if (Decimal.TryParse(elevationValue, out elevation)) { elevation = Convert.ToDecimal(elevationValue); } else { elevation = 0.00M; }
                    if (rdbVSFeet.Checked) { lengthUnitID = 4; } else { lengthUnitID = 5; }
                    if (Decimal.TryParse(azimuthValue, out azimuth)) { azimuth = Convert.ToDecimal(azimuthValue); } else { azimuth = 0.00M; }
                    if (rdbNFeet.Checked) { northUnitID = 4; } else { northUnitID = 5; }
                    if (Decimal.TryParse(northValue, out northing)) { northing = Convert.ToDecimal(northValue); } else { northing = 0.00M; }
                    if (rdbEFeet.Checked) { eastUnitID = 4; } else { eastUnitID = 5; }
                    if (Decimal.TryParse(eastValue, out easting)) { easting = Convert.ToDecimal(eastValue); } else { easting = 0.00M; }
                    iResult = WPN.processMetaInfo(optrID, ClientID, wellPadID, wellID, datumID, depthRefID, elevation, elevationRefUnitID, azimuth, lengthUnitID, northing, northUnitID, easting, eastUnitID, fileName, fileExtension, fileD, LoginName, domain, GetClientDBString);
                    System.Data.DataTable rwpTable = new System.Data.DataTable();
                    rwpTable = (System.Data.DataTable)iResult[0];
                    HttpContext.Current.Session["RawWPlanDataTable"] = rwpTable;
                    //HttpContext.Current.Session["FileID"] = Convert.ToInt32(iResult[1]);
                    this.divMetaInfo.Visible = false;
                    this.processWPData.Visible = true;
                    this.gvwRawWellPlan.DataSource = rwpTable;
                    this.gvwRawWellPlan.DataBind();
                    this.gvwRawWellPlan.Visible = true;
                    this.btnWPUpload.CssClass = "btn btn-default text-center";
                    this.btnWPUpload.Enabled = true;
                    this.btnWPUpload.Visible = false;
                    this.btnWPAddClear.CssClass = "btn btn-default text-center";
                    this.btnWPAddClear.Enabled = false;
                    this.btnWPAddClear.Visible = false;
                    this.btnWPProcess.CssClass = "btn btn-success text-center";
                    this.btnWPProcess.Enabled = true;
                    this.btnWPProcess.Visible = true;
                    this.btnWPProcessRest.CssClass = "btn btn-warning text-center";
                    this.btnWPProcessRest.Enabled = true;
                    this.btnWPProcessRest.Visible = true;
                    this.liFile.Attributes["class"] = "stepper-done";
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wellID + " Added Well/Lateral Plan (Calculated Values) ");
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWPAddClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtWPDRElevation.Text = "";
                this.txtWPDRElevation.Enabled = false;
                this.txtWPVSAzimuth.Text = "";
                this.txtWPVSAzimuth.Enabled = false;
                this.txtWPNorthing.Text = "";
                this.txtWPNorthing.Enabled = false;
                this.txtWPEasting.Text = "";
                this.txtWPEasting.Enabled = false;
                this.FileUploadControl.Dispose();
                this.FileUploadControl.Enabled = false;
                this.rdbGE.Checked = false;
                this.rdbMSL.Checked = false;
                this.divDR.Attributes["class"] = "form-control bg-gray-light";
                this.rdbGElev.Checked = false;
                this.rdbGElev.Enabled = false;
                this.rdbKB.Checked = false;
                this.rdbKB.Enabled = false;
                this.rdbRF.Checked = false;
                this.rdbRF.Enabled = false;
                this.divElevUnit.Attributes["class"] = "form-control bg-gray-light";
                this.rdbElFeet.Checked = false;
                this.rdbElFeet.Enabled = false;
                this.rdbElMeter.Checked = false;
                this.rdbElMeter.Enabled = false;
                this.divVSUnit.Attributes["class"] = "form-control bg-gray-light";
                this.rdbVSFeet.Checked = false;
                this.rdbVSFeet.Enabled = false;
                this.rdbVSMeter.Checked = false;
                this.rdbVSMeter.Enabled = false;
                this.divN.Attributes["class"] = "form-control bg-gray-light";
                this.rdbNFeet.Checked = false;
                this.rdbNFeet.Enabled = false;
                this.rdbNMeter.Checked = false;
                this.rdbNMeter.Enabled = false;
                this.divE.Attributes["class"] = "form-control bg-gray-light";
                this.rdbEFeet.Checked = false;
                this.rdbEFeet.Enabled = false;
                this.rdbEMeter.Checked = false;
                this.rdbEMeter.Enabled = false;
                this.resetWellPlanRadios(null, null);
                this.divMetaInfo.Visible = true;
                this.gvwRawWellPlan.DataSource = null;
                this.gvwRawWellPlan.DataBind();
                this.gvwRawWellPlan.Visible = false;
                this.gvwWPCalc.DataSource = null;
                this.gvwWPCalc.DataBind();
                this.gvwWPCalc.Visible = false;
                HttpContext.Current.Session["RawDataTable"] = null;
                HttpContext.Current.Session["FileID"] = null;
                this.planEnclosure.Visible = false;
                this.btnWPAddClear.CssClass = "btn btn-default text-center";
                this.btnWPAddClear.Enabled = false;
                this.btnWPUpload.CssClass = "btn btn-default text-center";
                this.btnWPUpload.Enabled = false;
                this.liFile.Attributes["class"] = "stepper-todo";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWPDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnWPInfoReset_Click(null, null);
                this.btnTIReset_Click(null, null);
                this.btnWPAddClear_Click(null, null);
                this.liMI.Attributes["class"] = "stepper-todo";
                this.liTI.Attributes["class"] = "stepper-todo";
                this.liFile.Attributes["class"] = "stepper-todo";
                this.liPlan.Attributes["class"] = "stepper-todo";
                this.lblWellName.Text = "";
                this.divWellNameDetail.Visible = false;
                this.divWellList.Visible = true;
                this.divWellPlan.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnEditStatus_Click(object sender, EventArgs e)
        {
            try
            {
                this.divWellAdd.Visible = false;
                this.divWellRF.Visible = false;
                this.divWellPlan.Visible = false;
                this.divWellPlacement.Visible = false;
                this.divWellStatus.Visible = true;
                this.charts.Visible = false;
                this.divWRegister.Visible = false;
                Button welBtn = (Button)sender;
                GridViewRow gRow = (GridViewRow)welBtn.NamingContainer;
                String wellname = null;
                wellname = Convert.ToString(Server.HtmlDecode(gRow.Cells[2].Text));
                Int32 rowIndex = Convert.ToInt32(gRow.RowIndex);
                Int32 optrID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue), wpdID = Convert.ToInt32(this.gvwWellPads.SelectedValue), welID = Convert.ToInt32(gvwWell.DataKeys[rowIndex]["welID"]);
                Dictionary<Int32, String> stList = DMG.getWellStatusListForClients();
                this.ddlWSWellStatus.Items.Clear();
                this.ddlWSWellStatus.DataSource = stList;
                this.ddlWSWellStatus.DataTextField = "Value";
                this.ddlWSWellStatus.DataValueField = "Key";
                this.ddlWSWellStatus.DataBind();
                this.ddlWSWellStatus.Items.Insert(0, new ListItem("--- Select Well Status ---", "-1"));
                this.ddlWSWellStatus.SelectedIndex = -1;
                this.ddlWSWellStatus.Focus();
                this.divDisplayButtons.Visible = false;
                this.ltrECHarts3DwithPlan.Text = "";
                this.divWellDetail.Visible = false;
                this.gvwWellDetail.DataSource = null;
                this.gvwWellDetail.DataBind();
                this.divgvwWellDetail.Visible = false;
                this.divWellRefMag.Visible = false;
                this.gvwRMList.DataSource = null;
                this.gvwRMList.DataBind();
                this.divgvwRMList.Visible = false;
                this.gvwiRMList.Visible = false;
                this.gvwiRMList.DataSource = null;
                this.gvwiRMList.DataBind();
                this.divlblWellPlanTI.Visible = false;
                this.gvwWellPlanInfo.DataSource = null;
                this.gvwWellPlanInfo.DataBind();
                this.divgvwWellPlanInfo.Visible = false;
                this.lblWellPlanCalc.Visible = false;
                this.gvwWellPlanCalc.DataSource = null;
                this.gvwWellPlanCalc.DataBind();
                this.gvwWellPlanCalc.Visible = false;
                this.lblWPCTieIn.Visible = false;
                this.gvwWPCTieIn.DataSource = null;
                this.gvwWPCTieIn.DataBind();
                this.gvwWPCTieIn.Visible = false;
                this.lblWellPlaceCalc.Visible = false;
                this.gvwWellPlaceCalc.DataSource = null;
                this.gvwWellPlaceCalc.DataBind();
                this.gvwWellPlaceCalc.Visible = false;
                this.divlblWellRegistrations.Visible = false;
                this.gvwWReg.DataSource = null;
                this.gvwWReg.DataBind();
                this.gvwWReg.Visible = false;
                this.btnWellReset.CssClass = "btn btn-warning text-center";
                this.btnWellReset.Enabled = true;
                this.charts.Visible = false;
                this.lblWellName.Text = " Selected Well: " + wellname;
                this.divWellNameDetail.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWSWellStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnWSTClear.CssClass = "btn btn-warning text-center";
                this.btnWSTClear.Enabled = true;
                this.btnWSTUpdate.CssClass = "btn btn-success text-center";
                this.btnWSTUpdate.Enabled = true;
                this.btnWSTUpdate.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRegisterWell_Click(object sender, EventArgs e)
        {
            try
            {
                this.divWellAdd.Visible = false;
                this.divWellRF.Visible = false;
                this.divWellPlan.Visible = false;
                this.divWellPlacement.Visible = false;
                this.divWellStatus.Visible = false;
                this.charts.Visible = false;
                this.divWRegister.Visible = true;
                Button welBtn = (Button)sender;
                GridViewRow gRow = (GridViewRow)welBtn.NamingContainer;
                Int32 rowIndex = Convert.ToInt32(gRow.RowIndex);
                Int32 optrID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue), wpdID = Convert.ToInt32(this.gvwWellPads.SelectedValue), welID = Convert.ToInt32(gvwWell.DataKeys[rowIndex]["welID"]);
                this.hdnWell.Value = welID.ToString();
                this.divDisplayButtons.Visible = false;
                this.charts.Visible = false;
                this.ltrECHarts3DwithPlan.Text = "";
                this.divWellDetail.Visible = false;
                this.gvwWellDetail.DataSource = null;
                this.gvwWellDetail.DataBind();
                this.divgvwWellDetail.Visible = false;
                this.divWellRefMag.Visible = false;
                this.gvwRMList.DataSource = null;
                this.gvwRMList.DataBind();
                this.divgvwRMList.Visible = false;
                this.gvwiRMList.Visible = false;
                this.gvwiRMList.DataSource = null;
                this.gvwiRMList.DataBind();
                this.divlblWellPlanTI.Visible = false;
                this.gvwWellPlanInfo.DataSource = null;
                this.gvwWellPlanInfo.DataBind();
                this.divgvwWellPlanInfo.Visible = false;
                this.lblWellPlanCalc.Visible = false;
                this.gvwWellPlanCalc.DataSource = null;
                this.gvwWellPlanCalc.DataBind();
                this.gvwWellPlanCalc.Visible = false;
                this.lblWPCTieIn.Visible = false;
                this.gvwWPCTieIn.DataSource = null;
                this.gvwWPCTieIn.DataBind();
                this.gvwWPCTieIn.Visible = false;
                this.lblWellPlaceCalc.Visible = false;
                this.gvwWellPlaceCalc.DataSource = null;
                this.gvwWellPlaceCalc.DataBind();
                this.gvwWellPlaceCalc.Visible = false;
                this.divlblWellRegistrations.Visible = false;
                this.gvwWReg.DataSource = null;
                this.gvwWReg.DataBind();
                this.gvwWReg.Visible = false;
                Dictionary<Int32, String> sgpList = SRV.getSrvGroupList();
                this.ddlWRSrvGroup.Items.Clear();
                this.ddlWRSrvGroup.DataSource = sgpList;
                this.ddlWRSrvGroup.DataTextField = "Value";
                this.ddlWRSrvGroup.DataValueField = "Key";
                this.ddlWRSrvGroup.DataBind();
                this.ddlWRSrvGroup.Items.Insert(0, new ListItem("--- Select SMARTs Service Group ---", "-1"));
                this.ddlWRSrvGroup.SelectedIndex = -1;                
                this.btnWellReset.CssClass = "btn btn-warning text-center";
                this.btnWellReset.Enabled = true;
                this.charts.Visible = false;
                this.lblWellName.Text = "";
                this.divWellNameDetail.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWRSrvGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 sgpID = -99, gpKeys = -99, gpID = -99;
                sgpID = Convert.ToInt32(this.ddlWRSrvGroup.SelectedValue);
                Dictionary<Int32, String> srvList = SRV.getServiceList(sgpID);
                gpKeys = srvList.Keys.Count;
                if (gpKeys >= 1)
                {
                    foreach (KeyValuePair<Int32, String> item in srvList)
                    {
                        gpID = item.Key;
                    }

                    if (gpID.Equals(-99))
                    {
                        this.ddlWRService.BorderColor = CLR.Maroon;
                        this.ddlWRService.Items.Clear();
                        this.ddlWRService.DataBind();
                        this.ddlWRService.Items.Insert(0, new ListItem("--- Select SMARTs Service ---", "-1"));
                        this.ddlWRService.SelectedIndex = -1;
                        this.ddlWRService.Enabled = false;
                        this.ddlWRService.ToolTip = "Unable to load Data";
                    }
                    else
                    {
                        this.ddlWRService.BorderColor = CLR.Green;
                        this.ddlWRService.Items.Clear();
                        this.ddlWRService.DataSource = srvList;
                        this.ddlWRService.DataTextField = "Value";
                        this.ddlWRService.DataValueField = "Key";
                        this.ddlWRService.DataBind();
                        this.ddlWRService.Items.Insert(0, new ListItem("--- Select SMARTs Service ---", "-1"));
                        this.ddlWRService.SelectedIndex = -1;
                        this.ddlWRService.Enabled = true;
                        this.ddlWRService.Focus();
                    }
                }
                else
                {
                    this.ddlWRService.BorderColor = CLR.Maroon;
                    this.ddlWRService.Items.Clear();
                    this.ddlWRService.DataBind();
                    this.ddlWRService.Items.Insert(0, new ListItem("--- Select SMARTs Service ---", "-1"));
                    this.ddlWRService.SelectedIndex = -1;
                    this.ddlWRService.Enabled = false;
                    this.ddlWRService.ToolTip = "Unable to load Service Groups";
                }
                this.btnAddWRegClear.Enabled = true;
                this.btnAddWRegClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlWRService_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> ctList = CTRT.getClientContractListForRegistration(ClientID);
                this.ddlClientContract.Items.Clear();
                this.ddlClientContract.DataSource = ctList;
                this.ddlClientContract.DataTextField = "Value";
                this.ddlClientContract.DataValueField = "Key";
                this.ddlClientContract.DataBind();
                this.ddlClientContract.Items.Insert(0, new ListItem("--- Select Contract ---", "-1"));
                this.ddlClientContract.SelectedIndex = -1;
                this.ddlClientContract.Enabled = true;
                this.btnAddWReg.Enabled = false;
                this.btnAddWReg.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlClientContract_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ContractID = Convert.ToInt32(this.ddlClientContract.SelectedValue);
                Int32 ctStat = CTRT.getClientContractStatusForRegistration(ContractID);
                if (!ctStat.Equals(1))
                {
                    this.btnAddWReg.Enabled = false;
                    this.btnAddWReg.CssClass = "btn btn-default text-center";
                    this.enReg.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iReg.Attributes["class"] = "icon fa fa-warning";
                    this.spnReg.InnerText = " Warning!";
                    this.lblSrvRegSuccess.Text = "!!! Error !!! Contract Expired. Unable to continue";
                    this.enReg.Visible = true;
                }
                else
                {
                    this.btnAddWReg.Enabled = true;
                    this.btnAddWReg.CssClass = "btn btn-success text-center";
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddWReg_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, oprID = -99, wpdID = -99, wellID = -99, srvGrpID = -99, srvID = -99;
                String srvName = String.Empty;
                oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                wpdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                wellID = Convert.ToInt32(this.hdnWell.Value);
                srvGrpID = Convert.ToInt32(this.ddlWRSrvGroup.SelectedValue);
                srvID = Convert.ToInt32(this.ddlWRService.SelectedValue);
                srvName = Convert.ToString(this.ddlWRService.SelectedItem);
                iResult = AST.insertWellServiceRegistration(oprID, ClientID, wpdID, wellID, srvGrpID, srvID, LoginName, domain, GetClientDBString);
                if (iResult.Equals(1))
                {
                    this.btnAddWRegClear_Click(null, null);
                    this.enReg.Attributes["class"] = "alert alert-success alert-dismissable";
                    this.iReg.Attributes["class"] = "icon fa fa-check-circle";
                    this.spnReg.InnerText = " Success!";
                    this.lblSrvRegSuccess.Text = "Successfully Registered Well/Lateral ";
                    this.enReg.Visible = true;
                    this.wpdEnclosure.Visible = true;
                    logger.Info(" User: " + LoginName + " Realm: " + domain + " Well Pad Name: " + wpdID + " Well Name: " + wellID + " for SMARTs Service: " + srvName + ". Service Registration");
                    this.btnAddWReg.Enabled = false;
                    this.btnAddWReg.CssClass = "btn btn-default text-center";
                }
                else if (iResult.Equals(-1))
                {
                    this.btnAddWRegClear_Click(null, null);
                    this.enReg.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iReg.Attributes["class"] = "icon fa fa-warning";
                    this.spnReg.InnerText = " Warning!";
                    this.lblSrvRegSuccess.Text = "!!! Error !!! Duplicate Regisration Request";
                    this.enReg.Visible = true;
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Pad Name: " + wpdID + " Well Name: " + wellID + " for SMARTs Service: " + srvName + ". Service Registration");
                    this.btnAddWRegClear.Enabled = true;
                    this.btnAddWRegClear.CssClass = "btn btn-warning text-center";
                }
                else
                {
                    this.btnAddWRegClear_Click(null, null);
                    this.enReg.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iReg.Attributes["class"] = "icon fa fa-warning";
                    this.spnReg.InnerText = " Warning!";
                    this.lblSrvRegSuccess.Text = "!!! Error !!! Unable to register Well/Lateral for SMARTs Service";
                    this.enReg.Visible = true;
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Pad Name: " + wpdID + " Well Name: " + wellID + " for SMARTs Service: " + srvName + ". Service Registration");
                    this.btnAddWRegClear.Enabled = true;
                    this.btnAddWRegClear.CssClass = "btn btn-warning text-center";
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddWRegClear_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> sgpList = SRV.getSrvGroupList();
                this.ddlWRSrvGroup.Items.Clear();
                this.ddlWRSrvGroup.DataTextField = "Value";
                this.ddlWRSrvGroup.DataValueField = "Key";
                this.ddlWRSrvGroup.DataBind();
                this.ddlWRSrvGroup.Items.Insert(0, new ListItem("--- Select SMARTs Service Group ---", "-1"));
                this.ddlWRSrvGroup.SelectedIndex = -1;
                this.ddlWRService.Items.Clear();
                this.ddlWRService.DataBind();
                this.ddlWRService.Items.Insert(0, new ListItem("--- Select SMARTs Service ---", "-1"));
                this.ddlWRService.SelectedIndex = -1;
                this.ddlWRService.Enabled = false;
                this.ddlClientContract.Items.Clear();
                this.ddlClientContract.DataBind();
                this.ddlClientContract.Items.Insert(0, new ListItem("--- Select Contract ---", "-1"));
                this.ddlClientContract.SelectedIndex = -1;
                this.ddlClientContract.Enabled = false;
                this.enReg.Visible = false;
                this.lblSrvRegSuccess.Text = "";
                this.btnAddWReg.Enabled = false;
                this.btnAddWReg.CssClass = "btn btn-default text-center";
                this.btnAddWRegClear.Enabled = false;
                this.btnAddWRegClear.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddWRegDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnAddWRegClear_Click(null, null);
                this.divWellList.Visible = true;
                this.divWRegister.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWellNew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 resultSilo = -99, resultDemog = -99, welOptrID = -99, welPad = -99, latC = -99, lngC = -99, mmdID = -99, cordID = -99, projID = -99;
                String welOptrName = String.Empty, wName = String.Empty, wLwrName = String.Empty, wUWI = String.Empty, wAPI = String.Empty, wPrm = String.Empty, lati = String.Empty, longi = String.Empty;
                Decimal wLat = -99.000000000M, wLon = -99.000000000M;                
                DateTime wDate = DateTime.Now;                

                welOptrID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                welPad = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                GridViewRow oRow = this.gvwGlobalOptr.SelectedRow;
                welOptrName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                wName = Convert.ToString(this.txtWellAddName.Text);
                wUWI = Convert.ToString(this.txtWellAddUWI.Text);
                wAPI = Convert.ToString(this.txtAddWellAPI.Text);
                wDate = Convert.ToDateTime(this.clndrWellSpudDate.SelectedDate);
                lati = Convert.ToString(this.txtWellAddLat.Text);
                longi = Convert.ToString(this.txtWellAddLon.Text);
                if (this.rdbWEast.Checked)
                {
                    latC = 9;
                }
                else
                {
                    latC = 25;
                }
                if (this.rdbWNorth.Checked)
                {
                    lngC = 1;
                }
                else
                {
                    lngC = 17;
                }
                if (this.rdbBGGM.Checked)
                {
                    mmdID = 1;
                }
                else if (this.rdbHDGM.Checked)
                {
                    mmdID = 2;
                }
                else if (this.rdbIFR.Checked)
                {
                    mmdID = 3;
                }
                else if (this.rdbIGRF.Checked)
                {
                    mmdID = 4;
                }
                else
                {
                    mmdID = 5;
                }
                cordID = Convert.ToInt32(this.ddlAddJobCord.SelectedValue);
                projID = Convert.ToInt32(this.ddlAddJobProj.SelectedValue);
                wPrm = Convert.ToString(this.txtWellAddPermit.Text);
                if (String.IsNullOrEmpty(welOptrName) || String.IsNullOrEmpty(wName) || String.IsNullOrEmpty(lati) || String.IsNullOrEmpty(longi) || String.IsNullOrEmpty(wPrm))
                {
                    this.wellEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iWellMessage.Attributes["class"] = "icon fa fa-warning";
                    this.spnWellHeader.InnerText = " Warning!";
                    this.lblWellSuccess.Text = "Null values found. Please provide all values";
                    this.wellEnclosure.Visible = true;
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wName + " Error Adding Well/Lateral ");
                }
                else
                {
                    TextInfo tiWNme = new CultureInfo("en-US", false).TextInfo;
                    wLwrName = tiWNme.ToLower(wName);
                    wLwrName = wLwrName.Replace(" ", "");
                    if (!Decimal.TryParse(lati, out wLat) || !Decimal.TryParse(longi, out wLon))
                    {
                        this.wellEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iWellMessage.Attributes["class"] = "icon fa fa-warning";
                        this.spnWellHeader.InnerText = " Warning!";
                        this.lblWellSuccess.Text = "Error insertng the Well. (Please check values).";
                        this.wellEnclosure.Visible = true;
                        logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wName + " Error Adding Well/Lateral (Latitude)");
                    }
                    else
                    {
                        wLat = Convert.ToDecimal(lati);
                        wLon = Convert.ToDecimal(longi);
                        //if ((wLat.Equals(-99.00M)) || (wLat < -90.00M) || (wLat > 90.00M))
                        if ((wLat < -90.00M) || (wLat > 90.00M))
                        {
                            this.wellEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                            this.iWellMessage.Attributes["class"] = "icon fa fa-warning";
                            this.spnWellHeader.InnerText = " Warning!";
                            this.lblWellSuccess.Text = "Error insertng the Well. (Latitude Values should be between -90.00 (min) and 90.00 (max)).";
                            this.wellEnclosure.Visible = true;
                            logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wName + " Error Adding Well/Lateral (Latitude)");
                        }
                        //else if ((wLon.Equals(-99.00M) || (wLon < -180.00M) || (wLon > 180.00M)))
                        else if (((wLon < -180.00M) || (wLon > 180.00M)))
                        {
                            this.wellEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                            this.iWellMessage.Attributes["class"] = "icon fa fa-warning";
                            this.spnWellHeader.InnerText = " Warning!";
                            this.lblWellSuccess.Text = "Error insertng the Well. (Longitude Values should be between -180.00 (min) and 180.00 (max)).";
                            logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wName + " Error Adding Well/Lateral (Longitude)");
                            this.wellEnclosure.Visible = true;
                        }
                        else
                        {
                            //Adding Well to Silo
                            resultSilo = AST.addWellSilo(welOptrID, welOptrName, wName, wLon, lngC, wLat, latC, welPad, wAPI, wUWI, wPrm, wDate, mmdID, cordID, projID, siloWSt, domain, GetClientDBString, LoginName, domain);
                            //Adding Well to DemogDB
                            resultDemog = AST.addWellDemog(welOptrID, welOptrName, wName, wLon, lngC, wLat, latC, welPad, wAPI, wUWI, wPrm, wDate, mmdID, cordID, projID, globeWSt, domain, LoginName, domain);

                            if (resultSilo.Equals(1) && resultDemog.Equals(1))
                            {
                                this.wellEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                                this.iWellMessage.Attributes["class"] = "icon fa fa-check-circle";
                                this.spnWellHeader.InnerText = " Success!";
                                this.lblWellSuccess.Text = "Successfully added Well, " + wName + ", to SMARTs";
                                this.wellEnclosure.Visible = true;
                                this.btnWellAddDone.Focus();
                                this.createAssetCharts(null, null);
                                this.btnWellNew.Enabled = false;
                                this.btnWellNew.CssClass = "btn btn-default text-center";
                                logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wName + " Added Well/Lateral ");
                            }
                            else
                            {
                                this.wellEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                                this.iWellMessage.Attributes["class"] = "icon fa fa-warning";
                                this.spnWellHeader.InnerText = " Warning!";
                                this.lblWellSuccess.Text = "There was a problem inserting the Well. Make sure you are providing all values. Please try again.";
                                this.wellEnclosure.Visible = true;
                                logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wName + " Added Well/Lateral (Null Values)");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddWellCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtWellAddName.Text = "";
                this.txtWellAddUWI.Text = "";
                this.txtAddWellAPI.Text = "";
                this.clndrWellSpudDate.SelectedDate = DateTime.Now;
                this.txtWellSpudDate.Text = "";
                this.txtWellAddPermit.Text = "";
                this.txtWellAddLat.Text = "";
                this.txtWellAddLat.Enabled = false;
                this.txtWellAddLon.Text = "";
                this.txtWellAddLon.Enabled = false;
                this.rdbBGGM.Checked = false;
                this.rdbHDGM.Checked = false;
                this.rdbIFR.Checked = false;
                this.rdbIIFR.Checked = false;
                this.rdbIGRF.Checked = false;
                this.ddlAddJobCord.Items.Clear();
                this.ddlAddJobCord.DataBind();
                this.ddlAddJobCord.Items.Insert(0, new ListItem("--- Select Coordinate System ---", "-1"));
                this.ddlAddJobCord.SelectedIndex = -1;
                this.ddlAddJobCord.Enabled = false;
                this.ddlAddJobProj.Items.Clear();
                this.ddlAddJobProj.DataBind();
                this.ddlAddJobProj.Items.Insert(0, new ListItem("--- Select Projection ---", "-1"));
                this.ddlAddJobProj.SelectedIndex = -1;
                this.ddlAddJobProj.Enabled = false;
                this.divLatO.Attributes["class"] = "form-control bg-gray-light";
                this.rdbWNorth.Checked = false;
                this.rdbWNorth.Enabled = false;
                this.rdbWSouth.Checked = false;
                this.rdbWSouth.Enabled = false;
                this.divLngO.Attributes["class"] = "form-control bg-gray-light";
                this.rdbWEast.Checked = false;
                this.rdbWEast.Enabled = false;
                this.rdbWWest.Checked = false;
                this.rdbWWest.Enabled = false;
                this.wellEnclosure.Visible = false;
                this.lblWellSuccess.Text = "";
                this.btnWellNew.Enabled = false;
                this.btnWellNew.CssClass = "btn btn-default text-center";
                this.btnAddWellCancel.Enabled = false;
                this.btnAddWellCancel.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWellAddDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnAddWellCancel_Click(null, null);
                this.divWellList.Visible = true;
                this.divWellAdd.Visible = false;
                this.divWellRF.Visible = false;
                Int32 pdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                System.Data.DataTable pdTable = AST.getWellForWellPadTable(pdID, GetClientDBString);
                this.gvwWell.DataSource = pdTable;
                this.gvwWell.PageIndex = 0;
                this.gvwWell.SelectedIndex = -1;
                this.gvwWell.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSectionList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 wlID = Convert.ToInt32(this.gvwWell.SelectedValue);
                System.Data.DataTable wsTable = OPR.getWellSectionToWellTable(wlID, GetClientDBString);
                this.gvwSectionList.DataSource = wsTable;
                this.gvwSectionList.PageIndex = e.NewPageIndex;
                this.gvwSectionList.SelectedIndex = -1;
                this.gvwSectionList.DataBind();
                this.btnSectionReset.Enabled = true;
                this.btnSectionReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSectionList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 opID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue), wpdID = Convert.ToInt32(this.gvwWellPads.SelectedValue), wlID = Convert.ToInt32(this.gvwWell.SelectedValue), scID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                System.Data.DataTable rnTable = OPR.getRunForWellSectionTable(opID, wpdID, wlID, scID, GetClientDBString);
                this.gvwRunList.DataSource = rnTable;
                this.gvwRunList.PageIndex = 0;                
                this.gvwRunList.SelectedIndex = -1;
                this.gvwRunList.DataBind();
                this.gvwRunList.Visible = true;
                GridViewRow oRow = this.gvwGlobalOptr.SelectedRow;
                String oName = Convert.ToString(Server.HtmlDecode(oRow.Cells[2].Text));
                GridViewRow pRow = this.gvwWellPads.SelectedRow;
                String pName = Convert.ToString(Server.HtmlDecode(pRow.Cells[2].Text));
                GridViewRow wRow = this.gvwWell.SelectedRow;
                String wName = Convert.ToString(Server.HtmlDecode(wRow.Cells[2].Text));
                GridViewRow dRow = this.gvwSectionList.SelectedRow;
                String scName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                this.lblRunList.Text = "Operator Company: <strong><u>" + oName + "</u></strong> Well Pad: <strong><u>" + pName + "</u></strong> Well/Lateral: <strong><u>" + wName + "</u></strong> Section: <strong><u>" + scName + "</u></strong> Run(s) List";
                this.divSection.Visible = false;
                this.divSectionAdd.Visible = false;
                this.divRunList.Visible = true;
                this.li1.Attributes["class"] = "stepper-done";
                this.li2.Attributes["class"] = "stepper-done";
                this.li3.Attributes["class"] = "stepper-done";
                this.li4.Attributes["class"] = "stepper-done";
                this.li5.Attributes["class"] = "stepper-done";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRunList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 optrID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue), wpdID = Convert.ToInt32(this.gvwWellPads.SelectedValue), wellID = Convert.ToInt32(this.gvwWell.SelectedValue), sectionID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                System.Data.DataTable rnTable = OPR.getRunForWellSectionTable(optrID, wpdID, wellID, sectionID, GetClientDBString);
                this.gvwRunList.DataSource = rnTable;
                this.gvwRunList.PageIndex = e.NewPageIndex;
                this.gvwRunList.SelectedIndex = -1;
                this.gvwRunList.DataBind();
                this.btnRunReset.Enabled = true;
                this.btnRunReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        protected void btnExit_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnWellReset_Click(null,null);
                this.btnRunReset_Click(null, null);
                System.Data.DataTable operatorCoTable = CLNT.getGlobalOperatorCoWellTable(ClientType);
                this.gvwGlobalOptr.DataSource = operatorCoTable;
                this.gvwGlobalOptr.PageIndex = 0;
                this.gvwGlobalOptr.SelectedIndex = -1;
                this.gvwGlobalOptr.DataBind();
                this.btnOprReset.Enabled = false;
                this.btnOprReset.CssClass = "btn btn-default text-center";
                this.divOperators.Visible = true;
                this.divOperatorPads.Visible = false;
                this.divNewPad.Visible = false;
                this.divWellList.Visible = false;
                this.divWellAdd.Visible = false;
                this.divWellPlan.Visible = false;
                this.divWellStatus.Visible = false;
                this.divWRegister.Visible = false;
                this.divSection.Visible = false;
                this.divSectionAdd.Visible = false;
                this.divRunList.Visible = false;
                this.divRunAdd.Visible = false;
                this.lblOperatorList.Text = "Operator Companies List";
                this.lblPadList.Text = "Well Pad(s) List";
                this.lblWellHeading.Text = "Well(s) / Lateral(s) List";
                this.lblWellAddHeading.Text = "Add New Well(s) / Lateral(s)";
                this.lblSectionList.Text = "Well/Lateral Section List";
                this.lblRunList.Text = "Run(s) List";
                this.li1.Attributes["class"] = "stepper-done";
                this.li2.Attributes["class"] = "stepper-todo";
                this.li3.Attributes["class"] = "stepper-todo";
                this.li4.Attributes["class"] = "stepper-todo";
                this.li5.Attributes["class"] = "stepper-todo";
                this.lblWellName.Text = "";
                this.divWellNameDetail.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSectionAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnSectionReset_Click(null, null);
                this.divSection.Visible = false;
                this.divSectionAdd.Visible = true;
                Int32 wellID = Convert.ToInt32(this.gvwWell.SelectedValue);
                Dictionary<Int32, String> scList = OPR.getWellSectionsList(wellID, GetClientDBString);
                this.ddlNWSWSection.Items.Clear();
                this.ddlNWSWSection.DataSource = scList;
                this.ddlNWSWSection.DataTextField = "Value";
                this.ddlNWSWSection.DataValueField = "Key";
                this.ddlNWSWSection.DataBind();
                this.ddlNWSWSection.Items.Insert(0, new ListItem("--- Select Well/Lateral Section ---", "-1"));
                this.ddlNWSWSection.SelectedIndex = -1;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSectionReset_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 wellID = Convert.ToInt32(this.gvwWell.SelectedValue);
                Int32 sectionID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                System.Data.DataTable secTable = OPR.getWellSectionTable(wellID, sectionID, GetClientDBString);
                this.gvwSectionList.DataSource = secTable;
                this.gvwSectionList.PageIndex = 0;
                this.gvwSectionList.SelectedIndex = -1;
                this.gvwSectionList.DataBind();
                this.btnSectionReset.Enabled = false;
                this.btnSectionReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSectionStepBack_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnSectionReset_Click(null, null);
                this.divWellList.Visible = true;
                this.divSection.Visible = false;
                this.li1.Attributes["class"] = "stepper-done";
                this.li2.Attributes["class"] = "stepper-done";
                this.li3.Attributes["class"] = "stepper-done";
                this.li4.Attributes["class"] = "stepper-todo";
                this.li5.Attributes["class"] = "stepper-todo";
                this.lblWellName.Text = "";
                this.divWellNameDetail.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRunAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnRunReset_Click(null, null);
                Dictionary<Int32, String> rnList = AST.getRunNamesList();
                this.ddlActRun.Items.Clear();
                this.ddlActRun.DataSource = rnList;
                this.ddlActRun.DataTextField = "Value";
                this.ddlActRun.DataValueField = "Key";
                this.ddlActRun.DataBind();
                this.ddlActRun.Items.Insert(0, new ListItem("--- Select Run ---", "-1"));
                this.ddlActRun.SelectedIndex = -1;
                this.divRunList.Visible = false;
                this.divRunAdd.Visible = true;
                this.runEnclosure.Visible = false;
                this.lblActRunSuccess.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRunReset_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 optrID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue), wpdID = Convert.ToInt32(this.gvwWellPads.SelectedValue), wellID = Convert.ToInt32(this.gvwWell.SelectedValue), sectionID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                System.Data.DataTable rnTable = OPR.getRunForWellSectionTable(optrID, wpdID, wellID, sectionID, GetClientDBString);
                this.gvwRunList.DataSource = rnTable;
                this.gvwRunList.PageIndex = 0;
                this.gvwRunList.SelectedIndex = -1;
                this.gvwRunList.DataBind();
                this.divlblRTIDetail.Visible = false;
                this.gvwRTI.DataSource = null;
                this.gvwRTI.DataBind();
                this.divgvwRTI.Visible = false;
                this.lblBHADetail.Visible = false;
                this.gvwBHA.DataSource = null;
                this.gvwBHA.DataBind();
                this.gvwBHA.Visible = false;
                this.hdnRunID.Value = String.Empty;
                this.btnRunReset.Enabled = false;
                this.btnRunReset.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRunStepBack_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnRunReset_Click(null, null);
                this.divSection.Visible = true;
                this.hdnRunID.Value = String.Empty;
                this.divRunList.Visible = false;
                this.li1.Attributes["class"] = "stepper-done";
                this.li2.Attributes["class"] = "stepper-done";
                this.li3.Attributes["class"] = "stepper-done";
                this.li4.Attributes["class"] = "stepper-done";
                this.li5.Attributes["class"] = "stepper-todo";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlActRun_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 sqcKeys = -99, sqckID = -99;
                Dictionary<Int32, String> sqcList = AST.getSQCList(GetClientDBString);
                sqcKeys = sqcList.Keys.Count;
                if (sqcKeys >= (1))
                {
                    foreach (KeyValuePair<Int32, String> item in sqcList)
                    {
                        sqckID = item.Key;
                    }

                    if (sqckID.Equals(-99))
                    {
                        this.ddlActRunSQC.BorderColor = CLR.Maroon;
                        this.ddlActRunSQC.Items.Clear();
                        this.ddlActRunSQC.DataBind();
                        this.ddlActRunSQC.Items.Insert(0, new ListItem("--- Select Survey Qualification Criteria ---", "-1"));
                        this.ddlActRunSQC.SelectedIndex = -1;
                        this.ddlActRunSQC.Enabled = false;
                        this.ddlActRunSQC.ToolTip = "Unable to load Survey Qualification Criteria";
                    }
                    else
                    {
                        this.ddlActRunSQC.BorderColor = CLR.Green;
                        this.ddlActRunSQC.Items.Clear();
                        this.ddlActRunSQC.DataSource = sqcList;
                        this.ddlActRunSQC.DataTextField = "Value";
                        this.ddlActRunSQC.DataValueField = "Key";
                        this.ddlActRunSQC.DataBind();
                        this.ddlActRunSQC.Items.Insert(0, new ListItem("--- Select Survey Qualification Criteria ---", "-1"));
                        this.ddlActRunSQC.SelectedIndex = -1;
                        this.ddlActRunSQC.Enabled = true;
                        this.ddlActRunSQC.Focus();
                    }
                }
                else
                {
                    this.ddlActRunSQC.BorderColor = CLR.Maroon;
                    this.ddlActRunSQC.Items.Clear();
                    this.ddlActRunSQC.DataBind();
                    this.ddlActRunSQC.Items.Insert(0, new ListItem("--- Select Survey Qualification Criteria ---", "-1"));
                    this.ddlActRunSQC.SelectedIndex = -1;
                    this.ddlActRunSQC.Enabled = false;
                    this.ddlActRunSQC.ToolTip = "Unable to load Data";
                }
                this.txtActRunAzm.Text = "";
                this.txtActRunAzm.Enabled = false;
                this.txtActRunInc.Text = "";
                this.txtActRunInc.Enabled = false;
                this.txtActRunTAzm.Text = "";
                this.txtActRunTAzm.Enabled = false;
                this.txtActRunTInc.Text = "";
                this.txtActRunTInc.Enabled = false;
                this.ddlRunDepthUnit.Items.Clear();
                this.ddlRunDepthUnit.DataBind();
                this.ddlRunDepthUnit.Items.Insert(0, new ListItem("--- Select Depth Unit ---", "-1"));
                this.ddlRunDepthUnit.SelectedIndex = -1;
                this.ddlRunDepthUnit.Enabled = false;
                this.txtRunStartD.Text = "";
                this.txtRunStartD.Enabled = false;
                this.txtRunEndD.Text = "";
                this.txtRunEndD.Enabled = false;
                this.ddlActRunToolCode.Items.Clear();
                this.ddlActRunToolCode.DataBind();
                this.ddlActRunToolCode.Items.Insert(0, new ListItem("--- Select Toolcode ---", "-1"));
                this.ddlActRunToolCode.SelectedIndex = -1;
                this.ddlActRunToolCode.Enabled = false;
                this.ddlActRunStatus.Items.Clear();
                this.ddlActRunStatus.DataBind();
                this.ddlActRunStatus.Items.Insert(0, new ListItem("--- Select Run Status ---", "-1"));
                this.ddlActRunStatus.SelectedIndex = -1;
                this.ddlActRunStatus.Enabled = false;
                this.lblActRunSuccess.Text = "";
                this.lblActRunSuccess.Visible = false;
                this.btnActRunAdd.Enabled = false;
                this.btnActRunCancel.Enabled = true;
                this.btnActRunCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlActRunSQC_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 lnKeys = 0, lnkID = 0;
                Dictionary<Int32, String> lnList = AST.getLengthUnits();
                lnKeys = lnList.Keys.Count;
                if (lnKeys >= (1))
                {
                    foreach (KeyValuePair<Int32, String> item in lnList)
                    {
                        lnkID = item.Key;
                    }

                    if (lnkID.Equals(-99))
                    {
                        this.ddlRunDepthUnit.BorderColor = CLR.Maroon;
                        this.ddlRunDepthUnit.Items.Clear();
                        this.ddlRunDepthUnit.DataBind();
                        this.ddlRunDepthUnit.Items.Insert(0, new ListItem("--- Select Depth Unit ---", "-1"));
                        this.ddlRunDepthUnit.SelectedIndex = -1;
                        this.ddlRunDepthUnit.ToolTip = "Unable to load Depth Unit's list";
                    }
                    else
                    {
                        this.ddlRunDepthUnit.BorderColor = CLR.Green;
                        this.ddlRunDepthUnit.Items.Clear();
                        this.ddlRunDepthUnit.DataSource = lnList;
                        this.ddlRunDepthUnit.DataTextField = "Value";
                        this.ddlRunDepthUnit.DataValueField = "Key";
                        this.ddlRunDepthUnit.DataBind();
                        this.ddlRunDepthUnit.Items.Insert(0, new ListItem("--- Select Depth Unit ---", "-1"));
                        this.ddlRunDepthUnit.SelectedIndex = -1;
                        this.ddlRunDepthUnit.Enabled = true;
                        this.txtActRunInc.Text = "";
                        this.txtActRunInc.Enabled = true;
                        this.txtActRunAzm.Text = "";
                        this.txtActRunAzm.Enabled = true;
                        this.txtActRunTInc.Text = "";
                        this.txtActRunTInc.Enabled = true;
                        this.txtActRunTAzm.Text = "";
                        this.txtActRunTAzm.Enabled = true;
                        this.txtRunStartD.Text = "";
                        this.txtRunStartD.Enabled = false;
                        this.txtRunEndD.Text = "";
                        this.txtRunEndD.Enabled = false;
                        this.txtActRunInc.Focus();
                    }
                }
                else
                {
                    this.ddlRunDepthUnit.BorderColor = CLR.Maroon;
                    this.ddlRunDepthUnit.Items.Clear();
                    this.ddlRunDepthUnit.DataBind();
                    this.ddlRunDepthUnit.Items.Insert(0, new ListItem("--- Select Depth Unit ---", "-1"));
                    this.ddlRunDepthUnit.SelectedIndex = -1;
                    this.ddlRunDepthUnit.ToolTip = "Unable to load Data";
                }                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlRunDepthUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 tcKeys = 0, tckID = 0;
                Dictionary<Int32, String> tcList = DMG.getToolCodesList();
                tcKeys = tcList.Keys.Count;
                if (tcKeys >= (1))
                {
                    foreach (KeyValuePair<Int32, String> item in tcList)
                    {
                        tckID = item.Key;
                    }

                    if (tckID.Equals(-99))
                    {
                        this.ddlActRunToolCode.BorderColor = CLR.Maroon;
                        this.ddlActRunToolCode.Items.Clear();
                        this.ddlActRunToolCode.DataBind();
                        this.ddlActRunToolCode.Items.Insert(0, new ListItem("--- Select Toolcode ---", "-1"));
                        this.ddlActRunToolCode.SelectedIndex = -1;
                        this.ddlActRunToolCode.ToolTip = "Unable to load Toolcode list";
                    }
                    else
                    {
                        this.ddlActRunToolCode.BorderColor = CLR.Green;
                        this.ddlActRunToolCode.Items.Clear();
                        this.ddlActRunToolCode.DataSource = tcList;
                        this.ddlActRunToolCode.DataTextField = "Value";
                        this.ddlActRunToolCode.DataValueField = "Key";
                        this.ddlActRunToolCode.DataBind();
                        this.ddlActRunToolCode.Items.Insert(0, new ListItem("--- Select Toolcode ---", "-1"));
                        this.ddlActRunToolCode.SelectedIndex = -1;
                        this.ddlActRunToolCode.Enabled = true;
                        this.txtRunStartD.Text = "";
                        this.txtRunStartD.Enabled = true;
                        this.txtRunEndD.Text = "";
                        this.txtRunEndD.Enabled = true;
                        this.txtRunStartD.Focus();
                    }
                }
                else
                {
                    this.ddlActRunToolCode.BorderColor = CLR.Maroon;
                    this.ddlActRunToolCode.Items.Clear();
                    this.ddlActRunToolCode.DataBind();
                    this.ddlActRunToolCode.Items.Insert(0, new ListItem("--- Select Toolcode ---", "-1"));
                    this.ddlActRunToolCode.SelectedIndex = -1;
                    this.ddlActRunToolCode.ToolTip = "Unable to load Data";
                }
                this.lblActRunSuccess.Text = "";
                this.lblActRunSuccess.Visible = false;
                this.btnActRunAdd.Enabled = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlActRunToolCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> rsList = AST.getRunStatusList();
                this.lblActRunSuccess.Text = "";
                this.lblActRunSuccess.Visible = true;
                this.btnActRunAdd.Enabled = true;
                this.txtActRunInc.Focus();
                this.ddlActRunStatus.Items.Clear();
                this.ddlActRunStatus.DataSource = rsList;
                this.ddlActRunStatus.DataTextField = "Value";
                this.ddlActRunStatus.DataValueField = "Key";
                this.ddlActRunStatus.DataBind();
                this.ddlActRunStatus.Items.Insert(0, new ListItem("--- Select Run Status ---", "-1"));
                this.ddlActRunStatus.SelectedIndex = -1;
                this.ddlActRunStatus.Enabled = true;
                this.ddlActRunStatus.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlActRunStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {                
                this.btnActRunAdd.CssClass = "btn btn-success text-center";
                this.btnActRunAdd.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnActRunAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 runRes = -99, operatorID = -99, wellpadID = -99, wellID = -99, wellSectionID = -99, rnID = -99, tcID = -99, stID = -99, sqcID = -99, lengthUnit = -99;
                String wlName = String.Empty, rnName = String.Empty, wsName = String.Empty, rI = String.Empty, rA = String.Empty, rTI = String.Empty, rTA = String.Empty, sDepth = String.Empty, eDepth = String.Empty;
                Decimal rnInc = -99.00M, rnAzm = -99.00M, rnTInc = -99.00M, rnTAzm = -99.00M, startDepth = -99.00M, endDepth = -99.00M;
                operatorID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                wellpadID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                wellID = Convert.ToInt32(this.gvwWell.SelectedValue);
                wellSectionID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                rnID = Convert.ToInt32(this.ddlActRun.SelectedValue);
                rnName = Convert.ToString(this.ddlActRun.SelectedItem);
                tcID = Convert.ToInt32(this.ddlActRunToolCode.SelectedValue);
                sqcID = Convert.ToInt32(this.ddlActRunSQC.SelectedValue);
                lengthUnit = Convert.ToInt32(this.ddlRunDepthUnit.SelectedValue);
                rI = this.txtActRunInc.Text;
                rA = this.txtActRunAzm.Text;
                rTI = this.txtActRunTInc.Text;
                rTA = this.txtActRunTAzm.Text;
                sDepth = this.txtRunStartD.Text;
                eDepth = this.txtRunEndD.Text;
                stID = Convert.ToInt32(this.ddlActRunStatus.SelectedValue);
                wlName = AST.getWellName(wellID, GetClientDBString);
                wsName = OPR.getWellSectionNameFromID(wellSectionID, GetClientDBString);
                if (String.IsNullOrEmpty(rI) || String.IsNullOrEmpty(rA) || String.IsNullOrEmpty(rTI) || String.IsNullOrEmpty(rTA) || String.IsNullOrEmpty(sDepth) || String.IsNullOrEmpty(eDepth))
                {
                    this.lblActRunSuccess.Text = "Cannot add Run. All values are required. Please try again.";
                    this.runEnclosure.Visible = true;
                    this.runEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iRNMessage.Attributes["class"] = "icon fa fa-warning";
                    this.spnRun.InnerText = " Warning!";
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wlName + " Service Order: " + wsName + " Run: " + rnName + " Error Adding Run ");
                }
                else
                {
                    rnInc = Convert.ToDecimal(rI);
                    rnAzm = Convert.ToDecimal(rA);
                    rnTInc = Convert.ToDecimal(rTI);
                    rnTAzm = Convert.ToDecimal(rTA);
                    startDepth = Convert.ToDecimal(sDepth);
                    endDepth = Convert.ToDecimal(eDepth);
                    runRes = Convert.ToInt32(OPR.addActiveRun(rnInc, rnAzm, rnTInc, rnTAzm, startDepth, endDepth, lengthUnit, operatorID, wellpadID, wellID, wellSectionID, wsName, tcID, sqcID, stID, wlName, rnID, rnName, LoginName, domain, GetClientDBString));
                    switch (runRes)
                    {
                        case 1:
                            {
                                this.runEnclosure.Visible = true;
                                this.runEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                                this.iRNMessage.Attributes["class"] = "icon fa fa-check-circle";
                                this.spnRun.InnerText = " Success!";
                                logger.Info(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wlName + " Well Section: " + wsName + " Run: " + rnName + " Added Run ");
                                this.lblActRunSuccess.Text = "Successfully added Active Run to <b><u> " + wlName + "</u></b>.<br />(Please add a BHA Signature for this RUN before proceeding to Data Audit/QC).";                                
                                this.lblActRunSuccess.Visible = true;
                                //Add Tie-In for Run if > Run1                                
                                break;
                            }
                        case -1:
                            {
                                this.lblActRunSuccess.Text = "Unable to add Well Profile Run. Please try again.";
                                this.runEnclosure.Visible = true;
                                this.runEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iRNMessage.Attributes["class"] = "icon fa fa-warning";
                                this.spnRun.InnerText = " Warning!";
                                logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wlName + " Service Order: " + wsName + " Run: " + rnName + " Error Adding Run ");
                                break;
                            }
                        case -2:
                            {
                                this.lblActRunSuccess.Text = "Unable to create Run-to-Well Section Association. Please try again.";
                                this.runEnclosure.Visible = true;
                                this.runEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iRNMessage.Attributes["class"] = "icon fa fa-warning";
                                this.spnRun.InnerText = " Warning!";
                                logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wlName + " Service Order: " + wsName + " Run: " + rnName + " Error Adding Run ");
                                break;
                            }
                        default:
                            {
                                this.lblActRunSuccess.Text = "Unable to create Run folder in the system. Please try again.";
                                this.runEnclosure.Visible = true;
                                this.runEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iRNMessage.Attributes["class"] = "icon fa fa-warning";
                                this.spnRun.InnerText = " Warning!";
                                logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wlName + " Service Order: " + wsName + " Run: " + rnName + " Error Adding Run Folder in HDD ");
                                break;
                            }
                    }
                    this.btnActRunCancel.CssClass = "btn btn-default text-center";
                    this.btnActRunCancel.Enabled = false;
                    this.btnActRunAdd.CssClass = "btn btn-default text-center";
                    this.btnActRunAdd.Enabled = false;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnActRunCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.ddlActRun.BorderColor = CLR.Empty;
                Dictionary<Int32, String> rnList = AST.getRunNamesList();
                this.ddlActRun.Items.Clear();
                this.ddlActRun.DataSource = rnList;
                this.ddlActRun.DataTextField = "Value";
                this.ddlActRun.DataValueField = "Key";
                this.ddlActRun.Items.Clear();
                this.ddlActRun.DataBind();
                this.ddlActRun.Items.Insert(0, new ListItem("--- Select Run ---", "-1"));
                this.ddlActRun.SelectedIndex = -1;
                this.ddlActRunSQC.BorderColor = CLR.Empty;
                this.ddlActRunSQC.Items.Clear();
                this.ddlActRunSQC.DataBind();
                this.ddlActRunSQC.Items.Insert(0, new ListItem("--- Select Survey Qualification Criteria ---", "-1"));
                this.ddlActRunSQC.SelectedIndex = -1;
                this.ddlActRunSQC.Enabled = false;
                this.ddlRunDepthUnit.Items.Clear();
                this.ddlRunDepthUnit.DataBind();
                this.ddlRunDepthUnit.Items.Insert(0, new ListItem("--- Select Depth Unit ---", "-1"));
                this.ddlRunDepthUnit.SelectedIndex = -1;
                this.ddlRunDepthUnit.Enabled = false;
                this.txtRunStartD.Text = "";
                this.txtRunStartD.Enabled = false;
                this.txtRunEndD.Text = "";
                this.txtRunEndD.Enabled = false;
                this.ddlActRunToolCode.BorderColor = CLR.Empty;
                this.ddlActRunToolCode.Items.Clear();
                this.ddlActRunToolCode.DataBind();
                this.ddlActRunToolCode.Items.Insert(0, new ListItem("--- Select Toolcode ---", "-1"));
                this.ddlActRunToolCode.SelectedIndex = -1;
                this.ddlActRunToolCode.Enabled = false;
                this.txtActRunAzm.Text = "";
                this.txtActRunAzm.Enabled = false;
                this.txtActRunInc.Text = "";
                this.txtActRunInc.Enabled = false;
                this.txtActRunTAzm.Text = "";
                this.txtActRunTAzm.Enabled = false;
                this.txtActRunTInc.Text = "";
                this.txtActRunTInc.Enabled = false;
                this.btnActRunAdd.Enabled = false;
                this.ddlActRunStatus.Items.Clear();
                this.ddlActRunStatus.DataBind();
                this.ddlActRunStatus.Items.Insert(0, new ListItem("--- Select Run Status ---", "-1"));
                this.ddlActRunStatus.SelectedIndex = -1;
                this.ddlActRunStatus.Enabled = false;
                this.btnActRunCancel.CssClass = "btn btn-default text-center";
                this.btnActRunCancel.Enabled = false;
                this.btnActRunAdd.CssClass = "btn btn-default text-center";
                this.btnActRunAdd.Enabled = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnActRunDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnActRunCancel_Click(null, null);
                this.divRunList.Visible = true;
                this.divRunAdd.Visible = false;
                Int32 optrID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue), wpdID = Convert.ToInt32(this.gvwWellPads.SelectedValue), wlID = Convert.ToInt32(this.gvwWell.SelectedValue), scID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                System.Data.DataTable rnTable = OPR.getRunForWellSectionTable(optrID, wpdID, wlID, scID, GetClientDBString);
                this.gvwRunList.DataSource = rnTable;
                this.gvwRunList.PageIndex = 0;
                this.gvwRunList.SelectedIndex = -1;
                this.gvwRunList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void createChartPlan()
        {
            String echarts3D = String.Empty;
            String result = String.Empty;
            try
            {
                Int32 wID = Convert.ToInt32(this.hdnWell.Value);
                System.Data.DataTable dsChartDatawithPlan = new System.Data.DataTable();
                System.Data.DataTable getScalerEastingNorthing = new System.Data.DataTable();

                String wellName = AST.getWellName(wID, GetClientDBString);
                Decimal maxTVD = CHRTRes.getMaxTVDforWellPlan(wID, GetClientDBString);
                dsChartDatawithPlan = CHRTRes.getDataTableforQCWellGraphWithPlan(wID, GetClientDBString);
                getScalerEastingNorthing = CHRTRes.getScallingFactorChartInfo_WPlan(wID, GetClientDBString);

                echarts3D = CHRT.getChartsQCResultsECHRT_3DWellPlanOnly("BodyContent_ctrlConfig_divECharts3D", wellName, dsChartDatawithPlan, getScalerEastingNorthing , maxTVD);
                this.ltrECHarts3DwithPlan.Text = echarts3D;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
            finally
            {
                //dsChartDatawithPlan.Dispose();
                result = String.Empty;
            }
        }

        protected void btnDisplayChartsWellPlan_Click(object sender, EventArgs e)
        {
            try
            {
                this.createChartPlan();
                this.charts.Visible = true;
                this.divDisplayButtons.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWReg_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 opID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 wpdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                Int32 wID = Convert.ToInt32(this.gvwWell.SelectedValue);
                System.Data.DataTable rgrsTable = AST.getWellServiceRegistration(opID, ClientID, wpdID, wID, GetClientDBString);
                this.gvwWReg.DataSource = rgrsTable;
                this.gvwWReg.PageIndex = e.NewPageIndex;
                this.gvwWReg.SelectedIndex = -1;
                this.gvwWReg.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWReg_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = ((DataRowView)e.Row.DataItem).Row;
                    String cClr = Convert.ToString(dr["appColor"]);
                    e.Row.Cells[3].BackColor = CLR.FromName(cClr);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddRefMag_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 result = -99, ifrVal = -99, welID = -99, mdO = -99, gcO = -99, jcID = -99, muID = -99, acuID = -99, optrID = -99, wpdID = -99;
                Decimal mdec = -99.00M, dip = -99.00M, BT = -99.000000M, GT = -99.000000M, GC = -99.00M;
                String wellName = String.Empty, jobName = String.Empty;
                String rmMDec = String.Empty, rmDip = String.Empty, rmBTotal = String.Empty, rmGTotal = String.Empty, rmGConv = String.Empty;
                optrID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                wpdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                //Get Values and check for validity
                welID = Convert.ToInt32(this.hdnWell.Value);
                if (rdbIFRYes.Checked) { ifrVal = 2; } else { ifrVal = 1; }
                if (rdbCTrue.Checked) { jcID = 1; } else { jcID = 2; }
                if (rdbGEast.Checked) { gcO = 1; } else { gcO = 2; }
                rmGConv = this.txtGConv.Text;
                if (String.IsNullOrEmpty(rmGConv))
                {
                    this.rfEnclosure.Visible = true;
                    this.rfEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iRFMessage.Attributes["class"] = "icon fa fa-warning";
                    this.spnRF.InnerText = " Warning!";
                    this.lblRMSuccess.Text = "Unable to accept Empty/Null values for Grid Convergence. Please provide a valid Grid Convergence value";
                }
                else
                {
                    if (Decimal.TryParse(rmGConv, out GC)) { GC = Convert.ToDecimal(rmGConv); } else {
                        this.rfEnclosure.Visible = true;
                        this.rfEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iRFMessage.Attributes["class"] = "icon fa fa-warning";
                        this.spnRF.InnerText = " Warning!";
                        this.lblRMSuccess.Text = "Invalid value for Grid Convergence. Please provide a valid Grid Convergence value";
                    }
                    if (this.rdbMDEast.Checked) { mdO = 9; } else { mdO = 25; }
                    rmMDec = this.txtMDec.Text;
                    if (String.IsNullOrEmpty(rmMDec))
                    {
                        this.rfEnclosure.Visible = true;
                        this.rfEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iRFMessage.Attributes["class"] = "icon fa fa-warning";
                        this.spnRF.InnerText = " Warning!";
                        this.lblRMSuccess.Text = "Unable to accept Empty/Null value for Magnetic Declination. Please provide a valid Magnetic Declination value";
                    }
                    else
                    {
                        if (Decimal.TryParse(rmMDec, out mdec)) { mdec = Convert.ToDecimal(rmMDec); } else {
                            this.rfEnclosure.Visible = true;
                            this.rfEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                            this.iRFMessage.Attributes["class"] = "icon fa fa-warning";
                            this.spnRF.InnerText = " Warning!";
                            this.lblRMSuccess.Text = "Invalid value for Magnetic Declination. Please provide a valid Magnetic Declination value";
                        }
                        rmDip = this.txtDip.Text;
                        if (String.IsNullOrEmpty(rmDip))
                        {
                            this.rfEnclosure.Visible = true;
                            this.rfEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                            this.iRFMessage.Attributes["class"] = "icon fa fa-warning";
                            this.spnRF.InnerText = " Warning!";
                            this.lblRMSuccess.Text = "Unable to accept Empty/Null value for Dip. Please provide a valid Dip value";
                        }
                        else
                        {
                            if (Decimal.TryParse(rmDip, out dip)) { dip = Convert.ToDecimal(rmDip); } else {
                                this.rfEnclosure.Visible = true;
                                this.rfEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iRFMessage.Attributes["class"] = "icon fa fa-warning";
                                this.spnRF.InnerText = " Warning!";
                                this.lblRMSuccess.Text = "Invalid value for Dip. Please provide a valid Dip value";
                            }
                            muID = Convert.ToInt32(this.ddlRFBTotalUnits.SelectedValue);
                            rmBTotal = this.txtBT.Text;
                            if (string.IsNullOrEmpty(rmBTotal))
                            {
                                this.rfEnclosure.Visible = true;
                                this.rfEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iRFMessage.Attributes["class"] = "icon fa fa-warning";
                                this.spnRF.InnerText = " Warning!";
                                this.lblRMSuccess.Text = "Unable to accept Empty/Null value for B-Total. Please provide a valud B-Total value";
                            }
                            else
                            {
                                if (Decimal.TryParse(rmBTotal, out BT)) { BT = Convert.ToDecimal(rmBTotal); } else {
                                    this.rfEnclosure.Visible = true;
                                    this.rfEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                    this.iRFMessage.Attributes["class"] = "icon fa fa-warning";
                                    this.spnRF.InnerText = " Warning!";
                                    this.lblRMSuccess.Text = "Invalid value for B-Total. Please provide a valid B-Total value";
                                }
                                acuID = Convert.ToInt32(this.ddlRFGTotalUnits.SelectedValue);
                                rmGTotal = this.txtGT.Text;
                                if (String.IsNullOrEmpty(rmGTotal))
                                {
                                    this.rfEnclosure.Visible = true;
                                    this.rfEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                    this.iRFMessage.Attributes["class"] = "icon fa fa-warning";
                                    this.spnRF.InnerText = " Warning!";
                                    this.lblRMSuccess.Text = "Unable to accept Empty/Null value for G-Total. Please provide a valude G-Total value";
                                }
                                else
                                {
                                    if (Decimal.TryParse(rmGTotal, out GT)) { GT = Convert.ToDecimal(rmGTotal); } else {
                                        this.rfEnclosure.Visible = true;
                                        this.rfEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                        this.iRFMessage.Attributes["class"] = "icon fa fa-warning";
                                        this.spnRF.InnerText = " Warning!";
                                        this.lblRMSuccess.Text = "Invalid value for G-Total. Please provide a valid G-Total value";
                                    }

                                    result = OPR.addRefMag(optrID, ClientID, wpdID, welID, ifrVal, mdec, mdO, dip, BT, muID, GT, acuID, GC, gcO, jcID, GetClientDBString);
                                    if (result.Equals(1))
                                    {
                                        this.rfEnclosure.Visible = true;
                                        this.rfEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                                        this.iRFMessage.Attributes["class"] = "icon fa fa-check-circle";
                                        this.spnRF.InnerText = " Success!";
                                        this.lblRMSuccess.Text = "Successfully added Reference Magnetics";
                                        logger.Info("User: " + LoginName + "Realm: " + domain + " Well Name: " + wellName + " Service Order: " + jobName + "Added Reference Magnetics");
                                    }
                                    else
                                    {
                                        this.rfEnclosure.Visible = true;
                                        this.rfEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                                        this.iRFMessage.Attributes["class"] = "icon fa fa-warning";
                                        this.spnRF.InnerText = " Warning!";
                                        this.lblRMSuccess.Text = "Could not insert Reference Magnetics. Please try again.";
                                        logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wellName + " Service Order: " + jobName + " Error Adding Reference Magnetics ");
                                    }
                                    this.btnAddRefMag.Enabled = false;
                                    this.btnAddRefMag.CssClass = "btn btn-default text-center";
                                    this.btnAddRefMagCancel.Enabled = true;
                                    this.btnAddRefMagCancel.CssClass = "btn btn-warning text-center";
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddRefMagCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.rdbIFRNo.Checked = false;
                this.rdbIFRYes.Checked = false;
                this.rdbCTrue.Checked = false;
                this.rdbCGrid.Checked = false;
                this.divConv.Attributes["class"] = "form-control bg-gray-light";
                this.rdbGEast.Checked = false;
                this.rdbGWest.Checked = false;
                this.divConvD.Attributes["class"] = "form-control bg-gray-light";
                this.txtMDec.Text = "";
                this.txtMDec.Enabled = false;
                this.txtDip.Text = "";
                this.txtDip.Enabled = false;
                this.txtBT.Text = "";
                this.txtBT.Enabled = false;
                this.txtGT.Text = "";
                this.txtGT.Enabled = false;
                this.txtGConv.Text = "";
                this.txtGConv.Enabled = false;
                this.divMDEW.Attributes["class"] = "form-control bg-gray-light";
                this.rdbMDEast.Checked = false;
                this.rdbMDWest.Checked = false;
                this.ddlRFBTotalUnits.Items.Clear();
                this.ddlRFBTotalUnits.DataBind();
                this.ddlRFBTotalUnits.Items.Insert(0, new ListItem("--- Select B-Total Units ---", "-1"));
                this.ddlRFBTotalUnits.SelectedIndex = -1;
                this.ddlRFBTotalUnits.Enabled = false;
                this.ddlRFGTotalUnits.Items.Clear();
                this.ddlRFGTotalUnits.DataBind();
                this.ddlRFGTotalUnits.Items.Insert(0, new ListItem("--- Select G-Total Units ---", "-1"));
                this.ddlRFGTotalUnits.SelectedIndex = -1;
                this.ddlRFGTotalUnits.Enabled = false;
                this.rfEnclosure.Visible = false;
                this.btnAddRefMagCancel.CssClass = "btn btn-default text-center";
                this.btnAddRefMagCancel.Enabled = false;
                this.btnAddRefMag.CssClass = "btn btn-default text-center";
                this.btnAddRefMag.Enabled = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddRefMagDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnAddRefMagCancel_Click(null, null);
                this.btnAddWell.Visible = true;
                this.btnWellReset.Visible = true;
                this.btnWellStepBack.Visible = true;
                this.btnWellExit.Visible = true;
                this.divWellRF.Visible = false;
                this.lblWellName.Text = "";
                this.divWellNameDetail.Visible = false;
                this.hdnWell.Value = String.Empty;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbMDEast_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbMDWest.Checked = false;
                this.txtMDec.Text = "";
                this.txtMDec.Enabled = true;
                this.txtMDec.Focus();
                this.txtDip.Text = "";
                this.txtDip.Enabled = true;
                Dictionary<Int32, String> btList = DMG.getMagUnitsList();
                this.ddlRFBTotalUnits.Items.Clear();
                this.ddlRFBTotalUnits.DataSource = btList;
                this.ddlRFBTotalUnits.DataTextField = "Value";
                this.ddlRFBTotalUnits.DataValueField = "Key";
                this.ddlRFBTotalUnits.DataBind();
                this.ddlRFBTotalUnits.Items.Insert(0, new ListItem("--- Select B-Total Units ---", "-1"));
                this.ddlRFBTotalUnits.SelectedIndex = -1;
                this.ddlRFBTotalUnits.Enabled = true;
                this.rfEnclosure.Visible = false;
                this.lblRMSuccess.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbMDWest_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbMDEast.Checked = false;
                this.txtMDec.Text = "";
                this.txtMDec.Enabled = true;
                this.txtMDec.Focus();
                this.txtDip.Text = "";
                this.txtDip.Enabled = true;
                Dictionary<Int32, String> btList = DMG.getMagUnitsList();
                this.ddlRFBTotalUnits.Items.Clear();
                this.ddlRFBTotalUnits.DataSource = btList;
                this.ddlRFBTotalUnits.DataTextField = "Value";
                this.ddlRFBTotalUnits.DataValueField = "Key";
                this.ddlRFBTotalUnits.DataBind();
                this.ddlRFBTotalUnits.Items.Insert(0, new ListItem("--- Select B-Total Units ---", "-1"));
                this.ddlRFBTotalUnits.SelectedIndex = -1;
                this.ddlRFBTotalUnits.Enabled = true;
                this.rfEnclosure.Visible = false;
                this.lblRMSuccess.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlRFBTotalUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtBT.Text = "";
                this.txtBT.Enabled = true;
                this.txtBT.Focus();
                Dictionary<Int32, String> gtList = AST.getAccelerometerUnits();
                this.ddlRFGTotalUnits.Items.Clear();
                this.ddlRFGTotalUnits.DataSource = gtList;
                this.ddlRFGTotalUnits.DataTextField = "Value";
                this.ddlRFGTotalUnits.DataValueField = "Key";
                this.ddlRFGTotalUnits.DataBind();
                this.ddlRFGTotalUnits.Items.Insert(0, new ListItem("--- Select G-Total Units ---", "-1"));
                this.ddlRFGTotalUnits.SelectedIndex = -1;
                this.ddlRFGTotalUnits.Enabled = true;
                this.rfEnclosure.Visible = false;
                this.lblRMSuccess.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlRFGTotalUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtGT.Text="";
                this.txtGT.Enabled = true;
                this.txtGT.Focus();
                this.btnAddRefMag.Enabled = true;
                this.btnAddRefMag.CssClass = "btn btn-success text-center";
                this.btnAddRefMagCancel.Enabled = true;
                this.btnAddRefMagCancel.CssClass = "btn btn-warning text-center";
                this.rfEnclosure.Visible = false;
                this.lblRMSuccess.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbCTrue_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.divConvD.Attributes["class"] = "form-control";
                this.rdbCGrid.Checked = false;                
                this.rdbGEast.Enabled = true;
                this.rdbGEast.Focus();
                this.rdbGWest.Enabled = true;
                this.txtGConv.Text = "";
                this.txtGConv.Enabled = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbCGrid_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbCTrue.Checked = false;
                this.divConvD.Attributes["class"] = "form-control";
                this.rdbGEast.Checked = false;
                this.rdbGEast.Enabled = true;
                this.rdbGEast.Focus();
                this.rdbGWest.Checked = false;
                this.rdbGWest.Enabled = true;                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbGEast_CheckedChanged(object sender, EventArgs e)
        {
            try
            {                
                this.rdbGWest.Checked = false;
                this.txtGConv.Text = "";
                this.txtGConv.Enabled = true;
                this.txtGConv.Focus();
                this.divMDEW.Attributes["class"] = "form-control";
                this.rdbMDEast.Enabled = true;
                this.rdbMDWest.Enabled = true;
                this.txtDip.Text = "";
                this.txtDip.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbGWest_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbGEast.Checked = false;
                this.txtGConv.Text = "";
                this.txtGConv.Enabled = true;
                this.txtGConv.Focus();
                this.divMDEW.Attributes["class"] = "form-control";
                this.rdbMDEast.Enabled = true;
                this.rdbMDWest.Enabled = true;
                this.txtDip.Text = "";
                this.txtDip.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbIFRYes_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbIFRNo.Checked = false;
                this.divConv.Attributes["class"] = "form-control";
                this.rdbCGrid.Enabled = true;
                this.rdbCTrue.Enabled = true;
                this.rfEnclosure.Visible = false;
                this.lblRMSuccess.Text = "";
                this.btnAddRefMagCancel.Enabled = true;
                this.btnAddRefMagCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbIFRNo_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbIFRYes.Checked = false;
                this.divConv.Attributes["class"] = "form-control";
                this.rdbCGrid.Enabled = true;
                this.rdbCTrue.Enabled = true;
                this.rfEnclosure.Visible = false;
                this.lblRMSuccess.Text = "";
                this.btnAddRefMagCancel.Enabled = true;
                this.btnAddRefMagCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbBGGM_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 datKeys = 0, dtkID = 0;
                Dictionary<Int32, String> datValues = AST.getCordSysList();
                datKeys = datValues.Keys.Count;
                if (datKeys >= (1))
                {
                    foreach (KeyValuePair<Int32, String> item in datValues)
                    {
                        dtkID = item.Key;
                    }

                    if (dtkID.Equals(-99))
                    {
                        this.ddlAddJobCord.BorderColor = CLR.Maroon;
                        this.ddlAddJobCord.Items.Clear();
                        this.ddlAddJobCord.DataBind();
                        this.ddlAddJobCord.Items.Insert(0, new ListItem("--- Select Coordinate System ---", "-1"));
                        this.ddlAddJobCord.SelectedIndex = -1;
                        this.ddlAddJobCord.ToolTip = "Unable to load Coordinate System List";
                    }
                    else
                    {
                        this.ddlAddJobCord.BorderColor = CLR.Green;
                        this.ddlAddJobCord.Items.Clear();
                        this.ddlAddJobCord.DataSource = datValues;
                        this.ddlAddJobCord.DataTextField = "Value";
                        this.ddlAddJobCord.DataValueField = "Key";
                        this.ddlAddJobCord.DataBind();
                        this.ddlAddJobCord.Items.Insert(0, new ListItem("--- Select Coordinate System ---", "-1"));
                        this.ddlAddJobCord.SelectedIndex = -1;
                        this.ddlAddJobCord.Enabled = true;

                        this.ddlAddJobCord.Focus();
                    }
                }
                else
                {
                    this.ddlAddJobCord.BorderColor = CLR.Maroon;
                    this.ddlAddJobCord.Items.Clear();
                    this.ddlAddJobCord.DataBind();
                    this.ddlAddJobCord.Items.Insert(0, new ListItem("--- Select Coordinate System ---", "-1"));
                    this.ddlAddJobCord.SelectedIndex = -1;
                    this.ddlAddJobCord.ToolTip = "Unable to load Data";
                }
                this.rdbHDGM.Checked = false;
                this.rdbIFR.Checked = false;
                this.rdbIGRF.Checked = false;
                this.rdbIIFR.Checked = false;
                this.btnAddWellCancel.Enabled = true;
                this.btnAddWellCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbHDGM_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 datKeys = 0, dtkID = 0;
                Dictionary<Int32, String> datValues = AST.getCordSysList();
                datKeys = datValues.Keys.Count;
                if (datKeys >= (1))
                {
                    foreach (KeyValuePair<Int32, String> item in datValues)
                    {
                        dtkID = item.Key;
                    }

                    if (dtkID.Equals(-99))
                    {
                        this.ddlAddJobCord.BorderColor = CLR.Maroon;
                        this.ddlAddJobCord.Items.Clear();
                        this.ddlAddJobCord.DataBind();
                        this.ddlAddJobCord.Items.Insert(0, new ListItem("--- Select Coordinate System ---", "-1"));
                        this.ddlAddJobCord.SelectedIndex = -1;
                        this.ddlAddJobCord.ToolTip = "Unable to load Coordinate System List";
                    }
                    else
                    {
                        this.ddlAddJobCord.BorderColor = CLR.Green;
                        this.ddlAddJobCord.Items.Clear();
                        this.ddlAddJobCord.DataSource = datValues;
                        this.ddlAddJobCord.DataTextField = "Value";
                        this.ddlAddJobCord.DataValueField = "Key";
                        this.ddlAddJobCord.DataBind();
                        this.ddlAddJobCord.Items.Insert(0, new ListItem("--- Select Coordinate System ---", "-1"));
                        this.ddlAddJobCord.SelectedIndex = -1;
                        this.ddlAddJobCord.Enabled = true;

                        this.ddlAddJobCord.Focus();
                    }
                }
                else
                {
                    this.ddlAddJobCord.BorderColor = CLR.Maroon;
                    this.ddlAddJobCord.Items.Clear();
                    this.ddlAddJobCord.DataBind();
                    this.ddlAddJobCord.Items.Insert(0, new ListItem("--- Select Coordinate System ---", "-1"));
                    this.ddlAddJobCord.SelectedIndex = -1;
                    this.ddlAddJobCord.ToolTip = "Unable to load Data";
                }
                this.rdbBGGM.Checked = false;
                this.rdbIFR.Checked = false;
                this.rdbIGRF.Checked = false;
                this.rdbIIFR.Checked = false;
                this.rfEnclosure.Visible = false;
                this.lblRMSuccess.Text = "";
                this.btnAddWellCancel.Enabled = true;
                this.btnAddWellCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbIFR_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 datKeys = 0, dtkID = 0;
                Dictionary<Int32, String> datValues = AST.getCordSysList();
                datKeys = datValues.Keys.Count;
                if (datKeys >= (1))
                {
                    foreach (KeyValuePair<Int32, String> item in datValues)
                    {
                        dtkID = item.Key;
                    }

                    if (dtkID.Equals(-99))
                    {
                        this.ddlAddJobCord.BorderColor = CLR.Maroon;
                        this.ddlAddJobCord.Items.Clear();
                        this.ddlAddJobCord.DataBind();
                        this.ddlAddJobCord.Items.Insert(0, new ListItem("--- Select Coordinate System ---", "-1"));
                        this.ddlAddJobCord.SelectedIndex = -1;
                        this.ddlAddJobCord.ToolTip = "Unable to load Coordinate System List";
                    }
                    else
                    {
                        this.ddlAddJobCord.BorderColor = CLR.Green;
                        this.ddlAddJobCord.Items.Clear();
                        this.ddlAddJobCord.DataSource = datValues;
                        this.ddlAddJobCord.DataTextField = "Value";
                        this.ddlAddJobCord.DataValueField = "Key";
                        this.ddlAddJobCord.DataBind();
                        this.ddlAddJobCord.Items.Insert(0, new ListItem("--- Select Coordinate System ---", "-1"));
                        this.ddlAddJobCord.SelectedIndex = -1;
                        this.ddlAddJobCord.Enabled = true;

                        this.ddlAddJobCord.Focus();
                    }
                }
                else
                {
                    this.ddlAddJobCord.BorderColor = CLR.Maroon;
                    this.ddlAddJobCord.Items.Clear();
                    this.ddlAddJobCord.DataBind();
                    this.ddlAddJobCord.Items.Insert(0, new ListItem("--- Select Coordinate System ---", "-1"));
                    this.ddlAddJobCord.SelectedIndex = -1;
                    this.ddlAddJobCord.ToolTip = "Unable to load Data";
                }
                this.rdbBGGM.Checked = false;
                this.rdbHDGM.Checked = false;                
                this.rdbIGRF.Checked = false;
                this.rdbIIFR.Checked = false;
                this.rfEnclosure.Visible = false;
                this.lblRMSuccess.Text = "";
                this.btnAddWellCancel.Enabled = true;
                this.btnAddWellCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbIGRF_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 datKeys = 0, dtkID = 0;
                Dictionary<Int32, String> datValues = AST.getCordSysList();
                datKeys = datValues.Keys.Count;
                if (datKeys >= (1))
                {
                    foreach (KeyValuePair<Int32, String> item in datValues)
                    {
                        dtkID = item.Key;
                    }

                    if (dtkID.Equals(-99))
                    {
                        this.ddlAddJobCord.BorderColor = CLR.Maroon;
                        this.ddlAddJobCord.Items.Clear();
                        this.ddlAddJobCord.DataBind();
                        this.ddlAddJobCord.Items.Insert(0, new ListItem("--- Select Coordinate System ---", "-1"));
                        this.ddlAddJobCord.SelectedIndex = -1;
                        this.ddlAddJobCord.ToolTip = "Unable to load Coordinate System List";
                    }
                    else
                    {
                        this.ddlAddJobCord.BorderColor = CLR.Green;
                        this.ddlAddJobCord.Items.Clear();
                        this.ddlAddJobCord.DataSource = datValues;
                        this.ddlAddJobCord.DataTextField = "Value";
                        this.ddlAddJobCord.DataValueField = "Key";
                        this.ddlAddJobCord.DataBind();
                        this.ddlAddJobCord.Items.Insert(0, new ListItem("--- Select Coordinate System ---", "-1"));
                        this.ddlAddJobCord.SelectedIndex = -1;
                        this.ddlAddJobCord.Enabled = true;

                        this.ddlAddJobCord.Focus();
                    }
                }
                else
                {
                    this.ddlAddJobCord.BorderColor = CLR.Maroon;
                    this.ddlAddJobCord.Items.Clear();
                    this.ddlAddJobCord.DataBind();
                    this.ddlAddJobCord.Items.Insert(0, new ListItem("--- Select Coordinate System ---", "-1"));
                    this.ddlAddJobCord.SelectedIndex = -1;
                    this.ddlAddJobCord.ToolTip = "Unable to load Data";
                }
                this.rdbBGGM.Checked = false;
                this.rdbHDGM.Checked = false;
                this.rdbIFR.Checked = false;
                this.rdbIIFR.Checked = false;
                this.btnAddWellCancel.Enabled = true;
                this.btnAddWellCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbIIFR_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 datKeys = 0, dtkID = 0;
                Dictionary<Int32, String> datValues = AST.getCordSysList();
                datKeys = datValues.Keys.Count;
                if (datKeys >= (1))
                {
                    foreach (KeyValuePair<Int32, String> item in datValues)
                    {
                        dtkID = item.Key;
                    }

                    if (dtkID.Equals(-99))
                    {
                        this.ddlAddJobCord.BorderColor = CLR.Maroon;
                        this.ddlAddJobCord.Items.Clear();
                        this.ddlAddJobCord.DataBind();
                        this.ddlAddJobCord.Items.Insert(0, new ListItem("--- Select Coordinate System ---", "-1"));
                        this.ddlAddJobCord.SelectedIndex = -1;
                        this.ddlAddJobCord.ToolTip = "Unable to load Coordinate System List";
                    }
                    else
                    {
                        this.ddlAddJobCord.BorderColor = CLR.Green;
                        this.ddlAddJobCord.Items.Clear();
                        this.ddlAddJobCord.DataSource = datValues;
                        this.ddlAddJobCord.DataTextField = "Value";
                        this.ddlAddJobCord.DataValueField = "Key";
                        this.ddlAddJobCord.DataBind();
                        this.ddlAddJobCord.Items.Insert(0, new ListItem("--- Select Coordinate System ---", "-1"));
                        this.ddlAddJobCord.SelectedIndex = -1;
                        this.ddlAddJobCord.Enabled = true;

                        this.ddlAddJobCord.Focus();
                    }
                }
                else
                {
                    this.ddlAddJobCord.BorderColor = CLR.Maroon;
                    this.ddlAddJobCord.Items.Clear();
                    this.ddlAddJobCord.DataBind();
                    this.ddlAddJobCord.Items.Insert(0, new ListItem("--- Select Coordinate System ---", "-1"));
                    this.ddlAddJobCord.SelectedIndex = -1;
                    this.ddlAddJobCord.ToolTip = "Unable to load Data";
                }
                this.rdbBGGM.Checked = false;
                this.rdbHDGM.Checked = false;
                this.rdbIFR.Checked = false;
                this.rdbIGRF.Checked = false;
                this.btnAddWellCancel.Enabled = true;
                this.btnAddWellCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAddJobCord_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 pjKeys = 0, pjkID = 0;
                Dictionary<Int32, String> projValues = AST.getProjectionsList();
                pjKeys = projValues.Keys.Count;
                if (pjKeys >= (1))
                {
                    foreach (KeyValuePair<Int32, String> item in projValues)
                    {
                        pjkID = item.Key;
                    }

                    if (pjkID.Equals(-99))
                    {
                        this.ddlAddJobProj.BorderColor = CLR.Maroon;
                        this.ddlAddJobProj.Items.Clear();
                        this.ddlAddJobProj.DataBind();
                        this.ddlAddJobProj.Items.Insert(0, new ListItem("--- Select Projection ---", "-1"));
                        this.ddlAddJobProj.SelectedIndex = -1;
                        this.ddlAddJobProj.ToolTip = "Unable to load Projections list";
                    }
                    else
                    {
                        this.ddlAddJobProj.BorderColor = CLR.Green;
                        this.ddlAddJobProj.Items.Clear();
                        this.ddlAddJobProj.DataSource = projValues;
                        this.ddlAddJobProj.DataTextField = "Value";
                        this.ddlAddJobProj.DataValueField = "Key";
                        this.ddlAddJobProj.DataBind();
                        this.ddlAddJobProj.Items.Insert(0, new ListItem("--- Select Projection ---", "-1"));
                        this.ddlAddJobProj.SelectedIndex = -1;
                        this.ddlAddJobProj.Enabled = true;
                        this.ddlAddJobProj.Focus();
                    }
                }
                else
                {
                    this.ddlAddJobProj.BorderColor = CLR.Maroon;
                    this.ddlAddJobProj.Items.Clear();
                    this.ddlAddJobProj.DataBind();
                    this.ddlAddJobProj.Items.Insert(0, new ListItem("--- Select Projection ---", "-1"));
                    this.ddlAddJobProj.SelectedIndex = -1;
                    this.ddlAddJobProj.ToolTip = "Unable to load Data";
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAddJobProj_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.divLatO.Attributes["class"] = "form-control";
                this.rdbWNorth.Enabled = true;
                this.rdbWNorth.Checked = false;
                this.rdbWSouth.Enabled = true;
                this.rdbWSouth.Checked = false;
                this.txtWellAddLat.Text = "";
                this.txtWellAddLat.Enabled = true;
                this.rdbWNorth.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbWNorth_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbWSouth.Checked = false;
                this.txtWellAddLat.Text = "";
                this.txtWellAddLat.Enabled = true;
                this.txtWellAddLat.Focus();
                this.rdbWEast.Checked = false;
                this.rdbWEast.Enabled = true;
                this.rdbWWest.Checked = false;
                this.rdbWWest.Enabled = true;
                this.divLngO.Attributes["class"] = "form-control";                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbWSouth_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbWNorth.Checked = false;
                this.txtWellAddLat.Text = "";
                this.txtWellAddLat.Enabled = true;
                this.txtWellAddLat.Focus();
                this.rdbWEast.Checked = false;
                this.rdbWEast.Enabled = true;
                this.rdbWWest.Checked = false;
                this.rdbWWest.Enabled = true;
                this.divLngO.Attributes["class"] = "form-control";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbWEast_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbWWest.Checked = false;
                this.txtWellAddLon.Text = "";
                this.txtWellAddLon.Enabled = true;
                this.txtWellAddLon.Focus();
                this.btnWellNew.Enabled = true;
                this.btnWellNew.CssClass = "btn btn-success text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbWWest_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbWEast.Checked = false;
                this.txtWellAddLon.Text = "";
                this.txtWellAddLon.Enabled = true;
                this.txtWellAddLon.Focus();
                this.btnWellNew.Enabled = true;
                this.btnWellNew.CssClass = "btn btn-success text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWSTUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, welID = Convert.ToInt32(this.hdnWell.Value), oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue), wstID = Convert.ToInt32(this.ddlWSWellStatus.SelectedValue);
                String stName = Convert.ToString(this.ddlWSWellStatus.SelectedItem);
                iResult = AST.updateWellStatus(oprID, welID, wstID, GetClientDBString);
                if (iResult.Equals(1))
                {
                    this.btnWSTClear_Click(null, null);
                    this.wstEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                    this.iWST.Attributes["class"] = "icon fa fa-check-circle";
                    this.spnWST.InnerText = " Updated!";
                    this.lblWST.Text = "Successfully updated Well Status.";
                    this.wstEnclosure.Visible = true;
                    this.createAssetCharts(null, null);
                    logger.Info(" User: " + LoginName + " Realm: " + domain + " Well ID: " + welID + " Status: " + stName + " Updated ");
                }
                else
                {
                    this.wstEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iWST.Attributes["class"] = "icon fa fa-warning";
                    this.spnWST.InnerText = " Warning!";
                    this.lblWST.Text = "!!! Error !!! Missing value detected. Please select all parameters";
                    this.wstEnclosure.Visible = true;
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Well ID: " + welID + " Status: " + stName + " Updating Status (Missing Values) ");
                    this.btnWSTUpdate.Enabled = false;
                    this.btnWSTUpdate.CssClass = "btn btn-default text-center";
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWSTClear_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> stList = DMG.getWellStatusListForClients();
                this.ddlWSWellStatus.Items.Clear();
                this.ddlWSWellStatus.DataSource = stList;
                this.ddlWSWellStatus.DataTextField = "Value";
                this.ddlWSWellStatus.DataValueField = "Key";
                this.ddlWSWellStatus.DataBind();
                this.ddlWSWellStatus.Items.Insert(0, new ListItem("--- Select Well Status ---", "-1"));
                this.ddlWSWellStatus.SelectedIndex = -1;
                this.btnWSTUpdate.Enabled = false;
                this.btnWSTUpdate.CssClass = "btn btn-default text-center";
                this.btnWSTClear.Enabled = false;
                this.btnWSTClear.CssClass = "btn btn-default text-center";
                this.wstEnclosure.Visible = false;
                this.lblWST.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWSTDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnWSTClear_Click(null, null);
                this.divWellList.Visible = true;
                this.divWellStatus.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbGE_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbMSL.Checked = false;
                this.divDR.Attributes["class"] = "form-control";
                this.rdbGElev.Checked = false;
                this.rdbGElev.Enabled = true;
                this.rdbGElev.Focus();
                this.rdbKB.Checked = false;
                this.rdbKB.Enabled = true;
                this.rdbRF.Checked = false;
                this.rdbRF.Enabled = true;
                this.btnWPInfoReset.Enabled = true;
                this.btnWPInfoReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbMSL_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbGE.Checked = false;
                this.divDR.Attributes["class"] = "form-control";
                this.rdbGElev.Checked = false;
                this.rdbGElev.Enabled = true;
                this.rdbGElev.Focus();
                this.rdbKB.Checked = false;
                this.rdbKB.Enabled = true;
                this.rdbRF.Checked = false;
                this.rdbRF.Enabled = true;
                this.btnWPInfoReset.Enabled = true;
                this.btnWPInfoReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbGElev_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbKB.Checked = false;
                this.rdbRF.Checked = false;
                this.divElevUnit.Attributes["class"] = "input-group form-control";
                this.rdbElFeet.Checked = false;
                this.rdbElFeet.Enabled = true;
                this.rdbElFeet.Focus();
                this.rdbElMeter.Checked = false;
                this.rdbElMeter.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbKB_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbGElev.Checked = false;
                this.rdbRF.Checked = false;
                this.divElevUnit.Attributes["class"] = "input-group form-control";
                this.rdbElFeet.Checked = false;
                this.rdbElFeet.Enabled = true;
                this.rdbElFeet.Focus();
                this.rdbElMeter.Checked = false;
                this.rdbElMeter.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbRF_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbGElev.Checked = false;
                this.rdbKB.Checked = false;
                this.divElevUnit.Attributes["class"] = "input-group form-control";
                this.rdbElFeet.Checked = false;
                this.rdbElFeet.Enabled = true;
                this.rdbElMeter.Checked = false;
                this.rdbElMeter.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbElFeet_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbElMeter.Checked = false;
                this.txtWPDRElevation.Text = "";
                this.txtWPDRElevation.Enabled = true;
                this.txtWPDRElevation.Focus();
                this.divVSUnit.Attributes["class"] = "form-control";
                this.rdbVSFeet.Checked = false;
                this.rdbVSFeet.Enabled = true;
                this.rdbVSMeter.Checked = false;
                this.rdbVSMeter.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbElMeter_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbElFeet.Checked = false;
                this.txtWPDRElevation.Text = "";
                this.txtWPDRElevation.Enabled = true;
                this.txtWPDRElevation.Focus();
                this.divVSUnit.Attributes["class"] = "form-control";
                this.rdbVSFeet.Checked = false;
                this.rdbVSFeet.Enabled = true;
                this.rdbVSMeter.Checked = false;
                this.rdbVSMeter.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbVSFeet_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbVSMeter.Checked = false;
                this.txtWPVSAzimuth.Text = "";
                this.txtWPVSAzimuth.Enabled = true;
                this.txtWPVSAzimuth.Focus();
                this.divN.Attributes["class"] = "form-control";
                this.rdbNFeet.Checked = false;
                this.rdbNFeet.Enabled = true;
                this.rdbNMeter.Checked = false;
                this.rdbNMeter.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbVSMeter_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbVSFeet.Checked = false;
                this.txtWPVSAzimuth.Text = "";
                this.txtWPVSAzimuth.Enabled = true;
                this.txtWPVSAzimuth.Focus();
                this.divN.Attributes["class"] = "form-control";
                this.rdbNFeet.Checked = false;
                this.rdbNFeet.Enabled = true;
                this.rdbNMeter.Checked = false;
                this.rdbNMeter.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbNFeet_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbNMeter.Checked = false;
                this.txtWPNorthing.Text = "";
                this.txtWPNorthing.Enabled = true;
                this.txtWPNorthing.Focus();
                this.divE.Attributes["class"] = "form-control";
                this.rdbEFeet.Checked = false;
                this.rdbEFeet.Enabled = true;
                this.rdbEMeter.Checked = false;
                this.rdbEMeter.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbNMeter_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbNFeet.Checked = false;
                this.txtWPNorthing.Text = "";
                this.txtWPNorthing.Enabled = true;
                this.txtWPNorthing.Focus();
                this.divE.Attributes["class"] = "form-control";
                this.rdbEFeet.Checked = false;
                this.rdbEFeet.Enabled = true;
                this.rdbEMeter.Checked = false;
                this.rdbEMeter.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbEFeet_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbEMeter.Checked = false;
                this.txtWPEasting.Text = "";
                this.txtWPEasting.Enabled = true;
                this.txtWPEasting.Focus();
                this.FileUploadControl.Enabled = true;
                this.btnWPInfoNext.Enabled = true;
                this.btnWPInfoNext.CssClass = "btn btn-info text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbEMeter_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbEFeet.Checked = false;
                this.txtWPEasting.Text = "";
                this.txtWPEasting.Enabled = true;
                this.txtWPEasting.Focus();
                this.FileUploadControl.Enabled = true;
                this.btnWPInfoNext.Enabled = true;
                this.btnWPInfoNext.CssClass = "btn btn-info text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlNWSWSection_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnNWSWAdd.CssClass = "btn btn-success text-center";
                this.btnNWSWAdd.Enabled = true;
                this.btnNWSWClear.CssClass = "btn btn-warning text-center";
                this.btnNWSWClear.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnNWSWAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, wellID = -99, sectionID = -99;
                wellID = Convert.ToInt32(this.gvwWell.SelectedValue);
                sectionID = Convert.ToInt32(this.ddlNWSWSection.SelectedValue);
                GridViewRow wgRow = this.gvwWell.SelectedRow;
                String welName = Convert.ToString(Server.HtmlDecode(wgRow.Cells[2].Text));
                String secName = Convert.ToString(this.ddlNWSWSection.SelectedItem);
                if (!wellID.Equals(-1) || !sectionID.Equals(-1))
                {
                    iResult = DMG.addWellSectionToWell(sectionID, wellID, GetClientDBString);
                    if (iResult.Equals(1))
                    {
                        this.lblNWSWSuccess.Text = "Successfully inserted Well Section for selected Well/Lateral";
                        this.wsEnclosure.Visible = true;
                        this.wsEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iWSMessage.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnWS.InnerText = " Success!";
                        this.btnNWSWDone.Focus();
                        logger.Info(" User: " + LoginName + " Realm: " + domain + " Well Name: " + welName + " Service Order: " + secName + " Added Well Section ");
                        this.btnNWSWAdd.CssClass = "btn btn-default text-center";
                        this.btnNWSWAdd.Enabled = false;
                    }
                    else
                    {
                        this.lblNWSWSuccess.Text = "Unable to insert Well Section";
                        this.wsEnclosure.Visible = true;
                        this.wsEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iWSMessage.Attributes["class"] = "icon fa fa-warning";
                        this.spnWS.InnerText = " Warning!";
                        this.btnNWSWDone.Focus();
                        logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + welName + " Service Order: " + secName + " Error Adding Well Section ");
                    }
                }
                else
                {
                    this.lblNWSWSuccess.Text = "Unable to insert Well Section";
                    this.wsEnclosure.Visible = true;
                    this.wsEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iWSMessage.Attributes["class"] = "icon fa fa-warning";
                    this.spnWS.InnerText = " Warning!";
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + welName + " Service Order: " + secName + " Other Error Adding Well Section ");
                }
                this.btnNWSWAdd.CssClass = "btn btn-default text-center";
                this.btnNWSWAdd.Enabled = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnNWSWClear_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 wellID = Convert.ToInt32(this.gvwWell.SelectedValue);
                Dictionary<Int32, String> scList = OPR.getWellSectionsList(wellID, GetClientDBString);
                this.ddlNWSWSection.Items.Clear();
                this.ddlNWSWSection.DataSource = scList;
                this.ddlNWSWSection.DataTextField = "Value";
                this.ddlNWSWSection.DataValueField = "Key";
                this.ddlNWSWSection.DataBind();
                this.ddlNWSWSection.Items.Insert(0, new ListItem("--- Select Well/Lateral Section ---", "-1"));
                this.ddlNWSWSection.SelectedIndex = -1;
                this.btnNWSWClear.CssClass = "btn btn-default text-center";
                this.btnNWSWClear.Enabled = false;
                this.btnNWSWAdd.CssClass = "btn btn-default text-center";
                this.btnNWSWAdd.Enabled = false;
                this.wsEnclosure.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnNWSWDone_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 wellID = Convert.ToInt32(this.gvwWell.SelectedValue);
                this.btnNWSWClear_Click(null, null);
                this.divSection.Visible = true;
                System.Data.DataTable wsTable = OPR.getWellSectionToWellTable(wellID, GetClientDBString);
                this.gvwSectionList.DataSource = wsTable;
                this.gvwSectionList.PageIndex = 0;
                this.gvwSectionList.SelectedIndex = -1;
                this.gvwSectionList.DataBind();
                this.divSectionAdd.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRunDetails_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 welID = -99, wsID = -99, runID = -99, opID = -99, pdID = -99;
                Button rnBtn = (Button)sender;
                GridViewRow gRow = (GridViewRow)rnBtn.NamingContainer;
                Int32 rowIndex = Convert.ToInt32(gRow.RowIndex);
                opID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                pdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                runID = Convert.ToInt32(gvwRunList.DataKeys[rowIndex]["runID"]);                
                welID = Convert.ToInt32(this.gvwWell.SelectedValue);
                wsID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                this.divlblRTIDetail.Visible = true;
                System.Data.DataTable rtiList = WPN.getRunTieInSurveyTable(runID, wsID, welID, pdID, ClientID, opID, GetClientDBString); 
                this.gvwRTI.DataSource = rtiList;
                this.gvwRTI.DataBind();
                this.divgvwRTI.Visible = true;
                this.lblBHADetail.Visible = true;
                System.Data.DataTable bhaTable = AST.getBhaSign(runID, GetClientDBString);
                this.gvwBHA.DataSource = bhaTable;
                this.gvwBHA.DataBind();
                this.gvwBHA.Visible = true;
                this.btnRunReset.Enabled = true;
                this.btnRunReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRunStatus_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 rnID = -99;
                Button stBtn = (Button)sender;
                GridViewRow gRow = (GridViewRow)stBtn.NamingContainer;
                Int32 rowIndex = Convert.ToInt32(gRow.RowIndex);
                rnID = Convert.ToInt32(gvwRunList.DataKeys[rowIndex]["runID"]);
                this.hdnRunID.Value = rnID.ToString();
                this.divlblRTIDetail.Visible = false;
                this.gvwRTI.DataSource = null;
                this.gvwRTI.DataBind();
                this.divgvwRTI.Visible = false;
                this.lblBHADetail.Visible = false;
                this.gvwBHA.DataSource = null;
                this.gvwBHA.DataBind();
                this.gvwBHA.Visible = false;
                this.divRunList.Visible = false;
                this.divRunAdd.Visible = false;
                this.divBHA.Visible = false;
                this.divRunTieIn.Visible = false;
                this.divRunStatus.Visible = true;
                Dictionary<Int32, String> sttList = DMG.getRunStatusListForClients();
                this.ddlRunStatus.Items.Clear();
                this.ddlRunStatus.DataSource = sttList;
                this.ddlRunStatus.DataTextField = "Value";
                this.ddlRunStatus.DataValueField = "Key";
                this.ddlRunStatus.DataBind();
                this.ddlRunStatus.Items.Insert(0, new ListItem("--- Select Run Status ---", "-1"));
                this.ddlRunStatus.SelectedIndex = -1;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddBHA_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 rnID = -99;
                Button rnBtn = (Button)sender;
                GridViewRow gRow = (GridViewRow)rnBtn.NamingContainer;
                Int32 rowIndex = Convert.ToInt32(gRow.RowIndex);
                rnID = Convert.ToInt32(gvwRunList.DataKeys[rowIndex]["runID"]);
                this.hdnRunID.Value = rnID.ToString();
                this.divlblRTIDetail.Visible = false;
                this.gvwRTI.DataSource = null;
                this.gvwRTI.DataBind();
                this.divgvwRTI.Visible = false;
                this.lblBHADetail.Visible = false;
                this.gvwBHA.DataSource = null;
                this.gvwBHA.DataBind();
                this.gvwBHA.Visible = false;
                this.divRunList.Visible = false;
                this.divRunAdd.Visible = false;
                this.divBHA.Visible = true;
                this.divRunTieIn.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddTieIn_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 rnID = -99;
                Button rnBtn = (Button)sender;
                GridViewRow gRow = (GridViewRow)rnBtn.NamingContainer;
                Int32 rowIndex = Convert.ToInt32(gRow.RowIndex);
                rnID = Convert.ToInt32(gvwRunList.DataKeys[rowIndex]["runID"]);
                this.hdnRunID.Value = rnID.ToString();
                this.divlblRTIDetail.Visible = false;
                this.gvwRTI.DataSource = null;
                this.gvwRTI.DataBind();
                this.divgvwRTI.Visible = false;
                this.lblBHADetail.Visible = false;
                this.gvwBHA.DataSource = null;
                this.gvwBHA.DataBind();
                this.gvwBHA.Visible = false;
                this.divRunList.Visible = false;
                this.divRunAdd.Visible = false;
                this.divBHA.Visible = false;
                this.divRunTieIn.Visible = true;
                Int32 wellID = Convert.ToInt32(this.gvwWell.SelectedValue);
                Int32 wellSectionID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                Int32 runID = Convert.ToInt32(this.hdnRunID.Value);
                String rnName = AST.getRunName(runID);
                this.rdbRTIMDFt.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnBHAAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 result = -99, secID = -99, padID = -99, oprID = -99;
                Int32 welID = -99, runID = -99, bhaMSzU = -99, bhaDSCU = -99, bhaMDBLenU = -99, bhaAbvU = -99, bhaBlwU = -99;
                Decimal bhaMSz = -99.00M, bhaDSC = -99.00M, bhaMDBLen = -99.00M, bhaAbv = -99.00M, bhaBlw = -99.00M;
                Int32 bhaGTopU = -99, bhaGDSCU = -99, bhaStat = -99;
                String wName = String.Empty, rName = String.Empty, bhaName = String.Empty, MtrSize = String.Empty, DClrSize = String.Empty, Length = String.Empty;
                String jName = String.Empty, secName = String.Empty;
                String Above = String.Empty, Below = String.Empty;
                oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                padID = Convert.ToInt32(this.gvwWellPads.SelectedValue);                
                welID = Convert.ToInt32(this.gvwWell.SelectedValue);
                secID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                wName = AST.getWellName(welID, GetClientDBString);
                runID = Convert.ToInt32(this.hdnRunID.Value);
                bhaName = Convert.ToString(this.txtBHAName.Text);
                MtrSize = Convert.ToString(this.txtAddBHAMSize.Text);
                if (rdbMSUIn.Checked) { bhaMSzU = 3; } else { bhaMSzU = 2; }
                DClrSize = Convert.ToString(this.txtBHADSC.Text);
                if (rdbCSUIn.Checked) { bhaDSCU = 3; } else { bhaDSCU = 2; }
                Length = Convert.ToString(this.txtBHAMDBLen.Text);
                if (rdbMDBInch.Checked) { bhaMDBLenU = 3; } else if (rdbMDBFeet.Checked) { bhaMDBLenU = 4; } else { bhaMDBLenU = 5; }
                Above = Convert.ToString(this.txtBHAAbv.Text);
                if (rdbAbvIn.Checked) { bhaAbvU = 3; } else if (rdbAbvFt.Checked) { bhaAbvU = 4; } else { bhaAbvU = 5; }
                Below = Convert.ToString(this.txtBHABlw.Text);
                if (rdbBlwIn.Checked) { bhaBlwU = 3; } else if (rdbBlwFt.Checked) { bhaBlwU = 4; } else { bhaBlwU = 5; }
                bhaStat = 1;
                GridViewRow sgRow = this.gvwSectionList.SelectedRow;
                secName = Convert.ToString(Server.HtmlDecode(sgRow.Cells[2].Text));
                GridViewRow rgRow = this.gvwRunList.SelectedRow;
                rName = AST.getRunName(runID);
                
                if ((runID.Equals(-99)) || (runID.Equals(null)) || (String.IsNullOrEmpty(MtrSize)) || (bhaMSzU.Equals(-99)) || (bhaMSzU.Equals(null)) || (String.IsNullOrEmpty(DClrSize)) || (bhaDSCU.Equals(-99)) || (bhaDSCU.Equals(null)) || (String.IsNullOrEmpty(Length)) || (bhaMDBLenU.Equals(-99)) || (bhaMDBLenU.Equals(null)) || (String.IsNullOrEmpty(Above)) || (bhaAbvU.Equals(-99)) || (bhaAbvU.Equals(null)) || (String.IsNullOrEmpty(Below)) || (bhaBlwU.Equals(-99)))
                {
                    this.bhaEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iBHAMessage.Attributes["class"] = "icon fa fa-warning";
                    this.spnBHA.InnerText = " Warning!";
                    this.lblBHASuccess.Text = "Missing values. Please provide all values and measurements unit to add BHA to SMARTs";
                    this.bhaEnclosure.Visible = true;
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Service Order: " + jName + " Well_Name: " + wName + " Section: " + secName + " Run: " + rName + " Error Adding BHA ");
                }
                else if(!Decimal.TryParse(MtrSize, out bhaMSz))
                {
                    this.bhaEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iBHAMessage.Attributes["class"] = "icon fa fa-warning";
                    this.spnBHA.InnerText = " Warning!";
                    this.lblBHASuccess.Text = "Invalid Motor Size value. Please provide a valid value for BHA Motor Size";
                    this.bhaEnclosure.Visible = true;
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Service Order: " + jName + " Well_Name: " + wName + " Section: " + secName + " Run: " + rName + " Error Adding BHA ");
                }
                else if (!Decimal.TryParse(DClrSize, out bhaDSC))
                {
                    this.bhaEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iBHAMessage.Attributes["class"] = "icon fa fa-warning";
                    this.spnBHA.InnerText = " Warning!";
                    this.lblBHASuccess.Text = "Invalid Drill-Collar Size value. Please provide a valid value for BHA Drill-Collar Size.";
                    this.bhaEnclosure.Visible = true;
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Service Order: " + jName + " Well_Name: " + wName + " Section: " + secName + " Run: " + rName + " Error Adding BHA ");
                }
                else if (!Decimal.TryParse(Length, out bhaMDBLen))
                {
                    this.bhaEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iBHAMessage.Attributes["class"] = "icon fa fa-warning";
                    this.spnBHA.InnerText = " Warning!";
                    this.lblBHASuccess.Text = "Invalid Motor plus Drill-bit Length. Please provide a valid value for BHA Motor plus Drill-bit length.";
                    this.bhaEnclosure.Visible = true;
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Service Order: " + jName + " Well_Name: " + wName + " Section: " + secName + " Run: " + rName + " Error Adding BHA ");
                }
                else if (!Decimal.TryParse(Above, out bhaAbv))
                {
                    this.bhaEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iBHAMessage.Attributes["class"] = "icon fa fa-warning";
                    this.spnBHA.InnerText = " Warning!";
                    this.lblBHASuccess.Text = "Invalid Space-Above value. Please provide a valid value for Non-Mag. Space-Above value";
                    this.bhaEnclosure.Visible = true;
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Service Order: " + jName + " Well_Name: " + wName + " Section: " + secName + " Run: " + rName + " Error Adding BHA ");
                }
                else if (!Decimal.TryParse(Below, out bhaBlw))
                {
                    this.bhaEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iBHAMessage.Attributes["class"] = "icon fa fa-warning";
                    this.spnBHA.InnerText = " Warning!";
                    this.lblBHASuccess.Text = "Invalid Space-Below value. Please provide a valid value for Non-Mag. Space-Below value";
                    this.bhaEnclosure.Visible = true;
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Service Order: " + jName + " Well_Name: " + wName + " Section: " + secName + " Run: " + rName + " Error Adding BHA ");
                }
                else
                {
                    bhaMSz = Convert.ToDecimal(MtrSize);
                    bhaDSC = Convert.ToDecimal(DClrSize);
                    bhaMDBLen = Convert.ToDecimal(Length);
                    bhaAbv = Convert.ToDecimal(Above);
                    bhaBlw = Convert.ToDecimal(Below);
                    result = Convert.ToInt32(OPR.addBHASignature(runID, secID, welID, padID, ClientID, oprID, bhaName, bhaMSz, bhaMSzU, bhaDSC, bhaDSCU, bhaMDBLen, bhaMDBLenU, bhaAbv, bhaAbvU, bhaBlw, bhaBlwU, bhaStat, GetClientDBString));
                    
                    if (result.Equals(1))
                    {
                        this.bhaEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iBHAMessage.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnBHA.InnerText = " Success!";
                        this.lblBHASuccess.Text = "Successfully added Bottom-Hole-Assembly (BHA) signature";
                        this.bhaEnclosure.Visible = true;
                        logger.Info(" User: " + LoginName + " Realm: " + domain + " Service Order: " + jName + " Well Name: " + wName + " Section: " + secName + " Run: " + rName + " Added BHA ");
                    }
                    else
                    {
                        this.bhaEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iBHAMessage.Attributes["class"] = "icon fa fa-warning";
                        this.spnBHA.InnerText = " Warning!";
                        this.lblBHASuccess.Text = "Missing values. Please provide all values and measurements unit to add BHA to SMARTs.";
                        this.bhaEnclosure.Visible = true;
                        logger.Error(" User: " + LoginName + " Realm: " + domain + " Service Order: " + jName + " Well_Name: " + wName + " Section: " + secName + " Run: " + rName + " Error Adding BHA (Null Values)");
                    }
                }
                this.btnBHAAddNew.CssClass = "btn btn-default text-center";
                this.btnBHAAddNew.Enabled = false;
                this.btnBHAAddNewCancel.CssClass = "btn btn-default text-center";
                this.btnBHAAddNewCancel.Enabled = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnBHAAddNewCancel_Click(object sender, EventArgs e)
        {
            try
            {                
                this.txtBHAName.Text = "";
                this.txtAddBHAMSize.Text = "";
                this.txtAddBHAMSize.Enabled = false;
                this.txtBHADSC.Text = "";
                this.txtBHADSC.Enabled = false;
                this.txtBHAMDBLen.Text = "";
                this.txtBHAMDBLen.Enabled = false;
                this.txtBHAAbv.Text = "";
                this.txtBHAAbv.Enabled = false;
                this.txtBHABlw.Text = "";
                this.txtBHABlw.Enabled = false;this.bhaEnclosure.Visible = false;
                this.btnBHAAddNew.CssClass = "btn btn-default text-center";
                this.btnBHAAddNew.Enabled = false;
                this.btnBHAAddNewCancel.CssClass = "btn btn-default text-center";
                this.btnBHAAddNewCancel.Enabled = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnBHAAddDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnBHAAddNewCancel_Click(null, null);
                this.divRunAdd.Visible = false;
                this.divRunList.Visible = true;
                this.divRunTieIn.Visible = false;
                this.divBHA.Visible = false;
                Int32 optrID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue), wpdID = Convert.ToInt32(this.gvwWellPads.SelectedValue), wlID = Convert.ToInt32(this.gvwWell.SelectedValue), scID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                System.Data.DataTable rnTable = OPR.getRunForWellSectionTable(optrID, wpdID, wlID, scID, GetClientDBString);
                this.gvwRunList.DataSource = rnTable;
                this.gvwRunList.SelectedIndex = -1;
                this.gvwRunList.DataBind();
                this.gvwRunList.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbMSUIn_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbMSUMm.Checked = false;
                this.txtAddBHAMSize.Text = "";
                this.txtAddBHAMSize.Enabled = true;
                this.txtAddBHAMSize.Focus();
                this.divClrSize.Attributes["class"] = "form-control";
                this.rdbCSUIn.Checked = false;
                this.rdbCSUIn.Enabled = true;
                this.rdbCSUMm.Checked = false;
                this.rdbCSUMm.Enabled = true;
                this.btnBHAAddNewCancel.Enabled = true;
                this.btnBHAAddNewCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbMSUMm_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbMSUIn.Checked = false;
                this.txtAddBHAMSize.Text = "";
                this.txtAddBHAMSize.Enabled = true;
                this.txtAddBHAMSize.Focus();
                this.divClrSize.Attributes["class"] = "form-control";
                this.rdbCSUIn.Checked = false;
                this.rdbCSUIn.Enabled = true;
                this.rdbCSUMm.Checked = false;
                this.rdbCSUMm.Enabled = true;
                this.btnBHAAddNewCancel.Enabled = true;
                this.btnBHAAddNewCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbCSUIn_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbCSUMm.Checked = false;
                this.txtBHADSC.Text = "";
                this.txtBHADSC.Enabled = true;
                this.txtBHADSC.Focus();
                this.divMDB.Attributes["class"] = "form-control";
                this.rdbMDBInch.Checked = false;
                this.rdbMDBInch.Enabled = true;
                this.rdbMDBFeet.Checked = false;
                this.rdbMDBFeet.Enabled = true;
                this.rdbMDBMeter.Checked = false;
                this.rdbMDBMeter.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbCSUMm_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbCSUIn.Checked = false;
                this.txtBHADSC.Text = "";
                this.txtBHADSC.Enabled = true;
                this.txtBHADSC.Focus();
                this.divMDB.Attributes["class"] = "form-control";
                this.rdbMDBInch.Checked = false;
                this.rdbMDBInch.Enabled = true;
                this.rdbMDBFeet.Checked = false;
                this.rdbMDBFeet.Enabled = true;
                this.rdbMDBMeter.Checked = false;
                this.rdbMDBMeter.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbMDBInch_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbMDBFeet.Checked = false;
                this.rdbMDBMeter.Checked = false;
                this.txtBHAMDBLen.Text = "";
                this.txtBHAMDBLen.Enabled = true;
                this.txtBHAMDBLen.Focus();
                this.divAbv.Attributes["class"] = "form-control";
                this.rdbAbvFt.Checked = false;
                this.rdbAbvFt.Enabled = true;
                this.rdbAbvIn.Checked = false;
                this.rdbAbvIn.Enabled = true;
                this.rdbAbvMt.Checked = false;
                this.rdbAbvMt.Enabled = true;

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbMDBFeet_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbMDBInch.Checked = false;
                this.rdbMDBMeter.Checked = false;
                this.txtBHAMDBLen.Text = "";
                this.txtBHAMDBLen.Enabled = true;
                this.txtBHAMDBLen.Focus();
                this.divAbv.Attributes["class"] = "form-control";
                this.rdbAbvFt.Checked = false;
                this.rdbAbvFt.Enabled = true;
                this.rdbAbvIn.Checked = false;
                this.rdbAbvIn.Enabled = true;
                this.rdbAbvMt.Checked = false;
                this.rdbAbvMt.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbMDBMeter_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbMDBFeet.Checked = false;
                this.rdbMDBInch.Checked = false;
                this.txtBHAMDBLen.Text = "";
                this.txtBHAMDBLen.Enabled = true;
                this.txtBHAMDBLen.Focus();
                this.divAbv.Attributes["class"] = "form-control";
                this.rdbAbvFt.Checked = false;
                this.rdbAbvFt.Enabled = true;
                this.rdbAbvIn.Checked = false;
                this.rdbAbvIn.Enabled = true;
                this.rdbAbvMt.Checked = false;
                this.rdbAbvMt.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbAbvIn_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbAbvFt.Checked = false;
                this.rdbAbvMt.Checked = false;
                this.txtBHAAbv.Text = "";                
                this.txtBHAAbv.Enabled = true;
                this.txtBHAAbv.Focus();
                this.divBlw.Attributes["class"] = "form-control";
                this.rdbBlwFt.Checked = false;
                this.rdbBlwFt.Enabled = true;
                this.rdbBlwIn.Checked = false;
                this.rdbBlwIn.Enabled = true;
                this.rdbBlwMt.Checked = false;
                this.rdbBlwMt.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbAbvFt_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbAbvIn.Checked = false;
                this.rdbAbvMt.Checked = false;
                this.txtBHAAbv.Text = "";
                this.txtBHAAbv.Enabled = true;
                this.txtBHAAbv.Focus();
                this.divBlw.Attributes["class"] = "form-control";
                this.rdbBlwFt.Checked = false;
                this.rdbBlwFt.Enabled = true;
                this.rdbBlwIn.Checked = false;
                this.rdbBlwIn.Enabled = true;
                this.rdbBlwMt.Checked = false;
                this.rdbBlwMt.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbAbvMt_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbAbvIn.Checked = false;
                this.rdbAbvFt.Checked = false;
                this.txtBHAAbv.Text = "";
                this.txtBHAAbv.Enabled = true;
                this.txtBHAAbv.Focus();
                this.divBlw.Attributes["class"] = "form-control";
                this.rdbBlwFt.Checked = false;
                this.rdbBlwFt.Enabled = true;
                this.rdbBlwIn.Checked = false;
                this.rdbBlwIn.Enabled = true;
                this.rdbBlwMt.Checked = false;
                this.rdbBlwMt.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbBlwIn_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbBlwFt.Checked = false;
                this.rdbBlwMt.Checked = false;
                this.txtBHABlw.Text = "";
                this.txtBHABlw.Enabled = true;
                this.txtBHABlw.Focus();
                this.btnBHAAddNew.Enabled = true;
                this.btnBHAAddNew.CssClass = "btn btn-success text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbBlwFt_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbBlwIn.Checked = false;
                this.rdbBlwMt.Checked = false;
                this.txtBHABlw.Text = "";
                this.txtBHABlw.Enabled = true;
                this.txtBHABlw.Focus();
                this.btnBHAAddNew.Enabled = true;
                this.btnBHAAddNew.CssClass = "btn btn-success text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbBlwMt_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbBlwFt.Checked = false;
                this.rdbBlwIn.Checked = false;
                this.txtBHABlw.Text = "";
                this.txtBHABlw.Enabled = true;
                this.txtBHABlw.Focus();
                this.btnBHAAddNew.Enabled = true;
                this.btnBHAAddNew.CssClass = "btn btn-success text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbRTIMDFt_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbRTIMDM.Checked = false;
                this.txtRTIMD.Text = "";
                this.txtRTIMD.Enabled = true;
                this.txtRTIMD.Focus();
                this.txtRTIInc.Text = "";
                this.txtRTIInc.Enabled = true;
                this.txtRTIAzm.Text = "";
                this.txtRTIAzm.Enabled = true;
                this.divRTITVD.Attributes["class"] = "form-control";
                this.rdbRTITVDFt.Checked = false;
                this.rdbRTITVDFt.Enabled = true;
                this.rdbRTITVDM.Checked = false;
                this.rdbRTITVDM.Enabled = true;
                this.rtiEnclosure.Visible = false;
                this.lblRTI.Text = "";
                this.btnRTIClear.Enabled = true;
                this.btnRTIClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbRTIMDM_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbRTIMDFt.Checked = false;
                this.txtRTIMD.Text = "";
                this.txtRTIMD.Enabled = true;
                this.txtRTIMD.Focus();
                this.txtRTIInc.Text = "";
                this.txtRTIInc.Enabled = true;
                this.txtRTIAzm.Text = "";
                this.txtRTIAzm.Enabled = true;
                this.divRTITVD.Attributes["class"] = "form-control";
                this.rdbRTITVDFt.Checked = false;
                this.rdbRTITVDFt.Enabled = true;
                this.rdbRTITVDM.Checked = false;
                this.rdbRTITVDM.Enabled = true;
                this.rtiEnclosure.Visible = false;
                this.lblRTI.Text = "";
                this.btnRTIClear.Enabled = true;
                this.btnRTIClear.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbRTITVDFt_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbRTITVDM.Checked = false;
                this.txtRTITVD.Text = "";
                this.txtRTITVD.Enabled = true;
                this.txtRTITVD.Focus();
                this.divRTINorth.Attributes["class"] = "form-control";
                this.rdbRTINFt.Checked = false;
                this.rdbRTINFt.Enabled = true;
                this.rdbRTINM.Checked = false;
                this.rdbRTINM.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbRTITVDM_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbRTITVDFt.Checked = false;
                this.txtRTITVD.Text = "";
                this.txtRTITVD.Enabled = true;
                this.txtRTITVD.Focus();
                this.divRTINorth.Attributes["class"] = "form-control";
                this.rdbRTINFt.Checked = false;
                this.rdbRTINFt.Enabled = true;
                this.rdbRTINM.Checked = false;
                this.rdbRTINM.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbRTINFt_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbRTINM.Checked = false;
                this.txtRTINorth.Text = "";
                this.txtRTINorth.Enabled = true;
                this.txtRTINorth.Focus();
                this.divRTIEast.Attributes["class"] = "form-control";
                this.rdbRTIEFt.Checked = false;
                this.rdbRTIEFt.Enabled = true;
                this.rdbRTIEM.Checked = false;
                this.rdbRTIEM.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbRTINM_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbRTINFt.Checked = false;
                this.txtRTINorth.Text = "";
                this.txtRTINorth.Enabled = true;
                this.txtRTINorth.Focus();
                this.divRTIEast.Attributes["class"] = "form-control";
                this.rdbRTIEFt.Checked = false;
                this.rdbRTIEFt.Enabled = true;
                this.rdbRTIEM.Checked = false;
                this.rdbRTIEM.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbRTIEFt_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbRTIEM.Checked = false;
                this.txtRTIEast.Text = "";
                this.txtRTIEast.Enabled = true;
                this.txtRTIEast.Focus();
                this.divRTIVS.Attributes["class"] = "form-control";
                this.rdbRTIVSFt.Checked = false;
                this.rdbRTIVSFt.Enabled = true;
                this.rdbRTIVSM.Checked = false;
                this.rdbRTIVSM.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbRTIEM_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbRTIEFt.Checked = false;
                this.txtRTIEast.Text = "";
                this.txtRTIEast.Enabled = true;
                this.txtRTIEast.Focus();
                this.divRTIVS.Attributes["class"] = "form-control";
                this.rdbRTIVSFt.Checked = false;
                this.rdbRTIVSFt.Enabled = true;
                this.rdbRTIVSM.Checked = false;
                this.rdbRTIVSM.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbRTIVSFt_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbRTIVSM.Checked = false;
                this.txtRTIVS.Text = "";
                this.txtRTIVS.Enabled = true;
                this.txtRTIVS.Focus();
                this.divRTISS.Attributes["class"] = "form-control";
                this.rdbRTISSFt.Checked = false;
                this.rdbRTISSFt.Enabled = true;
                this.rdbRTISSM.Checked = false;
                this.rdbRTISSM.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbRTIVSM_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbRTIVSFt.Checked = false;
                this.txtRTIVS.Text = "";
                this.txtRTIVS.Enabled = true;
                this.txtRTIVS.Focus();
                this.divRTISS.Attributes["class"] = "form-control";
                this.rdbRTISSFt.Checked = false;
                this.rdbRTISSFt.Enabled = true;
                this.rdbRTISSM.Checked = false;
                this.rdbRTISSM.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbRTISSFt_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbRTISSM.Checked = false;
                this.txtRTISubsea.Text = "";
                this.txtRTISubsea.Enabled = true;
                this.txtRTISubsea.Focus();
                this.btnRTIAdd.Enabled = true;
                this.btnRTIAdd.CssClass = "btn btn-success text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbRTISSM_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbRTISSFt.Checked = false;
                this.txtRTISubsea.Text = "";
                this.txtRTISubsea.Enabled = true;
                this.txtRTISubsea.Focus();
                this.btnRTIAdd.Enabled = true;
                this.btnRTIAdd.CssClass = "btn btn-success text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRTIAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, wellID = -99, wsID = -99, runID = -99, oprID = -99, pdID = -99;
                Decimal vMD = -99.00M, vInc = -99.000000000000M, vAzm = -99.000000000000M, vTVD = -99.000000000000M, vNorth = -99.000000000000M, vEast = -99.000000000000M, vVS = -99.000000000000M, vSubsea = -99.000000000000M;
                Int32 mdU = -99, tvdU = -99, nU = -99, eU = -99, vsU = -99, ssU = -99;
                String MDVal = String.Empty, IncVal = String.Empty, AzmVal = String.Empty, TVDVal = String.Empty, NVal = String.Empty, EVal = String.Empty, VSVal = String.Empty, SubSeaVal = String.Empty;
                String wlName = String.Empty, scName = String.Empty, rnName = String.Empty;
                GridViewRow wgRow = this.gvwWell.SelectedRow;
                wlName = Convert.ToString(Server.HtmlDecode(wgRow.Cells[2].Text));
                GridViewRow sgRow = this.gvwSectionList.SelectedRow;
                oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                scName = Convert.ToString(Server.HtmlDecode(sgRow.Cells[2].Text));
                runID = Convert.ToInt32(this.hdnRunID.Value);
                rnName = AST.getRunName(runID);
                wellID = Convert.ToInt32(this.gvwWell.SelectedValue);
                pdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                wsID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                MDVal = Convert.ToString(this.txtRTIMD.Text); if (rdbRTIMDFt.Checked) { mdU = 4; } else { mdU = 5; }
                IncVal = Convert.ToString(this.txtRTIInc.Text);
                AzmVal = Convert.ToString(this.txtRTIAzm.Text);
                TVDVal = Convert.ToString(this.txtRTITVD.Text); if (rdbRTITVDFt.Checked) { tvdU = 4; } else { tvdU = 5; }
                NVal = Convert.ToString(this.txtRTINorth.Text); if (rdbRTINFt.Checked) { nU = 4; } else { nU = 5; }
                EVal = Convert.ToString(this.txtRTIEast.Text); if (rdbRTIEFt.Checked) { eU = 4; } else { eU = 5; }
                VSVal = Convert.ToString(this.txtRTIVS.Text); if (rdbRTIVSFt.Checked) { vsU = 4; } else { vsU = 5; }
                SubSeaVal = Convert.ToString(this.txtRTISubsea.Text); if (rdbRTISSFt.Checked) { ssU = 4; } else { ssU = 5; }
                if (String.IsNullOrEmpty(MDVal) || String.IsNullOrEmpty(IncVal) || String.IsNullOrEmpty(AzmVal) || String.IsNullOrEmpty(TVDVal) || String.IsNullOrEmpty(NVal) || String.IsNullOrEmpty(EVal) || String.IsNullOrEmpty(VSVal) || String.IsNullOrEmpty(SubSeaVal))
                {
                    this.rtiEnclosure.Visible = true;
                    this.rtiEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iRTI.Attributes["class"] = "icon fa fa-warning";
                    this.spnRTI.InnerText = " Warning!";
                    this.lblRTI.Text = "Missing values detected. Please provide all values";
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wlName + " Section: " + scName + " Run: " + rnName + " Error Null Tie-In ");
                }
                else if(String.IsNullOrEmpty(EVal) || EVal.Equals("0.00"))
                {
                    this.rtiEnclosure.Visible = true;
                    this.rtiEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iRTI.Attributes["class"] = "icon fa fa-warning";
                    this.spnRTI.InnerText = " Warning!";
                    this.lblRTI.Text = "Tie-In East can't have 0.00 value. Please provide valid numbers for Tie-In East";
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wlName + " Section: " + scName + " Run: " + rnName + " Error Null Tie-In East ");
                }
                    else if(!Decimal.TryParse(EVal, out vEast))
                {
                    this.rtiEnclosure.Visible = true;
                    this.rtiEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iRTI.Attributes["class"] = "icon fa fa-warning";
                    this.spnRTI.InnerText = " Warning!";
                    this.lblRTI.Text = "Invalid Tie-In East value. Please provide valid numbers for Tie-In East";
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wlName + " Section: " + scName + " Run: " + rnName + " Error Null Tie-In East ");
                }
                else if (String.IsNullOrEmpty(NVal) || NVal.Equals("0.00"))
                {
                    this.rtiEnclosure.Visible = true;
                    this.rtiEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iRTI.Attributes["class"] = "icon fa fa-warning";
                    this.spnRTI.InnerText = " Warning!";
                    this.lblRTI.Text = "Tie-In North can't have 0.00 value. Please provide valid numbers for Tie-In North";
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wlName + " Section: " + scName + " Run: " + rnName + " Error Null Tie-In North");
                }
                else if (!Decimal.TryParse(NVal, out vNorth))
                {
                    this.rtiEnclosure.Visible = true;
                    this.rtiEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iRTI.Attributes["class"] = "icon fa fa-warning";
                    this.spnRTI.InnerText = " Warning!";
                    this.lblRTI.Text = "Invalid Tie-In North value. Please provide valid numbers for Tie-In North";
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wlName + " Section: " + scName + " Run: " + rnName + " Error Null Tie-In North");
                }
                else if (String.IsNullOrEmpty(SubSeaVal) || SubSeaVal.Equals("0.00"))
                {
                    this.rtiEnclosure.Visible = true;
                    this.rtiEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iRTI.Attributes["class"] = "icon fa fa-warning";
                    this.spnRTI.InnerText = " Warning!";
                    this.lblRTI.Text = "Sub-sea Elevation can't have 0.00 value. Please provide valid numbers for Sub-sea Elevation";
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wlName + " Section: " + scName + " Run: " + rnName + " Error Null Sub-sea Elevation ");
                }
                else if (!Decimal.TryParse(SubSeaVal, out vSubsea))
                {
                    this.rtiEnclosure.Visible = true;
                    this.rtiEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iRTI.Attributes["class"] = "icon fa fa-warning";
                    this.spnRTI.InnerText = " Warning!";
                    this.lblRTI.Text = "Invalid Sub-sea value. Please provide valid numbers for Sub-sea value";
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wlName + " Section: " + scName + " Run: " + rnName + " Error Invalid Sub-sea elevation ");
                }
                else
                {
                    vMD = Convert.ToDecimal(MDVal);
                    vInc = Convert.ToDecimal(IncVal);
                    vAzm = Convert.ToDecimal(AzmVal);
                    vTVD = Convert.ToDecimal(TVDVal);
                    vNorth = Convert.ToDecimal(NVal);
                    vEast = Convert.ToDecimal(EVal);
                    vVS = Convert.ToDecimal(VSVal);
                    vSubsea = Convert.ToDecimal(SubSeaVal);
                    iResult = WPN.insertRunTieInSurvey(runID, wsID, wellID, pdID, ClientID, oprID, vMD, mdU, vInc, vAzm, vTVD, tvdU, vNorth, nU, vEast, eU, vVS, vsU, vSubsea, ssU, username, domain, GetClientDBString);
                    if (iResult.Equals(1))
                    {
                        this.rtiEnclosure.Visible = true;
                        this.rtiEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iRTI.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnRTI.InnerText = " Success!";
                        this.lblRTI.Text = "Successfully inserted Tie-In Survey";
                        logger.Info(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wlName + " Section: " + scName + " Run: " + rnName + " Added Null Tie-In ");
                    }
                    else
                    {
                        this.rtiEnclosure.Visible = true;
                        this.rtiEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iRTI.Attributes["class"] = "icon fa fa-warning";
                        this.spnRTI.InnerText = " Warning!";
                        this.lblRTI.Text = "Unable to insert Tie-In Survey";
                        logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wlName + " Section: " + scName + " Run: " + rnName + " Error Adding Tie-In ");
                    }
                }
                this.btnRTIAdd.CssClass = "btn btn-default text-center";
                this.btnRTIAdd.Enabled = false;
                this.btnRTIClear.CssClass = "btn btn-default text-center";
                this.btnRTIClear.Enabled = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRTIClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.rdbRTIMDFt.Checked = false;
                this.rdbRTIMDM.Checked = false;
                this.txtRTIMD.Text = "";
                this.txtRTIMD.Enabled = false;
                this.txtRTIInc.Text = "";
                this.txtRTIInc.Enabled = false;
                this.txtRTIAzm.Text = "";
                this.txtRTIAzm.Enabled = false;
                this.divRTITVD.Attributes["class"] = "form-control bg-gray-light";
                this.rdbRTITVDFt.Checked = false;
                this.rdbRTITVDFt.Enabled = false;
                this.rdbRTITVDM.Checked = false;
                this.rdbRTITVDM.Enabled = false;
                this.txtRTITVD.Text = "";
                this.txtRTITVD.Enabled = false;
                this.divRTINorth.Attributes["class"] = "form-control bg-gray-light";
                this.rdbRTINFt.Checked = false;
                this.rdbRTINFt.Enabled = false;
                this.rdbRTINM.Checked = false;
                this.rdbRTINM.Enabled = false;
                this.txtRTINorth.Text = "";
                this.txtRTINorth.Enabled = false;
                this.divRTIEast.Attributes["class"] = "form-control bg-gray-light";
                this.rdbRTIEFt.Checked = false;
                this.rdbRTIEFt.Enabled = false;
                this.rdbRTIEM.Checked = false;
                this.rdbRTIEM.Enabled = false;
                this.txtRTIEast.Text = "";
                this.txtRTIEast.Enabled = false;
                this.divRTIVS.Attributes["class"] = "form-control bg-gray-light";
                this.rdbRTIVSFt.Checked = false;
                this.rdbRTIVSFt.Enabled = false;
                this.rdbRTIVSM.Checked = false;
                this.rdbRTIVSM.Enabled = false;
                this.txtRTIVS.Text = "";
                this.txtRTIVS.Enabled = false;
                this.divRTISS.Attributes["class"] = "form-control bg-gray-light";
                this.rdbRTISSFt.Checked = false;
                this.rdbRTISSFt.Enabled = false;
                this.rdbRTISSM.Checked = false;
                this.rdbRTISSM.Enabled = false;
                this.txtRTISubsea.Text = "";
                this.txtRTISubsea.Enabled = false;
                this.rtiEnclosure.Visible = false;
                this.lblRTI.Text = "";
                this.btnRTIClear.CssClass = "btn btn-default text-center";
                this.btnRTIClear.Enabled = false;
                this.btnRTIAdd.CssClass = "btn btn-default text-center";
                this.btnRTIAdd.Enabled = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnTRIDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnRTIClear_Click(null, null);
                this.divRunList.Visible = true;
                this.divRunAdd.Visible = false;
                this.divRunTieIn.Visible = false;
                this.divBHA.Visible = false;
                Int32 optrID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue), wpdID = Convert.ToInt32(this.gvwWellPads.SelectedValue), wlID = Convert.ToInt32(this.gvwWell.SelectedValue), scID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                System.Data.DataTable rnTable = OPR.getRunForWellSectionTable(optrID, wpdID, wlID, scID, GetClientDBString);
                this.gvwRunList.DataSource = rnTable;
                this.gvwRunList.SelectedIndex = -1;
                this.gvwRunList.DataBind();
                this.gvwRunList.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbMDUnitFT_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbMDUnitM.Checked = false;
                this.txtTIMD.Text = "";
                this.txtTIMD.Enabled = true;
                this.txtTIMD.Focus();
                this.divVD.Attributes["class"] = "form-control";
                this.rdbTVDFt.Checked = false;
                this.rdbTVDFt.Enabled = true;
                this.rdbTVDM.Checked = false;
                this.rdbTVDM.Enabled = true;
                this.btnTIReset.Enabled = true;
                this.btnTIReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbMDUnitM_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbMDUnitFT.Checked = false;
                this.txtTIMD.Text = "";
                this.txtTIMD.Enabled = true;
                this.txtTIMD.Focus();
                this.divVD.Attributes["class"] = "form-control";
                this.rdbTVDFt.Checked = false;
                this.rdbTVDFt.Enabled = true;
                this.rdbTVDM.Checked = false;
                this.rdbTVDM.Enabled = true;
                this.btnTIReset.Enabled = true;
                this.btnTIReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbTVDFt_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbTVDM.Checked = false;
                this.txtTITVD.Text = "";
                this.txtTITVD.Enabled = true;
                this.txtTITVD.Focus();
                this.divNOff.Attributes["class"] = "form-control";
                this.rdbNOFt.Checked = false;
                this.rdbNOFt.Enabled = true;
                this.rdbNOM.Checked = false;
                this.rdbNOM.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbTVDM_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbTVDFt.Checked = false;
                this.txtTITVD.Text = "";
                this.txtTITVD.Enabled = true;
                this.txtTITVD.Focus();
                this.divNOff.Attributes["class"] = "form-control";
                this.rdbNOFt.Checked = false;
                this.rdbNOFt.Enabled = true;
                this.rdbNOM.Checked = false;
                this.rdbNOM.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbNOFt_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbNOM.Checked = false;
                this.txtTINorth.Text = "";
                this.txtTINorth.Enabled = true;
                this.txtTINorth.Focus();
                this.divEOff.Attributes["class"] = "form-control";
                this.rdbEOFt.Checked = false;
                this.rdbEOFt.Enabled = true;
                this.rdbEOM.Checked = false;
                this.rdbEOM.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbNOM_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbNOFt.Checked = false;
                this.txtTINorth.Text = "";
                this.txtTINorth.Enabled = true;
                this.txtTINorth.Focus();
                this.divEOff.Attributes["class"] = "form-control";
                this.rdbEOFt.Checked = false;
                this.rdbEOFt.Enabled = true;
                this.rdbEOM.Checked = false;
                this.rdbEOM.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbEOFt_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbEOM.Checked = false;
                this.txtTIEast.Text = "";
                this.txtTIEast.Enabled = true;
                this.txtTIEast.Focus();
                this.divVSec.Attributes["class"] = "form-control";
                this.rdbVSecFt.Checked = false;
                this.rdbVSecFt.Enabled = true;
                this.rdbVSMtr.Checked = false;
                this.rdbVSMtr.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbEOM_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbEOFt.Checked = false;
                this.txtTIEast.Text = "";
                this.txtTIEast.Enabled = true;
                this.txtTIEast.Focus();
                this.divVSec.Attributes["class"] = "form-control";
                this.rdbVSecFt.Checked = false;
                this.rdbVSecFt.Enabled = true;
                this.rdbVSMtr.Checked = false;
                this.rdbVSMtr.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbVSecFt_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbVSMtr.Checked = false;
                this.txtVS.Text = "";
                this.txtVS.Enabled = true;
                this.txtVS.Focus();
                this.divDLRate.Attributes["class"] = "form-control";
                this.rdbDLSFt.Checked = false;
                this.rdbDLSFt.Enabled = true;
                this.rdbDLSM.Checked = false;
                this.rdbDLSM.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbVSMtr_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbVSecFt.Checked = false;
                this.txtVS.Text = "";
                this.txtVS.Enabled = true;
                this.txtVS.Focus();
                this.divDLRate.Attributes["class"] = "form-control";
                this.rdbDLSFt.Checked = false;
                this.rdbDLSFt.Enabled = true;
                this.rdbDLSM.Checked = false;
                this.rdbDLSM.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbSSDFt_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbSSDM.Checked = false;
                this.txtSS.Text = "";
                this.txtSS.Enabled = true;
                this.txtSS.Focus();
                this.btnTINext.CssClass = "btn btn-success text-center";
                this.btnTINext.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbSSDM_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbSSDFt.Checked = false;
                this.txtSS.Text = "";
                this.txtSS.Enabled = true;
                this.txtSS.Focus();
                this.btnTINext.CssClass = "btn btn-success text-center";
                this.btnTINext.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void resetWellPlanRadios(object sender, EventArgs e)
        {
            try
            {
                this.divVD.Attributes["class"] = "form-control bg-gray-light";
                this.rdbTVDFt.Checked=false;
                this.rdbTVDFt.Enabled=false;
                this.rdbTVDM.Checked = false;
                this.rdbTVDM.Enabled = false;
                this.divNOff.Attributes["class"] = "form-control bg-gray-light";
                this.rdbNOFt.Checked = false;
                this.rdbNOFt.Enabled = false;
                this.rdbNOM.Checked = false;
                this.rdbNOM.Enabled = false;
                this.divEOff.Attributes["class"] = "form-control bg-gray-light";
                this.rdbEOFt.Checked = false;
                this.rdbEOFt.Enabled = false;
                this.rdbEOM.Checked = false;
                this.rdbEOM.Enabled = false;
                this.divVSec.Attributes["class"] = "form-control bg-gray-light";
                this.rdbVSecFt.Checked = false;
                this.rdbVSecFt.Enabled = false;
                this.rdbVSMtr.Checked = false;
                this.rdbVSMtr.Enabled = false;
                this.divDLRate.Attributes["class"] = "form-control bg-gray-light";
                this.rdbDLSFt.Checked = false;
                this.rdbDLSFt.Enabled = false;
                this.rdbDLSM.Checked = false;
                this.rdbDLSM.Enabled = false;
                this.divSSElevUnit.Attributes["class"] = "form-control bg-gray-light";
                this.rdbSSDFt.Checked = false;
                this.rdbSSDFt.Enabled = false;
                this.rdbSSDM.Checked = false;
                this.rdbSSDM.Enabled = false;
                this.btnWPProcess.CssClass = "btn btn-default text-center";
                this.btnWPProcess.Enabled = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWPInfoNext_Click(object sender, EventArgs e)
        {
            try
            {
                String depthREVal = String.Empty, vsAzimVal = String.Empty, northingValue = String.Empty, eastingValue = String.Empty, wellName = String.Empty;
                Decimal refElev = -99.00M, azimVal = -99.00M, nVal = -99.00M, eVal = -99.00M;
                Int32 wellID = Convert.ToInt32(this.gvwWell.SelectedValue);
                wellName = AST.getWellName(wellID, GetClientDBString);
                depthREVal = Convert.ToString(this.txtWPDRElevation.Text);
                vsAzimVal = Convert.ToString(this.txtWPVSAzimuth.Text);
                northingValue = Convert.ToString(this.txtWPNorthing.Text);
                eastingValue = Convert.ToString(this.txtWPEasting.Text);
                if (String.IsNullOrEmpty(depthREVal) || String.IsNullOrEmpty(vsAzimVal) || String.IsNullOrEmpty(northingValue) || String.IsNullOrEmpty(eastingValue))
                {
                    this.planEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iPlan.Attributes["class"] = "icon fa fa-warning";
                    this.spnPlan.InnerText = " Warning!";
                    this.lblPlanSuccess.Text = "Missing Values. Please provide all values";
                    this.planEnclosure.Visible = true;
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wellName + " Added Well/Lateral Plan (Null Values) ");
                }
                else if ( !Decimal.TryParse(depthREVal, out refElev) || !Decimal.TryParse(vsAzimVal, out azimVal) || !Decimal.TryParse(northingValue, out nVal) || !Decimal.TryParse(eastingValue, out eVal) )
                {
                    this.planEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iPlan.Attributes["class"] = "icon fa fa-warning";
                    this.spnPlan.InnerText = " Warning!";
                    this.lblPlanSuccess.Text = "Invalid Values. Please provide correct values";
                    this.planEnclosure.Visible = true;
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wellName + " Added Well/Lateral Plan (Not Decimal Values) ");
                }
                else
                {
                    this.divMetaInfo.Visible = false;
                    this.divPlanTieIn.Visible = true;
                    this.divMD.Attributes["class"] = "form-control";
                    this.rdbMDUnitFT.Checked = false;
                    this.rdbMDUnitFT.Enabled = true;
                    this.rdbMDUnitFT.Focus();
                    this.rdbMDUnitM.Checked = false;
                    this.rdbMDUnitM.Enabled = true;
                    this.liMI.Attributes["class"] = "stepper-done";
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnWPInfoReset_Click(object sender, EventArgs e)
        {
            try
            {
                this.rdbGE.Checked = false;
                this.rdbMSL.Checked = false;
                this.divDR.Attributes["class"] = "form-control bg-gray-light";
                this.rdbGElev.Checked = false;
                this.rdbGElev.Enabled = false;
                this.rdbKB.Checked = false;
                this.rdbKB.Enabled = false;
                this.rdbRF.Checked = false;
                this.rdbRF.Enabled = false;
                this.divElevUnit.Attributes["class"] = "form-control bg-gray-light";
                this.rdbElFeet.Checked = false;
                this.rdbElFeet.Enabled = false;
                this.rdbElMeter.Checked = false;
                this.rdbElMeter.Enabled = false;
                this.txtWPDRElevation.Text = "";
                this.txtWPDRElevation.Enabled = false;
                this.divVSUnit.Attributes["class"] = "form-control bg-gray-light";
                this.rdbVSFeet.Checked = false;
                this.rdbVSFeet.Enabled = false;
                this.rdbVSMeter.Checked = false;
                this.rdbVSMeter.Enabled = false;
                this.txtWPVSAzimuth.Text = "";
                this.txtWPVSAzimuth.Enabled = false;
                this.divN.Attributes["class"] = "form-control bg-gray-light";
                this.rdbNFeet.Checked = false;
                this.rdbNFeet.Enabled = false;
                this.rdbNMeter.Checked = false;
                this.rdbNMeter.Enabled = false;
                this.txtWPNorthing.Text = "";
                this.txtWPNorthing.Enabled = false;
                this.divE.Attributes["class"] = "form-control bg-gray-light";
                this.rdbEFeet.Checked = false;
                this.rdbEFeet.Enabled = false;
                this.rdbEMeter.Checked = false;
                this.rdbEMeter.Enabled = false;
                this.txtWPEasting.Text = "";
                this.txtWPEasting.Enabled = false; 
                this.btnWPInfoNext.Enabled = false;
                this.planEnclosure.Visible = false;
                this.lblPlanSuccess.Text = "";
                this.btnWPInfoNext.CssClass = "btn btn-default text-center";
                this.btnWPInfoReset.Enabled = false;
                this.btnWPInfoReset.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnTINext_Click(object sender, EventArgs e)
        {
            try
            {
                String mdVal = String.Empty, tvdVal = String.Empty, noVal = String.Empty, eoVal = String.Empty, vsVal = String.Empty, dlVal = String.Empty, ssVal = String.Empty;
                Decimal mdepth = -99.00M, tvd = -99.00M, noffset = -99.00M, eoffset = -99.00M, vsec = -99.00M, dogleg = -99.00M, subsea = -99.00M;
                mdVal = Convert.ToString(this.txtTIMD.Text);
                tvdVal = Convert.ToString(this.txtTITVD.Text);
                noVal = Convert.ToString(this.txtTINorth.Text);
                eoVal = Convert.ToString(this.txtTIEast.Text);
                vsVal = Convert.ToString(this.txtVS.Text);
                dlVal = Convert.ToString(this.txtDGLG.Text);
                ssVal = Convert.ToString(this.txtSS.Text);
                if (String.IsNullOrEmpty(mdVal) || String.IsNullOrEmpty(tvdVal) || String.IsNullOrEmpty(noVal) || String.IsNullOrEmpty(eoVal) || String.IsNullOrEmpty(vsVal) || String.IsNullOrEmpty(dlVal) || String.IsNullOrEmpty(ssVal))
                {
                    Int32 wellID = Convert.ToInt32(this.gvwWell.SelectedValue);
                    String wellName = AST.getWellName(wellID, GetClientDBString);
                    this.planEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iPlan.Attributes["class"] = "icon fa fa-warning";
                    this.spnPlan.InnerText = " Warning!";
                    this.lblPlanSuccess.Text = "Missing Values. Please provide all values";
                    this.planEnclosure.Visible = true;
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wellName + " Added Well/Lateral Tie-In (Null Values) ");
                }
                else if (!Decimal.TryParse(mdVal, out mdepth) || !Decimal.TryParse(tvdVal, out tvd) || !Decimal.TryParse(noVal, out noffset) || !Decimal.TryParse(eoVal, out eoffset) || !Decimal.TryParse(vsVal, out vsec) || !Decimal.TryParse(dlVal, out dogleg) || !Decimal.TryParse(ssVal, out subsea))
                {
                    Int32 wellID = Convert.ToInt32(this.gvwWell.SelectedValue);
                    String wellName = AST.getWellName(wellID, GetClientDBString);
                    this.planEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iPlan.Attributes["class"] = "icon fa fa-warning";
                    this.spnPlan.InnerText = " Warning!";
                    this.lblPlanSuccess.Text = "Invalid Values. Please provide correct values";
                    this.planEnclosure.Visible = true;
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wellName + " Added Well/Lateral Tie-In (Not Decimal Values) ");
                }
                else
                {
                    this.divPlanTieIn.Visible = false;
                    this.liTI.Attributes["class"] = "stepper-done";
                    this.divPlanFile.Visible = true;
                    this.btnWPUpload.Enabled = true;
                    this.btnWPUpload.CssClass = "btn btn-success text-center";
                    this.btnWPUpload.Visible = true;
                    this.btnWPAddClear.Enabled = true;
                    this.btnWPAddClear.CssClass = "btn btn-warning text-center";
                    this.btnWPAddClear.Visible = true;
                    this.btnWPDone.Visible = true;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnTIReset_Click(object sender, EventArgs e)
        {
            try
            {
                this.chkPlanAllZero.Checked = false;
                this.divMD.Attributes["class"] = "form-control";
                this.rdbMDUnitFT.Checked = false;
                this.rdbMDUnitFT.Focus();
                this.rdbMDUnitM.Checked = false;
                this.txtTIMD.Text = "";
                this.txtTIMD.Enabled = false;
                this.divVD.Attributes["class"] = "form-control bg-gray-light";
                this.rdbTVDFt.Checked = false;
                this.rdbTVDFt.Enabled = false;
                this.rdbTVDM.Checked = false;
                this.rdbTVDM.Enabled = false;
                this.txtTITVD.Text = "";
                this.txtTITVD.Enabled = false;
                this.divNOff.Attributes["class"] = "form-control bg-gray-light";
                this.rdbNOFt.Checked = false;
                this.rdbNOFt.Enabled = false;
                this.rdbNOM.Checked = false;
                this.rdbNOM.Checked = false;
                this.txtTINorth.Text = "";
                this.txtTINorth.Enabled = false;
                this.divEOff.Attributes["class"] = "form-control bg-gray-light";
                this.rdbEOFt.Checked = false;
                this.rdbEOFt.Enabled = false;
                this.rdbEOM.Checked = false;
                this.rdbEOM.Enabled = false;
                this.txtTIEast.Text = "";
                this.txtTIEast.Enabled = false;
                this.divVSec.Attributes["class"] = "form-control bg-gray-light";
                this.rdbVSecFt.Checked = false;
                this.rdbVSecFt.Enabled = false;
                this.rdbVSMtr.Checked = false;
                this.rdbVSMtr.Enabled = false;
                this.txtVS.Text = "";
                this.txtVS.Enabled = false;
                this.divDLRate.Attributes["class"] = "form-control bg-gray-light";
                this.rdbDLSFt.Checked = false;
                this.rdbDLSFt.Enabled = false;
                this.rdbDLSM.Checked = false;
                this.rdbDLSM.Enabled = false;
                this.txtDGLG.Text = "";
                this.txtDGLG.Enabled = false;
                this.divSSElevUnit.Attributes["class"] = "form-control bg-gray-light";
                this.rdbSSDFt.Checked = false;
                this.rdbSSDFt.Enabled = false;
                this.rdbSSDM.Checked = false; 
                this.rdbSSDM.Enabled = false;
                this.txtSS.Text = "";
                this.txtSS.Enabled = false;
                this.planEnclosure.Visible = false;
                this.lblPlanSuccess.Text = "";
                this.btnTINext.Enabled = false;
                this.btnTINext.CssClass = "btn btn-default text-center";
                this.btnTIReset.Enabled = false;
                this.btnTIReset.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void chkPlanAllZero_CheckedChanged(object sender, EventArgs e)
        {
            Int32 lenUnit = -99;
            if (this.rdbVSFeet.Checked) { lenUnit = 4; } else { lenUnit = 5; }
            if (lenUnit.Equals(4))
            {
                this.rdbMDUnitFT.Checked = true; this.rdbMDUnitM.Checked = false; this.txtTIMD.Text = "0.00"; this.txtTIMD.Enabled = true;
                this.divVD.Attributes["class"] = "form-control"; this.rdbTVDFt.Checked = true; this.rdbTVDFt.Enabled = true; this.rdbTVDM.Checked = false; this.rdbTVDM.Enabled = true; this.txtTITVD.Text = "0.00"; this.txtTITVD.Enabled = true;
                this.divNOff.Attributes["class"] = "form-control"; this.rdbNOFt.Checked = true; this.rdbNOFt.Enabled = true; this.rdbNOM.Checked = false; this.rdbNOM.Enabled = true; this.txtTINorth.Text = "0.00"; this.txtTINorth.Enabled = true;
                this.divEOff.Attributes["class"] = "form-control"; this.rdbEOFt.Checked = true; this.rdbEOFt.Enabled = true; this.rdbEOM.Checked = false; this.rdbEOM.Enabled = true; this.txtTIEast.Text = "0.00"; this.txtTIEast.Enabled = true;
                this.divVSec.Attributes["class"] = "form-control"; this.rdbVSecFt.Checked = true; this.rdbVSecFt.Enabled = true; this.rdbVSMtr.Checked = false; this.rdbVSMtr.Enabled = true; this.txtVS.Text = "0.00"; this.txtVS.Enabled = true;
                this.divDLRate.Attributes["class"] = "form-control"; this.rdbDLSFt.Checked = true; this.rdbDLSFt.Enabled = true; this.rdbDLSM.Checked = false; this.rdbDLSM.Enabled = true; this.txtDGLG.Text = "0.00"; this.txtDGLG.Enabled = true;
                this.divSSElevUnit.Attributes["class"] = "form-control"; this.rdbSSDFt.Checked = true; this.rdbSSDFt.Enabled = true; this.rdbSSDM.Checked = false; this.rdbSSDM.Enabled = true; this.txtSS.Text = "0.00"; this.txtSS.Enabled = true;
                this.btnTINext.Enabled = true;
                this.btnTINext.CssClass = "btn btn-info text-center";
                this.btnTIReset.Enabled = true;
                this.btnTIReset.CssClass = "btn btn-warning text-center";
            }
            else
            {
                this.rdbMDUnitFT.Checked = false; this.rdbMDUnitM.Checked = true; this.txtTIMD.Text = "0.00"; this.txtTIMD.Enabled = true;
                this.divVD.Attributes["class"] = "form-control"; this.rdbTVDFt.Checked = false; this.rdbTVDFt.Enabled = true; this.rdbTVDM.Checked = true; this.rdbTVDM.Enabled = true; this.txtTITVD.Text = "0.00"; this.txtTITVD.Enabled = true;
                this.divNOff.Attributes["class"] = "form-control"; this.rdbNOFt.Checked = false; this.rdbNOFt.Enabled = true; this.rdbNOM.Checked = true; this.rdbNOM.Enabled = true; this.txtTINorth.Text = "0.00"; this.txtTINorth.Enabled = true;
                this.divEOff.Attributes["class"] = "form-control"; this.rdbEOFt.Checked = false; this.rdbEOFt.Enabled = true; this.rdbEOM.Checked = true; this.rdbEOM.Enabled = true; this.txtTIEast.Text = "0.00"; this.txtTIEast.Enabled = true;
                this.divVSec.Attributes["class"] = "form-control"; this.rdbVSecFt.Checked = false; this.rdbVSecFt.Enabled = true; this.rdbVSMtr.Checked = true; this.rdbVSMtr.Enabled = true; this.txtVS.Text = "0.00"; this.txtVS.Enabled = true;
                this.divDLRate.Attributes["class"] = "form-control"; this.rdbDLSFt.Checked = false; this.rdbDLSFt.Enabled = true; this.rdbDLSM.Checked = true; this.rdbDLSM.Enabled = true; this.txtDGLG.Text = "0.00"; this.txtDGLG.Enabled = true;
                this.divSSElevUnit.Attributes["class"] = "form-control"; this.rdbSSDFt.Checked = false; this.rdbSSDFt.Enabled = true; this.rdbSSDM.Checked = true; this.rdbSSDM.Enabled = true; this.txtSS.Text = "0.00"; this.txtSS.Enabled = true;
                this.btnTINext.Enabled = true;
                this.btnTINext.CssClass = "btn btn-info text-center";
                this.btnTIReset.Enabled = true;
                this.btnTIReset.CssClass = "btn btn-warning text-center";
            }
        }

        protected void btnAddPlacement_Click(object sender, EventArgs e)
        {
            try
            {
                this.divWellAdd.Visible = false;
                this.divWellRF.Visible = false;
                this.divWellPlan.Visible = false;
                this.divWellPlacement.Visible = true;
                this.divWellStatus.Visible = false;
                this.divWRegister.Visible = false;
                this.charts.Visible = false;
                this.btnAddWell.Visible = false;
                this.btnWellReset.Visible = false;
                this.btnWellStepBack.Visible = false;
                this.btnWellExit.Visible = false;
                Button welBtn = (Button)sender;
                GridViewRow gRow = (GridViewRow)welBtn.NamingContainer;
                String wellname = null;
                wellname = Convert.ToString(Server.HtmlDecode(gRow.Cells[2].Text));
                Int32 rowIndex = Convert.ToInt32(gRow.RowIndex);
                Int32 optrID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue), wpdID = Convert.ToInt32(this.gvwWellPads.SelectedValue), welID = Convert.ToInt32(gvwWell.DataKeys[rowIndex]["welID"]);
                this.hdnWell.Value = welID.ToString();
                this.divDisplayButtons.Visible = false;
                this.ltrECHarts3DwithPlan.Text = "";
                this.divWellDetail.Visible = false;
                this.gvwWellDetail.DataSource = null;
                this.gvwWellDetail.DataBind();
                this.divgvwWellDetail.Visible = false;
                this.divWellRefMag.Visible = false;
                this.gvwRMList.DataSource = null;
                this.gvwRMList.DataBind();
                this.divgvwRMList.Visible = false;
                this.gvwiRMList.Visible = false;
                this.gvwiRMList.DataSource = null;
                this.gvwiRMList.DataBind();
                this.divgvwRMList.Visible = false;
                this.divlblWellPlanTI.Visible = false;
                this.gvwWellPlanInfo.DataSource = null;
                this.gvwWellPlanInfo.DataBind();
                this.divgvwWellPlanInfo.Visible = false;
                this.lblWellPlanCalc.Visible = false;
                this.gvwWellPlanCalc.DataSource = null;
                this.gvwWellPlanCalc.DataBind();
                this.gvwWellPlanCalc.Visible = false;
                this.lblWPCTieIn.Visible = false;
                this.gvwWPCTieIn.DataSource = null;
                this.gvwWPCTieIn.DataBind();
                this.gvwWPCTieIn.Visible = false;
                this.lblWellPlaceCalc.Visible = false;
                this.gvwWellPlaceCalc.DataSource = null;
                this.gvwWellPlaceCalc.DataBind();
                this.gvwWellPlaceCalc.Visible = false;
                this.divlblWellRegistrations.Visible = false;
                this.gvwWReg.DataSource = null;
                this.gvwWReg.DataBind();
                this.gvwWReg.Visible = false;
                this.btnWellReset.CssClass = "btn btn-warning text-center";
                this.btnWellReset.Enabled = true;
                this.btnPFProcessDone.Visible = true;
                this.btnPFProcessDone.Enabled = true;
                this.btnPFProcessDone.CssClass = "btn btn-danger text-center";
                this.lblWellName.Text = " Selected Well: " + wellname;
                this.divWellNameDetail.Visible = true;
                this.liWP1.Attributes["class"] = "stepper-todo";
                this.liWP2.Attributes["class"] = "stepper-todo";
                this.liWP3.Attributes["class"] = "stepper-todo";
                this.chkTIPlacementFeet.Checked = false;
                this.chkTIPlacementMeter.Checked = false;
                this.btnTIPReset_Click(null, null);
                this.divTIPlacement.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void chkTIPlacementFeet_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbTIPFeet.Checked = true;
                this.rdbTIPMeter.Checked = false;
                this.txtTIPMD.Text = "0.00";
                this.txtTIPMD.Enabled = true;
                this.txtTIPInc.Text = "0.00";
                this.txtTIPInc.Enabled = true;
                this.txtTIPAzm.Text = "0.00";
                this.txtTIPAzm.Enabled = true;
                this.divTIPVD.Attributes["class"] = "form-control";
                this.rdbTIPVDFeet.Checked = true;
                this.rdbTIPVDFeet.Enabled = true;
                this.rdbTIPVDMeter.Checked = false;
                this.rdbTIPVDMeter.Enabled = true;
                this.txtTIPVertDepth.Text = "0.00";
                this.txtTIPVertDepth.Enabled = true;
                this.divTIPNOff.Attributes["class"] = "form-control";
                this.rdbTIPNOffFeet.Checked = true;
                this.rdbTIPNOffFeet.Enabled = true;
                this.rdbTIPNOffMeter.Checked = false;
                this.rdbTIPNOffMeter.Enabled = true;
                this.txtTIPNOffset.Text = "0.00";
                this.txtTIPNOffset.Enabled = true;
                this.divTIPEOff.Attributes["class"] = "form-control";
                this.rdbTIPEOffFeet.Checked = true;
                this.rdbTIPEOffFeet.Enabled = true;
                this.rdbTIPEOffMeter.Checked = false;
                this.rdbTIPEOffMeter.Enabled = true;
                this.txtTIPEOffset.Text = "0.00";
                this.txtTIPEOffset.Enabled = true;
                this.divTIPVS.Attributes["class"] = "form-control";
                this.rdbTIPVSFeet.Checked = true;
                this.rdbTIPVSFeet.Enabled = true;
                this.rdbTIPVSMeter.Checked = false;
                this.rdbTIPVSMeter.Enabled = true;
                this.txtTIPVSection.Text = "0.00";
                this.txtTIPVSection.Enabled = true;
                this.divTIPSS.Attributes["class"] = "form-control";
                this.rdbTIPSSFeet.Checked = true;
                this.rdbTIPSSFeet.Enabled = true;
                this.rdbTIPSSMeter.Checked = false;
                this.rdbTIPSSMeter.Enabled = true;
                this.txtTIPSSElev.Text = "0.00";
                this.txtTIPSSElev.Enabled = true;
                this.btnTIPNext.Enabled = true;
                this.btnTIPNext.CssClass = "btn btn-info text-center";
                this.btnTIPReset.Enabled = true;
                this.btnTIPReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void chkTIPlacementMeter_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbTIPFeet.Checked = false;
                this.rdbTIPMeter.Checked = true;
                this.txtTIPMD.Text = "0.00";
                this.txtTIPMD.Enabled = true;
                this.txtTIPInc.Text = "0.00";
                this.txtTIPInc.Enabled = true;
                this.txtTIPAzm.Text = "0.00";
                this.txtTIPAzm.Enabled = true;
                this.divTIPVD.Attributes["class"] = "form-control";
                this.rdbTIPVDFeet.Checked = false;
                this.rdbTIPVDFeet.Enabled = true;
                this.rdbTIPVDMeter.Checked = true;
                this.rdbTIPVDMeter.Enabled = true;
                this.txtTIPVertDepth.Text = "0.00";
                this.txtTIPVertDepth.Enabled = true;
                this.divTIPNOff.Attributes["class"] = "form-control";
                this.rdbTIPNOffFeet.Checked = false;
                this.rdbTIPNOffFeet.Enabled = true;
                this.rdbTIPNOffMeter.Checked = true;
                this.rdbTIPNOffMeter.Enabled = true;
                this.txtTIPNOffset.Text = "0.00";
                this.txtTIPNOffset.Enabled = true;
                this.divTIPEOff.Attributes["class"] = "form-control";
                this.rdbTIPEOffFeet.Checked = false;
                this.rdbTIPEOffFeet.Enabled = true;
                this.rdbTIPEOffMeter.Checked = true;
                this.rdbTIPEOffMeter.Enabled = true;
                this.txtTIPEOffset.Text = "0.00";
                this.txtTIPEOffset.Enabled = true;
                this.divTIPVS.Attributes["class"] = "form-control";
                this.rdbTIPVSFeet.Checked = false;
                this.rdbTIPVSFeet.Enabled = true;
                this.rdbTIPVSMeter.Checked = true;
                this.rdbTIPVSMeter.Enabled = true;
                this.txtTIPVSection.Text = "0.00";
                this.txtTIPVSection.Enabled = true;
                this.divTIPSS.Attributes["class"] = "form-control";
                this.rdbTIPSSFeet.Checked = false;
                this.rdbTIPSSFeet.Enabled = true;
                this.rdbTIPSSMeter.Checked = true;
                this.rdbTIPSSMeter.Enabled = true;
                this.txtTIPSSElev.Text = "0.00";
                this.txtTIPSSElev.Enabled = true;
                this.btnTIPNext.Enabled = true;
                this.btnTIPNext.CssClass = "btn btn-info text-center";
                this.btnTIPReset.Enabled = true;
                this.btnTIPReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbTIPFeet_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbTIPMeter.Checked = false;
                this.txtTIPMD.Enabled = true;
                this.txtTIPMD.Text = "";
                this.txtTIPMD.Focus();
                this.txtTIPInc.Enabled = true;
                this.txtTIPInc.Text = "";
                this.txtTIPAzm.Enabled = true;
                this.txtTIPAzm.Text = "";
                this.divTIPVD.Attributes["class"] = "form-control";
                this.rdbTIPVDFeet.Checked = false;
                this.rdbTIPVDFeet.Enabled = true;
                this.rdbTIPVDMeter.Checked = false;
                this.rdbTIPVDMeter.Enabled = true;
                this.encPlacement.Visible = false;
                this.btnTIPReset.Enabled = true;
                this.btnTIPReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbTIPMeter_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbTIPFeet.Checked = false;
                this.txtTIPMD.Enabled = true;
                this.txtTIPMD.Text = "";
                this.txtTIPMD.Focus();
                this.txtTIPInc.Enabled = true;
                this.txtTIPInc.Text = "";
                this.txtTIPAzm.Enabled = true;
                this.txtTIPAzm.Text = "";
                this.divTIPVD.Attributes["class"] = "form-control";
                this.rdbTIPVDFeet.Checked = false;
                this.rdbTIPVDFeet.Enabled = true;
                this.rdbTIPVDMeter.Checked = false;
                this.rdbTIPVDMeter.Enabled = true;
                this.encPlacement.Visible = false;
                this.btnTIPReset.Enabled = true;
                this.btnTIPReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbTIPVDFeet_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbTIPVDMeter.Checked = false;
                this.divTIPNOff.Attributes["class"] = "form-control";
                this.rdbTIPNOffFeet.Checked = false;
                this.rdbTIPNOffFeet.Enabled = true;
                this.rdbTIPNOffMeter.Checked = false;
                this.rdbTIPNOffMeter.Enabled = true;
                this.txtTIPNOffset.Enabled = true;
                this.txtTIPNOffset.Text = "";
                this.txtTIPVertDepth.Text = "";
                this.txtTIPVertDepth.Enabled = true;
                this.txtTIPVertDepth.Focus();
                this.btnTIPReset.Enabled = true;
                this.btnTIPReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbTIPVDMeter_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbTIPVDFeet.Checked = false;
                this.divTIPNOff.Attributes["class"] = "form-control";
                this.rdbTIPNOffFeet.Checked = false;
                this.rdbTIPNOffFeet.Enabled = true;
                this.rdbTIPNOffMeter.Checked = false;
                this.rdbTIPNOffMeter.Enabled = true;
                this.txtTIPNOffset.Enabled = true;
                this.txtTIPNOffset.Text = "";
                this.txtTIPVertDepth.Text = "";
                this.txtTIPVertDepth.Enabled = true;
                this.txtTIPVertDepth.Focus();
                this.btnTIPReset.Enabled = true;
                this.btnTIPReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbTIPNOffFeet_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbTIPNOffMeter.Checked = false;
                this.txtTIPNOffset.Text = "";
                this.txtTIPNOffset.Enabled = true;
                this.txtTIPNOffset.Focus();
                this.divTIPEOff.Attributes["class"] = "form-control";
                this.rdbTIPEOffFeet.Checked = false;
                this.rdbTIPEOffFeet.Enabled = true;
                this.rdbTIPEOffMeter.Checked = false;
                this.rdbTIPEOffMeter.Enabled = true;
                this.btnTIPReset.Enabled = true;
                this.btnTIPReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbTIPNOffMeter_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbTIPNOffFeet.Checked = false;
                this.txtTIPNOffset.Text = "";
                this.txtTIPNOffset.Enabled = true;
                this.txtTIPNOffset.Focus();
                this.divTIPEOff.Attributes["class"] = "form-control";
                this.rdbTIPEOffFeet.Checked = false;
                this.rdbTIPEOffFeet.Enabled = true;
                this.rdbTIPEOffMeter.Checked = false;
                this.rdbTIPEOffMeter.Enabled = true;
                this.btnTIPReset.Enabled = true;
                this.btnTIPReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbTIPEOffFeet_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbTIPEOffMeter.Checked = false;
                this.txtTIPEOffset.Text = "";
                this.txtTIPEOffset.Enabled = true;
                this.txtTIPEOffset.Focus();
                this.divTIPVS.Attributes["class"] = "form-control";
                this.rdbTIPVSFeet.Checked = false;
                this.rdbTIPVSFeet.Enabled = true;
                this.rdbTIPVSMeter.Checked = false;
                this.rdbTIPVSMeter.Enabled = true;
                this.btnTIPReset.Enabled = true;
                this.btnTIPReset.CssClass = "btn btn-warning text-center";

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbTIPEOffMeter_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbTIPEOffFeet.Checked = false;
                this.txtTIPEOffset.Text = "";
                this.txtTIPEOffset.Enabled = true;
                this.txtTIPEOffset.Focus();
                this.divTIPVS.Attributes["class"] = "form-control";
                this.rdbTIPVSFeet.Checked = false;
                this.rdbTIPVSFeet.Enabled = true;
                this.rdbTIPVSMeter.Checked = false;
                this.rdbTIPVSMeter.Enabled = true;
                this.btnTIPReset.Enabled = true;
                this.btnTIPReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbTIPVSFeet_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbTIPVSMeter.Checked = false;
                this.txtTIPVSection.Text = "";
                this.txtTIPVSection.Enabled = true;
                this.txtTIPVSection.Focus();
                this.divTIPSS.Attributes["class"] = "form-control";
                this.rdbTIPSSFeet.Checked = false;
                this.rdbTIPSSFeet.Enabled = true;
                this.rdbTIPSSMeter.Checked = false;
                this.rdbTIPSSMeter.Enabled = true;
                this.btnTIPReset.Enabled = true;
                this.btnTIPReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbTIPVSMeter_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbTIPVSFeet.Checked = false;
                this.txtTIPVSection.Text = "";
                this.txtTIPVSection.Enabled = true;
                this.txtTIPVSection.Focus();
                this.divTIPSS.Attributes["class"] = "form-control";
                this.rdbTIPSSFeet.Checked = false;
                this.rdbTIPSSFeet.Enabled = true;
                this.rdbTIPSSMeter.Checked = false;
                this.rdbTIPSSMeter.Enabled = true;
                this.btnTIPReset.Enabled = true;
                this.btnTIPReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbTIPSSFeet_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbTIPSSMeter.Checked = false;
                this.txtTIPSSElev.Text = "";
                this.txtTIPSSElev.Enabled = true;
                this.txtTIPSSElev.Focus();
                this.btnTIPReset.Enabled = true;
                this.btnTIPReset.CssClass = "btn btn-warning text-center";
                this.btnTIPNext.Enabled = true;
                this.btnTIPNext.CssClass = "btn btn-info text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbTIPSSMeter_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbTIPSSFeet.Checked = false;
                this.txtTIPSSElev.Text = "";
                this.txtTIPSSElev.Enabled = true;
                this.txtTIPSSElev.Focus();
                this.btnTIPReset.Enabled = true;
                this.btnTIPReset.CssClass = "btn btn-warning text-center";
                this.btnTIPNext.Enabled = true;
                this.btnTIPNext.CssClass = "btn btn-info text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnTIPNext_Click(object sender, EventArgs e)
        {
            try
            {
                String mdVal = String.Empty, incVal = String.Empty, azmVal = String.Empty, tvdVal = String.Empty, nOffVal = String.Empty, eOffVal = String.Empty, vsVal = String.Empty, ssVal = String.Empty;
                Decimal md = -99.00M, inc = -99.00M, azm = -99.00M, tvd = -99.00M, nOff = -99.00M, eOff = -99.00M, vs = -99.00M, ss = -99.00M;
                mdVal = Convert.ToString(this.txtTIPMD.Text); incVal = Convert.ToString(this.txtTIPInc.Text); azmVal = Convert.ToString(this.txtTIPAzm.Text);
                tvdVal = Convert.ToString(this.txtTIPVertDepth.Text); nOffVal = Convert.ToString(this.txtTIPNOffset.Text); eOffVal = Convert.ToString(this.txtTIPEOffset.Text);
                vsVal = Convert.ToString(this.txtTIPVSection.Text); ssVal = Convert.ToString(this.txtTIPSSElev.Text);
                Int32 welID = Convert.ToInt32(this.hdnWell.Value);
                String wellName = AST.getWellName(welID, GetClientDBString);
                if (String.IsNullOrEmpty(mdVal) || !Decimal.TryParse(mdVal, out md) || String.IsNullOrEmpty(incVal) || !Decimal.TryParse(incVal, out inc) || String.IsNullOrEmpty(azmVal) || !Decimal.TryParse(azmVal, out azm) || String.IsNullOrEmpty(tvdVal) || !Decimal.TryParse(tvdVal, out tvd) || String.IsNullOrEmpty(nOffVal) || !Decimal.TryParse(nOffVal, out nOff) || String.IsNullOrEmpty(eOffVal) || !Decimal.TryParse(eOffVal, out eOff) || String.IsNullOrEmpty(vsVal) || !Decimal.TryParse(vsVal, out vs) || String.IsNullOrEmpty(ssVal) || !Decimal.TryParse(ssVal, out ss))
                {
                    this.encPlacement.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iPlace.Attributes["class"] = "icon fa fa-warning";
                    this.spnPlace.InnerText = " Warning!";
                    this.lblPlacementSuccess.Text = "Missing Values. Please provide all values";
                    this.encPlacement.Visible = true;
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wellName + " Invalid values while inserting Well Placement Tie-In Data (Null Values) ");
                }
                else
                {
                    this.divTIPlacement.Visible = false;
                    this.divTIPFile.Visible = true;
                    this.btnTIPNext.Visible = false;
                    this.btnTIPReset.Visible = false;
                    this.btnPFUpload.Visible = true;
                    this.btnPFUpload.Enabled = true;
                    this.tipFileUpload.Enabled = true;
                    if (tipFileUpload.HasFile) { tipFileUpload.Dispose(); }
                    this.btnPFUpload.CssClass = "btn btn-success text-center";
                    this.btnPFClear.Visible = true;
                    this.btnPFClear.Enabled = true;
                    this.btnPFClear.CssClass = "btn btn-warning text-center";
                    this.liWP1.Attributes["class"] = "stepper-done";
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnTIPReset_Click(object sender, EventArgs e)
        {
            try
            {
                this.chkTIPlacementFeet.Checked = false;
                this.chkTIPlacementMeter.Checked = false;
                this.rdbTIPMeter.Checked = false;
                this.rdbTIPFeet.Checked = false;
                this.rdbTIPMeter.Checked = false;
                this.txtTIPMD.Text = "";
                this.txtTIPMD.Enabled = false;
                this.txtTIPInc.Text = "";
                this.txtTIPInc.Enabled = false;
                this.txtTIPAzm.Text = "";
                this.txtTIPAzm.Enabled = false;
                this.divTIPVD.Attributes["class"] = "form-control bg-gray-light";
                this.rdbTIPVDFeet.Checked = false;
                this.rdbTIPVDFeet.Enabled = false;
                this.rdbTIPVDMeter.Checked = false;
                this.rdbTIPVDMeter.Enabled = false;
                this.txtTIPVertDepth.Text = "";
                this.txtTIPVertDepth.Enabled = false;
                this.divTIPNOff.Attributes["class"] = "form-control bg-gray-light";
                this.rdbTIPNOffFeet.Checked = false;
                this.rdbTIPNOffFeet.Enabled = false;
                this.rdbTIPNOffMeter.Checked = false;
                this.rdbTIPNOffMeter.Enabled = false;
                this.txtTIPNOffset.Text = "";
                this.txtTIPNOffset.Enabled = false;
                this.divTIPEOff.Attributes["class"] = "form-control bg-gray-light";
                this.rdbTIPEOffFeet.Checked = false;
                this.rdbTIPEOffFeet.Enabled = false;
                this.rdbTIPEOffMeter.Checked = false;
                this.rdbTIPEOffMeter.Enabled = false;
                this.txtTIPEOffset.Text = "";
                this.txtTIPEOffset.Enabled = false;
                this.divTIPVS.Attributes["class"] = "form-control bg-gray-light";
                this.rdbTIPVSFeet.Checked = false;
                this.rdbTIPVSFeet.Enabled = false;
                this.rdbTIPVSMeter.Checked = false;
                this.rdbTIPVSMeter.Enabled = false;
                this.txtTIPVSection.Text = "";
                this.txtTIPVSection.Enabled = false;
                this.divTIPSS.Attributes["class"] = "form-control bg-gray-light";
                this.rdbTIPSSFeet.Checked = false;
                this.rdbTIPSSFeet.Enabled = false;
                this.rdbTIPSSMeter.Checked = false;
                this.rdbTIPSSMeter.Enabled = false;
                this.txtTIPSSElev.Text = "";
                this.txtTIPSSElev.Enabled = false;
                this.btnTIPNext.Enabled = false;
                this.btnTIPNext.CssClass = "btn btn-default text-center";
                this.btnTIPReset.Enabled = false;
                this.btnTIPReset.CssClass = "btn btn-default text-center";
                this.encPlacement.Visible = false;
                this.lblPlacementSuccess.Text = "";
                this.divPlacementData.Visible = false;
                this.divTIPFile.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnPFUpload_Click(object sender, EventArgs e)
        {
            try
            {
                ArrayList iResult = new ArrayList();
                List<String> pfFileData = new List<String>();
                String dLine = String.Empty, fileName = String.Empty, fileExtension= String.Empty;
                String[] lineRead = new String[] { };
                Int32 mdUnits = -99;
                if (rdbDataMDFeet.Checked) { mdUnits = 4; } else { mdUnits = 5; }
                Int32 welID = Convert.ToInt32(this.hdnWell.Value);
                String welName = AST.getWellName(welID, GetClientDBString);
                if (tipFileUpload.HasFile)
                {
                    fileName = Path.GetFileName(tipFileUpload.PostedFile.FileName);
                    fileExtension = Path.GetExtension(tipFileUpload.PostedFile.FileName);
                    if (!fileExtension.Equals(".csv"))
                    {
                        this.encPlacement.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iPlace.Attributes["class"] = "icon fa fa-warning";
                        this.spnPlace.InnerText = " Warning!";
                        this.lblPlacementSuccess.Text = "Incorrect Data File Type. (Only accepting '.csv' file)";
                        this.encPlacement.Visible = true;
                        logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + welName + " Incorrect File Type while inserting Well Placement Tie-In Data (Null Values) ");
                    }
                    else
                    {
                        StreamReader readD = new StreamReader(tipFileUpload.FileContent);
                        do
                        {
                            dLine = readD.ReadLine();
                            lineRead = dLine.Split(',');
                            if (String.IsNullOrEmpty(lineRead[0].Trim()))
                            {
                                continue;
                            }
                            else
                            {
                                pfFileData.Add(dLine);
                            }
                        }
                        while (readD.Peek() != -1);
                        readD.Close();

                        iResult = WPN.processPlacementInfo(fileName, fileExtension, pfFileData, mdUnits);
                        System.Data.DataTable rwpTable = new System.Data.DataTable();
                        this.divMDButtons.Visible = true;
                        this.rdbDataMDFeet.Checked = false;
                        this.rdbDataMDMeter.Checked = false;
                        rwpTable = (System.Data.DataTable)iResult[0];
                        HttpContext.Current.Session["uploadedPlacementTable"] = rwpTable;
                        this.divTIPFile.Visible = false;
                        this.divPlacementData.Visible = true;
                        this.processWPData.Visible = true;
                        this.gvwPlacement.DataSource = rwpTable;
                        this.gvwPlacement.PageIndex = 0;
                        this.gvwPlacement.SelectedIndex = -1;
                        this.gvwPlacement.DataBind();
                        this.gvwPlacement.Visible = true;
                        this.gvwWPCalculated.DataSource = null;
                        this.gvwWPCalculated.DataBind();
                        this.btnPFUpload.Visible = false;
                        this.btnPFUpload.Enabled = false;
                        this.btnPFUpload.CssClass = "btn btn-default text-center";
                        this.btnPFClear.Visible = false;
                        this.btnPFClear.Enabled = false;
                        this.btnPFClear.CssClass = "btn btn-default text-center";
                        this.btnPFProcess.Visible = true;
                        this.btnPFProcess.Enabled = false;
                        this.btnPFProcess.CssClass = "btn btn-default text-center";
                        this.btnPFProcessReset.Visible = true;
                        this.btnPFProcessReset.Enabled = true;
                        this.btnPFProcessReset.CssClass = "btn btn-warning text-center";
                        this.liWP1.Attributes["class"] = "stepper-done";
                        this.liWP2.Attributes["class"] = "stepper-done";
                        logger.Info(" User: " + LoginName + " Realm: " + domain + " Well Name: " + welName + " Added Well/Lateral Plan (Calculated Values) ");
                    }
                }
                else
                {
                    this.encPlacement.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iPlace.Attributes["class"] = "icon fa fa-warning";
                    this.spnPlace.InnerText = " Warning!";
                    this.lblPlacementSuccess.Text = "Missing Data File. Please select a Data file";
                    this.encPlacement.Visible = true;
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + welName + " NO Data File while inserting Well Placement Tie-In Data (Null Values) ");
                }                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnPFClear_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.tipFileUpload.HasFile) { tipFileUpload.Dispose(); }
                this.tipFileUpload.Focus();
                this.encPlacement.Visible = false;
                this.lblPlacementSuccess.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwPlacement_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable rwpTable = (System.Data.DataTable)HttpContext.Current.Session["uploadedPlacementTable"];
                this.gvwPlacement.DataSource = rwpTable;
                this.gvwPlacement.PageIndex = e.NewPageIndex;
                this.gvwPlacement.SelectedIndex = -1;
                this.gvwPlacement.DataBind();
                this.btnPFProcessReset.Visible = true;
                this.btnPFProcessReset.Enabled = true;
                this.btnPFProcessReset.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbDataMDFeet_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbDataMDMeter.Checked = false;
                this.btnPFProcess.Enabled = true;
                this.btnPFProcess.CssClass = "btn btn-success text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void rdbDataMDMeter_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.rdbDataMDFeet.Checked = false;
                this.btnPFProcess.Enabled = true;
                this.btnPFProcess.CssClass = "btn btn-success text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        } 

        protected void btnPFProcess_Click(object sender, EventArgs e)
        {
            try
            {
                ArrayList iResult = new ArrayList();
                Int32 OperatorID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                Int32 WellPadID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                System.Data.DataTable wpcData = new System.Data.DataTable();
                Decimal mdti = -99.00M, incti = -99.00M, azmti = -99.00M, tvdti = -99.00M, nOffti = -99.00M, eOffti = -99.00M, vsti = -99.00M, ssti = -99.00M;
                Int32 mdU = -99, tvdU = -99, nOffU = -99, eOffU = -99, vsU = -99, ssU = -99;
                mdti = Convert.ToDecimal(this.txtTIPMD.Text); incti = Convert.ToDecimal(this.txtTIPInc.Text); azmti = Convert.ToDecimal(this.txtTIPAzm.Text);
                tvdti = Convert.ToDecimal(this.txtTIPVertDepth.Text); nOffti = Convert.ToDecimal(this.txtTIPNOffset.Text); eOffti = Convert.ToDecimal(this.txtTIPEOffset.Text);
                vsti = Convert.ToDecimal(this.txtTIPVSection.Text); ssti = Convert.ToDecimal(this.txtTIPSSElev.Text);
                if (rdbTIPFeet.Checked) { mdU = 4; } else { mdU = 5; }
                if (rdbTIPVDFeet.Checked) { tvdU = 4; } else { tvdU = 5; }
                if (rdbTIPNOffFeet.Checked) { nOffU = 4; } else { nOffU = 5; }
                if (rdbTIPEOffFeet.Checked) { eOffU = 4; } else { eOffU = 5; }
                if (rdbTIPVSFeet.Checked) { vsU = 4; } else { vsU = 5; }
                if (rdbTIPSSFeet.Checked) { ssU = 4; } else { ssU = 5; }
                System.Data.DataTable wpData = (System.Data.DataTable)HttpContext.Current.Session["uploadedPlacementTable"];
                Int32 WellID = Convert.ToInt32(this.hdnWell.Value);
                String wellName = AST.getWellName(WellID, GetClientDBString);
                iResult = WPN.processPlacementData(OperatorID, ClientID, WellPadID, WellID, HttpContext.Current.Session["WellPlanDataFileName"].ToString(), HttpContext.Current.Session["WellPlanDataFileExtension"].ToString(), mdti, mdU, incti, azmti, tvdti, tvdU, nOffti, nOffU, eOffti, eOffU, vsti, vsU, ssti, ssU, wpData, LoginName, domain, GetClientDBString);
                if (Convert.ToInt32(iResult[0]).Equals(-3))
                {
                    this.encPlacement.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iPlace.Attributes["class"] = "icon fa fa-warning";
                    this.spnPlace.InnerText = " Warning!";
                    this.lblPlacementSuccess.Text = "Problem computing Well Placement Data. Please try again";
                    this.encPlacement.Visible = true;
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wellName + " Problem computing Well Placement Data after processing file ");
                    //WPN.deleteWellPlacementInputData(OperatorID, ClientID, WellPadID, WellID, GetClientDBString);
                }
                else if(Convert.ToInt32(iResult[0]).Equals(-2))
                {
                    this.gvwPlacement.DataSource = null;
                    this.gvwPlacement.DataBind();
                    this.rdbDataMDFeet.Checked=false;
                    this.rdbDataMDMeter.Checked = false;
                    this.btnPFProcess.Enabled = false;
                    this.btnPFProcess.CssClass = "btn btn-default text-center";
                    this.encPlacement.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iPlace.Attributes["class"] = "icon fa fa-warning";
                    this.spnPlace.InnerText = " Warning!";
                    this.lblPlacementSuccess.Text = "Found Duplicate Data File. Unable to proceed";
                    this.encPlacement.Visible = true;
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wellName + " Problem computing Well Placement Data after processing file (Duplicate File) ");
                }
                else
                {
                    wpcData = (System.Data.DataTable)iResult[1];
                    HttpContext.Current.Session["wpcData"] = wpcData;
                    this.gvwWPCalculated.DataSource = wpcData;
                    this.gvwWPCalculated.PageIndex = 0;
                    this.gvwWPCalculated.SelectedIndex = -1;
                    this.gvwWPCalculated.DataBind();
                    this.gvwWPCalculated.Visible = true;
                    this.gvwPlacement.Visible = false;
                    this.btnPFProcess.Enabled = false;
                    this.btnPFProcess.CssClass = "btn btn-default text-center";
                    this.btnPFProcessReset.Enabled = false;
                    this.btnPFProcessReset.CssClass = "btn btn-default text-center";
                    this.liWP3.Attributes["class"] = "stepper-done";
                    this.divMDButtons.Visible = false;
                    this.encPlacement.Attributes["class"] = "alert alert-success alert-dismissable";
                    this.iPlace.Attributes["class"] = "icon fa fa-check-circle";
                    this.spnPlace.InnerText = " Success!";
                    this.lblPlacementSuccess.Text = "Successfully computed uploaded Well Placement data";
                    this.encPlacement.Visible = true;
                    logger.Info(" User: " + LoginName + " Realm: " + domain + " Well Name: " + wellName + " Successfully computed Well Placement Data after processing file ");
                }                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnPFProcessReset_Click(object sender, EventArgs e)
        {
            try
            {
                HttpContext.Current.Session["uploadedPlacementTable"] = null;
                if (tipFileUpload.HasFile) { tipFileUpload.Dispose(); }
                this.gvwPlacement.DataSource = null;
                this.gvwPlacement.DataBind();
                this.divTIPFile.Visible = true;
                this.divPlacementData.Visible = false;
                this.encPlacement.Visible = false;
                this.lblPlacementSuccess.Text = "";
                this.liWP2.Attributes["class"] = "stepper-todo";
                this.btnPFUpload.Visible = true;
                this.btnPFUpload.Enabled = true;
                this.btnPFUpload.CssClass = "btn btn-success text-center";
                this.btnPFClear.Visible = true;
                this.btnPFClear.Enabled = true;
                this.btnPFClear.CssClass = "btn btn-warning text-center";
                this.btnPFProcess.Visible = false;
                this.btnPFProcess.Enabled = false;
                this.btnPFProcess.CssClass = "btn btn-default text-center";
                this.btnPFProcessReset.Visible = false;
                this.btnPFProcessReset.Enabled = false;
                this.btnPFProcessReset.CssClass = "btn btn-default text-center";
                this.rdbDataMDFeet.Checked = false;
                this.rdbDataMDMeter.Checked = false;
                this.gvwWPCalculated.DataSource = null;
                this.gvwWPCalculated.DataBind();
                this.gvwWPCalculated.Visible = false;
                this.btnPFProcess.Enabled = false;
                this.btnPFProcess.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnPFProcessDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnTIPReset_Click(null, null);
                this.btnPFClear_Click(null, null);
                this.btnPFProcessReset_Click(null, null);
                this.divWellPlacement.Visible = false;
                this.lblWellName.Text = "";
                this.divWellNameDetail.Visible = false;
                this.hdnWell.Value = String.Empty;
                this.btnAddWell.Visible = true;
                this.btnWellReset.Visible = true;
                this.btnWellStepBack.Visible = true;
                this.btnWellExit.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwWPCalculated_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable wpcTable = (System.Data.DataTable)HttpContext.Current.Session["wpcData"];
                this.gvwWPCalculated.DataSource = wpcTable;
                this.gvwWPCalculated.PageIndex = e.NewPageIndex;
                this.gvwWPCalculated.SelectedIndex = -1;
                this.gvwWPCalculated.DataBind();
                this.encPlacement.Visible = false;
                this.lblPlacementSuccess.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlRunStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnUpdateRunStatus.Enabled = true;
                this.btnUpdateRunStatus.CssClass = "btn btn-success text-center";
                this.btnUpdateRunStatusClear.Enabled = true;
                this.btnUpdateRunStatusClear.CssClass = "btn btn-warning text-center";
                this.rstEnclosure.Visible = false;
                this.lblRST.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnUpdateRunStatus_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, oprID = -99, wpdID = -99, wellID = -99, secID = -99, runID = -99, dmgstID = -99;                
                oprID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue);
                wpdID = Convert.ToInt32(this.gvwWellPads.SelectedValue);
                wellID = Convert.ToInt32(this.gvwWell.SelectedValue);
                secID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                runID = Convert.ToInt32(this.hdnRunID.Value);
                dmgstID = Convert.ToInt32(this.ddlRunStatus.SelectedValue);
                iResult = OPR.updateRunStatus(runID, dmgstID, GetClientDBString);
                if (iResult.Equals(1))
                {
                    this.btnUpdateRunStatusClear_Click(null, null);
                    this.rstEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                    this.iRST.Attributes["class"] = "icon fa fa-check-circle";
                    this.spnRST.InnerText = " Success!";
                    this.lblRST.Text = "Successfully Updated Run Status ";
                    this.rstEnclosure.Visible = true;
                    logger.Info(" User: " + LoginName + " Realm: " + domain + " Well Pad Name: " + wpdID + " Well Name: " + wellID + " for Section: " + secID + " for Run: " + runID + " updated Run Status to: " + dmgstID);
                    this.btnUpdateRunStatus.Enabled = false;
                    this.btnUpdateRunStatus.CssClass = "btn btn-default text-center";
                }
                else if (iResult.Equals(-1))
                {
                    this.btnUpdateRunStatusClear_Click(null, null);
                    this.rstEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iRST.Attributes["class"] = "icon fa fa-warning";
                    this.spnRST.InnerText = " Warning!";
                    this.lblRST.Text = "!!! Error !!! Duplicate Regisration Request";
                    this.rstEnclosure.Visible = true;
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Pad Name: " + wpdID + " Well Name: " + wellID + " for Section: " + secID + " for Run: " + runID + " updated Run Status to: " + dmgstID);
                    this.btnAddWRegClear.Enabled = true;
                    this.btnAddWRegClear.CssClass = "btn btn-warning text-center";
                }
                else
                {
                    this.btnUpdateRunStatusClear_Click(null, null);
                    this.rstEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iRST.Attributes["class"] = "icon fa fa-warning";
                    this.spnRST.InnerText = " Warning!";
                    this.lblRST.Text = "!!! Error !!! Unable to register Well/Lateral for SMARTs Service";
                    this.rstEnclosure.Visible = true;
                    logger.Error(" User: " + LoginName + " Realm: " + domain + " Well Pad Name: " + wpdID + " Well Name: " + wellID + " for Section: " + secID + " for Run: " + runID + " updated Run Status Error");
                    this.btnAddWRegClear.Enabled = true;
                    this.btnAddWRegClear.CssClass = "btn btn-warning text-center";
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnUpdateRunStatusClear_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> stList = DMG.getRunStatusListForClients();
                this.ddlRunStatus.Items.Clear();
                this.ddlRunStatus.DataSource = stList;
                this.ddlRunStatus.DataValueField = "Key";
                this.ddlRunStatus.DataTextField = "Value";
                this.ddlRunStatus.DataBind();
                this.ddlRunStatus.Items.Insert(0, new ListItem("--- Select Run Status ---", "-1"));
                this.ddlRunStatus.SelectedIndex = -1;
                this.btnUpdateRunStatus.Enabled = false;
                this.btnUpdateRunStatus.CssClass = "btn btn-default text-center";
                this.btnUpdateRunStatusClear.Enabled = false;
                this.btnUpdateRunStatusClear.CssClass = "btn btn-default text-center";
                this.rstEnclosure.Visible = false;
                this.lblRST.Text = "";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnUpdateRunStatusDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnUpdateRunStatusClear_Click(null, null);
                this.divlblRTIDetail.Visible = false;
                this.gvwRTI.DataSource = null;
                this.gvwRTI.DataBind();
                this.divgvwRTI.Visible = false;
                this.lblBHADetail.Visible = false;
                this.gvwBHA.DataSource = null;
                this.gvwBHA.DataBind();
                this.gvwBHA.Visible = false;
                this.divRunList.Visible = true;
                this.divRunAdd.Visible = false;
                this.divBHA.Visible = false;
                this.divRunTieIn.Visible = false;
                this.divRunStatus.Visible = false;
                Int32 optrID = Convert.ToInt32(this.gvwGlobalOptr.SelectedValue), wpdID = Convert.ToInt32(this.gvwWellPads.SelectedValue), wlID = Convert.ToInt32(this.gvwWell.SelectedValue), scID = Convert.ToInt32(this.gvwSectionList.SelectedValue);
                System.Data.DataTable rnTable = OPR.getRunForWellSectionTable(optrID, wpdID, wlID, scID, GetClientDBString);
                this.gvwRunList.DataSource = rnTable;
                this.gvwRunList.SelectedIndex = -1;
                this.gvwRunList.DataBind();
                this.gvwRunList.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }                                                               
    }
}