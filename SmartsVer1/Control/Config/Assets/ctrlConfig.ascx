﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlConfig.ascx.cs" Inherits="SmartsVer1.Control.Config.Assets.ctrlConfig" %>
<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
<!--  Stylesheet -->
<link href="../../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript -->
<script src="../../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<!-- Javascript for Footer -->
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>
<!--- Page local style --->
<style type="text/css" style="background-color: #d9534f; color: #FFFFFF">
    .rtAligned {
        text-align: right;
    }

    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    .sqcLabel {
        min-width: 135px;
        text-align: right;
    }

    .tiLabel {
        min-width: 180px;
        text-align: right;
    }

    .bhaLabel {
        min-width: 170px;
        text-align: right;
    }

    .padLabel {
        min-width: 145px;
        text-align: right;
    }

    .wellLabel {
        min-width: 150px;
        text-align: right;
    }

    .rfLabel {
        min-width: 110px;
        text-align: right;
    }

    .planLabel {
        min-width: 140px;
        text-align:right;
    }

    .padRight {
        padding-right: 25px;
    }

    .btRadio {
        margin-right: 1%;
    }

    .nav a:hover {
        background-color: lightblue;
    }

    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper[data-stepper-steps="8"] li {
            width: 12%;
        }

        ol.stepper[data-stepper-steps="9"] li {
            width: 11%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }    
</style>
<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom: 2px; padding-left: 2px; padding-top: 2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">(
                            <asp:LoginName ID="LoginName1" runat="server" />
                    )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>
        </asp:LoginView>
    </div>
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i>SMARTs Panel</a></li>
                <li>Configuration</li>
                <li><a href="../../../Config/Assets/fecDRAssets_2A.aspx">Configuration</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- Page Content -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul class="nav nav-pills nav-justified">
                <li class="active"><a data-toggle="pill" href="#well">Wells / Laterals</a></li>
                <li><a data-toggle="pill" href="#sqc">Survey Qualification Criterion</a></li>
            </ul>
            <div class="tab-content">
                <div id="sqc" class="tab-pane fade">
                    <asp:UpdateProgress ID="uprgrsSqc" runat="server" DisplayAfter="0" DynamicLayout="true" AssociatedUpdatePanelID="upnlSqc">
                        <ProgressTemplate>
                            <div id="divSQCProcessing" runat="server" class="updateProgress">
                                <asp:Image ID="imgSQCProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlSqc">
                        <ContentTemplate>
                            <div id="divSQCList" runat="server">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">
                                        <h5 class="panel-title"><span>Survey Qualification Criterion (SQC) List</span></h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group">
                                                    <asp:LinkButton ID="btnSQCNew" runat="server" CssClass="btn btn-info text-center" OnClick="btnSQCNew_Click"><i class="fa fa-plus"> Add New</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnSQCReset" runat="server" CssClass="btn btn-warning text-center" Visible="false" OnClick="btnSQCReset_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="table-responsive">
                                                    <asp:GridView ID="gvwSQC" runat="server" CssClass="mydatagrid" AllowPaging="True" AutoGenerateColumns="False"
                                                        DataKeyNames="sqcID" OnPageIndexChanging="gvwSQC_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                        PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow">
                                                        <Columns>
                                                            <asp:BoundField DataField="sqcID" Visible="false" />
                                                            <asp:BoundField DataField="sqcName" HeaderText="Name">
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="sqcDip" HeaderText="Dip Tolerance°">
                                                                <ItemStyle CssClass="padRight" HorizontalAlign="Right" VerticalAlign="Middle" Font-Bold="True"></ItemStyle>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="sqcBT" HeaderText="B-Total Tolerance (nT)">
                                                                <ItemStyle CssClass="padRight" HorizontalAlign="Right" VerticalAlign="Middle" Font-Bold="True"></ItemStyle>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="sqcGT" HeaderText="G-Total Tolerance (g)">
                                                                <ItemStyle CssClass="padRight" HorizontalAlign="Right" VerticalAlign="Middle" Font-Bold="True"></ItemStyle>
                                                            </asp:BoundField>                                                            
                                                        </Columns>                                                      
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divSQCAdd" runat="server" visible="false">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">
                                        <h5 class="panel-title"><span>Add New Survey Qualification Criterion (SQC)</span></h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon sqcLabel bg-light-blue">SQC Name</span>
                                                    <asp:TextBox ID="txtSQCName" runat="server" CssClass="form-control" AutoComplete="off" placeholder="Survey Qualification Criteria Name/Identifier ... (required)" />
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon sqcLabel bg-light-blue">Dip Tolerance</span>
                                                    <asp:TextBox ID="txtSQCDip" runat="server" CssClass="form-control" AutoComplete="off" placeholder="Dip Tolerance (Degree - °) ... (required)" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon sqcLabel bg-light-blue">B-Total Tolerance</span>
                                                    <asp:TextBox ID="txtSQCBT" runat="server" CssClass="form-control" AutoComplete="off" placeholder="B-Total Tolerance (Nano Tesla - nT) ... (required)" />
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon sqcLabel bg-light-blue">G-Total Tolerance</span>
                                                    <asp:TextBox ID="txtSQCGT" runat="server" CssClass="form-control" AutoComplete="off" placeholder="G-Total Tolerance (Acceleration due to Gravity - g) ... (required)" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div runat="server" id="sqcEnclosure" visible="false">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                    <h4><i runat="server" id="iSQCMessage"><span runat="server" id="sqcHeader"></span></i></h4>
                                                    <asp:Label ID="lblSQCSuccess" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:LinkButton ID="btnAddSQC" runat="server" CssClass="btn btn-success text-center" OnClick="btnAddSQC_Click"><i class="fa fa-plus"> Add</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnAddSQCReset" runat="server" CssClass="btn btn-warning text-center" OnClick="btnAddSQCReset_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnAddSQCExit" runat="server" CssClass="btn btn-danger text-center" OnClick="btnAddSQCExit_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div id="well" class="tab-pane fade in active">
                    <asp:UpdateProgress ID="uprgrsWell" runat="server" DisplayAfter="0" DynamicLayout="true" AssociatedUpdatePanelID="upnlWell">
                        <ProgressTemplate>
                            <div id="divWellProcessing" runat="server" class="updateProgress">
                                <asp:Image ID="imgWellProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlWell">
                        <ContentTemplate>
                            <div id="divGagesOuter" runat="server">
                                <div class="panel panel-danger">
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="divWellGages" runat="server" class="panel-collapse collapse in">
                                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-sm-offset-1">
                                                    <div class="small-box bg-light-blue">
                                                        <div id="divOPCount" class="inner" runat="server">
                                                        </div>
                                                        <div class="icon">
                                                            <i class="fa fa-users" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                    <div class="small-box bg-blue">
                                                        <div id="divWCount" class="inner" runat="server">
                                                        </div>
                                                        <div class="icon">
                                                            <i class="fa fa-tint" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                    <div class="small-box bg-green">
                                                        <div id="divLWell" class="inner" runat="server">
                                                        </div>
                                                        <div class="icon">
                                                            <i class="fa fa-heartbeat" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                    <div class="small-box bg-red">
                                                        <div id="divMWell" class="inner" runat="server">
                                                        </div>
                                                        <div class="icon">
                                                            <i class="fa fa-chain-broken" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                    <div class="small-box bg-orange">
                                                        <div id="divPWell" class="inner" runat="server">
                                                        </div>
                                                        <div class="icon">
                                                            <i class="fa fa-chain-broken" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divProgress" runat="server" class="panel panel-danger">
                                <div class="panel-body">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <ol id="olWTJStepper" runat="server" class="stepper" data-stepper-steps="5">
                                            <li id="li1" runat="server" class="stepper-done">Operator Company</li>
                                            <li id="li2" runat="server" class="stepper-todo">Well Pad</li>
                                            <li id="li3" runat="server" class="stepper-todo">Well/Lateral</li>
                                            <li id="li4" runat="server" class="stepper-todo">Well Section</li>
                                            <li id="li5" runat="server" class="stepper-todo">Run</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                            <div id="divOperators" runat="server">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">
                                        <asp:Label ID="lblOperatorList" runat="server" class="panel-title" Text="Operator Companies List" />
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:LinkButton ID="btnOprReset" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnOprReset_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="table-responsive">
                                                <asp:GridView runat="server" ID="gvwGlobalOptr" DataKeyNames="clntID" CssClass="mydatagrid" AllowPaging="True" AllowSorting="True"
                                                    AutoGenerateColumns="False" EmptyDataText="No Operator Company found."
                                                    OnPageIndexChanging="gvwGlobalOptr_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15"
                                                    HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow"
                                                    OnSelectedIndexChanged="gvwGlobalOptr_SelectedIndexChanged">
                                                    <Columns>
                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                            <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                        </asp:ButtonField>
                                                        <asp:BoundField DataField="clntID" Visible="False" />
                                                        <asp:BoundField DataField="clntName" HeaderText="Operator Company">
                                                            <ItemStyle CssClass="padRight" HorizontalAlign="Center" VerticalAlign="Middle" Font-Bold="True"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="wpdCount" HeaderText="Global Well Pad Count" Visible="false">
                                                            <ItemStyle CssClass="padRight" HorizontalAlign="Right" VerticalAlign="Middle" Font-Bold="True"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="welCount" HeaderText="Global Well Count" Visible="false">
                                                            <ItemStyle CssClass="padRight" HorizontalAlign="Right" VerticalAlign="Middle" Font-Bold="True"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="welGlobal" HeaderText="SMARTs Well Count">
                                                            <ItemStyle CssClass="padRight" HorizontalAlign="Center" VerticalAlign="Middle" Font-Bold="True"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:TemplateField ShowHeader="False">
                                                            <ItemTemplate>
                                                                <asp:Button runat="server" CommandName="" CssClass="btn btn-info text-center" Text="View Detail" CausesValidation="false"
                                                                    ID="btnOprDetails" OnClick="btnOprDetails_Click"></asp:Button>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                <asp:GridView runat="server" ID="gvwGlobalOptrAddressList" AutoGenerateColumns="False" DataKeyNames="clntAddID" AllowPaging="True"
                                                    AllowSorting="True" CssClass="mydatagrid" EmptyDataText="No Address(es) found for selected Operator." ShowHeaderWhenEmpty="True"
                                                    Visible="False" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                    RowStyle-CssClass="rows" OnPageIndexChanging="gvwGlobalOptrAddressList_PageIndexChanging">
                                                    <Columns>
                                                        <asp:BoundField DataField="clntAddID" Visible="false" />
                                                        <asp:BoundField DataField="dmgID" HeaderText="Type" />
                                                        <asp:BoundField DataField="title" HeaderText="Title" />
                                                        <asp:BoundField DataField="st1" HeaderText="Street 1" />
                                                        <asp:BoundField DataField="st2" HeaderText="Street 2" />
                                                        <asp:BoundField DataField="st3" HeaderText="Street 3" />
                                                        <asp:BoundField DataField="cityID" HeaderText="City/Town" />
                                                        <asp:BoundField DataField="cntyID" HeaderText="County/District" />
                                                        <asp:BoundField DataField="stID" HeaderText="State/Province" />
                                                        <asp:BoundField DataField="ctyID" HeaderText="Country/Nation" />
                                                        <asp:BoundField DataField="zipID" HeaderText="Zip/Postal Code" />
                                                        <asp:BoundField DataField="sregID" HeaderText="Sub-Region" />
                                                        <asp:BoundField DataField="regID" HeaderText="Region" />
                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                    </Columns>
                                                </asp:GridView>
                                                <p>
                                                    <i class="fa fa-exclamation-circle"></i><small>Missing an Operator Company? Write an email to <a href="mailto:support@smartwelldynamics.com">
                                                        support@smartwelldynamics.com</a> to add more Operator Companies.</small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divOperatorPads" runat="server" visible="false">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">
                                        <asp:Label ID="lblPadList" runat="server" class="panel-title" Text="Well Pad(s) List" />
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group text-left">
                                                <asp:LinkButton ID="btnAddPad" runat="server" CssClass="btn btn-info text-center" OnClick="btnAddPad_Click"><i class="fa fa-plus"> Add Well Pad</i></asp:LinkButton>
                                                <asp:LinkButton ID="btnPadReset" runat="server" CssClass="btn btn-default text=center" Enabled="false" OnClick="btnPadReset_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                <asp:LinkButton ID="btnPadStepBack" runat="server" CssClass="btn btn-warning text-center" OnClick="btnPadStepBack_Click"><i class="fa fa-undo"> Step Back</i></asp:LinkButton>
                                                <asp:LinkButton ID="btnPadExit" runat="server" CssClass="btn btn-danger text-center" OnClick="btnExit_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="table-responsive">
                                                <asp:GridView ID="gvwWellPads" runat="server" EmptyDataText="No Well Pad(s) found for selected Operator"
                                                    ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="wpdID"
                                                    AllowPaging="True" CssClass="mydatagrid" OnPageIndexChanging="gvwWellPads_PageIndexChanging" OnSelectedIndexChanged="gvwWellPads_SelectedIndexChanged"
                                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                    RowStyle-CssClass="rows">
                                                    <Columns>
                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                            <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                        </asp:ButtonField>
                                                        <asp:BoundField DataField="wpdID" HeaderText="wpd" ReadOnly="True" Visible="false" />
                                                        <asp:BoundField DataField="welPadName" HeaderText="Well Pad" />
                                                        <asp:BoundField DataField="field" HeaderText="O&G Field" />
                                                        <asp:BoundField DataField="county" HeaderText="County/District/Division/Municipality" />
                                                        <asp:BoundField DataField="state" HeaderText="State/Province" />
                                                        <asp:BoundField DataField="country" HeaderText="Country/Nation" />
                                                        <asp:BoundField DataField="sreg" HeaderText="Sub-Region" />
                                                        <asp:BoundField DataField="reg" HeaderText="Major Region" />
                                                        <asp:BoundField DataField="swlCount" HeaderText="SMARTs Well Count">
                                                            <ItemStyle CssClass="padRight" HorizontalAlign="Right" VerticalAlign="Middle" Font-Bold="True"></ItemStyle>
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divNewPad" runat="server" visible="false">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">
                                        <asp:Label ID="lblNewPad" runat="server" class="panel-title" Text="Add New Well Pad" />
                                    </div>
                                    <div class="panel-body">
                                        <div id="divWellPadAdd" runat="server">
                                            <div class="panel-body">
                                                <div id="divAddWellPadFlow" runat="server">
                                                    <div class="table-responsive">
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon padLabel bg-light-blue">Geographic Region</span>
                                                                <asp:DropDownList ID="ddlWPDRegion" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control"
                                                                    OnSelectedIndexChanged="ddlWPDRegion_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0" Text="--- Select Major Geographic Region ---" />
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon padLabel bg-light-blue">Sub-Region</span>
                                                                <asp:DropDownList ID="ddlWPDSubRegion" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control"
                                                                    Enabled="False" OnSelectedIndexChanged="ddlWPDSubRegion_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0" Text="--- Select Geographic Sub-Region ---" />
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon padLabel bg-light-blue">Country/Nation</span>
                                                                <asp:DropDownList ID="ddlWPDCountry" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control"
                                                                    Enabled="False" OnSelectedIndexChanged="ddlWPDCountry_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0" Text="--- Select Country/Nation ---" />
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon padLabel bg-light-blue">State/Province</span>
                                                                <asp:DropDownList ID="ddlWPDState" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control"
                                                                    Enabled="False" OnSelectedIndexChanged="ddlWPDState_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0" Text="--- Select State/Province ---" />
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon padLabel bg-light-blue">County/District</span>
                                                                <asp:DropDownList ID="ddlWPDCounty" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control"
                                                                    Enabled="False" OnSelectedIndexChanged="ddlWPDCounty_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0" Text="--- Select County/District/Division ---" />
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon padLabel bg-light-blue">O&G Field</span>
                                                                <asp:DropDownList ID="ddlWPDField" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control"
                                                                    Enabled="False" OnSelectedIndexChanged="ddlWPDField_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0" Text="--- Select Oil/Gas Field ---" />
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon padLabel bg-light-blue">Well Pad Name</span>
                                                                <asp:TextBox ID="txtWPDName" runat="server" CssClass="form-control has-feedback" Enabled="False" CausesValidation="True"
                                                                    AutoComplete="off" placeholder="Well Pad Name (required)" />
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div runat="server" id="wpdEnclosure" visible="false">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                <h4><i runat="server" id="iWPD"><span runat="server" id="spnWPD"></span></i></h4>
                                                                <asp:Label ID="lblWPD" runat="server" />
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="input-group" style="width: 100%;">
                                                                <asp:LinkButton ID="btnWPDAdd" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnWPDAdd_Click"><i class="fa fa-plus"> Well Pad</i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnWPDReset" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnWPDReset_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnWPDDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnWPDDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divWellList" runat="server" visible="false">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">
                                        <asp:Label ID="lblWellHeading" runat="server" class="panel-title" Text="Well(s) / Lateral(s) List" />
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:LinkButton ID="btnAddWell" runat="server" CssClass="btn btn-info text-center" OnClick="btnAddWell_Click"><i class="fa fa-plus"> Add Well/Lateral</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnWellReset" runat="server" CssClass="btn btn-default text=center" Enabled="false" OnClick="btnWellReset_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnWellStepBack" runat="server" CssClass="btn btn-warning text-center" OnClick="btnWellStepBack_Click"><i class="fa fa-undo"> Step Back</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnWellExit" runat="server" CssClass="btn btn-danger text-center" OnClick="btnExit_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <asp:HiddenField ID="hdnWell" runat="server" />
                                                <asp:GridView runat="server" ID="gvwWell" DataKeyNames="welID" CssClass="mydatagrid" AllowPaging="True" AllowSorting="True"
                                                    AutoGenerateColumns="False" EmptyDataText="No Well(s) found for selected Operator Company."
                                                    OnPageIndexChanging="gvwWell_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header"
                                                    PagerStyle-CssClass="pager" RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow" OnSelectedIndexChanged="gvwWell_SelectedIndexChanged">
                                                    <Columns>
                                                        <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                            <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                        </asp:ButtonField>
                                                        <asp:BoundField DataField="welID" HeaderText="welID" Visible="False" />
                                                        <asp:BoundField DataField="welName" HeaderText="Well Name" />
                                                        <asp:BoundField DataField="field" HeaderText="Field" />
                                                        <asp:BoundField DataField="county" HeaderText="County / District / Municipality" />
                                                        <asp:BoundField DataField="state" HeaderText="State / Province" />
                                                        <asp:BoundField DataField="spud" HeaderText="Spud Date" DataFormatString="{0:MMM d, yyyy}" />
                                                        <asp:BoundField DataField="wstID" HeaderText="SMARTs Status" />
                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d, yyyy --- hh:mm tt}" ReadOnly="True" />
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnWellDetails" runat="server" CommandName="Detail" CssClass="btn btn-info text-center" Text="View Detail"
                                                                    OnClick="btnWellDetails_Click" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnAddRF" runat="server" CommandName="RefMag" CssClass="btn btn-info text-center" Text="Add Reference Magnetics"
                                                                    OnClick="btnAddRF_Click" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnAddPlan" runat="server" CommandName="Plan" CssClass="btn btn-info text-center" Text="Add Plan" OnClick="btnAddPlan_Click" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnAddPlacement" runat="server" CommandName="Placement" CssClass="btn btn-info text-center" Text="Add Well Placement"
                                                                    OnClick="btnAddPlacement_Click" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnEditStatus" runat="server" CommandName="Status" CssClass="btn btn-info text-center" Text="Edit Status"
                                                                    OnClick="btnEditStatus_Click" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnRegisterWell" runat="server" CommandName="Register" CssClass="btn btn-info text-center" Text="Register Service"
                                                                    OnClick="btnRegisterWell_Click" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div runat="server" id="divWellNameDetail" visible="false">
                                                    <asp:Label ID="lblWellName" runat="server" Text="" ForeColor="White" Font-Bold="true" Font-Size="Large" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div id="divDisplayButtons" style="padding-bottom: 10px; padding-top: 10px"
                                                    runat="server" visible="false">
                                                    <div class="input-group" style="width: 100%; padding-bottom: 5px; padding-top: 5px">
                                                        <asp:LinkButton ID="btnDisplayChartsWellPlan" runat="server" CssClass="btn btn-primary text-center" BackColor="BlueViolet"
                                                            Width="25%" BorderStyle="Solid" BorderWidth="2" OnClick="btnDisplayChartsWellPlan_Click">
                                                                <i class="fa fa-cubes"> 3D Well Plan </i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="panel panel-info panel-body">
                                                    <div id="charts" runat="server" visible="false">
                                                        <h3 class="bg-gray box-title">Well Plan Chart - 3D </h3>
                                                        <div id="divECharts3D" runat="server" style="height: 700px;"></div>
                                                        <asp:Literal ID="ltrECHarts3DwithPlan" runat="server"></asp:Literal>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="divWellDetail" runat="server" visible="false">
                                                <asp:Label ID="lblWellDetail" runat="server" CssClass="label label-info text-center" Font-Size="Larger" Text="Detailed Information"
                                                    BackColor="DarkBlue" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="divgvwWellDetail" runat="server"
                                                visible="false">
                                                <asp:GridView runat="server" ID="gvwWellDetail" DataKeyNames="welID" CssClass="mydatagrid" AllowPaging="True" AllowSorting="True"
                                                    Enabled="false" AutoGenerateColumns="False" HeaderStyle-CssClass="header" RowStyle-CssClass="rows"
                                                    SelectedRowStyle-CssClass="selectedrow">
                                                    <Columns>
                                                        <asp:BoundField DataField="welID" HeaderText="welID" Visible="False" />
                                                        <asp:BoundField DataField="welPermit" HeaderText="Well Permit" />
                                                        <asp:BoundField DataField="welAPI" HeaderText="American Petroleum Institude ID (API)" />
                                                        <asp:BoundField DataField="welUWI" HeaderText="Unique Well Identifier (UWI)" />
                                                        <asp:BoundField DataField="welLongitude" HeaderText="Longitude" />
                                                        <asp:BoundField DataField="welLatitude" HeaderText="Latitude" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="divWellRefMag" runat="server" style="padding-top: 4px; padding-bottom: 1px"
                                                visible="false">
                                                <asp:Label ID="lblWellRefMag" runat="server" CssClass="label label-info text-center" Font-Size="Larger" Text="Reference Magnetics"
                                                    BackColor="DarkBlue" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="divgvwRMList" runat="server" style="padding-bottom: 4px"
                                                visible="false">
                                                <asp:GridView runat="server" ID="gvwRMList" CssClass="mydatagrid" EmptyDataText="No Reference Magnetics available for selected Well/Lateral"
                                                    AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="rfmgID" ShowHeaderWhenEmpty="True" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                    PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows">
                                                    <Columns>
                                                        <asp:BoundField DataField="rfmgID" HeaderText="rfmgID" Visible="False" />
                                                        <asp:BoundField DataField="ifrID" HeaderText="IFR/IIFR" />
                                                        <asp:BoundField DataField="mdec" HeaderText="Magnetic Declination" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="dip" HeaderText="Dip" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="bTotal" HeaderText="B-Total" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="gTotal" HeaderText="G-Total" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="jcID" HeaderText="Convergence" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="gconv" HeaderText="Grid Convergence" ItemStyle-HorizontalAlign="Right" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <asp:GridView ID="gvwiRMList" runat="server" CssClass="mydatagrid" AllowPaging="True" EmptyDataText="No Reference Magnetics available for selected Well/Lateral"
                                                    ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" DataKeyNames="rfmgID" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                    HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                    RowStyle-CssClass="rows" Visible="false">
                                                    <Columns>
                                                        <asp:BoundField DataField="rfmgID" HeaderText="rfmgID" Visible="False" />
                                                        <asp:BoundField DataField="ifrID" HeaderText="IFR/IIFR" />
                                                        <asp:BoundField DataField="mdec" HeaderText="IFR-Magnetic Declination" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="dip" HeaderText="IFR-Dip" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="bTotal" HeaderText="IFR-B-Total" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="gTotal" HeaderText="IFR-G-Total" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="jcID" HeaderText="Convergence" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="gconv" HeaderText="Grid Convergence" ItemStyle-HorizontalAlign="Right" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="divlblWellPlanTI" runat="server" style="padding-top: 4px; padding-bottom: 1px"
                                                visible="false">
                                                <asp:Label ID="lblWellPlanTI" runat="server" CssClass="label label-info text-center" Font-Size="Larger" Text="Well Plan Tie-In"
                                                    BackColor="DarkBlue" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="divgvwWellPlanInfo" runat="server" visible="false"
                                                style="padding-bottom: 4px">
                                                <asp:GridView runat="server" ID="gvwWellPlanInfo" DataKeyNames="wpiID" CssClass="mydatagrid"
                                                    Enabled="false" AutoGenerateColumns="False" HeaderStyle-CssClass="header" RowStyle-CssClass="rows"
                                                    SelectedRowStyle-CssClass="selectedrow" ShowHeaderWhenEmpty="true">
                                                    <Columns>
                                                        <asp:BoundField DataField="wpiID" HeaderText="wpiID" Visible="False" />
                                                        <asp:BoundField DataField="datID" HeaderText="Permanent Datum" />
                                                        <asp:BoundField DataField="wpdrID" HeaderText="Depth Reference" />
                                                        <asp:BoundField DataField="wpiElev" HeaderText="Depth Ref. Elevation" />
                                                        <asp:BoundField DataField="luID" HeaderText="Units" />
                                                        <asp:BoundField DataField="wpiAzm" HeaderText="Vertical Section Azimuth" />
                                                        <asp:BoundField DataField="wpiNorthing" HeaderText="Northing" />
                                                        <asp:BoundField DataField="wpiNorthingUnit" HeaderText="Unit" />
                                                        <asp:BoundField DataField="wpiEasting" HeaderText="Easting" />
                                                        <asp:BoundField DataField="wpiEastingUnit" HeaderText="Unit" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="divlblWellPlanCalc" runat="server" style="padding-top: 4px; padding-bottom: 1px">
                                                <asp:Label ID="lblWellPlanCalc" runat="server" CssClass="label label-info text-center" Font-Size="Larger" Text="Well Plan"
                                                    BackColor="DarkBlue" ForeColor="DarkBlue" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <asp:GridView ID="gvwWellPlanCalc" runat="server" CssClass="mydatagrid" AllowPaging="True" AutoGenerateColumns="False"
                                                    DataKeyNames="wpcID" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15"
                                                    HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                    EmptyDataText="Unable to load data from Well Plan File" OnPageIndexChanging="gvwWellPlanCalc_PageIndexChanging">
                                                    <Columns>
                                                        <asp:BoundField DataField="wpcID" Visible="false" />
                                                        <asp:BoundField DataField="md" HeaderText="Measured Depth" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="inc" HeaderText="Inclination°" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="azm" HeaderText="Azimuth°" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="tvd" HeaderText="TVD" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="cummDeltaNorthing" HeaderText="North" DataFormatString="{0:0.0000}" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="cummDeltaEasting" HeaderText="East" DataFormatString="{0:0.0000}" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="vs" HeaderText="Vertical Section" DataFormatString="{0:0.0000}" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="subsea" HeaderText="Subsea" DataFormatString="{0:0.0000}" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="dogleg100" HeaderText="DLS" DataFormatString="{0:0.0000}" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="northing" HeaderText="Northing" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="easting" HeaderText="Easting" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="div2" runat="server" style="padding-top: 4px; padding-bottom: 1px">
                                                <asp:Label ID="lblWPCTieIn" runat="server" CssClass="label label-info text-center" Font-Size="Larger" Text="Well Placement Tie-In"
                                                    BackColor="DarkBlue" ForeColor="DarkBlue" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <asp:GridView ID="gvwWPCTieIn" runat="server" CssClass="mydatagrid" AllowPaging="True" AutoGenerateColumns="False"
                                                    DataKeyNames="wpctiID" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15"
                                                    HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                    EmptyDataText="Unable to load Well Placement Tie-In data" OnPageIndexChanging="gvwWellPlaceCalc_PageIndexChanging">
                                                    <Columns>
                                                        <asp:BoundField DataField="wpctiID" Visible="false" />
                                                        <asp:BoundField DataField="MDepth" HeaderText="Measured Depth" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="Inc" HeaderText="Inclination°" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="Azm" HeaderText="Azimuth°" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="Tvd" HeaderText="TVD" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="NorthOffset" HeaderText="North Offset" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="EastOffset" HeaderText="East Offset" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="VS" HeaderText="Vertical Section" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="Subsea" HeaderText="Subsea" ItemStyle-HorizontalAlign="Right" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <asp:Label ID="lblWellPlaceCalc" runat="server" CssClass="label label-info text-center" Font-Size="Larger" Text="Well Placement Data"
                                                    BackColor="DarkBlue" ForeColor="DarkBlue" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <asp:GridView ID="gvwWellPlaceCalc" runat="server" CssClass="mydatagrid" AllowPaging="True" AutoGenerateColumns="False"
                                                    DataKeyNames="wpcdID" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15"
                                                    HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                    EmptyDataText="Unable to load data from Well Plan File" OnPageIndexChanging="gvwWellPlaceCalc_PageIndexChanging">
                                                    <Columns>
                                                        <asp:BoundField DataField="wpcdID" Visible="false" />
                                                        <asp:BoundField DataField="md" HeaderText="Measured Depth" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="inc" HeaderText="Inclination°" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="azm" HeaderText="Azimuth°" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="tvd" HeaderText="TVD" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="cummDeltaNorthing" HeaderText="North" DataFormatString="{0:0.0000}" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="cummDeltaEasting" HeaderText="East" DataFormatString="{0:0.0000}" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="vs" HeaderText="Vertical Section" DataFormatString="{0:0.0000}" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="subsea" HeaderText="Subsea" DataFormatString="{0:0.0000}" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="dogleg100" HeaderText="DLS" DataFormatString="{0:0.0000}" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="northing" HeaderText="Northing" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="easting" HeaderText="Easting" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="divlblWellRegistrations" runat="server" visible="false">
                                                <asp:Label ID="lblWellRegistrations" runat="server" CssClass="label label-info text-center" Font-Size="Larger" Text="SMART Service Registration"
                                                    BackColor="DarkBlue" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <asp:GridView ID="gvwWReg" runat="server" CssClass="mydatagrid" AllowPaging="True" AutoGenerateColumns="False"
                                                    DataKeyNames="wsrID" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15"
                                                    HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                    EmptyDataText="Unable to load Service Registrations" OnPageIndexChanging="gvwWReg_PageIndexChanging" OnRowDataBound="gvwWReg_RowDataBound">
                                                    <Columns>
                                                        <asp:BoundField DataField="wsrID" Visible="false" />
                                                        <asp:BoundField DataField="srvGrpID" HeaderText="Service Group" />
                                                        <asp:BoundField DataField="srvID" HeaderText="Service" />
                                                        <asp:BoundField DataField="isApproved" HeaderText="Registration Status" />
                                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d, yyyy --- hh:mm tt}" ReadOnly="True" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divWellAdd" runat="server" visible="false">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">
                                        <asp:Label ID="lblWellAddHeading" runat="server" class="panel-title" Text="Add New Well(s) / Lateral(s)" />
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon wellLabel bg-light-blue">Well Name</span>
                                                        <asp:TextBox ID="txtWellAddName" runat="server" CssClass="form-control" AutoComplete="off" placeholder="Well Name (required)" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon wellLabel bg-light-blue">UWI</span>
                                                        <asp:TextBox ID="txtWellAddUWI" runat="server" CssClass="form-control" AutoComplete="off" placeholder="Universal Well Identifier (UWI)" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon wellLabel bg-light-blue">API</span>
                                                        <asp:TextBox ID="txtAddWellAPI" runat="server" CssClass="form-control" AutoComplete="off" placeholder="American Petroleum Institute Identifier (API)" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon wellLabel bg-light-blue">Permit #</span>
                                                        <asp:TextBox ID="txtWellAddPermit" runat="server" CssClass="form-control" AutoComplete="off" placeholder="Permit Number (required)" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group date">
                                                        <span class="input-group-addon wellLabel bg-light-blue">Spud Date</span>
                                                        <asp:TextBox runat="server" ID="txtWellSpudDate" CssClass="form-control" AutoComplete="off"
                                                            placeholder="Spud Date (required)" />
                                                        <ajaxToolkit:CalendarExtender ID="clndrWellSpudDate" runat="server" TargetControlID="txtWellSpudDate" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group date">
                                                        <span class="input-group-addon wellLabel bg-light-blue">Magnetic Model</span>
                                                        <div class="form-control" style="width: 100%;">
                                                            <asp:RadioButton ID="rdbBGGM" runat="server" CssClass="radio-inline" Checked="false" Text="BGGM" AutoPostBack="true" OnCheckedChanged="rdbBGGM_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbHDGM" runat="server" CssClass="radio-inline" Checked="false" Text="HDGM" AutoPostBack="true" OnCheckedChanged="rdbHDGM_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbIFR" runat="server" CssClass="radio-inline" Checked="false" Text="IFR" AutoPostBack="true" OnCheckedChanged="rdbIFR_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbIGRF" runat="server" CssClass="radio-inline" Checked="false" Text="IGRF" AutoPostBack="true" OnCheckedChanged="rdbIGRF_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbIIFR" runat="server" CssClass="radio-inline" Checked="false" Text="IIFR" AutoPostBack="true" OnCheckedChanged="rdbIIFR_CheckedChanged" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                    <div class="input-group date">
                                                        <span class="input-group-addon wellLabel bg-light-blue">Coordinate System</span>
                                                        <asp:DropDownList ID="ddlAddJobCord" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control"
                                                            Enabled="False" OnSelectedIndexChanged="ddlAddJobCord_SelectedIndexChanged">
                                                            <asp:ListItem Value="0" Text="--- Select Coordinate System ---" />
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                    <div class="input-group date">
                                                        <span class="input-group-addon wellLabel bg-light-blue">Projection</span>
                                                        <asp:DropDownList ID="ddlAddJobProj" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control"
                                                            Enabled="False" OnSelectedIndexChanged="ddlAddJobProj_SelectedIndexChanged">
                                                            <asp:ListItem Value="0" Text="--- Select Projection ---" />
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                    <div class="input-group">
                                                        <span class="input-group-addon wellLabel bg-light-blue">Latitude</span>
                                                        <div id="divLatO" runat="server" class="form-control bg-gray-light" style="width: 100%;">
                                                            <asp:RadioButton ID="rdbWNorth" runat="server" CssClass="radio-inline" Checked="false" Text="North (N)" Enabled="false" AutoPostBack="true"
                                                                OnCheckedChanged="rdbWNorth_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbWSouth" runat="server" CssClass="radio-inline" Checked="false" Text="South (S)" Enabled="false" AutoPostBack="true"
                                                                OnCheckedChanged="rdbWSouth_CheckedChanged" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                    <asp:TextBox ID="txtWellAddLat" runat="server" CssClass="form-control" AutoComplete="off" Enabled="false"
                                                        placeholder="Latitude (-90 to 90 degrees) (required)" />
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                    <div class="input-group">
                                                        <span class="input-group-addon wellLabel bg-light-blue">Longitude</span>
                                                        <div id="divLngO" runat="server" class="form-control bg-gray-light" style="width: 100%;">
                                                            <asp:RadioButton ID="rdbWEast" runat="server" CssClass="radio-inline" Checked="false" Text="East (E)" Enabled="false" AutoPostBack="true"
                                                                OnCheckedChanged="rdbWEast_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbWWest" runat="server" CssClass="radio-inline" Checked="false" Text="West (W)" Enabled="false" AutoPostBack="true"
                                                                OnCheckedChanged="rdbWWest_CheckedChanged" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                    <asp:TextBox ID="txtWellAddLon" runat="server" CssClass="form-control" CausesValidation="True" Enabled="false"
                                                        AutoComplete="off" placeholder="Longitude (-180 to 180 degrees) (required)" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div runat="server" id="wellEnclosure" visible="false">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <h4><i runat="server" id="iWellMessage"><span runat="server" id="spnWellHeader"></span></i></h4>
                                                        <asp:Label ID="lblWellSuccess" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="input-group" style="width: 100%;">
                                                        <asp:LinkButton ID="btnWellNew" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnWellNew_Click"><i class="fa fa-plus"> Well/Lateral</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnAddWellCancel" runat="server" CssClass="btn btn-warning text-center" OnClick="btnAddWellCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="btnWellAddDone" CssClass="btn btn-danger text-center" OnClick="btnWellAddDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divWellRF" runat="server" visible="false">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">
                                        <asp:Label ID="lblWellRF" runat="server" class="panel-title" Text="Add New Reference Magnetics" />
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon rfLabel bg-light-blue">IFR / IIFR</span>
                                                        <div class="form-control">
                                                            <asp:RadioButton ID="rdbIFRYes" runat="server" AutoPostBack="true" Text="Yes" Checked="false" CssClass="radio-inline" OnCheckedChanged="rdbIFRYes_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbIFRNo" runat="server" AutoPostBack="true" Text="No" Checked="false" CssClass="radio-inline" OnCheckedChanged="rdbIFRNo_CheckedChanged" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon rfLabel bg-light-blue">Convergence</span>
                                                        <div id="divConv" runat="server" class="form-control bg-gray-light">
                                                            <asp:RadioButton ID="rdbCTrue" runat="server" AutoPostBack="true" Text="True" Enabled="false" Checked="false" CssClass="radio-inline"
                                                                OnCheckedChanged="rdbCTrue_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbCGrid" runat="server" AutoPostBack="true" Text="Grid" Enabled="false" Checked="false" CssClass="radio-inline"
                                                                OnCheckedChanged="rdbCGrid_CheckedChanged" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group" style="width: 100%;">
                                                        <div id="divConvD" runat="server" class="form-control bg-gray-light">
                                                            <asp:RadioButton ID="rdbGEast" runat="server" AutoPostBack="true" Text="East of Central Meridian" Enabled="false" Checked="false"
                                                                CssClass="radio-inline" OnCheckedChanged="rdbGEast_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbGWest" runat="server" AutoPostBack="true" Text="West of Central Meridian" Enabled="false" Checked="false"
                                                                CssClass="radio-inline" OnCheckedChanged="rdbGWest_CheckedChanged" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" style="padding-left: 10px; padding-right: 0px">
                                                    <asp:TextBox ID="txtGConv" runat="server" CssClass="form-control" Enabled="false" AutoComplete="off" placeholder="Grid Convergence - (Degree - °) ... (required)" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon rfLabel bg-light-blue">Mag. Dec.</span>
                                                        <div id="divMDEW" runat="server" class="form-control bg-gray-light">
                                                            <asp:RadioButton ID="rdbMDEast" runat="server" AutoPostBack="true" Text="East (E)" Enabled="false" Checked="false" CssClass="radio-inline"
                                                                OnCheckedChanged="rdbMDEast_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbMDWest" runat="server" AutoPostBack="true" Text="West (W)" Enabled="false" Checked="false" CssClass="radio-inline"
                                                                OnCheckedChanged="rdbMDWest_CheckedChanged" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <asp:TextBox ID="txtMDec" runat="server" CssClass="form-control has-feedback" Enabled="false" AutoComplete="off" placeholder="Magnetic Declination .. (required)" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon rfLabel bg-light-blue">Dip</span>
                                                        <asp:TextBox ID="txtDip" runat="server" CssClass="form-control has-feedback" Enabled="false" AutoComplete="off" placeholder="Dip (Degrees -°) .. (required)" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon rfLabel bg-light-blue">B-Total</span>
                                                        <asp:DropDownList ID="ddlRFBTotalUnits" runat="server" CssClass="form-control" Enabled="false" AutoPostBack="true" AppendDataBoundItems="true"
                                                            OnSelectedIndexChanged="ddlRFBTotalUnits_SelectedIndexChanged">
                                                            <asp:ListItem Text="--- Select B-Total Units ---" Value="0"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <asp:TextBox ID="txtBT" runat="server" CssClass="form-control has-feedback" Enabled="false" AutoComplete="off" placeholder="B-Total (required)" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon rfLabel bg-light-blue">G-Total</span>
                                                        <asp:DropDownList ID="ddlRFGTotalUnits" runat="server" CssClass="form-control" Enabled="false" AutoPostBack="true" AppendDataBoundItems="true"
                                                            OnSelectedIndexChanged="ddlRFGTotalUnits_SelectedIndexChanged">
                                                            <asp:ListItem Text="--- Select G-Total Units ---" Value="0"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <asp:TextBox ID="txtGT" runat="server" CssClass="form-control has-feedback" Enabled="false" AutoComplete="off" placeholder="G-Total (required)" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div id="rfEnclosure" runat="server" visible="false">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <h4><i runat="server" id="iRFMessage"><span runat="server" id="spnRF"></span></i></h4>
                                                        <asp:Label runat="server" ID="lblRMSuccess" CssClass="lblSuccess" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="input-group" style="width: 100%;">
                                                        <asp:LinkButton ID="btnAddRefMag" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnAddRefMag_Click"><i class="fa fa-plus"> Reference Magnetics</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnAddRefMagCancel" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnAddRefMagCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnAddRefMagDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnAddRefMagDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divWellPlan" runat="server" visible="false">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">
                                        <asp:Label ID="lblPlanHeading" runat="server" class="panel-title" Text="Add New Well(s) / Lateral(s) Plan" />
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="panel panel-danger panel-body">
                                                            <ol id="olPlan" runat="server" class="stepper" data-stepper-steps="4">
                                                                <li id="liMI" runat="server" class="stepper-todo">Meta Info</li>
                                                                <li id="liTI" runat="server" class="stepper-todo">Tie-In</li>
                                                                <li id="liFile" runat="server" class="stepper-todo">Upload Well Plan</li>
                                                                <li id="liPlan" runat="server" class="stepper-todo">Well Plan</li>
                                                            </ol>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divMetaInfo" runat="server" class="panel panel-danger panel-body">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue planLabel">Permanent Datum</span>
                                                                <div class="form-control" style="width: 100%;">
                                                                    <asp:RadioButton ID="rdbGE" runat="server" AutoPostBack="true" Text="Ground Elevation (GE)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbGE_CheckedChanged" />
                                                                    <asp:RadioButton ID="rdbMSL" runat="server" AutoPostBack="true" Text="Mean Sea Level (MSL)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbMSL_CheckedChanged" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                                            <div id="divDepthRef" runat="server" class="input-group">
                                                                <span class="input-group-addon bg-light-blue planLabel">Depth Ref.</span>
                                                                <div id="divDR" runat="server" class="form-control bg-gray-light" style="width: 100%;">
                                                                    <asp:RadioButton ID="rdbGElev" runat="server" AutoPostBack="true" Text="Ground Elevation (GE)" Checked="false" Enabled="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbGElev_CheckedChanged" />
                                                                    <asp:RadioButton ID="rdbKB" runat="server" AutoPostBack="true" Text="Kelly Bushing (KB)" Checked="false" Enabled="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbKB_CheckedChanged" />
                                                                    <asp:RadioButton ID="rdbRF" runat="server" AutoPostBack="true" Text="Rig Floor (RF)" Checked="false" Enabled="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbRF_CheckedChanged" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue planLabel">Depth Ref. Elev.</span>
                                                                <div id="divElevUnit" runat="server" class="bg-gray-light form-control" style="width: 100%;">
                                                                    <asp:RadioButton ID="rdbElFeet" runat="server" AutoPostBack="true" Text="Feet (ft)" Checked="false" Enabled="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbElFeet_CheckedChanged" />
                                                                    <asp:RadioButton ID="rdbElMeter" runat="server" AutoPostBack="true" Text="Meter (m)" Checked="false" Enabled="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbElMeter_CheckedChanged" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                                            <asp:TextBox ID="txtWPDRElevation" runat="server" CssClass="form-control" Enabled="false" Text="" placeholder="Depth Reference Elevation .. (required)"
                                                                AutoComplete="off" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue planLabel">Length Unit</span>
                                                                <div id="divVSUnit" runat="server" class="bg-gray-light form-control" style="width: 100%;">
                                                                    <asp:RadioButton ID="rdbVSFeet" runat="server" AutoPostBack="true" Text="Feet (ft)" Checked="false" Enabled="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbVSFeet_CheckedChanged" />
                                                                    <asp:RadioButton ID="rdbVSMeter" runat="server" AutoPostBack="true" Text="Meter (m)" Checked="false" Enabled="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbVSMeter_CheckedChanged" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <asp:TextBox ID="txtWPVSAzimuth" runat="server" CssClass="form-control" Enabled="false" Text="" placeholder="Vertical Section Azimuth - (°) ... (required)"
                                                                AutoComplete="off" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue planLabel">Northing</span>
                                                                <div id="divN" runat="server" class="bg-gray-light form-control" style="width: 100%;">
                                                                    <asp:RadioButton ID="rdbNFeet" runat="server" AutoPostBack="true" Text="Feet (ft)" Checked="false" Enabled="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbNFeet_CheckedChanged" />
                                                                    <asp:RadioButton ID="rdbNMeter" runat="server" AutoPostBack="true" Text="Meter (m)" Checked="false" Enabled="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbNMeter_CheckedChanged" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <asp:TextBox ID="txtWPNorthing" runat="server" CssClass="form-control" Enabled="false" Text="" placeholder="Northing - (Decimal) ... (required)"
                                                                AutoComplete="off" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue planLabel">Easting</span>
                                                                <div id="divE" runat="server" class="bg-gray-light form-control" style="width: 100%;">
                                                                    <asp:RadioButton ID="rdbEFeet" runat="server" AutoPostBack="true" Text="Feet (ft)" Checked="false" Enabled="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbEFeet_CheckedChanged" />
                                                                    <asp:RadioButton ID="rdbEMeter" runat="server" AutoPostBack="true" Text="Meter (m)" Checked="false" Enabled="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbEMeter_CheckedChanged" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <asp:TextBox ID="txtWPEasting" runat="server" CssClass="form-control" Enabled="false" Text="" placeholder="Easting - (Decimal) ... (required)"
                                                                AutoComplete="off" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="input-group" style="width: 100%;">
                                                                <asp:LinkButton ID="btnWPInfoNext" runat="server" Enabled="false" CssClass="btn btn-default text-center" OnClick="btnWPInfoNext_Click"><i class="fa fa-forward"> Next</i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnWPInfoReset" runat="server" Enabled="false" CssClass="btn btn-default text-center" OnClick="btnWPInfoReset_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divPlanTieIn" runat="server" class="panel panel-danger panel-body" visible="false">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="form-control">
                                                                <asp:CheckBox ID="chkPlanAllZero" runat="server" CssClass="checkbox-inline" Text="All Zero" AutoPostBack="true" Checked="false"
                                                                    OnCheckedChanged="chkPlanAllZero_CheckedChanged" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue planLabel">Measured Depth</span>
                                                                <div id="divMD" runat="server" class="form-control bg-gray-light" style="width: 100%;">
                                                                    <asp:RadioButton ID="rdbMDUnitFT" runat="server" AutoPostBack="true" Text="Feet (ft)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbMDUnitFT_CheckedChanged" />
                                                                    <asp:RadioButton ID="rdbMDUnitM" runat="server" AutoPostBack="true" Text="Meter (m)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbMDUnitM_CheckedChanged" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                            <asp:TextBox ID="txtTIMD" runat="server" CssClass="form-control" Text="" AutoComplete="off" Enabled="false" placeholder="Tie-In Measured Depth ... (required)" />
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue planLabel">Vertical Depth</span>
                                                                <div id="divVD" runat="server" class="form-control bg-gray-light" style="width: 100%;">
                                                                    <asp:RadioButton ID="rdbTVDFt" runat="server" AutoPostBack="true" Text="Feet (ft)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbTVDFt_CheckedChanged" Enabled="false" />
                                                                    <asp:RadioButton ID="rdbTVDM" runat="server" AutoPostBack="true" Text="Meter (m)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbTVDM_CheckedChanged" Enabled="false" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                            <asp:TextBox ID="txtTITVD" runat="server" CssClass="form-control" Text="" Enabled="false" AutoComplete="off" placeholder="Vertical Depth ... (required)" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue planLabel">N. Offset</span>
                                                                <div id="divNOff" runat="server" class="form-control bg-gray-light" style="width: 100%;">
                                                                    <asp:RadioButton ID="rdbNOFt" runat="server" AutoPostBack="true" Text="Feet (ft)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbNOFt_CheckedChanged" Enabled="false" />
                                                                    <asp:RadioButton ID="rdbNOM" runat="server" AutoPostBack="true" Text="Meter (m)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbNOM_CheckedChanged" Enabled="false" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                            <asp:TextBox ID="txtTINorth" runat="server" CssClass="form-control" Text="" Enabled="false" AutoComplete="off" placeholder="North Offset ... (required)" />
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue planLabel">E. Offset</span>
                                                                <div id="divEOff" runat="server" class="form-control bg-gray-light" style="width: 100%;">
                                                                    <asp:RadioButton ID="rdbEOFt" runat="server" AutoPostBack="true" Text="Feet (ft)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbEOFt_CheckedChanged" Enabled="false" />
                                                                    <asp:RadioButton ID="rdbEOM" runat="server" AutoPostBack="true" Text="Meter (m)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbEOM_CheckedChanged" Enabled="false" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                            <asp:TextBox ID="txtTIEast" runat="server" CssClass="form-control" Text="" Enabled="false" AutoComplete="off" placeholder="East Offset ... (required)" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue planLabel">Vertical Section</span>
                                                                <div id="divVSec" runat="server" class="form-control bg-gray-light" style="width: 100%;">
                                                                    <asp:RadioButton ID="rdbVSecFt" runat="server" AutoPostBack="true" Text="Feet (ft)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbVSecFt_CheckedChanged" Enabled="false" />
                                                                    <asp:RadioButton ID="rdbVSMtr" runat="server" AutoPostBack="true" Text="Meter (m)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbVSMtr_CheckedChanged" Enabled="false" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                            <asp:TextBox ID="txtVS" runat="server" CssClass="form-control" Text="" Enabled="false" AutoComplete="off" placeholder="Vertical Section ... (required)" />
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue planLabel">Dogleg Rate</span>
                                                                <div id="divDLRate" runat="server" class="form-control bg-gray-light" style="width: 100%;">
                                                                    <asp:RadioButton ID="rdbDLSFt" runat="server" AutoPostBack="true" Text="°/100 Ft" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbDLSFt_CheckedChanged" Enabled="false" />
                                                                    <asp:RadioButton ID="rdbDLSM" runat="server" AutoPostBack="true" Text="°/100 M" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbDLSM_CheckedChanged" Enabled="false" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                            <asp:TextBox ID="txtDGLG" runat="server" CssClass="form-control" Text="" Enabled="false" AutoComplete="off" placeholder="Dogleg Rate ... (required)" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue planLabel">Subsea Elevation</span>
                                                                <div id="divSSElevUnit" runat="server" class="form-control bg-gray-light" style="width: 100%;">
                                                                    <asp:RadioButton ID="rdbSSDFt" runat="server" AutoPostBack="true" Text="Feet (ft)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbSSDFt_CheckedChanged" Enabled="false" />
                                                                    <asp:RadioButton ID="rdbSSDM" runat="server" AutoPostBack="true" Text="Meter (m)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbSSDM_CheckedChanged" Enabled="false" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                            <asp:TextBox ID="txtSS" runat="server" CssClass="form-control" Text="" Enabled="false" AutoComplete="off" placeholder="Subsea Elevation ... (required)" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="input-group" style="width: 100%;">
                                                                <asp:LinkButton ID="btnTINext" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnTINext_Click"><i class="fa fa-forward"> Next</i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnTIReset" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnTIReset_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divPlanFile" runat="server" class="panel panel-danger panel-body" visible="false">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue planLabel">Upload File</span>
                                                                <asp:FileUpload ID="FileUploadControl" runat="server" CssClass="form-control" Enabled="false" placeholder="Upload Well Plan" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div runat="server" id="processWPData">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <asp:GridView ID="gvwRawWellPlan" runat="server" CssClass="mydatagrid" AllowPaging="True" AutoGenerateColumns="False"
                                                                DataKeyNames="datarowID" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                                EmptyDataText="Unable to load data from Well Plan File" OnPageIndexChanging="gvwRawWellPlan_PageIndexChanging">
                                                                <Columns>
                                                                    <asp:BoundField DataField="datarowID" Visible="false" />
                                                                    <asp:BoundField DataField="md" HeaderText="Measured Depth" />
                                                                    <asp:BoundField DataField="inc" HeaderText="Inclination°" />
                                                                    <asp:BoundField DataField="azm" HeaderText="Azimuth°" />
                                                                    <asp:BoundField DataField="tvd" HeaderText="TVD" />
                                                                    <asp:BoundField DataField="sselev" HeaderText="Subsea Elevation" />
                                                                    <asp:BoundField DataField="noff" HeaderText="N. Offset" />
                                                                    <asp:BoundField DataField="eoff" HeaderText="E. Offset" />
                                                                    <asp:BoundField DataField="dls" HeaderText="DLS" />
                                                                    <asp:BoundField DataField="tface" HeaderText="T. Face" />
                                                                    <asp:BoundField DataField="vs" HeaderText="VS" />
                                                                    <asp:BoundField DataField="north" HeaderText="Northing" />
                                                                    <asp:BoundField DataField="east" HeaderText="Easting" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div runat="server" id="divWellPlanCalc">
                                                                <asp:GridView ID="gvwWPCalc" runat="server" CssClass="mydatagrid" AllowPaging="True" AutoGenerateColumns="False"
                                                                    DataKeyNames="datarowID" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                    HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                                    EmptyDataText="Unable to load data from Well Plan File" OnPageIndexChanging="gvwWPCalc_PageIndexChanging">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="datarowID" Visible="false" />
                                                                        <asp:BoundField DataField="md" HeaderText="Measured Depth" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                                        <asp:BoundField DataField="inc" HeaderText="Inclination°" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                                        <asp:BoundField DataField="azm" HeaderText="Azimuth°" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                                        <asp:BoundField DataField="tvd" HeaderText="TVD" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                                        <asp:BoundField DataField="cummNorthing" HeaderText="North" DataFormatString="{0:0.0000}" ItemStyle-HorizontalAlign="Right" />
                                                                        <asp:BoundField DataField="cummEasting" HeaderText="East" DataFormatString="{0:0.0000}" ItemStyle-HorizontalAlign="Right" />
                                                                        <asp:BoundField DataField="vs" HeaderText="Vertical Section" DataFormatString="{0:0.0000}" ItemStyle-HorizontalAlign="Right" />
                                                                        <asp:BoundField DataField="subsea" HeaderText="Subsea" DataFormatString="{0:0.0000}" ItemStyle-HorizontalAlign="Right" />
                                                                        <asp:BoundField DataField="dogleg100" HeaderText="DLS" DataFormatString="{0:0.0000}" ItemStyle-HorizontalAlign="Right" />
                                                                        <asp:BoundField DataField="northing" HeaderText="Northing" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                                        <asp:BoundField DataField="easting" HeaderText="Easting" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div runat="server" id="planEnclosure" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iPlan"><span runat="server" id="spnPlan"></span></i></h4>
                                                            <asp:Label ID="lblPlanSuccess" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group" style="width: 100%;">
                                                            <asp:LinkButton ID="btnWPUpload" runat="server" CssClass="btn btn-default text-center" Visible="false" Enabled="false" OnClick="btnWPUpload_Click"><i class="fa fa-upload"> Upload File</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnWPAddClear" runat="server" CssClass="btn btn-default text-center" Visible="false" Enabled="false"
                                                                OnClick="btnWPAddClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnWPProcess" runat="server" CssClass="btn btn-default text-center" Text="Process Well Plan" Visible="false"
                                                                OnClick="btnWPProcess_Click"><i class="fa fa-check"> Process</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnWPProcessRest" runat="server" CssClass="btn btn-default text-center" Visible="false" OnClick="btnWPProcessRest_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnWPDone" runat="server" CssClass="btn btn-danger text-center" Visible="false" OnClick="btnWPDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divWellPlacement" runat="server" visible="false">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">
                                        <asp:Label ID="lblPlacementHeading" runat="server" class="panel-title" Text="Add New Well(s) / Lateral(s) Placement Data" />
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="panel panel-danger panel-body">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <ol id="olTIPlacement" runat="server" class="stepper" data-stepper-steps="3">
                                                                    <li id="liWP1" runat="server" class="stepper-todo">Tie-In</li>
                                                                    <li id="liWP2" runat="server" class="stepper-todo">Upload Well Placement Data</li>
                                                                    <li id="liWP3" runat="server" class="stepper-todo">Confirmation</li>
                                                                </ol>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divTIPlacement" runat="server" class="panel panel-danger panel-body">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-control">
                                                                <asp:CheckBox ID="chkTIPlacementFeet" runat="server" CssClass="checkbox-inline" Text="All Zero (Feet)" AutoPostBack="true"
                                                                    Checked="false" OnCheckedChanged="chkTIPlacementFeet_CheckedChanged" />
                                                                <asp:CheckBox ID="chkTIPlacementMeter" runat="server" CssClass="checkbox-inline" Text="All Zero (Meter)" AutoPostBack="true"
                                                                    Checked="false" OnCheckedChanged="chkTIPlacementMeter_CheckedChanged" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue planLabel">Measured Depth</span>
                                                                <div id="divTIPMD" runat="server" class="form-control" style="width: 100%;">
                                                                    <asp:RadioButton ID="rdbTIPFeet" runat="server" AutoPostBack="true" Text="Feet (ft)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbTIPFeet_CheckedChanged" />
                                                                    <asp:RadioButton ID="rdbTIPMeter" runat="server" AutoPostBack="true" Text="Meter (m)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbTIPMeter_CheckedChanged" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                            <asp:TextBox ID="txtTIPMD" runat="server" CssClass="form-control" Text="" AutoComplete="off" Enabled="false" placeholder="Tie-In Measured Depth ... (required)" />
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue planLabel">Inclination</span>
                                                                <asp:TextBox ID="txtTIPInc" runat="server" CssClass="form-control" Text="" AutoComplete="off" Enabled="false" placeholder="Tie-In Inclination° ... (required)" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue planLabel">Azimuth</span>
                                                                <asp:TextBox ID="txtTIPAzm" runat="server" CssClass="form-control" Text="" AutoComplete="off" Enabled="false" placeholder="Tie-In Azimuth° ... (required)" />
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue planLabel">Vertical Depth</span>
                                                                <div id="divTIPVD" runat="server" class="form-control bg-gray-light" style="width: 100%;">
                                                                    <asp:RadioButton ID="rdbTIPVDFeet" runat="server" AutoPostBack="true" Text="Feet (ft)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbTIPVDFeet_CheckedChanged" Enabled="false" />
                                                                    <asp:RadioButton ID="rdbTIPVDMeter" runat="server" AutoPostBack="true" Text="Meter (m)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbTIPVDMeter_CheckedChanged" Enabled="false" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                            <asp:TextBox ID="txtTIPVertDepth" runat="server" CssClass="form-control" Text="" Enabled="false" AutoComplete="off" placeholder="Vertical Depth ... (required)" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <span class="input-group-addon bg-light-blue planLabel">N. Offset</span>
                                                                <div id="divTIPNOff" runat="server" class="form-control bg-gray-light" style="width: 100%;">
                                                                    <asp:RadioButton ID="rdbTIPNOffFeet" runat="server" AutoPostBack="true" Text="Feet (ft)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbTIPNOffFeet_CheckedChanged" Enabled="false" />
                                                                    <asp:RadioButton ID="rdbTIPNOffMeter" runat="server" AutoPostBack="true" Text="Meter (m)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbTIPNOffMeter_CheckedChanged" Enabled="false" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                            <asp:TextBox ID="txtTIPNOffset" runat="server" CssClass="form-control" Text="" Enabled="false" AutoComplete="off" placeholder="North Offset ... (required)" />
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue planLabel">E. Offset</span>
                                                                <div id="divTIPEOff" runat="server" class="form-control bg-gray-light" style="width: 100%;">
                                                                    <asp:RadioButton ID="rdbTIPEOffFeet" runat="server" AutoPostBack="true" Text="Feet (ft)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbTIPEOffFeet_CheckedChanged" Enabled="false" />
                                                                    <asp:RadioButton ID="rdbTIPEOffMeter" runat="server" AutoPostBack="true" Text="Meter (m)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbTIPEOffMeter_CheckedChanged" Enabled="false" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                            <asp:TextBox ID="txtTIPEOffset" runat="server" CssClass="form-control" Text="" Enabled="false" AutoComplete="off" placeholder="East Offset ... (required)" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue planLabel">Vertical Section</span>
                                                                <div id="divTIPVS" runat="server" class="form-control bg-gray-light" style="width: 100%;">
                                                                    <asp:RadioButton ID="rdbTIPVSFeet" runat="server" AutoPostBack="true" Text="Feet (ft)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbTIPVSFeet_CheckedChanged" Enabled="false" />
                                                                    <asp:RadioButton ID="rdbTIPVSMeter" runat="server" AutoPostBack="true" Text="Meter (m)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbTIPVSMeter_CheckedChanged" Enabled="false" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                            <asp:TextBox ID="txtTIPVSection" runat="server" CssClass="form-control" Text="" Enabled="false" AutoComplete="off" placeholder="Vertical Section ... (required)" />
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue planLabel">Subsea Elevation</span>
                                                                <div id="divTIPSS" runat="server" class="form-control bg-gray-light" style="width: 100%;">
                                                                    <asp:RadioButton ID="rdbTIPSSFeet" runat="server" AutoPostBack="true" Text="Feet (ft)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbTIPSSFeet_CheckedChanged" Enabled="false" />
                                                                    <asp:RadioButton ID="rdbTIPSSMeter" runat="server" AutoPostBack="true" Text="Meter (m)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbTIPSSMeter_CheckedChanged" Enabled="false" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                            <asp:TextBox ID="txtTIPSSElev" runat="server" CssClass="form-control" Text="" Enabled="false" AutoComplete="off" placeholder="Subsea Elevation ... (required)" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divTIPFile" runat="server" class="panel panel-danger panel-body" visible="false">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue planLabel">Upload File</span>
                                                                <asp:FileUpload ID="tipFileUpload" runat="server" CssClass="form-control" Enabled="false" placeholder="Upload Well Placement Data" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div runat="server" id="divPlacementData" visible="false">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <asp:GridView ID="gvwPlacement" runat="server" CssClass="mydatagrid" AllowPaging="True" AutoGenerateColumns="False"
                                                                DataKeyNames="datarowID" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                                HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                                EmptyDataText="Unable to load data from Well Plan File" OnPageIndexChanging="gvwPlacement_PageIndexChanging">
                                                                <Columns>
                                                                    <asp:BoundField DataField="datarowID" Visible="false" />
                                                                    <asp:BoundField DataField="md" HeaderText="Measured Depth" />
                                                                    <asp:BoundField DataField="inc" HeaderText="Inclination°" />
                                                                    <asp:BoundField DataField="azm" HeaderText="Azimuth°" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div id="divMDButtons" runat="server" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="input-group">
                                                                <span class="input-group-addon bg-light-blue planLabel">Measured Depth Unit</span>
                                                                <div id="divMDRadioButtons" runat="server" class="form-control" style="width: 100%;">
                                                                    <asp:RadioButton ID="rdbDataMDFeet" runat="server" AutoPostBack="true" Text="Feet (ft)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbDataMDFeet_CheckedChanged" />
                                                                    <asp:RadioButton ID="rdbDataMDMeter" runat="server" AutoPostBack="true" Text="Meter (m)" Checked="false"
                                                                        CssClass="radio-inline" OnCheckedChanged="rdbDataMDMeter_CheckedChanged" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <asp:GridView ID="gvwWPCalculated" runat="server" CssClass="mydatagrid" AllowPaging="True" AutoGenerateColumns="False"
                                                                DataKeyNames="datarowID" AlternatingRowStyle-CssClass="AlternatingRowStyle" Visible="false"
                                                                HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" ShowHeaderWhenEmpty="true"
                                                                EmptyDataText="Unable to load data for Well Placement" OnPageIndexChanging="gvwWPCalculated_PageIndexChanging">
                                                                <Columns>
                                                                    <asp:BoundField DataField="datarowID" Visible="false" />
                                                                    <asp:BoundField DataField="plcMD" HeaderText="Measured Depth" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="plcInc" HeaderText="Inclination°" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="plcAzm" HeaderText="Azimuth°" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="plcTvd" HeaderText="TVD" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="plcCummNorthing" HeaderText="North" DataFormatString="{0:0.0000}" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="plcCummEasting" HeaderText="East" DataFormatString="{0:0.0000}" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="plcVs" HeaderText="Vertical Section" DataFormatString="{0:0.0000}" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="plcSubsea" HeaderText="Subsea" DataFormatString="{0:0.0000}" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="plcDogleg100" HeaderText="DLS" DataFormatString="{0:0.0000}" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="plcNorthing" HeaderText="Northing" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="plcEasting" HeaderText="Easting" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div runat="server" id="encPlacement" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iPlace"><span runat="server" id="spnPlace"></span></i></h4>
                                                            <asp:Label ID="lblPlacementSuccess" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group" style="width: 100%;">
                                                            <asp:LinkButton ID="btnTIPNext" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnTIPNext_Click"><i class="fa fa-forward"> Next</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnTIPReset" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnTIPReset_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnPFUpload" runat="server" CssClass="btn btn-default text-center" Visible="false" Enabled="false" OnClick="btnPFUpload_Click"><i class="fa fa-upload"> Upload File</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnPFClear" runat="server" CssClass="btn btn-default text-center" Visible="false" Enabled="false" OnClick="btnPFClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnPFProcess" runat="server" CssClass="btn btn-default text-center" Text="Process Well Placement Data"
                                                                Visible="false" Enabled="false" OnClick="btnPFProcess_Click"><i class="fa fa-check"> Process</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnPFProcessReset" runat="server" CssClass="btn btn-default text-center" Visible="false" OnClick="btnPFProcessReset_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnPFProcessDone" runat="server" CssClass="btn btn-danger text-center" Visible="false" OnClick="btnPFProcessDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                            <div id="divWellStatus" runat="server" visible="false">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">
                                        <asp:Label ID="lblWStatusHeading" runat="server" class="panel-title" Text="Edit Well(s) / Lateral(s) Status" />
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue">Well Status</span>
                                                        <asp:DropDownList runat="server" ID="ddlWSWellStatus" CssClass="form-control"
                                                            AutoPostBack="True" OnSelectedIndexChanged="ddlWSWellStatus_SelectedIndexChanged" AppendDataBoundItems="True">
                                                            <asp:ListItem Value="0" Text="--- Select Well Status ---" />
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div runat="server" id="wstEnclosure" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iWST"><span runat="server" id="spnWST"></span></i></h4>
                                                            <asp:Label ID="lblWST" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group" style="width: 100%;">
                                                            <asp:LinkButton ID="btnWSTUpdate" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnWSTUpdate_Click"><i
                                                            class="fa fa-wrench"> Update</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnWSTClear" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnWSTClear_Click"><i
                                                            class="fa fa-refresh"> Refresh</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnWSTDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnWSTDone_Click"><i
                                                            class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divWRegister" runat="server" visible="false">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">
                                        <asp:Label ID="lblWRegister" runat="server" class="panel-title" Text="SMARTs Well(s) / Lateral(s) Registration" />
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue">Service Group</span>
                                                        <asp:DropDownList runat="server" ID="ddlWRSrvGroup" CssClass="form-control col-xs-6 col-sm-6 col-md-6 col-lg-6"
                                                            AutoPostBack="True" OnSelectedIndexChanged="ddlWRSrvGroup_SelectedIndexChanged" AppendDataBoundItems="True">
                                                            <asp:ListItem Value="0" Text="--- Select SMARTs Service Group ---" />
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue">Service</span>
                                                        <asp:DropDownList runat="server" ID="ddlWRService" CssClass="form-control col-xs-6 col-sm-6 col-md-6 col-lg-6"
                                                            AutoPostBack="True" Enabled="false" OnSelectedIndexChanged="ddlWRService_SelectedIndexChanged" AppendDataBoundItems="True">
                                                            <asp:ListItem Value="0" Text="--- Select SMARTs Service ---" />
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue">Contract</span>
                                                        <asp:DropDownList runat="server" ID="ddlClientContract" CssClass="form-control col-xs-6 col-sm-6 col-md-6 col-lg-6"
                                                            AutoPostBack="True" Enabled="false" OnSelectedIndexChanged="ddlClientContract_SelectedIndexChanged" AppendDataBoundItems="True">
                                                            <asp:ListItem Value="0" Text="--- Select Contract ---" />
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div runat="server" id="enReg" visible="false">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <h4><i runat="server" id="iReg"><span runat="server" id="spnReg"></span></i></h4>
                                                        <asp:Label ID="lblSrvRegSuccess" runat="server" Text="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="input-group" style="width: 100%;">
                                                        <asp:LinkButton ID="btnAddWReg" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnAddWReg_Click"><i class="fa fa-pencil-square"> Register for Service</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnAddWRegClear" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnAddWRegClear_Click"><i class="fa fa-refresh"> Refresh</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnAddWRegDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnAddWRegDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divSection" runat="server" visible="false">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">
                                        <asp:Label ID="lblSectionList" runat="server" class="panel-title" Text="Well/Lateral Section List" />
                                    </div>
                                    <div class="panel-body">
                                        <div class="panel-body">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:LinkButton ID="btnSectionAdd" runat="server" CssClass="btn btn-info text-center" OnClick="btnSectionAdd_Click"><i class="fa fa-plus"> Add Well/Lateral Section</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnSectionReset" runat="server" CssClass="btn btn-default text=center" Enabled="false" OnClick="btnSectionReset_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnSectionStepBack" runat="server" CssClass="btn btn-warning text-center" OnClick="btnSectionStepBack_Click"><i class="fa fa-undo"> Step Back</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnSectionExit" runat="server" CssClass="btn btn-danger text-center" OnClick="btnExit_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="table-responsive">
                                                    <asp:GridView ID="gvwSectionList" runat="server" CssClass="mydatagrid" EmptyDataText="No Well/Lateral Sections found"
                                                        AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="wswID" ShowHeaderWhenEmpty="True"
                                                        AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                        RowStyle-CssClass="rows" OnPageIndexChanging="gvwSectionList_PageIndexChanging" OnSelectedIndexChanged="gvwSectionList_SelectedIndexChanged">
                                                        <Columns>
                                                            <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                            </asp:ButtonField>
                                                            <asp:BoundField DataField="wswID" Visible="false" />
                                                            <asp:BoundField DataField="wsName" HeaderText="Well Section" />
                                                            <asp:BoundField DataField="rnCount" HeaderText="Associated Run Count" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divSectionAdd" runat="server" visible="false">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">
                                        <asp:Label ID="lblSectionAdd" runat="server" class="panel-title" Text="Add New Well/Lateral Section" />
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="table-responsive">
                                                <div class="input-group">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <span runat="server" id="spnWSName" class="input-group-addon bg-light-blue">Well Section</span>
                                                        <asp:DropDownList ID="ddlNWSWSection" runat="server" CssClass="form-control" AutoPostBack="True" AppendDataBoundItems="True"
                                                            OnSelectedIndexChanged="ddlNWSWSection_SelectedIndexChanged">
                                                            <asp:ListItem Value="0" Text="--- Select Well/Lateral Section ---" />
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div id="wsEnclosure" runat="server" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iWSMessage"><span runat="server" id="spnWS"></span></i></h4>
                                                            <asp:Label runat="server" ID="lblNWSWSuccess" CssClass="lblSuccess" />
                                                        </div>
                                                    </div>
                                                    <div class=" col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group" style="width: 100%;">
                                                            <asp:LinkButton ID="btnNWSWAdd" runat="server" Enabled="false" CssClass="btn btn-default text-center" OnClick="btnNWSWAdd_Click"><i class="fa fa-plus"> Well Section</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnNWSWClear" runat="server" Enabled="false" CssClass="btn btn-default text-center" OnClick="btnNWSWClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnNWSWDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnNWSWDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divRunList" runat="server" visible="false">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">
                                        <asp:Label ID="lblRunList" runat="server" class="panel-title" Text="Run(s) List" />
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="input-group" style="width: 100%;">
                                                        <asp:LinkButton ID="btnRunAdd" runat="server" CssClass="btn btn-info text-center" OnClick="btnRunAdd_Click"><i class="fa fa-plus"> Add Run</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnRunReset" runat="server" CssClass="btn btn-default text=center" Enabled="false" OnClick="btnRunReset_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnRunStepBack" runat="server" CssClass="btn btn-warning text-center" OnClick="btnRunStepBack_Click"><i class="fa fa-undo"> Step Back</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnRunExit" runat="server" CssClass="btn btn-danger text-center" OnClick="btnExit_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <asp:HiddenField ID="hdnRunID" runat="server" />
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <asp:GridView ID="gvwRunList" runat="server" CssClass="mydatagrid" EmptyDataText="No RUN(s) found"
                                                        AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="runID" ShowHeaderWhenEmpty="True"
                                                        AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                        RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow" OnPageIndexChanging="gvwRunList_PageIndexChanging">
                                                        <Columns>
                                                            <asp:BoundField DataField="runID" Visible="false" />
                                                            <asp:BoundField DataField="runName" HeaderText="Run" />
                                                            <asp:BoundField DataField="Inc" HeaderText="Inclination°" ItemStyle-HorizontalAlign="Right" />
                                                            <asp:BoundField DataField="Azm" HeaderText="Azimuth°" ItemStyle-HorizontalAlign="Right" />
                                                            <asp:BoundField DataField="TInc" HeaderText="Proposed End-Run Inclination°" ItemStyle-HorizontalAlign="Right" />
                                                            <asp:BoundField DataField="TAzm" HeaderText="Proposed End-Run Azimuth°" ItemStyle-HorizontalAlign="Right" />
                                                            <asp:BoundField DataField="stat" HeaderText="Status" />
                                                            <asp:BoundField DataField="tcID" HeaderText="Toolcode" />
                                                            <asp:BoundField DataField="sqcID" HeaderText="Survey Qualification Criteria" />
                                                            <asp:BoundField DataField="sDepth" HeaderText="Start M. Depth" ItemStyle-HorizontalAlign="Right" />
                                                            <asp:BoundField DataField="eDepth" HeaderText="End M. Depth" ItemStyle-HorizontalAlign="Right" />
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                                <ItemTemplate>
                                                                    <asp:Button runat="server" CssClass="btn btn-info text-center" Text="View Detail" CausesValidation="false"
                                                                        ID="btnRunDetails" OnClick="btnRunDetails_Click"></asp:Button>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                                <ItemTemplate>
                                                                    <asp:Button runat="server" CssClass="btn btn-info text-center" Text="Add Tie-In Survey" CausesValidation="false"
                                                                        ID="btnAddTieIn" OnClick="btnAddTieIn_Click"></asp:Button>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                                <ItemTemplate>
                                                                    <asp:Button runat="server" CssClass="btn btn-info text-center" Text="Add BHA" CausesValidation="false"
                                                                        ID="btnAddBHA" OnClick="btnAddBHA_Click"></asp:Button>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                                <ItemTemplate>
                                                                    <asp:Button runat="server" CssClass="btn btn-info text-center" Text="Update Status" CausesValidation="false"
                                                                        ID="btnRunStatus" OnClick="btnRunStatus_Click"></asp:Button>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="divlblRTIDetail" runat="server" visible="false">
                                                    <asp:Label ID="lblRTIDetail" runat="server" BackColor="LightBlue" Text="Run Tie-In Survey" Font-Size="Larger"></asp:Label>    
                                                   </div>                                            
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="divgvwRTI" runat="server" visible="false">
                                                    <asp:GridView ID="gvwRTI" runat="server" CssClass="mydatagrid" EmptyDataText="No Tie-In Survey found for selected Run"
                                                        AutoGenerateColumns="False" DataKeyNames="rtiID" ShowHeaderWhenEmpty="True" HeaderStyle-CssClass="header" RowStyle-CssClass="rows">
                                                        <Columns>
                                                            <asp:BoundField DataField="rtiID" Visible="false" />
                                                            <asp:BoundField DataField="MDepth" HeaderText="Measured Depth" ItemStyle-HorizontalAlign="Right" />
                                                            <asp:BoundField DataField="Inc" HeaderText="Inclination°" ItemStyle-HorizontalAlign="Right" />
                                                            <asp:BoundField DataField="Azm" HeaderText="Azimuth°" ItemStyle-HorizontalAlign="Right" />
                                                            <asp:BoundField DataField="TVD" HeaderText="TVD" ItemStyle-HorizontalAlign="Right" />
                                                            <asp:BoundField DataField="North" HeaderText="North" ItemStyle-HorizontalAlign="Right" />
                                                            <asp:BoundField DataField="East" HeaderText="East" ItemStyle-HorizontalAlign="Right" />
                                                            <asp:BoundField DataField="VS" HeaderText="Vertical Section" ItemStyle-HorizontalAlign="Right" />
                                                            <asp:BoundField DataField="Subsea" HeaderText="Subsea Elevation" ItemStyle-HorizontalAlign="Right" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <asp:Label ID="lblBHADetail" runat="server" Text="Bottom-Hole-Assembly (BHA) Signature"
                                                        Font-Size="Larger" Visible="false" BackColor="LightBlue" for="gvwBHA"></asp:Label>                                                
                                                    <asp:GridView ID="gvwBHA" runat="server" CssClass="mydatagrid" AllowPaging="True" AutoGenerateColumns="False"
                                                        EmptyDataText="No BHA Signature(s) found." DataKeyNames="bhaID"
                                                        AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                        Visible="false"
                                                        RowStyle-CssClass="rows" SelectedRowStyle-CssClass="selectedrow" ShowHeaderWhenEmpty="true">
                                                        <Columns>
                                                            <asp:BoundField DataField="bhaID" Visible="False" />
                                                            <asp:BoundField DataField="bhaName" HeaderText="BHA Signature Name" />
                                                            <asp:BoundField DataField="bhaMSize" HeaderText="Motor Size" ItemStyle-HorizontalAlign="Right" />
                                                            <asp:BoundField DataField="bhaMSizeUID" HeaderText="Unit" />
                                                            <asp:BoundField DataField="bhaDSCSize" HeaderText="Drill String/Collar Size" ItemStyle-HorizontalAlign="Right" />
                                                            <asp:BoundField DataField="bhaDSCUID" HeaderText="Unit" />
                                                            <asp:BoundField DataField="bhaMDBLen" HeaderText="Motor + Drill Bit Length" ItemStyle-HorizontalAlign="Right" />
                                                            <asp:BoundField DataField="bhaMDBLenUID" HeaderText="Unit" />
                                                            <asp:BoundField DataField="bhaNMSpAbv" HeaderText="Non-Mag Space (Above)" ItemStyle-HorizontalAlign="Right" />
                                                            <asp:BoundField DataField="bhaNMSAUID" HeaderText="Unit" />
                                                            <asp:BoundField DataField="bhaNMSpBlw" HeaderText="Non-Mag Space (Below)" ItemStyle-HorizontalAlign="Right" />
                                                            <asp:BoundField DataField="bhaNMSBUID" HeaderText="Unit" />
                                                            <asp:BoundField DataField="dmgstID" HeaderText="Status" />
                                                            <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy hh:mm tt}" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divRunAdd" runat="server" visible="false">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">
                                        <asp:Label ID="lblRunAdd" runat="server" class="panel-title" Text="Add New Run" />
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon sqcLabel bg-light-blue">Run</span>
                                                        <asp:DropDownList ID="ddlActRun" runat="server" CssClass="form-control" AutoPostBack="True" AppendDataBoundItems="True"
                                                            OnSelectedIndexChanged="ddlActRun_SelectedIndexChanged">
                                                            <asp:ListItem Value="0" Text="--- Select Run ---" />
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon sqcLabel bg-light-blue">SQC</span>
                                                        <asp:DropDownList ID="ddlActRunSQC" runat="server" CssClass="form-control" AutoPostBack="True" AppendDataBoundItems="True"
                                                            Enabled="False" OnSelectedIndexChanged="ddlActRunSQC_SelectedIndexChanged">
                                                            <asp:ListItem Value="0" Text="--- Select Survey Qualification Criteria ---" />
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon sqcLabel bg-light-blue">Inclination</span>
                                                        <asp:TextBox ID="txtActRunInc" runat="server" CssClass="form-control" Text="" Enabled="False" AutoComplete="off"
                                                            placeholder="Run Inclination° (required)" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon sqcLabel bg-light-blue">Azimuth</span>
                                                        <asp:TextBox ID="txtActRunAzm" runat="server" CssClass="form-control" Text="" Enabled="False" AutoComplete="off"
                                                            placeholder="Run Azimuth° (required)" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon sqcLabel bg-light-blue">End-Run Inc.</span>
                                                        <asp:TextBox ID="txtActRunTInc" runat="server" CssClass="form-control" Text="" Enabled="False" AutoComplete="off"
                                                            placeholder="Proposed End-Run Inclination° (required)" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon sqcLabel bg-light-blue">End-Run Azm.</span>
                                                        <asp:TextBox ID="txtActRunTAzm" runat="server" CssClass="form-control" Text="" Enabled="False" AutoComplete="off"
                                                            placeholder="Proposed End-Run Azimuth° (required)" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon sqcLabel bg-light-blue">Depth Unit</span>
                                                        <asp:DropDownList ID="ddlRunDepthUnit" runat="server" CssClass="form-control" AutoPostBack="True" AppendDataBoundItems="True"
                                                            Enabled="False" OnSelectedIndexChanged="ddlRunDepthUnit_SelectedIndexChanged">
                                                            <asp:ListItem Value="0" Text="--- Select Depth Unit ---" />
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon sqcLabel bg-light-blue">Start M. Depth</span>
                                                        <asp:TextBox ID="txtRunStartD" runat="server" CssClass="form-control" Text="" Enabled="False" AutoComplete="off"
                                                            placeholder="Start Depth --- (required)" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon sqcLabel bg-light-blue">End M. Depth</span>
                                                        <asp:TextBox ID="txtRunEndD" runat="server" CssClass="form-control" Text="" Enabled="False" AutoComplete="off"
                                                            placeholder="End Depth --- (required)" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon sqcLabel bg-light-blue">Toolcode</span>
                                                        <asp:DropDownList ID="ddlActRunToolCode" runat="server" CssClass="form-control" AutoPostBack="True" AppendDataBoundItems="True"
                                                            Enabled="False" OnSelectedIndexChanged="ddlActRunToolCode_SelectedIndexChanged">
                                                            <asp:ListItem Value="0" Text="--- Select ToolCode ---" />
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon sqcLabel bg-light-blue">Status</span>
                                                        <asp:DropDownList ID="ddlActRunStatus" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                            Enabled="false" OnSelectedIndexChanged="ddlActRunStatus_SelectedIndexChanged">
                                                            <asp:ListItem Value="0" Text="--- Select Run Status ---" />
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div id="runEnclosure" runat="server" visible="false">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <h4><i runat="server" id="iRNMessage"><span runat="server" id="spnRun"></span></i></h4>
                                                        <asp:Label runat="server" ID="lblActRunSuccess" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="input-group" style="width: 100%;">
                                                        <asp:LinkButton ID="btnActRunAdd" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnActRunAdd_Click"><i class="fa fa-plus"> Run</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnActRunCancel" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnActRunCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnActRunDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnActRunDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divRunTieIn" runat="server" visible="false">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">
                                        <asp:Label ID="lblTIHeading" runat="server" class="panel-title" Text="Add Tie-In" />
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <div class="row">
                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon tiLabel bg-light-blue">Measured Depth</span>
                                                        <div id="divRTIMD" runat="server" class="form-control" style="width: 100%;">
                                                            <asp:RadioButton ID="rdbRTIMDFt" runat="server" AutoPostBack="true" Checked="false" CssClass="radio-inline" Text="Feet (ft)"
                                                                OnCheckedChanged="rdbRTIMDFt_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbRTIMDM" runat="server" AutoPostBack="true" Checked="false" CssClass="radio-inline" Text="Meter (m)"
                                                                OnCheckedChanged="rdbRTIMDM_CheckedChanged" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                                                    <asp:TextBox ID="txtRTIMD" runat="server" CssClass="form-control rtAligned" Font-Size="Small" Text="" AutoComplete="off"
                                                        Enabled="false"
                                                        placeholder="Measured Depth (0.00) (required)" />
                                                </div>
                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon tiLabel bg-light-blue">Tie-In Inclination</span>
                                                        <asp:TextBox ID="txtRTIInc" runat="server" CssClass="form-control rtAligned" Text="" AutoComplete="off" Enabled="false"
                                                            placeholder="Tie-In Inclination° .. (required)" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon tiLabel bg-light-blue">Tie-In TVD</span>
                                                        <div id="divRTITVD" runat="server" class="form-control bg-gray-light">
                                                            <asp:RadioButton ID="rdbRTITVDFt" runat="server" AutoPostBack="true" Enabled="false" Checked="false" CssClass="radio-inline"
                                                                Text="Feet (ft)" OnCheckedChanged="rdbRTITVDFt_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbRTITVDM" runat="server" AutoPostBack="true" Enabled="false" Checked="false" CssClass="radio-inline"
                                                                Text="Meter (m)" OnCheckedChanged="rdbRTITVDM_CheckedChanged" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
                                                    <asp:TextBox ID="txtRTITVD" runat="server" CssClass="form-control rtAligned" Text="" AutoComplete="off" Enabled="false"
                                                        placeholder="Tie-In TVD .. (required)" Font-Size="Small" />
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon tiLabel bg-light-blue">Tie-In Azimuth</span>
                                                        <asp:TextBox ID="txtRTIAzm" runat="server" CssClass="form-control rtAligned" Text="" AutoComplete="off" Enabled="false"
                                                            placeholder="Tie-In Azimuth° .. (required)" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon tiLabel bg-light-blue">Tie-In North</span>
                                                        <div id="divRTINorth" runat="server" class="form-control bg-gray-light" style="width: 100%;">
                                                            <asp:RadioButton ID="rdbRTINFt" runat="server" AutoPostBack="true" Enabled="false" Checked="false" CssClass="radio-inline"
                                                                Text="Feet (ft)" OnCheckedChanged="rdbRTINFt_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbRTINM" runat="server" AutoPostBack="true" Enabled="false" Checked="false" CssClass="radio-inline"
                                                                Text="Meter (m)" OnCheckedChanged="rdbRTINM_CheckedChanged" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
                                                    <asp:TextBox ID="txtRTINorth" runat="server" CssClass="form-control rtAligned" Text="" AutoComplete="off" Enabled="false"
                                                        placeholder="Tie-In North .. (required)" />
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon tiLabel bg-light-blue">Tie-In East</span>
                                                        <div id="divRTIEast" runat="server" class="form-control bg-gray-light" style="width: 100%;">
                                                            <asp:RadioButton ID="rdbRTIEFt" runat="server" AutoPostBack="true" Enabled="false" Checked="false" CssClass="radio-inline"
                                                                Text="Feet (ft)" OnCheckedChanged="rdbRTIEFt_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbRTIEM" runat="server" AutoPostBack="true" Enabled="false" Checked="false" CssClass="radio-inline"
                                                                Text="Meter (m)" OnCheckedChanged="rdbRTIEM_CheckedChanged" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
                                                    <asp:TextBox ID="txtRTIEast" runat="server" CssClass="form-control rtAligned" Text="" AutoComplete="off" Enabled="false"
                                                        placeholder="Tie-In East .. (required)" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon tiLabel bg-light-blue">Tie-In Vertical Section</span>
                                                        <div id="divRTIVS" runat="server" class="form-control bg-gray-light" style="width: 100%;">
                                                            <asp:RadioButton ID="rdbRTIVSFt" runat="server" AutoPostBack="true" Enabled="false" Checked="false" CssClass="radio-inline"
                                                                Text="Feet (ft)" OnCheckedChanged="rdbRTIVSFt_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbRTIVSM" runat="server" AutoPostBack="true" Enabled="false" Checked="false" CssClass="radio-inline"
                                                                Text="Meter (m)" OnCheckedChanged="rdbRTIVSM_CheckedChanged" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
                                                    <asp:TextBox ID="txtRTIVS" runat="server" CssClass="form-control rtAligned" Text="" AutoComplete="off" Enabled="false"
                                                        placeholder="Tie-In Vert Section .. (required)" />
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon tiLabel bg-light-blue">Tie-In Subsea Elevation</span>
                                                        <div id="divRTISS" runat="server" class="form-control bg-gray-light" style="width: 100%;">
                                                            <asp:RadioButton ID="rdbRTISSFt" runat="server" AutoPostBack="true" Enabled="false" Checked="false" CssClass="radio-inline"
                                                                Text="Feet (ft)" OnCheckedChanged="rdbRTISSFt_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbRTISSM" runat="server" AutoPostBack="true" Enabled="false" Checked="false" CssClass="radio-inline"
                                                                Text="Meter (m)" OnCheckedChanged="rdbRTISSM_CheckedChanged" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
                                                    <asp:TextBox ID="txtRTISubsea" runat="server" CssClass="form-control rtAligned" Text="" AutoComplete="off" Enabled="false"
                                                        placeholder="Tie-In Subsea E.. (required)" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div id="rtiEnclosure" runat="server" visible="false">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <h4><i runat="server" id="iRTI"><span runat="server" id="spnRTI"></span></i></h4>
                                                        <asp:Label runat="server" ID="lblRTI" Text="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="input-group" style="width: 100%;">
                                                        <asp:LinkButton ID="btnRTIAdd" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnRTIAdd_Click"><i class="fa fa-plus"> Tie-in</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnRTIClear" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnRTIClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnTRIDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnTRIDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divBHA" runat="server" visible="false">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">
                                        <asp:Label ID="lblBHAHeading" runat="server" class="panel-title" Text="Add Bottom-Hole-Assembly (BHA) Signature" />
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bhaLabel bg-light-blue">BHA ID</span>
                                                        <asp:TextBox ID="txtBHAName" runat="server" CssClass="form-control" AutoComplete="off" placeholder="BHA Signature ID ... (required)" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bhaLabel bg-light-blue">Motor Size</span>
                                                        <div id="divMtrSize" runat="server" class="form-control" style="width: 100%;">
                                                            <asp:RadioButton ID="rdbMSUIn" runat="server" AutoPostBack="true" Text="Inch (in)" Checked="false"
                                                                CssClass="radio-inline" OnCheckedChanged="rdbMSUIn_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbMSUMm" runat="server" AutoPostBack="true" Text="Millimeter (mm)" Checked="false"
                                                                CssClass="radio-inline" OnCheckedChanged="rdbMSUMm_CheckedChanged" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <asp:TextBox ID="txtAddBHAMSize" runat="server" CssClass="form-control rtAligned" Enabled="False" AutoComplete="off" placeholder="BHA Motor Size ... (required)" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bhaLabel bg-light-blue">Drill String/Collar Size</span>
                                                        <div id="divClrSize" runat="server" class="form-control bg-gray-light" style="width: 100%;">
                                                            <asp:RadioButton ID="rdbCSUIn" runat="server" AutoPostBack="true" Text="Inch (in)" Checked="false"
                                                                CssClass="radio-inline" OnCheckedChanged="rdbCSUIn_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbCSUMm" runat="server" AutoPostBack="true" Text="Millimeter (mm)" Checked="false"
                                                                CssClass="radio-inline" OnCheckedChanged="rdbCSUMm_CheckedChanged" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <asp:TextBox ID="txtBHADSC" runat="server" CssClass="form-control rtAligned" Enabled="False" AutoComplete="off" placeholder="BHA Drill String/Collar Size ... (required)" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bhaLabel bg-light-blue">Motor + Drill Bit Length</span>
                                                        <div id="divMDB" runat="server" class="form-control bg-gray-light" style="width: 100%;">
                                                            <asp:RadioButton ID="rdbMDBInch" runat="server" AutoPostBack="true" Text="Inch (in)" Checked="false"
                                                                CssClass="radio-inline" OnCheckedChanged="rdbMDBInch_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbMDBFeet" runat="server" AutoPostBack="true" Text="Feet (ft)" Checked="false"
                                                                CssClass="radio-inline" OnCheckedChanged="rdbMDBFeet_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbMDBMeter" runat="server" AutoPostBack="true" Text="Meter (m)" Checked="false"
                                                                CssClass="radio-inline" OnCheckedChanged="rdbMDBMeter_CheckedChanged" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <asp:TextBox ID="txtBHAMDBLen" runat="server" CssClass="form-control rtAligned" Enabled="False" AutoComplete="off" placeholder="Motor + Drill-bit Length ... (required)" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bhaLabel bg-light-blue">Space Above</span>
                                                        <div id="divAbv" runat="server" class="form-control bg-gray-light" style="width: 100%;">
                                                            <asp:RadioButton ID="rdbAbvIn" runat="server" AutoPostBack="true" Text="Inch (in)" Checked="false"
                                                                CssClass="radio-inline" OnCheckedChanged="rdbAbvIn_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbAbvFt" runat="server" AutoPostBack="true" Text="Feet (ft)" Checked="false"
                                                                CssClass="radio-inline" OnCheckedChanged="rdbAbvFt_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbAbvMt" runat="server" AutoPostBack="true" Text="Meter (m)" Checked="false"
                                                                CssClass="radio-inline" OnCheckedChanged="rdbAbvMt_CheckedChanged" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <asp:TextBox ID="txtBHAAbv" runat="server" CssClass="form-control rtAligned" Enabled="False" AutoComplete="off" placeholder="Non-Mag Space Above ... (required)" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bhaLabel bg-light-blue">Space Below</span>
                                                        <div id="divBlw" runat="server" class="form-control bg-gray-light" style="width: 100%;">
                                                            <asp:RadioButton ID="rdbBlwIn" runat="server" AutoPostBack="true" Text="Inch (in)" Checked="false"
                                                                CssClass="radio-inline" OnCheckedChanged="rdbBlwIn_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbBlwFt" runat="server" AutoPostBack="true" Text="Feet (ft)" Checked="false"
                                                                CssClass="radio-inline" OnCheckedChanged="rdbBlwFt_CheckedChanged" />
                                                            <asp:RadioButton ID="rdbBlwMt" runat="server" AutoPostBack="true" Text="Meter (m)" Checked="false"
                                                                CssClass="radio-inline" OnCheckedChanged="rdbBlwMt_CheckedChanged" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <asp:TextBox ID="txtBHABlw" runat="server" CssClass="form-control rtAligned" Enabled="False" AutoComplete="off" placeholder="Non-Mag Space Below ... (required)" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div id="bhaEnclosure" runat="server" visible="false">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <h4><i runat="server" id="iBHAMessage"><span runat="server" id="spnBHA"></span></i></h4>
                                                        <asp:Label runat="server" ID="lblBHASuccess" Text="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="input-group" style="width: 100%;">
                                                        <asp:LinkButton ID="btnBHAAddNew" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnBHAAddNew_Click"><i class="fa fa-plus"> BHA Signature</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnBHAAddNewCancel" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnBHAAddNewCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnBHAAddDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnBHAAddDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divRunStatus" runat="server" visible="false">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">
                                        <asp:Label ID="lblRStat" runat="server" class="panel-title" Text="Update Run Status" />
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon bhaLabel bg-light-blue">Run Status</span>
                                                    <asp:DropDownList ID="ddlRunStatus" runat="server" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true"
                                                        OnSelectedIndexChanged="ddlRunStatus_SelectedIndexChanged">
                                                        <asp:ListItem Text="--- Select Run Status ---" Value="0"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div id="rstEnclosure" runat="server" visible="false">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                    <h4><i runat="server" id="iRST"><span runat="server" id="spnRST"></span></i></h4>
                                                    <asp:Label runat="server" ID="lblRST" Text="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group">
                                                    <asp:LinkButton ID="btnUpdateRunStatus" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnUpdateRunStatus_Click"><i class="fa fa-plus"> Update Run Status</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnUpdateRunStatusClear" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnUpdateRunStatusClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnUpdateRunStatusDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnUpdateRunStatusDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnWPUpload" />
                            <asp:PostBackTrigger ControlID="btnPFUpload" />
                            <asp:PostBackTrigger ControlID="btnDisplayChartsWellPlan" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</div>
