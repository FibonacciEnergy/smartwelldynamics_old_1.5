﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI.WebControls;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using TA = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis;
using Color = System.Drawing.Color;

namespace SmartsVer1.Control.Config.TA
{
    public partial class ctrlFETrendAnalysis : System.Web.UI.UserControl
    {
        String LoginName, username, domain, GetClientDBString;
        Int32 ClientID = -99;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();
                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);
                username = addr.User;
                domain = addr.Host;
                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(LoginName);
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
                //Datasources for page controls
                System.Data.DataTable taBTTable = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getBTotalParametersTable();
                this.gvwBTList.DataSource = taBTTable;
                this.gvwBTList.DataBind();
                System.Data.DataTable taGTTable = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getGTotalParametersTable();
                this.gvwGTList.DataSource = taGTTable;
                this.gvwGTList.DataBind();
                System.Data.DataTable taDipTable = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getDipParametersTable();
                this.gvwDipList.DataSource = taDipTable;
                this.gvwDipList.DataBind();
                System.Data.DataTable taIncTable = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getIncParametersTable();
                this.gvwIncList.DataSource = taIncTable;
                this.gvwIncList.DataBind();
                System.Data.DataTable taDBzTable = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getDBzParametersTable();
                this.gvwDBzList.DataSource = taDBzTable;
                this.gvwDBzList.DataBind();
                System.Data.DataTable taSCAZTable = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getSCAZParametersTable();
                this.gvwSCAZ.DataSource = taSCAZTable;
                this.gvwSCAZ.DataBind();
                System.Data.DataTable boxyTable = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getBoxyParametersTable();
                this.gvwBoxy.DataSource = boxyTable;
                this.gvwBoxy.DataBind();
                System.Data.DataTable tacrTable = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getDFLimitTable();
                this.gvwTACr.DataSource = tacrTable;
                this.gvwTACr.DataBind();
                System.Data.DataTable tarmTable = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getRefMagParametersTable();
                this.gvwRMList.DataSource = tarmTable;
                this.gvwRMList.DataBind();
                System.Data.DataTable tanmTable = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getNMSParametersTable();
                this.gvwNMSpList.DataSource = tanmTable;
                this.gvwNMSpList.DataBind();
                Dictionary<Int32, String> taBTList = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getBTotalParametersList();
                this.ddlBT.Items.Clear();
                this.ddlBT.DataSource = taBTList;
                this.ddlBT.DataTextField = "Value";
                this.ddlBT.DataValueField = "Key";
                this.ddlBT.DataBind();
                this.ddlBT.Items.Insert(0, new ListItem("--- Select B-Total Parameter ---", "-1"));
                this.ddlBT.SelectedIndex = -1;
                Dictionary<Int32, String> taGTList = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getGTotalParametersList();
                this.ddlGT.Items.Clear();
                this.ddlGT.DataSource = taGTList;
                this.ddlGT.DataTextField = "Value";
                this.ddlGT.DataValueField = "Key";
                this.ddlGT.DataBind();
                this.ddlGT.Items.Insert(0, new ListItem("--- Select G-Total Parameter ---", "-1"));
                this.ddlGT.SelectedIndex = -1;
                Dictionary<Int32, String> taDipList = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getDipParametersList();
                this.ddlDip.Items.Clear();
                this.ddlDip.DataSource = taDipList;
                this.ddlDip.DataTextField = "Value";
                this.ddlDip.DataValueField = "Key";
                this.ddlDip.DataBind();
                this.ddlDip.Items.Insert(0, new ListItem("--- Select Dip Parameter ---", "-1"));
                this.ddlDip.SelectedIndex = -1;
                Dictionary<Int32, String> taIncList = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getIncParametersList();
                this.ddlInc.Items.Clear();
                this.ddlInc.DataSource = taIncList;
                this.ddlInc.DataTextField = "Value";
                this.ddlInc.DataValueField = "Key";
                this.ddlInc.DataBind();
                this.ddlInc.Items.Insert(0, new ListItem("--- Select Inclination Parameter ---", "-1"));
                this.ddlInc.SelectedIndex = -1;
                Dictionary<Int32, String> taDBzList = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getDBzParametersList();
                this.ddlDBz.Items.Clear();
                this.ddlDBz.DataSource = taDBzList;
                this.ddlDBz.DataTextField = "Value";
                this.ddlDBz.DataValueField = "Key";
                this.ddlDBz.DataBind();
                this.ddlDBz.Items.Insert(0, new ListItem("--- Select Delta-Bz Parameter ---", "-1"));
                this.ddlDBz.SelectedIndex = -1;
                Dictionary<Int32, String> taSCAZList = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getSCAZParametersList();
                this.ddlSCAZ.Items.Clear();
                this.ddlSCAZ.DataSource = taSCAZList;
                this.ddlSCAZ.DataTextField = "Value";
                this.ddlSCAZ.DataValueField = "Key";
                this.ddlSCAZ.DataBind();
                this.ddlSCAZ.Items.Insert(0, new ListItem("--- Select SCAZ Parameter ---", "-1"));
                this.ddlSCAZ.SelectedIndex = -1;
                Dictionary<Int32, String> tarmList = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getRefMagParametersList();
                this.ddlRM.Items.Clear();
                this.ddlRM.DataSource = tarmList;
                this.ddlRM.DataTextField = "Value";
                this.ddlRM.DataValueField = "Key";
                this.ddlRM.DataBind();
                this.ddlRM.Items.Insert(0, new ListItem("--- Select Ref.-Mags. Parameter ---", "-1"));
                this.ddlRM.SelectedIndex = -1;
                Dictionary<Int32, String> tanmList = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getNMSParametersList();
                this.ddlNMSp.Items.Clear();
                this.ddlNMSp.DataSource = tarmList;
                this.ddlNMSp.DataTextField = "Value";
                this.ddlNMSp.DataValueField = "Key";
                this.ddlNMSp.DataBind();
                this.ddlNMSp.Items.Insert(0, new ListItem("--- Select Ref.-Mags. Parameter ---", "-1"));
                this.ddlNMSp.SelectedIndex = -1;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwBTList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                this.btnBTCancel.Visible = true;
                System.Data.DataTable taBTTable = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getBTotalParametersTable();
                this.gvwBTList.PageIndex = e.NewPageIndex;
                this.gvwBTList.DataSource = taBTTable;
                this.gvwBTList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwBTList_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            try
            {
                this.btnBTCancel.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnBTAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.tblBTotalList.Visible = false;
                this.tblBTotalAdd.Visible = true;
                this.txtBTAdd.Text = "";
                this.lblBTSuccess.Text = "";
                this.lblBTSuccess.Visible = false;

                this.txtBTAdd.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnBTCancel_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable taBTTable = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getBTotalParametersTable();
                this.gvwBTList.DataSource = taBTTable;
                this.gvwBTList.DataBind();
                this.gvwBTList.SelectedIndex = -1;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnBTAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                String value = String.Empty;
                value = Convert.ToString(this.txtBTAdd.Text);
                if (String.IsNullOrEmpty(value))
                {
                    this.txtBTAdd.Text = "";
                    this.lblBTSuccess.Text = "Unable to insert value. Please try again.";
                    this.lblBTSuccess.ForeColor = Color.Maroon;
                    this.lblBTSuccess.Visible = true;
                }
                else
                {
                    iResult = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.insertBTParameter(value);
                    if (!iResult.Equals(1))
                    {
                        this.txtBTAdd.Text = "";
                        this.lblBTSuccess.Text = "Unable to insert value. Please try again.";
                        this.lblBTSuccess.ForeColor = Color.Maroon;
                        this.lblBTSuccess.Visible = true;
                    }
                    else
                    {
                        this.txtBTAdd.Text = "";
                        this.lblBTSuccess.Text = "Successfully insert new B-Total Parameter.";
                        this.lblBTSuccess.ForeColor = Color.Green;
                        this.lblBTSuccess.Visible = true;

                        this.btnBTAddDone.Focus();
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnBTAddClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtBTAdd.Text = "";
                this.lblBTSuccess.Text = "";
                this.lblBTSuccess.Visible = false;
                this.txtBTAdd.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnBTAddDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnBTAddClear_Click(null, null);
                this.tblBTotalList.Visible = true;
                this.tblBTotalAdd.Visible = false;
                System.Data.DataTable taBTTable = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getBTotalParametersTable();
                this.gvwBTList.DataSource = taBTTable;
                this.gvwBTList.DataBind();
                this.gvwBTList.SelectedIndex = -1;

                this.gvwBTList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwGTList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                this.btnGTCancel.Visible = true;
                System.Data.DataTable taGTTable = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getGTotalParametersTable();
                this.gvwGTList.PageIndex = e.NewPageIndex;
                this.gvwGTList.DataSource = taGTTable;
                this.gvwGTList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwGTList_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            try
            {
                this.btnGTCancel.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnGTAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.tblGTotalList.Visible = false;
                this.tblGTotalAdd.Visible = true;
                this.lblGTSuccess.Text = "";
                this.lblGTSuccess.Visible = false;

                this.txtGTAdd.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnGTCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.lblGTSuccess.Text = "";
                this.lblGTSuccess.Visible = false;
                this.txtGTAdd.Text = "";

                this.txtGTAdd.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnGTAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                String value = String.Empty;
                value = Convert.ToString(this.txtGTAdd.Text);
                if (String.IsNullOrEmpty(value))
                {
                    this.txtGTAdd.Text = "";
                    this.lblGTSuccess.Text = "Unable to insert value. Please try again.";
                    this.lblGTSuccess.ForeColor = Color.Maroon;
                    this.lblGTSuccess.Visible = true;
                }
                else
                {
                    iResult = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.insertGTParameter(value);
                    if (!iResult.Equals(1))
                    {
                        this.txtGTAdd.Text = "";
                        this.lblGTSuccess.Text = "Unable to insert value. Please try again.";
                        this.lblGTSuccess.ForeColor = Color.Maroon;
                        this.lblGTSuccess.Visible = true;
                    }
                    else
                    {
                        this.txtGTAdd.Text = "";
                        this.lblGTSuccess.Text = "Successfully insert new G-Total Parameter.";
                        this.lblGTSuccess.ForeColor = Color.Green;
                        this.lblGTSuccess.Visible = true;

                        this.btnAddGTDone.Focus();
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnGTAddClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtGTAdd.Text = "";
                this.lblGTSuccess.Text = "";
                this.lblGTSuccess.Visible = false;

                this.txtGTAdd.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddGTDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnGTAddClear_Click(null, null);
                this.tblGTotalList.Visible = true;
                this.tblGTotalAdd.Visible = false;
                System.Data.DataTable taGTTable = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getGTotalParametersTable();
                this.gvwGTList.DataSource = taGTTable;
                this.gvwGTList.DataBind();

                this.gvwGTList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDipAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.tblDipList.Visible = false;
                this.tblDipAdd.Visible = true;
                this.lblDipSuccess.Text = "";
                this.lblDipSuccess.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDipCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.lblDipSuccess.Text = "";
                this.lblDipSuccess.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDipAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                String value = String.Empty;
                value = Convert.ToString(this.txtDipAdd.Text);
                if (String.IsNullOrEmpty(value))
                {
                    this.txtDipAdd.Text = "";
                    this.lblDipSuccess.Text = "Unable to insert null value. Please try again";
                    this.lblDipSuccess.ForeColor = Color.Maroon;
                    this.lblDipSuccess.Visible = true;

                    this.txtDipAdd.Focus();
                }
                else
                {
                    iResult = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.insertDipParameter(value);
                    if (!iResult.Equals(1))
                    {
                        this.txtDipAdd.Text = "";
                        this.lblDipSuccess.Text = "Unable to insert value. Please try again";
                        this.lblDipSuccess.ForeColor = Color.Maroon;
                        this.lblDipSuccess.Visible = true;

                        this.txtDipAdd.Focus();
                    }
                    else
                    {
                        this.txtDipAdd.Text = "";
                        this.lblDipSuccess.Text = "Successfully inserted new Dip Parameter";
                        this.lblDipSuccess.ForeColor = Color.Green;
                        this.lblDipSuccess.Visible = true;

                        this.btnDipAddDone.Focus();
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDipAddClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtDipAdd.Text = "";
                this.lblDipSuccess.Text = "";
                this.lblDipSuccess.Visible = false;

                this.txtDipAdd.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDipAddDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnDipAddClear_Click(null, null);
                this.tblDipList.Visible = true;
                this.tblDipAdd.Visible = false;
                System.Data.DataTable taDipTable = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getDipParametersTable();
                this.gvwDipList.DataSource = taDipTable;
                this.gvwDipList.DataBind();
                this.gvwDipList.SelectedIndex = -1;

                this.gvwDipList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwIncList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable taIncTable = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getIncParametersTable();
                this.gvwIncList.DataSource = taIncTable;
                this.gvwIncList.DataBind();
                this.gvwIncList.SelectedIndex = -1;
                this.btnIncCancel.Visible = true;

                this.gvwIncList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwIncList_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            try
            {
                this.btnIncCancel.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnIncAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.tblIncList.Visible = false;
                this.tblIncAdd.Visible = true;
                this.lblIncSuccess.Text = "";
                this.lblIncSuccess.Visible = false;
                this.txtIncAdd.Text = "";

                this.txtIncAdd.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnIncCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.gvwIncList.SelectedIndex = -1;
                this.lblIncSuccess.Text = "";
                this.lblIncSuccess.Visible = false;
                this.btnIncCancel.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnIncAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                String value = String.Empty;
                value = Convert.ToString(this.txtIncAdd.Text);
                if (String.IsNullOrEmpty(value))
                {
                    this.txtIncAdd.Text = "";
                    this.lblIncSuccess.Text = "Unable to insert null parameter. Please try again";
                    this.lblIncSuccess.ForeColor = Color.Maroon;
                    this.lblIncSuccess.Visible = true;

                    this.txtIncAdd.Focus();
                }
                else
                {
                    iResult = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.insertIncParameter(value);
                    if (!iResult.Equals(1))
                    {
                        this.txtIncAdd.Text = "";
                        this.lblIncSuccess.Text = "Unable to insert new parameter. Please try again";
                        this.lblIncSuccess.ForeColor = Color.Maroon;
                        this.lblIncSuccess.Visible = true;

                        this.txtIncAdd.Focus();
                    }
                    else
                    {
                        this.txtIncAdd.Text = "";
                        this.lblIncSuccess.Text = "Successfully insert new Inclination parameter";
                        this.lblIncSuccess.ForeColor = Color.Green;
                        this.lblIncSuccess.Visible = true;

                        this.btnIncAddDone.Focus();
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnIncAddClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.lblIncSuccess.Text = "";
                this.lblIncSuccess.Visible = false;
                this.txtIncAdd.Text = "";

                this.txtIncAdd.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnIncAddDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.tblIncList.Visible = true;
                this.tblIncAdd.Visible = false;
                this.btnIncAddClear_Click(null, null);
                System.Data.DataTable taIncTable = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getIncParametersTable();
                this.gvwIncList.DataSource = taIncTable;
                this.gvwIncList.DataBind();

                this.gvwIncList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwDBzList_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            try
            {
                this.btnDBzCancel.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwDBzList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                this.btnDBzCancel.Visible = true;
                System.Data.DataTable taDBzTable = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getDBzParametersTable();
                this.gvwDBzList.DataSource = taDBzTable;
                this.gvwDBzList.PageIndex = e.NewPageIndex;
                this.gvwDBzList.DataBind();

                this.gvwDBzList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDBzAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.tblDBzList.Visible = false;
                this.tblDBzAdd.Visible = true;
                this.lblDBzSuccess.Text = "";
                this.lblDBzSuccess.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDBzCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.gvwDBzList.SelectedIndex = -1;
                this.lblDBzSuccess.Text = "";
                this.lblDBzSuccess.Visible = false;
                this.btnDBzCancel.Visible = false;

                this.gvwDBzList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDBzAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                String value = String.Empty;
                value = Convert.ToString(this.txtDBzAdd.Text);
                if (String.IsNullOrEmpty(value))
                {
                    this.txtDBzAdd.Text = "";
                    this.lblDBzSuccess.Text = "Unable to insert null parameter. Please try again";
                    this.lblDBzSuccess.ForeColor = Color.Maroon;
                    this.lblDBzSuccess.Visible = true;

                    this.txtDBzAdd.Focus();
                }
                else
                {
                    iResult = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.insertDBzParameter(value);
                    if (!iResult.Equals(1))
                    {
                        this.btnDBzAddClear_Click(null, null);
                        this.lblDBzSuccess.Text = "Unable to insert parameter. Please try again";
                        this.lblDBzSuccess.ForeColor = Color.Maroon;
                        this.lblDBzSuccess.Focus();

                        this.txtDBzAdd.Focus();
                    }
                    else
                    {
                        this.btnDBzAddClear_Click(null, null);
                        this.lblDBzSuccess.Text = "Successfully inserted new Delta-Bz Parameter";
                        this.lblDBzSuccess.ForeColor = Color.Green;
                        this.lblDBzSuccess.Visible = true;

                        this.btnDBzAddDone.Focus();
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDBzAddClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtDBzAdd.Text = "";
                this.lblDBzSuccess.Text = "";
                this.lblDBzSuccess.Visible = false;
                this.txtDBzAdd.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDBzAddDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnDBzAddClear_Click(null, null);
                this.tblDBzList.Visible = true;
                this.tblDBzAdd.Visible = false;
                System.Data.DataTable taDBzTable = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getDBzParametersTable();
                this.gvwDBzList.DataSource = taDBzTable;
                this.gvwDBzList.DataBind();
                this.gvwDBzList.SelectedIndex = -1;

                this.gvwDBzList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSCAZAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.tblSCAZList.Visible = false;
                this.tblSCAZAdd.Visible = true;
                this.lblSCAZSuccess.Text = "";
                this.lblSCAZSuccess.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSCAZCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.tblSCAZList.Visible = true;
                this.tblSCAZAdd.Visible = false;
                this.lblSCAZSuccess.Text = "";
                this.lblSCAZSuccess.Visible = false;
                this.btnSCAZCancel.Visible = false;
                this.gvwSCAZ.SelectedIndex = -1;

                this.gvwSCAZ.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSCAZ_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable taSCAZTable = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getSCAZParametersTable();
                this.gvwSCAZ.DataSource = taSCAZTable;
                this.gvwSCAZ.PageIndex = e.NewPageIndex;
                this.gvwSCAZ.DataBind();
                this.btnSCAZCancel.Visible = true;

                this.gvwSCAZ.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSCAZ_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            try
            {
                this.btnSCAZCancel.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSCAZAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                String value = String.Empty;
                value = Convert.ToString(this.txtSCAZAdd.Text);
                if (String.IsNullOrEmpty(value))
                {
                    this.txtSCAZAdd.Text = "";
                    this.lblSCAZSuccess.Text = "Unable to insert null Parameter value. Please try again";
                    this.lblSCAZAdd.ForeColor = Color.Maroon;
                    this.lblSCAZAdd.Visible = true;

                    this.txtSCAZAdd.Focus();
                }
                else
                {
                    iResult = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.insertSCAZParameter(value);
                    if (!iResult.Equals(1))
                    {
                        this.txtSCAZAdd.Text = "";
                        this.lblSCAZSuccess.Text = "Unable to insert Parameter value. Please try again";
                        this.lblSCAZAdd.ForeColor = Color.Maroon;
                        this.lblSCAZAdd.Visible = true;

                        this.txtSCAZAdd.Focus();
                    }
                    else
                    {
                        this.txtSCAZAdd.Text = "";
                        this.lblSCAZSuccess.Text = "Successfully inserted new Parameter value";
                        this.lblSCAZAdd.ForeColor = Color.Green;
                        this.lblSCAZAdd.Visible = true;

                        this.btnAddSCAZDone.Focus();
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSCAZAddClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtSCAZAdd.Text = "";
                this.lblSCAZSuccess.Text = "";
                this.lblSCAZSuccess.Visible = false;

                this.txtSCAZAdd.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddSCAZDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnSCAZAddClear_Click(null, null);
                this.tblSCAZAdd.Visible = false;
                this.tblSCAZList.Visible = true;
                System.Data.DataTable taSCAZTable = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getSCAZParametersTable();
                this.gvwSCAZ.DataSource = taSCAZTable;
                this.gvwSCAZ.DataBind();
                this.gvwSCAZ.SelectedIndex = -1;

                this.gvwSCAZ.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnNMSpAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.tblNMSpList.Visible = false;
                this.tblNMSpAdd.Visible = true;
                this.lblNMSpSuccess.Text = "";
                this.lblNMSpSuccess.Visible = false;
                this.txtNMAdd.Text = "";

                this.txtNMAdd.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnNMSpCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.lblNMSpSuccess.Text = "";
                this.lblNMSpSuccess.Visible = false;
                this.gvwNMSpList.SelectedIndex = -1;
                this.btnNMSpCancel.Visible = false;

                this.gvwNMSpList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNMSNew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                String value = String.Empty;
                value = Convert.ToString(this.txtNMAdd.Text);
                if (String.IsNullOrEmpty(value))
                {
                    this.txtNMAdd.Text = "";
                    this.lblNMSpSuccess.Text = "Unable to insert null parameter name. Please try again";
                    this.lblNMSpSuccess.ForeColor = Color.Maroon;
                    this.lblNMSpSuccess.Visible = true;
                    this.txtNMAdd.Focus();
                }
                else
                {
                    iResult = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.insertNMSParameter(value);
                    if (!iResult.Equals(1))
                    {
                        this.txtNMAdd.Text = "";
                        this.lblNMSpSuccess.Text = "Unable to insert parameter. Please try again";
                        this.lblNMSpSuccess.ForeColor = Color.Maroon;
                        this.lblNMSpSuccess.Visible = true;
                        this.txtNMAdd.Focus();
                    }
                    else
                    {
                        this.txtNMAdd.Text = "";
                        this.lblNMSpSuccess.Text = "Successfully insert Non-Mag Space Parameter";
                        this.lblNMSpSuccess.ForeColor = Color.Green;
                        this.lblNMSpSuccess.Visible = true;

                        this.btnAddNMSDone.Focus();
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNMSClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtNMAdd.Text = "";
                this.lblNMSpSuccess.Text = "";
                this.lblNMSpSuccess.Visible = false;
                this.txtNMAdd.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNMSDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnAddNMSClear_Click(null, null);
                this.tblNMSpList.Visible = true;
                this.tblNMSpAdd.Visible = false;
                System.Data.DataTable nmTable = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getNMSParametersTable();
                this.gvwNMSpList.DataSource = nmTable;
                this.gvwNMSpList.DataBind();
                this.gvwNMSpList.SelectedIndex = -1;

                this.gvwNMSpList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwNMSpList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable nmTable = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getNMSParametersTable();
                this.gvwNMSpList.DataSource = nmTable;
                this.gvwNMSpList.PageIndex = e.NewPageIndex;
                this.gvwNMSpList.DataBind();
                this.btnNMSpCancel.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwNMSpList_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            try
            {
                this.btnNMSpCancel.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRMAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.tblRMList.Visible = false;
                this.tblRMAdd.Visible = true;
                this.lblRMSuccess.Text = "";
                this.lblRMSuccess.Visible = false;
                this.txtRMAdd.Text = "";
                this.txtRMAdd.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRMCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.gvwRMList.SelectedIndex = -1;
                this.lblRMSuccess.Text = "";
                this.lblRMSuccess.Visible = false;
                this.btnRMCancel.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRMList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                this.btnRMCancel.Visible = true;
                System.Data.DataTable rmTable = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getRefMagParametersTable();
                this.gvwRMList.DataSource = rmTable;
                this.gvwRMList.PageIndex = e.NewPageIndex;
                this.gvwRMList.DataBind();

                this.gvwRMList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwRMList_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            try
            {
                this.btnRMCancel.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRMAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                String value = String.Empty;
                value = Convert.ToString(this.txtRMAdd.Text);
                if (String.IsNullOrEmpty(value))
                {
                    this.txtRMAdd.Text = "";
                    this.lblRMSuccess.Text = "Unable to insert null parameter value. Please try again";
                    this.lblRMSuccess.ForeColor = Color.Maroon;
                    this.lblRMSuccess.Visible = true;
                    this.txtRMAdd.Focus();
                }
                else
                {
                    iResult = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.insertRefMagParameter(value);
                    if (!iResult.Equals(1))
                    {
                        this.txtRMAdd.Text = "";
                        this.lblRMSuccess.Text = "Unable to insert parameter value. Please try again";
                        this.lblRMSuccess.ForeColor = Color.Maroon;
                        this.lblRMSuccess.Visible = true;
                        this.txtRMAdd.Focus();
                    }
                    else
                    {
                        this.txtRMAdd.Text = "";
                        this.lblRMSuccess.Text = "Successfully inserted new Reference Magnetics parameter";
                        this.lblRMSuccess.ForeColor = Color.Green;
                        this.lblRMSuccess.Visible = true;
                        this.txtRMAdd.Focus();
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRMAddClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtRMAdd.Text = "";
                this.lblRMSuccess.Text = "";
                this.lblRMSuccess.Visible = false;
                this.txtRMAdd.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRMAddDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnRMAddClear_Click(null, null);
                this.tblRMAdd.Visible = false;
                this.tblRMList.Visible = true;
                System.Data.DataTable rmTable = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getRefMagParametersTable();
                this.gvwRMList.DataSource = rmTable;
                this.gvwRMList.DataBind();
                this.gvwRMList.SelectedIndex = -1;

                this.gvwRMList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwBoxy_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwBoxy_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnTACancel_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> taBTList = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getBTotalParametersList();
                this.ddlBT.Items.Clear();
                this.ddlBT.DataSource = taBTList;
                this.ddlBT.DataTextField = "Value";
                this.ddlBT.DataValueField = "Key";
                this.ddlBT.DataBind();
                this.ddlBT.Items.Insert(0, new ListItem("--- Select B-Total Parameter ---", "-1"));
                this.ddlBT.SelectedIndex = -1;
                this.ddlGT.Items.Clear();
                this.ddlGT.DataBind();
                this.ddlGT.Items.Insert(0, new ListItem("--- Select G-Total Parameter ---", "-1"));
                this.ddlGT.SelectedIndex = -1;
                this.ddlGT.Enabled = false;
                this.ddlDip.Items.Clear();
                this.ddlDip.DataBind();
                this.ddlDip.Items.Insert(0, new ListItem("--- Select Dip Parameter ---", "-1"));
                this.ddlDip.SelectedIndex = -1;
                this.ddlDip.Enabled = false;
                this.ddlInc.Items.Clear();
                this.ddlInc.DataBind();
                this.ddlInc.Items.Insert(0, new ListItem("--- Select Inclination Parameter ---", "-1"));
                this.ddlInc.SelectedIndex = -1;
                this.ddlInc.Enabled = false;
                this.ddlDBz.Items.Clear();
                this.ddlDBz.DataBind();
                this.ddlDBz.Items.Insert(0, new ListItem("--- Select Delta-Bz Parameter ---", "-1"));
                this.ddlDBz.SelectedIndex = -1;
                this.ddlDBz.Enabled = false;
                this.ddlSCAZ.Items.Clear();
                this.ddlSCAZ.DataBind();
                this.ddlSCAZ.Items.Insert(0, new ListItem("--- Select SCAZ Parameter ---", "-1"));
                this.ddlSCAZ.SelectedIndex = -1;
                this.ddlSCAZ.Enabled = false;
                this.ddlRM.Items.Clear();
                this.ddlRM.DataBind();
                this.ddlRM.Items.Insert(0, new ListItem("--- Select Ref.-Mags. Parameter ---", "-1"));
                this.ddlRM.SelectedIndex = -1;
                this.ddlRM.Enabled = false;
                this.ddlNMSp.Items.Clear();
                this.ddlNMSp.DataBind();
                this.ddlNMSp.Items.Insert(0, new ListItem("--- Select Ref.-Mags. Parameter ---", "-1"));
                this.ddlNMSp.SelectedIndex = -1;
                this.ddlNMSp.Enabled = false;
                this.txtAnalysis.Text = "";
                this.txtAnalysis.Enabled = false;

                this.ddlBT.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlBT_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> taGTList = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getGTotalParametersList();
                this.ddlGT.Items.Clear();
                this.ddlGT.DataSource = taGTList;
                this.ddlGT.DataTextField = "Value";
                this.ddlGT.DataValueField = "Key";
                this.ddlGT.DataBind();
                this.ddlGT.Items.Insert(0, new ListItem("--- Select G-Total Parameter ---", "-1"));
                this.ddlGT.SelectedIndex = -1;
                this.ddlGT.Enabled = true;
                this.ddlDip.Items.Clear();
                this.ddlDip.DataBind();
                this.ddlDip.Items.Insert(0, new ListItem("--- Select Dip Parameter ---", "-1"));
                this.ddlDip.SelectedIndex = -1;
                this.ddlDip.Enabled = false;
                this.ddlInc.Items.Clear();
                this.ddlInc.DataBind();
                this.ddlInc.Items.Insert(0, new ListItem("--- Select Inclination Parameter ---", "-1"));
                this.ddlInc.SelectedIndex = -1;
                this.ddlInc.Enabled = false;
                this.ddlDBz.Items.Clear();
                this.ddlDBz.DataBind();
                this.ddlDBz.Items.Insert(0, new ListItem("--- Select Delta-Bz Parameter ---", "-1"));
                this.ddlDBz.SelectedIndex = -1;
                this.ddlDBz.Enabled = false;
                this.ddlSCAZ.Items.Clear();
                this.ddlSCAZ.DataBind();
                this.ddlSCAZ.Items.Insert(0, new ListItem("--- Select SCAZ Parameter ---", "-1"));
                this.ddlSCAZ.SelectedIndex = -1;
                this.ddlSCAZ.Enabled = false;
                this.ddlNMSp.Items.Clear();
                this.ddlNMSp.DataBind();
                this.ddlNMSp.Items.Insert(0, new ListItem("--- Select Non-Mag. Space Parameter ---", "-1"));
                this.ddlNMSp.SelectedIndex = -1;
                this.ddlNMSp.Enabled = false;
                this.ddlRM.Items.Clear();
                this.ddlRM.DataBind();
                this.ddlRM.Items.Insert(0, new ListItem("--- Select Ref. Mags. Parameter ---", "-1"));
                this.ddlRM.SelectedIndex = -1;
                this.ddlRM.Enabled = false;

                this.ddlGT.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlGT_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> dipList = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getDipParametersList();
                this.ddlDip.Items.Clear();
                this.ddlDip.DataSource = dipList;
                this.ddlDip.DataTextField = "Value";
                this.ddlDip.DataValueField = "Key";
                this.ddlDip.DataBind();
                this.ddlDip.Items.Insert(0, new ListItem("--- Select Dip Parameter ---", "-1"));
                this.ddlDip.SelectedIndex = -1;
                this.ddlDip.Enabled = true;
                this.ddlInc.Items.Clear();
                this.ddlInc.DataBind();
                this.ddlInc.Items.Insert(0, new ListItem("--- Select Inclination Parameter ---", "-1"));
                this.ddlInc.SelectedIndex = -1;
                this.ddlInc.Enabled = false;
                this.ddlDBz.Items.Clear();
                this.ddlDBz.DataBind();
                this.ddlDBz.Items.Insert(0, new ListItem("--- Select Delta-Bz Parameter ---", "-1"));
                this.ddlDBz.SelectedIndex = -1;
                this.ddlDBz.Enabled = false;
                this.ddlSCAZ.Items.Clear();
                this.ddlSCAZ.DataBind();
                this.ddlSCAZ.Items.Insert(0, new ListItem("--- Select SCAZ Parameter ---", "-1"));
                this.ddlSCAZ.SelectedIndex = -1;
                this.ddlSCAZ.Enabled = false;
                this.ddlNMSp.Items.Clear();
                this.ddlNMSp.DataBind();
                this.ddlNMSp.Items.Insert(0, new ListItem("--- Select Non-Mag. Space Parameter ---", "-1"));
                this.ddlNMSp.SelectedIndex = -1;
                this.ddlNMSp.Enabled = false;
                this.ddlRM.Items.Clear();
                this.ddlRM.DataBind();
                this.ddlRM.Items.Insert(0, new ListItem("--- Select Ref. Mags. Parameter ---", "-1"));
                this.ddlRM.SelectedIndex = -1;
                this.ddlRM.Enabled = false;

                this.ddlDip.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlDip_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> incList = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getIncParametersList();
                this.ddlInc.Items.Clear();
                this.ddlInc.DataSource = incList;
                this.ddlInc.DataTextField = "Value";
                this.ddlInc.DataValueField = "Key";
                this.ddlInc.DataBind();
                this.ddlInc.Items.Insert(0, new ListItem("--- Select Inclination Parameter ---", "-1"));
                this.ddlInc.SelectedIndex = -1;
                this.ddlInc.Enabled = true;
                this.ddlDBz.Items.Clear();
                this.ddlDBz.DataBind();
                this.ddlDBz.Items.Insert(0, new ListItem("--- Select Delta-Bz Parameter ---", "-1"));
                this.ddlDBz.SelectedIndex = -1;
                this.ddlDBz.Enabled = false;
                this.ddlSCAZ.Items.Clear();
                this.ddlSCAZ.DataBind();
                this.ddlSCAZ.Items.Insert(0, new ListItem("--- Select SCAZ Parameter ---", "-1"));
                this.ddlSCAZ.SelectedIndex = -1;
                this.ddlSCAZ.Enabled = false;
                this.ddlNMSp.Items.Clear();
                this.ddlNMSp.DataBind();
                this.ddlNMSp.Items.Insert(0, new ListItem("--- Select Non-Mag. Space Parameter ---", "-1"));
                this.ddlNMSp.SelectedIndex = -1;
                this.ddlNMSp.Enabled = false;
                this.ddlRM.Items.Clear();
                this.ddlRM.DataBind();
                this.ddlRM.Items.Insert(0, new ListItem("--- Select Ref. Mags. Parameter ---", "-1"));
                this.ddlRM.SelectedIndex = -1;
                this.ddlRM.Enabled = false;

                this.ddlInc.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlInc_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> dbzList = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getDBzParametersList();
                this.ddlDBz.Items.Clear();
                this.ddlDBz.DataSource = dbzList;
                this.ddlDBz.DataTextField = "Value";
                this.ddlDBz.DataValueField = "Key";
                this.ddlDBz.DataBind();
                this.ddlDBz.Items.Insert(0, new ListItem("--- Select Delta-Bz Parameter ---", "-1"));
                this.ddlDBz.SelectedIndex = -1;
                this.ddlDBz.Enabled = true;
                this.ddlSCAZ.Items.Clear();
                this.ddlSCAZ.DataBind();
                this.ddlSCAZ.Items.Insert(0, new ListItem("--- Select SCAZ Parameter ---", "-1"));
                this.ddlSCAZ.SelectedIndex = -1;
                this.ddlSCAZ.Enabled = false;
                this.ddlNMSp.Items.Clear();
                this.ddlNMSp.DataBind();
                this.ddlNMSp.Items.Insert(0, new ListItem("--- Select Non-Mag. Space Parameter ---", "-1"));
                this.ddlNMSp.SelectedIndex = -1;
                this.ddlNMSp.Enabled = false;
                this.ddlRM.Items.Clear();
                this.ddlRM.DataBind();
                this.ddlRM.Items.Insert(0, new ListItem("--- Select Ref. Mags. Parameter ---", "-1"));
                this.ddlRM.SelectedIndex = -1;
                this.ddlRM.Enabled = false;

                this.ddlDBz.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlDBz_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> scazList = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getSCAZParametersList();
                this.ddlSCAZ.Items.Clear();
                this.ddlSCAZ.DataSource = scazList;
                this.ddlSCAZ.DataTextField = "Value";
                this.ddlSCAZ.DataValueField = "Key";
                this.ddlSCAZ.DataBind();
                this.ddlSCAZ.Items.Insert(0, new ListItem("--- Select SCAZ Parameter ---", "-1"));
                this.ddlSCAZ.SelectedIndex = -1;
                this.ddlSCAZ.Enabled = true;
                this.ddlNMSp.Items.Clear();
                this.ddlNMSp.DataBind();
                this.ddlNMSp.Items.Insert(0, new ListItem("--- Select Non-Mag. Space Parameter ---", "-1"));
                this.ddlNMSp.SelectedIndex = -1;
                this.ddlNMSp.Enabled = false;
                this.ddlRM.Items.Clear();
                this.ddlRM.DataBind();
                this.ddlRM.Items.Insert(0, new ListItem("--- Select Ref. Mags. Parameter ---", "-1"));
                this.ddlRM.SelectedIndex = -1;
                this.ddlRM.Enabled = false;

                this.ddlSCAZ.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlSCAZ_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> nmList = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getNMSParametersList();
                this.ddlNMSp.Items.Clear();
                this.ddlNMSp.DataSource = nmList;
                this.ddlNMSp.DataTextField = "Value";
                this.ddlNMSp.DataValueField = "Key";
                this.ddlNMSp.DataBind();
                this.ddlNMSp.Items.Insert(0, new ListItem("--- Select Non-Mag. Space Parameter ---", "-1"));
                this.ddlNMSp.SelectedIndex = -1;
                this.ddlNMSp.Enabled = true;
                this.ddlRM.Items.Clear();
                this.ddlRM.DataBind();
                this.ddlRM.Items.Insert(0, new ListItem("--- Select Ref. Mags. Parameter ---", "-1"));
                this.ddlRM.SelectedIndex = -1;
                this.ddlRM.Enabled = false;

                this.ddlNMSp.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlNMSp_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> rmList = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getRefMagParametersList();
                this.ddlRM.Items.Clear();
                this.ddlRM.DataSource = rmList;
                this.ddlRM.DataTextField = "Value";
                this.ddlRM.DataValueField = "Key";
                this.ddlRM.DataBind();
                this.ddlRM.Items.Insert(0, new ListItem("--- Select Ref. Mags. Parameter ---", "-1"));
                this.ddlRM.SelectedIndex = -1;
                this.ddlRM.Enabled = true;

                this.ddlRM.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlRM_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                String iResult = String.Empty;
                Int32 bt = -99, gt = -99, dp = -99, inc = -99, dbz = -99, scaz = -99, nm = -99, rm = -99;
                bt = Convert.ToInt32(this.ddlBT.SelectedValue);
                gt = Convert.ToInt32(this.ddlGT.SelectedValue);
                dp = Convert.ToInt32(this.ddlDip.SelectedValue);
                inc = Convert.ToInt32(this.ddlInc.SelectedValue);
                dbz = Convert.ToInt32(this.ddlDBz.SelectedValue);
                scaz = Convert.ToInt32(this.ddlSCAZ.SelectedValue);
                nm = Convert.ToInt32(this.ddlNMSp.SelectedValue);
                rm = Convert.ToInt32(this.ddlRM.SelectedValue);
                iResult = SmartsVer1.Helpers.DemogHelpers.TrendAnalysis.getTAStatement(bt, gt, dp, inc, dbz, scaz, nm, rm);
                this.txtAnalysis.Text = iResult;
                this.txtAnalysis.Enabled = true;

                this.txtAnalysis.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}