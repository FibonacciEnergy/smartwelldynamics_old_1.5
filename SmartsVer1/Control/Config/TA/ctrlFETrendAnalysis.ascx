﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlFETrendAnalysis.ascx.cs" Inherits="SmartsVer1.Control.Config.TA.ctrlFETrendAnalysis" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<!--  Stylesheet -->
<link href="../../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript -->
<script src="../../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>

<!--- Page local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }

    .rotate {
        -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
        -moz-transform: rotate(90deg); /* Firefox */
        -ms-transform: rotate(90deg); /* IE 9 */
        transform: rotate(90deg); /* Standard syntax */
    }

    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper[data-stepper-steps="8"] li {
            width: 12%;
        }

        ol.stepper[data-stepper-steps="9"] li {
            width: 11%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }    
</style>
<!--- Page local script --->
<script type="text/javascript">
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("rotate");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").addClass("rotate");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("rotate");
        });
    });
</script>

<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">        
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>Configuration</li>
                <li><a href="../../../Config/TA/fesDRTrendAnalysis_2A.aspx"> Trend Analysis</a></li>
            </ol>
        </div>
    </div>    
</div>

<!-- Page Content -->
<div class="content-wrapper viewer" style="clear: right; width: 100%;">

    <asp:Table ID="tblTotals" runat="server" Width="100%">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Middle" Width="50%">
                <div id="divBTotals" runat="server" class="content-wrapper viewer divShadow" style="width: 100%;">
                    <asp:UpdateProgress ID="uprgsBT" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlBT">
                        <ProgressTemplate>
                            <div id="divBTs" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                z-index: 100;">
                                <asp:Image ID="imgBTProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel ID="upnlBT" runat="server">
                        <ContentTemplate>
                            <asp:Table ID="tblBTotalList" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#c0c0c0" ColumnSpan="2">
                                        <asp:Label ID="lblBTHead" runat="server" CssClass="genLabel" Text="B-Total Parameters" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Middle" ColumnSpan="2">
                                        <asp:GridView ID="gvwBTList" runat="server" AutoGenerateColumns="False" DataKeyNames="tabtID" AllowPaging="True"
                                            CssClass="mydatagrid" Width="100%" EmptyDataText="No B-Total Parameters found." ShowHeaderWhenEmpty="True"
                                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                            RowStyle-CssClass="rows" OnPageIndexChanging="gvwBTList_PageIndexChanging" OnSelectedIndexChanging="gvwBTList_SelectedIndexChanging">
                                            <Columns>
                                                <asp:BoundField DataField="tabtID" HeaderText="tabtID" Visible="False" />
                                                <asp:BoundField DataField="tabtValue" HeaderText="B-Total Parameters" />
                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                            </Columns>
                                        </asp:GridView>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnBTAdd" runat="server" Text="Add" CssClass="red btn" Width="85%" OnClick="btnBTAdd_Click" CausesValidation="false" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Button ID="btnBTCancel" runat="server" Text="Cancel" CssClass="red btn" Width="25%" OnClick="btnBTCancel_Click" Visible="false"
                                            CausesValidation="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <asp:Table ID="tblBTotalAdd" runat="server" Width="100%" Visible="false">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#c0c0c0" ColumnSpan="2">
                                        <asp:Label ID="lblBTAddHead" runat="server" CssClass="genLabel" Text="Add New B-Total Parameters" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblBTAdd" runat="server" CssClass="txtLabel" Text="New B-Total Parameter" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:TextBox ID="txtBTAdd" runat="server" CssClass="genTextBox" Width="80%" Text="" AutoComplete="off" />
                                        <asp:RequiredFieldValidator ID="rfvBTAdd" runat="server" ErrorMessage="*" ForeColor="Maroon" Display="Dynamic" SetFocusOnError="true"
                                            ControlToValidate="txtBTAdd" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%"></asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Label ID="lblBTSuccess" runat="server" Text="" Visible="false" CssClass="lblSuccess" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnBTAddNew" runat="server" CssClass="red btn" Width="85%" Text="Add Parameter" OnClick="btnBTAddNew_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Button ID="btnBTAddClear" runat="server" CssClass="red btn" Width="25%" Text="Clear Values" OnClick="btnBTAddClear_Click"
                                            CausesValidation="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnBTAddDone" runat="server" CssClass="red btn" Width="85%" Text="Done" OnClick="btnBTAddDone_Click" CausesValidation="false" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%"></asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:TableCell>
            <asp:TableCell VerticalAlign="Middle" Width="50%">
                <div id="divGTotal" runat="server" class="content-wrapper viewer divShadow" style="width: 100%;">
                    <asp:UpdateProgress ID="uprgsGT" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlGT">
                        <ProgressTemplate>
                            <div id="divGTs" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                z-index: 100;">
                                <asp:Image ID="imgGTProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel ID="upnlGT" runat="server">
                        <ContentTemplate>
                            <asp:Table ID="tblGTotalList" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#c0c0c0" ColumnSpan="2">
                                        <asp:Label ID="lblGTHead" runat="server" CssClass="genLabel" Text="G-Total Parameters" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Middle" ColumnSpan="2">
                                        <asp:GridView ID="gvwGTList" runat="server" AutoGenerateColumns="False" DataKeyNames="tagtID" AllowPaging="True"
                                            CssClass="mydatagrid" Width="100%" EmptyDataText="No G-Total Parameters found." ShowHeaderWhenEmpty="True"
                                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header"
                                            PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnPageIndexChanging="gvwGTList_PageIndexChanging" OnSelectedIndexChanging="gvwGTList_SelectedIndexChanging">
                                            <Columns>
                                                <asp:BoundField DataField="tagtID" HeaderText="tagtID" Visible="False" />
                                                <asp:BoundField DataField="tagtValue" HeaderText="G-Total Parameters" />
                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                            </Columns>
                                        </asp:GridView>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnGTAdd" runat="server" Text="Add" CssClass="red btn" Width="85%" OnClick="btnGTAdd_Click" CausesValidation="false" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Button ID="btnGTCancel" runat="server" Text="Cancel" CssClass="red btn" Width="25%" OnClick="btnGTCancel_Click" Visible="false"
                                            CausesValidation="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <asp:Table ID="tblGTotalAdd" runat="server" Width="100%" Visible="false">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#c0c0c0" ColumnSpan="2">
                                        <asp:Label ID="lblGTAddHead" runat="server" CssClass="genLabel" Text="Add New G-Total Parameters" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblGTAdd" runat="server" CssClass="txtLabel" Text="New G-Total Parameter" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:TextBox ID="txtGTAdd" runat="server" CssClass="genTextBox" Width="80%" Text="" AutoComplete="off" />
                                        <asp:RequiredFieldValidator ID="rfvGTAdd" runat="server" ErrorMessage="*" ForeColor="Maroon" Display="Dynamic" SetFocusOnError="true"
                                            ControlToValidate="txtGTAdd" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Middle" Width="25%"></asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Label ID="lblGTSuccess" runat="server" Text="" Visible="false" CssClass="lblSuccess" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnGTAddNew" runat="server" CssClass="red btn" Width="85%" Text="Add Parameter" OnClick="btnGTAddNew_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Button ID="btnGTAddClear" runat="server" CssClass="red btn" Width="25%" Text="Clear Values" OnClick="btnGTAddClear_Click"
                                            CausesValidation="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnAddGTDone" runat="server" CssClass="red btn" Width="85%" Text="Done" OnClick="btnAddGTDone_Click" CausesValidation="false" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%"></asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Middle" Width="50%">
                <div id="divDip" runat="server" class="content-wrapper viewer divShadow" style="width: 100%;">
                    <asp:UpdateProgress ID="uprgsDip" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlDip">
                        <ProgressTemplate>
                            <div id="divDips" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                z-index: 100;">
                                <asp:Image ID="imgDipProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel ID="upnlDip" runat="server">
                        <ContentTemplate>
                            <asp:Table ID="tblDipList" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#c0c0c0" ColumnSpan="2">
                                        <asp:Label ID="lblDipHead" runat="server" CssClass="genLabel" Text="Dip Parameters" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2">
                                        <asp:GridView ID="gvwDipList" runat="server" AutoGenerateColumns="False" DataKeyNames="tadipID"
                                            AllowPaging="True" CssClass="mydatagrid" Width="100%" EmptyDataText="No Dip Parameters found." ShowHeaderWhenEmpty="True"
                                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                            RowStyle-CssClass="rows">
                                            <Columns>
                                                <asp:BoundField DataField="tadipID" HeaderText="tadipID" Visible="False" />
                                                <asp:BoundField DataField="tadipValue" HeaderText="Dip Parameters" />
                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                            </Columns>
                                        </asp:GridView>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnDipAdd" runat="server" Text="Add" CssClass="red btn" Width="85%" OnClick="btnDipAdd_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Button ID="btnDipCancel" runat="server" Text="Cancel" CssClass="red btn" Width="25%" OnClick="btnDipCancel_Click" CausesValidation="false"
                                            Visible="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <asp:Table ID="tblDipAdd" runat="server" Visible="false" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2" BackColor="#c0c0c0">
                                        <asp:Label ID="lblDipAddHead" runat="server" CssClass="genLabel" Text="Add New Dip Parameters" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblDipAdd" runat="server" Text="Add New Parameter" CssClass="txtLabel" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:TextBox ID="txtDipAdd" runat="server" CssClass="genTextBox" Text="" AutoComplete="off" />
                                        <asp:RequiredFieldValidator ID="rfvDipAdd" runat="server" ControlToValidate="txtDipAdd" Display="Dynamic" ErrorMessage="*"
                                            ForeColor="Maroon" SetFocusOnError="true" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%"></asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Label ID="lblDipSuccess" runat="server" Text="" Visible="false" CssClass="lblSuccess" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnDipAddNew" runat="server" CssClass="red btn" Width="85%" Text="Add Parameter" OnClick="btnDipAddNew_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Button ID="btnDipAddClear" runat="server" CssClass="red btn" Width="25%" Text="Clear Values" OnClick="btnDipAddClear_Click"
                                            CausesValidation="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnDipAddDone" runat="server" CssClass="red btn" Width="85%" Text="Done" OnClick="btnDipAddDone_Click" CausesValidation="false" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%"></asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:TableCell>
            <asp:TableCell VerticalAlign="Middle" Width="50%">
                <div id="divInc" runat="server" class="content-wrapper viewer divShadow" style="width: 100%;">
                    <asp:UpdateProgress ID="uprgsInc" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlInc">
                        <ProgressTemplate>
                            <div id="divIncs" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                z-index: 100;">
                                <asp:Image ID="imgIncProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel ID="upnlInc" runat="server">
                        <ContentTemplate>
                            <asp:Table ID="tblIncList" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#c0c0c0" ColumnSpan="2">
                                        <asp:Label ID="lblIncHead" runat="server" CssClass="genLabel" Text="Inclination Parameters" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Middle" BackColor="#c0c0c0" ColumnSpan="2">
                                        <asp:GridView ID="gvwIncList" runat="server" AutoGenerateColumns="False" DataKeyNames="taiID"
                                            AllowPaging="True" CssClass="mydatagrid" Width="99.6%" EmptyDataText="No Inclination Parameters found."
                                            ShowHeaderWhenEmpty="True" AlternatingRowStyle-CssClass="AlternatingRowStyle" RowStyle-CssClass="rows"
                                            PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" OnPageIndexChanging="gvwIncList_PageIndexChanging"
                                            OnSelectedIndexChanging="gvwIncList_SelectedIndexChanging">
                                            <Columns>
                                                <asp:BoundField DataField="taiID" Visible="False" />
                                                <asp:BoundField DataField="taiValue" HeaderText="Inclination Parameters" />
                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                            </Columns>
                                        </asp:GridView>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnIncAdd" runat="server" Text="Add" CssClass="red btn" Width="85%" OnClick="btnIncAdd_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Button ID="btnIncCancel" runat="server" Text="Cancel" CssClass="red btn" Width="25%" Visible="false" OnClick="btnIncCancel_Click" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <asp:Table ID="tblIncAdd" runat="server" Width="100%" Visible="false">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2" BackColor="#c0c0c0">
                                        <asp:Label ID="lblIncAddHeading" runat="server" CssClass="genLabel" Text="New Inclination Parameters" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblIncAdd" runat="server" CssClass="txtLabel" Text="New Inclination Parameter" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:TextBox ID="txtIncAdd" runat="server" CssClass="genTextBox" Text="" Width="80%" autocomplete="off" />
                                        <asp:RequiredFieldValidator ID="rfvIncAdd" runat="server" ControlToValidate="txtIncAdd" ErrorMessage="*" Display="Dynamic"
                                            SetFocusOnError="true" ForeColor="Maroon" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%"></asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Label ID="lblIncSuccess" runat="server" Text="" Visible="false" CssClass="lblSuccess" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnIncAddNew" runat="server" CssClass="red btn" Width="85%" Text="Add Parameter" OnClick="btnIncAddNew_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Button ID="btnIncAddClear" runat="server" CssClass="red btn" Width="25%" Text="Clear Values" OnClick="btnIncAddClear_Click"
                                            CausesValidation="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnIncAddDone" runat="server" CssClass="red btn" Width="85%" Text="Done" OnClick="btnIncAddDone_Click" CausesValidation="false" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%"></asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Middle" Width="50%">
                <div id="divDBz" runat="server" class="content-wrapper viewer divShadow" style="width: 100%;">
                    <asp:UpdateProgress ID="uprgsDBz" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlDBz">
                        <ProgressTemplate>
                            <div id="divDBzs" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                z-index: 100;">
                                <asp:Image ID="imgDBzProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel ID="upnlDBz" runat="server">
                        <ContentTemplate>
                            <asp:Table ID="tblDBzList" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2" BackColor="#c0c0c0">
                                        <asp:Label ID="lblDBzHead" runat="server" CssClass="genLabel" Text="Delta-Bz Parameters" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Middle" ColumnSpan="2">
                                        <asp:GridView ID="gvwDBzList" runat="server" AutoGenerateColumns="False" AllowPaging="True" CssClass="mydatagrid" Width="100%"
                                            EmptyDataText="No Delta Bz Parameters found."
                                            ShowHeaderWhenEmpty="True" DataKeyNames="tabzID" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15"
                                            HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnSelectedIndexChanging="gvwDBzList_SelectedIndexChanging"
                                            OnPageIndexChanging="gvwDBzList_PageIndexChanging">
                                            <Columns>
                                                <asp:BoundField DataField="tabzID" Visible="False" />
                                                <asp:BoundField DataField="tabzValue" HeaderText="Delta Bz Parameters" />
                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                            </Columns>
                                        </asp:GridView>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnDBzAdd" runat="server" Text="Add" CssClass="red btn" Width="85%" OnClick="btnDBzAdd_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Button ID="btnDBzCancel" runat="server" Text="Cancel" CssClass="red btn" Width="25%" OnClick="btnDBzCancel_Click" Visible="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <asp:Table ID="tblDBzAdd" runat="server" Width="100%" Visible="false">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2" BackColor="#c0c0c0">
                                        <asp:Label ID="lblDBzAddHead" runat="server" CssClass="genLabel" Text="Add New Delta-Bz Parameters" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblDBzAdd" runat="server" CssClass="txtLabel" Text="New Delta-Bz Parameter" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:TextBox ID="txtDBzAdd" runat="server" CssClass="genTextBox" Width="80%" Text="" AutoComplete="off" />
                                        <asp:RequiredFieldValidator ID="rfvDBzAdd" runat="server" Display="Dynamic" ErrorMessage="*" ForeColor="Maroon" ControlToValidate="txtDBzAdd"
                                            SetFocusOnError="true" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%"></asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Label ID="lblDBzSuccess" runat="server" Text="" Visible="false" CssClass="lblSuccess" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnDBzAddNew" runat="server" CssClass="red btn" Width="85%" Text="Add Parameter" OnClick="btnDBzAddNew_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Button ID="btnDBzAddClear" runat="server" CssClass="red btn" Width="25%" Text="Clear Values" OnClick="btnDBzAddClear_Click"
                                            CausesValidation="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnDBzAddDone" runat="server" CssClass="red btn" Width="85%" Text="Done" OnClick="btnDBzAddDone_Click" CausesValidation="false" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%"></asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:TableCell>
            <asp:TableCell VerticalAlign="Middle" Width="50%">
                <div id="divSCAZ" runat="server" class="content-wrapper viewer divShadow" style="width: 100%;">
                    <asp:UpdateProgress ID="uprgsSCAZ" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlSCAZ">
                        <ProgressTemplate>
                            <div id="divSCAZs" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                z-index: 100;">
                                <asp:Image ID="imgSCAZProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel ID="upnlSCAZ" runat="server">
                        <ContentTemplate>
                            <asp:Table ID="tblSCAZList" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2" BackColor="#c0c0c0">
                                        <asp:Label ID="lblSCAZHead" runat="server" CssClass="genLabel" Text="Short Collar Azimuth (SCAZ) Parameters" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Middle" ColumnSpan="2">
                                        <asp:GridView ID="gvwSCAZ" runat="server" AutoGenerateColumns="False" AllowPaging="True" CssClass="mydatagrid" Width="100%"
                                            EmptyDataText="No Short Collar Azimuth Parameters found."
                                            ShowHeaderWhenEmpty="True" DataKeyNames="tascazID" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header"
                                            PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnPageIndexChanging="gvwSCAZ_PageIndexChanging" OnSelectedIndexChanging="gvwSCAZ_SelectedIndexChanging">
                                            <Columns>
                                                <asp:BoundField DataField="tascazID" Visible="False" />
                                                <asp:BoundField DataField="tascazValue" HeaderText="SCAZ Parameters" />
                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                            </Columns>
                                        </asp:GridView>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnSCAZAdd" runat="server" Text="Add" CssClass="red btn" Width="85%" OnClick="btnSCAZAdd_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Button ID="btnSCAZCancel" runat="server" Text="Cancel" CssClass="red btn" Width="25%" OnClick="btnSCAZCancel_Click"
                                            Visible="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <asp:Table ID="tblSCAZAdd" runat="server" Width="100%" Visible="false">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2" BackColor="#c0c0c0">
                                        <asp:Label ID="lblSCAZAddHead" runat="server" CssClass="genLabel" Text="Add New Short Collar Azimuth (SCAZ) Parameters" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblSCAZAdd" runat="server" CssClass="txtLabel" Text="New SCAZ Parameter" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:TextBox ID="txtSCAZAdd" runat="server" Width="80%" CssClass="genTextBox" AutoComplete="off" Text="" />
                                        <asp:RequiredFieldValidator ID="rfvSCAZAdd" ControlToValidate="txtSCAZAdd" runat="server" ErrorMessage="*" ForeColor="Maroon"
                                            Display="Dynamic" SetFocusOnError="true" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%"></asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Label ID="lblSCAZSuccess" runat="server" Text="" Visible="false" CssClass="lblSuccess" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnSCAZAddNew" runat="server" CssClass="red btn" Width="85%" Text="Add Parameter" OnClick="btnSCAZAddNew_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Button ID="btnSCAZAddClear" runat="server" CssClass="red btn" Width="25%" Text="Clear Values" OnClick="btnSCAZAddClear_Click"
                                            CausesValidation="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnAddSCAZDone" runat="server" CssClass="red btn" Width="85%" Text="Done" OnClick="btnAddSCAZDone_Click"
                                            CausesValidation="false" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%"></asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Middle" Width="50%">
                <div id="divNMSp" runat="server" class="content-wrapper viewer divShadow" style="width: 100%;">
                    <asp:UpdateProgress ID="uprgsNMSp" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlNMSp">
                        <ProgressTemplate>
                            <div id="divNMSps" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                z-index: 100;">
                                <asp:Image ID="imgNMSpProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel ID="upnlNMSp" runat="server">
                        <ContentTemplate>
                            <asp:Table ID="tblNMSpList" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2" BackColor="#c0c0c0">
                                        <asp:Label ID="lblNMSpHead" runat="server" CssClass="genLabel" Text="Non-Mag Spacing Parameters" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2">
                                        <asp:GridView ID="gvwNMSpList" runat="server" AutoGenerateColumns="False" AllowPaging="True" CssClass="mydatagrid" Width="100%"
                                            EmptyDataText="No Non-Magnetic Spacing Parameters found." ShowHeaderWhenEmpty="True" DataKeyNames="tanmspID" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                            PageSize="15" HeaderStyle-CssClass="header"
                                            PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnPageIndexChanging="gvwNMSpList_PageIndexChanging" OnSelectedIndexChanging="gvwNMSpList_SelectedIndexChanging">
                                            <Columns>
                                                <asp:BoundField DataField="tanmspID" Visible="False" />
                                                <asp:BoundField DataField="tanmspValue" HeaderText="Non-Mag Space Parameters" />
                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                            </Columns>
                                        </asp:GridView>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnNMSpAdd" runat="server" Text="Add" CssClass="red btn" Width="85%" OnClick="btnNMSpAdd_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Button ID="btnNMSpCancel" runat="server" Text="Cancel" CssClass="red btn" Width="25%" OnClick="btnNMSpCancel_Click"
                                            Visible="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <asp:Table ID="tblNMSpAdd" runat="server" Width="100%" Visible="false">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2" BackColor="#c0c0c0">
                                        <asp:Label ID="lblNMSpAddHead" runat="server" CssClass="genLabel" Text="Non-Mag Spacing Parameters" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblNMAdd" runat="server" CssClass="txtLabel" Text="New Parameter Name" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:TextBox ID="txtNMAdd" runat="server" CssClass="genTextBox" Width="80%" Text="" AutoComplete="off" />
                                        <asp:RequiredFieldValidator ID="rfvNMAdd" runat="server" ControlToValidate="txtNMAdd" ErrorMessage="*" ForeColor="Maroon"
                                            Display="Dynamic" SetFocusOnError="true" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%"></asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Label ID="lblNMSpSuccess" runat="server" Text="" Visible="false" CssClass="lblSuccess" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnAddNMSNew" runat="server" CssClass="red btn" Width="85%" Text="Add Parameter" OnClick="btnAddNMSNew_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Button ID="btnAddNMSClear" runat="server" CssClass="red btn" Width="25%" Text="Clear Values" OnClick="btnAddNMSClear_Click"
                                            CausesValidation="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnAddNMSDone" runat="server" CssClass="red btn" Width="85%" Text="Done" OnClick="btnAddNMSDone_Click" CausesValidation="false" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%"></asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:TableCell>
            <asp:TableCell VerticalAlign="Middle" Width="50%">
                <div id="divRM" runat="server" class="content-wrapper viewer divShadow" style="width: 100%;">
                    <asp:UpdateProgress ID="uprgsRM" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlRM">
                        <ProgressTemplate>
                            <div id="divRMs" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                z-index: 100;">
                                <asp:Image ID="imgRMProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel ID="upnlRM" runat="server">
                        <ContentTemplate>
                            <asp:Table ID="tblRMList" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2" BackColor="#c0c0c0">
                                        <asp:Label ID="lblRMHead" runat="server" CssClass="genLabel" Text="Reference Magnetics Parameters" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2">
                                        <asp:GridView ID="gvwRMList" runat="server" AutoGenerateColumns="False" AllowPaging="True" CssClass="mydatagrid" Width="100%"
                                            EmptyDataText="No Reference Magnetics Parameters found." ShowHeaderWhenEmpty="True" DataKeyNames="tarmID" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                            PageSize="15" HeaderStyle-CssClass="header"
                                            PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnPageIndexChanging="gvwRMList_PageIndexChanging" OnSelectedIndexChanging="gvwRMList_SelectedIndexChanging">
                                            <Columns>
                                                <asp:BoundField DataField="tarmID" Visible="False" />
                                                <asp:BoundField DataField="tarmValue" HeaderText="Reference Magnetics Parameters" />
                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                            </Columns>
                                        </asp:GridView>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnRMAdd" runat="server" Text="Add" CssClass="red btn" Width="85%" OnClick="btnRMAdd_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Button ID="btnRMCancel" runat="server" Text="Cancel" CssClass="red btn" Width="15%" OnClick="btnRMCancel_Click" Visible="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <asp:Table ID="tblRMAdd" runat="server" Width="100%" Visible="false">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2" BackColor="#c0c0c0">
                                        <asp:Label ID="lblRMAddHead" runat="server" CssClass="genLabel" Width="99.6%" Text="Add New Reference Magnetics Parameters" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Label ID="lblRMAdd" runat="server" Class="txtLabel" Text="Insert New Parameter" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:TextBox ID="txtRMAdd" runat="server" CssClass="genTextBox" Width="80%" Text="" AutoComplete="off" />
                                        <asp:RequiredFieldValidator ID="rfvRMAdd" runat="server" ControlToValidate="txtRMAdd" ErrorMessage="*" ForeColor="Maroon"
                                            Display="Dynamic" SetFocusOnError="true" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%"></asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Label ID="lblRMSuccess" runat="server" Text="" Visible="false" CssClass="lblSuccess" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnRMAddNew" runat="server" CssClass="red btn" Width="85%" Text="Add Parameter" OnClick="btnRMAddNew_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                                        <asp:Button ID="btnRMAddClear" runat="server" CssClass="red btn" Width="25%" Text="Clear Values" OnClick="btnRMAddClear_Click"
                                            CausesValidation="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%">
                                        <asp:Button ID="btnRMAddDone" runat="server" CssClass="red btn" Width="85%" Text="Done" OnClick="btnRMAddDone_Click" CausesValidation="false" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%"></asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Middle" Width="50%">
                <div id="divTACriteria" runat="server" class="content-wrapper viewer divShadow" style="width: 100%;">
                    <asp:UpdateProgress ID="updsTACriteria" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlTACr">
                        <ProgressTemplate>
                            <div id="divTACr" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                z-index: 100;">
                                <asp:Image ID="imgTACrProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel ID="upnlTACr" runat="server">
                        <ContentTemplate>
                            <asp:Table ID="tblTAOption" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#c0c0c0">
                                        <asp:Label ID="lblTACrHeader" runat="server" CssClass="genLabel" Text="Trend Analysis Statement - Qualification Criteria" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                        <asp:GridView ID="gvwTACr" runat="server" AutoGenerateColumns="False" DataKeyNames="dflID" AllowPaging="True"
                                            CssClass="mydatagrid" EmptyDataText="No Criterion Found" ShowHeaderWhenEmpty="True" Width="100%"
                                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                            RowStyle-CssClass="rows">
                                            <Columns>
                                                <asp:BoundField DataField="dflID" Visible="False" />
                                                <asp:BoundField DataField="dflLowerBT" HeaderText="Lower B-Total" />
                                                <asp:BoundField DataField="dflLowerDip" HeaderText="Lower Dip" />
                                                <asp:BoundField DataField="dflLowerGT" HeaderText="Lower G-Total" />
                                                <asp:BoundField DataField="dflUpperBT" HeaderText="Upper B-Total" />
                                                <asp:BoundField DataField="dflUpperDip" HeaderText="Upper Dip" />
                                                <asp:BoundField DataField="dflUpperGT" HeaderText="Upper G-Total" />
                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                            </Columns>
                                        </asp:GridView>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:TableCell>
            <asp:TableCell VerticalAlign="Middle" Width="50%">
                <div id="divBoxyDialog" runat="server" class="content-wrapper viewer divShadow" style="width: 100%;">
                    <asp:UpdateProgress ID="uprgrssBoxy" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlBoxy">
                        <ProgressTemplate>
                            <div id="divBoxyCr" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                z-index: 100;">
                                <asp:Image ID="imgBoxyCrProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel ID="upnlBoxy" runat="server">
                        <ContentTemplate>
                            <asp:Table ID="tblBoxyList" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2" BackColor="#c0c0c0">
                                        <asp:Label ID="lblBoxyHead" runat="server" CssClass="genLabel" Text="Boxy Average Error" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Middle" ColumnSpan="2">
                                        <asp:GridView ID="gvwBoxy" runat="server" AutoGenerateColumns="False" DataKeyNames="baeID" AllowPaging="True"
                                            CssClass="mydatagrid" EmptyDataText="No Criterion Found" ShowHeaderWhenEmpty="True" Width="100%"
                                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                            RowStyle-CssClass="rows" OnPageIndexChanging="gvwBoxy_PageIndexChanging" OnSelectedIndexChanged="gvwBoxy_SelectedIndexChanged">
                                            <Columns>
                                                <asp:BoundField DataField="baeID" Visible="False" />
                                                <asp:BoundField DataField="baeValue" HeaderText="Boxy Average Error" />
                                                <asp:BoundField DataField="uTime" HeaderText="Last Updated" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}"
                                                    ReadOnly="True" />
                                            </Columns>
                                        </asp:GridView>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" Width="25%"></asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" Width="75%"></asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</div>
<div id="divTA" runat="server" class="content-wrapper viewer divShadow" style="width: 100%;">
    <asp:UpdateProgress ID="uprgsTA" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlTA">
        <ProgressTemplate>
            <div id="divTAs" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center; z-index: 100;">
                <asp:Image ID="imgTAProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upnlTA" runat="server">
        <ContentTemplate>
            <asp:Table ID="tblTAList" runat="server" Width="100%">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#c0c0c0" ColumnSpan="3">
                        <asp:Label ID="lblTAHead" runat="server" CssClass="genLabel" Text="Trend Analysis Statement" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="33%">
                        <asp:DropDownList ID="ddlBT" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList" Width="98%" OnSelectedIndexChanged="ddlBT_SelectedIndexChanged">
                            <asp:ListItem Value="0">--- Select B-Total Parameter ---</asp:ListItem>
                        </asp:DropDownList>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="33%">
                        <asp:DropDownList ID="ddlGT" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList" Width="98%" Enabled="false" OnSelectedIndexChanged="ddlGT_SelectedIndexChanged">
                            <asp:ListItem Value="0">--- Select G-Total Parameter ---</asp:ListItem>
                        </asp:DropDownList>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="33%">
                        <asp:DropDownList ID="ddlDip" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList" Width="98%" Enabled="false" OnSelectedIndexChanged="ddlDip_SelectedIndexChanged">
                            <asp:ListItem Value="0">--- Select Dip Parameter ---</asp:ListItem>
                        </asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="33%">
                        <asp:DropDownList ID="ddlInc" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList" Width="98%" Enabled="false" OnSelectedIndexChanged="ddlInc_SelectedIndexChanged">
                    <asp:ListItem Value="0">--- Select Inclination Parameter ---</asp:ListItem>
                </asp:DropDownList>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="33%">
                        <asp:DropDownList ID="ddlDBz" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList" Width="98%" Enabled="false" OnSelectedIndexChanged="ddlDBz_SelectedIndexChanged">
                    <asp:ListItem Value="0">--- Select Delta-Bz Parameter ---</asp:ListItem>
                </asp:DropDownList>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="33%">
                        <asp:DropDownList ID="ddlSCAZ" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList" Width="98%" Enabled="false" OnSelectedIndexChanged="ddlSCAZ_SelectedIndexChanged">
                    <asp:ListItem Value="0">--- Select SCAZ Parameter ---</asp:ListItem>
                </asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="33%">
                        <asp:DropDownList ID="ddlNMSp" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList" Width="98%" Enabled="false" OnSelectedIndexChanged="ddlNMSp_SelectedIndexChanged">
                    <asp:ListItem Value="0">--- Select Non-Mag Space Parameter ---</asp:ListItem>
                </asp:DropDownList>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="33%">
                        <asp:DropDownList ID="ddlRM" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList" Width="98%" Enabled="false" OnSelectedIndexChanged="ddlRM_SelectedIndexChanged">
                    <asp:ListItem Value="0">--- Select Ref. Mags. Parameter ---</asp:ListItem>
                </asp:DropDownList>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="33%"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>                    
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="33%" ColumnSpan="3">
                        <asp:TextBox ID="txtAnalysis" runat="server" Width="95%" Height="150px" Enabled="false" CssClass="genTextBox" TextMode="MultiLine" Font-Size="Large" />
                    </asp:TableCell>                    
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="33%">                        
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="33%">
                        <asp:Button ID="btnTACancel" runat="server" Text="Clear Values" CssClass="red btn" Width="60%" OnClick="btnTACancel_Click" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="33%"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>