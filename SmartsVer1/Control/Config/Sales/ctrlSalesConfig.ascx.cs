﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Net.Mail;
using System.Web.Configuration;
using System.Web.Security;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using LD = SmartsVer1.Helpers.SalesHelpers.Leads;
using CLR = System.Drawing.Color;

namespace SmartsVer1.Control.Config.Sales
{
    public partial class ctrlSalesConfig : System.Web.UI.UserControl
    {
        Int32 ClientID = -99;
        String LoginName, username, domain, GetClientDBString;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);
                username = addr.User;
                domain = addr.Host;
                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(LoginName);
                ClientID = Convert.ToInt32(usrP.clntID);                                                
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
                //Assigning Datasources
                System.Data.DataTable chlList = LD.getSMChannelTable();
                this.gvwChlList.DataSource = chlList;
                this.gvwChlList.DataBind();
                System.Data.DataTable conList = LD.getLinkedConnectionStatusTable();
                this.gvwLinkedStatus.DataSource = conList;
                this.gvwLinkedStatus.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwChlList_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable chlList = LD.getSMChannelTable();
                this.gvwChlList.PageIndex = e.NewPageIndex;
                this.gvwChlList.DataSource = chlList;
                this.gvwChlList.DataBind();
                this.gvwChlList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddChl_Click(object sender, EventArgs e)
        {
            try
            {
                btnClearChlList_Click(null, null);
                this.divChlList.Visible = false;
                this.divChlAdd.Visible = true;
                this.txtChlName.Text = "";
                this.btnAddNewChlDone.Visible = true;

                this.txtChlName.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnClearChlList_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable chlList = LD.getSMChannelTable();
                this.gvwChlList.SelectedIndex = -1;
                this.gvwChlList.DataSource = chlList;
                this.gvwChlList.DataBind();
                this.gvwChlList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewChl_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                String name = String.Empty, desc = String.Empty;
                name = Convert.ToString(this.txtChlName.Text);
                desc = Convert.ToString(this.txtChlDesc.Text);
                if (string.IsNullOrEmpty(desc))
                {
                    desc = "---";
                }
                if (string.IsNullOrEmpty(name))
                {
                    this.txtChlName.Text = "";
                    this.txtChlDesc.Text = "";
                    this.lblChlSuccess.ForeColor = CLR.Maroon;
                    this.lblChlSuccess.Text = "Unable to insert null value. Please provide a valid Channel Name";
                    this.lblChlSuccess.Visible = true;

                    this.txtChlName.Focus();
                }
                else
                {
                    iResult = LD.insertNewSMChannel(name, desc);
                    if (iResult.Equals(1))
                    {
                        this.txtChlName.Text = "";
                        this.txtChlDesc.Text = "";
                        this.lblChlSuccess.ForeColor = CLR.Green;
                        this.lblChlSuccess.Text = "Successfully inserted S&M Channel Name into database";
                        this.lblChlSuccess.Visible = true;

                        this.txtChlName.Focus();
                    }
                    else
                    {
                        this.txtChlName.Text = "";
                        this.txtChlDesc.Text = "";
                        this.lblChlSuccess.ForeColor = CLR.Maroon;
                        this.lblChlSuccess.Text = "Unable to insert value";
                        this.lblChlSuccess.Visible = true;

                        this.txtChlName.Focus();
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewChlClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtChlName.Text = "";
                this.txtChlDesc.Text = "";
                this.txtChlName.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewChlDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnAddNewChlClear_Click(null, null);
                this.divChlList.Visible = true;
                this.divChlAdd.Visible = false;
                System.Data.DataTable chlList = LD.getSMChannelTable();
                this.gvwChlList.SelectedIndex = -1;
                this.gvwChlList.DataSource = chlList;
                this.gvwChlList.DataBind();
                this.btnAddNewChlDone.Visible = false;

                this.gvwChlList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwLinkedStatus_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable sttTable = LD.getLinkedConnectionStatusTable();
                this.gvwLinkedStatus.PageIndex = e.NewPageIndex;
                this.gvwLinkedStatus.DataSource = sttTable;
                this.gvwLinkedStatus.DataBind();
                this.gvwLinkedStatus.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnACNew_Click(object sender, EventArgs e)
        {
            try
            {
                btnACCancel_Click(null, null);
                this.divLinkedStatusList.Visible = false;
                this.divLinkedStatusAdd.Visible = true;
                this.btnLSADone.Visible = true;
                this.txtLSAValue.Text = "";
                this.txtLSADesc.Text = "";
                this.txtLSAValue.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnACCancel_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable sttTable = LD.getLinkedConnectionStatusTable();
                this.gvwLinkedStatus.DataSource = sttTable;
                this.gvwLinkedStatus.DataBind();
                this.gvwLinkedStatus.SelectedIndex = -1;
                this.gvwLinkedStatus.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnLSANew_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                String name = String.Empty, desc = String.Empty;
                name = Convert.ToString(this.txtLSAValue.Text);
                desc = Convert.ToString(this.txtLSADesc.Text);
                if (string.IsNullOrEmpty(name))
                {
                    this.lblLSASuccess.ForeColor = CLR.Maroon;
                    this.lblLSASuccess.Text = "Unable to insert null values. Please provide a valid Status Name and description";
                    this.lblLSASuccess.Visible = true;

                    this.txtLSAValue.Focus();
                }
                else
                {
                    if (string.IsNullOrEmpty(desc))
                    {
                        desc = "---";
                    }
                    iResult = LD.insertNewLinkedInConnectionstatus(name, desc);
                    if (iResult.Equals(1))
                    {
                        this.lblLSASuccess.ForeColor = CLR.Green;
                        this.lblLSASuccess.Text = "Successfully inserted Status value and description";
                        this.lblLSASuccess.Visible = true;
                        this.txtLSAValue.Text = "";
                        this.txtLSADesc.Text = "";

                        this.btnLSADone.Focus();
                    }
                    else
                    {
                        this.lblLSASuccess.ForeColor = CLR.Maroon;
                        this.lblLSASuccess.Text = "Unable to insert values";
                        this.lblLSASuccess.Visible = true;
                        this.txtLSAValue.Text = "";
                        this.txtLSADesc.Text = "";

                        this.txtLSAValue.Focus();
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnLSAClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtLSAValue.Text = "";
                this.txtLSADesc.Text = "";
                this.lblLSASuccess.Text = "";
                this.lblLSASuccess.Visible = false;

                this.txtLSAValue.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnLSADone_Click(object sender, EventArgs e)
        {
            try
            {
                btnLSAClear_Click(null, null);
                this.divLinkedStatusAdd.Visible = false;
                this.divLinkedStatusList.Visible = true;
                this.lblLSASuccess.Text = "";
                this.lblLSASuccess.Visible = false;
                this.btnLSADone.Visible = false;
                System.Data.DataTable sttTable = LD.getLinkedConnectionStatusTable();
                this.gvwLinkedStatus.DataSource = sttTable;
                this.gvwLinkedStatus.DataBind();
                this.gvwLinkedStatus.SelectedIndex = -1;
                this.gvwLinkedStatus.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}