﻿using System;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Net.Mail;
using System.Web.Profile;
using System.Web.Security;
using System.Globalization;
using System.Data.SqlClient;
using System.Collections;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using CLR = System.Drawing.Color;
using USR = SmartsVer1.Helpers.AccountHelpers.CreateUser;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using SCH = SmartsVer1.Helpers.AccountHelpers.ClientScheduler;
using MAP = SmartsVer1.Helpers.ChartHelpers.GMaps;

namespace SmartsVer1.Control.Config.Users
{
    public partial class ctrlHRAssignment : System.Web.UI.UserControl
    {
        String LoginName, username, domain, GetClientDBString;
        Int32 ClientID = -99;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
                //DataSource for controls on page
                System.Data.DataTable usrinfoTable = USR.getClientUsersTable(ClientID);
                System.Data.DataTable usrAssignments = SCH.getClientUserAssignmentTable(GetClientDBString);
                Dictionary<System.Guid, String> roleList = USR.getAllRoles();
                //Assign Connection String value to all SqlDataSource Controls on the page.
                this.ddlAAUserRole.DataSource = roleList;
                this.ddlAAUserRole.DataTextField = "Value";
                this.ddlAAUserRole.DataValueField = "Key";
                this.ddlAAUserRole.DataBind();
                this.gvwAsgList.DataSource = usrAssignments;
                this.gvwAsgList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");
                this.clndrAAStart.SelectedDate = DateTime.Now;
                this.clndrAAEnd.SelectedDate = DateTime.Now;
                this.ltrMap.Text = MAP.getHRAssignmentMap(ClientID, "BodyContent_ctrlHRAssignment_divMap", GetClientDBString);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddAssign_Click(object sender, EventArgs e)
        {
            try
            {
                btnAddNewAsgClear_Click(null, null);
                this.gvwAsgList.Visible = false;
                this.btnAddAssign.Visible = false;
                this.divASGList.Visible = false;
                this.divASGAdd.Visible = true;
                this.btnAddNewAsgDone.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAAUserRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                String roleName = Convert.ToString(this.ddlAAUserRole.SelectedItem);
                Dictionary<Guid, String> usersList = USR.getRoleUsersList(ClientID, roleName);
                this.ddlAAUser.Items.Clear();
                this.ddlAAUser.DataSource = usersList;
                this.ddlAAUser.DataTextField = "Value";
                this.ddlAAUser.DataValueField = "Key";
                this.ddlAAUser.DataBind();
                this.ddlAAUser.Items.Insert(0, new ListItem("--- Select User ---", Guid.Empty.ToString()));
                this.ddlAAUser.SelectedIndex = -1;
                this.ddlAAUser.Enabled = true;
                this.ddlAALoc.Items.Clear();
                this.ddlAALoc.DataBind();
                this.ddlAALoc.Items.Insert(0, new ListItem("--- Select Location ---", "-1"));
                this.ddlAALoc.SelectedIndex = -1;
                this.ddlAALoc.Enabled = false;
                this.asgEnclosure.Visible = false;
                this.btnAddNewAsg.Enabled = false;

                this.ddlAAUser.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAAUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> locList = SCH.getUserAssignmentLocation(ClientID);
                this.ddlAALoc.Items.Clear();
                this.ddlAALoc.DataSource = locList;
                this.ddlAALoc.DataTextField = "Value";
                this.ddlAALoc.DataValueField = "Key";
                this.ddlAALoc.DataBind();
                this.ddlAALoc.Items.Insert(0, new ListItem("--- Select Location ---", "-1"));
                this.ddlAALoc.SelectedIndex = -1;
                this.ddlAALoc.Enabled = true;
                this.asgEnclosure.Visible = false;
                this.btnAddNewAsg.Enabled = false;

                this.ddlAALoc.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAALoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtAAStart.Text = "";
                this.txtAAStart.Enabled = true;
                this.asgEnclosure.Visible = false;
                this.btnAddNewAsg.Enabled = true;

                this.txtAAStart.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewAsg_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                String userID = null;

                Int32 locID = -99;
                DateTime sDate = DateTime.Now;
                DateTime eDate = DateTime.Now;
                userID = this.ddlAAUser.SelectedValue;
                Guid uID = new Guid(userID);
                locID = Convert.ToInt32(this.ddlAALoc.SelectedValue);
                sDate = Convert.ToDateTime(this.clndrAAStart.SelectedDate);
                eDate = Convert.ToDateTime(this.clndrAAEnd.SelectedDate);
                if (String.IsNullOrEmpty(userID))
                {
                    btnAddNewAsgClear_Click(null, null);
                    this.asgEnclosure.Visible = true;
                    this.asgEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iASGMessage.Attributes["class"] = "icon fa fa-warning";
                    this.spnASG.InnerText = " Missing information!";
                    this.lblAsgSuccess.Text = "Please select a User.";                    
                }
                else
                {
                    iResult = SmartsVer1.Helpers.AccountHelpers.CreateUser.insertUAssignment(uID, locID, sDate, eDate, LoginName, domain, GetClientDBString);
                    if (iResult.Equals(1))
                    {
                        btnAddNewAsgClear_Click(null, null);
                        this.lblAsgSuccess.Text = "Successfully created User assignment.";
                        this.asgEnclosure.Visible = true;
                        this.asgEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iASGMessage.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnASG.InnerText = " Success!";
                    }
                    else
                    {
                        btnAddNewAsgClear_Click(null, null);
                        this.lblAsgSuccess.Text = "Unable to create User assignment. Please try again";
                        this.asgEnclosure.Visible = true;
                        this.asgEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iASGMessage.Attributes["class"] = "icon fa fa-warning";
                        this.spnASG.InnerText = " Warning!";
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewAsgClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.ddlAAUserRole.Items.Clear();
                this.ddlAAUserRole.DataBind();
                this.ddlAAUserRole.Items.Insert(0, new ListItem("--- Select System Role ---", "-1"));
                this.ddlAAUserRole.SelectedIndex = -1;
                this.ddlAAUser.Items.Clear();
                this.ddlAAUser.DataBind();
                this.ddlAAUser.Items.Insert(0, new ListItem("--- Select User ---", "-1"));
                this.ddlAAUser.SelectedIndex = -1;
                this.ddlAAUser.Enabled = false;
                this.ddlAALoc.Items.Clear();
                this.ddlAALoc.DataBind();
                this.ddlAALoc.Items.Insert(0, new ListItem("--- Select Location ---", "-1"));
                this.ddlAALoc.SelectedIndex = -1;
                this.ddlAALoc.Enabled = false;
                this.clndrAAStart.SelectedDate = DateTime.Now;
                this.clndrAAEnd.SelectedDate = DateTime.Now;
                this.asgEnclosure.Visible = false;
                this.btnAddNewAsg.Enabled = false;
                this.ddlAAUserRole.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNewAsgDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnAddNewAsgClear_Click(null, null);
                this.gvwAsgList.Visible = true;
                this.btnAddAssign.Visible = true;
                this.divASGList.Visible = true;
                this.divASGAdd.Visible = false;
                this.btnAddNewAsgDone.Visible = false;
                System.Data.DataTable usrAssignments = SCH.getClientUserAssignmentTable(GetClientDBString);
                this.gvwAsgList.DataSource = usrAssignments;
                this.gvwAsgList.DataBind();
                this.ltrMap.Text = MAP.getServiceLandingMap(ClientID, "BodyContent_ctrlHRAssignment_divMap", GetClientDBString);

                this.gvwAsgList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwAsgList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable usrAssignments = SCH.getUserAssignmentTable(GetClientDBString);
                this.gvwAsgList.PageIndex = e.NewPageIndex;
                this.gvwAsgList.DataSource = usrAssignments;
                this.gvwAsgList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}