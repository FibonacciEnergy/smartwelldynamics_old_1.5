﻿using System;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Net.Mail;
using System.Web.Profile;
using System.Web.Security;
using System.Globalization;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using USR = SmartsVer1.Helpers.AccountHelpers.CreateUser;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;

namespace SmartsVer1.Control.Config.Users
{
    public partial class ctrlUsers : System.Web.UI.UserControl
    {
        String LoginName, username, domain, GetClientDBString;
        Int32 ClientID;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
                //Populating dropdown lists
                Dictionary<Int32, String> cTypList = CLNT.getClientTypelist();

                //Assigning datasources to dropdowns
                this.ddlClientType.DataSource = cTypList;
                this.ddlClientType.DataTextField = "Value";
                this.ddlClientType.DataValueField = "Key";
                this.ddlClientType.DataBind();
                this.ddlAddUserClientType.DataSource = cTypList;
                this.ddlAddUserClientType.DataTextField = "Value";
                this.ddlAddUserClientType.DataValueField = "Key";
                this.ddlAddUserClientType.DataBind();
                this.ddlAppClientType.DataSource = cTypList;
                this.ddlAppClientType.DataTextField = "Value";
                this.ddlAppClientType.DataValueField = "Key";
                this.ddlAppClientType.DataBind();
                this.ddlLockoutClntType.DataSource = cTypList;
                this.ddlLockoutClntType.DataTextField = "Value";
                this.ddlLockoutClntType.DataValueField = "Key";
                this.ddlLockoutClntType.DataBind();
                this.ddlPwdClientType.DataSource = cTypList;
                this.ddlPwdClientType.DataTextField = "Value";
                this.ddlPwdClientType.DataValueField = "Key";
                this.ddlPwdClientType.DataBind();
                this.ddlRoleClientType.DataSource = cTypList;
                this.ddlRoleClientType.DataTextField = "Value";
                this.ddlRoleClientType.DataValueField = "Key";
                this.ddlRoleClientType.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlClientType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 tID = Convert.ToInt32(this.ddlClientType.SelectedValue);
                Dictionary<Int32, String> clist = CLNT.getTypeClientList(tID);
                this.ddlClientList.Items.Clear();
                this.ddlClientList.DataSource = clist;
                this.ddlClientList.DataTextField = "Value";
                this.ddlClientList.DataValueField = "Key";
                this.ddlClientList.DataBind();
                this.ddlClientList.Items.Insert(0, new ListItem("--- Select System Client ---", "-1"));
                this.ddlClientList.SelectedIndex = -1;
                this.ddlClientList.Enabled = true;
                this.gvwUsersList.DataBind();
                this.gvwUsersList.SelectedIndex = -1;
                this.gvwUsersList.Visible = false;
                this.btnAccountsDone.Visible = true;

                this.ddlClientList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwUsersList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow drRes = ((DataRowView)e.Row.DataItem).Row;
                    Int32 userApproved = Convert.ToInt32(drRes["enabled"]);
                    Int32 userLockedOut = Convert.ToInt32(drRes["locked"]);

                    CheckBox chkUApp = e.Row.Cells[6].FindControl("chkUserApproved") as CheckBox;
                    CheckBox chkULck = e.Row.Cells[7].FindControl("chkDisabled") as CheckBox;

                    if (userApproved.Equals(1))
                    {
                        chkUApp.Checked = true;
                    }
                    else
                    {
                        chkUApp.Checked = false;
                    }

                    if (userLockedOut.Equals(1))
                    {
                        chkULck.Checked = true;
                    }
                    else
                    {
                        chkULck.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwUsersList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 clID = Convert.ToInt32(this.ddlClientList.SelectedValue);
                DataTable tableD = USR.getClientUsersTable(clID);
                this.gvwUsersList.PageIndex = e.NewPageIndex;
                this.gvwUsersList.DataSource = tableD;
                this.gvwUsersList.DataBind();
                this.gvwUsersList.SelectedIndex = -1;
                this.gvwUserDetail.DataBind();
                this.gvwUserDetail.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwUsersList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                System.Guid user = new System.Guid();
                user = (Guid)this.gvwUsersList.SelectedValue;
                System.Data.DataTable detailTable = USR.getUserDetailTable(user);
                this.gvwUserDetail.DataSource = detailTable;
                this.gvwUserDetail.DataBind();
                this.gvwUserDetail.Visible = true;
                System.Data.DataTable usrinfoTable = USR.getUserProfileInfo(user);
                this.gvwUserAddressInfo.DataSource = usrinfoTable;
                this.gvwUserAddressInfo.DataBind();
                this.gvwUserAddressInfo.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAccountsDone_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> cTypList = CLNT.getClientTypelist();
                this.ddlClientType.Items.Clear();
                this.ddlClientType.DataSource = cTypList;
                this.ddlClientType.DataTextField = "Value";
                this.ddlClientType.DataValueField = "Key";
                this.ddlClientType.DataBind();
                this.ddlClientType.Items.Insert(0, new ListItem("--- Select SMARTs Client Type ---", "-1"));
                this.ddlClientType.SelectedIndex = -1;
                this.ddlClientList.Items.Clear();
                this.ddlClientList.DataBind();
                this.ddlClientList.Items.Insert(0, new ListItem("--- Select SMARTs Client ---", "-1"));
                this.ddlClientList.SelectedIndex = -1;
                this.ddlClientList.Enabled = false;
                this.gvwUsersList.DataBind();
                this.gvwUsersList.SelectedIndex = -1;
                this.gvwUsersList.Visible = false;
                this.gvwUserDetail.DataBind();
                this.gvwUserDetail.Visible = false;
                this.gvwUserAddressInfo.DataBind();
                this.gvwUserAddressInfo.Visible = false;
                this.enUser.Visible = false;
                this.btnAccountsDone.Visible = false;

                this.ddlClientType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAddUserClientType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 typID = Convert.ToInt32(this.ddlAddUserClientType.SelectedValue);
                Dictionary<Int32, String> clntList = CLNT.getTypeClientList(typID);
                this.ddlAddUserClient.Items.Clear();
                this.ddlAddUserClient.DataSource = clntList;
                this.ddlAddUserClient.DataTextField = "Value";
                this.ddlAddUserClient.DataValueField = "Key";
                this.ddlAddUserClient.DataBind();
                this.ddlAddUserClient.Items.Insert(0, new ListItem("--- Select SMART Client ---", "-1"));
                this.ddlAddUserClient.SelectedIndex = -1;
                this.ddlAddUserClient.Enabled = true;
                this.ddlUserRole.Items.Clear();
                this.ddlUserRole.DataBind();
                this.ddlUserRole.Items.Insert(0, new ListItem("--- Select User Role ---", "-1"));
                this.ddlUserRole.SelectedIndex = -1;
                this.ddlUserRole.Enabled = false;
                this.txtFirst.Text = "";
                this.txtFirst.Enabled = false;
                this.txtMiddle.Text = "";
                this.txtMiddle.Enabled = false;
                this.txtLast.Text = "";
                this.txtLast.Enabled = false;
                this.txtDesig.Text = "";
                this.txtDesig.Enabled = false;
                this.txtSt1.Text = "";
                this.txtSt1.Enabled = false;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = false;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = false;
                this.ddlReg.Items.Clear();
                this.ddlReg.DataBind();
                this.ddlReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlReg.SelectedIndex = -1;
                this.ddlReg.Enabled = false;
                this.ddlSReg.Items.Clear();
                this.ddlSReg.DataBind();
                this.ddlSReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlSReg.SelectedIndex = -1;
                this.ddlSReg.Enabled = false;
                this.ddlCountry.Items.Clear();
                this.ddlCountry.DataBind();
                this.ddlCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlCountry.SelectedIndex = -1;
                this.ddlCountry.Enabled = false;
                this.ddlState.Items.Clear();
                this.ddlState.DataBind();
                this.ddlState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlState.SelectedIndex = -1;
                this.ddlState.Enabled = false;
                this.ddlCounty.Items.Clear();
                this.ddlCounty.DataBind();
                this.ddlCounty.Items.Insert(0, new ListItem("--- Select County/District/Division/Municipality ---", "-1"));
                this.ddlCounty.SelectedIndex = -1;
                this.ddlCounty.Enabled = false;
                this.ddlCity.Items.Clear();
                this.ddlCity.DataBind();
                this.ddlCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCity.SelectedIndex = -1;
                this.ddlCity.Enabled = false;
                this.ddlZip.Items.Clear();
                this.ddlZip.DataBind();
                this.ddlZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlZip.SelectedIndex = -1;
                this.ddlZip.Enabled = false;
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = false;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = false;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;
                this.enUser.Visible = false;

                this.ddlAddUserClient.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAddUserClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<System.Guid, String> rList = USR.getAllRoles();
                this.ddlUserRole.Items.Clear();
                this.ddlUserRole.DataSource = rList;
                this.ddlUserRole.DataTextField = "Value";
                this.ddlUserRole.DataValueField = "Key";
                this.ddlUserRole.DataBind();
                this.ddlUserRole.Items.Insert(0, new ListItem("--- Select User Role ---", "-1"));
                this.ddlUserRole.SelectedIndex = -1;
                this.ddlUserRole.Enabled = true;
                this.txtFirst.Text = "";
                this.txtFirst.Enabled = false;
                this.txtMiddle.Text = "";
                this.txtMiddle.Enabled = false;
                this.txtLast.Text = "";
                this.txtLast.Enabled = false;
                this.txtDesig.Text = "";
                this.txtDesig.Enabled = false;
                this.txtSt1.Text = "";
                this.txtSt1.Enabled = false;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = false;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = false;
                this.ddlReg.Items.Clear();
                this.ddlReg.DataBind();
                this.ddlReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlReg.SelectedIndex = -1;
                this.ddlReg.Enabled = false;
                this.ddlSReg.Items.Clear();
                this.ddlSReg.DataBind();
                this.ddlSReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlSReg.SelectedIndex = -1;
                this.ddlSReg.Enabled = false;
                this.ddlCountry.Items.Clear();
                this.ddlCountry.DataBind();
                this.ddlCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlCountry.SelectedIndex = -1;
                this.ddlCountry.Enabled = false;
                this.ddlState.Items.Clear();
                this.ddlState.DataBind();
                this.ddlState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlState.SelectedIndex = -1;
                this.ddlState.Enabled = false;
                this.ddlCounty.Items.Clear();
                this.ddlCounty.DataBind();
                this.ddlCounty.Items.Insert(0, new ListItem("--- Select County/District/Division/Municipality ---", "-1"));
                this.ddlCounty.SelectedIndex = -1;
                this.ddlCounty.Enabled = false;
                this.ddlCity.Items.Clear();
                this.ddlCity.DataBind();
                this.ddlCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCity.SelectedIndex = -1;
                this.ddlCity.Enabled = false;
                this.ddlZip.Items.Clear();
                this.ddlZip.DataBind();
                this.ddlZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlZip.SelectedIndex = -1;
                this.ddlZip.Enabled = false;
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = false;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = false;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;

                this.ddlUserRole.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlUserRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> regList = DMG.getRegionList();
                this.txtFirst.Text = "";
                this.txtFirst.Enabled = true;
                this.txtMiddle.Text = "";
                this.txtMiddle.Enabled = true;
                this.txtLast.Text = "";
                this.txtLast.Enabled = true;
                this.txtDesig.Text = "";
                this.txtDesig.Enabled = true;
                this.txtSt1.Text = "";
                this.txtSt1.Enabled = true;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = true;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = true;
                this.ddlReg.Items.Clear();
                this.ddlReg.DataSource = regList;
                this.ddlReg.DataTextField = "Value";
                this.ddlReg.DataValueField = "Key";
                this.ddlReg.DataBind();
                this.ddlReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlReg.SelectedIndex = -1;
                this.ddlReg.Enabled = true;
                this.ddlSReg.Items.Clear();
                this.ddlSReg.DataBind();
                this.ddlSReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlSReg.SelectedIndex = -1;
                this.ddlSReg.Enabled = false;
                this.ddlCountry.Items.Clear();
                this.ddlCountry.DataBind();
                this.ddlCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlCountry.SelectedIndex = -1;
                this.ddlCountry.Enabled = false;
                this.ddlState.Items.Clear();
                this.ddlState.DataBind();
                this.ddlState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlState.SelectedIndex = -1;
                this.ddlState.Enabled = false;
                this.ddlCounty.Items.Clear();
                this.ddlCounty.DataBind();
                this.ddlCounty.Items.Insert(0, new ListItem("--- Select County/District/Division/Municipality ---", "-1"));
                this.ddlCounty.SelectedIndex = -1;
                this.ddlCounty.Enabled = false;
                this.ddlCity.Items.Clear();
                this.ddlCity.DataBind();
                this.ddlCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCity.SelectedIndex = -1;
                this.ddlCity.Enabled = false;
                this.ddlZip.Items.Clear();
                this.ddlZip.DataBind();
                this.ddlZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlZip.SelectedIndex = -1;
                this.ddlZip.Enabled = false;
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = false;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = false;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;

                this.txtFirst.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.ddlAddUserClientType.Items.Clear();
                this.ddlAddUserClientType.DataBind();
                this.ddlAddUserClientType.Items.Insert(0, new ListItem("--- Select Client Type ---", "-1"));
                this.ddlAddUserClientType.SelectedIndex = -1;
                this.ddlAddUserClientType.Enabled = true;
                this.ddlAddUserClient.Items.Clear();
                this.ddlAddUserClient.DataBind();
                this.ddlAddUserClient.Items.Insert(0, new ListItem("--- Select SMART Client ---", "-1"));
                this.ddlAddUserClient.SelectedIndex = -1;
                this.ddlAddUserClient.Enabled = false;
                this.ddlUserRole.Items.Clear();
                this.ddlUserRole.DataBind();
                this.ddlUserRole.Items.Insert(0, new ListItem("--- Select User Role ---", "-1"));
                this.ddlUserRole.SelectedIndex = -1;
                this.ddlUserRole.Enabled = false;
                this.txtFirst.Text = "";
                this.txtFirst.Enabled = false;
                this.txtMiddle.Text = "";
                this.txtMiddle.Enabled = false;
                this.txtLast.Text = "";
                this.txtLast.Enabled = false;
                this.txtDesig.Text = "";
                this.txtDesig.Enabled = false;
                this.txtSt1.Text = "";
                this.txtSt1.Enabled = false;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = false;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = false;
                this.ddlReg.Items.Clear();
                this.ddlReg.DataBind();
                this.ddlReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlReg.SelectedIndex = -1;
                this.ddlReg.Enabled = false;
                this.ddlSReg.Items.Clear();
                this.ddlSReg.DataBind();
                this.ddlSReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlSReg.SelectedIndex = -1;
                this.ddlSReg.Enabled = false;
                this.ddlCountry.Items.Clear();
                this.ddlCountry.DataBind();
                this.ddlCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlCountry.SelectedIndex = -1;
                this.ddlCountry.Enabled = false;
                this.ddlState.Items.Clear();
                this.ddlState.DataBind();
                this.ddlState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlState.SelectedIndex = -1;
                this.ddlState.Enabled = false;
                this.ddlCounty.Items.Clear();
                this.ddlCounty.DataBind();
                this.ddlCounty.Items.Insert(0, new ListItem("--- Select County/District/Division/Municipality ---", "-1"));
                this.ddlCounty.SelectedIndex = -1;
                this.ddlCounty.Enabled = false;
                this.ddlCity.Items.Clear();
                this.ddlCity.DataBind();
                this.ddlCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCity.SelectedIndex = -1;
                this.ddlCity.Enabled = false;
                this.ddlZip.Items.Clear();
                this.ddlZip.DataBind();
                this.ddlZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlZip.SelectedIndex = -1;
                this.ddlZip.Enabled = false;
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = false;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = false;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;
                this.enUser.Visible = false;

                this.ddlAddUserClientType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlPhoneType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = true;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = true;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = true;
                this.btnAddUser.Enabled = true;

                this.txtEMail.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlClientList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 clntID = Convert.ToInt32(this.ddlClientList.SelectedValue);
                System.Data.DataTable usrTable = USR.getClientUsersTable(clntID);
                this.gvwUsersList.DataSource = usrTable;
                this.gvwUsersList.DataBind();
                this.gvwUsersList.Visible = true;

                this.gvwUsersList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 rID = Convert.ToInt32(this.ddlReg.SelectedValue);
                Dictionary<Int32, String> srList = DMG.getSubRegionList(rID);
                this.ddlSReg.Items.Clear();
                this.ddlSReg.DataSource = srList;
                this.ddlSReg.DataTextField = "Value";
                this.ddlSReg.DataValueField = "Key";
                this.ddlSReg.DataBind();
                this.ddlSReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlSReg.SelectedIndex = -1;
                this.ddlSReg.Enabled = true;
                this.ddlCountry.Items.Clear();
                this.ddlCountry.DataBind();
                this.ddlCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlCountry.SelectedIndex = -1;
                this.ddlCountry.Enabled = false;
                this.ddlState.Items.Clear();
                this.ddlState.DataBind();
                this.ddlState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlState.SelectedIndex = -1;
                this.ddlState.Enabled = false;
                this.ddlCounty.Items.Clear();
                this.ddlCounty.DataBind();
                this.ddlCounty.Items.Insert(0, new ListItem("--- Select County/District/Division/Municipality ---", "-1"));
                this.ddlCounty.SelectedIndex = -1;
                this.ddlCounty.Enabled = false;
                this.ddlCity.Items.Clear();
                this.ddlCity.DataBind();
                this.ddlCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCity.SelectedIndex = -1;
                this.ddlCity.Enabled = false;
                this.ddlZip.Items.Clear();
                this.ddlZip.DataBind();
                this.ddlZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlZip.SelectedIndex = -1;
                this.ddlZip.Enabled = false;
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = false;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = false;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;

                this.ddlSReg.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlSReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlSReg.SelectedValue);
                Dictionary<Int32, String> ctyList = DMG.getCountryList(srID);
                this.ddlCountry.Items.Clear();
                this.ddlCountry.DataSource = ctyList;
                this.ddlCountry.DataTextField = "Value";
                this.ddlCountry.DataValueField = "Key";
                this.ddlCountry.DataBind();
                this.ddlCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlCountry.SelectedIndex = -1;
                this.ddlCountry.Enabled = true;
                this.ddlState.Items.Clear();
                this.ddlState.DataBind();
                this.ddlState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlState.SelectedIndex = -1;
                this.ddlState.Enabled = false;
                this.ddlCounty.Items.Clear();
                this.ddlCounty.DataBind();
                this.ddlCounty.Items.Insert(0, new ListItem("--- Select County/District/Division/Municipality ---", "-1"));
                this.ddlCounty.SelectedIndex = -1;
                this.ddlCounty.Enabled = false;
                this.ddlCity.Items.Clear();
                this.ddlCity.DataBind();
                this.ddlCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCity.SelectedIndex = -1;
                this.ddlCity.Enabled = false;
                this.ddlZip.Items.Clear();
                this.ddlZip.DataBind();
                this.ddlZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlZip.SelectedIndex = -1;
                this.ddlZip.Enabled = false;
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = false;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = false;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;

                this.ddlCountry.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctyID = Convert.ToInt32(this.ddlCountry.SelectedValue);
                Dictionary<Int32, String> stList = DMG.getStateList(ctyID);
                this.ddlState.Items.Clear();
                this.ddlState.DataSource = stList;
                this.ddlState.DataTextField = "Value";
                this.ddlState.DataValueField = "Key";
                this.ddlState.DataBind();
                this.ddlState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlState.SelectedIndex = -1;
                this.ddlState.Enabled = true;
                this.ddlCounty.Items.Clear();
                this.ddlCounty.DataBind();
                this.ddlCounty.Items.Insert(0, new ListItem("--- Select County/District/Division/Municipality ---", "-1"));
                this.ddlCounty.SelectedIndex = -1;
                this.ddlCounty.Enabled = false;
                this.ddlCity.Items.Clear();
                this.ddlCity.DataBind();
                this.ddlCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCity.SelectedIndex = -1;
                this.ddlCity.Enabled = false;
                this.ddlZip.Items.Clear();
                this.ddlZip.DataBind();
                this.ddlZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlZip.SelectedIndex = -1;
                this.ddlZip.Enabled = false;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = false;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = false;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;

                this.ddlState.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 stID = Convert.ToInt32(this.ddlState.SelectedValue);
                Dictionary<Int32, String> cntyList = DMG.getCountyList(stID);
                this.ddlCounty.Items.Clear();
                this.ddlCounty.DataSource = cntyList;
                this.ddlCounty.DataTextField = "Value";
                this.ddlCounty.DataValueField = "Key";
                this.ddlCounty.DataBind();
                this.ddlCounty.Items.Insert(0, new ListItem("--- Select County/District/Division/Municipality ---", "-1"));
                this.ddlCounty.SelectedIndex = -1;
                this.ddlCounty.Enabled = true;
                this.ddlCity.Items.Clear();
                this.ddlCity.DataBind();
                this.ddlCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCity.SelectedIndex = -1;
                this.ddlCity.Enabled = false;
                this.ddlZip.Items.Clear();
                this.ddlZip.DataBind();
                this.ddlZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlZip.SelectedIndex = -1;
                this.ddlZip.Enabled = false;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = false;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = false;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;

                this.ddlCounty.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCounty_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cntyID = Convert.ToInt32(this.ddlCounty.SelectedValue);
                Dictionary<Int32, String> cityList = DMG.getCityList(cntyID);
                this.ddlCity.Items.Clear();
                this.ddlCity.DataSource = cityList;
                this.ddlCity.DataTextField = "Value";
                this.ddlCity.DataValueField = "Key";
                this.ddlCity.DataBind();
                this.ddlCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCity.SelectedIndex = -1;
                this.ddlCity.Enabled = true;
                this.ddlZip.Items.Clear();
                this.ddlZip.DataBind();
                this.ddlZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlZip.SelectedIndex = -1;
                this.ddlZip.Enabled = false;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = false;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = false;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;

                this.ddlCity.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cntyID = Convert.ToInt32(this.ddlCounty.SelectedValue);
                Dictionary<Int32, String> zipList = DMG.getZipList(cntyID);
                this.ddlZip.Items.Clear();
                this.ddlZip.DataSource = zipList;
                this.ddlZip.DataTextField = "Value";
                this.ddlZip.DataValueField = "Key";
                this.ddlZip.DataBind();
                this.ddlZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlZip.SelectedIndex = -1;
                this.ddlZip.Enabled = true;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = false;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = false;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;

                this.ddlZip.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlZip_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> dmgList = DMG.getDemogTypeList();
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = true;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataSource = dmgList;
                this.ddlPhoneType.DataTextField = "Value";
                this.ddlPhoneType.DataValueField = "Key";
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = true;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;

                this.txtPhone.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNUser_Click(object sender, EventArgs e)
        {
            try
            {
                this.divUserList.Visible = false;
                this.divUserAdd.Visible = true;
                Dictionary<Int32, String> cTypList = CLNT.getClientTypelist();
                this.ddlAddUserClientType.Items.Clear();
                this.ddlAddUserClient.DataSource = cTypList;
                this.ddlAddUserClient.DataTextField = "Value";
                this.ddlAddUserClient.DataValueField = "Key";
                this.ddlAddUserClientType.DataBind();
                this.ddlAddUserClientType.Items.Insert(0, new ListItem("--- Select Client Type ---", "-1"));
                this.ddlAddUserClientType.SelectedIndex = -1;
                this.btnAddUserDone.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddUserDone_Click(object sender, EventArgs e)
        {
            try
            {
                this.divUserList.Visible = true;
                this.divUserAdd.Visible = false;
                this.btnCancel_Click(null, null);
                this.btnAddUserDone.Visible = false;
                this.btnAccountsDone_Click(null, null);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAppClientType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 typID = Convert.ToInt32(this.ddlAppClientType.SelectedValue);
                Dictionary<Int32, String> clntList = CLNT.getTypeClientList(typID);
                this.ddlAppClientList.Items.Clear();
                this.ddlAppClientList.DataSource = clntList;
                this.ddlAppClientList.DataTextField = "Value";
                this.ddlAppClientList.DataValueField = "Key";
                this.ddlAppClientList.DataBind();
                this.ddlAppClientList.Items.Insert(0, new ListItem("--- Select SMARTs Client ---", "-1"));
                this.ddlAppClientList.SelectedIndex = -1;
                this.ddlAppClientList.Enabled = true;
                this.ddlAppUserList.Items.Clear();
                this.ddlAppUserList.DataBind();
                this.ddlAppUserList.Items.Insert(0, new ListItem("--- Select SMARTs User Account ---", "-1"));
                this.ddlAppUserList.SelectedIndex = -1;
                this.ddlAppUserList.Enabled = false;
                this.ddlApproval.Items.Clear();
                this.ddlApproval.DataBind();
                this.ddlApproval.Items.Insert(0, new ListItem("--- Select User Account Status ---", "-1"));
                this.ddlApproval.Items.Insert(1, new ListItem("Enabled", "1"));
                this.ddlApproval.Items.Insert(2, new ListItem("Disabled", "2"));
                this.ddlApproval.SelectedIndex = -1;
                this.ddlApproval.Enabled = false;
                this.usrEnclosure.Visible = false;
                this.btnAppCancel.Enabled = true;

                this.ddlAppClientList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAppClientList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 clntID = Convert.ToInt32(this.ddlAppClientList.SelectedValue);
                Dictionary<System.Guid, String> usrList = USR.getClientUsersList(clntID);
                this.ddlAppUserList.Items.Clear();
                this.ddlAppUserList.DataSource = usrList;
                this.ddlAppUserList.DataTextField = "Value";
                this.ddlAppUserList.DataValueField = "Key";
                this.ddlAppUserList.DataBind();
                this.ddlAppUserList.Items.Insert(0, new ListItem("--- Select SMARTs User Account ---", "-1"));
                this.ddlAppUserList.SelectedIndex = -1;
                this.ddlAppUserList.Enabled = true;
                this.ddlApproval.Items.Clear();
                this.ddlApproval.DataBind();
                this.ddlApproval.Items.Insert(0, new ListItem("--- Select User Account Status ---", "-1"));
                this.ddlApproval.Items.Insert(1, new ListItem("Enabled", "1"));
                this.ddlApproval.Items.Insert(2, new ListItem("Disabled", "2"));
                this.ddlApproval.SelectedIndex = -1;
                this.ddlApproval.Enabled = false;
                this.usrEnclosure.Visible = false;
                this.ddlAppUserList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlAppUserList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.ddlApproval.Items.Clear();
                this.ddlApproval.DataBind();
                this.ddlApproval.Items.Insert(0, new ListItem("--- Select User Account Status ---", "-1"));
                this.ddlApproval.Items.Insert(1, new ListItem("Enabled", "1"));
                this.ddlApproval.Items.Insert(2, new ListItem("Disabled", "2"));
                this.ddlApproval.SelectedIndex = -1;
                this.ddlApproval.Enabled = true;
                this.usrEnclosure.Visible = false;
                this.ddlApproval.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAppCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.ddlAppClientType.Items.Clear();
                this.ddlAppClientType.DataBind();
                this.ddlAppClientType.Items.Insert(0, new ListItem("--- Select SMARTs Client Type ---", "-1"));
                this.ddlAppClientType.SelectedIndex = -1;
                this.ddlAppClientList.Items.Clear();
                this.ddlAppClientList.DataBind();
                this.ddlAppClientList.Items.Insert(0, new ListItem("--- Select SMARTs Client ---", "-1"));
                this.ddlAppClientList.SelectedIndex = -1;
                this.ddlAppClientList.Enabled = false;
                this.ddlAppUserList.Items.Clear();
                this.ddlAppUserList.DataBind();
                this.ddlAppUserList.Items.Insert(0, new ListItem("--- Select SMARTs User Account ---", "-1"));
                this.ddlAppUserList.SelectedIndex = -1;
                this.ddlAppUserList.Enabled = false;
                this.ddlApproval.Items.Clear();
                this.ddlApproval.DataBind();
                this.ddlApproval.Items.Insert(0, new ListItem("--- Select User Account Status ---", "-1"));
                this.ddlApproval.Items.Insert(1, new ListItem("Enabled", "1"));
                this.ddlApproval.Items.Insert(2, new ListItem("Disabled", "2"));
                this.ddlApproval.SelectedIndex = -1;
                this.ddlApproval.Enabled = false;
                this.usrEnclosure.Visible = false;
                this.ddlAppClientType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlApproval_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 appStat = -99;
                Guid uID = new System.Guid();
                String uName = null;
                uID = Guid.Parse(this.ddlAppUserList.SelectedValue);
                uName = USR.getUserNameFromId(uID);
                MembershipUser mUser = Membership.GetUser(uName);

                appStat = Convert.ToInt32(this.ddlApproval.SelectedValue);
                if (appStat.Equals(1))
                {
                    mUser.IsApproved = true;
                    Membership.UpdateUser(mUser);
                    this.usrEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                    this.iEnable.Attributes["class"] = "icon fa fa-chain";
                    this.spnEnable.InnerText = " Success";
                    this.lblAppSuccess.Text = "User Account has been successfully Enabled";
                    this.usrEnclosure.Visible = true; 
                    
                    this.btnAppCancel.Focus();
                }
                else
                {
                    mUser.IsApproved = false;
                    Membership.UpdateUser(mUser);
                    this.usrEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iEnable.Attributes["class"] = "icon fa fa-chain-broken";
                    this.spnEnable.InnerText = " Warning";
                    this.lblAppSuccess.Text = "User Account has been successfully Disabled";
                    this.usrEnclosure.Visible = true; 

                    this.btnAppCancel.Focus();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlLockoutClntType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 typID = Convert.ToInt32(this.ddlLockoutClntType.SelectedValue);
                Dictionary<Int32, String> clntList = CLNT.getTypeClientList(typID);
                this.ddlLockoutClntList.Items.Clear();
                this.ddlLockoutClntList.DataSource = clntList;
                this.ddlLockoutClntList.DataTextField = "Value";
                this.ddlLockoutClntList.DataValueField = "Key";
                this.ddlLockoutClntList.DataBind();
                this.ddlLockoutClntList.Items.Insert(0, new ListItem("--- Select SMARTs Client ---", "-1"));
                this.ddlLockoutClntList.SelectedIndex = -1;
                this.ddlLockoutClntList.Enabled = true;
                this.ddlLockoutUserList.Items.Clear();
                this.ddlLockoutUserList.DataBind();
                this.ddlLockoutUserList.Items.Insert(0, new ListItem("--- Select SMARTs User Account ---", "-1"));
                this.ddlLockoutUserList.SelectedIndex = -1;
                this.ddlLockoutUserList.Enabled = false;
                this.ddlLockout.Items.Clear();
                this.ddlLockout.DataBind();
                this.ddlLockout.Items.Insert(0, new ListItem("--- UnLock User Account ---", "-1"));
                this.ddlLockout.Items.Insert(1, new ListItem("UnLock", "1"));
                this.ddlLockout.SelectedIndex = -1;
                this.ddlLockout.Enabled = false;
                this.lckEnclosure.Visible = false;
                this.btnLockoutCancel.Enabled = true;

                this.ddlLockoutClntList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlLockoutClntList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 clntID = Convert.ToInt32(this.ddlLockoutClntList.SelectedValue);
                Dictionary<System.Guid, String> usrList = USR.getClientUsersList(clntID);
                this.ddlLockoutUserList.Items.Clear();
                this.ddlLockoutUserList.DataSource = usrList;
                this.ddlLockoutUserList.DataTextField = "Value";
                this.ddlLockoutUserList.DataValueField = "Key";
                this.ddlLockoutUserList.DataBind();
                this.ddlLockoutUserList.Items.Insert(0, new ListItem("--- Select SMARTs User Account ---", "-1"));
                this.ddlLockoutUserList.SelectedIndex = -1;
                this.ddlLockoutUserList.Enabled = true;
                this.ddlLockout.Items.Clear();
                this.ddlLockout.DataBind();
                this.ddlLockout.Items.Insert(0, new ListItem("--- UnLock User Account ---", "-1"));
                this.ddlLockout.Items.Insert(1, new ListItem("UnLock", "1"));
                this.ddlLockout.SelectedIndex = -1;
                this.ddlLockout.Enabled = false;
                this.lckEnclosure.Visible = false;
                this.ddlLockoutUserList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlLockoutUserList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.ddlLockout.Items.Clear();
                this.ddlLockout.DataBind();
                this.ddlLockout.Items.Insert(0, new ListItem("--- UnLock User Account ---", "-1"));
                this.ddlLockout.Items.Insert(1, new ListItem("UnLock", "1"));
                this.ddlLockout.SelectedIndex = -1;
                this.ddlLockout.Enabled = true;
                this.lckEnclosure.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlLockout_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 lckStat = -99;
                System.Guid sUser = new System.Guid();
                sUser = new System.Guid(this.ddlLockoutUserList.SelectedValue);
                String uName = String.Empty;
                uName = Convert.ToString(Membership.GetUser(sUser));
                MembershipUser mUser = Membership.GetUser(uName);

                lckStat = Convert.ToInt32(this.ddlLockout.SelectedValue);
                if (lckStat.Equals(1))
                {
                    mUser.UnlockUser();
                    Membership.UpdateUser(mUser);
                    this.lckEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                    this.iLock.Attributes["class"] = "icon fa fa-unlock";
                    this.spnLock.InnerText = " Success";
                    this.lblLockoutSuccess.Text = "User Account has been successfully UnLocked";
                    this.lckEnclosure.Visible = true;                     
                    this.btnLockoutCancel.Focus();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnLockoutCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.ddlLockoutClntType.Items.Clear();
                this.ddlLockoutClntType.DataBind();
                this.ddlLockoutClntType.Items.Insert(0, new ListItem("--- Select SMARTs Client Type ---", "-1"));
                this.ddlLockoutClntType.SelectedIndex = -1;
                this.ddlLockoutClntList.Items.Clear();
                this.ddlLockoutClntList.DataBind();
                this.ddlLockoutClntList.Items.Insert(0, new ListItem("--- Select SMARTs Client ---", "-1"));
                this.ddlLockoutClntList.SelectedIndex = -1;
                this.ddlLockoutClntList.Enabled = false;
                this.ddlLockoutUserList.Items.Clear();
                this.ddlLockoutUserList.DataBind();
                this.ddlLockoutUserList.Items.Insert(0, new ListItem("--- Select SMARTs User Account ---", "-1"));
                this.ddlLockoutUserList.SelectedIndex = -1;
                this.ddlLockoutUserList.Enabled = false;
                this.ddlLockout.Items.Clear();
                this.ddlLockout.DataBind();
                this.ddlLockout.Items.Insert(0, new ListItem("--- UnLock User Account ---", "-1"));
                this.ddlLockout.Items.Insert(1, new ListItem("UnLock", "1"));
                this.ddlLockout.SelectedIndex = -1;
                this.ddlLockout.Enabled = false;
                this.lckEnclosure.Visible = false;
                this.btnLockoutCancel.Enabled = false;

                this.ddlLockoutClntType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlPwdClientType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 typID = Convert.ToInt32(this.ddlPwdClientType.SelectedValue);
                Dictionary<Int32, String> clntList = CLNT.getTypeClientList(typID);
                this.ddlPwdClient.Items.Clear();
                this.ddlPwdClient.DataSource = clntList;
                this.ddlPwdClient.DataTextField = "Value";
                this.ddlPwdClient.DataValueField = "Key";
                this.ddlPwdClient.DataBind();
                this.ddlPwdClient.Items.Insert(0, new ListItem("--- Select SMARTs Client ---", "-1"));
                this.ddlPwdClient.SelectedIndex = -1;
                this.ddlPwdClient.Enabled = true;
                this.ddlPwdUser.Items.Clear();
                this.ddlPwdUser.DataBind();
                this.ddlPwdUser.Items.Insert(0, new ListItem("--- Select SMARTs User Account ---", "-1"));
                this.ddlPwdUser.SelectedIndex = -1;
                this.ddlPwdUser.Enabled = false;
                this.txtPwdPassword.Text = "";
                this.txtPwdPassword.Enabled = false;
                this.pwdEnclosure.Visible = false;
                this.btnUpdUsrCancel.Enabled = true;

                this.ddlPwdClient.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlPwdClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 clntID = Convert.ToInt32(this.ddlPwdClient.SelectedValue);
                Dictionary<System.Guid, String> usrList = USR.getClientUsersList(clntID);
                this.ddlPwdUser.Items.Clear();
                this.ddlPwdUser.DataSource = usrList;
                this.ddlPwdUser.DataTextField = "Value";
                this.ddlPwdUser.DataValueField = "Key";
                this.ddlPwdUser.DataBind();
                this.ddlPwdUser.Items.Insert(0, new ListItem("--- Select SMARTs User Account ---", "-1"));
                this.ddlPwdUser.SelectedIndex = -1;
                this.ddlPwdUser.Enabled = true;
                this.txtPwdPassword.Text = "";
                this.txtPwdPassword.Enabled = false;
                this.pwdEnclosure.Visible = false;

                this.ddlPwdUser.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlPwdUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtPwdPassword.Text = "";
                this.txtPwdPassword.Enabled = true;
                this.pwdEnclosure.Visible = false;
                this.txtPwdPassword.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnUpdUsrPassword_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                Int32 iLocked = -99;
                Guid uID = new Guid();
                String usrName = String.Empty;
                String usrPwd = String.Empty;

                uID = Guid.Parse(this.ddlPwdUser.SelectedValue);
                usrName = USR.getUserNameFromId(uID);
                usrPwd = Convert.ToString(this.txtPwdPassword.Text);

                if (String.IsNullOrEmpty(usrName) || String.IsNullOrEmpty(usrPwd))
                {
                    btnUpdUsrCancel_Click(null, null);
                    this.pwdEnclosure.Attributes["class"] = "alert alert-danger alert-dismissable";
                    this.iPwd.Attributes["class"] = "icon fa fa-warning";
                    this.spnPwd.InnerText = " Warning";
                    this.lblPwdSuccess.Text = "Missing values detected. Please provide all values before changing User Password.";
                    this.pwdEnclosure.Visible = true;                     
                }
                else
                {
                    MembershipUser pUser = Membership.GetUser(usrName);
                    iLocked = Convert.ToInt32(pUser.IsLockedOut);
                    if (iLocked.Equals(0))
                    {
                        iResult = USR.updatePassword(usrName, usrPwd);
                        if (iResult.Equals(1))
                        {
                            btnUpdUsrCancel_Click(null, null);
                            this.pwdEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                            this.iPwd.Attributes["class"] = "icon fa fa-check-circle-o";
                            this.spnPwd.InnerText = " Success";
                            this.lblPwdSuccess.Text = "Successfully updated User Password.";
                            this.pwdEnclosure.Visible = true; 
                            this.ddlPwdClientType.Focus();
                        }
                        else
                        {
                            btnUpdUsrCancel_Click(null, null);
                            this.pwdEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                            this.iPwd.Attributes["class"] = "icon fa fa-warning";
                            this.spnPwd.InnerText = " Warning";
                            this.lblPwdSuccess.Text = "Unable to update User Password.<br/>Please try again.";
                            this.pwdEnclosure.Visible = true;                              
                            this.ddlPwdClientType.Focus();
                        }
                    }
                    else
                    {
                        btnUpdUsrCancel_Click(null, null);
                        this.pwdEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iPwd.Attributes["class"] = "icon fa fa-warning";
                        this.spnPwd.InnerText = " Warning";
                        this.lblPwdSuccess.Text = "User Account is Locked. Unable to update User Password.<br/>Please try again.";
                        this.pwdEnclosure.Visible = true;                       
                        
                        this.ddlPwdClientType.Focus();
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnUpdUsrCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.ddlPwdClientType.Items.Clear();
                this.ddlPwdClientType.DataBind();
                this.ddlPwdClientType.Items.Insert(0, new ListItem("--- Select SMARTs Client Type ---", "-1"));
                this.ddlPwdClientType.SelectedIndex = -1;
                this.ddlPwdClientType.Enabled = true;
                this.ddlPwdClient.Items.Clear();
                this.ddlPwdClient.DataBind();
                this.ddlPwdClient.Items.Insert(0, new ListItem("--- Select SMARTs Client ---", "-1"));
                this.ddlPwdClient.SelectedIndex = -1;
                this.ddlPwdClient.Enabled = false;
                this.ddlPwdUser.Items.Clear();
                this.ddlPwdUser.DataBind();
                this.ddlPwdUser.Items.Insert(0, new ListItem("--- Select SMARTs User Account ---", "-1"));
                this.ddlPwdUser.SelectedIndex = -1;
                this.ddlPwdUser.Enabled = false;
                this.txtPwdPassword.Text = "";
                this.txtPwdPassword.Enabled = false;
                this.pwdEnclosure.Visible = false;
                this.btnUpdUsrCancel.Enabled = true;

                this.ddlPwdClientType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, genderID = -99, clntType = -99, clntName = -99, phnType = -99;
                String usrRole = String.Empty, FName = String.Empty, MName = String.Empty, LName = String.Empty, UsrPh = String.Empty;
                String UsrEml = String.Empty, UsrPwd = String.Empty, Desig = String.Empty, st1 = String.Empty, st2 = String.Empty, st3 = String.Empty;
                Int32 reg = -99, sreg = -99, Country = -99, State = -99, County = -99, City = -99, zip = -99;

                TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                clntType = Convert.ToInt32(this.ddlAddUserClientType.SelectedValue);
                clntName = Convert.ToInt32(this.ddlAddUserClient.SelectedValue);
                FName = ti.ToTitleCase(this.txtFirst.Text);
                MName = ti.ToTitleCase(this.txtMiddle.Text);
                LName = ti.ToTitleCase(this.txtLast.Text);
                UsrPwd = this.txtPassword.Text;
                UsrPh = this.txtPhone.Text;
                UsrEml = this.txtEMail.Text;
                usrRole = Convert.ToString(this.ddlUserRole.SelectedItem);
                phnType = Convert.ToInt32(this.ddlPhoneType.SelectedValue);
                Desig = Convert.ToString(this.txtDesig.Text);
                st1 = Convert.ToString(this.txtSt1.Text);
                st2 = Convert.ToString(this.txtSt2.Text);
                st3 = Convert.ToString(this.txtSt3.Text);
                reg = Convert.ToInt32(this.ddlReg.SelectedValue);
                sreg = Convert.ToInt32(this.ddlSReg.SelectedValue);
                Country = Convert.ToInt32(this.ddlCountry.SelectedValue);
                State = Convert.ToInt32(this.ddlState.SelectedValue);
                County = Convert.ToInt32(this.ddlCounty.SelectedValue);
                City = Convert.ToInt32(this.ddlCity.SelectedValue);
                zip = Convert.ToInt32(this.ddlZip.SelectedValue);

                if (String.IsNullOrEmpty(FName) || String.IsNullOrEmpty(LName) || String.IsNullOrEmpty(UsrPwd) || String.IsNullOrEmpty(UsrPh) || String.IsNullOrEmpty(UsrEml) || String.IsNullOrEmpty(Desig) || String.IsNullOrEmpty(st1))
                {

                    this.lblUserSuccess.Text = "Detected Missing values. Please provide all required values to create a User's account.";
                    this.lblUserSuccess.ForeColor = System.Drawing.Color.Red;
                    this.lblUserSuccess.Visible = true;
                }
                else
                {
                    iResult = USR.createUser(clntName, FName, MName, LName, UsrPwd, UsrPh, UsrEml, usrRole, phnType, Desig, st1, st2, st3, reg, sreg, Country, State, County, City, zip, genderID);

                    btnCancel_Click(null, null); /*Calling Cancel button routine to reset Create user*/

                    switch (iResult)
                    {
                        case 1:
                            {
                                this.enUser.Attributes["class"] = "alert alert-success alert-dismissable";
                                this.iUser.Attributes["class"] = "icon fa fa-check-circle";
                                this.spnUser.InnerText = " Success ";
                                this.lblUserSuccess.Text = "User Account was successfully created.";
                                this.enUser.Visible = true; 
                                break;
                            }
                        case -1:
                            {
                                this.enUser.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iUser.Attributes["class"] = "icon fa fa-warning";
                                this.spnUser.InnerText = " !!! Error. Unable to proceed !!!";
                                this.lblUserSuccess.Text = "Email address provided is already being used. <br/>Please recreate the User Account with a valid Unique Email address.";
                                this.enUser.Visible = true; 
                                break;
                            }
                        case -2:
                            {
                                this.enUser.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iUser.Attributes["class"] = "icon fa fa-warning";
                                this.spnUser.InnerText = " !!! Error. Unable to proceed !!!";
                                this.lblUserSuccess.Text = "Duplicate UserName found. Please provide User's First, Middle and Last Name to create a new user.<br/>If this repeats, please use a suffix to create a unqiue user account.";
                                this.enUser.Visible = true; 
                                break;
                            }
                        case -3:
                            {
                                this.enUser.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iUser.Attributes["class"] = "icon fa fa-warning";
                                this.spnUser.InnerText = " !!! Error. Unable to proceed !!!";
                                this.lblUserSuccess.Text = "Invalid Email Address";
                                this.enUser.Visible = true; 
                                break;
                            }
                        case -4:
                            {
                                this.enUser.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iUser.Attributes["class"] = "icon fa fa-warning";
                                this.spnUser.InnerText = " !!! Error. Unable to proceed !!!";
                                this.lblUserSuccess.Text = "Invalid/Duplicate Username. Please try again";
                                this.enUser.Visible = true; 
                                break;
                            }
                        case -5:
                            {
                                this.enUser.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iUser.Attributes["class"] = "icon fa fa-warning";
                                this.spnUser.InnerText = " !!! Error. Unable to proceed !!!";
                                this.lblUserSuccess.Text = "Invalid Password. Please try again";
                                this.enUser.Visible = true; 
                                break;
                            }
                        case -6:
                            {
                                this.enUser.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iUser.Attributes["class"] = "icon fa fa-warning";
                                this.spnUser.InnerText = " !!! Error. Unable to proceed !!!";
                                this.lblUserSuccess.Text = "Provider Error. Please try again";
                                this.enUser.Visible = true; 
                                break;
                            }
                        case -7:
                            {
                                this.enUser.Attributes["class"] = "alert alert-warning alert-dismissable";
                                this.iUser.Attributes["class"] = "icon fa fa-warning";
                                this.spnUser.InnerText = " !!! Error. Unable to proceed !!!";
                                this.lblUserSuccess.Text = "User Account rejected. Please try again";
                                this.enUser.Visible = true; 
                                break;
                            }
                        default:
                            {
                                this.enUser.Attributes["class"] = "alert alert-danger alert-dismissable";
                                this.iUser.Attributes["class"] = "icon fa fa-danger";
                                this.spnUser.InnerText = " !!! Error. Unable to proceed !!!";
                                this.lblUserSuccess.Text = "Unable to create User";
                                this.enUser.Visible = true;                                
                                break;
                            }
                    }
                }

                //this.divAddUserDialog.Visible = false;
                //this.btnAddNUser.Focus();
                this.btnAddUserDone.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlRoleClientType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 typID = Convert.ToInt32(this.ddlRoleClientType.SelectedValue);
                Dictionary<Int32, String> clntList = CLNT.getTypeClientList(typID);
                this.ddlRoleClient.Items.Clear();
                this.ddlRoleClient.DataSource = clntList;
                this.ddlRoleClient.DataTextField = "Value";
                this.ddlRoleClient.DataValueField = "Key";
                this.ddlRoleClient.DataBind();
                this.ddlRoleClient.Items.Insert(0, new ListItem("--- Select SMARTs Client ---", "-1"));
                this.ddlRoleClient.SelectedIndex = -1;
                this.ddlRoleClient.Enabled = true;
                this.ddlRoleUser.Items.Clear();
                this.ddlRoleUser.DataBind();
                this.ddlRoleUser.Items.Insert(0, new ListItem("--- Select SMARTs User Account ---", "-1"));
                this.ddlRoleUser.SelectedIndex = -1;
                this.ddlRoleUser.Enabled = false;
                this.ddlRole.Items.Clear();
                this.ddlRole.DataBind();
                this.ddlRole.Items.Insert(0, new ListItem("--- Select SMARTs Role ---", "-1"));
                this.ddlRole.SelectedIndex = -1;
                this.ddlRole.Enabled = false;
                this.rlEnclosure.Visible = false;
                this.btnRoleCancel.Enabled = true;
                this.ddlRoleClient.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlRoleClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 clntID = Convert.ToInt32(this.ddlRoleClient.SelectedValue);
                Dictionary<System.Guid, String> usrList = USR.getClientUsersList(clntID);
                this.ddlRoleUser.Items.Clear();
                this.ddlRoleUser.DataSource = usrList;
                this.ddlRoleUser.DataTextField = "Value";
                this.ddlRoleUser.DataValueField = "Key";
                this.ddlRoleUser.DataBind();
                this.ddlRoleUser.Items.Insert(0, new ListItem("--- Select SMARTs User Account ---", "-1"));
                this.ddlRoleUser.SelectedIndex = -1;
                this.ddlRoleUser.Enabled = true;
                this.rlEnclosure.Visible = false;
                this.ddlRoleUser.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlRoleUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<System.Guid, String> rList = USR.getAllRoles();
                this.ddlRole.Items.Clear();
                this.ddlRole.DataSource = rList;
                this.ddlRole.DataTextField = "Value";
                this.ddlRole.DataValueField = "Key";
                this.ddlRole.DataBind();
                this.ddlRole.Items.Insert(0, new ListItem("--- Select SMARTs Role ---", "-1"));
                this.ddlRole.SelectedIndex = -1;
                this.ddlRole.Enabled = true;
                this.rlEnclosure.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnRoleUpdate.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRoleUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                System.Guid userID = new System.Guid(), userRole = new System.Guid();
                userID = Guid.Parse(this.ddlRoleUser.SelectedValue);
                userRole = Guid.Parse(this.ddlRole.SelectedValue);
                iResult = USR.updateUserRole(userID, userRole);
                if (iResult.Equals(1))
                {
                    btnRoleCancel_Click(null, null);
                    this.rlEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                    this.iRole.Attributes["class"] = "icon fa fa-check-circle";
                    this.spnRole.InnerText = " Success ";
                    this.lblRoleSuccess.Text = "User Role was successfully updated";
                    this.rlEnclosure.Visible = true;
                }
                else
                {
                    btnRoleCancel_Click(null, null);
                    this.rlEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iRole.Attributes["class"] = "icon fa fa-warning";
                    this.spnRole.InnerText = " Warning";
                    this.lblRoleSuccess.Text = "Unable to update User Role";
                    this.rlEnclosure.Visible = true;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnRoleCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> cTypList = CLNT.getClientTypelist();
                this.ddlRoleClientType.Items.Clear();
                this.ddlRoleClientType.DataSource = cTypList;
                this.ddlRoleClientType.DataTextField = "Value";
                this.ddlRoleClientType.DataValueField = "Key";
                this.ddlRoleClientType.DataBind();
                this.ddlRoleClientType.Items.Insert(0, new ListItem("--- Select SMARTs Client Type ---", "-1"));
                this.ddlRoleClientType.SelectedIndex = -1;
                this.ddlRoleClient.Items.Clear();
                this.ddlRoleClient.DataBind();
                this.ddlRoleClient.Items.Insert(0, new ListItem("--- Select SMARTs Client ---", "-1"));
                this.ddlRoleClient.SelectedIndex = -1;
                this.ddlRoleClient.Enabled = false;
                this.ddlRoleUser.Items.Clear();
                this.ddlRoleUser.DataBind();
                this.ddlRoleUser.Items.Insert(0, new ListItem("--- Select SMARTs User Account ---", "-1"));
                this.ddlRoleUser.SelectedIndex = -1;
                this.ddlRoleUser.Enabled = false;
                this.ddlRole.Items.Clear();
                this.ddlRole.DataBind();
                this.ddlRole.Items.Insert(0, new ListItem("--- Select SMARTs Role ---", "-1"));
                this.ddlRole.SelectedIndex = -1;
                this.ddlRole.Enabled = false;
                this.rlEnclosure.Visible = false;
                this.ddlRoleClientType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        
    }
}