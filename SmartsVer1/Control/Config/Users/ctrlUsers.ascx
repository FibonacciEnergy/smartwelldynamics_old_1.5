﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlUsers.ascx.cs" Inherits="SmartsVer1.Control.Config.Users.ctrlUsers" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<!-- Stylesheet -->
<link href="../../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- JavaScripts -->
<script src="../../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<!-- Javascript for Footer -->
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>
<!--- local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }
    .swdMenu {
        color: #ffffff;
    }
    .spnLabel {
        min-width: 120px;
        text-align: right;
    }
</style>

<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box-header with-border">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                 <div class="box-tools pull-right">
                    <ol class="breadcrumb">
                        <li><a href="../../../Viewer/fesViewer.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                        <li>Human Resource</li>
                        <li><a href="../../../Config/Users/fesDRHR_2A.aspx">User Accounts</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!--- Page Content --->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul class="nav nav-pills nav-justified">
                <li class="nav-item active"><a data-target="#UsrAct" data-toggle="tab">User Accounts</a></li>
                <li class="nav-item"><a data-target="#EnDb" data-toggle="tab">Enable/Disable Accounts</a></li>
                <li class="nav-item"><a data-target="#LkUnk" data-toggle="tab">Lock/Unlock Accounts</a></li>
                <li class="nav-item"><a data-target="#Role" data-toggle="tab">User Account Role</a></li>
                <li class="nav-item"><a data-target="#Pswd" data-toggle="tab">User Account Password</a></li>
            </ul>
            <div class="tab-content">
                <div id="UsrAct" class="tab-pane fade in active">
                    <asp:UpdateProgress ID="uprgsUser" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlUserList">
                        <ProgressTemplate>
                            <div id="divUserProcessing" runat="server" class="updateProgress">
                                <asp:Image ID="imgUserProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlUserList">
                        <ContentTemplate>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h5 class="panel-title"><span>System Client Accounts List</span></h5>
                                </div>
                                <div class="panel-body">
                                    <div style="min-height: 450px;">
                                        <div id="divUserList" runat="server">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Client Type</span>
                                                        <asp:DropDownList ID="ddlClientType" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                            OnSelectedIndexChanged="ddlClientType_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- Select SMARTs Client Type ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Client</span>
                                                        <asp:DropDownList ID="ddlClientList" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                            Enabled="False" OnSelectedIndexChanged="ddlClientList_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- Select SMARTs Client ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <asp:GridView ID="gvwUsersList" runat="server" AutoGenerateColumns="False" AllowPaging="True" CssClass="mydatagrid" EmptyDataText="No User Accounts Found."
                                                        ShowHeaderWhenEmpty="True" Visible="False" OnRowDataBound="gvwUsersList_RowDataBound" DataKeyNames="UserId"
                                                        AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                        RowStyle-CssClass="rows" OnPageIndexChanging="gvwUsersList_PageIndexChanging" SelectedRowStyle-CssClass="selectedrow"
                                                        OnSelectedIndexChanged="gvwUsersList_SelectedIndexChanged">
                                                        <Columns>
                                                            <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                <ControlStyle CssClass="btn btn-primary text-center"></ControlStyle>
                                                            </asp:ButtonField>
                                                            <asp:BoundField DataField="UserId" HeaderText="UserId" SortExpression="UserId" Visible="False" />
                                                            <asp:BoundField DataField="lastName" HeaderText="Last Name" />
                                                            <asp:BoundField DataField="firstName" HeaderText="First Name" />
                                                            <asp:BoundField DataField="userName" HeaderText="UserName" />
                                                            <asp:BoundField DataField="roleName" HeaderText="Role Name" />
                                                            <asp:TemplateField HeaderText="Enabled">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkUserApproved" runat="server" />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Locked Out">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkDisabled" runat="server" />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <asp:GridView runat="server" ID="gvwUserDetail" AutoGenerateColumns="False" DataKeyNames="uId" CssClass="mydatagrid"
                                                        Visible="False" AlternatingRowStyle-CssClass="AlternatingRowStyle" HeaderStyle-CssClass="header" RowStyle-CssClass="rows">
                                                        <Columns>
                                                            <asp:BoundField DataField="UserId" Visible="false" />
                                                            <asp:BoundField DataField="Email" HeaderText="Email" />
                                                            <asp:BoundField DataField="CreateDate" HeaderText="User Creation Date" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                            <asp:BoundField DataField="LastLoginDate" HeaderText="Last Login Date" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                            <asp:BoundField DataField="LastActivityDate" HeaderText="Last Activity Date" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <asp:GridView runat="server" ID="gvwUserAddressInfo" AutoGenerateColumns="False" DataKeyNames="usrID" CssClass="mydatagrid"
                                                        Visible="False" AlternatingRowStyle-CssClass="AlternatingRowStyle" HeaderStyle-CssClass="header" RowStyle-CssClass="rows">
                                                        <Columns>
                                                            <asp:BoundField DataField="usrID" Visible="false" />
                                                            <asp:BoundField DataField="addT" HeaderText="Address Type" />
                                                            <asp:BoundField DataField="dsg" HeaderText="Designation" />
                                                            <asp:BoundField DataField="Street1" HeaderText="Street 1" />
                                                            <asp:BoundField DataField="Street2" HeaderText="Street 2" />
                                                            <asp:BoundField DataField="Street3" HeaderText="Street 3" />
                                                            <asp:BoundField DataField="uCity" HeaderText="City/Town" />
                                                            <asp:BoundField DataField="uCounty" HeaderText="County/District" />
                                                            <asp:BoundField DataField="uState" HeaderText="State/Province" />
                                                            <asp:BoundField DataField="uCountry" HeaderText="Country" />
                                                            <asp:BoundField DataField="uZip" HeaderText="Zip/Postal Code" />
                                                            <asp:BoundField DataField="uSubReg" HeaderText="Sub-Region" />
                                                            <asp:BoundField DataField="uReg" HeaderText="Region" />
                                                            <asp:BoundField DataField="uPh" HeaderText="Phone" />
                                                            <asp:BoundField DataField="uPhT" HeaderText="Type" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="input-group" style="width: 100%;">
                                                        <asp:Button runat="server" ID="btnAddNUser" CssClass="btn btn-info" Width="10%" Text="Add New User"
                                                            OnClick="btnAddNUser_Click" CausesValidation="False" />
                                                        <asp:Button ID="btnAccountsDone" runat="server" Width="10%" Text="Cancel" CssClass="btn btn-warning" Visible="false"
                                                            OnClick="btnAccountsDone_Click" CausesValidation="False" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divUserAdd" runat="server" visible="false">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Client Type</span>
                                                        <asp:DropDownList ID="ddlAddUserClientType" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control"
                                                            ToolTip="User Account Parent Company Type (Required)" OnSelectedIndexChanged="ddlAddUserClientType_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- Select Client Type ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Client</span>
                                                        <asp:DropDownList ID="ddlAddUserClient" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control"
                                                            Enabled="False" ToolTip="User Account Parent Company (Required)" OnSelectedIndexChanged="ddlAddUserClient_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- Select SMART Client ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Role</span>
                                                        <asp:DropDownList ID="ddlUserRole" runat="server" CssClass="form-control" AppendDataBoundItems="True" AutoPostBack="True"
                                                            Enabled="False"
                                                            OnSelectedIndexChanged="ddlUserRole_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- Select User Role ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">First Name</span>
                                                        <asp:TextBox ID="txtFirst" runat="server" CssClass="form-control" Enabled="false" placeholder="First Name (required)" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Middle Name</span>
                                                        <asp:TextBox ID="txtMiddle" runat="server" CssClass="form-control" Enabled="false" placeholder="Middle Name" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Last Name</span>
                                                        <asp:TextBox ID="txtLast" runat="server" CssClass="form-control" Enabled="false" placeholder="Last Name (required)" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Designation</span>
                                                        <asp:TextBox ID="txtDesig" runat="server" CssClass="form-control" Enabled="false" placeholder="Designation/Job Role (required)" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4"></div>
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Street 1</span>
                                                        <asp:TextBox ID="txtSt1" runat="server" CssClass="form-control" Enabled="false" placeholder="Street 1 (required)" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Street 2</span>
                                                        <asp:TextBox ID="txtSt2" runat="server" CssClass="form-control" Enabled="false" placeholder="Street 2" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Street 3</span>
                                                        <asp:TextBox ID="txtSt3" runat="server" CssClass="form-control" Enabled="false" placeholder="Street 3" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Maj. Region</span>
                                                        <asp:DropDownList ID="ddlReg" runat="server" CssClass="form-control" AppendDataBoundItems="True" AutoPostBack="True" Enabled="False"
                                                            OnSelectedIndexChanged="ddlReg_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- Select Major Geographic Region ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Sub-Region</span>
                                                        <asp:DropDownList ID="ddlSReg" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control" Enabled="False"
                                                            OnSelectedIndexChanged="ddlSReg_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- Select Geographic Sub-Region ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Country/Nation</span>
                                                        <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control" AppendDataBoundItems="True" AutoPostBack="True"
                                                            Enabled="False"
                                                            OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- Select Country/Nation ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">State/Province</span>
                                                        <asp:DropDownList ID="ddlState" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control" Enabled="False"
                                                            OnSelectedIndexChanged="ddlState_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- Select State/Province ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">County/District</span>
                                                        <asp:DropDownList ID="ddlCounty" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control" Enabled="False"
                                                            OnSelectedIndexChanged="ddlCounty_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- Select County/District/Division/Municipality ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">City/Nation</span>
                                                        <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control" Enabled="False"
                                                            OnSelectedIndexChanged="ddlCity_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- Select City/Town ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Zip/Postalcode</span>
                                                        <asp:DropDownList ID="ddlZip" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control" Enabled="False"
                                                            OnSelectedIndexChanged="ddlZip_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- Select Zip/Postal Code ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4"></div>
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Phone</span>
                                                        <asp:TextBox ID="txtPhone" runat="server" CssClass="form-control" Enabled="false" TextMode="Phone" placeholder="Phone (required)" />
                                                        <asp:RequiredFieldValidator runat="server" ID="rfvPhone" ControlToValidate="txtPhone" ErrorMessage="*" ForeColor="Maroon"
                                                            Display="Dynamic" SetFocusOnError="True" ValidationGroup="CreateUser" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Type</span>
                                                        <asp:DropDownList ID="ddlPhoneType" runat="server" Enabled="False" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control"
                                                            OnSelectedIndexChanged="ddlPhoneType_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- Select Phone Type ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Email</span>
                                                        <asp:TextBox ID="txtEMail" runat="server" CssClass="form-control" Enabled="false" TextMode="Email" placeholder="Email (required)" />
                                                        <asp:RequiredFieldValidator runat="server" ID="rfvEMail" ControlToValidate="txtEmail" ErrorMessage="*" ForeColor="Maroon"
                                                            Display="Dynamic" SetFocusOnError="True" ValidationGroup="CreateUser" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Password</span>
                                                        <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" Enabled="false" TextMode="Password" placeholder="Password (required)" />
                                                        <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword" ErrorMessage="*" ForeColor="Maroon"
                                                            Display="Dynamic" SetFocusOnError="True" ValidationGroup="CreateUser" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon bg-light-blue spnLabel">Confirm Password</span>
                                                        <asp:TextBox ID="txtCPassword" runat="server" CssClass="form-control" Enabled="false" TextMode="Password" placeholder="Confirm Password (required)" />                                                        
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <asp:RequiredFieldValidator ID="rfvCPassword" runat="server" ControlToValidate="txtCPassword" ErrorMessage="*" ForeColor="Maroon"
                                                            Display="Dynamic" SetFocusOnError="True" ValidationGroup="CreateUser" />
                                                        <asp:CompareValidator ID="cmpCPassword" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtCPassword" ForeColor="Maroon"
                                                            ErrorMessage="*" SetFocusOnError="True" ValidationGroup="CreateUser" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div runat="server" id="enUser" visible="false">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <h4><i runat="server" id="iUser"><span runat="server" id="spnUser"></span></i></h4>
                                                        <asp:Label ID="lblUserSuccess" runat="server" CssClass="lblContact" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="input-group" style="width: 100%;">
                                                        <asp:LinkButton ID="btnAddUser" runat="server" CssClass="btn btn-success text-center" OnClick="btnAddUser_Click" ValidationGroup="CreateUser"><i class="fa fa-plus"> User</i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-warning text-center" OnClick="btnCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="btnAddUserDone" CssClass="btn btn-danger text-center" OnClick="btnAddUserDone_Click"><i class="fa fa-times-rectangle"> Exit</i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div id="EnDb" class="tab-pane fade">
                    <asp:UpdateProgress ID="uprgrsEnDb" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlEnable">
                        <ProgressTemplate>
                            <div id="divEnDbProcessing" runat="server" class="updateProgress">
                                <asp:Image ID="imgEnDbProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlEnable">
                        <ContentTemplate>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h5 class="panel-title"><span>Enable/Disable SMARTs User Account</span></h5>
                                </div>
                                <div class="panel-body">
                                    <div style="min-height: 450px;">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-6 col-lg-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon bg-light-blue spnLabel">Client Type</span>
                                                    <asp:DropDownList ID="ddlAppClientType" runat="server" CssClass="form-control"
                                                        AutoPostBack="True" AppendDataBoundItems="True" OnSelectedIndexChanged="ddlAppClientType_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select SMARTs Client Type ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-12 col-sm-6 col-lg-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon bg-light-blue spnLabel">Client</span>
                                                    <asp:DropDownList ID="ddlAppClientList" runat="server" AutoPostBack="True" AppendDataBoundItems="True"
                                                        CssClass="form-control" Enabled="False" OnSelectedIndexChanged="ddlAppClientList_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select SMARTs Client ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-6 col-lg-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon bg-light-blue spnLabel">User Account</span>
                                                    <asp:DropDownList ID="ddlAppUserList" runat="server" AutoPostBack="true" AppendDataBoundItems="true" CssClass="form-control"
                                                        Enabled="false" OnSelectedIndexChanged="ddlAppUserList_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select SMARTs User Account ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-12 col-sm-6 col-lg-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon bg-light-blue spnLabel">Status</span>
                                                    <asp:DropDownList ID="ddlApproval" runat="server" CssClass="form-control" AppendDataBoundItems="True" AutoPostBack="True"
                                                        Enabled="False" OnSelectedIndexChanged="ddlApproval_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select User Account Status ---</asp:ListItem>
                                                        <asp:ListItem Value="1">Enabled</asp:ListItem>
                                                        <asp:ListItem Value="2">Disabled</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div runat="server" id="usrEnclosure" visible="false">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                    <h4><i runat="server" id="iEnable"><span runat="server" id="spnEnable"></span></i></h4>
                                                    <asp:Label ID="lblAppSuccess" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="width: 100%;">
                                                <asp:Button ID="btnAppCancel" runat="server" CssClass="btn btn-warning" Width="10%" Text="Cancel" Enabled="false"
                                                    OnClick="btnAppCancel_Click" CausesValidation="False" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div id="LkUnk" class="tab-pane fade">
                    <asp:UpdateProgress ID="uprgrsLock" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlLock">
                        <ProgressTemplate>
                            <div id="divLockProcessing" runat="server" class="updateProgress">
                                <asp:Image ID="imgLockProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlLock">
                        <ContentTemplate>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h5 class="panel-title"><span>Lock/Unlock SMARTs User Account</span></h5>
                                </div>
                                <div class="panel-body">
                                    <div style="min-height: 450px;">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-6 col-lg-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon bg-light-blue spnLabel">Client Type</span>
                                                    <asp:DropDownList ID="ddlLockoutClntType" runat="server" CssClass="form-control"
                                                        AutoPostBack="True" AppendDataBoundItems="True" OnSelectedIndexChanged="ddlLockoutClntType_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select SMARTs Client Type ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-12 col-sm-6 col-lg-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon bg-light-blue spnLabel">Client</span>
                                                    <asp:DropDownList ID="ddlLockoutClntList" runat="server" AutoPostBack="True" AppendDataBoundItems="True"
                                                        CssClass="form-control" Enabled="False" OnSelectedIndexChanged="ddlLockoutClntList_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select SMARTs Client ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-6 col-lg-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon bg-light-blue spnLabel">User Account</span>
                                                    <asp:DropDownList ID="ddlLockoutUserList" runat="server" AutoPostBack="true" AppendDataBoundItems="true" CssClass="form-control"
                                                        Enabled="false" OnSelectedIndexChanged="ddlLockoutUserList_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select SMARTs User Account ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-12 col-sm-6 col-lg-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon bg-light-blue spnLabel">Status</span>
                                                    <asp:DropDownList ID="ddlLockout" runat="server" CssClass="form-control" AppendDataBoundItems="True" AutoPostBack="True"
                                                        Enabled="False" OnSelectedIndexChanged="ddlLockout_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- UnLock User Account ---</asp:ListItem>
                                                        <asp:ListItem Value="1">UnLock</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div runat="server" id="lckEnclosure" visible="false">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                    <h4><i runat="server" id="iLock"><span runat="server" id="spnLock"></span></i></h4>
                                                    <asp:Label ID="lblLockoutSuccess" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="width: 100%;">
                                                <asp:Button ID="btnLockoutCancel" runat="server" CssClass="btn btn-warning" Width="10%" Text="Cancel" Enabled="false"
                                                    OnClick="btnLockoutCancel_Click" CausesValidation="False" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div id="Role" class="tab-pane fade">
                    <asp:UpdateProgress ID="uprgrsRole" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlRole">
                        <ProgressTemplate>
                            <div id="divRoleProcessing" runat="server" class="updateProgress">
                                <asp:Image ID="imgRoleProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlRole">
                        <ContentTemplate>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h5 class="panel-title"><span>SMARTs User Role</span></h5>
                                </div>
                                <div class="panel-body">
                                    <div style="min-height: 450px;">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-6 col-lg-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon bg-light-blue spnLabel">Client Type</span>
                                                    <asp:DropDownList ID="ddlRoleClientType" runat="server" CssClass="form-control"
                                                        AutoPostBack="True" AppendDataBoundItems="True" OnSelectedIndexChanged="ddlRoleClientType_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select SMARTs Client Type ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-12 col-sm-6 col-lg-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon bg-light-blue spnLabel">Client</span>
                                                    <asp:DropDownList ID="ddlRoleClient" runat="server" AutoPostBack="True" AppendDataBoundItems="True"
                                                        CssClass="form-control" Enabled="False" OnSelectedIndexChanged="ddlRoleClient_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select SMARTs Client ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-6 col-lg-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon bg-light-blue spnLabel">User Account</span>
                                                    <asp:DropDownList ID="ddlRoleUser" runat="server" AutoPostBack="true" AppendDataBoundItems="true" CssClass="form-control"
                                                        Enabled="false" OnSelectedIndexChanged="ddlRoleUser_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select SMARTs User Account ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-12 col-sm-6 col-lg-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon bg-light-blue spnLabel">Role</span>
                                                    <asp:DropDownList ID="ddlRole" runat="server" AutoPostBack="true" AppendDataBoundItems="true" CssClass="form-control"
                                                        Enabled="false" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select SMARTs Role ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div runat="server" id="rlEnclosure" visible="false">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                    <h4><i runat="server" id="iRole"><span runat="server" id="spnRole"></span></i></h4>
                                                    <asp:Label ID="lblRoleSuccess" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="width: 100%;">
                                                <asp:Button ID="btnRoleUpdate" runat="server" CssClass="btn btn-success" Width="10%" Text="Update" OnClick="btnRoleUpdate_Click" />
                                                <asp:Button ID="btnRoleCancel" runat="server" CssClass="btn btn-warning" Width="10%" Text="Cancel" Enabled="false"
                                                    OnClick="btnRoleCancel_Click" CausesValidation="False" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div id="Pswd" class="tab-pane fade">
                    <asp:UpdateProgress ID="uprgrsPassword" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlPassword">
                        <ProgressTemplate>
                            <div id="divPasswordProcessing" runat="server" class="updateProgress">
                                <asp:Image ID="imgPasswordProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlPassword">
                        <ContentTemplate>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h5 class="panel-title"><span>SMARTs User Account Password</span></h5>
                                </div>
                                <div class="panel-body">
                                    <div style="min-height: 450px;">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-6 col-lg-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon bg-light-blue spnLabel">Client Type</span>
                                                    <asp:DropDownList ID="ddlPwdClientType" runat="server" CssClass="form-control"
                                                        AutoPostBack="True" AppendDataBoundItems="True" OnSelectedIndexChanged="ddlPwdClientType_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select SMARTs Client Type ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-12 col-sm-6 col-lg-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon bg-light-blue spnLabel">Client</span>
                                                    <asp:DropDownList ID="ddlPwdClient" runat="server" AutoPostBack="True" AppendDataBoundItems="True"
                                                        CssClass="form-control" Enabled="False" OnSelectedIndexChanged="ddlPwdClient_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select SMARTs Client ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-6 col-lg-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon bg-light-blue spnLabel">User Account</span>
                                                    <asp:DropDownList ID="ddlPwdUser" runat="server" AutoPostBack="true" AppendDataBoundItems="true" CssClass="form-control"
                                                        Enabled="false" OnSelectedIndexChanged="ddlPwdUser_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- Select SMARTs User Account ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-12 col-sm-6 col-lg-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon bg-light-blue spnLabel">Password</span>
                                                    <asp:TextBox ID="txtPwdPassword" runat="server" CssClass="form-control" Text="" Enabled="false" AutoComplete="off" placeholder="New Password" />
                                                    <asp:RequiredFieldValidator ID="rfvPwdPassword" runat="server" ControlToValidate="txtPwdPassword" Display="Dynamic" ForeColor="Maroon"
                                                        SetFocusOnError="True" Text="*" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div runat="server" id="pwdEnclosure" visible="false">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                    <h4><i runat="server" id="iPwd"><span runat="server" id="spnPwd"></span></i></h4>
                                                    <asp:Label ID="lblPwdSuccess" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="input-group" style="width: 100%;">
                                                    <asp:Button ID="btnUpdUsrPassword" runat="server" CssClass="btn btn-success" Width="10%" Text="Update" OnClick="btnUpdUsrPassword_Click" />
                                                    <asp:Button ID="btnUpdUsrCancel" runat="server" CssClass="btn btn-warning" Text="Cancel" OnClick="btnUpdUsrCancel_Click"
                                                        CausesValidation="False" Width="10%" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</div>

