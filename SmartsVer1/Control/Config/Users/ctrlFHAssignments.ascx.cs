﻿using System;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Net.Mail;
using System.Web.Profile;
using System.Web.Security;
using System.Globalization;
using System.Data.SqlClient;
using System.Collections;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using CLR = System.Drawing.Color;
using USR = SmartsVer1.Helpers.AccountHelpers.CreateUser;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using SCH = SmartsVer1.Helpers.AccountHelpers.ClientScheduler;

namespace SmartsVer1.Control.Config.Users
{
    public partial class ctrlFHAssignments : System.Web.UI.UserControl
    {
        String LoginName, username, domain, GetClientDBString;
        Int32 ClientID = -99;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.clndrSAStart.SelectedDate = DateTime.Now;
                this.clndrSAEnd.SelectedDate = DateTime.Now;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }   

        protected void ddlSchWell_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 jID = Convert.ToInt32(this.ddlSchJob.SelectedValue);
                Int32 wID = Convert.ToInt32(this.ddlSchWell.SelectedValue);
                System.Data.DataTable schTable = SCH.getFHScheduleTable(jID, wID, GetClientDBString);
                this.gvwSchedule.DataSource = schTable;
                this.gvwSchedule.DataBind();
                this.gvwSchedule.Visible = true;

                this.gvwSchedule.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSchedule_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                foreach (GridViewRow row in gvwSchedule.Rows)
                {
                    row.BackColor = System.Drawing.ColorTranslator.FromHtml("#ff8000");
                    System.Guid uID = (Guid)this.gvwSchedule.DataKeys[row.RowIndex].Values[1];
                    System.Data.DataTable pAddress = SCH.getAddressDetail(uID);
                    this.gvwProfileAdd.DataSource = pAddress;
                    this.gvwProfileAdd.DataBind();
                    this.gvwProfileAdd.Visible = true;
                    System.Data.DataTable uAddress = SCH.getUserOfficeAddress(uID, GetClientDBString);
                    this.gvwAssignmentAdd.DataSource = uAddress;
                    this.gvwAssignmentAdd.DataBind();
                    this.gvwAssignmentAdd.Visible = true;
                    System.Guid sch = (Guid)this.gvwSchedule.DataKeys[row.RowIndex].Values[2];
                    System.Data.DataTable sAddress = SCH.getAddressDetail(sch);
                    this.gvwSchedulerAdd.DataSource = sAddress;
                    this.gvwSchedulerAdd.DataBind();
                    this.gvwSchedulerAdd.Visible = true;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSchedule_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Int32 jID = Convert.ToInt32(this.ddlSchJob.SelectedValue);
                Int32 wID = Convert.ToInt32(this.ddlSchWell.SelectedValue);
                System.Data.DataTable schTable = SCH.getFHScheduleTable(jID, wID, GetClientDBString);
                this.gvwSchedule.PageIndex = e.NewPageIndex;
                this.gvwSchedule.DataSource = schTable;
                this.gvwSchedule.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnNewSch_Click(object sender, EventArgs e)
        {
            try
            {
                btnNewSchClear_Click(null, null);
                this.divSchList.Visible = false;
                this.divSchAdd.Visible = true;
                this.btnSchDone.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnNewSchClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.ddlSchJob.Items.Clear();
                this.ddlSchJob.DataBind();
                this.ddlSchJob.Items.Insert(0, new ListItem("--- Select Job ---", "-1"));
                this.ddlSchJob.SelectedIndex = -1;
                this.ddlSchJob.Enabled = false;
                this.ddlSchWell.Items.Clear();
                this.ddlSchWell.DataBind();
                this.ddlSchWell.Items.Insert(0, new ListItem("--- Select Well ---", "-1"));
                this.ddlSchWell.SelectedIndex = -1;
                this.ddlSchWell.Enabled = false;
                this.gvwSchedule.DataBind();
                this.gvwSchedule.Visible = false;
                this.gvwProfileAdd.DataBind();
                this.gvwProfileAdd.Visible = false;
                this.gvwAssignmentAdd.DataBind();
                this.gvwAssignmentAdd.Visible = false;
                this.gvwSchedulerAdd.DataBind();
                this.gvwSchedulerAdd.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlSAWell_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                String roleName = "Fieldhand";
                Dictionary<Guid, String> usersList = new Dictionary<Guid, String>();
                usersList = SmartsVer1.Helpers.AccountHelpers.CreateUser.getRoleUsersList(ClientID, roleName);
                this.ddlSAUser.Items.Clear();
                this.ddlSAUser.DataSource = usersList;
                this.ddlSAUser.DataTextField = "Value";
                this.ddlSAUser.DataValueField = "Key";
                this.ddlSAUser.DataBind();
                this.ddlSAUser.Items.Insert(0, new ListItem("--- Select User ---", "-1"));
                this.ddlSAUser.SelectedIndex = -1;
                this.ddlSAUser.Enabled = true;
                this.clndrSAStart.SelectedDate = DateTime.Now;
                this.clndrSAStart.Enabled = false;
                this.txtSAStart.Enabled = false;
                this.clndrSAEnd.SelectedDate = DateTime.Now;
                this.clndrSAEnd.Enabled = false;
                this.txtSAEnd.Enabled = false;
                this.btnSchAdd.Enabled = false;
                this.schEnclosure.Visible = false;
                this.ddlSAUser.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlSAUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.clndrSAStart.SelectedDate = DateTime.Now;
                this.clndrSAStart.Enabled = true;
                this.txtSAStart.Enabled = true;
                this.clndrSAEnd.SelectedDate = DateTime.Now;
                this.clndrSAEnd.Enabled = true;
                this.txtSAEnd.Enabled = true;
                this.btnSchAdd.Enabled = false;

                this.clndrSAStart.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSchAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, wID = -99;
                String Usr = null;
                DateTime sD = DateTime.Now;
                DateTime eD = DateTime.Now;

                wID = Convert.ToInt32(this.ddlSAWell.SelectedValue);
                Usr = Convert.ToString(this.ddlSAUser.SelectedValue);
                sD = Convert.ToDateTime(this.clndrSAStart.SelectedDate);
                eD = Convert.ToDateTime(this.clndrSAEnd.SelectedDate);

                if (String.IsNullOrEmpty(Usr) || clndrSAStart.SelectedDate.Equals(DateTime.MinValue) || clndrSAEnd.SelectedDate.Equals(DateTime.MinValue))
                {
                    btnSchClear_Click(null, null);
                    this.lblSchAdd.Text = "Please select a User, Start Date and End Date before creating Schedule for user.";
                    this.schEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iSCHMessage.Attributes["class"] = "icon fa fa-warning";
                    this.spnSCH.InnerText = " Missing information!";
                    this.schEnclosure.Visible = true;
                }
                else
                {
                    Guid uID = new Guid(Usr);
                    iResult = SmartsVer1.Helpers.AccountHelpers.CreateUser.insertFHSchedule(wID, uID, sD, eD, LoginName, domain, GetClientDBString);
                    if (iResult.Equals(1))
                    {
                        btnSchClear_Click(null, null);
                        this.lblSchAdd.Text = "Successfully created Schedule for selected Fieldhand.";
                        this.schEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                        this.iSCHMessage.Attributes["class"] = "icon fa fa-check-circle";
                        this.spnSCH.InnerText = " Success!";
                        this.schEnclosure.Visible = true;
                    }
                    else
                    {
                        btnSchClear_Click(null, null);
                        this.lblSchAdd.Text = "Unable to create Schedule for selected Fieldhand. Please try again.";
                        this.schEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                        this.iSCHMessage.Attributes["class"] = "icon fa fa-warning";
                        this.spnSCH.InnerText = " Warning!";
                        this.schEnclosure.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSchClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.ddlSAWell.Items.Clear();
                this.ddlSAWell.DataBind();
                this.ddlSAWell.Items.Insert(0, new ListItem("--- Select Well ---", "-1"));
                this.ddlSAWell.SelectedIndex = -1;
                this.ddlSAWell.Enabled = false;
                this.ddlSAUser.Items.Clear();
                this.ddlSAUser.DataBind();
                this.ddlSAUser.Items.Insert(0, new ListItem("--- Select User ---", "-1"));
                this.ddlSAUser.SelectedIndex = -1;
                this.ddlSAUser.Enabled = false;
                this.clndrSAStart.SelectedDate = DateTime.Now;
                this.clndrSAStart.Enabled = false;
                this.clndrSAEnd.SelectedDate = DateTime.Now;
                this.clndrSAEnd.Enabled = false;
                this.schEnclosure.Visible = false;
                this.btnSchAdd.Enabled = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void clndrSAStart_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                this.clndrSAEnd.SelectedDate = DateTime.Now;
                this.clndrSAEnd.Enabled = true;
                this.schEnclosure.Visible = false;
                this.btnSchAdd.Enabled = false;

                this.clndrSAEnd.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void clndrSAEnd_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                this.schEnclosure.Visible = false;
                this.btnSchAdd.Enabled = true;

                this.btnSchAdd.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnSchDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnSchClear_Click(null, null);
                this.divSchAdd.Visible = false;
                this.divSchList.Visible = true;
                this.btnSchDone.Visible = false;
                this.ddlSchJob.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}