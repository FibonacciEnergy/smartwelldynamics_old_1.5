﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlFHAssignments.ascx.cs" Inherits="SmartsVer1.Control.Config.Users.ctrlFHAssignments" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

<link href="../../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript -->
<script src="../../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>

<!--- Page local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }

    .rotate {
        -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
        -moz-transform: rotate(90deg); /* Firefox */
        -ms-transform: rotate(90deg); /* IE 9 */
        transform: rotate(90deg); /* Standard syntax */
    }

    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper[data-stepper-steps="8"] li {
            width: 12%;
        }

        ol.stepper[data-stepper-steps="9"] li {
            width: 11%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }    
</style>
<!--- Page local script --->
<script type="text/javascript">
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("rotate");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").addClass("rotate");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("rotate");
        });
    });
</script>

<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>Configuration</li>
                <li><a href="../../../Config/Users/fecDRFHAssign_2A.aspx"> Fieldhands Schedule</a></li>
            </ol>
        </div>
    </div>
</div>

<!-- Page Content -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <asp:UpdateProgress ID="upgrsSCH" runat="server" AssociatedUpdatePanelID="upnlSCH" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="divSCHSurround" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                        z-index: 100;">
                        <asp:Image ID="upgrsSCHImage" runat="server" CssClass="center" AlternateText="Processing ...." ForeColor="Purple" ImageAlign="Middle"
                            ImageUrl="~/Images/Progress.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel runat="server" ID="upnlSCH">
                <ContentTemplate>
                    <div id="divGRTitle" class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <hr style="margin: auto;" />
                            <div class="bg-light-blue">
                                <div class="text-center">
                                    <h1 class="box-title">Fieldhand Assignments</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="divSchList" runat="server">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon bg-light-blue">Job/Work Order #</span>
                                    <asp:DropDownList runat="server" ID="ddlSchJob" CssClass="form-control" Enabled="false" AutoPostBack="True"
                                        AppendDataBoundItems="True"
                                        OnSelectedIndexChanged="ddlSchJob_SelectedIndexChanged">
                                        <asp:ListItem Value="0" Text="--- Select Job ---" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon bg-light-blue">Well</span>
                                    <asp:DropDownList runat="server" ID="ddlSchWell" CssClass="form-control" Enabled="false" AutoPostBack="true"
                                        AppendDataBoundItems="true"
                                        OnSelectedIndexChanged="ddlSchWell_SelectedIndexChanged">
                                        <asp:ListItem Value="0" Text="--- Select Well ---" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <asp:GridView runat="server" ID="gvwSchedule" CssClass="mydatagrid" AutoGenerateColumns="false" EmptyDataText="No Fieldhands scheduled yet!"
                                ShowHeaderWhenEmpty="true" DataKeyNames="uschID,fhand,scheduler" AllowPaging="True" AllowSorting="True"
                                Visible="false" AlternatingRowStyle-CssClass="AlternatingRowStyle" HeaderStyle-CssClass="header" RowStyle-CssClass="rows"
                                OnSelectedIndexChanged="gvwSchedule_SelectedIndexChanged"
                                OnPageIndexChanging="gvwSchedule_PageIndexChanging">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" ItemStyle-ForeColor="Blue" SelectText="View Detail" />
                                    <asp:BoundField DataField="uschID" Visible="false" />
                                    <asp:BoundField DataField="fhand" Visible="false" />
                                    <asp:BoundField DataField="lName" HeaderText="Last Name" />
                                    <asp:BoundField DataField="fName" HeaderText="First Name" />
                                    <asp:BoundField DataField="fhDesig" HeaderText="Designation" />
                                    <asp:BoundField DataField="fhStart" HeaderText="Start" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                    <asp:BoundField DataField="fhEnd" HeaderText="End" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                    <asp:BoundField DataField="scheduler" Visible="false" />
                                    <asp:BoundField DataField="schdlr" HeaderText="Scheduled By" />
                                    <asp:BoundField DataField="schDesig" HeaderText="Designation" />
                                    <asp:BoundField DataField="uTime" HeaderText="Last Updated" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                </Columns>
                            </asp:GridView>
                            <asp:GridView runat="server" ID="gvwProfileAdd" CssClass="mydatagrid" AutoGenerateColumns="false" EmptyDataText="No Profile Address found for selected Fieldhand."
                                ShowHeaderWhenEmpty="true" DataKeyNames="pAddID" AllowPaging="True" AllowSorting="True" Visible="false"
                                AlternatingRowStyle-CssClass="AlternatingRowStyle" HeaderStyle-CssClass="header" RowStyle-CssClass="rows">
                                <Columns>
                                    <asp:BoundField DataField="pAddID" Visible="false" />
                                    <asp:BoundField DataField="pAddTitle" HeaderText="Title" />
                                    <asp:BoundField DataField="pAddSt1" HeaderText="Street 1" />
                                    <asp:BoundField DataField="pAddSt2" HeaderText="Street 2" />
                                    <asp:BoundField DataField="pAddSt3" HeaderText="Street 3" />
                                    <asp:BoundField DataField="pAddCity" HeaderText="City/Town" />
                                    <asp:BoundField DataField="pAddCounty" HeaderText="County/District" />
                                    <asp:BoundField DataField="pAddState" HeaderText="State/Province" />
                                    <asp:BoundField DataField="pAddZip" HeaderText="Zip/Postal Code" />
                                    <asp:BoundField DataField="pAddCountry" HeaderText="Country" />
                                    <asp:BoundField DataField="pAddPh" HeaderText="Phone" />
                                    <asp:BoundField DataField="pAddPhT" HeaderText="Type" />
                                </Columns>
                            </asp:GridView>
                            <asp:GridView runat="server" ID="gvwAssignmentAdd" CssClass="mydatagrid" AutoGenerateColumns="false" EmptyDataText="No Office Address found for selected Fieldhand."
                                ShowHeaderWhenEmpty="true" DataKeyNames="pAddID" AllowPaging="True" AllowSorting="True" Visible="false"
                                AlternatingRowStyle-CssClass="AlternatingRowStyle" HeaderStyle-CssClass="header" RowStyle-CssClass="rows">
                                <Columns>
                                    <asp:BoundField DataField="pAddID" Visible="false" />
                                    <asp:BoundField DataField="pAddTitle" HeaderText="Title" />
                                    <asp:BoundField DataField="pAddSt1" HeaderText="Street 1" />
                                    <asp:BoundField DataField="pAddSt2" HeaderText="Street 2" />
                                    <asp:BoundField DataField="pAddSt3" HeaderText="Street 3" />
                                    <asp:BoundField DataField="pAddCity" HeaderText="City/Town" />
                                    <asp:BoundField DataField="pAddCounty" HeaderText="County/District" />
                                    <asp:BoundField DataField="pAddState" HeaderText="State/Province" />
                                    <asp:BoundField DataField="pAddZip" HeaderText="Zip/Postal Code" />
                                    <asp:BoundField DataField="pAddCountry" HeaderText="Country" />
                                    <asp:BoundField DataField="pAddPh" HeaderText="Phone" />
                                    <asp:BoundField DataField="pAddPhT" HeaderText="Type" />
                                </Columns>
                            </asp:GridView>
                            <asp:GridView runat="server" ID="gvwSchedulerAdd" CssClass="mydatagrid" AutoGenerateColumns="false" EmptyDataText="No Office Address found for Scheduler."
                                ShowHeaderWhenEmpty="true" DataKeyNames="pAddID" AllowPaging="True" AllowSorting="True" Visible="false"
                                AlternatingRowStyle-CssClass="AlternatingRowStyle" HeaderStyle-CssClass="header" RowStyle-CssClass="rows">
                                <Columns>
                                    <asp:BoundField DataField="pAddID" Visible="false" />
                                    <asp:BoundField DataField="pAddTitle" HeaderText="Title" />
                                    <asp:BoundField DataField="pAddSt1" HeaderText="Street 1" />
                                    <asp:BoundField DataField="pAddSt2" HeaderText="Street 2" />
                                    <asp:BoundField DataField="pAddSt3" HeaderText="Street 3" />
                                    <asp:BoundField DataField="pAddCity" HeaderText="City/Town" />
                                    <asp:BoundField DataField="pAddCounty" HeaderText="County/District" />
                                    <asp:BoundField DataField="pAddState" HeaderText="State/Province" />
                                    <asp:BoundField DataField="pAddZip" HeaderText="Zip/Postal Code" />
                                    <asp:BoundField DataField="pAddCountry" HeaderText="Country" />
                                    <asp:BoundField DataField="pAddPh" HeaderText="Phone" />
                                    <asp:BoundField DataField="pAddPhT" HeaderText="Type" />
                                </Columns>
                            </asp:GridView>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="input-group" style="width: 100%;">
                                <asp:Button ID="btnNewSch" runat="server" CssClass="btn btn-info text-center" Width="10%" Text="Schedule Fieldhand" OnClick="btnNewSch_Click"
                                    CausesValidation="False" />
                                <asp:Button ID="btnNewSchClear" runat="server" CssClass="btn btn-warning text-center" Width="10%" Text="Clear Values" OnClick="btnNewSchClear_Click"
                                    CausesValidation="False" />
                            </div>
                        </div>
                    </div>
                    <div id="divSchAdd" runat="server" visible="false">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon bg-light-blue">Well</span>
                                    <asp:DropDownList runat="server" ID="ddlSAWell" CssClass="form-control" Enabled="false" AutoPostBack="true"
                                        AppendDataBoundItems="true"
                                        OnSelectedIndexChanged="ddlSAWell_SelectedIndexChanged">
                                        <asp:ListItem Value="0" Text="--- Select Well ---" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon bg-light-blue">User</span>
                                    <asp:DropDownList runat="server" ID="ddlSAUser" CssClass="form-control" Enabled="False" AppendDataBoundItems="True"
                                        AutoPostBack="True" OnSelectedIndexChanged="ddlSAUser_SelectedIndexChanged">
                                        <asp:ListItem Value="0" Text="--- Select User ---" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon bg-light-blue">Start</span>
                                    <asp:TextBox runat="server" ID="txtSAStart" CssClass="form-control" Enabled="false" TextMode="DateTimeLocal" />
                                    <ajaxToolkit:CalendarExtender ID="clndrSAStart" runat="server" TargetControlID="txtSAStart" />
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon bg-light-blue">End</span>
                                    <asp:TextBox runat="server" ID="txtSAEnd" CssClass="form-control" Enabled="false" TextMode="DateTimeLocal" />
                                    <ajaxToolkit:CalendarExtender ID="clndrSAEnd" runat="server" TargetControlID="txtSAEnd" />
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="width: 100%;">
                                <div id="schEnclosure" runat="server" visible="false">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                    <h4><i runat="server" id="iSCHMessage"><span runat="server" id="spnSCH"></span></i></h4>
                                    <asp:Label ID="lblSchAdd" runat="server" />
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="width: 100%;">
                                <asp:Button ID="btnSchAdd" runat="server" CssClass="btn btn-success text-center" Width="10%" Enabled="false" Text="Add Schedule"
                                    OnClick="btnSchAdd_Click" />
                                <asp:Button ID="btnSchClear" runat="server" CssClass="btn btn-warning text-center" Width="10%" Text="Clear Selections" OnClick="btnSchClear_Click"
                                    CausesValidation="False" />
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="width: 100%;">
                                <asp:Button ID="btnSchDone" runat="server" CssClass="btn btn-primary text-center" Width="10%" Text="Done" Visible="false"
                                    OnClick="btnSchDone_Click"
                                    CausesValidation="False" />
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
