﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlClientDRHR.ascx.cs" Inherits="SmartsVer1.Control.Config.Users.ctrlClientDRHR" %>

<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<!-- Stylesheet -->
<link href="../../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<link href='https://api.mapbox.com/mapbox-gl-js/v0.53.0/mapbox-gl.css' rel='stylesheet' />

<!--- local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }
    .swdMenu {
        color: #ffffff;
    }
    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper[data-stepper-steps="8"] li {
            width: 12%;
        }

        ol.stepper[data-stepper-steps="9"] li {
            width: 11%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }
</style>
<!-- JavaScripts -->
<script src="../../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<script src='https://static-assets.mapbox.com/gl-pricing/dist/mapbox-gl.js'></script>
<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>Configuration</li>
                <li><a href="../../../Config/Users/fecCRDHR_2A.aspx"> User Accounts</a></li>
            </ol>
        </div>
    </div>
</div>
<!--- PageContent --->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div id="acrdUsersOuter" runat="server" class="panel-group">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancUserList" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlClientDRHR_acrdUsersOuter" href="#BodyContent_ctrlClientDRHR_divUserOuter">
                                <span class="glyphicon glyphicon-menu-right"></span> User Accounts</a>
                        </h5>
                    </div>
                    <div id="divUserOuter" runat="server" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="uprgrsAcctList" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlAcctList">
                                <ProgressTemplate>
                                    <div id="divAcctListProcessing" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                        z-index: 100;">
                                        <asp:Image ID="imgAcctListProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel runat="server" ID="upnlAcctList">
                                <ContentTemplate>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div id="acrdAccttList" class="panel-group">
                                            <div class="panel panel-danger panel-body">
                                                <div id="divAccttList" runat="server" class="panel-collapse collapse in">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group" style="width: 100%;">
                                                            <asp:LinkButton ID="btnAddNUser" runat="server" CssClass="btn btn-info text-center" OnClick="btnAddNUser_Click"><i class="fa fa-plus"> User Account</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnListCancel" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnListCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                        </div>
                                                        <div class="panel-group" id="acrdUL">
                                                            <div class="panel panel-info">
                                                                <div class="panel-heading">
                                                                    <h5 class="panel-title" style="text-align: left">
                                                                        <a id="ancACL" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlClientDRHR_acrdUL" href="#BodyContent_ctrlClientDRHR_divUList">
                                                                            User Accounts List</a>
                                                                    </h5>
                                                                </div>
                                                                <div id="divUList" runat="server" class="panel-collapse collapse in">
                                                                    <div class="panel-body">
                                                                        <asp:GridView ID="gvwUsersList" runat="server" AutoGenerateColumns="False" AllowPaging="True" CssClass="mydatagrid" EmptyDataText="No User Accounts Found."
                                                                            ShowHeaderWhenEmpty="True" OnRowDataBound="gvwUsersList_RowDataBound" DataKeyNames="UserId" OnSelectedIndexChanged="gvwUsersList_SelectedIndexChanged"
                                                                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="8" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                                                                            RowStyle-CssClass="rows" OnPageIndexChanging="gvwUsersList_PageIndexChanging" SelectedRowStyle-CssClass="selectedrow">
                                                                            <Columns>
                                                                                <asp:ButtonField CommandName="Select" Text="Select" ButtonType="Button">
                                                                                    <ControlStyle CssClass="btn btn-info text-center"></ControlStyle>
                                                                                </asp:ButtonField>
                                                                                <asp:BoundField DataField="UserId" Visible="False" />
                                                                                <asp:BoundField DataField="lastname" HeaderText="Last Name" />
                                                                                <asp:BoundField DataField="firstName" HeaderText="First Name" />
                                                                                <asp:BoundField DataField="userName" HeaderText="User Name" />
                                                                                <asp:BoundField DataField="roleName" HeaderText="System Role" />
                                                                                <asp:TemplateField HeaderText="Enabled">
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="chkUserApproved" runat="server" />
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Locked Out">
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="chkDisabled" runat="server" />
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-info">
                                                                <div class="panel-heading">
                                                                    <h5 class="panel-title" style="text-align: left">
                                                                        <a id="a1" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlClientDRHR_acrdUL" href="#BodyContent_ctrlClientDRHR_divUList">
                                                                            User Account Detailed Info</a>
                                                                    </h5>
                                                                </div>
                                                                <div id="divUDetail" runat="server" class="panel-collapse collapse">
                                                                    <div class="panel-body">
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <asp:GridView runat="server" ID="gvwUserDetail" AutoGenerateColumns="False" DataKeyNames="uId" CssClass="mydatagrid"
                                                                                AlternatingRowStyle-CssClass="AlternatingRowStyle" HeaderStyle-CssClass="header" RowStyle-CssClass="rows">
                                                                                <Columns>
                                                                                    <asp:BoundField DataField="UserId" Visible="false" />
                                                                                    <asp:BoundField DataField="Email" HeaderText="Email" />
                                                                                    <asp:BoundField DataField="CreateDate" HeaderText="User Creation Date" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                                                    <asp:BoundField DataField="LastLoginDate" HeaderText="Last Login Date" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                                                    <asp:BoundField DataField="LastActivityDate" HeaderText="Last Activity Date" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                            <asp:GridView runat="server" ID="gvwUserAddressInfo" AutoGenerateColumns="False" DataKeyNames="usrID" CssClass="mydatagrid"
                                                                                AlternatingRowStyle-CssClass="AlternatingRowStyle" HeaderStyle-CssClass="header" RowStyle-CssClass="rows">
                                                                                <Columns>
                                                                                    <asp:BoundField DataField="usrID" Visible="false" />
                                                                                    <asp:BoundField DataField="addT" HeaderText="Address Type" />
                                                                                    <asp:BoundField DataField="dsg" HeaderText="Designation" />
                                                                                    <asp:BoundField DataField="Street1" HeaderText="Street 1" />
                                                                                    <asp:BoundField DataField="Street2" HeaderText="Street 2" />
                                                                                    <asp:BoundField DataField="Street3" HeaderText="Street 3" />
                                                                                    <asp:BoundField DataField="uCity" HeaderText="City/Town" />
                                                                                    <asp:BoundField DataField="uCounty" HeaderText="County/District" />
                                                                                    <asp:BoundField DataField="uState" HeaderText="State/Province" />
                                                                                    <asp:BoundField DataField="uCountry" HeaderText="Country" />
                                                                                    <asp:BoundField DataField="uZip" HeaderText="Zip/Postal Code" />
                                                                                    <asp:BoundField DataField="uSubReg" HeaderText="Sub-Region" />
                                                                                    <asp:BoundField DataField="uReg" HeaderText="Region" />
                                                                                    <asp:BoundField DataField="uPh" HeaderText="Phone" />
                                                                                    <asp:BoundField DataField="uPhT" HeaderText="Type" />
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                                <div id="divMap" runat="server" style="position: absolute; align-content: center; top: 0; bottom: 0; width: 100%; height: 200px;"></div>                                                                        
                                                                                <asp:Literal ID="ltrAddressMap" runat="server"></asp:Literal>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-success">
                                                                <div class="panel-heading">
                                                                    <h5 class="panel-title" style="text-align: left">
                                                                        <a id="aACAdd" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlClientDRHR_acrdUL" href="#BodyContent_ctrlClientDRHR_divUList">
                                                                            Create User Account</a>
                                                                    </h5>
                                                                </div>
                                                                <div id="divAddUsr" runat="server" class="panel-collapse collapse">
                                                                    <div class="panel-body">
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon bg-light-blue">First Name</span>
                                                                                    <asp:TextBox ID="txtFirst" runat="server" CssClass="form-control" placeholder="First Name (required)" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon bg-light-blue">Middle Name</span>
                                                                                    <asp:TextBox ID="txtMiddle" runat="server" CssClass="form-control" placeholder="Middle Name" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon bg-light-blue">Last Name</span>
                                                                                    <asp:TextBox ID="txtLast" runat="server" CssClass="form-control" placeholder="Last Name (required)" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon bg-light-blue">Designation</span>
                                                                                    <asp:TextBox ID="txtDesig" runat="server" CssClass="form-control" Text="" placeholder="Designation (required)" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon bg-light-blue">Gender</span>
                                                                                    <asp:DropDownList ID="ddlUsrGender" runat="server" AutoPostBack="True" CssClass="form-control" OnSelectedIndexChanged="ddlUsrGender_SelectedIndexChanged">
                                                                                        <asp:ListItem Value="0" Text="--- Select Gender ---" />
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon bg-light-blue">Geographic Region</span>
                                                                                    <asp:DropDownList ID="ddlReg" runat="server" CssClass="form-control" AppendDataBoundItems="True" AutoPostBack="True" Enabled="False"
                                                                                        OnSelectedIndexChanged="ddlReg_SelectedIndexChanged">
                                                                                        <asp:ListItem Value="0" Text="--- Select Major Geographic Region ---" />
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon bg-light-blue">Sub-Region</span>
                                                                                    <asp:DropDownList ID="ddlSReg" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control" Enabled="False"
                                                                                        OnSelectedIndexChanged="ddlSReg_SelectedIndexChanged">
                                                                                        <asp:ListItem Value="0" Text="--- Select Geographic Sub-Region ---" />
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon bg-light-blue">Country/Nation</span>
                                                                                    <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control" AppendDataBoundItems="True" AutoPostBack="True"
                                                                                        Enabled="False"
                                                                                        OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                                                                                        <asp:ListItem Value="0" Text="--- Select Country/Nation ---" />
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon bg-light-blue">State/Province</span>
                                                                                    <asp:DropDownList ID="ddlState" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control" Enabled="False"
                                                                                        OnSelectedIndexChanged="ddlState_SelectedIndexChanged">
                                                                                        <asp:ListItem Value="0" Text="--- Select State/Province ---" />
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon bg-light-blue">County/District</span>
                                                                                    <asp:DropDownList ID="ddlCounty" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control" Enabled="False"
                                                                                        OnSelectedIndexChanged="ddlCounty_SelectedIndexChanged">
                                                                                        <asp:ListItem Value="0" Text="--- Select County/District/Division/Municipality ---" />
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon bg-light-blue">City/Town</span>
                                                                                    <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control" Enabled="False"
                                                                                        OnSelectedIndexChanged="ddlCity_SelectedIndexChanged">
                                                                                        <asp:ListItem Value="0" Text="--- Select City/Town ---" />
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon bg-light-blue">Zip/Postalcode</span>
                                                                                    <asp:DropDownList ID="ddlZip" runat="server" AutoPostBack="True" AppendDataBoundItems="True" CssClass="form-control" Enabled="False"
                                                                                        OnSelectedIndexChanged="ddlZip_SelectedIndexChanged">
                                                                                        <asp:ListItem Value="0">--- Select Zip/Postal Code ---</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon bg-light-blue">Street 1</span>
                                                                                    <asp:TextBox ID="txtSt1" runat="server" CssClass="form-control" Enabled="false" placeholder="Street 1 (required)" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon bg-light-blue">Street 2</span>
                                                                                    <asp:TextBox ID="txtSt2" runat="server" CssClass="form-control" Enabled="false" placeholder="Street 2" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon bg-light-blue">Street 3</span>
                                                                                    <asp:TextBox ID="txtSt3" runat="server" CssClass="form-control" Enabled="false" placeholder="Street 3" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon bg-light-blue">Email</span>
                                                                                    <asp:TextBox ID="txtEMail" runat="server" CssClass="form-control" Enabled="false" TextMode="Email" placeholder="Email (required)" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon bg-light-blue">Phone</span>
                                                                                    <asp:TextBox ID="txtPhone" runat="server" CssClass="form-control" Enabled="false" TextMode="Phone" placeholder="Phone (required)" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon bg-light-blue">Type</span>
                                                                                    <asp:DropDownList ID="ddlPhoneType" runat="server" Enabled="False" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control"
                                                                                        OnSelectedIndexChanged="ddlPhoneType_SelectedIndexChanged">
                                                                                        <asp:ListItem Value="0" Text="--- Select Phone Type ---" />
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon bg-light-blue">User Role</span>
                                                                                    <asp:DropDownList ID="ddlUserRole" runat="server" CssClass="form-control" AppendDataBoundItems="True" AutoPostBack="True"
                                                                                        Enabled="false" OnSelectedIndexChanged="ddlUserRole_SelectedIndexChanged">
                                                                                        <asp:ListItem Value="0" Text="--- Select User Role ---" />
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon bg-light-blue">Password</span>
                                                                                    <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" Enabled="false" TextMode="Password" placeholder="Password (required)" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon bg-light-blue">Confirm Password</span>
                                                                                    <asp:TextBox ID="txtCPassword" runat="server" CssClass="form-control" Enabled="false" TextMode="Password" placeholder="Confirm Password (required)" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                                <div id="usrEnclosure" runat="server" visible="false">
                                                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                                    <h4><i runat="server" id="iUSRMessage"><span runat="server" id="spnUSR"></span></i></h4>
                                                                                    <asp:Label runat="server" ID="lblSuccess" CssClass="form-control" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                                <div class="input-group" style="width: 100%;">
                                                                                    <asp:LinkButton ID="btnAddUser" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnAddUser_Click"><i class="fa fa-plus"> User</i></asp:LinkButton>
                                                                                    <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                                                    <asp:LinkButton ID="btnAddUserDone" runat="server" CssClass="btn btn-danger text-center" OnClick="btnAddUserDone_Click"><i class="fa fa-times-rectangle"> Done</i></asp:LinkButton>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="gvwUsersList" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h5 class="panel-title" style="text-align: left">
                            <a id="ancUserStatus" runat="server" data-toggle="collapse" data-parent="#BodyContent_ctrlClientDRHR_acrdUsersOuter" href="#BodyContent_ctrlClientDRHR_divUserStatus">
                                <span class="glyphicon glyphicon-menu-right"></span> User Account Status</a>
                        </h5>
                    </div>
                    <div id="divUserStatus" runat="server" class="panel-collapse collapse">
                        <div class="panel-body">
                            <asp:UpdateProgress ID="uprgrsupnlAcctStatus" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlAcctStatus">
                                <ProgressTemplate>
                                    <div id="divupnlAcctStatusProcessing" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle;
                                        text-align: center; z-index: 100;">
                                        <asp:Image ID="imgupnlAcctStatusProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel runat="server" ID="upnlAcctStatus">
                                <ContentTemplate>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div id="divAccttStatus" class="panel-group">
                                            <div class="panel panel-danger panel-body">
                                                <div id="divUserList" runat="server">
                                                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                        <div class="input-group">
                                                            <span class="input-group-addon bg-light-blue">Type</span>
                                                            <asp:DropDownList ID="ddlLockoutUserType" runat="server" AutoPostBack="true" AppendDataBoundItems="true" CssClass="form-control"
                                                                OnSelectedIndexChanged="ddlLockoutUserType_SelectedIndexChanged">
                                                                <asp:ListItem Value="0" Text="--- Select User Account Type ---" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                        <div class="input-group">
                                                            <span class="input-group-addon bg-light-blue">Account</span>
                                                            <asp:DropDownList ID="ddlLockoutUserList" runat="server" AutoPostBack="true" AppendDataBoundItems="true" CssClass="form-control"
                                                                OnSelectedIndexChanged="ddlLockoutUserList_SelectedIndexChanged" Enabled="False">
                                                                <asp:ListItem Value="0" Text="--- Select SMARTs User Account ---" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                        <div class="input-group">
                                                            <span class="input-group-addon bg-light-blue">Status</span>
                                                            <asp:DropDownList ID="ddlLockout" runat="server" CssClass="form-control" AppendDataBoundItems="True" AutoPostBack="True"
                                                                Enabled="False" OnSelectedIndexChanged="ddlLockout_SelectedIndexChanged">
                                                                <asp:ListItem Value="0" Text="--- UnLock User Account ---" />
                                                                <asp:ListItem Value="1" Text="UnLock" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div id="stEnclosure" runat="server" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <h4><i runat="server" id="iSTMessage"><span runat="server" id="spnStatus"></span></i></h4>
                                                            <asp:Label ID="lblLockoutSuccess" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="input-group" style="width: 100%;">
                                                            <asp:LinkButton ID="btnLockoutCancel" runat="server" CssClass="btn btn-default text-center" Enabled="false" OnClick="btnLockoutCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

