﻿using System;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Net.Mail;
using System.Web.Profile;
using System.Web.Security;
using System.Globalization;
using System.Data.SqlClient;
using System.Collections;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using CLR = System.Drawing.Color;
using MAP = SmartsVer1.Helpers.ChartHelpers.MapBox;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using USR = SmartsVer1.Helpers.AccountHelpers.CreateUser;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using SCH = SmartsVer1.Helpers.AccountHelpers.ClientScheduler;

namespace SmartsVer1.Control.Config.Users
{
    public partial class ctrlClientDRHR : System.Web.UI.UserControl
    {
        String LoginName, username, domain, GetClientDBString;
        Int32 ClientID = -99;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
                //DataSource for controls on page
                System.Data.DataTable usrinfoTable = USR.getClientUsersTable(ClientID);
                Dictionary<System.Guid, String> roleList = USR.getAllRoles();

                //Assign Connection String value to all SqlDataSource Controls on the page.
                this.gvwUsersList.DataSource = usrinfoTable;
                this.gvwUsersList.DataBind();
                this.ddlUserRole.DataSource = roleList;
                this.ddlUserRole.DataTextField = "Value";
                this.ddlUserRole.DataValueField = "Key";
                this.ddlUserRole.DataBind();
                this.ddlLockoutUserType.DataSource = roleList;
                this.ddlLockoutUserType.DataTextField = "Value";
                this.ddlLockoutUserType.DataValueField = "Key";
                this.ddlLockoutUserType.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwUsersList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow drRes = ((DataRowView)e.Row.DataItem).Row;
                    Int32 userApproved = -99;
                    Int32 userLockedOut = -99;
                    userApproved = Convert.ToInt32(drRes["enabled"]);
                    userLockedOut = Convert.ToInt32(drRes["locked"]);
                    CheckBox chkUApp = e.Row.Cells[6].FindControl("chkUserApproved") as CheckBox;
                    CheckBox chkULck = e.Row.Cells[7].FindControl("chkDisabled") as CheckBox;

                    if (userApproved.Equals(1))
                    {
                        chkUApp.Checked = true;
                    }
                    else
                    {
                        chkUApp.Checked = false;
                    }

                    if (userLockedOut.Equals(1))
                    {
                        chkULck.Checked = true;
                    }
                    else
                    {
                        chkULck.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnListCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.divUList.Attributes["class"] = "panel-collapse collapse in";
                this.divUDetail.Attributes["class"] = "panel-collapse collapse";
                this.divAddUsr.Attributes["class"] = "panel-collapse collapse";
                this.ancACL.InnerHtml = "User Accounts List";
                System.Data.DataTable usrinfoTable = USR.getClientUsersTable(ClientID);
                this.gvwUsersList.DataSource = usrinfoTable;
                this.gvwUsersList.PageIndex = 0;
                this.gvwUsersList.DataBind();
                this.gvwUsersList.SelectedIndex = -1;
                this.gvwUserDetail.DataBind();
                this.gvwUserAddressInfo.DataBind();
                this.btnListCancel.Enabled = false;
                this.btnListCancel.CssClass = "btn btn-default text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlUserRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {                
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = true;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = true;
                this.btnAddUser.Enabled = true;
                this.btnAddUser.CssClass = "btn btn-success text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlUsrGender_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Int32, String> regList = DMG.getRegionList();
                this.ddlReg.Items.Clear();
                this.ddlReg.DataSource = regList;
                this.ddlReg.DataTextField = "Value";
                this.ddlReg.DataValueField = "Key";
                this.ddlReg.DataBind();
                this.ddlReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlReg.SelectedIndex = -1;
                this.ddlReg.Enabled = true;
                this.ddlSReg.Items.Clear();
                this.ddlSReg.DataBind();
                this.ddlSReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlSReg.SelectedIndex = -1;
                this.ddlSReg.Enabled = false;
                this.ddlCountry.Items.Clear();
                this.ddlCountry.DataBind();
                this.ddlCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlCountry.SelectedIndex = -1;
                this.ddlCountry.Enabled = false;
                this.ddlState.Items.Clear();
                this.ddlState.DataBind();
                this.ddlState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlState.SelectedIndex = -1;
                this.ddlState.Enabled = false;
                this.ddlCounty.Items.Clear();
                this.ddlCounty.DataBind();
                this.ddlCounty.Items.Insert(0, new ListItem("--- Select County/District/Division/Municipality ---", "-1"));
                this.ddlCounty.SelectedIndex = -1;
                this.ddlCounty.Enabled = false;
                this.ddlCity.Items.Clear();
                this.ddlCity.DataBind();
                this.ddlCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCity.SelectedIndex = -1;
                this.ddlCity.Enabled = false;
                this.ddlZip.Items.Clear();
                this.ddlZip.DataBind();
                this.ddlZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlZip.SelectedIndex = -1;
                this.ddlZip.Enabled = false;
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = false;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = false;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;
                this.btnAddUser.Enabled = false;
                this.btnAddUser.CssClass = "btn btn-default text-center";
                this.btnCancel.Enabled = true;
                this.btnCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwUsersList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.divUList.Attributes["class"] = "panel-collapse collapse";
                this.divUDetail.Attributes["class"] = "panel-collapse collapse in";
                GridViewRow dRow = this.gvwUsersList.SelectedRow;
                String lName = Convert.ToString(Server.HtmlDecode(dRow.Cells[2].Text));
                String fName = Convert.ToString(Server.HtmlDecode(dRow.Cells[3].Text));
                String role = Convert.ToString(Server.HtmlDecode(dRow.Cells[5].Text));
                this.ancACL.InnerHtml = "User : <span style='text-decoration: underline; font-weight: bold'>" + lName + ", " + fName + "( " + role + " )" + "</span>";
                System.Guid user = new System.Guid();
                user = (Guid)this.gvwUsersList.SelectedValue;
                System.Data.DataTable detailTable = USR.getUserDetailTable(user);
                this.gvwUserDetail.DataSource = detailTable;
                this.gvwUserDetail.DataBind();
                System.Data.DataTable usrinfoTable = USR.getUserProfileInfo(user);
                this.gvwUserAddressInfo.DataSource = usrinfoTable;
                this.gvwUserAddressInfo.DataBind();
                this.btnListCancel.Enabled = true;
                this.btnListCancel.CssClass = "btn btn-warning text-center";
                this.ltrAddressMap.Text = MAP.getUserAssignedLocation(ClientID, user, "BodyContent_ctrlClientDRHR_divMap", GetClientDBString);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {                
                this.txtFirst.Text = "";
                this.txtMiddle.Text = "";
                this.txtLast.Text = "";
                this.txtDesig.Text = "";
                Dictionary<Int32, String> gndList = DMG.getGenderList();
                this.ddlUsrGender.Items.Clear();
                this.ddlUsrGender.DataSource = gndList;
                this.ddlUsrGender.DataTextField = "Value";
                this.ddlUsrGender.DataValueField = "Key";
                this.ddlUsrGender.DataBind();
                this.ddlUsrGender.Items.Insert(0, new ListItem("--- Select Gender ---", "-1"));
                this.ddlUsrGender.SelectedIndex = -1;
                this.txtSt1.Text = "";
                this.txtSt1.Enabled = false;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = false;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = false;
                this.ddlReg.Items.Clear();
                this.ddlReg.DataBind();
                this.ddlReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlReg.SelectedIndex = -1;
                this.ddlReg.Enabled = false;
                this.ddlSReg.Items.Clear();
                this.ddlSReg.DataBind();
                this.ddlSReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlSReg.SelectedIndex = -1;
                this.ddlSReg.Enabled = false;
                this.ddlCountry.Items.Clear();
                this.ddlCountry.DataBind();
                this.ddlCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlCountry.SelectedIndex = -1;
                this.ddlCountry.Enabled = false;
                this.ddlState.Items.Clear();
                this.ddlState.DataBind();
                this.ddlState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlState.SelectedIndex = -1;
                this.ddlState.Enabled = false;
                this.ddlCounty.Items.Clear();
                this.ddlCounty.DataBind();
                this.ddlCounty.Items.Insert(0, new ListItem("--- Select County/District/Division/Municipality ---", "-1"));
                this.ddlCounty.SelectedIndex = -1;
                this.ddlCounty.Enabled = false;
                this.ddlCity.Items.Clear();
                this.ddlCity.DataBind();
                this.ddlCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCity.SelectedIndex = -1;
                this.ddlCity.Enabled = false;
                this.ddlZip.Items.Clear();
                this.ddlZip.DataBind();
                this.ddlZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlZip.SelectedIndex = -1;
                this.ddlZip.Enabled = false;
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = false;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = false;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;
                this.ddlUserRole.Items.Clear();
                this.ddlUserRole.DataBind();
                this.ddlUserRole.Items.Insert(0, new ListItem("--- Select User Role ---", "-1"));
                this.ddlUserRole.SelectedIndex = -1;
                this.ddlUserRole.Enabled = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99, clntName = -99, phnType = -99, reg = -99, sreg = -99, Country = -99, State = -99, County = -99;
                Int32 City = -99, zip = -99, gID = -99;
                String usrRole = String.Empty, FName = String.Empty, MName = String.Empty, LName = String.Empty, UsrPh = String.Empty, UsrEml = String.Empty;
                String UsrPwd = String.Empty, Desig = String.Empty, st1 = String.Empty, st2 = String.Empty, st3 = String.Empty;

                TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                //clntType = 1;
                clntName = Convert.ToInt32(Session["clntID"]);
                FName = ti.ToTitleCase(this.txtFirst.Text);
                MName = ti.ToTitleCase(this.txtMiddle.Text);
                LName = ti.ToTitleCase(this.txtLast.Text);
                UsrPwd = this.txtPassword.Text;
                UsrPh = this.txtPhone.Text;
                UsrEml = this.txtEMail.Text;
                usrRole = Convert.ToString(this.ddlUserRole.SelectedItem);
                phnType = Convert.ToInt32(this.ddlPhoneType.SelectedValue);
                Desig = Convert.ToString(this.txtDesig.Text);
                st1 = Convert.ToString(this.txtSt1.Text);
                st2 = Convert.ToString(this.txtSt2.Text);
                st3 = Convert.ToString(this.txtSt3.Text);
                reg = Convert.ToInt32(this.ddlReg.SelectedValue);
                sreg = Convert.ToInt32(this.ddlSReg.SelectedValue);
                Country = Convert.ToInt32(this.ddlCountry.SelectedValue);
                State = Convert.ToInt32(this.ddlState.SelectedValue);
                County = Convert.ToInt32(this.ddlCounty.SelectedValue);
                City = Convert.ToInt32(this.ddlCity.SelectedValue);
                zip = Convert.ToInt32(this.ddlZip.SelectedValue);
                gID = Convert.ToInt32(this.ddlUsrGender.SelectedValue);

                iResult = USR.createUser(clntName, FName, MName, LName, UsrPwd, UsrPh, UsrEml, usrRole, phnType, Desig, st1, st2, st3, reg, sreg, Country, State, County, City, zip, gID);

                switch (iResult)
                {
                    case 1:
                        {
                            this.usrEnclosure.Visible = true;
                            this.usrEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                            this.iUSRMessage.Attributes["class"] = "icon fa fa-check-circle";
                            this.spnUSR.InnerText = " Success!";
                            this.lblSuccess.Text = "User Account was successfully created.";
                            
                            break;
                        }
                    case -1:
                        {
                            this.usrEnclosure.Visible = true;
                            this.usrEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                            this.iUSRMessage.Attributes["class"] = "icon fa fa-warning";
                            this.spnUSR.InnerText = " Warning!";
                            this.lblSuccess.Text = "ERROR: " + "Email address provided is already being used. <br/>Please recreate the User Account with a valid Unique Email address.";                            
                            break;
                        }
                    case -2:
                        {
                            this.usrEnclosure.Visible = true;
                            this.usrEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                            this.iUSRMessage.Attributes["class"] = "icon fa fa-warning";
                            this.spnUSR.InnerText = " Warning!";
                            this.lblSuccess.Text = "ERROR: " + "Duplicate UserName found. Please provide User's First, Middle and Last Name to create a new user.<br/>If this repeats, please use a suffix to create a unqiue user account.";
                            break;
                        }
                    case -3:
                        {
                            this.usrEnclosure.Visible = true;
                            this.usrEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                            this.iUSRMessage.Attributes["class"] = "icon fa fa-warning";
                            this.spnUSR.InnerText = " Warning!";
                            this.lblSuccess.Text = "ERROR: " + "You have provided an Invalid Email address.";
                            break;
                        }
                    case -4:
                        {
                            this.usrEnclosure.Visible = true;
                            this.usrEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                            this.iUSRMessage.Attributes["class"] = "icon fa fa-warning";
                            this.spnUSR.InnerText = " Warning!";
                            this.lblSuccess.Text = "ERROR: " + "Invalid Username. <br/>Please check the User Accounts list for a unique username and try again.";
                            break;
                        }
                    case -5:
                        {
                            this.usrEnclosure.Visible = true;
                            this.usrEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                            this.iUSRMessage.Attributes["class"] = "icon fa fa-warning";
                            this.spnUSR.InnerText = " Warning!";
                            this.lblSuccess.Text = "ERROR: " + "Invalid Password provided.";
                            break;
                        }
                    case -6:
                        {
                            this.usrEnclosure.Visible = true;
                            this.usrEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                            this.iUSRMessage.Attributes["class"] = "icon fa fa-warning";
                            this.spnUSR.InnerText = " Warning!";
                            this.lblSuccess.Text = "ERROR: " + "Oops! Provider Error. Please try again.";
                            break;
                        }
                    case -7:
                        {
                            this.usrEnclosure.Visible = true;
                            this.usrEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                            this.iUSRMessage.Attributes["class"] = "icon fa fa-warning";
                            this.spnUSR.InnerText = " Warning!";
                            this.lblSuccess.Text = "ERROR: " + "User Account rejected. Please try again.";
                            break;
                        }
                    default:
                        {
                            this.usrEnclosure.Visible = true;
                            this.usrEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                            this.iUSRMessage.Attributes["class"] = "icon fa fa-warning";
                            this.spnUSR.InnerText = " Warning!";
                            this.lblSuccess.Text = "ERROR: " + "Unable to create User Account. Please try again.";
                            break;
                        }
                }
                btnCancel_Click(null, null); /*Calling Cancel button routine to reset Create user*/
                this.btnAddNUser.Focus();
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        protected void ddlPhoneType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<System.Guid, String> roleList = USR.getAllRoles();
                this.ddlUserRole.Items.Clear();
                this.ddlUserRole.DataSource = roleList;
                this.ddlUserRole.DataTextField = "Value";
                this.ddlUserRole.DataValueField = "Key";
                this.ddlUserRole.DataBind();
                this.ddlUserRole.Items.Insert(0, new ListItem("--- Select User Role ---", "-1"));
                this.ddlUserRole.SelectedIndex = -1;
                this.ddlUserRole.Enabled = true;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;
                this.btnAddUser.Enabled = false;
                this.btnAddUser.CssClass = "btn btn-default text-center";

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlClientList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.gvwUsersList.DataBind();
                this.gvwUsersList.Visible = true;

                this.gvwUsersList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlReg.SelectedValue);
                Dictionary<Int32, String> srList = DMG.getSubRegionList(srID);
                this.ddlSReg.Items.Clear();
                this.ddlSReg.DataSource = srList;
                this.ddlSReg.DataTextField = "Value";
                this.ddlSReg.DataValueField = "Key";
                this.ddlSReg.DataBind();
                this.ddlSReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlSReg.SelectedIndex = -1;
                this.ddlSReg.Enabled = true;
                this.ddlCountry.Items.Clear();
                this.ddlCountry.DataBind();
                this.ddlCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlCountry.SelectedIndex = -1;
                this.ddlCountry.Enabled = false;
                this.ddlState.Items.Clear();
                this.ddlState.DataBind();
                this.ddlState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlState.SelectedIndex = -1;
                this.ddlState.Enabled = false;
                this.ddlCounty.Items.Clear();
                this.ddlCounty.DataBind();
                this.ddlCounty.Items.Insert(0, new ListItem("--- Select County/District/Division/Municipality ---", "-1"));
                this.ddlCounty.SelectedIndex = -1;
                this.ddlCounty.Enabled = false;
                this.ddlCity.Items.Clear();
                this.ddlCity.DataBind();
                this.ddlCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCity.SelectedIndex = -1;
                this.ddlCity.Enabled = false;
                this.ddlZip.Items.Clear();
                this.ddlZip.DataBind();
                this.ddlZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlZip.SelectedIndex = -1;
                this.ddlZip.Enabled = false;
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = false;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = false;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;

                this.ddlSReg.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlSReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 srID = Convert.ToInt32(this.ddlSReg.SelectedValue);
                Dictionary<Int32, String> ctyList = DMG.getCountryList(srID);
                this.ddlCountry.Items.Clear();
                this.ddlCountry.DataSource = ctyList;
                this.ddlCountry.DataTextField = "Value";
                this.ddlCountry.DataValueField = "Key";
                this.ddlCountry.DataBind();
                this.ddlCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlCountry.SelectedIndex = -1;
                this.ddlCountry.Enabled = true;
                this.ddlState.Items.Clear();
                this.ddlState.DataBind();
                this.ddlState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlState.SelectedIndex = -1;
                this.ddlState.Enabled = false;
                this.ddlCounty.Items.Clear();
                this.ddlCounty.DataBind();
                this.ddlCounty.Items.Insert(0, new ListItem("--- Select County/District/Division/Municipality ---", "-1"));
                this.ddlCounty.SelectedIndex = -1;
                this.ddlCounty.Enabled = false;
                this.ddlCity.Items.Clear();
                this.ddlCity.DataBind();
                this.ddlCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCity.SelectedIndex = -1;
                this.ddlCity.Enabled = false;
                this.ddlZip.Items.Clear();
                this.ddlZip.DataBind();
                this.ddlZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlZip.SelectedIndex = -1;
                this.ddlZip.Enabled = false;
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = false;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = false;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;

                this.ddlCountry.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 ctyID = Convert.ToInt32(this.ddlCountry.SelectedValue);
                Dictionary<Int32, String> stList = DMG.getStateList(ctyID);
                this.ddlState.Items.Clear();
                this.ddlState.DataSource = stList;
                this.ddlState.DataTextField = "Value";
                this.ddlState.DataValueField = "Key";
                this.ddlState.DataBind();
                this.ddlState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlState.SelectedIndex = -1;
                this.ddlState.Enabled = true;
                this.ddlCounty.Items.Clear();
                this.ddlCounty.DataBind();
                this.ddlCounty.Items.Insert(0, new ListItem("--- Select County/District/Division/Municipality ---", "-1"));
                this.ddlCounty.SelectedIndex = -1;
                this.ddlCounty.Enabled = false;
                this.ddlCity.Items.Clear();
                this.ddlCity.DataBind();
                this.ddlCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCity.SelectedIndex = -1;
                this.ddlCity.Enabled = false;
                this.ddlZip.Items.Clear();
                this.ddlZip.DataBind();
                this.ddlZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlZip.SelectedIndex = -1;
                this.ddlZip.Enabled = false;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = false;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = false;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;

                this.ddlState.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 stID = Convert.ToInt32(this.ddlState.SelectedValue);
                Dictionary<Int32, String> cntyList = DMG.getCountyList(stID);
                this.ddlCounty.Items.Clear();
                this.ddlCounty.DataSource = cntyList;
                this.ddlCounty.DataTextField = "Value";
                this.ddlCounty.DataValueField = "Key";
                this.ddlCounty.DataBind();
                this.ddlCounty.Items.Insert(0, new ListItem("--- Select County/District/Division/Municipality ---", "-1"));
                this.ddlCounty.SelectedIndex = -1;
                this.ddlCounty.Enabled = true;
                this.ddlCity.Items.Clear();
                this.ddlCity.DataBind();
                this.ddlCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCity.SelectedIndex = -1;
                this.ddlCity.Enabled = false;
                this.ddlZip.Items.Clear();
                this.ddlZip.DataBind();
                this.ddlZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlZip.SelectedIndex = -1;
                this.ddlZip.Enabled = false;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = false;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = false;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;

                this.ddlCounty.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCounty_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cntID = Convert.ToInt32(this.ddlCounty.SelectedValue);
                Dictionary<Int32, String> cityList = DMG.getCityList(cntID);
                this.ddlCity.Items.Clear();
                this.ddlCity.DataSource = cityList;
                this.ddlCity.DataTextField = "Value";
                this.ddlCity.DataValueField = "Key";
                this.ddlCity.DataBind();
                this.ddlCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCity.SelectedIndex = -1;
                this.ddlCity.Enabled = true;
                this.ddlZip.Items.Clear();
                this.ddlZip.DataBind();
                this.ddlZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlZip.SelectedIndex = -1;
                this.ddlZip.Enabled = false;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = false;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = false;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;

                this.ddlCity.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 cntID = Convert.ToInt32(this.ddlCounty.SelectedValue);
                Dictionary<Int32, String> zipList = DMG.getZipList(cntID);
                this.ddlZip.Items.Clear();
                this.ddlZip.DataSource = zipList;
                this.ddlZip.DataTextField = "Value";
                this.ddlZip.DataValueField = "Key";
                this.ddlZip.DataBind();
                this.ddlZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlZip.SelectedIndex = -1;
                this.ddlZip.Enabled = true;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = false;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = false;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;

                this.ddlZip.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlZip_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtSt2.Text = "";
                this.txtSt1.Enabled = true;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = true;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = true;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = true;
                Dictionary<Int32, String> dmgList = DMG.getDemogTypeList();
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = true;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataSource = dmgList;
                this.ddlPhoneType.DataTextField = "Value";
                this.ddlPhoneType.DataValueField = "Key";
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = true;                
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;

                this.txtPhone.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNUser_Click(object sender, EventArgs e)
        {
            try
            {
                this.divUList.Attributes["class"] = "panel-collapse collapse";
                this.divUDetail.Attributes["class"] = "panel-collapse collapse";
                this.divAddUsr.Attributes["class"] = "panel-collapse collapse in";
                this.ancACL.InnerHtml = " User Accounts List";
                this.txtFirst.Text = "";
                this.txtMiddle.Text = "";
                this.txtLast.Text = "";
                this.txtDesig.Text = "";
                Dictionary<Int32, String> gndList = DMG.getGenderList();
                this.ddlUsrGender.Items.Clear();
                this.ddlUsrGender.DataSource = gndList;
                this.ddlUsrGender.DataTextField = "Value";
                this.ddlUsrGender.DataValueField = "Key";
                this.ddlUsrGender.DataBind();
                this.ddlUsrGender.Items.Insert(0, new ListItem("--- Select Gender ---", "-1"));
                this.ddlUsrGender.SelectedIndex = -1;
                this.ddlUsrGender.Enabled = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddUserDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnCancel_Click(null, null);
                this.gvwUsersList.Visible = true;
                this.gvwUsersList.SelectedIndex = -1;
                this.btnAddUserDone.Visible = false;
                //btnAccountsDone_Click(null, null);

                this.ddlUserRole.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlLockoutUserType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<System.Guid, String> usrList = USR.getClientUsersList(ClientID);
                this.ddlLockoutUserList.DataSource = usrList;
                this.ddlLockoutUserList.DataTextField = "Value";
                this.ddlLockoutUserList.DataValueField = "Key";
                this.ddlLockoutUserList.DataBind();
                this.ddlLockoutUserList.Items.Clear();
                this.ddlLockoutUserList.DataBind();
                this.ddlLockoutUserList.Items.Insert(0, new ListItem("--- Select SMARTs User Account ---", "-1"));
                this.ddlLockoutUserList.SelectedIndex = -1;
                this.ddlLockoutUserList.Enabled = true;
                this.ddlLockout.Items.Clear();
                this.ddlLockout.DataBind();
                this.ddlLockout.Items.Insert(0, new ListItem("--- UnLock User Account ---", "-1"));
                this.ddlLockout.Items.Insert(1, new ListItem("UnLock", "1"));
                this.ddlLockout.SelectedIndex = -1;
                this.ddlLockout.Enabled = false;
                this.stEnclosure.Visible = false;
                this.lblLockoutSuccess.Text = "";
                this.btnLockoutCancel.Enabled = true;
                this.btnLockoutCancel.CssClass = "btn btn-warning text-center";

                Session["userRole"] = Convert.ToString(this.ddlLockoutUserType.SelectedItem);

                this.ddlLockoutUserList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlLockoutUserList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.ddlLockout.Items.Clear();
                this.ddlLockout.DataBind();
                this.ddlLockout.Items.Insert(0, new ListItem("--- UnLock User Account ---", "-1"));
                this.ddlLockout.Items.Insert(1, new ListItem("UnLock", "1"));
                this.ddlLockout.SelectedIndex = -1;
                this.ddlLockout.Enabled = true;
                this.stEnclosure.Visible = false;
                this.lblLockoutSuccess.Text = "";
                this.btnLockoutCancel.Enabled = true;
                this.btnLockoutCancel.CssClass = "btn btn-warning text-center";

                this.ddlLockout.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlLockout_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 lckStat = -99;
                String uName = null;
                System.Guid userId = new System.Guid();
                userId = Guid.Parse(this.ddlLockoutUserList.SelectedValue);
                uName = USR.getUserNameFromId(userId);
                
                MembershipUser mUser = Membership.GetUser(uName);

                lckStat = Convert.ToInt32(this.ddlLockout.SelectedValue);
                if (lckStat.Equals(1))
                {
                    mUser.UnlockUser();
                    Membership.UpdateUser(mUser);
                    this.stEnclosure.Attributes["class"] = "alert alert-success alert-dismissable";
                    this.iSTMessage.Attributes["class"] = "icon fa fa-check-circle";
                    this.spnStatus.InnerText = " Success!";
                    this.lblLockoutSuccess.Text = "User Account has been successfully UnLocked.";
                    this.stEnclosure.Visible = true;
                    System.Data.DataTable usrinfoTable = USR.getClientUsersTable(ClientID);
                    this.gvwUsersList.DataSource = usrinfoTable;
                    this.gvwUsersList.DataBind();
                }
                else
                {
                    this.stEnclosure.Attributes["class"] = "alert alert-warning alert-dismissable";
                    this.iSTMessage.Attributes["class"] = "icon fa fa-warning";
                    this.spnStatus.InnerText = " Warning!";
                    this.lblLockoutSuccess.Text = "Unable to unlock user. Please contact support";
                    this.stEnclosure.Visible = true;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnLockoutCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.ddlLockoutUserType.Items.Clear();
                this.ddlLockoutUserType.DataBind();
                this.ddlLockoutUserType.Items.Insert(0, new ListItem("--- Select User Account Type ---", "-1"));
                this.ddlLockoutUserType.SelectedIndex = -1;
                this.ddlLockoutUserList.Items.Clear();
                this.ddlLockoutUserList.DataBind();
                this.ddlLockoutUserList.Items.Insert(0, new ListItem("--- Select SMARTs User Account ---", "-1"));
                this.ddlLockoutUserList.SelectedIndex = -1;
                this.ddlLockoutUserList.Enabled = false;
                this.ddlLockout.Items.Clear();
                this.ddlLockout.DataBind();
                this.ddlLockout.Items.Insert(0, new ListItem("--- UnLock User Account ---", "-1"));
                this.ddlLockout.Items.Insert(1, new ListItem("UnLock", "1"));
                this.ddlLockout.SelectedIndex = -1;
                this.ddlLockout.Enabled = false;
                this.stEnclosure.Visible = false;
                this.btnLockoutCancel.Enabled = false;
                this.btnLockoutCancel.CssClass = "btn btn-default text-center";

                Session["userRole"] = null;

                this.ddlLockoutUserType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwUsersList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable usrinfoTable = USR.getClientUsersTable(ClientID);
                this.gvwUsersList.PageIndex = e.NewPageIndex;
                this.gvwUsersList.DataSource = usrinfoTable;
                this.gvwUsersList.DataBind();
                this.gvwUsersList.Focus();
                this.btnListCancel.Enabled = true;
                this.btnListCancel.CssClass = "btn btn-warning text-center";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}