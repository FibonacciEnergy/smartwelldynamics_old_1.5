﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlHRAssignment.ascx.cs" Inherits="SmartsVer1.Control.Config.Users.ctrlHRAssignment" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<!--- Stylesheet --->
<link href="../../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript -->
<script src="../../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>

<!--- Page local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }

    .rotate {
        -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
        -moz-transform: rotate(90deg); /* Firefox */
        -ms-transform: rotate(90deg); /* IE 9 */
        transform: rotate(90deg); /* Standard syntax */
    }

    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper[data-stepper-steps="8"] li {
            width: 12%;
        }

        ol.stepper[data-stepper-steps="9"] li {
            width: 11%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }    
</style>
<!--- Page local script --->
<script type="text/javascript">
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("rotate");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").addClass("rotate");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("rotate");
        });
    });
</script>
<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!-- Map Literal -->
<asp:Literal ID="ltrMap" runat="server"></asp:Literal>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">        
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>Configuration</li>
                <li><a href="../../../Config/Users/fecDRHRAssign_2A.aspx"> User Assignment(s)</a></li>
            </ol>
        </div>
    </div>    
</div>

<!-- Page Content -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <asp:UpdateProgress ID="upgrsAssign" runat="server" AssociatedUpdatePanelID="upnlAssign" DisplayAfter="0">
                        <ProgressTemplate>
                            <div id="divAssignSurround" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                                z-index: 100;">
                                <asp:Image ID="upgrsAssignImage" runat="server" CssClass="center" AlternateText="Processing ...." ForeColor="Purple" ImageAlign="Middle"
                                    ImageUrl="~/Images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlAssign">
                        <ContentTemplate>
                            <div id="divGRTitle" class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <hr style="margin: auto;" />
                                    <div class="bg-light-blue">
                                        <div class="text-center">
                                            <h1 class="box-title">User Assigment</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div runat="server" id="divASGList">
                                <asp:GridView ID="gvwAsgList" runat="server" CssClass="mydatagrid" AutoGenerateColumns="False"
                                    EmptyDataText="No User Assignments found." ShowHeaderWhenEmpty="True" DataKeyNames="asgID"
                                    AlternatingRowStyle-CssClass="AlternatingRowStyle" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" OnPageIndexChanging="gvwAsgList_PageIndexChanging">
                                    <Columns>
                                        <asp:BoundField DataField="asgID" Visible="false" />
                                        <asp:BoundField DataField="lastName" HeaderText="Last Name" />
                                        <asp:BoundField DataField="firstName" HeaderText="First Name" />
                                        <asp:BoundField DataField="desig" HeaderText="Designation" />
                                        <asp:BoundField DataField="addID" HeaderText="Location Name" />
                                        <asp:BoundField DataField="sDate" HeaderText="Start Date" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                        <asp:BoundField DataField="eDate" HeaderText="End Date" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                        <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                    </Columns>
                                </asp:GridView>
                                <div class="input-group" style="width: 100%;">
                                    <asp:Button ID="btnAddAssign" runat="server" CssClass="btn btn-info text-center" Width="10%" Text="Add Assignment" OnClick="btnAddAssign_Click" />
                                </div>
                            </div>
                            <div runat="server" id="divASGAdd" visible="false">
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <div class="input-group">
                                        <span class="input-group-addon bg-light-blue">Role</span>
                                        <asp:DropDownList runat="server" ID="ddlAAUserRole" CssClass="form-control" AppendDataBoundItems="true" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlAAUserRole_SelectedIndexChanged">
                                            <asp:ListItem Value="0" Text="--- Select System Role ---" />
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <div class="input-group">
                                        <span class="input-group-addon bg-light-blue">Account</span>
                                        <asp:DropDownList runat="server" ID="ddlAAUser" CssClass="form-control" AppendDataBoundItems="True" AutoPostBack="True"
                                            Enabled="False" OnSelectedIndexChanged="ddlAAUser_SelectedIndexChanged">
                                            <asp:ListItem Value="0" Text="--- Select User ---" />
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <div class="input-group">
                                        <span class="input-group-addon bg-light-blue">Location</span>
                                        <asp:DropDownList runat="server" ID="ddlAALoc" CssClass="form-control" AppendDataBoundItems="true" AutoPostBack="true"
                                            Enabled="false" OnSelectedIndexChanged="ddlAALoc_SelectedIndexChanged">
                                            <asp:ListItem Value="0" Text="--- Select Location ---" />
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <div class="input-group">
                                        <span class="input-group-addon bg-light-blue">Start</span>
                                        <asp:TextBox runat="server" ID="txtAAStart" CssClass="form-control" Text="" />
                                        <ajaxToolkit:CalendarExtender ID="clndrAAStart" runat="server" TargetControlID="txtAAStart" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <div class="input-group">
                                        <span class="input-group-addon bg-light-blue">End</span>
                                        <asp:TextBox runat="server" ID="txtAAEnd" CssClass="form-control" Text="" />
                                        <ajaxToolkit:CalendarExtender ID="clndrAAEnd" runat="server" TargetControlID="txtAAEnd" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div id="asgEnclosure" runat="server" visible="false">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <h4><i runat="server" id="iASGMessage"><span runat="server" id="spnASG"></span></i></h4>
                                        <asp:Label ID="lblAsgSuccess" runat="server" Text="" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="input-group" style="width: 100%;">
                                        <asp:Button ID="btnAddNewAsg" runat="server" CssClass="btn btn-success text-center" Width="10%" Enabled="false" Text="Assign User to Location"
                                            OnClick="btnAddNewAsg_Click" />
                                        <asp:Button ID="btnAddNewAsgClear" runat="server" CssClass="btn btn-warning text-center" Width="10%" Text="Clear Selections"
                                            OnClick="btnAddNewAsgClear_Click" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="input-group" style="width: 100%;">
                                        <asp:Button ID="btnAddNewAsgDone" runat="server" CssClass="btn btn-primary text-center" Width="10%" Text="Done" Visible="false"
                                            OnClick="btnAddNewAsgDone_Click"
                                            CausesValidation="False" />
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- JavaScripts -->
<%--<script src="../../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>--%>
