﻿using System;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Net.Mail;
using System.Web.Profile;
using System.Web.Security;
using System.Globalization;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using USR = SmartsVer1.Helpers.AccountHelpers.CreateUser;

namespace SmartsVer1.Control.Config.Users
{
    public partial class ctrlCRDHR : System.Web.UI.UserControl
    {
        String LoginName, username, domain;
        Int32 ClientID = -99;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }


        protected void gvwUsersList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow drRes = ((DataRowView)e.Row.DataItem).Row;
                    Guid userID = new Guid();
                    userID = (Guid)(drRes["UserId"]);
                    String UName = null;
                    String firstName = null;
                    String lastName = null;
                    String roleName = null;
                    Int32 userApproved = -99;
                    Int32 userLockedOut = -99;

                    //Getting UserName from UserDB
                    String selUName = "SELECT [UserName] FROM aspnet_Users WHERE UserId = @UserId";
                    using (SqlConnection userCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ConnectionString))
                    {
                        try
                        {
                            userCon.Open();

                            SqlCommand getUName = new SqlCommand(selUName, userCon);
                            getUName.Parameters.AddWithValue("@UserId", SqlDbType.UniqueIdentifier).Value = userID.ToString();
                            using (SqlDataReader readUName = getUName.ExecuteReader())
                            {
                                try
                                {
                                    if (readUName.HasRows)
                                    {
                                        while (readUName.Read())
                                        {
                                            UName = readUName.GetString(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readUName.Close();
                                    readUName.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            userCon.Close();
                            userCon.Dispose();
                        }
                    }

                    //Getting User Profile info
                    UP profile = (UP)UP.Create(UName);
                    firstName = profile.FirstName;
                    lastName = profile.LastName;
                    MembershipUser selUser = Membership.GetUser(UName);
                    userApproved = Convert.ToInt32(selUser.IsApproved);
                    userLockedOut = Convert.ToInt32(selUser.IsLockedOut);
                    //Getting User Role Name
                    String[] roles = Roles.GetRolesForUser(UName);
                    roleName = Convert.ToString(roles.GetValue(0));

                    Label lblUserFName = e.Row.Cells[7].FindControl("lblUserFN") as Label;
                    lblUserFName.Text = firstName.ToString();
                    Label lblUserLName = e.Row.Cells[7].FindControl("lblUserLN") as Label;
                    lblUserLName.Text = lastName.ToString();
                    Label lblIDValue = e.Row.Cells[4].FindControl("lblUID") as Label;
                    lblIDValue.Text = UName.ToString();
                    Label lblURoleName = e.Row.Cells[5].FindControl("lblURole") as Label;
                    lblURoleName.Text = roleName.ToString();
                    CheckBox chkUApp = e.Row.Cells[11].FindControl("chkUserApproved") as CheckBox;
                    CheckBox chkULck = e.Row.Cells[12].FindControl("chkDisabled") as CheckBox;

                    if (userApproved.Equals(1))
                    {
                        chkUApp.Checked = true;
                    }
                    else
                    {
                        chkUApp.Checked = false;
                    }

                    if (userLockedOut.Equals(1))
                    {
                        chkULck.Checked = true;
                    }
                    else
                    {
                        chkULck.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwUsersList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnListCancel.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnListCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.gvwUsersList.SelectedIndex = -1;
                this.gvwUsersList.DataBind();
                this.gvwUsersList.Visible = true;
                this.sdsUsersList.DataBind();
                this.lblSuccess.Text = "";
                this.lblSuccess.CssClass = "lblSuccess";
                this.lblSuccess.Visible = false;
                this.btnListCancel.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlUserRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtFirst.Text = "";
                this.txtFirst.Enabled = true;
                this.txtMiddle.Text = "";
                this.txtMiddle.Enabled = true;
                this.txtLast.Text = "";
                this.txtLast.Enabled = true;
                this.txtDesig.Text = "";
                this.txtDesig.Enabled = true;
                this.txtSt1.Text = "";
                this.txtSt1.Enabled = true;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = true;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = true;
                this.ddlReg.Items.Clear();
                this.ddlReg.DataBind();
                this.ddlReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlReg.SelectedIndex = -1;
                this.ddlReg.Enabled = true;
                this.ddlSReg.Items.Clear();
                this.ddlSReg.DataBind();
                this.ddlSReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlSReg.SelectedIndex = -1;
                this.ddlSReg.Enabled = false;
                this.ddlCountry.Items.Clear();
                this.ddlCountry.DataBind();
                this.ddlCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlCountry.SelectedIndex = -1;
                this.ddlCountry.Enabled = false;
                this.ddlState.Items.Clear();
                this.ddlState.DataBind();
                this.ddlState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlState.SelectedIndex = -1;
                this.ddlState.Enabled = false;
                this.ddlCounty.Items.Clear();
                this.ddlCounty.DataBind();
                this.ddlCounty.Items.Insert(0, new ListItem("--- Select County/District/Division/Municipality ---", "-1"));
                this.ddlCounty.SelectedIndex = -1;
                this.ddlCounty.Enabled = false;
                this.ddlCity.Items.Clear();
                this.ddlCity.DataBind();
                this.ddlCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCity.SelectedIndex = -1;
                this.ddlCity.Enabled = false;
                this.ddlZip.Items.Clear();
                this.ddlZip.DataBind();
                this.ddlZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlZip.SelectedIndex = -1;
                this.ddlZip.Enabled = false;
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = false;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = false;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;

                this.txtFirst.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.ddlUserRole.Items.Clear();
                this.ddlUserRole.DataBind();
                this.ddlUserRole.Items.Insert(0, new ListItem("--- Select User Role ---", "-1"));
                this.ddlUserRole.SelectedIndex = -1;
                this.ddlUserRole.Enabled = false;
                this.txtFirst.Text = "";
                this.txtFirst.Enabled = false;
                this.txtMiddle.Text = "";
                this.txtMiddle.Enabled = false;
                this.txtLast.Text = "";
                this.txtLast.Enabled = false;
                this.txtDesig.Text = "";
                this.txtDesig.Enabled = false;
                this.txtSt1.Text = "";
                this.txtSt1.Enabled = false;
                this.txtSt2.Text = "";
                this.txtSt2.Enabled = false;
                this.txtSt3.Text = "";
                this.txtSt3.Enabled = false;
                this.ddlReg.Items.Clear();
                this.ddlReg.DataBind();
                this.ddlReg.Items.Insert(0, new ListItem("--- Select Major Geographic Region ---", "-1"));
                this.ddlReg.SelectedIndex = -1;
                this.ddlReg.Enabled = false;
                this.ddlSReg.Items.Clear();
                this.ddlSReg.DataBind();
                this.ddlSReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlSReg.SelectedIndex = -1;
                this.ddlSReg.Enabled = false;
                this.ddlCountry.Items.Clear();
                this.ddlCountry.DataBind();
                this.ddlCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlCountry.SelectedIndex = -1;
                this.ddlCountry.Enabled = false;
                this.ddlState.Items.Clear();
                this.ddlState.DataBind();
                this.ddlState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlState.SelectedIndex = -1;
                this.ddlState.Enabled = false;
                this.ddlCounty.Items.Clear();
                this.ddlCounty.DataBind();
                this.ddlCounty.Items.Insert(0, new ListItem("--- Select County/District/Division/Municipality ---", "-1"));
                this.ddlCounty.SelectedIndex = -1;
                this.ddlCounty.Enabled = false;
                this.ddlCity.Items.Clear();
                this.ddlCity.DataBind();
                this.ddlCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCity.SelectedIndex = -1;
                this.ddlCity.Enabled = false;
                this.ddlZip.Items.Clear();
                this.ddlZip.DataBind();
                this.ddlZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlZip.SelectedIndex = -1;
                this.ddlZip.Enabled = false;
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = false;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = false;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;
                this.lblSuccess.Text = "";
                this.lblSuccess.CssClass = "lblSuccess";
                this.lblSuccess.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iResult = -99;
                Int32 genderID = -99;
                Int32 clntName = -99;
                String usrRole = null;
                Int32 phnType = -99;
                String FName = null;
                String MName = null;
                String LName = null;
                String UsrPh = null;
                String UsrEml = null;
                String UsrPwd = null;
                String Desig = null;
                String st1 = null;
                String st2 = null;
                String st3 = null;
                Int32 reg = -99;
                Int32 sreg = -99;
                Int32 Country = -99;
                Int32 State = -99;
                Int32 County = -99;
                Int32 City = -99;
                Int32 zip = -99;

                TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                //clntType = 1;
                clntName = Convert.ToInt32(Session["clntID"]);
                FName = ti.ToTitleCase(this.txtFirst.Text);
                MName = ti.ToTitleCase(this.txtMiddle.Text);
                LName = ti.ToTitleCase(this.txtLast.Text);
                UsrPwd = this.txtPassword.Text;
                UsrPh = this.txtPhone.Text;
                UsrEml = this.txtEMail.Text;
                usrRole = Convert.ToString(this.ddlUserRole.SelectedItem);
                phnType = Convert.ToInt32(this.ddlPhoneType.SelectedValue);
                Desig = Convert.ToString(this.txtDesig.Text);
                st1 = Convert.ToString(this.txtSt1.Text);
                st2 = Convert.ToString(this.txtSt2.Text);
                st3 = Convert.ToString(this.txtSt3.Text);
                reg = Convert.ToInt32(this.ddlReg.SelectedValue);
                sreg = Convert.ToInt32(this.ddlSReg.SelectedValue);
                Country = Convert.ToInt32(this.ddlCountry.SelectedValue);
                State = Convert.ToInt32(this.ddlState.SelectedValue);
                County = Convert.ToInt32(this.ddlCounty.SelectedValue);
                City = Convert.ToInt32(this.ddlCity.SelectedValue);
                zip = Convert.ToInt32(this.ddlZip.SelectedValue);

                iResult = USR.createUser(clntName, FName, MName, LName, UsrPwd, UsrPh, UsrEml, usrRole, phnType, Desig, st1, st2, st3, reg, sreg, Country, State, County, City, zip, genderID);

                btnCancel_Click(null, null); /*Calling Cancel button routine to reset Create user*/
                switch (iResult)
                {
                    case 1:
                        {
                            this.lblSuccess.ForeColor = System.Drawing.Color.Green;
                            this.lblSuccess.Font.Size = 14;
                            this.lblSuccess.Font.Bold = true;
                            this.lblSuccess.Text = "User Account was successfully created.";
                            this.lblSuccess.Visible = true;
                            break;
                        }
                    case -1:
                        {
                            this.lblSuccess.Text = "ERROR: " + "Email address provided is already being used. <br/>Please recreate the User Account with a valid Unique Email address.";
                            this.lblSuccess.Visible = true;
                            break;
                        }
                    case -2:
                        {
                            this.lblSuccess.ForeColor = System.Drawing.Color.Red;
                            this.lblSuccess.Font.Size = 14;
                            this.lblSuccess.Font.Bold = true;
                            this.lblSuccess.Text = "ERROR: " + "Duplicate UserName found. Please provide User's First, Middle and Last Name to create a new user.<br/>If this repeats, please use a suffix to create a unqiue user account.";
                            this.lblSuccess.Visible = true;
                            break;
                        }
                    case -3:
                        {
                            this.lblSuccess.ForeColor = System.Drawing.Color.Red;
                            this.lblSuccess.Font.Size = 14;
                            this.lblSuccess.Font.Bold = true;
                            this.lblSuccess.Text = "ERROR: " + "You have provided an Invalid Email address.";
                            this.lblSuccess.Visible = true;
                            break;
                        }
                    case -4:
                        {
                            this.lblSuccess.Text = "ERROR: " + "Invalid Username. <br/>Please check the User Accounts list for a unique username and try again.";
                            this.lblSuccess.Visible = true;
                            break;
                        }
                    case -5:
                        {
                            this.lblSuccess.Text = "ERROR: " + "Invalid Password provided.";
                            this.lblSuccess.Visible = true;
                            break;
                        }
                    case -6:
                        {
                            this.lblSuccess.Text = "ERROR: " + "Oops! Provider Error. Please try again.";
                            this.lblSuccess.Visible = true;
                            break;
                        }
                    case -7:
                        {
                            this.lblSuccess.ForeColor = System.Drawing.Color.Red;
                            this.lblSuccess.Font.Size = 14;
                            this.lblSuccess.Font.Bold = true;
                            this.lblSuccess.Text = "ERROR: " + "User Account rejected. Please try again.";
                            this.lblSuccess.Visible = true;
                            break;
                        }
                    default:
                        {
                            this.lblSuccess.ForeColor = System.Drawing.Color.Red;
                            this.lblSuccess.Font.Size = 14;
                            this.lblSuccess.Font.Bold = true;
                            this.lblSuccess.Text = "ERROR: " + "Unable to create User Account. Please try again.";
                            this.lblSuccess.Visible = true;
                            break;
                        }
                }
                this.btnAddNUser.Focus();
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        protected void ddlPhoneType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = true;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = true;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = true;
                this.btnAddUser.Enabled = true;

                this.txtEMail.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlClientList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.gvwUsersList.DataBind();
                this.gvwUsersList.Visible = true;

                this.gvwUsersList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.ddlSReg.Items.Clear();
                this.ddlSReg.DataBind();
                this.ddlSReg.Items.Insert(0, new ListItem("--- Select Geographic Sub-Region ---", "-1"));
                this.ddlSReg.SelectedIndex = -1;
                this.ddlSReg.Enabled = true;
                this.ddlCountry.Items.Clear();
                this.ddlCountry.DataBind();
                this.ddlCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlCountry.SelectedIndex = -1;
                this.ddlCountry.Enabled = false;
                this.ddlState.Items.Clear();
                this.ddlState.DataBind();
                this.ddlState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlState.SelectedIndex = -1;
                this.ddlState.Enabled = false;
                this.ddlCounty.Items.Clear();
                this.ddlCounty.DataBind();
                this.ddlCounty.Items.Insert(0, new ListItem("--- Select County/District/Division/Municipality ---", "-1"));
                this.ddlCounty.SelectedIndex = -1;
                this.ddlCounty.Enabled = false;
                this.ddlCity.Items.Clear();
                this.ddlCity.DataBind();
                this.ddlCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCity.SelectedIndex = -1;
                this.ddlCity.Enabled = false;
                this.ddlZip.Items.Clear();
                this.ddlZip.DataBind();
                this.ddlZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlZip.SelectedIndex = -1;
                this.ddlZip.Enabled = false;
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = false;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = false;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;

                this.ddlSReg.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlSReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.ddlCountry.Items.Clear();
                this.ddlCountry.DataBind();
                this.ddlCountry.Items.Insert(0, new ListItem("--- Select Country/Nation ---", "-1"));
                this.ddlCountry.SelectedIndex = -1;
                this.ddlCountry.Enabled = true;
                this.ddlState.Items.Clear();
                this.ddlState.DataBind();
                this.ddlState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlState.SelectedIndex = -1;
                this.ddlState.Enabled = false;
                this.ddlCounty.Items.Clear();
                this.ddlCounty.DataBind();
                this.ddlCounty.Items.Insert(0, new ListItem("--- Select County/District/Division/Municipality ---", "-1"));
                this.ddlCounty.SelectedIndex = -1;
                this.ddlCounty.Enabled = false;
                this.ddlCity.Items.Clear();
                this.ddlCity.DataBind();
                this.ddlCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCity.SelectedIndex = -1;
                this.ddlCity.Enabled = false;
                this.ddlZip.Items.Clear();
                this.ddlZip.DataBind();
                this.ddlZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlZip.SelectedIndex = -1;
                this.ddlZip.Enabled = false;
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = false;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = false;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;

                this.ddlCountry.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.ddlState.Items.Clear();
                this.ddlState.DataBind();
                this.ddlState.Items.Insert(0, new ListItem("--- Select State/Province ---", "-1"));
                this.ddlState.SelectedIndex = -1;
                this.ddlState.Enabled = true;
                this.ddlCounty.Items.Clear();
                this.ddlCounty.DataBind();
                this.ddlCounty.Items.Insert(0, new ListItem("--- Select County/District/Division/Municipality ---", "-1"));
                this.ddlCounty.SelectedIndex = -1;
                this.ddlCounty.Enabled = false;
                this.ddlCity.Items.Clear();
                this.ddlCity.DataBind();
                this.ddlCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCity.SelectedIndex = -1;
                this.ddlCity.Enabled = false;
                this.ddlZip.Items.Clear();
                this.ddlZip.DataBind();
                this.ddlZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlZip.SelectedIndex = -1;
                this.ddlZip.Enabled = false;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = false;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = false;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;

                this.ddlState.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.ddlCounty.Items.Clear();
                this.ddlCounty.DataBind();
                this.ddlCounty.Items.Insert(0, new ListItem("--- Select County/District/Division/Municipality ---", "-1"));
                this.ddlCounty.SelectedIndex = -1;
                this.ddlCounty.Enabled = true;
                this.ddlCity.Items.Clear();
                this.ddlCity.DataBind();
                this.ddlCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCity.SelectedIndex = -1;
                this.ddlCity.Enabled = false;
                this.ddlZip.Items.Clear();
                this.ddlZip.DataBind();
                this.ddlZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlZip.SelectedIndex = -1;
                this.ddlZip.Enabled = false;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = false;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = false;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;

                this.ddlCounty.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCounty_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.ddlCity.Items.Clear();
                this.ddlCity.DataBind();
                this.ddlCity.Items.Insert(0, new ListItem("--- Select City/Town ---", "-1"));
                this.ddlCity.SelectedIndex = -1;
                this.ddlCity.Enabled = true;
                this.ddlZip.Items.Clear();
                this.ddlZip.DataBind();
                this.ddlZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlZip.SelectedIndex = -1;
                this.ddlZip.Enabled = false;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = false;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = false;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;

                this.ddlCity.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.ddlZip.Items.Clear();
                this.ddlZip.DataBind();
                this.ddlZip.Items.Insert(0, new ListItem("--- Select Zip/Postal Code ---", "-1"));
                this.ddlZip.SelectedIndex = -1;
                this.ddlZip.Enabled = true;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = false;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = false;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;

                this.ddlZip.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlZip_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtPhone.Text = "";
                this.txtPhone.Enabled = true;
                this.ddlPhoneType.Items.Clear();
                this.ddlPhoneType.DataBind();
                this.ddlPhoneType.Items.Insert(0, new ListItem("--- Select Phone Type ---", "-1"));
                this.ddlPhoneType.SelectedIndex = -1;
                this.ddlPhoneType.Enabled = true;
                this.txtEMail.Text = "";
                this.txtEMail.Enabled = false;
                this.txtPassword.Text = "";
                this.txtPassword.Enabled = false;
                this.txtCPassword.Text = "";
                this.txtCPassword.Enabled = false;

                this.txtPhone.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddNUser_Click(object sender, EventArgs e)
        {
            try
            {
                this.divUserAccountList.Visible = false;
                this.divAddUserDialog.Visible = true;
                this.ddlUserRole.Items.Clear();
                this.ddlUserRole.DataBind();
                this.ddlUserRole.Items.Insert(0, new ListItem("--- Select User Role ---", "-1"));
                this.ddlUserRole.SelectedIndex = -1;

                this.lblSuccess.Text = "";
                this.lblSuccess.CssClass = "genLabel";
                this.lblSuccess.Visible = false;
                this.btnAddUserDone.Visible = true;

                this.ddlUserRole.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddUserDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnCancel_Click(null, null);
                this.divUserAccountList.Visible = true;
                this.divAddUserDialog.Visible = false;
                this.btnAddUserDone.Visible = false;
                btnListCancel_Click(null, null);

                this.ddlUserRole.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlLockoutUserType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.ddlLockoutUserList.Items.Clear();
                this.ddlLockoutUserList.DataBind();
                this.ddlLockoutUserList.Items.Insert(0, new ListItem("--- Select SMARTs User Account ---", "-1"));
                this.ddlLockoutUserList.SelectedIndex = -1;
                this.ddlLockoutUserList.Enabled = true;
                this.ddlLockout.Items.Clear();
                this.ddlLockout.DataBind();
                this.ddlLockout.Items.Insert(0, new ListItem("--- UnLock User Account ---", "-1"));
                this.ddlLockout.Items.Insert(1, new ListItem("UnLock", "1"));
                this.ddlLockout.SelectedIndex = -1;
                this.ddlLockout.Enabled = false;
                this.lblLockoutSuccess.Text = "";
                this.lblLockoutSuccess.CssClass = "genLabel";
                this.lblLockoutSuccess.Visible = false;
                this.btnLockoutCancel.Enabled = true;

                Session["userRole"] = Convert.ToString(this.ddlLockoutUserType.SelectedItem);

                this.ddlLockoutUserList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlLockoutUserList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.ddlLockout.Items.Clear();
                this.ddlLockout.DataBind();
                this.ddlLockout.Items.Insert(0, new ListItem("--- UnLock User Account ---", "-1"));
                this.ddlLockout.Items.Insert(1, new ListItem("UnLock", "1"));
                this.ddlLockout.SelectedIndex = -1;
                this.ddlLockout.Enabled = true;
                this.lblLockoutSuccess.Text = "";
                this.lblLockoutSuccess.CssClass = "genLabel";
                this.lblLockoutSuccess.Visible = false;
                this.btnLockoutCancel.Enabled = true;

                this.ddlLockout.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void ddlLockout_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 lckStat = -99;
                String uName = null;
                uName = Convert.ToString(this.ddlLockoutUserList.SelectedItem);
                MembershipUser mUser = Membership.GetUser(uName);

                lckStat = Convert.ToInt32(this.ddlLockout.SelectedValue);
                if (lckStat.Equals(1))
                {
                    mUser.UnlockUser();
                    Membership.UpdateUser(mUser);
                    this.lblLockoutSuccess.ForeColor = System.Drawing.Color.Green;
                    this.lblLockoutSuccess.Font.Size = 14;
                    this.lblLockoutSuccess.Text = "User Account has been successfully UnLocked.";
                    this.lblLockoutSuccess.Visible = true;
                    this.gvwUsersList.DataBind();

                    this.btnLockoutCancel.Focus();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnLockoutCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.ddlLockoutUserType.Items.Clear();
                this.ddlLockoutUserType.DataBind();
                this.ddlLockoutUserType.Items.Insert(0, new ListItem("--- Select User Account Type ---", "-1"));
                this.ddlLockoutUserType.SelectedIndex = -1;
                this.ddlLockoutUserList.Items.Clear();
                this.ddlLockoutUserList.DataBind();
                this.ddlLockoutUserList.Items.Insert(0, new ListItem("--- Select SMARTs User Account ---", "-1"));
                this.ddlLockoutUserList.SelectedIndex = -1;
                this.ddlLockoutUserList.Enabled = false;
                this.ddlLockout.Items.Clear();
                this.ddlLockout.DataBind();
                this.ddlLockout.Items.Insert(0, new ListItem("--- UnLock User Account ---", "-1"));
                this.ddlLockout.Items.Insert(1, new ListItem("UnLock", "1"));
                this.ddlLockout.SelectedIndex = -1;
                this.ddlLockout.Enabled = false;
                this.lblLockoutSuccess.Text = "";
                this.lblLockoutSuccess.CssClass = "genLabel";
                this.lblLockoutSuccess.Visible = false;
                this.btnLockoutCancel.Enabled = false;

                Session["userRole"] = null;

                this.ddlLockoutUserType.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}