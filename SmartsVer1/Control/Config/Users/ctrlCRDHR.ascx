﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlCRDHR.ascx.cs" Inherits="SmartsVer1.Control.Config.Users.ctrlCRDHR" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

<link href="../../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!--- local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }
    .rotate{
		-webkit-transform: rotate(90deg);  /* Chrome, Safari, Opera */
			-moz-transform: rotate(90deg);  /* Firefox */
			-ms-transform: rotate(90deg);  /* IE 9 */
				transform: rotate(90deg);  /* Standard syntax */    
    }
    .swdMenu {
        color: #ffffff;
    }
    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper[data-stepper-steps="8"] li {
            width: 12%;
        }

        ol.stepper[data-stepper-steps="9"] li {
            width: 11%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }
</style>

<!-- Javascript for accordion -->
<script type="text/javascript">
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("rotate");
        });
        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").addClass("rotate");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("rotate");
        });
    });
</script>

<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!-- Page Breadcrumb -->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../../Viewer/crdViewer.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>Configuration</li>
                <li><a href="../../../Config/Users/fecCRDHR_2A.aspx"> Users</a></li>
            </ol>
        </div>
    </div>
</div>

<div class="content-wrapper viewer" style="clear: right; width: 100%;">
    <div id="divTop" runat="server" class="viewer" style="width: 98.5%;">
        <asp:UpdateProgress ID="uprgsClnts" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlClients">
            <ProgressTemplate>
                <div id="divClntsProgrs" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center; z-index: 100;">
                    <asp:Image ID="imgClntsProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="upnlClients" runat="server">
            <ContentTemplate>
                <div id="divUserAccountList" runat="server" class="viewer divShadow" style="width: 99.1%;">
                    <asp:Label ID="lblUserAccountList" runat="server" CssClass="genLabel" Width="99.6%" Text="User Accounts List" />
                    <asp:GridView ID="gvwUsersList" runat="server" AutoGenerateColumns="False" DataSourceID="sdsUsersList"
                        AllowPaging="True" AllowSorting="True" CssClass="gvwList" EmptyDataText="No User Accounts Found."
                        ShowHeaderWhenEmpty="True" Width="100%" OnRowDataBound="gvwUsersList_RowDataBound" DataKeyNames="UserId" OnSelectedIndexChanged="gvwUsersList_SelectedIndexChanged">
                        <Columns>
                            <asp:CommandField ShowSelectButton="True"></asp:CommandField>
                            <asp:BoundField DataField="UserId" HeaderText="UserId" SortExpression="UserId" Visible="False" />
                            <asp:BoundField DataField="clntID" HeaderText="clntID" SortExpression="clntID" Visible="False" />
                            <asp:BoundField DataField="Expr1" HeaderText="Expr1" ReadOnly="True" InsertVisible="False" SortExpression="Expr1" Visible="False">
                            </asp:BoundField>
                            <asp:BoundField DataField="Expr2" HeaderText="Expr2" SortExpression="Expr2" InsertVisible="False" ReadOnly="True" Visible="False">
                            </asp:BoundField>
                            <asp:BoundField DataField="Expr3" HeaderText="Expr3" SortExpression="Expr3" Visible="False"></asp:BoundField>
                            <asp:TemplateField HeaderText="First Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblUserFN" runat="server" Text="" CssClass="genLabel" Width="99.6%" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Last Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblUserLN" runat="server" Text="" CssClass="genLabel" Width="99.6%" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="UserName" HeaderText="UserName" SortExpression="UserName" Visible="False"></asp:BoundField>
                            <asp:TemplateField HeaderText="User Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblUID" runat="server" CssClass="genLabel" Text="" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="System Role">
                                <ItemTemplate>
                                    <asp:Label ID="lblURole" runat="server" CssClass="genLabel" Text="" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Enabled">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkUserApproved" runat="server" Enabled="False" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Locked Out">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkDisabled" runat="server" Enabled="False" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="LastActivityDate" HeaderText="Last Activity Date" SortExpression="LastActivityDate" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}"
                                ReadOnly="True"></asp:BoundField>
                        </Columns>
                        <FooterStyle BackColor="Gray" ForeColor="White" />
                        <HeaderStyle BackColor="#99CCFF" BorderColor="Silver" BorderStyle="Solid" />
                        <PagerSettings Mode="NumericFirstLast" />
                        <RowStyle BorderColor="#CCCCCC" BorderStyle="Solid" />
                        <SelectedRowStyle BackColor="#CCFFCC" />
                    </asp:GridView>
                    <asp:SqlDataSource runat="server" ID="sdsUsersList" ConnectionString='<%$ ConnectionStrings:FEUsersDB %>'
                        SelectCommand="SELECT UserInClient.UserId, UserInClient.clntID, Client.clntID AS Expr1, ClientType.ctypID AS Expr2, aspnet_Users.UserId AS Expr3, aspnet_Users.UserName, aspnet_Users.LastActivityDate FROM UserInClient INNER JOIN Client ON UserInClient.clntID = Client.clntID INNER JOIN ClientType ON Client.ctypID = ClientType.ctypID INNER JOIN aspnet_Users ON UserInClient.UserId = aspnet_Users.UserId WHERE (Client.clntID = @clntID)">
                        <SelectParameters>
                            <asp:SessionParameter Name="clntID" SessionField="clntID" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:GridView runat="server" ID="gvwUserDetail" AutoGenerateColumns="False" DataKeyNames="UserId"
                        DataSourceID="sdsUserDetail" CssClass="gvwList" Width="99.6%">
                        <Columns>
                            <asp:BoundField DataField="UserId" HeaderText="UserId" ReadOnly="True" SortExpression="UserId" Visible="false"></asp:BoundField>
                            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email"></asp:BoundField>
                            <asp:BoundField DataField="CreateDate" HeaderText="User Creation Date" SortExpression="CreateDate"
                                DataFormatString="{0:MMM d , yyyy --- hh:mm tt}"></asp:BoundField>
                            <asp:BoundField DataField="LastLoginDate" HeaderText="Last Login Date" SortExpression="LastLoginDate" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}">
                            </asp:BoundField>
                            <asp:BoundField DataField="LastActivityDate" HeaderText="Last Activity Date" SortExpression="LastActivityDate" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}">
                            </asp:BoundField>
                        </Columns>
                        <FooterStyle BackColor="Gray" ForeColor="White" />
                        <HeaderStyle BackColor="#99CCFF" BorderColor="Silver" BorderStyle="Solid" />
                        <PagerSettings Mode="NumericFirstLast" />
                        <RowStyle BorderColor="#CCCCCC" BorderStyle="Solid" />
                        <SelectedRowStyle BackColor="#CCFFCC" />
                    </asp:GridView>
                    <asp:SqlDataSource runat="server" ID="sdsUserDetail" ConnectionString='<%$ ConnectionStrings:FEUsersDB %>' SelectCommand="SELECT [UserId], [Email], [CreateDate], [LastLoginDate], [LastActivityDate] FROM [vw_aspnet_MembershipUsers] WHERE ([UserId] = @UserId)">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="gvwUsersList" PropertyName="SelectedValue" Name="UserId" Type="Object"></asp:ControlParameter>
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:Button runat="server" ID="btnAddNUser" CssClass="red btn" Width="25%" Text="Add New User" OnClick="btnAddNUser_Click" />
                    <asp:Button ID="btnListCancel" runat="server" Width="15%" Text="Cancel" CssClass="red btn" Visible="false" OnClick="btnListCancel_Click" />
                </div>
                <div id="divAddUserDialog" runat="server" class="viewer divShadow" visible="false" style="width: 99.1%;">
                    <asp:Label ID="lblAddNewUser" runat="server" Text="Add New SMART User" CssClass="genLabel" Width="99.6%"
                        Font-Underline="True" Font-Size="Larger" />

                    <asp:Label ID="lblUserRole" runat="server" CssClass="genLabel" Width="25%" Text="Select User Role" />
                    <asp:DropDownList ID="ddlUserRole" runat="server" CssClass="ddlList" AppendDataBoundItems="True" DataSourceID="sdsAddUserRole"
                        DataTextField="RoleName" DataValueField="RoleId" AutoPostBack="True" Enabled="True" OnSelectedIndexChanged="ddlUserRole_SelectedIndexChanged"
                        Width="70%">
                        <asp:ListItem Value="0">--- Select User Role ---</asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource runat="server" ID="sdsAddUserRole" ConnectionString='<%$ ConnectionStrings:FEUsersDB %>'
                        SelectCommand="SELECT ApplicationId, RoleId, RoleName FROM vw_aspnet_Roles WHERE (NOT (RoleId IN (SELECT RoleId FROM aspnet_Roles WHERE (RoleName = 'Director') OR (RoleName = 'Coordinator')))) ORDER BY RoleName">
                    </asp:SqlDataSource>
                    <asp:Label ID="lblFirst" runat="server" CssClass="genLabel" Width="25%" Text="First Name" />
                    <asp:TextBox ID="txtFirst" runat="server" CssClass="genTextBox" Enabled="false" Width="70%" />
                    <asp:RequiredFieldValidator ID="rfvFirst" runat="server" ControlToValidate="txtFirst" ErrorMessage="*" ForeColor="Maroon" />
                    <asp:Label ID="lblMiddle" runat="server" CssClass="genLabel" Width="25%" Text="Middle Name" />
                    <asp:TextBox ID="txtMiddle" runat="server" CssClass="genTextBox" Enabled="false" Width="70%" />
                    <asp:Label ID="lblLast" runat="server" CssClass="genLabel" Width="25%" Text="Last Name" />
                    <asp:TextBox ID="txtLast" runat="server" CssClass="genTextBox" Enabled="false" Width="70%" />
                    <asp:RequiredFieldValidator ID="rfvLast" runat="server" ControlToValidate="txtLast" ErrorMessage="*" ForeColor="Maroon" />
                    <asp:Label ID="lblDesignation" runat="server" CssClass="genLabel" Width="25%" Text="Designation" />
                    <asp:TextBox ID="txtDesig" runat="server" CssClass="genTextBox" Enabled="false" Width="70%" />
                    <asp:RequiredFieldValidator ID="rfvDesig" runat="server" ControlToValidate="txtDesig" ErrorMessage="*" ForeColor="Maroon" />
                    <asp:Label ID="lblSt1" runat="server" CssClass="genLabel" Width="25%" Text="Street 1" />
                    <asp:TextBox ID="txtSt1" runat="server" CssClass="genTextBox" Enabled="false" Width="70%" />
                    <asp:RequiredFieldValidator ID="rfvSt1" runat="server" ControlToValidate="txtSt1" ErrorMessage="*" ForeColor="Maroon" />
                    <asp:Label ID="lblSt2" runat="server" CssClass="genLabel" Width="25%" Text="Street 2" />
                    <asp:TextBox ID="txtSt2" runat="server" CssClass="genTextBox" Enabled="false" Width="70%" />
                    <asp:Label ID="lblSt3" runat="server" CssClass="genLabel" Width="25%" Text="Street 3" />
                    <asp:TextBox ID="txtSt3" runat="server" CssClass="genTextBox" Enabled="false" Width="70%" />
                    <asp:Label ID="lblReg" runat="server" CssClass="genLabel" Width="25%" Text="Major Geographic Region" />
                    <asp:DropDownList ID="ddlReg" runat="server" CssClass="ddlList" AppendDataBoundItems="True"
                        DataSourceID="sdsAddUserRegion" DataTextField="grName" DataValueField="grID" AutoPostBack="True" Enabled="False" OnSelectedIndexChanged="ddlReg_SelectedIndexChanged"
                        Width="70%">
                        <asp:ListItem Value="0">--- Select Major Geographic Region ---</asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource runat="server" ID="sdsAddUserRegion" ConnectionString='<%$ ConnectionStrings:FEDemogDB %>' SelectCommand="SELECT [grID], [grName] FROM [dmgGeoRegion] ORDER BY [grName]">
                    </asp:SqlDataSource>
                    <asp:Label ID="lblSReg" runat="server" CssClass="genLabel" Width="25%" Text="Geographic Sub-Region" />
                    <asp:DropDownList ID="ddlSReg" runat="server" DataSourceID="sdsAddUserSubRegion" DataTextField="gsrName" DataValueField="gsrID"
                        AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList" Enabled="False" OnSelectedIndexChanged="ddlSReg_SelectedIndexChanged"
                        Width="70%">
                        <asp:ListItem Value="0">--- Select Geographic Sub-Region ---</asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource runat="server" ID="sdsAddUserSubRegion" ConnectionString='<%$ ConnectionStrings:FEDemogDB %>' SelectCommand="SELECT [gsrID], [gsrName], [grID] FROM [dmgGeoSubRegion] WHERE ([grID] = @grID) ORDER BY [gsrName]">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ddlReg" PropertyName="SelectedValue" Name="grID" Type="Int32"></asp:ControlParameter>
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:Label ID="lblCountry" runat="server" CssClass="genLabel" Width="25%" Text="Country/Nation" />
                    <asp:DropDownList ID="ddlCountry" runat="server" CssClass="ddlList" AppendDataBoundItems="True" DataSourceID="sdsAddUserCountry"
                        DataTextField="ctyName" DataValueField="ctyID" AutoPostBack="True" Enabled="False" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                        Width="70%">
                        <asp:ListItem Value="0">--- Select Country/Nation ---</asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource runat="server" ID="sdsAddUserCountry" ConnectionString='<%$ ConnectionStrings:FEDemogDB %>' SelectCommand="SELECT [ctyID], [ctyName], [gsrID] FROM [dmgCountry] WHERE ([gsrID] = @gsrID) ORDER BY [ctyName]">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ddlSReg" PropertyName="SelectedValue" Name="gsrID" Type="Int32"></asp:ControlParameter>
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:Label ID="lblState" runat="server" CssClass="genLabel" Width="25%" Text="State/Province" />
                    <asp:DropDownList ID="ddlState" runat="server" DataSourceID="sdsAddUserState" DataTextField="stName" DataValueField="stID"
                        AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList" Enabled="False" OnSelectedIndexChanged="ddlState_SelectedIndexChanged"
                        Width="70%">
                        <asp:ListItem Value="0">--- Select State/Province ---</asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource runat="server" ID="sdsAddUserState" ConnectionString='<%$ ConnectionStrings:FEDemogDB %>' SelectCommand="SELECT [stID], [stName], [ctyID] FROM [dmgState] WHERE ([ctyID] = @ctyID) ORDER BY [stName]">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ddlCountry" PropertyName="SelectedValue" Name="ctyID" Type="Int32"></asp:ControlParameter>
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:Label ID="lblCounty" runat="server" CssClass="genLabel" Width="25%" Text="County/District/Division/Municipality" />
                    <asp:DropDownList ID="ddlCounty" runat="server" DataSourceID="sdsAddUserCounty" DataTextField="cntyName" DataValueField="cntyID"
                        AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList" Enabled="False" OnSelectedIndexChanged="ddlCounty_SelectedIndexChanged"
                        Width="70%">
                        <asp:ListItem Value="0">--- Select County/District/Division/Municipality ---</asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource runat="server" ID="sdsAddUserCounty" ConnectionString='<%$ ConnectionStrings:FEDemogDB %>' SelectCommand="SELECT [cntyID], [cntyName], [stID] FROM [dmgCountyDistrict] WHERE ([stID] = @stID) ORDER BY [cntyName]">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ddlState" PropertyName="SelectedValue" Name="stID" Type="Int32"></asp:ControlParameter>
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:Label ID="lblCity" runat="server" CssClass="genLabel" Width="25%" Text="City/Town" />
                    <asp:DropDownList ID="ddlCity" runat="server" DataSourceID="sdsAddUserCity" DataTextField="cityName" DataValueField="cityID"
                        AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList" Enabled="False" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged"
                        Width="70%">
                        <asp:ListItem Value="0">--- Select City/Town ---</asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource runat="server" ID="sdsAddUserCity" ConnectionString='<%$ ConnectionStrings:FEDemogDB %>' SelectCommand="SELECT [cityID], [cityName], [cntyID] FROM [dmgCity] WHERE ([cntyID] = @cntyID) ORDER BY [cityName]">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ddlCounty" PropertyName="SelectedValue" Name="cntyID" Type="Int32"></asp:ControlParameter>
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:Label ID="lblZip" runat="server" CssClass="genLabel" Width="25%" Text="Zip/Postal Code" />
                    <asp:DropDownList ID="ddlZip" runat="server" DataSourceID="sdsAddUserZip" DataTextField="zipCode" DataValueField="zipID"
                        AutoPostBack="True" AppendDataBoundItems="True" CssClass="ddlList" Enabled="False" OnSelectedIndexChanged="ddlZip_SelectedIndexChanged"
                        Width="70%">
                        <asp:ListItem Value="0">--- Select Zip/Postal Code ---</asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource runat="server" ID="sdsAddUserZip" ConnectionString='<%$ ConnectionStrings:FEDemogDB %>' SelectCommand="SELECT [zipID], [zipCode], [cntyID] FROM [dmgZip] WHERE ([cntyID] = @cntyID) ORDER BY [zipCode]">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ddlCounty" PropertyName="SelectedValue" Name="cntyID" Type="Int32"></asp:ControlParameter>
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:Label ID="lblPhone" runat="server" CssClass="genLabel" Width="25%" Text="Phone" />
                    <asp:TextBox ID="txtPhone" runat="server" CssClass="genTextBox" Enabled="false" TextMode="Phone" Width="70%" />
                    <asp:RequiredFieldValidator runat="server" ID="rfvPhone" ControlToValidate="txtPhone" ErrorMessage="*" ForeColor="Maroon" />
                    <asp:Label ID="lblPhoneType" runat="server" CssClass="genLabel" Width="25%" Text="Type" />
                    <asp:DropDownList ID="ddlPhoneType" runat="server" Enabled="False" AppendDataBoundItems="True" DataSourceID="sdsAddUserPhoneType"
                        DataTextField="dmgName" DataValueField="dmgID" AutoPostBack="True" CssClass="ddlList" OnSelectedIndexChanged="ddlPhoneType_SelectedIndexChanged"
                        Width="70%">
                        <asp:ListItem Value="0">--- Select Phone Type ---</asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource runat="server" ID="sdsAddUserPhoneType" ConnectionString='<%$ ConnectionStrings:FEDemogDB %>' SelectCommand="SELECT [dmgID], [dmgName] FROM [dmgDemogType] ORDER BY [dmgName]">
                    </asp:SqlDataSource>
                    <asp:Label ID="lblEMail" runat="server" CssClass="genLabel" Width="25%" Text="E-Mail" />
                    <asp:TextBox ID="txtEMail" runat="server" CssClass="genTextBox" Enabled="false" TextMode="Email" Width="70%" />
                    <asp:RequiredFieldValidator runat="server" ID="rfvEMail" ControlToValidate="txtEmail" ErrorMessage="*" ForeColor="Maroon" />
                    <asp:Label ID="lblPassword" runat="server" CssClass="genLabel" Width="25%" Text="Password" />
                    <asp:TextBox ID="txtPassword" runat="server" CssClass="genTextBox" Enabled="false" TextMode="Password" Width="70%" />
                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword" ErrorMessage="*" ForeColor="Maroon" />
                    <asp:Label ID="lblCPassword" runat="server" CssClass="genLabel" Width="25%" Text="Confirm Password" />
                    <asp:TextBox ID="txtCPassword" runat="server" CssClass="genTextBox" Enabled="false" TextMode="Password" Width="70%" />
                    <asp:RequiredFieldValidator ID="rfvCPassword" runat="server" ControlToValidate="txtCPassword" ErrorMessage="*" ForeColor="Maroon" />
                    <asp:CompareValidator ID="cmpCPassword" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtCPassword" ForeColor="Maroon"
                        ErrorMessage="*" />

                    <asp:Button ID="btnAddUser" runat="server" Text="Add User" CssClass="red btn" OnClick="btnAddUser_Click" CausesValidation="True"
                        Width="25%" />
                    <asp:Button ID="btnCancel" runat="server" Text="Clear Values" CssClass="red btn" OnClick="btnCancel_Click" Width="15%" />
                </div>
                <asp:Label runat="server" ID="lblSuccess" Visible="False" CssClass="genLabel" Width="99.6%" ForeColor="White" />
                <asp:Button runat="server" ID="btnAddUserDone" CssClass="red btn" Width="25%" Visible="false" Text="Done" OnClick="btnAddUserDone_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="divUsrAcctStatus" runat="server" class="viewer" style="width: 98.5%;">
        <asp:UpdateProgress ID="uprgrsAccStatus" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlUsrAcc">
            <ProgressTemplate>
                <div id="divAccntPrgrs" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center; z-index: 100;">
                    <asp:Image ID="imgAccntProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="upnlUsrAcc" runat="server">
            <ContentTemplate>
                <div id="divAccountStatus" runat="server" class="viewer divShadow" style="width: 99.1%;">
                    <asp:Label ID="lblAccntLabel" runat="server" CssClass="genLabel" Width="99.6%" Text="User Account Configuration" />
                    <div id="divAccntLockout" runat="server" class="viewer">
                        <asp:Label ID="lblLockoutLabel" runat="server" CssClass="genLabel" Width="99.6%" Text="User Account Lock/UnLock"
                            Font-Underline="True" />
                        <asp:Label ID="lblLockoutUserType" runat="server" CssClass="genLabel" Width="25%" Text="Select User Type" />
                        <asp:DropDownList ID="ddlLockoutUserType" runat="server" AutoPostBack="true" AppendDataBoundItems="true" CssClass="ddlList"
                            Width="60%" DataSourceID="sdsLockoutUserType" DataValueField="RoleId" DataTextField="RoleName" OnSelectedIndexChanged="ddlLockoutUserType_SelectedIndexChanged">
                            <asp:ListItem Value="0">--- Select User Account Type ---</asp:ListItem>
                        </asp:DropDownList>
                        <asp:SqlDataSource runat="server" ID="sdsLockoutUserType" ConnectionString='<%$ ConnectionStrings:FEUsersDB %>' SelectCommand="SELECT [RoleId], [RoleName] FROM [vw_aspnet_Roles] ORDER BY [RoleName]">
                        </asp:SqlDataSource>
                        <asp:Label ID="lblLockoutUserList" runat="server" CssClass="genLabel" Width="25%" Text="Select System User" />
                        <asp:DropDownList ID="ddlLockoutUserList" runat="server" AutoPostBack="true" AppendDataBoundItems="true" CssClass="ddlList"
                            Width="60%"
                            DataSourceID="sdsLockoutUserList" DataValueField="UserId" DataTextField="UserName" OnSelectedIndexChanged="ddlLockoutUserList_SelectedIndexChanged"
                            Enabled="False">
                            <asp:ListItem Value="0">--- Select SMARTs User Account ---</asp:ListItem>
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="sdsLockoutUserList" runat="server" ConnectionString='<%$ ConnectionStrings:FEUsersDB %>'
                            SelectCommand="SELECT aspnet_Users.UserId, aspnet_Users.UserName, UserInClient.clntID, aspnet_Roles.RoleId, aspnet_Membership.IsLockedOut FROM aspnet_Users INNER JOIN UserInClient ON aspnet_Users.UserId = UserInClient.UserId INNER JOIN aspnet_UsersInRoles ON aspnet_Users.UserId = aspnet_UsersInRoles.UserId INNER JOIN aspnet_Roles ON aspnet_UsersInRoles.RoleId = aspnet_Roles.RoleId INNER JOIN aspnet_Membership ON aspnet_Users.UserId = aspnet_Membership.UserId WHERE (UserInClient.clntID = @clntID) AND (aspnet_Roles.RoleId IN (SELECT RoleId FROM aspnet_Roles AS aspnet_Roles_1 WHERE (RoleName = @RoleName))) AND (aspnet_Membership.IsLockedOut = 1)">
                            <SelectParameters>
                                <asp:SessionParameter Name="clntID" SessionField="clntID" />
                                <asp:SessionParameter Name="RoleName" SessionField="userRole" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <asp:Label ID="lblLockout" runat="server" CssClass="genLabel" Width="25%" Text="UnLock User Account" />
                        <asp:DropDownList ID="ddlLockout" runat="server" CssClass="ddlList" AppendDataBoundItems="True" AutoPostBack="True"
                            Enabled="False" Width="60%" OnSelectedIndexChanged="ddlLockout_SelectedIndexChanged">
                            <asp:ListItem Value="0">--- UnLock User Account ---</asp:ListItem>
                            <asp:ListItem Value="1">UnLock</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Label ID="lblLockoutSuccess" runat="server" CssClass="genLabel" Width="99.6%" Text="" Visible="false" />
                        <asp:Button ID="btnLockoutCancel" runat="server" CssClass="red btn" Width="25%" Text="Cancel" Enabled="false" OnClick="btnLockoutCancel_Click" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>

<!-- JavaScripts -->
<%--<script src="../../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>--%>