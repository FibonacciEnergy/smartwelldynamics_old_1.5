﻿using System;
using System.Web;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI.WebControls;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;

namespace SmartsVer1.Control.Config.Units
{
    public partial class ctrlClientUnits : System.Web.UI.UserControl
    {
        String LoginName, username, domain, GetClientDBString;
        Int32 ClientID = -99;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
                //Getting datasources for gridview controls
                System.Data.DataTable accelTable = DMG.getAccelUnitsTable();
                System.Data.DataTable magTable = DMG.getMagUnitsTable();
                System.Data.DataTable lenTable = DMG.getLenUnitsTable();
                System.Data.DataTable datTable = AST.getCordSysTable();
                System.Data.DataTable prjTable = DMG.getProjTable();
                System.Data.DataTable mmdlTable = DMG.getMMDLTable();
                System.Data.DataTable jcTable = DMG.getConvergenceTable();
                System.Data.DataTable drefTable = DMG.getDepthRefTable(); 

                //Assigning datasources to gridviews
                this.gvwAcelU.DataSource = accelTable;
                this.gvwAcelU.DataBind();
                this.gvwMagU.DataSource = magTable;
                this.gvwMagU.DataBind();
                this.gvwLenU.DataSource = lenTable;
                this.gvwLenU.DataBind();
                this.gvwDatumList.DataSource = datTable;
                this.gvwDatumList.DataBind();
                this.gvwProj.DataSource = prjTable;
                this.gvwProj.DataBind();
                this.gvwMagModel.DataSource = mmdlTable;
                this.gvwMagModel.DataBind();
                this.gvwConverg.DataSource = jcTable;
                this.gvwConverg.DataBind();
                this.gvwDRef.DataSource = drefTable;
                this.gvwDRef.DataBind();                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwDatumList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable datTable = AST.getCordSysTable();
                this.gvwDatumList.DataSource = datTable;
                this.gvwDatumList.PageIndex = e.NewPageIndex;                
                this.gvwDatumList.DataBind();
                this.gvwDatumList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDTClear_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable datTable = AST.getCordSysTable();
                this.gvwDatumList.PageIndex = 0;
                this.gvwDatumList.SelectedIndex = -1;
                this.gvwDatumList.DataSource = datTable;
                this.gvwDatumList.DataBind();
                this.gvwDatumList.Focus();
                this.btnDTClear.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwProj_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable prjTable = DMG.getProjTable();
                this.gvwProj.PageIndex = e.NewPageIndex;
                this.gvwProj.DataSource = prjTable;
                this.gvwProj.DataBind();
                this.gvwProj.Focus();
                this.btnPRJClear.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnPRJClear_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable prjTable = DMG.getProjTable();
                this.gvwProj.DataSource = prjTable;
                this.gvwProj.PageIndex = 0;
                this.gvwProj.SelectedIndex = -1;
                this.gvwProj.DataBind();
                this.gvwProj.Focus();
                this.btnPRJClear.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwMagModel_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable mmdlTable = DMG.getMMDLTable();
                this.gvwMagModel.PageIndex = e.NewPageIndex;
                this.gvwMagModel.DataSource = mmdlTable;
                this.gvwMagModel.DataBind();
                this.gvwMagModel.Focus();
                this.btnMDClear.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnMDClear_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable mmdlTable = DMG.getMMDLTable();
                this.gvwMagModel.PageIndex = 0;
                this.gvwMagModel.SelectedIndex = -1;
                this.gvwMagModel.DataSource = mmdlTable;
                this.gvwMagModel.DataBind();
                this.gvwMagModel.Focus();
                this.btnMDClear.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwConverg_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable jcTable = DMG.getConvergenceTable();
                this.gvwConverg.PageIndex = e.NewPageIndex;
                this.gvwConverg.DataSource = jcTable;
                this.gvwConverg.DataBind();
                this.gvwConverg.Focus();
                this.btnGCClear.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnGCClear_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable jcTable = DMG.getConvergenceTable();
                this.gvwConverg.PageIndex = 0;
                this.gvwConverg.SelectedIndex = -1;
                this.gvwConverg.DataBind();
                this.gvwConverg.Focus();
                this.btnGCClear.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        protected void gvwLenU_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable jcTable = DMG.getLenUnitsTable();
                this.gvwLenU.PageIndex = e.NewPageIndex;
                this.gvwLenU.DataSource = jcTable;
                this.gvwLenU.DataBind();
                this.gvwLenU.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnLNClear_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable jcTable = DMG.getLenUnitsTable();
                this.gvwLenU.PageIndex = 0;
                this.gvwLenU.SelectedIndex = -1;
                this.gvwLenU.DataSource = jcTable;
                this.gvwLenU.DataBind();
                this.gvwLenU.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwDRef_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable drefTable = DMG.getDepthRefTable();
                this.gvwDRef.PageIndex = 0;
                this.gvwDRef.SelectedIndex = -1;
                this.gvwDRef.DataSource = drefTable;
                this.gvwDRef.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
        
        protected void btnDRefClear_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable drefTable = DMG.getDepthRefTable();
                this.gvwDRef.PageIndex = 0;
                this.gvwDRef.SelectedIndex = -1;
                this.gvwDRef.DataSource = drefTable;
                this.gvwDRef.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        
    }
}