﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlClientUnits.ascx.cs" Inherits="SmartsVer1.Control.Config.Units.ctrlClientUnits" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

<!--- Stylesheet --->
<link href="../../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript -->
<script src="../../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<!-- Javascript for Footer -->
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>
<!--- Page local style --->
<style type="text/css" style="background-color: #d9534f; color: #FFFFFF">
    .rtAligned {
        text-align: right;
    }

    .swdMenu {
        color: #ffffff;
    }    
</style>
<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<!--- Login --->
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">        
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>Configuration</li>
                <li><a href="../../../Config/Units/fecUnits_2A.aspx"> Measurement Units</a></li>
            </ol>
        </div>
    </div>    
</div>
<!-- Page Content -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul class="nav nav-pills nav-justified">
                <li class="active"><a data-toggle="pill" href="#acc">Accelerometer Units</a></li>
                <li><a data-toggle="pill" href="#mag">Magnetometer Units</a></li>
                <li><a data-toggle="pill" href="#len">Length Measurements Units</a></li>
                <li><a data-toggle="pill" href="#prj">Projections</a></li>
                <li><a data-toggle="pill" href="#mmdl">Magnetic Models</a></li>
                <li><a data-toggle="pill" href="#gc">Grid Convergence</a></li>
                <li><a data-toggle="pill" href="#dat">Datums</a></li>
                <li><a data-toggle="pill" href="#ddr">Depth References</a></li>
            </ul>
            <div class="tab-content">
                <div id="acc" class="tab-pane fade in active">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h5 class="panel-title"><span>Accelerometer Units List</span></h5>
                        </div>
                        <div class="panel-body" style="min-height: 450px;">
                            <asp:UpdateProgress ID="uprgsAccel" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlAccel">
                                <ProgressTemplate>
                                    <div id="divAccelProcessing" runat="server" class="updateProgress">
                                        <asp:Image ID="imgAccelProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel runat="server" ID="upnlAccel">
                                <ContentTemplate>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:GridView ID="gvwAcelU" runat="server" AutoGenerateColumns="False" DataKeyNames="acuID" AllowPaging="True"
                                                CssClass="mydatagrid" EmptyDataText="No Accelerometer Units found." ShowHeaderWhenEmpty="True" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows">
                                                <Columns>
                                                    <asp:BoundField DataField="acuID" HeaderText="acuID" ReadOnly="True" Visible="False" />
                                                    <asp:BoundField DataField="acuName" HeaderText="Accelerometer Unit" />
                                                    <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div id="mag" class="tab-pane fade">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h5 class="panel-title"><span>Magnetometer Units List</span></h5>
                        </div>
                        <div class="panel-body" style="min-height: 450px;">
                            <asp:UpdateProgress ID="uprgrsMag" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlMag">
                                <ProgressTemplate>
                                    <div id="divMagProcessing" runat="server" class="updateProgress">
                                        <asp:Image ID="imgMagProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel runat="server" ID="upnlMag">
                                <ContentTemplate>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:GridView ID="gvwMagU" runat="server" AutoGenerateColumns="False" AllowPaging="True" CssClass="mydatagrid"
                                                DataKeyNames="muID" EmptyDataText="No Magnetometer Units found." ShowHeaderWhenEmpty="True" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows">
                                                <Columns>
                                                    <asp:BoundField DataField="muID" HeaderText="muID" Visible="False" />
                                                    <asp:BoundField DataField="muName" HeaderText="Magnetometer Unit" />
                                                    <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div id="len" class="tab-pane fade">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h5 class="panel-title"><span>Length Meaurement Units List</span></h5>
                        </div>
                        <div class="panel-body" style="min-height: 450px;">
                            <asp:UpdateProgress ID="uprgrsLength" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlLength">
                                <ProgressTemplate>
                                    <div id="divLengthProcessing" runat="server" class="updateProgress">
                                        <asp:Image ID="imgLengthProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel runat="server" ID="upnlLength">
                                <ContentTemplate>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:GridView ID="gvwLenU" runat="server" AutoGenerateColumns="False" AllowPaging="True" CssClass="mydatagrid"
                                                DataKeyNames="luID" EmptyDataText="No Length/Distance Measurement Units found." ShowHeaderWhenEmpty="True" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnPageIndexChanging="gvwLenU_PageIndexChanging">
                                                <Columns>
                                                    <asp:BoundField DataField="luID" HeaderText="luID" Visible="False" />
                                                    <asp:BoundField DataField="luName" HeaderText="Distance/Length Measurement Unit" />
                                                    <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div id="prj" class="tab-pane fade">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h5 class="panel-title"><span>Projections List</span></h5>
                        </div>
                        <div class="panel-body" style="min-height: 450px;">
                            <asp:UpdateProgress ID="uprgrsPrj" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlPrj">
                                <ProgressTemplate>
                                    <div id="divPrjProcessing" runat="server" class="updateProgress">
                                        <asp:Image ID="imgPrjProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel runat="server" ID="upnlPrj">
                                <ContentTemplate>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group" style="width: 100%;">
                                                <asp:LinkButton ID="btnPRJClear" runat="server" CssClass="btn btn-warning text-center" OnClear="btnPRJClear_Click"><i class="fa fa-refresh"> Refresh</i></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:GridView ID="gvwProj" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                                CssClass="mydatagrid" DataKeyNames="projID" EmptyDataText="No Projections found." ShowHeaderWhenEmpty="True" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnPageIndexChanging="gvwProj_PageIndexChanging">
                                                <Columns>
                                                    <asp:BoundField DataField="projID" Visible="False" />
                                                    <asp:BoundField DataField="projName" HeaderText="Projection" />
                                                    <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div id="mmdl" class="tab-pane fade">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h5 class="panel-title"><span>Magnetic Models List</span></h5>
                        </div>
                        <div class="panel-body" style="min-height: 450px;">
                            <asp:UpdateProgress ID="uprgrsMmdl" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlMmdl">
                                <ProgressTemplate>
                                    <div id="divMmdlProcessing" runat="server" class="updateProgress">
                                        <asp:Image ID="imgMmdlProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel runat="server" ID="upnlMmdl">
                                <ContentTemplate>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group" style="width: 100%;">
                                                <asp:LinkButton ID="btnMDClear" runat="server" CssClass="btn btn-warning text-center" OnClick="btnMDClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:GridView ID="gvwMagModel" runat="server" AutoGenerateColumns="False" AllowPaging="True" CssClass="mydatagrid"
                                                DataKeyNames="mmdlID" EmptyDataText="No Magnetic Model found." ShowHeaderWhenEmpty="True" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnPageIndexChanging="gvwMagModel_PageIndexChanging">
                                                <Columns>
                                                    <asp:BoundField DataField="mmdlID" Visible="False" />
                                                    <asp:BoundField DataField="mmdlName" HeaderText="Magnetic Model" />
                                                    <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div id="gc" class="tab-pane fade">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h5 class="panel-title"><span>Grid Convergence List</span></h5>
                        </div>
                        <div class="panel-body" style="min-height: 450px;">
                            <asp:UpdateProgress ID="uprgrsGC" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlGC">
                                <ProgressTemplate>
                                    <div id="divGCProcessing" runat="server" class="updateProgress">
                                        <asp:Image ID="imgGCProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel runat="server" ID="upnlGC">
                                <ContentTemplate>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group" style="width: 100%;">
                                                <asp:LinkButton ID="btnGCClear" runat="server" CssClass="btn btn-warning text-center" OnClick="btnGCClear_Click"><i class="fa fa-refresh"> Refresh</i></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:GridView ID="gvwConverg" runat="server" AutoGenerateColumns="False" AllowPaging="True" CssClass="mydatagrid"
                                                DataKeyNames="jcID" EmptyDataText="No Grid Convergence Parameters found." ShowHeaderWhenEmpty="True" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnPageIndexChanging="gvwConverg_PageIndexChanging">
                                                <Columns>
                                                    <asp:BoundField DataField="jcID" Visible="False" />
                                                    <asp:BoundField DataField="jcName" HeaderText="Grid Convergence" />
                                                    <asp:BoundField DataField="uTime" HeaderText="Last Updated On" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div id="dat" class="tab-pane fade">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h5 class="panel-title"><span>Datums List</span></h5>
                        </div>
                        <div class="panel-body" style="min-height: 450px;">
                            <asp:UpdateProgress ID="uprgrsDat" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlDat">
                                <ProgressTemplate>
                                    <div id="divDatProcessing" runat="server" class="updateProgress">
                                        <asp:Image ID="imgDatProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel runat="server" ID="upnlDat">
                                <ContentTemplate>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="input-group" style="width: 100%;">
                                                <asp:LinkButton ID="btnDTClear" runat="server" CssClass="btn btn-warning text-center" OnClick="btnDTClear_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <asp:GridView ID="gvwDatumList" runat="server" AutoGenerateColumns="False" AllowPaging="True" CssClass="mydatagrid"
                                                DataKeyNames="cordID" EmptyDataText="No Datum found." ShowHeaderWhenEmpty="True" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                                PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnPageIndexChanging="gvwDatumList_PageIndexChanging">
                                                <Columns>
                                                    <asp:BoundField DataField="cordID" HeaderText="cordID" Visible="False" />
                                                    <asp:BoundField DataField="cordName" HeaderText="Datum" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div id="ddr" class="tab-pane fade">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h5 class="panel-title"><span>Depth References List</span></h5>
                        </div>
                        <div class="panel-body" style="min-height: 450px;">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <asp:GridView ID="gvwDRef" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        CssClass="mydatagrid" DataKeyNames="wpdrID" EmptyDataText="No Datum found." ShowHeaderWhenEmpty="True" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                                        PageSize="5" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnPageIndexChanging="gvwDRef_PageIndexChanging">
                                        <Columns>
                                            <asp:BoundField DataField="wpdrID" HeaderText="wpdrID" Visible="False" />
                                            <asp:BoundField DataField="wpdrName" HeaderText="Datum Depth Reference" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
