﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI.WebControls;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;

namespace SmartsVer1.Control.Config.Units
{
    public partial class ctrlFEUnits : System.Web.UI.UserControl
    {
        String LoginName, username, domain;
        Int32 ClientID = -99;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;
                UP usrP = UP.GetUserProfile(LoginName);
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwAcelUList_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                this.sdsAcelUList.UpdateParameters["uTime"].DefaultValue = Context.Timestamp.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAcelUAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.divAcelUAdd.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAcelUCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.divAcelUAdd.Visible = false;

                this.gvwAcelUList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void dvwAcelUAdd_ItemInserting(object sender, DetailsViewInsertEventArgs e)
        {
            try
            {
                String uName = null;

                TextBox txtUName = (TextBox)dvwAcelUAdd.FindControl("txtAccUName");
                uName = Convert.ToString(txtUName.Text);
                if (String.IsNullOrEmpty(uName))
                {
                    this.lblAccUAddSuccess.Text = "Please provide a valid Accelerometer Unit Name before click Insert button.";
                    this.lblAccUAddSuccess.Visible = true;

                    e.Cancel = true;
                    this.dvwAcelUAdd.Focus();
                }
                else
                {
                    this.sdsAcelUAdd.InsertParameters["cTime"].DefaultValue = Context.Timestamp.ToString();
                    this.sdsAcelUAdd.InsertParameters["uTime"].DefaultValue = Context.Timestamp.ToString();

                    this.lblAccUAddSuccess.Text = "Successfully added new Accelerometer Unit.";
                    this.lblAccUAddSuccess.Visible = true;

                    this.gvwAcelUList.DataBind();

                    this.dvwAcelUAdd.Focus();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAccUAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.lblAccUAddSuccess.Text = "";
                this.lblAccUAddSuccess.Visible = false;
                this.divAcelUAdd.Visible = false;

                this.gvwAcelUList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnMagUAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.divMagUAdd.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnMagUCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.divMagUAdd.Visible = false;

                this.gvwMagUList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwMagUList_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                this.sdsMagUList.UpdateParameters["uTime"].DefaultValue = Context.Timestamp.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwLenU_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                this.sdsLenUList.UpdateParameters["uTime"].DefaultValue = Context.Timestamp.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnLenUAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.divLenUAdd.Visible = true;
                this.lblLenUAddSuccess.Text = "";
                this.lblLenUAddSuccess.Visible = false;

                this.dvwLenUAdd.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnLenUCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.divLenUAdd.Visible = false;
                this.lblLenUAddSuccess.Text = "";
                this.lblLenUAddSuccess.Visible = false;

                this.gvwLenU.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwDatumList_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                this.sdsDatumList.UpdateParameters["uTime"].DefaultValue = Context.Timestamp.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDatumAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.divDatumAdd.Visible = true;
                this.lblDatumAddSuccess.Text = "";
                this.lblDatumAddSuccess.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDatumCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.divDatumAdd.Visible = false;
                this.lblDatumAddSuccess.Text = "";
                this.lblDatumAddSuccess.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProjAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.divProjection.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProjCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.divProjection.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnMMAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.divMagModel.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnMMCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.divMagModel.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwMagModel_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                this.sdsMagModelList.UpdateParameters["uTime"].DefaultValue = Context.Timestamp.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwGCList_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                this.sdsGCList.UpdateParameters["uTime"].DefaultValue = Context.Timestamp.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnGCAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.divGCAdd.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnGCCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.divGCAdd.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void dvwMagUAdd_ItemInserting(object sender, DetailsViewInsertEventArgs e)
        {
            try
            {
                String uName = null;

                TextBox txtMU = (TextBox)dvwMagUAdd.FindControl("txtMagUAdd");
                uName = Convert.ToString(txtMU.Text);
                if (String.IsNullOrEmpty(uName))
                {
                    this.lblMagUAddSuccess.Text = "Please provide a valid Magnetometer Unit Name before click Insert button.";
                    this.lblMagUAddSuccess.Visible = true;
                    e.Cancel = true;

                    this.dvwMagUAdd.Focus();
                }
                else
                {
                    this.sdsMagUAdd.InsertParameters["cTime"].DefaultValue = Context.Timestamp.ToString();
                    this.sdsMagUAdd.InsertParameters["uTime"].DefaultValue = Context.Timestamp.ToString();

                    this.lblMagUAddSuccess.Text = "Successfully added new Magnetometer Unit.";
                    this.lblMagUAddSuccess.Visible = true;

                    this.gvwMagUList.DataBind();

                    this.dvwMagUAdd.Focus();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnMagUAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.lblMagUAddSuccess.Text = "";
                this.lblMagUAddSuccess.Visible = false;
                this.divMagUAdd.Visible = false;

                this.gvwMagUList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void dvwLenUAdd_ItemInserting(object sender, DetailsViewInsertEventArgs e)
        {
            try
            {
                String luName = null;

                TextBox txtLU = (TextBox)dvwLenUAdd.FindControl("txtLUAdd");
                luName = Convert.ToString(txtLU.Text);

                if (String.IsNullOrEmpty(luName))
                {
                    this.lblLenUAddSuccess.Text = "Please provide a Length/Distance Unit name before clicking Insert button.";
                    this.lblLenUAddSuccess.Visible = true;

                    e.Cancel = true;
                    this.dvwLenUAdd.Focus();
                }
                else
                {
                    this.sdsLenUAdd.InsertParameters["cTime"].DefaultValue = Context.Timestamp.ToString();
                    this.sdsLenUAdd.InsertParameters["uTime"].DefaultValue = Context.Timestamp.ToString();

                    this.lblLenUAddSuccess.Text = "Successfully added new Length/Distance Unit.";
                    this.lblLenUAddSuccess.Visible = true;

                    this.gvwLenU.DataBind();

                    this.dvwLenUAdd.Focus();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnLenUAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.lblLenUAddSuccess.Text = "";
                this.lblLenUAddSuccess.Visible = false;
                this.divLenUAdd.Visible = false;

                this.gvwLenU.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void dvwDatum_ItemInserting(object sender, DetailsViewInsertEventArgs e)
        {
            try
            {
                String uName = null;

                TextBox txtUName = (TextBox)dvwDatum.FindControl("txtDatAdd");
                uName = Convert.ToString(txtUName.Text);

                if (String.IsNullOrEmpty(uName))
                {
                    this.lblDatumAddSuccess.Text = "Please provide a Datum value before clicking the Insert button.";
                    this.lblDatumAddSuccess.Visible = true;
                    e.Cancel = true;

                    this.dvwDatum.Focus();
                }
                else
                {
                    this.sdsDatumAdd.InsertParameters["cTime"].DefaultValue = Context.Timestamp.ToString();
                    this.sdsDatumAdd.InsertParameters["uTime"].DefaultValue = Context.Timestamp.ToString();

                    this.lblDatumAddSuccess.Text = "Successfully added new Datum.";
                    this.lblDatumAddSuccess.Visible = true;

                    this.gvwDatumList.DataBind();

                    this.dvwDatum.Focus();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDatumAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.lblDatumAddSuccess.Text = "";
                this.lblDatumAddSuccess.Visible = false;
                this.divDatumAdd.Visible = false;

                this.gvwDatumList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void dvwProjection_ItemInserting(object sender, DetailsViewInsertEventArgs e)
        {
            try
            {
                String uName = null;

                TextBox txtUName = (TextBox)dvwProjection.FindControl("txtProjAdd");
                uName = Convert.ToString(txtUName.Text);

                if (String.IsNullOrEmpty(uName))
                {
                    this.lblProjAddSuccess.Text = "Please provide a Projection value before clicking the insert button.";
                    this.lblProjAddSuccess.Visible = true;

                    e.Cancel = true;
                    this.dvwProjection.Focus();
                }
                else
                {
                    this.sdsProjectionAdd.InsertParameters["cTime"].DefaultValue = Context.Timestamp.ToString();
                    this.sdsProjectionAdd.InsertParameters["uTime"].DefaultValue = Context.Timestamp.ToString();

                    this.lblProjAddSuccess.Text = "Successfully added new Projection value.";
                    this.lblProjAddSuccess.Visible = true;

                    this.gvwProjectionList.DataBind();

                    this.dvwProjection.Focus();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnProjAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.lblProjAddSuccess.Text = "";
                this.lblProjAddSuccess.Visible = false;
                this.divProjectionAdd.Visible = false;

                this.gvwProjectionList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void dvwMagmOdelAdd_ItemInserting(object sender, DetailsViewInsertEventArgs e)
        {
            try
            {
                String uName = null;

                TextBox txtMMDL = (TextBox)dvwMagmOdelAdd.FindControl("txtMMDL");
                uName = Convert.ToString(txtMMDL.Text);

                if (String.IsNullOrEmpty(uName))
                {
                    this.lblMMDLAddSuccess.Text = "Please provide a valid Magnetic Model name before clicking the insert button.";
                    this.lblMMDLAddSuccess.Visible = true;

                    e.Cancel = true;
                    this.dvwMagmOdelAdd.Focus();
                }
                else
                {
                    this.sdsMagModelAdd.InsertParameters["cTime"].DefaultValue = Context.Timestamp.ToString();
                    this.sdsMagModelAdd.InsertParameters["uTime"].DefaultValue = Context.Timestamp.ToString();

                    this.lblMMDLAddSuccess.Text = "Successfully added new Magnetic Model.";
                    this.lblMMDLAddSuccess.Visible = true;

                    this.gvwMagModel.DataBind();

                    this.dvwMagmOdelAdd.Focus();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnMMDLCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.divMagModelAdd.Visible = false;
                this.lblMMDLAddSuccess.Text = "";
                this.lblMMDLAddSuccess.Visible = false;

                this.gvwMagModel.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void dvwGCAdd_ItemInserting(object sender, DetailsViewInsertEventArgs e)
        {
            try
            {
                String uName = null;

                TextBox txtGCName = (TextBox)dvwGCAdd.FindControl("txtGCAdd");
                uName = Convert.ToString(txtGCName.Text);

                if (String.IsNullOrEmpty(uName))
                {
                    this.lblGCAddSuccess.Text = "Please provide a valid Grid Convergence value before clicking the Insert button.";
                    this.lblGCAddSuccess.Visible = true;

                    e.Cancel = true;
                    this.dvwGCAdd.Focus();
                }
                else
                {
                    this.sdsGCAdd.InsertParameters["cTime"].DefaultValue = Context.Timestamp.ToString();
                    this.sdsGCAdd.InsertParameters["uTime"].DefaultValue = Context.Timestamp.ToString();

                    this.lblGCAddSuccess.Text = "Successfully added New Grid Convergence.";
                    this.lblGCAddSuccess.Visible = true;

                    this.gvwGCList.DataBind();

                    this.dvwGCAdd.Focus();
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnGCAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.divGCAdd.Visible = false;
                this.lblGCAddSuccess.Text = "";
                this.lblGCAddSuccess.Visible = false;

                this.gvwGCList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}