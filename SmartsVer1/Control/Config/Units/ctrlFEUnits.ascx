﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlFEUnits.ascx.cs" Inherits="SmartsVer1.Control.Config.Units.ctrlFEUnits" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<!-- Stylesheet -->
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript -->
<script src="../../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<!--- Page local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }

    .rotate {
        -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
        -moz-transform: rotate(90deg); /* Firefox */
        -ms-transform: rotate(90deg); /* IE 9 */
        transform: rotate(90deg); /* Standard syntax */
    }

    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper[data-stepper-steps="8"] li {
            width: 12%;
        }

        ol.stepper[data-stepper-steps="9"] li {
            width: 11%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }    
</style>
<!--- Page local script --->
<script type="text/javascript">
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("rotate");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").addClass("rotate");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("rotate");
        });
    });
</script>

<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">        
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>System Configuration</li>
                <li><a href="../../../Config/Units/fesUnits_2A.aspx"> Measurement Units</a></li>
            </ol>
        </div>
    </div>    
</div>

<!-- Page Content -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="content-wrapper viewer" style="clear: right; width:98%;">

    <div id="divMUnits" runat="server" class="viewer" style="width:97.5%;">

        <asp:UpdateProgress ID="uprgsACU" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlAcelUnits">
            <ProgressTemplate>
                <div id="divACUs" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center; z-index: 100;">
                    <asp:Image ID="imgACUProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel runat="server" ID="upnlAcelUnits">
            <ContentTemplate>

                <asp:Label ID="lblAcelUHead" runat="server" Text="Accelerometer Unit" Width="99.6%" CssClass="genLabel" />
                <div id="divAcelUList" runat="server">
                    
                    <asp:GridView ID="gvwAcelUList" runat="server" AutoGenerateColumns="False" DataKeyNames="acuID" DataSourceID="sdsAcelUList"
                        AllowPaging="True" AllowSorting="True" CssClass="mydatagrid" Width="99.6%" EmptyDataText="No Accelerometer Units found."
                        ShowHeaderWhenEmpty="True" OnRowUpdating="gvwAcelUList_RowUpdating" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header"
                        PagerStyle-CssClass="pager" RowStyle-CssClass="rows">
                        <Columns>
                            <asp:BoundField DataField="acuID" HeaderText="acuID" Visible="False" />                            
                            <asp:BoundField DataField="acuName" HeaderText="Accelerometer Unit" />                            
                            <asp:BoundField DataField="uTime" HeaderText="Last Updated On" SortExpression="uTime" ReadOnly="True" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource runat="server" ID="sdsAcelUList" ConnectionString='<%$ ConnectionStrings:FEDemogDB %>' DeleteCommand="DELETE FROM [dmgAccelUnit] WHERE [acuID] = @acuID"
                        InsertCommand="INSERT INTO [dmgAccelUnit] ([acuName], [cTime], [uTime]) VALUES (@acuName, @cTime, @uTime)" SelectCommand="SELECT [acuID], [acuName], [cTime], [uTime] FROM [dmgAccelUnit] ORDER BY [acuName]"
                        UpdateCommand="UPDATE [dmgAccelUnit] SET [acuName] = @acuName, [cTime] = @cTime, [uTime] = @uTime WHERE [acuID] = @acuID">
                        <DeleteParameters>
                            <asp:Parameter Name="acuID" Type="Int32"></asp:Parameter>
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="acuName" Type="String"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="cTime"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="uTime"></asp:Parameter>
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="acuName" Type="String"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="cTime"></asp:Parameter>
                            <asp:Parameter Name="uTime" DbType="DateTime2"></asp:Parameter>
                            <asp:Parameter Name="acuID" Type="Int32"></asp:Parameter>
                        </UpdateParameters>
                    </asp:SqlDataSource>

                    <asp:Button ID="btnAcelUAdd" runat="server" CssClass="red btn" Text="Add New Accelerometer Unit" Width="25%" OnClick="btnAcelUAdd_Click" />
                    <asp:Button ID="btnAcelUCancel" runat="server" CssClass="red btn" Text="Cancel" OnClick="btnAcelUCancel_Click" />

                </div>
                <div id="divAcelUAdd" runat="server" class="viewer" visible="false">

                    <asp:DetailsView ID="dvwAcelUAdd" runat="server" DataKeyNames="acuID" DataSourceID="sdsAcelUAdd"
                        Height="50px" Width="98.6%" CellPadding="4" ForeColor="#333333" GridLines="None"
                        AutoGenerateRows="False" CaptionAlign="Top" DefaultMode="Insert" AutoGenerateInsertButton="True"
                        HeaderText="Add New Accelerometer Unit" HeaderStyle-BackColor="Silver" CssClass="borderRound"
                        OnItemInserting="dvwAcelUAdd_ItemInserting">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                        <CommandRowStyle BackColor="#FF6600" Font-Bold="True" ForeColor="White"></CommandRowStyle>
                        <EditRowStyle BackColor="#999999"></EditRowStyle>
                        <FieldHeaderStyle BackColor="LightSteelBlue" Font-Bold="True"></FieldHeaderStyle>
                        <Fields>
                            <asp:BoundField DataField="acuID" HeaderText="acuID" ReadOnly="True" InsertVisible="False" SortExpression="acuID"></asp:BoundField>
                            <asp:TemplateField HeaderText="Accelerometer Unit" SortExpression="acuName">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("acuName") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="txtAccUName" runat="server" CssClass="genTextBox" Text='<%# Bind("acuName") %>' Width="98%"></asp:TextBox>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("acuName") %>'></asp:Label>
                                </ItemTemplate>
                                <ControlStyle CssClass="genTextBox" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="cTime" HeaderText="cTime" SortExpression="cTime" Visible="False"></asp:BoundField>
                            <asp:BoundField DataField="uTime" HeaderText="uTime" SortExpression="uTime" Visible="False"></asp:BoundField>
                        </Fields>
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"></FooterStyle>
                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <PagerStyle HorizontalAlign="Center" BackColor="#284775" ForeColor="White"></PagerStyle>
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" CssClass="TextBox"></RowStyle>
                    </asp:DetailsView>
                    <asp:SqlDataSource runat="server" ID="sdsAcelUAdd" ConnectionString='<%$ ConnectionStrings:FEDemogDB %>' DeleteCommand="DELETE FROM [dmgAccelUnit] WHERE [acuID] = @acuID"
                        InsertCommand="INSERT INTO [dmgAccelUnit] ([acuName], [cTime], [uTime]) VALUES (@acuName, @cTime, @uTime)" SelectCommand="SELECT [acuID], [acuName], [cTime], [uTime] FROM [dmgAccelUnit]"
                        UpdateCommand="UPDATE [dmgAccelUnit] SET [acuName] = @acuName, [cTime] = @cTime, [uTime] = @uTime WHERE [acuID] = @acuID">
                        <DeleteParameters>
                            <asp:Parameter Name="acuID" Type="Int32"></asp:Parameter>
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="acuName" Type="String"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="cTime"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="uTime"></asp:Parameter>
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="acuName" Type="String"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="cTime"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="uTime"></asp:Parameter>
                            <asp:Parameter Name="acuID" Type="Int32"></asp:Parameter>
                        </UpdateParameters>
                    </asp:SqlDataSource>
                    <asp:Label ID="lblAccUAddSuccess" runat="server" Text="" CssClass="lblSuccess" Width="98%" Visible="false" />

                    <asp:Button ID="btnAccUAddCancel" runat="server" Text="Cancel" CssClass="red btn" OnClick="btnAccUAddCancel_Click" />

                </div>

            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    <div id="divMagUnits" runat="server" class="viewer" style="width:97.5%;">

        <asp:UpdateProgress ID="uprgsMU" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlMagU">
            <ProgressTemplate>
                <div id="divMUs" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center; z-index: 100;">
                    <asp:Image ID="imgMUProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel runat="server" ID="upnlMagU">
            <ContentTemplate>

                <asp:Label ID="lblMagUHead" runat="server" Text="Magnetometer Unit" Width="99.6%" CssClass="genLabel" />
                <div id="divMagUList" runat="server">
                    
                    <asp:GridView ID="gvwMagUList" runat="server" AutoGenerateColumns="False" DataKeyNames="muID" DataSourceID="sdsMagUList"
                        AllowPaging="True" AllowSorting="True" CssClass="mydatagrid" Width="99.6%" EmptyDataText="No Magnetometer Units found."
                        ShowHeaderWhenEmpty="True" OnRowUpdating="gvwMagUList_RowUpdating" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header"
                        PagerStyle-CssClass="pager" RowStyle-CssClass="rows">
                        <Columns>
                            <asp:BoundField DataField="muID" HeaderText="muID" Visible="False" />
                            <asp:BoundField DataField="muName" HeaderText="Magnetometer Unit" />
                            <asp:BoundField DataField="uTime" HeaderText="Last Updated On" SortExpression="uTime" ReadOnly="True" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />                                
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource runat="server" ID="sdsMagUList" ConnectionString='<%$ ConnectionStrings:FEDemogDB %>' DeleteCommand="DELETE FROM [dmgMagUnit] WHERE [muID] = @muID"
                        InsertCommand="INSERT INTO [dmgMagUnit] ([muName], [cTime], [uTime]) VALUES (@muName, @cTime, @uTime)" SelectCommand="SELECT [muID], [muName], [cTime], [uTime] FROM [dmgMagUnit] ORDER BY [muName]"
                        UpdateCommand="UPDATE [dmgMagUnit] SET [muName] = @muName, [cTime] = @cTime, [uTime] = @uTime WHERE [muID] = @muID">
                        <DeleteParameters>
                            <asp:Parameter Name="muID" Type="Int32"></asp:Parameter>
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="muName" Type="String"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="cTime"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="uTime"></asp:Parameter>
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="muName" Type="String"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="cTime"></asp:Parameter>
                            <asp:Parameter Name="uTime" DbType="DateTime2"></asp:Parameter>
                            <asp:Parameter Name="muID" Type="Int32"></asp:Parameter>
                        </UpdateParameters>
                    </asp:SqlDataSource>

                    <asp:Button ID="btnMagUAdd" runat="server" CssClass="red btn" Text="Add New Magnetometer Unit" Width="25%" OnClick="btnMagUAdd_Click" />
                    <asp:Button ID="btnMagUCancel" runat="server" CssClass="red btn" Text="Cancel" OnClick="btnMagUCancel_Click" />

                </div>
                <div id="divMagUAdd" runat="server" class="viewer" visible="false">

                    <asp:DetailsView ID="dvwMagUAdd" runat="server" DataKeyNames="muID" DataSourceID="sdsMagUAdd"
                        Height="50px" Width="98.6%" CellPadding="4" ForeColor="#333333" GridLines="None"
                        AutoGenerateRows="False" CaptionAlign="Top" DefaultMode="Insert" AutoGenerateInsertButton="True"
                        HeaderText="Add New Magnetometer Unit" HeaderStyle-BackColor="Silver" CssClass="borderRound" OnItemInserting="dvwMagUAdd_ItemInserting">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                        <CommandRowStyle BackColor="#FF6600" Font-Bold="True" ForeColor="White"></CommandRowStyle>
                        <EditRowStyle BackColor="#999999"></EditRowStyle>
                        <FieldHeaderStyle BackColor="LightSteelBlue" Font-Bold="True"></FieldHeaderStyle>
                        <Fields>
                            <asp:BoundField DataField="muID" HeaderText="muID" ReadOnly="True" InsertVisible="False" SortExpression="muID"></asp:BoundField>
                            <asp:TemplateField HeaderText="Magnetometer Unit" SortExpression="muName">
                                <InsertItemTemplate>
                                    <asp:TextBox runat="server" Text='<%# Bind("muName") %>' ID="txtMagUAdd"></asp:TextBox>
                                </InsertItemTemplate>
                                <ControlStyle CssClass="genTextBox"></ControlStyle>
                            </asp:TemplateField>
                            <asp:BoundField DataField="cTime" HeaderText="cTime" SortExpression="cTime" Visible="False"></asp:BoundField>
                            <asp:BoundField DataField="uTime" HeaderText="uTime" SortExpression="uTime" Visible="False"></asp:BoundField>
                        </Fields>
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"></FooterStyle>
                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <PagerStyle HorizontalAlign="Center" BackColor="#284775" ForeColor="White"></PagerStyle>
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" CssClass="TextBox"></RowStyle>
                    </asp:DetailsView>
                    <asp:SqlDataSource runat="server" ID="sdsMagUAdd" ConnectionString='<%$ ConnectionStrings:FEDemogDB %>' DeleteCommand="DELETE FROM [dmgMagUnit] WHERE [muID] = @muID"
                        InsertCommand="INSERT INTO [dmgMagUnit] ([muName], [cTime], [uTime]) VALUES (@muName, @cTime, @uTime)" SelectCommand="SELECT [muID], [muName], [cTime], [uTime] FROM [dmgMagUnit]"
                        UpdateCommand="UPDATE [dmgMagUnit] SET [muName] = @muName, [cTime] = @cTime, [uTime] = @uTime WHERE [muID] = @muID">
                        <DeleteParameters>
                            <asp:Parameter Name="muID" Type="Int32"></asp:Parameter>
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="muName" Type="String"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="cTime"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="uTime"></asp:Parameter>
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="muName" Type="String"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="cTime"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="uTime"></asp:Parameter>
                            <asp:Parameter Name="muID" Type="Int32"></asp:Parameter>
                        </UpdateParameters>
                    </asp:SqlDataSource>
                    <asp:Label ID="lblMagUAddSuccess" runat="server" Text="" CssClass="lblSuccess" Width="98%" Visible="false" />

                    <asp:Button ID="btnMagUAddCancel" runat="server" Text="Cancel" CssClass="red btn" OnClick="btnMagUAddCancel_Click" />

                </div>

            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    <div id="divLenUnits" runat="server" class="viewer" style="width:97.5%;">

        <asp:UpdateProgress ID="uprgsLU" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlLenU">
            <ProgressTemplate>
                <div id="divLUs" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center; z-index: 100;">
                    <asp:Image ID="imgLUProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="upnlLenU" runat="server">
            <ContentTemplate>

                <asp:Label ID="lblLenUHead" runat="server" Text="Length/Distance Unit" Width="99.6%" CssClass="genLabel" />
                <div id="divLenU" runat="server">
                    
                    <asp:GridView ID="gvwLenU" runat="server" AutoGenerateColumns="False" DataKeyNames="luID" DataSourceID="sdsLenUList" AllowPaging="True"
                        AllowSorting="True" CssClass="mydatagrid" Width="99.6%" EmptyDataText="No Length/Distance Units found." ShowHeaderWhenEmpty="True"
                        OnRowUpdating="gvwLenU_RowUpdating" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header"
                        PagerStyle-CssClass="pager" RowStyle-CssClass="rows">
                        <Columns>
                            <asp:BoundField DataField="luID" HeaderText="luID" Visible="False" />
                            <asp:BoundField DataField="luName" HeaderText="Length/Distance Unit" />                                
                            <asp:BoundField DataField="uTime" HeaderText="Last Updated On" SortExpression="uTime" ReadOnly="True" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />
                        </Columns>
                    </asp:GridView>

                    <asp:SqlDataSource runat="server" ID="sdsLenUList" ConnectionString='<%$ ConnectionStrings:FEDemogDB %>' SelectCommand="SELECT [luID], [luName], [cTime], [uTime] FROM [dmgLengthUnit]"
                        DeleteCommand="DELETE FROM [dmgLengthUnit] WHERE [luID] = @luID" InsertCommand="INSERT INTO [dmgLengthUnit] ([luName], [cTime], [uTime]) VALUES (@luName, @cTime, @uTime)"
                        UpdateCommand="UPDATE [dmgLengthUnit] SET [luName] = @luName, [cTime] = @cTime, [uTime] = @uTime WHERE [luID] = @luID">
                        <DeleteParameters>
                            <asp:Parameter Name="luID" Type="Int32"></asp:Parameter>
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="luName" Type="String"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="cTime"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="uTime"></asp:Parameter>
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="luName" Type="String"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="cTime"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="uTime"></asp:Parameter>
                            <asp:Parameter Name="luID" Type="Int32"></asp:Parameter>
                        </UpdateParameters>
                    </asp:SqlDataSource>

                    <asp:Button ID="btnLenUAdd" runat="server" CssClass="red btn" Text="Add New Length/Distance Unit" Width="25%" OnClick="btnLenUAdd_Click" />
                    <asp:Button ID="btnLenUCancel" runat="server" CssClass="red btn" Text="Cancel" OnClick="btnLenUCancel_Click" />

                </div>
                <div id="divLenUAdd" runat="server" class="viewer" visible="false">

                    <asp:DetailsView ID="dvwLenUAdd" runat="server" DataKeyNames="luID" DataSourceID="sdsLenUAdd"
                        Height="50px" Width="98.6%" CellPadding="4" ForeColor="#333333" GridLines="None"
                        AutoGenerateRows="False" CaptionAlign="Top" DefaultMode="Insert" AutoGenerateInsertButton="True"
                        HeaderText="Add New Length/Distance Unit" HeaderStyle-BackColor="Silver" CssClass="borderRound" OnItemInserting="dvwLenUAdd_ItemInserting">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                        <CommandRowStyle BackColor="#FF6600" Font-Bold="True" ForeColor="White"></CommandRowStyle>
                        <EditRowStyle BackColor="#999999"></EditRowStyle>
                        <FieldHeaderStyle BackColor="LightSteelBlue" Font-Bold="True"></FieldHeaderStyle>
                        <Fields>
                            <asp:BoundField DataField="luID" HeaderText="luID" ReadOnly="True" InsertVisible="False" SortExpression="luID" Visible="False">
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Length/Distance Unit" SortExpression="luName">
                                <InsertItemTemplate>
                                    <asp:TextBox runat="server" Text='<%# Bind("luName") %>' ID="txtLUAdd"></asp:TextBox>
                                </InsertItemTemplate>
                                <ControlStyle CssClass="genTextBox"></ControlStyle>
                            </asp:TemplateField>
                            <asp:BoundField DataField="cTime" HeaderText="cTime" SortExpression="cTime" Visible="False"></asp:BoundField>
                            <asp:BoundField DataField="uTime" HeaderText="uTime" SortExpression="uTime" Visible="False"></asp:BoundField>
                        </Fields>
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"></FooterStyle>
                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <PagerStyle HorizontalAlign="Center" BackColor="#284775" ForeColor="White"></PagerStyle>
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" CssClass="TextBox"></RowStyle>
                    </asp:DetailsView>
                    <asp:SqlDataSource runat="server" ID="sdsLenUAdd" ConnectionString='<%$ ConnectionStrings:FEDemogDB %>' DeleteCommand="DELETE FROM [dmgLengthUnit] WHERE [luID] = @luID"
                        InsertCommand="INSERT INTO [dmgLengthUnit] ([luName], [cTime], [uTime]) VALUES (@luName, @cTime, @uTime)" SelectCommand="SELECT [luID], [luName], [cTime], [uTime] FROM [dmgLengthUnit]"
                        UpdateCommand="UPDATE [dmgLengthUnit] SET [luName] = @luName, [cTime] = @cTime, [uTime] = @uTime WHERE [luID] = @luID">
                        <DeleteParameters>
                            <asp:Parameter Name="luID" Type="Int32"></asp:Parameter>
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="luName" Type="String"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="cTime"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="uTime"></asp:Parameter>
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="luName" Type="String"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="cTime"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="uTime"></asp:Parameter>
                            <asp:Parameter Name="luID" Type="Int32"></asp:Parameter>
                        </UpdateParameters>
                    </asp:SqlDataSource>
                    <asp:Label ID="lblLenUAddSuccess" runat="server" Text="" CssClass="lblSuccess" Width="98%" Visible="false" />

                    <asp:Button ID="btnLenUAddCancel" runat="server" Text="Cancel" CssClass="red btn" OnClick="btnLenUAddCancel_Click" />

                </div>

            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    <div id="divDatum" runat="server" class="viewer" style="width:97.5%;">

        <asp:UpdateProgress ID="uprgsDat" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlDatum">
            <ProgressTemplate>
                <div id="divDats" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center; z-index: 100;">
                    <asp:Image ID="imgDatProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="upnlDatum" runat="server">
            <ContentTemplate>

                <asp:Label ID="lblDatumHead" runat="server" Text="Datum" Width="99.6%" CssClass="genLabel" />
                <div id="divDatumList" runat="server">
                    
                    <asp:GridView ID="gvwDatumList" runat="server" AutoGenerateColumns="False" DataKeyNames="datID" DataSourceID="sdsDatumList"
                        AllowPaging="True" AllowSorting="True" CssClass="mydatagrid" Width="99.6%" EmptyDataText="No Datum found." ShowHeaderWhenEmpty="True"
                        OnRowUpdating="gvwDatumList_RowUpdating" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header"
                        PagerStyle-CssClass="pager" RowStyle-CssClass="rows">
                        <Columns>
                            <asp:BoundField DataField="datID" HeaderText="datID" Visible="False" />                            
                            <asp:BoundField DataField="datName" HeaderText="Datum" />
                        </Columns>
                    </asp:GridView>

                    <asp:SqlDataSource runat="server" ID="sdsDatumList" ConnectionString='<%$ ConnectionStrings:FEDemogDB %>' SelectCommand="SELECT [datID], [datName] FROM [dmgDatum] ORDER BY [datName]"
                        InsertCommand="INSERT INTO [dmgDatum] ([datName]) VALUES (@datName)"
                        UpdateCommand="UPDATE [dmgDatum] SET [datName] = @datName WHERE [datID] = @datID">
                        <DeleteParameters>
                            <asp:Parameter Name="datID" Type="Int32"></asp:Parameter>
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="datName" Type="String"></asp:Parameter>
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="datName" Type="String"></asp:Parameter>
                            <asp:Parameter Name="datID" Type="Int32"></asp:Parameter>
                        </UpdateParameters>
                    </asp:SqlDataSource>

                    <asp:Button ID="btnDatumAdd" runat="server" CssClass="red btn" Text="Add New Datum" Width="25%" OnClick="btnDatumAdd_Click" />
                    <asp:Button ID="btnDatumCancel" runat="server" CssClass="red btn" Text="Cancel" OnClick="btnDatumCancel_Click" />

                </div>
                <div id="divDatumAdd" runat="server" class="viewer" visible="false">

                    <asp:DetailsView ID="dvwDatum" runat="server" DataKeyNames="datID" DataSourceID="sdsDatumAdd"
                        Height="50px" Width="98.6%" CellPadding="4" ForeColor="#333333" GridLines="None"
                        AutoGenerateRows="False" CaptionAlign="Top" DefaultMode="Insert" AutoGenerateInsertButton="True"
                        HeaderText="Add New Datum" HeaderStyle-BackColor="Silver" CssClass="borderRound" OnItemInserting="dvwDatum_ItemInserting">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                        <CommandRowStyle BackColor="#FF6600" Font-Bold="True" ForeColor="White"></CommandRowStyle>
                        <EditRowStyle BackColor="#999999"></EditRowStyle>
                        <FieldHeaderStyle BackColor="LightSteelBlue" Font-Bold="True"></FieldHeaderStyle>
                        <Fields>
                            <asp:BoundField DataField="datID" HeaderText="datID" ReadOnly="True" InsertVisible="False" SortExpression="datID" Visible="False">
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Datum" SortExpression="datName">
                                <InsertItemTemplate>
                                    <asp:TextBox runat="server" Text='<%# Bind("datName") %>' ID="txtDatAdd"></asp:TextBox>
                                </InsertItemTemplate>
                                <ControlStyle CssClass="genTextBox"></ControlStyle>
                            </asp:TemplateField>
                        </Fields>
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"></FooterStyle>
                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <PagerStyle HorizontalAlign="Center" BackColor="#284775" ForeColor="White"></PagerStyle>
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" CssClass="TextBox"></RowStyle>
                    </asp:DetailsView>
                    <asp:SqlDataSource runat="server" ID="sdsDatumAdd" ConnectionString='<%$ ConnectionStrings:FEDemogDB %>' DeleteCommand="DELETE FROM [dmgDatum] WHERE [datID] = @datID"
                        InsertCommand="INSERT INTO [dmgDatum] ([datName]) VALUES (@datName)" SelectCommand="SELECT [datID], [datName] FROM [dmgDatum]"
                        UpdateCommand="UPDATE [dmgDatum] SET [datName] = @datName WHERE [datID] = @datID">
                        <DeleteParameters>
                            <asp:Parameter Name="datID" Type="Int32"></asp:Parameter>
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="datName" Type="String"></asp:Parameter>
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="datName" Type="String"></asp:Parameter>
                            <asp:Parameter Name="datID" Type="Int32"></asp:Parameter>
                        </UpdateParameters>
                    </asp:SqlDataSource>
                    <asp:Label ID="lblDatumAddSuccess" runat="server" Text="" CssClass="lblSuccess" Width="98%" Visible="false" />

                    <asp:Button ID="btnDatumAddCancel" runat="server" Text="Cancel" CssClass="red btn" OnClick="btnDatumAddCancel_Click" />

                </div>

            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    <div id="divProjection" runat="server" class="viewer" style="width:97.5%;">

        <asp:UpdateProgress ID="uprgsProj" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlProjection">
            <ProgressTemplate>
                <div id="divProjs" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center; z-index: 100;">
                    <asp:Image ID="imgProjProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="upnlProjection" runat="server">
            <ContentTemplate>

                <asp:Label ID="lblProjectionHead" runat="server" Text="Projection" Width="99.6%" CssClass="genLabel" />
                <div id="divProjectionList" runat="server">
                    
                    <asp:GridView ID="gvwProjectionList" runat="server" AutoGenerateColumns="False" DataKeyNames="projID" DataSourceID="sdsProjectionList"
                        AllowPaging="True" AllowSorting="True" CssClass="mydatagrid" Width="99.6%" EmptyDataText="No Projection found." ShowHeaderWhenEmpty="True"
                        AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header"
                        PagerStyle-CssClass="pager" RowStyle-CssClass="rows">
                        <Columns>
                            <asp:BoundField DataField="projID" Visible="False" />
                            <asp:BoundField DataField="projName" HeaderText="Datum" />
                            <asp:BoundField DataField="uTime" HeaderText="Last Updated On" SortExpression="uTime" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />                                
                        </Columns>
                    </asp:GridView>
                    
                    <asp:SqlDataSource runat="server" ID="sdsProjectionList" ConnectionString='<%$ ConnectionStrings:FEDemogDB %>' SelectCommand="SELECT projID, projName, uTime FROM dmgProjection">
                    </asp:SqlDataSource>

                    <asp:Button ID="btnProjAdd" runat="server" CssClass="red btn" Text="Add New Projection" Width="25%" OnClick="btnProjAdd_Click" />
                    <asp:Button ID="btnProjCancel" runat="server" CssClass="red btn" Text="Cancel" OnClick="btnProjCancel_Click" />

                </div>
                <div id="divProjectionAdd" runat="server" class="viewer" visible="false">

                    <asp:DetailsView ID="dvwProjection" runat="server" DataKeyNames="projID" DataSourceID="sdsProjectionAdd"
                        Height="50px" Width="98.6%" CellPadding="4" ForeColor="#333333" GridLines="None"
                        AutoGenerateRows="False" CaptionAlign="Top" DefaultMode="Insert" AutoGenerateInsertButton="True"
                        HeaderText="Add New Projection" HeaderStyle-BackColor="Silver" CssClass="borderRound" OnItemInserting="dvwProjection_ItemInserting">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                        <CommandRowStyle BackColor="#FF6600" Font-Bold="True" ForeColor="White"></CommandRowStyle>
                        <EditRowStyle BackColor="#999999"></EditRowStyle>
                        <FieldHeaderStyle BackColor="LightSteelBlue" Font-Bold="True"></FieldHeaderStyle>
                        <Fields>
                            <asp:BoundField DataField="projID" HeaderText="projID" ReadOnly="True" InsertVisible="False" SortExpression="projID" Visible="False">
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Projection" SortExpression="projName">
                                <InsertItemTemplate>
                                    <asp:TextBox runat="server" Text='<%# Bind("projName") %>' ID="txtProjAdd"></asp:TextBox>
                                </InsertItemTemplate>
                                <ControlStyle CssClass="genTextBox"></ControlStyle>
                            </asp:TemplateField>
                            <asp:BoundField DataField="cTime" HeaderText="cTime" SortExpression="cTime" Visible="False"></asp:BoundField>
                            <asp:BoundField DataField="uTime" HeaderText="uTime" SortExpression="uTime" Visible="False"></asp:BoundField>
                        </Fields>
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"></FooterStyle>
                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <PagerStyle HorizontalAlign="Center" BackColor="#284775" ForeColor="White"></PagerStyle>
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" CssClass="TextBox"></RowStyle>
                    </asp:DetailsView>
                    <asp:SqlDataSource runat="server" ID="sdsProjectionAdd" ConnectionString='<%$ ConnectionStrings:FEDemogDB %>' DeleteCommand="DELETE FROM [dmgProjection] WHERE [projID] = @projID"
                        InsertCommand="INSERT INTO [dmgProjection] ([projName], [cTime], [uTime]) VALUES (@projName, @cTime, @uTime)" SelectCommand="SELECT [projID], [projName], [cTime], [uTime] FROM [dmgProjection]"
                        UpdateCommand="UPDATE [dmgProjection] SET [projName] = @projName, [cTime] = @cTime, [uTime] = @uTime WHERE [projID] = @projID">
                        <DeleteParameters>
                            <asp:Parameter Name="projID" Type="Int32"></asp:Parameter>
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="projName" Type="String"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="cTime"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="uTime"></asp:Parameter>
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="projName" Type="String"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="cTime"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="uTime"></asp:Parameter>
                            <asp:Parameter Name="projID" Type="Int32"></asp:Parameter>
                        </UpdateParameters>
                    </asp:SqlDataSource>
                    <asp:Label ID="lblProjAddSuccess" runat="server" Text="" CssClass="lblSuccess" Width="98%" Visible="false" />

                    <asp:Button ID="btnProjAddCancel" runat="server" Text="Cancel" CssClass="red btn" OnClick="btnProjAddCancel_Click" />

                </div>

            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    <div id="divMagModel" runat="server" class="viewer" style="width:97.5%;">

        <asp:UpdateProgress ID="uprgsMMdl" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlMagModel">
            <ProgressTemplate>
                <div id="divMMdls" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center; z-index: 100;">
                    <asp:Image ID="imgMMdlProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="upnlMagModel" runat="server">
            <ContentTemplate>

                <asp:Label ID="lblMagModelHead" runat="server" Text="Magnetic Model" Width="99.6%" CssClass="genLabel" />
                <div id="divMagModelList" runat="server">
                    
                    <asp:GridView ID="gvwMagModel" runat="server" AutoGenerateColumns="False" DataKeyNames="mmdlID" DataSourceID="sdsMagModelList"
                        AllowPaging="True" AllowSorting="True" CssClass="mydatagrid" Width="99.6%" EmptyDataText="No Magnetic Model found."
                        ShowHeaderWhenEmpty="True" OnRowUpdating="gvwMagModel_RowUpdating" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                        PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                        RowStyle-CssClass="rows">
                        <Columns>
                            <asp:BoundField DataField="mmdlID" HeaderText="mmdlID" Visible="False" />                            
                            <asp:BoundField DataField="mmdlName" HeaderText="Magnetic Model" />
                            <asp:BoundField DataField="uTime" HeaderText="Last Updated On" SortExpression="uTime" Visible="False" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="true" />
                        </Columns>
                    </asp:GridView>

                    <asp:SqlDataSource runat="server" ID="sdsMagModelList" ConnectionString='<%$ ConnectionStrings:FEDemogDB %>' SelectCommand="SELECT [mmdlID], [mmdlName], [cTime], [uTime] FROM [dmgMagModel] ORDER BY [mmdlName]"
                        DeleteCommand="DELETE FROM [dmgMagModel] WHERE [mmdlID] = @mmdlID" InsertCommand="INSERT INTO [dmgMagModel] ([mmdlName], [cTime], [uTime]) VALUES (@mmdlName, @cTime, @uTime)"
                        UpdateCommand="UPDATE [dmgMagModel] SET [mmdlName] = @mmdlName, [cTime] = @cTime, [uTime] = @uTime WHERE [mmdlID] = @mmdlID">
                        <DeleteParameters>
                            <asp:Parameter Name="mmdlID" Type="Int32"></asp:Parameter>
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="mmdlName" Type="String"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="cTime"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="uTime"></asp:Parameter>
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="mmdlName" Type="String"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="cTime"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="uTime"></asp:Parameter>
                            <asp:Parameter Name="mmdlID" Type="Int32"></asp:Parameter>
                        </UpdateParameters>
                    </asp:SqlDataSource>

                    <asp:Button ID="btnMMAdd" runat="server" CssClass="red btn" Text="Add New Magnetic Model" Width="25%" OnClick="btnMMAdd_Click" />
                    <asp:Button ID="btnMMCancel" runat="server" CssClass="red btn" Text="Cancel" OnClick="btnMMCancel_Click" />

                </div>
                <div id="divMagModelAdd" runat="server" class="viewer" style="width: 99.6%;" visible="false">

                    <asp:DetailsView ID="dvwMagmOdelAdd" runat="server" DataKeyNames="mmdlID" DataSourceID="sdsMagModelAdd"
                        Height="50px" Width="98.6%" CellPadding="4" ForeColor="#333333" GridLines="None"
                        AutoGenerateRows="False" CaptionAlign="Top" DefaultMode="Insert" AutoGenerateInsertButton="True"
                        HeaderText="Add New Magnetic Model" HeaderStyle-BackColor="Silver" CssClass="borderRound" OnItemInserting="dvwMagmOdelAdd_ItemInserting">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                        <CommandRowStyle BackColor="#FF6600" Font-Bold="True" ForeColor="White"></CommandRowStyle>
                        <EditRowStyle BackColor="#999999"></EditRowStyle>
                        <FieldHeaderStyle BackColor="LightSteelBlue" Font-Bold="True"></FieldHeaderStyle>
                        <Fields>
                            <asp:BoundField DataField="mmdlID" HeaderText="mmdlID" ReadOnly="True" InsertVisible="False" SortExpression="mmdlID" Visible="False">
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Magnetic Model" SortExpression="mmdlName">
                                <InsertItemTemplate>
                                    <asp:TextBox runat="server" Text='<%# Bind("mmdlName") %>' ID="txtMMDL"></asp:TextBox>
                                </InsertItemTemplate>
                                <ControlStyle CssClass="genTextBox"></ControlStyle>
                            </asp:TemplateField>
                            <asp:BoundField DataField="cTime" HeaderText="cTime" SortExpression="cTime" Visible="False"></asp:BoundField>
                            <asp:BoundField DataField="uTime" HeaderText="uTime" SortExpression="uTime" Visible="False"></asp:BoundField>
                        </Fields>
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"></FooterStyle>
                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <PagerStyle HorizontalAlign="Center" BackColor="#284775" ForeColor="White"></PagerStyle>
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" CssClass="TextBox"></RowStyle>
                    </asp:DetailsView>
                    <asp:SqlDataSource runat="server" ID="sdsMagModelAdd" ConnectionString='<%$ ConnectionStrings:FEDemogDB %>' DeleteCommand="DELETE FROM [dmgMagModel] WHERE [mmdlID] = @mmdlID"
                        InsertCommand="INSERT INTO [dmgMagModel] ([mmdlName], [cTime], [uTime]) VALUES (@mmdlName, @cTime, @uTime)" SelectCommand="SELECT [mmdlID], [mmdlName], [cTime], [uTime] FROM [dmgMagModel]"
                        UpdateCommand="UPDATE [dmgMagModel] SET [mmdlName] = @mmdlName, [cTime] = @cTime, [uTime] = @uTime WHERE [mmdlID] = @mmdlID">
                        <DeleteParameters>
                            <asp:Parameter Name="mmdlID" Type="Int32"></asp:Parameter>
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="mmdlName" Type="String"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="cTime"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="uTime"></asp:Parameter>
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="mmdlName" Type="String"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="cTime"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="uTime"></asp:Parameter>
                            <asp:Parameter Name="mmdlID" Type="Int32"></asp:Parameter>
                        </UpdateParameters>
                    </asp:SqlDataSource>
                    <asp:Label ID="lblMMDLAddSuccess" runat="server" Text="" CssClass="lblSuccess" Width="98%" Visible="false" />

                    <asp:Button ID="btnMMDLCancel" runat="server" Text="Cancel" CssClass="red btn" OnClick="btnMMDLCancel_Click" />

                </div>

            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    <div id="divConvergence" runat="server" class="viewer" style="width:97.5%;">

        <asp:UpdateProgress ID="uprgsGC" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlGC">
            <ProgressTemplate>
                <div id="divGCs" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center; z-index: 100;">
                    <asp:Image ID="imgGCProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="upnlGC" runat="server">
            <ContentTemplate>

                <asp:Label ID="lblGCHead" runat="server" Text="Grid Convergence" Width="99.6%" CssClass="genLabel" />
                <div id="divGC" runat="server">
                    
                    <asp:GridView ID="gvwGCList" runat="server" AutoGenerateColumns="False" DataKeyNames="jcID" DataSourceID="sdsGCList"
                        AllowPaging="True" AllowSorting="True" CssClass="mydatagrid" Width="99.6%" EmptyDataText="No Grid Convergence values found."
                        ShowHeaderWhenEmpty="True" OnRowUpdating="gvwGCList_RowUpdating" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header"
                        PagerStyle-CssClass="pager" RowStyle-CssClass="rows">
                        <Columns>
                            <asp:BoundField DataField="jcID" HeaderText="jcID" Visible="False" />                            
                            <asp:BoundField DataField="jcName" HeaderText="Grid Convergence" />                                
                            <asp:BoundField DataField="uTime" HeaderText="Last Updated On" SortExpression="uTime" ReadOnly="True" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" />                                
                        </Columns>
                    </asp:GridView>

                    <asp:SqlDataSource runat="server" ID="sdsGCList" ConnectionString='<%$ ConnectionStrings:FEDemogDB %>' SelectCommand="SELECT [jcID], [jcName], [cTime], [uTime] FROM [dmgJobConvergence] ORDER BY [jcName]"
                        DeleteCommand="DELETE FROM [dmgJobConvergence] WHERE [jcID] = @jcID" InsertCommand="INSERT INTO [dmgJobConvergence] ([jcName], [cTime], [uTime]) VALUES (@jcName, @cTime, @uTime)"
                        UpdateCommand="UPDATE [dmgJobConvergence] SET [jcName] = @jcName, [cTime] = @cTime, [uTime] = @uTime WHERE [jcID] = @jcID">
                        <DeleteParameters>
                            <asp:Parameter Name="jcID" Type="Int32"></asp:Parameter>
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="jcName" Type="String"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="cTime"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="uTime"></asp:Parameter>
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="jcName" Type="String"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="cTime"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="uTime"></asp:Parameter>
                            <asp:Parameter Name="jcID" Type="Int32"></asp:Parameter>
                        </UpdateParameters>
                    </asp:SqlDataSource>

                    <asp:Button ID="btnGCAdd" runat="server" CssClass="red btn" Text="Add New Grid Convergence" Width="25%" OnClick="btnGCAdd_Click" />
                    <asp:Button ID="btnGCCancel" runat="server" CssClass="red btn" Text="Cancel" OnClick="btnGCCancel_Click" />

                </div>
                <div id="divGCAdd" runat="server" class="viewer" style="width: 99.6%;" visible="false">

                    <asp:DetailsView ID="dvwGCAdd" runat="server" DataKeyNames="jcID" DataSourceID="sdsGCAdd"
                        Height="50px" Width="98.6%" CellPadding="4" ForeColor="#333333" GridLines="None"
                        AutoGenerateRows="False" CaptionAlign="Top" DefaultMode="Insert" AutoGenerateInsertButton="True"
                        HeaderText="Add New Grid Convergence" HeaderStyle-BackColor="Silver" CssClass="borderRound" OnItemInserting="dvwGCAdd_ItemInserting">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                        <CommandRowStyle BackColor="#FF6600" Font-Bold="True" ForeColor="White"></CommandRowStyle>
                        <EditRowStyle BackColor="#999999"></EditRowStyle>
                        <FieldHeaderStyle BackColor="LightSteelBlue" Font-Bold="True"></FieldHeaderStyle>
                        <Fields>
                            <asp:BoundField DataField="jcID" HeaderText="jcID" ReadOnly="True" InsertVisible="False" SortExpression="jcID" Visible="False">
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Grid Convergence" SortExpression="jcName">
                                <InsertItemTemplate>
                                    <asp:TextBox runat="server" Text='<%# Bind("jcName") %>' ID="txtGCAdd"></asp:TextBox>
                                </InsertItemTemplate>
                                <ControlStyle CssClass="genTextBox"></ControlStyle>
                            </asp:TemplateField>
                            <asp:BoundField DataField="cTime" HeaderText="cTime" SortExpression="cTime" Visible="False"></asp:BoundField>
                            <asp:BoundField DataField="uTime" HeaderText="uTime" SortExpression="uTime" Visible="False"></asp:BoundField>
                        </Fields>
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"></FooterStyle>
                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <PagerStyle HorizontalAlign="Center" BackColor="#284775" ForeColor="White"></PagerStyle>
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" CssClass="TextBox"></RowStyle>
                    </asp:DetailsView>
                    <asp:SqlDataSource runat="server" ID="sdsGCAdd" ConnectionString='<%$ ConnectionStrings:FEDemogDB %>' DeleteCommand="DELETE FROM [dmgJobConvergence] WHERE [jcID] = @jcID"
                        InsertCommand="INSERT INTO [dmgJobConvergence] ([jcName], [cTime], [uTime]) VALUES (@jcName, @cTime, @uTime)" SelectCommand="SELECT [jcID], [jcName], [cTime], [uTime] FROM [dmgJobConvergence]"
                        UpdateCommand="UPDATE [dmgJobConvergence] SET [jcName] = @jcName, [cTime] = @cTime, [uTime] = @uTime WHERE [jcID] = @jcID">
                        <DeleteParameters>
                            <asp:Parameter Name="jcID" Type="Int32"></asp:Parameter>
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="jcName" Type="String"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="cTime"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="uTime"></asp:Parameter>
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="jcName" Type="String"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="cTime"></asp:Parameter>
                            <asp:Parameter DbType="DateTime2" Name="uTime"></asp:Parameter>
                            <asp:Parameter Name="jcID" Type="Int32"></asp:Parameter>
                        </UpdateParameters>
                    </asp:SqlDataSource>
                    <asp:Label ID="lblGCAddSuccess" runat="server" Text="" CssClass="lblSuccess" Width="98%" Visible="false" />

                    <asp:Button ID="btnGCAddCancel" runat="server" Text="Cancel" CssClass="red btn" OnClick="btnGCAddCancel_Click" />

                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
    </div>
</div>