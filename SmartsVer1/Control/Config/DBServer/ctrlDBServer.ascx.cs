﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI.WebControls;
using CLR = System.Drawing.Color;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using DB = SmartsVer1.Helpers.DatabaseHelpers.DBConfigs;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;

namespace SmartsVer1.Control.Config.DBServer
{
    public partial class ctrlDBServer : System.Web.UI.UserControl
    {
        String LoginName, username, domain, GetClientDBString;
        Int32 ClientID = -99;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
                //Datasources for page controls
                System.Data.DataTable dbTable = DB.getDatabaseListTable();
                this.gvwDBList.DataSource = dbTable;
                this.gvwDBList.DataBind();
                System.Data.DataTable cdbTable = DB.getClientConnectedDatabaseListTable();
                this.gvwConDB.DataSource = DB.getClientConnectedDatabaseListTable();
                this.gvwConDB.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwDBList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable dbTable = DB.getDatabaseListTable();
                this.gvwDBList.PageIndex = e.NewPageIndex;
                this.gvwDBList.DataSource = dbTable;
                this.gvwDBList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwConDB_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable cdbTable = DB.getClientConnectedDatabaseListTable();
                this.gvwConDB.PageIndex = e.NewPageIndex;
                this.gvwConDB.DataSource = cdbTable;
                this.gvwConDB.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwDBList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnDBCancel.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwConDB_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnConDBCancel.Visible = true;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnDBCancel_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable dbTable = DB.getDatabaseListTable();
                this.gvwDBList.DataSource = dbTable;
                this.gvwDBList.DataBind();
                this.btnDBCancel.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnConDBCancel_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataTable cdbTable = DB.getClientConnectedDatabaseListTable();
                this.gvwConDB.DataSource = cdbTable;
                this.gvwConDB.DataBind();
                this.btnConDBCancel.Visible = false;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}