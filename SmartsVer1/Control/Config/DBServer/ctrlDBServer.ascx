﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlDBServer.ascx.cs" Inherits="SmartsVer1.Control.Config.DBServer.ctrlDBServer" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript -->
<script src="../../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>

<!--- Page local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }

    .rotate {
        -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
        -moz-transform: rotate(90deg); /* Firefox */
        -ms-transform: rotate(90deg); /* IE 9 */
        transform: rotate(90deg); /* Standard syntax */
    }

    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper[data-stepper-steps="8"] li {
            width: 12%;
        }

        ol.stepper[data-stepper-steps="9"] li {
            width: 11%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }    
</style>
<!--- Page local script --->
<script type="text/javascript">
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("rotate");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").addClass("rotate");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("rotate");
        });
    });
</script>

<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">        
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>Configuration</li>
                <li><a href="../../../Config/DBServer/fesDBServer_2A.aspx"> Client DB Servers</a></li>
            </ol>
        </div>
    </div>    
</div>

<!-- Page Content -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div id="divDBList" runat="server" class="content-wrapper viewer divShadow" style="width: 100%;">
            <asp:Table ID="tblDBList" runat="server" Width="100%">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#c0c0c0" ColumnSpan="2">
                        <asp:Label ID="lblDBListHeading" runat="server" CssClass="genLabel" ForeColor="White" Text="Current Available Databases" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2">
                        <asp:GridView ID="gvwDBList" runat="server" CssClass="mydatagrid" EmptyDataText="No Databases configured." AllowPaging="True" Width="99.6%" AutoGenerateColumns="False" 
                            ShowHeaderWhenEmpty="True" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" 
                            RowStyle-CssClass="rows" DataKeyNames="cmID" OnPageIndexChanging="gvwDBList_PageIndexChanging" OnSelectedIndexChanged="gvwDBList_SelectedIndexChanged">
                            <Columns>
                                <asp:BoundField DataField="cmID" Visible="false" />
                                <asp:BoundField DataField="cmName" HeaderText="Database Name" />
                                <asp:BoundField DataField="dbSize" HeaderText="Database Size (MB)" />
                                <asp:BoundField DataField="uTime" HeaderText="Last Updated" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                            </Columns>
                        </asp:GridView>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" Width="25%"></asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" Width="75%">
                        <asp:Button ID="btnDBCancel" runat="server" CssClass="btn red" Width="45%" Text="Reset Grid" Visible="false" OnClick="btnDBCancel_Click" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </div>
        <div id="divConDB" runat="server" class="content-wrapper viewer divShadow" style="width: 100%;">
        <asp:Table ID="tblConDB" runat="server" Width="100%">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#c0c0c0" ColumnSpan="2">
                    <asp:Label ID="lblConDBHeading" runat="server" CssClass="genLabel" ForeColor="White" Text="Current Client-Connected Databases" />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2">
                    <asp:GridView ID="gvwConDB" runat="server" CssClass="mydatagrid" EmptyDataText="No Databases configured." AllowPaging="True" Width="99.6%" AutoGenerateColumns="False" 
                        ShowHeaderWhenEmpty="True" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" 
                        RowStyle-CssClass="rows" DataKeyNames="cdbID" OnPageIndexChanging="gvwConDB_PageIndexChanging" OnSelectedIndexChanged="gvwConDB_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField DataField="cdbID" Visible="false" />
                            <asp:BoundField DataField="clntName" HeaderText="Client Name" />
                            <asp:BoundField DataField="clntRealm" HeaderText="Client Realm" />
                            <asp:BoundField DataField="cmName" HeaderText="Database Name" />
                            <asp:BoundField DataField="dbSize" HeaderText="Database Size (MB)" />
                            <asp:BoundField DataField="uTime" HeaderText="Last Updated" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                        </Columns>
                    </asp:GridView>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Middle" Width="25%"></asp:TableCell>
                <asp:TableCell VerticalAlign="Middle" Width="75%">
                    <asp:Button ID="btnConDBCancel" runat="server" CssClass="btn red" Width="45%" Text="Reset Grid" Visible="false" OnClick="btnConDBCancel_Click" />
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
    </div>
</div>