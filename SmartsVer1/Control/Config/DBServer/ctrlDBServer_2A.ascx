﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlDBServer_2A.ascx.cs" Inherits="SmartsVer1.Control.Config.DBServer.ctrlDBServer_2A" %>
<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
<!--  Stylesheet -->
<link href="../../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript -->
<script src="../../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<!-- Javascript for Footer -->
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>
<!--- Page local style --->
<style type="text/css" style="background-color: #d9534f; color: #FFFFFF">
    .rtAligned {
        text-align: right;
    }

    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    .sqcLabel {
        min-width: 135px;
        text-align: right;
    }

    .tiLabel {
        min-width: 180px;
        text-align: right;
    }

    .bhaLabel {
        min-width: 170px;
        text-align: right;
    }

    .padLabel {
        min-width: 145px;
        text-align: right;
    }

    .wellLabel {
        min-width: 150px;
        text-align: right;
    }

    .rfLabel {
        min-width: 110px;
        text-align: right;
    }

    .planLabel {
        min-width: 140px;
        text-align:right;
    }

    .padRight {
        padding-right: 25px;
    }

    .btRadio {
        margin-right: 1%;
    }

    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper[data-stepper-steps="8"] li {
            width: 12%;
        }

        ol.stepper[data-stepper-steps="9"] li {
            width: 11%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }    
</style>
<!--- Page Menu --->
<asp:UpdateProgress ID="uprgrsConfig" runat="server" DisplayAfter="0" DynamicLayout="true" AssociatedUpdatePanelID="upnlConfig">
    <ProgressTemplate>
        <div id="divSQCProcessing" runat="server" class="updateProgress">
            <asp:Image ID="imgSQCProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<asp:UpdatePanel runat="server" ID="upnlConfig">
    <ContentTemplate>
        <div class="row bg-black">
            <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom: 2px; padding-left: 2px; padding-top: 2px">
                <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
            </div>
            <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
                <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
            </div>
        </div>
        <div class="row bg-black">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
                <asp:LoginView ID='LoginView1' runat='server'>
                    <AnonymousTemplate>
                        <li style="vertical-align: middle; display: table-cell;">
                            <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                                ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                        </li>
                    </AnonymousTemplate>
                    <LoggedInTemplate>
                        <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">(
                            <asp:LoginName ID="LoginName1" runat="server" />
                            )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                        </li>
                    </LoggedInTemplate>
                </asp:LoginView>
            </div>
        </div>
        <!--- Page Breadcrumb --->
        <div class="row bg-white">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box-tools pull-right">
                    <ol class="breadcrumb">
                        <li><a href="../../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                        <li>Configuration</li>
                        <li><a href="../../../Config/DBServer/fesDBServer_2A.aspx"> Client DB Servers</a></li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- Page Content -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row bg-white">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul class="nav nav-pills nav-justified">
                        <li class="active"><a data-toggle="pill" href="#DBList">Client Databases</a></li>
                        <li><a data-toggle="pill" href="#ConnList">Database Connections</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="DBList" class="tab-pane fade in active">
                            <div id="divDBList" runat="server">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h5 class="panel-title"><span>Client Databases List</span></h5>
                                            </div>
                                            <div class="panel-body">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="input-group">
                                                        <asp:LinkButton ID="btnDBCancel" runat="server" CssClass="btn btn-warning text-center" OnClick="btnDBCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <asp:GridView ID="gvwDBList" runat="server" CssClass="mydatagrid" EmptyDataText="No Databases configured." AllowPaging="True"
                                                        AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15"
                                                        HeaderStyle-CssClass="header"
                                                        PagerStyle-CssClass="pager" RowStyle-CssClass="rows" DataKeyNames="cmID" OnPageIndexChanging="gvwDBList_PageIndexChanging"
                                                        OnSelectedIndexChanged="gvwDBList_SelectedIndexChanged">
                                                        <Columns>
                                                            <asp:BoundField DataField="cmID" Visible="false" />
                                                            <asp:BoundField DataField="cmName" HeaderText="Database Name" />
                                                            <asp:BoundField DataField="dbSize" HeaderText="Database Size (MB)" />
                                                            <asp:BoundField DataField="uTime" HeaderText="Last Updated" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div id="ConnList" class="tab-pane fade">
                            <div id="divConnlist" runat="server">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h5 class="panel-title"><span>Current Client-Connected Databases</span></h5>
                                            </div>
                                            <div class="panel-body">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="input-group">
                                                        <asp:LinkButton ID="btnConDBCancel" runat="server" CssClass="btn btn-warning text-center" OnClick="btnConDBCancel_Click"><i class="fa fa-refresh"> Reset</i></asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <asp:GridView ID="gvwConDB" runat="server" CssClass="mydatagrid" EmptyDataText="No Databases configured." AllowPaging="True"
                                                        AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15"
                                                        HeaderStyle-CssClass="header"
                                                        PagerStyle-CssClass="pager" RowStyle-CssClass="rows" DataKeyNames="cdbID" OnPageIndexChanging="gvwConDB_PageIndexChanging"
                                                        OnSelectedIndexChanged="gvwConDB_SelectedIndexChanged">
                                                        <Columns>
                                                            <asp:BoundField DataField="cdbID" Visible="false" />
                                                            <asp:BoundField DataField="clntName" HeaderText="Client Name" />
                                                            <asp:BoundField DataField="clntRealm" HeaderText="Client Realm" />
                                                            <asp:BoundField DataField="cmName" HeaderText="Database Name" />
                                                            <asp:BoundField DataField="dbSize" HeaderText="Database Size (MB)" />
                                                            <asp:BoundField DataField="uTime" HeaderText="Last Updated" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}" ReadOnly="True" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
