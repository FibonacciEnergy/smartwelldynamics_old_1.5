﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Net.Mail;
using System.Web.UI;
using System.Data.SqlTypes;
using System.Web.Security;
using System.Web.UI.WebControls;
using SQ = Microsoft.SqlServer.Types;
using CLR = System.Drawing.Color;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;

namespace SmartsVer1.Control.Config.NonMagCalc
{
    public partial class ctrlNMCalc : System.Web.UI.UserControl
    {
        String LoginName, username, domain, GetClientDBString;
        Int32 ClientID = -99;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();
                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);
                username = addr.User;
                domain = addr.Host;
                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
                //DataSources for GridView lists
                DataTable tableMagLim = DMG.getBHAMagLimitTable();
                DataTable tableBGA = DMG.getBGATable();
                DataTable tableBHAAna = DMG.getBHAnalysisTable();
                DataTable tableDCP = DMG.getDCPoleStrengthTable();
                DataTable tableMTR = DMG.getMtrPoleStrengthTable();
                DataTable tableLCR = DMG.getLCRefTable();

                //Assigning Datasources
                this.gvwMagLimList.DataSource = tableMagLim;
                this.gvwMagLimList.DataBind();
                this.gvwBGAList.DataSource = tableBGA;
                this.gvwBGAList.DataBind();
                this.gvwBHAAnaList.DataSource = tableBHAAna;
                this.gvwBHAAnaList.DataBind();
                this.gvwDCPSList.DataSource = tableDCP;
                this.gvwDCPSList.DataBind();
                this.gvwMPSList.DataSource = tableMTR;
                this.gvwMPSList.DataBind();
                this.gvwSRefList.DataSource = tableLCR;
                this.gvwSRefList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwMagLimList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable mlmTable = DMG.getBHAMagLimitTable();
                this.gvwMagLimList.PageIndex = e.NewPageIndex;
                this.gvwMagLimList.DataSource = mlmTable;
                this.gvwMagLimList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnMagLimAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                this.divMagLimList.Visible = false;
                this.divMagLimAdd.Visible = true;
                btnAddMagLimCancel_Click(null, null);
                this.btnMagLimDone.Visible = true;

                this.txtAddMagLim.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddMagLim_Click(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnAddMagLimCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtAddMagLim.Text = "";
                this.lblMagLimSuccess.Text = "";
                this.lblMagLimSuccess.CssClass = "lblSuccess";
                this.lblMagLimSuccess.Visible = false;

                this.txtAddMagLim.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnMagLimDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnAddMagLimCancel_Click(null, null);
                this.divMagLimAdd.Visible = false;
                this.btnMagLimDone.Visible = false;
                DataTable mlmTable = DMG.getBHAMagLimitTable();
                this.gvwMagLimList.DataSource = mlmTable;
                this.gvwMagLimList.DataBind();
                this.divMagLimList.Visible = true;

                this.gvwMagLimList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwBGAList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable bgaTable = DMG.getBGATable();
                this.gvwBGAList.PageIndex = e.NewPageIndex;
                this.gvwBGAList.DataSource = bgaTable;
                this.gvwBGAList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnBGAAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.divBGAList.Visible = false;
                this.divBGAAdd.Visible = true;
                btnBGAAddCancel_Click(null, null);
                this.btnBGADone.Visible = true;

                this.txtAddBGA.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnBGAAddNew_Click(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnBGAAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtAddBGA.Text = "";
                this.lblBGASuccess.Text = "";
                this.lblBGASuccess.CssClass = "lblSuccess";
                this.lblBGASuccess.Visible = false;

                this.txtAddBGA.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnBGADone_Click(object sender, EventArgs e)
        {
            try
            {
                btnBGAAddCancel_Click(null, null);
                this.divBGAAdd.Visible = false;
                this.btnBGADone.Visible = false;
                DataTable bgaTable = DMG.getBGATable();
                this.gvwBGAList.DataSource = bgaTable;
                this.gvwBGAList.DataBind();
                this.divBGAList.Visible = true;

                this.gvwBGAList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwBHAAnaList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable bhaTable = DMG.getBHAnalysisTable();
                this.gvwBHAAnaList.PageIndex = e.NewPageIndex;
                this.gvwBHAAnaList.DataSource = bhaTable;
                this.gvwBHAAnaList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnBHAAnaAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.divBHAAnaList.Visible = false;
                this.divBHAAnaAdd.Visible = true;
                btnBHAAnaAddCancel_Click(null, null);
                this.btnBHAAnaDone.Visible = true;

                this.txtBHAAnaAdd.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnBHAAnaAddNew_Click(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnBHAAnaAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtBHAAnaAdd.Text = "";
                this.lblBHAAnaSuccess.Text = "";
                this.lblBHAAnaSuccess.CssClass = "lblSuccess";
                this.lblBHAAnaSuccess.Visible = false;

                this.txtBHAAnaAdd.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void btnBHAAnaDone_Click(object sender, EventArgs e)
        {
            try
            {
                btnBHAAnaAddCancel_Click(null, null);
                this.divBHAAnaAdd.Visible = false;
                this.btnBHAAnaDone.Visible = false;
                DataTable bhaTable = DMG.getBHAnalysisTable();
                this.gvwBHAAnaList.DataSource = bhaTable;
                this.gvwBHAAnaList.DataBind();
                this.divBHAAnaList.Visible = true;

                this.gvwBHAAnaList.Focus();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwDCPSList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable dcpsTable = DMG.getDCPoleStrengthTable();
                this.gvwDCPSList.PageIndex = e.NewPageIndex;
                this.gvwDCPSList.DataSource = dcpsTable;
                this.gvwDCPSList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwMPSList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable mpsTable = DMG.getMtrPoleStrengthTable();
                this.gvwMPSList.PageIndex = e.NewPageIndex;
                this.gvwMPSList.DataSource = mpsTable;
                this.gvwMPSList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwSRefList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable refTable = DMG.getLCRefTable();
                this.gvwSRefList.PageIndex = e.NewPageIndex;
                this.gvwSRefList.DataSource = refTable;
                this.gvwSRefList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}