﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlNMCalc.ascx.cs" Inherits="SmartsVer1.Control.Config.NonMagCalc.ctrlNMCalc" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

<link href="../../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- Javascript -->
<script src="../../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>

<!--- Page local style --->
<style type="text/css">
    .rtAligned {
        text-align: right;
    }

    .rotate {
        -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
        -moz-transform: rotate(90deg); /* Firefox */
        -ms-transform: rotate(90deg); /* IE 9 */
        transform: rotate(90deg); /* Standard syntax */
    }

    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper[data-stepper-steps="8"] li {
            width: 12%;
        }

        ol.stepper[data-stepper-steps="9"] li {
            width: 11%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }    
</style>
<!--- Page local script --->
<script type="text/javascript">
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("rotate");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").addClass("rotate");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("rotate");
        });
    });
</script>

<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">        
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>SMART Utilities</li>
                <li><a href="../../../Config/NonMagCalc/NonMagCalc_2A.aspx"> Non-Mag Space Calculator Service</a></li>
            </ol>
        </div>
    </div>    
</div>

<!-- Page Content -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div id="divMagLimDialog" runat="server" class="viewer" style="width: 98.5%;">
            <asp:UpdateProgress ID="upgrsMagLim" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlMagLim">
                <ProgressTemplate>
                    <div id="divML" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                        z-index: 100;">
                        <asp:Image ID="imgMLProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="upnlMagLim" runat="server">
                <ContentTemplate>
                    <div id="divMagLimList" runat="server" class="viewer divShadow" style="width: 99.1%;">
                        <asp:Label ID="lblMagLimHead" runat="server" CssClass="genLabel" Width="99.6%" Text="BHA Mag. Limit" />
                        <asp:GridView ID="gvwMagLimList" runat="server" CssClass="mydatagrid" Width="99.6%" AutoGenerateColumns="False"
                            AllowPaging="True" AllowSorting="True" DataKeyNames="mlID" OnPageIndexChanging="gvwMagLimList_PageIndexChanging"
                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                            RowStyle-CssClass="rows">
                            <Columns>
                                <asp:BoundField DataField="mlID" HeaderText="mlID" Visible="False" />
                                <asp:BoundField DataField="mlValue" HeaderText="BHA Mag. Limit"></asp:BoundField>
                            </Columns>
                        </asp:GridView>

                        <asp:Button ID="btnMagLimAddNew" runat="server" CssClass="red btn" Text="Add New Mag. Limit" Width="25%" OnClick="btnMagLimAddNew_Click" />

                    </div>
                    <div id="divMagLimAdd" runat="server" class="viewer divShadow" style="width: 99.1%;" visible="false">
                        <asp:Label ID="lblAddMagLimHead" runat="server" CssClass="genLabel" Width="99.6%" Text="Add New BHA Mag. Limit" />
                        <asp:Label ID="lblAddMagLim" runat="server" CssClass="txtLabel" Text="Major Geographic Region" Width="25%" />
                        <asp:TextBox ID="txtAddMagLim" runat="server" CssClass="genTextBox" Text="" Width="65%" CausesValidation="True" />
                        <asp:RequiredFieldValidator ID="rfvAddMagLim" runat="server" ControlToValidate="txtAddMagLim" Display="Dynamic" ErrorMessage="*"
                            ForeColor="Maroon" SetFocusOnError="True" />
                        <br />
                        <asp:Label ID="lblMagLimSuccess" runat="server" CssClass="lblSuccess" Visible="False" Width="99.6%" />
                        <br />
                        <asp:Button ID="btnAddMagLim" runat="server" CssClass="red btn" Width="25%" Text="Add Mag. Limit" OnClick="btnAddMagLim_Click" />
                        <asp:Button ID="btnAddMagLimCancel" runat="server" CssClass="red btn" Width="15%" Text="Clear Values" OnClick="btnAddMagLimCancel_Click"
                            CausesValidation="False" />

                    </div>
                    <asp:Button ID="btnMagLimDone" runat="server" CssClass="red btn" Width="20%" Text="Done" OnClick="btnMagLimDone_Click" CausesValidation="False"
                        Visible="False" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div id="divBGADialog" runat="server" class="viewer" style="width: 98.5%;">
            <asp:UpdateProgress ID="uprgrsBGA" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlBGA">
                <ProgressTemplate>
                    <div id="divBGA" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                        z-index: 100;">
                        <asp:Image ID="imgBGAProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="upnlBGA" runat="server">
                <ContentTemplate>
                    <div id="divBGAList" runat="server" class="viewer divShadow" style="width: 99.1%;">
                        <asp:Label ID="lblBGAHead" runat="server" CssClass="genLabel" Width="99.6%" Text="BHA Background Anamoly" />
                        <asp:GridView ID="gvwBGAList" runat="server" Width="99.6%" AutoGenerateColumns="False" DataKeyNames="bgaID"
                            AllowPaging="True" AllowSorting="True" CssClass="mydatagrid" OnPageIndexChanging="gvwBGAList_PageIndexChanging"
                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                            RowStyle-CssClass="rows">
                            <Columns>
                                <asp:BoundField DataField="bgaID" HeaderText="bgaID" Visible="False" />
                                <asp:BoundField DataField="bgaStatement" HeaderText="BHA Background Anamoly" />
                            </Columns>
                        </asp:GridView>
                        <asp:Button ID="btnBGAAdd" runat="server" CssClass="red btn" Text="Add New Background Anamoly" Width="25%" OnClick="btnBGAAdd_Click" />
                    </div>
                    <div id="divBGAAdd" runat="server" class="viewer divShadow" style="width: 99.1%;" visible="false">
                        <asp:Label ID="lblAddBGAHead" runat="server" CssClass="genLabel" Width="99.6%" Text="Add New BHA Background Anamoly" />
                        <asp:Label ID="lblAddBGA" runat="server" CssClass="txtLabel" Text="BHA Background Anamoly Statement" Width="25%" />
                        <asp:TextBox ID="txtAddBGA" runat="server" CssClass="genTextBox" Text="" Width="65%" CausesValidation="true" />
                        <asp:RequiredFieldValidator ID="rfvAddBGA" runat="server" ControlToValidate="txtAddBGA" ErrorMessage="*" Display="Dynamic"
                            SetFocusOnError="true" ForeColor="Maroon" />
                        <br />
                        <asp:Label ID="lblBGASuccess" runat="server" CssClass="lblSuccess" Visible="false" />
                        <br />
                        <asp:Button ID="btnBGAAddNew" runat="server" CssClass="red btn" Text="Add New Statement" Width="25%" OnClick="btnBGAAddNew_Click" />
                        <asp:Button ID="btnBGAAddCancel" runat="server" CssClass="red btn" Text="Clear Values" CausesValidation="false" OnClick="btnBGAAddCancel_Click"
                            Width="15%" />
                    </div>
                    <asp:Button ID="btnBGADone" runat="server" CssClass="red btn" Text="Done" Width="20%" CausesValidation="false" Visible="false"
                        OnClick="btnBGADone_Click" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div id="divBHAAnaDialog" runat="server" class="viewer" style="width: 98.5%;">
            <asp:UpdateProgress ID="upprgrsBHAAna" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlBHAAna">
                <ProgressTemplate>
                    <div id="divBGAA" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                        z-index: 100;">
                        <asp:Image ID="imgBGAAProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="upnlBHAAna" runat="server">
                <ContentTemplate>
                    <div id="divBHAAnaList" runat="server" class="viewer divShadow" style="width: 99.1%;">
                        <asp:Label ID="lblBHAAnaHead" runat="server" CssClass="genLabel" Width="99.6%" Text="BHA Analysis" />
                        <asp:GridView ID="gvwBHAAnaList" runat="server" Width="99.6%" AutoGenerateColumns="False" DataKeyNames="bhaqID"
                            AllowPaging="True" AllowSorting="True" CssClass="mydatagrid" OnPageIndexChanging="gvwBHAAnaList_PageIndexChanging"
                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                            RowStyle-CssClass="rows">
                            <Columns>
                                <asp:BoundField DataField="bhaqID" HeaderText="bhaqID" Visible="False" />
                                <asp:BoundField DataField="bhaqValue" HeaderText="BHA Analysis Statement" />
                            </Columns>
                        </asp:GridView>
                        <asp:Button ID="btnBHAAnaAdd" runat="server" CssClass="red btn" Text="Add New Analysis Statement" Width="25%" OnClick="btnBHAAnaAdd_Click" />
                    </div>
                    <div id="divBHAAnaAdd" runat="server" class="viewer divShadow" style="width: 99.1%;" visible="false">
                        <asp:Label ID="lblBHAAnaAddHead" runat="server" CssClass="genLabel" Width="99.6%" Text="Add New BHA Analysis" />
                        <asp:Label ID="lblBHAAnaAdd" runat="server" CssClass="txtLabel" Text="BHA Analysis Statement" Width="25%" />
                        <asp:TextBox ID="txtBHAAnaAdd" runat="server" CssClass="genTextBox" Text="" Width="70%" CausesValidation="true" />
                        <asp:RequiredFieldValidator ID="rfvBHAAnaAdd" runat="server" ControlToValidate="txtBHAAnaAdd" ErrorMessage="*" ForeColor="Maroon"
                            SetFocusOnError="true" Display="Dynamic" />
                        <br />
                        <asp:Label ID="lblBHAAnaSuccess" runat="server" CssClass="lblSuccess" Width="99.6%" Visible="false" />
                        <br />
                        <asp:Button ID="btnBHAAnaAddNew" runat="server" CssClass="red btn" Text="Add New Statement" Width="25%" OnClick="btnBHAAnaAddNew_Click"
                            CausesValidation="True" />
                        <asp:Button ID="btnBHAAnaAddCancel" runat="server" CssClass="red btn" Text="Clear Values" Width="15%" CausesValidation="false"
                            OnClick="btnBHAAnaAddCancel_Click" />
                    </div>
                    <asp:Button ID="btnBHAAnaDone" runat="server" CssClass="red btn" Text="Done" Visible="false" CausesValidation="false" OnClick="btnBHAAnaDone_Click"
                        Width="20%" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div id="divDCPSDialog" runat="server" class="viewer" style="width: 98.5%;">
            <asp:UpdateProgress ID="uprgrsDCPS" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlDCPS">
                <ProgressTemplate>
                    <div id="divDCPS" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                        z-index: 100;">
                        <asp:Image ID="imgDCPSProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="upnlDCPS" runat="server">
                <ContentTemplate>
                    <div id="divDCPSList" runat="server" class="viewer divShadow" style="width: 99.1%;">
                        <asp:Label ID="lblDCPSHead" runat="server" CssClass="genLabel" Width="99.6%" Text="Drill Collar Pole-Strength" />
                        <asp:GridView ID="gvwDCPSList" runat="server" Width="99.6%" AutoGenerateColumns="False" DataKeyNames="dcpsID" AllowPaging="True"
                            AllowSorting="True" CssClass="mydatagrid" OnPageIndexChanging="gvwDCPSList_PageIndexChanging"
                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                            RowStyle-CssClass="rows">
                            <Columns>
                                <asp:BoundField DataField="dcpsID" HeaderText="dcpsID" Visible="False" />
                                <asp:BoundField DataField="dcpsParameter" HeaderText="Drill Collar Size" />
                                <asp:BoundField DataField="dcpsValue" HeaderText="Pole-Strength Value" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div id="divMPSDialog" runat="server" class="viewer" style="width: 98.5%;">
            <asp:UpdateProgress ID="uprgrsMPS" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlMPS">
                <ProgressTemplate>
                    <div id="divMPS" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                        z-index: 100;">
                        <asp:Image ID="imgMPSProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="upnlMPS" runat="server">
                <ContentTemplate>
                    <div id="divCountyList" runat="server" class="viewer divShadow" style="width: 99.1%;">
                        <asp:Label ID="lbnMPSHead" runat="server" CssClass="genLabel" Width="99.6%" Text="Motor Pole-Strength" />
                        <asp:GridView runat="server" ID="gvwMPSList" AutoGenerateColumns="False" DataKeyNames="mpsID" AllowPaging="True" AllowSorting="True"
                            CssClass="mydatagrid" Width="98.6%" OnPageIndexChanging="gvwMPSList_PageIndexChanging"
                            AlternatingRowStyle-CssClass="AlternatingRowStyle" PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                            RowStyle-CssClass="rows">
                            <Columns>
                                <asp:BoundField DataField="mpsID" HeaderText="mpsID" Visible="False" />
                                <asp:BoundField DataField="mpsSize" HeaderText="Motor Size" />
                                <asp:BoundField DataField="mpsValue" HeaderText="Pole-Strength" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div id="divSRefDialog" runat="server" class="viewer" style="width: 98.5%;">
            <asp:UpdateProgress ID="uprgrsSRef" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlSRef">
                <ProgressTemplate>
                    <div id="divSRef" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center;
                        z-index: 100;">
                        <asp:Image ID="imgSRefProcessing" runat="server" CssClass="center" ImageUrl="~/Images/Progress.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="upnlSRef" runat="server">
                <ContentTemplate>
                    <div id="divSRefList" runat="server" class="viewer divShadow" style="width: 99.1%;">
                        <asp:Label ID="lblSRefHead" runat="server" CssClass="genLabel" Width="99.6%" Text="Long-Collar Survey Direction Reference" />
                        <asp:GridView ID="gvwSRefList" runat="server" Width="99.6%" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True"
                            CssClass="mydatagrid" DataKeyNames="refID" OnPageIndexChanging="gvwSRefList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                            PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows">
                            <Columns>
                                <asp:BoundField DataField="refID" HeaderText="refID" Visible="False" />
                                <asp:BoundField DataField="refName" HeaderText="Reference ID" />
                                <asp:BoundField DataField="refValue" HeaderText="Value" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
