﻿using System;
using System.Data;
using System.Web.UI;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using CLR = System.Drawing.Color;
using CHRT = SmartsVer1.Helpers.ChartHelpers.Gauges;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using CTRT = SmartsVer1.Helpers.AccountHelpers.CreateContract;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;

namespace SmartsVer1.Control.Viewer
{
    public partial class ctrlFESViewer : System.Web.UI.UserControl
    {
        String username, domain, LoginName, GetClientDBString;
        Int32 ClientID = -99, wellCount = -99, aRunCount = -99, opCount = -99, clntCount = -99;
        List<Int32> rawQC = new List<Int32>();
        List<Int32> resQC = new List<Int32>();
        List<Int32> mssr = new List<Int32>();
        List<Int32> lp = new List<Int32>();
        List<Int32> wp = new List<Int32>();
        List<Int32> bg = new List<Int32>();
        List<Int32> regCs = new List<Int32>();

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();
                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);
                username = addr.User;
                domain = addr.Host;
                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);                
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //User control Section
                wellCount = CHRT.getGlobalWellCount();
                regCs = CHRT.getGlobalWellRegistrations();
                rawQC = CHRT.getGlobalRawQCCount();
                resQC = CHRT.getGlobalResQCCount();
                mssr = CHRT.getGlobalMSSRCount();
                lp = CHRT.getGlobalLogPrintCount();
                wp = CHRT.getGlobalWellProfileCount();
                bg = CHRT.getGlobalBGCount();
                aRunCount = CHRT.getGlobalActiveRun();
                opCount = CLNT.getOperatorCoCount();
                clntCount = CLNT.getClientCoCount();
                this.divGuageWell.InnerHtml = "<h3>" + wellCount.ToString() + "</h3><span>Total Wells</span>";
                this.divRegRequests.InnerHtml = "<h3>" + regCs[0].ToString() + "</h3><span> Registration Requests</span>";
                this.divRegApproved.InnerHtml = "<h3>" + regCs[1].ToString() + "</h3><span> Registration Approved</span>";
                this.divGuageRun.InnerHtml = "<h3>" + aRunCount.ToString() + "</h3><span>Active Run(s)</span>";
                this.divGuageOpr.InnerHtml = "<h3>" + opCount.ToString() + "</h3><span>Operators</span>";
                this.divGuageClients.InnerHtml = "<h3>" + clntCount.ToString() + "</h3><span>Client Companies</span>";
                this.spnQC.InnerHtml = "<small>" + rawQC[0].ToString() + " - (24-Hr)</small>";
                this.spnQC24.InnerHtml = "<small>" + rawQC[1].ToString() + " - (2 Week)</small>";
                this.spnQC30.InnerHtml = "<small>" + rawQC[2].ToString() + " - (30 Days)</small>";
                this.spnRQC.InnerHtml = "<small>" + resQC[0].ToString() + " - (24-Hr)</small>";
                this.spnRQC24.InnerHtml = "<small>" + resQC[1].ToString() + " - (2 Week)</small>";
                this.spnRQC30.InnerHtml = "<small>" + resQC[2].ToString() + " - (30 Days)</small>";
                this.spnMSSR.InnerHtml = "<small>" + mssr[0].ToString() + " - (24-Hr)</small>";
                this.spnMSSR24.InnerHtml = "<small>" + mssr[1].ToString() + " - (2 Week)</small>";
                this.spnMSSR30.InnerHtml = "<small>" + mssr[2].ToString() + " - (30 Days)</small>";
                this.spnLP.InnerHtml = "<small>" + lp[0].ToString() + " - (24-Hr)</small>";
                this.spnLP24.InnerHtml = "<small>" + lp[1].ToString() + " - (2 Week)</small>";
                this.spnLP30.InnerHtml = "<small>" + lp[2].ToString() + " - (30 Days)</small>";
                this.spnWP.InnerHtml = "<small>" + wp[0].ToString() + " - (24-Hr)</small>";
                this.spnWP24.InnerHtml = "<small>" + wp[1].ToString() + " - (2 Week)</small>";
                this.spnWP30.InnerHtml = "<small>" + wp[2].ToString() + " - (30 Days)</small>";
                this.spnBG.InnerHtml = "<small>" + bg[0].ToString() + " - (24-Hr)</small>";
                this.spnBG24.InnerHtml = "<small>" + bg[1].ToString() + " - (2 Week)</small>";
                this.spnBG30.InnerHtml = "<small>" + bg[2].ToString() + " - (30 Days)</small>";
                List<Int32> ContractsInfo = CTRT.getContractsDataGlobalList();
                this.spnExp.InnerHtml = "<small>" + ContractsInfo[0].ToString() + " - Expiring</small>";
                this.spnNeg.InnerHtml = "<small>" + ContractsInfo[1].ToString() + " - ReNegotiate</small>";
                this.spnVld.InnerHtml = "<small>" + ContractsInfo[2].ToString() + " - Valid</small>";
                this.spnFEDir.InnerHtml = "<small>" + ": FE Directors</small>";
                this.spnFESp.InnerHtml = "<small>" + ": FE Support</small>";
                this.spnDir.InnerHtml = "<small>" + ": Directors</small>";
                this.spnCrd.InnerHtml = "<small>" + ": Coordinators</small>";
                this.spnFld.InnerHtml = "<small>" + ": Fieldhands</small>";
                this.spnLab.InnerHtml = "<small>" + ": Lab. Techs</small>";
                this.spnRwr.InnerHtml = "<small>" + ": Reviewers</small>";
                List<long> fldSizes = CHRT.getFolderSizeGlobalList();
                this.spnSrv.InnerHtml = "<small>" + fldSizes[0].ToString() + " Mb - Services</small>";
                this.spnLPL.InnerHtml = "<small>" + fldSizes[1].ToString() + " Mb - Log Print</small>";
                this.spnAdt.InnerHtml = "<small>" + fldSizes[2].ToString() + " Mb - Audit Logs</small>";

                //Contract's Grid Datasource
                DataTable contractsTable = CTRT.getGlobalContractTable();
                this.gvwContractsList.DataSource = contractsTable;
                this.gvwContractsList.DataBind();

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void imgbtnSolar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://www.n3kl.org/sun/noaa.html");
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void imgbtnGeoMag_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://www.n3kl.org/sun/noaa.html");
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwContractsList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow drRes = ((DataRowView)e.Row.DataItem).Row;

                    DateTime sD = Convert.ToDateTime(drRes["ctrtStart"]);
                    DateTime eD = Convert.ToDateTime(drRes["ctrtEnd"]);
                    Double ttlDays = -99;
                    Double rmgDays = -99;
                    ttlDays = (eD - sD).TotalDays;
                    rmgDays = (eD - DateTime.Now).TotalDays;
                    if (rmgDays < 0)
                    {
                        rmgDays = 0;
                    }

                    Double difference = Convert.ToDouble((eD - DateTime.Now).TotalDays);

                    Label lblTDays = e.Row.Cells[7].FindControl("lblDttl") as Label;
                    lblTDays.Text = ttlDays.ToString();
                    Label lblRDays = e.Row.Cells[8].FindControl("lblDrm") as Label;
                    lblRDays.Text = Math.Round(rmgDays, 2).ToString();

                    if (eD < DateTime.Now)
                    {
                        e.Row.BackColor = CLR.Red;
                        e.Row.ForeColor = CLR.White;
                        lblTDays.ForeColor = CLR.White;
                        lblRDays.ForeColor = CLR.White;
                    }
                    else if (difference < 60)
                    {
                        e.Row.BackColor = CLR.LightYellow;
                    }
                    else
                    {
                        e.Row.BackColor = CLR.LightGreen;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}