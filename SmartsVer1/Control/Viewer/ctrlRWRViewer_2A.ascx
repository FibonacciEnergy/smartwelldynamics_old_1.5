﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlRWRViewer_2A.ascx.cs" Inherits="SmartsVer1.Control.Viewer.ctrlRWRViewer_2A" %>

<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true" EnablePartialRendering="true"></asp:ScriptManager>
<!-- Stylesheet -->
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/Bootstrap/CSS/dataTables.bootstrap.min.css" rel="stylesheet" />
<!-- JavaScripts -->
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<!-- Javascript for Footer -->
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>
<style type="text/css">
    .swdMenu {
        color: #ffffff;
    }
</style>

<asp:Literal runat="server" ID="litWellMap"></asp:Literal>
    <asp:HiddenField ID="jsonMap" runat="server" />
    <asp:HiddenField ID="jsonMain" runat="server" />
    <asp:HiddenField ID="jsonOffices" runat="server" />
    <asp:HiddenField ID="jsonWell" runat="server" />
    <asp:HiddenField ID="jsonGlobalWells" runat="server" />
    <asp:HiddenField ID="jsonLabs" runat="server" />

<!-- Page Content -->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!-- Page Breadcrumb -->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../Viewer/rwrViewer_2A.aspx" <i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- Page Content -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-info">            
                <div class="panel-body">
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <div class="small-box bg-blue">
                            <div id="divOperators" class="inner" runat="server">
                            </div>
                            <div class="icon">
                                <i class="fa fa-bank" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <div class="small-box bg-green">
                            <div id="divProviders" class="inner" runat="server">
                            </div>
                            <div class="icon">
                                <i class="fa fa-group" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <div class="small-box bg-orange">
                            <div id="divPads" class="inner" runat="server">
                            </div>
                            <div class="icon">
                                <i class="fa fa-th" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <div class="small-box bg-red">
                            <div id="divWells" class="inner" runat="server">
                            </div>
                            <div class="icon">
                                <i class="fa fa-tint" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
