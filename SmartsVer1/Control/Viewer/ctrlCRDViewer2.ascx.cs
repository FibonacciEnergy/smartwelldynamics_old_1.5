﻿using System;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Net.Mail;
using System.Collections;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using CLR = System.Drawing.Color;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using USR = SmartsVer1.Helpers.AccountHelpers.CreateUser;
using CHRT = SmartsVer1.Helpers.ChartHelpers.Gauges;
using LGP = SmartsVer1.Helpers.LogPrint.LogComments;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using CTRT = SmartsVer1.Helpers.AccountHelpers.CreateContract;
using TRK = SmartsVer1.Helpers.AccountHelpers.Tracker;

namespace SmartsVer1.Control.Viewer
{
    public partial class ctrlCRDViewer2 : System.Web.UI.UserControl
    {
        String LoginName, username, domain, fldRole, crdRole, labRole, dirRole, GetClientDBString;
        Int32 ClientID, rgwCount, wellPadCount, wellCount, runCount, fldUsr, crdUsr, labUsr, dirUsr, aprvd, lckd;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                GetClientDBString = clntConn.getClientDBConnection(domain);                
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {                
                //Session Tracking Session
                String sessStart = TRK.sessionCheck();
                //User control Section
                List<Int32> assetDetails = CHRT.getClientAssets(ClientID, GetClientDBString);
                rgwCount = assetDetails[0];
                wellPadCount = assetDetails[1];
                wellCount = assetDetails[2];
                runCount = assetDetails[3];

                ArrayList pInfo = new ArrayList();
                pInfo = CHRT.getTotalQCProcessed(GetClientDBString);
                Int32 processedQCCount1 = Convert.ToInt32(pInfo[0]);
                Int32 processedQCCount2 = Convert.ToInt32(pInfo[1]);
                DateTime date24 = Convert.ToDateTime(pInfo[2]);
                DateTime date14 = Convert.ToDateTime(pInfo[3]);
                this.divGuageJob.InnerHtml = "<h3>" + rgwCount.ToString() + "</h3><span>In Process Well(s)/Lateral(s)</span>";
                this.divGuageWell.InnerHtml = "<h3>" + wellPadCount.ToString() + " / " + wellCount.ToString() + "</h3><span>Total Well Pad(s) / Well(s)/Lateral(s)</span>";
                this.divGuageBHA.InnerHtml = "<h3>" + runCount.ToString() + "</h3><span>Active Run</span>";
                this.divGuage1.InnerHtml = "<h3>" + processedQCCount1.ToString() + "</h3><span>Audit/QC (24 Hr)</span>";
                this.divGuage2.InnerHtml = "<h3>" + processedQCCount2.ToString() + "</h3><span>Audit/QC (14 Days)</span>";

                // Log Print Charts
                Int32 totalcount, submitted, inprogress, complete;
                totalcount = submitted = inprogress = complete = 0;

                System.Data.DataTable dt = new DataTable();
                dt = LGP.getOrderCounts(ClientID);
                foreach (DataRow row in dt.Rows)
                {
                    totalcount = Convert.ToInt32(row[0].ToString());
                    submitted = Convert.ToInt32(row[1].ToString());
                    inprogress = Convert.ToInt32(row[2].ToString());
                    complete = Convert.ToInt32(row[3].ToString());
                }
                this.divGuageinprogressOrders.InnerHtml = "<h3>" + inprogress.ToString() + "</h3><span>Log Print In-Porgress</span>";
                this.divGuagecompleteOrders.InnerHtml = "<h3>" + complete.ToString() + "</h3><span>Log Print Completed</span>";

                Int32 rInd = -99;
                System.Data.DataTable usrTable = USR.getClientUserPerRole(ClientID);
                foreach (DataRow row in usrTable.Rows)
                {
                    rInd = usrTable.Rows.IndexOf(row);
                    switch (rInd)
                    {
                        case 0: { crdRole = Convert.ToString(row[0]); crdUsr = Convert.ToInt32(row[1]); break; }
                        case 1: { dirRole = Convert.ToString(row[0]); dirUsr = Convert.ToInt32(row[1]); break; }
                        case 2: { fldRole = Convert.ToString(row[0]); fldUsr = Convert.ToInt32(row[1]); break; }
                        case 3: { labRole = Convert.ToString(row[0]); labUsr = Convert.ToInt32(row[1]); break; }
                    }
                }
                if (String.IsNullOrEmpty(crdRole)) { crdRole = "Coordinator"; crdUsr = 0; }
                if (String.IsNullOrEmpty(dirRole)) { dirRole = "Director"; dirUsr = 0; }
                if (String.IsNullOrEmpty(fldRole)) { fldRole = "Fieldhand"; fldUsr = 0; }
                if (String.IsNullOrEmpty(labRole)) { labRole = "Lab Tech"; labUsr = 0; }
                this.divDIR.InnerHtml = "<h3>" + dirUsr.ToString() + " / " + crdUsr.ToString() + "</h3>";
                this.divCRD.InnerHtml = "<h5><span>" + dirRole.ToString() + " / " + crdRole.ToString() + "</span></h5>";
                this.divFLD.InnerHtml = "<h3>" + fldUsr.ToString() + " / " + labUsr.ToString() + "</h3>";
                this.divLT.InnerHtml = "<h5><span>" + fldRole.ToString() + " / " + labRole.ToString() + "</span></h5>";

                Int32 stInd = -99;
                System.Data.DataTable statusTable = USR.getUserStats(ClientID);
                foreach (DataRow row in statusTable.Rows)
                {
                    stInd = statusTable.Rows.IndexOf(row);
                    switch (stInd)
                    {
                        case 0: { aprvd = Convert.ToInt32(row[1]); break; }
                        case 1: { lckd = Convert.ToInt32(row[1]); break; }
                    }
                }
                this.divApvd.InnerHtml = "<h3>" + aprvd.ToString() + " / " + lckd.ToString() + "</h3>";
                this.divLckd.InnerHtml = "<h5><span>Approved / Locked</span></h5>";

                System.Data.DataTable cttTable = CTRT.getClientContractTable(ClientID);
                this.gvwContractList.DataSource = cttTable;
                this.gvwContractList.PageIndex = 0;
                this.gvwContractList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void imgbtnSolar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://www.n3kl.org/sun/noaa.html");
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void imgbtnGeoMag_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://www.n3kl.org/sun/noaa.html");
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwContractList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                System.Data.DataTable cttTable = CTRT.getClientContractTable(ClientID);
                this.gvwContractList.DataSource = cttTable;
                this.gvwContractList.PageIndex = e.NewPageIndex;
                this.gvwContractList.DataBind();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void gvwContractList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow drRes = ((DataRowView)e.Row.DataItem).Row;

                    DateTime sD = Convert.ToDateTime(drRes["ctrtStart"]);
                    DateTime eD = Convert.ToDateTime(drRes["ctrtEnd"]);
                    Double ttlDays = -99;
                    Double rmgDays = -99;
                    ttlDays = (eD - sD).TotalDays;
                    rmgDays = (eD - DateTime.Now).TotalDays;

                    Double difference = Convert.ToDouble((eD - DateTime.Now).TotalDays);

                    Label lblTDays = e.Row.Cells[7].FindControl("lblDttl") as Label;
                    lblTDays.Text = ttlDays.ToString();
                    Label lblRDays = e.Row.Cells[8].FindControl("lblDrm") as Label;
                    if (rmgDays < 0)
                    {
                        lblRDays.Text = "0.00";
                    }
                    else
                    {
                        lblRDays.Text = Math.Round(rmgDays, 2).ToString();
                    }

                    if (eD < DateTime.Now)
                    {
                        e.Row.BackColor = CLR.Red;
                    }
                    else if (difference < 60)
                    {
                        e.Row.BackColor = CLR.Yellow;
                    }
                    else
                    {
                        e.Row.BackColor = CLR.Green;
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        
    }
}