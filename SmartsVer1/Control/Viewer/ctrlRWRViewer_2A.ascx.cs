﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Web.Security;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using GM = SmartsVer1.Helpers.ChartHelpers.GMaps;

namespace SmartsVer1.Control.Viewer
{
    public partial class ctrlRWRViewer_2A : System.Web.UI.UserControl
    {
        String LoginName, username, domain, GetClientDBString;
        Int32 ClientID;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();
                GetClientDBString = clntConn.getClientDBConnection(domain);

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Int32 padCount = -99, welCount = -99, spCount = -99, opCount = -99;
                List<Int32> assetCounts = CLNT.getReviewerAssetGlobalCounts();
                opCount = assetCounts[0];
                spCount = assetCounts[1];
                padCount = assetCounts[2];
                welCount = assetCounts[3];
                this.divOperators.InnerHtml = "<h3>" + opCount.ToString() + "</h3><span>Global Operator Companies</span>";
                this.divProviders.InnerHtml = "<h3>" + spCount.ToString() + "</h3><span>Service Provider Companies</span>";
                this.divPads.InnerHtml = "<h3>" + padCount.ToString() + "</h3><span>Total Global Well Pads</span>";
                this.divWells.InnerHtml = "<h3>" + spCount.ToString() + "</h3><span>Total Global Well(s)/Lateral(s)</span>";
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}