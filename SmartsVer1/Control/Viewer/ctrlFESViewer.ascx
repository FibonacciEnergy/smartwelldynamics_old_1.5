﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlFESViewer.ascx.cs" Inherits="SmartsVer1.Control.Viewer.ctrlFESViewer" %>

<!-- Stylesheet -->
<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- JavaScripts -->
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/jquery.slimscroll.min.js"></script>
<!--- local style --->
<style type="text/css">
    .swdMenu {
        color: #ffffff;
    }
</style>
<!-- Javascript for Footer -->
<script>
    $(document).ready(function () {
        setInterval(function () {
            var docHeight = $(window).height();
            var footerHeight = $('#footer').height();
            var footerTop = $('#footer').position().top + footerHeight;
            var marginTop = (docHeight - footerTop - 30);

            if (footerTop < docHeight)
                $('#footer').css('margin-top', marginTop + 'px');
            else
                $('#footer').css('margin-top', '0px');
        }, 250);
    });
</script>
<!--- Page Menu -->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!-- Page Breadcrumb -->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../Viewer/fesViewer.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- Page Content -->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-danger">
            <div class="panel-heading bg-danger">
                <h5 class="text-danger">Magnetic Acitivity</h5>
            </div>
            <div class="panel-body">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div id="divSolar" class="bg-danger with-border">
                        <div class="panel-body">
                            <div class="col-xs-3 text-left">
                                <asp:Image ID="imgSolar" runat="server" ImageUrl="~/Images/SolarRays.png" ImageAlign="Middle" />
                            </div>
                            <div class="col-xs-9 text-right">
                                <div>
                                    <h4>Solar X-Rays Activity</h4>
                                </div>
                                <div id="divSolarLnk" runat="server">
                                    <asp:ImageButton ID="imgbSolarLink" runat="server" Width="25%" ImageUrl="http://www.n3kl.org/sun/images/status.gif?"
                                        ImageAlign="AbsMiddle" OnClick="imgbtnSolar_Click" CausesValidation="False" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div id="divMag" class="bg-danger with-border">
                        <div class="panel-body">
                            <div class="col-xs-3">
                                <asp:Image ID="imgMag" runat="server" ImageUrl="~/Images/MagField.png" ImageAlign="Middle" Height="90px" />
                            </div>
                            <div class="col-xs-9 text-right">
                                <div>
                                    <h4>Geomagnetic Field Activity</h4>
                                </div>
                                <div id="divMagLnk" runat="server">
                                    <asp:ImageButton ID="imgbMag" runat="server" Width="25%" ImageUrl="http://www.n3kl.org/sun/images/kpstatus.gif?"
                                        ImageAlign="AbsMiddle" OnClick="imgbtnGeoMag_Click" CausesValidation="False" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div id="divGlobe" class="bg-danger with-border">
                        <div class="panel-body">
                            <div class="col-xs-3">
                                <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/MagField_2.png" ImageAlign="Middle" Height="90px" />
                            </div>
                            <div class="col-xs-9 text-right">
                                <div>
                                    <h4>Regional Magnetic Activity</h4>
                                </div>
                                <div id="divGlobeLnk" runat="server">
                                    <a href="https://geomag.usgs.gov/plots/dst.php" target="_blank" style="color: black;">USA</a> |
                                                    <a href="http://www.spaceweather.ca/current-actuelle/short-court/sfst-5-eng.php" target="_blank" style="color: black;">Canada</a>
                                    |
                                                    <a href="http://www.intermagnet.org/activitymap/activitymap-eng.php" target="_blank" style="color: black;">Global</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
            <div class="panel panel-info">
                <div class="panel-heading with-border">
                    <h5 class="box-title">SMARTs Status</h5>
                </div>
                <div class="panel panel-body">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="small-box bg-green">
                            <div id="divGuageWell" class="inner" runat="server">
                            </div>
                            <div class="icon">
                                <i class="fa fa-list" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="small-box bg-orange">
                            <div id="divGuageRun" class="inner" runat="server">
                            </div>
                            <div class="icon">
                                <i class="fa fa-bolt" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="small-box bg-aqua">
                            <div id="divGuageOpr" class="inner" runat="server">
                            </div>
                            <div class="icon">
                                <i class="fa  fa-bank" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="small-box bg-teal">
                            <div id="divGuageClients" class="inner" runat="server">
                            </div>
                            <div class="icon">
                                <i class="fa fa-group" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <div class="panel panel-info">
                <div class="panel-heading with-border">
                    <h5 class="box-title">Well / Lateral Registrations</h5>
                </div>
                <div class="panel panel-body">
                    <div class="col-xs-12 col-sm-3 col-md-6 col-lg-6">
                        <div class="small-box bg-orange">
                            <div id="divRegRequests" class="inner" runat="server">
                            </div>
                            <div class="icon">
                                <i class="fa fa-pencil" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-6 col-lg-6">
                        <div class="small-box bg-green">
                            <div id="divRegApproved" class="inner" runat="server">
                            </div>
                            <div class="icon">
                                <i class="fa fa-pencil-square" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-info">
            <div class="panel-heading with-border">
                <h5 class="box-title">SMARTs Ops</h5>
            </div>
            <div class="panel panel-body">
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <div class="info-box bg-yellow">
                        <span class="info-box-icon"><i class="fa fa-edit text-info mb-3 sr-icons"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Raw Audit/QC</span>
                            <span id="spnQC" runat="server" class="info-box-number"></span>
                            <span id="spnQC24" runat="server" class="info-box-number"></span>
                            <span id="spnQC30" runat="server" class="info-box-number"></span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <div class="info-box bg-yellow">
                        <span class="info-box-icon"><i class="fa fa-edit text-info mb-3 sr-icons"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Results Audit/QC</span>
                            <span id="spnRQC" runat="server" class="info-box-number"></span>
                            <span id="spnRQC24" runat="server" class="info-box-number"></span>
                            <span id="spnRQC30" runat="server" class="info-box-number"></span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <div class="info-box bg-red">
                        <span class="info-box-icon"><i class="fa fa-sun-o text-info mb-3 sr-icons"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">MSSR</span>
                            <span id="spnMSSR" runat="server" class="info-box-number"></span>
                            <span id="spnMSSR24" runat="server" class="info-box-number"></span>
                            <span id="spnMSSR30" runat="server" class="info-box-number"></span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <div class="info-box bg-blue">
                        <span class="info-box-icon"><i class="fa fa-print text-info mb-3 sr-icons"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Logs Mgt.</span>
                            <span id="spnLP" runat="server" class="info-box-number"></span>
                            <span id="spnLP24" runat="server" class="info-box-number"></span>
                            <span id="spnLP30" runat="server" class="info-box-number"></span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <div class="info-box bg-green">
                        <span class="info-box-icon"><i class="fa fa-line-chart text-info mb-3 sr-icons"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Well Profile</span>
                            <span id="spnWP" runat="server" class="info-box-number"></span>
                            <span id="spnWP24" runat="server" class="info-box-number"></span>
                            <span id="spnWP30" runat="server" class="info-box-number"></span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">                    
                    <div class="info-box bg-purple">
                        <span class="info-box-icon"><i class="fa fa-undo text-info mb-3 sr-icons"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Reverse Vector</span>
                            <span id="spnBG" runat="server" class="info-box-number"></span>
                            <span id="spnBG24" runat="server" class="info-box-number"></span>
                            <span id="spnBG30" runat="server" class="info-box-number"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <div class="panel panel-info">
                <div class="panel-heading with-border">
                    <h5 class="panel-title">SMARTs Contracts</h5>
                </div>
                <div class="panel panel-body">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="info-box bg-light-blue">
                            <span class="info-box-icon"><i class="fa fa-shield text-info mb-3 sr-icons"></i></span>
                            <div class="info-box-content">
                                <span id="spnExp" runat="server" class="info-box-number"></span>
                                <span id="spnNeg" runat="server" class="info-box-number"></span>
                                <span id="spnVld" runat="server" class="info-box-number"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="info-box bg-light-blue">
                            <span class="info-box-icon"><i class="fa fa-ambulance text-info mb-3 sr-icons"></i></span>
                            <div class="info-box-content">
                                <span id="Span1" runat="server" class="info-box-number"></span>
                                <span id="Span2" runat="server" class="info-box-number"></span>
                                <span id="Span3" runat="server" class="info-box-number"></span>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <div class="panel panel-info">
                <div class="panel-heading with-border">
                    <h5 class="panel-title">SMARTs Accounts/Roles</h5>
                </div>
                <div class="panel panel-body">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="info-box bg-light-blue">
                            <span class="info-box-icon"><i class="fa fa-group text-info mb-3 sr-icons"></i></span>
                            <div class="info-box-content">
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <span id="spnFEDir" runat="server" class="info-box-number"></span>
                                    <span id="spnFESp" runat="server" class="info-box-number"></span>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <span id="spnDir" runat="server" class="info-box-number"></span>
                                    <span id="spnCrd" runat="server" class="info-box-number"></span>
                                    <span id="spnFld" runat="server" class="info-box-number"></span>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <span id="spnLab" runat="server" class="info-box-number"></span>
                                    <span id="spnRwr" runat="server" class="info-box-number"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <div class="panel panel-info">
                <div class="panel-heading with-border">
                    <h1 class="panel-title">Folder Size</h1>
                </div>
                <div class="panel panel-body">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="info-box bg-light-blue">
                            <span class="info-box-icon"><i class="fa fa-database text-info mb-3 sr-icons"></i></span>
                            <div class="info-box-content">
                                <span id="spnSrv" runat="server" class="info-box-number"></span>
                                <span id="spnLPL" runat="server" class="info-box-number"></span>
                                <span id="spnAdt" runat="server" class="info-box-number"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-info">
            <div class="panel-heading with-border">
                <h1 class="panel-title">SMARTs Contracts</h1>
            </div>
            <div class="panel panel-body">
                <asp:GridView ID="gvwContractsList" runat="server" CssClass="mydatagrid" Width="100%" AutoGenerateColumns="False" DataKeyNames="ctrtID"
                    AllowPaging="True" PageSize="15" PagerStyle-CssClass="pager" HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                    OnRowDataBound="gvwContractsList_RowDataBound" EmptyDataText="No Contracts found in the System." ShowHeaderWhenEmpty="True"
                    SelectedRowStyle-CssClass="selectedrow">
                    <Columns>
                        <asp:BoundField DataField="ctrtID" HeaderText="ctrtID" ReadOnly="True" InsertVisible="False" SortExpression="ctrtID" Visible="False" />
                        <asp:BoundField DataField="ctID" HeaderText="Type" />
                        <asp:BoundField DataField="ctrtName" HeaderText="Name" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="cstatID" HeaderText="Status" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="ctrtStart" HeaderText="Contract Start Date" SortExpression="ctrtStart" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}"
                            ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="ctrtEnd" HeaderText="Contract End Date" SortExpression="ctrtEnd" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}"
                            ItemStyle-HorizontalAlign="Center" />
                        <asp:TemplateField HeaderText="Total Contract Days" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblDttl" runat="server" Text="" BackColor="Transparent" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Contract Days Remaining" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblDrm" runat="server" Text="" BackColor="Transparent" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="uTime" HeaderText="Last Updated" SortExpression="uTime" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}"
                            ItemStyle-HorizontalAlign="Center" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        </div>
    </div>
</div>
<asp:Literal ID="ltrLoadGraphs" runat="server"></asp:Literal>