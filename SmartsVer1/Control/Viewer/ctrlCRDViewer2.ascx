﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlCRDViewer2.ascx.cs" Inherits="SmartsVer1.Control.Viewer.ctrlCRDViewer2" %>
<!-- Stylesheet -->
<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />

<style type="text/css">
    .swdMenu {
        color: #ffffff;
    }
</style>

<!-- Page Content -->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!-- Page Breadcrumb -->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../Viewer/crdViewer2.aspx" <i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- Page Content -->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
            <div class="panel panel-danger">
                <div class="panel-heading bg-danger">
                    <h5 class="text-danger">Magnetic Acitivity</h5>
                </div>
                <div class="panel-body">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div id="divSolar" class="bg-danger with-border">
                            <div class="panel-body">
                                <div class="col-xs-3 text-left">
                                    <asp:Image ID="imgSolar" runat="server" ImageUrl="~/Images/SolarRays.png" ImageAlign="Middle" />
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>
                                        <h4 class="text-danger">Solar X-Rays</h4>
                                    </div>
                                    <div id="divSolarLnk" runat="server">
                                        <asp:ImageButton ID="imgbSolarLink" runat="server" Width="40%" ImageUrl="http://www.n3kl.org/sun/images/status.gif?"
                                            ImageAlign="AbsMiddle" OnClick="imgbtnSolar_Click" CausesValidation="False" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div id="divMag" class="bg-danger with-border">
                            <div class="panel-body">
                                <div class="col-xs-3">
                                    <asp:Image ID="imgMag" runat="server" ImageUrl="~/Images/MagField.png" ImageAlign="Middle" Height="90px" />
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>
                                        <h4 class="text-danger">Geomagnetic Field</h4>
                                    </div>
                                    <div id="divMagLnk" runat="server">
                                        <asp:ImageButton ID="imgbMag" runat="server" Width="40%" ImageUrl="http://www.n3kl.org/sun/images/kpstatus.gif?"
                                            ImageAlign="AbsMiddle" OnClick="imgbtnGeoMag_Click" CausesValidation="False" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div id="divGlobe" class="bg-danger with-border">
                            <div class="panel-body">
                                <div class="col-xs-3">
                                    <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/MagField_2.png" ImageAlign="Middle" Height="90px" />
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>
                                        <h4 class="text-danger">Regional Magnetics</h4>
                                    </div>
                                    <div id="divGlobeLnk" runat="server">
                                        <a href="https://geomag.usgs.gov/plots/dst.php" target="_blank" style="color: black;">USA</a> |
                                                    <a href="http://www.spaceweather.ca/current-actuelle/short-court/sfst-5-eng.php" target="_blank" style="color: black;">Canada</a>
                                        |
                                                    <a href="http://www.intermagnet.org/activitymap/activitymap-eng.php" target="_blank" style="color: black;">Global</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <div class="panel panel-primary">
                <div class="panel-heading bg-primary">
                    <h5 class="text-white">SMARTs Platform Usage</h5>
                </div>
                <div class="panel-body">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="small-box bg-blue">
                            <div id="divGuage1" class="inner" runat="server"></div>
                            <div class="icon">
                                <i class="fa fa-list" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="small-box bg-blue">
                            <div id="divGuage2" class="inner" runat="server"></div>
                            <div class="icon">
                                <i class="fa fa-list" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
            <div class="panel panel-primary">
                <div class="panel-heading bg-light-blue">
                    <h5 class="text-white">SMARTs Status</h5>
                </div>
                <div class="panel-body">                    
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="small-box bg-blue">
                            <div id="divGuageWell" class="inner" runat="server"></div>
                            <div class="icon">
                                <i class="fa fa-tint" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="small-box bg-green">
                            <div id="divGuageJob" class="inner" runat="server"></div>
                            <div class="icon">
                                <i class="fa fa-list" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="small-box bg-orange">
                            <div id="divGuageBHA" class="inner" runat="server"></div>
                            <div class="icon">
                                <i class="fa fa-bolt" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <div class="panel panel-primary">
                <div class="panel-heading bg-light-blue">
                    <h5 class="text-white">Log Print</h5>
                </div>
                <div class="panel-body">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="small-box bg-purple">
                            <div id="divGuageinprogressOrders" class="inner" runat="server"></div>
                            <div class="icon">
                                <i class="fa fa-print" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="small-box bg-green">
                            <div id="divGuagecompleteOrders" class="inner" runat="server"></div>
                            <div class="icon">
                                <i class="fa fa-print" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading bg-light-blue-gradient">
                    <h5 class="text-white">Users</h5>
                </div>
                <div class="panel-body">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="small-box bg-light-blue">
                            <div id="divDIR" runat="server" class="inner"></div>
                            <div id="divCRD" runat="server" class="inner"></div>
                            <div class="icon">
                                <i class="fa fa-users" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="small-box bg-light-blue">
                            <div id="divFLD" runat="server" class="inner"></div>
                            <div id="divLT" runat="server" class="inner"></div>
                            <div class="icon">
                                <i class="fa fa-users" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="small-box bg-light-blue">
                            <div id="divApvd" runat="server" class="inner"></div>
                            <div id="divLckd" runat="server" class="inner"></div>
                            <div class="icon">
                                <i class="fa fa-users" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading bg-blue-gradient">
                    <h5 class="text-white">SMARTs Contracts</h5>
                </div>
                <div class="panel-body">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <asp:GridView ID="gvwContractList" runat="server" CssClass="mydatagrid" EmptyDataText="No Contracts found." AllowPaging="True"
                            AutoGenerateColumns="False"
                            DataKeyNames="ctrtID" ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwContractList_PageIndexChanging" AlternatingRowStyle-CssClass="AlternatingRowStyle"
                            PageSize="15" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager" RowStyle-CssClass="rows" OnRowDataBound="gvwContractList_RowDataBound">
                            <Columns>
                                <asp:BoundField DataField="ctrtID" Visible="false" />
                                <asp:BoundField DataField="ctID" HeaderText="Contract Type" />
                                <asp:BoundField DataField="ctrtName" HeaderText="Contract Title" />
                                <asp:BoundField DataField="cstatID" HeaderText="Status" />
                                <asp:BoundField DataField="ctrtStart" HeaderText="Start Date" />
                                <asp:BoundField DataField="ctrtEnd" HeaderText="End Date" />
                                <asp:TemplateField HeaderText="Total Contract Days" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDttl" runat="server" Text="" BackColor="Transparent" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Contract Days Remaining" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDrm" runat="server" Text="" BackColor="Transparent" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="uTime" HeaderText="Last Updated" SortExpression="uTime" DataFormatString="{0:MMM d , yyyy --- hh:mm tt}"
                                    ItemStyle-HorizontalAlign="Center" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Script -->
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/jquery.slimscroll.min.js"></script>
