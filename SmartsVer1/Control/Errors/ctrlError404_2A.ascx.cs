﻿using System;
using System.Net.Mail;
using System.Web;
using System.Web.Security;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using log4net;

namespace SmartsVer1.Control.Errors
{
    public partial class ctrlError404_2A : System.Web.UI.UserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ctrlError404_2A));
        String LoginName, username, domain, GetClientDBString;
        Int32 ClientID = -99;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                ClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(ClientID, LoginName);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Response.Status = "404 Not Found";
                Response.StatusCode = 404;
                Response.TrySkipIisCustomErrors = true;
                FormsAuthentication.SetAuthCookie(HttpContext.Current.User.Identity.Name, true);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}