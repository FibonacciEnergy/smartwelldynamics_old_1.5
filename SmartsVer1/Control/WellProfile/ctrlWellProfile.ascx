﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrlWellProfile.ascx.cs" Inherits="SmartsVer1.Control.WellProfile.ctrlWellProfile" %>

<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

<link href="../../Content/SWD/feGridview.css" rel="stylesheet" />
<link href="../../Content/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="../../Content/IonIcons/css/ionicons.min.css" rel="stylesheet" />
<link href="../../Content/AdminLTE/AdminLTE.min.css" rel="stylesheet" />
<link href="../../Content/Skins/css/skin-blue.min.css" rel="stylesheet" />
<!-- JavaScripts -->
<script src="../../Scripts/Bootstrap/jQuery/jquery.min.js"></script>
<script src="../../Scripts/Bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../Scripts/Bootstrap/AdminLTE/dist/js/adminlte.min.js"></script>
<!--- Page local style --->
<style type="text/css">

    .panelBkg {
        background-color: #C1CFDD;
    }
    .rtAligned {
        text-align: right;
    }

    .rotate {
        -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
        -moz-transform: rotate(90deg); /* Firefox */
        -ms-transform: rotate(90deg); /* IE 9 */
        transform: rotate(90deg); /* Standard syntax */
    }

    .swdMenu {
        color: #ffffff;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    ol.stepper {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 0;
        table-layout: fixed;
        width: 100%;
    }

        ol.stepper li {
            display: table-cell;
            text-align: center;
            line-height: 3em;
        }

        ol.stepper[data-stepper-steps="2"] li {
            width: 49%;
        }

        ol.stepper[data-stepper-steps="3"] li {
            width: 33%;
        }

        ol.stepper[data-stepper-steps="4"] li {
            width: 24%;
        }

        ol.stepper[data-stepper-steps="5"] li {
            width: 19%;
        }

        ol.stepper[data-stepper-steps="6"] li {
            width: 16%;
        }

        ol.stepper[data-stepper-steps="7"] li {
            width: 14%;
        }

        ol.stepper[data-stepper-steps="8"] li {
            width: 12%;
        }

        ol.stepper[data-stepper-steps="9"] li {
            width: 11%;
        }

        ol.stepper li.stepper-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
        }

        ol.stepper li.stepper-todo {
            color: silver;
            border-bottom: 4px solid silver;
        }

        ol.stepper li:after {
            content: "\00a0\00a0";
        }

        ol.stepper li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            left: 50%;
            line-height: 1em;
        }

        ol.stepper li.stepper-done:before {
            content: "\2713";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
        }

        ol.stepper li.stepper-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
        }
</style>

<!--- Page local script --->
<script type="text/javascript">
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("rotate");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").addClass("rotate");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("rotate");
        });
    });
</script>

<!--- Page Menu --->
<div class="row bg-black">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding-bottom:2px;padding-left:2px;padding-top:2px">
        <asp:Image ID="imgLogo" runat="server" ImageAlign="Right" ImageUrl="~/Images/SWDLogo_90.png" AlternateText="SMARTs Logo" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 align-left">
        <asp:Literal ID="ltrMenuLiteral" runat="server"></asp:Literal>
    </div>
</div>
<div class="row bg-black">    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
        <asp:LoginView ID='LoginView1' runat='server'>
            <AnonymousTemplate>
                <li style="vertical-align: middle; display: table-cell;">
                    <asp:HyperLink ID="hlnkLogIn" runat='server' NavigateUrl='~/Account/SMARTAccess2.aspx' Text='Login' Font-Size="Medium" Font-Names="Segoe UI"
                        ForeColor="White"><span class='fa fa-sign-in'></span> Login</asp:HyperLink>
                </li>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <li style="list-style-type: none; list-style-image: none; margin-right: 15px;">
                    ( <asp:LoginName ID="LoginName1" runat="server" /> )                    
                    <asp:LoginStatus ID='LoginStatus1' runat='server' CssClass='fa fa-sign-out' LogoutAction='Redirect'
                        LogoutText=' Logout' LogoutPageUrl='~/SWDHome.aspx' ForeColor="White" Font-Size="Medium" Font-Underline="True"></asp:LoginStatus>
                </li>
            </LoggedInTemplate>            
        </asp:LoginView>        
    </div>    
</div>
<!--- Page Breadcrumb --->
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">        
        <div class="box-tools pull-right">
            <ol class="breadcrumb">
                <li><a href="../../../Viewer/crdViewer2.aspx"><i class="fa fa-dashboard"></i> SMARTs Panel</a></li>
                <li>Configuration</li>
                <li><a href="../../WellProfile/fecWellProfile_2A.aspx"> Well Profile Generator</a></li>
            </ol>
        </div>
    </div>    
</div>

<!-- Page Content -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row bg-white">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h1 class="box-title">Magnetic Acitivty</h1>
                </div>
                <div class="box-body">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div id="divSolar" class="bg-aqua with-border">
                            <div class="box-body">
                                <div class="col-xs-3 text-left">
                                    <asp:Image ID="imgSolar" runat="server" ImageUrl="~/Images/SolarRays.png" ImageAlign="Middle" />
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>
                                        <h4>Solar X-Rays Activity</h4>
                                    </div>
                                    <div id="divSolarLnk" runat="server">
                                        <asp:ImageButton ID="imgbSolarLink" runat="server" Width="25%" ImageUrl="http://www.n3kl.org/sun/images/status.gif?"
                                            ImageAlign="AbsMiddle" OnClick="imgbtnSolar_Click" CausesValidation="False" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div id="divMag" class="bg-aqua with-border">
                            <div class="box-body">
                                <div class="col-xs-3">
                                    <asp:Image ID="imgMag" runat="server" ImageUrl="~/Images/MagField.png" ImageAlign="Middle" Height="90px" />
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>
                                        <h4>Geomagnetic Field Activity</h4>
                                    </div>
                                    <div id="divMagLnk" runat="server">
                                        <asp:ImageButton ID="imgbMag" runat="server" Width="25%" ImageUrl="http://www.n3kl.org/sun/images/kpstatus.gif?"
                                            ImageAlign="AbsMiddle" OnClick="imgbtnGeoMag_Click" CausesValidation="False" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div id="divGlobe" class="bg-aqua with-border">
                            <div class="box-body">
                                <div class="col-xs-3">
                                    <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/MagField_2.png" ImageAlign="Middle" Height="90px" />
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>
                                        <h4>Regional Magnetic Activity</h4>
                                    </div>
                                    <div id="divGlobeLnk" runat="server">
                                        <a href="http://geomag.usgs.gov/realtime/" target="_blank" style="color: black;">USA</a> |
                                                    <a href="http://www.spaceweather.ca/current-actuelle/short-court/sfst-5-eng.php" target="_blank" style="color: black;">Canada</a>
                                        |
                                                    <a href="http://www.intermagnet.org/activitymap/activitymap-eng.php" target="_blank" style="color: black;">Global</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row bg-white">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h1 class="box-title">Summary Information</h1>
            </div>
            <div class="box-body">
                <div class="row">
                    <div id="divGuages" runat="server" class="col-xs-12 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            <div class="small-box bg-blue">
                                <div id="divGuageJob" runat="server" class="inner" />
                                <div class="icon">
                                    <i class="fa fa-list" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            <div class="small-box bg-orange-active">
                                <div id="divGuageWell" runat="server" class="inner" />
                                <div class="icon">
                                    <i class="fa fa-tasks" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            <div class="small-box bg-olive">
                                <div id="divAscWell" runat="server" class="inner" />
                                <div class="icon">
                                    <i class="fa fa-chain-broken" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            <div class="small-box bg-olive">
                                <div id="divMRM" runat="server" class="inner" />
                                <div class="icon">
                                    <i class="fa fa-heartbeat" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            <div class="small-box bg-olive">
                                <div id="divMsngBHA" runat="server" class="inner" />
                                <div class="icon">
                                    <i class="fa fa-chain-broken" style="font-size: smaller; vertical-align: text-top; text-align: right;"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<asp:UpdatePanel runat="server" ID="upnlMetaInfo">
    <ContentTemplate>
        <div runat="server" id="divInputData" class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h1 class="box-title">SMARTs Well Profile</h1>
                    </div>
                    <div class="box-body">
                        <asp:UpdateProgress ID="upgrsMeta" runat="server" AssociatedUpdatePanelID="upnlMeta" DisplayAfter="0">
                            <ProgressTemplate>
                                <div id="divProgSurround" runat="server" class="overlay" style="width: 100%; height: 100%; vertical-align: middle; text-align: center; z-index: 100;">
                                    <asp:Image ID="upgrsMetaImage" runat="server" CssClass="center" AlternateText="Processing ...." ForeColor="Purple" ImageAlign="Middle" ImageUrl="~/Images/Progress.gif" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:UpdatePanel runat="server" ID="upnlMeta">
                            <ContentTemplate>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <asp:Image ID="imgPlaceHolder" runat="server" AlternateText="Working diligently to bring this online ...." ImageAlign="AbsMiddle" ImageUrl="~/Images/WorkingToBuildPage.png" />
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>

<!-- Page Content End -->
