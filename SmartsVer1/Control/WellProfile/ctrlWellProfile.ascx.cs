﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI.WebControls;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using QC = SmartsVer1.QCHelpers.sdRawDataQC;
using Color = System.Drawing.Color;
using MNL = SmartsVer1.Helpers.Literals.MenuLoader;
using CHRT = SmartsVer1.Helpers.ChartHelpers.Gauges;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using OPR = SmartsVer1.Helpers.QCHelpers.Opr;
using MAP = SmartsVer1.Helpers.ChartHelpers.GMaps;
using LIT = SmartsVer1.Helpers.Literals.MenuItems;
using log4net;

namespace SmartsVer1.Control.WellProfile
{
    public partial class ctrlWellProfile : System.Web.UI.UserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ctrlWellProfile));
        const Int32 JobClassID = 1;
        const Int32 ServiceID = 41;
        const Int32 ClientTypID = 4;
        String LoginName, username, domain, GetClientDBString;
        Int32 SysClientID = -99, jobCount = 0, wellCount = 0, runCount = 0, mrmCount = 0, mbhaCount = 0;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                GetClientConnections clntConn = new GetClientConnections();

                LoginName = Membership.GetUser().ToString();
                MailAddress addr = new MailAddress(LoginName);

                username = addr.User;
                domain = addr.Host;

                GetClientDBString = clntConn.getClientDBConnection(domain);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                SysClientID = Convert.ToInt32(usrP.clntID);
                //Page Menu
                this.ltrMenuLiteral.Text = MNL.getPageMenu(SysClientID, LoginName);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");
                Int32 sStatus = DMG.getServiceAvailability(ServiceID);
                if (sStatus.Equals(2))
                {
                    this.divInputData.Visible = true;

                }
                else
                {
                    this.divInputData.Visible = false;
                }

                if (!Page.IsPostBack)
                {
                    //this.createAssetCharts(null, null);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void imgbtnSolar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://www.n3kl.org/sun/noaa.html");
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void imgbtnGeoMag_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://www.n3kl.org/sun/noaa.html");
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //protected void createAssetCharts(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        List<Int32> wellsDetails = CHRT.getClientRawQCSummary(GetClientDBString);
        //        jobCount = wellsDetails[0];
        //        wellCount = wellsDetails[1];
        //        mrmCount = wellsDetails[2];
        //        runCount = wellsDetails[3];
        //        mbhaCount = wellsDetails[4];
        //        this.divGuageJob.InnerHtml = "<h3>" + jobCount.ToString() + "</h3><span>Data Audit Job(s)</span>";
        //        this.divGuageWell.InnerHtml = "<h3>" + wellCount.ToString() + "</h3><span>Data Audit Well(s)</span>";
        //        this.divAscWell.InnerHtml = "<h3>" + mrmCount.ToString() + "</h3><span>Well(s) Missing Ref. Mags.</span>";
        //        this.divMRM.InnerHtml = "<h3>" + runCount.ToString() + "</h3><span>Data Audit Active Run</span>";
        //        this.divMsngBHA.InnerHtml = "<h3>" + mbhaCount.ToString() + "</h3><span>Active Run(s) Missing BHA Signature</span>";
        //    }
        //    catch (Exception ex)
        //    { throw new System.Exception(ex.ToString()); }
        //}
    }
}