﻿<%@ Page Title="SMARTs Panel" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="fesViewer.aspx.cs" Inherits="SmartsVer1.Viewer.fesViewer" %>

<%@ Register Src="~/Control/Viewer/ctrlFESViewer.ascx" TagPrefix="uc1" TagName="ctrlFESViewer" %>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlFESViewer runat="server" ID="ctrlFESViewer" />
</asp:Content>
