﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Net.Mail;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Configuration;

namespace SmartsVer1
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.ScriptResourceMapping.AddDefinition("jquery", new ScriptResourceDefinition
                {
                    Path = "~/Scripts/jquery-3.1.1.min.js"
                });
                SqlServerTypes.Utilities.LoadNativeAssemblies(Server.MapPath("~/bin"));
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            try
            {
                if (AppConfiguration.SendMailOnError)
                {
                    if (HttpContext.Current.Server.GetLastError() != null)
                    {
                        //SMTP Credentials
                        String SMTP_USERNAME = WebConfigurationManager.AppSettings["ErrorUser"];
                        String SMTP_PASSWORD = WebConfigurationManager.AppSettings["ErrorPassword"];

                        //SMTP Host
                        String SMTP_HOST = WebConfigurationManager.AppSettings["SmtpHost"];
                        //SMTP PORT
                        int SMTP_PORT = Convert.ToInt32(WebConfigurationManager.AppSettings["SmtpPort"]);

                        Exception myEx = HttpContext.Current.Server.GetLastError().GetBaseException();
                        String mailSubject = "Error in website " + Request.Url.ToString();
                        String mailMessage = String.Empty;
                        mailMessage += "<strong>Message</strong><br / >" + myEx.Message + "<br />";
                        mailMessage += "<strong>Stack Trace</strong><br />" + myEx.StackTrace + "<br />";
                        mailMessage += "<strong>Query String</strong><br />" + Request.QueryString.ToString() + "<br />";

                        MailMessage webMessage = new MailMessage(AppConfiguration.ErrorFromAddress, AppConfiguration.ErrorToAddress, mailSubject, mailMessage);

                        webMessage.IsBodyHtml = true;

                        //Creating new SMTP Client
                        SmtpClient feSmtp = new SmtpClient(SMTP_HOST, SMTP_PORT);
                        //Creating new SMTP Network Credentials
                        feSmtp.Credentials = new System.Net.NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);
                        //Creating Secure connection - false for testing
                        feSmtp.EnableSsl = true;
                        //Sending email
                        feSmtp.Send(webMessage);
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Session_End(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected void Application_End(object sender, EventArgs e)
        {
            try
            { }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}