﻿<%@ Page Title="Non-Magnetic Space Calculator" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="fesDRNonMag_2A.aspx.cs" Inherits="SmartsVer1.Utilities.fesDRNonMag_2A" %>

<%@ Register Src="~/Control/Utils/ctrlFESNonMag.ascx" TagPrefix="uc1" TagName="ctrlFESNonMag" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlFESNonMag runat="server" ID="ctrlFESNonMag" />
</asp:Content>
