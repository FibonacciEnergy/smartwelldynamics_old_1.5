﻿<%@ Page Title="Magnetic Storm Survey Rectifier" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="fecCRDMagStorm_2A.aspx.cs" Inherits="SmartsVer1.Utilities.fecCRDMagStorm_2A" %>

<%@ Register Src="~/Control/Utils/ctrlFECMagStorm.ascx" TagPrefix="uc1" TagName="ctrlFECMagStorm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlFECMagStorm runat="server" ID="ctrlFECMagStorm" />
</asp:Content>
