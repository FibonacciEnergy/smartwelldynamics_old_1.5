﻿<%@ Page Title="BHA Non-Mag Space Calculator" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="fecCRDNonMag_2A.aspx.cs" Inherits="SmartsVer1.Utilities.fecCRDNonMag_2A" %>

<%@ Register Src="~/Control/Utils/ctrlFECNonMag.ascx" TagPrefix="uc1" TagName="ctrlFECNonMag" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlFECNonMag runat="server" ID="ctrlFECNonMag" />
</asp:Content>
