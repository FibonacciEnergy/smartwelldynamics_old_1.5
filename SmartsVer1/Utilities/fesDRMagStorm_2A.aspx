﻿<%@ Page Title="Magnetic Storm Survey Rectifier" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="fesDRMagStorm_2A.aspx.cs" Inherits="SmartsVer1.Utilities.fesDRMagStorm_2A" %>

<%@ Register Src="~/Control/Utils/ctrlFESMagStorm.ascx" TagPrefix="uc1" TagName="ctrlFESMagStorm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlFESMagStorm runat="server" ID="ctrlFESMagStorm" />
</asp:Content>
