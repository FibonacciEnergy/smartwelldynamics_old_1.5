﻿<%@ Page Title="Unit(s) Converters" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="feUnitConverter_2A.aspx.cs" Inherits="SmartsVer1.Utilities.feUnitConverter_2A" %>

<%@ Register Src="~/Control/Utils/ctrlFESUnitConv.ascx" TagPrefix="uc1" TagName="ctrlFESUnitConv" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlFESUnitConv runat="server" ID="ctrlFESUnitConv" />
</asp:Content>
