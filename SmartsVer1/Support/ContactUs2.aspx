﻿<%@ Page Title="SMARTs Contact" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="ContactUs2.aspx.cs" Inherits="SmartsVer1.Support.ContactUs2" %>

<%@ Register Src="~/Control/Support/ctrlContactUs2.ascx" TagPrefix="uc1" TagName="ctrlContactUs2" %>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlContactUs2 runat="server" ID="ctrlContactUs2" />
</asp:Content>