﻿<%@ Page Title="SMARTs Visualisations" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="SmartAnalysisCharts2.aspx.cs" Inherits="SmartsVer1.Support.SmartAnalysisCharts2" %>

<%@ Register Src="~/Control/Support/ctrlAnalysisCharts2.ascx" TagPrefix="uc1" TagName="ctrlAnalysisCharts2" %>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlAnalysisCharts2 runat="server" id="ctrlAnalysisCharts2" />
</asp:Content>
