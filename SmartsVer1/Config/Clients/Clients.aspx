﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SMARTs.Master" AutoEventWireup="true" CodeBehind="Clients.aspx.cs" Inherits="SmartsVer1.Config.Clients.Clients" %>

<%@ Register Src="~/Control/MenuItems/Clients/ctrlClientCRDMenu.ascx" TagPrefix="uc1" TagName="ctrlClientCRDMenu" %>
<%@ Register Src="~/Control/Config/Clients/ctrlClients.ascx" TagPrefix="uc2" TagName="ctrlClients" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="NavigationContent" runat="server">
    <uc1:ctrlClientCRDMenu runat="server" ID="ctrlClientCRDMenu" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="BodyContent" runat="server">
    <uc2:ctrlClients runat="server" ID="ctrlClients" />
</asp:Content>
