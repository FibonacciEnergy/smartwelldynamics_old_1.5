﻿<%@ Page Title="Non-Magnetic Space Calculator Configuration" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="NonMagCalc_2A.aspx.cs" Inherits="SmartsVer1.Config.NonMagCalc.NonMagCalc_2A" %>

<%@ Register Src="~/Control/Config/NonMagCalc/ctrlNMCalc.ascx" TagPrefix="uc1" TagName="ctrlNMCalc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlNMCalc runat="server" ID="ctrlNMCalc" />
</asp:Content>
