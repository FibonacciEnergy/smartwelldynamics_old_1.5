﻿<%@ Page Title="Measurements Units" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="fesUnits_2A.aspx.cs" Inherits="SmartsVer1.Config.Units.fesUnits_2A" %>

<%@ Register Src="~/Control/Config/Units/ctrlFEUnits.ascx" TagPrefix="uc1" TagName="ctrlFEUnits" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlFEUnits runat="server" ID="ctrlFEUnits" />
</asp:Content>
