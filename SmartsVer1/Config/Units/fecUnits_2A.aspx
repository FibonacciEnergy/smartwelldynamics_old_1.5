﻿<%@ Page Title="Measurement Units" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="fecUnits_2A.aspx.cs" Inherits="SmartsVer1.Config.Units.fecUnits_2A" %>

<%@ Register Src="~/Control/Config/Units/ctrlClientUnits.ascx" TagPrefix="uc1" TagName="ctrlClientUnits" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlClientUnits runat="server" ID="ctrlClientUnits" />
</asp:Content>
