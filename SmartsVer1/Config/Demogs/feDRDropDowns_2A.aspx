﻿<%@ Page Title="DropDowns Parameters" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="feDRDropDowns_2A.aspx.cs" Inherits="SmartsVer1.Config.Demogs.feDRDropDowns_2A" %>

<%@ Register Src="~/Control/Config/Demogs/ctrlFEDRDropDowns.ascx" TagPrefix="uc1" TagName="ctrlFEDRDropDowns" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlFEDRDropDowns runat="server" ID="ctrlFEDRDropDowns" />
</asp:Content>
