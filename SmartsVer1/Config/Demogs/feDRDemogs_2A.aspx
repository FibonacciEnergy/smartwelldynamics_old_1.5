﻿<%@ Page Title="Demographics" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="feDRDemogs_2A.aspx.cs" Inherits="SmartsVer1.Config.Demogs.feDRDemogs_2A" %>

<%@ Register Src="~/Control/Config/Demogs/ctrlFEDRDemogs.ascx" TagPrefix="uc1" TagName="ctrlFEDRDemogs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlFEDRDemogs runat="server" ID="ctrlFEDRDemogs" />
</asp:Content>
