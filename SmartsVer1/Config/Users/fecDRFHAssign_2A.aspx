﻿<%@ Page Title="Fieldhands Assignments" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="fecDRFHAssign_2A.aspx.cs" Inherits="SmartsVer1.Config.Users.fecDRFHAssign_2A" %>

<%@ Register Src="~/Control/Config/Users/ctrlFHAssignments.ascx" TagPrefix="uc1" TagName="ctrlFHAssignments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlFHAssignments runat="server" ID="ctrlFHAssignments" />
</asp:Content>
