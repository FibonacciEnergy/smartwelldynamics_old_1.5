﻿<%@ Page Title="Work Locations" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="fecClientLoc_2A.aspx.cs" Inherits="SmartsVer1.Config.Locations.fecClientLoc_2A" %>

<%@ Register Src="~/Control/Config/Location/ctrlClientLocations.ascx" TagPrefix="uc1" TagName="ctrlClientLocations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlClientLocations runat="server" ID="ctrlClientLocations" />
</asp:Content>
