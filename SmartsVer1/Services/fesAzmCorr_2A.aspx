﻿<%@ Page Title="SMARTs Azimuth Corrections" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="fesAzmCorr_2A.aspx.cs" Inherits="SmartsVer1.Services.fesAzmCorr_2A" %>

<%@ Register Src="~/Control/Services/ctrlAzmCorrQC.ascx" TagPrefix="uc1" TagName="ctrlAzmCorrQC" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlAzmCorrQC runat="server" ID="ctrlAzmCorrQC" />
</asp:Content>
