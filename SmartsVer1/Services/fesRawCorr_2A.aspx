﻿<%@ Page Title="SMARTs Multi-Station Analysis" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="fesRawCorr_2A.aspx.cs" Inherits="SmartsVer1.Services.fesRawCorr_2A" %>

<%@ Register Src="~/Control/Services/ctrlCorrQC.ascx" TagPrefix="uc1" TagName="ctrlCorrQC" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlCorrQC runat="server" ID="ctrlCorrQC" />
</asp:Content>
