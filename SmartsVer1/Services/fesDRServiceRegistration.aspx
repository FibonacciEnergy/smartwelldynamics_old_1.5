﻿<%@ Page Title="Service Registration" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="fesDRServiceRegistration.aspx.cs" Inherits="SmartsVer1.Services.fesDRServiceRegistration" %>

<%@ Register Src="~/Control/Services/ctrlServiceRegistration.ascx" TagPrefix="uc1" TagName="ctrlServiceRegistration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlServiceRegistration runat="server" id="ctrlServiceRegistration" />
</asp:Content>
