﻿<%@ Page Title="Data Input Report" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="DataInputReport_2A.aspx.cs" Inherits="SmartsVer1.Reports.QC.DataInputReport_2A" %>

<%@ Register Src="~/Control/Reports/QC/ctrlDataInput.ascx" TagPrefix="uc1" TagName="ctrlDataInput" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlDataInput runat="server" ID="ctrlDataInput" />
</asp:Content>
