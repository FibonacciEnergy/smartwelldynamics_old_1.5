﻿<%@ Page Title="Data Audit/QC Report" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="QCReport_2A.aspx.cs" Inherits="SmartsVer1.Reports.QC.QCReport_2A" %>

<%@ Register Src="~/Control/Reports/QC/ctrlDataAudit.ascx" TagPrefix="uc1" TagName="ctrlDataAudit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlDataAudit runat="server" ID="ctrlDataAudit" />
</asp:Content>
