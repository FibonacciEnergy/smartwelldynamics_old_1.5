﻿<%@ Page Title="SMARTs Reports" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="ReviewerReport_2A.aspx.cs" Inherits="SmartsVer1.Reports.Reviewer.ReviewerReport_2A" %>

<%@ Register Src="~/Control/Reports/Reviewer/ctrlRWRReports.ascx" TagPrefix="uc1" TagName="ctrlRWRReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlRWRReports runat="server" ID="ctrlRWRReports" />
</asp:Content>
