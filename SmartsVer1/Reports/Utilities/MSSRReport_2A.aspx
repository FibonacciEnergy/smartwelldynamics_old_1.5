﻿<%@ Page Title="Magnetic Storm Survey Rectifier Report" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="MSSRReport_2A.aspx.cs" Inherits="SmartsVer1.Reports.Utilities.MSSRReport_2A" %>

<%@ Register Src="~/Control/Reports/Utilities/ctrlMSSRReport.ascx" TagPrefix="uc1" TagName="ctrlMSSRReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlMSSRReport runat="server" ID="ctrlMSSRReport" />
</asp:Content>
