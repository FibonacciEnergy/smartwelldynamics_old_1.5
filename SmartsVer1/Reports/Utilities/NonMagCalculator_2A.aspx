﻿<%@ Page Title="BHA Non-Magnetic Space Calculator Report" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="NonMagCalculator_2A.aspx.cs" Inherits="SmartsVer1.Reports.Utilities.NonMagCalculator_2A" %>

<%@ Register Src="~/Control/Reports/Utilities/ctrlNonMagReport.ascx" TagPrefix="uc1" TagName="ctrlNonMagReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlNonMagReport runat="server" ID="ctrlNonMagReport" />
</asp:Content>
