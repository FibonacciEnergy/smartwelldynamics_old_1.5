﻿<%@ Page Title="Well Profile Report" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="WPReport_2A.aspx.cs" Inherits="SmartsVer1.Reports.WP.WPReport_2A" %>

<%@ Register Src="~/Control/Reports/WellProfile/ctrlWPReport.ascx" TagPrefix="uc1" TagName="ctrlWPReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlWPReport runat="server" ID="ctrlWPReport" />
</asp:Content>
