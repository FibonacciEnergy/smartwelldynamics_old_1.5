﻿<%@ Page Title="Well(s)/Lateral(s) Report" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="Well_2A.aspx.cs" Inherits="SmartsVer1.Reports.Assets.Well_2A" %>

<%@ Register Src="~/Control/Reports/Assets/ctrlWellReport.ascx" TagPrefix="uc1" TagName="ctrlWellReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlWellReport runat="server" id="ctrlWellReport" />
</asp:Content>
