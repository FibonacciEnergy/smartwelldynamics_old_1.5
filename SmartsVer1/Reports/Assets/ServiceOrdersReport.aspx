﻿<%@ Page Title="Service Orders Report(s)" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="ServiceOrdersReport.aspx.cs" Inherits="SmartsVer1.Reports.Assets.ServiceOrdersReport" %>

<%@ Register Src="~/Control/Reports/Assets/ctrlJobsReport.ascx" TagPrefix="uc1" TagName="ctrlJobsReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlJobsReport runat="server" ID="ctrlJobsReport" />
</asp:Content>
