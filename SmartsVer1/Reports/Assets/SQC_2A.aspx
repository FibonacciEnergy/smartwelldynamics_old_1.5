﻿<%@ Page Title="Survey Qualification Criteria" Language="C#" MasterPageFile="~/SMART2.Master" AutoEventWireup="true" CodeBehind="SQC_2A.aspx.cs" Inherits="SmartsVer1.Reports.Assets.SQC_2A" %>

<%@ Register Src="~/Control/Reports/Assets/ctrlSQCReport.ascx" TagPrefix="uc1" TagName="ctrlSQCReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
    <uc1:ctrlSQCReport runat="server" ID="ctrlSQCReport" />
</asp:Content>
