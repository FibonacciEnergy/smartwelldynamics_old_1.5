﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.SqlServer.Types;
using System.Web.Configuration;
using SRV = SmartsVer1.Helpers.DemogHelpers.ServicesConfig;

namespace SmartsVer1.Helpers.SalesHelpers
{
    public class Sales
    {
        void main()
        {
        }

        public static Int32 deleteSalesCampaignPrice(Int32 SalesCampaignPriceID)
        {
            try
            {
                Int32 iReply = -99;
                String delData = "DELETE FROM [CampaignPrices] WHERE [cpID] = @cpID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESalesDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delCmd = new SqlCommand(delData, dataCon);
                        delCmd.Parameters.AddWithValue("@cpID", SqlDbType.Int).Value = SalesCampaignPriceID.ToString();
                        iReply = Convert.ToInt32(delCmd.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getCampaignList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [scID], [scName] FROM [SalesCampaign] ORDER BY [scName] ASC";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESalesDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                                else
                                {
                                    iReply.Add(-1, "Not found");
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getCampaignNameFromID(Int32 CampaignID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [scName] FROM [SalesCampaign] WHERE [scID] = @scID";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESalesDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@scID", SqlDbType.Int).Value = CampaignID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                                else
                                {
                                    iReply = "-1";
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getCampaignTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn scID = iReply.Columns.Add("scID", typeof(Int32));
                DataColumn scName = iReply.Columns.Add("scName", typeof(String));
                DataColumn minContract = iReply.Columns.Add("minContract", typeof(String));
                DataColumn minWell = iReply.Columns.Add("minWell", typeof(String));
                DataColumn cStart = iReply.Columns.Add("cStart", typeof(DateTime));
                DataColumn cEnd = iReply.Columns.Add("cEnd", typeof(DateTime));
                DataColumn cEndClr = iReply.Columns.Add("cEndClr", typeof(String));
                DataColumn cPromotion = iReply.Columns.Add("cPromotion", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                Int32 id = -99, Ctrt = -99, Well = -99, Pro = -99;
                String name = String.Empty, mC = String.Empty, mW = String.Empty, promotion = String.Empty, color = String.Empty;
                DateTime start = DateTime.Now, end = DateTime.Now, update = DateTime.Now;                
                String selData = "SELECT [scID], [scName], [minContract], [minWell], [cStart], [cEnd], [cPromotion], [uTime] FROM [SalesCampaign] ORDER BY [cEnd] DESC";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESalesDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        Ctrt = readData.GetInt32(2);
                                        if (Ctrt.Equals(0))
                                        {
                                            mC = "---";
                                        }
                                        else
                                        {
                                            mC = Convert.ToString(Ctrt);
                                        }
                                        Well = readData.GetInt32(3);
                                        if (Well.Equals(0))
                                        {
                                            mW = "---";
                                        }
                                        else
                                        {
                                            mW = Convert.ToString(Well);
                                        }
                                        start = readData.GetDateTime(4);
                                        end = readData.GetDateTime(5);
                                        if (end < DateTime.Now.AddMonths(2))
                                        {
                                            color = "Yellow";
                                        }
                                        else
                                        {
                                            color = "Green";
                                        }
                                        Pro = readData.GetInt32(6);
                                        if (Pro.Equals(0))
                                        {
                                            promotion = String.Format("{0} - ({1})", Pro, "Contract Length");
                                        }
                                        else
                                        {
                                            promotion = String.Format("{0} - ({1})", Pro, "Months");
                                        }
                                        update = readData.GetDateTime(7);
                                        iReply.Rows.Add(id, name, mC, mW, start, end, color, promotion, update);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal getServicePricerReducer(Int32 ServiceGroupID, Int32 ServiceID, Int32 SalesCampaingID)
        {
            try
            {
                Decimal iReply = -99.00M;
                String selData = "SELECT [cpReducer] FROM [CampaignPrices] WHERE [srvGrpID] = @srvGrpID AND [srvID] = @srvID AND [scID] = @scID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESalesDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@srvGrpID", SqlDbType.Int).Value = ServiceGroupID.ToString();
                        getData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        getData.Parameters.AddWithValue("@scID", SqlDbType.Int).Value = SalesCampaingID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetDecimal(0);
                                    }
                                }
                                else
                                {
                                    iReply = -99.00M;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static System.Data.DataTable getPriceTable(Int32 CampaignID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn cpID = iReply.Columns.Add("cpID", typeof(Int32));
                DataColumn srvGrpID = iReply.Columns.Add("srvGrpID", typeof(String));
                DataColumn srvID = iReply.Columns.Add("srvID", typeof(String));
                DataColumn cpReducer = iReply.Columns.Add("cpReducer", typeof(String));
                DataColumn stdPrice = iReply.Columns.Add("stdPrice", typeof(String));
                DataColumn proPrice = iReply.Columns.Add("proPrice", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                Int32 id = -99, gpID = -99, svID = -99;
                String reducer = String.Empty, std = String.Empty, promo = String.Empty, groupName = String.Empty, serviceName = String.Empty, labelStd = String.Empty, labelPromo = String.Empty;
                DateTime uT = DateTime.Now;
                Decimal priceRed = -99.00M, priceStd = -99.00M, pricePromo = -99.00M;
                String selData = "SELECT [cpID], [srvGrpID], [srvID], [cpReducer], [uTime] FROM [CampaignPrices] WHERE [scID] = @scID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESalesDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@scID", SqlDbType.Int).Value = CampaignID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        gpID = readData.GetInt32(1);
                                        svID = readData.GetInt32(2);
                                        priceRed = readData.GetDecimal(3);
                                        reducer = String.Format("{0} {1}", priceRed, "%");
                                        uT = readData.GetDateTime(4);
                                        groupName = SRV.getSrvGroupName(gpID);
                                        serviceName = SRV.getServiceName(svID);
                                        List<String> stdPrices = SRV.getStandardPriceWithUnitName(gpID, svID);
                                        std = String.Format("{0} {1}", Convert.ToString(stdPrices[0]), Convert.ToString(stdPrices[1]));
                                        priceStd = Convert.ToDecimal(stdPrices[0]);
                                        pricePromo = Decimal.Subtract(priceStd, Decimal.Multiply(priceStd, Decimal.Divide(priceRed, 100)));
                                        promo = String.Format("{0} {1}", Convert.ToString(pricePromo), Convert.ToString(stdPrices[1]));
                                        iReply.Rows.Add(id, groupName, serviceName, reducer, std, promo, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getPriceTableForCampaign(Int32 CampaignID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn cpID = iReply.Columns.Add("cpID", typeof(Int32));
                DataColumn srvGrpID = iReply.Columns.Add("srvGrpID", typeof(String));
                DataColumn srvID = iReply.Columns.Add("srvID", typeof(Int32));
                DataColumn srvName = iReply.Columns.Add("srvName", typeof(String));
                DataColumn cpReducer = iReply.Columns.Add("cpReducer", typeof(String));
                DataColumn stdPrice = iReply.Columns.Add("stdPrice", typeof(String));
                DataColumn proPrice = iReply.Columns.Add("proPrice", typeof(Decimal));
                DataColumn unitID = iReply.Columns.Add("unitID", typeof(Int32));
                DataColumn priceUnit = iReply.Columns.Add("priceUnit", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                Int32 id = -99, gpID = -99, svID = -99, scuID = -99;
                String reducer = String.Empty, std = String.Empty, groupName = String.Empty, serviceName = String.Empty, labelStd = String.Empty, labelPromo = String.Empty, pUnit = String.Empty;
                DateTime uT = DateTime.Now;
                Decimal priceRed = -99.00M, priceStd = -99.00M, pricePromo = -99.00M, promo = -99.00M;
                String selData = "SELECT [cpID], [srvGrpID], [srvID], [cpReducer], [uTime] FROM [CampaignPrices] WHERE [scID] = @scID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESalesDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@scID", SqlDbType.Int).Value = CampaignID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        gpID = readData.GetInt32(1);
                                        svID = readData.GetInt32(2);
                                        priceRed = readData.GetDecimal(3);
                                        reducer = String.Format("{0} {1}", priceRed, "%");
                                        uT = readData.GetDateTime(4);
                                        groupName = SRV.getSrvGroupName(gpID);
                                        serviceName = SRV.getServiceName(svID);
                                        List<String> stdPrices = SRV.getStandardPriceWithUnitName(gpID, svID);
                                        std = String.Format("{0} {1}", Convert.ToString(stdPrices[0]), Convert.ToString(stdPrices[1]));
                                        priceStd = Convert.ToDecimal(stdPrices[0]);
                                        pricePromo = Decimal.Subtract(priceStd, Decimal.Multiply(priceStd, Decimal.Divide(priceRed, 100)));
                                        promo = Convert.ToDecimal(pricePromo);
                                        pUnit = Convert.ToString(stdPrices[1]);
                                        scuID = SRV.getServiceChargeUnitID(pUnit);
                                        iReply.Rows.Add(id, groupName, svID, serviceName, reducer, std, promo, scuID, pUnit, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertSalesCampaignPrice(Int32 CampaignID, Int32 ServiceGroupID, Int32 ServiceID, Decimal PriceReducer, String UserName, String UserRealm)
        {
            try
            {
                Int32 iReply = -99;
                String insData = "INSERT INTO [CampaignPrices] ([scID], [srvGrpID], [srvID], [cpReducer], [cTime], [uTime], [username], [realm]) VALUES (@scID, @srvGrpID, @srvID, @cpReducer, @cTime, @uTime, @username, @realm)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESalesDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand addData = new SqlCommand(insData, dataCon);
                        addData.Parameters.AddWithValue("@scID", SqlDbType.Int).Value = CampaignID.ToString();
                        addData.Parameters.AddWithValue("@srvGrpID", SqlDbType.Int).Value = ServiceGroupID.ToString();
                        addData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        addData.Parameters.AddWithValue("@cpReducer", SqlDbType.Decimal).Value = PriceReducer.ToString();
                        addData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        addData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        addData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        addData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserRealm.ToString();

                        iReply = Convert.ToInt32(addData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertSalesCampaign(String CampaignName, Int32 MinimumContractLength, Int32 MinimumWellsRequired, DateTime CampaignStart, DateTime CampaignEnd, Int32 PromotionMonths, String UserName, String UserRealm)
        {
            try
            {
                Int32 iReply = -99;
                String insData = "INSERT INTO [SalesCampaign] ([scName], [minContract], [minWell], [cStart], [cEnd], [cPromotion], [cTime], [uTime], [username], [realm]) VALUES (@scName, @minContract, @minWell, @cStart, @cEnd, @cPromotion, @cTime, @uTime, @username, @realm)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESalesDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand addData = new SqlCommand(insData, dataCon);
                        addData.Parameters.AddWithValue("@scName", SqlDbType.NVarChar).Value = CampaignName.ToString();
                        addData.Parameters.AddWithValue("@minContract", SqlDbType.Int).Value = MinimumContractLength.ToString();
                        addData.Parameters.AddWithValue("@minWell", SqlDbType.Int).Value = MinimumWellsRequired.ToString();
                        addData.Parameters.AddWithValue("@cStart", SqlDbType.DateTime2).Value = CampaignStart.ToString();
                        addData.Parameters.AddWithValue("@cEnd", SqlDbType.DateTime2).Value = CampaignEnd.ToString();
                        addData.Parameters.AddWithValue("@cPromotion", SqlDbType.Int).Value = PromotionMonths.ToString();
                        addData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        addData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        addData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        addData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserRealm.ToString();
                        iReply = Convert.ToInt32(addData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteContractedPriceRow(Int32 ContractID, Int32 SalePriceID)
        {
            String selData = "DELETE FROM [ServicePrice] WHERE [ctrtID] = @ctrtID AND [spID] = @spID";
            using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
            {
                try
                {
                    dataCon.Open();
                    SqlCommand delData = new SqlCommand(selData, dataCon);
                    delData.Parameters.AddWithValue("@ctrtID", SqlDbType.Int).Value = ContractID.ToString();
                    delData.Parameters.AddWithValue("@spID", SqlDbType.Int).Value = SalePriceID.ToString();
                    return Convert.ToInt32(delData.ExecuteNonQuery());
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
                finally
                {
                    dataCon.Close();
                    dataCon.Dispose();
                }
            }
        }
    }
}