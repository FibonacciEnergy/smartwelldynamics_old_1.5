﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections;
using System.Globalization;
using System.Data.SqlClient;
using Microsoft.SqlServer.Types;
using System.Web.Configuration;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using USR = SmartsVer1.Helpers.AccountHelpers.CreateUser;
using MAP = SmartsVer1.Helpers.ChartHelpers.MapBox;

namespace SmartsVer1.Helpers.SalesHelpers
{
    public class Leads
    {
        void main()
        {}

        public static Int32 findDuplicateLeadCompany(String CompanyName, String CompanyDomain)
        {
            try
            {
                Int32 iReply = -99, nCount = -99, dCount = -99;
                String lowerCompanyName = CompanyName.ToLower();
                lowerCompanyName.Replace(" ", String.Empty);
                String lowerCompanyDomain = CompanyDomain.ToLower();
                lowerCompanyDomain.Replace(" ", String.Empty);
                String selName = "SELECT COUNT(*) FROM [LeadCompany] WHERE [coName] = @coName";
                //String selDomain = "SELECT COUNT(*) FROM [LeadCompany] WHERE [coWebURL] = @coWebURL"; 
                TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getName = new SqlCommand(selName, dataCon);
                        getName.Parameters.AddWithValue("@coName", SqlDbType.NVarChar).Value = lowerCompanyName.ToString();
                        using (SqlDataReader readData = getName.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        nCount = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    nCount = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                        //SqlCommand getDomain = new SqlCommand(selDomain, dataCon);
                        //getDomain.Parameters.AddWithValue("@coWebURL", SqlDbType.NVarChar).Value = lowerCompanyDomain.ToString();
                        //using (SqlDataReader readDomain = getDomain.ExecuteReader())
                        //{
                        //    try
                        //    {
                        //        if (readDomain.HasRows)
                        //        {
                        //            while (readDomain.Read())
                        //            {
                        //                dCount = readDomain.GetInt32(0);
                        //            }
                        //        }
                        //        else
                        //        {
                        //            dCount = 0;
                        //        }
                        //    }
                        //    catch (Exception ex)
                        //    { throw new System.Exception(ex.ToString()); }
                        //    finally
                        //    {
                        //        readDomain.Close();
                        //        readDomain.Dispose();
                        //    }
                        //}
                        //if (nCount.Equals(0) && dCount.Equals(0))
                        if(nCount.Equals(0))                        
                        {
                            iReply = 0; // No Duplicate found
                        }
                        else
                        {
                            iReply = -1; // Duplicate found
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 findDuplicateEmail(String LeadEmail)
        {
            try
            {
                Int32 iReply = -99, nCount = -99;
                String lowerEmail = LeadEmail.ToLower();
                lowerEmail.Replace(" ", String.Empty);
                String selName = "SELECT COUNT(*) FROM [LeadEmail] WHERE [leadEmail] = @leadEmail";
                //String selDomain = "SELECT COUNT(*) FROM [LeadCompany] WHERE [coWebURL] = @coWebURL"; 
                TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getName = new SqlCommand(selName, dataCon);
                        getName.Parameters.AddWithValue("@leadEmail", SqlDbType.NVarChar).Value = LeadEmail.ToString();
                        using (SqlDataReader readData = getName.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        nCount = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    nCount = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                        if (nCount.Equals(0))
                        {
                            iReply = 0; // No Duplicate found
                        }
                        else
                        {
                            iReply = -1; // Duplicate found
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 findDuplicatePhone(String PhoneNumber)
        {
            try
            {
                Int32 iReply = -99, nCount = -99;
                String lowerPhone = PhoneNumber.ToLower();
                lowerPhone.Replace(" ", String.Empty);
                String selName = "SELECT COUNT(*) FROM [LeadPhoneNumber] WHERE [phValue] = @phValue";
                TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getName = new SqlCommand(selName, dataCon);
                        getName.Parameters.AddWithValue("@phValue", SqlDbType.NVarChar).Value = PhoneNumber.ToString();
                        using (SqlDataReader readData = getName.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        nCount = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    nCount = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                        if (nCount.Equals(0))
                        {
                            iReply = 0; // No Duplicate found
                        }
                        else
                        {
                            iReply = -1; // Duplicate found
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getAddressIDListByState(Int32 StateID)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                List<Int32> addressIDList = new List<Int32>();
                String selAddID = "SELECT [addID] FROM [StreetAddress] WHERE [stID] = @stID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selAddID, dataCon);
                        getData.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = StateID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply.Add(readData.GetInt32(0));
                                    }
                                }
                                else
                                {
                                    iReply.Add(0);
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getCompaniesAddressTable(Int32 CompanyID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn addID = iReply.Columns.Add("addID", typeof(Int32));
                DataColumn title = iReply.Columns.Add("title", typeof(String));
                DataColumn st1 = iReply.Columns.Add("st1", typeof(String));
                DataColumn st2 = iReply.Columns.Add("st2", typeof(String));
                DataColumn st3 = iReply.Columns.Add("st3", typeof(String));
                DataColumn cityID = iReply.Columns.Add("cityID", typeof(String));
                DataColumn stID = iReply.Columns.Add("stID", typeof(String));
                DataColumn zipID = iReply.Columns.Add("zipID", typeof(String));
                DataColumn ctyID = iReply.Columns.Add("ctyID", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                Int32 id = -99, ctID = -99, sID = -99, zID = -99, ciID = -99;
                String name = String.Empty, city = String.Empty, country = String.Empty, state = String.Empty, zip = String.Empty, s1 = String.Empty, s2 = String.Empty, s3 = String.Empty;
                DateTime uT = DateTime.Now;
                String selData = "SELECT [addID], [title], [st1], [st2], [st3], [cityID], [stID], [zipID], [ctyID], [uTime] FROM [StreetAddress] WHERE [addID] IN (SELECT [addID] FROM [LeadCompanyToAddress] WHERE [coID] = @coID) ORDER BY [title]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@coID", SqlDbType.Int).Value = CompanyID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        s1 = readData.GetString(2);
                                        s2 = readData.GetString(3);
                                        if (String.IsNullOrEmpty(s2))
                                        {
                                            s2 = "---";
                                        }
                                        s3 = readData.GetString(4);
                                        if (String.IsNullOrEmpty(s3))
                                        {
                                            s3 = "---";
                                        }
                                        ciID = readData.GetInt32(5);
                                        city = DMG.getCityName(ciID);
                                        sID = readData.GetInt32(6);
                                        state = DMG.getStateName(sID);
                                        zID = readData.GetInt32(7);
                                        zip = DMG.getZipCode(zID);
                                        ctID = readData.GetInt32(8);
                                        country = DMG.getCountryName(ctID);
                                        uT = readData.GetDateTime(9);
                                        iReply.Rows.Add(id, name, s1, s2, s3, city, state, zip, country, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();

                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply.DefaultView.Sort = "title";
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getPersonAddressTable(Int32 LeadPersonID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn addID = iReply.Columns.Add("addID", typeof(Int32));
                DataColumn title = iReply.Columns.Add("title", typeof(String));
                DataColumn st1 = iReply.Columns.Add("st1", typeof(String));
                DataColumn st2 = iReply.Columns.Add("st2", typeof(String));
                DataColumn st3 = iReply.Columns.Add("st3", typeof(String));
                DataColumn cityID = iReply.Columns.Add("cityID", typeof(String));
                DataColumn stID = iReply.Columns.Add("stID", typeof(String));
                DataColumn zipID = iReply.Columns.Add("zipID", typeof(String));
                DataColumn ctyID = iReply.Columns.Add("ctyID", typeof(String));
                DataColumn lati = iReply.Columns.Add("lati", typeof(Decimal));
                DataColumn longi = iReply.Columns.Add("longi", typeof(Decimal));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                Int32 id = -99, ctID = -99, sID = -99, zID = -99, ciID = -99;
                String name = String.Empty, city = String.Empty, country = String.Empty, state = String.Empty, zip = String.Empty, s1 = String.Empty, s2 = String.Empty, s3 = String.Empty;
                DateTime uT = DateTime.Now;
                Decimal latit = -99.00M, longid = -99.00M;
                String selData = "SELECT [addID], [title], [st1], [st2], [st3], [cityID], [stID], [zipID], [ctyID], [latitude], [longitude], [uTime] FROM [StreetAddress] WHERE [addID] IN (SELECT [addID] FROM [LeadPersonToAddress] WHERE [lpID] = @lpID) ORDER BY [title]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@lpID", SqlDbType.Int).Value = LeadPersonID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        s1 = readData.GetString(2);
                                        s2 = readData.GetString(3);
                                        if (String.IsNullOrEmpty(s2))
                                        {
                                            s2 = "---";
                                        }                                        
                                        s3 = readData.GetString(4);
                                        if (String.IsNullOrEmpty(s3))
                                        {
                                            s3 = "---";
                                        }
                                        ciID = readData.GetInt32(5);
                                        city = DMG.getCityName(ciID);
                                        sID = readData.GetInt32(6);
                                        state = DMG.getStateName(sID);
                                        zID = readData.GetInt32(7);
                                        zip = DMG.getZipCode(zID);
                                        ctID = readData.GetInt32(8);
                                        country = DMG.getCountryName(ctID);
                                        latit = readData.GetDecimal(9);
                                        longid = readData.GetDecimal(10);
                                        uT = readData.GetDateTime(11);
                                        iReply.Rows.Add(id, name, s1, s2, s3, city, state, zip, country, latit, longid, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();

                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }

                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getPersonEmailTable(Int32 LeadPersonID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn lemlID = iReply.Columns.Add("lemlID", typeof(Int32));
                DataColumn leadEmail = iReply.Columns.Add("leadEmail", typeof(String));
                DataColumn dmgID = iReply.Columns.Add("dmgID", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                Int32 id = -99, typeID = -99;
                String phone = String.Empty, eType = String.Empty;
                DateTime uT = DateTime.Now;
                String selData = "SELECT [lemlID], [leadEmail], [dmgID], [uTime] FROM [LeadEmail] WHERE [lemlID] IN (SELECT [lemlID] FROM [LeadPersonToEmail] WHERE [lpID] = @lpID)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@lpID", SqlDbType.Int).Value = LeadPersonID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        phone = readData.GetString(1);
                                        typeID = readData.GetInt32(2);
                                        eType = DMG.getDemogTypeName(typeID);
                                        uT = readData.GetDateTime(3);
                                        iReply.Rows.Add(id, phone, eType, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();

                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }

                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getPersonPhoneTable(Int32 LeadPersonID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn phID = iReply.Columns.Add("phID", typeof(Int32));
                DataColumn phValue = iReply.Columns.Add("phValue", typeof(String));
                DataColumn phExtension = iReply.Columns.Add("phExtension", typeof(String));
                DataColumn ptaType = iReply.Columns.Add("dmgID", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                Int32 id = -99, typeID = -99;
                String phone = String.Empty, extension = String.Empty, pType = String.Empty;
                DateTime uT = DateTime.Now;
                String selData = "SELECT [phID], [phValue], [phExtension], [dmgID], [uTime] FROM [LeadPhoneNumber] WHERE [phID] IN (SELECT [phID] FROM [LeadPersonToPhone] WHERE [lpID] = @lpID)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@lpID", SqlDbType.Int).Value = LeadPersonID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        phone = readData.GetString(1);
                                        extension = readData.GetString(2);
                                        typeID = readData.GetInt32(3);                                        
                                        pType = DMG.getDemogTypeName(typeID);
                                        uT = readData.GetDateTime(4);
                                        iReply.Rows.Add(id, phone, extension, pType, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();

                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }

                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getCompaniesList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;                
                String selData = "SELECT [coID], [coName] FROM [LeadCompany] ORDER BY [coName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getCompaniesListForState(Int32 StateID)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String name = String.Empty;
                List<Int32> coIDList = getCompanyIDListByState(StateID);                
                String selData = "SELECT [coName] FROM [LeadCompany] WHERE [coID] = @coID";
                foreach (var id in coIDList)
                {
                    using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                    {
                        try
                        {
                            dataCon.Open();
                            SqlCommand getData = new SqlCommand(selData, dataCon);
                            getData.Parameters.AddWithValue("@coID", SqlDbType.Int).Value = id.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            name = readData.GetString(0);
                                            iReply.Add(id, name);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            dataCon.Close();
                            dataCon.Dispose();
                        }
                    }
                }            
                iReply.OrderBy(iList => iReply.Values);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getCompaniesListWithCount()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99, count = -99;
                String name = String.Empty, value = String.Empty;
                String selData = "SELECT [coID], [coName] FROM [LeadCompany] ORDER BY [coName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        count = getLeadPersonCount(id);
                                        value = String.Format("{0} - ( Leads: {1} )",name, count);
                                        iReply.Add(id, value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getCompanyName(Int32 CompanyID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [coName] FROM [LeadCompany] WHERE [coID] = @coID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@coID", SqlDbType.Int).Value = CompanyID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getCompaniesTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn coID = iReply.Columns.Add("coID", typeof(Int32));
                DataColumn coName = iReply.Columns.Add("coName", typeof(String));
                DataColumn coWebURL = iReply.Columns.Add("coWebURL", typeof(String));
                DataColumn coLnkURL = iReply.Columns.Add("coLnkURL", typeof(String));
                DataColumn infID = iReply.Columns.Add("infID", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                Int32 id = -99, cF = -99;
                String name = String.Empty, wURL = String.Empty, lURL = String.Empty, follow = String.Empty;
                DateTime uT = DateTime.Now;
                String selData = "SELECT [coID], [coName], [coWebURL], [coLnkURL], [infID], [uTime] FROM [LeadCompany] ORDER BY [coName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        wURL = readData.GetString(2);
                                        if(string.IsNullOrEmpty(wURL))
                                        {
                                            wURL = "---";
                                        }
                                        lURL = readData.GetString(3);
                                        if (String.IsNullOrEmpty(lURL))
                                        {
                                            lURL = "---";
                                        }
                                        cF = readData.GetInt32(4);
                                        follow = getInFollowName(cF);
                                        uT = readData.GetDateTime(5);
                                        iReply.Rows.Add(id, name, wURL, lURL, follow, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply.DefaultView.Sort = "coName";
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getCompaniesTableForState(Int32 StateID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn coID = iReply.Columns.Add("coID", typeof(Int32));
                DataColumn coName = iReply.Columns.Add("coName", typeof(String));
                DataColumn coWebURL = iReply.Columns.Add("coWebURL", typeof(String));
                DataColumn coLnkURL = iReply.Columns.Add("coLnkURL", typeof(String));
                DataColumn infID = iReply.Columns.Add("infID", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                Int32 id = -99, cF = -99;
                String name = String.Empty, wURL = String.Empty, lURL = String.Empty, follow = String.Empty;
                DateTime uT = DateTime.Now;
                String selData = "Select [coID], [coName], [coWebURL], [coLnkURL], [infID], [uTime] FROM [LeadCompany] WHERE [coID] IN (SELECT [coID] FROM [LeadCompanyToAddress] WHERE [addID] IN (SELECT [addID] FROM [StreetAddress] WHERE [stID] = @stID))";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = StateID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        wURL = readData.GetString(2);
                                        if (String.IsNullOrEmpty(wURL))
                                        {
                                            wURL = "---";
                                        }
                                        lURL = readData.GetString(3);
                                        if (String.IsNullOrEmpty(lURL))
                                        {
                                            lURL = "---";
                                        }
                                        cF = readData.GetInt32(4);
                                        follow = getInFollowName(cF);
                                        uT = readData.GetDateTime(5);
                                        iReply.Rows.Add(id, name, wURL, lURL, follow, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply.DefaultView.Sort = "coName";
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getCompaniesTableByState(Int32 StateID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn coID = iReply.Columns.Add("coID", typeof(Int32));
                DataColumn coName = iReply.Columns.Add("coName", typeof(String));
                DataColumn coWebURL = iReply.Columns.Add("coWebURL", typeof(String));
                DataColumn coLnkURL = iReply.Columns.Add("coLnkURL", typeof(String));
                DataColumn infID = iReply.Columns.Add("infID", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                Int32 id = -99, cF = -99;
                String name = String.Empty, wURL = String.Empty, lURL = String.Empty, follow = String.Empty;
                DateTime uT = DateTime.Now;
                List<Int32> coIDList = getCompanyIDListByState(StateID);
                Int32 list1 = coIDList[0];
                if (list1.Equals(0))
                {
                    
                }
                else
                {
                    foreach (var i in coIDList)
                    {
                        String selData = "SELECT [coID], [coName], [coWebURL], [coLnkURL], [infID], [uTime] FROM [LeadCompany] WHERE [coID] = @coID ORDER BY [coName]";
                        using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                        {
                            try
                            {
                                dataCon.Open();
                                SqlCommand getData = new SqlCommand(selData, dataCon);
                                getData.Parameters.AddWithValue("@coID", SqlDbType.Int).Value = i.ToString();
                                using (SqlDataReader readData = getData.ExecuteReader())
                                {
                                    try
                                    {
                                        if (readData.HasRows)
                                        {
                                            while (readData.Read())
                                            {
                                                id = readData.GetInt32(0);
                                                name = readData.GetString(1);
                                                wURL = readData.GetString(2);
                                                if (string.IsNullOrEmpty(wURL))
                                                {
                                                    wURL = "---";
                                                }
                                                lURL = readData.GetString(3);
                                                if (String.IsNullOrEmpty(lURL))
                                                {
                                                    lURL = "---";
                                                }
                                                cF = readData.GetInt32(4);
                                                follow = getInFollowName(cF);
                                                uT = readData.GetDateTime(5);
                                                iReply.Rows.Add(id, name, wURL, lURL, follow, uT);
                                                iReply.AcceptChanges();
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    { throw new System.Exception(ex.ToString()); }
                                    finally
                                    {
                                        readData.Close();
                                        readData.Dispose();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                dataCon.Close();
                                dataCon.Dispose();
                            }
                        }
                    }
                }
                iReply.DefaultView.Sort = "coName";
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getCompanyIDListByState(Int32 StateID)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                List<Int32> addressIDList = getAddressIDListByState(StateID);
                Int32 firstID = addressIDList[0], id = -99;
                Boolean isInList;
                if (firstID.Equals(0))
                {
                    iReply.Add(0);
                }
                else
                {                    
                    foreach (var i in addressIDList)
                    {
                        String selData = "SELECT [coID] FROM [LeadCompanyToAddress] WHERE [addID] = @addID";
                        using(SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                        {
                            try
                            {
                                dataCon.Open();
                                SqlCommand getData = new SqlCommand(selData, dataCon);
                                getData.Parameters.AddWithValue("@addID", SqlDbType.Int).Value = i.ToString();
                                using (SqlDataReader readData = getData.ExecuteReader())
                                {
                                    try
                                    {
                                        if (readData.HasRows)
                                        {
                                            while (readData.Read())
                                            {
                                                id = readData.GetInt32(0);
                                                if (!iReply.Contains(id))
                                                {
                                                    iReply.Add(id);
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    { throw new System.Exception(ex.ToString()); }
                                    finally
                                    {
                                        readData.Close();
                                        readData.Dispose();
                                    }
                                }
                            }
                            catch(Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                dataCon.Close();
                                dataCon.Dispose();
                            }
                        }
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getGlobalLeadPersonList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99, comp = -99;
                String lName = String.Empty, fName = String.Empty, desig = String.Empty, coName = String.Empty, value = String.Empty;
                String selData = "SELECT [lpID], [lastName], [firstName], [lpDesignation], [coID] FROM [LeadPerson] WHERE [coID] = @coID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        lName = readData.GetString(1);
                                        fName = readData.GetString(2);
                                        desig = readData.GetString(3);
                                        comp = readData.GetInt32(4);
                                        coName = getCompanyName(comp);
                                        value = String.Format("{0} {1} - {2}, {3}", lName, fName, desig, coName);
                                        iReply.Add(id, value);
                                    }
                                }
                                else
                                {
                                    iReply.Add(-99, "No Values found");
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getGlobalLeadPersonTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn lpID = iReply.Columns.Add("lpID", typeof(Int32));
                DataColumn lastName = iReply.Columns.Add("lastName", typeof(String));
                DataColumn firstName = iReply.Columns.Add("firstName", typeof(String));
                DataColumn middleName = iReply.Columns.Add("middleName", typeof(String));
                DataColumn lpDesignation = iReply.Columns.Add("lpDesignation", typeof(String));
                DataColumn coID = iReply.Columns.Add("coID", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                Int32 id = -99, comp = -99;
                String lName = String.Empty, fName = String.Empty, mName = String.Empty, desig = String.Empty, company = String.Empty;
                DateTime uT = DateTime.Now;
                String selData = "SELECT [lpID], [lastName], [middleName], [firstName], [lpDesignation], [coID], [uTime] FROM [LeadPerson] ORDER BY [lastName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        lName = readData.GetString(1);
                                        fName = readData.GetString(2);
                                        mName = readData.GetString(3);
                                        desig = readData.GetString(4);
                                        comp = readData.GetInt32(5);
                                        company = getCompanyName(comp);
                                        uT = readData.GetDateTime(6);
                                        iReply.Rows.Add(id, lName, fName, mName, desig, company, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                                else
                                {
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply.DefaultView.Sort = "lastName";
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        } 

        public static Dictionary<Int32, String> getInFollowList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [infID], [infName] FROM [InFollowStatus] ORDER BY [infName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getInFollowName(Int32 LinkedInFollowID)
        {
            try
            {
                String iReply = String.Empty;                
                String selData = "SELECT [infName] FROM [InFollowStatus] WHERE [infID] = @infID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@infID", SqlDbType.Int).Value = LinkedInFollowID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);                                        
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getLeadNurtureTable(Int32 LeadCompanyID, Int32 LeadPersonID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn lnID = iReply.Columns.Add("lnID", typeof(Int32));
                DataColumn lnComment = iReply.Columns.Add("lnComment", typeof(String));
                DataColumn UserId = iReply.Columns.Add("UserId", typeof(String));
                DataColumn lnTimeStamp = iReply.Columns.Add("lnTimeStamp", typeof(DateTime));
                DataColumn ctID = iReply.Columns.Add("ctID", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                Int32 id = -99, cID = -99;
                Guid usrID = new Guid();
                String comment = String.Empty, uName = String.Empty, cType = String.Empty;
                DateTime ts = DateTime.Now, ut = DateTime.Now;
                String selData = "SELECT [lnID], [lnComment], [UserId], [lnTimeStamp], [ctID], [uTime] FROM [LeadNurturing] WHERE [coID] = @coID AND [lpID] = @lpID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@coID", SqlDbType.Int).Value = LeadCompanyID.ToString();
                        getData.Parameters.AddWithValue("@lpID", SqlDbType.Int).Value = LeadPersonID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        comment = readData.GetString(1);
                                        usrID = readData.GetGuid(2);
                                        uName = USR.getUserNameFromId(usrID);
                                        ts = readData.GetDateTime(3);
                                        cID = readData.GetInt32(4);
                                        cType = DMG.getContactValueFromID(cID);
                                        ut = readData.GetDateTime(5);
                                        iReply.Rows.Add(id, comment, uName, ts, cType, ut);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getLeadPersonCount(Int32 LeadCompanyID)
        {
            try
            {
                Int32 iReply = -99;
                String selCount = "SELECT COUNT(lpID) FROM [LeadPerson] WHERE [coID] = @coID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selCount, dataCon);
                        getData.Parameters.AddWithValue("@coID", SqlDbType.Int).Value = LeadCompanyID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static Dictionary<Int32, String> getLeadPersonList(Int32 LeadCompanyID)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String lName = String.Empty, fName = String.Empty, desig = String.Empty, value = String.Empty;
                String selData = "SELECT [lpID], [lastName], [firstName], [lpDesignation] FROM [LeadPerson] WHERE [coID] = @coID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@coID", SqlDbType.Int).Value = LeadCompanyID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        lName = readData.GetString(1);
                                        fName = readData.GetString(2);
                                        desig = readData.GetString(3);
                                        value = String.Format("{0} {1}, {2}", lName, fName, desig);
                                        iReply.Add(id, value);
                                    }
                                }
                                else
                                {
                                    iReply.Add(-99, "No Values found");
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static System.Data.DataTable getLeadPersonTable(Int32 LeadCompanyID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn lpID = iReply.Columns.Add("lpID", typeof(Int32));
                DataColumn lastName = iReply.Columns.Add("lastName", typeof(String));
                DataColumn firstName = iReply.Columns.Add("firstName", typeof(String));
                DataColumn middleName = iReply.Columns.Add("middleName", typeof(String));
                DataColumn lpDesignation = iReply.Columns.Add("lpDesignation", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                Int32 id = -99;
                String lName = String.Empty, fName = String.Empty, mName = String.Empty, desig = String.Empty;
                DateTime uT = DateTime.Now;
                String selData = "SELECT [lpID], [lastName], [firstName], [middleName], [lpDesignation], [uTime] FROM [LeadPerson] WHERE [coID] = @coID ORDER BY [lastName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@coID", SqlDbType.Int).Value = LeadCompanyID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        lName = readData.GetString(1);
                                        fName = readData.GetString(2);
                                        mName = readData.GetString(3);
                                        desig = readData.GetString(4);
                                        uT = readData.GetDateTime(5);
                                        iReply.Rows.Add(id, lName, fName, mName, desig, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                                else
                                {
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getLinkedConnectionStatusList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [conID], [conName] FROM [LinkedConnection] ORDER BY [conName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getLinkedConnectionStatusTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn conID = iReply.Columns.Add("conID", typeof(Int32));
                DataColumn conName = iReply.Columns.Add("conName", typeof(String));
                DataColumn conDesc = iReply.Columns.Add("conDesc", typeof(String));
                Int32 id = -99;
                String name = String.Empty, desc = String.Empty;
                String selData = "SELECT [conID], [conName], [conDesc] FROM [LinkedConnection] ORDER BY [conName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        desc = readData.GetString(2);
                                        iReply.Rows.Add(id, name, desc);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }               

        public static Dictionary<Int32, String> getSMChannelList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [chlID], [chlName] FROM [SMChannel] ORDER BY [chlName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getSMChannelTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn chlID = iReply.Columns.Add("chlID", typeof(Int32));
                DataColumn chlName = iReply.Columns.Add("chlName", typeof(String));
                DataColumn chlDesc = iReply.Columns.Add("chlDesc", typeof(String));
                Int32 id = -99;
                String name = String.Empty, desc = String.Empty;
                String selData = "SELECT [chlID], [chlName], [chlDesc] FROM [SMChannel] ORDER BY [chlName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        desc = readData.GetString(2);
                                        if (string.IsNullOrEmpty(desc))
                                        {
                                            desc = "---";
                                        }
                                        iReply.Rows.Add(id, name, desc);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getStateWithCompanyCountList(Int32 CountryID)
        {
            try
            {
                try
                {
                    Int32 stID = -99, listCount = -99;
                    String stName = String.Empty;
                    Dictionary<Int32, String> stateList = new Dictionary<Int32, String>();
                    String selState = "SELECT [stID], [stName] FROM [dmgState] WHERE [ctyID] = @ctyID ORDER BY [stName]";
                    using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                    {
                        try
                        {
                            dataCon.Open();
                            SqlCommand getState = new SqlCommand(selState, dataCon);
                            getState.Parameters.AddWithValue("@ctyID", SqlDbType.Int).Value = CountryID.ToString();
                            using (SqlDataReader readState = getState.ExecuteReader())
                            {
                                try
                                {
                                    if (readState.HasRows)
                                    {
                                        while (readState.Read())
                                        {
                                            stID = readState.GetInt32(0);
                                            List<Int32> addList = getAddressIDListByState(stID);
                                            if (addList[0].Equals(0))
                                            {
                                                listCount = 0;
                                            }
                                            else
                                            {
                                                List<Int32> coList = getCompanyIDListByAddress(addList);
                                                Int32 count = coList[0];
                                                if (count.Equals(0))
                                                {
                                                    listCount = 0;
                                                }
                                                else
                                                {
                                                    listCount = coList.Count();
                                                }
                                            }
                                            stName = readState.GetString(1);
                                            stName = String.Format("{0} - ( {1} )", stName, listCount);
                                            stateList.Add(stID, stName);
                                        }
                                    }
                                    else
                                    {
                                        stateList.Add(0, " --- No State/Province found for selected Country/Nation --- ");
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readState.Close();
                                    readState.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            dataCon.Close();
                            dataCon.Dispose();
                        }
                    }
                    return stateList;
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getCompanyIDListByAddress(List<Int32> addList)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                foreach (var i in addList)
                {
                    String selData = "SELECT [coID] FROM [LeadCompanyToAddress] WHERE [addID] = @addID";
                    using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                    {
                        try
                        {
                            dataCon.Open();
                            SqlCommand getData = new SqlCommand(selData, dataCon);
                            getData.Parameters.AddWithValue("@addID", SqlDbType.Int).Value = i.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            iReply.Add(readData.GetInt32(0));
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            dataCon.Close();
                            dataCon.Dispose();
                        }
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertNewLeadPerson(String FirstName, String MiddleName, String LastName, String Designation, Int32 LeadCompanyID, String LinkedInProfileLink, Int32 LinkedInConnectionStatus, String UserName, String UserRealm)
        {
            try
            {
                Int32 iReply = -99;
                String insData = "INSERT INTO [LeadPerson] ([firstName], [middleName], [lastName], [coID], [lpDesignation], [lpLinkedIn], [lpLinkedInCon], [cTime], [uTime], [username], [realm]) VALUES (@firstName, @middleName, @lastName, @coID, @lpDesignation, @lpLinkedIn, @lpLinkedInCon, @cTime, @uTime, @username, @realm)";
                using(SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand addData = new SqlCommand(insData, dataCon);
                        addData.Parameters.AddWithValue("@firstName", SqlDbType.NVarChar).Value = FirstName.ToString();
                        addData.Parameters.AddWithValue("@middleName", SqlDbType.NVarChar).Value = MiddleName.ToString();
                        addData.Parameters.AddWithValue("@lastName", SqlDbType.NVarChar).Value = LastName.ToString();
                        addData.Parameters.AddWithValue("@coID", SqlDbType.Int).Value = LeadCompanyID.ToString();
                        addData.Parameters.AddWithValue("@lpDesignation", SqlDbType.NVarChar).Value = Designation.ToString();
                        addData.Parameters.AddWithValue("@lpLinkedIn", SqlDbType.NVarChar).Value = LinkedInProfileLink.ToString();
                        addData.Parameters.AddWithValue("@lpLinkedInCon", SqlDbType.Int).Value = LinkedInConnectionStatus.ToString();
                        addData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        addData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        addData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        addData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserRealm.ToString();
                        iReply = Convert.ToInt32(addData.ExecuteNonQuery());
                    }
                    catch(Exception ex)
                    {throw new System.Exception(ex.ToString());}
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch(Exception ex)
            {throw new System.Exception(ex.ToString());}
        }

        public static Int32 insertLeadCompanyAddress(Int32 LeadCompanyID, Int32 DemogTypeID, Int32 RegionID, Int32 SubRegionID, Int32 CountryID, Int32 StateID, Int32 CountyID, Int32 CityID, Int32 ZipID, String Street1, String Street2, String Street3, String AddressTitle, String UserName, String UserRealm)
        {
            try
            {
                Int32 iReply = -99, aID = -99;
                String cityName = DMG.getCityName(CityID);
                String stateName = DMG.getStateName(StateID);
                String zipName = DMG.getZipCode(ZipID);
                String selData = "INSERT INTO [StreetAddress] ([dmgID], [regID], [sregID],[ctyID], [stID], [cntyID], [cityID], [zipID], [st1], [st2], [st3], [cTime], [uTime], [title], [latitude], [longitude], [location], [username], [realm]) VALUES (@dmgID, @regID, @sregID, @ctyID, @stID, @cntyID, @cityID, @zipID, @st1, @st2, @st3, @cTime, @uTime, @title, @latitude, @longitude, @location, @username, @realm); SELECT SCOPE_IDENTITY()";
                String stAdd = Street1 + " " + cityName + " " + stateName + " " + zipName;
                List<Double> latlng = MAP.getLatLngFromAddress(stAdd);
                Double lng = Convert.ToDouble(latlng[0]);
                Double lat = Convert.ToDouble(latlng[1]);
                SqlGeography locat = SqlGeography.Point(lat, lng, 4236); //WGS84 
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(selData, dataCon);
                        insData.Parameters.AddWithValue("@dmgID", SqlDbType.Int).Value = DemogTypeID.ToString();
                        insData.Parameters.AddWithValue("@regID", SqlDbType.Int).Value = RegionID.ToString();
                        insData.Parameters.AddWithValue("@sregID", SqlDbType.Int).Value = SubRegionID.ToString();
                        insData.Parameters.AddWithValue("@ctyID", SqlDbType.Int).Value = CountryID.ToString();
                        insData.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = StateID.ToString();
                        insData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                        insData.Parameters.AddWithValue("@cityID", SqlDbType.Int).Value = CityID.ToString();
                        insData.Parameters.AddWithValue("@zipID", SqlDbType.Int).Value = ZipID.ToString();
                        insData.Parameters.AddWithValue("@st1", SqlDbType.NVarChar).Value = Street1.ToString();
                        insData.Parameters.AddWithValue("@st2", SqlDbType.NVarChar).Value = Street2.ToString();
                        insData.Parameters.AddWithValue("@st3", SqlDbType.NVarChar).Value = Street3.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@title", SqlDbType.NVarChar).Value = AddressTitle.ToString();
                        insData.Parameters.AddWithValue("@latitude", SqlDbType.Decimal).Value = lat.ToString();
                        insData.Parameters.AddWithValue("@longitude", SqlDbType.Decimal).Value = lng.ToString();
                        insData.Parameters.AddWithValue("@location", SqlDbType.VarChar).Value = locat.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserRealm.ToString();
                        aID = Convert.ToInt32(insData.ExecuteScalar());
                        iReply = insertCompanyToAddress(LeadCompanyID, aID);
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertLeadComments(Int32 LeadCompanyID, Int32 LeadPersonID, String Comments, System.Guid SmartUserAccount, DateTime CommentTimeStamp, Int32 ContactTypeID, String UserName, String UserRealm)
        {
            try
            {
                Int32 iReply = -99;
                String insData = "INSERT INTO [LeadNurturing] ([coID], [lpID], [lnComment], [UserId], [lnTimeStamp], [ctID], [cTime], [uTime], [UserName], [UserRealm]) VALUES (@coID, @lpID, @lnComment, @UserId, @lnTimeStamp, @ctID, @cTime, @uTime, @UserName, @UserRealm)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insValues = new SqlCommand(insData, dataCon);
                        insValues.Parameters.AddWithValue("@coID", SqlDbType.Int).Value = LeadCompanyID.ToString();
                        insValues.Parameters.AddWithValue("@lpID", SqlDbType.Int).Value = LeadPersonID.ToString();
                        insValues.Parameters.AddWithValue("@lnComment", SqlDbType.VarChar).Value = Comments.ToString();
                        insValues.Parameters.AddWithValue("@UserId", SqlDbType.UniqueIdentifier).Value = SmartUserAccount.ToString();
                        insValues.Parameters.AddWithValue("@lnTimeStamp", SqlDbType.DateTime2).Value = CommentTimeStamp.ToString();
                        insValues.Parameters.AddWithValue("@ctID", SqlDbType.Int).Value = ContactTypeID.ToString();
                        insValues.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insValues.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insValues.Parameters.AddWithValue("@UserName", SqlDbType.NVarChar).Value = UserName.ToString();
                        insValues.Parameters.AddWithValue("@UserRealm", SqlDbType.NVarChar).Value = UserRealm.ToString();
                        iReply = Convert.ToInt32(insValues.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();

                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertLeadPersonAddress(Int32 LeadPersonID, Int32 DemogTypeID, Int32 RegionID, Int32 SubRegionID, Int32 CountryID, Int32 StateID, Int32 CountyID, Int32 CityID, Int32 ZipID, String Street1, String Street2, String Street3, String AddressTitle, String UserName, String UserRealm)
        {
            try
            {
                Int32 iReply = -99, aID = -99;
                String cityName = DMG.getCityName(CityID);
                String stateName = DMG.getStateName(StateID);
                String zipName = DMG.getZipCode(ZipID);
                String selData = "INSERT INTO [StreetAddress] ([dmgID], [regID], [sregID],[ctyID], [stID], [cntyID], [cityID], [zipID], [st1], [st2], [st3], [cTime], [uTime], [title], [latitude], [longitude], [location], [username], [realm]) VALUES (@dmgID, @regID, @sregID, @ctyID, @stID, @cntyID, @cityID, @zipID, @st1, @st2, @st3, @cTime, @uTime, @title, @latitude, @longitude, @location, @username, @realm); SELECT SCOPE_IDENTITY()";
                String stAdd = Street1 + " " + cityName + " " + stateName + " " + zipName;
                List<Double> latlng = MAP.getLatLngFromAddress(stAdd);
                Double lng = Convert.ToDouble(latlng[0]);
                Double lat = Convert.ToDouble(latlng[1]);
                SqlGeography locat = SqlGeography.Point(lat, lng, 4236); //WGS84 
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(selData, dataCon);
                        insData.Parameters.AddWithValue("@dmgID", SqlDbType.Int).Value = DemogTypeID.ToString();
                        insData.Parameters.AddWithValue("@regID", SqlDbType.Int).Value = RegionID.ToString();
                        insData.Parameters.AddWithValue("@sregID", SqlDbType.Int).Value = SubRegionID.ToString();
                        insData.Parameters.AddWithValue("@ctyID", SqlDbType.Int).Value = CountryID.ToString();
                        insData.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = StateID.ToString();
                        insData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                        insData.Parameters.AddWithValue("@cityID", SqlDbType.Int).Value = CityID.ToString();
                        insData.Parameters.AddWithValue("@zipID", SqlDbType.Int).Value = ZipID.ToString();
                        insData.Parameters.AddWithValue("@st1", SqlDbType.NVarChar).Value = Street1.ToString();
                        insData.Parameters.AddWithValue("@st2", SqlDbType.NVarChar).Value = Street2.ToString();
                        insData.Parameters.AddWithValue("@st3", SqlDbType.NVarChar).Value = Street3.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@title", SqlDbType.NVarChar).Value = AddressTitle.ToString();
                        insData.Parameters.AddWithValue("@latitude", SqlDbType.Decimal).Value = lat.ToString();
                        insData.Parameters.AddWithValue("@longitude", SqlDbType.Decimal).Value = lng.ToString();
                        insData.Parameters.AddWithValue("@location", SqlDbType.VarChar).Value = locat.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserRealm.ToString();
                        aID = Convert.ToInt32(insData.ExecuteScalar());
                        iReply = insertPersonToAddress(LeadPersonID, aID);
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
        
        public static Int32 insertNewLinkedInConnectionstatus(String statusName, String statusDescription)
        {
            try
            {
                Int32 iReply = -99;
                String insData = "INSERT INTO [LinkedConnection] ([conName], [conDesc]) VALUES (@conName, @conDesc)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand addData = new SqlCommand(insData, dataCon);
                        addData.Parameters.AddWithValue("@conName", SqlDbType.NVarChar).Value = statusName.ToString();
                        addData.Parameters.AddWithValue("@conDesc", SqlDbType.NVarChar).Value = statusDescription.ToString();
                        iReply = Convert.ToInt32(addData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertNewSMChannel(String SMChannelName, String Description)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "INSERT INTO [SMChannel] ([chlName], [chlDesc]) VALUES (@chlName, @chlDesc)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(selData, dataCon);
                        insData.Parameters.AddWithValue("@chlName", SqlDbType.NVarChar).Value = SMChannelName.ToString();
                        insData.Parameters.AddWithValue("@chlDesc", SqlDbType.NVarChar).Value = Description.ToString();
                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertNewSMLeadCompany(String CompanyName, String WebsiteURL, String LinkedInURL, Int32 LinkedInFollowValue, String UserName, String UserDomain)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "INSERT INTO [LeadCompany] ([coName], [coWebURL], [coLnkURL], [cTime], [uTime], [username], [realm], [infID]) VALUES (@coName, @coWebURL, @coLnkURL, @cTime, @uTime, @username, @realm, @infID)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(selData, dataCon);
                        insData.Parameters.AddWithValue("@coName", SqlDbType.NVarChar).Value = CompanyName.ToString();
                        insData.Parameters.AddWithValue("@coWebURL", SqlDbType.NVarChar).Value = WebsiteURL.ToString();
                        insData.Parameters.AddWithValue("@coLnkURL", SqlDbType.NVarChar).Value = LinkedInURL.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserDomain.ToString();
                        insData.Parameters.AddWithValue("@infID", SqlDbType.Int).Value = LinkedInFollowValue.ToString();
                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertPersonToAddress(Int32 personID, Int32 addressID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "INSERT INTO [LeadPersonToAddress] ([lpID], [addID]) VALUES (@lpID, @addID)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(selData, dataCon);
                        insData.Parameters.AddWithValue("@lpID", SqlDbType.Int).Value = personID.ToString();
                        insData.Parameters.AddWithValue("@addID", SqlDbType.Int).Value = addressID.ToString();
                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertCompanyToAddress(Int32 companyID, Int32 addressID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "INSERT INTO [LeadCompanyToAddress] ([coID], [addID]) VALUES (@coID, @addID)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(selData, dataCon);
                        insData.Parameters.AddWithValue("@coID", SqlDbType.Int).Value = companyID.ToString();
                        insData.Parameters.AddWithValue("@addID", SqlDbType.Int).Value = addressID.ToString();
                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertNewLeadPersonEmail(Int32 CompanyID, Int32 LeadPersonID, Int32 EmailTypeID, String EmailAddress)
        {
            try
            {
                Int32 iReply = -99, emailID = -99, dup = -99;
                dup = findDuplicateEmail(EmailAddress);
                if (dup.Equals(-1))
                {
                    iReply = -1;
                }
                else
                {
                    String selData = "INSERT INTO [LeadEmail] ([leadEmail], [dmgID], [cTime], [uTime]) VALUES (@leadEmail, @dmgID, @cTime, @uTime); SELECT SCOPE_IDENTITY()";
                    using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                    {
                        try
                        {
                            dataCon.Open();
                            SqlCommand insData = new SqlCommand(selData, dataCon);
                            insData.Parameters.AddWithValue("@leadEmail", SqlDbType.NVarChar).Value = EmailAddress.ToString();
                            insData.Parameters.AddWithValue("@dmgID", SqlDbType.Int).Value = EmailTypeID.ToString();
                            insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                            insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                            emailID = Convert.ToInt32(insData.ExecuteScalar());
                            iReply = insertLeadPersonToEmail(LeadPersonID, emailID);
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            dataCon.Close();
                            dataCon.Dispose();
                        }
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertLeadPersonToEmail(Int32 LeadPersonID, Int32 EmailID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "INSERT INTO [LeadPersonToEmail] ([lpID], [lemlID]) VALUES (@lpID, @lemlID)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(selData, dataCon);
                        insData.Parameters.AddWithValue("@lpID", SqlDbType.Int).Value = LeadPersonID.ToString();                        
                        insData.Parameters.AddWithValue("@lemlID", SqlDbType.Int).Value = EmailID.ToString();
                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertNewLeadPersonPhone(String PhoneNumber, String PhoneExtension, Int32 PhoneTypeID, Int32 LeadPersonID)
        {
            try
            {
                Int32 iReply = -99, phoneID = -99, dup = -99;
                //dup = findDuplicatePhone(PhoneNumber);
                if (dup.Equals(-1))
                {
                    iReply = -1;
                }
                else
                {
                    String selData = "INSERT INTO [LeadPhoneNumber] ([phValue], [phExtension], [dmgID], [cTime], [uTime]) VALUES (@phValue, @phExtension, @dmgID, @cTime, @uTime); SELECT SCOPE_IDENTITY()";
                    using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                    {
                        try
                        {
                            dataCon.Open();
                            SqlCommand insData = new SqlCommand(selData, dataCon);
                            insData.Parameters.AddWithValue("@phValue", SqlDbType.NVarChar).Value = PhoneNumber.ToString();
                            insData.Parameters.AddWithValue("@phExtension", SqlDbType.NVarChar).Value = PhoneExtension.ToString();
                            insData.Parameters.AddWithValue("@dmgID", SqlDbType.Int).Value = PhoneTypeID.ToString();
                            insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                            insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                            phoneID = Convert.ToInt32(insData.ExecuteScalar());
                            iReply = insertLeadPersonToPhone(LeadPersonID, phoneID);
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            dataCon.Close();
                            dataCon.Dispose();
                        }
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertLeadPersonToPhone(Int32 LeadPersonID, Int32 PhoneID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "INSERT INTO [LeadPersonToPhone] ([lpID], [phID]) VALUES (@lpID, @phID)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FELeadsDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(selData, dataCon);
                        insData.Parameters.AddWithValue("@lpID", SqlDbType.Int).Value = LeadPersonID.ToString();
                        insData.Parameters.AddWithValue("@phID", SqlDbType.Int).Value = PhoneID.ToString();
                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}