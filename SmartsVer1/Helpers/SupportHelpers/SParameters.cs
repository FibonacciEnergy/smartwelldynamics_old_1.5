﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Configuration;
using System.Data.SqlClient;

namespace SmartsVer1.Helpers.SupportHelpers
{
    public class SParameters
    {
        static void Main()
        { }

        public static Dictionary<Int32, String> getSupportDepartmentList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String value = String.Empty, selData = "SELECT [sdptID], [sdptValue] FROM [SDepartment] ORDER BY [sdptValue]";
                using(SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Add(id, value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getSupportDepartmentAbvr(Int32 DepartmentID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [sdptAbvr] FROM [SDepartment] WHERE [sdptID] = @sdptID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@sdptID", SqlDbType.Int).Value = DepartmentID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getSupportDepartmentTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn sdptID = iReply.Columns.Add("sdptID", typeof(Int32));
                DataColumn sdptValue = iReply.Columns.Add("sdptValue", typeof(String));
                DataColumn sdptAbvr = iReply.Columns.Add("sdptAbvr", typeof(String));
                Int32 id = -99;
                String value = String.Empty, abvr = String.Empty, selData = "SELECT [sdptID], [sdptValue], [sdptAbvr] FROM [SDepartment] ORDER BY [sdptValue]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        abvr = readData.GetString(2);
                                        iReply.Rows.Add(id, value, abvr);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getSupportDepartmentName(Int32 DepartmentID)
        {
            try
            {
                String iReply = String.Empty; ;
                String selData = "SELECT [sdptValue] FROM [SDepartment] WHERE [sdptID] = @sdptID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@sdptID", SqlDbType.Int).Value = DepartmentID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getSupportStatusList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String value = String.Empty, selData = "SELECT [sstID], [sstValue] FROM [SStatus] ORDER BY [sstValue]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Add(id, value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getSupportStatusTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn sdptID = iReply.Columns.Add("sstID", typeof(Int32));
                DataColumn sdptValue = iReply.Columns.Add("sstValue", typeof(String));
                DataColumn sdptAbvr = iReply.Columns.Add("sstAbvr", typeof(String));
                Int32 id = -99;
                String value = String.Empty, abvr = String.Empty, selData = "SELECT [sstID], [sstValue], [sstAbvr] FROM [SStatus] ORDER BY [sstValue]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        abvr = readData.GetString(2);
                                        iReply.Rows.Add(id, value, abvr);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getSupportTicketStatusName(Int32 SupportStatusID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [sstValue] FROM [SStatus] WHERE [sstID] = @sstID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@sstID", SqlDbType.Int).Value = SupportStatusID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}