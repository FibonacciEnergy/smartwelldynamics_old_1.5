﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using AC = SmartsVer1.Helpers.AccountHelpers.AppConfig;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using USR = SmartsVer1.Helpers.AccountHelpers.CreateUser;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using CT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using SRV = SmartsVer1.Helpers.DemogHelpers.ServicesConfig;
using SPR = SmartsVer1.Helpers.SupportHelpers.SParameters;

namespace SmartsVer1.Helpers.SupportHelpers
{
    public class SRequest
    {
        static void Main()
        { }

        public static Int32 createRequest(Int32 RequestType, String Requester, String RequestDesc, String RequestJustification, String RequestVersion, String UserName, String UserDomain)
        {
            try
            {
                Int32 iReply = -99, rowID = -99, iNotify = -99, iRespond = -99;

                Guid ruID = (Guid)(USR.getUserIdFromUserName(Requester));
                String insertStatement = "INSERT INTO SRequestList ([stypID], [requester], [reqDesc], [reqJustif], [reqVersion], [cTime], [uTime], [username], [realm]) VALUES (@stypID, @requester, @reqDesc, @reqJustif, @reqVersion, @cTime, @uTime, @username, @realm); SELECT SCOPE_IDENTITY();";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(insertStatement, dataCon);
                        insData.Parameters.AddWithValue("@stypID", SqlDbType.Int).Value = RequestType.ToString();
                        insData.Parameters.AddWithValue("@requester", SqlDbType.UniqueIdentifier).Value = ruID.ToString();
                        insData.Parameters.AddWithValue("@reqDesc", SqlDbType.NVarChar).Value = RequestDesc.ToString();
                        insData.Parameters.AddWithValue("@reqJustif", SqlDbType.NVarChar).Value = RequestJustification.ToString();
                        insData.Parameters.AddWithValue("@reqVersion", SqlDbType.NVarChar).Value = RequestVersion.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserDomain.ToString();

                        //iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                        rowID = Convert.ToInt32(insData.ExecuteScalar());
                        if (rowID > 0)
                        {
                            iReply = Convert.ToInt32(createRequestProgress(rowID, RequestType, UserName, UserDomain));
                            if (iReply.Equals(1))
                            {
                                iReply = Convert.ToInt32(createRequestProgressDetail(rowID, UserName, UserDomain));
                                if (iReply.Equals(1))
                                {
                                    iReply = Convert.ToInt32(createRequestProgressTest(rowID, UserName, UserDomain));
                                    iNotify = Convert.ToInt32(sendNotification(ruID, rowID, DateTime.Now, RequestDesc, RequestJustification, RequestVersion, RequestType));
                                    iRespond = Convert.ToInt32(sendRequestReceipt(ruID, rowID, DateTime.Now, RequestDesc, RequestJustification, RequestVersion, RequestType));

                                    if (iReply.Equals(1) && iNotify.Equals(1) && iRespond.Equals(1))
                                    {
                                        iReply = 1;
                                    }
                                    else
                                    {
                                        iReply = 0;
                                    }
                                }
                                else
                                {
                                    iReply = 0;
                                }
                            }
                            else
                            {
                                iReply = 0;
                            }
                        }
                        else
                        {
                            iReply = 0;
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch(Exception ex)
            {throw new System.Exception(ex.ToString());}
        }

        private static Int32 createRequestProgress(Int32 RequestID, Int32 RequestType, String UserName, String UserDomain)
        {
            try
            {
                Int32 iReply = -99;

                String insertStatement = "INSERT INTO [SRequestProgress] ([srlID],[stypID], [rprgStart], [devTime], [pstID], [cTime], [uTime], [username], [realm]) VALUES (@srlID, @stypID, @rprgStart, @devTime, @pstID, @cTime, @uTime, @username, @realm)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(insertStatement, dataCon);
                        insData.Parameters.AddWithValue("@srlID", SqlDbType.Int).Value = RequestID.ToString();
                        insData.Parameters.AddWithValue("@stypID", SqlDbType.Int).Value = RequestType.ToString();
                        insData.Parameters.AddWithValue("@rprgStart", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@devTime", SqlDbType.NVarChar).Value = String.Empty.ToString();
                        insData.Parameters.AddWithValue("@pstID", SqlDbType.Int).Value = '1'.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserDomain.ToString();

                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        private static Int32 createRequestProgressDetail(Int32 RequestID, String UserName, String UserRealm)
        {
            try
            {
                Int32 iReply = -99;
                String insStat = "INSERT INTO [SRequestProgressDetail] ([rprgID], [developer], [resource], [approver], [cTime], [uTime], [username], [realm]) VALUES (@rprgID, @developer, @resource, @approver, @cTime, @uTime, @username, @realm)";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(insStat, dataCon);
                        insData.Parameters.AddWithValue("@rprgID", SqlDbType.Int).Value = RequestID.ToString();
                        insData.Parameters.AddWithValue("@developer", SqlDbType.UniqueIdentifier).Value = Guid.Empty.ToString();
                        insData.Parameters.AddWithValue("@resource", SqlDbType.UniqueIdentifier).Value = Guid.Empty.ToString();
                        insData.Parameters.AddWithValue("@approver", SqlDbType.UniqueIdentifier).Value = Guid.Empty.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserRealm.ToString();

                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch(Exception ex)
            {throw new System.Exception(ex.ToString());}
        }        

        private static Int32 createRequestProgressTest(Int32 RequestID, String UserName, String UserRealm)
        {
            try
            {
                Int32 iReply = -99;
                String insStat = "INSERT INTO [SRequestProgressTest] ([rprgID], [tplan], [vplan], [cTime], [uTime], [username], [realm]) VALUES (@rprgID, @tplan, @vplan, @cTime, @uTime, @username, @realm)";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(insStat, dataCon);
                        insData.Parameters.AddWithValue("@rprgID", SqlDbType.Int).Value = RequestID.ToString();
                        insData.Parameters.AddWithValue("@tplan", SqlDbType.NVarChar).Value = String.Empty.ToString();
                        insData.Parameters.AddWithValue("@vplan", SqlDbType.NVarChar).Value = String.Empty.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserRealm.ToString();

                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }                

        public static String getRTypeName(Int32 RequestTypeID)
        {
            try
            {
                String iReply = null;
                String selData = "SELECT [stypVal] FROM [SType] WHERE [stypID] = @stypID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@stypID", SqlDbType.Int).Value = RequestTypeID.ToString();
                        using(SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if(readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch(Exception ex)
                            {throw new System.Exception(ex.ToString());}
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getStatusName(Int32 StatusTypeID)
        {
            try
            {
                String iReply = null;
                String selData = "SELECT [pStatus] FROM [RProgressStatus] WHERE [pstID] = @pstID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@pstID", SqlDbType.Int).Value = StatusTypeID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                                else
                                {
                                    iReply = " --- ";
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getRequestList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Dictionary<Int32, String> typeList = new Dictionary<Int32, String>();
                Int32 rCount = -99;
                Int32 rID = -99;
                String rType = null;
                String selData = "SELECT [stypID], [stypVal] FROM [SType]";
                String selCount = "SELECT COUNT(srlID) FROM [SRequestList] WHERE [stypID] = @stypID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        Int32 sID = -99;
                        String sVal = null;                        
                        
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        sID = readData.GetInt32(0);
                                        sVal = readData.GetString(1);
                                        typeList.Add(sID, sVal);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                        foreach (var pair in typeList)
                        {
                            rID = pair.Key;
                            rType = pair.Value;
                            SqlCommand getCount = new SqlCommand(selCount, dataCon);
                            getCount.Parameters.AddWithValue("@stypID", SqlDbType.Int).Value = rID.ToString();
                            using (SqlDataReader readCount = getCount.ExecuteReader())
                            {
                                try
                                {
                                    if (readCount.HasRows)
                                    {
                                        while (readCount.Read())
                                        {
                                            rCount = readCount.GetInt32(0);
                                            rType = pair.Value + " ( " + rCount + " ) ";
                                            iReply.Add(rID, rType);
                                        }
                                    }
                                    else
                                    {
                                        rType = pair.Value + " ( 0 ) ";
                                        iReply.Add(rID, rType);
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readCount.Close();
                                    readCount.Dispose();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();                        
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getRequestDataTable(Int32 RequestTypeID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn srlID = iReply.Columns.Add("srlID", typeof(Int32));
                DataColumn requester = iReply.Columns.Add("requester", typeof(String));
                DataColumn reqDesc = iReply.Columns.Add("reqDesc", typeof(String));
                DataColumn reqJustif = iReply.Columns.Add("reqJustif", typeof(String));
                DataColumn reqVersion = iReply.Columns.Add("reqVersion", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [srlID], [requester], [reqDesc], [reqJustif], [reqVersion], [uTime] FROM [SRequestList] WHERE [stypID] = @stypID ORDER BY [uTime]";
                Int32 id = 0;
                System.Guid user = new Guid();
                String un = String.Empty, desc = String.Empty, just = String.Empty, ver = String.Empty, lName = String.Empty, fName = String.Empty, desig = String.Empty, req = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@stypID", SqlDbType.Int).Value = RequestTypeID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        user = readData.GetGuid(1);
                                        un = USR.getUserNameFromId(user);
                                        UP uprof = UP.GetUserProfile(un);
                                        lName = uprof.LastName;
                                        fName = uprof.FirstName;
                                        desig = uprof.designation;
                                        req = String.Format("{0}, {1} - ( {2} )", lName, fName, desig);
                                        desc = readData.GetString(2);
                                        just = readData.GetString(3);
                                        ver = readData.GetString(4);
                                        uT = readData.GetDateTime(5);
                                        iReply.Rows.Add(id, req, desc, just, ver, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<String> getRPItems(Int32 RequestID)
        {
            try
            {
                List<String> iReply = new List<String>();
                Int32 rID = -99;
                String dTime = null;
                Int32 pstID = -99;
                String sName = null;
                String selData = "SELECT [rprgID], [devTime], [pstID] FROM [SRequestProgress] WHERE [srlID] = @srlID";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@srlID", SqlDbType.Int).Value = RequestID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        rID = readData.GetInt32(0);
                                        dTime = readData.GetString(1);
                                        pstID = readData.GetInt32(2);
                                        sName = getStatusName(pstID);
                                        iReply.Add((Convert.ToString(rID)));
                                        iReply.Add(dTime);
                                        iReply.Add(sName);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch(Exception ex)
            {throw new System.Exception(ex.ToString());}
        }

        public static List<String> getRPDItems(Int32 RequestID)
        {
            try
            {
                List<String> iReply = new List<String>();
                Guid dv = Guid.Empty;
                Guid rs = Guid.Empty;
                Guid ap = Guid.Empty;
                String dvp = String.Empty, rse = String.Empty, app = String.Empty;
                String selData = "SELECT [developer], [resource], [approver] FROM [SRequestProgressDetail] WHERE [rprgID] = @rprgID";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@rprgID", SqlDbType.Int).Value = RequestID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        dv = readData.GetGuid(0);
                                        rs = readData.GetGuid(1);
                                        ap = readData.GetGuid(2);
                                        if (dv.Equals(Guid.Empty))
                                        {
                                            dvp = " --- ";
                                        }
                                        else
                                        {
                                            dvp = USR.getUserNameFromId(dv);
                                        }
                                        if (rs.Equals(Guid.Empty))
                                        {
                                            rse = " --- ";
                                        }
                                        else
                                        {
                                            rse = USR.getUserNameFromId(rs);
                                        }
                                        if (ap.Equals(Guid.Empty))
                                        {
                                            app = " --- ";
                                        }
                                        else
                                        {
                                            app = USR.getUserNameFromId(ap);
                                        }
                                        iReply.Add(dvp);
                                        iReply.Add(rse);
                                        iReply.Add(app);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch(Exception ex)
            {throw new System.Exception(ex.ToString());}
        }

        public static List<String> getRPTItems(Int32 RequestID)
        {
            try
            {
                List<String> iReply = new List<String>();
                String tP = String.Empty, vP = String.Empty;
                String selData = "SELECT [tplan], [vplan] FROM [SRequestProgressTest] WHERE [rprgID] = @rprgID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@rprgID", SqlDbType.Int).Value = RequestID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        tP = readData.GetString(0);
                                        vP = readData.GetString(1);
                                        if (String.IsNullOrEmpty(tP))
                                        {
                                            tP = " --- ";
                                        }
                                        if (String.IsNullOrEmpty(vP))
                                        {
                                            vP = " --- ";
                                        }
                                        iReply.Add(tP);
                                        iReply.Add(vP);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch(Exception ex)
            {throw new System.Exception(ex.ToString());}
        }

        public static Dictionary<Int32, String> getResolvedList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Dictionary<Int32, String> typeList = new Dictionary<Int32, String>();
                Int32 rCount = -99;
                Int32 rID = -99;
                String rType = null;
                String selData = "SELECT [stypID], [stypVal] FROM [SType]";
                String selCount = "SELECT COUNT(srlID) FROM [SRequestResolution] WHERE [stypID] = @stypID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        Int32 sID = -99;
                        String sVal = null;

                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        sID = readData.GetInt32(0);
                                        sVal = readData.GetString(1);
                                        typeList.Add(sID, sVal);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                        foreach (var pair in typeList)
                        {
                            rID = pair.Key;
                            rType = pair.Value;
                            SqlCommand getCount = new SqlCommand(selCount, dataCon);
                            getCount.Parameters.AddWithValue("@stypID", SqlDbType.Int).Value = rID.ToString();
                            using (SqlDataReader readCount = getCount.ExecuteReader())
                            {
                                try
                                {
                                    if (readCount.HasRows)
                                    {
                                        while (readCount.Read())
                                        {
                                            rCount = readCount.GetInt32(0);
                                            rType = pair.Value + " ( " + rCount + " ) ";
                                            iReply.Add(rID, rType);
                                        }
                                    }
                                    else
                                    {
                                        rType = pair.Value + " ( 0 ) ";
                                        iReply.Add(rID, rType);
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readCount.Close();
                                    readCount.Dispose();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getSupReqDataGlobal()
        {            
            try
            {
                DataSet dsData = new DataSet();
                String SelectQuery = "SELECT PS.pStatus rStatus , count(PS.pStatus) rCount FROM [RProgressStatus]] PS , [SRequestProgress] P";
                SelectQuery += " where  P.pstID = PS.pstID group by PS.pStatus";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        SqlCommand getUsers = new SqlCommand(SelectQuery, dataCon);
                        using (SqlDataAdapter sqlCmd = new SqlDataAdapter(getUsers))
                        {
                            try
                            {
                                dataCon.Open();
                                sqlCmd.Fill(dsData);
                                dataCon.Close();
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }

                }

                return dsData.Tables[0];

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //public static String getSupReqData()
        //{
        //    try
        //    {
        //        Google.DataTable.Net.Wrapper.DataTable dt = new Google.DataTable.Net.Wrapper.DataTable();
        //        dt.AddColumn(new Google.DataTable.Net.Wrapper.Column(Google.DataTable.Net.Wrapper.ColumnType.String, "0", "Request Type"));
        //        dt.AddColumn(new Google.DataTable.Net.Wrapper.Column(Google.DataTable.Net.Wrapper.ColumnType.Number, "1", "Count"));
        //        Int32 psID = 0, cID = 0, count = 0;
        //        String psValue = String.Empty, rqValue = String.Empty;
        //        var stList = new List<KeyValuePair<Int32, String>>();
        //        var rqList = new List<KeyValuePair<Int32, String>>();
        //        String selPStat = "SELECT [pstID], [pStatus] FROM RProgressStatus";
        //        String selData = "SELECT COUNT(stypID) FROM SRequestProgress WHERE [pstID] = @pstID";
        //        using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
        //        {
        //            try
        //            {
        //                dataCon.Open();
        //                SqlCommand getPStat = new SqlCommand(selPStat, dataCon);
        //                using (SqlDataReader readPStat = getPStat.ExecuteReader())
        //                {
        //                    try
        //                    {
        //                        if (readPStat.HasRows)
        //                        {
        //                            while (readPStat.Read())
        //                            {
        //                                psID = readPStat.GetInt32(0);
        //                                psValue = readPStat.GetString(1);
        //                                stList.Add(new KeyValuePair<Int32, String>(psID, psValue));
        //                            }
        //                        }
        //                    }
        //                    catch (Exception ex)
        //                    { throw new System.Exception(ex.ToString()); }
        //                    finally
        //                    {
        //                        readPStat.Close();
        //                        readPStat.Dispose();
        //                    }
        //                }
        //                foreach (var pair in stList)
        //                {
        //                    cID = pair.Key;
        //                    rqValue = pair.Value;
        //                    SqlCommand getData = new SqlCommand(selData, dataCon);
        //                    getData.Parameters.AddWithValue("@pstID", SqlDbType.Int).Value = cID.ToString();
        //                    using (SqlDataReader readData = getData.ExecuteReader())
        //                    {
        //                        try
        //                        {
        //                            if (readData.HasRows)
        //                            {
        //                                while (readData.Read())
        //                                {
        //                                    count = readData.GetInt32(0);
        //                                }
        //                            }
        //                        }
        //                        catch (Exception ex)
        //                        { throw new System.Exception(ex.ToString()); }
        //                        finally
        //                        {
        //                            readData.Close();
        //                            readData.Dispose();
        //                        }
        //                    }
        //                    rqValue = rqValue + " ( " + count + " ) ";
        //                    rqList.Add(new KeyValuePair<Int32, String>(count, rqValue));
        //                }
        //            }
        //            catch (Exception ex)
        //            { throw new System.Exception(ex.ToString()); }
        //            finally
        //            {
        //                dataCon.Close();
        //                dataCon.Dispose();
        //            }
        //        }
        //        foreach (var val in rqList)
        //        {
        //            Google.DataTable.Net.Wrapper.Row gr = dt.NewRow();
        //            gr.AddCellRange(new Google.DataTable.Net.Wrapper.Cell[]{                        
        //                new Google.DataTable.Net.Wrapper.Cell(val.Value),
        //                new Google.DataTable.Net.Wrapper.Cell(val.Key)
        //            });
        //            dt.AddRow(gr);
        //        }
        //        return dt.GetJson();
        //    }
        //    catch (Exception ex)
        //    { throw new System.Exception(ex.ToString()); }
        //}

        public static System.Data.DataTable getSRequestProgress(Int32 RequestTypeID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn rprgID = iReply.Columns.Add("rprgID", typeof(Int32));
                DataColumn srlID = iReply.Columns.Add("srlID", typeof(Int32));
                DataColumn rprgStart = iReply.Columns.Add("rprgStart", typeof(DateTime));
                DataColumn devTime = iReply.Columns.Add("devTime", typeof(String));
                DataColumn pstID = iReply.Columns.Add("pstID", typeof(String));
                DataColumn flag = iReply.Columns.Add("flag", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [rprgID], [srlID], [rprgStart], [devTime], [pstID], [uTime] FROM [SRequestProgress] WHERE ([stypID] = @stypID) ORDER BY [srlID] DESC";
                Int32 id = 0, lstID = 0, stID = 0, fg = 0;
                DateTime start = DateTime.Now, uT = DateTime.Now;
                String dev = String.Empty, statusN = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@stypID", SqlDbType.Int).Value = RequestTypeID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        lstID = readData.GetInt32(1);
                                        start = readData.GetDateTime(2);
                                        dev = readData.GetString(3);
                                        if (!String.IsNullOrEmpty(dev))
                                        {
                                            dev = String.Format("{0} {1}", dev, " day(s) ");
                                        }
                                        else
                                        {
                                            dev = "---";
                                        }
                                        stID = readData.GetInt32(4);
                                        statusN = getStatusName(stID);
                                        fg = stID;
                                        uT = readData.GetDateTime(5);
                                        iReply.Rows.Add(id, lstID, start, dev, statusN, fg, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getSRequestProgressDetail(Int32 RequestProgressID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn rprgdID = iReply.Columns.Add("rprgdID", typeof(Int32));
                DataColumn developer = iReply.Columns.Add("developer", typeof(String));
                DataColumn resource = iReply.Columns.Add("resource", typeof(String));
                DataColumn approver = iReply.Columns.Add("approver", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [rprgdID], [developer], [resource], [approver], [uTime] FROM [SRequestProgressDetail] WHERE ([rprgID] = @rprgID)";                
                Int32 id = 0;
                System.Guid dev = new System.Guid(), res = new System.Guid(), app = new System.Guid();
                String devName = String.Empty, resName = String.Empty, appName = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@rprgID", SqlDbType.Int).Value = RequestProgressID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        dev = readData.GetGuid(1);
                                        devName = USR.getUserNameFromId(dev);
                                        UP dProf = UP.GetUserProfile(devName);
                                        devName = String.Format("{0}, {1} ( {2} )", dProf.LastName, dProf.FirstName, dProf.designation);
                                        res = readData.GetGuid(2);
                                        resName = USR.getUserNameFromId(res);                                        
                                        UP rProf = UP.GetUserProfile(resName);
                                        resName = String.Format("{0}, {1} ( {2} )", rProf.LastName, rProf.FirstName, rProf.designation);
                                        app = readData.GetGuid(3);
                                        appName = USR.getUserNameFromId(app);
                                        UP aProf = UP.GetUserProfile(appName);
                                        appName = String.Format("{0}, {1} ( {2} )", aProf.LastName, aProf.FirstName, aProf.designation);
                                        uT = readData.GetDateTime(4);
                                        iReply.Rows.Add(id, devName, resName, appName, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getSRequestTestDetail(Int32 RequestProgressID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn rprgtdID = iReply.Columns.Add("rprgtdID", typeof(Int32));
                DataColumn tplan = iReply.Columns.Add("tplan", typeof(String));
                DataColumn vplan = iReply.Columns.Add("vplan", typeof(String));
                String selData = "SELECT [rprgtdID], [tplan], [vplan] FROM [SRequestProgressTest] WHERE ([rprgID] = @rprgID)";
                Int32 id = 0;
                String pTest = String.Empty, vTest = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@rprgID", SqlDbType.Int).Value = RequestProgressID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {

                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        pTest = readData.GetString(1);
                                        if (String.IsNullOrEmpty(pTest))
                                        {
                                            pTest = "---";
                                        }
                                        vTest = readData.GetString(2);
                                        if (String.IsNullOrEmpty(vTest))
                                        {
                                            vTest = "---";
                                        }
                                        iReply.Rows.Add(id, pTest, vTest);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 sendDemoRequest(String FirstName, String LastName, String Title, String CompanyName, String WorkPhone, String WEmail, Int32 PreferredMethod)
        {
            try
            {
                Int32 iReply = -99;

                //SMTP Credentials
                String SMTP_USERNAME = WebConfigurationManager.AppSettings["SmtpUser"];
                String SMTP_PASSWORD = WebConfigurationManager.AppSettings["SmtpPassword"];

                //SMTP Host
                String SMTP_HOST = WebConfigurationManager.AppSettings["SmtpHost"];
                //SMTP PORT
                int SMTP_PORT = Convert.ToInt32(WebConfigurationManager.AppSettings["SmtpPort"]);

                String fileName = HttpContext.Current.Server.MapPath("~/App_Data/DemoReq.txt");
                String mailBody = File.ReadAllText(fileName);

                mailBody = mailBody.Replace("##FName##", FirstName.ToString());
                mailBody = mailBody.Replace("##LName##", LastName.ToString());
                mailBody = mailBody.Replace("##LName##", LastName.ToString());
                mailBody = mailBody.Replace("##Title##", LastName.ToString());
                mailBody = mailBody.Replace("##Company##", CompanyName.ToString());
                mailBody = mailBody.Replace("##WPhone##", WorkPhone.ToString());
                mailBody = mailBody.Replace("##WEmail##", WEmail.ToString());
                mailBody = mailBody.Replace("##Preferred##", PreferredMethod.ToString());

                MailMessage myEmail = new MailMessage();
                myEmail.Subject = "Email from Smart Well Dynamics Website - Contact page.";
                myEmail.Body = mailBody;

                myEmail.From = new MailAddress(AC.FromAddress, AC.FromName);
                myEmail.To.Add(new MailAddress(AC.ToAddress, AC.ToName));
                myEmail.ReplyToList.Add(new MailAddress(WEmail));

                //Creating new SMTP Client
                using (SmtpClient feSmtp = new SmtpClient(SMTP_HOST, SMTP_PORT))
                {
                    try
                    {
                        //Creating new SMTP Network Credentials
                        feSmtp.Credentials = new System.Net.NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);
                        //Creating Secure connection
                        feSmtp.EnableSsl = true;
                        //Sending email
                        feSmtp.Send(myEmail);
                        iReply = 1;
                    }
                    catch (Exception ex)
                    { throw new System.Net.Mail.SmtpException(ex.ToString()); }                    
                    finally
                    {
                        feSmtp.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 sendServiceRequest(String FirstName, String LastName, String Title, String CompanyName, String WorkPhone, String WEmail , String Operator, String Comments)
        {
            try
            {
                Int32 iReply = -99;

                //SMTP Credentials
                String SMTP_USERNAME = WebConfigurationManager.AppSettings["SmtpUser"];
                String SMTP_PASSWORD = WebConfigurationManager.AppSettings["SmtpPassword"];

                //SMTP Host
                String SMTP_HOST = WebConfigurationManager.AppSettings["SmtpHost"];
                //SMTP PORT
                int SMTP_PORT = Convert.ToInt32(WebConfigurationManager.AppSettings["SmtpPort"]);

                String fileName = HttpContext.Current.Server.MapPath("~/App_Data/ServiceReq.txt");
                String mailBody = File.ReadAllText(fileName);

                mailBody = mailBody.Replace("##FName##", FirstName.ToString());
                mailBody = mailBody.Replace("##LName##", LastName.ToString());
                mailBody = mailBody.Replace("##LName##", LastName.ToString());
                mailBody = mailBody.Replace("##Title##", LastName.ToString());
                mailBody = mailBody.Replace("##Company##", CompanyName.ToString());
                mailBody = mailBody.Replace("##WPhone##", WorkPhone.ToString());
                mailBody = mailBody.Replace("##WEmail##", WEmail.ToString());
                mailBody = mailBody.Replace("##Operator##", Operator.ToString());
                mailBody = mailBody.Replace("##Comments##", Comments.ToString());

                MailMessage myEmail = new MailMessage();
                myEmail.Subject = "Email from Smart Well Dynamics Website - Service Request page.";
                myEmail.Body = mailBody;

                myEmail.From = new MailAddress(AC.FromAddress, AC.FromName);
                myEmail.To.Add(new MailAddress(AC.ToAddress, AC.ToName));
                myEmail.ReplyToList.Add(new MailAddress(WEmail));

                //Creating new SMTP Client
                using (SmtpClient feSmtp = new SmtpClient(SMTP_HOST, SMTP_PORT))
                {
                    try
                    {
                        //Creating new SMTP Network Credentials
                        feSmtp.Credentials = new System.Net.NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);
                        //Creating Secure connection
                        feSmtp.EnableSsl = true;
                        //Sending email
                        feSmtp.Send(myEmail);
                        iReply = 1;
                    }
                    catch (Exception ex)
                    { throw new System.Net.Mail.SmtpException(ex.ToString()); }
                    finally
                    {
                        feSmtp.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 sendContactMessage(String FirstName, String LastName, String CompanyName, String WorkPhone, String WEmail, String Comments)
        {
            try
            {
                Int32 iReply = -99;

                //SMTP Credentials
                String SMTP_USERNAME = WebConfigurationManager.AppSettings["SmtpUser"];
                String SMTP_PASSWORD = WebConfigurationManager.AppSettings["SmtpPassword"];

                //SMTP Host
                String SMTP_HOST = WebConfigurationManager.AppSettings["SmtpHost"];
                //SMTP PORT
                int SMTP_PORT = Convert.ToInt32(WebConfigurationManager.AppSettings["SmtpPort"]);

                String fileName = HttpContext.Current.Server.MapPath("~/App_Data/Email.txt");
                String mailBody = File.ReadAllText(fileName);

                mailBody = mailBody.Replace("##FName##", FirstName.ToString());
                mailBody = mailBody.Replace("##LName##", LastName.ToString());
                mailBody = mailBody.Replace("##Company##", CompanyName.ToString());
                mailBody = mailBody.Replace("##WPhone##", WorkPhone.ToString());
                mailBody = mailBody.Replace("##WEmail##", WEmail.ToString());
                mailBody = mailBody.Replace("##Comments##", Comments.ToString());

                MailMessage myEmail = new MailMessage();
                myEmail.Subject = "Email from Smart Well Dynamics Website - Contact page.";
                myEmail.Body = mailBody;

                myEmail.From = new MailAddress(AC.FromAddress, AC.FromName);
                myEmail.To.Add(new MailAddress(AC.ToAddress, AC.ToName));
                myEmail.ReplyToList.Add(new MailAddress(WEmail));

                //Creating new SMTP Client
                using (SmtpClient feSmtp = new SmtpClient(SMTP_HOST, SMTP_PORT))
                {
                    try
                    {
                        //Creating new SMTP Network Credentials
                        feSmtp.Credentials = new System.Net.NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);
                        //Creating Secure connection
                        
                        feSmtp.EnableSsl = true;
                        
                        //Sending email
                        feSmtp.Send(myEmail);
                        iReply = 1;
                    }
                    catch (Exception ex)
                    { throw new System.Net.Mail.SmtpException(ex.ToString()); }
                    finally
                    {
                        feSmtp.Dispose();
                    }
                }                
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 sendRequestReceipt(Guid RequesterID, Int32 RequestID, DateTime TimeStamp, String Description, String Justification, String Version, Int32 RequestTypeID)
        {
            try
            {
                Int32 iReply = -99;

                //User Info
                String UName = USR.getUserNameFromId(RequesterID);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                Int32 clntID = Convert.ToInt32(usrP.clntID);
                String clntName = CT.getClientName(clntID);
                String uph = usrP.upPhone;
                String ueml = Membership.GetUser(UName).Email;
                String reqStatus = getStatusName(RequestTypeID);

                //SMTP Credentials
                String SMTP_USERNAME = WebConfigurationManager.AppSettings["SmtpUser"];
                String SMTP_PASSWORD = WebConfigurationManager.AppSettings["SmtpPassword"];

                //SMTP Host
                String SMTP_HOST = WebConfigurationManager.AppSettings["SmtpHost"];
                //SMTP PORT
                int SMTP_PORT = Convert.ToInt32(WebConfigurationManager.AppSettings["SmtpPort"]);

                
                string fileName = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/SupportRequester.txt");
                string mailBody = File.ReadAllText(fileName);
                
                mailBody = mailBody.Replace("##RequestID##", RequestID.ToString());
                mailBody = mailBody.Replace("##SubmitTimestamp##", TimeStamp.ToString());
                mailBody = mailBody.Replace("##Description##", Description.ToString());
                mailBody = mailBody.Replace("##Justification##", Justification.ToString());
                mailBody = mailBody.Replace("##Version##", Version.ToString());
                mailBody = mailBody.Replace("##RequestType##", reqStatus.ToString());

                MailMessage myEmail = new MailMessage();
                myEmail.Subject = "Email from Smart Well Dynamics Website - Web Support Request.";
                myEmail.Body = mailBody;

                myEmail.From = new MailAddress(AC.FromAddress, AC.FromName);
                myEmail.To.Add(new MailAddress(ueml));

                //SmtpClient myClient = new SmtpClient();
                //myClient.Send( myEmail );

                //Creating new SMTP Client
                using (SmtpClient feSmtp = new SmtpClient(SMTP_HOST, SMTP_PORT))
                {
                    try
                    {
                        //Creating new SMTP Network Credentials
                        feSmtp.Credentials = new System.Net.NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);
                        //Creating Secure connection
                        
                        feSmtp.EnableSsl = true;
                        
                        //Sending email
                        feSmtp.Send(myEmail);
                    }
                    catch (Exception ex)
                    { throw new System.Net.Mail.SmtpException(ex.ToString()); }
                    finally
                    {
                        feSmtp.Dispose();
                    }
                }
                iReply = 1;

                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 sendNotification(Guid RequesterID, Int32 RequestID, DateTime TimeStamp, String Description, String Justification, String Version, Int32 RequestTypeID)
        {
            try
            {
                Int32 iReply = -99;

                //User Info
                String UName = USR.getUserNameFromId(RequesterID);
                UP usrP = UP.GetUserProfile(Membership.GetUser().UserName);
                String fN = usrP.FirstName;
                String lN = usrP.LastName;
                Int32 clntID = Convert.ToInt32(usrP.clntID);
                String clntName = CT.getClientName(clntID);
                String uph = usrP.upPhone;
                String ueml = Membership.GetUser(UName).Email;
                String reqStatus = getStatusName(RequestTypeID);

                //SMTP Credentials
                String SMTP_USERNAME = WebConfigurationManager.AppSettings["SmtpUser"];
                String SMTP_PASSWORD = WebConfigurationManager.AppSettings["SmtpPassword"];

                //SMTP Host
                String SMTP_HOST = WebConfigurationManager.AppSettings["SmtpHost"];
                //SMTP PORT
                int SMTP_PORT = Convert.ToInt32(WebConfigurationManager.AppSettings["SmtpPort"]);


                string fileName = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/SupportNotifier.txt");
                string mailBody = File.ReadAllText(fileName);

                mailBody = mailBody.Replace("##FName##", fN.ToString());
                mailBody = mailBody.Replace("##LName##", lN.ToString());
                mailBody = mailBody.Replace("##Company##", clntName.ToString());
                mailBody = mailBody.Replace("##WPhone##", uph.ToString());
                mailBody = mailBody.Replace("##WEmail##", ueml.ToString());
                mailBody = mailBody.Replace("##RequestID##", RequestID.ToString());
                mailBody = mailBody.Replace("##SubmitTimestamp##", TimeStamp.ToString());
                mailBody = mailBody.Replace("##Description##", Description.ToString());
                mailBody = mailBody.Replace("##Justification##", Justification.ToString());
                mailBody = mailBody.Replace("##Version##", Version.ToString());
                mailBody = mailBody.Replace("##RequestType##", reqStatus.ToString());

                MailMessage myEmail = new MailMessage();
                myEmail.Subject = "Email from Smart Well Dynamics Website - Web Support Request.";
                myEmail.Body = mailBody;

                myEmail.From = new MailAddress(AC.FromAddress, AC.FromName);
                myEmail.To.Add(new MailAddress(AC.ToAddress, AC.ToName));

                //SmtpClient myClient = new SmtpClient();
                //myClient.Send( myEmail );

                //Creating new SMTP Client
                using (SmtpClient feSmtp = new SmtpClient(SMTP_HOST, SMTP_PORT))
                {
                    try
                    {
                        //Creating new SMTP Network Credentials
                        feSmtp.Credentials = new System.Net.NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);
                        //Creating Secure connection

                        feSmtp.EnableSsl = true;

                        //Sending email
                        feSmtp.Send(myEmail);
                    }
                    catch (Exception ex)
                    { throw new System.Net.Mail.SmtpException(ex.ToString()); }
                    finally
                    {
                        feSmtp.Dispose();
                    }
                }
                iReply = 1;

                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }       

        private static Int32 supportRequestResolution(Int32 SRequestID, Int32 ProgressStatusID, String RequestVersion, String RequestDescription, Guid Approver, String UserName, String UserRealm)
        {
            try
            {
                Int32 iReply = -99;
                String insStat = "INSERT INTO SRequestResolution ([srlID], [pstID], [reqVersion], [reqDesc], [UserId], [cTime], [uTime], [username], [realm]) VALUES (@srlID, @pstID, @reqVersion, @reqDesc, @UserId, @cTime, @uTime, @username, @realm)";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(insStat, dataCon);
                        insData.Parameters.AddWithValue("@srlID", SqlDbType.Int).Value = SRequestID.ToString();
                        insData.Parameters.AddWithValue("@pstID", SqlDbType.Int).Value = ProgressStatusID.ToString();
                        insData.Parameters.AddWithValue("@reqVersion", SqlDbType.NVarChar).Value = RequestVersion.ToString();
                        insData.Parameters.AddWithValue("@reqDesc", SqlDbType.Text).Value = RequestDescription.ToString();
                        insData.Parameters.AddWithValue("@UserId", SqlDbType.UniqueIdentifier).Value = Approver.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserRealm.ToString();

                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }

                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 updateSRequestProgress(Int32 RequestID, Int32 SProgressID, Int32 DevTime, Int32 PStatusID, Guid Developer, Guid Resource, Guid Approver, String TestPlan, String ValidPlan, String UserName, String UserRealm)
        {
            try
            {
                Int32 iReply = -99;
                Int32 iReplyD = -99;
                Int32 iReplyT = -99;

                String updStat = "UPDATE SRequestProgress SET [devTime] = @devTime, [pstID] = @pstID, [uTime] = @uTime, [username] = @username, [realm] = @realm WHERE [rprgID] = @rprgID";
                iReplyT = updateSRequestProgessTest(RequestID, TestPlan, ValidPlan, UserName, UserRealm);
                if (iReplyT.Equals(1))
                {
                    iReplyD = updateSRequestProgressDetail(RequestID, Developer, Resource, Approver, UserName, UserRealm);
                    if (iReplyD.Equals(1))
                    {
                        using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                        {
                            try
                            {
                                dataCon.Open();
                                SqlCommand insData = new SqlCommand(updStat, dataCon);
                                insData.Parameters.AddWithValue("@devTime", SqlDbType.Int).Value = DevTime.ToString();
                                insData.Parameters.AddWithValue("@pstID", SqlDbType.Int).Value = PStatusID.ToString();
                                insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                                insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                                insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserRealm.ToString();
                                insData.Parameters.AddWithValue("@rprgID", SqlDbType.Int).Value = RequestID.ToString();

                                iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                dataCon.Close();
                                dataCon.Dispose();
                            }
                        }
                    }
                    else
                    {
                        iReply = -99;
                    }
                }
                else
                {
                    iReply = -99;
                }

                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 updateSRequestProgressDetail(Int32 RequestID, Guid Developer, Guid Resource, Guid Approver, String UserName, String UserRealm)
        {
            try
            {
                Int32 iReply = -99;
                String updStat = "UPDATE SRequestProgressDetail SET [developer] = @developer, [resource] = @resource, [approver] = @approver, [uTime] = @uTime, [username] = @username, [realm] = @realm WHERE [rprgID] = @rprgID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(updStat, dataCon);
                        insData.Parameters.AddWithValue("@developer", SqlDbType.UniqueIdentifier).Value = Developer.ToString();
                        insData.Parameters.AddWithValue("@resource", SqlDbType.UniqueIdentifier).Value = Resource.ToString();
                        insData.Parameters.AddWithValue("@approver", SqlDbType.UniqueIdentifier).Value = Approver.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserRealm.ToString();
                        insData.Parameters.AddWithValue("@rprgID", SqlDbType.Int).Value = RequestID.ToString();

                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 updateSRequestProgessTest(Int32 RequestID, String TestPlan, String ValidPlan, String UserName, String UserRealm)
        {
            try
            {
                Int32 iReply = -99;
                String updateState = "UPDATE SRequestProgressTest SET [tplan] = @tplan, [vplan] = @vplan, [uTime] = @uTime, [username] = @username, [realm] = @realm WHERE [rprgID] = @rprgID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand updateData = new SqlCommand(updateState, dataCon);
                        updateData.Parameters.AddWithValue("@tplan", SqlDbType.NVarChar).Value = TestPlan.ToString();
                        updateData.Parameters.AddWithValue("@vplan", SqlDbType.NVarChar).Value = ValidPlan.ToString();
                        updateData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        updateData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        updateData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserRealm.ToString();
                        updateData.Parameters.AddWithValue("@rprgID", SqlDbType.Int).Value = RequestID.ToString();

                        iReply = Convert.ToInt32(updateData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 sendWellServiceRegistrationMessage(String LoginName, String LoginRealm, Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 ServiceGroupID, Int32 ServiceID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String OprCompany = CLNT.getClientName(OperatorID);
                String ClientCo = CLNT.getClientName(ClientID);
                String PadName = AST.getWellPadName(WellPadID, ClientDBString);
                String WellName = AST.getWellName(WellID, ClientDBString);
                String SrvGroup = SRV.getSrvGroupName(ServiceGroupID);
                String Service = SRV.getServiceName(ServiceID);
                //SMTP Credentials
                String SMTP_USERNAME = WebConfigurationManager.AppSettings["SupportUser"];
                String SMTP_PASSWORD = WebConfigurationManager.AppSettings["SupportPassword"];

                //SMTP Host
                String SMTP_HOST = WebConfigurationManager.AppSettings["SmtpHost"];
                //SMTP PORT
                int SMTP_PORT = Convert.ToInt32(WebConfigurationManager.AppSettings["SmtpPort"]);

                String fileName = HttpContext.Current.Server.MapPath("~/App_Data/SrvRegistration.txt");
                String mailBody = File.ReadAllText(fileName);

                mailBody = mailBody.Replace("##Operator##", OprCompany.ToString());
                mailBody = mailBody.Replace("##Client##", ClientCo.ToString());
                mailBody = mailBody.Replace("##Pad##", PadName.ToString());
                mailBody = mailBody.Replace("##Well##", WellName.ToString());
                mailBody = mailBody.Replace("##Group##", SrvGroup.ToString());
                mailBody = mailBody.Replace("##Service##", Service.ToString());
                mailBody = mailBody.Replace("##Login##", LoginName.ToString());
                mailBody = mailBody.Replace("##Domain##", LoginRealm.ToString());

                MailMessage myEmail = new MailMessage();
                myEmail.Subject = "Email from Smart Well Dynamics Website - Service Registration";
                myEmail.Body = mailBody;

                myEmail.From = new MailAddress(AC.SupportFromAddress, AC.SupportFromName);
                myEmail.To.Add(new MailAddress(AC.SupportToAddress, AC.SupportToName));                

                //Creating new SMTP Client
                using (SmtpClient feSmtp = new SmtpClient(SMTP_HOST, SMTP_PORT))
                {
                    try
                    {
                        //Creating new SMTP Network Credentials
                        feSmtp.Credentials = new System.Net.NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);
                        //Creating Secure connection

                        feSmtp.EnableSsl = true;

                        //Sending email
                        feSmtp.Send(myEmail);
                        iReply = 1;
                    }
                    catch (Exception ex)
                    { throw new System.Net.Mail.SmtpException(ex.ToString()); }
                    finally
                    {
                        feSmtp.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 sendWellServiceRegistrationUpdateMessage(String LoginName, String LoginRealm, Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 ServiceGroupID, Int32 ServiceID, String ApprovedStatusValue, String ConfirmationNumber, DateTime ApprovalTimestamp, String RequestingUser, String RequesterEmail, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String OprCompany = CLNT.getClientName(OperatorID);
                String ClientCo = CLNT.getClientName(ClientID);
                String PadName = AST.getWellPadName(WellPadID, ClientDBString);
                String WellName = AST.getWellName(WellID, ClientDBString);
                String SrvGroup = SRV.getSrvGroupName(ServiceGroupID);
                String Service = SRV.getServiceName(ServiceID);
                //SMTP Credentials
                String SMTP_USERNAME = WebConfigurationManager.AppSettings["SupportUser"];
                String SMTP_PASSWORD = WebConfigurationManager.AppSettings["SupportPassword"];

                //SMTP Host
                String SMTP_HOST = WebConfigurationManager.AppSettings["SmtpHost"];
                //SMTP PORT
                int SMTP_PORT = Convert.ToInt32(WebConfigurationManager.AppSettings["SmtpPort"]);

                String fileName = HttpContext.Current.Server.MapPath("~/App_Data/UpdateSrvRegistration.txt");
                String mailBody = File.ReadAllText(fileName);

                mailBody = mailBody.Replace("##Operator##", OprCompany.ToString());
                mailBody = mailBody.Replace("##Client##", ClientCo.ToString());
                mailBody = mailBody.Replace("##Pad##", PadName.ToString());
                mailBody = mailBody.Replace("##Well##", WellName.ToString());
                mailBody = mailBody.Replace("##Group##", SrvGroup.ToString());
                mailBody = mailBody.Replace("##Service##", Service.ToString());
                mailBody = mailBody.Replace("##Login##", LoginName.ToString());
                mailBody = mailBody.Replace("##Domain##", LoginRealm.ToString());
                mailBody = mailBody.Replace("##AprrovedStatus##", ApprovedStatusValue.ToString());
                mailBody = mailBody.Replace("##Confirmation##", ConfirmationNumber.ToString());
                mailBody = mailBody.Replace("##Timestamp##", ApprovalTimestamp.ToString());

                MailMessage myEmail = new MailMessage();
                myEmail.Subject = "Email from Smart Well Dynamics Website - Service Registration";
                myEmail.Body = mailBody;

                myEmail.From = new MailAddress(AC.SupportFromAddress, AC.SupportFromName);
                myEmail.To.Add(new MailAddress(AC.SupportToAddress, AC.SupportToName));
                myEmail.To.Add(new MailAddress(RequesterEmail, RequestingUser));

                //Creating new SMTP Client
                using (SmtpClient feSmtp = new SmtpClient(SMTP_HOST, SMTP_PORT))
                {
                    try
                    {
                        //Creating new SMTP Network Credentials
                        feSmtp.Credentials = new System.Net.NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);
                        //Creating Secure connection

                        feSmtp.EnableSsl = true;

                        //Sending email
                        feSmtp.Send(myEmail);
                        iReply = 1;
                    }
                    catch (Exception ex)
                    { throw new System.Net.Mail.SmtpException(ex.ToString()); }
                    finally
                    {
                        feSmtp.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 sendClientSupportRequest(String LoginName, String LoginRealm, Int32 ClientID, Int32 DepartmentID, String DepartmentName, String Subject, String MessageBody)
        {
            try
            {
                Int32 iReply = -99, iTicket = -99, ClientDepttTicket = -99, LastTicket = -99;
                String STicketNum = String.Empty, stktStatus = String.Empty, RequesterEmail = String.Empty, RequestingUser = String.Empty;
                //Insert Support Ticket into SMARTs
                UP usrP = UP.GetUserProfile(LoginName);
                RequesterEmail = Membership.GetUser(LoginName).Email;
                iTicket = insertClientSupportRequest(Subject, MessageBody, LoginName, ClientID);
                ClientDepttTicket = insertClientDepttTicket(ClientID, DepartmentID, iTicket);
                LastTicket = getLastClientDepttTicket(ClientID, DepartmentID);
                STicketNum = createSupportTicketNumber(ClientID, DepartmentID, LastTicket);
                //Update Ticket Number to inserted Ticket
                updateClientSupportRequest(iTicket, STicketNum);
                stktStatus = getSupportTicketStatus(iTicket);
                    //SMTP Credentials
                    String SMTP_USERNAME = WebConfigurationManager.AppSettings["SupportUser"];
                    String SMTP_PASSWORD = WebConfigurationManager.AppSettings["SupportPassword"];
                    //SMTP Host
                    String SMTP_HOST = WebConfigurationManager.AppSettings["SmtpHost"];
                    //SMTP PORT
                    int SMTP_PORT = Convert.ToInt32(WebConfigurationManager.AppSettings["SmtpPort"]);

                    String fileName = HttpContext.Current.Server.MapPath("~/App_Data/SupportRequest.txt");
                    String mailBody = File.ReadAllText(fileName);

                    mailBody = mailBody.Replace("##TicketNo##", STicketNum.ToString());
                    mailBody = mailBody.Replace("##Deptt##", DepartmentName.ToString());
                    mailBody = mailBody.Replace("##Status##", stktStatus.ToString());
                    mailBody = mailBody.Replace("##Subject##", Subject.ToString());
                    mailBody = mailBody.Replace("##Text##", MessageBody.ToString());

                    MailMessage myEmail = new MailMessage();
                    myEmail.Subject = "Email from SMARTs Support";
                    myEmail.Body = mailBody;

                    myEmail.From = new MailAddress(AC.SupportFromAddress, AC.SupportFromName);
                    myEmail.To.Add(new MailAddress(AC.SupportToAddress, AC.SupportToName));
                    myEmail.To.Add(new MailAddress(RequesterEmail, RequestingUser));

                    //Creating new SMTP Client
                    using (SmtpClient feSmtp = new SmtpClient(SMTP_HOST, SMTP_PORT))
                    {
                        try
                        {
                            //Creating new SMTP Network Credentials
                            feSmtp.Credentials = new System.Net.NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);
                            //Creating Secure connection

                            feSmtp.EnableSsl = true;

                            //Sending email
                            feSmtp.Send(myEmail);
                            iReply = 1;
                        }
                        catch (Exception ex)
                        { throw new System.Net.Mail.SmtpException(ex.ToString()); }
                        finally
                        {
                            feSmtp.Dispose();
                        }
                    }                
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static void updateClientSupportRequest(Int32 iTicket, String STicketNumber)
        {
            try
            {
                String updData = "UPDATE [SupportTicket] SET [stktNumber] = @stktNumber WHERE [stktID] = @stktID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(updData, dataCon);
                        getData.Parameters.AddWithValue("@stktNumber", SqlDbType.NVarChar).Value = STicketNumber.ToString();
                        getData.Parameters.AddWithValue("@stktID", SqlDbType.Int).Value = iTicket.ToString();
                        getData.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 updateSupportTicketAssignment(Int32 TicketID, System.Guid RepresentativeID, String AssignedBy, String AssignedByDomain)
        {
            try
            {
                Int32 iReply = -99;
                String updData = "UPDATE [SupportTicket] SET [stktAssigned] = @stktAssigned, [asgTime] = @asgTime WHERE [stktID] = @stktID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(updData, dataCon);
                        getData.Parameters.AddWithValue("@stktAssigned", SqlDbType.UniqueIdentifier).Value = RepresentativeID.ToString();
                        getData.Parameters.AddWithValue("@asgTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        getData.Parameters.AddWithValue("@stktID", SqlDbType.Int).Value = TicketID.ToString();
                        iReply = Convert.ToInt32(getData.ExecuteNonQuery());
                        if (iReply.Equals(1))
                        {
                            iReply = sendTicketAssignmentNotification(RepresentativeID, TicketID, AssignedBy, AssignedByDomain);
                            updateTicketActionTime(TicketID);
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getAssignedSupportTickets(System.Guid UserAccountID)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String TicketNumber = String.Empty, selData = "SELECT [stktID], [stktNumber] FROM [SupportTicket] WHERE [stktAssigned] = @stktAssigned";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@stktAssigned", SqlDbType.Int).Value = UserAccountID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        TicketNumber = readData.GetString(1);
                                        iReply.Add(id, TicketNumber);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 updateTicketActionTime(Int32 TicketID)
        {
            try
            {
                Int32 iReply = -99;
                String updData = "UPDATE [SupportTicket] SET [uTime] = @uTime WHERE [stktID] = @stktID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand updateCmd = new SqlCommand(updData, dataCon);
                        updateCmd.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        updateCmd.Parameters.AddWithValue("@stktID", SqlDbType.Int).Value = TicketID.ToString();
                        iReply = Convert.ToInt32(updateCmd.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getSupportTicketStatus(Int32 TicketSequence)
        {
            try
            {
                Int32 id = -99;
                String iReply = String.Empty, selData = "SELECT [sstID] FROM [SupportTicket] WHERE [stktID] = @stktID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@stktID", SqlDbType.Int).Value = TicketSequence.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        iReply = SPR.getSupportTicketStatusName(id);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getSupportTicketNumber(Int32 TicketSequence)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [stktNumber] FROM [SupportTicket] WHERE [stktID] = @stktID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@stktID", SqlDbType.Int).Value = TicketSequence.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 getLastClientDepttTicket(Int32 ClientID, Int32 DepartmentID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT MAX(stktID) FROM [ClientDepttTicket] WHERE [clntID] = @clntID AND [sdptID] = @sdptID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@sdptID", SqlDbType.Int).Value = DepartmentID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 insertClientDepttTicket(Int32 ClientID, Int32 DepartmentID, Int32 iTicket)
        {
            try
            {
                Int32 iReply = -99;
                String insData = "INSERT INTO [ClientDepttTicket] ([stktID], [clntID], [sdptID]) VALUES (@stktID, @clntID, @sdptID)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand addData = new SqlCommand(insData, dataCon);
                        addData.Parameters.AddWithValue("@stktID", SqlDbType.Int).Value = iTicket.ToString();
                        addData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        addData.Parameters.AddWithValue("@sdptID", SqlDbType.Int).Value = DepartmentID.ToString();
                        iReply = Convert.ToInt32(addData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertClientSupportRequest(String RequestSubject, String SupportRequest, String RequestingUser, Int32 ClientID)
        {
            try
            {
                Int32 iReply = -99, SDeptt = 6, SStat = 1;
                String TicketNumber = "---";
                Guid asgUser = new Guid();
                String insData = "INSERT INTO [SupportTicket] ([stktNumber], [stktSubject], [stktRequest], [stktRequester], [clntID], [sdptID], [sstID], [cTime], [uTime], [stktAssigned], [asgTime]) VALUES (@stktNumber, @stktSubject, @stktRequest, @stktRequester, @clntID, @sdptID, @sstID, @cTime, @uTime, @stktAssigned, @asgTime); SELECT SCOPE_IDENTITY();";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand addData = new SqlCommand(insData, dataCon);
                        addData.Parameters.AddWithValue("@stktNumber", SqlDbType.NVarChar).Value = TicketNumber.ToString();
                        addData.Parameters.AddWithValue("@stktSubject", SqlDbType.NVarChar).Value = RequestSubject.ToString();
                        addData.Parameters.AddWithValue("@stktRequest", SqlDbType.NVarChar).Value = SupportRequest.ToString();
                        addData.Parameters.AddWithValue("@stktRequester", SqlDbType.NVarChar).Value = RequestingUser.ToString();
                        addData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        addData.Parameters.AddWithValue("@sdptID", SqlDbType.Int).Value = SDeptt.ToString();
                        addData.Parameters.AddWithValue("@sstID", SqlDbType.Int).Value = SStat.ToString();
                        addData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        addData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        addData.Parameters.AddWithValue("@stktAssigned", SqlDbType.UniqueIdentifier).Value = asgUser.ToString();
                        addData.Parameters.AddWithValue("@asgTime", SqlDbType.DateTime2).Value = "1900-01-01 00:00:00".ToString();
                        iReply = Convert.ToInt32(addData.ExecuteScalar());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String createSupportTicketNumber(Int32 ClientID, Int32 SDepartmentID, Int32 LastTicketNumber)
        {
            try
            {
                String iReply = String.Empty, depttAbvr = String.Empty, clntNumb = String.Empty, ticktNumb = String.Empty;
                depttAbvr = SPR.getSupportDepartmentAbvr(SDepartmentID);
                clntNumb = Convert.ToString(ClientID).PadLeft(3, '0');
                ticktNumb = Convert.ToString(LastTicketNumber + 1).PadLeft(12, '0');
                iReply = String.Format("{0}-{1}-{2}", depttAbvr, clntNumb, ticktNumb);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getGlobalSupportTicketTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn stktID = iReply.Columns.Add("stktID", typeof(Int32));
                DataColumn stktNumber = iReply.Columns.Add("stktNumber", typeof(String));
                DataColumn stktSubject = iReply.Columns.Add("stktSubject", typeof(String));
                DataColumn stktRequester = iReply.Columns.Add("stktRequester", typeof(String));
                DataColumn sdptID = iReply.Columns.Add("sdptID", typeof(String));
                DataColumn sstID = iReply.Columns.Add("sstID", typeof(String));
                DataColumn stktAssigned = iReply.Columns.Add("stktAssigned", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                Int32 id = -99, dept = -99, stat = -99;
                DateTime uT = DateTime.Now;
                Guid asgU = Guid.Empty;
                String tNumb = String.Empty, tSub = String.Empty, dptName = String.Empty, sttName = String.Empty, usrName = String.Empty, assgnTo = String.Empty;
                String selData = "SELECT [stktID], [stktNumber], [stktSubject], [stktRequester], [sdptID], [sstID], [stktAssigned], [uTime] FROM [SupportTicket] ORDER BY [cTime] DESC";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        tNumb = readData.GetString(1);
                                        tSub = readData.GetString(2);
                                        usrName = readData.GetString(3);
                                        dept = readData.GetInt32(4);
                                        dptName = SPR.getSupportDepartmentName(dept);
                                        stat = readData.GetInt32(5);
                                        sttName = SPR.getSupportTicketStatusName(stat);
                                        asgU = readData.GetGuid(6);
                                        assgnTo = USR.getUserNameFromId(asgU);
                                        if (String.IsNullOrEmpty(assgnTo))
                                        {
                                            assgnTo = "---";
                                        }
                                        uT = readData.GetDateTime(7);
                                        iReply.Rows.Add(id, tNumb, tSub, usrName, dptName, sttName, assgnTo, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getSupportTicketTable(Int32 ClientID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn stktID = iReply.Columns.Add("stktID", typeof(Int32));
                DataColumn stktNumber = iReply.Columns.Add("stktNumber", typeof(String));
                DataColumn stktSubject = iReply.Columns.Add("stktSubject", typeof(String));
                DataColumn stktRequester = iReply.Columns.Add("stktRequester", typeof(String));
                DataColumn sdptID = iReply.Columns.Add("sdptID", typeof(String));
                DataColumn sstID = iReply.Columns.Add("sstID", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                Int32 id = -99, dept = -99, stat = -99;
                DateTime uT = DateTime.Now;
                String tNumb = String.Empty, tSub = String.Empty, dptName = String.Empty, sttName = String.Empty, usrName = String.Empty;
                String selData = "SELECT [stktID], [stktNumber], [stktSubject], [stktRequester], [sdptID], [sstID], [uTime] FROM [SupportTicket] WHERE [clntID] = @clntID ORDER BY [cTime] DESC";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        tNumb = readData.GetString(1);
                                        tSub = readData.GetString(2);
                                        usrName = readData.GetString(3);
                                        dept = readData.GetInt32(4);
                                        dptName = SPR.getSupportDepartmentName(dept);
                                        stat = readData.GetInt32(5);
                                        sttName = SPR.getSupportTicketStatusName(stat);
                                        uT = readData.GetDateTime(6);
                                        iReply.Rows.Add(id, tNumb, tSub, usrName, dptName, sttName, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getSupportTicketListForClient(Int32 ClientID)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99, dptt = -99, stat = -99;
                String tNumb = String.Empty, dpttName = String.Empty, sttName = String.Empty, value = String.Empty;
                String selData = "SELECT [stktID], [stktNumber], [sdptID], [sstID] FROM [SupportTicket] WHERE [clntID] = @clntID ORDER BY [cTime] DESC";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        tNumb = readData.GetString(1);
                                        dptt = readData.GetInt32(2);
                                        dpttName = SPR.getSupportDepartmentName(dptt);
                                        stat = readData.GetInt32(3);
                                        sttName = SPR.getSupportTicketStatusName(stat);
                                        value = String.Format("{0} --- {1} / {2}", tNumb, dpttName, sttName);
                                        iReply.Add(id, value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getSupportTicketDetailTable(Int32 SupportTicketID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn stktID = iReply.Columns.Add("stktID", typeof(Int32));
                DataColumn stktRequest = iReply.Columns.Add("stktRequest", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                Int32 id = -99;
                String tReq = String.Empty;
                DateTime uT = DateTime.Now;
                String selData = "SELECT [stktID], [stktRequest], [uTime] FROM [SupportTicket] WHERE [stktID] = @stktID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@stktID", SqlDbType.Int).Value = SupportTicketID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        tReq = readData.GetString(1);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, tReq, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getSupportTicketResponseTable(Int32 SupportTicketID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn strpID = iReply.Columns.Add("strpID", typeof(Int32));
                DataColumn strpText = iReply.Columns.Add("strpText", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                Int32 id = -99;
                String tReq = String.Empty;
                DateTime uT = DateTime.Now;
                String selData = "SELECT [strpID], [strpText], [uTime] FROM [SupportTicketResponse] WHERE [stktID] = @stktID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@stktID", SqlDbType.Int).Value = SupportTicketID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        tReq = readData.GetString(1);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, tReq, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 sendTicketAssignmentNotification(Guid RepresentativeID, Int32 TicketID, String TicketAssignedBy, String TicketAssignedByDomain)
        {
            try
            {
                Int32 iReply = -99;
                String TicketNumber = getSupportTicketNumber(TicketID);
                //SMTP Credentials
                String SMTP_USERNAME = WebConfigurationManager.AppSettings["SmtpUser"];
                String SMTP_PASSWORD = WebConfigurationManager.AppSettings["SmtpPassword"];

                //SMTP Host
                String SMTP_HOST = WebConfigurationManager.AppSettings["SmtpHost"];
                //SMTP PORT
                int SMTP_PORT = Convert.ToInt32(WebConfigurationManager.AppSettings["SmtpPort"]);


                string fileName = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/AssignmentNotification.txt");
                string mailBody = File.ReadAllText(fileName);

                mailBody = mailBody.Replace("##Ticket##", TicketNumber.ToString());
                mailBody = mailBody.Replace("##Assigner##", TicketAssignedBy.ToString());
                mailBody = mailBody.Replace("##Domain##", TicketAssignedByDomain.ToString());

                MailMessage myEmail = new MailMessage();
                myEmail.Subject = "Support Ticket Assignment Notification - SMARTs Support";
                myEmail.Body = mailBody;

                myEmail.From = new MailAddress(AC.FromAddress, AC.FromName);
                myEmail.To.Add(new MailAddress(AC.ToAddress, AC.ToName));

                //SmtpClient myClient = new SmtpClient();
                //myClient.Send( myEmail );

                //Creating new SMTP Client
                using (SmtpClient feSmtp = new SmtpClient(SMTP_HOST, SMTP_PORT))
                {
                    try
                    {
                        //Creating new SMTP Network Credentials
                        feSmtp.Credentials = new System.Net.NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);
                        //Creating Secure connection

                        feSmtp.EnableSsl = true;

                        //Sending email
                        feSmtp.Send(myEmail);
                    }
                    catch (Exception ex)
                    { throw new System.Net.Mail.SmtpException(ex.ToString()); }
                    finally
                    {
                        feSmtp.Dispose();
                    }
                }
                iReply = 1;

                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 submitTicketResponse(Int32 TicketID, String ResponseText, Int32 StatusID, String RespondingUser)
        {
            try
            {
                Int32 iReply = -99, insertReply = -99, sendEmail = -99, updateTktStatus = -99;
                String reqUser = String.Empty, statusName = String.Empty;
                System.Guid resUser = System.Guid.Empty;
                reqUser = getSupportRequestingUser(TicketID);
                resUser = USR.getUserIdFromUserName(RespondingUser);
                statusName = SPR.getSupportTicketStatusName(StatusID);
                insertReply = insertSupportTicketReply(TicketID, ResponseText, resUser, reqUser);
                updateTktStatus = updateTicketActionTime(TicketID);
                sendEmail = submitSupportTicketReply(TicketID, ResponseText, statusName, reqUser);
                if (insertReply.Equals(1) && updateTktStatus.Equals(1) && sendEmail.Equals(1))
                {
                    iReply = 1;
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static String getSupportRequestingUser(Int32 TicketID)
        {
            try
            {                
                String iReply = String.Empty, selData = "SELECT [stktRequester] FROM [SupportTicket] WHERE [stktID] = @stktID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@stktID", SqlDbType.Int).Value = TicketID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertSupportTicketReply(Int32 TicketID, String ResponseText, System.Guid AssignedUser, String RequestedBy)
        {
            try
            {
                Int32 iReply = -99;
                String insStat = "INSERT INTO [SupportTicketResponse] ([strpText], [uTime], [stktID], [stktAssigned], [stktRequester]) VALUES (@strpText, @uTime, @stktID, @stktAssigned, @stktRequester)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FESupportDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(insStat, dataCon);
                        insData.Parameters.AddWithValue("@strpText", SqlDbType.NVarChar).Value = ResponseText.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@stktID", SqlDbType.Int).Value = TicketID.ToString();
                        insData.Parameters.AddWithValue("@stktAssigned", SqlDbType.UniqueIdentifier).Value = AssignedUser.ToString();
                        insData.Parameters.AddWithValue("@stktRequester", SqlDbType.NVarChar).Value = RequestedBy.ToString();
                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 submitSupportTicketReply(Int32 TicketID, String ResponseText, String StatusName, String RequestingUser)
        {
            try
            {
                Int32 iReply = -99;
                String TicketNumber = getSupportTicketNumber(TicketID);
                UP usrP = UP.GetUserProfile(RequestingUser);
                String FName = usrP.FirstName;
                String LName = usrP.LastName;
                String requestingName = String.Format("{0}, {1}", LName, FName);
                String usrEmail = Membership.GetUser(RequestingUser).Email;
                //SMTP Credentials
                String SMTP_USERNAME = WebConfigurationManager.AppSettings["SmtpUser"];
                String SMTP_PASSWORD = WebConfigurationManager.AppSettings["SmtpPassword"];

                //SMTP Host
                String SMTP_HOST = WebConfigurationManager.AppSettings["SmtpHost"];
                //SMTP PORT
                int SMTP_PORT = Convert.ToInt32(WebConfigurationManager.AppSettings["SmtpPort"]);


                string fileName = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/SupportReply.txt");
                string mailBody = File.ReadAllText(fileName);

                mailBody = mailBody.Replace("##Ticket##", TicketNumber.ToString());
                mailBody = mailBody.Replace("##Status##", StatusName.ToString());
                mailBody = mailBody.Replace("##Reply##", ResponseText.ToString());

                MailMessage myEmail = new MailMessage();
                myEmail.Subject = "Support Ticket Response Notification - SMARTs Support";
                myEmail.Body = mailBody;

                myEmail.From = new MailAddress(AC.FromAddress, AC.FromName);
                myEmail.To.Add(new MailAddress(AC.ToAddress, AC.ToName));
                myEmail.To.Add(new MailAddress(usrEmail, requestingName));
                //SmtpClient myClient = new SmtpClient();
                //myClient.Send( myEmail );

                //Creating new SMTP Client
                using (SmtpClient feSmtp = new SmtpClient(SMTP_HOST, SMTP_PORT))
                {
                    try
                    {
                        //Creating new SMTP Network Credentials
                        feSmtp.Credentials = new System.Net.NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);
                        //Creating Secure connection

                        feSmtp.EnableSsl = true;

                        //Sending email
                        feSmtp.Send(myEmail);
                    }
                    catch (Exception ex)
                    { throw new System.Net.Mail.SmtpException(ex.ToString()); }
                    finally
                    {
                        feSmtp.Dispose();
                    }
                }
                iReply = 1;

                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}