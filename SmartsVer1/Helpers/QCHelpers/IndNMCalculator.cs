﻿using System;
using System.Web;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMC = SmartsVer1.Helpers.QCHelpers.NonMagCalc;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using Units = SmartsVer1.Helpers.DemogHelpers.UnitsConverter;

namespace SmartsVer1.Helpers.QCHelpers
{
    public class IndNMCalculator
    {        
        static void Main()
        { }

        public static Int32 checkRF(Int32 CalculatorID, String dbConn)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(inmcRMID) FROM [IndNonMagCalcRefMag] WHERE [inmcID] = @inmcID";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@inmcID", SqlDbType.Int).Value = CalculatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 checkBHA(Int32 CalculatorID, String dbConn)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(inmcBHAID) FROM [IndNonMagCalcBHA] WHERE [inmcID] = @inmcID";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@inmcID", SqlDbType.Int).Value = CalculatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 checkWET(Int32 CalculatorID, String dbConn)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(wetID) FROM [IndNonMagCalcWET] WHERE [inmcID] = @inmcID";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@inmcID", SqlDbType.Int).Value = CalculatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static List<String> getCalculatorReferenceMagnetics(Int32 CalculatorID, String dbConn)
        {
            try
            {
                List<String> iReply = new List<String>();
                String selData = "SELECT [inmcRMGC], [inmcRMConvergence], [inmcRMMDec], [inmcRMDip], [inmcRMBT] FROM IndNonMagCalcRefMag WHERE [inmcID] = @inmcID";
                Int32 id = -99, bt = -99;
                Decimal gc = -99.00M, dec = -99.00M, dip = -99.00M;
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@inmcID", SqlDbType.Int).Value = CalculatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        iReply.Add(Convert.ToString(id));
                                        gc = readData.GetDecimal(1);
                                        iReply.Add(Convert.ToString(gc));
                                        dec = readData.GetDecimal(2);
                                        iReply.Add(Convert.ToString(dec));
                                        dip = readData.GetDecimal(3);
                                        iReply.Add(Convert.ToString(dip));
                                        bt = readData.GetInt32(4);
                                        iReply.Add(Convert.ToString(bt));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal getConvertedSpace(Int32 SpaceUnit, Decimal SpaceValue)
        {
            try
            {
                //Millimeter = 2; Inches = 3; Feet = 4; Meter = 5
                Decimal iResult = -99.00M;
                if (SpaceUnit.Equals(2))
                {
                    iResult = Decimal.Multiply(SpaceValue, 0.0393699237404577M);
                }
                else if (SpaceUnit.Equals(3))
                {
                    iResult = Decimal.Multiply(SpaceValue, 0.0254M);
                }
                else if (SpaceUnit.Equals(4))
                {
                    iResult = Decimal.Multiply(SpaceValue, 0.3048M);
                }
                else
                {
                    iResult = SpaceValue;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal getConvertedGaussmeter(Int32 GMeterUnit, Decimal GMeterValue)
        {
            try
            {
                //Gauss = 1; Tesla = 2; Milli = 3; Micro = 4; Nano = 5
                Decimal iResult = -99.00M;
                if (GMeterUnit.Equals(2))
                {
                    iResult = Decimal.Multiply(GMeterValue, 10000.00M);
                }
                else if (GMeterUnit.Equals(3))
                {
                    iResult = Decimal.Multiply(GMeterValue, 10.00M);
                }
                else if (GMeterUnit.Equals(4))
                {
                    iResult = Decimal.Multiply(GMeterValue, 0.01M);
                }
                else if (GMeterUnit.Equals(5))
                {
                    iResult = Decimal.Multiply(GMeterValue, 0.00001M);
                }
                else
                {
                    iResult = GMeterValue;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addCalculatorName(String calculatorName, String dbConn)
        {
            try
            {                
                Int32 iResult = -99;
                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;
                String insertNMCName = "INSERT INTO IndNonMagCalcName ([inmcName], [cTime], [uTime]) VALUES (@calculatorName, @crTime, @updTime)";
                using (SqlConnection clntConn = new SqlConnection(dbConn))
                {
                    try
                    {
                        clntConn.Open();
                        SqlCommand insertName = new SqlCommand(insertNMCName, clntConn);
                        insertName.Parameters.AddWithValue("@calculatorName", SqlDbType.NVarChar).Value = calculatorName.ToString();
                        insertName.Parameters.AddWithValue("@crTime", SqlDbType.DateTime).Value = crTime.ToString();
                        insertName.Parameters.AddWithValue("@updTime", SqlDbType.DateTime).Value = updTime.ToString();

                        iResult = insertName.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addRefMags(Int32 calculatorID, String refmagName, Int32 GridConvergence, Decimal ConvergenceValue, Decimal MagDec, Decimal Dip, Int32 BTotal, String dbConn)
        {
            try
            {
                Int32 iResult = -99;
                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;
                String insertRefMag = "INSERT INTO IndNonMagCalcRefMag ([inmcID], [inmcRMName], [inmcRMGC], [inmcRMConvergence], [inmcRMMDec], [inmcRMDip], [inmcRMBT], [cTime], [uTime]) VALUES (@inmcID, @inmcRMName, @inmcRMGC, @inmcRMConvergence, @inmcRMMDec, @inmcRMDip, @inmcRMBT, @cTime, @uTime)";
                using (SqlConnection clntConn = new SqlConnection(dbConn))
                {
                    try
                    {
                        clntConn.Open();

                        //String rfName = null;
                        //Int32 requesterID = 1;
                        //rfName = getDataSetName(requesterID, dbConn);

                        SqlCommand insertRF = new SqlCommand(insertRefMag, clntConn);
                        insertRF.Parameters.AddWithValue("@inmcID", SqlDbType.Int).Value = calculatorID.ToString();
                        insertRF.Parameters.AddWithValue("@inmcRMName", SqlDbType.NVarChar).Value = refmagName.ToString();
                        insertRF.Parameters.AddWithValue("@inmcRMGC", SqlDbType.Int).Value = GridConvergence.ToString();
                        insertRF.Parameters.AddWithValue("@inmcRMConvergence", SqlDbType.Decimal).Value = ConvergenceValue.ToString();
                        insertRF.Parameters.AddWithValue("@inmcRMMDec", SqlDbType.Decimal).Value = MagDec.ToString();
                        insertRF.Parameters.AddWithValue("@inmcRMDip", SqlDbType.Decimal).Value = Dip.ToString();
                        insertRF.Parameters.AddWithValue("@inmcRMBT", SqlDbType.Int).Value = BTotal.ToString();
                        insertRF.Parameters.AddWithValue("@cTime", SqlDbType.DateTime).Value = crTime.ToString();
                        insertRF.Parameters.AddWithValue("@uTime", SqlDbType.DateTime).Value = updTime.ToString();

                        iResult = insertRF.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addBHA(Int32 dataSetID, Decimal MotorSize, Int32 MotorSizeUnit, Decimal DrillStringCollarSize, Int32 DrillStringCollarSizeUnit, Decimal Length, Int32 LengthUnit, Decimal SpaceAbv, Int32 SpaceAbvUnit, Decimal SpaceBlw, Int32 SpaceBlwUnit, Decimal MotorGReading, Int32 MotorGReadingUnit, Decimal MotorGDistance, Int32 MotorGDistanceUnit, Decimal CollarGReading, Int32 CollarGReadingUnit, Decimal CollarGDistance, Int32 CollarGDistanceU, String dbConnection)
        {
            try
            {
                Int32 iResult = -99, bhaID = -99;
                String insertBHA = "INSERT INTO [IndNonMagCalcBHA] ([inmcID], [inmcBHAMSize], [inmcBHAMSizeU], [inmcBHADSCSize], [inmcBHADSCSizeU], [inmcBHALength], [inmcBHALengthU], [inmcBHALenAbv], [inmcBHALenAbvU], [inmcBHALenBlw], [inmcBHALenBlwU], [cTime], [uTime]) VALUES (@inmcID, @inmcBHAMSize, @inmcBHAMSizeU, @inmcBHADSCSize, @inmcBHADSCSizeU, @inmcBHALength, @inmcBHALengthU, @inmcBHALenAbv, @inmcBHALenAbvU, @inmcBHALenBlw, @inmcBHALenBlwU, @cTime, @uTime)";
                String insertBHADetail = "INSERT INTO [IndNonMagCalcBHADetail] ([inmcBHAID], [inmcBHAMtrG], [inmcBHAMtrGU], [inmcBHATop], [inmcBHATopU], [inmcBHAClrG], [inmcBHAClrGU], [inmcBHABtm], [inmcBHABtmU], [cTime], [uTime]) VALUES (@inmcBHAID, @inmcBHAMtrG, @inmcBHAMtrGU, @inmcBHATop, @inmcBHATopU, @inmcBHAClrG, @inmcBHAClrGU, @inmcBHABtm, @inmcBHABtmU, @cTime, @uTime)";
                using(SqlConnection bhaCon = new SqlConnection(dbConnection))
                {
                    try
                    {
                        bhaCon.Open();
                        SqlCommand insBHA = new SqlCommand(insertBHA, bhaCon);
                        insBHA.Parameters.AddWithValue("@inmcID", SqlDbType.Int).Value = dataSetID.ToString();
                        insBHA.Parameters.AddWithValue("@inmcBHAMSize", SqlDbType.Decimal).Value = MotorSize.ToString();
                        insBHA.Parameters.AddWithValue("@inmcBHAMSizeU", SqlDbType.Int).Value = MotorSizeUnit.ToString();
                        insBHA.Parameters.AddWithValue("@inmcBHADSCSize", SqlDbType.Decimal).Value = DrillStringCollarSize.ToString();
                        insBHA.Parameters.AddWithValue("@inmcBHADSCSizeU", SqlDbType.Int).Value = DrillStringCollarSizeUnit.ToString();
                        insBHA.Parameters.AddWithValue("@inmcBHALength", SqlDbType.Decimal).Value = Length.ToString();
                        insBHA.Parameters.AddWithValue("@inmcBHALengthU", SqlDbType.Int).Value = LengthUnit.ToString();
                        insBHA.Parameters.AddWithValue("@inmcBHALenAbv", SqlDbType.Decimal).Value = SpaceAbv.ToString();
                        insBHA.Parameters.AddWithValue("@inmcBHALenAbvU", SqlDbType.Int).Value = SpaceAbvUnit.ToString();
                        insBHA.Parameters.AddWithValue("@inmcBHALenBlw", SqlDbType.Decimal).Value = SpaceBlw.ToString();
                        insBHA.Parameters.AddWithValue("@inmcBHALenBlwU", SqlDbType.Int).Value = SpaceBlwUnit.ToString();
                        insBHA.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insBHA.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();

                        bhaID = Convert.ToInt32(insBHA.ExecuteScalar());

                        SqlCommand insBHAD = new SqlCommand(insertBHADetail, bhaCon);
                        insBHAD.Parameters.AddWithValue("@inmcBHAID", SqlDbType.Int).Value = bhaID.ToString();
                        insBHAD.Parameters.AddWithValue("@inmcBHAMtrG", SqlDbType.Decimal).Value = MotorGReading.ToString();
                        insBHAD.Parameters.AddWithValue("@inmcBHAMtrGU", SqlDbType.Int).Value = MotorGReadingUnit.ToString();
                        insBHAD.Parameters.AddWithValue("@inmcBHATop", SqlDbType.Decimal).Value = MotorGDistance.ToString();
                        insBHAD.Parameters.AddWithValue("@inmcBHATopU", SqlDbType.Int).Value = MotorGDistanceUnit.ToString();
                        insBHAD.Parameters.AddWithValue("@inmcBHAClrG", SqlDbType.Decimal).Value = CollarGReading.ToString();
                        insBHAD.Parameters.AddWithValue("@inmcBHAClrGU", SqlDbType.Int).Value = CollarGReadingUnit.ToString();
                        insBHAD.Parameters.AddWithValue("@inmcBHABtm", SqlDbType.Decimal).Value = CollarGDistance.ToString();
                        insBHAD.Parameters.AddWithValue("@inmcBHABtmU", SqlDbType.Int).Value = CollarGDistanceU.ToString();
                        insBHAD.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insBHAD.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();

                        iResult = Convert.ToInt32(insBHAD.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        bhaCon.Close();
                        bhaCon.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addWET(Int32 calcID, String WETDatasetName, Decimal TInclination, Decimal Azimuth, String dbConnection)
        {
            try
            {
                Int32 iResult = -99;
                Decimal MagAzm = -99.00M;
                Decimal HzComp = -99.00M;
                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;
                MagAzm = calcMagAzm(calcID, Azimuth, dbConnection);
                HzComp = calcHzComp(calcID, dbConnection);

                String insertCommand = "INSERT INTO IndNonMagCalcWET ([inmcID], [wetName], [wetTInc], [wetAzm], [wetMagAzm], [wetHzComp], [cTime], [uTime]) VALUES (@inmcID, @wetName, @wetTInc, @wetAzm, @wetMagAzm, @wetHzComp, @cTime, @uTime)";
                using (SqlConnection insCon = new SqlConnection(dbConnection))
                {
                    try
                    {
                        insCon.Open();
                        SqlCommand insData = new SqlCommand(insertCommand, insCon);
                        insData.Parameters.AddWithValue("@inmcID", SqlDbType.Int).Value = calcID.ToString();
                        insData.Parameters.AddWithValue("@wetName", SqlDbType.NVarChar).Value = WETDatasetName.ToString();
                        insData.Parameters.AddWithValue("@wetTInc", SqlDbType.Decimal).Value = TInclination.ToString();
                        insData.Parameters.AddWithValue("@wetAzm", SqlDbType.Decimal).Value = Azimuth.ToString();
                        insData.Parameters.AddWithValue("@wetMagAzm", SqlDbType.Decimal).Value = MagAzm.ToString();
                        insData.Parameters.AddWithValue("@wetHzComp", SqlDbType.Decimal).Value = HzComp.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = crTime.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = updTime.ToString();

                        iResult = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        insCon.Close();
                        insCon.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addSpacing(Int32 calcID, String dsName, Decimal dsAbove, Decimal dsBelow, String dbConnection)
        {
            try
            {
                Int32 iResult = -99;
                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;

                String insertCommand = "INSERT INTO IndNonMagCalcAddSpacing ([inmcID], [inmcspName], [inmcspAbv], [inmcspBlw], [cTime], [uTime]) VALUES (@inmcID, @inmcspName, @inmcspAbv, @inmcspBlw, @cTime, @uTime)";
                using (SqlConnection insCon = new SqlConnection(dbConnection))
                {
                    try
                    {
                        insCon.Open();
                        SqlCommand insData = new SqlCommand(insertCommand, insCon);
                        insData.Parameters.AddWithValue("@inmcID", SqlDbType.Int).Value = calcID.ToString();
                        insData.Parameters.AddWithValue("@inmcspName", SqlDbType.NVarChar).Value = dsName.ToString();
                        insData.Parameters.AddWithValue("@inmcspAbv", SqlDbType.Decimal).Value = dsAbove.ToString();
                        insData.Parameters.AddWithValue("@inmcspBlw", SqlDbType.Decimal).Value = dsBelow.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = crTime.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = updTime.ToString();

                        iResult = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        insCon.Close();
                        insCon.Dispose();
                    }
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addCombo(Int32 calculatorID, Int32 refMagsID, Int32 bhaID, Int32 wetID, Int32 addSpID, String comboName, String dbConnection)
        {
            try
            {
                Int32 iResult = -99;
                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;

                String insertCommand = "INSERT INTO IndNonMagCalcCombo ([cmboName], [inmcID], [inmcRMID], [inmcBHAID], [wetID], [inmcspID], [cTime], [uTime]) VALUES (@cmboName, @inmcID, @inmcRMID, @inmcBHAID, @wetID, @inmcspID, @cTime, @uTime)";
                using (SqlConnection insCon = new SqlConnection(dbConnection))
                {
                    try
                    {
                        insCon.Open();
                        SqlCommand insData = new SqlCommand(insertCommand, insCon);
                        insData.Parameters.AddWithValue("@cmboName", SqlDbType.NVarChar).Value = comboName.ToString();
                        insData.Parameters.AddWithValue("@inmcID", SqlDbType.Int).Value = calculatorID.ToString();
                        insData.Parameters.AddWithValue("@inmcRMID", SqlDbType.Int).Value = refMagsID.ToString();
                        insData.Parameters.AddWithValue("@inmcBHAID", SqlDbType.Int).Value = bhaID.ToString();
                        insData.Parameters.AddWithValue("@wetID", SqlDbType.Int).Value = wetID.ToString();
                        insData.Parameters.AddWithValue("@inmcspID", SqlDbType.Int).Value = addSpID.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = crTime.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = updTime.ToString();
                        iResult = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        insCon.Close();
                        insCon.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static String getDataSetName(Int32 requesterID, String dbConn)
        {
            try
            {
                String iResult = null;
                Int32 namVal = -99;
                String nextName = null;

                switch (requesterID)
                {
                    case 1: //Ref. Mag. Dataset Name
                        {
                            String lastDSName = "SELECT MAX(SUBSTRING(inmcRFName,5,6)) FROM IndNonMagCalcRefMag";
                            using (SqlConnection rfConn = new SqlConnection(dbConn))
                            {
                                try
                                {
                                    rfConn.Open();
                                    
                                    SqlCommand getName = new SqlCommand(lastDSName, rfConn);
                                    using(SqlDataReader readName = getName.ExecuteReader())
                                    {
                                        if (readName.HasRows)
                                        {
                                            while (readName.Read())
                                            {
                                                namVal = Convert.ToInt32(readName.GetString(0));
                                            }
                                        }
                                        else
                                        {
                                            namVal = 0;
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    rfConn.Close();
                                    rfConn.Dispose();
                                }
                            }
                            nextName = Convert.ToString(namVal + 1);
                            iResult = String.Format("{0}{1}", "REF-", nextName);
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getNMCalcID(String nmcName, String dbConn)
        {
            try
            {
                Int32 iResult = -99;
                String selectID = "SELECT [inmcID] FROM IndNonMagCalcName WHERE [inmcName] = @inmcName";
                using (SqlConnection clntConn = new SqlConnection(dbConn))
                {
                    try
                    {
                        clntConn.Open();
                        SqlCommand getID = new SqlCommand(selectID, clntConn);
                        getID.Parameters.AddWithValue("@inmcName", SqlDbType.NVarChar).Value = nmcName.ToString();
                        using (SqlDataReader readID = getID.ExecuteReader())
                        {
                            try
                            {
                                if (readID.HasRows)
                                {
                                    while (readID.Read())
                                    {
                                        iResult = readID.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readID.Close();
                                readID.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal calcMagAzm(Int32 inmcID, Decimal CurrentAzm, String connectionString)
        {
            try
            {
                Decimal iResult = -99.00M;
                Int32 GC = -99;
                Decimal GConvg = -99.00M;
                Decimal MagDec = -99.00M;

                String selData = "SELECT [inmcRMGC], [inmcRMConvergence], [inmcRMMDec] FROM IndNonMagCalcRefMag WHERE [inmcID] = @inmcID";
                using (SqlConnection metCon = new SqlConnection(connectionString))
                {
                    try
                    {
                        metCon.Open();
                        SqlCommand getData = new SqlCommand(selData, metCon);
                        getData.Parameters.AddWithValue("@inmcID", SqlDbType.Int).Value = inmcID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader()) 
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read()) 
                                    {
                                        GC = readData.GetInt32(0);
                                        GConvg = readData.GetDecimal(1);
                                        MagDec = readData.GetDecimal(2);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        metCon.Close();
                        metCon.Dispose();
                    }
                }

                if(GC.Equals(2))
                {
                    if( (CurrentAzm - MagDec + GConvg) > 359.99M)
                    {
                      iResult = (CurrentAzm - MagDec + GConvg) - 360.00M;
                    }
                    else  if( (CurrentAzm - MagDec + GConvg) < 0 )
                    {
                      iResult = ( (CurrentAzm - MagDec + GConvg) - 360.00M);
                    }
                    else
                    {
                      iResult = ( CurrentAzm - MagDec + GConvg );
                    }
                }
                else if( (CurrentAzm - MagDec) > 359.99M )
                {
                  iResult = ( (CurrentAzm - MagDec) - 360 );
                }
                else  if( (CurrentAzm - MagDec) < 0 )
                {
                  iResult = ( (CurrentAzm - MagDec) - 360.00M );
                }
                else
                {
                    iResult = (CurrentAzm - MagDec);
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal calcHzComp(Int32 inmcID, String connectionString)
        {
            try
            {
                Decimal iResult = -99.00M;
                Decimal dip = -99.00M;
                Int32 BTotal = -99;

                String selData = "SELECT [inmcRMDip], [inmcRMBT] FROM IndNonMagCalcRefMag WHERE [inmcID] = @inmcID";
                using (SqlConnection metCon = new SqlConnection(connectionString))
                {
                    try
                    {
                        metCon.Open();
                        SqlCommand getData = new SqlCommand(selData, metCon);
                        getData.Parameters.AddWithValue("@inmcID", SqlDbType.Int).Value = inmcID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        dip = readData.GetDecimal(0);
                                        BTotal = readData.GetInt32(1);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        metCon.Close();
                        metCon.Dispose();
                    }
                }
                iResult = BTotal * (Decimal)( Math.Cos( Units.ToRadians( (Double)dip )));

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal colE(Int32 SurveyID, Decimal MDepth, Decimal prevMDepth, Decimal TieIn_MDepth)
        {
            try
            {
                Decimal iResult = -99.00M;
                if (SurveyID.Equals(1))
                {
                    iResult = Decimal.Subtract(MDepth, TieIn_MDepth);
                }
                else 
                {
                    iResult = MDepth - prevMDepth;
                }
                return iResult;

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal colF(Int32 SurveyID, Decimal Inclination, Decimal prevInclination, Decimal Azimuth, Decimal prevAzimuth)
        {
            try
            {
                Decimal iResult = -99.00M;

                if (SurveyID.Equals(1))
                {
                    iResult = 0.00M;
                }
                else
                {
                    //iResult = Convert.ToDecimal(Math.Cos((Double)Units.ToRadians((Double)Inclination - (Double)prevInclination)) - (Math.Sin((Double)Units.ToRadians((Double)prevInclination)) * Math.Sin((Double)Units.ToRadians((Double)Inclination)) * (1 - Math.Cos((Double)Units.ToRadians((Double)Azimuth - (Double)prevAzimuth)))));
                    iResult = Convert.ToDecimal((Math.Cos(Units.ToRadians((Double)Inclination - (Double)prevInclination))) - (Math.Sin(Units.ToRadians((Double)prevInclination)) * Math.Sin(Units.ToRadians((Double)Inclination)) * (1 - Math.Cos(Units.ToRadians((Double)Azimuth - (Double)prevAzimuth)))));
                }                
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal colG(Decimal colFValue)
        {
            try
            {
                Decimal iResult = -99.00M;

                if (colFValue.Equals(0.00M))
                {
                    iResult = 0.00M;
                }
                else
                {
                    iResult = Convert.ToDecimal( Math.Atan( Math.Sqrt( ( 1 / Math.Pow( (Double)colFValue , 2) ) - 1 ) ) );
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal colH(Decimal colGValue)
        {
            try
            {
                Decimal iResult = -99.00M;
                if (colGValue.Equals(0))
                {
                    iResult = 1.00M;
                }
                else
                {
                    iResult = ( 2 / colGValue ) * (Decimal)Math.Tan( (Double)colGValue / 2 );
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal colI(Int32 SurveyID, Decimal Inclination, Decimal prevInclination, Decimal Azimuth, Decimal svwpColEVal, Decimal svwpColHVal)
        {
            try
            {
                Decimal iResult = -99.00M;
                if (SurveyID.Equals(1))
                {
                    iResult = 0.00M;
                }
                else if (Inclination.Equals(Azimuth))
                {
                    iResult = svwpColEVal * (Decimal)Math.Cos( Units.ToRadians( (Double)Inclination ) );
                }
                else
                {
                    iResult = ( svwpColEVal / 2 ) * ( (Decimal)Math.Cos( Units.ToRadians( (Double)prevInclination ) ) + (Decimal)Math.Cos( Units.ToRadians( (Double)Inclination ) ) ) * svwpColHVal;
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal colJ(Int32 SurveyID, Decimal Inclination, Decimal prevInclination, Decimal Azimuth, Decimal prevAzimuth, Decimal prevColE, Decimal svwpColEVal, Decimal Depth, Decimal prevDepth, Decimal svwpColHVal)
        {
            try
            {
                Decimal iResult = -99.00M;

                if (SurveyID.Equals(1))
                {
                    iResult = 0.00M;
                }
                else if( ( (Decimal)Math.Abs( (Double)Inclination - (Double)prevInclination ) < 0.5M ) && ( Azimuth.Equals( prevColE ) ) )
                {
                    iResult = svwpColEVal * (Decimal)Math.Sin( Units.ToRadians ( (Double)Azimuth ) );
                }
                else
                {
                    iResult = ( ( 0.5M * ( Depth - prevDepth ) ) * ( ( (Decimal)Math.Sin( Units.ToRadians( (Double)prevInclination ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)Azimuth ) ) ) + ( (Decimal)Math.Sin( Units.ToRadians( (Double)Inclination ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)Azimuth ) ) ) ) ) * svwpColEVal;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal colK(Int32 SurveyID, Decimal Depth, Decimal prevDepth, Decimal Inclination, Decimal prevInclination, Decimal Azimuth, Decimal prevAzimuth, Decimal svwpColHValue)
        {
            try
            {
                Decimal iResult = -99.00M;

                if(SurveyID.Equals(1))
                {
                    iResult = 0.00M;
                }
                else
                {
                    iResult = 0.5M * ( Depth - prevDepth ) * ( ( (Decimal)Math.Sin( Units.ToRadians( (Double)prevInclination ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)prevAzimuth ) ) ) + ( (Decimal)Math.Sin( Units.ToRadians( (Double)Inclination) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)Azimuth) ) ) ) * svwpColHValue;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal colL(Int32 surveyNumber, Int32 ComboID, Decimal Northing, Decimal Depth, Decimal svwpColJVal, String dbConn)
        {
            try
            {
                Decimal iResult = -99.00M;
                Decimal sumColJ = -99.00M;

                if (surveyNumber.Equals(1))
                {
                    sumColJ = 0.00M;
                }
                else
                {
                    using (SqlConnection metaCon = new SqlConnection(dbConn))
                    {
                        try
                        {
                            metaCon.Open();
                            String selData = "SELECT SUM(svwpColJ) FROM IndNonMagCalcResults WHERE [cmboID] = @cmboID";
                            SqlCommand getData = new SqlCommand(selData, metaCon);
                            getData.Parameters.AddWithValue("@cmboID", SqlDbType.Int).Value = ComboID.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            sumColJ = readData.GetDecimal(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            metaCon.Close();
                            metaCon.Dispose();
                        }
                    }
                }

                sumColJ = sumColJ + svwpColJVal;
                iResult = Northing + sumColJ;

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal colM(Int32 surveyNumber, Int32 ComboID, Decimal Easting, Decimal Depth, Decimal svwpColKVal, String dbConn)
        {
            try
            {
                Decimal iResult = -99.00M;
                Decimal sumColK = -99.00M;

                if (surveyNumber.Equals(1))
                {
                    sumColK = 0.00M;
                }
                else
                {
                    using (SqlConnection metaCon = new SqlConnection(dbConn))
                    {
                        try
                        {
                            metaCon.Open();
                            String selData = "SELECT SUM(svwpColK) FROM IndNonMagCalcResults WHERE [cmboID] = @cmboID";
                            SqlCommand getData = new SqlCommand(selData, metaCon);
                            getData.Parameters.AddWithValue("@cmboID", SqlDbType.Int).Value = ComboID.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            sumColK = readData.GetDecimal(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            metaCon.Close();
                            metaCon.Dispose();
                        }
                    }
                }
                sumColK = sumColK + svwpColKVal;
                iResult = Easting + sumColK;

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal colN(Int32 surveyNumber, Int32 ComboID, Decimal TieInTVD, Decimal Depth, Decimal svwpColIVal, String dbConn)
        {
            try
            {
                Decimal iResult = -99.00M;
                Decimal sumColI = -99.00M;

                if (surveyNumber.Equals(1))
                {
                    sumColI = 0.00M;
                }
                else
                {
                    using (SqlConnection metaCon = new SqlConnection(dbConn))
                    {
                        try
                        {
                            metaCon.Open();
                            String selData = "SELECT SUM(svwpColI) FROM IndNonMagCalcResults WHERE [cmboID] = @cmboID";
                            SqlCommand getData = new SqlCommand(selData, metaCon);
                            getData.Parameters.AddWithValue("@cmboID", SqlDbType.Int).Value = ComboID.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            sumColI = readData.GetDecimal(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            metaCon.Close();
                            metaCon.Dispose();
                        }
                    }
                }
                sumColI = sumColI + svwpColIVal;
                iResult = TieInTVD + sumColI;

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal colP(Decimal Depth, Decimal svwpColLVal, Decimal svwpColMVal)
        {
            try
            {
                Decimal iResult = -99.00M;

                iResult = Convert.ToDecimal(Math.Sqrt(Math.Pow((Double)svwpColLVal, 2) + Math.Pow((Double)svwpColMVal, 2)));

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal colQ(Decimal TotalBHAInterference, Decimal BHOR, Decimal MagDeclination, Decimal Inclination, Decimal Azimuth)
        {
            try
            {
                Decimal iResult = -99.000000M;

                //iResult = Convert.ToDecimal(Math.Atan((Double)(TotalBHAInterference * (Decimal)Math.Sin(Units.ToRadians((Double)Inclination)) * (Decimal)Math.Sin(Units.ToRadians((Double)(Azimuth - MagDeclination)))) / (Double)((HzComponent + TotalBHAInterference * (Decimal)Math.Sin(Units.ToRadians((Double)Inclination)) * (Decimal)Math.Cos(Units.ToRadians( (Double)( Azimuth - MagDeclination)))))));
                iResult = (Decimal)Math.Atan((Double)((TotalBHAInterference * (Decimal)Math.Sin(Units.ToRadians((Double)Inclination)) * (Decimal)Math.Cos(Units.ToRadians((Double)Azimuth - (Double)MagDeclination))) / (BHOR + (TotalBHAInterference * (Decimal)Math.Sin(Units.ToRadians((Double)Inclination)) * (Decimal)Math.Cos(Units.ToRadians((Double)Azimuth - (Double)MagDeclination))))));

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal svwpNMColQ(Decimal newTotalInterference, Decimal Inclination, Decimal Azimuth, Decimal cnvBHACollarSize, Decimal BHOR)
        {
            try
            {
                Decimal iResult = -99.000000M;
                
                iResult = Convert.ToDecimal( Math.Atan( (Double)( newTotalInterference * (Decimal)Math.Sin( Units.ToRadians( (Double)Inclination ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)Azimuth - (Double)cnvBHACollarSize ) ) ) / (Double)( BHOR + newTotalInterference * (Decimal)Math.Sin( Units.ToRadians( (Double)Inclination ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)Azimuth - (Double)cnvBHACollarSize ) ) ) ) );

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal colR(Int32 surveyNumber, Int32 DepthUnit, Decimal svwpColPVal, Decimal prevColP, Decimal svwpColQVal)
        {
            try
            {
                Decimal iResult = -99.000000M;

                if (DepthUnit.Equals(5))
                {
                    if (surveyNumber.Equals(1))
                    {
                        iResult = 30 * (Decimal)Math.Sin((Double)svwpColQVal);
                    }
                    else
                    {
                        iResult = (svwpColPVal - prevColP) * (Decimal)Math.Sin((Double)svwpColQVal);
                    }
                }
                else
                {
                    if (surveyNumber.Equals(1))
                    {
                        iResult = 100 * (Decimal)Math.Sin((Double)svwpColQVal);
                    }
                    else
                    {
                        iResult = (svwpColPVal - prevColP) * (Decimal)Math.Sin((Double)svwpColQVal);
                    }
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        private static Decimal colS(Int32 surveyNumber, Int32 MetaComboID, Decimal svwpColRVal, String dbConn)
        {
            try
            {
                Decimal iResult = -99.000000M;
                Decimal sumColR = -99.000000M;

                if (surveyNumber.Equals(1))
                {
                    sumColR = 0.000000M;
                }
                else
                {
                    using (SqlConnection metaCon = new SqlConnection(dbConn))
                    {
                        try
                        {
                            metaCon.Open();
                            String selData = "SELECT SUM(svwpColR) FROM IndNonMagCalcResults WHERE [cmboID] = @cmboID";
                            SqlCommand getData = new SqlCommand(selData, metaCon);
                            getData.Parameters.AddWithValue("@cmboID", SqlDbType.Int).Value = MetaComboID.ToString();

                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            sumColR = readData.GetDecimal(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            metaCon.Close();
                            metaCon.Dispose();
                        }
                    }
                }
                iResult = sumColR + svwpColRVal;
                return iResult;
            }
            catch(Exception ex)
            {throw new System.Exception(ex.ToString());}
        }

        private static Decimal gwpColS(Int32 surveyNumber, Int32 MetaComboID, Decimal gwpColRVal, String dbConn)
        {
            try
            {
                Decimal iResult = -99.000000M;
                Decimal sumColR = -99.000000M;

                if (surveyNumber.Equals(1))
                {
                    sumColR = 0.000000M;
                }
                else
                {
                    using (SqlConnection metaCon = new SqlConnection(dbConn))
                    {
                        try
                        {
                            metaCon.Open();
                            String selData = "SELECT SUM(gwpColR) FROM IndNonMagCalcResults WHERE [cmboID] = @cmboID";
                            SqlCommand getData = new SqlCommand(selData, metaCon);
                            getData.Parameters.AddWithValue("@cmboID", SqlDbType.Int).Value = MetaComboID.ToString();

                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            sumColR = readData.GetDecimal(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            metaCon.Close();
                            metaCon.Dispose();
                        }
                    }
                }
                iResult = sumColR + gwpColRVal;
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal svwpCTE(Int32 ComboID, Int32 surveyID, Decimal svwpColRVal, String dbConnection)
        {
            try
            {
                Decimal iResult = -99.000000M;
                Decimal sumCTE = -99.000000M;

                if (surveyID.Equals(1))
                {
                    iResult = 0.000000M;
                }
                else
                {
                    using (SqlConnection metaCon = new SqlConnection(dbConnection))
                    {
                        try
                        {
                            metaCon.Open();
                            String selData = "SELECT SUM(svwpColR) FROM IndNonMagCalcResults WHERE [cmboID] = @cmboID";
                            SqlCommand getData = new SqlCommand(selData, metaCon);
                            getData.Parameters.AddWithValue("@cmboID", SqlDbType.Int).Value = ComboID.ToString();

                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            sumCTE = readData.GetDecimal(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            metaCon.Close();
                            metaCon.Dispose();
                        }
                    }
                    iResult = sumCTE + svwpColRVal;
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal gwpCTE(Int32 ComboID, Int32 surveyID, Decimal gwpColRVal, String dbConnection)
        {
            try
            {
                Decimal iResult = -99.000000M;
                Decimal sumCTE = -99.000000M;

                if (surveyID.Equals(1))
                {
                    iResult = 0.000000M;
                }
                else
                {
                    using (SqlConnection metaCon = new SqlConnection(dbConnection))
                    {
                        try
                        {
                            metaCon.Open();
                            String selData = "SELECT SUM(gwpColR) FROM IndNonMagCalcResults WHERE [cmboID] = @cmboID";
                            SqlCommand getData = new SqlCommand(selData, metaCon);
                            getData.Parameters.AddWithValue("@cmboID", SqlDbType.Int).Value = ComboID.ToString();

                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            sumCTE = readData.GetDecimal(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            metaCon.Close();
                            metaCon.Dispose();
                        }
                    }
                    iResult = sumCTE + gwpColRVal;
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal getCMA( Int32 gConvergence, Decimal Convergence, Decimal refMagMDec, Decimal wetAzm ) //Current Magnetic Azimuth - ColD19
        {
            try
            {
                if( gConvergence.Equals(2) ) //2 = Grid, 1 = True
                {
                    if( ( wetAzm - refMagMDec + Convergence ) > 359.99M )
                    {
                        return ( wetAzm - refMagMDec + Convergence ) -360;
                    }
                    else if( ( wetAzm - refMagMDec + Convergence ) < 0 )
                    {
                        return ( wetAzm - refMagMDec + Convergence ) - 360;
                    }
                    else
                    {
                        return ( wetAzm - refMagMDec + Convergence );
                    }
                }
                else
                {
                    if( ( wetAzm - refMagMDec ) > 359.99M )
                    {
                        return ( wetAzm - refMagMDec ) - 360;
                    }
                    else if( ( wetAzm - refMagMDec ) < 0 )
                    {
                        return ( wetAzm - refMagMDec ) - 360;
                    }
                    else
                    {
                        return ( wetAzm - refMagMDec );
                    }
                 }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal getBHOR(Decimal rmDip, Int32 rmBT) //Bhor = Bon = Horizontal Component of Bt(nT) - ColD21
        {
            try
            {
                return ( rmBT * (Decimal)Math.Cos( Units.ToRadians( (Double)rmDip ) ) );
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal getMPS(Decimal cnvMotorSize)
        {
            try
            {
                return ( ( 49.072M * (Decimal)Math.Abs((Double)cnvMotorSize) ) + 129.23M );
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal getGMtrMPS(Decimal cnvMotorSize, Decimal cnvGMtrMotor, Decimal cnvGMtrTop)
        {
            try
            {
                Decimal valA = Decimal.Multiply(cnvMotorSize / 12, 0.3048M);
                Decimal valB = Decimal.Add(cnvGMtrTop, valA);
                Decimal valC = (Decimal)(Math.Pow((Double)valB, 2));
                Decimal valD = (Decimal)(4 * Math.PI) * valC;
                Decimal valE = cnvGMtrMotor * valD;
                return (  valE * 100 );
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        private static Decimal getDCPS(Decimal cnvCollarSize)
        {
            try
            {
                //return ((49.072M * (Decimal)Math.Abs((Double)cnvCollarSize)) + 129.23M);
                return ((84.764M * (Decimal)Math.Abs((Double)cnvCollarSize)) + 93.224M);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal getGMtrDCPS(Decimal cnvCollarSize, Decimal cnvGMtrCollar, Decimal cnvGMtrBottom)
        {
            try
            {
                Decimal valA = cnvCollarSize / 12;
                Decimal valB = valA * 0.3048M;
                Decimal valC = valB + cnvGMtrBottom;
                Decimal valD = (Decimal)Math.Pow((Double)valC, 2);
                Decimal valE = valD * (Decimal)(4 * Math.PI);
                Decimal valF = cnvGMtrCollar * valE;
                return (valF * 100);
                //return (cnvGMtrCollar * (Decimal)(4 * Math.PI)) * (Decimal)(Math.Pow((Double)(cnvGMtrBottom + (cnvCollarSize / 12) * 0.3048M) , 2)) * 100;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal getMtrInterference(Decimal mtrPoleStrength, Decimal cnvlengthBelow, Decimal cnvMtrDrlLength)
        {
            try
            {
                //return ( ( mtrPoleStrength / (Decimal)( 4 * Math.PI ) ) * ( ( 1 / ( (Decimal)Math.Pow( ( (Double)cnvlengthBelow + 0.01 ) , 2 ) ) ) - ( 1 / ( (Decimal)Math.Pow( ( (Double)cnvMtrDrlLength + (Double)cnvMtrDrlLength + 0.01 ) , 2 ) ) ) ) * 1000 );
                return ( mtrPoleStrength / (Decimal)( 4 * Math.PI) * (Decimal)( ( 1 / Math.Pow( (Double)( cnvlengthBelow + 0.01M ) , 2 ) ) - ( 1 / Math.Pow( (Double)( cnvlengthBelow + cnvMtrDrlLength + 0.01M ) , 2 ) ) ) ) * 1000;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal getGaussmeterMotorInterference(Decimal cnvbhaLength, Decimal bhaLengthBelow, Decimal GMtrMPS)
        {
            try
            {
                return ( ( GMtrMPS / (Decimal)( 4 * Math.PI ) ) * ( ( 1 / (Decimal)Math.Pow( (Double)( bhaLengthBelow + 0.01M ) , 2 ) ) - ( 1 / (Decimal)Math.Pow( (Double)( bhaLengthBelow + cnvbhaLength + 0.01M ) , 2 ) ) ) ) * 1000;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal getClrInterference(Decimal clrPoleStrength, Decimal cnvlengthAbove)
        {
            try
            {
                return ( ( ( clrPoleStrength / (Decimal)(4 * Math.PI) ) * ( 1 / ( (Decimal)Math.Pow( ((Double)cnvlengthAbove + 0.01) , 2 ) ) ) ) * 1000 );
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal getGaussmeterCollarInterference(Decimal cnvLengthAbove, Decimal GMtrCollarMPS)
        {
            try
            {
                return ( ( GMtrCollarMPS / (Decimal)( 4 * Math.PI ) ) * ( ( 1 / (Decimal)Math.Pow( (Double)( cnvLengthAbove + 0.01M ) , 2 ) ) ) ) * 1000;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal getLCAZError(Decimal wetInclination, Decimal wetAzimuth, Decimal Bhor, Decimal TotalBHAInterference)
        {
            try
            {
                return (Decimal)Math.Abs(Units.ToDegrees(Math.Atan(((Double)TotalBHAInterference * Math.Sin(Units.ToRadians((Double)wetInclination)) * Math.Sin(Units.ToRadians((Double)wetAzimuth))) / ((Double)Bhor + ((Double)TotalBHAInterference * (Math.Sin(Units.ToRadians((Double)wetInclination)) * Math.Cos(Units.ToRadians((Double)wetAzimuth))))))));
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal getNMSpAllowableError(Decimal wetPERInclination, Decimal wetPERMagAzimuth)
        {
            try
            {
                return 0.25M + (Decimal)Math.Abs(0.6 * Math.Sin(Units.ToRadians((Double)wetPERInclination)) * Math.Sin(Units.ToRadians((Double)wetPERMagAzimuth)));
                //return 0.5M + (Decimal)(Math.Abs(0.35 * Math.Sin(Units.ToRadians((Double)wetPERInclination) * Math.Sin(Units.ToRadians((Double)wetPERMagAzimuth)))));
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal getNewDCInterference(Decimal cnvSpAbove, Decimal DCPoleStrength, Decimal cnvAddSpAbove)
        {
            try
            {
                if (cnvAddSpAbove.Equals(null))
                {
                    return ( DCPoleStrength / (Decimal)( 4 * Math.PI ) ) * ( (Decimal)( 1 / Math.Pow( ( (Double)cnvSpAbove + 0.01 ) , 2 ) ) ) * 1000;
                }
                else
                {
                    return ( DCPoleStrength / (Decimal)( 4 * Math.PI ) * ( ( 1 / (Decimal)Math.Pow( (Double)( cnvSpAbove + cnvAddSpAbove + 0.01M ) , 2 ) ) ) ) * 1000;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal getNewMtrInterference(Decimal cnvSpBelow, Decimal cnvBHALength, Decimal mtrPS, Decimal cnvAddSpBelow)
        {
            try
            {
                if (cnvAddSpBelow.Equals(null))
                {
                    return ( ( ( mtrPS / (Decimal)(4 * Math.PI)) ) * ( (Decimal)(1 / Math.Pow( ( (Double)cnvSpBelow + 0.01 ) , 2 ) ) - (Decimal)( 1 / Math.Pow( ( (Double)cnvSpBelow + (Double)cnvBHALength + 0.01 ) , 2 ) ) ) ) * 1000;
                }
                else
                {
                    return (((mtrPS / (Decimal)(4 * Math.PI))) * ((Decimal)(1 / Math.Pow(((Double)cnvSpBelow + (Double)cnvAddSpBelow + 0.01), 2)) - (Decimal)(1 / Math.Pow(((Double)cnvSpBelow + (Double)cnvBHALength + (Double)cnvAddSpBelow + 0.01), 2)))) * 1000;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal getNewLCAZError(Decimal welTInclination, Decimal wetMagAzimuth, Decimal BHOR, Decimal totalBHAInterference)
        {
            try
            {
                return (Decimal)Math.Abs(Units.ToDegrees(Math.Atan((Double)(totalBHAInterference * (Decimal)Math.Sin(Units.ToRadians((Double)welTInclination)) * (Decimal)Math.Sin(Units.ToRadians((Double)wetMagAzimuth))) / (Double)(BHOR + (totalBHAInterference * ((Decimal)Math.Sin(Units.ToRadians((Double)welTInclination)) * (Decimal)Math.Cos(Units.ToRadians((Double)wetMagAzimuth))))))));
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal getNewBTSDistance( Decimal cnvSpaceBelow, Decimal cnvBHALength, Decimal cnvAddSpBelow )
        {
            try
            {
                return Decimal.Add(cnvSpaceBelow, Decimal.Add(cnvBHALength, cnvAddSpBelow));
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal getAccumulativeTargetError(Int32 tisDepthUnit, Decimal addNonMagCTE)
        {
            try
            {
                if (tisDepthUnit.Equals(5))
                {
                    return Math.Abs(addNonMagCTE);
                }
                else
                {
                    return Math.Abs( Decimal.Multiply( addNonMagCTE , 0.3048M ) );
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Decimal> processAdditionalSpace(Int32 CalculatorID, Decimal AdditionalSpaceAbove, Decimal AdditionalSpaceBelow, String dbConn)
        {
            try
            {
                List<Decimal> iReply = new List<Decimal>(), wet = new List<Decimal>();
                List<String> bha = new List<String>(), bhaDet = new List<String>(), refmag = new List<String>();                
                Int32 bhaID = -99, motorSU = -99, collarSU = -99, lengthU = -99, spAbvU = -99, spBlwU = -99, mtrGU = -99, mtrDU = -99, clrGU = -99, clrDU = -99, gConvU = -99, bTotal = -99;
                Decimal motorS = -99.00M, collarS = -99.00M, length = -99.00M, spAbv = -99.00M, spBlw = -99.00M, motorG = -99.000000M, mtrD = -99.00M, collarG = -99.000000M, clrD = -99.00M, gConvergence = -99.00M;
                Decimal mDec = -99.00M, dip = -99.00M, TInc = -99.00M, TAzm = -99.00M, MagAzm = -99.00M, HzComp = -99.00M;
                bha = getBHASignature(CalculatorID, dbConn);
                bhaID = Convert.ToInt32(bha[0]);
                motorS = Convert.ToDecimal(bha[1]);
                motorSU = Convert.ToInt32(bha[2]);
                collarS = Convert.ToDecimal(bha[3]);
                collarSU = Convert.ToInt32(bha[4]);
                length = Convert.ToDecimal(bha[5]);
                lengthU = Convert.ToInt32(bha[6]);
                spAbv = Convert.ToDecimal(bha[7]);
                spAbvU = Convert.ToInt32(bha[8]);
                spBlw = Convert.ToDecimal(bha[9]);
                spBlwU = Convert.ToInt32(bha[10]);
                bhaDet = getBHADetail(bhaID, dbConn);
                motorG = Convert.ToDecimal(bhaDet[0]);
                mtrGU = Convert.ToInt32(bhaDet[1]);
                mtrD = Convert.ToDecimal(bhaDet[2]);
                mtrDU = Convert.ToInt32(bhaDet[3]);
                collarG = Convert.ToDecimal(bhaDet[4]);
                clrGU = Convert.ToInt32(bhaDet[5]);
                clrD = Convert.ToDecimal(bhaDet[6]);
                clrDU = Convert.ToInt32(bhaDet[7]);
                wet = getWETList(CalculatorID, dbConn);
                TInc = wet[0];
                TAzm = wet[1];
                MagAzm = wet[2];
                HzComp = wet[3];
                refmag = getNMCRefMagList(CalculatorID, dbConn);
                gConvU = Convert.ToInt32(refmag[0]);
                gConvergence = Convert.ToDecimal(refmag[1]);
                mDec = Convert.ToDecimal(refmag[2]);
                dip = Convert.ToDecimal(refmag[3]);
                bTotal = Convert.ToInt32(refmag[4]);
                Decimal convMotorSize = Units.convMotorSize(motorS, motorSU);
                Decimal convCollarSize = Units.convMotorSize(collarS, collarSU);
                Decimal convBHALength = Units.convMDLen(length, lengthU);
                Decimal convSpaceAbove = Units.convNMSp(spAbv, spAbvU);
                Decimal convSpaceBelow = Units.convNMSp(spBlw, spBlwU);
                Decimal convGSpMtr = Units.convGMtrDistance(mtrD, mtrDU);
                Decimal convGSpBlw = Units.convGMtrDistance(clrD, clrDU);
                Decimal convAddSpAbv = Units.convNMSp(AdditionalSpaceAbove, lengthU);
                Decimal convAddSpBlw = Units.convNMSp(AdditionalSpaceBelow, lengthU);
                //Standard Values
                Decimal stdMPS = getMPS(convMotorSize); //Std. Motor Pole Strength
                Decimal stdDCPS = getDCPS(convCollarSize); //Std. Drill Collar Pole Strength
                Decimal mtrIntf = getMtrInterference(stdMPS, convSpaceBelow, convBHALength); //Std. Motor Interference
                Decimal clrIntf = getClrInterference(stdDCPS, convSpaceAbove); //Std. Drill Collar Interference
                Decimal ttlIntf = Decimal.Add(mtrIntf, clrIntf); //Total BHA Interference
                Decimal lcazBHA = getLCAZError(TInc, MagAzm, HzComp, ttlIntf); //Delta-LCAZ error due to BHA
                Decimal alwErr = getNMSpAllowableError(TInc, MagAzm); // NM Space Allowable Error
                Decimal btsD = Decimal.Add(convSpaceBelow, convBHALength); //Bit to Sensor Distance
                Decimal newDCIntf = getNewDCInterference(convSpaceAbove, stdDCPS, convAddSpAbv); //New Drill Collar Interference
                Decimal newMtrIntf = getNewMtrInterference(convSpaceBelow, convBHALength, stdMPS, convAddSpBlw); //New Motor Interference
                Decimal newttlIntf = Decimal.Add(newDCIntf, newMtrIntf); //New Total Interference
                Decimal newlcazBHA = getLCAZError(TInc, MagAzm, HzComp, newttlIntf); //New Delta-LCAZ Error due to BHA
                Decimal newBTS = (convSpaceBelow + convBHALength + AdditionalSpaceBelow); //New Bit to Sensor Distance
                //Gaussmeter Values
                Decimal gMPS = getGMtrMPS(convMotorSize, motorG, convGSpMtr); //Gaussmeter based Motor Pole Strength
                Decimal gDCPS = getGMtrDCPS(convCollarSize, collarG, convGSpBlw); //Gaussmeter based Drill Collar Pole Strength
                Decimal gMtrIntf = getGaussmeterMotorInterference(convBHALength, convSpaceBelow, gMPS); //Gaussmeter based Motor Interference
                Decimal gClrIntf = getGaussmeterCollarInterference(convSpaceAbove, gDCPS); //Gaussmeter based Drill Collar Interference
                Decimal gTtlIntf = Decimal.Add(gMtrIntf, gClrIntf); //Gaussmeter based Total Interference
                Decimal gLCAZ = getLCAZError(TInc, MagAzm, HzComp, gTtlIntf); //Gaussmeter based LCAZ
                Decimal newGDCIntf = getNewDCInterference(convSpaceAbove, gDCPS, convAddSpAbv); // New Gaussmeter Drill Collar Interference
                Decimal newGMtrIntf = getNewMtrInterference(convSpaceBelow, convBHALength, gMtrIntf, convAddSpBlw); //New Gaussmeter Motor Interference
                Decimal newGTtlIntf = Decimal.Add(newGDCIntf, newGMtrIntf); // New Total BHA Interference
                Decimal newGLCAZ = getLCAZError(TInc, MagAzm, HzComp, newGTtlIntf); //New Gaussmeter LCAZ Error due to BHA
                //Results
                iReply.Add(stdMPS);  // 1
                iReply.Add(stdDCPS); // 2
                iReply.Add(mtrIntf); // 3
                iReply.Add(clrIntf); // 4
                iReply.Add(ttlIntf); // 5
                iReply.Add(lcazBHA); // 6
                iReply.Add(alwErr); // 7
                iReply.Add(btsD);   // 8
                iReply.Add(newDCIntf); // 9
                iReply.Add(newMtrIntf); // 10
                iReply.Add(newttlIntf); // 11
                iReply.Add(newlcazBHA); // 12
                iReply.Add(newBTS); // 13
                iReply.Add(gMPS); // 14
                iReply.Add(gDCPS); //15
                iReply.Add(gMtrIntf); //16
                iReply.Add(gClrIntf); //17
                iReply.Add(gTtlIntf); //18
                iReply.Add(gLCAZ); //19
                iReply.Add(newGDCIntf); //20
                iReply.Add(newGMtrIntf); //21
                iReply.Add(newGTtlIntf); //22
                iReply.Add(newGLCAZ); //23
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 processSurvey(String UserName, String ClientName, Int32 MetaComboID, Int32 TieInSurveyID, Decimal Depth, Decimal Inclination, Decimal Azimuth, String dbConnection)
        {
            try
            {
                Int32 iResult = -99;
                Int32 iStore = -99;
                //Int32 iProcess = -99;
                Int32 iAccount = -99;
                Int32 calcNameID = -99;         //Calculator Name ID
                Int32 calcRMID = -99;           //Calculator Ref. Mag. ID
                Int32 calcBHAID = -99;          //Calculator BHA ID
                Int32 calcWetID = -99;          //Calculator Well End Trajectory ID
                Int32 calcAddSpaceID = -99;     //Calculator Additional Space ID

                Decimal motorSize = -99.00M;
                Int32 motorSizeUnit = -99;
                Decimal collarSize = -99.00M;
                Int32 collarSizeUnit = -99;
                Decimal bhaLength = -99.00M;
                Int32 bhaLengthUnit = -99;
                Decimal lenAbove = -99.00M;
                Int32 lenAboveUnit = -99;
                Decimal lenBelow = -99.00M;
                Int32 lenBelowUnit = -99;
                Decimal motorG = -99.000000M;
                Int32 motorGUnit = -99;
                Decimal collarG = -99.000000M;
                Int32 collarGUnit = -99;
                Decimal collarAdd = -99.00M;
                Int32 collarAddUnit = -99;
                Decimal motorAdd = -99.00M;
                Int32 motorAddUnit = -99;
                Decimal convergence = -99.00M;
                Int32 gConvergence = -99;
                Decimal refMagMDec = -99.00M;
                Decimal refMagDip = -99.00M;
                Int32 refMagBTotal = -99;
                Decimal wetTInc = -99.00M;
                Decimal wetAzm = -99.00M;
                Decimal wetMagAzm = -99.00M;
                Decimal wetHzComp = -99.00M;
                Decimal tisDepth = -99.00M;
                Int32 tisDepthU = -99; //Tie-In Survey Depth Unit
                Decimal tisInc = -99.00M;
                Decimal tisAzm = -99.00M;
                Decimal tisTVD = -99.00M;
                Decimal tisNorth = -99.00M;
                Decimal tisEast = -99.00M;
                Decimal clrSp = -99.00M; //Space Above sensors towards collar
                Decimal mtrSp = -99.00M; //Space Below sensors towards motor
                Decimal prevDepth = 0.00M;
                Decimal prevColE = 0.00M;
                Decimal prevIncliation = 0.00M;
                Decimal prevAzimuth = 0.00M;
                Decimal prevColP = 0.00M;
                // Processing Input Survey Number for selected MetaCombo
                Int32 sID = Convert.ToInt32(getInputSurveyID(MetaComboID, dbConnection));

                // *********** Processing meta data Variables **********
                using (SqlConnection metaCon = new SqlConnection(dbConnection))
                {
                    try
                    {
                        String metaCombo = "SELECT [inmcID], [inmcRMID], [inmcBHAID], [wetID], [inmcspID] FROM IndNonMagCalcCombo WHERE [cmboID] = @cmboID";
                        String metaBHA = "SELECT [inmcBHAID], [inmcBHAMSize], [inmcBHAMSizeU], [inmcBHADSCSize], [inmcBHADSCSizeU], [inmcBHALength], [inmcBHALengthU], [inmcBHALenAbv], [inmcBHALenAbvU], [inmcBHALenBlw], [inmcBHALenBlwU] FROM IndNonMagCalcBHA WHERE [inmcID] = @inmcID";
                        String metaBHADetail = "SELECT [inmcBHAMtrG], [inmcBHAMtrGU], [inmcBHAClrG], [inmcBHAClrGU], [inmcBHAClrAdd], [inmcBHAClrAddU], [inmcBHAMtrAdd], [inmcBHAMtrAddU] FROM IndNonMagCalcBHADetail WHERE [inmcBHAID] = @inmcBHAID";
                        //String metaRefMag = "SELECT [inmcRMGC], [inmcRMConvergence], [inmcRMMDec], [inmcRMDip], [inmcRMBT] FROM IndNonMagCalcRefMag WHERE [inmcID] = @inmcID";
                        String metaWET = "SELECT [wetTInc], [wetAzm], [wetMagAzm], [wetHzComp] FROM IndNonMagCalcWET WHERE [inmcID] = @inmcID";
                        String metaTieIn = "SELECT [depth], [depthUnit], [inclination], [azimuth], [tvd], [northing], [easting] FROM IndNonMagCalcTieInSurvey WHERE [comboID] = @comboID";
                        String metaAddSpacing = "SELECT [inmcspAbv], [inmcspBlw] FROM IndNonMagCalcAddSpacing WHERE [inmcID] = @inmcID";
                        String selPrevData = "SELECT [prevDepth], [prevsvwpColEVal], [prevInclination], [prevAzimuth], [prevsvwpColPVal] FROM IndNonMagCalcResults WHERE [cmboID] = @cmboID AND [srvyID] = @srvyID";

                        metaCon.Open();
                        SqlCommand getCombo = new SqlCommand(metaCombo, metaCon);
                        getCombo.Parameters.AddWithValue("@cmboID", SqlDbType.Int).Value = MetaComboID.ToString();
                        using (SqlDataReader readCombo = getCombo.ExecuteReader())
                        {
                            try
                            {
                                if (readCombo.HasRows)
                                {
                                    while (readCombo.Read())
                                    {
                                        calcNameID = readCombo.GetInt32(0);
                                        calcRMID = readCombo.GetInt32(1);
                                        calcBHAID = readCombo.GetInt32(2);
                                        calcWetID = readCombo.GetInt32(3);
                                        calcAddSpaceID = readCombo.GetInt32(4);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readCombo.Close();
                                readCombo.Dispose();
                            }
                        }
                        SqlCommand getBHA = new SqlCommand(metaBHA, metaCon);
                        getBHA.Parameters.AddWithValue("@inmcID", SqlDbType.Int).Value = calcNameID.ToString();
                        using (SqlDataReader readBHA = getBHA.ExecuteReader())
                        {
                            try
                            {
                                if (readBHA.HasRows)
                                {
                                    while (readBHA.Read())
                                    {
                                        calcBHAID = readBHA.GetInt32(0);
                                        motorSize = readBHA.GetDecimal(1);
                                        motorSizeUnit = readBHA.GetInt32(2);
                                        collarSize = readBHA.GetDecimal(3);
                                        collarSizeUnit = readBHA.GetInt32(4);
                                        bhaLength = readBHA.GetDecimal(5);
                                        bhaLengthUnit = readBHA.GetInt32(6);
                                        lenAbove = readBHA.GetDecimal(7);
                                        lenAboveUnit = readBHA.GetInt32(8);
                                        lenBelow = readBHA.GetDecimal(9);
                                        lenBelowUnit = readBHA.GetInt32(10);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readBHA.Close();
                                readBHA.Dispose();
                            }
                        }
                        SqlCommand getBHADetail = new SqlCommand(metaBHADetail, metaCon);
                        getBHADetail.Parameters.AddWithValue("@inmcBHAID", SqlDbType.Int).Value = calcBHAID.ToString();
                        using (SqlDataReader readBHADetail = getBHADetail.ExecuteReader())
                        {
                            try
                            {
                                if (readBHADetail.HasRows)
                                {
                                    while (readBHADetail.Read())
                                    {
                                        motorG = readBHADetail.GetDecimal(0);
                                        motorGUnit = readBHADetail.GetInt32(1);
                                        collarG = readBHADetail.GetDecimal(2);
                                        collarGUnit = readBHADetail.GetInt32(3);
                                        collarAdd = readBHADetail.GetDecimal(4);
                                        collarAddUnit = readBHADetail.GetInt32(5);
                                        motorAdd = readBHADetail.GetDecimal(6);
                                        motorAddUnit = readBHADetail.GetInt32(7);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readBHADetail.Close();
                                readBHADetail.Dispose();
                            }
                        }
                        
                        SqlCommand getWET = new SqlCommand(metaWET, metaCon);
                        getWET.Parameters.AddWithValue("@inmcID", SqlDbType.Int).Value = calcNameID.ToString();
                        using (SqlDataReader readWET = getWET.ExecuteReader())
                        {
                            try
                            {
                                if (readWET.HasRows)
                                {
                                    while (readWET.Read())
                                    {
                                        wetTInc = readWET.GetDecimal(0);
                                        wetAzm = readWET.GetDecimal(1);
                                        wetMagAzm = readWET.GetDecimal(2);
                                        wetHzComp = readWET.GetDecimal(3);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readWET.Close();
                                readWET.Dispose();
                            }
                        }
                        SqlCommand getTieIn = new SqlCommand(metaTieIn, metaCon);
                        getTieIn.Parameters.AddWithValue("@comboID", SqlDbType.Int).Value = calcNameID.ToString();
                        using (SqlDataReader readTieIn = getTieIn.ExecuteReader())
                        {
                            try
                            {
                                if (readTieIn.HasRows)
                                {
                                    while (readTieIn.Read())
                                    {
                                        tisDepth = readTieIn.GetDecimal(0);
                                        tisDepthU = readTieIn.GetInt32(1);
                                        tisInc = readTieIn.GetDecimal(2);
                                        tisAzm = readTieIn.GetDecimal(3);
                                        tisTVD = readTieIn.GetDecimal(4);
                                        tisNorth = readTieIn.GetDecimal(5);
                                        tisEast = readTieIn.GetDecimal(6);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readTieIn.Close();
                                readTieIn.Dispose();
                            }
                        }
                        SqlCommand getAddSp = new SqlCommand(metaAddSpacing, metaCon);
                        getAddSp.Parameters.AddWithValue("@inmcID", SqlDbType.Int).Value = calcNameID.ToString();
                        using (SqlDataReader readAddSp = getAddSp.ExecuteReader())
                        {
                            try
                            {
                                if (readAddSp.HasRows)
                                {
                                    while (readAddSp.Read())
                                    {
                                        clrSp = readAddSp.GetDecimal(0);
                                        mtrSp = readAddSp.GetDecimal(1);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readAddSp.Close();
                                readAddSp.Dispose();
                            }
                        }
                        //Calculated Values and variables used in processing
                        if (sID.Equals(1))
                        {
                            prevDepth = 0.00M;
                            prevColE = 0.00M;
                            prevIncliation = 0.00M;
                            prevAzimuth = 0.00M;
                            prevColP = 0.00M;
                        }
                        else
                        {
                            Int32 prevID = sID - 1;
                            SqlCommand getPrevData = new SqlCommand(selPrevData, metaCon);
                            getPrevData.Parameters.AddWithValue("@cmboID", SqlDbType.Int).Value = MetaComboID.ToString();
                            getPrevData.Parameters.AddWithValue("@srvyID", SqlDbType.Int).Value = prevID.ToString();
                            using (SqlDataReader readPrevData = getPrevData.ExecuteReader())
                            {
                                try
                                {
                                    if (readPrevData.HasRows)
                                    {
                                        while (readPrevData.Read())
                                        {
                                            prevDepth = readPrevData.GetDecimal(0);
                                            prevColE = readPrevData.GetDecimal(1);
                                            prevIncliation = readPrevData.GetDecimal(2);
                                            prevAzimuth = readPrevData.GetDecimal(3);
                                            prevColP = readPrevData.GetDecimal(4);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readPrevData.Close();
                                    readPrevData.Dispose();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        metaCon.Close();
                        metaCon.Dispose();
                    }                    
                }                

                // Finished processing meta data variables
                //*********************  Processing Variables  ****************
                Decimal CMA = -99.00M; //Current Magnetic Azimuth - colD19
                Decimal BHOR = -99.00M; //Horizontal Component of Bt(nT) - colD21
                Decimal cnvSpAbv = -99.00M; //Converted Additional Space Above - colI21
                Decimal cnvSpBlw = -99.00M; //Converted Additional Space Below - colI22
                Decimal cnvLenAbv = -99.00M; //Converted BHA Length Abv
                Decimal cnvLenBlw = -99.00M; //Converted BHA Length Below
                Decimal cnvBHALen = -99.00M;
                Decimal cnvClrSz = -99.00M;
                Decimal cnvMtrSz = -99.00M;
                Decimal stdValMPS = -99.00M; //Standard Value Motor Pole Strength
                Decimal stdValDCPS = -99.00M; //Standard Value Drill Collar Pole Strength
                Decimal interfDC = -99.00M; //Drill Collar Interference
                Decimal interfMtr = -99.00M; //Motor Interference
                Decimal gmtrMtrMPS = -99.00M; //Guassmeter based MPS - ColH31
                Decimal gmtrClrMPS = -99.00M; //Guassmeter based DCPS - colH32
                Decimal gmtrInterMtr = -99.00M; //Guassmeter Motor Interference - ColH34
                Decimal gmtrInterClr = -99.00M; //Guassmeter Collar Interference - ColH35
                Decimal cnvTGuassmeter = -99.00M; //Converted Guassmeter Reading from Top
                Decimal cnvBGuassmeter = -99.00M; //Converted Guassmeter Reading from Bottom
                Decimal MagDeclination = -99.00M; //Ref. Mag. Magnetic Declination
                Decimal HzComponent = -99.00M; //WET wetHzComp                
                Decimal TotalBHAInterference = -99.00M; //Total BHA Interference - colD37
                Decimal GMtrTotalBHAInterference = -99.00M; //Total Gaussmeter BHA Interference  - colH37
                Decimal bhaLCAZError = -99.00M; //Delta-LCAZ Error due to BHA - colD38
                Decimal GMtrLCAZError = -99.00M; //Gaussmeter Delta-LCAZ Error due to BHA - colH38
                Decimal bhaAllowableError = -99.00M; //NM Spacing Allowable Error - colD39
                Decimal btsDistance = -99.00M; //Bit To Sensor Distance - colD40
                Decimal newDCInterference = -99.00M; //New Drill Collar Interference - colD46
                Decimal newGmtrDCInterference = -99.00M; //New Guassmeter Drill Collar Interference - colH46
                Decimal newMtrInterference = -99.00M; //New Motor Interference - colD47
                Decimal newGmtrMotorInterference = -99.00M; //New Gaussmeter Motor Interference = colH47
                Decimal newTotalInterference = -99.00M; //New Total Interference - colD49
                Decimal newGmtrTotalInterference = -99.00M; //New Guassmeter Total Interference = colH49
                Decimal newLCAZError = -99.00M; //NEW Delta-LCAZ Error due to BHA - colD50
                Decimal newGMtrLCAZError = -99.00M; //New Gausmeter Delta-LCAZ Error due to BHA - colH50
                Decimal newBTSDistance = -99.00M; //NEW Bit-To-Sensor Distance - colD51
                                                
                //Standard Value Well Position Variables
                Decimal svwpColEVal = -99.00M;
                Decimal svwpColFVal = -99.00M;
                Decimal svwpColGVal = -99.00M;
                Decimal svwpColHVal = -99.00M;
                Decimal svwpColIVal = -99.00M;
                Decimal svwpColJVal = -99.00M;
                Decimal svwpColKVal = -99.00M;
                Decimal svwpColLVal = -99.00M;
                Decimal svwpColMVal = -99.00M;
                Decimal svwpColNVal = -99.00M;
                Decimal svwpColPVal = -99.00M;
                Decimal svwpColQVal = -99.000000M;
                Decimal svwpColRVal = -99.000000M;
                Decimal svwpColSVal = -99.000000M;
                Decimal svwpCTEVal = -99.000000M; //svwpCummulative Target Error - colD41
                //Standard Value Well Position + Additional NM
                Decimal svwpnmColQVal = -99.000000M;
                Decimal svwpnmColRVal = -99.000000M;
                Decimal svwpnmColSVal = -99.000000M;
                Decimal svwpnmCTEVal = -99.000000M; //new Accumulative Target Error - colD52
                //Gaussmeter Well Position
                Decimal gwpCTEVal = -99.000000M; //Gaussmeter Cummulative Target Error - colH41
                Decimal gwpColQVal = -99.000000M;
                Decimal gwpColRVal = -99.000000M;
                Decimal gwpColSVal = -99.000000M;
                //Gaussmeter Well Position + Additional Non-Mag
                Decimal gwpnmCTEVal = -99.000000M; //Guassmeter new Accumulative Target Error - colH52
                Decimal gwpnmColQVal = -99.000000M;
                Decimal gwpnmColRVal = -99.000000M;
                Decimal gwpnmColSVal = -99.000000M;
                //Worst Case Well Position
                //Decimal wpColEVal = -99.00M;
                //Decimal wpColFVal = -99.00M;
                //Decimal wpColGVal = -99.00M;
                //Decimal wpColHVal = -99.00M;
                //Decimal wpColIVal = -99.00M;
                //Decimal wpColJVal = -99.00M;
                //Decimal wpColKVal = -99.00M;
                //Decimal wpColLVal = -99.00M;
                //Decimal wpColMVal = -99.00M;
                //Decimal wpColNVal = -99.00M;
                //Decimal wpColPVal = -99.00M;
                //Decimal wpColQVal = -99.000000M;
                //Decimal wpColRVal = -99.000000M;
                //Decimal wpColSVal = -99.000000M;
                //Worst Case Well Position + Additional Non-Mag
                //Decimal wpnmColEVal = -99.00M;
                //Decimal wpnmColFVal = -99.00M;
                //Decimal wpnmColGVal = -99.00M;
                //Decimal wpnmColHVal = -99.00M;
                //Decimal wpnmColIVal = -99.00M;
                //Decimal wpnmColJVal = -99.00M;
                //Decimal wpnmColKVal = -99.00M;
                //Decimal wpnmColLVal = -99.00M;
                //Decimal wpnmColMVal = -99.00M;
                //Decimal wpnmColNVal = -99.00M;
                //Decimal wpnmColPVal = -99.00M;
                //Decimal wpnmColQVal = -99.000000M;
                //Decimal wpnmColRVal = -99.000000M;
                //Decimal wpnmColSVal = -99.000000M;                                               

                //Calculating values
                CMA = getCMA(gConvergence, convergence, refMagMDec, wetAzm);
                BHOR = getBHOR(refMagDip, refMagBTotal);
                cnvSpAbv = getConvertedSpace(lenAboveUnit, clrSp);
                cnvSpBlw = getConvertedSpace(lenBelowUnit, mtrSp);
                cnvMtrSz = getConvertedSpace(motorSizeUnit, motorSize);
                cnvClrSz = getConvertedSpace(collarSizeUnit, collarSize);
                cnvLenAbv = getConvertedSpace(lenAboveUnit, lenAbove);
                cnvLenBlw = getConvertedSpace(lenBelowUnit, lenBelow);
                cnvBHALen = getConvertedSpace(bhaLengthUnit, bhaLength);
                cnvTGuassmeter = getConvertedGaussmeter(motorGUnit, motorG);
                cnvBGuassmeter = getConvertedGaussmeter(collarGUnit, collarG);
                stdValMPS = getMPS(cnvMtrSz);
                stdValDCPS = getDCPS(cnvClrSz);
                interfMtr = getMtrInterference(stdValMPS, cnvLenBlw, cnvBHALen);
                interfDC = getClrInterference(stdValDCPS, cnvLenAbv);
                TotalBHAInterference = (interfMtr + interfDC);
                bhaLCAZError = getLCAZError(wetTInc, wetAzm, BHOR, TotalBHAInterference);
                bhaAllowableError = getNMSpAllowableError(wetTInc, wetMagAzm);
                btsDistance = (cnvBHALen + cnvLenBlw);
                newDCInterference = getNewDCInterference(cnvLenAbv, stdValDCPS, cnvSpAbv);                
                newMtrInterference = getNewMtrInterference(cnvLenBlw, cnvBHALen, stdValMPS, cnvSpBlw);
                newTotalInterference = (newDCInterference + newMtrInterference);
                newLCAZError = getNewLCAZError(wetTInc, wetMagAzm, BHOR, newTotalInterference);
                newBTSDistance = getNewBTSDistance(cnvLenBlw, cnvBHALen, cnvSpBlw);
                svwpColEVal = colE(sID, Depth, prevDepth, tisDepth);
                svwpColFVal = colF(sID, Inclination, prevIncliation, Azimuth, prevAzimuth);
                svwpColGVal = colG(svwpColFVal);
                svwpColHVal = colH(svwpColGVal);
                svwpColIVal = colI(sID, Inclination, prevIncliation, Azimuth, svwpColEVal, svwpColHVal);
                svwpColJVal = colJ(sID, Inclination, prevIncliation, Azimuth, prevAzimuth, prevColE, svwpColEVal, Depth, prevDepth, svwpColHVal);
                svwpColKVal = colK(sID, Depth, prevDepth, Inclination, prevIncliation, Azimuth, prevAzimuth, svwpColHVal);
                svwpColLVal = colL(sID, MetaComboID, tisNorth, Depth, svwpColJVal, dbConnection);
                svwpColMVal = colM(sID, MetaComboID, tisEast, Depth, svwpColKVal, dbConnection);
                svwpColNVal = colN(sID, MetaComboID, tisTVD, Depth, svwpColIVal, dbConnection);
                svwpColPVal = colP(Depth, svwpColLVal, svwpColMVal);
                svwpColQVal = colQ(TotalBHAInterference, HzComponent, MagDeclination, Inclination, Azimuth);
                svwpColRVal = colR(sID, tisDepthU, svwpColPVal, prevColP, svwpColQVal);
                svwpColSVal = colS(sID, MetaComboID, svwpColRVal, dbConnection);
                svwpCTEVal = svwpCTE(MetaComboID, sID, svwpColRVal, dbConnection);                
                svwpnmColQVal = svwpNMColQ(newTotalInterference, Inclination, Azimuth, cnvClrSz, BHOR);
                svwpnmColRVal = colR(sID, tisDepthU, svwpColPVal, prevColP, svwpnmColQVal);
                svwpnmColSVal = colS(sID, MetaComboID, svwpnmColRVal, dbConnection);
                svwpnmCTEVal = getAccumulativeTargetError(tisDepthU, svwpCTEVal);
                //Calculating Worst Gaussmeter based values
                gmtrMtrMPS = getGMtrMPS(cnvMtrSz, cnvTGuassmeter, cnvLenAbv);
                gmtrClrMPS = getGMtrDCPS(cnvClrSz, cnvBGuassmeter, cnvLenBlw);
                gmtrInterMtr = getGaussmeterMotorInterference(cnvBHALen, cnvLenBlw, gmtrMtrMPS);
                gmtrInterClr = getGaussmeterCollarInterference(cnvLenAbv, gmtrClrMPS);
                GMtrTotalBHAInterference = (gmtrInterMtr + gmtrInterClr);
                GMtrLCAZError = getLCAZError(wetTInc, wetAzm, BHOR, GMtrTotalBHAInterference);
                gwpColQVal = colQ(GMtrTotalBHAInterference, HzComponent, MagDeclination, Inclination, Azimuth);
                gwpColRVal = colR(sID, tisDepthU, svwpColPVal, prevColP, gwpColQVal);
                gwpColSVal = gwpColS(sID, MetaComboID, gwpColRVal, dbConnection);
                gwpCTEVal = gwpCTE(MetaComboID, sID, gwpColRVal, dbConnection);
                newGmtrDCInterference = getNewDCInterference(cnvLenAbv, gmtrClrMPS, cnvSpAbv);
                newGmtrMotorInterference = getNewMtrInterference(cnvLenBlw, cnvBHALen, gmtrMtrMPS, cnvSpBlw);
                newGmtrTotalInterference = (newGmtrDCInterference + newGmtrMotorInterference);
                newGMtrLCAZError = getNewLCAZError(wetTInc, wetMagAzm, BHOR, newGmtrTotalInterference);
                gwpnmCTEVal = getAccumulativeTargetError(tisDepthU, gwpCTEVal);

                //Assiging current values to last row values for next row processing
                prevDepth = Depth;
                prevColE = svwpColEVal;
                prevIncliation = Inclination;
                prevAzimuth = Azimuth;
                prevColP = svwpColPVal;

                iStore = storeResult(UserName, ClientName, MetaComboID, sID, Depth, Inclination, Azimuth, svwpColEVal, svwpColFVal, svwpColGVal, svwpColHVal, svwpColIVal, svwpColJVal, svwpColKVal, svwpColLVal, svwpColMVal, svwpColNVal, svwpColPVal, svwpColQVal, svwpColRVal, svwpColSVal, svwpCTEVal, svwpnmColQVal, svwpnmColRVal, svwpColSVal, gwpColQVal, gwpColRVal, gwpColSVal, gwpnmColQVal, gwpnmColRVal, gwpnmColSVal, CMA, BHOR, stdValMPS, stdValDCPS, interfMtr, interfDC, TotalBHAInterference, bhaLCAZError, bhaAllowableError, btsDistance, newDCInterference, newMtrInterference, newTotalInterference, newLCAZError, newBTSDistance, svwpnmCTEVal, gmtrMtrMPS, gmtrClrMPS, gmtrInterMtr, gmtrInterClr, GMtrTotalBHAInterference, GMtrLCAZError, gwpCTEVal, newGmtrDCInterference, newGmtrMotorInterference, newGmtrTotalInterference, newGMtrLCAZError, gwpnmCTEVal, prevDepth, prevColE, prevIncliation, prevAzimuth, prevColP, dbConnection);
                iAccount = accountResult(UserName, ClientName, MetaComboID, sID, Depth);                               

                if (iStore > 0 && iAccount.Equals(1))
                {
                    iResult = iStore;
                }
                else
                {
                    iResult = 0;
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 storeTieInSurvey(String clntUserName, String clntDomain, String SurveyName, Int32 CalcComboID, Decimal SrvyDepth, Int32 SrvyDepthUnit, Decimal SrvyInclination, Decimal SrvyAzimuth, Decimal SrvyTVD, Decimal SrvyNorthing, Decimal SrvyEasting, String dbConnection)
        {
            try
            {
                Int32 iResult = -99;
                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;                

                String insertCommand = "INSERT INTO IndNonMagCalcTieInSurvey ([tisName], [comboID], [depth], [depthUnit], [inclination], [azimuth], [tvd], [northing], [easting], [clntUserName], [clntDomain], [cTime], [uTime]) VALUES (@tisName, @comboID, @depth, @depthUnit, @inclination, @azimuth, @tvd, @northing, @easting, @clntUserName, @clntDomain, @cTime, @uTime)";
                using(SqlConnection insCon = new SqlConnection(dbConnection))
                {
                    try
                    {
                        insCon.Open();
                        SqlCommand insertData = new SqlCommand(insertCommand, insCon);
                        insertData.Parameters.AddWithValue("@comboID", SqlDbType.Int).Value = CalcComboID.ToString();
                        insertData.Parameters.AddWithValue("@tisName", SqlDbType.NVarChar).Value = SurveyName.ToString();
                        insertData.Parameters.AddWithValue("@depth", SqlDbType.Decimal).Value = SrvyDepth.ToString();
                        insertData.Parameters.AddWithValue("@depthUnit", SqlDbType.Int).Value = SrvyDepthUnit.ToString();
                        insertData.Parameters.AddWithValue("@inclination", SqlDbType.Decimal).Value = SrvyInclination.ToString();
                        insertData.Parameters.AddWithValue("@azimuth", SqlDbType.Decimal).Value = SrvyAzimuth.ToString();
                        insertData.Parameters.AddWithValue("@tvd", SqlDbType.Decimal).Value = SrvyTVD.ToString();
                        insertData.Parameters.AddWithValue("@northing", SqlDbType.Decimal).Value = SrvyNorthing.ToString();
                        insertData.Parameters.AddWithValue("@easting", SqlDbType.Decimal).Value = SrvyEasting.ToString();
                        insertData.Parameters.AddWithValue("@clntUserName", SqlDbType.NVarChar).Value = clntUserName.ToString();
                        insertData.Parameters.AddWithValue("@clntDomain", SqlDbType.NVarChar).Value = clntDomain.ToString();
                        insertData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = crTime.ToString();
                        insertData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = updTime.ToString();

                        iResult = Convert.ToInt32(insertData.ExecuteNonQuery());
                    }
                    catch(Exception ex)
                    {throw new System.Exception(ex.ToString());}
                    finally
                    {
                        insCon.Close();
                        insCon.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        private static Int32 accountResult(String UserName, String Domain, Int32 CalcComboID, Int32 SurveyID, Decimal Depth)
        {
            try
            {
                Int32 iResult = -99;
                DateTime updTime = DateTime.Now;

                String insertCommand = "INSERT INTO accBHACalculator ([clntUsername], [clntRealm], [comboID], [srvyNo], [depth], [pTime]) VALUES (@clntUsername, @clntRealm, @comboID, @srvyNo, @depth, @pTime)";
                using (SqlConnection insCon = new SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["FEAccountingDB"].ConnectionString))
                {
                    try
                    {
                        insCon.Open();
                        SqlCommand insData = new SqlCommand(insertCommand, insCon);
                        insData.Parameters.AddWithValue("@clntUsername", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@clntRealm", SqlDbType.NVarChar).Value = Domain.ToString();
                        insData.Parameters.AddWithValue("@comboID", SqlDbType.Int).Value = CalcComboID.ToString();
                        insData.Parameters.AddWithValue("@srvyNo", SqlDbType.Int).Value = SurveyID.ToString();
                        insData.Parameters.AddWithValue("@depth", SqlDbType.Decimal).Value = Depth.ToString();
                        insData.Parameters.AddWithValue("@pTime", SqlDbType.DateTime2).Value = updTime.ToString();

                        iResult = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch(Exception ex)
                    {throw new System.Exception(ex.ToString());}
                    finally
                    {
                        insCon.Close();
                        insCon.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 storeResult(String clntUserName, String clntDomain, Int32 CalcComboID, Int32 SurveyNumber, Decimal MD, Decimal Inc, Decimal Azm, Decimal colE, Decimal colF, Decimal colG, Decimal colH, Decimal colI, Decimal colJ, Decimal colK, Decimal colL, Decimal colM, Decimal colN, Decimal colP, Decimal colQ, Decimal colR, Decimal colS, Decimal CTE, Decimal nmcolQ, Decimal nmcolR, Decimal nmcolS, Decimal gcolQ, Decimal gcolR, Decimal gcolS, Decimal gnmcolQ, Decimal gnmcolR, Decimal gnmcolS, Decimal CMA, Decimal BHOR, Decimal stdValMPS, Decimal stdValDCPS, Decimal interfMtr, Decimal interfDC, Decimal totalBHAInterference, Decimal bhaLCAZError, Decimal bhaAllowableError, Decimal btsDistance, Decimal newDCInterference, Decimal newMtrInterference, Decimal newTotalInterference, Decimal newLCAZError, Decimal newBTSDistance, Decimal svwpnmCTEVal, Decimal gmtrMtrMPS, Decimal gmtrClrMPS, Decimal gmtrInterMtr, Decimal gmtrInterClr, Decimal GMtrTotalBHAInterference, Decimal GMtrLCAZError, Decimal gwpCTEVal, Decimal newGmtrDCInterference, Decimal newGmtrMotorInterference, Decimal newGmtrTotalInterference, Decimal newGMtrLCAZError, Decimal gwpnmCTEVal, Decimal prevDepth, Decimal prevsvwpColEVal, Decimal prevInclination, Decimal prevAzimuth, Decimal prevsvwpColPVal, String dbConnection)
        {
            try
            {
                Int32 iResult = -99;
                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;

                String insertCommand = "INSERT INTO IndNonMagCalcResults ([cmboID], [srvyID], [MD], [Inc], [Azm], [svwpColE], [svwpColF], [svwpColG], [svwpColH], [svwpColI], [svwpColJ], [svwpColK], [svwpColL], [svwpColM], [svwpColN], [svwpColP], [svwpColQ], [svwpColR], [svwpColS], [svwpCTE], [svwpnmColQ], [svwpnmColR], [svwpnmColS], [gwpColQ], [gwpColR], [gwpColS], [gwpnmColQ], [gwpnmColR], [gwpnmColS], [CMA], [BHOR], [stdValMPS], [stdValDCPS], [interfMtr], [interfDC], [totalBHAInterference], [bhaLCAZError], [bhaAllowableError], [btsDistance], [newDCInterference], [newMtrInterference], [newTotalInterference], [newLCAZError], [newBTSDistance], [svwpnmCTEVal], [gmtrMtrMPS], [gmtrClrMPS], [gmtrInterMtr], [gmtrInterClr], [GMtrTotalBHAInterference], [GMtrLCAZError], [gwpCTEVal], [newGmtrDCInterference], [newGmtrMotorInterference], [newGmtrTotalInterference], [newGMtrLCAZError], [gwpnmCTEVal], [prevDepth], [prevsvwpColEVal], [prevInclination], [prevAzimuth], [prevsvwpColPVal], [UserName], [Realm], [cTime], [uTime]) VALUES (@cmboID, @srvyID, @MD, @Inc, @Azm, @svwpColE, @svwpColF, @svwpColG, @svwpColH, @svwpColI, @svwpColJ, @svwpColK, @svwpColL, @svwpColM, @svwpColN, @svwpColP, @svwpColQ, @svwpColR, @svwpColS, @svwpCTE, @svwpnmColQ, @svwpnmColR, @svwpnmColS, @gwpColQ, @gwpColR, @gwpColS, @gwpnmColQ, @gwpnmColR, @gwpnmColS, @CMA, @BHOR, @stdValMPS, @stdValDCPS, @interfMtr, @interfDC, @totalBHAInterference, @bhaLCAZError, @bhaAllowableError, @btsDistance, @newDCInterference, @newMtrInterference, @newTotalInterference, @newLCAZError, @newBTSDistance, @svwpnmCTEVal, @gmtrMtrMPS, @gmtrClrMPS, @gmtrInterMtr, @gmtrInterClr, @GMtrTotalBHAInterference, @GMtrLCAZError, @gwpCTEVal, @newGmtrDCInterference, @newGmtrMotorInterference, @newGmtrTotalInterference, @newGMtrLCAZError, @gwpnmCTEVal, @prevDepth, @prevsvwpColEVal, @prevInclination, @prevAzimuth, @prevsvwpColPVal, @UserName, @Realm, @cTime, @uTime); SELECT SCOPE_IDENTITY()";
                using (SqlConnection insCon = new SqlConnection(dbConnection))
                {
                    try
                    {
                        insCon.Open();
                        SqlCommand insertData = new SqlCommand(insertCommand, insCon);
                        insertData.Parameters.AddWithValue("@cmboID", SqlDbType.Int).Value = CalcComboID.ToString();
                        insertData.Parameters.AddWithValue("@srvyID", SqlDbType.Int).Value = SurveyNumber.ToString();
                        insertData.Parameters.AddWithValue("@MD", SqlDbType.Decimal).Value = MD.ToString();
                        insertData.Parameters.AddWithValue("@Inc", SqlDbType.Decimal).Value = Inc.ToString();
                        insertData.Parameters.AddWithValue("@Azm", SqlDbType.Decimal).Value = Azm.ToString();
                        insertData.Parameters.AddWithValue("@svwpColE", SqlDbType.Decimal).Value = colE.ToString();
                        insertData.Parameters.AddWithValue("@svwpColF", SqlDbType.Decimal).Value = colF.ToString();
                        insertData.Parameters.AddWithValue("@svwpColG", SqlDbType.Decimal).Value = colG.ToString();
                        insertData.Parameters.AddWithValue("@svwpColH", SqlDbType.Decimal).Value = colH.ToString();
                        insertData.Parameters.AddWithValue("@svwpColI", SqlDbType.Decimal).Value = colI.ToString();
                        insertData.Parameters.AddWithValue("@svwpColJ", SqlDbType.Decimal).Value = colJ.ToString();
                        insertData.Parameters.AddWithValue("@svwpColK", SqlDbType.Decimal).Value = colK.ToString();
                        insertData.Parameters.AddWithValue("@svwpColL", SqlDbType.Decimal).Value = colL.ToString();
                        insertData.Parameters.AddWithValue("@svwpColM", SqlDbType.Decimal).Value = colM.ToString();
                        insertData.Parameters.AddWithValue("@svwpColN", SqlDbType.Decimal).Value = colN.ToString();
                        insertData.Parameters.AddWithValue("@svwpColP", SqlDbType.Decimal).Value = colP.ToString();
                        insertData.Parameters.AddWithValue("@svwpColQ", SqlDbType.Decimal).Value = colQ.ToString();
                        insertData.Parameters.AddWithValue("@svwpColR", SqlDbType.Decimal).Value = colR.ToString();
                        insertData.Parameters.AddWithValue("@svwpColS", SqlDbType.Decimal).Value = colS.ToString();
                        insertData.Parameters.AddWithValue("@svwpCTE", SqlDbType.Decimal).Value = CTE.ToString();
                        insertData.Parameters.AddWithValue("@svwpnmColQ", SqlDbType.Decimal).Value = nmcolQ.ToString();
                        insertData.Parameters.AddWithValue("@svwpnmColR", SqlDbType.Decimal).Value = nmcolR.ToString();
                        insertData.Parameters.AddWithValue("@svwpnmColS", SqlDbType.Decimal).Value = nmcolS.ToString();
                        insertData.Parameters.AddWithValue("@gwpColQ", SqlDbType.Decimal).Value = gcolQ.ToString();
                        insertData.Parameters.AddWithValue("@gwpColR", SqlDbType.Decimal).Value = gcolR.ToString();
                        insertData.Parameters.AddWithValue("@gwpColS", SqlDbType.Decimal).Value = gcolS.ToString();
                        insertData.Parameters.AddWithValue("@gwpnmColQ", SqlDbType.Decimal).Value = gnmcolQ.ToString();
                        insertData.Parameters.AddWithValue("@gwpnmColR", SqlDbType.Decimal).Value = gnmcolR.ToString();
                        insertData.Parameters.AddWithValue("@gwpnmColS", SqlDbType.Decimal).Value = gnmcolS.ToString();
                        insertData.Parameters.AddWithValue("@CMA", SqlDbType.Decimal).Value = CMA.ToString();
                        insertData.Parameters.AddWithValue("@BHOR", SqlDbType.Decimal).Value = BHOR.ToString();
                        insertData.Parameters.AddWithValue("@stdValMPS", SqlDbType.Decimal).Value = stdValMPS.ToString();
                        insertData.Parameters.AddWithValue("@stdValDCPS", SqlDbType.Decimal).Value = stdValDCPS.ToString();
                        insertData.Parameters.AddWithValue("@interfMtr", SqlDbType.Decimal).Value = interfMtr.ToString();
                        insertData.Parameters.AddWithValue("@interfDC", SqlDbType.Decimal).Value = interfDC.ToString();
                        insertData.Parameters.AddWithValue("@totalBHAInterference", SqlDbType.Decimal).Value = totalBHAInterference.ToString();
                        insertData.Parameters.AddWithValue("@bhaLCAZError", SqlDbType.Decimal).Value = bhaLCAZError.ToString();
                        insertData.Parameters.AddWithValue("@bhaAllowableError", SqlDbType.Decimal).Value = bhaAllowableError.ToString();
                        insertData.Parameters.AddWithValue("@btsDistance", SqlDbType.Decimal).Value = btsDistance.ToString();
                        insertData.Parameters.AddWithValue("@newDCInterference", SqlDbType.Decimal).Value = newDCInterference.ToString();
                        insertData.Parameters.AddWithValue("@newMtrInterference", SqlDbType.Decimal).Value = newMtrInterference.ToString();
                        insertData.Parameters.AddWithValue("@newTotalInterference", SqlDbType.Decimal).Value = newTotalInterference.ToString();
                        insertData.Parameters.AddWithValue("@newLCAZError", SqlDbType.Decimal).Value = newLCAZError.ToString();
                        insertData.Parameters.AddWithValue("@newBTSDistance", SqlDbType.Decimal).Value = newBTSDistance.ToString();
                        insertData.Parameters.AddWithValue("@svwpnmCTEVal", SqlDbType.Decimal).Value = svwpnmCTEVal.ToString();
                        insertData.Parameters.AddWithValue("@gmtrMtrMPS", SqlDbType.Decimal).Value = gmtrMtrMPS.ToString();
                        insertData.Parameters.AddWithValue("@gmtrClrMPS", SqlDbType.Decimal).Value = gmtrClrMPS.ToString();
                        insertData.Parameters.AddWithValue("@gmtrInterMtr", SqlDbType.Decimal).Value = gmtrInterMtr.ToString();
                        insertData.Parameters.AddWithValue("@gmtrInterClr", SqlDbType.Decimal).Value = gmtrInterClr.ToString();
                        insertData.Parameters.AddWithValue("@GMtrTotalBHAInterference", SqlDbType.Decimal).Value = GMtrTotalBHAInterference.ToString();
                        insertData.Parameters.AddWithValue("@GMtrLCAZError", SqlDbType.Decimal).Value = GMtrLCAZError.ToString();
                        insertData.Parameters.AddWithValue("@gwpCTEVal", SqlDbType.Decimal).Value = gwpCTEVal.ToString();
                        insertData.Parameters.AddWithValue("@newGmtrDCInterference", SqlDbType.Decimal).Value = newGmtrDCInterference.ToString();
                        insertData.Parameters.AddWithValue("@newGmtrMotorInterference", SqlDbType.Decimal).Value = newGmtrMotorInterference.ToString();
                        insertData.Parameters.AddWithValue("@newGmtrTotalInterference", SqlDbType.Decimal).Value = newGmtrTotalInterference.ToString();
                        insertData.Parameters.AddWithValue("@newGMtrLCAZError", SqlDbType.Decimal).Value = newGMtrLCAZError.ToString();
                        insertData.Parameters.AddWithValue("@gwpnmCTEVal", SqlDbType.Decimal).Value = gwpnmCTEVal.ToString();
                        insertData.Parameters.AddWithValue("@prevDepth", SqlDbType.Decimal).Value = prevDepth.ToString();
                        insertData.Parameters.AddWithValue("@prevsvwpColEVal", SqlDbType.Decimal).Value = prevsvwpColEVal.ToString();
                        insertData.Parameters.AddWithValue("@prevInclination", SqlDbType.Decimal).Value = prevInclination.ToString();
                        insertData.Parameters.AddWithValue("@prevAzimuth", SqlDbType.Decimal).Value = prevAzimuth.ToString();
                        insertData.Parameters.AddWithValue("@prevsvwpColPVal", SqlDbType.Decimal).Value = prevsvwpColPVal.ToString();
                        insertData.Parameters.AddWithValue("@UserName", SqlDbType.NVarChar).Value = clntUserName.ToString();
                        insertData.Parameters.AddWithValue("@Realm", SqlDbType.NVarChar).Value = clntDomain.ToString();
                        insertData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = crTime.ToString();
                        insertData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = updTime.ToString();

                        iResult = Convert.ToInt32(insertData.ExecuteScalar());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        insCon.Close();
                        insCon.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 getTieInSurveyID(Int32 comboID, String dbConnection)
        {
            try
            {
                Int32 iResult = -99;
                String selSrvyNo = "SELECT ISNULL(MAX(srvyRowID), 0) + 1 FROM IndNonMagCalcTieInSurvey WHERE [comboID] = @comboID";

                using (SqlConnection snoCon = new SqlConnection(dbConnection))
                {
                    try
                    {
                        snoCon.Open();
                        SqlCommand getSNO = new SqlCommand(selSrvyNo, snoCon);
                        getSNO.Parameters.AddWithValue("@comboID", SqlDbType.Int).Value = comboID.ToString();
                        using (SqlDataReader readSNO = getSNO.ExecuteReader())
                        {
                            try
                            {
                                if (readSNO.HasRows)
                                {
                                    while (readSNO.Read())
                                    {
                                        iResult = readSNO.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readSNO.Close();
                                readSNO.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        snoCon.Close();
                        snoCon.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 getInputSurveyID(Int32 comboID, String dbConnection)
        {
            try
            {
                Int32 iResult = -99;
                String selSrvyNo = "SELECT ISNULL(MAX(srvyID), 0) + 1 FROM IndNonMagCalcResults WHERE [cmboID] = @cmboID";

                using (SqlConnection snoCon = new SqlConnection(dbConnection))
                {
                    try
                    {
                        snoCon.Open();
                        SqlCommand getSNO = new SqlCommand(selSrvyNo, snoCon);
                        getSNO.Parameters.AddWithValue("@cmboID", SqlDbType.Int).Value = comboID.ToString();
                        using (SqlDataReader readSNO = getSNO.ExecuteReader())
                        {
                            try
                            {
                                if (readSNO.HasRows)
                                {
                                    while (readSNO.Read())
                                    {
                                        iResult = readSNO.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readSNO.Close();
                                readSNO.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        snoCon.Close();
                        snoCon.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getBHACalcNamesTable(String dbConn)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn inmcID = iReply.Columns.Add("inmcID", typeof(Int32));
                DataColumn inmcName = iReply.Columns.Add("inmcName", typeof(String));
                DataColumn refM = iReply.Columns.Add("refM", typeof(String));
                DataColumn bha = iReply.Columns.Add("bha", typeof(String));
                DataColumn wet = iReply.Columns.Add("wet", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(String));
                String selData = "SELECT [inmcID], [inmcName], [uTime] FROM [IndNonMagCalcName] ORDER BY [inmcName]";
                Int32 id = -99, r = -99, b = -99, w = -99;
                String name = String.Empty, rf = String.Empty, ba = String.Empty, wt = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        uT = readData.GetDateTime(2);
                                        r = checkRF(id, dbConn);
                                        if (r >= 1)
                                        {
                                            rf = "Available";
                                        }
                                        else
                                        {
                                            rf = "Un-Available";
                                        }
                                        b = checkBHA(id, dbConn);
                                        if (b >= 1)
                                        {
                                            ba = "Available";
                                        }
                                        else
                                        {
                                            ba = "Un-Available";
                                        }
                                        w = checkWET(id, dbConn);
                                        if (w >= 1)
                                        {
                                            wt = "Available";
                                        }
                                        else
                                        {
                                            wt = "Un-Available";
                                        }
                                        iReply.Rows.Add(id, name, rf, ba, wt, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getBHACalcNamesList(String dbConn)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [inmcID], [inmcName] FROM [IndNonMagCalcName] ORDER BY [inmcName]";
                Int32 id = -99, b = -99, r = -99, w = -99;
                String name = String.Empty, rf = String.Empty, bha = String.Empty, wet = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        r = checkRF(id, dbConn);
                                        if (r >= 1)
                                        {
                                            rf = "";
                                        }
                                        else
                                        {
                                            rf = "Ref. Mags Not Available";
                                        }
                                        b = checkBHA(id, dbConn);
                                        if (b >= 1)
                                        {
                                            bha = "";
                                        }
                                        else
                                        {
                                            bha = "BHA Signature Not Available";
                                        }
                                        w = checkWET(id, dbConn);
                                        if (w >= 1)
                                        {
                                            wet = "";
                                        }
                                        else
                                        {
                                            wet = "Well End Trajectory Not Available";
                                        }
                                        name = String.Format("{0} --- {1} {2} {3}", name, rf, bha, wet);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<String> getBHASignature(Int32 CalculatorID, String dbConn)
        {
            try
            {
                List<String> iReply = new List<String>();
                Int32 bID = -99, msu = -99, csu = -99, lu = -99, abvU = -99, blwU = -99;
                Decimal mtrSize = -99.00M, clrSize = -99.00M, length = -99.00M, spcAbv = -99.00M, spcBlw = -99.00M;
                String selData = "SELECT [inmcBHAID], [inmcBHAMSize], [inmcBHAMSizeU], [inmcBHADSCSize], [inmcBHADSCSizeU], [inmcBHALength], [inmcBHALengthU], [inmcBHALenAbv], [inmcBHALenAbvU], [inmcBHALenBlw], [inmcBHALenBlwU] FROM [IndNonMagCalcBHA] WHERE [inmcID] = @inmcID";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@inmcID", SqlDbType.Int).Value = CalculatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        bID = readData.GetInt32(0);
                                        iReply.Add(Convert.ToString(bID));
                                        mtrSize = readData.GetDecimal(1);
                                        iReply.Add(Convert.ToString(mtrSize));
                                        msu = readData.GetInt32(2);
                                        iReply.Add(Convert.ToString(msu));
                                        clrSize = readData.GetDecimal(3);
                                        iReply.Add(Convert.ToString(clrSize));
                                        csu = readData.GetInt32(4);
                                        iReply.Add(Convert.ToString(csu));
                                        length = readData.GetDecimal(5);
                                        iReply.Add(Convert.ToString(length));
                                        lu = readData.GetInt32(6);
                                        iReply.Add(Convert.ToString(lu));
                                        spcAbv = readData.GetDecimal(7);
                                        iReply.Add(Convert.ToString(spcAbv));
                                        abvU = readData.GetInt32(8);
                                        iReply.Add(Convert.ToString(abvU));
                                        spcBlw = readData.GetDecimal(9);
                                        iReply.Add(Convert.ToString(spcBlw));
                                        blwU = readData.GetInt32(10);
                                        iReply.Add(Convert.ToString(blwU));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<String> getBHADetail(Int32 bhaID, String dbConn)
        {
            try
            {
                List<String> iReply = new List<String>();
                Int32 mgU = -99, cgU = -99, tpU = -99, bmU = -99;
                Decimal mg = -99.000000M, cg = -99.000000M, topD = -99.00M, btmD = -99.00M;
                String selData = "SELECT [inmcBHAMtrG], [inmcBHAMtrGU], [inmcBHATop], [inmcBHATopU], [inmcBHAClrG], [inmcBHAClrGU], [inmcBHABtm], [inmcBHABtmU] FROM [IndNonMagCalcBHADetail] WHERE [inmcBHAID] = @inmcBHAID";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@inmcBHAID", SqlDbType.Int).Value = bhaID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        mg = readData.GetDecimal(0);
                                        iReply.Add(Convert.ToString(mg));
                                        mgU = readData.GetInt32(1);
                                        iReply.Add(Convert.ToString(mgU));
                                        topD = readData.GetDecimal(2);
                                        iReply.Add(Convert.ToString(topD));
                                        tpU = readData.GetInt32(3);
                                        iReply.Add(Convert.ToString(tpU));
                                        cg = readData.GetDecimal(4);
                                        iReply.Add(Convert.ToString(cg));
                                        cgU = readData.GetInt32(5);
                                        iReply.Add(Convert.ToString(cgU));
                                        btmD = readData.GetDecimal(6);
                                        iReply.Add(Convert.ToString(btmD));
                                        bmU = readData.GetInt32(7);
                                        iReply.Add(Convert.ToString(bmU));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<String> getNMCRefMagList(Int32 NMCalcID, String dbConn)
        {
            try
            {
                List<String> iReply = new List<String>();
                Int32 gcID = -99, bt = -99;
                Decimal gc = -99.00M, dec = -99.00M, dip = -99.00M;
                String selData = "SELECT [inmcRMGC], [inmcRMConvergence], [inmcRMMDec], [inmcRMDip], [inmcRMBT] FROM [IndNonMagCalcRefMag] WHERE [inmcID] = @inmcID";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@inmcID", SqlDbType.Int).Value = NMCalcID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        gcID = readData.GetInt32(0);
                                        iReply.Add(Convert.ToString(gcID));
                                        gc = readData.GetDecimal(1);
                                        iReply.Add(Convert.ToString(gc));
                                        dec = readData.GetDecimal(2);
                                        iReply.Add(Convert.ToString(dec));
                                        dip = readData.GetDecimal(3);
                                        iReply.Add(Convert.ToString(dip));
                                        bt = readData.GetInt32(4);
                                        iReply.Add(Convert.ToString(bt));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getNMCRefMagTable(Int32 NMCalcID, String dbConn)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn inmcRMID = iReply.Columns.Add("inmcRMID", typeof(Int32));
                DataColumn inmcRMGC = iReply.Columns.Add("inmcRMGC", typeof(String));
                DataColumn inmcRMConvergence = iReply.Columns.Add("inmcRMConvergence", typeof(Decimal));
                DataColumn inmcRMMDec = iReply.Columns.Add("inmcRMMDec", typeof(Decimal));
                DataColumn inmcRMDip = iReply.Columns.Add("inmcRMDip", typeof(Decimal));
                DataColumn inmcRMBT = iReply.Columns.Add("inmcRMBT", typeof(Decimal));
                Int32 id = -99, gcID = -99, bt = -99;
                Decimal gc = -99.00M, dec = -99.00M, dip = -99.00M;
                String selData = "SELECT [inmcRMID], [inmcRMGC], [inmcRMConvergence], [inmcRMMDec], [inmcRMDip], [inmcRMBT] FROM [IndNonMagCalcRefMag] WHERE [inmcID] = @inmcID";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@inmcID", SqlDbType.Int).Value = NMCalcID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        gcID = readData.GetInt32(1);
                                        gc = readData.GetDecimal(2);
                                        dec = readData.GetDecimal(3);
                                        dip = readData.GetDecimal(4);
                                        bt = readData.GetInt32(5);
                                        iReply.Rows.Add(id, gc, dec, dip, bt);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }                    
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getNMCBHATable(Int32 NMCalcID, String dbConn)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn inmcBHAID = iReply.Columns.Add("inmcBHAID", typeof(Int32));
                DataColumn inmcBHAMSize = iReply.Columns.Add("inmcBHAMSize", typeof(Decimal));
                DataColumn inmcBHAMSizeU = iReply.Columns.Add("inmcBHAMSizeU", typeof(String));
                DataColumn inmcBHADSCSize = iReply.Columns.Add("inmcBHADSCSize", typeof(Decimal));
                DataColumn inmcBHADSCSizeU = iReply.Columns.Add("inmcBHADSCSizeU", typeof(String));
                DataColumn inmcBHALength = iReply.Columns.Add("inmcBHALength", typeof(Decimal));
                DataColumn inmcBHALengthU = iReply.Columns.Add("inmcBHALengthU", typeof(String));
                DataColumn inmcBHALenAbv = iReply.Columns.Add("inmcBHALenAbv", typeof(Decimal));
                DataColumn inmcBHALenAbvU = iReply.Columns.Add("inmcBHALenAbvU", typeof(String));
                DataColumn inmcBHALenBlw = iReply.Columns.Add("inmcBHALenBlw", typeof(Decimal));
                DataColumn inmcBHALenBlwU = iReply.Columns.Add("inmcBHALenBlwU", typeof(String));
                String selData = "SELECT [inmcBHAID], [inmcBHAMSize], [inmcBHAMSizeU], [inmcBHADSCSize], [inmcBHADSCSizeU], [inmcBHALength], [inmcBHALengthU], [inmcBHALenAbv], [inmcBHALenAbvU], [inmcBHALenBlw], [inmcBHALenBlwU] FROM [IndNonMagCalcBHA] WHERE [inmcID] = @inmcID";
                Int32 id = -99, mtU = -99, csU = -99, lU = -99, aU = -99, bU = -99;
                String mUName = String.Empty, csUName = String.Empty, lUName = String.Empty, aUName = String.Empty, bUName = String.Empty;
                Decimal mtr = -99.00M, dcs = -99.00M, len = -99.00M, abv = -99.00M, blw = -99.00M;
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@inmcID", SqlDbType.Int).Value = NMCalcID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        mtr = readData.GetDecimal(1);
                                        mtU = readData.GetInt32(2);
                                        mUName = AST.getLengthUnitName(mtU);
                                        dcs = readData.GetDecimal(3);
                                        csU = readData.GetInt32(4);
                                        csUName = AST.getLengthUnitName(csU);
                                        len = readData.GetDecimal(5);
                                        lU = readData.GetInt32(6);
                                        lUName = AST.getLengthUnitName(lU);
                                        abv = readData.GetDecimal(7);
                                        aU = readData.GetInt32(8);
                                        aUName = AST.getLengthUnitName(aU);
                                        blw = readData.GetDecimal(9);
                                        bU = readData.GetInt32(10);
                                        bUName = AST.getLengthUnitName(bU);
                                        iReply.Rows.Add(id, mtr, mUName, dcs, csUName, len, lUName, abv, aUName, blw, bUName);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getNMCBHADetailTable(Int32 NMCBHAId, String dbConn)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn inmcBHADetID = iReply.Columns.Add("inmcBHADetID", typeof(Int32));
                DataColumn inmcBHAMtrG = iReply.Columns.Add("inmcBHAMtrG", typeof(Decimal));
                DataColumn inmcBHAMtrGU = iReply.Columns.Add("inmcBHAMtrGU", typeof(String));
                DataColumn inmcBHAClrG = iReply.Columns.Add("inmcBHAClrG", typeof(Decimal));
                DataColumn inmcBHAClrGU = iReply.Columns.Add("inmcBHAClrGU", typeof(String));
                String selData = "SELECT [inmcBHADetID], [inmcBHAMtrG], [inmcBHAMtrGU], [inmcBHAClrG], [inmcBHAClrGU] FROM [IndNonMagCalcBHADetail] WHERE [inmcBHAID] = @NMCBHAId";
                Int32 id = -99, mgU = -99, cgU = -99;
                String mgUName = String.Empty, cgUName = String.Empty;
                Decimal mtr = -99.000000M, clr = -99.000000M;
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@NMCBHAId", SqlDbType.Int).Value = NMCBHAId.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        mtr = readData.GetDecimal(1);
                                        mgU = readData.GetInt32(2);
                                        mgUName = AST.getMagUnitName(mgU);
                                        clr = readData.GetDecimal(3);
                                        cgU = readData.GetInt32(4);
                                        cgUName = AST.getMagUnitName(cgU);
                                        iReply.Rows.Add(id, mtr, mgUName, clr, cgUName);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Decimal> getWETList(Int32 NMCalcID, String dbConn)
        {
            try
            {
                List<Decimal> iReply = new List<Decimal>();
                String selData = "SELECT [wetTInc], [wetAzm], [wetMagAzm], [wetHzComp] FROM [IndNonMagCalcWET] WHERE [inmcID] = @inmcID";
                Decimal inc = -99.00M, azm = -99.00M, mazm = -99.00M, hz = -99.00M;
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@inmcID", SqlDbType.Int).Value = NMCalcID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        inc = readData.GetDecimal(0);
                                        iReply.Add(inc);
                                        azm = readData.GetDecimal(1);
                                        iReply.Add(azm);
                                        mazm = readData.GetDecimal(2);
                                        iReply.Add(mazm);
                                        hz = readData.GetDecimal(3);
                                        iReply.Add(hz);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWETTable(Int32 NMCalcID, String dbConn)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wetID = iReply.Columns.Add("wetID", typeof(Int32));
                DataColumn wetTInc = iReply.Columns.Add("wetTInc", typeof(Decimal));
                DataColumn wetAzm = iReply.Columns.Add("wetAzm", typeof(Decimal));
                String selData = "SELECT [wetID], [wetTInc], [wetAzm] FROM [IndNonMagCalcWET] WHERE [inmcID] = @inmcID";
                Int32 id = -99;
                Decimal inc = -99.00M, azm = -99.00M;
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@inmcID", SqlDbType.Int).Value = NMCalcID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        inc = readData.GetDecimal(1);
                                        azm = readData.GetDecimal(2);
                                        iReply.Rows.Add(id, inc, azm);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertCalcRefMags(Int32 CalcID, Int32 GridConvergence, Decimal GConvergence, Decimal MagDeclination, Decimal Dip, Int32 BTotal, String dbConn)
        {
            try
            {
                Int32 iReply = -99;
                String insertData = "INSERT INTO [IndNonMagCalcRefMag] ([inmcID], [inmcRMGC], [inmcRMConvergence], [inmcRMMDec], [inmcRMDip], [inmcRMBT], [cTime], [uTime]) VALUES (@inmcID, @inmcRMGC, @inmcRMConvergence, @inmcRMMDec, @inmcRMDip, @inmcRMBT, @cTime, @uTime)";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(insertData, dataCon);
                        insData.Parameters.AddWithValue("@inmcID", SqlDbType.Int).Value = CalcID.ToString();
                        insData.Parameters.AddWithValue("@inmcRMGC", SqlDbType.Int).Value = GridConvergence.ToString();
                        insData.Parameters.AddWithValue("@inmcRMConvergence", SqlDbType.Decimal).Value = GConvergence.ToString();
                        insData.Parameters.AddWithValue("@inmcRMMDec", SqlDbType.Decimal).Value = MagDeclination.ToString();
                        insData.Parameters.AddWithValue("@inmcRMDip", SqlDbType.Decimal).Value = Dip.ToString();
                        insData.Parameters.AddWithValue("@inmcRMBT", SqlDbType.Int).Value = BTotal.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());                        
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertCalcWET(Int32 CalcID, Decimal Inclination, Decimal Azimuth, String dbConn)
        {
            try
            {
                Int32 iReply = -99, gcID = -99, bt = -99;
                Decimal gConverg = -99.00M, cMAzm = -99.00M, dec = -99.00M, dip = -99.00M, bhor = -99.00M;
                List<String> refMag = getCalculatorReferenceMagnetics(CalcID, dbConn);
                gcID = Convert.ToInt32(refMag[0]);
                gConverg = Convert.ToDecimal(refMag[1]);
                dec = Convert.ToDecimal(refMag[2]);
                dip = Convert.ToDecimal(refMag[3]);
                bt = Convert.ToInt32(refMag[4]);
                cMAzm = getCMA(gcID, gConverg, dec, Azimuth);
                bhor = getBHOR(dip, bt);
                String insData = "INSERT INTO [IndNonMagCalcWET] ([inmcID], [wetTInc], [wetAzm], [wetMagAzm], [wetHzComp], [cTime], [uTime]) VALUES (@inmcID, @wetTInc, @wetAzm, @wetMagAzm, @wetHzComp, @cTime, @uTime)";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insertData = new SqlCommand(insData, dataCon);
                        insertData.Parameters.AddWithValue("@inmcID", SqlDbType.Int).Value = CalcID.ToString();
                        insertData.Parameters.AddWithValue("@wetTInc", SqlDbType.Decimal).Value = Inclination.ToString();
                        insertData.Parameters.AddWithValue("@wetAzm", SqlDbType.Decimal).Value = Azimuth.ToString();
                        insertData.Parameters.AddWithValue("@wetMagAzm", SqlDbType.Decimal).Value = cMAzm.ToString();
                        insertData.Parameters.AddWithValue("@wetHzComp", SqlDbType.Decimal).Value = bhor.ToString();
                        insertData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insertData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        iReply = Convert.ToInt32(insertData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}