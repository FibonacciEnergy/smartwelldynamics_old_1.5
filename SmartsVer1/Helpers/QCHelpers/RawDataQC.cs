﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Linq;
using System.Collections;
using System.Globalization;
using System.Data.SqlClient;
using System.Web.SessionState;
using System.Web.Configuration;
using System.Collections.Generic;
using MNFit = MathNet.Numerics.Fit;
using MNST = MathNet.Numerics.Statistics.Statistics;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using OPR = SmartsVer1.Helpers.QCHelpers.Opr;
using NMS = SmartsVer1.Helpers.QCHelpers.NonMagCalc;
using ACC = SmartsVer1.Helpers.QCHelpers.AccountingQC;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using WPN = SmartsVer1.Helpers.QCHelpers.WellPlan;
using Units = SmartsVer1.Helpers.DemogHelpers.UnitsConverter;

namespace SmartsVer1.QCHelpers
{
    public class sdRawDataQC
    {                
        static void Main()
        { }
                                                                                      
        public static Decimal getGTF( Decimal Gx, Decimal Gy ) //GTF - φ - ColP
        {
            Double testValue = Units.ToDegrees(Math.Atan2((Double)(Gy) , (Double)(-1*Gx)));
            if (testValue < 0) { return Convert.ToDecimal(testValue + 360); } else { return Convert.ToDecimal(testValue); }
        }

        public static Decimal colQ( Decimal Bx, Decimal By ) //MTF
        {
            Decimal val = Convert.ToDecimal(Units.ToDegrees(Math.Atan((Double)((-1 * By) / Bx))));
            if (Bx > 0) { if (By > 0) { return ( val + 360.00M ); } else { return val; } } else { return (val + 180.00M); }            
        }

        public static Decimal colS( Int32 IFR, Decimal ifrMDec, Decimal MDec ) //Δ Dec
        {
            if (IFR.Equals(2)) { return (ifrMDec - MDec); } else { return 0.25M; }
        }

        public static Decimal colT(Decimal Gx, Decimal Gy, Decimal Gz) //Inc - θ
        {
            return Decimal.Multiply((Decimal)(Math.Atan2(Math.Sqrt((Math.Pow((Double)Gx, 2)) + (Math.Pow((Double)Gy, 2))), (Double)Gz)), Decimal.Divide(180M, (Decimal)Math.PI));
        }

        public static Int32 colV(Decimal colTVal) //Inc - Angle Condition 
        {
            if (colTVal <= 4.99M) { return 1; } else { return 0; }
        }

        public static Decimal colW(Int32 chkShotValue, Int32 srvyID, Decimal colTCurr, Decimal colTPrev) //Inc - Fluctuations
        {
            if (chkShotValue.Equals(1) || srvyID.Equals(1)) { return 0.0000M; } else if (srvyID.Equals(2)) { return Decimal.Divide(colTCurr, 2); } else { return Math.Abs(Decimal.Subtract(colTCurr, colTPrev)); }            
        }

        public static Decimal colY(Decimal GTF, Decimal Inclination, Decimal Bx, Decimal By, Decimal Bz) //Mag Azm - Ψm
        {
            Double testValue = (((Math.Atan2((-1*(((Double)Bx)*Math.Sin((Double)GTF*3.1415926/180)+((Double)By)*Math.Cos((Double)GTF*3.1415926/180))),((((Double)Bx)*Math.Cos((Double)GTF * 3.1415926/180) - ((Double)By)*Math.Sin((Double)GTF*3.1415926/180) )*Math.Cos((Double)Inclination*3.1415926/180)+((Double)Bz)*Math.Sin((Double)Inclination*3.1415926/180) ))*180/3.1415926+360) % 360));
            if(testValue > 360) { return Convert.ToDecimal(testValue - 360); } else { return Convert.ToDecimal(testValue); }
        }

        public static Decimal colZ(Decimal colMagneticAzimuth, Decimal MagneticDeclination, Decimal GridConvergence, Int32 LatiNS, Int32 MDecEW, Int32 gcEW) //Azm - Ψ - Plain
        {            
                //North = 1, East = 9, South = 17, West = 25;
                Decimal testValue = -99.0000M;
                if (LatiNS.Equals(1) && MDecEW.Equals(9) && gcEW.Equals(1))
                {
                    testValue = colMagneticAzimuth + MagneticDeclination - GridConvergence;
                }
                else if (LatiNS.Equals(1) && MDecEW.Equals(9) && gcEW.Equals(2))
                {
                    testValue = colMagneticAzimuth + MagneticDeclination + GridConvergence;
                }
                else if (LatiNS.Equals(1) && MDecEW.Equals(25) && gcEW.Equals(1))
                {
                    testValue = colMagneticAzimuth - MagneticDeclination - GridConvergence;
                }
                else if (LatiNS.Equals(1) && MDecEW.Equals(25) && gcEW.Equals(2))
                {
                    testValue = colMagneticAzimuth - MagneticDeclination + GridConvergence;
                }
                else if (LatiNS.Equals(17) && MDecEW.Equals(9) && gcEW.Equals(1))
                {
                    testValue = colMagneticAzimuth + MagneticDeclination + GridConvergence;
                }
                else if (LatiNS.Equals(17) && MDecEW.Equals(9) && gcEW.Equals(2))
                {
                    testValue = colMagneticAzimuth + MagneticDeclination - GridConvergence;
                }
                else if (LatiNS.Equals(17) && MDecEW.Equals(25) && gcEW.Equals(1))
                {
                    testValue = colMagneticAzimuth - MagneticDeclination + GridConvergence;
                }
                else if (LatiNS.Equals(17) && MDecEW.Equals(25) && gcEW.Equals(2))
                {
                    testValue = colMagneticAzimuth - MagneticDeclination - GridConvergence;
                }
                else
                {
                    testValue = colMagneticAzimuth;
                }
                //calculating value
                if (testValue > 359.99M)
                {
                    return testValue - 360.00M;
                }
                else if (testValue < 0.00M)
                {
                    return testValue + 360.00M;
                }
                else
                {
                    return testValue;
                }            
        }

        public static Decimal colAA(Decimal colMagneticAzimuth, Decimal iMagneticDeclination, Decimal GridConvergence, Int32 LatitudeNS, Int32 iMDecEW, Int32 GConvgEW) //Azm - Ψ - IFR
        {            
                //North = 1, East = 9, South = 17, West = 25;
                Decimal testValue = -99.0000M;
                if (LatitudeNS.Equals(1) && iMDecEW.Equals(9) && GConvgEW.Equals(1))
                {
                    testValue = colMagneticAzimuth + iMagneticDeclination - GridConvergence;
                }
                else if (LatitudeNS.Equals(1) && iMDecEW.Equals(9) && GConvgEW.Equals(2))
                {
                    testValue = colMagneticAzimuth + iMagneticDeclination + GridConvergence;
                }
                else if (LatitudeNS.Equals(1) && iMDecEW.Equals(25) && GConvgEW.Equals(1))
                {
                    testValue = colMagneticAzimuth - iMagneticDeclination - GridConvergence;
                }
                else if (LatitudeNS.Equals(1) && iMDecEW.Equals(25) && GConvgEW.Equals(2))
                {
                    testValue = colMagneticAzimuth - iMagneticDeclination + GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && iMDecEW.Equals(9) && GConvgEW.Equals(1))
                {
                    testValue = colMagneticAzimuth + iMagneticDeclination + GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && iMDecEW.Equals(9) && GConvgEW.Equals(2))
                {
                    testValue = colMagneticAzimuth + iMagneticDeclination - GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && iMDecEW.Equals(25) && GConvgEW.Equals(1))
                {
                    testValue = colMagneticAzimuth - iMagneticDeclination + GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && iMDecEW.Equals(25) && GConvgEW.Equals(2))
                {
                    testValue = colMagneticAzimuth - iMagneticDeclination - GridConvergence;
                }
                else
                {
                    testValue = colMagneticAzimuth;
                }
                //calculating value
                if (testValue > 359.99M){ return testValue - 360.00M; } else if (testValue < 0.00M) { return testValue + 360.00M; } else { return testValue; }            
        }

        public static Decimal colAB( Decimal MagneticAzimuth, Decimal MagneticDeclination, Int32 MDecCID ) //Azm - Ψ - Plain
        {            
                //North = 1, East = 9, South = 17, West = 25;
                Decimal testValue = -99.0000M;
                if (MDecCID.Equals(9)) { testValue = MagneticAzimuth + MagneticDeclination; } else { testValue = MagneticAzimuth - MagneticDeclination; }
                if (testValue > 359.99M){ return testValue - 360.00M; } else if (testValue < 0.00M) { return testValue + 360.00M; } else { return testValue; }            

            
        }

        public static Decimal colAC(Decimal MagneticAzimuth, Decimal iMagneticDeclination, Int32 iMDecCID) //Azm - Ψ - IFR
        {            
                //North = 1, East = 9, South = 17, West = 25;
                Decimal testValue = -99.0000M;
                if (iMDecCID.Equals(9)) { testValue = MagneticAzimuth + iMagneticDeclination; } else { testValue = MagneticAzimuth - iMagneticDeclination; }
                if (testValue > 359.99M) { return testValue - 360.00M; } else if (testValue < 0.00M) { return testValue + 360.00M; } else { return testValue; }
        }

        public static Decimal colAD( Int32 ifrVal , Decimal colZ , Decimal colAA ) //Grid - LC Azm - Ψ - Final
        {
            if (ifrVal.Equals(1)) { return colZ; } else { return colAA; }
        }

        public static Decimal colAE( Int32 ifrVal , Decimal colAB , Decimal colAC ) //True - LC Azm - Ψ - Final
        {
            if (ifrVal.Equals(1)) { return colAB; } else { return colAC; }
        }

        public static Decimal colAF( Int32 jobConvVal , Decimal colAD , Decimal colAE ) //LC Azm - Ψ - Final
        {
            if (jobConvVal.Equals(2)) { return colAD; } else { return colAE; }
        }        
        
        public static Decimal colAH(Decimal gtfVal, Decimal colT, Decimal colBQ, Decimal colBR, Decimal colCR) //Mag Azm - Ψm'
        {
            if (((Math.Atan2((Double)(-1 * ((colBQ) * (Decimal)Math.Sin((Double)gtfVal * 3.1415926 / 180) + (colBR) * (Decimal)Math.Cos((Double)gtfVal * 3.1415926 / 180))), (Double)((((colBQ) * (Decimal)Math.Cos((Double)gtfVal * 3.1415926 / 180) - (colBR) * (Decimal)Math.Sin((Double)gtfVal * 3.1415926 / 180)) * (Decimal)Math.Cos((Double)colT * 3.1415926 / 180) + (colCR) * (Decimal)Math.Sin((Double)colT * 3.1415926 / 180)))) * (180 / 3.1415926) + 360) % 360) > 360)
            {
                return (Decimal)(((Math.Atan2((Double)(-1 * ((colBQ) * (Decimal)Math.Sin((Double)gtfVal * 3.1415926 / 180) + (colBR) * (Decimal)Math.Cos((Double)gtfVal * 3.1415926 / 180))), (Double)((((colBQ) * (Decimal)Math.Cos((Double)gtfVal * 3.1415926 / 180) - (colBR) * (Decimal)Math.Sin((Double)gtfVal * 3.1415926 / 180)) * (Decimal)Math.Cos((Double)colT * 3.1415926 / 180) + (colCR) * (Decimal)Math.Sin((Double)colT * 3.1415926 / 180)))) * (180 / 3.1415926) + 360) % 360) - 360);
            }
            else
            {
                return (Decimal)((Math.Atan2((Double)(-1 * (((colBQ) * (Decimal)Math.Sin((Double)gtfVal * 3.1415926 / 180) + (colBR) * (Decimal)Math.Cos((Double)gtfVal * 3.1415926 / 180)))), (Double)((((colBQ) * (Decimal)Math.Cos((Double)gtfVal * 3.1415926 / 180) - (colBR) * (Decimal)Math.Sin((Double)gtfVal * 3.1415926 / 180)) * (Decimal)Math.Cos((Double)colT * 3.1415926 / 180) + (colCR) * (Decimal)Math.Sin((Double)colT * 3.1415926 / 180)))) * (180 / 3.1415926) + 360) % 360);
            }            
        }
        
        public static Decimal colAI(Decimal colAHVal, Decimal MagneticDeclination, Decimal GridConvergence, Int32 LatitudeNS, Int32 MDecCID, Int32 gcoID) //Azm - Ψ - Plain
        {
            //North = 1, East = 9, South = 17, West = 25;
                Decimal testValue = -99.0000M;
                if (LatitudeNS.Equals(1) && MDecCID.Equals(9) && gcoID.Equals(1))
                {
                    testValue = colAHVal + MagneticDeclination - GridConvergence;
                }
                else if (LatitudeNS.Equals(1) && MDecCID.Equals(9) && gcoID.Equals(2))
                {
                    testValue = colAHVal + MagneticDeclination + GridConvergence;
                }
                else if (LatitudeNS.Equals(1) && MDecCID.Equals(25) && gcoID.Equals(1))
                {
                    testValue = colAHVal - MagneticDeclination - GridConvergence;
                }
                else if (LatitudeNS.Equals(1) && MDecCID.Equals(25) && gcoID.Equals(2))
                {
                    testValue = colAHVal - MagneticDeclination + GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && MDecCID.Equals(9) && gcoID.Equals(1))
                {
                    testValue = colAHVal + MagneticDeclination + GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && MDecCID.Equals(9) && gcoID.Equals(2))
                {
                    testValue = colAHVal + MagneticDeclination - GridConvergence;                    
                }
                else if(LatitudeNS.Equals(17) && MDecCID.Equals(25) && gcoID.Equals(1))
                {
                    testValue = colAHVal - MagneticDeclination + GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && MDecCID.Equals(25) && gcoID.Equals(2))
                {
                    testValue = colAHVal - MagneticDeclination - GridConvergence;
                }
                else
                {
                    testValue = colAHVal;
                }

                if ( testValue > 359.99M )
                {
                    return testValue - 360.00M;
                }
                else if (testValue < 0.00M)
                {
                    return testValue + 360.00M;
                }
                else
                {
                    return testValue;
                }            
        }

        public static Decimal colAJ(Decimal colAHVal, Decimal iMagneticDeclination, Decimal GridConvergence, Int32 LatitudeNS, Int32 iMDecCID, Int32 gcoID) //Azm - Ψ - Plain
        {           
                //North = 1, East = 9, South = 17, West = 25;
                Decimal testValue = -99.0000M;
                if (LatitudeNS.Equals(1) && iMDecCID.Equals(9) && gcoID.Equals(1))
                {
                    testValue = colAHVal + iMagneticDeclination - GridConvergence;
                }
                else if (LatitudeNS.Equals(1) && iMDecCID.Equals(9) && gcoID.Equals(2))
                {
                    testValue = colAHVal + iMagneticDeclination + GridConvergence;
                }
                else if (LatitudeNS.Equals(1) && iMDecCID.Equals(25) && gcoID.Equals(1))
                {
                    testValue = colAHVal - iMagneticDeclination - GridConvergence;
                }
                else if (LatitudeNS.Equals(1) && iMDecCID.Equals(25) && gcoID.Equals(2))
                {
                    testValue = colAHVal - iMagneticDeclination + GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && iMDecCID.Equals(9) && gcoID.Equals(1))
                {
                    testValue = colAHVal + iMagneticDeclination + GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && iMDecCID.Equals(9) && gcoID.Equals(2))
                {
                    testValue = colAHVal + iMagneticDeclination - GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && iMDecCID.Equals(25) && gcoID.Equals(1))
                {
                    testValue = colAHVal - iMagneticDeclination + GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && iMDecCID.Equals(25) && gcoID.Equals(2))
                {
                    testValue = colAHVal - iMagneticDeclination - GridConvergence;
                }
                else
                {
                    testValue = colAHVal;
                }

                if (testValue > 359.99M) { return testValue - 360.00M; } else if (testValue < 0.00M) { return testValue + 360.00M; } else { return testValue; }            
        }

        public static Decimal colAK(Decimal colAHVal, Decimal MagneticDeclination, Int32 MDecCID) //Azm - Ψ - Plain
        {            
                //North = 1, East = 9, South = 17, West = 25;
                Decimal testValue = -99.0000M;
                if(MDecCID.Equals(9)) { testValue = colAHVal + MagneticDeclination; } else { testValue = colAHVal - MagneticDeclination; }
                if(testValue > 359.99M) { return testValue - 360.00M; } else if(testValue < 0.00M) { return testValue + 360.00M; } else { return testValue; }            
        }
        
        public static Decimal colAL(Decimal colAHVal, Decimal iMagneticDeclination, Int32 iMDecCID) //Azm - Ψ - IFR
        {            
                //North = 1, East = 9, South = 17, West = 25;
                Decimal testValue = -99.0000M;
                if (iMDecCID.Equals(9))
                {
                    testValue = colAHVal + iMagneticDeclination;
                }
                else
                {
                    testValue = colAHVal - iMagneticDeclination;
                }

                if (testValue > 359.99M)
                {
                    return testValue - 360.00M;
                }
                else if (testValue < 0.00M)
                {
                    return testValue + 360.00M;
                }
                else
                {
                    return testValue;
                }
        }
        
        public static Decimal colAM(Int32 ifrVal, Decimal colAI, Decimal colAJ) //Grid - LC Azm - Ψ - Final
        {            
            if (ifrVal.Equals(1)) { return colAI; } else { return colAJ; }
        }

        public static Decimal colAN(Int32 ifrVal, Decimal colAK, Decimal colAL) //True - LC Azm - Ψ - Final
        {
            if (ifrVal.Equals(1)) { return colAK; } else { return colAL; }
        }

        public static Decimal colAO(Int32 JobConverg, Decimal colAM, Decimal colAN) //Corrected Azm - Ψ' - Final
        {
            if (JobConverg.Equals(2)) { return colAM; } else { return colAN; } 
        }

        public static Decimal colAP( Decimal colY , Decimal colAH ) //Δ Azm Corrected
        {
            return Decimal.Subtract(colY, colAH);            
        }

        public static Decimal colAR( Int32 SrvyID , Decimal colYCurr , Decimal colYPrev ) //Azm - Fluctuations
        {
            if( SrvyID.Equals( 1 ) ) { return 0.00M; } else { return Convert.ToDecimal(Math.Abs( Decimal.Subtract( colYCurr , colYPrev ) ) ); }
        }

        public static Decimal colAS( Decimal colW , Decimal colAR ) //Inc-Azm Dir Resultant Vector
        {
            return Convert.ToDecimal(Math.Sqrt((((Math.Pow((Double)colW, 2)) + (Math.Pow((Double)colAR, 2))))));            
        }

        public static Decimal colAT( Int32 srvID , Int32 colV , Decimal colW , Decimal colAS , Decimal currDepth , Decimal prevDepth ) //DLS/Unit
        {            
                if (colV.Equals(1)) 
                {
                    if (srvID.Equals(1))
                    {
                        return colW;
                    }
                    else if (Decimal.Subtract(currDepth, prevDepth).Equals(0))
                    {
                        return 0.00M;
                    }
                    else
                    {
                        return Decimal.Divide(colW, Decimal.Subtract(currDepth, prevDepth));
                    }
                }
                else
                {
                    if (srvID == 1)
                    {
                        return colAS;
                    }
                    else if (Decimal.Subtract(currDepth, prevDepth).Equals(0))
                    {
                        return 0.00M;
                    }
                    else
                    {
                        return Decimal.Divide(colAS, Decimal.Subtract(currDepth, prevDepth));
                    }
                }
        }

        public static Decimal colAU( Int32 srvID , Int32 depU , Int32 colV , Decimal colAT , Decimal ref1 , Decimal ref2 , Decimal ref3 , Decimal ref4 ) //Dir Cond Ref
        {            
                if (srvID.Equals(1))
                {
                    return 0.00M;
                }
                else
                {
                    if (colV.Equals(1) && depU.Equals(5))
                    {
                        return Decimal.Subtract(colAT, ref3);
                    }
                    else if (colV.Equals(1) && depU.Equals(4))
                    {
                        return Decimal.Subtract(colAT, ref4);
                    }
                    else if (colV.Equals(0) && depU.Equals(5))
                    {
                        return Decimal.Subtract(colAT, ref1);
                    }
                    else
                    {
                        return Decimal.Subtract(colAT, ref2);
                    }
                }
        }

        public static Int32 colAV( Decimal colAU ) //Dir Cond
        {
            if( colAU > 0.01M ) { return 0; } else { return 1; }
        }

        public static Int32 colAW( Decimal colT ) //SC - Condt 1
        {
            if( colT > 70 || colT.Equals(70) ) { return 0; } else { return 1; }
        }

        public static Int32 colAX( Decimal colY ) //SC - Condt 2
        {            
                if( colY.Equals(0) || colY <= 69.99M )
                {
                    return 1;
                }
                else if( colY.Equals(70.00M) || colY <= 110.00M )
                {
                    return 0;
                }
                else if( colY.Equals(110.01M) || colY <= 249.99M )
                {
                    return 1;
                }
                else if( colY.Equals(250.00M) || colY <= 290.00M )
                {
                    return 0;
                }
                else if( colY.Equals(290.01M) || colY <= 359.99M )
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
        }

        public static Int32 colAY( Int32 colAW , Int32 colAX ) //Short Collar Application Condition
        {
            if( colAW.Equals(1) ){ return 1; } else if( colAX.Equals(1) ) { return 1; } else if( colAX.Equals(1) && colAW.Equals(1) ) { return 1; } else { return 0; }            
        }

        public static Decimal colBA( Decimal Gx, Decimal Gy, Decimal Gz, Decimal Bx, Decimal By, Decimal Bz, Decimal colCF, Decimal colCA ) //LC - Dip
        {
            return Convert.ToDecimal(Units.ToDegrees( Math.Asin((((Double)Gx * (Double)Bx) + ((Double)Gy * (Double)By) + ((Double)Gz * (Double)Bz)) / ((Double)colCF * (Double)colCA))));                                            
        }

        public static Decimal colBB( Int32 srvID , Decimal colBACurr , Decimal colBAPrev ) //∆Dip LC - Fluctuations
        {            
                if( srvID.Equals(1) ) { return 0.00M; } else if( srvID.Equals(2) ) { return Decimal.Divide( Decimal.Subtract( colBACurr, colBAPrev ) , 2 ); } else { return Decimal.Subtract( colBACurr , colBAPrev ); }
        }

        public static Decimal colBC( Int32 ifrVal , Decimal ifrDip , Decimal cnvDip ,Decimal colBA ) //∆Dip LC
        {
            if (ifrVal.Equals(2)) { return Decimal.Subtract(colBA, ifrDip); } else { return Decimal.Subtract(colBA, cnvDip); }                            
        }

        public static Int32 colBD(Int32 srvID, Decimal dflLowerDip, Decimal dflUpperDip, Decimal colBC, Decimal colBCPrev, String dbConn) //∆Dip LC Condition for Trend Analysis
        {                         
            if( srvID.Equals(1) )
            {
                if( colBC < ( -1 * dflLowerDip ) )
                {
                    return 3;
                }
                else if( colBC > dflLowerDip )
                {
                    return 2;
                }
                else
                {
                    return 1;
                }
            }
            else if (((colBC - colBCPrev) > dflUpperDip) || ((colBC - colBCPrev) < (-1 * dflUpperDip)))
            {
                return 4;
            }
            else if( (colBC) < ( -1 * dflLowerDip ) )
            {
                return 3;
            }
            else if( colBC > dflLowerDip )
            {
                return 2;
            }
            else
            {
                return 1;
            }            
        }

        public static Decimal colBF(Int32 ifrVal, Decimal gtfVal, Decimal colT, Decimal cnvRMGT, Decimal cnvIFRGT) // Gx'
        {
            if(	ifrVal.Equals(1) )
            {
                return (-1 * cnvRMGT * (Decimal)Math.Sin( (Double)Units.ToRadians((Double)colT) ) * (Decimal)Math.Cos( (Double)Units.ToRadians((Double)gtfVal)) );
            }
            else
            {
                return (-1 * cnvIFRGT * (Decimal)Math.Sin( (Double)Units.ToRadians((Double)colT) ) * (Decimal)Math.Cos( (Double)Units.ToRadians((Double)gtfVal)));
            }            
        }
        
        public static Decimal colBG(Int32 ifrVal, Decimal gtfVal, Decimal colT, Decimal cnvRMGT, Decimal cnvIFRGT) //GY'
        {
            if(ifrVal.Equals(1))
            {
                return ( cnvRMGT * (Decimal)Math.Sin( (Double)Units.ToRadians((Double)colT ) ) * (Decimal)Math.Sin((Double)Units.ToRadians( (Double)gtfVal ) ) );
            }
            else
            {
                return ( cnvIFRGT * (Decimal)Math.Sin( (Double)Units.ToRadians((Double)colT ) ) * (Decimal)Math.Sin( (Double)Units.ToRadians( (Double)gtfVal ) ) );
            }
        }
        
        public static Decimal colBH(Int32 ifrVal, Decimal colT, Decimal cnvGT, Decimal cnvIFRGT) //Gz'
        {
            if( ifrVal.Equals(1) )
            {
                return ( cnvGT * (Decimal)Math.Cos( (Double)Units.ToRadians( (Double) colT ) ) );
            }
            else
            {
                return ( cnvIFRGT * (Decimal)Math.Cos( (Double)Units.ToRadians( (Double)colT ) ) );
            }
        }
        
        public static Decimal colBI( Int32 ifrVal , Decimal convBTotal , Decimal convIBTotal , Decimal convDip , Decimal convIDip , Decimal MagAzm, Decimal GTF , Decimal Inclination) //Bx'-ref
        {
            if(ifrVal.Equals(1))
            {
                return Convert.ToDecimal( (Double)convBTotal*((Math.Cos(Units.ToRadians((Double)convDip))*Math.Cos(Units.ToRadians((Double)MagAzm))*Math.Cos(Units.ToRadians((Double)Inclination))*Math.Cos(Units.ToRadians((Double)GTF)))-(Math.Sin(Units.ToRadians((Double)convDip))*Math.Sin(Units.ToRadians((Double)Inclination))*Math.Cos(Units.ToRadians((Double)GTF)))-(Math.Cos(Units.ToRadians((Double)convDip))*Math.Sin(Units.ToRadians((Double)MagAzm))*Math.Sin(Units.ToRadians((Double)GTF)))));
            }
            else
            {
                //return Convert.ToDecimal( (Double)convIBTotal*((Math.Cos(Units.ToRadians((Double)convIDip))*Math.Cos(Units.ToRadians((Double)MagAzm))*Math.Cos(Units.ToRadians((Double)Inclination))*Math.Cos(Units.ToRadians((Double)GTF)))-(Math.Sin(Units.ToRadians((Double)convIDip))*Math.Sin(Units.ToRadians((Double)Inclination))*Math.Cos(Units.ToRadians((Double)GTF)))-(Math.Cos(Units.ToRadians((Double)convIDip))*Math.Sin(Units.ToRadians((Double)MagAzm))*Math.Sin(Units.ToRadians((Double)GTF)))));
                return Convert.ToDecimal( (Double)convIBTotal * ( ( Math.Cos(Units.ToRadians((Double)convIDip ) ) * Math.Cos(Units.ToRadians( (Double)MagAzm ) ) * Math.Cos(Units.ToRadians( (Double)Inclination ) ) * Math.Cos(Units.ToRadians( (Double)GTF ) ) ) - ( Math.Sin( Units.ToRadians( (Double)convIDip ) ) * Math.Sin( Units.ToRadians( (Double)Inclination ) ) * Math.Cos( Units.ToRadians( (Double)GTF ) ) ) - ( Math.Cos( Units.ToRadians( (Double)convIDip ) ) * Math.Sin( Units.ToRadians( (Double)MagAzm ) ) * Math.Sin( Units.ToRadians( (Double)GTF ) ) ) ) );
            }
        }

        public static Decimal colBJ( Int32 ifrVal , Decimal convDip , Decimal convBTotal , Decimal convIDip , Decimal convIBTotal, Decimal GTF , Decimal Inclination , Decimal MagAzm ) //By'-ref
        {
            if(ifrVal.Equals(1))
            {
                return Convert.ToDecimal( -1*(Double)convBTotal*((Math.Cos(Units.ToRadians((Double)convDip))*Math.Cos(Units.ToRadians((Double)MagAzm))*Math.Cos(Units.ToRadians((Double)Inclination))*Math.Sin(Units.ToRadians((Double)GTF)))+(Math.Cos(Units.ToRadians((Double)convDip))*Math.Sin(Units.ToRadians((Double)MagAzm))*Math.Cos(Units.ToRadians((Double)GTF)))-(Math.Sin(Units.ToRadians((Double)convDip))*Math.Sin(Units.ToRadians((Double)Inclination))*Math.Sin(Units.ToRadians((Double)GTF)))));
            }
            else
            {
                return Convert.ToDecimal( -1*(Double)convIBTotal*((Math.Cos(Units.ToRadians((Double)convIDip))*Math.Cos(Units.ToRadians((Double)MagAzm))*Math.Cos(Units.ToRadians((Double)Inclination))*Math.Sin(Units.ToRadians((Double)GTF)))+(Math.Cos(Units.ToRadians((Double)convIDip))*Math.Sin(Units.ToRadians((Double)MagAzm))*Math.Cos(Units.ToRadians((Double)GTF)))-(Math.Sin(Units.ToRadians((Double)convIDip))*Math.Sin(Units.ToRadians((Double)Inclination))*Math.Sin(Units.ToRadians((Double)GTF)))));
            }
        }

        public static Decimal colBK( Int32 ifrVal , Decimal convDip , Decimal convBTotal , Decimal convIDip , Decimal convIBTotal , Decimal Inclination , Decimal MagAzm ) //Bz'-ref
        {
            if(ifrVal.Equals(1))
            {
                return Convert.ToDecimal((Double)convBTotal*((Math.Sin((Double)convDip*3.1415926/180)*Math.Cos((Double)Inclination*3.1415926/180))+(Math.Cos((Double)convDip*3.1415926/180)*Math.Cos((Double)MagAzm*3.1415926/180)*Math.Sin((Double)Inclination*3.1415926/180))));
            }
            else
            {
                return Convert.ToDecimal((Double)convIBTotal*((Math.Sin((Double)convIDip*3.1415926/180)*Math.Cos((Double)Inclination*3.1415926/180))+(Math.Cos((Double)convIDip*3.1415926/180)*Math.Cos((Double)MagAzm*3.1415926/180)*Math.Sin((Double)Inclination*3.1415926/180))));
            }
        }

        //public static Decimal colBM(Int32 ServiceID, Int32 srvyID, Int32 RunID, Int32 WellSectionID, Int32 WellID, Decimal Bx, List<Double> bxValues, Decimal colBIVal, List<Double> biValues, String dbConn) //Bx_Bias
        //{
        //    Double[] x = new Double[] { }, bi = new Double[] { };
        //    x = bxValues.ToArray();

        //    if(srvyID.Equals(1) || srvyID.Equals(2) || srvyID.Equals(3) || MNST.PopulationVariance(x).Equals(0))
        //    {
        //        return colBIVal - Bx;
        //    }
        //    else
        //    {
        //        bi = biValues.ToArray();
        //        Tuple<Double, Double> p = MNFit.Line(x, bi);
        //        return Convert.ToDecimal(p.Item1);
        //    }
        //}

        public static Decimal colBM(Int32 SurveyRowID, Decimal Bx, Decimal By, Decimal Bz, Decimal BxRef, Decimal ByRef, Decimal ShortCollarBzRef, Decimal ThreeSigmaB, Decimal hqValue, List<Double> BxRefList, List<Double> ByRefList, List<Double> hqList)
        {
            Int32 testValue = -99;
            Double[] x = new Double[] { }, hq = new Double[] { };
            if( (Bx - BxRef > ThreeSigmaB) || (By - ByRef > ThreeSigmaB) || ( Bz - ShortCollarBzRef > ThreeSigmaB ) || ( Bx - BxRef < ( -1 * ThreeSigmaB ) ) || ( By - ByRef < (-1 * ThreeSigmaB) ) || ( Bz - ShortCollarBzRef < (-1 * ThreeSigmaB) ) )
            {
                testValue = 0;
            }
            else
            {
                testValue = 1;
            }

            if (SurveyRowID.Equals(1))
            {
                return hqValue;
            }
            else if (MNST.PopulationVariance(ByRefList).Equals(0))
            {
                if (testValue.Equals(0))
                {
                    return ByRef - By;
                }
                else if (testValue.Equals(1))
                {
                    return 0 - By;
                }
                else
                {
                    x = BxRefList.ToArray(); 
                    hq = hqList.ToArray();
                    Tuple<Double, Double> p = MNFit.Line(x, hq);
                    return Convert.ToDecimal(p.Item1);
                }
            }
            else
            {
                return 0;
            }
            
        }

        //public static Decimal colBN(Int32 ServiceID, Int32 srvyID, Int32 RunID, Int32 WellSectionID, Int32 WellID, Decimal By, List<Double> byValues, Decimal colBJVal, List<Double> bjValues, String dbConn) //By_Bias
        //{            
        //    Double[] y = new Double[] { }, bj = new Double[] { };
        //    y = byValues.ToArray();
        //    if (srvyID.Equals(1) || srvyID.Equals(2) || srvyID.Equals(3) || MNST.PopulationVariance(y).Equals(0))
        //    {
        //        return colBJVal - By;
        //    }
        //    else
        //    {
        //        bj = bjValues.ToArray();
        //        Tuple<Double, Double> p = MNFit.Line(y, bj);
        //        return Convert.ToDecimal(p.Item1);
        //    }
        //}

        public static Decimal colBN(Int32 SurveyRowID, Decimal Bx, Decimal By, Decimal Bz, Decimal BxRef, Decimal ByRef, Decimal ShortCollarBzRef, Decimal ThreeSigmaB, Decimal hrValue, List<Double> BxRefList, List<Double> ByRefList, List<Double> hrList)
        {
            Int32 testValue = -99;
            Double[] x = new Double[] { }, hr = new Double[] { };
            if ((Bx - BxRef > ThreeSigmaB) || (By - ByRef > ThreeSigmaB) || (Bz - ShortCollarBzRef > ThreeSigmaB) || (Bx - BxRef < (-1 * ThreeSigmaB)) || (By - ByRef < (-1 * ThreeSigmaB)) || (Bz - ShortCollarBzRef < (-1 * ThreeSigmaB)))
            {
                testValue = 0;
            }
            else
            {
                testValue = 1;
            }

            if (SurveyRowID.Equals(1))
            {
                return hrValue;
            }
            else if (MNST.PopulationVariance(ByRefList).Equals(0))
            {
                if (testValue.Equals(0))
                {
                    return ByRef - By;
                }
                else if (testValue.Equals(1))
                {
                    return 0 - By;
                }
                else
                {
                    x = BxRefList.ToArray();
                    hr = hrList.ToArray();
                    Tuple<Double, Double> p = MNFit.Line(x, hr);
                    return Convert.ToDecimal(p.Item1);
                }
            }
            else
            {
                return 0;
            }

        }

        public static Decimal bzBias(Int32 ServiceID, Int32 srvyID, Int32 RunID, Int32 WellSectionID, Int32 WellID, Decimal Bx, Decimal By, Decimal Bz, List<Double> bzList, Decimal colBIVal, Decimal colBJVal, Decimal colCNVal, Decimal colHQVal, Decimal TSBias, String dbConn) //bzBias
        {                    
                Boolean testValue;
                testValue = (Bx - colBIVal > TSBias || By - colBJVal > TSBias || Bz - colCNVal > TSBias || Bx - colBIVal < (-1 * TSBias) || By - colBJVal < (-1 * TSBias) || Bz - colCNVal < (-1 * TSBias));
                List<Double> BzBiasCList = new List<Double>();
                Double[] bz = new Double[] { }, bzBiasConditional = new Double[] { };
                bz = bzList.ToArray();
                if (ServiceID.Equals(46))
                {
                    BzBiasCList = getBzBiasConditionalList(ServiceID, WellID, WellSectionID, RunID, dbConn);
                }
                else
                {
                    BzBiasCList = getIDCorrBzBiasConditionalList(ServiceID, WellID, WellSectionID, RunID, dbConn);
                }
                BzBiasCList.Add((Double)colHQVal);
                bzBiasConditional = BzBiasCList.ToArray();

                if (srvyID.Equals(1))
                {
                    return colHQVal;
                }
                else if (MNST.PopulationVariance(bz).Equals(0))
                {
                    if (testValue.Equals(0))
                    {
                        return colBJVal - By;
                    }
                    else if (testValue.Equals(1))
                    {
                        return 0.00M - By;
                    }
                    else
                    {
                        Tuple<Double, Double> p = MNFit.Line(bz, bzBiasConditional);
                        return Convert.ToDecimal(p.Item1);
                    }
                }
                else
                {
                    return 0.00M;
                }
        }        
        
        public static Decimal colBO(Int32 ServiceID, Int32 srvyID , Int32 WellID, Int32 WellSectionID, Int32 RunID, Decimal currBx, List<Double> bxValues, Decimal colBICurr, List<Double> biValues, String dbConn) //Bx_SF
        {
            if ( srvyID.Equals(1) || srvyID.Equals(2) || srvyID.Equals(3) )
            {
                return 1.00M;
            }
            else
            {
                Double[] x = new Double[] { },  bi = new Double[] { };
                x = bxValues.ToArray();
                bi = biValues.ToArray();

                if (MNST.PopulationVariance(x).Equals(0))
                {
                    return 1.00M;
                }
                else
                {
                    Tuple<Double, Double> p = MNFit.Line(x, bi);
                    return Convert.ToDecimal(p.Item2);
                }                    
            }
        }

        public static Decimal colBP(Int32 ServiceID, Int32 srvyID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Decimal currBy, List<Double> byValues, Decimal colBJCurr, List<Double> bjValues, String dbConn) //By_SF
        {            
            if (srvyID.Equals(1) || srvyID.Equals(2) || srvyID.Equals(3) )
            {
                return 1.00M;
            }
            else
            {
                Double[] y = new Double[] { }, bj = new Double[] { };
                y = byValues.ToArray();
                bj = bjValues.ToArray();

                if (MNST.PopulationVariance(y).Equals(0))
                {
                    return 1.00M;
                }
                else
                {
                    Tuple<Double, Double> p = MNFit.Line(y, bj);
                    return Convert.ToDecimal(p.Item2);
                } 
            }
        }

        public static Decimal bzSF(Int32 ServiceID, Int32 srvyID , Int32 WellID, Int32 WellSectionID, Int32 RunID, Decimal currBz, List<Double> bzList, Decimal colCNCurr, List<Double> colCNValues, String dbConn) //Bz_Scale Factor
        {            
                if ( srvyID.Equals(1) || srvyID.Equals(2) || srvyID.Equals(3) )
                {
                    return 1.00M;
                }
                else
                {
                    Double[] x = new Double[] { },  cn = new Double[] { };
                    x = bzList.ToArray();
                    cn = colCNValues.ToArray();

                    if (MNST.PopulationVariance(x).Equals(0))
                    {
                        return 1.00M;
                    }
                    else
                    {
                        Tuple<Double, Double> p = MNFit.Line(x, cn);
                        return Convert.ToDecimal(p.Item2);
                    }                    
                }
        }

        public static Decimal colBQ( Decimal Bx , Decimal colBO , Decimal colBM ) //Bx_corrected
        {
            return Decimal.Add(Decimal.Multiply(colBO, Bx), colBM);
        }

        public static Decimal colBR( Decimal By , Decimal colBP , Decimal colBN ) //By_corrected
        {
            return Decimal.Add(Decimal.Multiply(By, colBP), colBN);
        }

        public static Decimal correctedBz(Decimal BzCorrected, Decimal BzScaleFactor, Decimal BzBias) //Bz_corrected
        {
            return Decimal.Add(Decimal.Multiply(BzCorrected, BzScaleFactor), BzBias);
        }

        public static Decimal colBS( Decimal Bx , Decimal colBQ ) //ΔBx_corrected
        {
            return Decimal.Subtract(colBQ, Bx);
        }

        public static Decimal colBT( Decimal By , Decimal colBR ) //ΔBy_corrected
        {
            return Decimal.Subtract(colBR, By);
        }

        public static Decimal deltaBzCorrected( Decimal Bz , Decimal BzCorrected ) //ΔBz_corrected
        {
            return Decimal.Subtract(BzCorrected, Bz);
        }

        public static Decimal colBU(Decimal colBQ, Decimal colBR, Decimal colCU) //Bt Corrected
        {
            return Convert.ToDecimal(Math.Sqrt((Double)Math.Abs((Double)(Math.Pow(((Double)colBQ), 2)) + (Double)(Math.Pow(((Double)colBR), 2)) + (Double)(Math.Pow(((Double)colCU), 2)))));
        }

        public static Decimal crrBTotalCorrected(Decimal BxCorrected, Decimal ByCorrected, Decimal BzCorrected) //Bt Corrected
        {
            return Convert.ToDecimal(Math.Sqrt((Double)Math.Abs((Double)(Math.Pow(((Double)BxCorrected), 2)) + (Double)(Math.Pow(((Double)ByCorrected), 2)) + (Double)(Math.Pow(((Double)BzCorrected), 2)))));
        }

        public static Decimal colBV(Decimal Gx, Decimal Gy, Decimal Gz, Decimal colBQ, Decimal colBR, Decimal colCU, Decimal colCF, Decimal colBU) //Dip corrected
        {
            return Decimal.Divide((Decimal.Multiply((Convert.ToDecimal(Math.Asin((Double)(Decimal.Divide((Decimal.Add((Decimal.Add((Decimal.Multiply(colBQ, Gx)), (Decimal.Multiply(colBR, Gy)))), (Decimal.Multiply(colCU, Gz)))), (Decimal.Multiply(colCF, colBU))))))), 180.00M)), ((Decimal)Math.PI));
        }

        public static Decimal crrDipCorrected(Decimal Gx, Decimal Gy, Decimal Gz, Decimal BxCorrected, Decimal ByCorrected, Decimal BzCorrected, Decimal GTotal, Decimal crrBTotal) //Dip corrected
        {
            Decimal val1 = BxCorrected * Gx;
            Decimal val2 = ByCorrected * Gy;
            Decimal val3 = BzCorrected * Gz;
            Decimal val4 = val1 + val2 + val3;
            Decimal val5 = GTotal * crrBTotal;
            Decimal val6 = val4 / val5;
            Decimal val7 = Convert.ToDecimal(Math.Asin((Double)val6));
            return Convert.ToDecimal(Units.ToDegrees((Double)val7));
            //return Convert.ToDecimal(Units.ToDegrees(Math.Asin(((((Double)BxCorrected) * ((Double)Gx) + ((Double)ByCorrected) * ((Double)Gy) + ((Double)BzCorrected) * ((Double)Gz)) / ((Double)GTotal * (Double)crrBTotal)))));
        }

        public static Decimal colBW(Int32 ifrVal, Decimal cnvBT, Decimal cnvIFRBT, Decimal colBU) //ΔBt corrected
        {
            if(ifrVal.Equals(1)) { return ( -1 * ( cnvBT - colBU ) ); } else { return ( -1 * ( cnvIFRBT- colBU ) ); }
        }

        public static Decimal crrDeltaBTotal(Int32 ifrVal, Decimal cnvBT, Decimal cnvIFRBT, Decimal crrBTotal) // Correction ΔBt corrected
        {
            if (ifrVal.Equals(1)) { return (-1 * (cnvBT - crrBTotal)); } else { return (-1 * (cnvIFRBT - crrBTotal)); }
        }

        public static Decimal colBX(Int32 ifrVal, Decimal cnvDip, Decimal cnvIFRDip, Decimal colBV) //ΔDip corrected
        {
            if (ifrVal.Equals(1)) { return Decimal.Multiply(-1, Decimal.Subtract(cnvDip, colBV)); } else {  return Decimal.Multiply(-1, Decimal.Subtract(cnvIFRDip, colBV)); }
        }

        public static Decimal crrDeltaDip(Int32 ifrVal, Decimal cnvDip, Decimal cnvIFRDip, Decimal crrDipCorrected) //Correction ΔDip corrected
        {
            if (ifrVal.Equals(1)) { return Decimal.Multiply(-1, Decimal.Subtract(cnvDip, crrDipCorrected)); } else { return Decimal.Multiply(-1, Decimal.Subtract(cnvIFRDip, crrDipCorrected)); }
        }

        public static Decimal colBZ( Decimal Bx , Decimal By ) //Boxy
        {
            return Convert.ToDecimal(Math.Sqrt((Double)Math.Abs(Decimal.Add((Decimal)Math.Pow((Double)Bx, (Double)2), (Decimal)Math.Pow((Double)By, (Double)2)))));
        }

        public static Decimal colCA( Decimal Bx , Decimal By , Decimal Bz ) //Btotal
        {
            return Convert.ToDecimal(Math.Sqrt((Math.Abs((((Math.Pow((Double)Bx, 2) + Math.Pow((Double)By, 2))) + Math.Pow((Double)Bz, 2))))));
        }

        public static Decimal colCB( Int32 srvID , Decimal colCAcurr , Decimal colCAprev ) //∆Bt - Fluctuations
        {            
            if (srvID.Equals(1)) { return 0.00M; } else if (srvID.Equals(2)) { return Convert.ToDecimal( ( (Double)colCAcurr - (Double)colCAprev ) / 2); } else { return Convert.ToDecimal( (Double)colCAcurr - (Double)colCAprev ); }
        }

        public static Int32 colCC(Int32 WellID, Int32 WellSectionID, Int32 RunID, Int32 srvID, Decimal colCD, Decimal colCDPrev, Decimal colIB, Decimal dflLBT, Decimal dflUBT, String dbConn ) //∆Bt Condition for Trend Analysis
        {            
            if (srvID.Equals(1))
            {
                if (colCD < (-1 * dflLBT) )
                {
                    return 3;
                }
                else if (colCD > dflLBT)
                {
                    return 2;
                }
                else
                {
                    return 1;
                }
            }
            else
            {
                if ((colCD - colCDPrev > dflUBT) || (colCD - colCDPrev < (-1 * dflUBT)) || (Math.Abs(colIB).Equals(0)))
                {
                    return 4;
                }
                else if (colCD < -1 * dflLBT)
                {
                    return 3;
                }
                else if (colCD > dflLBT)
                {
                    return 2;
                }
                else
                {
                    return 1;
                }
            }
        }

        public static Decimal colCD( Int32 ifrVal , Decimal cnvBT , Decimal cnvIFRBT , Decimal colCA ) //∆Btotal - LC
        {                        
                if (ifrVal.Equals(2)) { return Decimal.Subtract(colCA, cnvIFRBT); } else { return Decimal.Subtract(colCA, cnvBT);                     }
        }

        public static Decimal colCE( Decimal Gx , Decimal Gy ) //Goxy
        {
            return Convert.ToDecimal( Math.Sqrt( Math.Abs( ( Math.Pow( (Double)Gx , 2 ) + Math.Pow( (Double)Gy , 2 ) ) ) ) );
        }

        public static Decimal colCF( Decimal Gx, Decimal Gy , Decimal Gz ) //Gtotal
        {
            return Convert.ToDecimal( Math.Sqrt( Math.Abs( ( Math.Pow( (Double)Gx , 2 ) + ( ( Math.Pow( (Double)Gy , 2 ) + Math.Pow( (Double)Gz, 2 ) ) ) ) ) ) );
        }

        public static Decimal colCG( Int32 ifrVal , Decimal colCF , Decimal cnvGT , Decimal cnvIFRGT ) //∆G 
        {
            if (ifrVal.Equals(2)) { return Decimal.Subtract(colCF, cnvIFRGT); } else { return Decimal.Subtract(colCF, cnvGT); }
        }

        public static Decimal colCH( Int32 srvID , Decimal colCFcurr , Decimal colCFprev ) //∆G- Fluctuations
        {
            if (srvID.Equals(1)) { return 0.00M; } else if (srvID.Equals(2)) { return Decimal.Divide(Decimal.Subtract(colCFcurr, colCFprev), 2); } else { return Decimal.Subtract(colCFcurr, colCFprev); }
        }

        public static Int32 colCI( Int32 srvID , Decimal colCG , Decimal dflLowerGT , Decimal dflUpperGT, Decimal Gx, Decimal Gy, Decimal Gz ) //∆G Condition for Trend Analysis
        {                           
            if ( ( Math.Abs(Gx) > dflUpperGT ) || ( Math.Abs(Gy) > dflUpperGT ) || ( Math.Abs(Gz) > dflUpperGT ) ) { return 4; } else if( Gx.Equals(0.00M) && Gy.Equals(0.00M) ) { return 4;  } if( colCG > dflLowerGT ) { return 2; } else if( colCG < ( -1 * dflLowerGT ) ) { return 3; } else { return 1; }
        }

        public static Decimal colCK( Int32 ifrVal , Decimal Bz , Decimal colT , Decimal colY , Decimal colBK , Decimal cnvDip , Decimal cnvIFRDip , Decimal cnvBT , Decimal cnvIFRBT ) //dλ = Del Dip
        {
            if(ifrVal.Equals(1))
            {
                return ((((Bz - colBK) / cnvBT) * (((Decimal)Math.Cos(Units.ToRadians((Double)colT)) * (Decimal)Math.Cos(Units.ToRadians((Double)cnvDip))) - ((Decimal)Math.Sin(Units.ToRadians((Double)colT)) * (Decimal)Math.Cos(Units.ToRadians((Double)colY)) * (Decimal)Math.Sin(Units.ToRadians((Double)cnvDip))))) * 180 / (Decimal)Math.PI);
            }
            else
            {
                return ((((Bz - colBK) / cnvIFRBT) * (((Decimal)Math.Cos(Units.ToRadians((Double)colT)) * (Decimal)Math.Cos(Units.ToRadians((Double)cnvIFRDip))) - ((Decimal)Math.Sin(Units.ToRadians((Double)colT)) * (Decimal)Math.Cos(Units.ToRadians((Double)colY)) * (Decimal)Math.Sin(Units.ToRadians((Double)cnvIFRDip))))) * 180 / (Decimal)Math.PI);
            }         
        }

        public static Decimal colCL( Int32 ifrVal , Decimal colT , Decimal colY , Decimal colCK , Decimal cnvDip , Decimal cnvIFRDip ) //dAz1
        {           
                Double colTVal = Convert.ToDouble(colT);
                Double colYVal = Convert.ToDouble(colY);
                Double colCKVal = Convert.ToDouble(colCK);
                Double cnvDipVal = Convert.ToDouble(cnvDip);
                Double cnvIFRDipVal = Convert.ToDouble(cnvIFRDip);

                if (ifrVal.Equals(1))
                {
                    return Convert.ToDecimal(((-1 * ((colCKVal * ((Math.Sin(Units.ToRadians(colTVal))) * (Math.Sin(Units.ToRadians(colYVal)))))))) / (((Math.Cos(Units.ToRadians(cnvDipVal))) * ((((Math.Sin(Units.ToRadians(colTVal))) * ((Math.Cos(Units.ToRadians(colYVal))) * (Math.Sin(Units.ToRadians(cnvDipVal)))))) - (((Math.Cos(Units.ToRadians(colTVal))) * (Math.Cos(Units.ToRadians(cnvDipVal)))))))));
                }
                else
                {
                    return Convert.ToDecimal( -1 * ( ( (Double)colCK ) * ( Math.Sin( Units.ToRadians( (Double)colT ) ) * Math.Sin( Units.ToRadians ( (Double)colY ) ) ) ) / ( ( Math.Cos( Units.ToRadians( (Double)cnvIFRDip ) ) ) * ( ( Math.Sin( Units.ToRadians( (Double)colT ) ) * Math.Cos( Units.ToRadians( (Double)colY ) ) * Math.Sin( Units.ToRadians( (Double)cnvIFRDip ) ) ) - ( Math.Cos( Units.ToRadians( (Double)colT ) ) * Math.Cos( Units.ToRadians( (Double)cnvIFRDip ) ) ) ) ) );                    
                }
        }

        public static Decimal colCM( Decimal colY , Decimal colCL ) //Corr Azm
        {
            if (Decimal.Add(colCL, colY) > 359.99M) { return Decimal.Subtract(Decimal.Add(colCL, colY), 360.00M); } else { return Decimal.Add(colCL, colY); }
        }

        public static Decimal colCN( Int32 ifrVal , Decimal colT , Decimal colCM , Decimal cnvDip , Decimal cnvIFRDip , Decimal cnvBT , Decimal cnvIFRBT  ) //Bzo'
        {            
                if(ifrVal.Equals(1)) // No
                {
                    return Convert.ToDecimal(((Double)cnvBT * Math.Cos(Units.ToRadians((Double)cnvDip)) * Math.Sin(Units.ToRadians((Double)colT))* Math.Cos(Units.ToRadians((Double)colCM))) + ((Double)cnvBT * Math.Sin(Units.ToRadians((Double)cnvDip)) * Math.Cos(Units.ToRadians((Double)colT))));
                }
                else
                {
                    return Convert.ToDecimal(((Double)cnvIFRBT * Math.Cos(Units.ToRadians( (Double)cnvIFRDip )) * Math.Sin(Units.ToRadians( (Double)colT)) * Math.Cos(Units.ToRadians( (Double)colCM ))) + ( (Double)cnvIFRBT * Math.Sin(Units.ToRadians((Double)cnvIFRDip)) * Math.Cos(Units.ToRadians((Double)colT))));
                }
        }

        public static Decimal colCO(Decimal Bz, Decimal colCN, Decimal colCS, Decimal colGV, Decimal colID) //∆Bz-Corrected - Conditioned = Bzsc-BZLC
        {
            //if(Math.Abs(colCN-Bz)>Math.Abs(colGV)) { return Decimal.Multiply( Math.Sign(colCS) , colGV ); } else { return Decimal.Subtract( colCN , Bz ); }
            if (Math.Abs(colCN - Bz) > Math.Abs(colGV))
            { return ( Math.Sign(colID) * colGV ); }
            else
            { return colCN - Bz; }
        }

        public static Decimal colCP(Int32 srvyRowID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Decimal colCO, List<Decimal> coList, Decimal colCS, List<Decimal> csList, String dbConn) //Ave -∆Bz-Corrected - Conditioned = Bzsc-BZLC
        {
            if (Math.Abs(colCO) < Math.Abs(colCS)) 
            {                    
                return Decimal.Divide(Decimal.Add(coList.Average(), csList.Average()), 2);
            }
            else
            {
                return colCO;
            }
        }

        public static Decimal colCQ(Int32 ServiceID, Int32 srvyRowID, List<Double> SurveyIDList, Int32 WellID, Int32 WellSection, Int32 RunID, Decimal colHSVal, List<Double> HSValues, String dbConn) //∆BzSC - Trend
        {            
            Double result = -99.00;
            if ( srvyRowID.Equals(1) )
            {
                return colHSVal;
            }
            else
            {
                HSValues.Add((Double)colHSVal);
                SurveyIDList.Add((Double)srvyRowID);
                Double[] s = new Double[] { },  hsv = new Double[] { };
                s = SurveyIDList.ToArray();
                hsv = HSValues.ToArray();
                Tuple<Double, Double> p = MathNet.Numerics.Fit.Line(s, hsv);
                result = (p.Item1) + (p.Item2);
                if (result < 0.0000 || Double.IsNaN(result)) { return 0.00M; } else { return Convert.ToDecimal(result); }
            }
        }

        public static Decimal colCR(Decimal Bz, Decimal colCQ) //Bzo'-Trend Corrected - Conditioned
        {
            return Decimal.Add(colCQ, Bz);
        }

        public static Decimal colCS(Decimal Bz, Decimal colCN) //∆Bz = Bzsc-BZLC
        {
            return Decimal.Subtract(colCN, Bz);
        }

        public static Decimal colCT(Int32 ServiceID, Int32 srvyRowID, List<Double> SurveyIDValues, Int32 WellID, Int32 WellSectionID, Int32 RunID, Decimal colHTVal, List<Double> colHTValuesList, String dbConn) //∆Bz - Trend
        {            
            if (ServiceID.Equals(33) || ServiceID.Equals(34) || ServiceID.Equals(37) || ServiceID.Equals(38) || ServiceID.Equals(39) || ServiceID.Equals(40))
            {
                return qccolCT(ServiceID, srvyRowID, SurveyIDValues, WellID, WellSectionID, RunID, colHTVal, colHTValuesList, dbConn);
            }
            else
            {
                return corrcolCT(ServiceID, srvyRowID, SurveyIDValues, WellID, WellSectionID, RunID, colHTVal, colHTValuesList, dbConn);
            }
        }

        public static Decimal qccolCT(Int32 ServiceID, Int32 srvyRowID, List<Double> SurveyIDList, Int32 WellID, Int32 WellSectionID, Int32 RunID, Decimal colHTVal, List<Double> colHTValues, String dbConn) //∆Bz - Trend
        {                     
            if (srvyRowID.Equals(1))
            {
                return colHTVal;
            }
            else
            {
                colHTValues.Add((Double)colHTVal);
                //SurveyIDList.Add((Double)srvyRowID);
                Double[] s = new Double[] { }, htv = new Double[] { };
                s = SurveyIDList.ToArray();
                htv = colHTValues.ToArray();
                {
                    Tuple<Double, Double> p = MathNet.Numerics.Fit.Line(s, htv);
                    Double result = p.Item1 + p.Item2;
                    if (result < 0.0000 || Double.IsNaN(result)) { return 0.0000M; } else { return Convert.ToDecimal(result); }
                }
            }
        }

        public static Decimal corrcolCT(Int32 ServiceID, Int32 srvyRowID, List<Double> SurveyIDList, Int32 WellID, Int32 WellSectionID, Int32 RunID, Decimal colHTVal, List<Double> colHTValuesList, String dbConn) //∆Bz - Trend
        {            
            if (srvyRowID.Equals(1))
            {
                return colHTVal;
            }
            else
            {
                colHTValuesList.Add((Double)colHTVal);
                SurveyIDList.Add((Double)srvyRowID);
                Double[] s = new Double[] { }, htv = new Double[] { };
                s = SurveyIDList.ToArray();
                htv = colHTValuesList.ToArray();
                {
                    Tuple<Double, Double> p = MathNet.Numerics.Fit.Line(s, htv);

                    Double intercept = p.Item1;
                    Double slope = p.Item2;
                    return Convert.ToDecimal(intercept + slope);
                }
            }
        }

        public static Decimal idCorrcolCT(Int32 ServiceID, Int32 srvyRowID, List<Double> SurveyIDValues, Int32 WellID, Int32 WellSectionID, Int32 RunID, Decimal colHTVal, List<Double> colHTValuesList, String dbConn) //∆Bz - Trend
        {            
            if (srvyRowID.Equals(1))
            {
                return colHTVal;
            }
            else
            {
                colHTValuesList.Add((Double)colHTVal);
                SurveyIDValues.Add((Double)srvyRowID);
                Double[] s = new Double[] { }, htv = new Double[] { };
                s = SurveyIDValues.ToArray();
                htv = colHTValuesList.ToArray();
                {
                    Tuple<Double, Double> p = MathNet.Numerics.Fit.Line(s, htv);
                    Double intercept = p.Item1;
                    Double slope = p.Item2;
                    return Convert.ToDecimal(intercept + slope);
                }
            }
        }

        public static Decimal colCU(Decimal Bz, Decimal colCT) //Bzo'-Trend Corrected
        {
            return Decimal.Add(Bz, colCT);            
        }
        
        public static Decimal colCV(Decimal Bx, Decimal By, Decimal gtfVal, Decimal colT, Decimal colCU) //Corr Mag_SCAZ_Final
        {
            try
            {
                Decimal constA = 0.0000M;
                Decimal constB = 0.0000M;
                Decimal valA = 0.0000M;
                Decimal valB = 0.0000M;
                Decimal valC = 0.0000M;
                Decimal valD = 0.0000M;
                Decimal valE = 0.0000M;
                Decimal valF = 0.0000M;

                constA = Convert.ToDecimal(Math.PI / 180);
                constB = Convert.ToDecimal(180 / Math.PI);
                valB = Decimal.Multiply(gtfVal, constA);
                valC = Decimal.Multiply(colT, constA);
                valD = Decimal.Multiply(Decimal.Subtract(Decimal.Multiply(Bx, (decimal)Math.Cos((double)valB)), Decimal.Multiply(By, (decimal)Math.Sin((double)valB))), (decimal)Math.Cos((double)valC));
                valE = Decimal.Multiply(colCU, (decimal)Math.Sin((double)valC));
                valF = Decimal.Add((Decimal.Multiply(Bx, (decimal)Math.Sin((double)valB))), (Decimal.Multiply(By, (decimal)Math.Cos((double)valB))));
                valA = Decimal.Add((Decimal.Multiply(((decimal)Math.Atan2((double)(Decimal.Multiply(-1, valF)), (double)(Decimal.Add(valD, valE)))), constB)), 360);

                if ((valA % 360) > 360)
                {
                    return (valA % 360) - 360;
                }
                else
                {
                    return (valA % 360);
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colCW(Decimal colY, Decimal colCV) //ΔSCAZmag_Final
        {
            return Decimal.Subtract(colY, colCV);
        }

        public static Decimal colCX(Decimal MagneticDeclination, Decimal GridConvergence, Decimal colCVVal, Int32 LatitudeNS, Int32 MDecCID, Int32 gcoID) //SCAZ - Ψsc - Plain
        {            
                //North = 1, East = 9, South = 17, West = 25;
                Decimal testValue = -99.0000M;
                if(LatitudeNS.Equals(1) && MDecCID.Equals(9) && gcoID.Equals(1))
                {
                    testValue = colCVVal+MagneticDeclination-GridConvergence;
                }
                else if(LatitudeNS.Equals(1) && MDecCID.Equals(9) && gcoID.Equals(2))
                {
                    testValue = colCVVal + MagneticDeclination + GridConvergence;
                }
                else if (LatitudeNS.Equals(1) && MDecCID.Equals(25) && gcoID.Equals(1))
                {
                    testValue = colCVVal - MagneticDeclination - GridConvergence;
                }
                else if (LatitudeNS.Equals(1) && MDecCID.Equals(25) && gcoID.Equals(2))
                {
                    testValue = colCVVal - MagneticDeclination + GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && MDecCID.Equals(9) && gcoID.Equals(1))
                {
                    testValue = colCVVal + MagneticDeclination + GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && MDecCID.Equals(9) && gcoID.Equals(2))
                {
                    testValue = colCVVal + MagneticDeclination - GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && MDecCID.Equals(25) && gcoID.Equals(1))
                {
                    testValue = colCVVal - MagneticDeclination + GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && MDecCID.Equals(25) && gcoID.Equals(2))
                {
                    testValue = colCVVal - MagneticDeclination - GridConvergence;
                }
                else
                {
                    testValue = colCVVal;
                }

                if (testValue > 359.99M) { return testValue - 360.00M; } else if (testValue < 0.00M) { return testValue + 360.00M; } else { return testValue; }
        }

        public static Decimal colCY(Decimal iMagneticDeclination, Decimal GridConvergence, Decimal colCVVal, Int32 LatitudeNS, Int32 iMDecCID, Int32 gcoID) //SCAZ - Ψsc - Plain
        {            
                //North = 1, East = 9, South = 17, West = 25;
                Decimal testValue = -99.0000M;
                if (LatitudeNS.Equals(1) && iMDecCID.Equals(9) && gcoID.Equals(1))
                {
                    testValue = colCVVal + iMagneticDeclination - GridConvergence;
                }
                else if (LatitudeNS.Equals(1) && iMDecCID.Equals(9) && gcoID.Equals(2))
                {
                    testValue = colCVVal + iMagneticDeclination + GridConvergence;
                }
                else if (LatitudeNS.Equals(1) && iMDecCID.Equals(25) && gcoID.Equals(1))
                {
                    testValue = colCVVal - iMagneticDeclination - GridConvergence;
                }
                else if (LatitudeNS.Equals(1) && iMDecCID.Equals(25) && gcoID.Equals(2))
                {
                    testValue = colCVVal - iMagneticDeclination + GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && iMDecCID.Equals(9) && gcoID.Equals(1))
                {
                    testValue = colCVVal + iMagneticDeclination + GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && iMDecCID.Equals(9) && gcoID.Equals(2))
                {
                    testValue = colCVVal + iMagneticDeclination - GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && iMDecCID.Equals(25) && gcoID.Equals(1))
                {
                    testValue = colCVVal - iMagneticDeclination + GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && iMDecCID.Equals(25) && gcoID.Equals(2))
                {
                    testValue = colCVVal - iMagneticDeclination - GridConvergence;
                }
                else
                {
                    testValue = colCVVal;
                }

                if (testValue > 359.99M) { return testValue - 360.00M; } else if (testValue < 0.00M) { return testValue + 360.00M; } else { return testValue; }
        }

        public static Decimal colCZ(Decimal MagneticDeclination, Decimal colCVVal, Int32 MDecCID) //SCAZ - Ψsc - Plain
        {            
                //North = 1, East = 9, South = 17, West = 25;
                Decimal testValue = -99;
                if (MDecCID.Equals(9))
                {
                    testValue = colCVVal + MagneticDeclination;
                }
                else
                {
                    testValue = colCVVal - MagneticDeclination;
                }

                if (testValue > 359.99M) { return testValue - 360.00M; } else if (testValue < 0.00M) { return testValue + 360.00M; } else { return testValue; }
        }

        public static Decimal colDA(Decimal iMagneticDeclination, Decimal colCVVal, Int32 iMDecCID) //SCAZ - Ψsc - Plain
        {            
                //North = 1, East = 9, South = 17, West = 25;
                Decimal testValue = -99;
                if (iMDecCID.Equals(9))
                {
                    testValue = colCVVal + iMagneticDeclination;
                }
                else
                {
                    testValue = colCVVal - iMagneticDeclination;
                }

                if (testValue > 359.99M) { return testValue - 360.00M; } else if (testValue < 0.00M) { return testValue + 360.00M; } else { return testValue; }
        }

        public static Decimal colDB(Int32 ifrVal, Decimal colCX, Decimal colCY) //Grid - SCAZ - Ψsc - Final
        {
            if ( ifrVal.Equals(1) ) { return colCX; } else { return colCY; }
        }

        public static Decimal colDC(Int32 ifrVal, Decimal colCZ, Decimal colDA) //True - SCAZ - Ψsc - Final
        {
            if ( ifrVal.Equals(1) ) { return colCZ; } else { return colDA; }
        }

        public static Decimal colDD(Decimal jConverg, Decimal colDB, Decimal colDC) //SCAZ - Ψsc - Final
        {
            if( jConverg.Equals(2) ) { return colDB; } else { return colDC; }
        }

        public static Decimal colDE(Decimal colAF, Decimal colDD) //ΔSCAZFinal
        {
            return Decimal.Subtract( colAF, colDD );
        }

        public static Int32 colDF(Decimal colDE) //Short Collar  Azimuth Error - ∆SCAz° Condition for Trend Analysis
        {
            if ( ( colDE <= 0.50M && colDE >= -0.50M) ) { return 1; } else if( ( colDE > 0.50M || colDE <- 0.50M ) )  { return 2; } else { return 3; }
        }

        public static Int32 colDG(Decimal colCS, Decimal colGB, Decimal colGV) //∆∆Bz = ∆Bp - ∆Bzsz-zLC - Condition for Trend Analysis
        {
            if( ( Math.Abs( colCS ) - colGV ) > 0  ) { return 3;  } else if( ( Math.Abs( colCS ) - colGB ) > 0 )  { return 2; } else { return 1; }
        }

        public static Int32 colDH(Decimal Bx, Decimal By, Decimal colCU) //BtSCAZ corrected
        {            
                return Convert.ToInt32(Math.Sqrt(Math.Abs((Math.Pow((Double)Bx, 2)) + (Math.Pow((Double)By, 2)) + (Math.Pow((Double)colCU, 2)))));
        }

        public static Decimal colDI(Decimal Gx, Decimal Gy, Decimal Gz, Decimal Bx, Decimal By, Decimal colCF, Decimal colCU, Int32 colDH) //DipSCAZ corrected
        {
            return Convert.ToDecimal(Units.ToDegrees(Math.Asin(((((Double)Bx * (Double)Gx)) + ((Double)By * (Double)Gy) + ((Double)colCU * (Double)Gz)) / ((Double)colCF * (Double)colDH))));
        }

        public static Int32 colDJ(Int32 ifrVal, Decimal convBT, Decimal conviBT, Decimal colDH) //ΔBtSCAZ corrected
        {
            if(ifrVal.Equals(1)) { return Convert.ToInt32( -1 * ( convBT - colDH) ); } else { return Convert.ToInt32( -( conviBT - colDH ) ); }
        }

        public static Decimal colDK(Int32 ifrVal, Decimal cnvDip, Decimal cnviDip, Decimal colDI) //ΔDipSCAZ corrected
        {
            if( ifrVal.Equals(1)) { return -1 * ( cnvDip - colDI ); } else { return -1 * ( cnviDip - colDI ); }
        }

        public static Decimal colDR(Decimal colAF) //Azimuth° (Grid)
        {
            if ( (Double)colAF >= 360 ) { return Convert.ToDecimal( (Double)colAF - 360 ); } else { return colAF; }
        }

        public static Int32 colDW(Decimal sqcDip, Decimal colBC) //Dip Qualifier
        {
            if ( ( (Double)colBC <= (Double)sqcDip ) && ( (Double)colBC >= ( -1 * (Double)sqcDip ) ) ) { return 1; } else { return 2; }
        }

        public static Int32 colDY(Decimal sqccnvBT, Decimal colCD) //Btotal Qualifier
        {
            if ( ( colCD.Equals(sqccnvBT) || colCD < sqccnvBT ) && ( colCD.Equals( -1 * sqccnvBT ) || colCD > ( -1 * sqccnvBT ) ) ) { return 1; } else { return 2; }
        }

        public static Int32 colEA(Decimal sqccnvGT, Decimal colCG) //Gtotal Qualifier
        {
            if ( ( colCG <= sqccnvGT ) && ( colCG >= ( -1 * sqccnvGT ) ) ) { return 1; } else { return 2; }
        }

        public static Int32 colEG(Decimal colAY) //Short Collar Application
        {
            if ( colAY.Equals(0)) { return 1; } else { return 2; }
        }

        public static Int32 colEK(Decimal colEG) //BHA Non Mag Spacing (Std. Pole Strength)
        {
            if (Math.Abs(colEG) > 0.5M) { return 3; } else { return 4; }
        }

        public static Decimal colEI(Decimal colGC, Decimal colGE) //ΔAzimuth due to Motor only (Std. Pole Strength)°
        {
            return Decimal.Multiply( Math.Sign( colGE ) , colGC );            
        }

        public static Decimal colEJ(Decimal colGD, Decimal colGE) //ΔAzimuth due to Drill Collar only (Std. Pole Strength)°
        {
            return Decimal.Multiply( Math.Sign( colGE ) , colGD );
        }

        public static Int32 colEO( Decimal colEL) //BHA Non Mag Spacing (Gaussmeter based Pole Strength)
        {
            if(Math.Abs((Double)colEL) > 0.5) { return 3; } else { return 4; }
        }

        public static Decimal colEQ( Decimal colGW, Decimal colGY) //ΔAzimuth due to Motor only (Worst Case - Std. Pole Strength)°
        {
            return Decimal.Multiply( Math.Sign( colGY ) , colGW );
        }

        public static Decimal colER(Decimal colGX, Decimal colGY) //ΔAzimuth due to Drill Collar only (Worst Case - Std. Pole Strength)°
        {
            return Decimal.Multiply( Math.Sign( colGY ) , colGX );
        }

        public static Int32 colET( Decimal colEP ) //BHA Non Mag Spacing (Worst Case)
        {
            if(Math.Abs( (Double)colEP ) > 0.5 ) { return 3; } else { return 4; }
        }

        public static Decimal colEU(Int32 srvyRowID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Decimal colBACurr, List<Decimal> colBAList, String dbConn) //Minimum Dip
        {            
            if (srvyRowID.Equals(1)) { return colBACurr; } else { return Math.Min(colBACurr, colBAList.Min()); }
        }

        public static Decimal colEV( Int32 srvyRowID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Decimal colBACurr, List<Decimal> colBAList, String dbConn ) //Maximum Dip
        {
            if (srvyRowID.Equals(1)) { return colBACurr; } else { return Math.Max(colBACurr, colBAList.Max()); }
        }

        public static Decimal colEW( Decimal colEU , Decimal colEV ) //Dip Spread
        {
            return Decimal.Subtract(colEV, colEU);
        }

        public static Int32 colEX( Int32 srvyRowID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Decimal colCACurr, List<Decimal> colCAList, String dbConn ) //Minimum B-Total
        {            
            if (srvyRowID.Equals(1)) { return Convert.ToInt32(colCACurr); } else { return Convert.ToInt32(Math.Min(colCACurr, colCAList.Min())); }
        }

        public static Int32 colEY( Int32 srvyRowID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Decimal colCACurr, List<Decimal> colCAList, String dbConn ) //Maximum B-Total
        {            
            if (srvyRowID.Equals(1)) { return Convert.ToInt32(colCACurr); } else { return Convert.ToInt32(Math.Max(colCACurr, colCAList.Max())); }
        }

        public static Int32 colEZ( Int32 colEX, Int32 colEY) //B-Total Spread
        {
            return (Int32)(colEY - colEX);
        }

        public static Decimal colFA( Int32 srvyRowID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Decimal colCFCurr, List<Decimal> colCFList, String dbConn ) //Minimum G-Total
        {
            if (srvyRowID.Equals(1)) { return colCFCurr; } else { return Math.Min(colCFCurr, colCFList.Min()); }
        }

        public static Decimal colFB( Int32 srvyRowID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Decimal colCFCurr, List<Decimal> colCFList, String dbConn ) //Maximum G-Total
        {
            if (srvyRowID.Equals(1)) { return colCFCurr; } else { return Math.Max(colCFCurr, colCFList.Max()); }
        }

        public static Decimal colFC(Decimal colFA, Decimal colFB) // G-Total spread
        {
            return Decimal.Subtract(colFB, colFA);
        }        

        public static Int32 colFW( Decimal cnvMSize ) //Motor Pole strength (µWb)
        {
            if (cnvMSize.Equals(0)) { return 0; } else { return Convert.ToInt32((((((((-1.2952 * (Math.Pow(Math.Abs((Double)cnvMSize), 3)))) + ((26.353 * (Math.Pow(Math.Abs((Double)cnvMSize), 2)))))) - ((122.55 * Math.Abs((Double)cnvMSize))))) + 486.47)); }
        }

        public static Int32 colFX( Decimal cnvDSSize ) //Drill Collar Pole Strength  (µWb)
        {
            if (cnvDSSize.Equals(0)) { return 0; } else { return Convert.ToInt32((-1.0486 * Math.Pow(Math.Abs((Double)cnvDSSize), 3)) + (23.539 * Math.Pow(Math.Abs((Double)cnvDSSize), 2)) - (85.581 * Math.Abs((Double)cnvDSSize)) + 488.27); }
        }
        
        public static Decimal colFY( Int32 ifrVal , Decimal colBA , Decimal colCA ) //Bhor=Bon= Horizontal Component of Bt (nT)
        {
            if (ifrVal.Equals(1)) { return Convert.ToDecimal(colCA * (Decimal)Math.Cos(Units.ToRadians((Double)colBA))); } else { return Convert.ToDecimal(colCA * (Decimal)Math.Cos(Units.ToRadians((Double)colBA))); }
        }

        public static Decimal colFZ( Decimal NMSpAbv , Int32 colFX ) //Bp-Drill Collar - DC Interference (nT)
        {
            return Convert.ToDecimal( ( (Double)colFX / (4* Math.PI) * ( ( 1 / Math.Pow( ( (Double)NMSpAbv + 0.01 ) , 2 ) ) ) ) * 1000 );
        }

        public static Decimal colGA( Decimal NMSpBlw , Decimal cnvMDBLen , Decimal colFW ) // Bp-Motor - Motor Interference (nT)
        {
            return ( ( colFW / ( 4 * (Decimal)Math.PI ) ) * ( ( 1 / (Decimal)Math.Pow( ( (Double)NMSpBlw + 0.01 ) , 2 ) ) - ( 1 / (Decimal)Math.Pow( ( (Double)NMSpBlw + (Double)cnvMDBLen + 0.01 ) , 2 ) ) ) ) * 1000;
        }

        public static Decimal colGB( Decimal colFZ , Decimal colGA ) //Bp - BHA Interference (nT)
        {
            return Decimal.Add(colFZ, colGA);
        }

        public static Decimal colGC(Decimal colT, Decimal colY, Decimal colFY, Decimal colGA) //Downhole - Δ LCAZ Error due to Motor°
        {
            return Convert.ToDecimal(Math.Abs(Units.ToDegrees(Math.Atan(((Double)colGA * Math.Sin(Units.ToRadians((Double)colT)) * Math.Sin(Units.ToRadians((Double)colY))) / ((Double)colFY + ((Double)colGA * (Math.Sin(Units.ToRadians((Double)colT)) * Math.Cos(Units.ToRadians((Double)colY)))))))));
        }

        public static Decimal colGD(Decimal colT, Decimal colY, Decimal colFY, Decimal colFZ) //Downhole - Δ LCAZ Error due to Drill Collars° (with Std. Pole Strength)
        {
            return (Decimal)Math.Abs(Units.ToDegrees(Math.Atan((Double)(colFZ * (Decimal)Math.Sin(Units.ToRadians((Double)colT)) * (Decimal)Math.Sin(Units.ToRadians((Double)colY)))/(Double)(colFY+(colFZ * ( (Decimal)Math.Sin(Units.ToRadians((Double)colT)) * (Decimal)Math.Cos(Units.ToRadians((Double)colY))))))));
        }        

        public static Decimal colGE(Decimal Inclination, Decimal MagneticAzimuth, Decimal colFYVal, Decimal colGBVal, Decimal colIDVal) //Downhole - Δ LCAZ Error due to BHA°
        {
            //return Convert.ToDecimal(Math.Sign(colCTVal) * Math.Abs( Units.ToDegrees( Math.Atan( ( (Double)colGBVal * Math.Sin( Units.ToRadians( (Double)Inclination ) ) * Math.Sin( Units.ToRadians( (Double)MagneticAzimuth ) ) ) / ( (Double)colFYVal + ( (Double)colGBVal * ( Math.Sin( Units.ToRadians( (Double)Inclination ) ) * Math.Cos( Units.ToRadians( (Double)MagneticAzimuth )))))))));
            return (colIDVal * (Decimal)Math.Abs(Units.ToDegrees(Math.Atan((Double)(colGBVal * (Decimal)Math.Sin(Units.ToRadians((Double)Inclination)) * (Decimal)Math.Sin(Units.ToRadians((Double)MagneticAzimuth))) / (Double)(colFYVal + (colGBVal * ((Decimal)Math.Sin(Units.ToRadians((Double)Inclination)) * (Decimal)Math.Sin(Units.ToRadians((Double)MagneticAzimuth)))))))));
        }

        public static Int32 colGF(Decimal colGE) //Downhole - Δ LCAZ Error due to BHA Condition for Trend Analysis
        {
            if (Math.Abs(colGE) <= 0.5M) { return 1; } else { return 2; }
        }

        public static Decimal colGH( Decimal colFWVal , Decimal cnvBHAMSize , Decimal convBHAMGVal , Decimal cnvBHADGBottom ) //Downhole - MPS Motor (μWb)
        {
            if(convBHAMGVal.Equals(0))
            {
                return ( colFWVal * ( 4 * (Decimal)Math.PI * (Decimal)Math.Pow( (Double)cnvBHADGBottom + (Double)( ( cnvBHAMSize / 12 ) * 0.3048M ) , 2 ) ) * 100);
            }
            else
            {
                return (convBHAMGVal * (4 * (Decimal)Math.PI * (Decimal)Math.Pow((Double)cnvBHADGBottom + (Double)((cnvBHAMSize / 12) * 0.3048M), 2)) * 100);
            }
        }

        public static Decimal colGI( Decimal colGH , Decimal bhaNMSpBlw , Decimal cnvMDBLen ) //Bp-Motor (nTesla)
        {
            return ((Decimal)(1 / (4 * Math.PI)) * ((colGH / (Decimal)Math.Pow((Double)(bhaNMSpBlw + 0.01M), 2)) - (colGH / (Decimal)Math.Pow((Double)(cnvMDBLen + bhaNMSpBlw - 0.01M), 2)))) * 1000;
        }

        public static Decimal colGJ(Decimal colCT, Decimal colGI) //Bp-Drill Collar/Jar (nTesla)
        {
            return Convert.ToDecimal(Math.Abs((Math.Abs((Double)colCT) - Math.Abs((Double)colGI))));
        }

        public static Decimal colGK(Decimal colGJ, Decimal bhaNMSpAbv) //Downhole - MPS Drill Collar  (μWb)
        {
            return Convert.ToDecimal((4 * Math.PI) * (((Double)colGJ * (Math.Pow(((Double)bhaNMSpAbv + 0.01), 2))) * 0.001));
        }

        public static Int32 colGL(Decimal colGK, Decimal dcpsLimit) //Warning - Downhole - MPS Drill Collar  (μWb)
        {
            if (colGK > dcpsLimit) { return 1; } else { return 2; }
        }

        public static Decimal colGM(Decimal colGK, Decimal bhaDSCSize) //Actual - Gaussmeter (DRILL Collar/Jar) (nTesla)
        {
            return Convert.ToDecimal(((Double)colGK / ((4 * Math.PI) * (Math.Pow(((((Double)bhaDSCSize / 12) * 0.3048) + 0.01), 2)))) * 0.01);            
        }

        public static Decimal colGN(Decimal Inclination, Decimal MagneticAzimuth, Decimal colFYVal, Decimal colGIVal, Decimal colIDVal) //Actual Azimuth Error due to Motor °
        {
            return colIDVal * (Decimal)Math.Abs(Units.ToDegrees( Math.Atan((Double)( ( colGIVal * (Decimal)Math.Sin((Double)Inclination) * (Decimal)Math.Sin((Double)MagneticAzimuth) ) / ( colFYVal + colGIVal * (Decimal)Math.Sin((Double)Inclination) * (Decimal)Math.Cos((Double)MagneticAzimuth))))));
        }

        public static Decimal colGO(Decimal Inclination, Decimal MagneticAzimuth, Decimal colGJVal, Decimal colFYVal, Decimal colIDVal) //Actual Azimuth Error  due to Drill Collars°
        {
            return colIDVal * (Decimal)Math.Abs(Units.ToDegrees(Math.Atan((Double)((Double)(colGJVal*(Decimal)Math.Sin((Double)Inclination)*(Decimal)Math.Sin((Double)MagneticAzimuth))/(Double)(colFYVal+colGJVal*(Decimal)Math.Sin((Double)Inclination)*(Decimal)Math.Cos((Double)MagneticAzimuth))))));
        }
        
        public static Decimal colGP(Decimal Inclination, Decimal MagneticAzimuth, Decimal colFYVal, Decimal colCTVal, Decimal colIDVal) //Actual Azimuth Error °
        {
            return colIDVal * (Decimal)Math.Abs(Units.ToDegrees(Math.Atan( (Double)(colCTVal * (Decimal)Math.Sin((Double)Inclination) * (Decimal)Math.Sin((Double)MagneticAzimuth)) / (Double)(colFYVal + colCTVal * (Decimal)Math.Sin((Double)Inclination) * (Decimal)Math.Cos((Double)MagneticAzimuth)))));
        }

        public static Decimal colGR( Decimal colFW , Decimal colGH ) //Worst Case - Downhole - MPS Motor (μWb)
        {
            if (Math.Abs((Double)colGH) < Math.Abs((Double)colFW)) { return Convert.ToDecimal(Math.Abs((Double)colFW)); } else { return Convert.ToDecimal(Math.Abs((Double)colGH)); }
        }

        public static Decimal colGS( Decimal colGR , Decimal bhaNMSpBlw , Decimal bhaMDBLen ) //Worst Case - Bp-Motor (nTesla)
        {
            return Convert.ToDecimal(1 / (4 * Math.PI) * (((Double)colGR / Math.Pow(((Double)bhaNMSpBlw + 0.01), 2)) - ((Double)colGR / Math.Pow(((Double)bhaMDBLen + (Double)bhaNMSpBlw - 0.01), 2))) * 1000);
        }

        public static Decimal colGT( Decimal bhadmlValue ) //Worst Case - Downhole - MPS Drill Collar  (μWb)
        {
            return bhadmlValue;
        }

        public static Decimal colGU( Decimal bhaNMSpAbv , Decimal colGT ) //Worst Case - Gaussmeter (DRILL Collar/Jar) (nTesla)
        {
            return Convert.ToDecimal( ( (Double)colGT / (4 * Math.PI) * ((1 / Math.Pow(((Double)bhaNMSpAbv + 0.01) , 2)))) * 1000);
        }

        public static Decimal colGV( Decimal colGS , Decimal colGU ) //WORST CASE Bz Bias - Bp Worst (nT)
        {
            return Decimal.Add(colGS, colGU);
        }

        public static Decimal colGW( Decimal colT , Decimal colY , Decimal colFY , Decimal colGS ) //Worst Case Azimuth Error due to Motor°
        {
            return Convert.ToDecimal(Math.Abs((180 / Math.PI)*(Math.Atan((Double)colGS * Math.Sin((Double)colT) * Math.Sin((Double)colY) / ((Double)colFY + (Double)colGS * Math.Sin((Double)colT) * Math.Cos((Double)colY))))));
        }

        public static Decimal colGX( Decimal colT , Decimal colY , Decimal colFY , Decimal colGU ) //Worst Case Azimuth Error due to Drill Collars°
        {
            return Convert.ToDecimal(Math.Abs((180 / Math.PI) * (Math.Atan((Double)colGU * Math.Sin((Double)colT) * Math.Sin((Double)colY) / ((Double)colFY + (Double)colGU * Math.Sin((Double)colT) * Math.Cos((Double)colY))))));
        }

        public static Decimal colGY( Decimal Inclination, Decimal MagneticAzimuth, Decimal colFYVal, Decimal colGVVal, Decimal colIDVal) //Worst Case -Actual Azimuth Error °
        {
            return colIDVal * (Decimal)Math.Abs(Units.ToDegrees(Math.Atan((Double)(colGVVal * (Decimal)Math.Sin((Double)Inclination) * (Decimal)Math.Sin((Double)MagneticAzimuth)) / (Double)(colFYVal + colGVVal * (Decimal)Math.Sin((Double)Inclination) * (Decimal)Math.Cos((Double)MagneticAzimuth)))));
        }

        public static Decimal colGZ(Decimal colGV, Decimal colCT) //Δ (Worst Case Bz Bias - SCBz Bias) (nT)
        {
            return Convert.ToDecimal(Math.Abs((Double)colGV) - Math.Abs((Double)colCT));
        }

        public static Decimal colHA(Decimal colGY, Decimal colDE) //Δ (Worst Case Azimuth Error due to BHA - SCAZ)°
        {            
            return Convert.ToDecimal( Math.Abs((Double)colGY) - Math.Abs((Double)colDE) );
        }

        public static Int32 colHB(Decimal colGZ) //Hot BHA - Anomaly Indicator- 1 (Yes = 1, No = 0)
        {            
            if (colGZ <= 0) { return 1; } else { return 0; }
        }

        public static Int32 colHC( Int32 ifrVal ) //Magnetic Model Anomaly Indicator - 1 (IFR = 0, else = 1)
        {            
            if (ifrVal.Equals(2)) { return 0; } else { return 1; }
        }

        public static Int32 colHD(Decimal colHB, Decimal colHC) //Background Anomaly
        {            
            if (colHB.Equals(1) && colHC.Equals(1)) { return 2; } else if (colHB.Equals(1) && colHC.Equals(0)) { return 3; } else if (colHB.Equals(0) && colHC.Equals(1)) { return 4; } else { return 1; }
        }

        public static Int32 colHE(Decimal colHA) //Maximum Allowable Azimuth Error - Cuttoff Limit ( Above =1 (Not Good), Within limit = 0)
        {            
            if (colHA.Equals(0) || (colHA < 0)) { return 1; } else { return 0; }
        }

        public static Int32 colHG( Int32 ifrVal, Decimal colBD, Decimal colCS, Decimal colFZ, Decimal colGA, Decimal colGB ) //Reference Magnetics
        {           
                // value from database 1 = No; 2 = Yes (checking for Yes)
            if (ifrVal.Equals(2))
            { return 1; }
            else if( ( (colCS > colGB) && ifrVal.Equals(1) ) || ( (colBD.Equals(2)) && (colFZ > colGA) && ( ifrVal.Equals(1)) ) || ( ( colBD.Equals(3) ) && (colGA > colFZ) && ( ifrVal.Equals(1)) ) )
            { return 2; }
            else
            { return 1; }
        }

        public static Int32 colHH( Decimal colT ) //Inclination
        {        
            if (colT <= 5) { return 1; } else { return 2; }
        }

        public static Decimal colHQ(Decimal Bx, Decimal By, Decimal Bz, Decimal colBIVal, Decimal colBJVal, Decimal colCNVal, Decimal tsBias)  //Bx_Bias (Conditional)
        {            
            Decimal testValue = -99.0000M;
            if( (Bx - colBIVal > tsBias) || (By - colBJVal > tsBias) || (Bz - colCNVal > tsBias) || (Bx - colBIVal < ( -1 * tsBias )) || (By - colBJVal < ( -1 * tsBias )) || (Bz - colCNVal < ( -1 * tsBias )) )
            { testValue = 0.0000M; } else { testValue = 1.0000M; }
            if(testValue.Equals(1)) { return (colBIVal  - Bx); } else { return 0.0000M; }
        }        

        public static List<Double> getBzBiasConditionalList(Int32 ServiceID, Int32 WellID, Int32 WellSectionID, Int32 RunID, String dbConn)
        {
            try
            {            
                List<Double> iReply = new List<Double>();
                String selData = "SELECT [valBzBiasConditional] FROM [CorrectionInputToAnalysis] WHERE [srvID] = @srvID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    iReply.Add(Convert.ToDouble(readData.GetDecimal(0)));
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Double> getIDCorrBzBiasConditionalList(Int32 ServiceID, Int32 WellID, Int32 WellSectionID, Int32 RunID, String dbConn)
        {
            try
            {
                List<Double> iReply = new List<Double>();
                String selData = "SELECT [valBzBiasConditional] FROM [IncDipCorrInputToAnalysis] WHERE [srvID] = @srvID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    iReply.Add(Convert.ToDouble(readData.GetDecimal(0)));
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal colHR(Decimal Bx, Decimal By, Decimal Bz, Decimal colBIVal, Decimal colBJVal, Decimal colCNVal, Decimal TSBias) //By_Bias (Conditional)
        {            
                Int32 check = -99;
                if (((Bx - colBIVal) > TSBias) || ((By - colBJVal) > TSBias) || ((Bz - colCNVal) > TSBias) || ((Bx - colBIVal) < (-1 * TSBias)) || ((By - colBJVal) < (-1 * TSBias)) || ((Bz - colCNVal) < (-1 * TSBias)))
                { check = 0; } else { check = 1; }
                if (check.Equals(1)) { return (colBJVal - By); } else { return 0.0000M; }
        }
        
        public static Decimal bzBias_Conditional(Decimal Bx, Decimal By, Decimal Bz, Decimal colBIVal, Decimal colBJVal, Decimal colCNVal, Decimal TSBias) //Bz_Bias (Conditional)
        {            
                Int32 check = -99;
                if (((Bx - colBIVal) > TSBias) || ((By - colBJVal) > TSBias) || ((Bz - colCNVal) > TSBias) || ((Bx - colBIVal) < (-1 * TSBias)) || ((By - colBJVal) < (-1 * TSBias)) || ((Bz - colCNVal) < (-1 * TSBias)))
                { check = 0; } else { check = 1; }
                if (check.Equals(1)) { return (colCNVal - Bz); } else { return 0.0000M; }
        }

        public static Decimal colHS(Decimal Bx, Decimal By, Decimal Bz, Decimal colBIVal, Decimal colBJVal, Decimal colCNVal, Decimal colCSVal, Decimal colCPVal, Decimal TSBias)
        {            
            if( ( ( Bx - colBIVal ) > TSBias ) || ( ( By - colBJVal ) > TSBias ) || ( ( Bz - colCNVal ) > TSBias ) || ( ( Bx - colBIVal ) < (-1 * TSBias) ) || ( ( By - colBJVal ) < (-1 * TSBias) ) || ( ( Bz - colCNVal ) < (-1 * TSBias) ) )
            { return 0.0000M; } else { return colCPVal; }
        }

        public static Decimal colHT(Decimal Bx, Decimal By, Decimal Bz, Decimal colBIVal, Decimal colBJVal, Decimal colCNVal, Decimal colCSVal, Decimal TSBias)
        {
            if((Bx - colBIVal > TSBias) || (By - colBJVal > TSBias) || (Bz - colCNVal > TSBias) || (Bx - colBIVal < (-1 * TSBias)) || (By - colBJVal < (-1 * TSBias)) || (Bz - colCNVal < (-1 * TSBias)))
            { return 0.0000M; } else { return colCSVal; }
        }

        private static Int32 getNGZ(Decimal colTVal, Decimal colYVal, Int32 ngzInc, Int32 ngzAzm)
        {
            if( (colTVal >= ngzInc) && ( ((colYVal >= (90 - ngzAzm)) && ( colYVal <= (90 + ngzAzm) )) || ( (colYVal >= (270 - ngzAzm) ) && ( colYVal <= ( 270 + ngzAzm) ) ) ) )
            { return 1; } else { return 0; }
        }

        private static Decimal colHX(Decimal Bx, Decimal colBIVal)
        {
            return Decimal.Subtract(colBIVal, Bx);
        }

        private static Decimal colHY(Decimal By, Decimal colBJVal)
        {
            return Decimal.Subtract(colBJVal, By);            
        }

        private static Decimal colHZ(Decimal colHXVal, Decimal colHYVal)
        {            
            return Convert.ToDecimal(Math.Sqrt((Double)Decimal.Add((Decimal)Math.Pow((Double)colHXVal, 2), (Decimal)Math.Pow((Double)colHYVal, 2))));
        }

        private static Decimal colIA(Int32 srvyID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Decimal colHZVal, List<Decimal> colHZList, Int32 BoxyAvgError, String dbConnection)
        {            
            Decimal avgHZ = -99.00M;
            if (srvyID.Equals(1) || srvyID.Equals(2)) { return colHZVal; } 
            else { colHZList.Add(colHZVal); avgHZ = colHZList.Average(); if (colHZVal > (BoxyAvgError * avgHZ)) { return 0.0000M; } else { return colHZVal; } }
        }

        private static Decimal colIB(Decimal colIAVal)
        {
            if (colIAVal > 0.00M) { return 1.00M; } else { return 0.00M; }
        }

        public static Decimal colID(Decimal MagneticAzimuth)
        {
            if( ( MagneticAzimuth > 0.00M )  && ( MagneticAzimuth < 90.00M ) )
            {   return -1.00M; }
            else if( ( MagneticAzimuth > 90.0001M ) && ( MagneticAzimuth < 180.00M ) )
            { return 1.00M; }
            else if( ( MagneticAzimuth > 180.0001M ) && ( MagneticAzimuth < 270.00M ) ) 
            { return -1.00M; }
            else
            {   return 1.00M; }
        }

        public static Int32 colIF(Decimal sqcGTotal, Decimal DeltaGTotal)
        {
            if( DeltaGTotal > sqcGTotal )
            { return 1; }
            else if (DeltaGTotal < (-1 * sqcGTotal))
            { return 1 ; }
            else 
            { return 0; }
        }

        public static Int32 colIG(Decimal sqcBTotal, Decimal DeltaBTotal)
        {
            if( DeltaBTotal > sqcBTotal )
            { return 1; }
            else if (DeltaBTotal < ( -1 * sqcBTotal ) )
            { return 1; }
            else 
            { return 0; }
        }

        public static Int32 colIH(Decimal sqcDip, Decimal DeltaDip)
        {
            if( DeltaDip > sqcDip )
            { return 1; }
            else if (DeltaDip < (-1 * sqcDip))
            { return 1; }
            else 
            { return 0; }
        }

        public static Int32 colII(Decimal DeltaAzimuthError, Decimal NonMagSpacAllowableError)
        {
            if( DeltaAzimuthError > NonMagSpacAllowableError )
            { return 1; }
            else if (DeltaAzimuthError < ( -1 * NonMagSpacAllowableError ) )
            { return 1; }
            else 
            { return 0; }
        }

        public static Int32 colIJ(Decimal DeltaAzimuthBHAStandardPoleStrength, Decimal NonMagSpacAllowableError)
        {
            if( DeltaAzimuthBHAStandardPoleStrength > NonMagSpacAllowableError )
            { return 1; }
            else if (DeltaAzimuthBHAStandardPoleStrength < (-1 * NonMagSpacAllowableError))
            { return 1; }
            else 
            { return 0; }
        }

        public static Decimal colIK(Int32 colIFVal, Decimal GTotal)
        {
            if( colIFVal.Equals(1) ) { return GTotal; } else { return -1; }
        }

        public static Decimal colIL(Int32 colIGVal, Decimal BTotal)
        {
            if (colIGVal.Equals(1)) { return BTotal; } else { return -1; }
        }

        public static Decimal colIM(Int32 colIHVal, Decimal Dip)
        {
            if (colIHVal.Equals(1)) { return Dip; } else { return -1; }
        }

        public static Decimal colIN(Int32 colIIVal, Decimal Azimuth)
        {
            if (colIIVal.Equals(1)) { return Azimuth; } else { return -1; }
        }

        public static Decimal colIO(Int32 colIJVal, Decimal Azimuth)
        {
            if (colIJVal.Equals(1)) { return Azimuth; } else { return -1; }
        }

        public static Decimal ThreeSigmaBValue(List<Decimal> colGBVal)
        {
            return Decimal.Add( Convert.ToDecimal(colGBVal.Max()) , 100.00M );
        }

        public static Decimal biasCheck(Int32 ServiceID, Int32 srvyID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Decimal colGBVal, List<Decimal> ColGBValues, String dbConnection)
        {            
            if (srvyID.Equals(1)) { return ( colGBVal + 100.00M ); } else { return ( ColGBValues.Max() + 100.00M ); }
        }

        public static Int32 getTA(Int32 TARefMag, Int32 TAInclination, Int32 TAGTotal, Int32 TABTotal, Int32 TADip, Int32 TASCAZ1, Int32 TANMSpace, Int32 TABz)
        {            
            Int32 TAResult = -99;
            String getTA = "SELECT [taID] FROM [dmgQCTrendAnalysis] WHERE [tarmID] = @tarmID AND [taiID] = @taiID AND [tagtID] = @tagtID AND [tabtID] = @tabtID AND [tadipID] = @tadipID AND [tascazID] = @tascazID AND [tanmspID] = @tanmspID AND [tabzID] = @tabzID";
            using (SqlConnection clntConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
            {
                try
                {
                    clntConn.Open();
                    SqlCommand taStat = new SqlCommand(getTA, clntConn);
                    taStat.Parameters.AddWithValue("@tarmID", SqlDbType.Int).Value = TARefMag.ToString();
                    taStat.Parameters.AddWithValue("@taiID", SqlDbType.Int).Value = TAInclination.ToString();
                    taStat.Parameters.AddWithValue("@tagtID", SqlDbType.Int).Value = TAGTotal.ToString();
                    taStat.Parameters.AddWithValue("@tabtID", SqlDbType.Int).Value = TABTotal.ToString();
                    taStat.Parameters.AddWithValue("@tadipID", SqlDbType.Int).Value = TADip.ToString();
                    taStat.Parameters.AddWithValue("@tascazID", SqlDbType.Int).Value = TASCAZ1.ToString();
                    taStat.Parameters.AddWithValue("@tanmspID", SqlDbType.Int).Value = TANMSpace.ToString();
                    taStat.Parameters.AddWithValue("@tabzID", SqlDbType.Int).Value = TABz.ToString();
                    using (SqlDataReader readTA = taStat.ExecuteReader())
                    {
                        try
                        {
                            if (readTA.HasRows)
                            {
                                while (readTA.Read())
                                {
                                    TAResult = readTA.GetInt32(0);
                                }
                            }
                        }
                        catch (Exception ex) { throw new System.Exception(ex.ToString()); }
                        finally { readTA.Close(); readTA.Dispose(); }
                    }
                }
                catch (Exception ex) { throw new System.Exception(ex.ToString()); }
                finally { clntConn.Close(); clntConn.Dispose(); }
            }
            return TAResult;
        }

        public static Decimal calConvRunTargetAzimuth(Decimal RunTargetAzimuth, Int32 LatitudeNS, Decimal MagneticDeclination, Int32 MDecEW, Decimal GridConvergence, Int32 GCOrientation)
        {
            Decimal testValue = -99.0000M;
            if(LatitudeNS.Equals(1) && MDecEW.Equals(9) && GCOrientation.Equals(1))
            {
                testValue = RunTargetAzimuth - MagneticDeclination + GridConvergence;
            }
            else if (LatitudeNS.Equals(1) && MDecEW.Equals(9) && GCOrientation.Equals(2))
            {
                testValue = RunTargetAzimuth - MagneticDeclination - GridConvergence;
            }
            else if (LatitudeNS.Equals(1) && MDecEW.Equals(25) && GCOrientation.Equals(1))
            {
                testValue = RunTargetAzimuth + MagneticDeclination + GridConvergence;
            }
            else if (LatitudeNS.Equals(1) && MDecEW.Equals(25) && GCOrientation.Equals(2))
            {
                testValue = RunTargetAzimuth + MagneticDeclination - GridConvergence;
            }
            else if (LatitudeNS.Equals(17) && MDecEW.Equals(9) && GCOrientation.Equals(1))
            {
                testValue = RunTargetAzimuth - MagneticDeclination - GridConvergence;
            }
            else if (LatitudeNS.Equals(17) && MDecEW.Equals(9) && GCOrientation.Equals(2))
            {
                testValue = RunTargetAzimuth - MagneticDeclination + GridConvergence;
            }
            else if (LatitudeNS.Equals(17) && MDecEW.Equals(25) && GCOrientation.Equals(1))
            {
                testValue = RunTargetAzimuth + MagneticDeclination - GridConvergence;
            }
            else if (LatitudeNS.Equals(17) && MDecEW.Equals(25) && GCOrientation.Equals(2))
            {
                testValue = RunTargetAzimuth + MagneticDeclination + GridConvergence;
            }
            else 
            {
                testValue = RunTargetAzimuth;
            }                
            if (testValue > 359.99M) { return testValue - 360.00M; } else if (testValue < 0.00M) { return testValue + 360.00M; } else { return testValue; }
        }

        public static Decimal calConvRunTargetAzimuthIFR(Decimal RunTargetAzimuth, Int32 LatitudeNS, Decimal iMagneticDeclination, Int32 iMDecEW, Decimal GridConvergence, Int32 GCOrientation)
        {
            Decimal testValue = -99.0000M;
            if (LatitudeNS.Equals(1) && iMDecEW.Equals(9) && GCOrientation.Equals(1))
            {
                testValue = (RunTargetAzimuth - iMagneticDeclination + GridConvergence);
            }
            else if (LatitudeNS.Equals(1) && iMDecEW.Equals(9) && GCOrientation.Equals(2))
            {
                testValue = (RunTargetAzimuth - iMagneticDeclination - GridConvergence);
            }
            else if (LatitudeNS.Equals(1) && iMDecEW.Equals(25) && GCOrientation.Equals(1))
            {
                testValue = (RunTargetAzimuth + iMagneticDeclination + GridConvergence);
            }
            else if (LatitudeNS.Equals(1) && iMDecEW.Equals(25) && GCOrientation.Equals(2))
            {
                testValue = (RunTargetAzimuth + iMagneticDeclination - GridConvergence);
            }
            else if (LatitudeNS.Equals(17) && iMDecEW.Equals(9) && GCOrientation.Equals(2))
            {
                testValue = (RunTargetAzimuth - iMagneticDeclination + GridConvergence);
            }
            else if (LatitudeNS.Equals(17) && iMDecEW.Equals(9) && GCOrientation.Equals(1))
            {
                testValue = (RunTargetAzimuth - iMagneticDeclination - GridConvergence);
            }
            else if (LatitudeNS.Equals(17) && iMDecEW.Equals(25) && GCOrientation.Equals(2))
            {
                testValue = (RunTargetAzimuth + iMagneticDeclination + GridConvergence);
            }
            else if (LatitudeNS.Equals(17) && iMDecEW.Equals(25) && GCOrientation.Equals(1))
            {
                testValue = (RunTargetAzimuth + iMagneticDeclination - GridConvergence);
            }                
            else
            {
                testValue = RunTargetAzimuth;
            }
            //determine Converted Target Azimuth
            if (testValue > 359.99M) { return testValue - 360.00M; } else if (testValue < 0.00M) { return testValue + 360.00M; } else { return testValue; }
        }

        public static Decimal calProposedInclination(Decimal runInclination, Decimal RunTargetInclination)
        {            
            if (RunTargetInclination.Equals(null)) { return runInclination; } else { return RunTargetInclination; }
        }

        public static Decimal calConvProposedAzimuth(Int32 ifrVal, Int32 LatitudeNS, Decimal MagneticDeclination, Decimal iMagneticDeclination, Int32 MagDecEW, Int32 iMagDecEW, Decimal GridConvergence, Int32 GConvgEW, Decimal ProposedEndRunAzimuth)
        {            
            Decimal testValue = -99.0000M;
            //testValue calculation
            if (ifrVal.Equals(1))
            {
                if ((LatitudeNS.Equals(1) && MagDecEW.Equals(9) && GConvgEW.Equals(1)))
                {
                    testValue = ProposedEndRunAzimuth - MagneticDeclination + GridConvergence;
                }
                else if ((LatitudeNS.Equals(1) && MagDecEW.Equals(9) && GConvgEW.Equals(2)))
                {
                    testValue = ProposedEndRunAzimuth - MagneticDeclination - GridConvergence;
                }
                else if ((LatitudeNS.Equals(1) && MagDecEW.Equals(25) && GConvgEW.Equals(1)))
                {
                    testValue = ProposedEndRunAzimuth + MagneticDeclination + GridConvergence;
                }
                else if ((LatitudeNS.Equals(1) && MagDecEW.Equals(25) && GConvgEW.Equals(2)))
                {
                    testValue = ProposedEndRunAzimuth + MagneticDeclination - GridConvergence;
                }
                else if ((LatitudeNS.Equals(17) && MagDecEW.Equals(9) && GConvgEW.Equals(1)))
                {
                    testValue = ProposedEndRunAzimuth - MagneticDeclination - GridConvergence;
                }
                else if ((LatitudeNS.Equals(17) && MagDecEW.Equals(9) && GConvgEW.Equals(2)))
                {
                    testValue = ProposedEndRunAzimuth - MagneticDeclination + GridConvergence;
                }
                else if ((LatitudeNS.Equals(17) && MagDecEW.Equals(25) && GConvgEW.Equals(1)))
                {
                    testValue = ProposedEndRunAzimuth + MagneticDeclination - GridConvergence;
                }
                else if ((LatitudeNS.Equals(17) && MagDecEW.Equals(25) && GConvgEW.Equals(2)))
                {
                    testValue = ProposedEndRunAzimuth + MagneticDeclination + GridConvergence;
                }
                else
                {
                    testValue = ProposedEndRunAzimuth;
                }
            }
            else if (ifrVal.Equals(2))
            {
                if(LatitudeNS.Equals(1) && iMagDecEW.Equals(9) && GConvgEW.Equals(1))
                {
                    testValue = ProposedEndRunAzimuth -iMagneticDeclination+GridConvergence;
                }
                else if(LatitudeNS.Equals(1) && iMagDecEW.Equals(9) && GConvgEW.Equals(2))
                {
                    testValue = ProposedEndRunAzimuth -iMagneticDeclination-GridConvergence;
                }
                else if (LatitudeNS.Equals(1) && iMagDecEW.Equals(25) && GConvgEW.Equals(1))
                {
                    testValue = ProposedEndRunAzimuth +iMagneticDeclination+GridConvergence;
                }
                else if (LatitudeNS.Equals(1) && iMagDecEW.Equals(25) && GConvgEW.Equals(2))
                {
                    testValue = ProposedEndRunAzimuth +iMagneticDeclination-GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && iMagDecEW.Equals(9) && GConvgEW.Equals(1))
                {
                    testValue = ProposedEndRunAzimuth -iMagneticDeclination-GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && iMagDecEW.Equals(9) && GConvgEW.Equals(2))
                {
                    testValue = ProposedEndRunAzimuth -iMagneticDeclination+GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && iMagDecEW.Equals(25) && GConvgEW.Equals(1))
                {
                    testValue = ProposedEndRunAzimuth +iMagneticDeclination-GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && iMagDecEW.Equals(25) && GConvgEW.Equals(2))
                {
                    testValue = ProposedEndRunAzimuth +iMagneticDeclination+GridConvergence;
                }
                else
                {
                    testValue = ProposedEndRunAzimuth;
                }
            }                    
            //decision
            if (testValue > 359.99M) { return testValue - 360.00M; } else if (testValue < 0.00M) { return testValue + 360.00M; } else { return testValue; }
        }

        public static Decimal getNonMagAzimuthPlain(Int32 LatitudeNS, Int32 MDecCID, Int32 gcoID, Decimal CurrentGridAzimuth, Decimal MagneticDeclination, Decimal GridConvergence)
        {            
            Decimal testValue = -99.00M;
            //Calculating test value
            if (LatitudeNS.Equals(1) && MDecCID.Equals(9) && gcoID.Equals(1))
            {
                testValue = CurrentGridAzimuth - MagneticDeclination + GridConvergence;
            }
            else if (LatitudeNS.Equals(1) && MDecCID.Equals(9) && gcoID.Equals(2))
            {
                testValue = CurrentGridAzimuth - MagneticDeclination - GridConvergence;
            }
            else if (LatitudeNS.Equals(1) && MDecCID.Equals(25) && gcoID.Equals(1))
            {
                testValue = CurrentGridAzimuth + MagneticDeclination + GridConvergence;
            }
            else if (LatitudeNS.Equals(1) && MDecCID.Equals(25) && gcoID.Equals(2))
            {
                testValue = CurrentGridAzimuth + MagneticDeclination - GridConvergence;
            }
            else if (LatitudeNS.Equals(25) && MDecCID.Equals(9) && gcoID.Equals(1))
            {
                testValue = CurrentGridAzimuth - MagneticDeclination - GridConvergence;
            }
            else if (LatitudeNS.Equals(25) && MDecCID.Equals(9) && gcoID.Equals(2))
            {
                testValue = CurrentGridAzimuth - MagneticDeclination + GridConvergence;
            }
            else if (LatitudeNS.Equals(25) && MDecCID.Equals(25) && gcoID.Equals(1))
            {
                testValue = CurrentGridAzimuth + MagneticDeclination - GridConvergence;
            }
            else if (LatitudeNS.Equals(25) && MDecCID.Equals(25) && gcoID.Equals(2))
            {
                testValue = CurrentGridAzimuth + MagneticDeclination + GridConvergence;
            }
            else
            {
                testValue = CurrentGridAzimuth;
            }
            //Calculating value
            if (testValue > 359.99M) { return testValue - 360.00M; } else if (testValue < 0.00M) { return testValue + 360.00M; } else { return testValue; }
        }

        public static Decimal getNonMagAzimuthIFR(Int32 LatitudeNS, Int32 iMDecCID, Int32 gcoID, Decimal CurrentGridAzimuth, Decimal iMagneticDeclination, Decimal GridConvergence)
        {            
            Decimal testValue = -99.00M;
            //Calculating test value
            if (LatitudeNS.Equals(1) && iMDecCID.Equals(9) && gcoID.Equals(1))
            {
                testValue = CurrentGridAzimuth - iMagneticDeclination + GridConvergence;
            }
            else if (LatitudeNS.Equals(1) && iMDecCID.Equals(9) && gcoID.Equals(2))
            {
                testValue = CurrentGridAzimuth - iMagneticDeclination - GridConvergence;
            }
            else if (LatitudeNS.Equals(1) && iMDecCID.Equals(25) && gcoID.Equals(1))
            {
                testValue = CurrentGridAzimuth + iMagneticDeclination + GridConvergence;
            }
            else if (LatitudeNS.Equals(1) && iMDecCID.Equals(25) && gcoID.Equals(2))
            {
                testValue = CurrentGridAzimuth + iMagneticDeclination - GridConvergence;
            }
            else if (LatitudeNS.Equals(25) && iMDecCID.Equals(9) && gcoID.Equals(1))
            {
                testValue = CurrentGridAzimuth - iMagneticDeclination - GridConvergence;
            }
            else if (LatitudeNS.Equals(25) && iMDecCID.Equals(9) && gcoID.Equals(2))
            {
                testValue = CurrentGridAzimuth - iMagneticDeclination + GridConvergence;
            }
            else if (LatitudeNS.Equals(25) && iMDecCID.Equals(25) && gcoID.Equals(1))
            {
                testValue = CurrentGridAzimuth + iMagneticDeclination - GridConvergence;
            }
            else if (LatitudeNS.Equals(25) && iMDecCID.Equals(25) && gcoID.Equals(2))
            {
                testValue = CurrentGridAzimuth + iMagneticDeclination + GridConvergence;
            }
            else
            {
                testValue = CurrentGridAzimuth;
            }
            //Calculating value
            if (testValue > 359.99M) { return testValue - 360.00M; } else if (testValue < 0.00M) { return testValue + 360.00M; } else { return testValue; }          
        }

        public static ArrayList surveyQC(String UserName, String UserRealm, Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Int32 ServiceID, System.Data.DataTable SelectedValueTable, ArrayList RefMags, ArrayList SQCInfo, ArrayList RunInfo, ArrayList BHAInfo, ArrayList dflInfo, ArrayList dcpsInfo, ArrayList ngzInfo, ArrayList wellMetaInfo, List<Int32> WellInfo, Int32 boxyError, ArrayList tieInList, Int32 InputLengthUnit, Int32 InputAccelerometerUnit, Int32 InputMagnetometerUnit, Int32 DataEntryMethod, String FileName, String ClientDBConnection)
        {
            try
            {
                ArrayList iReply = new ArrayList(), depthThresholds = new ArrayList();
                Int32 qcResult = -99, acID = -99, plcResult = -99, runName = -99, jIFR = -99, jAcelU = -99, jMagU = -99, jLenU = -99, jConverg = -99, rID = -99, sqcID = -99, nextSeqPlacement = -99, runDepthUnit = -99;
                Int32 MSizeU = -99, DSCSizeU = -99, MDBU = -99, NMSpAbvU = -99, NMSpBlwU = -99, MGVU = -99, DGTU = -99, DSGU = -99, DGBTU = -99;
                Int32 ngzInc = -99, ngzAzm = -99, LatiNS = -99, LngiEW = -99, mdecCID = -99, imdecCID = -99, gcoID = -99;
                Decimal mdec = -99.00M, dip = -99.00M, bTotal = -99.000000M, gTotal = -99.000000M, gConv = -99.00M, imdec = -99.00M, idip = -99.00M, ibTotal = -99.00M, igTotal = -99.000000M, sqcDip = -99.0000M, sqcBTotal = -99.0000M, sqcGTotal = -99.0000000M;
                Decimal rInc = -99.00M, rAzm = -99.00M, rTInc = -99.00M, rTAzm = -99.00M, convRTAzm = -99.00M, convRTAzmIFR = -99.00M, convPTInc = -99.00M, convPTAzm = -99.00M, runStartDepth = -99.00M, runEndDepth = -99.00M, runConvertedStartDepth = -99.00M, runConvertedEndDepth = -99.00M;
                Decimal MSize = -99.00M, DSCSize = -99.00M, MDBLen = -99.00M, NMSpAbv = -99.00M, NMSpBlw = -99.00M, NMSpAlw = -99.00M, MGValue = -99.000000M, DGTop = -99.00M, DSGValue = -99.000000M, DGBtm = -99.00M;                
                Decimal dcpsLimit = -99.00M, dflLowBT = -99.00M, dflLowDip = -99.00M, dflLowGT = -99.00M, dflUprBT = -99.00M, dflUprDip = -99.00M, dflUprGT = -99.00M;
                Decimal lcsRef1 = 0.2500M, lcsRef2 = 0.2500M, lcsRef3 = 0.5600M, lcsRef4 = 0.5600M, BxBiasCheck = -99.00M, ByBiasCheck = -99.00M, BzBiasCheck = -99.00M, TSBias = -99.00M; //Toolstring Bias
                Int32 WellDepthFlag = -99, RunDepthFlag = -99, RunDepthUnit = -99; Decimal WellPlanTargetDepth = -99.00M;
                Decimal depthLimit = -99.00M, depthMargin = -99.00M;            
                List<Int32> srvyRowIDList = new List<Int32>();
                List<Double> srvyIDValues = new List<Double>(), hsValues = new List<Double>(), hqValues = new List<Double>(), hrValues = new List<Double>(), htValues = new List<Double>();
                List<Double> bxValues = new List<Double>(), byValues = new List<Double>();
                List<Double> biValues = new List<Double>(), bjValues = new List<Double>();
                List<Decimal> bxList = new List<Decimal>(), byList = new List<Decimal>(), bzList = new List<Decimal>(), refBxList = new List<Decimal>(), refByList = new List<Decimal>(), gbList = new List<Decimal>();
                List<Decimal> baList = new List<Decimal>(), caList = new List<Decimal>(), cfList = new List<Decimal>(), coList = new List<Decimal>(), csList = new List<Decimal>(), hsList = new List<Decimal>(), htList = new List<Decimal>(), hzList = new List<Decimal>();
                //Well Plan input table
                System.Data.DataTable rawPlanTable = new System.Data.DataTable(), scPlanTable = new System.Data.DataTable(), bhaPlanTable = new System.Data.DataTable(), crrPlanTable = new System.Data.DataTable();
                DataColumn rwplnDataRowID = rawPlanTable.Columns.Add("datarowID", typeof(Int32));
                DataColumn rwplnMD = rawPlanTable.Columns.Add("md", typeof(Decimal));
                DataColumn rwplnInc = rawPlanTable.Columns.Add("inc", typeof(Decimal));
                DataColumn rwplnAzm = rawPlanTable.Columns.Add("azm", typeof(Decimal));
                DataColumn scplnDataRowID = scPlanTable.Columns.Add("datarowID", typeof(Int32));
                DataColumn scplnMD = scPlanTable.Columns.Add("md", typeof(Decimal));
                DataColumn scplnInc = scPlanTable.Columns.Add("inc", typeof(Decimal));
                DataColumn scplnAzm = scPlanTable.Columns.Add("azm", typeof(Decimal));
                DataColumn baplnDataRowID = bhaPlanTable.Columns.Add("datarowID", typeof(Int32));
                DataColumn baplnMD = bhaPlanTable.Columns.Add("md", typeof(Decimal));
                DataColumn baplnInc = bhaPlanTable.Columns.Add("inc", typeof(Decimal));
                DataColumn baplnAzm = bhaPlanTable.Columns.Add("azm", typeof(Decimal));
                DataColumn crplnDataRowID = crrPlanTable.Columns.Add("datarowID", typeof(Int32));
                DataColumn crplnMD = crrPlanTable.Columns.Add("md", typeof(Decimal));
                DataColumn crplnInc = crrPlanTable.Columns.Add("inc", typeof(Decimal));
                DataColumn crplnAzm = crrPlanTable.Columns.Add("azm", typeof(Decimal));
                
                rID = RunID;
                jLenU = 5;
                //Well Info                
                LngiEW = WellInfo[0];
                LatiNS = WellInfo[1];
                //Ref. Mags.
                jIFR = Convert.ToInt32(RefMags[0]);
                mdec = Convert.ToDecimal(RefMags[1]);
                imdec = Convert.ToDecimal(RefMags[1]);
                mdecCID = Convert.ToInt32(RefMags[2]);
                dip = Convert.ToDecimal(RefMags[3]);
                idip = Convert.ToDecimal(RefMags[3]);
                bTotal = Convert.ToDecimal(RefMags[4]);
                ibTotal = Convert.ToDecimal(RefMags[4]);
                jMagU = Convert.ToInt32(RefMags[5]);
                gTotal = Convert.ToDecimal(RefMags[6]);
                igTotal = Convert.ToDecimal(RefMags[6]);
                jAcelU = Convert.ToInt32(RefMags[7]);
                gConv = Convert.ToDecimal(RefMags[8]);
                gcoID = Convert.ToInt32(RefMags[9]);
                jConverg = Convert.ToInt32(RefMags[10]);
                //Run Info                
                runName = Convert.ToInt32(RunInfo[0]);
                rInc = Convert.ToDecimal(RunInfo[1]);
                rAzm = Convert.ToDecimal(RunInfo[2]);
                rTInc = Convert.ToDecimal(RunInfo[3]);
                rTAzm = Convert.ToDecimal(RunInfo[4]);
                sqcID = Convert.ToInt32(RunInfo[5]);
                runStartDepth = Convert.ToDecimal(RunInfo[6]);
                runEndDepth = Convert.ToDecimal(RunInfo[7]);
                runDepthUnit = Convert.ToInt32(RunInfo[8]);
                if (runDepthUnit.Equals(4))
                {
                    runConvertedStartDepth = Units.convMDLen(runStartDepth, runDepthUnit);
                    runConvertedEndDepth = Units.convMDLen(runEndDepth, runDepthUnit);
                }
                //SQC Info                
                sqcDip = Convert.ToDecimal(SQCInfo[0]);
                sqcBTotal = Convert.ToDecimal(SQCInfo[1]);
                sqcGTotal = Convert.ToDecimal(SQCInfo[2]);
                //Run Info
                convPTInc = calConvProposedInclination(rInc, rTInc);
                convRTAzm = calConvRunTargetAzimuth(rAzm, LatiNS, mdec, mdecCID, gConv, gcoID);
                convRTAzmIFR = calConvRunTargetAzimuthIFR(rAzm, LatiNS, imdec, imdecCID, gConv, gcoID);
                convPTAzm = calConvProposedAzimuth(jIFR, LatiNS, mdec, imdec, mdecCID, imdecCID, gConv, gcoID, rTAzm);
                //BHA Info
                MSize = Convert.ToDecimal(BHAInfo[0]);
                MSizeU = Convert.ToInt32(BHAInfo[1]);
                DSCSize = Convert.ToDecimal(BHAInfo[2]);
                DSCSizeU = Convert.ToInt32(BHAInfo[3]);
                MDBLen = Convert.ToDecimal(BHAInfo[4]);
                MDBU = Convert.ToInt32(BHAInfo[5]);
                NMSpAbv = Convert.ToDecimal(BHAInfo[6]);
                NMSpAbvU = Convert.ToInt32(BHAInfo[7]);
                NMSpBlw = Convert.ToDecimal(BHAInfo[8]);
                NMSpBlwU = Convert.ToInt32(BHAInfo[9]);
                NMSpAlw = OPR.getAllowableSpace(convPTInc, convPTAzm);
                MGValue = Convert.ToDecimal(BHAInfo[11]);
                MGVU = Convert.ToInt32(BHAInfo[12]);
                DGTop = Convert.ToDecimal(BHAInfo[13]);
                DGTU = Convert.ToInt32(BHAInfo[14]);
                DSGValue = Convert.ToDecimal(BHAInfo[15]);
                DSGU = Convert.ToInt32(BHAInfo[16]);
                DGBtm = Convert.ToDecimal(BHAInfo[17]);
                DGBTU = Convert.ToInt32(BHAInfo[18]);
                //DFL Info                
                dflLowBT = Convert.ToDecimal(dflInfo[0]);
                dflLowDip = Convert.ToDecimal(dflInfo[1]);
                dflLowGT = Convert.ToDecimal(dflInfo[2]);
                dflUprBT = Convert.ToDecimal(dflInfo[3]);
                dflUprDip = Convert.ToDecimal(dflInfo[4]);
                dflUprGT = Convert.ToDecimal(dflInfo[5]);
                //DCPS Info                
                dcpsLimit = Convert.ToDecimal(dcpsInfo[0]);
                //NGZ info                
                ngzInc = Convert.ToInt32(ngzInfo[0]);
                ngzAzm = Convert.ToInt32(ngzInfo[1]);
                //Calculation only variables
                String srDate = String.Empty, srTime = String.Empty;
                Decimal DepthCurr = -99.00M, DepthPrev = -99.00M;
                Int32 nextSeq = -99, chkShot = -99, PlacementDataFileID = -99, PlacementTieInID = -99;
                Decimal depth = -99.00M, Gx = -99.000000000M, Gy = -99.000000000M, Gz = -99.000000000M, Bx = -99.000000000M, By = -99.000000000M, Bz = -99.000000000M;
                Decimal cnvDip = -99.00M, cnvBT = -99.00M, cnvGT = -99.000000000M, cnvIFRGT = -99.00000000M, cnvIFRDip = -99.000000M, cnvIFRBT = -99.000000M;
                Decimal cnvMSize = -99.00M, cnvSQCBTotal = -99.000000M, cnvSQCGTotal = -99.000000000M, cnvNMSpAbv = -99.00M, cnvNMSpBlw = -99.00M, cnvMDBLen = -99.00M;
                Decimal cnvMGValue = -99.00000000M, cnvDSSize = -99.00M, cnvDSGValue = -99.00000000M, cnvDGTop = -99.00M, cnvDGBtm = -99.00M;                
                Int32 colVVal = -99, colAVVal = -99, colAWVal = -99, colAXVal = -99, colAYVal = -99, colBDVal = -99, colCCVal = -99, colCIVal = -99, colDFVal = -99, colDGVal = -99;
                Int32 colDHVal = -99, colDJVal = -99, colDWVal = -99, colDYVal = -99, colEAVal = -99, colEGVal = -99, colEKVal = -99, colEOVal = -99, colETVal = -99, colEXVal = -99;
                Int32 colEYVal = -99, colEZVal = -99, colFWVal = -99, colFXVal = -99, colGFVal = -99, colGLVal = -99, colHBVal = -99, colHCVal = -99, colHDVal = -99, colHEVal = -99;
                Int32 colHGVal = -99, colHHVal = -99, taID = -99, colHVVal = -99;
                Decimal gtfVal = -99.000000000000M, colQVal = -99.000000000000M, colSVal = -99.000000000000M, colTVal = -99.000000000000M, colWVal = -99.000000000000M;
                Decimal colYVal = -99.000000000000M, colZVal = -99.000000000000M, colAAVal = -99.000000000000M, colABVal = -99.000000000000M, colACVal = -99.000000000000M;
                Decimal colADVal = -99.000000000000M, colAEVal = -99.000000000000M, colAFVal = -99.000000000000M, colAHVal = -99.000000000000M, colAIVal = -99.000000000000M;
                Decimal colAJVal = -99.000000000000M, colAKVal = -99.000000000000M, colALVal = -99.000000000000M, colAMVal = -99.000000000000M, colANVal = -99.000000000000M;
                Decimal colAOVal = -99.000000000000M, colAPVal = -99.000000000000M, colARVal = -99.000000000000M, colASVal = -99.000000000000M, colATVal = -99.000000000000M;
                Decimal colAUVal = -99.000000000000M, colBAVal = -99.000000000000M, colBBVal = -99.000000000000M, colBCVal = -99.000000000000M, colBFVal = -99.000000000000M;
                Decimal colBGVal = -99.000000000000M, colBHVal = -99.000000000000M, colBIVal = -99.000000000000M, colBJVal = -99.000000000000M, colBKVal = -99.000000000000M;
                Decimal colBMVal = -99.000000000000M, colBNVal = -99.000000000000M, colBOVal = -99.000000000000M, colBPVal = -99.000000000000M, colBQVal = -99.000000000000M;
                Decimal colBRVal = -99.000000000000M, colBSVal = -99.000000000000M, colBTVal = -99.000000000000M, colBUVal = -99.000000000000M, colBVVal = -99.000000000000M;
                Decimal colBWVal = -99.000000000000M, colBXVal = -99.000000000000M, colBZVal = -99.000000000000M, colCAVal = -99.000000000000M, colCBVal = -99.000000000000M;
                Decimal colCDVal = -99.000000000000M, colCEVal = -99.000000000000M, colCFVal = -99.000000000000M, colCGVal = -99.000000000000M, colCHVal = -99.000000000000M;
                Decimal colCKVal = -99.000000000000M, colCLVal = -99.000000000000M, colCMVal = -99.000000000000M, colCNVal = -99.000000000000M, colCOVal = -99.000000000000M;
                Decimal colCPVal = -99.000000000000M, colCQVal = -99.000000000000M, colCRVal = -99.000000000000M, colCSVal = -99.000000000000M, colCTVal = -99.000000000000M;
                Decimal colCUVal = -99.000000000000M, colCVVal = -99.000000000000M, colCWVal = -99.000000000000M, colCXVal = -99.000000000000M, colCYVal = -99.000000000000M;
                Decimal colCZVal = -99.000000000000M, colDAVal = -99.000000000000M, colDBVal = -99.000000000000M, colDCVal = -99.000000000000M, colDDVal = -99.000000000000M;
                Decimal colDEVal = -99.000000000000M, colDIVal = -99.000000000000M, colDKVal = -99.000000000000M, colDRVal = -99.000000000000M, colEIVal = -99.000000000000M;
                Decimal colEJVal = -99.000000000000M, colELVal = -99.000000000000M, colEPVal = -99.000000000000M, colEQVal = -99.000000000000M, colERVal = -99.000000000000M;
                Decimal colEUVal = -99.000000000000M, colEVVal = -99.000000000000M, colEWVal = -99.000000000000M, colFAVal = -99.000000000000M, colFBVal = -99.000000000000M;
                Decimal colFCVal = -99.000000000000M, colFYVal = -99.000000000000M, colFZVal = -99.000000000000M, colGAVal = -99.000000000000M, colGBVal = -99.000000000000M;
                Decimal colGCVal = -99.000000000000M, colGDVal = -99.000000000000M, colGEVal = -99.000000000000M, colGHVal = -99.000000000000M, colGIVal = -99.000000000000M;
                Decimal colGJVal = -99.000000000000M, colGKVal = -99.000000000000M, colGMVal = -99.000000000000M, colGNVal = -99.000000000000M, colGOVal = -99.000000000000M;
                Decimal colGPVal = -99.000000000000M, colGRVal = -99.000000000000M, colGSVal = -99.000000000000M, colGTVal = -99.000000000000M, colGUVal = -99.000000000000M;
                Decimal colGVVal = -99.000000000000M, colGWVal = -99.000000000000M, colGXVal = -99.000000000000M, colGYVal = -99.000000000000M, colGZVal = -99.000000000000M;
                Decimal colHAVal = -99.000000000000M, colHQVal = -99.000000000000M, colHRVal = -99.000000000000M, colHSVal = -99.000000000000M, colHTVal = -99.000000000000M;
                Decimal colHXVal = -99.0000M, colHYVal = -99.0000M, colHZVal = -99.0000M, colIAVal = -99.0000M, colIBVal = -99.0000M, nmAzmPlain = -99.00M, nmAzmIfr = -99.00M, deltaDepth = -99.0000M;
                Decimal colIDVal = -99.00M, colIKVal = -99.00M, colILVal = -99.00M, colIMVal = -99.00M, colINVal = -99.00M, colIOVal = -99.00M;
                Int32 colIFVal = -99, colIGVal = -99, colIHVal = -99, colIIVal = -99, colIJVal = -99;
                //Well Plan parameters                
                Int32 LengthUnit = -99;
                Decimal VertSecAzimuth = -99.00M, ReferenceElevation = -99.00M;
                Decimal tiMD = 0.00M, tiInc = 0.00M, tiAzm = 0.00M, tiTVD = 0.00M, tiNorth = 0.00M, tiEast = 0.00M, tiVS = 0.00M, tiSubSea = 0.00M;
                Int32 timdUnit = -99, titvdUnit = -99, tinUnit = -99, tieUnit = -99, tivsUnit = -99, tissUnit = -99;
                Decimal rw_nVal = 0.00M, rw_eVal = 0.00M, rw_nValTI = 0.00M, rw_eValTI = 0.00M;
                Decimal rw_tvdVal = -99.000000000000M, rw_vsVal = -99.000000000000M, rw_subseaVal = -99.000000000000M, rw_tvdValTI = -99.000000000000M, rw_vsValTI = -99.000000000000M, rw_subseaValTI = -99.000000000000M;
                Decimal rw_northingVal = -99.000000000000M, rw_eastingVal = -99.000000000000M, rw_northingValTI = -99.000000000000M, rw_eastingValTI = -99.000000000000M;
                Decimal rw_deltaMDVal = -99.000000000000M, rw_d1Val = -99.000000000000M, rw_dogleg1Val = -99.000000000000M, rw_rfVal = -99.000000000000M, rw_deltaTVDVal = -99.000000000000M, rw_deltaMDValTI = -99.000000000000M, rw_d1ValTI = -99.000000000000M, rw_dogleg1ValTI = -99.000000000000M, rw_rfValTI = -99.000000000000M, rw_deltaTVDValTI = -99.000000000000M;
                Decimal rw_cumDeltaNorthingVal = -99.000000000000M, rw_deltaNVal = -99.000000000000M, rw_cumDeltaEastingVal = -99.000000000000M, rw_deltaEVal = -99.000000000000M, rw_cumDeltaNorthingValTI = -99.000000000000M, rw_deltaNValTI = -99.000000000000M, rw_cumDeltaEastingValTI = -99.000000000000M, rw_deltaEValTI = -99.000000000000M;
                Decimal rw_deltaVSVal = -99.000000000000M, rw_clDistVal = -99.000000000000M, rw_clDirectAngleVal1 = -99.000000000000M, rw_clDirectAngleVal2 = -99.000000000000M, rw_deltaVSValTI = -99.000000000000M, rw_clDistValTI = -99.000000000000M, rw_clDirectAngleVal1TI = -99.000000000000M, rw_clDirectAngleVal2TI = -99.000000000000M;
                Decimal rw_dogLegVal = -99.000000000000M, rw_dls100Val = -99.000000000000M, rw_dogLegValTI = -99.000000000000M, rw_dls100ValTI = -99.000000000000M;
                Decimal rw_dataMD = -99.00M, rw_dataMDTI = -99.00M;
                Decimal rw_PreviousDepth = 0.00M, rw_PreviousTVD = 0.00M, rw_PreviousInclination = 0.000000000000M, rw_PreviousAzimuth = 0.000000000000M, rw_PreviousCumDeltaNorth = 0.000000000000M, rw_PreviousCumDeltaEast = 0.000000000000M, rw_PreviousDepthTI = 0.00M, rw_PreviousTVDTI = 0.00M, rw_PreviousInclinationTI = 0.000000000000M, rw_PreviousAzimuthTI = 0.000000000000M, rw_PreviousCumDeltaNorthTI = 0.000000000000M, rw_PreviousCumDeltaEastTI = 0.000000000000M;
                Decimal rw_PreviousVerticalSection = 0.000000000000M, rw_PreviousNorthing = 0.000000000000M, rw_PreviousEasting = 0.000000000000M, rw_PreviousVerticalSectionTI = 0.000000000000M, rw_PreviousNorthingTI = 0.000000000000M, rw_PreviousEastingTI = 0.000000000000M;
                Decimal sc_nVal = 0.00M, sc_eVal = 0.00M, sc_nValTI = 0.00M, sc_eValTI = 0.00M;
                Decimal sc_tvdVal = -99.000000000000M, sc_vsVal = -99.000000000000M, sc_subseaVal = -99.000000000000M, sc_tvdValTI = -99.000000000000M, sc_vsValTI = -99.000000000000M, sc_subseaValTI = -99.000000000000M;
                Decimal sc_northingVal = -99.000000000000M, sc_eastingVal = -99.000000000000M, sc_northingValTI = -99.000000000000M, sc_eastingValTI = -99.000000000000M;
                Decimal sc_deltaMDVal = -99.000000000000M, sc_d1Val = -99.000000000000M, sc_dogleg1Val = -99.000000000000M, sc_rfVal = -99.000000000000M, sc_deltaTVDVal = -99.000000000000M, sc_deltaMDValTI = -99.000000000000M, sc_d1ValTI = -99.000000000000M, sc_dogleg1ValTI = -99.000000000000M, sc_rfValTI = -99.000000000000M, sc_deltaTVDValTI = -99.000000000000M;
                Decimal sc_cumDeltaNorthingVal = -99.000000000000M, sc_deltaNVal = -99.000000000000M, sc_cumDeltaEastingVal = -99.000000000000M, sc_deltaEVal = -99.000000000000M, sc_cumDeltaNorthingValTI = -99.000000000000M, sc_deltaNValTI = -99.000000000000M, sc_cumDeltaEastingValTI = -99.000000000000M, sc_deltaEValTI = -99.000000000000M;
                Decimal sc_deltaVSVal = -99.000000000000M, sc_clDistVal = -99.000000000000M, sc_clDirectAngleVal1 = -99.000000000000M, sc_clDirectAngleVal2 = -99.000000000000M, sc_deltaVSValTI = -99.000000000000M, sc_clDistValTI = -99.000000000000M, sc_clDirectAngleVal1TI = -99.000000000000M, sc_clDirectAngleVal2TI = -99.000000000000M;
                Decimal sc_dogLegVal = -99.000000000000M, sc_dls100Val = -99.000000000000M, sc_dogLegValTI = -99.000000000000M, sc_dls100ValTI = -99.000000000000M;
                Decimal sc_dataMD = -99.00M, sc_dataMDTI = -99.00M, sc_dataInc = -99.00M;
                Decimal sc_PreviousDepth = 0.00M, sc_PreviousTVD = 0.00M, sc_PreviousInclination = 0.000000000000M, sc_PreviousAzimuth = 0.000000000000M, sc_PreviousCumDeltaNorth = 0.000000000000M, sc_PreviousCumDeltaEast = 0.000000000000M, sc_PreviousDepthTI = 0.00M, sc_PreviousTVDTI = 0.00M, sc_PreviousInclinationTI = 0.000000000000M, sc_PreviousAzimuthTI = 0.000000000000M, sc_PreviousCumDeltaNorthTI = 0.000000000000M, sc_PreviousCumDeltaEastTI = 0.000000000000M;
                Decimal sc_PreviousVerticalSection = 0.000000000000M, sc_PreviousNorthing = 0.000000000000M, sc_PreviousEasting = 0.000000000000M, sc_PreviousVerticalSectionTI = 0.000000000000M, sc_PreviousNorthingTI = 0.000000000000M, sc_PreviousEastingTI = 0.000000000000M;
                Decimal bh_nVal = 0.00M, bh_eVal = 0.00M, bh_nValTI = 0.00M, bh_eValTI = 0.00M;
                Decimal bh_tvdVal = -99.000000000000M, bh_vsVal = -99.000000000000M, bh_subseaVal = -99.000000000000M, bh_tvdValTI = -99.000000000000M, bh_vsValTI = -99.000000000000M, bh_subseaValTI = -99.000000000000M;
                Decimal bh_northingVal = -99.000000000000M, bh_eastingVal = -99.000000000000M, bh_northingValTI = -99.000000000000M, bh_eastingValTI = -99.000000000000M;
                Decimal bh_deltaMDVal = -99.000000000000M, bh_d1Val = -99.000000000000M, bh_dogleg1Val = -99.000000000000M, bh_rfVal = -99.000000000000M, bh_deltaTVDVal = -99.000000000000M, bh_deltaMDValTI = -99.000000000000M, bh_d1ValTI = -99.000000000000M, bh_dogleg1ValTI = -99.000000000000M, bh_rfValTI = -99.000000000000M, bh_deltaTVDValTI = -99.000000000000M;
                Decimal bh_cumDeltaNorthingVal = -99.000000000000M, bh_deltaNVal = -99.000000000000M, bh_cumDeltaEastingVal = -99.000000000000M, bh_deltaEVal = -99.000000000000M, bh_cumDeltaNorthingValTI = -99.000000000000M, bh_deltaNValTI = -99.000000000000M, bh_cumDeltaEastingValTI = -99.000000000000M, bh_deltaEValTI = -99.000000000000M;
                Decimal bh_deltaVSVal = -99.000000000000M, bh_clDistVal = -99.000000000000M, bh_clDirectAngleVal1 = -99.000000000000M, bh_clDirectAngleVal2 = -99.000000000000M, bh_deltaVSValTI = -99.000000000000M, bh_clDistValTI = -99.000000000000M, bh_clDirectAngleVal1TI = -99.000000000000M, bh_clDirectAngleVal2TI = -99.000000000000M;
                Decimal bh_dogLegVal = -99.000000000000M, bh_dls100Val = -99.000000000000M, bh_dogLegValTI = -99.000000000000M, bh_dls100ValTI = -99.000000000000M;
                Decimal bh_dataMD = -99.00M, bh_dataMDTI = -99.00M, bh_dataInc = -99.00M;
                Decimal bh_PreviousDepth = 0.00M, bh_PreviousTVD = 0.00M, bh_PreviousInclination = 0.000000000000M, bh_PreviousAzimuth = 0.000000000000M, bh_PreviousCumDeltaNorth = 0.000000000000M, bh_PreviousCumDeltaEast = 0.000000000000M, bh_PreviousDepthTI = 0.00M, bh_PreviousTVDTI = 0.00M, bh_PreviousInclinationTI = 0.000000000000M, bh_PreviousAzimuthTI = 0.000000000000M, bh_PreviousCumDeltaNorthTI = 0.000000000000M, bh_PreviousCumDeltaEastTI = 0.000000000000M;
                Decimal bh_PreviousVerticalSection = 0.000000000000M, bh_PreviousNorthing = 0.000000000000M, bh_PreviousEasting = 0.000000000000M, bh_PreviousVerticalSectionTI = 0.000000000000M, bh_PreviousNorthingTI = 0.000000000000M, bh_PreviousEastingTI = 0.000000000000M;
                Decimal cr_nVal = 0.00M, cr_eVal = 0.00M, cr_nValTI = 0.00M, cr_eValTI = 0.00M;
                Decimal cr_tvdVal = -99.000000000000M, cr_vsVal = -99.000000000000M, cr_subseaVal = -99.000000000000M, cr_tvdValTI = -99.000000000000M, cr_vsValTI = -99.000000000000M, cr_subseaValTI = -99.000000000000M;
                Decimal cr_northingVal = -99.000000000000M, cr_eastingVal = -99.000000000000M, cr_northingValTI = -99.000000000000M, cr_eastingValTI = -99.000000000000M;
                Decimal cr_deltaMDVal = -99.000000000000M, cr_d1Val = -99.000000000000M, cr_dogleg1Val = -99.000000000000M, cr_rfVal = -99.000000000000M, cr_deltaTVDVal = -99.000000000000M, cr_deltaMDValTI = -99.000000000000M, cr_d1ValTI = -99.000000000000M, cr_dogleg1ValTI = -99.000000000000M, cr_rfValTI = -99.000000000000M, cr_deltaTVDValTI = -99.000000000000M;
                Decimal cr_cumDeltaNorthingVal = -99.000000000000M, cr_deltaNVal = -99.000000000000M, cr_cumDeltaEastingVal = -99.000000000000M, cr_deltaEVal = -99.000000000000M, cr_cumDeltaNorthingValTI = -99.000000000000M, cr_deltaNValTI = -99.000000000000M, cr_cumDeltaEastingValTI = -99.000000000000M, cr_deltaEValTI = -99.000000000000M;
                Decimal cr_deltaVSVal = -99.000000000000M, cr_clDistVal = -99.000000000000M, cr_clDirectAngleVal1 = -99.000000000000M, cr_clDirectAngleVal2 = -99.000000000000M, cr_deltaVSValTI = -99.000000000000M, cr_clDistValTI = -99.000000000000M, cr_clDirectAngleVal1TI = -99.000000000000M, cr_clDirectAngleVal2TI = -99.000000000000M;
                Decimal cr_dogLegVal = -99.000000000000M, cr_dls100Val = -99.000000000000M, cr_dogLegValTI = -99.000000000000M, cr_dls100ValTI = -99.000000000000M;
                Decimal cr_dataMD = -99.00M, cr_dataMDTI = -99.00M, cr_dataInc = -99.00M;
                Decimal cr_PreviousDepth = 0.00M, cr_PreviousTVD = 0.00M, cr_PreviousInclination = 0.000000000000M, cr_PreviousAzimuth = 0.000000000000M, cr_PreviousCumDeltaNorth = 0.000000000000M, cr_PreviousCumDeltaEast = 0.000000000000M, cr_PreviousDepthTI = 0.00M, cr_PreviousTVDTI = 0.00M, cr_PreviousInclinationTI = 0.000000000000M, cr_PreviousAzimuthTI = 0.000000000000M, cr_PreviousCumDeltaNorthTI = 0.000000000000M, cr_PreviousCumDeltaEastTI = 0.000000000000M;
                Decimal cr_PreviousVerticalSection = 0.000000000000M, cr_PreviousNorthing = 0.000000000000M, cr_PreviousEasting = 0.000000000000M, cr_PreviousVerticalSectionTI = 0.000000000000M, cr_PreviousNorthingTI = 0.000000000000M, cr_PreviousEastingTI = 0.000000000000M;
                //Calculating BHA Non-Mag Space Parameters (Calculator II etc )
                Int32 nmsMtrPoleStrength = -99, nmsDCPoleStrength = -99;
                Decimal nmsTAzmMag = -99.0000M, horzBT = -99.0000M, calcmgrPS = -99.00000000M, calcdcPS = -99.00000000M, mtrSurfacePS = -99.00000000M, dscSurfacePoleStrength = -99.00000000M;
                Decimal mtrSurfaceInterference = -99.00M, dcSurfaceInterference = -99.00M, bhaTotalInterference = -99.00M, bhaLCAZError = -99.000000000M, dcMaxGaussmeter = -99.00000000M;
                Decimal wcMPSMotor = -99.000000M, bptopDrillCollar = -99.00000000M, bpbtmMotor = -99.00000000M, bpTotalBz = -99.00000000M, maxAllowableError = -99.000000000M;
                Decimal stdDCInterference = -99.00M, stdMtrInterference = -99.00M, stdTTLBHAInterference = -99.00M, stdLCAZErrorBHA = -99.00M, currBitToSensor = -99.00M;
                Decimal gmtrDCInterference = -99.00M, gmtrMtrInterference = -99.00M, gmtrTTLBHAInterference = -99.00M, gmtrLCAZErrorBHA = -99.00M, currGridAzm = -99.00M, currMagAzm = -99.00M;
                Decimal rwSupNorthing = -99.000000000000M, rwSupEasting = -99.000000000000M, scSupNorthing = -99.000000000000M, scSupEasting = -99.000000000000M;
                Decimal bhSupNorthing = -99.000000000000M, bhSupEasting = -99.000000000000M, crSupNorthing = -99.000000000000M, crSupEasting = -99.000000000000M;
                Decimal ThreeSigmaVal = -99.00M;
                Decimal colTCurr = -99.000000000000M, colTPrev = -99.000000000000M, colYCurr = -99.0000M, colYPrev = -99.0000M, colAFCurr = -99.0000M, colBACurr = -99.0000M, colCACurr = -99.00M, colCFCurr = -99.000000M; ;
                Decimal colAFPrev = -99.000000000000M, colBAPrev = -99.000000000000M, colBCPrev = -99.000000000000M, colCAPrev = -99.000000000000M, colCDPrev = -99.000000000000M, colCFPrev = -99.000000000000M;
                Decimal colGEPrev = -99.000000000000M;

                cnvDip = Units.convDip(dip);
                cnvBT = Units.convBTotal(bTotal, jMagU);
                cnvIFRDip = Units.convDip(idip);
                cnvIFRBT = Units.convBTotal(ibTotal, jMagU);
                cnvGT = Units.convGTotal(gTotal, jAcelU);
                cnvIFRGT = Units.convGTotal(igTotal, jAcelU);
                cnvMSize = Units.convMotorSize(MSize, MSizeU);
                cnvSQCBTotal = Units.convBTotal(sqcBTotal, jMagU);
                cnvSQCGTotal = Units.convGTotal(sqcGTotal, jAcelU);
                cnvMDBLen = Units.convMDLen(MDBLen, MDBU);
                cnvNMSpAbv = Units.convNMSp(NMSpAbv, NMSpAbvU);
                cnvNMSpBlw = Units.convNMSp(NMSpBlw, NMSpBlwU);
                cnvMGValue = Units.convGMeter(MGValue, MGVU);
                cnvDSSize = Units.convDSCSize(DSCSize, DSCSizeU);
                cnvDSGValue = Units.convGMeter(DSGValue, DSGU);
                cnvDGTop = Units.convGMtrDistance(DGTop, DGTU);
                cnvDGBtm = Units.convGMtrDistance(DGBtm, DGBTU);

                foreach (DataRow dRow in SelectedValueTable.Rows)
				{
					nextSeq = getNextSequence(OperatorID, ClientID, WellPadID, WellID, WellSectionID, RunID, ClientDBConnection);
                    if (nextSeq.Equals(1))
                    {
                        acID = 4;
                    }
                    else
                    {
                        acID = getAzimuthCriteriaID(OperatorID, WellPadID, WellID, WellSectionID, RunID, (nextSeq - 1), ClientDBConnection);
                    }
                    srvyRowIDList.Add(nextSeq);
                    srvyIDValues.Add((Double)nextSeq);
					depth = Convert.ToDecimal(dRow["Depth"]);
					Gx = Convert.ToDecimal(dRow["rGx"]);                    
					Gy = Convert.ToDecimal(dRow["rGy"]);
					Gz = Convert.ToDecimal(dRow["rGz"]);
					Bx = Convert.ToDecimal(dRow["rBx"]);
                    bxList.Add(Bx);
                    bxValues.Add((Double)Bx);
					By = Convert.ToDecimal(dRow["rBy"]);
                    byList.Add(By);
                    byValues.Add((Double)By);
					Bz = Convert.ToDecimal(dRow["rBz"]);
                    bzList.Add(Bz);
                    srDate = Convert.ToDateTime(dRow["rtimestamp"]).ToShortDateString();
                    srTime = Convert.ToDateTime(dRow["rtimestamp"]).ToShortTimeString();
                    if (!nextSeq.Equals(1))
                    {
                        List<Decimal> prevData = getPreviousRowData(OperatorID, ClientID, WellPadID, WellID, WellSectionID, RunID, (nextSeq - 1), ServiceID, ClientDBConnection);
                        DepthPrev = Convert.ToDecimal(prevData[0]);
                        colTPrev = Convert.ToDecimal(prevData[1]);
                        colYPrev = Convert.ToDecimal(prevData[2]);
                        colAFPrev = Convert.ToDecimal(prevData[3]);
                        colBAPrev = Convert.ToDecimal(prevData[4]);
                        colCAPrev = Convert.ToDecimal(prevData[5]);
                        colCFPrev = Convert.ToDecimal(prevData[6]);
                        rw_PreviousDepth = DepthPrev;
                        rw_PreviousInclination = colTPrev;
                        rw_PreviousAzimuth = Convert.ToDecimal(prevData[7]);
                        rw_PreviousTVD = Convert.ToDecimal(prevData[8]);
                        rw_PreviousCumDeltaNorth = Convert.ToDecimal(prevData[9]);
                        rw_PreviousCumDeltaEast = Convert.ToDecimal(prevData[10]);
                        rw_PreviousVerticalSection = Convert.ToDecimal(prevData[11]);
                        rw_PreviousNorthing = Convert.ToDecimal(prevData[12]);
                        rw_PreviousEasting = Convert.ToDecimal(prevData[13]);
                        rw_PreviousDepthTI = DepthPrev;
                        rw_PreviousInclinationTI = colTPrev;
                        rw_PreviousAzimuthTI = colAFPrev;
                        rw_PreviousTVDTI = Convert.ToDecimal(prevData[14]);
                        rw_PreviousCumDeltaNorthTI = Convert.ToDecimal(prevData[15]);
                        rw_PreviousCumDeltaEastTI = Convert.ToDecimal(prevData[16]);
                        rw_PreviousVerticalSectionTI = Convert.ToDecimal(prevData[17]);
                        rw_PreviousNorthingTI = Convert.ToDecimal(prevData[18]);
                        rw_PreviousEastingTI = Convert.ToDecimal(prevData[19]);
                        sc_PreviousDepth = DepthPrev;
                        sc_PreviousInclination = colTPrev;
                        sc_PreviousAzimuth = Convert.ToDecimal(prevData[20]);
                        sc_PreviousTVD = Convert.ToDecimal(prevData[21]);
                        sc_PreviousCumDeltaNorth = Convert.ToDecimal(prevData[22]);
                        sc_PreviousCumDeltaEast = Convert.ToDecimal(prevData[23]);
                        sc_PreviousVerticalSection = Convert.ToDecimal(prevData[24]);
                        sc_PreviousNorthing = Convert.ToDecimal(prevData[25]);
                        sc_PreviousEasting = Convert.ToDecimal(prevData[26]);
                        sc_PreviousDepthTI = DepthPrev;
                        sc_PreviousInclinationTI = colTPrev;
                        sc_PreviousAzimuthTI = Convert.ToDecimal(prevData[20]);
                        sc_PreviousTVDTI = Convert.ToDecimal(prevData[27]);
                        sc_PreviousCumDeltaNorthTI = Convert.ToDecimal(prevData[28]);
                        sc_PreviousCumDeltaEastTI = Convert.ToDecimal(prevData[29]);
                        sc_PreviousVerticalSectionTI = Convert.ToDecimal(prevData[30]);
                        sc_PreviousNorthingTI = Convert.ToDecimal(prevData[31]);
                        sc_PreviousEastingTI = Convert.ToDecimal(prevData[32]);
                        bh_PreviousDepth = DepthPrev;
                        bh_PreviousInclination = colTPrev;
                        colGEPrev = Convert.ToDecimal(prevData[58]);
                        bh_PreviousAzimuth = rw_PreviousAzimuth - colGEPrev;
                        bh_PreviousTVD = Convert.ToDecimal(prevData[33]);
                        bh_PreviousCumDeltaNorth = Convert.ToDecimal(prevData[34]);
                        bh_PreviousCumDeltaEast = Convert.ToDecimal(prevData[35]);
                        bh_PreviousVerticalSection = Convert.ToDecimal(prevData[36]);
                        bh_PreviousNorthing = Convert.ToDecimal(prevData[37]);
                        bh_PreviousEasting = Convert.ToDecimal(prevData[38]);
                        bh_PreviousDepthTI = DepthPrev;
                        bh_PreviousInclinationTI = colTPrev;
                        bh_PreviousAzimuthTI = rw_PreviousAzimuth - colGEPrev;
                        bh_PreviousTVDTI = Convert.ToDecimal(prevData[39]);
                        bh_PreviousCumDeltaNorthTI = Convert.ToDecimal(prevData[40]);
                        bh_PreviousCumDeltaEastTI = Convert.ToDecimal(prevData[41]);
                        bh_PreviousVerticalSectionTI = Convert.ToDecimal(prevData[42]);
                        bh_PreviousNorthingTI = Convert.ToDecimal(prevData[43]);
                        bh_PreviousEastingTI = Convert.ToDecimal(prevData[44]);
                        cr_PreviousDepth = DepthPrev;
                        cr_PreviousInclination = colTPrev;
                        cr_PreviousAzimuth = Convert.ToDecimal(prevData[45]);
                        cr_PreviousTVD = Convert.ToDecimal(prevData[46]);
                        cr_PreviousCumDeltaNorth = Convert.ToDecimal(prevData[47]);
                        cr_PreviousCumDeltaEast = Convert.ToDecimal(prevData[48]);
                        cr_PreviousVerticalSection = Convert.ToDecimal(prevData[49]);
                        cr_PreviousNorthing = Convert.ToDecimal(prevData[50]);
                        cr_PreviousEasting = Convert.ToDecimal(prevData[51]);
                        cr_PreviousDepthTI = DepthPrev;
                        cr_PreviousInclinationTI = colTPrev;
                        cr_PreviousAzimuthTI = Convert.ToDecimal(prevData[45]);
                        cr_PreviousTVDTI = Convert.ToDecimal(prevData[52]);
                        cr_PreviousCumDeltaNorthTI = Convert.ToDecimal(prevData[53]);
                        cr_PreviousCumDeltaEastTI = Convert.ToDecimal(prevData[54]);
                        cr_PreviousVerticalSectionTI = Convert.ToDecimal(prevData[55]);
                        cr_PreviousNorthingTI = Convert.ToDecimal(prevData[56]);
                        cr_PreviousEastingTI = Convert.ToDecimal(prevData[57]);
                    }
                    if (nextSeq.Equals(1))
                    {
                        DepthPrev = 0.00M;
                    }				    				    

				    DepthCurr = depth;
				    if ((DepthCurr < DepthPrev) || (DepthCurr.Equals(DepthPrev)) || (DepthCurr.Equals(DepthPrev + 0.25M)))
				    {
					    chkShot = 1;
				    }
				    else
				    {
					    chkShot = 0;
				    }				

				    deltaDepth = (DepthCurr - DepthPrev);
				    
				    gtfVal = getGTF(Gx, Gy);
				    colQVal = colQ(Bx, By);
				    colSVal = colS(jIFR, imdec, mdec);
				    colTVal = colT(Gx, Gy, Gz);
				    colTCurr = colTVal;
				    colVVal = colV(colTVal);
                    colWVal = colW(chkShot, nextSeq, colTCurr, colTPrev);
				    colYVal = colY(gtfVal, colTVal, Bx, By, Bz);
				    colYCurr = colYVal;
				    colZVal = colZ(colYVal, mdec, gConv, LatiNS, mdecCID, gcoID);
				    colAAVal = colAA(colYVal, imdec, gConv, LatiNS, imdecCID, gcoID);
				    colABVal = colAB(colYVal, mdec, mdecCID);
				    colACVal = colAC(colYVal, imdec, imdecCID);
				    colADVal = colAD(jIFR, colZVal, colAAVal);
				    colAEVal = colAE(jIFR, colABVal, colACVal);
				    colAFVal = colAF(jConverg, colADVal, colAEVal);
				    colAFCurr = colAFVal;
				    colBIVal = colBI(jIFR, cnvBT, cnvIFRBT, cnvDip, cnvIFRDip, colYVal, gtfVal, colTVal);
                    refBxList.Add(colBIVal);
                    biValues.Add((Double)colBIVal);
				    colHXVal = colHX(Bx, colBIVal);
				    colBJVal = colBJ(jIFR, cnvDip, cnvBT, cnvIFRDip, cnvIFRBT, gtfVal, colTVal, colYVal);
                    refByList.Add(colBJVal);
                    bjValues.Add((Double)colBJVal);
                    colHYVal = colHY(By, colBJVal);
				    colHZVal = colHZ(colHXVal, colHYVal);
                    hzList.Add(colHZVal);
                    colIAVal = colIA(nextSeq, WellID, WellSectionID, RunID, colHZVal, hzList, boxyError, ClientDBConnection);
				    colIBVal = colIB(colIAVal);
				    colBKVal = colBK(jIFR, cnvDip, cnvBT, cnvIFRDip, cnvIFRBT, colTVal, colYVal);
				    colCKVal = colCK(jIFR, Bz, colTVal, colYVal, colBKVal, cnvDip, cnvIFRDip, cnvBT, cnvIFRBT);
				    colCLVal = colCL(jIFR, colTVal, colYVal, colCKVal, cnvDip, cnvIFRDip);
				    colCMVal = colCM(colYVal, colCLVal);
				    colCNVal = colCN(jIFR, colTVal, colCMVal, cnvDip, cnvIFRDip, cnvBT, cnvIFRBT);
				    colFXVal = colFX(cnvDSSize);
				    colFWVal = colFW(cnvMSize);
				    colFZVal = colFZ(cnvNMSpAbv, colFXVal);
				    colGAVal = colGA(cnvNMSpBlw, cnvMDBLen, colFWVal);
				    colGBVal = colGB(colFZVal, colGAVal);
                    gbList.Add(colGBVal);
                    BxBiasCheck = biasCheck(ServiceID, nextSeq, WellID, WellSectionID, RunID, colGBVal, gbList, ClientDBConnection);
                    ByBiasCheck = biasCheck(ServiceID, nextSeq, WellID, WellSectionID, RunID, colGBVal, gbList, ClientDBConnection);
                    BzBiasCheck = biasCheck(ServiceID, nextSeq, WellID, WellSectionID, RunID, colGBVal, gbList, ClientDBConnection);
                    TSBias = biasCheck(ServiceID, nextSeq, WellID, WellSectionID, RunID, colGBVal, gbList, ClientDBConnection);
				    colHQVal = colHQ(Bx, By, Bz, colBIVal, colBJVal, colCNVal, TSBias);
                    hqValues.Add((Double)colHQVal);
                    ThreeSigmaVal = ThreeSigmaBValue(gbList);
                    colBMVal = colBM(nextSeq, Bx, By, Bz, colBIVal, colBJVal, colCNVal, ThreeSigmaVal, colHQVal, biValues, bjValues, hqValues);
                    colBOVal = colBO(ServiceID, nextSeq, WellID, WellSectionID, RunID, Bx, bxValues, colBIVal, biValues, ClientDBConnection);
				    colBQVal = colBQ(Bx, colBOVal, colBMVal);
				    colHRVal = colHR(Bx, By, Bz, colBIVal, colBJVal, colCNVal, TSBias);
                    hrValues.Add((Double)colHRVal);
                    colBNVal = colBN(nextSeq, Bx, By, Bz, colBIVal, colBJVal, colCNVal, ThreeSigmaVal, colHRVal, biValues, bjValues, hrValues);
                    colBPVal = colBP(ServiceID, nextSeq, WellID, WellSectionID, RunID, By, byValues, colBJVal, bjValues, ClientDBConnection);
				    colBRVal = colBR(By, colBPVal, colBNVal);
				    colCSVal = colCS(Bz, colCNVal);
                    csList.Add(colCSVal);
				    colGHVal = colGH(colFWVal, cnvMSize, cnvMGValue, cnvDGBtm);
				    colGRVal = colGR(colFWVal, colGHVal);
				    colGSVal = colGS(colGRVal, cnvNMSpBlw, cnvMDBLen);
				    colGTVal = colGT(dcpsLimit);
				    colGUVal = colGU(cnvNMSpAbv, colGTVal);
				    colGVVal = colGV(colGSVal, colGUVal);
                    colIDVal = colID(colYVal);                    
				    colCOVal = colCO(Bz, colCNVal, colCSVal, colGVVal, colIDVal);
                    coList.Add(colCOVal);
                    colCPVal = colCP(nextSeq, WellID, WellSectionID, RunID, colCOVal, coList, colCSVal, csList, ClientDBConnection);
				    colHSVal = colHS(Bx, By, Bz, colBIVal, colBJVal, colCNVal, colCSVal, colCPVal, TSBias);
                    hsList.Add(colHSVal);
                    hsValues.Add((Double)colHSVal);
                    colCQVal = colCQ(ServiceID, nextSeq, srvyIDValues, WellID, WellSectionID, RunID, colHSVal, hsValues, ClientDBConnection);
				    colCRVal = colCR(Bz, colCQVal);
				    colAHVal = colAH(gtfVal, colTVal, colBQVal, colBRVal, colCRVal);
				    colAIVal = colAI(colAHVal, mdec, gConv, LatiNS, mdecCID, gcoID);
				    colAJVal = colAJ(colAHVal, imdec, gConv, LatiNS, imdecCID, gcoID);
				    colAKVal = colAK(colAHVal, mdec, mdecCID);
				    colALVal = colAL(colAHVal, imdec, imdecCID);
				    colAMVal = colAM(jIFR, colAIVal, colAJVal);
				    colANVal = colAN(jIFR, colAKVal, colALVal);
				    colAOVal = colAO(jConverg, colAMVal, colANVal);
				    colAPVal = colAP(colYVal, colAHVal);
                    colIIVal = colII(colAPVal, NMSpAlw);
                    colINVal = colIN(colIIVal, colDRVal);
                    colARVal = colAR(nextSeq, colYCurr, colYPrev);
				    colASVal = colAS(colWVal, colARVal);
                    colATVal = colAT(nextSeq, colVVal, colWVal, colASVal, DepthCurr, DepthPrev);
                    colAUVal = colAU(nextSeq, jLenU, colVVal, colATVal, lcsRef1, lcsRef2, lcsRef3, lcsRef4);
				    colAVVal = colAV(colAUVal);
				    colAWVal = colAW(colTVal);
				    colAXVal = colAX(colYVal);
				    colAYVal = colAY(colAWVal, colAXVal);
				    colCAVal = colCA(Bx, By, Bz);
                    caList.Add(colCAVal);
				    colCFVal = colCF(Gx, Gy, Gz);
                    cfList.Add(colCFVal);
				    colCFCurr = colCFVal;
				    colBAVal = colBA(Gx, Gy, Gz, Bx, By, Bz, colCFVal, colCAVal);
                    baList.Add(colBAVal);
				    colBACurr = colBAVal;
                    colBBVal = colBB(nextSeq, colBACurr, colBAPrev);
				    colBCVal = colBC(jIFR, cnvIFRDip, cnvDip, colBACurr);
                    colBDVal = colBD(nextSeq, dflLowDip, dflUprDip, colBCVal, colBCPrev, ClientDBConnection);
				    colBFVal = colBF(jIFR, gtfVal, colTVal, cnvGT, cnvIFRGT);
				    colBGVal = colBG(jIFR, gtfVal, colTVal, cnvGT, cnvIFRGT);
				    colBHVal = colBH(jIFR, colTVal, cnvGT, cnvIFRGT);
				    colBSVal = colBS(Bx, colBQVal);
				    colBTVal = colBT(By, colBRVal);
				    colHTVal = colHT(Bx, By, Bz, colBIVal, colBJVal, colCNVal, colCSVal, TSBias);
                    htList.Add(colHTVal);
                    htValues.Add((Double)colHTVal);
                    colCTVal = colCT(ServiceID, nextSeq, srvyIDValues, WellID, WellSectionID, RunID, colHTVal, htValues, ClientDBConnection);
				    colCUVal = colCU(Bz, colCTVal);
				    colBUVal = colBU(colBQVal, colBRVal, colCUVal);
				    colCFVal = colCF(Gx, Gy, Gz);
				    colBVVal = colBV(Gx, Gy, Gz, colBQVal, colBRVal, colCUVal, colCFVal, colBUVal);
				    colBWVal = colBW(jIFR, cnvBT, cnvIFRBT, colBUVal);
				    colBXVal = colBX(jIFR, cnvDip, cnvIFRDip, colBVVal);
				    colBZVal = colBZ(Bx, By);
				    colCAVal = colCA(Bx, By, Bz);
				    colCACurr = colCAVal;
                    colCBVal = colCB(nextSeq, colCACurr, colCAPrev);
				    colCDVal = colCD(jIFR, cnvBT, cnvIFRBT, colCAVal);
                    colIGVal = colIG(cnvSQCBTotal, colCDVal);
                    colILVal = colIL(colIGVal, colCAVal);
                    colIHVal = colIH(sqcDip, colCDVal);
                    colIMVal = colIM(colIHVal, colBAVal);
                    colCCVal = colCC(WellID, WellSectionID, RunID, nextSeq, colCDVal, colCDPrev, colIBVal, dflLowBT, dflUprBT, ClientDBConnection);
				    colCEVal = colCE(Gx, Gy);
				    colCGVal = colCG(jIFR, colCFVal, cnvGT, cnvIFRGT);
                    colIFVal = colIF(cnvSQCGTotal, colCGVal);
                    colIKVal = colIK(colIFVal, colCFVal);
                    colCHVal = colCH(nextSeq, colCFCurr, colCFPrev);
                    colCIVal = colCI(nextSeq, colCGVal, dflLowGT, dflUprGT, Gx, Gy, Gz);
				    colCVVal = colCV(Bx, By, gtfVal, colTVal, colCUVal);
				    colCWVal = colCW(colYVal, colCVVal);
				    colCXVal = colCX(mdec, gConv, colCVVal, LatiNS, mdecCID, gcoID);
				    colCYVal = colCY(imdec, gConv, colCVVal, LatiNS, imdecCID, gcoID);
				    colCZVal = colCZ(mdec, colCVVal, mdecCID);
				    colDAVal = colDA(imdec, colCVVal, imdecCID);
				    colDBVal = colDB(jIFR, colCXVal, colCYVal);
				    colDCVal = colDC(jIFR, colCZVal, colDAVal);
				    colDDVal = colDD(jConverg, colDBVal, colDCVal);
				    colDEVal = colDE(colAFCurr, colDDVal);
				    colDFVal = colDF(colDEVal);
				    colDGVal = colDG(colCSVal, colGBVal, colGVVal);
				    colDHVal = colDH(Bx, By, colCUVal);
				    colDIVal = colDI(Gx, Gy, Gz, Bx, By, colCFVal, colCUVal, colDHVal);
				    colDJVal = colDJ(jIFR, cnvBT, cnvIFRBT, colDHVal);
				    colDKVal = colDK(jIFR, cnvDip, cnvIFRDip, colDIVal);
				    colDRVal = colDR(colAFCurr);
				    colDWVal = colDW(sqcDip, colBCVal);
				    colDYVal = colDY(cnvSQCBTotal, colCDVal);
				    colEAVal = colEA(cnvSQCGTotal, colCGVal);
				    colEGVal = colEG(colAYVal);
				    colFYVal = colFY(jIFR, colBACurr, colCAVal);
				    colGCVal = colGC(colTVal, colYVal, colFYVal, colGAVal);
                    colGEVal = colGE(colTVal, colYVal, colFYVal, colGBVal, colIDVal);
                    colIJVal = colIJ(colGEVal, NMSpAlw);
                    colIOVal = colIO(colIJVal, colDRVal);
                    colGDVal = colGD(colTVal, colYVal, colFYVal, colFZVal);
				    colEIVal = colEI(colGCVal, colGEVal);
				    colEJVal = colEJ(colGDVal, colGEVal);
				    colEKVal = colEK(colGEVal);
				    colELVal = colGPVal;
				    colEOVal = colEO(colELVal);
                    colGYVal = colGY(colTVal, colYVal, colFYVal, colGVVal, colIDVal);
				    colEPVal = colGYVal;
				    colGWVal = colGW(colTVal, colYVal, colFYVal, colGSVal);
				    colEQVal = colEQ(colGWVal, colGYVal);
				    colGXVal = colGX(colTVal, colYVal, colFYVal, colGUVal);
				    colERVal = colER(colGXVal, colGYVal);
				    colETVal = colET(colEPVal);
                    colEUVal = colEU(nextSeq, WellID, WellSectionID, RunID, colBACurr, baList, ClientDBConnection);
                    colEVVal = colEV(nextSeq, WellID, WellSectionID, RunID, colBACurr, baList, ClientDBConnection);
				    colEWVal = colEW(colEUVal, colEVVal);
                    colEXVal = colEX(nextSeq, WellID, WellSectionID, RunID, colCACurr, caList, ClientDBConnection);
                    colEYVal = colEY(nextSeq, WellID, WellSectionID, RunID, colCACurr, caList, ClientDBConnection);
				    colEZVal = colEZ(colEXVal, colEYVal);
                    colFAVal = colFA(nextSeq, WellID, WellSectionID, RunID, colCFCurr, cfList, ClientDBConnection);
                    colFBVal = colFB(nextSeq, WellID, WellSectionID, RunID, colCFCurr, cfList, ClientDBConnection);
				    colFCVal = colFC(colFAVal, colFBVal);
				    colGFVal = colGF(colGEVal);
				    colGIVal = colGI(colGHVal, cnvNMSpBlw, cnvMDBLen);
				    colGJVal = colGJ(colCTVal, colGIVal);
				    colGKVal = colGK(colGJVal, cnvNMSpAbv);
				    colGLVal = colGL(colGKVal, dcpsLimit);
				    colGMVal = colGM(colGKVal, DSCSize);
                    colGNVal = colGN(colTVal, colYVal, colFYVal, colGIVal, colIDVal);
                    colGOVal = colGO(colTVal, colYVal, colGJVal, colFYVal, colIDVal);
                    colGPVal = colGP(colTVal, colYVal, colFYVal, colCTVal, colIDVal);
				    colGZVal = colGZ(colGVVal, colCTVal);
				    colHAVal = colHA(colGYVal, colDEVal);
				    colHEVal = colHE(colHAVal);
				    colHBVal = colHB(colGZVal);
				    colHCVal = colHC(jIFR);
				    colHDVal = colHD(colHBVal, colHCVal);
				    colHHVal = colHH(colTVal);
				    colHGVal = colHG(jIFR, colBDVal, colCSVal, colFZVal, colGAVal, colGBVal);
				    colHVVal = getNGZ(colTVal, colYVal, ngzInc, ngzAzm);

				    HttpContext.Current.Session["tarmID"] = colHGVal;
				    HttpContext.Current.Session["taiID"] = colHHVal;
				    HttpContext.Current.Session["tadipID"] = colBDVal;
				    HttpContext.Current.Session["tabtID"] = colCCVal;
				    HttpContext.Current.Session["tagtID"] = colCIVal;
				    HttpContext.Current.Session["tascazID1"] = colDFVal;
				    HttpContext.Current.Session["tabzID"] = colDGVal;
				    HttpContext.Current.Session["tanmspID"] = colGFVal;

				    taID = getTA(colHGVal, colHHVal, colCIVal, colCCVal, colBDVal, colDFVal, colGFVal, colDGVal);                    

				    //Non-Mag Space II calculations
				    nmsTAzmMag = NMS.nmsTAzmMag(jIFR, mdec, imdec, gConv, convPTAzm); //Target Azm Magnetic - NMCalcII Col_I-9
				    nmsMtrPoleStrength = NMS.nmsMPStrength(cnvMSize); //Motor Pole Strength - NMCalcII Col_I-11
				    nmsDCPoleStrength = NMS.dcPoleStrength(cnvDSSize); // Drill Collar Pole Strength - NMCalcII Col_I-12
				    horzBT = NMS.horzBT(cnvDip, Convert.ToInt32(cnvBT)); // Horizontal Compoenent of Bt - NMCalcII Col_I-18
				    calcmgrPS = NMS.calcMGRStdMtrPoleStrength(MDBLen, nmsMtrPoleStrength); //NMSCalII Cell_I41
				    calcdcPS = NMS.calcDCPoleStrength(cnvDSSize, nmsDCPoleStrength); // NMSCalII Cell_I41
				    mtrSurfacePS = NMS.mtrSurfacePoleStrength(cnvMSize, cnvMGValue, cnvDGTop); // NMCalII Cell_C12
				    dscSurfacePoleStrength = NMS.dscSurfacePoleStrength(cnvDSSize, cnvDSGValue, DGBtm); // NMCalII Cell_C13
				    mtrSurfaceInterference = NMS.mtrSurfaceInterference(mtrSurfacePS, cnvNMSpBlw, cnvMDBLen); // NMCal Cell_C14
				    dcSurfaceInterference = NMS.dcSurfaceInterference(dscSurfacePoleStrength, cnvNMSpAbv); // NMCalII Cell_C15
				    bhaTotalInterference = NMS.bhaTotalInterference(mtrSurfaceInterference, dcSurfaceInterference); // NMCalII Cell_C16
				    bhaLCAZError = NMS.bhaLCAZError(bhaTotalInterference, rTInc, convPTAzm, horzBT); // NMCalII Cell_C17
				    dcMaxGaussmeter = NMS.dcMaxGaussmeter(dcpsLimit, cnvDSSize); // NMCalII Cell_C22
				    wcMPSMotor = NMS.wcMPSMotor(cnvMSize, mtrSurfacePS); //NMCalII Cell_C23
				    bptopDrillCollar = NMS.bptopDrillCollar(dcpsLimit, cnvNMSpAbv); //NMCalII Cell_C24
				    bpbtmMotor = NMS.bpbtmMotor(wcMPSMotor, cnvNMSpBlw, cnvMDBLen); // NMCalII Cell_C25
				    bpTotalBz = NMS.bpTotalBz(bptopDrillCollar, bpbtmMotor); // NMCalII Cell_C26
				    maxAllowableError = NMS.maxAllowableError(bpTotalBz, rTInc, convPTAzm, cnvBT, cnvDip); // NMCalII Cell_C27
				    //Non-Mag Spacing Calculator
				    nmAzmPlain = getNonMagAzimuthPlain(LatiNS, mdecCID, gcoID, currGridAzm, mdec, gConv);
				    nmAzmIfr = getNonMagAzimuthIFR(LatiNS, imdecCID, gcoID, currGridAzm, imdec, gConv);
				    currMagAzm = getCurrentMagneticAzimuth(jIFR, nmAzmPlain, nmAzmIfr);
				    stdDCInterference = NMS.stdvalDCInterference(nmsDCPoleStrength, NMSpAbv); // NMCal Cell_C34
				    stdMtrInterference = NMS.stdvalMTRInterference(nmsMtrPoleStrength, NMSpBlw, MDBLen); // NMCal Cell_C35                                                     
				    stdTTLBHAInterference = NMS.stdvalTTLBHAInterference(stdDCInterference, stdMtrInterference); //NMCal Cel_D37                    
				    gmtrDCInterference = dcSurfaceInterference; // NMCal Cell_G34
				    gmtrMtrInterference = mtrSurfaceInterference; // NMCal Cell_G35
				    gmtrTTLBHAInterference = NMS.ttlBHAInterference(gmtrDCInterference, gmtrMtrInterference); // NMCal Cell_G37
				    stdLCAZErrorBHA = NMS.stdLCAZError(stdTTLBHAInterference, colTCurr, nmsTAzmMag, horzBT); // NMSCal CellD_38
				    currBitToSensor = NMS.disBitToSensor(cnvNMSpBlw, cnvMDBLen); // NMSCal Cell_D40
				    gmtrLCAZErrorBHA = NMS.gmtrLCAZError(gmtrTTLBHAInterference, colTCurr, nmsTAzmMag, horzBT); // NMSCal CellG_38                    
				    // Calculating Well Plan values 
                    //TieIn List Info
                    tiMD = Convert.ToDecimal(tieInList[1]);
                    timdUnit = Convert.ToInt32(tieInList[2]);
                    if (timdUnit.Equals(4)) { tiMD = tiMD * 0.3048M; }
                    tiInc = Convert.ToDecimal(tieInList[3]);
                    tiAzm = Convert.ToDecimal(tieInList[4]);
                    tiTVD = Convert.ToDecimal(tieInList[5]);
                    titvdUnit = Convert.ToInt32(tieInList[6]);
                    if (titvdUnit.Equals(4)) { tiTVD = tiTVD * 0.3048M; }
                    tiNorth = Convert.ToDecimal(tieInList[7]);
                    tinUnit = Convert.ToInt32(tieInList[8]);
                    if (tinUnit.Equals(4)) { tiNorth = tiNorth * 0.3048M; }
                    tiEast = Convert.ToDecimal(tieInList[9]);
                    tieUnit = Convert.ToInt32(tieInList[10]);
                    if (tieUnit.Equals(4)) { tiEast = tiEast * 0.3048M; }
                    tiVS = Convert.ToDecimal(tieInList[11]);
                    tivsUnit = Convert.ToInt32(tieInList[12]);
                    if (tivsUnit.Equals(4)) { tiVS = tiVS * 0.3048M; }
                    tiSubSea = Convert.ToDecimal(tieInList[13]);
                    tissUnit = Convert.ToInt32(tieInList[14]);
                    if (tissUnit.Equals(4)) { tiSubSea = tiSubSea * 0.3048M; }

				    if (ServiceID.Equals(33) || ServiceID.Equals(34) || ServiceID.Equals(37) || ServiceID.Equals(38) || ServiceID.Equals(39) || ServiceID.Equals(40) || ServiceID.Equals(46)) //Regular Data Audit/QC OR Well Profile EXCEPT Correction
				    {
					    //PlanTypes: 1 = RawSurvey, 2 = SCSurvey, 3 = BHASurvey
					    //CalculateRawSurveyWellPlan
					    if (nextSeq.Equals(1))
					    {
						    rw_PreviousDepth = 0.00M;
						    rw_PreviousInclination = 0.00M;
						    rw_PreviousAzimuth = 0.00M;
						    rw_PreviousTVD = 0.00M;
						    rw_PreviousCumDeltaNorth = 0.00M;
						    rw_PreviousCumDeltaEast = 0.00M;
						    rw_PreviousVerticalSection = 0.00M;
						    rw_PreviousNorthing = 0.00M;
						    rw_PreviousEasting = 0.00M;                            
						    rw_PreviousDepthTI = 0.00M;
						    rw_PreviousInclinationTI = 0.00M;
						    rw_PreviousAzimuthTI = 0.00M;
						    rw_PreviousTVDTI = 0.00M;
						    rw_PreviousCumDeltaNorthTI = 0.00M;
						    rw_PreviousCumDeltaEastTI = 0.00M;
						    rw_PreviousVerticalSectionTI = 0.00M;
						    rw_PreviousNorthingTI = 0.00M;
						    rw_PreviousEastingTI = 0.00M;
						    sc_PreviousDepth = 0.00M;
						    sc_PreviousInclination = 0.00M;
						    sc_PreviousAzimuth = 0.00M;
						    sc_PreviousTVD = 0.00M;
						    sc_PreviousCumDeltaNorth = 0.00M;
						    sc_PreviousCumDeltaEast = 0.00M;
						    sc_PreviousVerticalSection = 0.00M;
						    sc_PreviousNorthing = 0.00M;
						    sc_PreviousEasting = 0.00M;
						    sc_PreviousDepthTI = 0.00M;
						    sc_PreviousInclinationTI = 0.00M;
						    sc_PreviousAzimuthTI = 0.00M;
						    sc_PreviousTVDTI = 0.00M;
						    sc_PreviousCumDeltaNorthTI = 0.00M;
						    sc_PreviousCumDeltaEastTI = 0.00M;
						    sc_PreviousVerticalSectionTI = 0.00M;
						    sc_PreviousNorthingTI = 0.00M;
						    sc_PreviousEastingTI = 0.00M;
						    bh_PreviousDepth = 0.00M;
						    bh_PreviousInclination = 0.00M;
						    bh_PreviousAzimuth = 0.00M;
						    bh_PreviousTVD = 0.00M;
						    bh_PreviousCumDeltaNorth = 0.00M;
						    bh_PreviousCumDeltaEast = 0.00M;
						    bh_PreviousVerticalSection = 0.00M;
						    bh_PreviousNorthing = 0.00M;
						    bh_PreviousEasting = 0.00M;
						    bh_PreviousDepthTI = 0.00M;
						    bh_PreviousInclinationTI = 0.00M;
						    bh_PreviousAzimuthTI = 0.00M;
						    bh_PreviousTVDTI = 0.00M;
						    bh_PreviousCumDeltaNorthTI = 0.00M;
						    bh_PreviousCumDeltaEastTI = 0.00M;
						    bh_PreviousVerticalSectionTI = 0.00M;
						    bh_PreviousNorthingTI = 0.00M;
						    bh_PreviousEastingTI = 0.00M;
                            cr_PreviousDepth = 0.00M;
                            cr_PreviousInclination = 0.00M;
                            cr_PreviousAzimuth = 0.00M;
                            cr_PreviousTVD = 0.00M;
                            cr_PreviousCumDeltaNorth = 0.00M;
                            cr_PreviousCumDeltaEast = 0.00M;
                            cr_PreviousVerticalSection = 0.00M;
                            cr_PreviousNorthing = 0.00M;
                            cr_PreviousEasting = 0.00M;
                            cr_PreviousDepthTI = 0.00M;
                            cr_PreviousInclinationTI = 0.00M;
                            cr_PreviousAzimuthTI = 0.00M;
                            cr_PreviousTVDTI = 0.00M;
                            cr_PreviousCumDeltaNorthTI = 0.00M;
                            cr_PreviousCumDeltaEastTI = 0.00M;
                            cr_PreviousVerticalSectionTI = 0.00M;
                            cr_PreviousNorthingTI = 0.00M;
                            cr_PreviousEastingTI = 0.00M;
					    }				    

					    ReferenceElevation = Convert.ToDecimal(wellMetaInfo[0]);
					    Int32 elevUnit = Convert.ToInt32(wellMetaInfo[1]);
					    if (elevUnit.Equals(4)) { ReferenceElevation = ReferenceElevation * 0.3048M; }
					    LengthUnit = Convert.ToInt32(wellMetaInfo[2]);
					    VertSecAzimuth = Convert.ToDecimal(wellMetaInfo[3]);

					    //WellPlan Calculations
                        rw_deltaMDVal = WPN.getMeasuredDepthDiff(nextSeq, tiMD, DepthCurr, rw_PreviousDepth);
                        rw_d1Val = WPN.getD1(nextSeq, colTVal, rw_PreviousInclination, colDRVal, rw_PreviousAzimuth);
                        rw_dogleg1Val = WPN.getDogLeg1(rw_d1Val);
                        rw_rfVal = WPN.getRF(nextSeq, rw_d1Val);
                        rw_deltaTVDVal = WPN.getDeltaTVD(nextSeq, rw_deltaMDVal, colTVal, rw_PreviousInclination, rw_rfVal);
                        rw_deltaNVal = WPN.getDeltaNorthing(nextSeq, rw_deltaTVDVal, rw_deltaMDVal, colTVal, rw_PreviousInclination, colDRVal, rw_PreviousAzimuth, rw_rfVal);
                        rw_cumDeltaNorthingVal = WPN.getCummulativeDeltaNorthing(nextSeq, tiNorth, rw_PreviousCumDeltaNorth, rw_deltaNVal);
                        rw_deltaEVal = WPN.getDeltaEasting(nextSeq, rw_deltaMDVal, rw_deltaTVDVal, rw_PreviousInclination, colTVal, rw_PreviousAzimuth, colDRVal, rw_rfVal);
                        rw_cumDeltaEastingVal = WPN.getCummulativeDeltaEasting(nextSeq, tiEast, rw_deltaEVal, rw_PreviousCumDeltaEast);
                        rw_clDistVal = WPN.getClosureDistance(rw_cumDeltaNorthingVal, rw_cumDeltaEastingVal);
                        rw_clDirectAngleVal1 = WPN.getColABValue(tiNorth, tiEast, rw_cumDeltaNorthingVal, rw_cumDeltaEastingVal, rw_clDistVal);
                        rw_clDirectAngleVal2 = WPN.getColACValue(tiNorth, tiEast, rw_cumDeltaNorthingVal, rw_cumDeltaEastingVal, rw_clDirectAngleVal1);
                        rw_deltaVSVal = WPN.getDeltaVerticalSection(VertSecAzimuth, rw_clDistVal, rw_clDirectAngleVal2);
                        rw_dogLegVal = WPN.getDogleg(nextSeq, colTVal, rw_PreviousInclination, colDRVal, rw_PreviousAzimuth);
                        rw_dls100Val = WPN.getDLS(nextSeq, LengthUnit, rw_deltaMDVal, rw_dogLegVal);
                        rw_tvdVal = WPN.getTVD(nextSeq, tiTVD, rw_deltaTVDVal, rw_PreviousTVD);
                        rw_vsVal = WPN.getVerticalSection(nextSeq, tiVS, rw_PreviousVerticalSection, rw_deltaVSVal);
                        rw_subseaVal = WPN.getSubsea(tiSubSea, rw_tvdVal);
                        rw_northingVal = WPN.getNorthing(nextSeq, rw_nVal, rw_PreviousNorthing, rw_deltaNVal);
                        rw_eastingVal = WPN.getEasting(nextSeq, rw_eVal, rw_PreviousEasting, rw_deltaEVal);
                        //Tie In Survey Values
                        rw_dataMDTI = WPN.getTieInMeasuredDepth(DepthCurr, cnvDGBtm, cnvMDBLen);
                        rw_deltaMDValTI = WPN.getMeasuredDepthDiff(nextSeq, tiMD, rw_dataMDTI, rw_PreviousDepth);
                        rw_d1ValTI = WPN.getD1(nextSeq, colTVal, rw_PreviousInclinationTI, colDRVal, rw_PreviousAzimuthTI);
                        rw_dogleg1ValTI = WPN.getDogLeg1(rw_d1ValTI);
                        rw_rfValTI = WPN.getRF(nextSeq, rw_d1ValTI);
                        rw_deltaTVDValTI = WPN.getDeltaTVD(nextSeq, rw_deltaMDValTI, colTVal, rw_PreviousInclinationTI, rw_rfValTI);
                        rw_deltaNValTI = WPN.getDeltaNorthing(nextSeq, rw_deltaTVDValTI, rw_deltaMDValTI, colTVal, rw_PreviousInclinationTI, colDRVal, rw_PreviousAzimuthTI, rw_rfValTI);
                        rw_cumDeltaNorthingValTI = WPN.getCummulativeDeltaNorthing(nextSeq, tiNorth, rw_PreviousCumDeltaNorthTI, rw_deltaNValTI);
                        rw_deltaEValTI = WPN.getDeltaEasting(nextSeq, rw_deltaMDValTI, rw_deltaTVDValTI, rw_PreviousInclinationTI, colTVal, rw_PreviousAzimuthTI, colDRVal, rw_rfValTI);
                        rw_cumDeltaEastingValTI = WPN.getCummulativeDeltaEasting(nextSeq, tiEast, rw_deltaEValTI, rw_PreviousCumDeltaEastTI);
                        rw_clDistValTI = WPN.getClosureDistance(rw_cumDeltaNorthingValTI, rw_cumDeltaEastingValTI);
                        rw_clDirectAngleVal1TI = WPN.getColABValue(tiNorth, tiEast, rw_cumDeltaNorthingValTI, rw_cumDeltaEastingValTI, rw_clDistValTI);
                        rw_clDirectAngleVal2TI = WPN.getColACValue(tiNorth, tiEast, rw_cumDeltaNorthingValTI, rw_cumDeltaEastingValTI, rw_clDirectAngleVal1TI);
                        rw_deltaVSValTI = WPN.getDeltaVerticalSection(VertSecAzimuth, rw_clDistValTI, rw_clDirectAngleVal2TI);
                        rw_dogLegValTI = WPN.getDogleg(nextSeq, colTVal, rw_PreviousInclinationTI, colDRVal, rw_PreviousAzimuthTI);
                        rw_dls100ValTI = WPN.getDLS(nextSeq, LengthUnit, rw_deltaMDValTI, rw_dogLegValTI);
                        rw_tvdValTI = WPN.getTVD(nextSeq, tiTVD, rw_deltaTVDValTI, rw_PreviousTVDTI);
                        rw_vsValTI = WPN.getVerticalSection(nextSeq, tiVS, rw_PreviousVerticalSectionTI, rw_deltaVSValTI);
                        rw_subseaValTI = WPN.getSubsea(tiSubSea, rw_tvdValTI);
                        rw_northingValTI = WPN.getNorthing(nextSeq, rw_nValTI, rw_PreviousNorthingTI, rw_deltaNValTI);
                        rw_eastingValTI = WPN.getEasting(nextSeq, rw_eValTI, rw_PreviousEastingTI, rw_deltaEValTI);
                        //SC Placement values
                        sc_deltaMDVal = WPN.getMeasuredDepthDiff(nextSeq, tiMD, DepthCurr, sc_PreviousDepth);
                        sc_d1Val = WPN.getD1(nextSeq, colTVal, sc_PreviousInclination, colDDVal, sc_PreviousAzimuth);
                        sc_dogleg1Val = WPN.getDogLeg1(sc_d1Val);
                        sc_rfVal = WPN.getRF(nextSeq, sc_d1Val);
                        sc_deltaTVDVal = WPN.getDeltaTVD(nextSeq, sc_deltaMDVal, colTVal, sc_PreviousInclination, sc_rfVal);
                        sc_deltaNVal = WPN.getDeltaNorthing(nextSeq, sc_deltaTVDVal, sc_deltaMDVal, colTVal, sc_PreviousInclination, colDDVal, sc_PreviousAzimuth, sc_rfVal);
                        sc_cumDeltaNorthingVal = WPN.getCummulativeDeltaNorthing(nextSeq, tiNorth, sc_PreviousCumDeltaNorth, sc_deltaNVal);
                        sc_deltaEVal = WPN.getDeltaEasting(nextSeq, sc_deltaMDVal, sc_deltaTVDVal, sc_PreviousInclination, colTVal, sc_PreviousAzimuth, colDDVal, sc_rfVal);
                        sc_cumDeltaEastingVal = WPN.getCummulativeDeltaEasting(nextSeq, tiEast, sc_deltaEVal, sc_PreviousCumDeltaEast);
                        sc_clDistVal = WPN.getClosureDistance(sc_cumDeltaNorthingVal, sc_cumDeltaEastingVal);
                        sc_clDirectAngleVal1 = WPN.getColABValue(tiNorth, tiEast, sc_cumDeltaNorthingVal, sc_cumDeltaEastingVal, sc_clDistVal);
                        sc_clDirectAngleVal2 = WPN.getColACValue(tiNorth, tiEast, sc_cumDeltaNorthingVal, sc_cumDeltaEastingVal, sc_clDirectAngleVal1);
                        sc_deltaVSVal = WPN.getDeltaVerticalSection(VertSecAzimuth, sc_clDistVal, sc_clDirectAngleVal2);
                        sc_dogLegVal = WPN.getDogleg(nextSeq, colTVal, sc_PreviousInclination, colDDVal, sc_PreviousAzimuth);
                        sc_dls100Val = WPN.getDLS(nextSeq, LengthUnit, sc_deltaMDVal, sc_dogLegVal);
                        sc_tvdVal = WPN.getTVD(nextSeq, tiTVD, sc_deltaTVDVal, sc_PreviousTVD);
                        sc_vsVal = WPN.getVerticalSection(nextSeq, tiVS, sc_PreviousVerticalSection, sc_deltaVSVal);
                        sc_subseaVal = WPN.getSubsea(tiSubSea, sc_tvdVal);
                        sc_northingVal = WPN.getNorthing(nextSeq, sc_nVal, sc_PreviousNorthing, sc_deltaNVal);
                        sc_eastingVal = WPN.getEasting(nextSeq, sc_eVal, sc_PreviousEasting, sc_deltaEVal);
                        //Tie In Short Collar                        
                        sc_dataMDTI = WPN.getTieInMeasuredDepth(DepthCurr, cnvDGBtm, cnvMDBLen);
                        sc_deltaMDValTI = WPN.getMeasuredDepthDiff(nextSeq, tiMD, sc_dataMDTI, sc_PreviousDepth);
                        sc_d1ValTI = WPN.getD1(nextSeq, colTVal, sc_PreviousInclinationTI, colDDVal, sc_PreviousAzimuthTI);
                        sc_dogleg1ValTI = WPN.getDogLeg1(sc_d1ValTI);
                        sc_rfValTI = WPN.getRF(nextSeq, sc_d1ValTI);
                        sc_deltaTVDValTI = WPN.getDeltaTVD(nextSeq, sc_deltaMDValTI, colTVal, sc_PreviousInclinationTI, sc_rfValTI);
                        sc_deltaNValTI = WPN.getDeltaNorthing(nextSeq, sc_deltaTVDValTI, sc_deltaMDValTI, colTVal, sc_PreviousInclinationTI, colDDVal, sc_PreviousAzimuthTI, sc_rfValTI);
                        sc_cumDeltaNorthingValTI = WPN.getCummulativeDeltaNorthing(nextSeq, tiNorth, sc_PreviousCumDeltaNorthTI, sc_deltaNValTI);
                        sc_deltaEValTI = WPN.getDeltaEasting(nextSeq, sc_deltaMDValTI, sc_deltaTVDValTI, sc_PreviousInclinationTI, colTVal, sc_PreviousAzimuthTI, colDDVal, sc_rfValTI);
                        sc_cumDeltaEastingValTI = WPN.getCummulativeDeltaEasting(nextSeq, tiEast, sc_deltaEValTI, sc_PreviousCumDeltaEastTI);
                        sc_clDistValTI = WPN.getClosureDistance(sc_cumDeltaNorthingValTI, sc_cumDeltaEastingValTI);
                        sc_clDirectAngleVal1TI = WPN.getColABValue(tiNorth, tiEast, sc_cumDeltaNorthingValTI, sc_cumDeltaEastingValTI, sc_clDistValTI);
                        sc_clDirectAngleVal2TI = WPN.getColACValue(tiNorth, tiEast, sc_cumDeltaNorthingValTI, sc_cumDeltaEastingValTI, sc_clDirectAngleVal1);
                        sc_deltaVSValTI = WPN.getDeltaVerticalSection(VertSecAzimuth, sc_clDistValTI, sc_clDirectAngleVal2TI);
                        sc_dogLegValTI = WPN.getDogleg(nextSeq, colTVal, sc_PreviousInclinationTI, colDDVal, sc_PreviousAzimuthTI);
                        sc_dls100ValTI = WPN.getDLS(nextSeq, LengthUnit, sc_deltaMDValTI, sc_dogLegValTI);
                        sc_tvdValTI = WPN.getTVD(nextSeq, tiTVD, sc_deltaTVDValTI, sc_PreviousTVDTI);
                        sc_vsValTI = WPN.getVerticalSection(nextSeq, tiVS, sc_PreviousVerticalSectionTI, sc_deltaVSValTI);
                        sc_subseaValTI = WPN.getSubsea(tiSubSea, sc_tvdValTI);
                        sc_northingValTI = WPN.getNorthing(nextSeq, sc_nValTI, sc_PreviousNorthingTI, sc_deltaNValTI);
                        sc_eastingValTI = WPN.getEasting(nextSeq, sc_eValTI, sc_PreviousEastingTI, sc_deltaEValTI);
                        //BHA based placement
                        Decimal bhAzm = colDRVal - colGEVal;
                        bh_deltaMDVal = WPN.getMeasuredDepthDiff(nextSeq, tiMD, DepthCurr, bh_PreviousDepth);
                        bh_d1Val = WPN.getD1(nextSeq, colTVal, bh_PreviousInclination, bhAzm, bh_PreviousAzimuth);
                        bh_dogleg1Val = WPN.getDogLeg1(bh_d1Val);
                        bh_rfVal = WPN.getRF(nextSeq, bh_d1Val);
                        bh_deltaTVDVal = WPN.getDeltaTVD(nextSeq, bh_deltaMDVal, colTVal, bh_PreviousInclination, bh_rfVal);
                        bh_deltaNVal = WPN.getDeltaNorthing(nextSeq, bh_deltaTVDVal, bh_deltaMDVal, colTVal, bh_PreviousInclination, bhAzm, bh_PreviousAzimuth, bh_rfVal);
                        bh_cumDeltaNorthingVal = WPN.getCummulativeDeltaNorthing(nextSeq, tiNorth, bh_PreviousCumDeltaNorth, bh_deltaNVal);
                        bh_deltaEVal = WPN.getDeltaEasting(nextSeq, bh_deltaMDVal, bh_deltaTVDVal, bh_PreviousInclination, colTVal, bh_PreviousAzimuth, bhAzm, bh_rfVal);
                        bh_cumDeltaEastingVal = WPN.getCummulativeDeltaEasting(nextSeq, tiEast, bh_deltaEVal, bh_PreviousCumDeltaEast);
                        bh_clDistVal = WPN.getClosureDistance(bh_cumDeltaNorthingVal, bh_cumDeltaEastingVal);
                        bh_clDirectAngleVal1 = WPN.getColABValue(tiNorth, tiEast, bh_cumDeltaNorthingVal, bh_cumDeltaEastingVal, bh_clDistVal);
                        bh_clDirectAngleVal2 = WPN.getColACValue(tiNorth, tiEast, bh_cumDeltaNorthingVal, bh_cumDeltaEastingVal, bh_clDirectAngleVal1);
                        bh_deltaVSVal = WPN.getDeltaVerticalSection(VertSecAzimuth, bh_clDistVal, bh_clDirectAngleVal2);
                        bh_dogLegVal = WPN.getDogleg(nextSeq, colTVal, bh_PreviousInclination, bhAzm, bh_PreviousAzimuth);
                        bh_dls100Val = WPN.getDLS(nextSeq, LengthUnit, bh_deltaMDVal, bh_dogLegVal);
                        bh_tvdVal = WPN.getTVD(nextSeq, tiTVD, bh_deltaTVDVal, bh_PreviousTVD);
                        bh_vsVal = WPN.getVerticalSection(nextSeq, tiVS, bh_PreviousVerticalSection, bh_deltaVSVal);
                        bh_subseaVal = WPN.getSubsea(tiSubSea, bh_tvdVal);
                        bh_northingVal = WPN.getNorthing(nextSeq, bh_nVal, bh_PreviousNorthing, bh_deltaNVal);
                        bh_eastingVal = WPN.getEasting(nextSeq, bh_eVal, bh_PreviousEasting, bh_deltaEVal);
                        //Tie In BHA
                        bh_dataMDTI = WPN.getTieInMeasuredDepth(DepthCurr, cnvDGBtm, cnvMDBLen);
                        bh_deltaMDValTI = WPN.getMeasuredDepthDiff(nextSeq, tiMD, bh_dataMDTI, bh_PreviousDepth);
                        bh_d1ValTI = WPN.getD1(nextSeq, colTVal, bh_PreviousInclinationTI, bhAzm, bh_PreviousAzimuthTI);
                        bh_dogleg1ValTI = WPN.getDogLeg1(bh_d1ValTI);
                        bh_rfValTI = WPN.getRF(nextSeq, bh_d1ValTI);
                        bh_deltaTVDValTI = WPN.getDeltaTVD(nextSeq, bh_deltaMDValTI, colTVal, bh_PreviousInclinationTI, bh_rfValTI);
                        bh_deltaNValTI = WPN.getDeltaNorthing(nextSeq, bh_deltaTVDValTI, bh_deltaMDValTI, colTVal, bh_PreviousInclinationTI, bhAzm, bh_PreviousAzimuthTI, bh_rfValTI);
                        bh_cumDeltaNorthingValTI = WPN.getCummulativeDeltaNorthing(nextSeq, tiNorth, bh_PreviousCumDeltaNorthTI, bh_deltaNValTI);
                        bh_deltaEValTI = WPN.getDeltaEasting(nextSeq, bh_deltaMDValTI, bh_deltaTVDValTI, bh_PreviousInclinationTI, colTVal, bh_PreviousAzimuthTI, bhAzm, bh_rfValTI);
                        bh_cumDeltaEastingValTI = WPN.getCummulativeDeltaEasting(nextSeq, tiEast, bh_deltaEValTI, bh_PreviousCumDeltaEastTI);
                        bh_clDistValTI = WPN.getClosureDistance(bh_cumDeltaNorthingValTI, bh_cumDeltaEastingValTI);
                        bh_clDirectAngleVal1TI = WPN.getColABValue(tiNorth, tiEast, bh_cumDeltaNorthingValTI, bh_cumDeltaEastingValTI, bh_clDistValTI);
                        bh_clDirectAngleVal2TI = WPN.getColACValue(tiNorth, tiEast, bh_cumDeltaNorthingValTI, bh_cumDeltaEastingValTI, bh_clDirectAngleVal1TI);
                        bh_deltaVSValTI = WPN.getDeltaVerticalSection(VertSecAzimuth, bh_clDistValTI, bh_clDirectAngleVal2TI);
                        bh_dogLegValTI = WPN.getDogleg(nextSeq, colTVal, bh_PreviousInclinationTI, bhAzm, bh_PreviousAzimuthTI);
                        bh_dls100ValTI = WPN.getDLS(nextSeq, LengthUnit, bh_deltaMDValTI, bh_dogLegValTI);
                        bh_tvdValTI = WPN.getTVD(nextSeq, tiTVD, bh_deltaTVDValTI, bh_PreviousTVDTI);
                        bh_vsValTI = WPN.getVerticalSection(nextSeq, tiVS, bh_PreviousVerticalSectionTI, bh_deltaVSValTI);
                        bh_subseaValTI = WPN.getSubsea(tiSubSea, bh_tvdValTI);
                        bh_northingValTI = WPN.getNorthing(nextSeq, bh_nValTI, bh_PreviousNorthingTI, bh_deltaNValTI);
                        bh_eastingValTI = WPN.getEasting(nextSeq, bh_eValTI, bh_PreviousEastingTI, bh_deltaEValTI);
                        //Corrected Azimuth Placement
                        //cr_PreviousDepth = Convert.ToDecimal(HttpContext.Current.Session["cr_PreviousDepth"]);
                        cr_deltaMDVal = WPN.getMeasuredDepthDiff(nextSeq, tiMD, DepthCurr, cr_PreviousDepth);
                        cr_d1Val = WPN.getD1(nextSeq, colTVal, cr_PreviousInclination, colAOVal, cr_PreviousAzimuth);
                        cr_dogleg1Val = WPN.getDogLeg1(cr_d1Val);
                        cr_rfVal = WPN.getRF(nextSeq, cr_d1Val);
                        cr_deltaTVDVal = WPN.getDeltaTVD(nextSeq, cr_deltaMDVal, colTVal, cr_PreviousInclination, cr_rfVal);
                        cr_deltaNVal = WPN.getDeltaNorthing(nextSeq, cr_deltaTVDVal, cr_deltaMDVal, colTVal, cr_PreviousInclination, colAOVal, cr_PreviousAzimuth, cr_rfVal);
                        cr_cumDeltaNorthingVal = WPN.getCummulativeDeltaNorthing(nextSeq, tiNorth, cr_PreviousCumDeltaNorth, cr_deltaNVal);
                        cr_deltaEVal = WPN.getDeltaEasting(nextSeq, cr_deltaMDVal, cr_deltaTVDVal, cr_PreviousInclination, colTVal, cr_PreviousAzimuth, colAOVal, cr_rfVal);
                        cr_cumDeltaEastingVal = WPN.getCummulativeDeltaEasting(nextSeq, tiEast, cr_deltaEVal, cr_PreviousCumDeltaEast);
                        cr_clDistVal = WPN.getClosureDistance(cr_cumDeltaNorthingVal, cr_cumDeltaEastingVal);
                        cr_clDirectAngleVal1 = WPN.getColABValue(tiNorth, tiEast, cr_cumDeltaNorthingVal, cr_cumDeltaEastingVal, cr_clDistVal);
                        cr_clDirectAngleVal2 = WPN.getColACValue(tiNorth, tiEast, cr_cumDeltaNorthingVal, cr_cumDeltaEastingVal, cr_clDirectAngleVal1);
                        cr_deltaVSVal = WPN.getDeltaVerticalSection(VertSecAzimuth, cr_clDistVal, cr_clDirectAngleVal2);
                        cr_dogLegVal = WPN.getDogleg(nextSeq, colTVal, cr_PreviousInclination, colAOVal, cr_PreviousAzimuth);
                        cr_dls100Val = WPN.getDLS(nextSeq, LengthUnit, cr_deltaMDVal, cr_dogLegVal);
                        cr_tvdVal = WPN.getTVD(nextSeq, tiTVD, cr_deltaTVDVal, cr_PreviousTVD);
                        cr_vsVal = WPN.getVerticalSection(nextSeq, tiVS, cr_PreviousVerticalSection, cr_deltaVSVal);
                        cr_subseaVal = WPN.getSubsea(tiSubSea, cr_tvdVal);
                        cr_northingVal = WPN.getNorthing(nextSeq, cr_nVal, cr_PreviousNorthing, cr_deltaNVal);
                        cr_eastingVal = WPN.getEasting(nextSeq, cr_eVal, cr_PreviousEasting, cr_deltaEVal);
                        //Tie In Survey Values
                        cr_dataMDTI = WPN.getTieInMeasuredDepth(DepthCurr, cnvDGBtm, cnvMDBLen);
                        cr_deltaMDValTI = WPN.getMeasuredDepthDiff(nextSeq, tiMD, cr_dataMDTI, cr_PreviousDepth);
                        cr_d1ValTI = WPN.getD1(nextSeq, colTVal, cr_PreviousInclinationTI, colAOVal, cr_PreviousAzimuthTI);
                        cr_dogleg1ValTI = WPN.getDogLeg1(cr_d1ValTI);
                        cr_rfValTI = WPN.getRF(nextSeq, cr_d1ValTI);
                        cr_deltaTVDValTI = WPN.getDeltaTVD(nextSeq, cr_deltaMDValTI, colTVal, cr_PreviousInclinationTI, cr_rfValTI);
                        cr_deltaNValTI = WPN.getDeltaNorthing(nextSeq, cr_deltaTVDValTI, cr_deltaMDValTI, colTVal, cr_PreviousInclinationTI, colAOVal, cr_PreviousAzimuthTI, cr_rfValTI);
                        cr_cumDeltaNorthingValTI = WPN.getCummulativeDeltaNorthing(nextSeq, tiNorth, cr_PreviousCumDeltaNorthTI, cr_deltaNValTI);
                        cr_deltaEValTI = WPN.getDeltaEasting(nextSeq, cr_deltaMDValTI, cr_deltaTVDValTI, cr_PreviousInclinationTI, colTVal, cr_PreviousAzimuthTI, colAOVal, cr_rfValTI);
                        cr_cumDeltaEastingValTI = WPN.getCummulativeDeltaEasting(nextSeq, tiEast, cr_deltaEValTI, cr_PreviousCumDeltaEastTI);
                        cr_clDistValTI = WPN.getClosureDistance(cr_cumDeltaNorthingValTI, cr_cumDeltaEastingValTI);
                        cr_clDirectAngleVal1TI = WPN.getColABValue(tiNorth, tiEast, cr_cumDeltaNorthingValTI, cr_cumDeltaEastingValTI, cr_clDistValTI);
                        cr_clDirectAngleVal2TI = WPN.getColACValue(tiNorth, tiEast, cr_cumDeltaNorthingValTI, cr_cumDeltaEastingValTI, cr_clDirectAngleVal1TI);
                        cr_deltaVSValTI = WPN.getDeltaVerticalSection(VertSecAzimuth, cr_clDistValTI, cr_clDirectAngleVal2TI);
                        cr_dogLegValTI = WPN.getDogleg(nextSeq, colTVal, cr_PreviousInclinationTI, colAOVal, cr_PreviousAzimuthTI);
                        cr_dls100ValTI = WPN.getDLS(nextSeq, LengthUnit, cr_deltaMDValTI, cr_dogLegValTI);
                        cr_tvdValTI = WPN.getTVD(nextSeq, tiTVD, cr_deltaTVDValTI, cr_PreviousTVDTI);
                        cr_vsValTI = WPN.getVerticalSection(nextSeq, tiVS, cr_PreviousVerticalSectionTI, cr_deltaVSValTI);
                        cr_subseaValTI = WPN.getSubsea(tiSubSea, cr_tvdValTI);
                        cr_northingValTI = WPN.getNorthing(nextSeq, cr_nValTI, cr_PreviousNorthingTI, cr_deltaNValTI);
                        cr_eastingValTI = WPN.getEasting(nextSeq, cr_eValTI, cr_PreviousEastingTI, cr_deltaEValTI);                                              
				    }

				    //Inserting Calculated values to RawQCResults table                     				    
                    switch (ServiceID)
                    {
                        case 33:{ //Raw Data Audit/QC
                            StoreRawFormatedData(srDate, srTime, depth, InputLengthUnit, Gx, Gy, Gz, Bx, By, Bz, InputAccelerometerUnit, InputMagnetometerUnit, RunID, WellID, WellSectionID, WellPadID, ClientID, OperatorID, nextSeq, DataEntryMethod, ServiceID, FileName, UserName, ClientDBConnection);
                                break; }
                        case 34: { // Results Data Audit/QC
                            StoreRawFormatedData(srDate, srTime, depth, InputLengthUnit, Gx, Gy, Gz, Bx, By, Bz, InputAccelerometerUnit, InputMagnetometerUnit, RunID, WellID, WellSectionID, WellPadID, ClientID, OperatorID, nextSeq, DataEntryMethod, ServiceID, FileName, UserName, ClientDBConnection);
                                break; }
                        case 37: { //3rd-Party Raw Data Audit/QC
                            Store3rdRawFormatedData(srDate, srTime, depth, InputLengthUnit, Gx, Gy, Gz, Bx, By, Bz, InputAccelerometerUnit, InputMagnetometerUnit, RunID, WellSectionID, WellID, WellPadID, ClientID, OperatorID, nextSeq, DataEntryMethod, ServiceID, FileName, UserName, ClientDBConnection);
                                break; }
                        case 38: { //3rd-Party Results Data Audit/QC
                            Store3rdRawFormatedData(srDate, srTime, depth, InputLengthUnit, Gx, Gy, Gz, Bx, By, Bz, InputAccelerometerUnit, InputMagnetometerUnit, RunID, WellSectionID, WellID, WellPadID, ClientID, OperatorID, nextSeq, DataEntryMethod, ServiceID, FileName, UserName, ClientDBConnection);
                                break; }
                        case 39: { //Historic Raw Data Audit/QC
                            StoreHistRawFormatedData(srDate, srTime, depth, InputLengthUnit, Gx, Gy, Gz, Bx, By, Bz, InputAccelerometerUnit, InputMagnetometerUnit, RunID, WellSectionID, WellID, WellPadID, ClientID, OperatorID, nextSeq, DataEntryMethod, ServiceID, FileName, UserName, ClientDBConnection);
                                break; }
                        case 40: { //Historic Results Data Audit/QC
                            StoreHistRawFormatedData(srDate, srTime, depth, InputLengthUnit, Gx, Gy, Gz, Bx, By, Bz, InputAccelerometerUnit, InputMagnetometerUnit, RunID, WellSectionID, WellID, WellPadID, ClientID, OperatorID, nextSeq, DataEntryMethod, ServiceID, FileName, UserName, ClientDBConnection);
                                break; }
                        case 47: { break; }
                        default: { break; }
                    }
                    qcResult = Convert.ToInt32(StoreQCData(nextSeq, OperatorID, ClientID, WellPadID, WellID, WellSectionID, RunID, sqcID, ServiceID, chkShot, DepthCurr, gtfVal, colQVal, colSVal, colTVal, colVVal, colWVal, colYVal, colZVal, colAAVal, colABVal, colACVal, colADVal, colAEVal, colAFVal, colAHVal, colAIVal, colAJVal, colAKVal, colALVal, colAMVal, colANVal, colAOVal, colAPVal, colARVal, colASVal, colATVal, colAUVal, colAVVal, colAWVal, colAXVal, colAYVal, colBAVal, colBBVal, colBCVal, colBDVal, colBFVal, colBGVal, colBHVal, colBIVal, colBJVal, colBKVal, colBMVal, colBNVal, colBOVal, colBPVal, colBQVal, colBRVal, colBSVal, colBTVal, colBUVal, colBVVal, colBWVal, colBXVal, colBZVal, colCAVal, colCBVal, colCCVal, colCDVal, colCEVal, colCFVal, colCGVal, colCHVal, colCIVal, colCKVal, colCLVal, colCMVal, colCNVal, colCOVal, colCPVal, colCQVal, colCRVal, colCSVal, colCTVal, colCUVal, colCVVal, colCWVal, colCXVal, colCYVal, colCZVal, colDAVal, colDBVal, colDCVal, colDDVal, colDEVal, colDFVal, colDGVal, colDHVal, colDIVal, colDJVal, colDKVal, srDate, srTime, colDRVal, colDWVal, colDYVal, colEAVal, colEGVal, colEIVal, colEJVal, colEKVal, colELVal, colGOVal, colGNVal, colEOVal, colEPVal, colEQVal, colERVal, colETVal, colEUVal, colEVVal, colEWVal, colEXVal, colEYVal, colEZVal, colFAVal, colFBVal, colFCVal, colFWVal, colFXVal, colFYVal, colFZVal, colGAVal, colGBVal, colGCVal, colGDVal, colGEVal, colGFVal, colGHVal, colGIVal, colGJVal, colGKVal, colGLVal, colGMVal, colGNVal, colGOVal, colGPVal, colGRVal, colGSVal, colGTVal, colGUVal, colGVVal, colGWVal, colGXVal, colGYVal, colGZVal, colHAVal, colHBVal, colHCVal, colHDVal, colHEVal, colHGVal, colHHVal, colHQVal, colHRVal, colHSVal, colHTVal, colHVVal, colHXVal, colHYVal, colHZVal, colIAVal, colIBVal, colIDVal, colIFVal, colIGVal, colIHVal, colIIVal, colIJVal, colIKVal, colILVal, colIMVal, colINVal, colIOVal, taID, stdDCInterference, stdMtrInterference, stdTTLBHAInterference, stdLCAZErrorBHA, currBitToSensor, gmtrDCInterference, gmtrMtrInterference, gmtrTTLBHAInterference, gmtrLCAZErrorBHA, rw_tvdVal, rw_vsVal, rw_northingVal, rw_eastingVal, rw_deltaMDVal, rw_d1Val, rw_dogleg1Val, rw_rfVal, rw_deltaTVDVal, rw_cumDeltaNorthingVal, rw_deltaNVal, rw_cumDeltaEastingVal, rw_deltaEVal, rw_deltaVSVal, rw_clDistVal, rw_clDirectAngleVal1, rw_clDirectAngleVal2, rw_dogLegVal, rw_dls100Val, rw_subseaVal, rw_dataMDTI, rw_tvdValTI, rw_vsValTI, rw_northingValTI, rw_eastingValTI, rw_deltaMDValTI, rw_d1ValTI, rw_dogleg1ValTI, rw_rfValTI, rw_deltaTVDValTI, rw_cumDeltaNorthingValTI, rw_deltaNValTI, rw_cumDeltaEastingValTI, rw_deltaEValTI, rw_deltaVSValTI, rw_clDistValTI, rw_clDirectAngleVal1TI, rw_clDirectAngleVal2TI, rw_dogLegValTI, rw_dls100ValTI, rw_subseaValTI, sc_tvdVal, sc_vsVal, sc_northingVal, sc_eastingVal, sc_deltaMDVal, sc_d1Val, sc_dogleg1Val, sc_rfVal, sc_deltaTVDVal, sc_cumDeltaNorthingVal, sc_deltaNVal, sc_cumDeltaEastingVal, sc_deltaEVal, sc_deltaVSVal, sc_clDistVal, sc_clDirectAngleVal1, sc_clDirectAngleVal2, sc_dogLegVal, sc_dls100Val, sc_subseaVal, sc_dataMDTI, sc_tvdValTI, sc_vsValTI, sc_northingValTI, sc_eastingValTI, sc_deltaMDValTI, sc_d1ValTI, sc_dogleg1ValTI, sc_rfValTI, sc_deltaTVDValTI, sc_cumDeltaNorthingValTI, sc_deltaNValTI, sc_cumDeltaEastingValTI, sc_deltaEValTI, sc_deltaVSValTI, sc_clDistValTI, sc_clDirectAngleVal1TI, sc_clDirectAngleVal2TI, sc_dogLegValTI, sc_dls100ValTI, sc_subseaValTI, bh_tvdVal, bh_vsVal, bh_northingVal, bh_eastingVal, bh_deltaMDVal, bh_d1Val, bh_dogleg1Val, bh_rfVal, bh_deltaTVDVal, bh_cumDeltaNorthingVal, bh_deltaNVal, bh_cumDeltaEastingVal, bh_deltaEVal, bh_deltaVSVal, bh_clDistVal, bh_clDirectAngleVal1, bh_clDirectAngleVal2, bh_dogLegVal, bh_dls100Val, bh_subseaVal, bh_dataMDTI, bh_tvdValTI, bh_vsValTI, bh_northingValTI, bh_eastingValTI, bh_deltaMDValTI, bh_d1ValTI, bh_dogleg1ValTI, bh_rfValTI, bh_deltaTVDValTI, bh_cumDeltaNorthingValTI, bh_deltaNValTI, bh_cumDeltaEastingValTI, bh_deltaEValTI, bh_deltaVSValTI, bh_clDistValTI, bh_clDirectAngleVal1TI, bh_clDirectAngleVal2TI, bh_dogLegValTI, bh_dls100ValTI, bh_subseaValTI, cr_tvdVal, cr_vsVal, cr_northingVal, cr_eastingVal, cr_deltaMDVal, cr_d1Val, cr_dogleg1Val, cr_rfVal, rw_deltaTVDVal, cr_cumDeltaNorthingVal, cr_deltaNVal, cr_cumDeltaEastingVal, cr_deltaEVal, cr_deltaVSVal, cr_clDistVal, cr_clDirectAngleVal1, cr_clDirectAngleVal2, cr_dogLegVal, cr_dls100Val, cr_subseaVal, cr_dataMDTI, cr_tvdValTI, cr_vsValTI, cr_northingValTI, cr_eastingValTI, cr_deltaMDValTI, cr_d1ValTI, cr_dogleg1ValTI, cr_rfValTI, cr_deltaTVDValTI, cr_cumDeltaNorthingValTI, cr_deltaNValTI, cr_cumDeltaEastingValTI, cr_deltaEValTI, cr_deltaVSValTI, cr_clDistValTI, cr_clDirectAngleVal1TI, cr_clDirectAngleVal2TI, cr_dogLegValTI, cr_dls100ValTI, cr_subseaValTI, acID, ClientDBConnection));
                    nextSeqPlacement = getPlacementNextSequence(OperatorID, ClientID, WellPadID, WellID, ClientDBConnection);
                    plcResult = Convert.ToInt32(StoreWellPlacementData(nextSeqPlacement, OperatorID, ClientID, WellPadID, WellID, DepthCurr, jLenU, colTVal, colDRVal, rw_tvdVal, rw_vsVal, rw_northingVal, rw_eastingVal, rw_deltaMDVal, rw_d1Val, rw_dogleg1Val, rw_rfVal, rw_deltaTVDVal, rw_cumDeltaNorthingVal, rw_deltaNVal, rw_cumDeltaEastingVal, rw_deltaEVal, rw_deltaVSVal, rw_clDistVal, rw_clDirectAngleVal1, rw_clDirectAngleVal2, rw_dogLegVal, rw_dls100Val, rw_subseaVal, rwSupNorthing, rwSupEasting, colTVal, colDDVal, sc_tvdVal, sc_vsVal, sc_northingVal, sc_eastingVal, sc_deltaMDVal, sc_d1Val, sc_dogleg1Val, sc_rfVal, sc_deltaTVDVal, sc_cumDeltaNorthingVal, sc_deltaNVal, sc_cumDeltaEastingVal, sc_deltaEVal, sc_deltaVSVal, sc_clDistVal, sc_clDirectAngleVal1, sc_clDirectAngleVal2, sc_dogLegVal, sc_dls100Val, sc_subseaVal, scSupNorthing, scSupEasting, colTVal, colDDVal, bh_tvdVal, bh_vsVal, bh_northingVal, bh_eastingVal, bh_deltaMDVal, bh_d1Val, bh_dogleg1Val, bh_rfVal, bh_deltaTVDVal, bh_cumDeltaNorthingVal, bh_deltaNVal, bh_cumDeltaEastingVal, bh_deltaEVal, bh_deltaVSVal, bh_clDistVal, bh_clDirectAngleVal1, bh_clDirectAngleVal2, bh_dogLegVal, bh_dls100Val, bh_subseaVal, bhSupNorthing, bhSupEasting, colTVal, colAOVal, cr_tvdVal, cr_vsVal, cr_northingVal, cr_eastingVal, cr_deltaMDVal, cr_d1Val, cr_dogleg1Val, cr_rfVal, rw_deltaTVDVal, cr_cumDeltaNorthingVal, cr_deltaNVal, cr_cumDeltaEastingVal, cr_deltaEVal, cr_deltaVSVal, cr_clDistVal, cr_clDirectAngleVal1, cr_clDirectAngleVal2, cr_dogLegVal, cr_dls100Val, cr_subseaVal, crSupNorthing, crSupEasting, PlacementDataFileID, PlacementTieInID, UserName, UserRealm, acID, ClientDBConnection));
				    if (qcResult.Equals(1))
				    {
					    qcResult = ACC.accServiceRendered(UserName, UserRealm, ServiceID, OperatorID, ClientID, WellPadID, WellID, WellSectionID, RunID, depth, chkShot);
				    }				    
			    }
                depthThresholds = getClientWellDepthThresholds(ClientID);
                depthLimit = Convert.ToDecimal(depthThresholds[0]);
                depthMargin = Convert.ToDecimal(depthThresholds[1]);
                WellPlanTargetDepth = getWellPlanTarget(OperatorID, WellPadID, WellID, ClientDBConnection);
                if (DepthCurr.Equals(depthLimit))
                {
                    WellDepthFlag = 1; //Reached Plan target depth
                }
                else if (DepthCurr < (WellPlanTargetDepth * depthMargin))
                {
                    WellDepthFlag = 2; //Reached Target MD and working within Plan Target depth margin
                }
                else if (DepthCurr > (WellPlanTargetDepth * depthMargin))
                {
                    WellDepthFlag = 3; //Exceeded Plan Target depth
                }
                else
                {
                    WellDepthFlag = 4; //Current MD below target depth
                }
                if (DepthCurr.Equals(runConvertedEndDepth))
                {
                    RunDepthFlag = 1; //Run End Depth reached
                }
                else if (DepthCurr > runConvertedEndDepth)
                {
                    RunDepthFlag = 2; //Run End Depth exceeded
                }
                else
                {
                    RunDepthFlag = 3; // Current Depth below Run End Depth
                }                    
                iReply.Add(qcResult);
                iReply.Add(WellDepthFlag);
                iReply.Add(RunDepthFlag);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static List<Decimal> getPreviousRowData(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Int32 SurveyRowID, Int32 ServiceID, String ClientDBConnection)
        {
            List<Decimal> iReply = new List<Decimal>();
            Decimal PrevDepth = -99.00M, colTPrev = -99.00M, colYPrev = -99.00M, colAFPrev = -99.00M, colBAPrev = -99.00M, colCAPrev = -99.00M, colCFPrev = -99.00M, colDR = -99.00M, colDD = -99.00M, colAO = -99.00M;
            Decimal rw_PreviousTVD = -99.00M, rw_PreviousCumDeltaNorth = -99.00M, colGE = -99.00M;
            Decimal rw_PreviousCumDeltaEast = -99.00M, rw_PreviousVerticalSection = -99.00M, rw_PreviousNorthing = -99.00M, rw_PreviousEasting = -99.00M;
            Decimal rw_PreviousTVDTI = -99.00M, rw_PreviousCumDeltaNorthTI = -99.00M, rw_PreviousCumDeltaEastTI = -99.00M;
            Decimal rw_PreviousVerticalSectionTI = -99.00M, rw_PreviousNorthingTI = -99.00M, rw_PreviousEastingTI = -99.00M;
            Decimal sc_PreviousTVD = -99.00M, sc_PreviousCumDeltaNorth = -99.00M, sc_PreviousCumDeltaEast = -99.00M, sc_PreviousVerticalSection = -99.00M;
            Decimal sc_PreviousNorthing = -99.00M, sc_PreviousEasting = -99.00M;
            Decimal sc_PreviousTVDTI = -99.00M, sc_PreviousCumDeltaNorthTI = -99.00M, sc_PreviousCumDeltaEastTI = -99.00M, sc_PreviousVerticalSectionTI = -99.00M, sc_PreviousNorthingTI = -99.00M;
            Decimal sc_PreviousEastingTI = -99.00M, bh_PreviousTVD = -99.00M, bh_PreviousCumDeltaNorth = -99.00M;
            Decimal bh_PreviousCumDeltaEast = -99.00M, bh_PreviousVerticalSection = -99.00M, bh_PreviousNorthing = -99.00M, bh_PreviousEasting = -99.00M;
            Decimal bh_PreviousTVDTI = -99.00M, bh_PreviousCumDeltaNorthTI = -99.00M, bh_PreviousCumDeltaEastTI = -99.00M, bh_PreviousVerticalSectionTI = -99.00M, bh_PreviousNorthingTI = -99.00M, bh_PreviousEastingTI = -99.00M;
            Decimal cr_PreviousTVD = -99.00M, cr_PreviousCumDeltaNorth = -99.00M, cr_PreviousCumDeltaEast = -99.00M, cr_PreviousVerticalSection = -99.00M, cr_PreviousNorthing = -99.00M;
            Decimal cr_PreviousEasting = -99.00M, cr_PreviousTVDTI = -99.00M;
            Decimal cr_PreviousCumDeltaNorthTI = -99.00M, cr_PreviousCumDeltaEastTI = -99.00M, cr_PreviousVerticalSectionTI = -99.00M, cr_PreviousNorthingTI = -99.00M, cr_PreviousEastingTI = -99.00M;
            String selData = "SELECT [Depth], [colT], [colY], [colAF], [colBA], [colCA], [colCF], [colDR], [rwTVD], [rwCummNorthing], [rwCummEasting], [rwVS], [rwNorthing], [rwEasting], [rwTvdTI], [rwCummNorthingTI], [rwCummEastingTI], [rwVSTI], [rwNorthingTI], [rwEastingTI], [colDD], [scTVD], [scCummNorthing], [scCummEasting], [scVS], [scNorthing], [scEasting], [scTvdTI], [scCummNorthingTI], [scCummEastingTI], [scVSTI], [scNorthingTI], [scEastingTI], [bhTVD], [bhCummNorthing], [bhCummEasting], [bhVS], [bhNorthing], [bhEasting], [bhTvdTI], [bhCummNorthingTI], [bhCummEastingTI], [bhVSTI], [bhNorthingTI], [bhEastingTI], [colAO], [crTVD], [crCummNorthing], [crCummEasting], [crVS], [crNorthing], [crEasting], [crTvdTI], [crCummNorthingTI], [crCummEastingTI], [crVSTI], [crNorthingTI], [crEastingTI], [colGE] FROM [RawQCResults] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID AND [srvID] = @srvID AND [srvyID] = @srvyID";
            using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
            {
                dataCon.Open();
                SqlCommand getData = new SqlCommand(selData, dataCon);
                getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                getData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                getData.Parameters.AddWithValue("@srvyID", SqlDbType.Int).Value = SurveyRowID.ToString();
                using (SqlDataReader readData = getData.ExecuteReader())
                {
                    if (readData.HasRows)
                    {
                        while (readData.Read())
                        {
                            PrevDepth = readData.GetDecimal(0);
                            iReply.Add(PrevDepth);
                            colTPrev = readData.GetDecimal(1);
                            iReply.Add(colTPrev);
                            colYPrev = readData.GetDecimal(2);
                            iReply.Add(colYPrev);
                            colAFPrev = readData.GetDecimal(3);
                            iReply.Add(colAFPrev);
                            colBAPrev = readData.GetDecimal(4);
                            iReply.Add(colBAPrev);
                            colCAPrev = readData.GetDecimal(5);
                            iReply.Add(colCAPrev);
                            colCFPrev = readData.GetDecimal(6);
                            iReply.Add(colCFPrev);
                            colDR = readData.GetDecimal(7);
                            iReply.Add(colDR);
                            rw_PreviousTVD = readData.GetDecimal(8);
                            iReply.Add(rw_PreviousTVD);
                            rw_PreviousCumDeltaNorth = readData.GetDecimal(9);
                            iReply.Add(rw_PreviousCumDeltaNorth);
                            rw_PreviousCumDeltaEast = readData.GetDecimal(10);
                            iReply.Add(rw_PreviousCumDeltaEast);
                            rw_PreviousVerticalSection = readData.GetDecimal(11);
                            iReply.Add(rw_PreviousVerticalSection);
                            rw_PreviousNorthing = readData.GetDecimal(12);
                            iReply.Add(rw_PreviousNorthing);
                            rw_PreviousEasting = readData.GetDecimal(13);
                            iReply.Add(rw_PreviousEasting);
                            rw_PreviousTVDTI = readData.GetDecimal(14);
                            iReply.Add(rw_PreviousTVDTI);
                            rw_PreviousCumDeltaNorthTI = readData.GetDecimal(15);
                            iReply.Add(rw_PreviousCumDeltaNorthTI);
                            rw_PreviousCumDeltaEastTI = readData.GetDecimal(16);
                            iReply.Add(rw_PreviousCumDeltaEastTI);
                            rw_PreviousVerticalSectionTI = readData.GetDecimal(17);
                            iReply.Add(rw_PreviousVerticalSectionTI);
                            rw_PreviousNorthingTI = readData.GetDecimal(18);
                            iReply.Add(rw_PreviousNorthingTI);
                            rw_PreviousEastingTI = readData.GetDecimal(19);
                            iReply.Add(rw_PreviousEastingTI);
                            colDD = readData.GetDecimal(20);
                            iReply.Add(colDD);
                            sc_PreviousTVD = readData.GetDecimal(21);
                            iReply.Add(sc_PreviousTVD);
                            sc_PreviousCumDeltaNorth = readData.GetDecimal(22);
                            iReply.Add(sc_PreviousCumDeltaNorth);
                            sc_PreviousCumDeltaEast = readData.GetDecimal(23);
                            iReply.Add(sc_PreviousCumDeltaEast);
                            sc_PreviousVerticalSection = readData.GetDecimal(24);
                            iReply.Add(sc_PreviousVerticalSection);
                            sc_PreviousNorthing = readData.GetDecimal(25);
                            iReply.Add(sc_PreviousNorthing);
                            sc_PreviousEasting = readData.GetDecimal(26);
                            iReply.Add(sc_PreviousEasting);
                            sc_PreviousTVDTI = readData.GetDecimal(27);
                            iReply.Add(sc_PreviousTVDTI);
                            sc_PreviousCumDeltaNorthTI = readData.GetDecimal(28);
                            iReply.Add(sc_PreviousCumDeltaNorthTI);
                            sc_PreviousCumDeltaEastTI = readData.GetDecimal(29);
                            iReply.Add(sc_PreviousCumDeltaEastTI);
                            sc_PreviousVerticalSectionTI = readData.GetDecimal(30);
                            iReply.Add(sc_PreviousVerticalSectionTI);
                            sc_PreviousNorthingTI = readData.GetDecimal(31);
                            iReply.Add(sc_PreviousNorthingTI);
                            sc_PreviousEastingTI = readData.GetDecimal(32);
                            iReply.Add(sc_PreviousEastingTI);
                            bh_PreviousTVD = readData.GetDecimal(33);
                            iReply.Add(bh_PreviousTVD);
                            bh_PreviousCumDeltaNorth = readData.GetDecimal(34);
                            iReply.Add(bh_PreviousCumDeltaNorth);
                            bh_PreviousCumDeltaEast = readData.GetDecimal(35);
                            iReply.Add(bh_PreviousCumDeltaEast);
                            bh_PreviousVerticalSection = readData.GetDecimal(36);
                            iReply.Add(bh_PreviousVerticalSection);
                            bh_PreviousNorthing = readData.GetDecimal(37);
                            iReply.Add(bh_PreviousNorthing);
                            bh_PreviousEasting = readData.GetDecimal(38);
                            iReply.Add(bh_PreviousEasting);
                            bh_PreviousTVDTI = readData.GetDecimal(39);
                            iReply.Add(bh_PreviousTVDTI);
                            bh_PreviousCumDeltaNorthTI = readData.GetDecimal(40);
                            iReply.Add(bh_PreviousCumDeltaNorthTI);
                            bh_PreviousCumDeltaEastTI = readData.GetDecimal(41);
                            iReply.Add(bh_PreviousCumDeltaEastTI);
                            bh_PreviousVerticalSectionTI = readData.GetDecimal(42);
                            iReply.Add(bh_PreviousVerticalSectionTI);
                            bh_PreviousNorthingTI = readData.GetDecimal(43);
                            iReply.Add(bh_PreviousNorthingTI);
                            bh_PreviousEastingTI = readData.GetDecimal(44);
                            iReply.Add(bh_PreviousEastingTI);
                            colAO = readData.GetDecimal(45);
                            iReply.Add(colAO);
                            cr_PreviousTVD = readData.GetDecimal(46);
                            iReply.Add(cr_PreviousTVD);
                            cr_PreviousCumDeltaNorth = readData.GetDecimal(47);
                            iReply.Add(cr_PreviousCumDeltaNorth);
                            cr_PreviousCumDeltaEast = readData.GetDecimal(48);
                            iReply.Add(cr_PreviousCumDeltaEast);
                            cr_PreviousVerticalSection = readData.GetDecimal(49);
                            iReply.Add(cr_PreviousVerticalSection);
                            cr_PreviousNorthing = readData.GetDecimal(50);
                            iReply.Add(cr_PreviousNorthing);
                            cr_PreviousEasting = readData.GetDecimal(51);
                            iReply.Add(cr_PreviousEasting);
                            cr_PreviousTVDTI = readData.GetDecimal(52);
                            iReply.Add(cr_PreviousTVDTI);
                            cr_PreviousCumDeltaNorthTI = readData.GetDecimal(53);
                            iReply.Add(cr_PreviousCumDeltaNorthTI);
                            cr_PreviousCumDeltaEastTI = readData.GetDecimal(54);
                            iReply.Add(cr_PreviousCumDeltaEastTI);
                            cr_PreviousVerticalSectionTI = readData.GetDecimal(55);
                            iReply.Add(cr_PreviousVerticalSectionTI);
                            cr_PreviousNorthingTI = readData.GetDecimal(56);
                            iReply.Add(cr_PreviousNorthingTI);
                            cr_PreviousEastingTI = readData.GetDecimal(57);
                            iReply.Add(cr_PreviousEastingTI);
                            colGE = readData.GetDecimal(58);
                            iReply.Add(colGE);
                        }
                    }
                }
            }
            return iReply;
        }
        
        private static Int32 getPlacementNextSequence(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, String ClientDBConnection)
        {
            Int32 iReply = -99;
            String selData = "SELECT ISNULL(MAX(datarowID), 0) FROM [WellPlacementData] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID AND [welID] = @welID";
            using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
            {
                try
                {
                    dataCon.Open();
                    SqlCommand getData = new SqlCommand(selData, dataCon);
                    getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                    getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                    getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                    getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                    iReply = Convert.ToInt32(getData.ExecuteScalar());
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
                finally
                {
                    dataCon.Close();
                    dataCon.Dispose();
                }
            }
            return iReply;
        }

        private static Int32 StoreWellPlacementData(Int32 nextSeqPlacement, Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Decimal MeasuredDepth, Int32 MeasuredDepthUnit, Decimal rw_Inclination, Decimal rw_Azimuth, Decimal rw_tvdVal, Decimal rw_vsVal, Decimal rw_northingVal, Decimal rw_eastingVal, Decimal rw_deltaMDVal, Decimal rw_d1Val, Decimal rw_dogleg1Val, Decimal rw_rfVal, Decimal rw_deltaTVDVal, Decimal rw_cumDeltaNorthingVal, Decimal rw_deltaNVal, Decimal rw_cumDeltaEastingVal, Decimal rw_deltaEVal, Decimal rw_deltaVSVal, Decimal rw_clDistVal, Decimal rw_clDirectAngleVal1, Decimal rw_clDirectAngleVal2, Decimal rw_dogLegVal, Decimal rw_dls100Val, Decimal rw_subseaVal, Decimal rwSupNorthing, Decimal rwSupEasting, Decimal sc_Inclination, Decimal sc_Azimuth, Decimal sc_tvdVal, Decimal sc_vsVal, Decimal sc_northingVal, Decimal sc_eastingVal, Decimal sc_deltaMDVal, Decimal sc_d1Val, Decimal sc_dogleg1Val, Decimal sc_rfVal, Decimal sc_deltaTVDVal, Decimal sc_cumDeltaNorthingVal, Decimal sc_deltaNVal, Decimal sc_cumDeltaEastingVal, Decimal sc_deltaEVal, Decimal sc_deltaVSVal, Decimal sc_clDistVal, Decimal sc_clDirectAngleVal1, Decimal sc_clDirectAngleVal2, Decimal sc_dogLegVal, Decimal sc_dls100Val, Decimal sc_subseaVal, Decimal scSupNorthing, Decimal scSupEasting, Decimal bh_Inclination, Decimal bh_Azimuth, Decimal bh_tvdVal, Decimal bh_vsVal, Decimal bh_northingVal, Decimal bh_eastingVal, Decimal bh_deltaMDVal, Decimal bh_d1Val, Decimal bh_dogleg1Val, Decimal bh_rfVal, Decimal bh_deltaTVDVal, Decimal bh_cumDeltaNorthingVal, Decimal bh_deltaNVal, Decimal bh_cumDeltaEastingVal, Decimal bh_deltaEVal, Decimal bh_deltaVSVal, Decimal bh_clDistVal, Decimal bh_clDirectAngleVal1, Decimal bh_clDirectAngleVal2, Decimal bh_dogLegVal, Decimal bh_dls100Val, Decimal bh_subseaVal, Decimal bhSupNorthing, Decimal bhSupEasting, Decimal cr_Inclination, Decimal cr_Azimuth, Decimal cr_tvdVal, Decimal cr_vsVal, Decimal cr_northingVal, Decimal cr_eastingVal, Decimal cr_deltaMDVal, Decimal cr_d1Val, Decimal cr_dogleg1Val, Decimal cr_rfVal, Decimal cr_deltaTVDVal, Decimal cr_cumDeltaNorthingVal, Decimal cr_deltaNVal, Decimal cr_cumDeltaEastingVal, Decimal cr_deltaEVal, Decimal cr_deltaVSVal, Decimal cr_clDistVal, Decimal cr_clDirectAngleVal1, Decimal cr_clDirectAngleVal2, Decimal cr_dogLegVal, Decimal cr_dls100Val, Decimal cr_subseaVal, Decimal crSupNorthing, Decimal crSupEasting, Int32 PlacementDataFileID, Int32 PlacementTieInID, String LoginName, String LoginRealm, Int32 AzimuthSelectionCriteriaID, String ClientDBConnection)
        {
            Int32 iReply = -99;
            String selData = "INSERT INTO [WellPlacementData] ([md], [mdUnit], [rwinc], [rwazm], [rwtvd], [rwvs], [rwnorthing], [rweasting], [rwdeltaMD], [rwd1], [rwdogleg1], [rwrf], [rwdeltaTVD], [rwcummDeltaNorthing], [rwdeltaNorthing], [rwcummDeltaEasting], [rwdeltaEasting], [rwdeltaVS], [rwclosureDistance], [rwclosureDirectAngle1], [rwclosureDirectAngle2], [rwdogleg], [rwdogleg100], [rwsubsea], [rwSupNorthing], [rwSupEasting], [scinc], [scazm], [sctvd], [scvs], [scnorthing], [sceasting], [scdeltaMD], [scd1], [scdogleg1], [scrf], [scdeltaTVD], [sccummDeltaNorthing], [scdeltaNorthing], [sccummDeltaEasting], [scdeltaEasting], [scdeltaVS], [scclosureDistance], [scclosureDirectAngle1], [scclosureDirectAngle2], [scdogleg], [scdogleg100], [scsubsea], [scSupNorthing], [scSupEasting], [bhinc], [bhazm], [bhtvd], [bhvs], [bhnorthing], [bheasting], [bhdeltaMD], [bhd1], [bhdogleg1], [bhrf], [bhdeltaTVD], [bhcummDeltaNorthing], [bhdeltaNorthing], [bhcummDeltaEasting], [bhdeltaEasting], [bhdeltaVS], [bhclosureDistance], [bhclosureDirectAngle1], [bhclosureDirectAngle2], [bhdogleg], [bhdogleg100], [bhsubsea], [bhSupNorthing], [bhSupEasting], [crinc], [crazm], [crtvd], [crvs], [crnorthing], [creasting], [crdeltaMD], [crd1], [crdogleg1], [crrf], [crdeltaTVD], [crcummDeltaNorthing], [crdeltaNorthing], [crcummDeltaEasting], [crdeltaEasting], [crdeltaVS], [crclosureDistance], [crclosureDirectAngle1], [crclosureDirectAngle2], [crdogleg], [crdogleg100], [crsubsea], [crSupNorthing], [crSupEasting], [optrID], [clntID], [wpdID], [welID], [datarowID], [wpfID], [wpctiID], [username], [realm], [cTime], [uTime], [acID]) VALUES (@md, @mdUnit, @rwinc, @rwazm, @rwtvd, @rwvs, @rwnorthing, @rweasting, @rwdeltaMD, @rwd1, @rwdogleg1, @rwrf, @rwdeltaTVD, @rwcummDeltaNorthing, @rwdeltaNorthing, @rwcummDeltaEasting, @rwdeltaEasting, @rwdeltaVS, @rwclosureDistance, @rwclosureDirectAngle1, @rwclosureDirectAngle2, @rwdogleg, @rwdogleg100, @rwsubsea, @rwSupNorthing, @rwSupEasting, @scinc, @scazm, @sctvd, @scvs, @scnorthing, @sceasting, @scdeltaMD, @scd1, @scdogleg1, @scrf, @scdeltaTVD, @sccummDeltaNorthing, @scdeltaNorthing, @sccummDeltaEasting, @scdeltaEasting, @scdeltaVS, @scclosureDistance, @scclosureDirectAngle1, @scclosureDirectAngle2, @scdogleg, @scdogleg100, @scsubsea, @scSupNorthing, @scSupEasting, @bhinc, @bhazm, @bhtvd, @bhvs, @bhnorthing, @bheasting, @bhdeltaMD, @bhd1, @bhdogleg1, @bhrf, @bhdeltaTVD, @bhcummDeltaNorthing, @bhdeltaNorthing, @bhcummDeltaEasting, @bhdeltaEasting, @bhdeltaVS, @bhclosureDistance, @bhclosureDirectAngle1, @bhclosureDirectAngle2, @bhdogleg, @bhdogleg100, @bhsubsea, @bhSupNorthing, @bhSupEasting, @crinc, @crazm, @crtvd, @crvs, @crnorthing, @creasting, @crdeltaMD, @crd1, @crdogleg1, @crrf, @crdeltaTVD, @crcummDeltaNorthing, @crdeltaNorthing, @crcummDeltaEasting, @crdeltaEasting, @crdeltaVS, @crclosureDistance, @crclosureDirectAngle1, @crclosureDirectAngle2, @crdogleg, @crdogleg100, @crsubsea, @crSupNorthing, @crSupEasting, @optrID, @clntID, @wpdID, @welID, @datarowID, @wpfID, @wpctiID, @username, @realm, @cTime, @uTime, @acID)";
            using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
            {
                try
                {
                    dataCon.Open();
                    SqlCommand insData = new SqlCommand(selData, dataCon);
                    insData.Parameters.AddWithValue("@md", SqlDbType.Decimal).Value = MeasuredDepth.ToString();
                    insData.Parameters.AddWithValue("@mdUnit", SqlDbType.Int).Value = MeasuredDepthUnit.ToString();
                    insData.Parameters.AddWithValue("@rwinc", SqlDbType.Decimal).Value = rw_Inclination.ToString();
                    insData.Parameters.AddWithValue("@rwazm", SqlDbType.Decimal).Value = rw_Azimuth.ToString();
                    insData.Parameters.AddWithValue("@rwtvd", SqlDbType.Decimal).Value = rw_tvdVal.ToString();
                    insData.Parameters.AddWithValue("@rwvs", SqlDbType.Decimal).Value = rw_vsVal.ToString();
                    insData.Parameters.AddWithValue("@rwnorthing", SqlDbType.Decimal).Value = rw_northingVal.ToString();
                    insData.Parameters.AddWithValue("@rweasting", SqlDbType.Decimal).Value = rw_eastingVal.ToString();
                    insData.Parameters.AddWithValue("@rwdeltaMD", SqlDbType.Decimal).Value = rw_deltaMDVal.ToString();
                    insData.Parameters.AddWithValue("@rwd1", SqlDbType.Decimal).Value = rw_d1Val.ToString();
                    insData.Parameters.AddWithValue("@rwdogleg1", SqlDbType.Decimal).Value = rw_dogleg1Val.ToString();
                    insData.Parameters.AddWithValue("@rwrf", SqlDbType.Decimal).Value = rw_rfVal.ToString();
                    insData.Parameters.AddWithValue("@rwdeltaTVD", SqlDbType.Decimal).Value = rw_deltaTVDVal.ToString();
                    insData.Parameters.AddWithValue("@rwcummDeltaNorthing", SqlDbType.Decimal).Value = rw_cumDeltaNorthingVal.ToString();
                    insData.Parameters.AddWithValue("@rwdeltaNorthing", SqlDbType.Decimal).Value = rw_deltaNVal.ToString();
                    insData.Parameters.AddWithValue("@rwcummDeltaEasting", SqlDbType.Decimal).Value = rw_cumDeltaEastingVal.ToString();
                    insData.Parameters.AddWithValue("@rwdeltaEasting", SqlDbType.Decimal).Value = rw_deltaEVal.ToString();
                    insData.Parameters.AddWithValue("@rwdeltaVS", SqlDbType.Decimal).Value = rw_deltaVSVal.ToString();
                    insData.Parameters.AddWithValue("@rwclosureDistance", SqlDbType.Decimal).Value = rw_clDistVal.ToString();
                    insData.Parameters.AddWithValue("@rwclosureDirectAngle1", SqlDbType.Decimal).Value = rw_clDirectAngleVal1.ToString();
                    insData.Parameters.AddWithValue("@rwclosureDirectAngle2", SqlDbType.Decimal).Value = rw_clDirectAngleVal2.ToString();
                    insData.Parameters.AddWithValue("@rwdogleg", SqlDbType.Decimal).Value = rw_dogLegVal.ToString();
                    insData.Parameters.AddWithValue("@rwdogleg100", SqlDbType.Decimal).Value = rw_dls100Val.ToString();
                    insData.Parameters.AddWithValue("@rwsubsea", SqlDbType.Decimal).Value = rw_subseaVal.ToString();
                    insData.Parameters.AddWithValue("@rwSupNorthing", SqlDbType.Decimal).Value = rwSupNorthing.ToString();
                    insData.Parameters.AddWithValue("@rwSupEasting", SqlDbType.Decimal).Value = rwSupEasting.ToString();
                    insData.Parameters.AddWithValue("@scinc", SqlDbType.Decimal).Value = sc_Inclination.ToString();
                    insData.Parameters.AddWithValue("@scazm", SqlDbType.Decimal).Value = sc_Azimuth.ToString();
                    insData.Parameters.AddWithValue("@sctvd", SqlDbType.Decimal).Value = sc_tvdVal.ToString();
                    insData.Parameters.AddWithValue("@scvs", SqlDbType.Decimal).Value = sc_vsVal.ToString();
                    insData.Parameters.AddWithValue("@scnorthing", SqlDbType.Decimal).Value = sc_northingVal.ToString();
                    insData.Parameters.AddWithValue("@sceasting", SqlDbType.Decimal).Value = sc_eastingVal.ToString();
                    insData.Parameters.AddWithValue("@scdeltaMD", SqlDbType.Decimal).Value = sc_deltaMDVal.ToString();
                    insData.Parameters.AddWithValue("@scd1", SqlDbType.Decimal).Value = sc_d1Val.ToString();
                    insData.Parameters.AddWithValue("@scdogleg1", SqlDbType.Decimal).Value = sc_dogleg1Val.ToString();
                    insData.Parameters.AddWithValue("@scrf", SqlDbType.Decimal).Value = sc_rfVal.ToString();
                    insData.Parameters.AddWithValue("@scdeltaTVD", SqlDbType.Decimal).Value = sc_deltaTVDVal.ToString();
                    insData.Parameters.AddWithValue("@sccummDeltaNorthing", SqlDbType.Decimal).Value = sc_cumDeltaNorthingVal.ToString();
                    insData.Parameters.AddWithValue("@scdeltaNorthing", SqlDbType.Decimal).Value = sc_deltaNVal.ToString();
                    insData.Parameters.AddWithValue("@sccummDeltaEasting", SqlDbType.Decimal).Value = sc_cumDeltaEastingVal.ToString();
                    insData.Parameters.AddWithValue("@scdeltaEasting", SqlDbType.Decimal).Value = sc_deltaEVal.ToString();
                    insData.Parameters.AddWithValue("@scdeltaVS", SqlDbType.Decimal).Value = sc_deltaVSVal.ToString();
                    insData.Parameters.AddWithValue("@scclosureDistance", SqlDbType.Decimal).Value = sc_clDistVal.ToString();
                    insData.Parameters.AddWithValue("@scclosureDirectAngle1", SqlDbType.Decimal).Value = sc_clDirectAngleVal1.ToString();
                    insData.Parameters.AddWithValue("@scclosureDirectAngle2", SqlDbType.Decimal).Value = sc_clDirectAngleVal2.ToString();
                    insData.Parameters.AddWithValue("@scdogleg", SqlDbType.Decimal).Value = sc_dogLegVal.ToString();
                    insData.Parameters.AddWithValue("@scdogleg100", SqlDbType.Decimal).Value = sc_dls100Val.ToString();
                    insData.Parameters.AddWithValue("@scsubsea", SqlDbType.Decimal).Value = sc_subseaVal.ToString();
                    insData.Parameters.AddWithValue("@scSupNorthing", SqlDbType.Decimal).Value = scSupNorthing.ToString();
                    insData.Parameters.AddWithValue("@scSupEasting", SqlDbType.Decimal).Value = scSupEasting.ToString();
                    insData.Parameters.AddWithValue("@bhinc", SqlDbType.Decimal).Value = bh_Inclination.ToString();
                    insData.Parameters.AddWithValue("@bhazm", SqlDbType.Decimal).Value = bh_Azimuth.ToString();
                    insData.Parameters.AddWithValue("@bhtvd", SqlDbType.Decimal).Value = bh_tvdVal.ToString();
                    insData.Parameters.AddWithValue("@bhvs", SqlDbType.Decimal).Value = bh_vsVal.ToString();
                    insData.Parameters.AddWithValue("@bhnorthing", SqlDbType.Decimal).Value = bh_northingVal.ToString();
                    insData.Parameters.AddWithValue("@bheasting", SqlDbType.Decimal).Value = bh_eastingVal.ToString();
                    insData.Parameters.AddWithValue("@bhdeltaMD", SqlDbType.Decimal).Value = bh_deltaMDVal.ToString();
                    insData.Parameters.AddWithValue("@bhd1", SqlDbType.Decimal).Value = bh_d1Val.ToString();
                    insData.Parameters.AddWithValue("@bhdogleg1", SqlDbType.Decimal).Value = bh_dogleg1Val.ToString();
                    insData.Parameters.AddWithValue("@bhrf", SqlDbType.Decimal).Value = bh_rfVal.ToString();
                    insData.Parameters.AddWithValue("@bhdeltaTVD", SqlDbType.Decimal).Value = bh_deltaTVDVal.ToString();
                    insData.Parameters.AddWithValue("@bhcummDeltaNorthing", SqlDbType.Decimal).Value = bh_cumDeltaNorthingVal.ToString();
                    insData.Parameters.AddWithValue("@bhdeltaNorthing", SqlDbType.Decimal).Value = bh_deltaNVal.ToString();
                    insData.Parameters.AddWithValue("@bhcummDeltaEasting", SqlDbType.Decimal).Value = bh_cumDeltaEastingVal.ToString();
                    insData.Parameters.AddWithValue("@bhdeltaEasting", SqlDbType.Decimal).Value = bh_deltaEVal.ToString();
                    insData.Parameters.AddWithValue("@bhdeltaVS", SqlDbType.Decimal).Value = bh_deltaVSVal.ToString();
                    insData.Parameters.AddWithValue("@bhclosureDistance", SqlDbType.Decimal).Value = bh_clDistVal.ToString();
                    insData.Parameters.AddWithValue("@bhclosureDirectAngle1", SqlDbType.Decimal).Value = bh_clDirectAngleVal1.ToString();
                    insData.Parameters.AddWithValue("@bhclosureDirectAngle2", SqlDbType.Decimal).Value = bh_clDirectAngleVal2.ToString();
                    insData.Parameters.AddWithValue("@bhdogleg", SqlDbType.Decimal).Value = bh_dogLegVal.ToString();
                    insData.Parameters.AddWithValue("@bhdogleg100", SqlDbType.Decimal).Value = bh_dls100Val.ToString();
                    insData.Parameters.AddWithValue("@bhsubsea", SqlDbType.Decimal).Value = bh_subseaVal.ToString();
                    insData.Parameters.AddWithValue("@bhSupNorthing", SqlDbType.Decimal).Value = bhSupNorthing.ToString();
                    insData.Parameters.AddWithValue("@bhSupEasting", SqlDbType.Decimal).Value = bhSupEasting.ToString();
                    insData.Parameters.AddWithValue("@crinc", SqlDbType.Decimal).Value = cr_Inclination.ToString();
                    insData.Parameters.AddWithValue("@crazm", SqlDbType.Decimal).Value = cr_Azimuth.ToString();
                    insData.Parameters.AddWithValue("@crtvd", SqlDbType.Decimal).Value = cr_tvdVal.ToString();
                    insData.Parameters.AddWithValue("@crvs", SqlDbType.Decimal).Value = cr_vsVal.ToString();
                    insData.Parameters.AddWithValue("@crnorthing", SqlDbType.Decimal).Value = cr_northingVal.ToString();
                    insData.Parameters.AddWithValue("@creasting", SqlDbType.Decimal).Value = cr_eastingVal.ToString();
                    insData.Parameters.AddWithValue("@crdeltaMD", SqlDbType.Decimal).Value = cr_deltaMDVal.ToString();
                    insData.Parameters.AddWithValue("@crd1", SqlDbType.Decimal).Value = cr_d1Val.ToString();
                    insData.Parameters.AddWithValue("@crdogleg1", SqlDbType.Decimal).Value = cr_dogleg1Val.ToString();
                    insData.Parameters.AddWithValue("@crrf", SqlDbType.Decimal).Value = cr_rfVal.ToString();
                    insData.Parameters.AddWithValue("@crdeltaTVD", SqlDbType.Decimal).Value = cr_deltaTVDVal.ToString();
                    insData.Parameters.AddWithValue("@crcummDeltaNorthing", SqlDbType.Decimal).Value = cr_cumDeltaNorthingVal.ToString();
                    insData.Parameters.AddWithValue("@crdeltaNorthing", SqlDbType.Decimal).Value = cr_deltaNVal.ToString();
                    insData.Parameters.AddWithValue("@crcummDeltaEasting", SqlDbType.Decimal).Value = cr_cumDeltaEastingVal.ToString();
                    insData.Parameters.AddWithValue("@crdeltaEasting", SqlDbType.Decimal).Value = cr_deltaEVal.ToString();
                    insData.Parameters.AddWithValue("@crdeltaVS", SqlDbType.Decimal).Value = cr_deltaVSVal.ToString();
                    insData.Parameters.AddWithValue("@crclosureDistance", SqlDbType.Decimal).Value = cr_clDistVal.ToString();
                    insData.Parameters.AddWithValue("@crclosureDirectAngle1", SqlDbType.Decimal).Value = cr_clDirectAngleVal1.ToString();
                    insData.Parameters.AddWithValue("@crclosureDirectAngle2", SqlDbType.Decimal).Value = cr_clDirectAngleVal2.ToString();
                    insData.Parameters.AddWithValue("@crdogleg", SqlDbType.Decimal).Value = cr_dogLegVal.ToString();
                    insData.Parameters.AddWithValue("@crdogleg100", SqlDbType.Decimal).Value = cr_dls100Val.ToString();
                    insData.Parameters.AddWithValue("@crsubsea", SqlDbType.Decimal).Value = cr_subseaVal.ToString();
                    insData.Parameters.AddWithValue("@crSupNorthing", SqlDbType.Decimal).Value = crSupNorthing.ToString();
                    insData.Parameters.AddWithValue("@crSupEasting", SqlDbType.Decimal).Value = crSupEasting.ToString();
                    insData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                    insData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                    insData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                    insData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                    insData.Parameters.AddWithValue("@datarowID", SqlDbType.Int).Value = nextSeqPlacement.ToString();
                    insData.Parameters.AddWithValue("@wpfID", SqlDbType.Int).Value = PlacementDataFileID.ToString();
                    insData.Parameters.AddWithValue("@wpctiID", SqlDbType.Int).Value = PlacementTieInID.ToString();
                    insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = LoginName.ToString();
                    insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = LoginRealm.ToString();
                    insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                    insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                    insData.Parameters.AddWithValue("@acID", SqlDbType.Int).Value = AzimuthSelectionCriteriaID.ToString();
                    iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
                finally
                {
                    dataCon.Close();
                    dataCon.Dispose();
                }
            }
            return iReply;
        }

        public static Decimal calConvProposedInclination(Decimal rInc, Decimal rTInc)
        {
            try
            {
                Decimal iReply = -99.0000M;
                if (rTInc.Equals(0.00M) || rTInc.Equals(null))
                {
                    iReply = rInc;
                }
                else
                {
                    iReply = rTInc;
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static decimal getCurrentMagneticAzimuth(Int32 jIFR, Decimal nmAzmPlain, Decimal nmAzmIfr)
        {
            try
            {
                Decimal iReply = -99.00M;
                if (jIFR.Equals(1))
                {
                    iReply = nmAzmPlain;
                }
                else
                {
                    iReply = nmAzmIfr;
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 StoreQCData(Int32 SurveyRowID, Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Int32 SQCID, Int32 ServiceTypeID, Int32 CheckShot, Decimal Depth, Decimal colPVal, Decimal colQVal, Decimal colSVal, Decimal colTVal, Int32 colVVal, Decimal colWVal, Decimal colYVal, Decimal colZVal, Decimal colAAVal, Decimal colABVal, Decimal colACVal, Decimal colADVal, Decimal colAEVal, Decimal colAFVal, Decimal colAHVal, Decimal colAIVal, Decimal colAJVal, Decimal colAKVal, Decimal colALVal, Decimal colAMVal, Decimal colANVal, Decimal colAOVal, Decimal colAPVal, Decimal colARVal, Decimal colASVal, Decimal colATVal, Decimal colAUVal, Int32 colAVVal, Int32 colAWVal, Int32 colAXVal, Int32 colAYVal, Decimal colBAVal, Decimal colBBVal, Decimal colBCVal, Int32 colBDVal, Decimal colBFVal, Decimal colBGVal, Decimal colBHVal, Decimal colBIVal, Decimal colBJVal, Decimal colBKVal, Decimal colBMVal, Decimal colBNVal, Decimal colBOVal, Decimal colBPVal, Decimal colBQVal, Decimal colBRVal, Decimal colBSVal, Decimal colBTVal, Decimal colBUVal, Decimal colBVVal, Decimal colBWVal, Decimal colBXVal, Decimal colBZVal, Decimal colCAVal, Decimal colCBVal, Int32 colCCVal, Decimal colCDVal, Decimal colCEVal, Decimal colCFVal, Decimal colCGVal, Decimal colCHVal, Int32 colCIVal, Decimal colCKVal, Decimal colCLVal, Decimal colCMVal, Decimal colCNVal, Decimal colCOVal, Decimal colCPVal, Decimal colCQVal, Decimal colCRVal, Decimal colCSVal, Decimal colCTVal, Decimal colCUVal, Decimal colCVVal, Decimal colCWVal, Decimal colCXVal, Decimal colCYVal, Decimal colCZVal, Decimal colDAVal, Decimal colDBVal, Decimal colDCVal, Decimal colDDVal, Decimal colDEVal, Int32 colDFVal, Int32 colDGVal, Int32 colDHVal, Decimal colDIVal, Int32 colDJVal, Decimal colDKVal, String srDate, String srTime, Decimal colDRVal, Int32 colDWVal, Int32 colDYVal, Int32 colEAVal, Int32 colEGVal, Decimal colEIVal, Decimal colEJVal, Int32 colEKVal, Decimal colELVal, Decimal colEMVal, Decimal colENVal, Int32 colEOVal, Decimal colEPVal, Decimal colEQVal, Decimal colERVal, Int32 colETVal, Decimal colEUVal, Decimal colEVVal, Decimal colEWVal, Int32 colEXVal, Int32 colEYVal, Int32 colEZVal, Decimal colFAVal, Decimal colFBVal, Decimal colFCVal, Int32 colFWVal, Int32 colFXVal, Decimal colFYVal, Decimal colFZVal, Decimal colGAVal, Decimal colGBVal, Decimal colGCVal, Decimal colGDVal, Decimal colGEVal, Int32 colGFVal, Decimal colGHVal, Decimal colGIVal, Decimal colGJVal, Decimal colGKVal, Int32 colGLVal, Decimal colGMVal, Decimal colGNVal, Decimal colGOVal, Decimal colGPVal, Decimal colGRVal, Decimal colGSVal, Decimal colGTVal, Decimal colGUVal, Decimal colGVVal, Decimal colGWVal, Decimal colGXVal, Decimal colGYVal, Decimal colGZVal, Decimal colHAVal, Int32 colHBVal, Int32 colHCVal, Int32 colHDVal, Int32 colHEVal, Int32 colHGVal, Int32 colHHVal, Decimal colHQVal, Decimal colHRVal, Decimal colHSVal, Decimal colHTVal, Int32 colHVVal, Decimal colHXVal, Decimal colHYVal, Decimal colHZVal, Decimal colIAVal, Decimal colIBVal, Decimal colIDVal, Int32 colIFVal, Int32 colIGVal, Int32 colIHVal, Int32 colIIVal, Int32 colIJVal, Decimal colIKVal, Decimal colILVal, Decimal colIMVal, Decimal colINVal, Decimal colIOVal, Int32 taID, Decimal stdDCInterference, Decimal stdMtrInterference, Decimal stdTTLBHAInterference, Decimal stdLCAZErrorBHA, Decimal currBitToSensor, Decimal gmtrDCInterference, Decimal gmtrMtrInterference, Decimal gmtrTTLBHAInterference, Decimal gmtrLCAZErrorBHA, Decimal rwTVD, Decimal rwVS, Decimal rwNorthing, Decimal rwEasting, Decimal rwDeltaMD, Decimal rwD1, Decimal rwDogleg1, Decimal rwRF, Decimal rwDeltaTVD, Decimal rwCummNorthing, Decimal rwDeltaNorth, Decimal rwCummEasting, Decimal rwDeltaEasting, Decimal rwDeltaVS, Decimal rwClsrDistance, Decimal rwClsrDirectAngle1, Decimal rwClsrDirectAngle2, Decimal rwDogleg, Decimal rwDogleg100, Decimal rwSubsea, Decimal rwMDTI, Decimal rwTvdTI, Decimal rwVSTI, Decimal rwNorthingTI, Decimal rwEastingTI, Decimal rwDeltaMDTI, Decimal rwD1TI, Decimal rwDogleg1TI, Decimal rwRFTI, Decimal rwDeltaTVDTI, Decimal rwCummNorthingTI, Decimal rwDeltaNorthTI, Decimal rwCummEastingTI, Decimal rwDeltaEastingTI, Decimal rwDeltaVSTI, Decimal rwClsrDistanceTI, Decimal rwClsrDirectAngle1TI, Decimal rwClsrDirectAngle2TI, Decimal rwDoglegTI, Decimal rwDogleg100TI, Decimal rwSubseaTI, Decimal scTVD, Decimal scVS, Decimal scNorthing, Decimal scEasting, Decimal scDeltaMD, Decimal scD1, Decimal scDogleg1, Decimal scRF, Decimal scDeltaTVD, Decimal scCummNorthing, Decimal scDeltaNorth, Decimal scCummEasting, Decimal scDeltaEasting, Decimal scDeltaVS, Decimal scClsrDistance, Decimal scClsrDirectAngle1, Decimal scClsrDirectAngle2, Decimal scDogleg, Decimal scDogleg100, Decimal scSubsea, Decimal scMDTI, Decimal scTvdTI, Decimal scVSTI, Decimal scNorthingTI, Decimal scEastingTI, Decimal scDeltaMDTI, Decimal scD1TI, Decimal scDogleg1TI, Decimal scRFTI, Decimal scDeltaTVDTI, Decimal scCummNorthingTI, Decimal scDeltaNorthTI, Decimal scCummEastingTI, Decimal scDeltaEastingTI, Decimal scDeltaVSTI, Decimal scClsrDistanceTI, Decimal scClsrDirectAngle1TI, Decimal scClsrDirectAngle2TI, Decimal scDoglegTI, Decimal scDogleg100TI, Decimal scSubseaTI, Decimal bhTVD, Decimal bhVS, Decimal bhNorthing, Decimal bhEasting, Decimal bhDeltaMD, Decimal bhD1, Decimal bhDogleg1, Decimal bhRF, Decimal bhDeltaTVD, Decimal bhCummNorthing, Decimal bhDeltaNorth, Decimal bhCummEasting, Decimal bhDeltaEasting, Decimal bhDeltaVS, Decimal bhClsrDistance, Decimal bhClsrDirectAngle1, Decimal bhClsrDirectAngle2, Decimal bhDogleg, Decimal bhDogleg100, Decimal bhSubsea, Decimal bhMDTI, Decimal bhTvdTI, Decimal bhVSTI, Decimal bhNorthingTI, Decimal bhEastingTI, Decimal bhDeltaMDTI, Decimal bhD1TI, Decimal bhDogleg1TI, Decimal bhRFTI, Decimal bhDeltaTVDTI, Decimal bhCummNorthingTI, Decimal bhDeltaNorthTI, Decimal bhCummEastingTI, Decimal bhDeltaEastingTI, Decimal bhDeltaVSTI, Decimal bhClsrDistanceTI, Decimal bhClsrDirectAngle1TI, Decimal bhClsrDirectAngle2TI, Decimal bhDoglegTI, Decimal bhDogleg100TI, Decimal bhSubseaTI, Decimal crTVD, Decimal crVS, Decimal crNorthing, Decimal crEasting, Decimal crDeltaMD, Decimal crD1, Decimal crDogleg1, Decimal crRF, Decimal crDeltaTVD, Decimal crCummNorthing, Decimal crDeltaNorth, Decimal crCummEasting, Decimal crDeltaEasting, Decimal crDeltaVS, Decimal crClsrDistance, Decimal crClsrDirectAngle1, Decimal crClsrDirectAngle2, Decimal crDogleg, Decimal crDogleg100, Decimal crSubsea, Decimal crMDTI, Decimal crTvdTI, Decimal crVSTI, Decimal crNorthingTI, Decimal crEastingTI, Decimal crDeltaMDTI, Decimal crD1TI, Decimal crDogleg1TI, Decimal crRFTI, Decimal crDeltaTVDTI, Decimal crCummNorthingTI, Decimal crDeltaNorthTI, Decimal crCummEastingTI, Decimal crDeltaEastingTI, Decimal crDeltaVSTI, Decimal crClsrDistanceTI, Decimal crClsrDirectAngle1TI, Decimal crClsrDirectAngle2TI, Decimal crDoglegTI, Decimal crDogleg100TI, Decimal crSubseaTI, Int32 AzimuthCriteriaID, String dbConn)
        {
            try
            {
                Int32 iResult = -99;
                String insertStatement = "INSERT INTO [RawQCResults] ([srvyID], [optrID], [clntID], [wpdID], [welID], [wswID] ,[runID] ,[sqcID] ,[srvID] ,[chkShot] ,[Depth] ,[colP] ,[colQ] ,[colS] ,[colT] ,[colV] ,[colW] ,[colY] ,[colZ] ,[colAA] ,[colAB] ,[colAC] ,[colAD] ,[colAE] ,[colAF] ,[colAH] ,[colAI] ,[colAJ] ,[colAK] ,[colAL] ,[colAM] ,[colAN] ,[colAO] ,[colAP] ,[colAR] ,[colAS] ,[colAT] ,[colAU] ,[colAV] ,[colAW] ,[colAX]  ,[colAY] ,[colBA] ,[colBB] ,[colBC] ,[colBD] ,[colBF] ,[colBG] ,[colBH] ,[colBI] ,[colBJ] ,[colBK] ,[colBM] ,[colBN] ,[colBO] ,[colBP] ,[colBQ] ,[colBR] ,[colBS] ,[colBT] ,[colBU] ,[colBV] ,[colBW] ,[colBX] ,[colBZ] ,[colCA] ,[colCB] ,[colCC] ,[colCD] ,[colCE] ,[colCF] ,[colCG] ,[colCH] ,[colCI] ,[colCK] ,[colCL] ,[colCM] ,[colCN] ,[colCO] ,[colCP] ,[colCQ] ,[colCR] ,[colCS] ,[colCT] ,[colCU] ,[colCV] ,[colCW] ,[colCX] ,[colCY] ,[colCZ] ,[colDA] ,[colDB] ,[colDC] ,[colDD] ,[colDE] ,[colDF] ,[colDG] ,[colDH] ,[colDI] ,[colDJ] ,[colDK] ,[colDM] ,[colDN] ,[colDO] ,[colDR] ,[colDW] ,[colDY] ,[colEA] ,[colEG] ,[colEI] ,[colEJ] ,[colEK] ,[colEL] ,[colEM] ,[colEN] ,[colEO] ,[colEP] ,[colEQ] ,[colER] ,[colET] ,[colEU] ,[colEV] ,[colEW] ,[colEX] ,[colEY] ,[colEZ] ,[colFA] ,[colFB] ,[colFC] ,[colFW] ,[colFX] ,[colFY] ,[colFZ] ,[colGA] ,[colGB] ,[colGC] ,[colGD] ,[colGE] ,[colGF] ,[colGH] ,[colGI] ,[colGJ] ,[colGK] ,[colGL] ,[colGM] ,[colGN] ,[colGO] ,[colGP] ,[colGR] ,[colGS] ,[colGT] ,[colGU] ,[colGV] ,[colGW] ,[colGX] ,[colGY] ,[colGZ] ,[colHA] ,[colHB] ,[colHC] ,[colHD] ,[colHE] ,[colHG] ,[colHH] ,[colHQ] ,[colHR] ,[colHS] ,[colHT] ,[colHV] ,[colHX] ,[colHY] ,[colHZ] ,[colIA] ,[colIB] , [colID] ,[colIF] ,[colIG] ,[colIH] ,[colII] ,[colIJ] ,[colIK] ,[colIL] ,[colIM] ,[colIN] ,[colIO], [taID] ,[NMSColD34] ,[NMSColD35] ,[NMSColD37] ,[NMSColD38] ,[NMSColD39] ,[NMSColG34] ,[NMSColG35] ,[NMSColG37] ,[NMSColG38] ,[rwTVD] ,[rwVS] ,[rwNorthing] ,[rwEasting] ,[rwDeltaMD] ,[rwD1] ,[rwDogleg1] ,[rwRF] ,[rwDeltaTVD] ,[rwCummNorthing] ,[rwDeltaNorth] ,[rwCummEasting] ,[rwDeltaEasting] ,[rwDeltaVS] ,[rwClsrDistance], [rwClsrDirectAngle1] ,[rwClsrDirectAngle2] ,[rwDogleg] ,[rwDogleg100] ,[rwSubsea] ,[rwMDTI] ,[rwTvdTI] ,[rwVSTI] ,[rwNorthingTI] ,[rwEastingTI] ,[rwDeltaMDTI] ,[rwD1TI] ,[rwDogleg1TI] ,[rwRFTI] ,[rwDeltaTVDTI] ,[rwCummNorthingTI] ,[rwDeltaNorthTI] ,[rwCummEastingTI] ,[rwDeltaEastingTI] ,[rwDeltaVSTI] ,[rwClsrDistanceTI] ,[rwClsrDirectAngle1TI] ,[rwClsrDirectAngle2TI] ,[rwDoglegTI] ,[rwDogleg100TI] ,[rwSubseaTI] ,[scTVD] ,[scVS] ,[scNorthing] ,[scEasting] ,[scDeltaMD] ,[scD1] ,[scDogleg1] ,[scRF] ,[scDeltaTVD] ,[scCummNorthing] ,[scDeltaNorth] ,[scCummEasting] ,[scDeltaEasting] ,[scDeltaVS] ,[scClsrDistance] ,[scClsrDirectAngle1] ,[scClsrDirectAngle2] ,[scDogleg] ,[scDogleg100] ,[scSubsea] ,[scMDTI] ,[scTvdTI] ,[scVSTI] ,[scNorthingTI] ,[scEastingTI] ,[scDeltaMDTI] ,[scD1TI] ,[scDogleg1TI] ,[scRFTI] ,[scDeltaTVDTI] ,[scCummNorthingTI] ,[scDeltaNorthTI] ,[scCummEastingTI] ,[scDeltaEastingTI] ,[scDeltaVSTI] ,[scClsrDistanceTI] ,[scClsrDirectAngle1TI] ,[scClsrDirectAngle2TI] ,[scDoglegTI] ,[scDogleg100TI] ,[scSubseaTI] ,[bhTVD] ,[bhVS] ,[bhNorthing] ,[bhEasting] ,[bhDeltaMD] ,[bhD1] ,[bhDogleg1] ,[bhRF] ,[bhDeltaTVD] ,[bhCummNorthing] ,[bhDeltaNorth] ,[bhCummEasting] ,[bhDeltaEasting] ,[bhDeltaVS] ,[bhClsrDistance] ,[bhClsrDirectAngle1] ,[bhClsrDirectAngle2] ,[bhDogleg] ,[bhDogleg100] ,[bhSubsea] ,[bhMDTI] ,[bhTvdTI] ,[bhVSTI] ,[bhNorthingTI] ,[bhEastingTI] ,[bhDeltaMDTI] ,[bhD1TI] ,[bhDogleg1TI] ,[bhRFTI] ,[bhDeltaTVDTI] ,[bhCummNorthingTI] ,[bhDeltaNorthTI] ,[bhCummEastingTI] ,[bhDeltaEastingTI] ,[bhDeltaVSTI] ,[bhClsrDistanceTI] ,[bhClsrDirectAngle1TI] ,[bhClsrDirectAngle2TI] ,[bhDoglegTI] ,[bhDogleg100TI] ,[bhSubseaTI] ,[crTVD] ,[crVS] ,[crNorthing] ,[crEasting] ,[crDeltaMD] ,[crD1] ,[crDogleg1] ,[crRF] ,[crDeltaTVD] ,[crCummNorthing] ,[crDeltaNorth] ,[crCummEasting] ,[crDeltaEasting] ,[crDeltaVS] ,[crClsrDistance] ,[crClsrDirectAngle1] ,[crClsrDirectAngle2] ,[crDogleg] ,[crDogleg100] ,[crSubsea] ,[crMDTI] ,[crTvdTI] ,[crVSTI] ,[crNorthingTI] ,[crEastingTI] ,[crDeltaMDTI] ,[crD1TI] ,[crDogleg1TI] ,[crRFTI] ,[crDeltaTVDTI] ,[crCummNorthingTI] ,[crDeltaNorthTI] ,[crCummEastingTI] ,[crDeltaEastingTI] ,[crDeltaVSTI] ,[crClsrDistanceTI] ,[crClsrDirectAngle1TI] ,[crClsrDirectAngle2TI] ,[crDoglegTI] ,[crDogleg100TI] ,[crSubseaTI] ,[pTime], [acID]) VALUES (@srvyID, @optrID, @clntID, @wpdID, @welID, @wswID, @runID, @sqcID, @srvID, @chkShot, @Depth, @colP, @colQ, @colS, @colT, @colV, @colW, @colY, @colZ, @colAA, @colAB, @colAC, @colAD, @colAE, @colAF, @colAH, @colAI, @colAJ, @colAK, @colAL, @colAM, @colAN, @colAO, @colAP, @colAR, @colAS, @colAT, @colAU, @colAV, @colAW, @colAX, @colAY, @colBA, @colBB, @colBC, @colBD, @colBF, @colBG, @colBH, @colBI, @colBJ, @colBK, @colBM, @colBN, @colBO, @colBP, @colBQ, @colBR, @colBS, @colBT, @colBU, @colBV, @colBW, @colBX, @colBZ, @colCA, @colCB, @colCC, @colCD, @colCE, @colCF, @colCG, @colCH, @colCI, @colCK, @colCL, @colCM, @colCN, @colCO, @colCP, @colCQ, @colCR, @colCS, @colCT, @colCU, @colCV, @colCW, @colCX, @colCY, @colCZ, @colDA, @colDB, @colDC, @colDD, @colDE, @colDF, @colDG, @colDH, @colDI, @colDJ, @colDK, @colDM, @colDN, @colDO, @colDR, @colDW, @colDY, @colEA, @colEG, @colEI, @colEJ, @colEK, @colEL, @colEM, @colEN, @colEO, @colEP, @colEQ, @colER, @colET, @colEU, @colEV, @colEW, @colEX, @colEY, @colEZ, @colFA, @colFB, @colFC, @colFW, @colFX, @colFY, @colFZ, @colGA, @colGB, @colGC, @colGD, @colGE, @colGF, @colGH, @colGI, @colGJ, @colGK, @colGL, @colGM, @colGN, @colGO, @colGP, @colGR, @colGS, @colGT, @colGU, @colGV, @colGW, @colGX, @colGY, @colGZ, @colHA, @colHB, @colHC, @colHD, @colHE, @colHG, @colHH, @colHQ, @colHR, @colHS, @colHT, @colHV, @colHX, @colHY, @colHZ, @colIA, @colIB, @colID, @colIF, @colIG, @colIH, @colII, @colIJ, @colIK, @colIL, @colIM, @colIN, @colIO, @taID, @NMSColD34, @NMSColD35, @NMSColD37, @NMSColD38, @NMSColD39, @NMSColG34, @NMSColG35, @NMSColG37, @NMSColG38, @rwTVD, @rwVS, @rwNorthing, @rwEasting, @rwDeltaMD, @rwD1, @rwDogleg1, @rwRF, @rwDeltaTVD, @rwCummNorthing, @rwDeltaNorth, @rwCummEasting, @rwDeltaEasting, @rwDeltaVS, @rwClsrDistance, @rwClsrDirectAngle1, @rwClsrDirectAngle2, @rwDogleg, @rwDogleg100, @rwSubsea, @rwMDTI, @rwTvdTI, @rwVSTI, @rwNorthingTI, @rwEastingTI, @rwDeltaMDTI, @rwD1TI, @rwDogleg1TI, @rwRFTI, @rwDeltaTVDTI, @rwCummNorthingTI, @rwDeltaNorthTI, @rwCummEastingTI, @rwDeltaEastingTI, @rwDeltaVSTI, @rwClsrDistanceTI, @rwClsrDirectAngle1TI, @rwClsrDirectAngle2TI, @rwDoglegTI, @rwDogleg100TI, @rwSubseaTI, @scTVD, @scVS, @scNorthing, @scEasting, @scDeltaMD, @scD1, @scDogleg1, @scRF, @scDeltaTVD, @scCummNorthing, @scDeltaNorth, @scCummEasting, @scDeltaEasting, @scDeltaVS, @scClsrDistance, @scClsrDirectAngle1, @scClsrDirectAngle2, @scDogleg, @scDogleg100, @scSubsea, @scMDTI, @scTvdTI, @scVSTI, @scNorthingTI, @scEastingTI, @scDeltaMDTI, @scD1TI, @scDogleg1TI, @scRFTI, @scDeltaTVDTI, @scCummNorthingTI, @scDeltaNorthTI, @scCummEastingTI, @scDeltaEastingTI, @scDeltaVSTI, @scClsrDistanceTI, @scClsrDirectAngle1TI, @scClsrDirectAngle2TI, @scDoglegTI, @scDogleg100TI, @scSubseaTI, @bhTVD, @bhVS, @bhNorthing, @bhEasting, @bhDeltaMD, @bhD1, @bhDogleg1, @bhRF, @bhDeltaTVD, @bhCummNorthing, @bhDeltaNorth, @bhCummEasting, @bhDeltaEasting, @bhDeltaVS, @bhClsrDistance, @bhClsrDirectAngle1, @bhClsrDirectAngle2, @bhDogleg, @bhDogleg100, @bhSubsea, @bhMDTI, @bhTvdTI, @bhVSTI, @bhNorthingTI, @bhEastingTI, @bhDeltaMDTI, @bhD1TI, @bhDogleg1TI, @bhRFTI, @bhDeltaTVDTI, @bhCummNorthingTI, @bhDeltaNorthTI, @bhCummEastingTI, @bhDeltaEastingTI, @bhDeltaVSTI, @bhClsrDistanceTI, @bhClsrDirectAngle1TI, @bhClsrDirectAngle2TI, @bhDoglegTI, @bhDogleg100TI, @bhSubseaTI, @crTVD, @crVS, @crNorthing, @crEasting, @crDeltaMD, @crD1, @crDogleg1, @crRF, @crDeltaTVD, @crCummNorthing, @crDeltaNorth, @crCummEasting, @crDeltaEasting, @crDeltaVS, @crClsrDistance, @crClsrDirectAngle1, @crClsrDirectAngle2, @crDogleg, @crDogleg100, @crSubsea, @crMDTI, @crTvdTI, @crVSTI, @crNorthingTI, @crEastingTI, @crDeltaMDTI, @crD1TI, @crDogleg1TI, @crRFTI, @crDeltaTVDTI, @crCummNorthingTI, @crDeltaNorthTI, @crCummEastingTI, @crDeltaEastingTI, @crDeltaVSTI, @crClsrDistanceTI, @crClsrDirectAngle1TI, @crClsrDirectAngle2TI, @crDoglegTI, @crDogleg100TI, @crSubseaTI, @pTime, @acID)";
                using (SqlConnection clntConn = new SqlConnection(dbConn))
                {
                    try
                    {
                        clntConn.Open();

                        SqlCommand insertRow = new SqlCommand(insertStatement, clntConn);

                        insertRow.Parameters.AddWithValue("@srvyID", SqlDbType.Int).Value = SurveyRowID.ToString();
                        insertRow.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        insertRow.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        insertRow.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        insertRow.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        insertRow.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        insertRow.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        insertRow.Parameters.AddWithValue("@sqcID", SqlDbType.Int).Value = SQCID.ToString();
                        insertRow.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceTypeID.ToString();
                        insertRow.Parameters.AddWithValue("@chkShot", SqlDbType.Int).Value = CheckShot.ToString();
                        insertRow.Parameters.AddWithValue("@Depth", SqlDbType.Decimal).Value = Depth.ToString();
                        insertRow.Parameters.AddWithValue("@colP", SqlDbType.Decimal).Value = colPVal.ToString();
                        insertRow.Parameters.AddWithValue("@colQ", SqlDbType.Decimal).Value = colQVal.ToString();
                        insertRow.Parameters.AddWithValue("@colS", SqlDbType.Decimal).Value = colSVal.ToString();
                        insertRow.Parameters.AddWithValue("@colT", SqlDbType.Decimal).Value = colTVal.ToString();
                        insertRow.Parameters.AddWithValue("@colV", SqlDbType.Int).Value = colVVal.ToString();
                        insertRow.Parameters.AddWithValue("@colW", SqlDbType.Decimal).Value = colWVal.ToString();
                        insertRow.Parameters.AddWithValue("@colY", SqlDbType.Decimal).Value = colYVal.ToString();
                        insertRow.Parameters.AddWithValue("@colZ", SqlDbType.Decimal).Value = colZVal.ToString();
                        insertRow.Parameters.AddWithValue("@colAA", SqlDbType.Decimal).Value = colAAVal.ToString();
                        insertRow.Parameters.AddWithValue("@colAB", SqlDbType.Decimal).Value = colABVal.ToString();
                        insertRow.Parameters.AddWithValue("@colAC", SqlDbType.Decimal).Value = colACVal.ToString();
                        insertRow.Parameters.AddWithValue("@colAD", SqlDbType.Decimal).Value = colADVal.ToString();
                        insertRow.Parameters.AddWithValue("@colAE", SqlDbType.Decimal).Value = colAEVal.ToString();
                        insertRow.Parameters.AddWithValue("@colAF", SqlDbType.Decimal).Value = colAFVal.ToString();
                        insertRow.Parameters.AddWithValue("@colAH", SqlDbType.Decimal).Value = colAHVal.ToString();
                        insertRow.Parameters.AddWithValue("@colAI", SqlDbType.Decimal).Value = colAIVal.ToString();
                        insertRow.Parameters.AddWithValue("@colAJ", SqlDbType.Decimal).Value = colAJVal.ToString();
                        insertRow.Parameters.AddWithValue("@colAK", SqlDbType.Decimal).Value = colAKVal.ToString();
                        insertRow.Parameters.AddWithValue("@colAL", SqlDbType.Decimal).Value = colALVal.ToString();
                        insertRow.Parameters.AddWithValue("@colAM", SqlDbType.Decimal).Value = colAMVal.ToString();
                        insertRow.Parameters.AddWithValue("@colAN", SqlDbType.Decimal).Value = colANVal.ToString();
                        insertRow.Parameters.AddWithValue("@colAO", SqlDbType.Decimal).Value = colAOVal.ToString();
                        insertRow.Parameters.AddWithValue("@colAP", SqlDbType.Decimal).Value = colAPVal.ToString();
                        insertRow.Parameters.AddWithValue("@colAR", SqlDbType.Decimal).Value = colARVal.ToString();
                        insertRow.Parameters.AddWithValue("@colAS", SqlDbType.Decimal).Value = colASVal.ToString();
                        insertRow.Parameters.AddWithValue("@colAT", SqlDbType.Decimal).Value = colATVal.ToString();
                        insertRow.Parameters.AddWithValue("@colAU", SqlDbType.Decimal).Value = colAUVal.ToString();
                        insertRow.Parameters.AddWithValue("@colAV", SqlDbType.Int).Value = colAVVal.ToString();
                        insertRow.Parameters.AddWithValue("@colAW", SqlDbType.Int).Value = colAWVal.ToString();
                        insertRow.Parameters.AddWithValue("@colAX", SqlDbType.Int).Value = colAXVal.ToString();
                        insertRow.Parameters.AddWithValue("@colAY", SqlDbType.Int).Value = colAYVal.ToString();
                        insertRow.Parameters.AddWithValue("@colBA", SqlDbType.Decimal).Value = colBAVal.ToString();
                        insertRow.Parameters.AddWithValue("@colBB", SqlDbType.Decimal).Value = colBBVal.ToString();
                        insertRow.Parameters.AddWithValue("@colBC", SqlDbType.Decimal).Value = colBCVal.ToString();
                        insertRow.Parameters.AddWithValue("@colBD", SqlDbType.Int).Value = colBDVal.ToString();
                        insertRow.Parameters.AddWithValue("@colBF", SqlDbType.Decimal).Value = colBFVal.ToString();
                        insertRow.Parameters.AddWithValue("@colBG", SqlDbType.Decimal).Value = colBGVal.ToString();
                        insertRow.Parameters.AddWithValue("@colBH", SqlDbType.Decimal).Value = colBHVal.ToString();
                        insertRow.Parameters.AddWithValue("@colBI", SqlDbType.Decimal).Value = colBIVal.ToString();
                        insertRow.Parameters.AddWithValue("@colBJ", SqlDbType.Decimal).Value = colBJVal.ToString();
                        insertRow.Parameters.AddWithValue("@colBK", SqlDbType.Decimal).Value = colBKVal.ToString();
                        insertRow.Parameters.AddWithValue("@colBM", SqlDbType.Decimal).Value = colBMVal.ToString();
                        insertRow.Parameters.AddWithValue("@colBN", SqlDbType.Decimal).Value = colBNVal.ToString();
                        insertRow.Parameters.AddWithValue("@colBO", SqlDbType.Decimal).Value = colBOVal.ToString();
                        insertRow.Parameters.AddWithValue("@colBP", SqlDbType.Decimal).Value = colBPVal.ToString();
                        insertRow.Parameters.AddWithValue("@colBQ", SqlDbType.Decimal).Value = colBQVal.ToString();
                        insertRow.Parameters.AddWithValue("@colBR", SqlDbType.Decimal).Value = colBRVal.ToString();
                        insertRow.Parameters.AddWithValue("@colBS", SqlDbType.Decimal).Value = colBSVal.ToString();
                        insertRow.Parameters.AddWithValue("@colBT", SqlDbType.Decimal).Value = colBTVal.ToString();
                        insertRow.Parameters.AddWithValue("@colBU", SqlDbType.Decimal).Value = colBUVal.ToString();
                        insertRow.Parameters.AddWithValue("@colBV", SqlDbType.Decimal).Value = colBVVal.ToString();
                        insertRow.Parameters.AddWithValue("@colBW", SqlDbType.Decimal).Value = colBWVal.ToString();
                        insertRow.Parameters.AddWithValue("@colBX", SqlDbType.Decimal).Value = colBXVal.ToString();
                        insertRow.Parameters.AddWithValue("@colBZ", SqlDbType.Decimal).Value = colBZVal.ToString();
                        insertRow.Parameters.AddWithValue("@colCA", SqlDbType.Decimal).Value = colCAVal.ToString();
                        insertRow.Parameters.AddWithValue("@colCB", SqlDbType.Decimal).Value = colCBVal.ToString();
                        insertRow.Parameters.AddWithValue("@colCC", SqlDbType.Int).Value = colCCVal.ToString();
                        insertRow.Parameters.AddWithValue("@colCD", SqlDbType.Decimal).Value = colCDVal.ToString();
                        insertRow.Parameters.AddWithValue("@colCE", SqlDbType.Decimal).Value = colCEVal.ToString();
                        insertRow.Parameters.AddWithValue("@colCF", SqlDbType.Decimal).Value = colCFVal.ToString();
                        insertRow.Parameters.AddWithValue("@colCG", SqlDbType.Decimal).Value = colCGVal.ToString();
                        insertRow.Parameters.AddWithValue("@colCH", SqlDbType.Decimal).Value = colCHVal.ToString();
                        insertRow.Parameters.AddWithValue("@colCI", SqlDbType.Int).Value = colCIVal.ToString();
                        insertRow.Parameters.AddWithValue("@colCK", SqlDbType.Decimal).Value = colCKVal.ToString();
                        insertRow.Parameters.AddWithValue("@colCL", SqlDbType.Decimal).Value = colCLVal.ToString();
                        insertRow.Parameters.AddWithValue("@colCM", SqlDbType.Decimal).Value = colCMVal.ToString();
                        insertRow.Parameters.AddWithValue("@colCN", SqlDbType.Decimal).Value = colCNVal.ToString();
                        insertRow.Parameters.AddWithValue("@colCO", SqlDbType.Decimal).Value = colCOVal.ToString();
                        insertRow.Parameters.AddWithValue("@colCP", SqlDbType.Decimal).Value = colCPVal.ToString();
                        insertRow.Parameters.AddWithValue("@colCQ", SqlDbType.Decimal).Value = colCQVal.ToString();
                        insertRow.Parameters.AddWithValue("@colCR", SqlDbType.Decimal).Value = colCRVal.ToString();
                        insertRow.Parameters.AddWithValue("@colCS", SqlDbType.Decimal).Value = colCSVal.ToString();
                        insertRow.Parameters.AddWithValue("@colCT", SqlDbType.Decimal).Value = colCTVal.ToString();
                        insertRow.Parameters.AddWithValue("@colCU", SqlDbType.Decimal).Value = colCUVal.ToString();
                        insertRow.Parameters.AddWithValue("@colCV", SqlDbType.Decimal).Value = colCVVal.ToString();
                        insertRow.Parameters.AddWithValue("@colCW", SqlDbType.Decimal).Value = colCWVal.ToString();
                        insertRow.Parameters.AddWithValue("@colCX", SqlDbType.Decimal).Value = colCXVal.ToString();
                        insertRow.Parameters.AddWithValue("@colCY", SqlDbType.Decimal).Value = colCYVal.ToString();
                        insertRow.Parameters.AddWithValue("@colCZ", SqlDbType.Decimal).Value = colCZVal.ToString();
                        insertRow.Parameters.AddWithValue("@colDA", SqlDbType.Decimal).Value = colDAVal.ToString();
                        insertRow.Parameters.AddWithValue("@colDB", SqlDbType.Decimal).Value = colDBVal.ToString();
                        insertRow.Parameters.AddWithValue("@colDC", SqlDbType.Decimal).Value = colDCVal.ToString();
                        insertRow.Parameters.AddWithValue("@colDD", SqlDbType.Decimal).Value = colDDVal.ToString();
                        insertRow.Parameters.AddWithValue("@colDE", SqlDbType.Decimal).Value = colDEVal.ToString();
                        insertRow.Parameters.AddWithValue("@colDF", SqlDbType.Int).Value = colDFVal.ToString();
                        insertRow.Parameters.AddWithValue("@colDG", SqlDbType.Int).Value = colDGVal.ToString();
                        insertRow.Parameters.AddWithValue("@colDH", SqlDbType.Int).Value = colDHVal.ToString();
                        insertRow.Parameters.AddWithValue("@colDI", SqlDbType.Decimal).Value = colDIVal.ToString();
                        insertRow.Parameters.AddWithValue("@colDJ", SqlDbType.Int).Value = colDJVal.ToString();
                        insertRow.Parameters.AddWithValue("@colDK", SqlDbType.Decimal).Value = colDKVal.ToString();
                        insertRow.Parameters.AddWithValue("@colDM", SqlDbType.Int).Value = SurveyRowID.ToString();
                        insertRow.Parameters.AddWithValue("@colDN", SqlDbType.NVarChar).Value = srDate.ToString();
                        insertRow.Parameters.AddWithValue("@colDO", SqlDbType.NVarChar).Value = srTime.ToString();
                        insertRow.Parameters.AddWithValue("@colDR", SqlDbType.Decimal).Value = colDRVal.ToString();
                        insertRow.Parameters.AddWithValue("@colDW", SqlDbType.Int).Value = colDWVal.ToString();
                        insertRow.Parameters.AddWithValue("@colDY", SqlDbType.Decimal).Value = colDYVal.ToString();
                        insertRow.Parameters.AddWithValue("@colEA", SqlDbType.Decimal).Value = colEAVal.ToString();
                        insertRow.Parameters.AddWithValue("@colEG", SqlDbType.Decimal).Value = colEGVal.ToString();
                        insertRow.Parameters.AddWithValue("@colEI", SqlDbType.Decimal).Value = colEIVal.ToString();
                        insertRow.Parameters.AddWithValue("@colEJ", SqlDbType.Int).Value = colEJVal.ToString();
                        insertRow.Parameters.AddWithValue("@colEK", SqlDbType.Int).Value = colEKVal.ToString();
                        insertRow.Parameters.AddWithValue("@colEL", SqlDbType.Decimal).Value = colGPVal.ToString();
                        insertRow.Parameters.AddWithValue("@colEM", SqlDbType.Decimal).Value = colGNVal.ToString();
                        insertRow.Parameters.AddWithValue("@colEN", SqlDbType.Decimal).Value = colGOVal.ToString();
                        insertRow.Parameters.AddWithValue("@colEO", SqlDbType.Int).Value = colEOVal.ToString();
                        insertRow.Parameters.AddWithValue("@colEP", SqlDbType.Decimal).Value = colGYVal.ToString();
                        insertRow.Parameters.AddWithValue("@colEQ", SqlDbType.Decimal).Value = colEQVal.ToString();
                        insertRow.Parameters.AddWithValue("@colER", SqlDbType.Decimal).Value = colERVal.ToString();
                        insertRow.Parameters.AddWithValue("@colET", SqlDbType.Int).Value = colETVal.ToString();
                        insertRow.Parameters.AddWithValue("@colEU", SqlDbType.Decimal).Value = colEUVal.ToString();
                        insertRow.Parameters.AddWithValue("@colEV", SqlDbType.Decimal).Value = colEVVal.ToString();
                        insertRow.Parameters.AddWithValue("@colEW", SqlDbType.Decimal).Value = colEWVal.ToString();
                        insertRow.Parameters.AddWithValue("@colEX", SqlDbType.Int).Value = colEXVal.ToString();
                        insertRow.Parameters.AddWithValue("@colEY", SqlDbType.Int).Value = colEYVal.ToString();
                        insertRow.Parameters.AddWithValue("@colEZ", SqlDbType.Int).Value = colEZVal.ToString();
                        insertRow.Parameters.AddWithValue("@colFA", SqlDbType.Decimal).Value = colFAVal.ToString();
                        insertRow.Parameters.AddWithValue("@colFB", SqlDbType.Decimal).Value = colFBVal.ToString();
                        insertRow.Parameters.AddWithValue("@colFC", SqlDbType.Decimal).Value = colFCVal.ToString();
                        insertRow.Parameters.AddWithValue("@colFW", SqlDbType.Int).Value = colFWVal.ToString();
                        insertRow.Parameters.AddWithValue("@colFX", SqlDbType.Int).Value = colFXVal.ToString();
                        insertRow.Parameters.AddWithValue("@colFY", SqlDbType.Decimal).Value = colFYVal.ToString();
                        insertRow.Parameters.AddWithValue("@colFZ", SqlDbType.Decimal).Value = colFZVal.ToString();
                        insertRow.Parameters.AddWithValue("@colGA", SqlDbType.Decimal).Value = colGAVal.ToString();
                        insertRow.Parameters.AddWithValue("@colGB", SqlDbType.Decimal).Value = colGBVal.ToString();
                        insertRow.Parameters.AddWithValue("@colGC", SqlDbType.Decimal).Value = colGCVal.ToString();
                        insertRow.Parameters.AddWithValue("@colGD", SqlDbType.Decimal).Value = colGDVal.ToString();
                        insertRow.Parameters.AddWithValue("@colGE", SqlDbType.Decimal).Value = colGEVal.ToString();
                        insertRow.Parameters.AddWithValue("@colGF", SqlDbType.Int).Value = colGFVal.ToString();
                        insertRow.Parameters.AddWithValue("@colGH", SqlDbType.Decimal).Value = colGHVal.ToString();
                        insertRow.Parameters.AddWithValue("@colGI", SqlDbType.Decimal).Value = colGIVal.ToString();
                        insertRow.Parameters.AddWithValue("@colGJ", SqlDbType.Decimal).Value = colGJVal.ToString();
                        insertRow.Parameters.AddWithValue("@colGK", SqlDbType.Decimal).Value = colGKVal.ToString();
                        insertRow.Parameters.AddWithValue("@colGL", SqlDbType.Int).Value = colGLVal.ToString();
                        insertRow.Parameters.AddWithValue("@colGM", SqlDbType.Decimal).Value = colGMVal.ToString();
                        insertRow.Parameters.AddWithValue("@colGN", SqlDbType.Decimal).Value = colGNVal.ToString();
                        insertRow.Parameters.AddWithValue("@colGO", SqlDbType.Decimal).Value = colGOVal.ToString();
                        insertRow.Parameters.AddWithValue("@colGP", SqlDbType.Decimal).Value = colGPVal.ToString();
                        insertRow.Parameters.AddWithValue("@colGR", SqlDbType.Decimal).Value = colGRVal.ToString();
                        insertRow.Parameters.AddWithValue("@colGS", SqlDbType.Decimal).Value = colGSVal.ToString();
                        insertRow.Parameters.AddWithValue("@colGT", SqlDbType.Decimal).Value = colGTVal.ToString();
                        insertRow.Parameters.AddWithValue("@colGU", SqlDbType.Decimal).Value = colGUVal.ToString();
                        insertRow.Parameters.AddWithValue("@colGV", SqlDbType.Decimal).Value = colGVVal.ToString();
                        insertRow.Parameters.AddWithValue("@colGW", SqlDbType.Decimal).Value = colGWVal.ToString();
                        insertRow.Parameters.AddWithValue("@colGX", SqlDbType.Decimal).Value = colGXVal.ToString();
                        insertRow.Parameters.AddWithValue("@colGY", SqlDbType.Decimal).Value = colGYVal.ToString();
                        insertRow.Parameters.AddWithValue("@colGZ", SqlDbType.Decimal).Value = colGZVal.ToString();
                        insertRow.Parameters.AddWithValue("@colHA", SqlDbType.Decimal).Value = colHAVal.ToString();
                        insertRow.Parameters.AddWithValue("@colHB", SqlDbType.Int).Value = colHBVal.ToString();
                        insertRow.Parameters.AddWithValue("@colHC", SqlDbType.Int).Value = colHCVal.ToString();
                        insertRow.Parameters.AddWithValue("@colHD", SqlDbType.Int).Value = colHDVal.ToString();
                        insertRow.Parameters.AddWithValue("@colHE", SqlDbType.Int).Value = colCIVal.ToString();
                        insertRow.Parameters.AddWithValue("@colHG", SqlDbType.Int).Value = colHGVal.ToString();
                        insertRow.Parameters.AddWithValue("@colHH", SqlDbType.Int).Value = colHHVal.ToString();
                        insertRow.Parameters.AddWithValue("@colHQ", SqlDbType.Decimal).Value = colHQVal.ToString();
                        insertRow.Parameters.AddWithValue("@colHR", SqlDbType.Decimal).Value = colHRVal.ToString();
                        insertRow.Parameters.AddWithValue("@colHS", SqlDbType.Decimal).Value = colHSVal.ToString();
                        insertRow.Parameters.AddWithValue("@colHT", SqlDbType.Decimal).Value = colHTVal.ToString();
                        insertRow.Parameters.AddWithValue("@colHV", SqlDbType.Int).Value = colHVVal.ToString();
                        insertRow.Parameters.AddWithValue("@colHX", SqlDbType.Decimal).Value = colHXVal.ToString();
                        insertRow.Parameters.AddWithValue("@colHY", SqlDbType.Decimal).Value = colHYVal.ToString();
                        insertRow.Parameters.AddWithValue("@colHZ", SqlDbType.Decimal).Value = colHZVal.ToString();
                        insertRow.Parameters.AddWithValue("@colIA", SqlDbType.Decimal).Value = colIAVal.ToString();
                        insertRow.Parameters.AddWithValue("@colIB", SqlDbType.Decimal).Value = colIBVal.ToString();
                        insertRow.Parameters.AddWithValue("@colID", SqlDbType.Decimal).Value = colIDVal.ToString();
                        insertRow.Parameters.AddWithValue("@colIF", SqlDbType.Decimal).Value = colIFVal.ToString();
                        insertRow.Parameters.AddWithValue("@colIG", SqlDbType.Decimal).Value = colIGVal.ToString();
                        insertRow.Parameters.AddWithValue("@colIH", SqlDbType.Decimal).Value = colIHVal.ToString();
                        insertRow.Parameters.AddWithValue("@colII", SqlDbType.Decimal).Value = colIIVal.ToString();
                        insertRow.Parameters.AddWithValue("@colIJ", SqlDbType.Decimal).Value = colIJVal.ToString();
                        insertRow.Parameters.AddWithValue("@colIK", SqlDbType.Decimal).Value = colIKVal.ToString();
                        insertRow.Parameters.AddWithValue("@colIL", SqlDbType.Decimal).Value = colILVal.ToString();
                        insertRow.Parameters.AddWithValue("@colIM", SqlDbType.Decimal).Value = colIMVal.ToString();
                        insertRow.Parameters.AddWithValue("@colIN", SqlDbType.Decimal).Value = colINVal.ToString();
                        insertRow.Parameters.AddWithValue("@colIO", SqlDbType.Decimal).Value = colIOVal.ToString();
                        insertRow.Parameters.AddWithValue("@taID", SqlDbType.Int).Value = taID.ToString();                        
                        insertRow.Parameters.AddWithValue("@NMSColD34", SqlDbType.Decimal).Value = stdDCInterference.ToString();
                        insertRow.Parameters.AddWithValue("@NMSColD35", SqlDbType.Decimal).Value = stdMtrInterference.ToString();
                        insertRow.Parameters.AddWithValue("@NMSColD37", SqlDbType.Decimal).Value = stdTTLBHAInterference.ToString();
                        insertRow.Parameters.AddWithValue("@NMSColD38", SqlDbType.Decimal).Value = stdLCAZErrorBHA.ToString();
                        insertRow.Parameters.AddWithValue("@NMSColD39", SqlDbType.Decimal).Value = currBitToSensor.ToString();
                        insertRow.Parameters.AddWithValue("@NMSColG34", SqlDbType.Decimal).Value = gmtrDCInterference.ToString();
                        insertRow.Parameters.AddWithValue("@NMSColG35", SqlDbType.Decimal).Value = gmtrMtrInterference.ToString();
                        insertRow.Parameters.AddWithValue("@NMSColG37", SqlDbType.Decimal).Value = gmtrTTLBHAInterference.ToString();
                        insertRow.Parameters.AddWithValue("@NMSColG38", SqlDbType.Decimal).Value = gmtrLCAZErrorBHA.ToString();
                        insertRow.Parameters.AddWithValue("@rwTVD", SqlDbType.Decimal).Value = rwTVD.ToString();
                        insertRow.Parameters.AddWithValue("@rwVS", SqlDbType.Decimal).Value = rwVS.ToString();
                        insertRow.Parameters.AddWithValue("@rwNorthing", SqlDbType.Decimal).Value = rwNorthing.ToString();
                        insertRow.Parameters.AddWithValue("@rwEasting", SqlDbType.Decimal).Value = rwEasting.ToString();
                        insertRow.Parameters.AddWithValue("@rwDeltaMD", SqlDbType.Decimal).Value = rwDeltaMD.ToString();
                        insertRow.Parameters.AddWithValue("@rwD1", SqlDbType.Decimal).Value = rwD1.ToString();
                        insertRow.Parameters.AddWithValue("@rwDogleg1", SqlDbType.Decimal).Value = rwDogleg1.ToString();
                        insertRow.Parameters.AddWithValue("@rwRF", SqlDbType.Decimal).Value = rwRF.ToString();
                        insertRow.Parameters.AddWithValue("@rwDeltaTVD", SqlDbType.Decimal).Value = rwDeltaTVD.ToString();
                        insertRow.Parameters.AddWithValue("@rwCummNorthing", SqlDbType.Decimal).Value = rwCummNorthing.ToString();
                        insertRow.Parameters.AddWithValue("@rwDeltaNorth", SqlDbType.Decimal).Value = rwDeltaNorth.ToString();
                        insertRow.Parameters.AddWithValue("@rwCummEasting", SqlDbType.Decimal).Value = rwCummEasting.ToString();
                        insertRow.Parameters.AddWithValue("@rwDeltaEasting", SqlDbType.Decimal).Value = rwDeltaEasting.ToString();
                        insertRow.Parameters.AddWithValue("@rwDeltaVS", SqlDbType.Decimal).Value = rwDeltaVS.ToString();
                        insertRow.Parameters.AddWithValue("@rwClsrDistance", SqlDbType.Decimal).Value = rwClsrDistance.ToString();
                        insertRow.Parameters.AddWithValue("@rwClsrDirectAngle1", SqlDbType.Decimal).Value = rwClsrDirectAngle1.ToString();
                        insertRow.Parameters.AddWithValue("@rwClsrDirectAngle2", SqlDbType.Decimal).Value = rwClsrDirectAngle2.ToString();
                        insertRow.Parameters.AddWithValue("@rwDogleg", SqlDbType.Decimal).Value = rwDogleg.ToString();
                        insertRow.Parameters.AddWithValue("@rwDogleg100", SqlDbType.Decimal).Value = rwDogleg100.ToString();
                        insertRow.Parameters.AddWithValue("@rwSubsea", SqlDbType.Decimal).Value = rwSubsea.ToString();
                        insertRow.Parameters.AddWithValue("@rwMDTI", SqlDbType.Decimal).Value = rwMDTI.ToString();
                        insertRow.Parameters.AddWithValue("@rwTvdTI", SqlDbType.Decimal).Value = rwTvdTI.ToString();
                        insertRow.Parameters.AddWithValue("@rwVSTI", SqlDbType.Decimal).Value = rwVSTI.ToString();
                        insertRow.Parameters.AddWithValue("@rwNorthingTI", SqlDbType.Decimal).Value = rwNorthingTI.ToString();
                        insertRow.Parameters.AddWithValue("@rwEastingTI", SqlDbType.Decimal).Value = rwEastingTI.ToString();
                        insertRow.Parameters.AddWithValue("@rwDeltaMDTI", SqlDbType.Decimal).Value = rwDeltaMDTI.ToString();
                        insertRow.Parameters.AddWithValue("@rwD1TI", SqlDbType.Decimal).Value = rwD1TI.ToString();
                        insertRow.Parameters.AddWithValue("@rwDogleg1TI", SqlDbType.Decimal).Value = rwDogleg1TI.ToString();
                        insertRow.Parameters.AddWithValue("@rwRFTI", SqlDbType.Decimal).Value = rwRFTI.ToString();
                        insertRow.Parameters.AddWithValue("@rwDeltaTVDTI", SqlDbType.Decimal).Value = rwDeltaTVDTI.ToString();
                        insertRow.Parameters.AddWithValue("@rwCummNorthingTI", SqlDbType.Decimal).Value = rwCummNorthingTI.ToString();
                        insertRow.Parameters.AddWithValue("@rwDeltaNorthTI", SqlDbType.Decimal).Value = rwDeltaNorthTI.ToString();
                        insertRow.Parameters.AddWithValue("@rwCummEastingTI", SqlDbType.Decimal).Value = rwCummEastingTI.ToString();
                        insertRow.Parameters.AddWithValue("@rwDeltaEastingTI", SqlDbType.Decimal).Value = rwDeltaEastingTI.ToString();
                        insertRow.Parameters.AddWithValue("@rwDeltaVSTI", SqlDbType.Decimal).Value = rwDeltaVSTI.ToString();
                        insertRow.Parameters.AddWithValue("@rwClsrDistanceTI", SqlDbType.Decimal).Value = rwClsrDistanceTI.ToString();
                        insertRow.Parameters.AddWithValue("@rwClsrDirectAngle1TI", SqlDbType.Decimal).Value = rwClsrDirectAngle1TI.ToString();
                        insertRow.Parameters.AddWithValue("@rwClsrDirectAngle2TI", SqlDbType.Decimal).Value = rwClsrDirectAngle2TI.ToString();
                        insertRow.Parameters.AddWithValue("@rwDoglegTI", SqlDbType.Decimal).Value = rwDoglegTI.ToString();
                        insertRow.Parameters.AddWithValue("@rwDogleg100TI", SqlDbType.Decimal).Value = rwDogleg100TI.ToString();
                        insertRow.Parameters.AddWithValue("@rwSubseaTI", SqlDbType.Decimal).Value = rwSubseaTI.ToString();
                        insertRow.Parameters.AddWithValue("@scTVD", SqlDbType.Decimal).Value = scTVD.ToString();
                        insertRow.Parameters.AddWithValue("@scVS", SqlDbType.Decimal).Value = scVS.ToString();
                        insertRow.Parameters.AddWithValue("@scNorthing", SqlDbType.Decimal).Value = scNorthing.ToString();
                        insertRow.Parameters.AddWithValue("@scEasting", SqlDbType.Decimal).Value = scEasting.ToString();
                        insertRow.Parameters.AddWithValue("@scDeltaMD", SqlDbType.Decimal).Value = scDeltaMD.ToString();
                        insertRow.Parameters.AddWithValue("@scD1", SqlDbType.Decimal).Value = scD1.ToString();
                        insertRow.Parameters.AddWithValue("@scDogleg1", SqlDbType.Decimal).Value = scDogleg1.ToString();
                        insertRow.Parameters.AddWithValue("@scRF", SqlDbType.Decimal).Value = scRF.ToString();
                        insertRow.Parameters.AddWithValue("@scDeltaTVD", SqlDbType.Decimal).Value = scDeltaTVD.ToString();
                        insertRow.Parameters.AddWithValue("@scCummNorthing", SqlDbType.Decimal).Value = scCummNorthing.ToString();
                        insertRow.Parameters.AddWithValue("@scDeltaNorth", SqlDbType.Decimal).Value = scDeltaNorth.ToString();
                        insertRow.Parameters.AddWithValue("@scCummEasting", SqlDbType.Decimal).Value = scCummEasting.ToString();
                        insertRow.Parameters.AddWithValue("@scDeltaEasting", SqlDbType.Decimal).Value = scDeltaEasting.ToString();
                        insertRow.Parameters.AddWithValue("@scDeltaVS", SqlDbType.Decimal).Value = scDeltaVS.ToString();
                        insertRow.Parameters.AddWithValue("@scClsrDistance", SqlDbType.Decimal).Value = scClsrDistance.ToString();
                        insertRow.Parameters.AddWithValue("@scClsrDirectAngle1", SqlDbType.Decimal).Value = scClsrDirectAngle1.ToString();
                        insertRow.Parameters.AddWithValue("@scClsrDirectAngle2", SqlDbType.Decimal).Value = scClsrDirectAngle2.ToString();
                        insertRow.Parameters.AddWithValue("@scDogleg", SqlDbType.Decimal).Value = scDogleg.ToString();
                        insertRow.Parameters.AddWithValue("@scDogleg100", SqlDbType.Decimal).Value = scDogleg100.ToString();
                        insertRow.Parameters.AddWithValue("@scSubsea", SqlDbType.Decimal).Value = scSubsea.ToString();
                        insertRow.Parameters.AddWithValue("@scMDTI", SqlDbType.Decimal).Value = scMDTI.ToString();
                        insertRow.Parameters.AddWithValue("@scTvdTI", SqlDbType.Decimal).Value = scTvdTI.ToString();
                        insertRow.Parameters.AddWithValue("@scVSTI", SqlDbType.Decimal).Value = scVSTI.ToString();
                        insertRow.Parameters.AddWithValue("@scNorthingTI", SqlDbType.Decimal).Value = scNorthingTI.ToString();
                        insertRow.Parameters.AddWithValue("@scEastingTI", SqlDbType.Decimal).Value = scEastingTI.ToString();
                        insertRow.Parameters.AddWithValue("@scDeltaMDTI", SqlDbType.Decimal).Value = scDeltaMDTI.ToString();
                        insertRow.Parameters.AddWithValue("@scD1TI", SqlDbType.Decimal).Value = scD1TI.ToString();
                        insertRow.Parameters.AddWithValue("@scDogleg1TI", SqlDbType.Decimal).Value = scDogleg1TI.ToString();
                        insertRow.Parameters.AddWithValue("@scRFTI", SqlDbType.Decimal).Value = scRFTI.ToString();
                        insertRow.Parameters.AddWithValue("@scDeltaTVDTI", SqlDbType.Decimal).Value = scDeltaTVDTI.ToString();
                        insertRow.Parameters.AddWithValue("@scCummNorthingTI", SqlDbType.Decimal).Value = scCummNorthingTI.ToString();
                        insertRow.Parameters.AddWithValue("@scDeltaNorthTI", SqlDbType.Decimal).Value = scDeltaNorthTI.ToString();
                        insertRow.Parameters.AddWithValue("@scCummEastingTI", SqlDbType.Decimal).Value = scCummEastingTI.ToString();
                        insertRow.Parameters.AddWithValue("@scDeltaEastingTI", SqlDbType.Decimal).Value = scDeltaEastingTI.ToString();
                        insertRow.Parameters.AddWithValue("@scDeltaVSTI", SqlDbType.Decimal).Value = scDeltaVSTI.ToString();
                        insertRow.Parameters.AddWithValue("@scClsrDistanceTI", SqlDbType.Decimal).Value = scClsrDistanceTI.ToString();
                        insertRow.Parameters.AddWithValue("@scClsrDirectAngle1TI", SqlDbType.Decimal).Value = scClsrDirectAngle1TI.ToString();
                        insertRow.Parameters.AddWithValue("@scClsrDirectAngle2TI", SqlDbType.Decimal).Value = scClsrDirectAngle2TI.ToString();
                        insertRow.Parameters.AddWithValue("@scDoglegTI", SqlDbType.Decimal).Value = scDoglegTI.ToString();
                        insertRow.Parameters.AddWithValue("@scDogleg100TI", SqlDbType.Decimal).Value = scDogleg100TI.ToString();
                        insertRow.Parameters.AddWithValue("@scSubseaTI", SqlDbType.Decimal).Value = scSubseaTI.ToString();
                        insertRow.Parameters.AddWithValue("@bhTVD", SqlDbType.Decimal).Value = bhTVD.ToString();
                        insertRow.Parameters.AddWithValue("@bhVS", SqlDbType.Decimal).Value = bhVS.ToString();
                        insertRow.Parameters.AddWithValue("@bhNorthing", SqlDbType.Decimal).Value = bhNorthing.ToString();
                        insertRow.Parameters.AddWithValue("@bhEasting", SqlDbType.Decimal).Value = bhEasting.ToString();
                        insertRow.Parameters.AddWithValue("@bhDeltaMD", SqlDbType.Decimal).Value = bhDeltaMD.ToString();
                        insertRow.Parameters.AddWithValue("@bhD1", SqlDbType.Decimal).Value = bhD1.ToString();
                        insertRow.Parameters.AddWithValue("@bhDogleg1", SqlDbType.Decimal).Value = bhDogleg1.ToString();
                        insertRow.Parameters.AddWithValue("@bhRF", SqlDbType.Decimal).Value = bhRF.ToString();
                        insertRow.Parameters.AddWithValue("@bhDeltaTVD", SqlDbType.Decimal).Value = bhDeltaTVD.ToString();
                        insertRow.Parameters.AddWithValue("@bhCummNorthing", SqlDbType.Decimal).Value = bhCummNorthing.ToString();
                        insertRow.Parameters.AddWithValue("@bhDeltaNorth", SqlDbType.Decimal).Value = bhDeltaNorth.ToString();
                        insertRow.Parameters.AddWithValue("@bhCummEasting", SqlDbType.Decimal).Value = bhCummEasting.ToString();
                        insertRow.Parameters.AddWithValue("@bhDeltaEasting", SqlDbType.Decimal).Value = bhDeltaEasting.ToString();
                        insertRow.Parameters.AddWithValue("@bhDeltaVS", SqlDbType.Decimal).Value = bhDeltaVS.ToString();
                        insertRow.Parameters.AddWithValue("@bhClsrDistance", SqlDbType.Decimal).Value = bhClsrDistance.ToString();
                        insertRow.Parameters.AddWithValue("@bhClsrDirectAngle1", SqlDbType.Decimal).Value = bhClsrDirectAngle1.ToString();
                        insertRow.Parameters.AddWithValue("@bhClsrDirectAngle2", SqlDbType.Decimal).Value = bhClsrDirectAngle2.ToString();
                        insertRow.Parameters.AddWithValue("@bhDogleg", SqlDbType.Decimal).Value = bhDogleg.ToString();
                        insertRow.Parameters.AddWithValue("@bhDogleg100", SqlDbType.Decimal).Value = bhDogleg100.ToString();
                        insertRow.Parameters.AddWithValue("@bhSubsea", SqlDbType.Decimal).Value = bhSubsea.ToString();
                        insertRow.Parameters.AddWithValue("@bhMDTI", SqlDbType.Decimal).Value = bhMDTI.ToString();
                        insertRow.Parameters.AddWithValue("@bhTvdTI", SqlDbType.Decimal).Value = bhTvdTI.ToString();
                        insertRow.Parameters.AddWithValue("@bhVSTI", SqlDbType.Decimal).Value = bhVSTI.ToString();
                        insertRow.Parameters.AddWithValue("@bhNorthingTI", SqlDbType.Decimal).Value = bhNorthingTI.ToString();
                        insertRow.Parameters.AddWithValue("@bhEastingTI", SqlDbType.Decimal).Value = bhEastingTI.ToString();
                        insertRow.Parameters.AddWithValue("@bhDeltaMDTI", SqlDbType.Decimal).Value = bhDeltaMDTI.ToString();
                        insertRow.Parameters.AddWithValue("@bhD1TI", SqlDbType.Decimal).Value = bhD1TI.ToString();
                        insertRow.Parameters.AddWithValue("@bhDogleg1TI", SqlDbType.Decimal).Value = bhDogleg1TI.ToString();
                        insertRow.Parameters.AddWithValue("@bhRFTI", SqlDbType.Decimal).Value = bhRFTI.ToString();
                        insertRow.Parameters.AddWithValue("@bhDeltaTVDTI", SqlDbType.Decimal).Value = bhDeltaTVDTI.ToString();
                        insertRow.Parameters.AddWithValue("@bhCummNorthingTI", SqlDbType.Decimal).Value = bhCummNorthingTI.ToString();
                        insertRow.Parameters.AddWithValue("@bhDeltaNorthTI", SqlDbType.Decimal).Value = bhDeltaNorthTI.ToString();
                        insertRow.Parameters.AddWithValue("@bhCummEastingTI", SqlDbType.Decimal).Value = bhCummEastingTI.ToString();
                        insertRow.Parameters.AddWithValue("@bhDeltaEastingTI", SqlDbType.Decimal).Value = bhDeltaEastingTI.ToString();
                        insertRow.Parameters.AddWithValue("@bhDeltaVSTI", SqlDbType.Decimal).Value = bhDeltaVSTI.ToString();
                        insertRow.Parameters.AddWithValue("@bhClsrDistanceTI", SqlDbType.Decimal).Value = bhClsrDistanceTI.ToString();
                        insertRow.Parameters.AddWithValue("@bhClsrDirectAngle1TI", SqlDbType.Decimal).Value = bhClsrDirectAngle1TI.ToString();
                        insertRow.Parameters.AddWithValue("@bhClsrDirectAngle2TI", SqlDbType.Decimal).Value = bhClsrDirectAngle2TI.ToString();
                        insertRow.Parameters.AddWithValue("@bhDoglegTI", SqlDbType.Decimal).Value = bhDoglegTI.ToString();
                        insertRow.Parameters.AddWithValue("@bhDogleg100TI", SqlDbType.Decimal).Value = bhDogleg100TI.ToString();
                        insertRow.Parameters.AddWithValue("@bhSubseaTI", SqlDbType.Decimal).Value = bhSubseaTI.ToString();
                        insertRow.Parameters.AddWithValue("@crTVD", SqlDbType.Decimal).Value = crTVD.ToString();
                        insertRow.Parameters.AddWithValue("@crVS", SqlDbType.Decimal).Value = crVS.ToString();
                        insertRow.Parameters.AddWithValue("@crNorthing", SqlDbType.Decimal).Value = crNorthing.ToString();
                        insertRow.Parameters.AddWithValue("@crEasting", SqlDbType.Decimal).Value = crEasting.ToString();
                        insertRow.Parameters.AddWithValue("@crDeltaMD", SqlDbType.Decimal).Value = crDeltaMD.ToString();
                        insertRow.Parameters.AddWithValue("@crD1", SqlDbType.Decimal).Value = crD1.ToString();
                        insertRow.Parameters.AddWithValue("@crDogleg1", SqlDbType.Decimal).Value = crDogleg1.ToString();
                        insertRow.Parameters.AddWithValue("@crRF", SqlDbType.Decimal).Value = crRF.ToString();
                        insertRow.Parameters.AddWithValue("@crDeltaTVD", SqlDbType.Decimal).Value = crDeltaTVD.ToString();
                        insertRow.Parameters.AddWithValue("@crCummNorthing", SqlDbType.Decimal).Value = crCummNorthing.ToString();
                        insertRow.Parameters.AddWithValue("@crDeltaNorth", SqlDbType.Decimal).Value = crDeltaNorth.ToString();
                        insertRow.Parameters.AddWithValue("@crCummEasting", SqlDbType.Decimal).Value = crCummEasting.ToString();
                        insertRow.Parameters.AddWithValue("@crDeltaEasting", SqlDbType.Decimal).Value = crDeltaEasting.ToString();
                        insertRow.Parameters.AddWithValue("@crDeltaVS", SqlDbType.Decimal).Value = crDeltaVS.ToString();
                        insertRow.Parameters.AddWithValue("@crClsrDistance", SqlDbType.Decimal).Value = crClsrDistance.ToString();
                        insertRow.Parameters.AddWithValue("@crClsrDirectAngle1", SqlDbType.Decimal).Value = crClsrDirectAngle1.ToString();
                        insertRow.Parameters.AddWithValue("@crClsrDirectAngle2", SqlDbType.Decimal).Value = crClsrDirectAngle2.ToString();
                        insertRow.Parameters.AddWithValue("@crDogleg", SqlDbType.Decimal).Value = crDogleg.ToString();
                        insertRow.Parameters.AddWithValue("@crDogleg100", SqlDbType.Decimal).Value = crDogleg100.ToString();
                        insertRow.Parameters.AddWithValue("@crSubsea", SqlDbType.Decimal).Value = crSubsea.ToString();
                        insertRow.Parameters.AddWithValue("@crMDTI", SqlDbType.Decimal).Value = crMDTI.ToString();
                        insertRow.Parameters.AddWithValue("@crTvdTI", SqlDbType.Decimal).Value = crTvdTI.ToString();
                        insertRow.Parameters.AddWithValue("@crVSTI", SqlDbType.Decimal).Value = crVSTI.ToString();
                        insertRow.Parameters.AddWithValue("@crNorthingTI", SqlDbType.Decimal).Value = crNorthingTI.ToString();
                        insertRow.Parameters.AddWithValue("@crEastingTI", SqlDbType.Decimal).Value = crEastingTI.ToString();
                        insertRow.Parameters.AddWithValue("@crDeltaMDTI", SqlDbType.Decimal).Value = crDeltaMDTI.ToString();
                        insertRow.Parameters.AddWithValue("@crD1TI", SqlDbType.Decimal).Value = crD1TI.ToString();
                        insertRow.Parameters.AddWithValue("@crDogleg1TI", SqlDbType.Decimal).Value = crDogleg1TI.ToString();
                        insertRow.Parameters.AddWithValue("@crRFTI", SqlDbType.Decimal).Value = crRFTI.ToString();
                        insertRow.Parameters.AddWithValue("@crDeltaTVDTI", SqlDbType.Decimal).Value = crDeltaTVDTI.ToString();
                        insertRow.Parameters.AddWithValue("@crCummNorthingTI", SqlDbType.Decimal).Value = crCummNorthingTI.ToString();
                        insertRow.Parameters.AddWithValue("@crDeltaNorthTI", SqlDbType.Decimal).Value = crDeltaNorthTI.ToString();
                        insertRow.Parameters.AddWithValue("@crCummEastingTI", SqlDbType.Decimal).Value = crCummEastingTI.ToString();
                        insertRow.Parameters.AddWithValue("@crDeltaEastingTI", SqlDbType.Decimal).Value = crDeltaEastingTI.ToString();
                        insertRow.Parameters.AddWithValue("@crDeltaVSTI", SqlDbType.Decimal).Value = crDeltaVSTI.ToString();
                        insertRow.Parameters.AddWithValue("@crClsrDistanceTI", SqlDbType.Decimal).Value = crClsrDistanceTI.ToString();
                        insertRow.Parameters.AddWithValue("@crClsrDirectAngle1TI", SqlDbType.Decimal).Value = crClsrDirectAngle1TI.ToString();
                        insertRow.Parameters.AddWithValue("@crClsrDirectAngle2TI", SqlDbType.Decimal).Value = crClsrDirectAngle2TI.ToString();
                        insertRow.Parameters.AddWithValue("@crDoglegTI", SqlDbType.Decimal).Value = crDoglegTI.ToString();
                        insertRow.Parameters.AddWithValue("@crDogleg100TI", SqlDbType.Decimal).Value = crDogleg100TI.ToString();
                        insertRow.Parameters.AddWithValue("@crSubseaTI", SqlDbType.Decimal).Value = crSubseaTI.ToString();
                        insertRow.Parameters.AddWithValue("@pTime", SqlDbType.DateTime).Value = DateTime.Now.ToString();
                        insertRow.Parameters.AddWithValue("@acID", SqlDbType.Int).Value = AzimuthCriteriaID.ToString();
                        
                        iResult = (Int32)insertRow.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }            
        }

        public static Int32 StoreRawFormatedData(String Date, String Time, Decimal depth, Int32 depthUnit, Decimal Gx, Decimal Gy, Decimal Gz, Decimal Bx, Decimal By, Decimal Bz, Int32 AccelUnit, Int32 MagUnit, Int32 RunID, Int32 WellID, Int32 WellSectionID, Int32 WellPadID, Int32 ClientID, Int32 OperatorID, Int32 SurveyID, Int32 dataRowType, Int32 serviceID, String fileName, String loginName, String DBConn)
        {
            try
            {
                Int32 iResult = -99;
                using (SqlConnection conRawData = new SqlConnection(DBConn))
                {
                    try
                    {
                        conRawData.Open();
                        String enterRawData = "INSERT INTO [SurveyFormattedData] ([sfdDate], [sfdTime], [sfdDepth], [lenU], [sfdGx], [sfdGy], [sfdGz], [sfdBx], [sfdBy], [sfdBz], [acuID], [muID], [runID], [wswID], [welID], [wpdID], [clntID], [optrID], [srvyRowID], [srvID], [demID], [fileName], [login], [cTime], [uTime]) VALUES (@sfdDate, @sfdTime, @sfdDepth, @lenU, @sfdGx, @sfdGy, @sfdGz, @sfdBx, @sfdBy, @sfdBz, @acuID, @muID, @runID, @wswID, @welID, @wpdID, @clntID, @optrID, @srvyRowID, @srvID, @demID, @fileName, @login, @cTime, @uTime)";
                        SqlCommand insertData = new SqlCommand(enterRawData, conRawData);
                        insertData.Parameters.AddWithValue("@sfdDate", SqlDbType.NVarChar).Value = Date.ToString();
                        insertData.Parameters.AddWithValue("@sfdTime", SqlDbType.NVarChar).Value = Time.ToString();
                        insertData.Parameters.AddWithValue("@sfdDepth", SqlDbType.Decimal).Value = depth.ToString();
                        insertData.Parameters.AddWithValue("@lenU", SqlDbType.Int).Value = depthUnit.ToString();
                        insertData.Parameters.AddWithValue("@sfdGx", SqlDbType.Decimal).Value = Gx.ToString();
                        insertData.Parameters.AddWithValue("@sfdGy", SqlDbType.Decimal).Value = Gy.ToString();
                        insertData.Parameters.AddWithValue("@sfdGz", SqlDbType.Decimal).Value = Gz.ToString();
                        insertData.Parameters.AddWithValue("@sfdBx", SqlDbType.Decimal).Value = Bx.ToString();
                        insertData.Parameters.AddWithValue("@sfdBy", SqlDbType.Decimal).Value = By.ToString();
                        insertData.Parameters.AddWithValue("@sfdBz", SqlDbType.Decimal).Value = Bz.ToString();
                        insertData.Parameters.AddWithValue("@acuID", SqlDbType.Int).Value = AccelUnit.ToString();
                        insertData.Parameters.AddWithValue("@muID", SqlDbType.Int).Value = MagUnit.ToString();
                        insertData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        insertData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        insertData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        insertData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        insertData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        insertData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        insertData.Parameters.AddWithValue("@srvyRowID", SqlDbType.Int).Value = SurveyID.ToString();
                        insertData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = serviceID.ToString();
                        insertData.Parameters.AddWithValue("@demID", SqlDbType.Int).Value = dataRowType.ToString();
                        insertData.Parameters.AddWithValue("@fileName", SqlDbType.NVarChar).Value = fileName.ToString();
                        insertData.Parameters.AddWithValue("@login", SqlDbType.NVarChar).Value = loginName.ToString();
                        insertData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insertData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();

                        iResult = insertData.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        conRawData.Close();
                        conRawData.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 Store3rdRawFormatedData(String Date, String Time, Decimal depth, Int32 depthUnit, Decimal Gx, Decimal Gy, Decimal Gz, Decimal Bx, Decimal By, Decimal Bz, Int32 AccelUnit, Int32 MagUnit, Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 WellPadID, Int32 ClientID, Int32 OperatorID, Int32 SurveyID, Int32 dataRowType, Int32 serviceID, String fileName, String loginName, String DBConn)
        {
            try
            {
                Int32 iResult = -99;
                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;
                using (SqlConnection conRawData = new SqlConnection(DBConn))
                {
                    try
                    {
                        conRawData.Open();
                        String enterRawData = "INSERT INTO [3rdSurveyFormattedData] ([sfdDate], [sfdTime], [sfdDepth], [lenU], [sfdGx], [sfdGy], [sfdGz], [sfdBx], [sfdBy], [sfdBz], [acuID], [muID], [runID], [wswID], [welID], [wpdID], [clntID], [optrID], [srvyRowID], [srvID], [demID], [fileName], [login], [cTime], [uTime]) VALUES (@sfdDate, @sfdTime, @sfdDepth, @lenU, @sfdGx, @sfdGy, @sfdGz, @sfdBx, @sfdBy, @sfdBz, @acuID, @muID, @runID, @wswID, @welID, @wpdID, @clntID, @optrID, @srvyRowID, @srvID, @demID, @fileName, @login, @cTime, @uTime)";
                        SqlCommand insertData = new SqlCommand(enterRawData, conRawData);
                        insertData.Parameters.AddWithValue("@sfdDate", SqlDbType.NVarChar).Value = Date.ToString();
                        insertData.Parameters.AddWithValue("@sfdTime", SqlDbType.NVarChar).Value = Time.ToString();
                        insertData.Parameters.AddWithValue("@sfdDepth", SqlDbType.Decimal).Value = depth.ToString();
                        insertData.Parameters.AddWithValue("@lenU", SqlDbType.Int).Value = depthUnit.ToString();
                        insertData.Parameters.AddWithValue("@sfdGx", SqlDbType.Decimal).Value = Gx.ToString();
                        insertData.Parameters.AddWithValue("@sfdGy", SqlDbType.Decimal).Value = Gy.ToString();
                        insertData.Parameters.AddWithValue("@sfdGz", SqlDbType.Decimal).Value = Gz.ToString();
                        insertData.Parameters.AddWithValue("@sfdBx", SqlDbType.Decimal).Value = Bx.ToString();
                        insertData.Parameters.AddWithValue("@sfdBy", SqlDbType.Decimal).Value = By.ToString();
                        insertData.Parameters.AddWithValue("@sfdBz", SqlDbType.Decimal).Value = Bz.ToString();
                        insertData.Parameters.AddWithValue("@acuID", SqlDbType.Int).Value = AccelUnit.ToString();
                        insertData.Parameters.AddWithValue("@muID", SqlDbType.Int).Value = MagUnit.ToString();
                        insertData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        insertData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        insertData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        insertData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        insertData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        insertData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        insertData.Parameters.AddWithValue("@srvyRowID", SqlDbType.Int).Value = SurveyID.ToString();
                        insertData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = serviceID.ToString();
                        insertData.Parameters.AddWithValue("@demID", SqlDbType.Int).Value = dataRowType.ToString();
                        insertData.Parameters.AddWithValue("@fileName", SqlDbType.NVarChar).Value = fileName.ToString();
                        insertData.Parameters.AddWithValue("@login", SqlDbType.NVarChar).Value = loginName.ToString();
                        insertData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insertData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();

                        iResult = insertData.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        conRawData.Close();
                        conRawData.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 StoreHistRawFormatedData(String Date, String Time, Decimal depth, Int32 depthUnit, Decimal Gx, Decimal Gy, Decimal Gz, Decimal Bx, Decimal By, Decimal Bz, Int32 AccelUnit, Int32 MagUnit, Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 WellPadID, Int32 ClientID, Int32 OperatorID, Int32 SurveyID, Int32 dataRowType, Int32 serviceID, String fileName, String loginName, String DBConn)
        {
            try
            {
                Int32 iResult = -99;
                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;
                using (SqlConnection conRawData = new SqlConnection(DBConn))
                {
                    try
                    {
                        conRawData.Open();
                        String enterRawData = "INSERT INTO [HistSurveyFormattedData] ([sfdDate], [sfdTime], [sfdDepth], [lenU], [sfdGx], [sfdGy], [sfdGz], [sfdBx], [sfdBy], [sfdBz], [acuID], [muID], [runID], [wswID], [welID], [wpdID], [clntID], [optrID], [srvyRowID], [srvID], [demID], [fileName], [login], [cTime], [uTime]) VALUES (@sfdDate, @sfdTime, @sfdDepth, @lenU, @sfdGx, @sfdGy, @sfdGz, @sfdBx, @sfdBy, @sfdBz, @acuID, @muID, @runID, @wswID, @welID, @wpdID, @clntID, @optrID, @srvyRowID, @srvID, @demID, @fileName, @login, @cTime, @uTime)";
                        SqlCommand insertData = new SqlCommand(enterRawData, conRawData);
                        insertData.Parameters.AddWithValue("@sfdDate", SqlDbType.NVarChar).Value = Date.ToString();
                        insertData.Parameters.AddWithValue("@sfdTime", SqlDbType.NVarChar).Value = Time.ToString();
                        insertData.Parameters.AddWithValue("@sfdDepth", SqlDbType.Decimal).Value = depth.ToString();
                        insertData.Parameters.AddWithValue("@lenU", SqlDbType.Int).Value = depthUnit.ToString();
                        insertData.Parameters.AddWithValue("@sfdGx", SqlDbType.Decimal).Value = Gx.ToString();
                        insertData.Parameters.AddWithValue("@sfdGy", SqlDbType.Decimal).Value = Gy.ToString();
                        insertData.Parameters.AddWithValue("@sfdGz", SqlDbType.Decimal).Value = Gz.ToString();
                        insertData.Parameters.AddWithValue("@sfdBx", SqlDbType.Decimal).Value = Bx.ToString();
                        insertData.Parameters.AddWithValue("@sfdBy", SqlDbType.Decimal).Value = By.ToString();
                        insertData.Parameters.AddWithValue("@sfdBz", SqlDbType.Decimal).Value = Bz.ToString();
                        insertData.Parameters.AddWithValue("@acuID", SqlDbType.Int).Value = AccelUnit.ToString();
                        insertData.Parameters.AddWithValue("@muID", SqlDbType.Int).Value = MagUnit.ToString();
                        insertData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        insertData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        insertData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        insertData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        insertData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        insertData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        insertData.Parameters.AddWithValue("@srvyRowID", SqlDbType.Int).Value = SurveyID.ToString();
                        insertData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = serviceID.ToString();
                        insertData.Parameters.AddWithValue("@demID", SqlDbType.Int).Value = dataRowType.ToString();
                        insertData.Parameters.AddWithValue("@fileName", SqlDbType.NVarChar).Value = fileName.ToString();
                        insertData.Parameters.AddWithValue("@login", SqlDbType.NVarChar).Value = loginName.ToString();
                        insertData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = crTime.ToString();
                        insertData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = updTime.ToString();

                        iResult = insertData.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        conRawData.Close();
                        conRawData.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getMetaResultsForWell(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, String ClientDBConnection)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn resID = iReply.Columns.Add("resID", typeof(Int32));
                DataColumn srvyID = iReply.Columns.Add("srvyID", typeof(Int32));
                DataColumn Depth = iReply.Columns.Add("Depth", typeof(Decimal));
                DataColumn acID = iReply.Columns.Add("acID", typeof(String));
                DataColumn WSection = iReply.Columns.Add("WSection", typeof(String));
                DataColumn taSt = iReply.Columns.Add("taSt", typeof(String));
                DataColumn chSt = iReply.Columns.Add("chSt", typeof(Int32));
                //Int32 id = -99, sID = -99, tanaID = -99, ct = -99, wsD = -99, counter = 1, azmcrrID = -99;
                Int32 id = -99, sID = -99, tanaID = -99, ct = -99, wsD = -99, azmcrrID = -99;
                Decimal sD = -99.00M;
                String tstat = String.Empty, section = String.Empty, azmcrrValue = String.Empty;
                String selData = "SELECT [resID], [srvyID], [wswID], [depth], [taID], [chkShot], [acID] FROM [RawQCResults] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID AND [welID] = @welID ORDER BY [depth] DESC";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {                                        
                                        id = readData.GetInt32(0);
                                        sID = readData.GetInt32(1);
                                        wsD = readData.GetInt32(2);
                                        section = OPR.getWellSectionNameFromID(wsD, ClientDBConnection);
                                        sD = readData.GetDecimal(3);
                                        tanaID = readData.GetInt32(4);
                                        tstat = getTrendAnalysisStatement(tanaID);
                                        ct = readData.GetInt32(5);
                                        azmcrrID = readData.GetInt32(6);
                                        azmcrrValue = DMG.getAzimuthCriteriaValueFromID(azmcrrID);
                                        iReply.Rows.Add(id, sID, sD, azmcrrValue, section, tstat, ct);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getMetaResults(Int32 OperatorID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn resID = iReply.Columns.Add("resID", typeof(Int32));
                DataColumn srvyID = iReply.Columns.Add("srvyID", typeof(Int32));
                DataColumn Depth = iReply.Columns.Add("Depth", typeof(Decimal));
                DataColumn acID = iReply.Columns.Add("acID", typeof(String));
                DataColumn taSt = iReply.Columns.Add("taSt", typeof(String));
                DataColumn chSt = iReply.Columns.Add("chSt", typeof(Int32));
                Int32 id = -99, sID = -99, tanaID = -99, ct = -99, azmcrtID = -99;
                Decimal sD = -99.00M;
                String tstat = String.Empty, azmCrtValue = String.Empty;
                String selData = "SELECT [resID], [srvyID], [depth], [acID], [taID], [chkShot], [pTime] FROM [RawQCResults] WHERE [optrID] = @optrID AND [wpdID] = @wpdID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID ORDER BY [srvyID] DESC";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        sID = readData.GetInt32(1);
                                        sD = readData.GetDecimal(2);
                                        azmcrtID = readData.GetInt32(3);
                                        azmCrtValue = DMG.getAzimuthCriteriaValueFromID(azmcrtID);
                                        tanaID = readData.GetInt32(4);
                                        tstat = getTrendAnalysisStatement(tanaID);
                                        ct = readData.GetInt32(5);
                                        iReply.Rows.Add(id, sID, sD, azmCrtValue, tstat);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getLimitedQCResults(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Decimal SurveyDepth, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn resID = iReply.Columns.Add("resID", typeof(Int32));
                DataColumn srvyID = iReply.Columns.Add("srvyID", typeof(Int32));
                DataColumn Depth = iReply.Columns.Add("Depth", typeof(Decimal));
                DataColumn taSt = iReply.Columns.Add("taSt", typeof(String));
                DataColumn inc = iReply.Columns.Add("inc", typeof(Decimal));
                DataColumn azm = iReply.Columns.Add("azm", typeof(Decimal));
                DataColumn dip = iReply.Columns.Add("dip", typeof(Decimal));
                DataColumn dipQ = iReply.Columns.Add("dipQ", typeof(String));
                DataColumn delDip = iReply.Columns.Add("delDip", typeof(Decimal));
                DataColumn bT = iReply.Columns.Add("bT", typeof(Decimal));
                DataColumn btQ = iReply.Columns.Add("btQ", typeof(String));
                DataColumn delBT = iReply.Columns.Add("delBT", typeof(Decimal));
                DataColumn gT = iReply.Columns.Add("gT", typeof(Decimal));
                DataColumn gtQ = iReply.Columns.Add("gtQ", typeof(String));
                DataColumn delGT = iReply.Columns.Add("delGT", typeof(Decimal));
                //iReply.PrimaryKey = new DataColumn[] { resID };
                Int32 id = -99, sID = -99, tanaID = -99, btQVal = -99, gtQVal = -99, dipQVal = -99;
                Decimal sD = -99.00M, incVal = -99.00M, azmVal = -99.00M, dipVal = -99.00M, delDipVal = -99.00M, btVal = -99.0M, delBTVal = -99.00M, gtVal = -99.00M, delGTVal = -99.00M;
                String tstat = String.Empty, dipColor = String.Empty, btColor = String.Empty, gtColor = String.Empty;
                String selData = "SELECT [resID], [srvyID], [depth], [taID], [colT], [colDR], [colBA], [colCA], [colCF], [colDW], [colBC], [colDY], [colCD], [colEA], [colCG] FROM [RawQCResults] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID AND [depth] = @depth";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        getData.Parameters.AddWithValue("@depth", SqlDbType.Decimal).Value = SurveyDepth.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        sID = readData.GetInt32(1);
                                        sD = readData.GetDecimal(2);
                                        tanaID = readData.GetInt32(3);
                                        tstat = getTrendAnalysisStatement(tanaID);
                                        incVal = readData.GetDecimal(4);
                                        azmVal = readData.GetDecimal(5);
                                        dipVal = readData.GetDecimal(6);
                                        btVal = readData.GetDecimal(7);
                                        gtVal = readData.GetDecimal(8);
                                        dipQVal = readData.GetInt32(9);
                                        if (dipQVal.Equals(1)) { dipColor = "Green"; } else { dipColor = "Yellow"; }
                                        delDipVal = readData.GetDecimal(10);
                                        btQVal = readData.GetInt32(11);
                                        if (btQVal.Equals(1)) { btColor = "Green"; } else { btColor = "Yellow"; }
                                        delBTVal = readData.GetDecimal(12);
                                        gtQVal = readData.GetInt32(13);
                                        if (gtQVal.Equals(1)) { gtColor = "Green"; } else { gtColor = "Yellow"; }
                                        delGTVal = readData.GetDecimal(14);
                                        iReply.Rows.Add(id, sID, sD, tstat, incVal, azmVal, dipVal, dipColor, delDipVal, btVal, btColor, delBTVal, gtVal, gtColor, delGTVal);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getLimitedQCAzimuths(Int32 WellID, Int32 WellSectionID, Int32 RunID, Decimal SurveyDepth, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn resID = iReply.Columns.Add("resID", typeof(Int32));
                DataColumn rawAzm = iReply.Columns.Add("rawAzm", typeof(Decimal));
                DataColumn lcAzm = iReply.Columns.Add("lcAzm", typeof(Decimal));
                DataColumn scAzm = iReply.Columns.Add("scAzm", typeof(Decimal));
                //iReply.PrimaryKey = new DataColumn[] { resID };
                Int32 id = -99;
                Decimal qcAzm = -99.00M, longcollarAzm = -99.00M, shortcollarAzm = -99.00M;
                String selData = "SELECT [resID], [colDR], [colAO], [colDD] FROM [RawQCResults] WHERE [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID AND [depth] = @depth";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        getData.Parameters.AddWithValue("@depth", SqlDbType.Decimal).Value = SurveyDepth.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        qcAzm = readData.GetDecimal(1);
                                        longcollarAzm = readData.GetDecimal(2);
                                        shortcollarAzm = readData.GetDecimal(3);
                                        iReply.Rows.Add(id, qcAzm, longcollarAzm, shortcollarAzm);
                                        iReply.AcceptChanges();                                
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getTrendAnalysisStatement(Int32 taID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [taAnalysis] FROM dmgQCTrendAnalysis WHERE [taID] = @taID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@taID", SqlDbType.Int).Value = taID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    iReply = readData.GetString(0);
                                }
                            }
                            else
                            {
                                iReply = "---";
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }                    
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getResSQCTable(Int32 QCResultID, String ClientDBConnection)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn sqcID = iReply.Columns.Add("sqcID", typeof(Int32));
                DataColumn sqcName = iReply.Columns.Add("sqcName", typeof(String));
                DataColumn sqcDip = iReply.Columns.Add("sqcDip", typeof(Decimal));
                DataColumn sqcBT = iReply.Columns.Add("sqcBT", typeof(Decimal));
                DataColumn sqcGT = iReply.Columns.Add("sqcGT", typeof(Decimal));
                iReply.PrimaryKey = new DataColumn[] { sqcID };
                Int32 id = -99, rsqcID = -99;
                String name = String.Empty;
                Decimal dip = -99.0000M, bTotal = -99.0000M, gTotal = -99.0000000M;
                rsqcID = getSelectedSQC(QCResultID, ClientDBConnection);
                String selData = "SELECT [sqcID], [sqcName], [sqcDip], [sqcBTotal], [sqcGTotal] FROM SQC WHERE [sqcID] = @sqcID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@sqcID", SqlDbType.Int).Value = rsqcID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        dip = readData.GetDecimal(2);
                                        bTotal = readData.GetDecimal(3);
                                        gTotal = readData.GetDecimal(4);
                                        iReply.Rows.Add(id, name, dip, bTotal, gTotal);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getSelectedSQC(Int32 ResRowID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT [sqcID] FROM [RawQCResults] WHERE [resID] = @resID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@resID", SqlDbType.Int).Value = ResRowID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getResParameters(Int32 ResRowID, String ClientDBConnection)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn resID = iReply.Columns.Add("resID", typeof(Int32));
                DataColumn colT = iReply.Columns.Add("inc", typeof(Decimal));
                DataColumn colDR = iReply.Columns.Add("azm", typeof(Decimal));
                DataColumn colBA = iReply.Columns.Add("dip", typeof(Decimal));
                DataColumn colCA = iReply.Columns.Add("bTotal", typeof(Decimal));
                DataColumn colCF = iReply.Columns.Add("gTotal", typeof(Decimal));
                DataColumn colP = iReply.Columns.Add("gtf", typeof(Decimal));
                DataColumn colQ = iReply.Columns.Add("mtf", typeof(Decimal));
                iReply.PrimaryKey = new DataColumn[] { resID };
                Int32 acID = -99;
                String selData = String.Empty;
                acID = getAzimuthCriteriaIDForResultRow(ResRowID, ClientDBConnection);
                switch (acID)
                {
                    case 1:
                        {// BHA
                        selData = "SELECT [resID], [colT], [colDR], [colGE], [colBA], [colCA], [colCF], [colP], [colQ] FROM [RawQCResults] WHERE [resID] = @resID"; 
                        iReply = getBHAQCParameters(ResRowID, selData, ClientDBConnection);
                        break; }
                    case 2:
                        {// Corrected
                        selData = "SELECT [resID], [colT], [colAO], [colBA], [colCA], [colCF], [colP], [colQ] FROM [RawQCResults] WHERE [resID] = @resID"; 
                        iReply = getCorrectedQCParameters(ResRowID, selData, ClientDBConnection);
                        break; }
                    case 3: { 
                        // MSSR
                        iReply = getMSSRQCParameters(ResRowID, selData, ClientDBConnection);
                        break; }
                    case 4:
                        {// Raw                        
                        selData = "SELECT [resID], [colT], [colDR], [colBA], [colCA], [colCF], [colP], [colQ] FROM [RawQCResults] WHERE [resID] = @resID"; 
                        iReply = getRawQCParameters(ResRowID, selData, ClientDBConnection);
                        break; }
                    default:
                        {// Short-Collar                        
                        selData = "SELECT [resID], [colT], [colDD], [colBA], [colCA], [colCF], [colP], [colQ] FROM [RawQCResults] WHERE [resID] = @resID"; 
                        iReply = getShortCollarQCParameters(ResRowID, selData, ClientDBConnection);
                        break; }
                }                                
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getRawQCParameters(Int32 ResultsRowID, String SelectStatement, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn resID = iReply.Columns.Add("resID", typeof(Int32));
                DataColumn colT = iReply.Columns.Add("inc", typeof(Decimal));
                DataColumn colDR = iReply.Columns.Add("azm", typeof(Decimal));
                DataColumn colBA = iReply.Columns.Add("dip", typeof(Decimal));
                DataColumn colCA = iReply.Columns.Add("bTotal", typeof(Decimal));
                DataColumn colCF = iReply.Columns.Add("gTotal", typeof(Decimal));
                DataColumn colP = iReply.Columns.Add("gtf", typeof(Decimal));
                DataColumn colQ = iReply.Columns.Add("mtf", typeof(Decimal));
                iReply.PrimaryKey = new DataColumn[] { resID };
                Int32 id = -99;
                Decimal inclination = -99.0000M, azimuth = -99.0000M, dip = -99.0000M, bTotal = -99.0000M, gTotal = -99.0000M, gtf = -99.0000M, mtf = -99.0000M;
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(SelectStatement, dataCon);
                        getData.Parameters.AddWithValue("@resID", SqlDbType.Int).Value = ResultsRowID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        inclination = readData.GetDecimal(1);
                                        azimuth = readData.GetDecimal(2);
                                        dip = readData.GetDecimal(3);
                                        bTotal = readData.GetDecimal(4);
                                        gTotal = readData.GetDecimal(5);
                                        gtf = readData.GetDecimal(6);
                                        mtf = readData.GetDecimal(7);
                                        iReply.Rows.Add(id, inclination, azimuth, dip, bTotal, gTotal, gtf, mtf);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getShortCollarQCParameters(Int32 ResultsRowID, String SelectStatement, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn resID = iReply.Columns.Add("resID", typeof(Int32));
                DataColumn colT = iReply.Columns.Add("inc", typeof(Decimal));
                DataColumn colDR = iReply.Columns.Add("azm", typeof(Decimal));
                DataColumn colBA = iReply.Columns.Add("dip", typeof(Decimal));
                DataColumn colCA = iReply.Columns.Add("bTotal", typeof(Decimal));
                DataColumn colCF = iReply.Columns.Add("gTotal", typeof(Decimal));
                DataColumn colP = iReply.Columns.Add("gtf", typeof(Decimal));
                DataColumn colQ = iReply.Columns.Add("mtf", typeof(Decimal));
                iReply.PrimaryKey = new DataColumn[] { resID };
                Int32 id = -99;
                Decimal inclination = -99.0000M, azimuth = -99.0000M, dip = -99.0000M, bTotal = -99.0000M, gTotal = -99.0000M, gtf = -99.0000M, mtf = -99.0000M;
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(SelectStatement, dataCon);
                        getData.Parameters.AddWithValue("@resID", SqlDbType.Int).Value = ResultsRowID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        inclination = readData.GetDecimal(1);
                                        azimuth = readData.GetDecimal(2);
                                        dip = readData.GetDecimal(3);
                                        bTotal = readData.GetDecimal(4);
                                        gTotal = readData.GetDecimal(5);
                                        gtf = readData.GetDecimal(6);
                                        mtf = readData.GetDecimal(7);
                                        iReply.Rows.Add(id, inclination, azimuth, dip, bTotal, gTotal, gtf, mtf);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getMSSRQCParameters(Int32 ResultsRowID, String SelectStatement, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getBHAQCParameters(Int32 ResultsRowID, String SelectStatement, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn resID = iReply.Columns.Add("resID", typeof(Int32));
                DataColumn colT = iReply.Columns.Add("inc", typeof(Decimal));
                DataColumn colDR = iReply.Columns.Add("azm", typeof(Decimal));
                DataColumn colBA = iReply.Columns.Add("dip", typeof(Decimal));
                DataColumn colCA = iReply.Columns.Add("bTotal", typeof(Decimal));
                DataColumn colCF = iReply.Columns.Add("gTotal", typeof(Decimal));
                DataColumn colP = iReply.Columns.Add("gtf", typeof(Decimal));
                DataColumn colQ = iReply.Columns.Add("mtf", typeof(Decimal));
                iReply.PrimaryKey = new DataColumn[] { resID };
                Int32 id = -99;
                Decimal inclination = -99.0000M, azimuth = -99.0000M, dip = -99.0000M, bTotal = -99.0000M, gTotal = -99.0000M, gtf = -99.0000M, mtf = -99.0000M, dr = -99.0000M, ge = -99.0000M;
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(SelectStatement, dataCon);
                        getData.Parameters.AddWithValue("@resID", SqlDbType.Int).Value = ResultsRowID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        inclination = readData.GetDecimal(1);
                                        dr = readData.GetDecimal(2);
                                        ge = readData.GetDecimal(3);
                                        azimuth = dr - ge;
                                        dip = readData.GetDecimal(4);
                                        bTotal = readData.GetDecimal(5);
                                        gTotal = readData.GetDecimal(6);
                                        gtf = readData.GetDecimal(7);
                                        mtf = readData.GetDecimal(8);
                                        iReply.Rows.Add(id, inclination, azimuth, dip, bTotal, gTotal, gtf, mtf);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getCorrectedQCParameters(Int32 ResultsRowID, String SelectStatement, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn resID = iReply.Columns.Add("resID", typeof(Int32));
                DataColumn colT = iReply.Columns.Add("inc", typeof(Decimal));
                DataColumn colDR = iReply.Columns.Add("azm", typeof(Decimal));
                DataColumn colBA = iReply.Columns.Add("dip", typeof(Decimal));
                DataColumn colCA = iReply.Columns.Add("bTotal", typeof(Decimal));
                DataColumn colCF = iReply.Columns.Add("gTotal", typeof(Decimal));
                DataColumn colP = iReply.Columns.Add("gtf", typeof(Decimal));
                DataColumn colQ = iReply.Columns.Add("mtf", typeof(Decimal));
                iReply.PrimaryKey = new DataColumn[] { resID };
                Int32 id = -99;
                Decimal inclination = -99.0000M, azimuth = -99.0000M, dip = -99.0000M, bTotal = -99.0000M, gTotal = -99.0000M, gtf = -99.0000M, mtf = -99.0000M;
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(SelectStatement, dataCon);
                        getData.Parameters.AddWithValue("@resID", SqlDbType.Int).Value = ResultsRowID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        inclination = readData.GetDecimal(1);
                                        azimuth = readData.GetDecimal(2);
                                        dip = readData.GetDecimal(3);
                                        bTotal = readData.GetDecimal(4);
                                        gTotal = readData.GetDecimal(5);
                                        gtf = readData.GetDecimal(6);
                                        mtf = readData.GetDecimal(7);
                                        iReply.Rows.Add(id, inclination, azimuth, dip, bTotal, gTotal, gtf, mtf);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getResQualifiersTable(Int32 ResRowID, String ClientDBConnection)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn resID = iReply.Columns.Add("resID", typeof(Int32));
                DataColumn DW = iReply.Columns.Add("DW", typeof(String));
                DataColumn DWClr = iReply.Columns.Add("DWClr", typeof(Int32));
                DataColumn BC = iReply.Columns.Add("BC", typeof(Decimal));
                DataColumn DY = iReply.Columns.Add("DY", typeof(String));
                DataColumn DYClr = iReply.Columns.Add("DYClr", typeof(Int32));
                DataColumn CD = iReply.Columns.Add("CD", typeof(Decimal));
                DataColumn EA = iReply.Columns.Add("EA", typeof(String));
                DataColumn EAClr = iReply.Columns.Add("EAClr", typeof(Int32));
                DataColumn CG = iReply.Columns.Add("CG", typeof(Decimal));
                DataColumn AO = iReply.Columns.Add("AO", typeof(Decimal));
                DataColumn AP = iReply.Columns.Add("AP", typeof(Decimal));
                DataColumn DD = iReply.Columns.Add("DD", typeof(Decimal));
                DataColumn DE = iReply.Columns.Add("DE", typeof(Decimal));
                DataColumn HV = iReply.Columns.Add("HV", typeof(String));
                DataColumn HVClr = iReply.Columns.Add("HVClr", typeof(Int32));
                iReply.PrimaryKey = new DataColumn[] { resID };
                Int32 id = -99, dwI = -99, dyI = -99, eaI = -99, hvI = -99, DipClr = -99, BTClr = -99, GTClr = -99, NoGoClr = -99;
                String dwVal = String.Empty, dyVal = String.Empty, eaVal = String.Empty, hvVal = String.Empty;
                Decimal bcVal = -99.00M, cdVal = -99.00M, cgVal = -99.00M, aoVal = -99.00M, apVal = -99.00M, ddVal = -99.00M, deVal = -99.00M;
                String selData = "SELECT [resID], [colDW], [colBC], [colDY], [colCD], [colEA], [colCG], [colAO], [colAP], [colDD], [colDE], [colHV] FROM [RawQCResults] WHERE [resID] = @resID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@resID", SqlDbType.Int).Value = ResRowID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        dwI = readData.GetInt32(1);
                                        dwVal = getDipQualifier(dwI);
                                        if(dwI.Equals(1))
                                        {DipClr = 1;}
                                        else
                                        { DipClr = 2;} 
                                        bcVal = readData.GetDecimal(2);
                                        dyI = readData.GetInt32(3);
                                        dyVal = getBTQualifier(dyI);
                                        if (dyI.Equals(1))
                                        { BTClr = 1; }
                                        else
                                        { BTClr = 2; }
                                        cdVal = readData.GetDecimal(4);
                                        eaI = readData.GetInt32(5);
                                        eaVal = getGTQualifier(eaI);
                                        if (eaI.Equals(1))
                                        { GTClr = 1; }
                                        else
                                        { GTClr = 2; }
                                        cgVal = readData.GetDecimal(6);
                                        aoVal = readData.GetDecimal(7);
                                        apVal = readData.GetDecimal(8);
                                        ddVal = readData.GetDecimal(9);
                                        deVal = readData.GetDecimal(10);
                                        hvI = readData.GetInt32(11);
                                        if (hvI.Equals(1))
                                        {
                                            hvVal = "Yes";
                                            NoGoClr = 2;
                                        }
                                        else
                                        {
                                            hvVal = "No";
                                            NoGoClr = 1;
                                        }
                                        iReply.Rows.Add(id, dwVal, DipClr, bcVal, dyVal, BTClr, cdVal, eaVal, GTClr, cgVal, aoVal, apVal, ddVal, deVal, hvVal, NoGoClr);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getDipQualifier(Int32 dipQID)
        {
            try
            {
                String iReply = string.Empty;
                String selData = "SELECT [dpqValue] FROM [dmgDipQualifier] WHERE ([dpqID] = @dpqID)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@dpqID", SqlDbType.Int).Value = dipQID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                                else
                                {
                                    iReply = "---";
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getBTQualifier(Int32 btQID)
        {
            try
            {
                String iReply = string.Empty;
                String selData = "SELECT [btqValue] FROM [dmgBTQualifier] WHERE ([btqID] = @btqID)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@btqID", SqlDbType.Int).Value = btQID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                                else
                                {
                                    iReply = "---";
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getGTQualifier(Int32 gtQID)
        {
            try
            {
                String iReply = string.Empty;
                String selData = "SELECT [gtqValue] FROM [dmgGTQualifier] WHERE ([gtqID] = @gtqID)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@gtqID", SqlDbType.Int).Value = gtQID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                                else
                                {
                                    iReply = "---";
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getBHAStatement(Int32 bhaqID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [bhaqValue] FROM [dmgBHAAnalysis] WHERE ([bhaqID] = @bhaqID)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@bhaqID", SqlDbType.Int).Value = bhaqID.ToString();
                        using(SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if(readData.HasRows)
                                {
                                    while(readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                                else
                                {
                                    iReply = "---";
                                }
                            }
                            catch(Exception ex)
                            {throw new System.Exception(ex.ToString());}
                            finally
                            {
                                dataCon.Close();
                                dataCon.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getResBHATable(Int32 ResRowID, String ClientDBConnection)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn resID = iReply.Columns.Add("resID", typeof(Int32));
                DataColumn azmBHA = iReply.Columns.Add("azmBHA", typeof(Decimal));
                DataColumn colGE = iReply.Columns.Add("colGE", typeof(Decimal));
                DataColumn colEI = iReply.Columns.Add("colEI", typeof(Decimal));
                DataColumn colEJ = iReply.Columns.Add("colEJ", typeof(Decimal));
                DataColumn colEK = iReply.Columns.Add("colEK", typeof(String));
                DataColumn EKClr = iReply.Columns.Add("EKClr", typeof(Int32));
                DataColumn colEL = iReply.Columns.Add("colEL", typeof(Decimal));
                DataColumn colEM = iReply.Columns.Add("colEM", typeof(Decimal));
                DataColumn colEN = iReply.Columns.Add("colEN", typeof(Decimal));
                DataColumn colEO = iReply.Columns.Add("colEO", typeof(String));
                DataColumn EOClr = iReply.Columns.Add("EOClr", typeof(Int32));
                iReply.PrimaryKey = new DataColumn[] { resID };
                Int32 id = -99, ekI = -99, eoI = -99;
                Decimal geVal = -99.00M, eiVal = -99M, ejVal = -99.00M, elVal = -99.00M, emVal = -99.00M, enVal = -99.00M, drVal = -99.00m, bAzm = -99.00M;
                String ekVal = String.Empty, eoVal = String.Empty;
                String selData = "SELECT [resID], [colDR], [colGE], [colEI], [colEJ], [colEK], [colEL], [colEM], [colEN], [colEO] FROM [RawQCResults] WHERE [resID] = @resID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@resID", SqlDbType.Int).Value = ResRowID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        drVal = readData.GetDecimal(1);
                                        geVal = readData.GetDecimal(2);
                                        bAzm = drVal - geVal;
                                        eiVal = readData.GetDecimal(3);
                                        ejVal = readData.GetDecimal(4);
                                        ekI = readData.GetInt32(5);
                                        ekVal = getBHAStatement(ekI);
                                        elVal = readData.GetDecimal(6);
                                        emVal = readData.GetDecimal(7);
                                        enVal = readData.GetDecimal(8);
                                        eoI = readData.GetInt32(9);
                                        eoVal = getBHAStatement(eoI);
                                        iReply.Rows.Add(id, bAzm, geVal, eiVal, ejVal, ekVal, ekI, elVal, emVal, enVal, eoVal, eoI);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }            
        }

        public static DataTable getNMCalcTable(Int32 ResRowID, String ClientDBConnection)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn resID = iReply.Columns.Add("resID", typeof(Int32));
                DataColumn D34 = iReply.Columns.Add("D34", typeof(Decimal));
                DataColumn D35 = iReply.Columns.Add("D35", typeof(Decimal));
                DataColumn D37 = iReply.Columns.Add("D37", typeof(Decimal));
                DataColumn D38 = iReply.Columns.Add("D38", typeof(Decimal));
                DataColumn D39 = iReply.Columns.Add("D39", typeof(Decimal));
                DataColumn G34 = iReply.Columns.Add("G34", typeof(Decimal));
                DataColumn G35 = iReply.Columns.Add("G35", typeof(Decimal));
                DataColumn G37 = iReply.Columns.Add("G37", typeof(Decimal));
                DataColumn G38 = iReply.Columns.Add("G38", typeof(Decimal));
                iReply.PrimaryKey = new DataColumn[] { resID };
                Int32 id = -99;
                Decimal d34Val = -99.00M, d35Val = -99.00M, d37Val = -99.00M, d38Val = -99.00M, d39Val = -99.00M, g34Val = -99.00M, g35Val = -99.00M, g37Val = -99.00M, g38Val = -99.00M;
                String selData = "SELECT [resID], [NMSColD34], [NMSColD35], [NMSColD37], [NMSColD38], [NMSColD39], [NMSColG34], [NMSColG35], [NMSColG37], [NMSColG38] FROM [RawQCResults] WHERE [resID] = @resID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@resID", SqlDbType.Int).Value = ResRowID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        d34Val = readData.GetDecimal(1);
                                        d35Val = readData.GetDecimal(2);
                                        d37Val = readData.GetDecimal(3);
                                        d38Val = readData.GetDecimal(4);
                                        d39Val = readData.GetDecimal(5);
                                        g34Val = readData.GetDecimal(6);
                                        g35Val = readData.GetDecimal(7);
                                        g37Val = readData.GetDecimal(8);
                                        g38Val = readData.GetDecimal(9);
                                        iReply.Rows.Add(id, d34Val, d35Val, d37Val, d38Val, d39Val, g34Val, g35Val, g37Val, g38Val);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getMinMaxTable(Int32 ResRowID, String ClientDBConnection)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn resID = iReply.Columns.Add("resID", typeof(Int32));
                DataColumn EU = iReply.Columns.Add("EU", typeof(Decimal));
                DataColumn EV = iReply.Columns.Add("EV", typeof(Decimal));
                DataColumn EW = iReply.Columns.Add("EW", typeof(Decimal));
                DataColumn EX = iReply.Columns.Add("EX", typeof(Int32));
                DataColumn EY = iReply.Columns.Add("EY", typeof(Int32));
                DataColumn EZ = iReply.Columns.Add("EZ", typeof(Int32));
                DataColumn FA = iReply.Columns.Add("FA", typeof(Decimal));
                DataColumn FB = iReply.Columns.Add("FB", typeof(Decimal));
                DataColumn FC = iReply.Columns.Add("FC", typeof(Decimal));
                iReply.PrimaryKey = new DataColumn[] { resID };
                Int32 id = -99, exVal = -99, eyVal = -99, ezVal = -99;
                Decimal euVal = -99.00M, evVal = -99.00M, ewVal = -99.00M, faVal = -99.0000M, fbVal = -99.0000M, fcVal = -99.0000M;
                String selData = "SELECT [resID], [colEU], [colEV], [colEW], [colEX], [colEY], [colEZ], [colFA], [colFB], [colFC] FROM [RawQCResults] WHERE [resID] = @resID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@resID", SqlDbType.Int).Value = ResRowID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        euVal = readData.GetDecimal(1);
                                        evVal = readData.GetDecimal(2);
                                        ewVal = readData.GetDecimal(3);
                                        exVal = readData.GetInt32(4);
                                        eyVal = readData.GetInt32(5);
                                        ezVal = readData.GetInt32(6);
                                        faVal = readData.GetDecimal(7);
                                        fbVal = readData.GetDecimal(8);
                                        fcVal = readData.GetDecimal(9);
                                        iReply.Rows.Add(id, euVal, evVal, ewVal, exVal, eyVal, ezVal, faVal, fbVal, fcVal);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getTieInSurveysTable(Decimal Depth, Int32 RunID, Int32 WellSectionID, Int32 WellID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn tisID = iReply.Columns.Add("tisID", typeof(Int32));
                DataColumn title = iReply.Columns.Add("title", typeof(String));
                DataColumn md = iReply.Columns.Add("md", typeof(Decimal));
                DataColumn inc = iReply.Columns.Add("inc", typeof(Decimal));
                DataColumn azm = iReply.Columns.Add("azm", typeof(Decimal));
                DataColumn tvd = iReply.Columns.Add("tvd", typeof(Decimal));
                DataColumn north = iReply.Columns.Add("north", typeof(Decimal));
                DataColumn east = iReply.Columns.Add("east", typeof(Decimal));                
                DataColumn vs = iReply.Columns.Add("vs", typeof(Int32));
                DataColumn subsea = iReply.Columns.Add("subsea", typeof(Int32));                                                                               
                System.Data.DataTable rawData = new System.Data.DataTable();
                System.Data.DataTable scData = new System.Data.DataTable();
                System.Data.DataTable bhaData = new System.Data.DataTable();
                rawData = getWPRawTieInSurvey(Depth, RunID, WellSectionID, WellID, ClientDBConnection);
                //scData = getWPSCTieInSurvey(Depth, RunID, WellSectionID, WellID, ClientDBConnection);
                //bhaData = getWPBHATieInSurvey(Depth, RunID, WellSectionID, WellID, ClientDBConnection);
                foreach (System.Data.DataRow dr in rawData.Rows)
                {
                    iReply.Rows.Add(dr.ItemArray);
                }
                //foreach (System.Data.DataRow dr in scData.Rows)
                //{
                //    iReply.Rows.Add(dr.ItemArray);
                //}
                //foreach (System.Data.DataRow dr in bhaData.Rows)
                //{
                //    iReply.Rows.Add(dr.ItemArray);
                //}
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getWellTieInSurveysTable(Decimal Depth, Int32 WellID, String ClientDBConnection)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn tisID = iReply.Columns.Add("tisID", typeof(Int32));
                DataColumn title = iReply.Columns.Add("title", typeof(String));
                DataColumn md = iReply.Columns.Add("md", typeof(Decimal));
                DataColumn inc = iReply.Columns.Add("inc", typeof(Decimal));
                DataColumn azm = iReply.Columns.Add("azm", typeof(Decimal));
                DataColumn tvd = iReply.Columns.Add("tvd", typeof(Decimal));
                DataColumn north = iReply.Columns.Add("north", typeof(Decimal));
                DataColumn east = iReply.Columns.Add("east", typeof(Decimal));
                DataColumn vs = iReply.Columns.Add("vs", typeof(Int32));
                DataColumn subsea = iReply.Columns.Add("subsea", typeof(Int32));
                System.Data.DataTable rawData = new System.Data.DataTable();
                System.Data.DataTable scData = new System.Data.DataTable();
                System.Data.DataTable bhaData = new System.Data.DataTable();
                rawData = getWPRawTieInSurveyForWell(Depth, WellID, ClientDBConnection);
                //scData = getWPSCTieInSurveyForWell(Depth, WellID, ClientDBConnection);
                //bhaData = getWPBHATieInSurveyForWell(Depth, WellID, ClientDBConnection);
                foreach (System.Data.DataRow dr in rawData.Rows)
                {
                    iReply.Rows.Add(dr.ItemArray);
                }
                //foreach (System.Data.DataRow dr in scData.Rows)
                //{
                //    iReply.Rows.Add(dr.ItemArray);
                //}
                //foreach (System.Data.DataRow dr in bhaData.Rows)
                //{
                //    iReply.Rows.Add(dr.ItemArray);
                //}
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal getWellPlanTarget(Int32 OperatorID, Int32 WellPadID, Int32 WellID, String ClientDBConnection)
        {
            Decimal iReply = -99.00M;
            String SelectQueryPlan = "Select MAX([md]) FROM [WellPlanCalculatedData] WHERE [welID] = @welID AND format( ctime , 'yyyyMMddHHmm' ) in (SELECT  format( Max(ctime) , 'yyyyMMddHHmm' ) FROM [WellPlanCalculatedData] where [optrID] = @optrID AND [wpdID] = @wpdID AND [welID] = @welID) ";
            using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
            {
                try
                {
                    dataCon.Open();
                    SqlCommand getData = new SqlCommand(SelectQueryPlan, dataCon);
                    getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                    getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                    getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                    using (SqlDataReader readData = getData.ExecuteReader())
                    {
                        if (readData.HasRows)
                        {
                            while (readData.Read())
                            {
                                iReply = readData.GetDecimal(0);
                            }
                        }
                    }
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
                finally
                {
                    dataCon.Close();
                    dataCon.Dispose();
                }
            }
            return iReply;
        }

        public static ArrayList getRunTargetDepth(Int32 OperatorID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, String ClientDBConnection)
        {
            ArrayList iReply = new ArrayList();
            Int32 lenID = -99; Decimal depth = -99.00M;
            String SelectQueryPlan = "Select [endDepth], [lenU] FROM [Run] WHERE [optrID] = @optrID AND [wpdID] = @wpdID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
            using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
            {
                try
                {
                    dataCon.Open();
                    SqlCommand getData = new SqlCommand(SelectQueryPlan, dataCon);
                    getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                    getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                    getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                    using (SqlDataReader readData = getData.ExecuteReader())
                    {
                        if (readData.HasRows)
                        {
                            while (readData.Read())
                            {
                                depth = readData.GetDecimal(0);
                                iReply.Add(depth);
                                lenID = readData.GetInt32(1);
                                iReply.Add(lenID);
                            }
                        }
                    }
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
                finally
                {
                    dataCon.Close();
                    dataCon.Dispose();
                }
            }
            return iReply;
        }

        public static System.Data.DataTable getWPBHATieInSurvey(Decimal Depth, Int32 RunID, Int32 WellSectionID, Int32 WellID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn tisID = iReply.Columns.Add("tisID", typeof(Int32));
                DataColumn title = iReply.Columns.Add("title", typeof(String));
                DataColumn md = iReply.Columns.Add("md", typeof(Decimal));
                DataColumn inc = iReply.Columns.Add("inc", typeof(Decimal));
                DataColumn azm = iReply.Columns.Add("azm", typeof(Decimal));
                DataColumn tvd = iReply.Columns.Add("tvd", typeof(Decimal));
                DataColumn north = iReply.Columns.Add("north", typeof(Decimal));
                DataColumn east = iReply.Columns.Add("east", typeof(Decimal));
                DataColumn vs = iReply.Columns.Add("vs", typeof(Int32));
                DataColumn subsea = iReply.Columns.Add("subsea", typeof(Int32));
                Int32 count = 0;
                String selData = "SELECT [Depth], [colT], [colDR], [colGE], [bhTvdTI], [bhCummNorthingTI], [bhCummEastingTI], [bhVSTI], [bhSubseaTI] FROM [RawQCResults] WHERE [Depth] = @Depth AND [runID] = @runID AND [wswID] = @wsID AND [welID] = @welID";
                Decimal deep = -99.00M, tiDR = -99.00M, tiGE = -99.00M,  tiAzm = -99.00M, tiInc = -99.00M, tiTVD = -99.0000M, tiVS = -99.0000M, tiCNorth = -99.0000M, tiCEast = -99.0000M, tiSubsea = -99.0000M;
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@Depth", SqlDbType.Int).Value = Depth.ToString();
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        getData.Parameters.AddWithValue("@wsID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count++;
                                        tiTVD = readData.GetDecimal(0);
                                        deep = readData.GetDecimal(1);
                                        tiInc = readData.GetDecimal(2);
                                        tiDR = readData.GetDecimal(3);
                                        tiGE = readData.GetDecimal(4);
                                        tiAzm = tiDR - tiGE;
                                        tiCNorth = readData.GetDecimal(5);
                                        tiCEast = readData.GetDecimal(6);
                                        tiVS = readData.GetDecimal(7);                                        
                                        tiSubsea = readData.GetDecimal(8);
                                        iReply.Rows.Add(count, "BHA", deep, tiInc, tiAzm, tiTVD, tiCNorth, tiCEast, tiVS, tiSubsea);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWPBHATieInSurveyForWell(Decimal Depth, Int32 WellID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn tisID = iReply.Columns.Add("tisID", typeof(Int32));
                DataColumn title = iReply.Columns.Add("title", typeof(String));
                DataColumn md = iReply.Columns.Add("md", typeof(Decimal));
                DataColumn inc = iReply.Columns.Add("inc", typeof(Decimal));
                DataColumn azm = iReply.Columns.Add("azm", typeof(Decimal));
                DataColumn tvd = iReply.Columns.Add("tvd", typeof(Decimal));
                DataColumn north = iReply.Columns.Add("north", typeof(Decimal));
                DataColumn east = iReply.Columns.Add("east", typeof(Decimal));
                DataColumn vs = iReply.Columns.Add("vs", typeof(Int32));
                DataColumn subsea = iReply.Columns.Add("subsea", typeof(Int32));
                Int32 count = 0;
                String selData = "SELECT [Depth], [colT], [colDR], [colGE], [bhTvdTI], [bhCummNorthingTI], [bhCummEastingTI], [bhVSTI], [bhSubseaTI] FROM [RawQCResults] WHERE [Depth] = @Depth AND [welID] = @welID";
                Decimal deep = -99.00M, tiTVD = -99.0000M, tiVS = -99.0000M, tiCNorth = -99.0000M, tiCEast = -99.0000M, tiSubsea = -99.0000M, tiInc = -99.00M, tiAzm = -99.00M, tiDR = -99.00M, tiGE = -99.00M;
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@Depth", SqlDbType.Int).Value = Depth.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count++;
                                        deep = readData.GetDecimal(0);
                                        tiInc = readData.GetDecimal(1);
                                        tiDR = readData.GetDecimal(2);
                                        tiGE = readData.GetDecimal(3);
                                        tiAzm = tiDR - tiGE;
                                        tiTVD = readData.GetDecimal(4);                                        
                                        tiCNorth = readData.GetDecimal(5);
                                        tiCEast = readData.GetDecimal(6);
                                        tiVS = readData.GetDecimal(7);
                                        tiSubsea = readData.GetDecimal(8);
                                        iReply.Rows.Add(count, "BHA", deep, tiInc, tiAzm, tiTVD, tiCNorth, tiCEast, tiVS, tiSubsea);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWPRawTieInSurvey(Decimal Depth, Int32 RunID, Int32 WellSectionID, Int32 WellID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn tisID = iReply.Columns.Add("tisID", typeof(Int32));
                DataColumn title = iReply.Columns.Add("title", typeof(String));
                DataColumn md = iReply.Columns.Add("md", typeof(Decimal));
                DataColumn inc = iReply.Columns.Add("inc", typeof(Decimal));
                DataColumn azm = iReply.Columns.Add("azm", typeof(Decimal));
                DataColumn tvd = iReply.Columns.Add("tvd", typeof(Decimal));
                DataColumn north = iReply.Columns.Add("north", typeof(Decimal));
                DataColumn east = iReply.Columns.Add("east", typeof(Decimal));
                DataColumn vs = iReply.Columns.Add("vs", typeof(Int32));
                DataColumn subsea = iReply.Columns.Add("subsea", typeof(Int32));
                Int32 count = 0;
                String selData = "SELECT [Depth], [colT], [colDR], [rwTvdTI], [rwCummNorthingTI], [rwCummEastingTI], [rwVSTI], [rwSubseaTI] FROM [RawQCResults] WHERE [Depth] = @Depth AND [welID] = @welID";
                Decimal deep = -99.00M, tiTVD = -99.0000M, tiVS = -99.0000M, tiCNorth = -99.0000M, tiCEast = -99.0000M, tiSubsea = -99.0000M, tiInc = -99.00M, tiAzm = -99.00M;
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@Depth", SqlDbType.Int).Value = Depth.ToString();
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        getData.Parameters.AddWithValue("@wsID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count++;
                                        deep = readData.GetDecimal(0);
                                        tiInc = readData.GetDecimal(1);
                                        tiAzm = readData.GetDecimal(2);
                                        tiTVD = readData.GetDecimal(3);
                                        tiCNorth = readData.GetDecimal(4);
                                        tiCEast = readData.GetDecimal(5);
                                        tiVS = readData.GetDecimal(6);                                        
                                        tiSubsea = readData.GetDecimal(7);
                                        iReply.Rows.Add(count, "Raw", deep, tiInc, tiAzm, tiTVD, tiCNorth, tiCEast, tiVS, tiSubsea);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWPRawTieInSurveyForWell(Decimal Depth, Int32 WellID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn tisID = iReply.Columns.Add("tisID", typeof(Int32));
                DataColumn title = iReply.Columns.Add("title", typeof(String));
                DataColumn md = iReply.Columns.Add("md", typeof(Decimal));
                DataColumn inc = iReply.Columns.Add("inc", typeof(Decimal));
                DataColumn azm = iReply.Columns.Add("azm", typeof(Decimal));
                DataColumn tvd = iReply.Columns.Add("tvd", typeof(Decimal));
                DataColumn north = iReply.Columns.Add("north", typeof(Decimal));
                DataColumn east = iReply.Columns.Add("east", typeof(Decimal));
                DataColumn vs = iReply.Columns.Add("vs", typeof(Int32));
                DataColumn subsea = iReply.Columns.Add("subsea", typeof(Int32));
                Int32 count = 0;
                String selData = "SELECT [Depth], [colT], [colDR], [rwTvdTI], [rwCummNorthingTI], [rwCummEastingTI], [rwVSTI], [rwSubseaTI] FROM [RawQCResults] WHERE [Depth] = @Depth AND [welID] = @welID";
                Decimal deep = -99.00M, tiTVD = -99.0000M, tiVS = -99.0000M, tiCNorth = -99.0000M, tiCEast = -99.0000M, tiSubsea = -99.0000M, tiInc = -99.00M, tiAzm = -99.00M;
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@Depth", SqlDbType.Int).Value = Depth.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count++;
                                        deep = readData.GetDecimal(0);
                                        tiInc = readData.GetDecimal(1);
                                        tiAzm = readData.GetDecimal(2);
                                        tiTVD = readData.GetDecimal(3);
                                        tiCNorth = readData.GetDecimal(4);
                                        tiCEast = readData.GetDecimal(5);
                                        tiVS = readData.GetDecimal(6);                                        
                                        tiSubsea = readData.GetDecimal(7);
                                        iReply.Rows.Add(count, "Raw", deep, tiInc, tiAzm, tiTVD, tiCNorth, tiCEast, tiVS, tiSubsea);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWPSCTieInSurvey(Decimal Depth, Int32 RunID, Int32 WellSectionID, Int32 WellID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn tisID = iReply.Columns.Add("tisID", typeof(Int32));
                DataColumn title = iReply.Columns.Add("title", typeof(String));
                DataColumn md = iReply.Columns.Add("md", typeof(Decimal));
                DataColumn inc = iReply.Columns.Add("inc", typeof(Decimal));
                DataColumn azm = iReply.Columns.Add("azm", typeof(Decimal));
                DataColumn tvd = iReply.Columns.Add("tvd", typeof(Decimal));
                DataColumn north = iReply.Columns.Add("north", typeof(Decimal));
                DataColumn east = iReply.Columns.Add("east", typeof(Decimal));
                DataColumn vs = iReply.Columns.Add("vs", typeof(Int32));
                DataColumn subsea = iReply.Columns.Add("subsea", typeof(Int32));
                Int32 count = 0;
                String selData = "SELECT [Depth], [colT], [colDD], [scTvdTI], [scCummNorthingTI], [scCummEastingTI], [scVSTI], [scSubseaTI] FROM [RawQCResults] WHERE [Depth] = @Depth AND [runID] = @runID AND [wswID] = @wsID AND [welID] = @welID";
                Decimal deep = -99.00M, tiInc = -99.00M, tiAzm = -99.00M, tiTVD = -99.0000M, tiVS = -99.0000M, tiCNorth = -99.0000M, tiCEast = -99.0000M, tiSubsea = -99.0000M;
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@Depth", SqlDbType.Int).Value = Depth.ToString();
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        getData.Parameters.AddWithValue("@wsID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count++;
                                        deep = readData.GetDecimal(0);
                                        tiInc = readData.GetDecimal(1);
                                        tiAzm = readData.GetDecimal(2);
                                        tiTVD = readData.GetDecimal(3);                                        
                                        tiCNorth = readData.GetDecimal(4);
                                        tiCEast = readData.GetDecimal(5);
                                        tiVS = readData.GetDecimal(6);
                                        tiSubsea = readData.GetDecimal(7);
                                        iReply.Rows.Add(count, "Short-Collar", deep, tiInc, tiAzm, tiTVD, tiCNorth, tiCEast, tiVS, tiSubsea);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWPSCTieInSurveyForWell(Decimal Depth, Int32 WellID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn tisID = iReply.Columns.Add("tisID", typeof(Int32));
                DataColumn title = iReply.Columns.Add("title", typeof(String));
                DataColumn md = iReply.Columns.Add("md", typeof(Decimal));
                DataColumn inc = iReply.Columns.Add("inc", typeof(Decimal));
                DataColumn azm = iReply.Columns.Add("azm", typeof(Decimal));
                DataColumn tvd = iReply.Columns.Add("tvd", typeof(Decimal));
                DataColumn north = iReply.Columns.Add("north", typeof(Decimal));
                DataColumn east = iReply.Columns.Add("east", typeof(Decimal));
                DataColumn vs = iReply.Columns.Add("vs", typeof(Int32));
                DataColumn subsea = iReply.Columns.Add("subsea", typeof(Int32));
                Int32 count = 0;
                String selData = "SELECT [Depth], [colT], [colDR], [scTvdTI], [scCummNorthingTI], [scCummEastingTI], [scVSTI], [scSubseaTI] FROM [RawQCResults] WHERE [Depth] = @Depth AND [welID] = @welID";
                Decimal deep = -99.00M, tiTVD = -99.0000M, tiVS = -99.0000M, tiCNorth = -99.0000M, tiCEast = -99.0000M, tiSubsea = -99.0000M, tiInc = -99.00M, tiAzm = -99.00M;
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@Depth", SqlDbType.Int).Value = Depth.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count++;
                                        deep = readData.GetDecimal(0);
                                        tiInc = readData.GetDecimal(1);
                                        tiAzm = readData.GetDecimal(2);
                                        tiTVD = readData.GetDecimal(3);                                        
                                        tiCNorth = readData.GetDecimal(4);
                                        tiCEast = readData.GetDecimal(5);
                                        tiVS = readData.GetDecimal(6);
                                        tiSubsea = readData.GetDecimal(7);
                                        iReply.Rows.Add(count, "Short-Collar", deep, tiInc, tiAzm, tiTVD, tiCNorth, tiCEast, tiVS, tiSubsea);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWPCorrTieInSurvey(Decimal Depth, Int32 RunID, Int32 WellSectionID, Int32 WellID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn tisID = iReply.Columns.Add("tisID", typeof(Int32));
                DataColumn title = iReply.Columns.Add("title", typeof(String));
                DataColumn tvd = iReply.Columns.Add("tvd", typeof(Decimal));
                DataColumn north = iReply.Columns.Add("north", typeof(Decimal));
                DataColumn east = iReply.Columns.Add("east", typeof(Decimal));
                DataColumn vs = iReply.Columns.Add("vs", typeof(Int32));
                DataColumn subsea = iReply.Columns.Add("subsea", typeof(Int32));
                DataColumn dls = iReply.Columns.Add("dls", typeof(Int32));
                DataColumn brate = iReply.Columns.Add("brate", typeof(Decimal));
                DataColumn trate = iReply.Columns.Add("trate", typeof(Decimal));
                DataColumn northing = iReply.Columns.Add("northing", typeof(Decimal));
                DataColumn easting = iReply.Columns.Add("easting", typeof(Decimal));
                Int32 count = 0;
                String selData = "SELECT [crTvdTI], [crVSTI], [crCummNorthingTI], [crCummEastingTI], [crSubseaTI], [crDogleg100TI], [crBRate100TI], [crTRate100TI], [crNorthingTI], [crEastingTI] FROM [RawQCResults] WHERE [Depth] = @Depth AND [runID] = @runID AND [wswID] = @wsID AND [welID] = @welID";
                Decimal tiTVD = -99.0000M, tiVS = -99.0000M, tiCNorth = -99.0000M, tiCEast = -99.0000M, tiSubsea = -99.0000M, tiDL = -99.0000M, tiBR = -99.0000M, tiTR = -99.0000M, tiN = -99.0000M, tiE = -99.0000M;
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@Depth", SqlDbType.Int).Value = Depth.ToString();
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        getData.Parameters.AddWithValue("@wsID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count++;
                                        tiTVD = readData.GetDecimal(0);
                                        tiVS = readData.GetDecimal(1);
                                        tiCNorth = readData.GetDecimal(2);
                                        tiCEast = readData.GetDecimal(3);
                                        tiSubsea = readData.GetDecimal(4);
                                        tiDL = readData.GetDecimal(5);
                                        tiBR = readData.GetDecimal(6);
                                        tiTR = readData.GetDecimal(7);
                                        tiN = readData.GetDecimal(8);
                                        tiE = readData.GetDecimal(9);
                                        iReply.Rows.Add(count, "MSA", tiTVD, tiVS, tiCNorth, tiCEast, tiSubsea, tiDL, tiBR, tiTR, tiN, tiE);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWPCorrTieInSurveyForWell(Decimal Depth, Int32 WellID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn tisID = iReply.Columns.Add("tisID", typeof(Int32));
                DataColumn title = iReply.Columns.Add("title", typeof(String));
                DataColumn tvd = iReply.Columns.Add("tvd", typeof(Decimal));
                DataColumn north = iReply.Columns.Add("north", typeof(Decimal));
                DataColumn east = iReply.Columns.Add("east", typeof(Decimal));
                DataColumn vs = iReply.Columns.Add("vs", typeof(Int32));
                DataColumn subsea = iReply.Columns.Add("subsea", typeof(Int32));
                DataColumn dls = iReply.Columns.Add("dls", typeof(Int32));
                DataColumn brate = iReply.Columns.Add("brate", typeof(Decimal));
                DataColumn trate = iReply.Columns.Add("trate", typeof(Decimal));
                DataColumn northing = iReply.Columns.Add("northing", typeof(Decimal));
                DataColumn easting = iReply.Columns.Add("easting", typeof(Decimal));
                Int32 count = 0;
                String selData = "SELECT [crTvdTI], [crVSTI], [crCummNorthingTI], [crCummEastingTI], [crSubseaTI], [crDogleg100TI], [crBRate100TI], [crTRate100TI], [crNorthingTI], [crEastingTI] FROM [RawQCResults] WHERE [Depth] = @Depth AND [welID] = @welID";
                Decimal tiTVD = -99.0000M, tiVS = -99.0000M, tiCNorth = -99.0000M, tiCEast = -99.0000M, tiSubsea = -99.0000M, tiDL = -99.0000M, tiBR = -99.0000M, tiTR = -99.0000M, tiN = -99.0000M, tiE = -99.0000M;
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@Depth", SqlDbType.Int).Value = Depth.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count++;
                                        tiTVD = readData.GetDecimal(0);
                                        tiVS = readData.GetDecimal(1);
                                        tiCNorth = readData.GetDecimal(2);
                                        tiCEast = readData.GetDecimal(3);
                                        tiSubsea = readData.GetDecimal(4);
                                        tiDL = readData.GetDecimal(5);
                                        tiBR = readData.GetDecimal(6);
                                        tiTR = readData.GetDecimal(7);
                                        tiN = readData.GetDecimal(8);
                                        tiE = readData.GetDecimal(9);
                                        iReply.Rows.Add(count, "MSA", tiTVD, tiVS, tiCNorth, tiCEast, tiSubsea, tiDL, tiBR, tiTR, tiN, tiE);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWPCalcTieInSurvey(Int32 ResRowID, Int32 WellID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn tisID = iReply.Columns.Add("tisID", typeof(Int32));
                DataColumn title = iReply.Columns.Add("title", typeof(String));
                DataColumn tvd = iReply.Columns.Add("tvd", typeof(Decimal));
                DataColumn north = iReply.Columns.Add("north", typeof(Decimal));
                DataColumn east = iReply.Columns.Add("east", typeof(Decimal));
                DataColumn vs = iReply.Columns.Add("vs", typeof(Int32));
                DataColumn subsea = iReply.Columns.Add("subsea", typeof(Int32));
                DataColumn dls = iReply.Columns.Add("dls", typeof(Int32));
                DataColumn brate = iReply.Columns.Add("brate", typeof(Decimal));
                DataColumn trate = iReply.Columns.Add("trate", typeof(Decimal));
                DataColumn northing = iReply.Columns.Add("northing", typeof(Decimal));
                DataColumn easting = iReply.Columns.Add("easting", typeof(Decimal));
                Int32 count = 0;
                String selData = "SELECT [tvd], [vs], [cummNorthing], [cummEasting], [subsea], [dogleg100], [bRate100], [tRate100], [northing], [easting] FROM [WellPlanCalculatedData] WHERE [datarowID] = @datarowID AND [welID] = @welID";
                Decimal tiTVD = -99.0000M, tiVS = -99.0000M, tiCNorth = -99.0000M, tiCEast = -99.0000M, tiSubsea = -99.0000M, tiDL = -99.0000M, tiBR = -99.0000M, tiTR = -99.0000M, tiN = -99.0000M, tiE = -99.0000M;
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@datarowID", SqlDbType.Int).Value = ResRowID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count++;
                                        tiTVD = readData.GetDecimal(0);
                                        tiVS = readData.GetDecimal(1);
                                        tiCNorth = readData.GetDecimal(2);
                                        tiCEast = readData.GetDecimal(3);
                                        tiSubsea = readData.GetDecimal(4);
                                        tiDL = readData.GetDecimal(5);
                                        tiBR = readData.GetDecimal(6);
                                        tiTR = readData.GetDecimal(7);
                                        tiN = readData.GetDecimal(8);
                                        tiE = readData.GetDecimal(9);
                                        iReply.Rows.Add(count, "Plan", tiTVD, tiVS, tiCNorth, tiCEast, tiSubsea, tiDL, tiBR, tiTR, tiN, tiE);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static ArrayList ProcessRawData(Int32 OperatorID, Int32 ClientID, Int32 ServiceID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Int32 DataFormatType, Int32 DataEntryType, Int32 InputLengthUnit, Int32 InputAccelerometerUnit, Int32 InputMagnetometerUnit, String FileName, List<System.Data.DataTable> UploadedDataTables, System.Data.DataTable SelectedTables, String UserName, String UserRealm, String ClientDBConnection)
        {
            try
            {
                ArrayList iReply = new ArrayList(), Headlist = new ArrayList(), DetailList = new ArrayList(), rowValid = new ArrayList(), rowInvalid = new ArrayList();
                ArrayList RefMags = new ArrayList(), SQCInfo = new ArrayList(), RunInfo = new ArrayList(), BHAInfo = new ArrayList();
                ArrayList dflInfo = new ArrayList(), dcpsInfo = new ArrayList(), ngzInfo = new ArrayList(); //No Go Zone Azimuth          
                ArrayList tieInList = new ArrayList(), wellMetaInfo = new ArrayList();
                List<Int32> WellInfo = new List<Int32> { };                
                Int32 sqcID = -99, boxyError = -99;
                //Meta Info                
                WellInfo = AST.getWellDirection(WellID, ClientDBConnection);
                RefMags = OPR.getRefMagList(OperatorID, ClientID, WellPadID, WellID, ClientDBConnection);
                RunInfo = AST.getRunList(RunID, ClientDBConnection);
                sqcID = Convert.ToInt32(RunInfo[5]);
                SQCInfo = AST.getSelectedSQCList(sqcID, ClientDBConnection);
                BHAInfo = AST.getBhaInfoList(RunID, ClientDBConnection);
                dflInfo = DMG.getdflMetaDataList();
                dcpsInfo = DMG.getdcpsDataList();
                ngzInfo = DMG.getngzDataList();
                boxyError = DMG.getBoxyError();
                tieInList = WPN.getWellPlanTieInSurveyList(RunID, WellSectionID, WellID, ClientDBConnection);                    
                wellMetaInfo = WPN.getWellMetaInfoList(WellID, WellPadID, ClientDBConnection);
                switch (ServiceID)
                {
                    //SMARTs Raw Data Audit/QC
                    case 33: {
                        iReply = surveyQC(UserName, UserRealm, OperatorID, ClientID, WellPadID, WellID, WellSectionID, RunID, ServiceID, SelectedTables, RefMags, SQCInfo, RunInfo, BHAInfo, dflInfo, dcpsInfo, ngzInfo, wellMetaInfo, WellInfo, boxyError, tieInList, InputLengthUnit, InputAccelerometerUnit, InputMagnetometerUnit, DataEntryType, FileName, ClientDBConnection);                                                                       
                        break;
                    }
                    //SMARTs Results Data Audit/QC
                    case 34: {
                        iReply = surveyQC(UserName, UserRealm, OperatorID, ClientID, WellPadID, WellID, WellSectionID, RunID, ServiceID, SelectedTables, RefMags, SQCInfo, RunInfo, BHAInfo, dflInfo, dcpsInfo, ngzInfo, wellMetaInfo, WellInfo, boxyError, tieInList, InputLengthUnit, InputAccelerometerUnit, InputMagnetometerUnit, DataEntryType, FileName, ClientDBConnection);
                        break;
                    }
                    //SMARTs 3rd Party Raw Data Audit/QC
                    case 37: {
                        iReply = surveyQC(UserName, UserRealm, OperatorID, ClientID, WellPadID, WellID, WellSectionID, RunID, ServiceID, SelectedTables, RefMags, SQCInfo, RunInfo, BHAInfo, dflInfo, dcpsInfo, ngzInfo, wellMetaInfo, WellInfo, boxyError, tieInList, InputLengthUnit, InputAccelerometerUnit, InputMagnetometerUnit, DataEntryType, FileName, ClientDBConnection);                                                                       
                        break; }
                    //SMARTs 3rd Party Results Data Audit/QC
                    case 38: {
                        iReply = surveyQC(UserName, UserRealm, OperatorID, ClientID, WellPadID, WellID, WellSectionID, RunID, ServiceID, SelectedTables, RefMags, SQCInfo, RunInfo, BHAInfo, dflInfo, dcpsInfo, ngzInfo, wellMetaInfo, WellInfo, boxyError, tieInList, InputLengthUnit, InputAccelerometerUnit, InputMagnetometerUnit, DataEntryType, FileName, ClientDBConnection);                                                                       
                        break; }
                    //Historic Well Raw Data Audit/QC Service
                    case 39: {
                        iReply = surveyQC(UserName, UserRealm, OperatorID, ClientID, WellPadID, WellID, WellSectionID, RunID, ServiceID, SelectedTables, RefMags, SQCInfo, RunInfo, BHAInfo, dflInfo, dcpsInfo, ngzInfo, wellMetaInfo, WellInfo, boxyError, tieInList, InputLengthUnit, InputAccelerometerUnit, InputMagnetometerUnit, DataEntryType, FileName, ClientDBConnection);                                                                       
                        break; }
                    //Historic Well Results Data Audit/QC
                    case 40: {
                        iReply = surveyQC(UserName, UserRealm, OperatorID, ClientID, WellPadID, WellID, WellSectionID, RunID, ServiceID, SelectedTables, RefMags, SQCInfo, RunInfo, BHAInfo, dflInfo, dcpsInfo, ngzInfo, wellMetaInfo, WellInfo, boxyError, tieInList, InputLengthUnit, InputAccelerometerUnit, InputMagnetometerUnit, DataEntryType, FileName, ClientDBConnection);                                                                       
                        break; }
                    //SMARTs Raw Data Audit/QC with Correction
                    case 46: {
                        iReply = surveyQC(UserName, UserRealm, OperatorID, ClientID, WellPadID, WellID, WellSectionID, RunID, ServiceID, SelectedTables, RefMags, SQCInfo, RunInfo, BHAInfo, dflInfo, dcpsInfo, ngzInfo, wellMetaInfo, WellInfo, boxyError, tieInList, InputLengthUnit, InputAccelerometerUnit, InputMagnetometerUnit, DataEntryType, FileName, ClientDBConnection);                                                                       
                        break; }
                    //SMARTs Azimuth Correction
                    default: {
                        iReply = surveyQC(UserName, UserRealm, OperatorID, ClientID, WellPadID, WellID, WellSectionID, RunID, ServiceID, SelectedTables, RefMags, SQCInfo, RunInfo, BHAInfo, dflInfo, dcpsInfo, ngzInfo, wellMetaInfo, WellInfo, boxyError, tieInList, InputLengthUnit, InputAccelerometerUnit, InputMagnetometerUnit, DataEntryType, FileName, ClientDBConnection);                                                                       
                        break; }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static int getRunCount(Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 WellPadID, Int32 ClientID, Int32 OperatorID, String dbConn)
        {
            try
            {
                Int32 runCount;                
                String selData = "SELECT Count(runID) FROM [RawQCResults] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        int count = Convert.ToInt32(getData.ExecuteScalar());
                        dataCon.Close();
                        if (count != 0)
                        {
                            runCount = count;
                        }
                        else
                        {
                           runCount = 0;
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                 }
                 return runCount;
            }                                            
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }        
        }

        public static Int32 getNextSequence(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT ISNULL(MAX(srvyRowID), 0) + 1 FROM [SurveyFormattedData] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @RunID";
                using (SqlConnection Conn = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        Conn.Open();  // Connection is opened to get runID and next sequence
                        SqlCommand nseqCMD = new SqlCommand(selData, Conn);
                        nseqCMD.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        nseqCMD.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        nseqCMD.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        nseqCMD.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        nseqCMD.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        nseqCMD.Parameters.AddWithValue("@RunID", SqlDbType.Int).Value = RunID.ToString();                        
                        using (SqlDataReader nseqRdr = nseqCMD.ExecuteReader())
                        {
                            try
                            {
                                while (nseqRdr.Read())
                                {
                                    iReply = nseqRdr.GetInt32(0);
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                nseqRdr.Close();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        Conn.Close();
                        Conn.Dispose();
                    }                    
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getDataTableforQCWellCompare( List<int> WellID, String dbConn)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn Survey_number = iReply.Columns.Add("Survey_number", typeof(Int32));

                DataColumn Inclination = iReply.Columns.Add("Inclination", typeof(Decimal));
                DataColumn Azimuth = iReply.Columns.Add("Azimuth", typeof(Decimal));
                DataColumn Dip = iReply.Columns.Add("Dip", typeof(Decimal));
                DataColumn BTotal = iReply.Columns.Add("B-Total", typeof(Decimal));
                DataColumn MDepth = iReply.Columns.Add("MDepth", typeof(Decimal));
                DataColumn TVD = iReply.Columns.Add("TVD", typeof(Decimal));
                DataColumn Northing = iReply.Columns.Add("Northing", typeof(Decimal));
                DataColumn Easting = iReply.Columns.Add("Easting", typeof(Decimal));
                DataColumn WellName = iReply.Columns.Add("WellName", typeof(String));
                DataColumn PlotDepth = iReply.Columns.Add("PlotDepth", typeof(Decimal));

                Int32 count = 0;
                Decimal inc = -99.0000M, azm = -99.00M, dp = -99.00M, bt = -99.00M, depth = -99.00M, plotdepth = -99.00M, tvd = -99.00M, northing = -99.00M, easting = -99.00M;
                String wellname = "";
                String getwellid = "";

                getwellid = "(''";
                foreach (int id in WellID)
                {
                    getwellid += "," + id.ToString() ;
                }
                getwellid += ")";

                String SelectQuery = "Select cast ( ROW_NUMBER() over (order by Depth) as int), [colT], [colDR], [colBA], [colCA] , [Depth] , [rwTVD] , [rwNorthing] , [rwEasting] , [Well].welName , [rwTVD] ";
                       SelectQuery += " FROM [RawQCResults] as RQCR , [Well] ";
                       SelectQuery += " WHERE [Well].welID = RQCR.welID ";
                       SelectQuery += " AND RQCR.welID in "; 
                       SelectQuery += getwellid; 
                       SelectQuery += " ORDER BY [Well].welName,  [Depth] ASC";
                
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        //cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        //count++;
                                        count = readData.GetInt32(0);
                                        inc = readData.GetDecimal(1);
                                        azm = readData.GetDecimal(2);
                                        dp = readData.GetDecimal(3);
                                        bt = readData.GetDecimal(4);
                                        depth = readData.GetDecimal(5);
                                        tvd = readData.GetDecimal(6);
                                        northing = readData.GetDecimal(7);
                                        easting = readData.GetDecimal(8);
                                        wellname = readData.GetString(9);
                                        plotdepth = readData.GetDecimal(10);
                                        iReply.Rows.Add(count, inc, azm, dp, bt, depth, tvd, northing, easting, wellname, plotdepth);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close(); // this is used for SQLDataReader
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static decimal getMaxTVDforWellsInComparrison(List<int> WellID, String dbConn)
        {
            try
            {
                decimal maxdepth = 0;

                String getwellid = "";

                getwellid = "(''";
                foreach (int id in WellID)
                {
                    getwellid += "," + id.ToString();
                }
                getwellid += ")";

                String SelectQuery = "Select max(rwTVD) ";
                SelectQuery += " FROM [RawQCResults] as RQCR , [Well] ";
                SelectQuery += " WHERE [Well].welID = RQCR.welID ";
                SelectQuery += " AND RQCR.welID in ";
                SelectQuery += getwellid;
                SelectQuery += " ; ";

                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        //cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        //count++;
                                        maxdepth = readData.GetDecimal(0);
                                    }
                                }
                                else
                                {
                                    maxdepth = 1000;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close(); // this is used for SQLDataReader
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return maxdepth;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }


        }

        public static DataTable getDataTableforQCWellGraphAZIM(Int32 WellID, String dbConn)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn Survey_number = iReply.Columns.Add("Survey_number", typeof(Int32));
                DataColumn MDepth = iReply.Columns.Add("MDepth", typeof(Decimal));
                DataColumn Azimuth = iReply.Columns.Add("Azimuth", typeof(Decimal));
                DataColumn ShortAzimuth = iReply.Columns.Add("ShortAzimuth", typeof(Decimal));
                DataColumn BHAAzimuth = iReply.Columns.Add("BHAAzimuth", typeof(Decimal));


                Int32 count = 0;
                Decimal inc = -99.0000M, azm = -99.00M, depth = -99.00M, bhaAzm = -99.00M, shortAzm = -99.00M;
                //String SelectQuery = "Select cast ( ROW_NUMBER() over (order by Depth) as int), [colT], [colDR], [colBA], [colCA] , [Depth] FROM [RawQCResults] WHERE [welID] = @welID ORDER BY [Depth] ASC ";
                String SelectQuery = "Select cast ( ROW_NUMBER() over (order by Depth) as int), [Depth],  [colDR] , [colDD] , ([colDR] - [colGE])  FROM [RawQCResults] WHERE [welID] = @welID ORDER BY [Depth] ASC ";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        //count++;
                                        count = readData.GetInt32(0);
                                        depth = readData.GetDecimal(1);
                                        azm = readData.GetDecimal(2);
                                        shortAzm = readData.GetDecimal(3);
                                        bhaAzm = readData.GetDecimal(4);
                                        iReply.Rows.Add(count, depth, azm, shortAzm , bhaAzm);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close(); // this is used for SQLDataReader
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal getMaxDepthForTieInSurveys(Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 WellPadID, Int32 ClientID, Int32 OperatorID, String ClientDBConnection)
        {
            try
            {
                Decimal iReply = -99.00M;
                String selData = "SELECT ISNULL(MAX(Depth), 0) FROM [RawQCResults] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();                        
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();                        
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetDecimal(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal getMaxWellDepthForTieInSurveys(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, String ClientDBConnection)
        {
            try
            {
                Decimal iReply = -99.00M;
                String selData = "SELECT ISNULL(MAX(Depth), 0) FROM [RawQCResults] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID AND [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetDecimal(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getLastAzimuthCriteriaID(Int32 OperatorID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String SelectQuery = "SELECT [acID] FROM [RawQCResults] WHERE [optrID] = @optrID AND [wpdID] = @wpdID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        cmd.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        cmd.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        cmd.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    iReply = 4; //dmgAzimuthCriteria 4 = Raw Azimuth
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close(); // this is used for SQLDataReader
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getAzimuthCriteriaID(Int32 OperatorID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Int32 SurveyID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String SelectQuery = "SELECT [acID] FROM [RawQCResults] WHERE [optrID] = @optrID AND [wpdID] = @wpdID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID AND [srvyID] = @srvyID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        cmd.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        cmd.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        cmd.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        cmd.Parameters.AddWithValue("@srvyID", SqlDbType.Int).Value = SurveyID.ToString();
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getAzimuthCriteriaIDForWell(Int32 OperatorID, Int32 WellPadID, Int32 WellID, Int32 SurveyID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String SelectQuery = "SELECT [acID] FROM [RawQCResults] WHERE [optrID] = @optrID AND [wpdID] = @wpdID AND [welID] = @welID AND [srvyID] = @srvyID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        cmd.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        cmd.Parameters.AddWithValue("@srvyID", SqlDbType.Int).Value = SurveyID.ToString();
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getAzimuthCriteriaIDForResultRow(Int32 ResultRowID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String SelectQuery = "SELECT [acID] FROM [RawQCResults] WHERE [resID] = @resID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@resID", SqlDbType.Int).Value = ResultRowID.ToString();
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getPlacementTableforSelectedAzimuthCriteria(Int32 OperatorID, Int32 WellPadID, Int32 WellID, Decimal MeasuredDepth, Int32 SelectedAzimuth, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                switch (SelectedAzimuth)
                {
                    case 1: 
                        { 
                            //BHA 
                            iReply = getPlacementTableforBHAAzimuthCriteria(OperatorID, WellPadID, WellID, MeasuredDepth, SelectedAzimuth, ClientDBString);
                            break; 
                        }
                    case 2: 
                        { 
                            //Corrected
                            iReply = getPlacementTableforSelectedCrrCriteria(OperatorID, WellPadID, WellID, MeasuredDepth, SelectedAzimuth, ClientDBString);
                            break; 
                        }
                    case 3: 
                        {
                            //MSSR
                            iReply = getPlacementTableforSelectedMSSRCriteria(OperatorID, WellPadID, WellID, MeasuredDepth, SelectedAzimuth, ClientDBString);
                            break; 
                        }
                    case 4: 
                        {
                            //Raw
                            iReply = getPlacementTableforRawAzimuthCriteria(OperatorID, WellPadID, WellID, MeasuredDepth, SelectedAzimuth, ClientDBString);
                            break; 
                        }
                    default: 
                        {
                            //Short-Collar
                            iReply = getPlacementTableforSCAzimuthCriteria(OperatorID, WellPadID, WellID, MeasuredDepth, SelectedAzimuth, ClientDBString);
                            break; 
                        }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getPlacementTableforRawAzimuthCriteria(Int32 OperatorID, Int32 WellPadID, Int32 WellID, Decimal MeasuredDepth, Int32 SelectedAzimuth, String ClientDBString)
        {            
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn srvID = iReply.Columns.Add("srvID", typeof(Int32));
                DataColumn tvd = iReply.Columns.Add("tvd", typeof(Decimal));
                DataColumn north = iReply.Columns.Add("north", typeof(Decimal));
                DataColumn east = iReply.Columns.Add("east", typeof(Decimal));
                DataColumn vs = iReply.Columns.Add("vs", typeof(Decimal));
                DataColumn subsea = iReply.Columns.Add("subsea", typeof(Decimal));
                DataColumn dls = iReply.Columns.Add("dls", typeof(Decimal));
                DataColumn northing = iReply.Columns.Add("northing", typeof(Decimal));
                DataColumn easting = iReply.Columns.Add("easting", typeof(Decimal));
                String selData = "SELECT [srvID], [rwTVD], [rwCummNorthing], [rwCummEasting], [rwDeltaVS], [rwSubsea], [rwDogleg], [rwNorthing], [rwEasting] FROM [RawQCResults] WHERE [optrID] = @optrID AND [wpdID] = @wpdID AND [welID] = @welID AND [Depth] = @Depth AND [acID] = @acID";
                Int32 sID = -99;
                Decimal rwTVDVal = -99.00M, rwCNVal = -99.00M, rwCEVal = -99.00M, rwDVSVal = -99.00M, rwSSVal = -99.00M, rwDLVal = -99.00M, rwNVal = -99.00M, rwEVal = -99.00M;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    dataCon.Open();
                    SqlCommand getData = new SqlCommand(selData, dataCon);
                    getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                    getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                    getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                    getData.Parameters.AddWithValue("@Depth", SqlDbType.Decimal).Value = MeasuredDepth.ToString();
                    getData.Parameters.AddWithValue("@acID", SqlDbType.Int).Value = SelectedAzimuth.ToString();
                    using (SqlDataReader readData = getData.ExecuteReader())
                    {
                        if (readData.HasRows)
                        {
                            while (readData.Read())
                            {
                                sID = readData.GetInt32(0);
                                rwTVDVal = readData.GetDecimal(1);
                                rwCNVal = readData.GetDecimal(2);
                                rwCEVal = readData.GetDecimal(3);
                                rwDVSVal = readData.GetDecimal(4);
                                rwSSVal = readData.GetDecimal(5);
                                rwDLVal = readData.GetDecimal(6);
                                rwNVal = readData.GetDecimal(7);
                                rwEVal = readData.GetDecimal(8);
                                iReply.Rows.Add(sID, rwTVDVal, rwCNVal, rwCEVal, rwDVSVal, rwSSVal, rwDLVal, rwNVal, rwEVal);
                                iReply.AcceptChanges();
                            }
                        }
                    }
                }
                return iReply;
        }

        public static System.Data.DataTable getPlacementTableforSCAzimuthCriteria(Int32 OperatorID, Int32 WellPadID, Int32 WellID, Decimal MeasuredDepth, Int32 SelectedAzimuth, String ClientDBString)
        {
            System.Data.DataTable iReply = new System.Data.DataTable();
            DataColumn srvID = iReply.Columns.Add("srvID", typeof(Int32));
            DataColumn tvd = iReply.Columns.Add("tvd", typeof(Decimal));
            DataColumn north = iReply.Columns.Add("north", typeof(Decimal));
            DataColumn east = iReply.Columns.Add("east", typeof(Decimal));
            DataColumn vs = iReply.Columns.Add("vs", typeof(Decimal));
            DataColumn subsea = iReply.Columns.Add("subsea", typeof(Decimal));
            DataColumn dls = iReply.Columns.Add("dls", typeof(Decimal));
            DataColumn northing = iReply.Columns.Add("northing", typeof(Decimal));
            DataColumn easting = iReply.Columns.Add("easting", typeof(Decimal));
            String selData = "SELECT [srvID], [scTVD], [scCummNorthing], [scCummEasting], [scDeltaVS], [scSubsea], [scDogleg], [scNorthing], [scEasting] FROM [RawQCResults] WHERE [optrID] = @optrID AND [wpdID] = @wpdID AND [welID] = @welID AND [Depth] = @Depth AND [acID] = @acID";
            Int32 sID = -99;
            Decimal rwTVDVal = -99.00M, rwCNVal = -99.00M, rwCEVal = -99.00M, rwDVSVal = -99.00M, rwSSVal = -99.00M, rwDLVal = -99.00M, rwNVal = -99.00M, rwEVal = -99.00M;
            using (SqlConnection dataCon = new SqlConnection(ClientDBString))
            {
                dataCon.Open();
                SqlCommand getData = new SqlCommand(selData, dataCon);
                getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                getData.Parameters.AddWithValue("@Depth", SqlDbType.Decimal).Value = MeasuredDepth.ToString();
                getData.Parameters.AddWithValue("@acID", SqlDbType.Int).Value = SelectedAzimuth.ToString();
                using (SqlDataReader readData = getData.ExecuteReader())
                {
                    if (readData.HasRows)
                    {
                        while (readData.Read())
                        {
                            sID = readData.GetInt32(0);
                            rwTVDVal = readData.GetDecimal(1);
                            rwCNVal = readData.GetDecimal(2);
                            rwCEVal = readData.GetDecimal(3);
                            rwDVSVal = readData.GetDecimal(4);
                            rwSSVal = readData.GetDecimal(5);
                            rwDLVal = readData.GetDecimal(6);
                            rwNVal = readData.GetDecimal(7);
                            rwEVal = readData.GetDecimal(8);
                            iReply.Rows.Add(sID, rwTVDVal, rwCNVal, rwCEVal, rwDVSVal, rwSSVal, rwDLVal, rwNVal, rwEVal);
                            iReply.AcceptChanges();
                        }
                    }
                }
            }
            return iReply;
        }

        public static System.Data.DataTable getPlacementTableforBHAAzimuthCriteria(Int32 OperatorID, Int32 WellPadID, Int32 WellID, Decimal MeasuredDepth, Int32 SelectedAzimuth, String ClientDBString)
        {
            System.Data.DataTable iReply = new System.Data.DataTable();
            DataColumn srvID = iReply.Columns.Add("srvID", typeof(Int32));
            DataColumn tvd = iReply.Columns.Add("tvd", typeof(Decimal));
            DataColumn north = iReply.Columns.Add("north", typeof(Decimal));
            DataColumn east = iReply.Columns.Add("east", typeof(Decimal));
            DataColumn vs = iReply.Columns.Add("vs", typeof(Decimal));
            DataColumn subsea = iReply.Columns.Add("subsea", typeof(Decimal));
            DataColumn dls = iReply.Columns.Add("dls", typeof(Decimal));
            DataColumn northing = iReply.Columns.Add("northing", typeof(Decimal));
            DataColumn easting = iReply.Columns.Add("easting", typeof(Decimal));
            String selData = "SELECT [srvID], [bhTVD], [bhCummNorthing], [bhCummEasting], [bhDeltaVS], [bhSubsea], [bhDogleg], [bhNorthing], [bhEasting] FROM [RawQCResults] WHERE [optrID] = @optrID AND [wpdID] = @wpdID AND [welID] = @welID AND [Depth] = @Depth AND [acID] = @acID";
            Int32 sID = -99;
            Decimal rwTVDVal = -99.00M, rwCNVal = -99.00M, rwCEVal = -99.00M, rwDVSVal = -99.00M, rwSSVal = -99.00M, rwDLVal = -99.00M, rwNVal = -99.00M, rwEVal = -99.00M;
            using (SqlConnection dataCon = new SqlConnection(ClientDBString))
            {
                dataCon.Open();
                SqlCommand getData = new SqlCommand(selData, dataCon);
                getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                getData.Parameters.AddWithValue("@Depth", SqlDbType.Decimal).Value = MeasuredDepth.ToString();
                getData.Parameters.AddWithValue("@acID", SqlDbType.Int).Value = SelectedAzimuth.ToString();
                using (SqlDataReader readData = getData.ExecuteReader())
                {
                    if (readData.HasRows)
                    {
                        while (readData.Read())
                        {
                            sID = readData.GetInt32(0);
                            rwTVDVal = readData.GetDecimal(1);
                            rwCNVal = readData.GetDecimal(2);
                            rwCEVal = readData.GetDecimal(3);
                            rwDVSVal = readData.GetDecimal(4);
                            rwSSVal = readData.GetDecimal(5);
                            rwDLVal = readData.GetDecimal(6);
                            rwNVal = readData.GetDecimal(7);
                            rwEVal = readData.GetDecimal(8);
                            iReply.Rows.Add(sID, rwTVDVal, rwCNVal, rwCEVal, rwDVSVal, rwSSVal, rwDLVal, rwNVal, rwEVal);
                            iReply.AcceptChanges();
                        }
                    }
                }
            }
            return iReply;
        }

        public static System.Data.DataTable getPlacementTableforSelectedCrrCriteria(Int32 OperatorID, Int32 WellPadID, Int32 WellID, Decimal MeasuredDepth, Int32 SelectedAzimuth, String ClientDBString)
        {
            System.Data.DataTable iReply = new System.Data.DataTable();
            DataColumn srvID = iReply.Columns.Add("srvID", typeof(Int32));
            DataColumn tvd = iReply.Columns.Add("tvd", typeof(Decimal));
            DataColumn north = iReply.Columns.Add("north", typeof(Decimal));
            DataColumn east = iReply.Columns.Add("east", typeof(Decimal));
            DataColumn vs = iReply.Columns.Add("vs", typeof(Decimal));
            DataColumn subsea = iReply.Columns.Add("subsea", typeof(Decimal));
            DataColumn dls = iReply.Columns.Add("dls", typeof(Decimal));
            DataColumn northing = iReply.Columns.Add("northing", typeof(Decimal));
            DataColumn easting = iReply.Columns.Add("easting", typeof(Decimal));
            String selData = "SELECT [srvID], [crTVD], [crCummNorthing], [crCummEasting], [crDeltaVS], [crSubsea], [crDogleg], [crNorthing], [crEasting] FROM [RawQCResults] WHERE [optrID] = @optrID AND [wpdID] = @wpdID AND [welID] = @welID AND [Depth] = @Depth AND [acID] = @acID";
            Int32 sID = -99;
            Decimal rwTVDVal = -99.00M, rwCNVal = -99.00M, rwCEVal = -99.00M, rwDVSVal = -99.00M, rwSSVal = -99.00M, rwDLVal = -99.00M, rwNVal = -99.00M, rwEVal = -99.00M;
            using (SqlConnection dataCon = new SqlConnection(ClientDBString))
            {
                dataCon.Open();
                SqlCommand getData = new SqlCommand(selData, dataCon);
                getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                getData.Parameters.AddWithValue("@Depth", SqlDbType.Decimal).Value = MeasuredDepth.ToString();
                getData.Parameters.AddWithValue("@acID", SqlDbType.Int).Value = SelectedAzimuth.ToString();
                using (SqlDataReader readData = getData.ExecuteReader())
                {
                    if (readData.HasRows)
                    {
                        while (readData.Read())
                        {
                            sID = readData.GetInt32(0);
                            rwTVDVal = readData.GetDecimal(1);
                            rwCNVal = readData.GetDecimal(2);
                            rwCEVal = readData.GetDecimal(3);
                            rwDVSVal = readData.GetDecimal(4);
                            rwSSVal = readData.GetDecimal(5);
                            rwDLVal = readData.GetDecimal(6);
                            rwNVal = readData.GetDecimal(7);
                            rwEVal = readData.GetDecimal(8);
                            iReply.Rows.Add(sID, rwTVDVal, rwCNVal, rwCEVal, rwDVSVal, rwSSVal, rwDLVal, rwNVal, rwEVal);
                            iReply.AcceptChanges();
                        }
                    }
                }
            }
            return iReply;
        }

        public static System.Data.DataTable getPlacementTableforSelectedMSSRCriteria(Int32 OperatorID, Int32 WellPadID, Int32 WellID, Decimal MeasuredDepth, Int32 SelectedAzimuth, String ClientDBString)
        {
            System.Data.DataTable iReply = new System.Data.DataTable();
            DataColumn srvID = iReply.Columns.Add("srvID", typeof(Int32));
            DataColumn tvd = iReply.Columns.Add("tvd", typeof(Decimal));
            DataColumn north = iReply.Columns.Add("north", typeof(Decimal));
            DataColumn east = iReply.Columns.Add("east", typeof(Decimal));
            DataColumn vs = iReply.Columns.Add("vs", typeof(Decimal));
            DataColumn subsea = iReply.Columns.Add("subsea", typeof(Decimal));
            DataColumn dls = iReply.Columns.Add("dls", typeof(Decimal));
            DataColumn northing = iReply.Columns.Add("northing", typeof(Decimal));
            DataColumn easting = iReply.Columns.Add("easting", typeof(Decimal));
            String selData = "SELECT [srvID], [crTVD], [crCummNorthing], [crCummEasting], [crDeltaVS], [crSubsea], [crDogleg], [crNorthing], [crEasting] FROM [RawQCResults] WHERE [optrID] = @optrID AND [wpdID] = @wpdID AND [welID] = @welID AND [Depth] = @Depth AND [acID] = @acID";
            Int32 sID = -99;
            Decimal rwTVDVal = -99.00M, rwCNVal = -99.00M, rwCEVal = -99.00M, rwDVSVal = -99.00M, rwSSVal = -99.00M, rwDLVal = -99.00M, rwNVal = -99.00M, rwEVal = -99.00M;
            using (SqlConnection dataCon = new SqlConnection(ClientDBString))
            {
                dataCon.Open();
                SqlCommand getData = new SqlCommand(selData, dataCon);
                getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                getData.Parameters.AddWithValue("@Depth", SqlDbType.Decimal).Value = MeasuredDepth.ToString();
                getData.Parameters.AddWithValue("@acID", SqlDbType.Int).Value = SelectedAzimuth.ToString();
                using (SqlDataReader readData = getData.ExecuteReader())
                {
                    if (readData.HasRows)
                    {
                        while (readData.Read())
                        {
                            sID = readData.GetInt32(0);
                            rwTVDVal = readData.GetDecimal(1);
                            rwCNVal = readData.GetDecimal(2);
                            rwCEVal = readData.GetDecimal(3);
                            rwDVSVal = readData.GetDecimal(4);
                            rwSSVal = readData.GetDecimal(5);
                            rwDLVal = readData.GetDecimal(6);
                            rwNVal = readData.GetDecimal(7);
                            rwEVal = readData.GetDecimal(8);
                            iReply.Rows.Add(sID, rwTVDVal, rwCNVal, rwCEVal, rwDVSVal, rwSSVal, rwDLVal, rwNVal, rwEVal);
                            iReply.AcceptChanges();
                        }
                    }
                }
            }
            return iReply;
        }

        public static Int32 updateAzimuthCriteriaID(Int32 OperatorID, Int32 WellPadID, Int32 WellID, Int32 SurveyID, Int32 AzimuthCriteriaID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String SelectQuery = "UPDATE [RawQCResults] SET [acID] = @acID WHERE [optrID] = @optrID AND [wpdID] = @wpdID AND [welID] = @welID AND [srvyID] = @srvyID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        cmd.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        cmd.Parameters.AddWithValue("@srvyID", SqlDbType.Int).Value = SurveyID.ToString();
                        cmd.Parameters.AddWithValue("@acID", SqlDbType.Int).Value = AzimuthCriteriaID.ToString();
                        iReply = Convert.ToInt32(cmd.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        private static ArrayList getClientWellDepthThresholds(Int32 ClientID)
        {
            ArrayList iReply = new ArrayList();
            String selData = "SELECT [dptLimit], [dptMargin] FROM [DepthProcessingThreshold] WHERE [clntID] = @clntID";
            using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
            {
                try
                {
                    dataCon.Open();
                    SqlCommand getData = new SqlCommand(selData, dataCon);
                    getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                    using (SqlDataReader readData = getData.ExecuteReader())
                    {
                        if (readData.HasRows)
                        {
                            while (readData.Read())
                            {
                                iReply.Add(readData.GetDecimal(0));
                                iReply.Add(readData.GetDecimal(1));
                            }
                        }
                    }
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
                finally
                {
                    dataCon.Close(); dataCon.Dispose();
                }
            }
            return iReply;
        }

        public static System.Data.DataTable getPlacementValuesTable(Decimal Depth, Int32 RunID, Int32 WellSectionID, Int32 WellID, String GetClientDBString)
        {
            System.Data.DataTable iReply = new System.Data.DataTable();
            return iReply;
        }
    }
}