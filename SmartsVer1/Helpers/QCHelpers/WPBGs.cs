﻿using System;
using System.Web;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using MNFit = MathNet.Numerics.Fit;
using MNST = MathNet.Numerics.Statistics;
using MNRG = MathNet.Numerics.LinearRegression;
using QC = SmartsVer1.QCHelpers.sdRawDataQC;
using OPR = SmartsVer1.Helpers.QCHelpers.Opr;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using ACCTNG = SmartsVer1.Helpers.QCHelpers.AccountingQC;
using Units = SmartsVer1.Helpers.DemogHelpers.UnitsConverter;

namespace SmartsVer1.Helpers.QCHelpers
{
    public class WPBGs
    {
        static void Main ()
        {}

        private static Decimal inputDip(Int32 ifrVal, Decimal jobCDip, Decimal jobCIDip)
        {
            try
            {
                if (ifrVal.Equals(1))
                {
                    return jobCDip;
                }
                else
                {
                    return jobCIDip;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal inputBT(Int32 ifrVal, Decimal jobCBTotal, Decimal jobCIBTotal)
        {
            try
            {
                if (ifrVal.Equals(1))
                {
                    return jobCBTotal;
                }
                else
                {
                    return jobCIBTotal;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal inputGT(Int32 ifrVal, Decimal jobCGTotal, Decimal jobCIGTotal)
        {
            try
            {
                if (ifrVal.Equals(1))
                {
                    return jobCGTotal;
                }
                else
                {
                    return jobCIGTotal;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //private static Decimal gridAzm(Int32 ifrVal, Int32 NSDirection, Int32 EWDirection, Decimal dsAzimuth, Decimal jobMDec, Decimal jobIMDec, Decimal gridConverg)
        private static Decimal gridAzm(Int32 ifrVal, Decimal inputAzm, Decimal jobMDec, Decimal jobIMDec, Decimal gridConverg)
        {
            try
            {
                Decimal iReply = -99.000000000M;
                Decimal valA = (inputAzm - jobMDec + gridConverg);
                Decimal valB = (inputAzm - jobIMDec + gridConverg);
                if (ifrVal.Equals(2)) //Yes
                {
                    if (valB > 359.999999999M)
                    {
                        iReply = Decimal.Subtract(valB, 360.000000000M);
                    }
                    else if (valB < 0.000000000M)
                    {
                        iReply = Decimal.Add(valB, 360.0000000000M);
                    }
                    else
                    {
                        iReply = valB;
                    }
                }
                else
                {
                    if (valA > 359.999999999M)
                    {
                        iReply = Decimal.Subtract(valA, 360.000000000M);
                    }
                    else if (valA < 0.000000000M)
                    {
                        iReply = Decimal.Add(valA, 360.0000000000M);
                    }
                    else
                    {
                        iReply = valA;
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal TrueAzm(Int32 ifrVal, Decimal dsAzimuth, Decimal jobMDec, Decimal jobIMDec)
        {
            try
            {
                Decimal iReply = -99.000000000000M;
                Decimal valA = dsAzimuth - jobMDec;
                Decimal valB = dsAzimuth - jobIMDec;
                if (ifrVal.Equals(2)) //Yes
                {
                    if (valB > 359.9999999999M)
                    {
                        iReply = Decimal.Subtract(valB, 360.000000000M);
                    }
                    else if (valB < 0)
                    {
                        iReply = Decimal.Add(valB, 360.0000000000M);
                    }
                    else
                    {
                        iReply = valB;
                    }
                }
                else
                {
                    if (valA > 359.999999999M)
                    {
                        iReply = Decimal.Subtract(valA, 360.000000000M);
                    }
                    else if (valA < 0)
                    {
                        iReply = Decimal.Add(valA, 360.000000000M);
                    }
                    else
                    {
                        iReply = valA;
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal FinalAzm(Int32 JobConvergence, Decimal gridAzm, Decimal trueAzm)
        {
            try
            {
                if (JobConvergence.Equals(2))
                {
                    return gridAzm;
                }
                else
                {
                    return trueAzm;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal calcGx( Decimal convGTotal, Decimal dsInclination, Decimal GTF)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = ( ( -1 * convGTotal ) * (Decimal)Math.Sin( Units.ToRadians( (Double)dsInclination ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)GTF ) ) );

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal calcGy( Decimal convGTotal, Decimal dsInclination, Decimal GTF )
        {
            try
            {
                Decimal iResult = -99.00000000M;
                iResult = convGTotal * (Decimal)Math.Sin( Units.ToRadians( (Double)dsInclination ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)GTF ) );
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal calcGz( Decimal convGTotal, Decimal dsInclination)
        {
            try
            {
                Decimal iResult = -99.00000000M;
                iResult = convGTotal * (Decimal)Math.Cos( Units.ToRadians( (Double)dsInclination ) );
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal calcBx( Decimal convBTotal, Decimal jobCDip, Decimal finalAzm, Decimal dsInclination, Decimal GTF)
        {
            try
            {
                Decimal iResult = -99.00000000M;
                Double rJobCDip = Convert.ToDouble(Units.ToRadians((Double)jobCDip));
                Double rFinalAzm = Convert.ToDouble(Units.ToRadians((Double)finalAzm));
                Double rDSInc = Convert.ToDouble(Units.ToRadians((Double)dsInclination));
                Double rGTF = Convert.ToDouble(Units.ToRadians((Double)GTF));
                Double ValA = Math.Cos(rJobCDip) * Math.Cos(rFinalAzm) * Math.Cos(rDSInc) * Math.Cos(rGTF);
                Double ValB = Math.Sin(rJobCDip) * Math.Sin(rDSInc) * Math.Cos(rGTF);
                Double ValC = Math.Cos(rJobCDip) * Math.Sin(rFinalAzm) * Math.Sin(rGTF);
                iResult = convBTotal * (Decimal)(ValA - ValB - ValC);
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal calcBy(Decimal convBTotal, Decimal jobCDip, Decimal dsInclination, Decimal finalAzm, Decimal GTF)
        {
            try
            {
                Decimal iResult = -99.00000000M;
                Double rJobCDip = Convert.ToDouble(Units.ToRadians((Double)jobCDip));
                Double rFinalAzm = Convert.ToDouble(Units.ToRadians((Double)finalAzm));
                Double rDSInc = Convert.ToDouble(Units.ToRadians((Double)dsInclination));
                Double rGTF = Convert.ToDouble(Units.ToRadians((Double)GTF));
                Double ValA = Math.Cos(rJobCDip) * Math.Cos(rFinalAzm) * Math.Cos(rDSInc) * Math.Sin(rGTF);
                Double ValB = Math.Cos(rJobCDip) * Math.Sin(rFinalAzm) * Math.Cos(rGTF);
                Double ValC = Math.Sin(rJobCDip) * Math.Sin(rDSInc) * Math.Sin(rGTF);
                iResult = (-1 * convBTotal) * (Decimal)(ValA + ValB - ValC);
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal calcBz(Decimal convBTotal, Decimal jobCDip, Decimal dsInclination, Decimal finalAzimuth)
        {
            try
            {
                Decimal iResult = -99.00000000M;
                Double rJobCDip = Convert.ToDouble(Units.ToRadians((Double)jobCDip));
                Double rFinalAzm = Convert.ToDouble(Units.ToRadians((Double)finalAzimuth));
                Double rDSInc = Convert.ToDouble(Units.ToRadians((Double)dsInclination));
                Double ValA = Math.Sin(rJobCDip) * Math.Cos(rDSInc);
                Double ValB = Math.Cos(rJobCDip) * Math.Cos(rFinalAzm) * Math.Sin(rDSInc);
                iResult = convBTotal * (Decimal)(ValA + ValB);
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Calculating WellProfile QC values
        private static Decimal qcColP( Decimal wpGx, Decimal wpGy ) //GTF
        {
            try
            {
                Decimal iResult = -99.00000000M;
                if( Units.ToDegrees( Math.Atan2( ( (Double)wpGy ) , ( -1 * (Double)wpGx ) ) ) < 0 )
                {
                    iResult = Convert.ToDecimal( Units.ToDegrees(Math.Atan2 ( ( (Double)wpGy ), ( -1 * (Double)wpGx ) ) ) + 360 );
                }
                else
                {
                    iResult = Convert.ToDecimal( Units.ToDegrees ( Math.Atan2 (( (Double)wpGy ) , ( -1 * (Double)wpGx ) ) ) );
                }

                return iResult;
            }
            catch(Exception ex)
            {throw new System.Exception(ex.ToString());}
        }

        private static Decimal qcColQ(Decimal wpBx, Decimal wpBy) //MTF
        {
            try
            {
                Decimal iResult = -99.00000000M;
                Double valBx = Convert.ToDouble(wpBx);
                Double valBy = Convert.ToDouble(wpBy);
                Double value = Math.Atan((-1 * valBy) / valBx);

                if(valBx > 0)
                {
                    iResult = Convert.ToDecimal(Units.ToDegrees(value) + 360);
                }
                else if(valBy > 0)
                {
                    iResult = Convert.ToDecimal(Units.ToDegrees(value));
                }
                else
                {
                    iResult = Convert.ToDecimal(Units.ToDegrees(value)+180);
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColS(Int32 ifrVal, Decimal jobMDec, Decimal jobIMDec)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if (ifrVal.Equals(2))
                {
                    iResult = Decimal.Subtract(jobIMDec, jobMDec);
                }
                else
                {
                    iResult = 0.2500M;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColT(Decimal wpGx, Decimal wpGy, Decimal wpGz)        
        {
            try
            {
                Decimal iResult = -99.00000000M;
                Double valGx = Convert.ToDouble(wpGx);
                Double valGy = Convert.ToDouble(wpGy);
                Double valGz = Convert.ToDouble(wpGz);
                Double value = Math.Atan2( Math.Sqrt( Math.Pow(valGx,2) + Math.Pow(valGy,2) ), valGz );

                iResult = Convert.ToDecimal(Units.ToDegrees(value));

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 qcColV(Decimal wpColTVal)
        {
            try
            {
                Int32 iResult = -99;

                if( (wpColTVal < 4.99M) || (wpColTVal.Equals(4.99M) ) )
                {
                    iResult = 1;
                }
                else
                {
                    iResult = 0;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColW(Int32 runID, Int32 srRowID, Decimal wpColTVal, String dbConn) //Inc - Fluctuations
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if(srRowID.Equals(1))
                {
                    iResult = 0.00M;
                }
                else if(srRowID.Equals(2))
                {
                    iResult = Decimal.Divide( wpColTVal, 2 );
                }
                else
                {
                    Int32 prevRowID = srRowID - 1;
                    Decimal prevColT = -99.000000000000M;

                    String getData = "SELECT [colT] FROM [WPQCResults] WHERE [srvyRowID] = @srvyRowID AND [runID] = @runID";

                    using (SqlConnection clntConn = new SqlConnection(dbConn))
                    {
                        try
                        {
                            clntConn.Open();
                            SqlCommand getColTPrev = new SqlCommand(getData, clntConn);
                            getColTPrev.Parameters.AddWithValue("@srvyRowID", SqlDbType.Int).Value = prevRowID.ToString();
                            getColTPrev.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = runID.ToString();

                            using (SqlDataReader readColT = getColTPrev.ExecuteReader())
                            {
                                try
                                {
                                    if (readColT.HasRows)
                                    {
                                        while (readColT.Read())
                                        {
                                            prevColT = readColT.GetDecimal(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readColT.Close();
                                    readColT.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            clntConn.Close();
                            clntConn.Dispose();
                        }
                    }

                    iResult = Decimal.Subtract(wpColTVal, prevColT);
                }

                return iResult;
            }
            catch(Exception ex)
            {throw new System.Exception(ex.ToString());}
        }        

        private static Decimal qcColY( Decimal calcBxVal, Decimal calcByVal, Decimal calcBzVal, Decimal wpColPVal, Decimal wpColTVal) //Calculating Magnetic Azimuth
        {
            try
            {
                Double value = ((Math.Atan2((-1 * (((Double)calcBxVal) * Math.Sin((Double)wpColPVal * 3.1415926 / 180) + ((Double)calcByVal) * Math.Cos((Double)wpColPVal * 3.1415926 / 180))), ((((Double)calcBxVal) * Math.Cos((Double)wpColPVal * 3.1415926 / 180) - ((Double)calcByVal) * Math.Sin((Double)wpColPVal * 3.1415926 / 180)) * Math.Cos((Double)wpColTVal * 3.1415926 / 180) + ((Double)calcBzVal) * Math.Sin((Double)wpColTVal * 3.1415926 / 180))) * 180 / 3.1415926) + 360) % 360;
                

                if( value > 360 )
                {
                    return Convert.ToDecimal( value - 360 );
                }
                else
                {
                    return Convert.ToDecimal( value );
                }

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        
        private static Decimal qcColZ(Decimal jobMDec, Decimal gridConvergence, Decimal wpColYVal) //Calculating Grid-Azimuth - Plain
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if ((wpColYVal + jobMDec - gridConvergence) > 359.99M)
                {
                    iResult = ((wpColYVal + jobMDec - gridConvergence) - 360.00M);
                }
                else if ((wpColYVal + jobMDec - gridConvergence) < 0.00M)
                {
                    iResult = ((wpColYVal + jobMDec - gridConvergence) + 360.00M);
                }
                else
                {
                    iResult = (wpColYVal + jobMDec - gridConvergence);
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColAA(Decimal jobIMDec, Decimal gridConvergence, Decimal wpColYVal) //Calculating Grid-Azimuth - IFR
        {
            try
            {
                Decimal iResult = -99.00000000M;
                Decimal value = (wpColYVal + jobIMDec - gridConvergence);

                if ( value > 359.999999999M )
                {
                    iResult = ( value - 360.000000000M );
                }
                else if ( value < 0.000000000M )
                {
                    iResult = ( value + 360.000000000M );
                }
                else
                {
                    iResult = ( value );
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColAB( Decimal jobMDec, Decimal qcColYVal) //True-Azimuth 
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if ((qcColYVal + jobMDec) > 359.999999999M)
                {
                    iResult = (qcColYVal + jobMDec) - 360.000000000M;
                }
                else if ((qcColYVal + jobMDec) < 0.0000000000M)
                {
                    iResult = (qcColYVal + jobMDec) + 360.0000000000M;
                }
                else
                {
                    iResult = (qcColYVal + jobMDec);
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColAC(Decimal jobIMDec, Decimal qcColYVal) //Calcualte True-Azimuth IFR
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if ((qcColYVal + jobIMDec) > 359.999999999M)
                {
                    iResult = (qcColYVal + jobIMDec) - 360.000000000M;
                }
                else if ((qcColYVal + jobIMDec) < 0.000000000M)
                {
                    iResult = (qcColYVal + jobIMDec) + 360.000000000M;
                }
                else
                {
                    iResult = (qcColYVal + jobIMDec);
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColAD(Int32 ifrVal, Decimal wpColZVal, Decimal wpColAAVal) //Calculate Final-Grid-Azimuth
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if(ifrVal.Equals(1))
                {
                    iResult = wpColZVal;
                }
                else
                {
                    iResult = wpColAAVal;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColAE(Int32 ifrVal, Decimal wpColABVal, Decimal wpColACVal) //Calculate Final-True-Azimuth
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if (ifrVal.Equals(1))
                {
                    iResult = wpColABVal;
                }
                else
                {
                    iResult = wpColACVal;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColAF(Int32 jobConvergence, Decimal wpColADVal, Decimal wpColAEVal) //Final-Selected-Azimuth
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if (jobConvergence.Equals(2)) //1 = True, 2 = Grid
                {
                    iResult = wpColADVal;
                }
                else
                {
                    iResult = wpColAEVal;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColAH( Decimal wpColPVal, Decimal wpColTVal, Decimal wpColBQVal, Decimal wpColBRVal, Decimal wpColCRVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;
                Double rcColPVal = Math.Cos(Units.ToRadians((Double)wpColPVal)); //To Radians, Cos
                Double rsColPVal = Math.Sin(Units.ToRadians((Double)wpColPVal)); //To Radians, Sin
                Double rcColTVal = Math.Cos(Units.ToRadians((Double)wpColTVal)); //To Radians, Cos
                Double rsColTVal = Math.Sin(Units.ToRadians((Double)wpColTVal)); //To Radians, Sin
                Double valA = ((((Double)wpColBQVal) * rcColPVal - ((Double)wpColBRVal) * rsColPVal) * rcColTVal + ((Double)wpColCRVal) * rsColTVal);
                Double valB = (((Double)wpColBQVal) * rsColPVal + ((Double)wpColBRVal) * rcColPVal);
                Double valC = Units.ToDegrees( Math.Atan2((-1 * valB), valA) ) + 360;
                if ((valC % 360) > 360)
                {
                    iResult = Convert.ToDecimal((valC % 360) - 360);
                }
                else
                {
                    iResult = Convert.ToDecimal(valC % 360);
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColAI(Decimal MDec, Decimal gridConvergence, Decimal wpColAHVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if( ( wpColAHVal + MDec - gridConvergence ) > 359.999999999M )
                {
                    iResult = ( ( wpColAHVal + MDec - gridConvergence ) - 360.000000000M );
                }
                else if( ( wpColAHVal + MDec - gridConvergence ) < 0 )
                {
                    iResult = ( ( wpColAHVal + MDec - gridConvergence ) + 360.000000000M );
                }
                else
                {
                    iResult = ( wpColAHVal + MDec - gridConvergence );
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColAJ(Decimal IMDec, Decimal gridConvergence, Decimal wpColAHVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if ((wpColAHVal + IMDec - gridConvergence) > 359.999999999M)
                {
                    iResult = ((wpColAHVal + IMDec - gridConvergence) - 360.0000000000M);
                }
                else if ((wpColAHVal + IMDec - gridConvergence) < 0)
                {
                    iResult = ((wpColAHVal + IMDec - gridConvergence) + 360.000000000M);
                }
                else
                {
                    iResult = (wpColAHVal + IMDec - gridConvergence);
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        private static Decimal qcColAK(Decimal MDec, Decimal wpColAHVal)
        {
            try
            {
                Decimal iResult = -99.0000000M;

                if( ( wpColAHVal + MDec ) > 359.999999999M )
                {
                    iResult = ( ( wpColAHVal + MDec ) - 360.0000000000M );
                }
                else if( ( wpColAHVal + MDec ) < 0.0000000000M )
                {
                    iResult = ( wpColAHVal + MDec ) + 360.000000000M;
                }
                else
                {
                    iResult = (wpColAHVal + MDec);
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColAL(Decimal IMDec, Decimal wpColAHVal)
        {
            try
            {
                Decimal iResult = -99.0000000M;

                if ((wpColAHVal + IMDec) > 359.999999999M)
                {
                    iResult = ((wpColAHVal + IMDec) - 360.0000000000M);
                }
                else if ((wpColAHVal + IMDec) < 0.0000000000M)
                {
                    iResult = (wpColAHVal + IMDec) + 360.0000000000M;
                }
                else
                {
                    iResult = (wpColAHVal + IMDec);
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColAM(Int32 ifrVal, Decimal wpColAIVal, Decimal wpColAJVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if (ifrVal.Equals(1))
                {
                    iResult = wpColAIVal;
                }
                else
                {
                    iResult = wpColAJVal;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColAN(Int32 ifrVal, Decimal wpColAKVal, Decimal wpColALVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if (ifrVal.Equals(1))
                {
                    iResult = wpColAKVal;
                }
                else
                {
                    iResult = wpColALVal;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColAO(Decimal jobConvergence, Decimal wpColAMVal, Decimal wpColANVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if (jobConvergence.Equals(2))
                {
                    iResult = wpColAMVal;
                }
                else
                {
                    iResult = wpColANVal;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColAP(Decimal wpColYVal, Decimal wpColAHVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Decimal.Subtract(wpColAHVal, wpColYVal);

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColAR(Int32 srvyRowID, Int32 RunID, Decimal wpColYVal, String dbConn)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if (srvyRowID.Equals(1))
                {
                    iResult = 0.00M;
                }
                else
                {
                    Int32 prevSrvyID = -99;
                    Decimal prevY = -99.00000000M;

                    prevSrvyID = srvyRowID - 1;

                    String selData = "SELECT [colY] FROM [WPQCResults] WHERE [srvyRowID] = @srvyRowID AND [runID] = @RunID";

                    using (SqlConnection clntConn = new SqlConnection(dbConn))
                    {
                        try
                        {
                            clntConn.Open();

                            SqlCommand getData = new SqlCommand(selData, clntConn);
                            getData.Parameters.AddWithValue("@srvyRowID", SqlDbType.Int).Value = prevSrvyID.ToString();
                            getData.Parameters.AddWithValue("@RunID", SqlDbType.Int).Value = RunID.ToString();

                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            prevY = readData.GetDecimal(0);
                                        }
                                    }

                                    iResult = Math.Abs(wpColYVal - prevY);
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            clntConn.Close();
                            clntConn.Dispose();
                        }
                    }
                }

                return iResult;
            }
            catch(Exception ex)
            {throw new System.Exception(ex.ToString());}
        }

        private static Decimal qcColAS(Decimal wpColWVal, Decimal wpColARVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Convert.ToDecimal( Math.Sqrt( Math.Pow((Double)wpColWVal, 2) + Math.Pow((Double)wpColARVal, 2) ) );

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColAT(Int32 srvyRowID, Int32 RunID, Decimal depth, Decimal wpColVVal, Decimal wpColWVal, Decimal wpColASVal, String dbConn)
        {
            try
            {
                Decimal iResult = -99.00000000M;
                Decimal depthDiff = -99.00M;

                using (SqlConnection clntConn = new SqlConnection(dbConn))
                {
                    try
                    {
                        clntConn.Open();
                        Int32 prevRowID = -99;
                        Decimal prevDepth = -99.00M;
                        prevRowID = srvyRowID - 1;

                        String selData = "SELECT [depth] FROM [WPQCResults] WHERE [srvyRowID] = @srvyRowID AND [runID] = @runID";
                        SqlCommand getData = new SqlCommand(selData, clntConn);
                        getData.Parameters.AddWithValue("@srvyRowID", SqlDbType.Int).Value = srvyRowID.ToString();
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();

                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        prevDepth = readData.GetDecimal(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                        depthDiff = Decimal.Subtract(depth, prevDepth);
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }

                if( wpColVVal.Equals(1))
                {
                    if(srvyRowID.Equals(1))
                    {
                        iResult = wpColWVal;
                    }
                    else if(depthDiff.Equals(0))
                    {
                        iResult = 0.00000000M;
                    }
                    else
                    {
                        iResult = Decimal.Divide( wpColWVal , depthDiff);
                    }
                }
                else if(srvyRowID.Equals(1))
                {
                    iResult = wpColASVal;
                }
                else if(depthDiff.Equals(0))
                {
                    iResult = 0.00000000M;
                }
                else
                {
                    iResult = Decimal.Divide(wpColASVal, depthDiff);
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColAU( Int32 srvyRowID, Int32 lenUnit, Int32 wpColVVal, Decimal wpColATVal, Decimal lcRef1, Decimal lcRef2, Decimal lcRef3, Decimal lcRef4)
        {
            try
            {
                Decimal iResult = -99.0000000M;

                if(srvyRowID.Equals(1))
                {
                    iResult = 0.00M;
                }
                else if( wpColVVal.Equals(1) && lenUnit.Equals(5))
                {
                    iResult = Decimal.Subtract( wpColATVal , lcRef3 );
                }
                else if( wpColVVal.Equals(1) && lenUnit.Equals(4))
                {
                    iResult = Decimal.Subtract( wpColATVal , lcRef4 );
                }
                else if( wpColVVal.Equals(0) && lenUnit.Equals(5))
                {
                    iResult = Decimal.Subtract( wpColATVal, lcRef1 );
                }
                else
                {
                    iResult = Decimal.Subtract( wpColATVal, lcRef2);
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 qcColAV(Decimal wpColAUVal)
        {
            try
            {
                Int32 iResult = -99;

                if (wpColAUVal > 0.01M)
                {
                    iResult = 0;
                }
                else
                {
                    iResult = 1;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 qcColAW(Decimal wpColTVal)
        {
            try
            {
                Int32 iResult = -99;

                if (wpColTVal.Equals(70) || wpColTVal > 70)
                {
                    iResult = 0;
                }
                else
                {
                    iResult = 1;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 qcColAX(Decimal wpColYVal)
        {
            try
            {
                Int32 iResult = -99;

                if( ( wpColYVal >= 0 ) && ( wpColYVal <= 69.99M ) )
                {
                    iResult = 1;
                }
                else if( ( wpColYVal >= 70 ) && ( wpColYVal <= 110 ) ) 
                {
                    iResult = 0;
                }
                else if( ( wpColYVal >= 110.01M ) && ( wpColYVal <= 249.99M ) )
                {
                    iResult = 1;
                }
                else if( ( wpColYVal >=250 ) && ( wpColYVal <= 290 ) )
                {
                    iResult = 0;
                }
                else if( ( wpColYVal >= 290.01M ) && ( wpColYVal <= 359.99M ) )
                {
                    iResult = 1;
                }
                else
                {
                    iResult = 0;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 qcColAY(Int32 wpColAWVal, Int32 wpColAXVal)
        {
            try
            {
                Int32 iResult = -99;

                if( wpColAWVal.Equals(1))
                {
                    iResult = 1;
                }
                else if( wpColAXVal.Equals(1))
                {
                    iResult = 1;
                }
                else if( (wpColAWVal.Equals(1)) && (wpColAXVal.Equals(1)) )
                {
                    iResult = 1;
                }
                else
                {
                    iResult = 0;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColBA(Decimal Bx, Decimal By, Decimal Bz, Decimal Gx, Decimal Gy, Decimal Gz, Int32 wpColCAVal, Decimal wpColCFVal)
        {
            try
            {
                Decimal iResult = -99.0000000M;
                Double valBx = Convert.ToDouble(Bx);
                Double valBy = Convert.ToDouble(By);
                Double valBz = Convert.ToDouble(Bz);
                Double valGx = Convert.ToDouble(Gx);
                Double valGy = Convert.ToDouble(Gy);
                Double valGz = Convert.ToDouble(Gz);
                Double valCA = Convert.ToDouble(wpColCAVal);
                Double valCF = Convert.ToDouble(wpColCFVal);
                Double value = ((valBx * valGx) + (valBy * valGy) + (valBz * valGz)) / (valCA * valCF);

                iResult = Convert.ToDecimal(Units.ToDegrees(Math.Asin(value)));

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColBB( Int32 srvyRowID, Int32 RunID, Decimal wpColBAVal, String dbConn )
        {
            try
            {
                Decimal iResult = -99.00000000M;
                Decimal prevBAVal = -99.00000000M;

                using (SqlConnection clntConn = new SqlConnection(dbConn))
                {
                    try
                    {
                        clntConn.Open();

                        Int32 prevRowID = -99;
                        prevRowID = srvyRowID - 1;

                        String selData = "SELECT [colBA] FROM [WPQCResults] WHERE [srvyRowID] = @srvyRowID AND [runID] = @runID";
                        SqlCommand getData = new SqlCommand(selData, clntConn);
                        getData.Parameters.AddWithValue("@srvyRowID", SqlDbType.Int).Value = prevRowID.ToString();
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();

                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        prevBAVal = readData.GetDecimal(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }                        
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }

                if(srvyRowID.Equals(1))
                {
                    iResult = 0.00000000M;
                }
                else if(srvyRowID.Equals(2))
                {
                    iResult = Decimal.Divide(Decimal.Subtract(wpColBAVal, prevBAVal), 2);
                }
                else
                {
                    iResult = Decimal.Subtract(wpColBAVal, prevBAVal);
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColBC(Int32 ifrVal, Decimal jobCDip, Decimal jobICDip, Decimal wpColBAVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if (ifrVal.Equals(2))
                {
                    iResult = Decimal.Subtract(wpColBAVal, jobICDip);
                }
                else
                {
                    iResult = Decimal.Subtract(wpColBAVal, jobCDip);
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 qcColBD(Int32 srvID, Decimal dflLowerDip, Decimal dflUpperDip, Decimal wpColBCVal, String dbConn)
        {
            try
            {
                Decimal prevCol1 = -99.0000M;
                Decimal prevCol2 = -99.0000M;
                Int32 srvID1 = -99;
                Int32 srvID2 = -99;
                if (!srvID.Equals(1) && !srvID.Equals(2))
                {
                    srvID1 = srvID - 1;
                    srvID2 = srvID - 2;
                    using (SqlConnection metaCon = new SqlConnection(dbConn))
                    {
                        try
                        {
                            String selMetaBC1 = "SELECT [colBC] FROM [WPQCResults] WHERE [srvyRowID] = @srvyID";
                            SqlCommand getMetaBC1 = new SqlCommand(selMetaBC1, metaCon);
                            getMetaBC1.Parameters.AddWithValue("@srvyID", SqlDbType.Int).Value = srvID1.ToString();
                            String selMetaBC2 = "SELECT [colBC] FROM [WPQCResults] WHERE [srvyRowID] = @srvyID";
                            SqlCommand getMetaBC2 = new SqlCommand(selMetaBC2, metaCon);
                            getMetaBC2.Parameters.AddWithValue("@srvyID", SqlDbType.Int).Value = srvID2.ToString();
                            metaCon.Open();
                            using (SqlDataReader readBC1 = getMetaBC1.ExecuteReader())
                            {
                                try
                                {
                                    if (readBC1.HasRows)
                                    {
                                        while (readBC1.Read())
                                        {
                                            prevCol1 = readBC1.GetDecimal(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readBC1.Close();
                                    readBC1.Dispose();
                                }
                            }
                            using (SqlDataReader readBC2 = getMetaBC2.ExecuteReader())
                            {
                                try
                                {
                                    if (readBC2.HasRows)
                                    {
                                        while (readBC2.Read())
                                        {
                                            prevCol2 = readBC2.GetDecimal(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readBC2.Close();
                                    readBC2.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            metaCon.Close();
                            metaCon.Dispose();
                        }
                    }
                }

                if (srvID.Equals(1))
                {
                    if (wpColBCVal < (-1 * dflLowerDip))
                    { return 3; }
                    else if ((wpColBCVal) > dflLowerDip)
                    { return 2; }
                    else
                    { return 1; }
                }
                else if (srvID.Equals(2))
                {
                    if (wpColBCVal < (-1 * dflLowerDip))
                    { return 3; }
                    else if (wpColBCVal > (dflLowerDip))
                    { return 2; }
                    else
                    { return 1; }
                }
                else if (((wpColBCVal - prevCol1) > dflUpperDip) && ((wpColBCVal - prevCol1) < (-1 * dflLowerDip)) || (((prevCol1 - prevCol2) > dflLowerDip) && ((prevCol1 - prevCol2) < (-1 * dflLowerDip))))
                { return 4; }
                else
                {
                    if (wpColBCVal < (-1 * dflLowerDip))
                    { return 3; }
                    else if (wpColBCVal > dflLowerDip)
                    { return 2; }
                    else
                    { return 1; }
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        private static Decimal qcColBF(Int32 ifrVal, Decimal convGTotal, Decimal convIGTotal, Decimal wpColPVal, Decimal wpColTVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if(ifrVal.Equals(1))
                {
                    iResult = Convert.ToDecimal(-1 * convGTotal * (Decimal)Math.Sin( Units.ToRadians((Double)wpColTVal) ) * (Decimal)Math.Cos( Units.ToRadians((Double)wpColPVal) ) );
                }
                else
                {
                    iResult = Convert.ToDecimal(-1 * convIGTotal * (Decimal)Math.Sin( Units.ToRadians((Double)wpColTVal ) ) * (Decimal)Math.Cos( Units.ToRadians((Double)wpColPVal ) ) );
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColBG(Int32 ifrVal, Decimal wpColPVal, Decimal wpColTVal, Decimal convGTotal, Decimal convIGTotal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if(ifrVal.Equals(1))
                {
                    iResult = Convert.ToDecimal( convGTotal * (Decimal)Math.Sin( Units.ToRadians( (Double)wpColTVal ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)wpColPVal ) ) );
                }
                else
                {
                    iResult = Convert.ToDecimal( convIGTotal * (Decimal)Math.Sin( Units.ToRadians( (Double)wpColTVal ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)wpColPVal ) ) );
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColBH(Int32 ifrVal, Decimal convGTotal, Decimal convIGTotal, Decimal wpColTVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if(ifrVal.Equals(1))
                {
                    iResult = Convert.ToDecimal( convGTotal * (Decimal)Math.Cos( Units.ToRadians( (Double)wpColTVal ) ) );
                }
                else
                {
                    iResult = Convert.ToDecimal( convIGTotal * (Decimal)Math.Cos( Units.ToRadians( (Double)wpColTVal ) ) );
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColBI( Int32 ifrVal, Decimal convDip, Decimal convIDip, Decimal convBTotal, Decimal convIBTotal, Decimal wpColPVal, Decimal wpColTVal, Decimal wpColYVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if (ifrVal.Equals(1))
                {
                    iResult = Convert.ToDecimal( convBTotal * ( ( (Decimal)Math.Cos( Units.ToRadians( (Double)convDip ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)wpColYVal ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)wpColTVal ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)wpColPVal ) ) ) - ( (Decimal)Math.Sin( Units.ToRadians( (Double)convDip ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)wpColTVal ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)wpColPVal ) ) ) - ( (Decimal)Math.Cos( Units.ToRadians( (Double)convDip ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)wpColYVal ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)wpColPVal ) ) ) ) );
                }
                else
                {
                    iResult = Convert.ToDecimal( convIBTotal * ( ( (Decimal)Math.Cos( Units.ToRadians( (Double)convIDip ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)wpColYVal ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)wpColTVal ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)wpColPVal ) ) ) - ( (Decimal)Math.Sin( Units.ToRadians( (Double)convIDip ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)wpColTVal ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)wpColPVal ) ) ) - ( (Decimal)Math.Cos( Units.ToRadians( (Double)convIDip ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)wpColYVal ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)wpColPVal ) ) ) ) );
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColBJ( Int32 ifrVal, Decimal convBTotal, Decimal convIBTotal, Decimal convDip, Decimal convIDip, Decimal wpColPVal, Decimal wpColTVal, Decimal wpColYVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if(ifrVal.Equals(1))
                {
                    iResult = Convert.ToDecimal( ( -1 * convBTotal ) * ( ( (Decimal)Math.Cos( Units.ToRadians( (Double)convDip ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)wpColYVal ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)wpColTVal ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)wpColPVal ) ) ) + ( (Decimal)Math.Cos( Units.ToRadians( (Double)convDip ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)wpColYVal ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)wpColPVal ) ) ) - ( (Decimal)Math.Sin( Units.ToRadians( (Double)convDip ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)wpColTVal ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)wpColPVal ) ) ) ) );
                }
                else
                {
                    iResult = Convert.ToDecimal( ( -1 * convBTotal ) * ( ( (Decimal)Math.Cos( Units.ToRadians( (Double)convIDip ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)wpColYVal ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)wpColTVal ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)wpColPVal ) ) ) + ( (Decimal)Math.Cos( Units.ToRadians( (Double)convIDip ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)wpColYVal ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)wpColPVal ) ) ) - ( (Decimal)Math.Sin( Units.ToRadians( (Double)convIDip ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)wpColTVal ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)wpColPVal ) ) ) ) );
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColBK( Int32 ifrVal, Decimal convDip, Decimal convIDip, Decimal convBTotal, Decimal convIBTotal, Decimal wpColTVal, Decimal wpColYVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if(ifrVal.Equals(1))
                {
                    iResult = Convert.ToDecimal( convBTotal * ( ( (Decimal)Math.Sin( Units.ToRadians( (Double)convDip ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)wpColTVal ) ) + ( (Decimal)Math.Cos( Units.ToRadians( (Double)convDip ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)wpColYVal ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)wpColTVal ) ) ) ) ) );
                }
                else
                {
                    iResult = Convert.ToDecimal( convIBTotal * ( ( (Decimal)Math.Sin( Units.ToRadians( (Double)convIBTotal ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)wpColTVal ) ) + ( (Decimal)Math.Cos( Units.ToRadians( (Double)convIDip ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)wpColYVal ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)wpColTVal ) ) ) ) ) );
                }

                return iResult;
            }
            catch(Exception ex)
            {throw new System.Exception(ex.ToString());}
        }

        private static Decimal qcColBM( Int32 srvyRowID, Int32 RunID, Decimal calcBxVal, Decimal wpColBIVal, String dbConn )
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if (srvyRowID.Equals(1) || srvyRowID.Equals(2))
                {
                    iResult = Decimal.Subtract(wpColBIVal, calcBxVal);
                }
                else
                {
                    List<Double> colBIList = new List<Double>();
                    List<Double> BxList = new List<Double>();
                    Double d1 = 0.00000000;
                    Double d2 = 0.00000000;
                    Decimal v1 = 0.00000000M;
                    Decimal v2 = 0.00000000M;
                    Double intercept = 0.00000000;

                    String selectBICommand = "SELECT [wpBx], [colBI] FROM [WPQCResults] WHERE [runID] = @runID";                    

                    using(SqlConnection clntConn = new SqlConnection(dbConn))
                    {
                        try
                        {
                            clntConn.Open();

                            SqlCommand getBIVals = new SqlCommand(selectBICommand, clntConn);
                            getBIVals.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();                            

                            using (SqlDataReader readBI = getBIVals.ExecuteReader())
                            {
                                try
                                {
                                    if (readBI.HasRows)
                                    {
                                        while (readBI.Read())
                                        {
                                            v1 = readBI.GetDecimal(1);
                                            d1 = Convert.ToDouble(v1);
                                            colBIList.Add(d1);
                                            v2 = readBI.GetDecimal(0);
                                            d2 = Convert.ToDouble(v2);
                                            BxList.Add(d2);
                                            
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readBI.Close();
                                    readBI.Dispose();
                                }
                            }                            

                            colBIList.Add((Double)wpColBIVal); //Adding current row's BI Value to calculation
                            BxList.Add((Double)calcBxVal);

                            Double[] x = new Double[] { };
                            Double[] bi = new Double[] { };

                            x = BxList.ToArray();
                            bi = colBIList.ToArray();

                            if (MNST.Statistics.Variance(x).Equals(0))
                            {
                                intercept = Convert.ToDouble(Decimal.Subtract(wpColBIVal, calcBxVal));
                            }
                            else
                            {
                                Tuple<Double, Double> p = MNFit.Line(x, bi);
                                intercept = p.Item1;
                            }
                            return (Decimal)intercept;                    
                        }
                        catch(Exception ex)
                        {throw new System.Exception(ex.ToString());}
                        finally
                        {
                            clntConn.Close();
                            clntConn.Dispose();
                        }
                    }
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColBN( Int32 srvyRowID, Int32 RunID, Decimal calcByVal, Decimal wpColBJVal, String dbConn )
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if (srvyRowID.Equals(1) || srvyRowID.Equals(2))
                {
                    iResult = Decimal.Subtract(wpColBJVal, calcByVal);
                }
                else
                {
                    List<Double> colBJList = new List<Double>();
                    List<Double> ByList = new List<Double>();
                    Double d1 = 0.00000000;
                    Double d2 = 0.00000000;
                    Decimal v1 = 0.00000000M;
                    Decimal v2 = 0.00000000M;
                    Double intercept = 0.00000000;

                    String selectBJCommand = "SELECT [wpBy], [colBJ] FROM [WPQCResults] WHERE [runID] = @runID";
                    
                    using (SqlConnection clntConn = new SqlConnection(dbConn))
                    {
                        try
                        {
                            clntConn.Open();

                            SqlCommand getBJVals = new SqlCommand(selectBJCommand, clntConn);
                            getBJVals.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                           
                            using (SqlDataReader readBJ = getBJVals.ExecuteReader())
                            {
                                try
                                {
                                    if (readBJ.HasRows)
                                    {
                                        while (readBJ.Read())
                                        {
                                            v2 = readBJ.GetDecimal(0);
                                            d2 = Convert.ToDouble(v2);
                                            ByList.Add(d2);
                                            v1 = readBJ.GetDecimal(1);
                                            d1 = Convert.ToDouble(v1);
                                            colBJList.Add(d1);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readBJ.Close();
                                    readBJ.Dispose();
                                }
                            }

                            ByList.Add((Double)calcByVal);
                            colBJList.Add((Double)wpColBJVal); //Adding current row's BI Value to calculation

                            Double[] y = new Double[] { };
                            Double[] bj = new Double[] { };

                            y = ByList.ToArray();
                            bj = colBJList.ToArray();

                            if (MNST.Statistics.Variance(y).Equals(0))
                            {
                                intercept = Convert.ToDouble(Decimal.Subtract(wpColBJVal, calcByVal));
                                iResult = Convert.ToDecimal(intercept);
                            }
                            else
                            {
                                Tuple<Double, Double> p = MNFit.Line(y, bj);
                                intercept = p.Item1;
                                iResult = Convert.ToDecimal(intercept);
                            }                            
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            clntConn.Close();
                            clntConn.Dispose();
                        }
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColBO( Int32 srvyRowID, Int32 RunID, Decimal calcBxVal , Decimal wpColBIVal, String dbConn )
        {
            try
            {
                Decimal iResult = -99.00M;

                if (srvyRowID.Equals(1))
                {
                    iResult = 1.00M;
                }
                else
                {
                    List<Double> colBIList = new List<Double>();
                    List<Double> BxList = new List<Double>();
                    Double d1 = 0.00;
                    Double d2 = 0.00;
                    Decimal v1 = 0.00M;
                    Decimal v2 = 0.00M;
                    
                    String selectBICommand = "SELECT [wpBx], [colBI] FROM [WPQCResults] WHERE [runID] = @runID";
                    
                    using (SqlConnection clntConn = new SqlConnection(dbConn))
                    {
                        try
                        {
                            clntConn.Open();

                            SqlCommand getBIVals = new SqlCommand(selectBICommand, clntConn);
                            getBIVals.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                            
                            using (SqlDataReader readBI = getBIVals.ExecuteReader())
                            {
                                try
                                {
                                    if (readBI.HasRows)
                                    {
                                        while (readBI.Read())
                                        {
                                            v2 = readBI.GetDecimal(0);
                                            d2 = Convert.ToDouble(v2);
                                            BxList.Add(d2);
                                            v1 = readBI.GetDecimal(1);
                                            d1 = Convert.ToDouble(v1);
                                            colBIList.Add(d1);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readBI.Close();
                                    readBI.Dispose();
                                }
                            }

                            BxList.Add((Double)calcBxVal);
                            colBIList.Add((Double)wpColBIVal); //Adding current row's BI Value to calculation

                            Double[] x = new Double[] { };
                            Double[] bi = new Double[] { };

                            x = BxList.ToArray();
                            bi = colBIList.ToArray();
                            //if (MNST.Statistics.Variance(x).Equals(0))
                            if(MNST.Statistics.PopulationVariance(x).Equals(0))                            
                            {
                                iResult = Convert.ToDecimal(1.00);
                            }
                            else
                            {
                                Tuple<Double, Double> p = MNFit.Line(x, bi);
                                iResult = Convert.ToDecimal(p.Item2);
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            clntConn.Close();
                            clntConn.Dispose();
                        }
                    }
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColBP(Int32 srvyRowID, Int32 RunID, Decimal calcByVal, Decimal wpColBJVal, String dbConn)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if (srvyRowID.Equals(1))
                {
                    iResult = 1.00000000M;
                }
                else
                {
                    List<Double> colBJList = new List<Double>();
                    List<Double> ByList = new List<Double>();
                    Double d1 = 0.00000000;
                    Double d2 = 0.00000000;
                    Decimal v1 = 0.00000000M;
                    Decimal v2 = 0.00000000M;
                    //Double slope = 0.00000000;

                    String selectBJCommand = "SELECT [wpBy], [colBJ] FROM [WPQCResults] WHERE [runID] = @runID";
                    
                    using (SqlConnection clntConn = new SqlConnection(dbConn))
                    {
                        try
                        {
                            clntConn.Open();

                            SqlCommand getBJVals = new SqlCommand(selectBJCommand, clntConn);
                            getBJVals.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                            
                            using (SqlDataReader readBJ = getBJVals.ExecuteReader())
                            {
                                try
                                {
                                    if (readBJ.HasRows)
                                    {
                                        while (readBJ.Read())
                                        {
                                            v2 = readBJ.GetDecimal(0);
                                            d2 = Convert.ToDouble(v2);
                                            ByList.Add(d2);
                                            v1 = readBJ.GetDecimal(1);
                                            d1 = Convert.ToDouble(v1);
                                            colBJList.Add(d1);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readBJ.Close();
                                    readBJ.Dispose();
                                }
                            }

                            ByList.Add((Double)calcByVal);
                            colBJList.Add((Double)wpColBJVal); //Adding current row's BI Value to calculation

                            Double[] y = new Double[] { };
                            Double[] bj = new Double[] { };

                            y = ByList.ToArray();
                            bj = colBJList.ToArray();

                            if (MNST.Statistics.Variance(y).Equals(0))
                            {
                                iResult = Convert.ToDecimal( 1.00 );
                            }
                            else
                            {
                                Tuple<Double, Double> p = MNFit.Line(y, bj);
                                iResult = Convert.ToDecimal(p.Item2);
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            clntConn.Close();
                            clntConn.Dispose();
                        }
                    }
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColBQ( Decimal calcBxVal, Decimal wpColBMVal, Decimal wpColBOVal )
        {
            try
            {
                Decimal iResult = -99.00000000M;
                iResult = Decimal.Add(calcBxVal, Decimal.Add(wpColBOVal, wpColBMVal));
                return iResult;
            }
            catch(Exception ex)
            {throw new System.Exception(ex.ToString());}
        }
        
        private static Decimal qcColBR( Decimal calcByVal, Decimal wpColBNVal, Decimal wpColBPVal)
        {
            try
            {
                Decimal iResult = -99.00M;
                iResult = Decimal.Add(calcByVal, Decimal.Add(wpColBNVal, wpColBPVal));
                return iResult;
            }
            catch(Exception ex)
            {throw new System.Exception(ex.ToString());}
        }

        private static Decimal qcColBS( Decimal Bx, Decimal wpColBQVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Decimal.Subtract(wpColBQVal, Bx);

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColBT(Decimal By, Decimal wpColBRVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Decimal.Subtract(wpColBRVal, By);

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColBU(Decimal wpColBQVal, Decimal wpColBRVal, Decimal wpColCUVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Convert.ToDecimal(Math.Sqrt(Math.Abs(Math.Pow((Double)wpColBQVal, 2) + Math.Pow((Double)wpColBRVal, 2) + Math.Pow((Double)wpColCUVal, 2))));

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColBV(Decimal Gx, Decimal Gy, Decimal Gz, Decimal wpColBQVal, Decimal wpColBRVal, Decimal wpColBUVal, Decimal wpColCFVal, Decimal wpColCUVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Convert.ToDecimal(Units.ToDegrees(Math.Asin(((Double)((wpColBQVal) * (Gx) + (wpColBRVal) * (Gy) + (wpColCUVal) * (Gz)) / (Double)(wpColCFVal * wpColBUVal)))));

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColBW(Int32 ifrVal, Decimal convBTotal, Decimal convIBTotal, Decimal wpColBUVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if(ifrVal.Equals(1))
                {
                    iResult = -1 * ( convBTotal - wpColBUVal );
                }
                else
                {
                    iResult = -1 * ( convIBTotal - wpColBUVal );
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColBX(Int32 ifrVal, Decimal convDip, Decimal convIDip, Decimal wpColBVVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if(ifrVal.Equals(1))
                {
                    iResult = -1 * ( convDip - wpColBVVal );
                }
                else
                {
                    iResult = -1 * ( convIDip - wpColBVVal );
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }      
        }

        private static Decimal qcColBZ(Decimal Bx, Decimal By)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Convert.ToDecimal( Math.Sqrt( Math.Abs( Math.Pow( (Double)Bx, 2) + Math.Pow( (Double)By, 2) ) ) );

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); } 
        }

        private static Int32 qcColCA(Decimal Bx, Decimal By, Decimal Bz)
        {
            try
            {
                Int32 iResult = -99;

                iResult = Convert.ToInt32(Math.Sqrt(Math.Abs(Math.Pow((Double)Bx, 2) + Math.Pow((Double)By, 2) + Math.Pow((Double)Bz, 2))));

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColCB(Int32 srvyRowID, Int32 RunID, Int32 wpColCAVal, String dbConn)
        {
            try
            {
                Decimal iResult = -99.00000000M;
                Decimal prevCA = -99;

                if (!srvyRowID.Equals(1))
                {
                    using (SqlConnection clntConn = new SqlConnection(dbConn))
                    {
                        try
                        {
                            clntConn.Open();

                            Int32 prevRowID = srvyRowID - 1;

                            String selData = "SELECT [colCA] FROM [WPQCResults] WHERE [srvyRowID] = @srvyRowID AND [runID] = @runID";

                            SqlCommand getData = new SqlCommand(selData, clntConn);
                            getData.Parameters.AddWithValue("@srvyRowID", SqlDbType.Int).Value = prevRowID.ToString();
                            getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();

                            using(SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            prevCA = readData.GetDecimal(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            clntConn.Close();
                            clntConn.Dispose();
                        }
                    }
                }

                if(srvyRowID.Equals(1))
                {
                    iResult = 0.00000000M;
                }
                else if(srvyRowID.Equals(2))
                {
                    iResult = Decimal.Divide( ( wpColCAVal - prevCA ) , 2 );
                }
                else
                {
                    iResult = ( wpColCAVal - prevCA );
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 qcColCC(Int32 srvID, Decimal dflLBT, Decimal dflUBT, Decimal wpColCDVal, String dbConn)
        {
            try
            {
                Decimal prevCol1 = -99.000000000M, prevCol2 = -99.00000000000M;
                Int32 iReply = -99, srvID1 = 0, srvID2 = 0;
                if (!srvID.Equals(1) && !srvID.Equals(2))
                {
                    srvID1 = srvID - 1;
                    srvID2 = srvID - 2;
                    using (SqlConnection metaCon = new SqlConnection(dbConn))
                    {
                        try
                        {
                            String selMetaCD1 = "SELECT [colCD] FROM [WPQCResults] WHERE [srvyRowID] = @srvyID";
                            SqlCommand getMetaCD1 = new SqlCommand(selMetaCD1, metaCon);
                            getMetaCD1.Parameters.AddWithValue("@srvyID", SqlDbType.Int).Value = srvID1.ToString();
                            String selMetaCD2 = "SELECT [colCD] FROM [WPQCResults] WHERE [srvyRowID] = @srvyID";
                            SqlCommand getMetaCD2 = new SqlCommand(selMetaCD2, metaCon);
                            getMetaCD2.Parameters.AddWithValue("@srvyID", SqlDbType.Int).Value = srvID2.ToString();
                            metaCon.Open();
                            using (SqlDataReader readCD1 = getMetaCD1.ExecuteReader())
                            {
                                try
                                {
                                    if (readCD1.HasRows)
                                    {
                                        while (readCD1.Read())
                                        {
                                            prevCol1 = readCD1.GetDecimal(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readCD1.Close();
                                    readCD1.Dispose();
                                }
                            }
                            using (SqlDataReader readCD2 = getMetaCD2.ExecuteReader())
                            {
                                try
                                {
                                    if (readCD2.HasRows)
                                    {
                                        while (readCD2.Read())
                                        {
                                            prevCol2 = readCD2.GetDecimal(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readCD2.Close();
                                    readCD2.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            metaCon.Close();
                            metaCon.Dispose();
                        }
                    }
                }

                if(srvID.Equals(1))
                {
                    if((wpColCDVal)<(-1*dflLBT))
                    {
                        iReply = 3;
                    }
                    if((wpColCDVal)>dflLBT)
                    {
                        iReply = 2;
                    }
                    else
                    {
                        iReply = 1;
	                }
                }
                else if ((wpColCDVal - prevCol1) > dflUBT || (wpColCDVal - prevCol1) < (-1*dflUBT))
                {
                    iReply = 4;
                }
                else if((wpColCDVal)<(-1*dflLBT))
                {
                    iReply = 3;
                }
                else if((wpColCDVal)>dflLBT)
                {
                    iReply = 2;
                }
                else
                {
                    iReply = 1;
                }
                return iReply;
            }
            catch (Exception ex)
            {   throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColCD(Int32 ifrVal, Decimal convBTotal, Decimal convIBTotal, Int32 wpColCAVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if(ifrVal.Equals(2))
                {
                    iResult = ( wpColCAVal - convIBTotal );
                }
                else
                {
                    iResult = (wpColCAVal - convBTotal);
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColCE(Decimal Gx, Decimal Gy)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Convert.ToDecimal( Math.Sqrt( Math.Abs( Math.Pow( (Double)Gx , 2 ) + Math.Pow( (Double)Gy ,2 ) ) ) );

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColCF(Decimal Gx, Decimal Gy, Decimal Gz)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Convert.ToDecimal(Math.Sqrt(Math.Abs(Math.Pow((Double)Gx,2) + Math.Pow((Double)Gy,2) + Math.Pow((Double)Gz,2))));

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }            
        }

        private static Decimal qcColCG(Int32 ifrVal, Decimal convGTotal, Decimal convIGTotal, Decimal wpColCFVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if (ifrVal.Equals(2))
                {
                    iResult = Decimal.Subtract( wpColCFVal , convIGTotal );
                }
                else
                {
                    iResult = Decimal.Subtract(wpColCFVal, convGTotal); ;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColCH(Int32 srvyRowID, Int32 RunID, Decimal wpColCFVal, String dbConn)
        {
            try
            {
                Decimal iResult = -99.00000000M;
                Decimal prevCF = -99.00000000M;

                if (!srvyRowID.Equals(1))
                {
                    Int32 prevRowID = srvyRowID - 1;

                    String selData = "SELECT [colCF] FROM [WPQCResults] WHERE [srvyRowID] = @srvyRowID AND [runID] = @runID";

                    using (SqlConnection clntConn = new SqlConnection(dbConn))
                    {
                        try
                        {
                            clntConn.Open();

                            SqlCommand getData = new SqlCommand(selData, clntConn);
                            getData.Parameters.AddWithValue("@srvyRowID", SqlDbType.Int).Value = prevRowID.ToString();
                            getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();

                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            prevCF = readData.GetDecimal(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            clntConn.Close();
                            clntConn.Dispose();
                        }
                    }
                }

                if (srvyRowID.Equals(1))
                {
                    iResult = 0.00000000M;
                }
                else if (srvyRowID.Equals(2))
                {
                    iResult = Decimal.Divide( Decimal.Subtract( wpColCFVal , prevCF ) , 2 );
                }
                else
                {
                    iResult = Decimal.Subtract(wpColCFVal, prevCF);
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 qcColCI(Int32 srvID, Decimal dflLowerGT, Decimal dflUpperGT, Decimal wpColCGVal, String dbConn)
        {
            try
            {
                Decimal prevCol1 = -99.0000M;
                Decimal prevCol2 = -99.0000M;
                Int32 srvID1 = -99;
                Int32 srvID2 = -99;
                if (!srvID.Equals(1) && !srvID.Equals(2))
                {
                    srvID1 = srvID - 1;
                    srvID2 = srvID - 2;
                    using (SqlConnection metaCon = new SqlConnection(dbConn))
                    {
                        try
                        {
                            String selMetaCG1 = "SELECT [colCG] FROM [WPQCResults] WHERE [srvyRowID] = @srvyID";
                            SqlCommand getMetaCG1 = new SqlCommand(selMetaCG1, metaCon);
                            getMetaCG1.Parameters.AddWithValue("@srvyID", SqlDbType.Int).Value = srvID1.ToString();
                            String selMetaCG2 = "SELECT [colCG] FROM [WPQCResults] WHERE [srvyRowID] = @srvyID";
                            SqlCommand getMetaCG2 = new SqlCommand(selMetaCG2, metaCon);
                            getMetaCG2.Parameters.AddWithValue("@srvyID", SqlDbType.Int).Value = srvID2.ToString();
                            metaCon.Open();
                            using (SqlDataReader readCG1 = getMetaCG1.ExecuteReader())
                            {
                                try
                                {
                                    if (readCG1.HasRows)
                                    {
                                        while (readCG1.Read())
                                        {
                                            prevCol1 = readCG1.GetDecimal(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readCG1.Close();
                                    readCG1.Dispose();
                                }
                            }
                            using (SqlDataReader readCG2 = getMetaCG2.ExecuteReader())
                            {
                                try
                                {
                                    if (readCG2.HasRows)
                                    {
                                        while (readCG2.Read())
                                        {
                                            prevCol2 = readCG2.GetDecimal(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readCG2.Close();
                                    readCG2.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            metaCon.Close();
                            metaCon.Dispose();
                        }
                    }
                }

                if (srvID.Equals(1))
                {
                    if (wpColCGVal < (-1 * dflLowerGT))
                    { return 3; }
                    else if ((wpColCGVal) > dflLowerGT)
                    { return 2; }
                    else
                    { return 1; }
                }
                else if (srvID.Equals(2))
                {
                    if (wpColCGVal < (-1 * dflLowerGT))
                    { return 3; }
                    else if (wpColCGVal > (dflLowerGT))
                    { return 2; }
                    else
                    { return 1; }
                }
                else if (((wpColCGVal - prevCol1) > dflUpperGT) && ((wpColCGVal - prevCol1) < (-1 * dflLowerGT)) || (((prevCol1 - prevCol2) > dflLowerGT) && ((prevCol1 - prevCol2) < (-1 * dflLowerGT))))
                { return 4; }
                else
                {
                    if (wpColCGVal < (-1 * dflLowerGT))
                    { return 3; }
                    else if (wpColCGVal > dflLowerGT)
                    { return 2; }
                    else
                    { return 1; }
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        private static Decimal qcColCK( Int32 ifrVal, Decimal convDip, Decimal convIDip, Decimal convBTotal, Decimal convIBTotal, Decimal calcBzVal, Decimal wpColTVal, Decimal wpColYVal, Decimal wpColBKVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if(ifrVal.Equals(1))
                {
                    iResult = Convert.ToDecimal( Units.ToRadians( (Double)( ( ( calcBzVal - wpColBKVal ) / convBTotal ) * ( ( (Decimal)Math.Cos( Units.ToRadians( (Double)wpColTVal ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)convDip ) ) ) - ( (Decimal)Math.Sin( Units.ToRadians( (Double)wpColTVal ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)wpColYVal ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)convDip ) ) ) ) ) ) );
                }
                else
                {
                    iResult = Convert.ToDecimal( Units.ToRadians( (Double)( ( (calcBzVal - wpColBKVal ) / convIBTotal ) * ( ( (Decimal)Math.Cos( Units.ToRadians( (Double)wpColTVal ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)convIDip ) ) ) - ( (Decimal)Math.Sin( Units.ToRadians( (Double)wpColTVal ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)wpColYVal ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)convIDip ) ) ) ) ) ) );
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

         private static Decimal qcColCL(Int32 ifrVal, Decimal convDip, Decimal convIDip, Decimal wpColTVal, Decimal wpColYVal, Decimal wpColCKVal)
         {
             try
            {
                Decimal iResult = -99.00000000M;

                if(ifrVal.Equals(1))
                {
                    iResult = Convert.ToDecimal( -1 * ( ( wpColCKVal ) * ( (Decimal)Math.Sin( Units.ToRadians( (Double)wpColTVal ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)wpColYVal ) ) ) ) / ( ( (Decimal)Math.Cos( Units.ToRadians( (Double)convDip ) ) ) * ( ( (Decimal)Math.Sin( Units.ToRadians( (Double)wpColTVal ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)wpColYVal ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)convDip ) ) ) - ( (Decimal)Math.Cos( Units.ToRadians( (Double)wpColTVal ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)convDip ) ) ) ) ) );
                }
                else
                {
                   iResult = Convert.ToDecimal( -1 * ( ( wpColCKVal ) * ( (Decimal)Math.Sin( Units.ToRadians( (Double)wpColTVal ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)wpColYVal ) ) ) ) / ( ( (Decimal)Math.Cos( Units.ToRadians( (Double)convIDip ) ) ) * ( ( (Decimal)Math.Sin( Units.ToRadians( (Double)wpColTVal ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)wpColYVal ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)convIDip ) ) ) - ( (Decimal)Math.Cos( Units.ToRadians( (Double)wpColTVal ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)convIDip ) ) ) ) ) );
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
         }

        private static Decimal qcColCM( Decimal wpColYVal, Decimal wpColCLVal )
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if( wpColCLVal + wpColYVal > 359.99M)
                {
                    iResult = Decimal.Subtract( Decimal.Add( wpColCLVal , wpColYVal ) , 360.00M );
                }
                else
                {
                    iResult = Decimal.Add( wpColCLVal , wpColYVal );
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColCN( Int32 ifrVal, Decimal convDip, Decimal convIDip, Decimal convBTotal, Decimal convIBTotal, Decimal wpColTVal, Decimal wpColCMVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if(ifrVal.Equals(1))
                {
                    iResult = (convBTotal * (Decimal)Math.Cos(Units.ToRadians((Double)convDip)) * (Decimal)Math.Sin(Units.ToRadians((Double)wpColTVal)) * (Decimal)Math.Cos(Units.ToRadians((Double)wpColCMVal ))) + ( convBTotal * (Decimal)Math.Sin(Units.ToRadians((Double)convDip))) * ((Decimal)Math.Cos(Units.ToRadians((Double)wpColTVal)));
                }
                else
                {
                    iResult = (convIBTotal * (Decimal)Math.Cos(Units.ToRadians((Double)convIDip)) * (Decimal)Math.Sin(Units.ToRadians((Double)wpColTVal)) * (Decimal)Math.Cos(Units.ToRadians((Double)wpColCMVal))) + (convIBTotal * (Decimal)Math.Sin(Units.ToRadians((Double)convIDip)) * (Decimal)Math.Cos(Units.ToRadians((Double)wpColTVal)));
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColCO( Decimal calcBzVal, Decimal wpColCNVal, Decimal wpColCSVal, Int32 wpColHBVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;
                if( Math.Abs( wpColCNVal - calcBzVal ) > Math.Abs( wpColHBVal ) )
                {
                    iResult = Math.Sign( wpColCSVal ) * wpColHBVal;
                }
                else
                {
                    iResult = wpColCNVal - calcBzVal;
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColCP( Int32 RunID, Decimal wpColCOVal, Decimal wpColCSVal, String dbConn)
        {
            try
            {
                Decimal iResult = -99.00000000M;
                Decimal coVal = -99.00000000M;
                Decimal csVal = -99.00000000M;
                Decimal avgCO = -99.00000000M;
                Decimal avgCS = -99.00000000M;
                List<Decimal> colCOList = new List<Decimal>();
                List<Decimal> colCSList = new List<Decimal>();

                if( Math.Abs( wpColCOVal ) < Math.Abs( wpColCSVal ) )
                {
                    String selVals = "SELECT [colCO], [colCS] FROM [WPQCResults] WHERE [runID] = @runID";
                    using(SqlConnection clntConn = new SqlConnection(dbConn))
                    {
                        try
                        {
                            clntConn.Open();

                            SqlCommand getVals = new SqlCommand(selVals, clntConn);
                            getVals.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();

                            using(SqlDataReader readVals = getVals.ExecuteReader())
                            {
                                try
                                {
                                    if(readVals.HasRows)
                                    {
                                        while(readVals.Read())
                                        {
                                            coVal = readVals.GetDecimal(0);
                                            csVal = readVals.GetDecimal(1);
                                            colCOList.Add(coVal);
                                            colCSList.Add(csVal);
                                        }
                                    }
                                }
                                catch(Exception ex)
                                {throw new System.Exception(ex.ToString());}
                                finally
                                {
                                    readVals.Close();
                                    readVals.Dispose();
                                }
                            }
                        }
                        catch(Exception ex)
                        {throw new System.Exception(ex.ToString());}
                        finally
                        {
                            clntConn.Close();
                            clntConn.Dispose();
                        }
                    }

                    colCOList.Add(wpColCOVal); //Current value added to list
                    colCSList.Add(wpColCSVal); //Current value added to list
                    
                    avgCO = colCOList.Average();
                    avgCS = colCSList.Average();

                    iResult = Decimal.Divide( Decimal.Add(avgCO , avgCS) , 2 );
                }
                else
                {
                    iResult = wpColCOVal;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColCQ(Int32 srvyRowID, Int32 RunID, Decimal wpColCPVal, String dbConn)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if (srvyRowID.Equals(1))
                {
                    iResult = wpColCPVal;
                }
                else
                {

                    Double[] trend = new Double[] { };

                    List<Double> cpList = new List<Double>();
                    List<Double> xVals = new List<Double>();
                    Decimal cpVal = 0.00000000M;
                    Int32 xVal = 0;

                    String dataCommand = "SELECT [srvyRowID], [colCP] FROM [WPQCResults] WHERE [runID] = @runID";

                    using (SqlConnection clntConn = new SqlConnection(dbConn))
                    {
                        try
                        {
                            clntConn.Open();

                            SqlCommand getData = new SqlCommand(dataCommand, clntConn);
                            getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();

                            using (SqlDataReader valueReader = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (valueReader.HasRows)
                                    {
                                        while (valueReader.Read())
                                        {
                                            xVal = valueReader.GetInt32(0);
                                            cpVal = valueReader.GetDecimal(1);
                                            xVals.Add((Double)xVal);
                                            cpList.Add((Double)cpVal);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    valueReader.Close();
                                    valueReader.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            clntConn.Close();
                            clntConn.Dispose();
                        }
                    }
                    xVals.Add((Double)srvyRowID);
                    cpList.Add((Double)wpColCPVal);

                    Double[] x = new Double[] { };
                    Double[] cp = new Double[] { };

                    x = xVals.ToArray();
                    cp = cpList.ToArray();

                    {
                        var parameters = MNRG.SimpleRegression.Fit(x, cp);
                        Double intercept = parameters.Item1;
                        Double slope = parameters.Item2;
                        //iResult = Convert.ToDecimal(intercept + (slope * srvyRowID));
                        iResult = Convert.ToDecimal(intercept + slope);
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColCR( Decimal calcBzVal, Decimal wpColCQVal )
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Decimal.Add(wpColCQVal, calcBzVal);

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColCS( Decimal calcBzVal, Decimal wpColCNVal )
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Decimal.Subtract( wpColCNVal , calcBzVal );

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColCT(Int32 srvyRowID, Int32 RunID, Decimal wpColCSVal, String dbConn)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if (srvyRowID.Equals(1) )
                {
                    iResult = 0.00000000M;
                }
                else
                {
                    Double[] trend = new Double[] { };

                    List<Double> csList = new List<Double>();
                    List<Double> xVals = new List<Double>();
                    Decimal csVal = 0.00000000M;
                    Int32 xVal = 0;

                    String dataCommand = "SELECT [srvyRowID], [colCS] FROM [WPQCResults] WHERE [runID] = @runID";

                    using (SqlConnection clntConn = new SqlConnection(dbConn))
                    {
                        try
                        {
                            clntConn.Open();

                            SqlCommand getData = new SqlCommand(dataCommand, clntConn);
                            getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();

                            using (SqlDataReader valueReader = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (valueReader.HasRows)
                                    {
                                        while (valueReader.Read())
                                        {
                                            xVal = valueReader.GetInt32(0);
                                            csVal = valueReader.GetDecimal(1);
                                            xVals.Add((Double)xVal);
                                            csList.Add((Double)csVal);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    valueReader.Close();
                                    valueReader.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            clntConn.Close();
                            clntConn.Dispose();
                        }
                    }
                    xVals.Add((Double)srvyRowID);
                    csList.Add((Double)wpColCSVal);

                    Double[] x = new Double[] { };
                    Double[] cs = new Double[] { };

                    x = xVals.ToArray();
                    cs = csList.ToArray();

                    {
                        var parameters = MNRG.SimpleRegression.Fit(x, cs);
                        Double intercept = parameters.Item1;
                        Double slope = parameters.Item2;
                        //iResult = Convert.ToDecimal(intercept + (slope * srvyRowID));
                        iResult = Convert.ToDecimal(intercept + slope);
                    }
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColCU(Decimal Bz, Decimal wpColCTVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Decimal.Add(wpColCTVal, Bz);

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColCV(Decimal Bx, Decimal By, Decimal wpColPVal, Decimal wpColTVal, Decimal wpColCUVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;
                Double rcColPVal = Math.Cos(Units.ToRadians((Double)wpColPVal));
                Double rsColPVal = Math.Sin(Units.ToRadians((Double)wpColPVal));
                Double rcColTVal = Math.Cos(Units.ToRadians((Double)wpColTVal));
                Double rsColTVal = Math.Sin(Units.ToRadians((Double)wpColTVal));
                Double valX = Convert.ToDouble(((Double)Bx * rcColPVal - (Double)By * rsColPVal) * rcColTVal + (Double)wpColCUVal * rsColTVal);
                Double valY = -1* ( (Double)Bx * rsColPVal + (Double)By * rcColPVal );
                Double CheckValue = Units.ToDegrees(Math.Atan2(valY,valX)) + 360;
                if ((CheckValue % 360) > 360)
                {
                    iResult = Convert.ToDecimal(CheckValue - 360);
                }
                else
                {
                    iResult = Convert.ToDecimal(CheckValue);
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColCW(Decimal wpColYVal, Decimal wpColCVVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Decimal.Subtract(wpColCVVal, wpColYVal);

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColCX(Decimal MDec, Decimal gridConvergence, Decimal wpColCVVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if( ( wpColCVVal + MDec - gridConvergence ) > 359.99M )
                {
                    iResult = (wpColCVVal + MDec - gridConvergence) - 360.00M;
                }
                else if( ( wpColCVVal + MDec - gridConvergence ) < 0.00M )
                {
                    iResult = ( wpColCVVal + MDec - gridConvergence ) + 360.00M;
                }
                else
                {
                    iResult = ( wpColCVVal + MDec - gridConvergence );
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColCY(Decimal iMDec, Decimal gridConvergence, Decimal wpColCVVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if( ( wpColCVVal + iMDec - gridConvergence ) > 359.99M )
                {
                    iResult = ( ( wpColCVVal + iMDec - gridConvergence ) - 360.00M );
                }
                else if( ( wpColCVVal + iMDec - gridConvergence ) < 0.00M )
                {
                    iResult = ( wpColCVVal + iMDec - gridConvergence ) + 360.00M;
                }
                else
                {
                    iResult = wpColCVVal + iMDec - gridConvergence;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColCZ(Decimal MDec, Decimal wpColCVVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if( ( wpColCVVal + MDec ) > 359.99M )
                {
                    iResult = ( ( wpColCVVal + MDec ) - 360.00M );
                }
                else if( ( wpColCVVal + MDec ) < 0.00M )
                {
                    iResult = ( wpColCVVal + MDec ) + 360.00M;
                }
                else
                {
                    iResult = wpColCVVal + MDec;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColDA(Decimal IMDec, Decimal wpColCVVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if ((wpColCVVal + IMDec) > 359.99M)
                {
                    iResult = ((wpColCVVal + IMDec) - 360.00M);
                }
                else if ((wpColCVVal + IMDec) < 0.00M)
                {
                    iResult = (wpColCVVal + IMDec) + 360.00M;
                }
                else
                {
                    iResult = wpColCVVal + IMDec;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColDB(Int32 ifrVal, Decimal wpColCXVal, Decimal wpColCYVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if (ifrVal.Equals(1))
                {
                    iResult = wpColCXVal;
                }
                else
                {
                    iResult = wpColCYVal;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColDC(Int32 ifrVal, Decimal wpColCZVal, Decimal wpColDAVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if (ifrVal.Equals(1))
                {
                    iResult = wpColCZVal;
                }
                else
                {
                    iResult = wpColDAVal;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColDD(Int32 jobConvergence, Decimal wpColDBVal, Decimal wpColDCVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if (jobConvergence.Equals(2)) // 1 = True; 2 = Grid
                {
                    iResult = wpColDBVal;
                }
                else
                {
                    iResult = wpColDCVal;
                }

                return iResult;
            }
            catch(Exception ex)
            {throw new System.Exception(ex.ToString());}
        }

        private static Decimal qcColDE(Decimal wpColAFVal, Decimal wpColDDVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Decimal.Subtract(wpColDDVal, wpColAFVal);

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 qcColDF(Decimal wpColDEVal)
        {
            try
            {
                Int32 iResult = -99;

                if( ( wpColDEVal <= 0.5M && wpColDEVal >= -0.5M ) )
                {
                    iResult = 1;
                }
                else if ( ( wpColDEVal > 0.5M || wpColDEVal < -0.5M ))
                {
                    iResult = 2;
                }
                else
                {
                    iResult = 3;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 qcColDG(Decimal wpColCSVal, Decimal wpColFVVal, Decimal wpColGPVal)
        {
            try
            {
                Int32 iResult = -99;

                if( ( (Decimal)Math.Abs((Double)wpColCSVal) - wpColGPVal ) > 0 )
                {
                    iResult = 3;
                }
                else if( ( (Decimal)Math.Abs((Double)wpColCSVal) - wpColFVVal) > 0 )
                {
                    iResult = 2;
                }
                else
                {
                    iResult = 1;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 qcColDH(Decimal Bx, Decimal By, Decimal wpColCUVal)
        {
            try
            {
                Int32 iResult = -99;

                iResult = Convert.ToInt32( Math.Sqrt( Math.Abs(Math.Pow( (Double)Bx , 2) + Math.Pow( (Double)By , 2) + Math.Pow((Double)wpColCUVal , 2))));

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColDI(Decimal Bx, Decimal By, Decimal Gx, Decimal Gy, Decimal Gz, Decimal wpColCFVal, Decimal wpColCUVal, Decimal wpColDHVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;
                Double valA = Convert.ToDouble((Bx) * (Gx) + (By) * (Gy) + (wpColCUVal) * (Gz));
                Double valB = Convert.ToDouble( wpColCFVal * wpColDHVal );
                Double value = Math.Asin(valA / valB);
                iResult = Convert.ToDecimal(Units.ToDegrees(value));                
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 qcColDJ(Int32 ifrVal, Decimal convBTotal, Decimal convIBTotal, Decimal wpColDHVal)
        {
            try
            {
                Int32 iResult = -99;

                if(ifrVal.Equals(1))
                {
                    iResult = Convert.ToInt32( -1 * ( convBTotal - wpColDHVal ) );
                }
                else
                {
                    iResult = Convert.ToInt32( -1 * ( convIBTotal - wpColDHVal ) );
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColDK(Int32 ifrVal, Decimal convDip, Decimal convIDip, Decimal wpColDIVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if(ifrVal.Equals(1))
                {
                    iResult = -1 * (convDip - wpColDIVal);
                }
                else
                {
                    iResult = -1 * (convIDip - wpColDIVal);
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColDX(Decimal wpColAFVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if( wpColAFVal >= 360.00M )
                {
                    iResult = Decimal.Subtract( wpColAFVal , 360.00M );
                }
                else
                {
                    iResult = wpColAFVal;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColEL(Decimal wpColFWVal, Decimal wpColFYVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Decimal.Multiply( Math.Sign(wpColFYVal) , wpColFWVal );

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColEM(Decimal wpColFXVal, Decimal wpColFYVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Decimal.Multiply(Math.Sign(wpColFYVal), wpColFXVal);

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 qcColEN(Decimal wpColFYVal)
        {
            try
            {
                Int32 iResult = -99;

                if(Math.Abs(wpColFYVal) > 0.5M )
                {
                    iResult = 3; //Not Enough Non Mag Spacing
                }
                else
                {
                    iResult = 4; //Good Non Mag Spacing
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 qcColFQ(Decimal bhaCMSize)
        {
            try
            {
                Int32 iResult = -99;

                if (bhaCMSize.Equals(0.00M))
                {
                    iResult = 0;
                }
                else
                {
                    //iResult = Convert.ToInt32( ( -1.2952M * Math.Abs( (Decimal)Math.Pow( (Double)bhaCMSize,3 ) ) ) + ( 26.353M * Math.Abs( (Decimal)Math.Pow( (Double)bhaCMSize , 2) ) ) - ( 122.55M * Math.Abs( bhaCMSize ) ) + 486.47M );
                    Double ValA = (-1.2952 * Math.Pow(Math.Abs((Double)bhaCMSize), 3));
                    Double ValB = (26.353 * Math.Pow(Math.Abs((Double)bhaCMSize), 2));
                    Double ValC = (122.55 * Math.Abs((Double)bhaCMSize));
                    iResult = Convert.ToInt32(ValA + ValB - ValC + 486.47);
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 qcColFR(Decimal bhaCDSCSize)
        {
            try
            {
                Int32 iResult = -99;
                Double ValA = -1.0486 * Math.Abs(Math.Pow((Double)bhaCDSCSize,3));
                Double ValB = 23.539 * Math.Abs(Math.Pow((Double)bhaCDSCSize,2));
                Double ValC = 85.581 * Math.Abs((Double)bhaCDSCSize);
                iResult = Convert.ToInt32( ValA + ValB - ValC + 488.27 );
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColFS(Int32 ifrVal, Decimal wpColBAVal, Int32 wpColCAVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if(ifrVal.Equals(1))
                {
                    iResult = wpColCAVal * (Decimal)Math.Cos( Units.ToRadians( (Double)wpColBAVal ) );
                }
                else
                {
                    iResult = wpColCAVal * (Decimal)Math.Cos( Units.ToRadians( (Double)wpColBAVal ) );
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColFT(Decimal bhaCNMSpAbv, Decimal wpColFRVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;
                Decimal ValA = wpColFRVal / (Decimal)(4 * Math.PI);
                Decimal ValB = Convert.ToDecimal(1 / (Math.Pow(((Double)bhaCNMSpAbv + 0.01),2)));
                iResult = ValA * ValB * 1000.00M;
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColFU(Decimal bhaCNMSpBlw, Decimal bhaCMDBLen, Decimal wpColFQVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = ( ( wpColFQVal / (Decimal)( 4 * (Decimal)Math.PI ) * (Decimal)( ( 1 / Math.Pow( ( (Double)bhaCNMSpBlw + 0.01 ) , 2 ) ) - ( 1 / Math.Pow( (Double)( bhaCNMSpBlw + bhaCMDBLen + 0.01M ) , 2 ) ) ) ) * 1000.00M );

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColFV(Decimal wpColFTVal, Decimal wpColFUVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Decimal.Add(wpColFTVal, wpColFUVal);

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColFW(Decimal wpColTVal, Decimal wpColYVal, Decimal wpColFSVal, Decimal wpColFUVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Convert.ToDecimal( Math.Abs(Units.ToDegrees(Math.Atan((Double)(wpColFUVal * (Decimal)Math.Sin(Units.ToRadians((Double)wpColTVal)) * (Decimal)Math.Sin(Units.ToRadians((Double)wpColYVal))) / (Double)(wpColFSVal + (wpColFUVal * ((Decimal)Math.Sin(Units.ToRadians((Double)wpColTVal)) * (Decimal)Math.Cos(Units.ToRadians((Double)wpColYVal)))))))));

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColFX(Decimal wpColTVal, Decimal wpColYVal, Decimal wpColFSVal, Decimal wpColFTVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Convert.ToDecimal(Math.Abs(Units.ToDegrees(Math.Atan((Double)(wpColFTVal * (Decimal)Math.Sin(Units.ToRadians((Double)wpColTVal)) * (Decimal)Math.Sin(Units.ToRadians((Double)wpColYVal))) / (Double)(wpColFSVal + (wpColFTVal * ((Decimal)Math.Sin(Units.ToRadians((Double)wpColTVal)) * (Decimal)Math.Cos(Units.ToRadians((Double)wpColYVal)))))))));

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColFY(Decimal wpColTVal, Decimal wpColYVal, Decimal wpColDEVal, Decimal wpColFSVal, Decimal wpColFVVal )
        {
            try
            {
                Decimal iResult = -99.00000000M;
                Double rColT = Units.ToRadians((Double)wpColTVal);
                Double rColY = Units.ToRadians((Double)wpColYVal);
                Double ValA = ((Double)wpColFVVal * Math.Sin(rColT) * Math.Sin(rColY));
                Double ValB = ((Double)wpColFSVal + ((Double)wpColFVVal * (Math.Sin(rColT) * Math.Cos(rColY))));
                Double rValC = Units.ToDegrees(Math.Atan(ValA / ValB));
                           
                iResult = Convert.ToDecimal( Math.Sign(wpColDEVal) * Math.Abs( rValC ) );

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 qcColFZ(Decimal wpColFYVal)
        {
            try
            {
                Int32 iResult = -99;

                if(Math.Abs(wpColFYVal)<=0.5M)
                {
                    iResult = 1;
                }
                else
                {
                    iResult = 2;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColGB(Decimal bhaCMSize, Decimal bhaCGMeter, Decimal bhaCGMeterBtm, Decimal wpColFQ)
        {
            try
            {
                Decimal iResult = -99.00M;
                Double ValA = Math.Pow((Double)bhaCGMeterBtm + (Double)bhaCMSize / 12 * 0.3048, 2);
                if(bhaCGMeter.Equals(0))
                {
                    iResult = ( wpColFQ * 4 * (Decimal)Math.PI * (Decimal)ValA * 100 );                    
                }
                else
                {
                    iResult = ( bhaCGMeter * 4 * (Decimal)Math.PI * (Decimal)ValA * 100 );                    
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColGC(Decimal bhaCNMSpBlw, Decimal bhaCMDBLen, Decimal wpColGB)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = 1 / (Decimal)( 4 * Math.PI) * ( Decimal.Divide( wpColGB , (Decimal)Math.Pow( ( (Double)bhaCNMSpBlw + 0.01 ) , 2 ) ) - Decimal.Divide( wpColGB , (Decimal)Math.Pow( (Double)( bhaCMDBLen + bhaCNMSpBlw - 0.01M ) , 2 ) ) ) * 1000.00M;

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColGD(Decimal wpColCTVal, Decimal wpColGCVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Math.Abs(Decimal.Subtract(Math.Abs(wpColCTVal), Math.Abs(wpColGCVal)));

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColGE(Decimal bhaCNMSpAbv, Decimal wpColGDVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = (Decimal)(4 * Math.PI) * ((wpColGDVal * (Decimal)(Math.Pow(((Double)bhaCNMSpAbv + 0.01), 2)))) * 0.001M;

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 qcColGF(Decimal worstDCPoleStrength, Decimal wpColGEVal)
        {
            try
            {
                Int32 iResult = -99;

                if (wpColGEVal > worstDCPoleStrength)
                {
                    iResult = 5; //BHA Magnetizaton outside limit
                }
                else
                {
                    iResult = 6; //BHA Magnitization within limit
                }
                
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColGG(Decimal bhaCDSCSize, Decimal wpColGEVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;
                iResult = wpColGEVal / (Decimal)( 4 * Math.PI * Math.Pow(( 0.01 + (Double)bhaCDSCSize / 12 * 0.3048 ) , 2) ) * 0.01M;                
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColGH(Decimal wpColTVal, Decimal wpColYVal, Decimal wpColDEVal, Decimal wpColFSVal, Decimal wpColGCVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Convert.ToDecimal(Math.Sign(wpColDEVal) * Math.Abs( Units.ToDegrees((Double)Math.Atan( (Double)(wpColGCVal * (Decimal)Math.Sin((Double)wpColTVal) * (Decimal)Math.Sin((Double)wpColYVal)) / (Double)( wpColFSVal + wpColGCVal * (Decimal)Math.Sin((Double)wpColTVal) * (Decimal)Math.Cos((Double)wpColYVal))))));

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColGI(Decimal wpColTVal, Decimal wpColYVal, Decimal wpColDEVal, Decimal wpColFSVal, Decimal wpColGDVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Convert.ToDecimal(Math.Sign(wpColDEVal) * Math.Abs(Units.ToDegrees(Math.Atan((Double)(wpColGDVal * (Decimal)Math.Sin((Double)wpColTVal) * (Decimal)Math.Sin((Double)wpColYVal) / (wpColFSVal + wpColGDVal * (Decimal)Math.Sin((Double)wpColTVal) * (Decimal)Math.Cos((Double)wpColYVal)))))));

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColGJ(Decimal wpColTVal, Decimal wpColYVal, Decimal wpColCTVal, Decimal wpColDEVal, Decimal wpColFSVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Convert.ToDecimal(Math.Sign(wpColDEVal)*Math.Abs(Units.ToDegrees(Math.Atan((Double)(wpColCTVal*(Decimal)Math.Sin((Double)wpColTVal)*(Decimal)Math.Sin((Double)wpColYVal))/(Double)(( wpColFSVal+wpColCTVal*(Decimal)Math.Sin((Double)wpColTVal)*(Decimal)Math.Cos((Double)wpColYVal)))))));

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColGL(Decimal wpColFQVal, Decimal wpColGBVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                if(Math.Abs(wpColGBVal)< Math.Abs(wpColFQVal))
                {
                    iResult = Math.Abs(wpColFQVal);
                }
                else                    
                {
                    iResult = Math.Abs(wpColGBVal);
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColGM(Decimal bhaCNMSBlw, Decimal bhaCMDBLen, Decimal wpColGLVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;
                
                iResult = ( 1 / ( 4 * (Decimal)Math.PI ) * ( ( wpColGLVal / (Decimal)Math.Pow( ( (Double)bhaCNMSBlw + 0.01 ) , 2 ) ) - ( wpColGLVal / (Decimal)Math.Pow( ( (Double)bhaCMDBLen + (Double)bhaCNMSBlw -0.01 ) , 2 ) ) ) )  * 1000.00M;

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColGN(Decimal worstDCPoleStrength)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = worstDCPoleStrength;

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColGO( Decimal bhaCNMSAbv, Decimal wpColGNVal )
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = ( wpColGNVal / (Decimal)( 4 * Math.PI ) * ( ( 1 / (Decimal)Math.Pow( ( (Double)bhaCNMSAbv + 0.01 ) , 2 ) ) ) ) * 1000.00M;

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColGP(Decimal wpColGMVal, Decimal wpColGOVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Decimal.Add(wpColGMVal, wpColGOVal);

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColGQ(Decimal wpColTVal, Decimal wpColYVal, Decimal wpColFSVal, Decimal wpColGMVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Convert.ToDecimal(Math.Abs(Units.ToDegrees(Math.Atan((Double)(wpColGMVal * (Decimal)Math.Sin((Double)wpColTVal) * (Decimal)Math.Sin((Double)wpColYVal)) / (Double)((wpColFSVal + wpColGMVal * (Decimal)Math.Sin((Double)wpColTVal) * (Decimal)Math.Cos((Double)wpColYVal)))))));

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColGR(Decimal wpColTVal, Decimal wpColYVal, Decimal wpColFSVal, Decimal wpColGOVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Convert.ToDecimal(Math.Abs(Units.ToDegrees(Math.Atan((Double)(wpColGOVal * (Decimal)Math.Sin((Double)wpColTVal) * (Decimal)Math.Sin((Double)wpColYVal)) / (Double)((wpColFSVal + wpColGOVal * (Decimal)Math.Sin((Double)wpColTVal) * (Decimal)Math.Cos((Double)wpColYVal)))))));

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColGS(Decimal wpColTVal, Decimal wpColYVal, Decimal wpColDEVal, Decimal wpColFSVal, Decimal wpColGPVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Convert.ToDecimal(Math.Sign(wpColDEVal) * Math.Abs(Units.ToDegrees( Math.Atan((Double)(wpColGPVal * (Decimal)Math.Sin((Double)wpColTVal) * (Decimal)Math.Sin((Double)wpColYVal)) / (Double)(wpColFSVal + wpColGPVal * (Decimal)Math.Sin((Double)wpColTVal) * (Decimal)Math.Cos((Double)wpColYVal))))));

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColGT(Decimal wpColCTVal, Decimal wpColGPVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Decimal.Subtract( Math.Abs(wpColGPVal), Math.Abs(wpColCTVal));

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal qcColGU(Decimal wpColDEVal, Decimal wpColGSVal)
        {
            try
            {
                Decimal iResult = -99.00000000M;

                iResult = Decimal.Subtract(Math.Abs(wpColGSVal), Math.Abs(wpColDEVal));

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 qcColGV(Decimal wpColGTVal)
        {
            try
            {
                Int32 iResult = -99;

                if (wpColGTVal < 0)
                {
                    iResult = 1;
                }
                else
                {
                    iResult = 0;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 qcColGW(Int32 ifrVal)
        {
            try
            {
                Int32 iResult = -99;

                if (ifrVal < 1)
                {
                    iResult = 0;
                }
                else
                {
                    iResult = 1;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 qcColGX(Decimal wpColGVVal, Decimal wpColGWVal)
        {
            try
            {
                Int32 iResult = -99;

                if( wpColGVVal.Equals(1) && wpColGWVal.Equals(1) )
                {
                    iResult = 2;
                }
                else if( wpColGVVal.Equals(1) && wpColGWVal.Equals(0) )
                {
                    iResult = 3;
                }
                else if ( wpColGVVal.Equals(0) && wpColGWVal.Equals(1) )
                {
                    iResult = 4;
                }
                else
                {
                    iResult = 1;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 qcColGY(Decimal wpColGUVal)
        {
            try
            {
                Int32 iResult = -99;

                if (wpColGUVal <= 0.00M)
                {
                    iResult = 1;
                }
                else
                {
                    iResult = 0;
                }
                
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 qcColHA(Int32 ifrVal, Int32 wpColBDVal, Decimal wpColCSVal, Decimal wpColFVVal, Decimal wpColFTVal, Decimal wpColFUVal)
        {
            try
            {
                Int32 iResult = -99;

                if(ifrVal.Equals(2))
                {
                    iResult = 1;
                }
                else if( ( wpColCSVal > wpColFVVal && ifrVal.Equals(1)) || (wpColBDVal.Equals(2) && wpColFTVal > wpColFUVal && ifrVal.Equals(1)) || ( wpColBDVal.Equals(3) && wpColFUVal > wpColFTVal && ifrVal.Equals(1)))
                {
                    iResult = 2;
                }
                else
                {
                    iResult = 1;
                }


                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 qcColHB( Decimal wpColTVal)
        {
            try
            {
                Int32 iResult = -99;

                if( wpColTVal <= 5 )
                {
                    iResult = 1;
                }
                else
                {
                    iResult = 2;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
                        
        //Processing Well Profile Dataset Row
        public static Int32 qcDSRow(String UserName, String ClntRealm, Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 RunID, Int32 BHAID, Int32 SQCID, Decimal dsDepth, Decimal dsInclination, Decimal dsAzimuth, Decimal dsGTF, Int32 srRowID, String dbConn)
        {
            try
            {
                Int32 iResult = -99, iInsertBG = -99, iAccountBG = -99, iInsertDS = -99, iAccountDS = -99, gcoID = -99, mdecCID = -99, imdecCID = -99;                
                Decimal lcRef1 = -99.0000M, lcRef2 = -99.0000M, lcRef3 = -99.0000M, lcRef4 = -99.0000M;
                Decimal dflLowerBT = -99.00M, dflLowerDip = -99.00M, dflLowerGT = -99.0000M, dflUpperBT = -99.00M, dflUpperDip = -99.00M, dflUpperGT = -99.0000M;
                Decimal worstDCPoleStrength = -99.00M;                
                Int32 ifrVal = -99, LenUnit = -99, jobConverg = -99, acelUnit = -99, magUnit = -99, ngzInc = -99, ngzAzm = -99;
                Int32 LatiNS = -99, LngiEW = -99;
                Decimal jobMDec = -99.00M, jobDip = -99.00M, jobCDip = -99.00M, jobBT = -99.000000M, jobCBT = -99.000000M, jobGT = -99.000000M;
                Decimal jobCGT = -99.000000000M, gridConverg = -99.00M, jobIMDec = -99.00M, jobIDip = -99.00M, jobICDip = -99.00M;
                Decimal jobIBT = -99.00M, jobICBT = -99.000000M, jobIGT = -99.00M, jobICGT = -99.000000000M;
                Decimal rInc = -99.00M, rAzm = -99.00M, rTInc = -99.00M, rTAzm = -99.00M;
                Decimal convRTAzm = -99.0000M, convRTAzmIFR = -99.0000M, convPTInc = -99.0000M, convPTAzm = -99.0000M;
                Decimal bhaMSize = -99.00M, bhaCMSize = -99.00M, bhaDCSize = -99.00M, bhaCDCSize = -99.00M, bhaMDBLen = -99.00M, bhaCMDBLen = -99.00M;
                Decimal bhaNMSAbv = -99.00M, bhaCNMSAbv = -99.00M, bhaNMSBlw = -99.00M, bhaCNMSBlw = -99.00M, bhaGMeter = -99.000000M;
                Int32 bhaMSizeUID = -99, bhaDCSizeUID = -99, bhaMDBLenUID = -99, bhaNMSAbvUID = -99, bhaNMSBlwUID = -99, bhaGMeterUID = -99;
                Decimal bhaCGMeter = -99.000000M, bhaGMeterTop = -99.00M, bhaCGMeterTop = -99.00M, bhaDSG = -99.000000M, bhaCDSG = -99.000000M, bhaGMeterBtm = -99.00M;
                Int32 bhaGMeterTopUID = -99, bhaDSGUID = -99, bhaGMeterBtmUID = -99;
                Decimal bhaCGMeterBtm = -99.00M, bhaAllowable = -99.00M;
                Decimal dipTol = -99.0000M, BTTol = -99.0000M, cBTTol = -99.000000M, GTTol = -99.0000000M, cGTTol = -99.000000000M;

                List<Int32> WellInfo = new List<Int32> { };
                ArrayList JobInfo = new ArrayList(), RefMags = new ArrayList(), SQCInfo = new ArrayList(), RunInfo = new ArrayList(), BHAInfo = new ArrayList();
                ArrayList dflInfo = new ArrayList(); //Delta Fluctuation Limit
                ArrayList dcpsInfo = new ArrayList(); //Drill Collar Pole Strength
                ArrayList ngzInfo = new ArrayList(); //No Go Zone Azimuth

                //Getting Meta Data
                
                acelUnit = Convert.ToInt32(JobInfo[1]);
                magUnit = Convert.ToInt32(JobInfo[2]);
                LenUnit = Convert.ToInt32(JobInfo[3]);
                jobConverg = Convert.ToInt32(JobInfo[4]);

                WellInfo = AST.getWellDirection(WellID, dbConn);
                LatiNS = WellInfo[0];
                LngiEW = WellInfo[1];

                //// Update Reference Magnetics
                RefMags = OPR.getRefMagList(OperatorID, ClientID, WellPadID, WellID, dbConn);
                ifrVal = Convert.ToInt32(RefMags[0]);
                jobMDec = Convert.ToDecimal(RefMags[1]);
                jobDip = Convert.ToDecimal(RefMags[3]);
                jobCDip = Units.convDip(jobDip);                
                jobBT = Convert.ToDecimal(RefMags[2]);
                jobCBT = Units.convBTotal(jobBT, magUnit);
                jobGT = Convert.ToDecimal(RefMags[3]);
                jobCGT = Units.convGTotal(jobGT, acelUnit);
                gridConverg = Convert.ToDecimal(RefMags[4]);
                mdecCID = Convert.ToInt32(RefMags[5]);
                gcoID = Convert.ToInt32(RefMags[6]);
                jobIMDec = Convert.ToDecimal(RefMags[7]);
                imdecCID = Convert.ToInt32(RefMags[8]);
                jobIDip = Convert.ToDecimal(RefMags[9]);
                jobICDip = Units.convDip(jobIDip);
                jobIBT = Convert.ToDecimal(RefMags[10]);
                jobICBT = Units.convBTotal(jobIBT, magUnit);
                jobIGT = Convert.ToDecimal(RefMags[11]);
                jobICGT = Units.convGTotal(jobIGT, acelUnit);

                SQCInfo = AST.getSelectedSQCList(SQCID, dbConn);
                dipTol = Convert.ToDecimal(SQCInfo[0]);
                BTTol = Convert.ToDecimal(SQCInfo[1]);
                GTTol = Convert.ToDecimal(SQCInfo[2]);
                
                RunInfo = AST.getWPRunList(RunID, dbConn);
                rInc = Convert.ToDecimal(RunInfo[0]);
                rAzm = Convert.ToDecimal(RunInfo[1]);
                rTInc = Convert.ToDecimal(RunInfo[2]);
                rTAzm = Convert.ToDecimal(RunInfo[3]);
                convRTAzm = QC.calConvRunTargetAzimuth(rAzm, LatiNS, jobMDec, mdecCID, gridConverg, gcoID);
                convRTAzmIFR = QC.calConvRunTargetAzimuthIFR(rAzm, LatiNS, jobIMDec, imdecCID, gridConverg, gcoID);
                convPTAzm = QC.calConvProposedAzimuth(ifrVal, LatiNS, jobMDec, jobIMDec, mdecCID, imdecCID, gridConverg, gcoID, rTAzm);
                BHAInfo = AST.getWPBhaInfoList(BHAID, dbConn);
                bhaMSize = Convert.ToDecimal(BHAInfo[0]);
                bhaMSizeUID = Convert.ToInt32(BHAInfo[1]);
                bhaCMSize = Units.convMotorSize(bhaMSize, bhaMSizeUID);
                bhaDCSize = Convert.ToDecimal(BHAInfo[2]);
                bhaDCSizeUID = Convert.ToInt32(BHAInfo[3]);
                bhaCDCSize = Units.convDSCSize(bhaDCSize, bhaDCSizeUID);
                bhaMDBLen = Convert.ToDecimal(BHAInfo[4]);
                bhaMDBLenUID = Convert.ToInt32(BHAInfo[5]);
                bhaCMDBLen = Units.convMDLen(bhaMDBLen, bhaMDBLenUID);
                bhaNMSAbv = Convert.ToDecimal(BHAInfo[6]);
                bhaNMSAbvUID = Convert.ToInt32(BHAInfo[7]);
                bhaCNMSAbv = Units.convNMSp(bhaNMSAbv, bhaNMSAbvUID);
                bhaNMSBlw = Convert.ToDecimal(BHAInfo[8]);
                bhaNMSBlwUID = Convert.ToInt32(BHAInfo[9]);
                bhaCNMSBlw = Units.convNMSp(bhaNMSBlw, bhaNMSBlwUID);
                bhaAllowable = OPR.getAllowableSpace(rTInc, rTAzm);
                bhaGMeter = Convert.ToDecimal(BHAInfo[11]);
                bhaGMeterUID = Convert.ToInt32(BHAInfo[12]);
                bhaCGMeter = Units.convGMeter(bhaGMeter, bhaGMeterUID);
                bhaGMeterTop = Convert.ToDecimal(BHAInfo[13]);
                bhaGMeterTopUID = Convert.ToInt32(BHAInfo[14]);
                bhaCGMeterTop = Units.convGMtrDistance(bhaGMeterTop, bhaGMeterTopUID);
                bhaDSG = Convert.ToDecimal(BHAInfo[15]);
                bhaDSGUID = Convert.ToInt32(BHAInfo[16]);
                bhaCDSG = Units.convGMeter(bhaDSG, bhaDSGUID);
                bhaGMeterBtm = Convert.ToDecimal(BHAInfo[17]);
                bhaGMeterBtmUID = Convert.ToInt32(BHAInfo[18]);
                bhaCGMeterBtm = Units.convGMtrDistance(bhaGMeterBtm, bhaGMeterBtmUID);

                dflInfo = DMG.getdflMetaDataList();
                dflLowerBT = Convert.ToDecimal(dflInfo[0]);
                dflLowerDip = Convert.ToDecimal(dflInfo[1]);                
                dflLowerGT = Convert.ToDecimal(dflInfo[2]);                
                dflUpperBT = Convert.ToDecimal(dflInfo[3]);
                dflUpperDip = Convert.ToDecimal(dflInfo[4]);
                dflUpperGT = Convert.ToDecimal(dflInfo[5]);

                dcpsInfo = DMG.getdcpsDataList();
                worstDCPoleStrength = Convert.ToDecimal(dcpsInfo[0]);

                ngzInfo = DMG.getngzDataList();
                ngzInc = Convert.ToInt32(ngzInfo[0]);
                ngzAzm = Convert.ToInt32(ngzInfo[1]);

                //boxyError = DMG.getBoxyError();
                
                //Calculate BGs
                Decimal inputDipVal = -99.0000M, inputBTVal = -99.000000000M, inputGTVal = -99.000000000M, inputCBTVal = -99.000000000M, inputCGTVal = -99.000000000M;
                Decimal gridAzmVal = -99.000000000M, TrueAzmVal = -99.000000000M, FinalAzmVal = -99.000000000M;
                Decimal calcGxVal = -99.000000000000M, calcGyVal = -99.000000000000M, calcGzVal = -99.000000000000M;
                Decimal calcBxVal = -99.000000000000M, calcByVal = -99.000000000000M, calcBzVal = -99.000000000000M;

                inputDipVal = inputDip(ifrVal, jobCDip, jobICDip);
                inputBTVal = inputBT(ifrVal, jobCBT, jobICBT);
                inputCBTVal = Units.convBTotal(inputBTVal, magUnit);
                inputGTVal = inputGT(ifrVal, jobCGT, jobICGT);
                inputCGTVal = Units.convGTotal(inputGTVal, acelUnit);

                //gridAzmVal = gridAzm(ifrVal, NSDirection, EWDirection, dsAzimuth, jobMDec, jobIMDec, gridConverg);
                //TrueAzmVal = TrueAzm(ifrVal, NSDirection, EWDirection, dsAzimuth, jobMDec, jobIMDec);
                gridAzmVal = gridAzm(ifrVal, dsAzimuth, jobMDec, jobIMDec, gridConverg);
                TrueAzmVal = TrueAzm(ifrVal, dsAzimuth, jobMDec, jobIMDec);
                FinalAzmVal = FinalAzm(jobConverg, gridAzmVal, TrueAzmVal);
                calcGxVal = calcGx(inputCGTVal, dsInclination, dsGTF);
                calcGyVal = calcGy(inputCGTVal, dsInclination, dsGTF);
                calcGzVal = calcGz(inputCGTVal, dsInclination);
                calcBxVal = calcBx(inputCBTVal, jobCDip, FinalAzmVal, dsInclination, dsGTF);
                calcByVal = calcBy(inputCBTVal, jobCDip, dsInclination, FinalAzmVal, dsGTF);
                calcBzVal = calcBz(inputCBTVal, jobCDip, dsInclination, FinalAzmVal);

                iInsertBG = insWPBG(WellPadID, dsDepth, calcGxVal, calcGyVal, calcGzVal, calcBxVal, calcByVal, calcBzVal, RunID, BHAID, srRowID, dbConn);
                if (iInsertBG.Equals(1))
                {
                    iAccountBG = ACCTNG.accWPBG(UserName, ClntRealm, WellPadID, RunID, BHAID, srRowID, dsDepth); //Accounting for WellProfile's BGs
                    if (iAccountBG.Equals(1))
                    {
                        //Process QC for calculated BGs
                        Decimal wpColPVal = -99.00000000M;
                        Decimal wpColQVal = -99.00000000M;
                        Decimal wpColSVal = -99.00000000M;
                        Decimal wpColTVal = -99.00000000M;
                        Int32 wpColVVal = -99;
                        Decimal wpColWVal = -99.00000000M;
                        Decimal wpColYVal = -99.00000000M;
                        Decimal wpColZVal = -99.00000000M;
                        Decimal wpColAAVal = -99.00000000M;
                        Decimal wpColABVal = -99.00000000M;
                        Decimal wpColACVal = -99.00000000M;
                        Decimal wpColADVal = -99.00000000M;
                        Decimal wpColAEVal = -99.00000000M;
                        Decimal wpColAFVal = -99.00000000M;
                        Decimal wpColAHVal = -99.00000000M;
                        Decimal wpColAIVal = -99.00000000M;
                        Decimal wpColAJVal = -99.00000000M;
                        Decimal wpColAKVal = -99.00000000M;
                        Decimal wpColALVal = -99.00000000M;
                        Decimal wpColAMVal = -99.00000000M;
                        Decimal wpColANVal = -99.00000000M;
                        Decimal wpColAOVal = -99.00000000M;
                        Decimal wpColAPVal = -99.00000000M;
                        Decimal wpColARVal = -99.00000000M;
                        Decimal wpColASVal = -99.00000000M;
                        Decimal wpColATVal = -99.00000000M;
                        Decimal wpColAUVal = -99.00000000M;
                        Int32 wpColAVVal = -99;
                        Int32 wpColAWVal = -99;
                        Int32 wpColAXVal = -99;
                        Int32 wpColAYVal = -99;
                        Decimal wpColBAVal = -99.00M;
                        Decimal wpColBBVal = -99.0000M;
                        Decimal wpColBCVal = -99.0000M;
                        Int32 wpColBDVal = -99;
                        Decimal wpColBFVal = -99.00M;
                        Decimal wpColBGVal = -99.00M;
                        Decimal wpColBHVal = -99.00M;
                        Decimal wpColBIVal = -99.00M;
                        Decimal wpColBJVal = -99.00M;
                        Decimal wpColBKVal = -99.00M;
                        Decimal wpColBMVal = -99.00M;
                        Decimal wpColBNVal = -99.00M;
                        Decimal wpColBOVal = -99.00M;
                        Decimal wpColBPVal = -99.00M;
                        Decimal wpColBQVal = -99.00M;
                        Decimal wpColBRVal = -99.00M;
                        Decimal wpColBSVal = -99.00M;
                        Decimal wpColBTVal = -99.00M;
                        Decimal wpColBUVal = -99.00M;
                        Decimal wpColBVVal = -99.00M;
                        Decimal wpColBWVal = -99.00M;
                        Decimal wpColBXVal = -99.00M;
                        Decimal wpColBZVal = -99.00000000M;
                        Int32 wpColCAVal = -99;
                        Decimal wpColCBVal = -99.00M;
                        Int32 wpColCCVal = -99;
                        Decimal wpColCDVal = -99.00M;
                        Decimal wpColCEVal = -99.000000M;
                        Decimal wpColCFVal = -99.0000M;
                        Decimal wpColCGVal = -99.0000M;
                        Decimal wpColCHVal = -99.0000M;
                        Int32 wpColCIVal = -99;
                        Decimal wpColCKVal = -99.0000M;
                        Decimal wpColCLVal = -99.0000M;
                        Decimal wpColCMVal = -99.0000M;
                        Decimal wpColCNVal = -99.0000M;
                        Decimal wpColCOVal = -99.0000M;
                        Decimal wpColCPVal = -99.0000M;
                        Decimal wpColCQVal = -99.0000M;
                        Decimal wpColCRVal = -99.0000M;
                        Decimal wpColCSVal = -99.0000M;
                        Decimal wpColCTVal = -99.0000M;
                        Decimal wpColCUVal = -99.0000M;
                        Decimal wpColCVVal = -99.0000M;
                        Decimal wpColCWVal = -99.0000M;
                        Decimal wpColCXVal = -99.0000M;
                        Decimal wpColCYVal = -99.0000M;
                        Decimal wpColCZVal = -99.0000M;
                        Decimal wpColDAVal = -99.0000M;
                        Decimal wpColDBVal = -99.0000M;
                        Decimal wpColDCVal = -99.0000M;
                        Decimal wpColDDVal = -99.0000M;
                        Decimal wpColDEVal = -99.0000M;
                        Int32 wpColDFVal = -99;
                        Int32 wpColDGVal = -99;
                        Int32 wpColDHVal = -99;
                        Decimal wpColDIVal = -99.00M;
                        Int32 wpColDJVal = -99;
                        Decimal wpColDKVal = -99.00M;
                        Decimal wpColDXVal = -99.00M;
                        Decimal wpColELVal = -99.00M;
                        Decimal wpColEMVal = -99.00M;
                        Int32 wpColENVal = -99;
                        Int32 wpColFQVal = -99;
                        Int32 wpColFRVal = -99;
                        Decimal wpColFSVal = -99.00M;
                        Decimal wpColFTVal = -99.00M;
                        Decimal wpColFUVal = -99.00M;
                        Decimal wpColFVVal = -99.00M;
                        Decimal wpColFWVal = -99.00M;
                        Decimal wpColFXVal = -99.00M;
                        Decimal wpColFYVal = -99.00M;
                        Int32 wpColFZVal = -99;
                        Decimal wpColGBVal = -99.00M;
                        Decimal wpColGCVal = -99.00M;
                        Decimal wpColGDVal = -99.00M;
                        Decimal wpColGEVal = -99.00M;
                        Int32 wpColGFVal = -99;
                        Decimal wpColGGVal = -99.00M;
                        Decimal wpColGHVal = -99.00M;
                        Decimal wpColGIVal = -99.00M;
                        Decimal wpColGJVal = -99.00M;
                        Decimal wpColGLVal = -99.00M;
                        Decimal wpColGMVal = -99.00M;
                        Decimal wpColGNVal = -99.00M;
                        Decimal wpColGOVal = -99.00M;
                        Decimal wpColGPVal = -99.00M;
                        Decimal wpColGQVal = -99.00M;
                        Decimal wpColGRVal = -99.00M;
                        Decimal wpColGSVal = -99.00M;
                        Decimal wpColGTVal = -99.00M;
                        Decimal wpColGUVal = -99.00M;
                        Int32 wpColGVVal = -99;
                        Int32 wpColGWVal = -99;
                        Int32 wpColGXVal = -99;
                        Int32 wpColGYVal = -99;
                        Int32 wpColHAVal = -99;
                        Int32 wpColHBVal = -99;
                        Int32 taID = -99;

                        //Calculating QC values

                        wpColPVal = qcColP(calcGxVal, calcGyVal);
                        wpColQVal = qcColQ(calcBxVal, calcByVal);
                        wpColSVal = qcColS(ifrVal, jobMDec, jobIMDec);
                        wpColTVal = qcColT(calcGxVal, calcGyVal, calcGzVal);
                        wpColVVal = qcColV(wpColTVal);
                        wpColWVal = qcColW(RunID, srRowID, wpColTVal, dbConn);
                        wpColYVal = qcColY(calcBxVal, calcByVal, calcBzVal, wpColPVal, wpColTVal);
                        wpColZVal = QC.colZ(wpColYVal, jobMDec, gridConverg, LatiNS, mdecCID, gcoID);
                        wpColAAVal = QC.colAA(wpColYVal, jobIMDec, gridConverg, LatiNS, imdecCID, gcoID);
                        wpColABVal = QC.colAB(wpColYVal, jobMDec, mdecCID);
                        wpColACVal = QC.colAC(wpColYVal, jobIMDec, imdecCID);
                        //wpColACVal = qcColAC(jobIMDec, wpColYVal);
                        wpColADVal = qcColAD(ifrVal, wpColZVal, wpColAAVal);
                        wpColAEVal = qcColAE(ifrVal, wpColABVal, wpColACVal);
                        wpColAFVal = qcColAF(jobConverg, wpColADVal, wpColAEVal);
                        wpColBIVal = qcColBI(ifrVal, jobCDip, jobICDip, jobCBT, jobICBT, wpColPVal, wpColTVal, wpColYVal);
                        wpColBJVal = qcColBJ(ifrVal, jobBT, jobIBT, jobCDip, jobICDip, wpColPVal, wpColTVal, wpColYVal);
                        wpColBKVal = qcColBK(ifrVal, jobCDip, jobICDip, jobCBT, jobICBT, wpColTVal, wpColYVal);
                        wpColBMVal = qcColBM(srRowID, RunID, calcBxVal, wpColBIVal, dbConn);
                        wpColBNVal = qcColBN(srRowID, RunID, calcByVal, wpColBJVal, dbConn);
                        wpColBOVal = qcColBO(srRowID, RunID, calcBxVal, wpColBIVal, dbConn);
                        wpColBPVal = qcColBP(srRowID, RunID, calcByVal, wpColBJVal, dbConn);
                        wpColBQVal = qcColBQ(calcBxVal, wpColBMVal, wpColBOVal);
                        wpColBRVal = qcColBR(calcByVal, wpColBNVal, wpColBPVal);
                        wpColCKVal = qcColCK(ifrVal, jobCDip, jobICDip, jobCBT, jobICBT, calcBzVal, wpColTVal, wpColYVal, wpColBKVal);
                        wpColCLVal = qcColCL(ifrVal, jobCDip, jobICDip, wpColTVal, wpColYVal, wpColCKVal);
                        wpColCMVal = qcColCM(wpColYVal, wpColCLVal);
                        wpColCNVal = qcColCN(ifrVal, jobCDip, jobICDip, jobCBT, jobICBT, wpColTVal, wpColCMVal);
                        wpColCSVal = qcColCS(calcBzVal, wpColCNVal);
                        wpColHBVal = qcColHB(wpColTVal);
                        wpColCOVal = qcColCO(calcBzVal, wpColCNVal, wpColCSVal, wpColHBVal);
                        wpColCPVal = qcColCP(RunID, wpColCOVal, wpColCSVal, dbConn);
                        wpColCQVal = qcColCQ(srRowID, RunID, wpColCPVal, dbConn);
                        wpColCRVal = qcColCR(calcBzVal, wpColCRVal);
                        wpColAHVal = qcColAH(wpColPVal, wpColTVal, wpColBQVal, wpColBRVal, wpColCRVal);
                        //wpColAIVal = QC.colAI(NSDirection, EWDirection, wpColAHVal, jobMDec, gridConverg);
                        wpColAIVal = qcColAI(jobMDec, gridConverg, wpColAHVal);
                        //wpColAJVal = QC.colAJ(NSDirection, EWDirection, wpColAHVal, jobIMDec, gridConverg);
                        wpColAJVal = qcColAJ(jobIMDec, gridConverg, wpColAHVal);
                        //wpColAKVal = QC.colAK(NSDirection, EWDirection, wpColAHVal, jobMDec);
                        wpColAKVal = qcColAK(jobMDec, wpColAHVal);
                        //wpColALVal = QC.colAL(NSDirection, EWDirection, wpColAHVal, jobIMDec);
                        wpColALVal = qcColAL(jobIMDec, wpColAHVal);
                        wpColAMVal = qcColAM(ifrVal, wpColAIVal, wpColAJVal);
                        wpColANVal = qcColAN(ifrVal, wpColAKVal, wpColALVal);
                        wpColAOVal = qcColAO(jobConverg, wpColAMVal, wpColANVal);
                        wpColAPVal = qcColAP(wpColYVal, wpColAHVal);
                        wpColARVal = qcColAR(srRowID, RunID, wpColYVal, dbConn);
                        wpColASVal = qcColAS(wpColWVal, wpColARVal);
                        wpColATVal = qcColAT(srRowID, RunID, dsDepth, wpColVVal, wpColWVal, wpColASVal, dbConn);
                        wpColAUVal = qcColAU(srRowID, LenUnit, wpColVVal, wpColATVal, lcRef1, lcRef2, lcRef3, lcRef4);
                        wpColAVVal = qcColAV(wpColAUVal);
                        wpColAWVal = qcColAW(wpColTVal);
                        wpColAXVal = qcColAX(wpColYVal);
                        wpColAYVal = qcColAY(wpColAWVal, wpColAXVal);
                        wpColCAVal = qcColCA(calcBxVal, calcByVal, calcBzVal);
                        wpColCFVal = qcColCF(calcGxVal, calcGyVal, calcGzVal);
                        wpColBAVal = qcColBA(calcBxVal, calcByVal, calcBzVal, calcGxVal, calcGyVal, calcGzVal, wpColCAVal, wpColCFVal);
                        wpColBBVal = qcColBB(srRowID, RunID, wpColBAVal, dbConn);
                        wpColBCVal = qcColBC(ifrVal, jobCDip, jobICDip, wpColBAVal);
                        wpColBDVal = qcColBD(srRowID, dflLowerBT, dflUpperDip, wpColBCVal, dbConn);
                        wpColBFVal = qcColBF(ifrVal, jobCGT, jobICGT, wpColPVal, wpColTVal);
                        wpColBGVal = qcColBG(ifrVal, wpColPVal, wpColTVal, jobCGT, jobICGT);
                        wpColBHVal = qcColBH(ifrVal, jobCGT, jobICGT, wpColTVal);
                        wpColBSVal = qcColBS(calcBxVal, wpColBQVal);
                        wpColBTVal = qcColBT(calcByVal, wpColBRVal);
                        wpColCTVal = qcColCT(srRowID, RunID, wpColCSVal, dbConn);
                        wpColCUVal = qcColCU(calcBzVal, wpColCTVal);
                        wpColBUVal = qcColBU(wpColBQVal, wpColBRVal, wpColCUVal);                        
                        wpColBVVal = qcColBV(calcGxVal, calcGyVal, calcGzVal, wpColBQVal, wpColBRVal, wpColBUVal, wpColCFVal, wpColCUVal);
                        wpColBWVal = qcColBW(ifrVal, jobCBT, jobICBT, wpColBUVal);
                        wpColBXVal = qcColBX(ifrVal, jobCDip, jobICDip, wpColBVVal);
                        wpColBZVal = qcColBZ(calcBxVal, calcByVal);                        
                        wpColCBVal = qcColCB(srRowID, RunID, wpColCAVal, dbConn);
                        wpColCCVal = qcColCC(srRowID, dflLowerBT, dflUpperBT, wpColCDVal, dbConn);
                        wpColCDVal = qcColCD(ifrVal, jobCBT, jobICBT, wpColCAVal);
                        wpColCEVal = qcColCE(calcGxVal, calcGyVal);
                        wpColCGVal = qcColCG(ifrVal, jobGT, jobIGT, wpColCFVal);
                        wpColCHVal = qcColCH(srRowID, RunID, wpColCFVal, dbConn);
                        wpColCIVal = qcColCI(srRowID, dflLowerBT, dflUpperBT, wpColCGVal, dbConn);
                        wpColCVVal = qcColCV(calcBxVal, calcByVal, wpColPVal, wpColTVal, wpColCUVal);
                        wpColCWVal = qcColCW(wpColYVal, wpColCVVal);
                        //wpColCXVal = QC.colCX(NSDirection, EWDirection, jobMDec, gridConverg, wpColCVVal);
                        wpColCXVal = qcColCX(jobMDec, gridConverg, wpColCVVal);
                        //wpColCYVal = QC.colCY(NSDirection, EWDirection, jobIMDec, gridConverg, wpColCVVal);
                        wpColCYVal = qcColCY(jobIMDec, gridConverg, wpColCYVal);
                        //wpColCZVal = QC.colCZ(NSDirection, EWDirection, jobMDec, wpColCVVal);
                        wpColCZVal = qcColCZ(jobMDec, wpColCVVal);
                        //wpColDAVal = QC.colDA(NSDirection, EWDirection, jobIMDec, wpColCVVal);
                        wpColDAVal = qcColDA(jobIMDec, wpColCVVal);
                        wpColDBVal = qcColDB(ifrVal, wpColCXVal, wpColCYVal);
                        wpColDCVal = qcColDC(ifrVal, wpColCZVal, wpColDAVal);
                        wpColDDVal = qcColDD(jobConverg, wpColDBVal, wpColDCVal);
                        wpColDEVal = qcColDE(wpColAFVal, wpColDDVal);
                        wpColFQVal = qcColFQ(bhaCMSize);
                        wpColFRVal = qcColFR(bhaCDCSize);
                        wpColFTVal = qcColFT(bhaCNMSAbv, wpColFRVal);
                        wpColFUVal = qcColFU(bhaCNMSBlw, bhaCMDBLen, wpColFQVal);
                        wpColFVVal = qcColFV(wpColFTVal, wpColFUVal);
                        wpColGBVal = qcColGB(bhaCMSize, bhaCGMeter, bhaCGMeterBtm, wpColFQVal);
                        wpColGLVal = qcColGL(wpColFQVal, wpColGBVal);
                        wpColGMVal = qcColGM(bhaCNMSBlw, bhaCMDBLen, wpColGLVal);
                        wpColGNVal = qcColGN(worstDCPoleStrength);
                        wpColGOVal = qcColGO(bhaCNMSAbv, wpColGNVal);
                        wpColGPVal = qcColGP(wpColGMVal, wpColGOVal);
                        wpColDFVal = qcColDF(wpColDEVal);
                        wpColDGVal = qcColDG(wpColCSVal, wpColFVVal, wpColGPVal);
                        wpColDHVal = qcColDH(calcBxVal, calcByVal, wpColCUVal);
                        wpColDIVal = qcColDI(calcBxVal, calcByVal, calcGxVal, calcGyVal, calcGzVal, wpColCFVal, wpColCUVal, wpColDHVal);
                        wpColDJVal = qcColDJ(ifrVal, jobCBT, jobICBT, wpColDHVal);
                        wpColDKVal = qcColDK(ifrVal, jobCDip, jobICDip, wpColDIVal);
                        wpColDXVal = qcColDX(wpColAFVal);                        
                        wpColFRVal = qcColFR(bhaCDCSize);
                        wpColFSVal = qcColFS(ifrVal, wpColBAVal, wpColCAVal);
                        wpColFWVal = qcColFW(wpColTVal, wpColYVal, wpColFSVal, wpColFUVal);
                        wpColFXVal = qcColFX(wpColTVal, wpColYVal, wpColFSVal, wpColFTVal);
                        wpColFYVal = qcColFY(wpColTVal, wpColYVal, wpColDEVal, wpColFSVal, wpColFVVal);
                        wpColFZVal = qcColFZ(wpColFYVal);
                        wpColELVal = qcColEL(wpColFWVal, wpColFYVal);
                        wpColEMVal = qcColEM(wpColFXVal, wpColFYVal);
                        wpColENVal = qcColEN(wpColFYVal);
                        wpColGCVal = qcColGC(bhaCNMSBlw, bhaCMDBLen, wpColGBVal);
                        wpColGDVal = qcColGD(wpColCTVal, wpColGCVal);
                        wpColGEVal = qcColGE(bhaCNMSAbv, wpColGDVal);
                        wpColGFVal = qcColGF(worstDCPoleStrength, wpColGEVal);
                        wpColGGVal = qcColGG(bhaCDCSize, wpColGEVal);
                        wpColGHVal = qcColGH(wpColTVal, wpColYVal, wpColDEVal, wpColFSVal, wpColGCVal);
                        wpColGIVal = qcColGI(wpColTVal, wpColYVal, wpColDEVal, wpColFSVal, wpColGDVal);
                        wpColGJVal = qcColGJ(wpColTVal, wpColYVal, wpColCTVal, wpColDEVal, wpColFSVal);
                        wpColGQVal = qcColGQ(wpColTVal, wpColYVal, wpColFSVal, wpColGMVal);
                        wpColGRVal = qcColGR(wpColTVal, wpColYVal, wpColFSVal, wpColGOVal);
                        wpColGSVal = qcColGS(wpColTVal, wpColYVal, wpColDEVal, wpColFSVal, wpColGPVal);
                        wpColGTVal = qcColGT(wpColCTVal, wpColGPVal);
                        wpColGUVal = qcColGU(wpColDEVal, wpColGSVal);
                        wpColGVVal = qcColGV(wpColGTVal);
                        wpColGWVal = qcColGW(ifrVal);
                        wpColGXVal = qcColGX(wpColGVVal, wpColGWVal);
                        wpColGYVal = qcColGY(wpColGUVal);
                        wpColHAVal = qcColHA(ifrVal, wpColBDVal, wpColCSVal, wpColFVVal, wpColFTVal, wpColFUVal);
                        taID = getTAID(wpColHAVal, wpColHBVal, wpColCIVal, wpColCCVal, wpColBDVal, wpColDFVal, wpColFZVal, wpColDGVal);

                        iInsertDS = insWPQC(WellPadID, srRowID, RunID, BHAID, dsDepth, calcGxVal, calcGyVal, calcGzVal, calcBxVal, calcByVal, calcBzVal, wpColPVal, wpColQVal, wpColSVal, wpColTVal, wpColVVal, wpColWVal, wpColYVal, wpColZVal, wpColAAVal, wpColABVal, wpColACVal, wpColADVal, wpColAEVal, wpColAFVal, wpColAHVal, wpColAIVal, wpColAJVal, wpColAKVal, wpColALVal, wpColAMVal, wpColANVal, wpColAOVal, wpColAPVal, wpColARVal, wpColASVal, wpColATVal, wpColAUVal, wpColAVVal, wpColAWVal, wpColAXVal, wpColAYVal, wpColBAVal, wpColBBVal, wpColBCVal, wpColBDVal, wpColBFVal, wpColBGVal, wpColBHVal, wpColBIVal, wpColBJVal, wpColBKVal, wpColBMVal, wpColBNVal, wpColBOVal, wpColBPVal, wpColBQVal, wpColBRVal, wpColBSVal, wpColBTVal, wpColBUVal, wpColBVVal, wpColBWVal, wpColBXVal, wpColBZVal, wpColCAVal, wpColCBVal, wpColCCVal, wpColCDVal, wpColCEVal, wpColCFVal, wpColCGVal, wpColCHVal, wpColCIVal, wpColCKVal, wpColCLVal, wpColCMVal, wpColCNVal, wpColCOVal, wpColCPVal, wpColCQVal, wpColCRVal, wpColCSVal, wpColCTVal, wpColCUVal, wpColCVVal, wpColCWVal, wpColCXVal, wpColCYVal, wpColCZVal, wpColDAVal, wpColDBVal, wpColDCVal, wpColDDVal, wpColDEVal, wpColDFVal, wpColDGVal, wpColDHVal, wpColDIVal, wpColDJVal, wpColDKVal, wpColDXVal, wpColELVal, wpColEMVal, wpColENVal, wpColFQVal, wpColFRVal, wpColFSVal, wpColFTVal, wpColFUVal, wpColFVVal, wpColFWVal, wpColFXVal, wpColFYVal, wpColFZVal, wpColGBVal, wpColGCVal, wpColGDVal, wpColGEVal, wpColGFVal, wpColGGVal, wpColGHVal, wpColGIVal, wpColGJVal, wpColGLVal, wpColGMVal, wpColGNVal, wpColGOVal, wpColGPVal, wpColGQVal, wpColGRVal, wpColGSVal, wpColGTVal, wpColGUVal, wpColGVVal, wpColGWVal, wpColGXVal, wpColGYVal, wpColHAVal, wpColHBVal, taID, dbConn);
                        if (iInsertDS.Equals(1))
                        {
                            iAccountDS = ACCTNG.accWP(UserName, ClntRealm, WellPadID, RunID, BHAID, srRowID, dsDepth);
                            if (iAccountDS.Equals(1))
                            {
                                iResult = iAccountDS;
                            }
                            else
                            {
                                //Couldn't account for QC of dataset row 
                                iResult = -4;
                            }
                        }
                        else
                        {
                            //Couldn't insert dataset qc row
                            iResult = -3;
                        }
                    }
                    else
                    {
                        //Couldn't account for generating BGs
                        iResult = -2;
                    }
                }
                else
                {
                    //Couldn't insert BGs so unable to account for it either
                    iResult = -1;
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 getTAID(Int32 wpColHAVal, Int32 wpColHBVal, Int32 wpColCCVal, Int32 wpColCIVal, Int32 wpColBDVal, Int32 wpColDFVal, Int32 wpColFZVal, Int32 wpColDGVal)
        {
            try
            {
                Int32 iResult = -99;

                String getTA = "SELECT taID FROM dmgQCTrendAnalysis WHERE tarmID = @tarmID AND taiID = @taiID AND tagtID = @tagtID AND tabtID = @tabtID AND tadipID = @tadipID AND tascazID = @tascazID AND tanmspID = @tanmspID AND tabzID = @tabzID";

                using (SqlConnection clntConn = new SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["FEDemogDB"].ConnectionString))
                {
                    try
                    {
                        clntConn.Open();
                        SqlCommand taStat = new SqlCommand(getTA, clntConn);

                        taStat.Parameters.AddWithValue("@tarmID", SqlDbType.Int).Value = wpColHAVal.ToString();
                        taStat.Parameters.AddWithValue("@taiID", SqlDbType.Int).Value = wpColHBVal.ToString();
                        taStat.Parameters.AddWithValue("@tagtID", SqlDbType.Int).Value = wpColCIVal.ToString();
                        taStat.Parameters.AddWithValue("@tabtID", SqlDbType.Int).Value = wpColCCVal.ToString();
                        taStat.Parameters.AddWithValue("@tadipID", SqlDbType.Int).Value = wpColBDVal.ToString();
                        taStat.Parameters.AddWithValue("@tascazID", SqlDbType.Int).Value = wpColDFVal.ToString();
                        taStat.Parameters.AddWithValue("@tanmspID", SqlDbType.Int).Value = wpColFZVal.ToString();
                        taStat.Parameters.AddWithValue("@tabzID", SqlDbType.Int).Value = wpColDGVal.ToString();

                        using (SqlDataReader readTA = taStat.ExecuteReader())
                        {
                            try
                            {
                                if (readTA.HasRows)
                                {
                                    while (readTA.Read())
                                    {
                                        iResult = readTA.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readTA.Close();
                                readTA.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }

                return iResult;
            }
            catch(Exception ex)
            {throw new System.Exception(ex.ToString());}
        }

        //Method to Insert generated WP BGs into WellProifle table
        private static Int32 insWPBG( Int32 wpdsID, Decimal dsDepth, Decimal wpGx, Decimal wpGy, Decimal wpGz, Decimal wpBx, Decimal wpBy, Decimal wpBz, Int32 RunID, Int32 BHAID, Int32 srvyRowID, String dbConn)
        {
            try
            {
                Int32 iResult = -99;
                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;

                String insertWPData = "INSERT INTO [WellProfile] (wpdsID, wpDepth, wpGx, wpGy, wpGz, wpBx, wpBy, wpBz, runID, srvyRowID, cTime, uTime, bhaID) VALUES (@wpdsID, @wpDepth, @wpGx, @wpGy, @wpGz, @wpBx, @wpBy, @wpBz, @RunID, @srvyRowID, @cTime, @uTime, @bhaID)";

                using (SqlConnection insBGCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        insBGCon.Open();

                        SqlCommand insBGs = new SqlCommand(insertWPData, insBGCon);
                        insBGs.Parameters.AddWithValue("@wpdsID", SqlDbType.Int).Value = wpdsID.ToString();
                        insBGs.Parameters.AddWithValue("@wpDepth", SqlDbType.Decimal).Value = dsDepth.ToString();
                        insBGs.Parameters.AddWithValue("@wpGx", SqlDbType.Decimal).Value = wpGx.ToString();
                        insBGs.Parameters.AddWithValue("@wpGy", SqlDbType.Decimal).Value = wpGy.ToString();
                        insBGs.Parameters.AddWithValue("@wpGz", SqlDbType.Decimal).Value = wpGz.ToString();
                        insBGs.Parameters.AddWithValue("@wpBx", SqlDbType.Decimal).Value = wpBx.ToString();
                        insBGs.Parameters.AddWithValue("@wpBy", SqlDbType.Decimal).Value = wpBy.ToString();
                        insBGs.Parameters.AddWithValue("@wpBz", SqlDbType.Decimal).Value = wpBz.ToString();
                        insBGs.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        insBGs.Parameters.AddWithValue("@srvyRowID", SqlDbType.Int).Value = srvyRowID.ToString();
                        insBGs.Parameters.AddWithValue("@cTime", SqlDbType.DateTime).Value = crTime.ToString();
                        insBGs.Parameters.AddWithValue("@uTime", SqlDbType.DateTime).Value = updTime.ToString();
                        insBGs.Parameters.AddWithValue("@bhaID", SqlDbType.Int).Value = BHAID.ToString();

                        iResult = Convert.ToInt32(insBGs.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        insBGCon.Close();
                        insBGCon.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Method to Insert processed WP QC Row into WPQCResults table
        private static Int32 insWPQC(Int32 wpdsID, Int32 srvyRowID, Int32 RunID, Int32 BHAID, Decimal dsDepth, Decimal Gx, Decimal Gy, Decimal Gz, Decimal Bx, Decimal By, Decimal Bz, Decimal wpColP, Decimal wpColQ, Decimal wpColS, Decimal wpColT, Int32 wpColV, Decimal wpColW, Decimal wpColY, Decimal wpColZ, Decimal wpColAA, Decimal wpColAB, Decimal wpColAC, Decimal wpColAD, Decimal wpColAE, Decimal wpColAF, Decimal wpColAH, Decimal wpColAI, Decimal wpColAJ, Decimal wpColAK, Decimal wpColAL, Decimal wpColAM, Decimal wpColAN, Decimal wpColAO, Decimal wpColAP, Decimal wpColAR, Decimal wpColAS, Decimal wpColAT, Decimal wpColAU, Int32 wpColAV, Int32 wpColAW, Int32 wpColAX, Int32 wpColAY, Decimal wpColBA, Decimal wpColBB, Decimal wpColBC, Int32 wpColBD, Decimal wpColBF, Decimal wpColBG, Decimal wpColBH, Decimal wpColBI, Decimal wpColBJ, Decimal wpColBK, Decimal wpColBM, Decimal wpColBN, Decimal wpColBO, Decimal wpColBP, Decimal wpColBQ, Decimal wpColBR, Decimal wpColBS, Decimal wpColBT, Decimal wpColBU, Decimal wpColBV, Decimal wpColBW, Decimal wpColBX, Decimal wpColBZ, Decimal wpColCA, Decimal wpColCB, Int32 wpColCC, Decimal wpColCD, Decimal wpColCE, Decimal wpColCF, Decimal wpColCG, Decimal wpColCH, Int32 wpColCI, Decimal wpColCK, Decimal wpColCL, Decimal wpColCM, Decimal wpColCN, Decimal wpColCO, Decimal wpColCP, Decimal wpColCQ, Decimal wpColCR, Decimal wpColCS, Decimal wpColCT, Decimal wpColCU, Decimal wpColCV, Decimal wpColCW, Decimal wpColCX, Decimal wpColCY, Decimal wpColCZ, Decimal wpColDA, Decimal wpColDB, Decimal wpColDC, Decimal wpColDD, Decimal wpColDE, Int32 wpColDF, Int32 wpColDG, Int32 wpColDH, Decimal wpColDI, Int32 wpColDJ, Decimal wpColDK, Decimal wpColDX, Decimal wpColEL, Decimal wpColEM, Int32 wpColEN, Int32 wpColFQ, Int32 wpColFR, Decimal wpColFS, Decimal wpColFT, Decimal wpColFU, Decimal wpColFV, Decimal wpColFW, Decimal wpColFX, Decimal wpColFY, Int32 wpColFZ, Decimal wpColGB, Decimal wpColGC, Decimal wpColGD, Decimal wpColGE, Int32 wpColGF, Decimal wpColGG, Decimal wpColGH, Decimal wpColGI, Decimal wpColGJ, Decimal wpColGL, Decimal wpColGM, Decimal wpColGN, Decimal wpColGO, Decimal wpColGP, Decimal wpColGQ, Decimal wpColGR, Decimal wpColGS, Decimal wpColGT, Decimal wpColGU, Int32 wpColGV, Int32 wpColGW, Int32 wpColGX, Int32 wpColGY, Int32 wpColHA, Int32 wpColHB, Int32 taID, String dbConn )
        {
            try
            {
                Int32 iResult = -99;
                DateTime crTime = DateTime.Now;

                String insertQCResult = "INSERT INTO [WPQCResults] (wpdsID, srvyRowID, runID, depth, wpGx, wpGy, wpGz, wpBx, wpBy, wpBz, ColP, ColQ, ColS, ColT, ColV, ColW, ColY, ColZ, ColAA, ColAB, ColAC, ColAD, ColAE, ColAF, ColAH, ColAI, ColAJ, ColAK, ColAL, ColAM, ColAN, ColAO, ColAP, ColAR, ColAS, ColAT, ColAU, ColAV, ColAW, ColAX, ColAY, ColBA, ColBB, ColBC, ColBD, ColBF, ColBG,  ColBH,  ColBI,  ColBJ,  ColBK,  ColBM,  ColBN,  ColBO,  ColBP,  ColBQ,  ColBR,  ColBS,  ColBT,  ColBU,  ColBV,  ColBW,  ColBX,  ColBZ,  ColCA,  ColCB,  ColCC,  ColCD,  ColCE,  ColCF,  ColCG,  ColCH,  ColCI,  ColCK,  ColCL,  ColCM,  ColCN,  ColCO,  ColCP,  ColCQ,  ColCR,  ColCS,  ColCT,  ColCU,  ColCV,  ColCW,  ColCX,  ColCY,  ColCZ,  ColDA,  ColDB,  ColDC,  ColDD,  ColDE,  ColDF,  ColDG,  ColDH,  ColDI,  ColDJ,  ColDK,  ColDX,  ColEL,  ColEM,  ColEN,  ColFQ,  ColFR,  ColFS,  ColFT,  ColFU,  ColFV,  ColFW,  ColFX,  ColFY,  ColFZ,  ColGB,  ColGC,  ColGD,  ColGE,  ColGF,  ColGG,  ColGH,  ColGI,  ColGJ, ColGL, ColGM, ColGN, ColGO, ColGP, ColGQ, ColGR, ColGS, ColGT, ColGU, ColGV, ColGW, ColGX, ColGY, ColHA, ColHB, taID, pTime, bhaID) VALUES (@wpdsID, @srvyRowID, @runID, @dsDepth, @wpGx, @wpGy, @wpGz, @wpBx, @wpBy, @wpBz, @wpColP, @wpColQ, @wpColS, @wpColT, @wpColV, @wpColW, @wpColY, @wpColZ, @wpColAA, @wpColAB, @wpColAC, @wpColAD, @wpColAE, @wpColAF, @wpColAH, @wpColAI, @wpColAJ, @wpColAK, @wpColAL, @wpColAM, @wpColAN, @wpColAO, @wpColAP, @wpColAR, @wpColAS, @wpColAT, @wpColAU, @wpColAV, @wpColAW, @wpColAX, @wpColAY, @wpColBA, @wpColBB, @wpColBC, @wpColBD, @wpColBF, @wpColBG, @wpColBH, @wpColBI, @wpColBJ, @wpColBK, @wpColBM, @wpColBN, @wpColBO, @wpColBP, @wpColBQ, @wpColBR, @wpColBS, @wpColBT, @wpColBU, @wpColBV, @wpColBW, @wpColBX, @wpColBZ, @wpColCA, @wpColCB, @wpColCC, @wpColCD, @wpColCE, @wpColCF, @wpColCG, @wpColCH, @wpColCI, @wpColCK, @wpColCL, @wpColCM, @wpColCN, @wpColCO, @wpColCP, @wpColCQ, @wpColCR, @wpColCS, @wpColCT, @wpColCU, @wpColCV, @wpColCW, @wpColCX, @wpColCY, @wpColCZ, @wpColDA, @wpColDB, @wpColDC, @wpColDD, @wpColDE, @wpColDF, @wpColDG, @wpColDH, @wpColDI, @wpColDJ, @wpColDK, @wpColDX, @wpColEL, @wpColEM, @wpColEN, @wpColFQ, @wpColFR, @wpColFS, @wpColFT, @wpColFU, @wpColFV, @wpColFW, @wpColFX, @wpColFY, @wpColFZ, @wpColGB, @wpColGC, @wpColGD, @wpColGE, @wpColGF, @wpColGG, @wpColGH, @wpColGI, @wpColGJ, @wpColGL, @wpColGM, @wpColGN, @wpColGO, @wpColGP, @wpColGQ, @wpColGR, @wpColGS, @wpColGT, @wpColGU, @wpColGV, @wpColGW, @wpColGX, @wpColGY, @wpColHA, @wpColHB, @taID, @pTime, @bhaID)";

                using (SqlConnection clntConn = new SqlConnection(dbConn))
                {
                    try
                    {
                        clntConn.Open();

                        SqlCommand insRes = new SqlCommand(insertQCResult, clntConn);
                        insRes.Parameters.AddWithValue("@wpdsID", SqlDbType.Int).Value = wpdsID.ToString();
                        insRes.Parameters.AddWithValue("@srvyRowID", SqlDbType.Int).Value = srvyRowID.ToString();
                        insRes.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        insRes.Parameters.AddWithValue("@dsDepth", SqlDbType.Decimal).Value = dsDepth.ToString();
                        insRes.Parameters.AddWithValue("@wpGx", SqlDbType.Decimal).Value = Gx.ToString();
                        insRes.Parameters.AddWithValue("@wpGy", SqlDbType.Decimal).Value = Gy.ToString();
                        insRes.Parameters.AddWithValue("@wpGz", SqlDbType.Decimal).Value = Gz.ToString();
                        insRes.Parameters.AddWithValue("@wpBx", SqlDbType.Decimal).Value = Bx.ToString();
                        insRes.Parameters.AddWithValue("@wpBy", SqlDbType.Decimal).Value = By.ToString();
                        insRes.Parameters.AddWithValue("@wpBz", SqlDbType.Decimal).Value = Bz.ToString();
                        insRes.Parameters.AddWithValue("@wpColP", SqlDbType.Decimal).Value = wpColP.ToString();
                        insRes.Parameters.AddWithValue("@wpColQ", SqlDbType.Decimal).Value = wpColQ.ToString();
                        insRes.Parameters.AddWithValue("@wpColS", SqlDbType.Decimal).Value = wpColS.ToString();
                        insRes.Parameters.AddWithValue("@wpColT", SqlDbType.Decimal).Value = wpColT.ToString();
                        insRes.Parameters.AddWithValue("@wpColV", SqlDbType.Int).Value = wpColV.ToString();
                        insRes.Parameters.AddWithValue("@wpColW", SqlDbType.Decimal).Value = wpColW.ToString();
                        insRes.Parameters.AddWithValue("@wpColY", SqlDbType.Decimal).Value = wpColY.ToString();
                        insRes.Parameters.AddWithValue("@wpColZ", SqlDbType.Decimal).Value = wpColZ.ToString();
                        insRes.Parameters.AddWithValue("@wpColAA", SqlDbType.Decimal).Value = wpColAA.ToString();
                        insRes.Parameters.AddWithValue("@wpColAB", SqlDbType.Decimal).Value = wpColAB.ToString();
                        insRes.Parameters.AddWithValue("@wpColAC", SqlDbType.Decimal).Value = wpColAC.ToString();
                        insRes.Parameters.AddWithValue("@wpColAD", SqlDbType.Decimal).Value = wpColAD.ToString();
                        insRes.Parameters.AddWithValue("@wpColAE", SqlDbType.Decimal).Value = wpColAE.ToString();
                        insRes.Parameters.AddWithValue("@wpColAF", SqlDbType.Decimal).Value = wpColAF.ToString();
                        insRes.Parameters.AddWithValue("@wpColAH", SqlDbType.Decimal).Value = wpColAH.ToString();
                        insRes.Parameters.AddWithValue("@wpColAI", SqlDbType.Decimal).Value = wpColAI.ToString();
                        insRes.Parameters.AddWithValue("@wpColAJ", SqlDbType.Decimal).Value = wpColAJ.ToString();
                        insRes.Parameters.AddWithValue("@wpColAK", SqlDbType.Decimal).Value = wpColAK.ToString();
                        insRes.Parameters.AddWithValue("@wpColAL", SqlDbType.Decimal).Value = wpColAL.ToString();
                        insRes.Parameters.AddWithValue("@wpColAM", SqlDbType.Decimal).Value = wpColAM.ToString();
                        insRes.Parameters.AddWithValue("@wpColAN", SqlDbType.Decimal).Value = wpColAN.ToString();
                        insRes.Parameters.AddWithValue("@wpColAO", SqlDbType.Decimal).Value = wpColAO.ToString();
                        insRes.Parameters.AddWithValue("@wpColAP", SqlDbType.Decimal).Value = wpColAP.ToString();
                        insRes.Parameters.AddWithValue("@wpColAR", SqlDbType.Decimal).Value = wpColAR.ToString();
                        insRes.Parameters.AddWithValue("@wpColAS", SqlDbType.Decimal).Value = wpColAS.ToString();
                        insRes.Parameters.AddWithValue("@wpColAT", SqlDbType.Decimal).Value = wpColAT.ToString();
                        insRes.Parameters.AddWithValue("@wpColAU", SqlDbType.Decimal).Value = wpColAU.ToString();
                        insRes.Parameters.AddWithValue("@wpColAV", SqlDbType.Int).Value = wpColAV.ToString();
                        insRes.Parameters.AddWithValue("@wpColAW", SqlDbType.Int).Value = wpColAW.ToString();
                        insRes.Parameters.AddWithValue("@wpColAX", SqlDbType.Int).Value = wpColAX.ToString();
                        insRes.Parameters.AddWithValue("@wpColAY", SqlDbType.Int).Value = wpColAY.ToString();
                        insRes.Parameters.AddWithValue("@wpColBA", SqlDbType.Decimal).Value = wpColBA.ToString();
                        insRes.Parameters.AddWithValue("@wpColBB", SqlDbType.Decimal).Value = wpColBB.ToString();
                        insRes.Parameters.AddWithValue("@wpColBC", SqlDbType.Decimal).Value = wpColBC.ToString();
                        insRes.Parameters.AddWithValue("@wpColBD", SqlDbType.Int).Value = wpColBD.ToString();
                        insRes.Parameters.AddWithValue("@wpColBF", SqlDbType.Decimal).Value = wpColBF.ToString();
                        insRes.Parameters.AddWithValue("@wpColBG", SqlDbType.Decimal).Value = wpColBG.ToString();
                        insRes.Parameters.AddWithValue("@wpColBH", SqlDbType.Decimal).Value = wpColBH.ToString();
                        insRes.Parameters.AddWithValue("@wpColBI", SqlDbType.Decimal).Value = wpColBI.ToString();
                        insRes.Parameters.AddWithValue("@wpColBJ", SqlDbType.Decimal).Value = wpColBJ.ToString();
                        insRes.Parameters.AddWithValue("@wpColBK", SqlDbType.Decimal).Value = wpColBK.ToString();
                        insRes.Parameters.AddWithValue("@wpColBM", SqlDbType.Decimal).Value = wpColBM.ToString();
                        insRes.Parameters.AddWithValue("@wpColBN", SqlDbType.Decimal).Value = wpColBN.ToString();
                        insRes.Parameters.AddWithValue("@wpColBO", SqlDbType.Decimal).Value = wpColBO.ToString();
                        insRes.Parameters.AddWithValue("@wpColBP", SqlDbType.Decimal).Value = wpColBP.ToString();
                        insRes.Parameters.AddWithValue("@wpColBQ", SqlDbType.Decimal).Value = wpColBQ.ToString();
                        insRes.Parameters.AddWithValue("@wpColBR", SqlDbType.Decimal).Value = wpColBR.ToString();
                        insRes.Parameters.AddWithValue("@wpColBS", SqlDbType.Decimal).Value = wpColBS.ToString();
                        insRes.Parameters.AddWithValue("@wpColBT", SqlDbType.Decimal).Value = wpColBT.ToString();
                        insRes.Parameters.AddWithValue("@wpColBU", SqlDbType.Decimal).Value = wpColBU.ToString();
                        insRes.Parameters.AddWithValue("@wpColBV", SqlDbType.Decimal).Value = wpColBV.ToString();
                        insRes.Parameters.AddWithValue("@wpColBW", SqlDbType.Decimal).Value = wpColBW.ToString();
                        insRes.Parameters.AddWithValue("@wpColBX", SqlDbType.Decimal).Value = wpColBX.ToString();
                        insRes.Parameters.AddWithValue("@wpColBZ", SqlDbType.Decimal).Value = wpColBZ.ToString();
                        insRes.Parameters.AddWithValue("@wpColCA", SqlDbType.Decimal).Value = wpColCA.ToString();
                        insRes.Parameters.AddWithValue("@wpColCB", SqlDbType.Decimal).Value = wpColCB.ToString();
                        insRes.Parameters.AddWithValue("@wpColCC", SqlDbType.Int).Value = wpColCC.ToString();
                        insRes.Parameters.AddWithValue("@wpColCD", SqlDbType.Decimal).Value = wpColCD.ToString();
                        insRes.Parameters.AddWithValue("@wpColCE", SqlDbType.Decimal).Value = wpColCE.ToString();
                        insRes.Parameters.AddWithValue("@wpColCF", SqlDbType.Decimal).Value = wpColCF.ToString();
                        insRes.Parameters.AddWithValue("@wpColCG", SqlDbType.Decimal).Value = wpColCG.ToString();
                        insRes.Parameters.AddWithValue("@wpColCH", SqlDbType.Decimal).Value = wpColCH.ToString();
                        insRes.Parameters.AddWithValue("@wpColCI", SqlDbType.Int).Value = wpColCI.ToString();
                        insRes.Parameters.AddWithValue("@wpColCK", SqlDbType.Decimal).Value = wpColCK.ToString();
                        insRes.Parameters.AddWithValue("@wpColCL", SqlDbType.Decimal).Value = wpColCL.ToString();
                        insRes.Parameters.AddWithValue("@wpColCM", SqlDbType.Decimal).Value = wpColCM.ToString();
                        insRes.Parameters.AddWithValue("@wpColCN", SqlDbType.Decimal).Value = wpColCN.ToString();
                        insRes.Parameters.AddWithValue("@wpColCO", SqlDbType.Decimal).Value = wpColCO.ToString();
                        insRes.Parameters.AddWithValue("@wpColCP", SqlDbType.Decimal).Value = wpColCP.ToString();
                        insRes.Parameters.AddWithValue("@wpColCQ", SqlDbType.Decimal).Value = wpColCQ.ToString();
                        insRes.Parameters.AddWithValue("@wpColCR", SqlDbType.Decimal).Value = wpColCR.ToString();
                        insRes.Parameters.AddWithValue("@wpColCS", SqlDbType.Decimal).Value = wpColCS.ToString();
                        insRes.Parameters.AddWithValue("@wpColCT", SqlDbType.Decimal).Value = wpColCT.ToString();
                        insRes.Parameters.AddWithValue("@wpColCU", SqlDbType.Decimal).Value = wpColCU.ToString();
                        insRes.Parameters.AddWithValue("@wpColCV", SqlDbType.Decimal).Value = wpColCV.ToString();
                        insRes.Parameters.AddWithValue("@wpColCW", SqlDbType.Decimal).Value = wpColCW.ToString();
                        insRes.Parameters.AddWithValue("@wpColCX", SqlDbType.Decimal).Value = wpColCX.ToString();
                        insRes.Parameters.AddWithValue("@wpColCY", SqlDbType.Decimal).Value = wpColCY.ToString();
                        insRes.Parameters.AddWithValue("@wpColCZ", SqlDbType.Decimal).Value = wpColCZ.ToString();
                        insRes.Parameters.AddWithValue("@wpColDA", SqlDbType.Decimal).Value = wpColDA.ToString();
                        insRes.Parameters.AddWithValue("@wpColDB", SqlDbType.Decimal).Value = wpColDB.ToString();
                        insRes.Parameters.AddWithValue("@wpColDC", SqlDbType.Decimal).Value = wpColDC.ToString();
                        insRes.Parameters.AddWithValue("@wpColDD", SqlDbType.Decimal).Value = wpColDD.ToString();
                        insRes.Parameters.AddWithValue("@wpColDE", SqlDbType.Decimal).Value = wpColDE.ToString();
                        insRes.Parameters.AddWithValue("@wpColDF", SqlDbType.Int).Value = wpColDF.ToString();
                        insRes.Parameters.AddWithValue("@wpColDG", SqlDbType.Int).Value = wpColDG.ToString();
                        insRes.Parameters.AddWithValue("@wpColDH", SqlDbType.Int).Value = wpColDH.ToString();
                        insRes.Parameters.AddWithValue("@wpColDI", SqlDbType.Decimal).Value = wpColDI.ToString();
                        insRes.Parameters.AddWithValue("@wpColDJ", SqlDbType.Int).Value = wpColDJ.ToString();
                        insRes.Parameters.AddWithValue("@wpColDK", SqlDbType.Decimal).Value = wpColDK.ToString();
                        insRes.Parameters.AddWithValue("@wpColDX", SqlDbType.Decimal).Value = wpColDX.ToString();
                        insRes.Parameters.AddWithValue("@wpColEL", SqlDbType.Decimal).Value = wpColEL.ToString();
                        insRes.Parameters.AddWithValue("@wpColEM", SqlDbType.Decimal).Value = wpColEM.ToString();
                        insRes.Parameters.AddWithValue("@wpColEN", SqlDbType.Int).Value = wpColEN.ToString();
                        insRes.Parameters.AddWithValue("@wpColFQ", SqlDbType.Int).Value = wpColFQ.ToString();
                        insRes.Parameters.AddWithValue("@wpColFR", SqlDbType.Int).Value = wpColFR.ToString();
                        insRes.Parameters.AddWithValue("@wpColFS", SqlDbType.Decimal).Value = wpColFS.ToString();
                        insRes.Parameters.AddWithValue("@wpColFT", SqlDbType.Decimal).Value = wpColFT.ToString();
                        insRes.Parameters.AddWithValue("@wpColFU", SqlDbType.Decimal).Value = wpColFU.ToString();
                        insRes.Parameters.AddWithValue("@wpColFV", SqlDbType.Decimal).Value = wpColFV.ToString();
                        insRes.Parameters.AddWithValue("@wpColFW", SqlDbType.Decimal).Value = wpColFW.ToString();
                        insRes.Parameters.AddWithValue("@wpColFX", SqlDbType.Decimal).Value = wpColFX.ToString();
                        insRes.Parameters.AddWithValue("@wpColFY", SqlDbType.Decimal).Value = wpColFY.ToString();
                        insRes.Parameters.AddWithValue("@wpColFZ", SqlDbType.Int).Value = wpColFZ.ToString();
                        insRes.Parameters.AddWithValue("@wpColGB", SqlDbType.Decimal).Value = wpColGB.ToString();
                        insRes.Parameters.AddWithValue("@wpColGC", SqlDbType.Decimal).Value = wpColGC.ToString();
                        insRes.Parameters.AddWithValue("@wpColGD", SqlDbType.Decimal).Value = wpColGD.ToString();
                        insRes.Parameters.AddWithValue("@wpColGE", SqlDbType.Decimal).Value = wpColGE.ToString();
                        insRes.Parameters.AddWithValue("@wpColGF", SqlDbType.Int).Value = wpColGF.ToString();
                        insRes.Parameters.AddWithValue("@wpColGG", SqlDbType.Decimal).Value = wpColGG.ToString();
                        insRes.Parameters.AddWithValue("@wpColGH", SqlDbType.Decimal).Value = wpColGH.ToString();
                        insRes.Parameters.AddWithValue("@wpColGI", SqlDbType.Decimal).Value = wpColGI.ToString();
                        insRes.Parameters.AddWithValue("@wpColGJ", SqlDbType.Decimal).Value = wpColGJ.ToString();
                        insRes.Parameters.AddWithValue("@wpColGL", SqlDbType.Decimal).Value = wpColGL.ToString();
                        insRes.Parameters.AddWithValue("@wpColGM", SqlDbType.Decimal).Value = wpColGM.ToString();
                        insRes.Parameters.AddWithValue("@wpColGN", SqlDbType.Decimal).Value = wpColGN.ToString();
                        insRes.Parameters.AddWithValue("@wpColGO", SqlDbType.Decimal).Value = wpColGO.ToString();
                        insRes.Parameters.AddWithValue("@wpColGP", SqlDbType.Decimal).Value = wpColGP.ToString();
                        insRes.Parameters.AddWithValue("@wpColGQ", SqlDbType.Decimal).Value = wpColGQ.ToString();
                        insRes.Parameters.AddWithValue("@wpColGR", SqlDbType.Decimal).Value = wpColGR.ToString();
                        insRes.Parameters.AddWithValue("@wpColGS", SqlDbType.Decimal).Value = wpColGS.ToString();
                        insRes.Parameters.AddWithValue("@wpColGT", SqlDbType.Decimal).Value = wpColGT.ToString();
                        insRes.Parameters.AddWithValue("@wpColGU", SqlDbType.Decimal).Value = wpColGU.ToString();
                        insRes.Parameters.AddWithValue("@wpColGV", SqlDbType.Int).Value = wpColGV.ToString();
                        insRes.Parameters.AddWithValue("@wpColGW", SqlDbType.Int).Value = wpColGW.ToString();
                        insRes.Parameters.AddWithValue("@wpColGX", SqlDbType.Int).Value = wpColGX.ToString();
                        insRes.Parameters.AddWithValue("@wpColGY", SqlDbType.Int).Value = wpColGY.ToString();
                        insRes.Parameters.AddWithValue("@wpColHA", SqlDbType.Int).Value = wpColHA.ToString();
                        insRes.Parameters.AddWithValue("@wpColHB", SqlDbType.Int).Value = wpColHB.ToString();
                        insRes.Parameters.AddWithValue("@taID", SqlDbType.Int).Value = taID.ToString();
                        insRes.Parameters.AddWithValue("@pTime", SqlDbType.DateTime).Value = crTime.ToString();
                        insRes.Parameters.AddWithValue("@bhaID", SqlDbType.Int).Value = BHAID.ToString();
                        
                        iResult = Convert.ToInt32(insRes.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}