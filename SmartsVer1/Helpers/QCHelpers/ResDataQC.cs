﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Web;
using System.Web.SessionState;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.IO;
using System.Globalization;
using System.Web.Configuration;
using NMS = SmartsVer1.Helpers.QCHelpers.NonMagCalc;
using Units = SmartsVer1.Helpers.DemogHelpers.UnitsConverter;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using OPR = SmartsVer1.Helpers.QCHelpers.Opr;
using RawQC = SmartsVer1.QCHelpers.sdRawDataQC;

namespace SmartsVer1.Helpers.QCHelpers
{
    public class ResDataQC
    {
        ArrayList Headlist = new ArrayList();
        ArrayList DetailList = new ArrayList();
        String[] det, headerTOP;
        Dictionary<String, String> HeaderInfo = new Dictionary<String, String>();
        Dictionary<String, Int32> HeaderGB = new Dictionary<String, Int32>();

        static void Main()
        { }

        public static Double getSlope(Int32 nCount, Double sumX, Double sumY, Double sumXY, Double sumXSquare)
        {
            try
            {
                return ((nCount * sumXY) - (sumX * sumY)) / ((nCount * sumXSquare) - (sumX * sumX));
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Double getIntercept(Int32 nCount, Double slope, Double sumX, Double sumY)
        {
            try
            {
                return (sumY - (slope * sumX)) / nCount;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal resColS(Int32 jIFR, Decimal colAEValue, Decimal colAFValue)
        {
            try
            {
                Decimal iReply = -99.00000000M, testValue = -99.00000000M;
                if(jIFR.Equals(1))
                {
	                testValue = colAEValue;
                }
                else
                {
	                testValue = colAFValue;

                }

                if (testValue > 359.99M)
                {
                    iReply = testValue - 360.00M;
                }
                else if (testValue < 0.00M)
                {
                    iReply = testValue + 360.00M;
                }
                else
                {
                    iReply = testValue;
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal resColT(Int32 jIFR, Decimal colAGValue, Decimal colAHValue)
        {
            try
            {
                Decimal iReply = -99.00000000M, testValue = -99.00000000M;
                if (jIFR.Equals(1))
                {
                    testValue = colAGValue;
                }
                else
                {
                    testValue = colAHValue;

                }

                if (testValue > 359.99M)
                {
                    iReply = testValue - 360.00M;
                }
                else if (testValue < 0.00M)
                {
                    iReply = testValue + 360.00M;
                }
                else
                {
                    iReply = testValue;
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal resColU( Int32 JConverg, Decimal resColS, Decimal resColT )
        {
            try
            {
                if (JConverg.Equals(2)) //Grid
                {
                    return resColS;
                }
                else
                {
                    return resColT;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal resColAE(Decimal GridAzimuth, Decimal MagneticDeclination,Decimal GridConvergence, Int32 LatitudeNS, Int32 MDecCID, Int32 gcoID)
        {
            try
            {
                Decimal iReply = -99.000000M, testValue = -99.000000M;
                if (LatitudeNS.Equals(1) && MDecCID.Equals(9) && gcoID.Equals(1))
                {
                    testValue = GridAzimuth - MagneticDeclination + GridConvergence;
                }
                else if (LatitudeNS.Equals(1) && MDecCID.Equals(9) && gcoID.Equals(2))
                {
                    testValue = GridAzimuth - MagneticDeclination - GridConvergence;
                }
                else if (LatitudeNS.Equals(1) && MDecCID.Equals(25) && gcoID.Equals(1))
                {
                    testValue = GridAzimuth + MagneticDeclination + GridConvergence;
                }
                else if (LatitudeNS.Equals(1) && MDecCID.Equals(25) && gcoID.Equals(2))
                {
                    testValue = GridAzimuth + MagneticDeclination - GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && MDecCID.Equals(9) && gcoID.Equals(1))
                {
                    testValue = GridAzimuth - MagneticDeclination - GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && MDecCID.Equals(9) && gcoID.Equals(2))
                {
                    testValue = GridAzimuth - MagneticDeclination + GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && MDecCID.Equals(25) && gcoID.Equals(1))
                {
                    testValue = GridAzimuth + MagneticDeclination - GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && MDecCID.Equals(25) && gcoID.Equals(2))
                {
                    testValue = GridAzimuth + MagneticDeclination + GridConvergence;
                }
                else
                {
                    testValue = GridAzimuth;
                }

                if (testValue > 359.99M)
                {
                    iReply = testValue - 360.00M;
                }
                else if (testValue < 0.00M)
                {
                    iReply = testValue + 360.00M;
                }
                else
                {
                    iReply = testValue;
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal resColAF(Decimal GridAzimuth, Decimal iMagneticDeclination, Decimal GridConvergence, Int32 LatitudeNS, Int32 iMDecCID, Int32 gcoID)
        {
            try
            {
                Decimal iReply = -99.000000M, testValue = -99.000000M;
                if (LatitudeNS.Equals(1) && iMDecCID.Equals(9) && gcoID.Equals(1))
                {
                    testValue = GridAzimuth - iMagneticDeclination + GridConvergence;
                }
                else if (LatitudeNS.Equals(1) && iMDecCID.Equals(9) && gcoID.Equals(2))
                {
                    testValue = GridAzimuth - iMagneticDeclination - GridConvergence;
                }
                else if (LatitudeNS.Equals(1) && iMDecCID.Equals(25) && gcoID.Equals(1))
                {
                    testValue = GridAzimuth + iMagneticDeclination + GridConvergence;
                }
                else if (LatitudeNS.Equals(1) && iMDecCID.Equals(25) && gcoID.Equals(2))
                {
                    testValue = GridAzimuth + iMagneticDeclination - GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && iMDecCID.Equals(9) && gcoID.Equals(1))
                {
                    testValue = GridAzimuth - iMagneticDeclination - GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && iMDecCID.Equals(9) && gcoID.Equals(2))
                {
                    testValue = GridAzimuth - iMagneticDeclination + GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && iMDecCID.Equals(25) && gcoID.Equals(1))
                {
                    testValue = GridAzimuth + iMagneticDeclination - GridConvergence;
                }
                else if (LatitudeNS.Equals(17) && iMDecCID.Equals(25) && gcoID.Equals(2))
                {
                    testValue = GridAzimuth + iMagneticDeclination + GridConvergence;
                }
                else
                {
                    testValue = GridAzimuth;
                }

                if (testValue > 359.99M)
                {
                    iReply = testValue - 360.00M;
                }
                else if (testValue < 0.00M)
                {
                    iReply = testValue + 360.00M;
                }
                else
                {
                    iReply = testValue;
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal resColAG(Decimal GridAzimuth, Decimal MagneticDeclination, Int32 MDecCID)
        {
            try
            {
                Decimal iReply = -99.0000M, testValue = -99.0000M;
                if (MDecCID.Equals(9))
                {
                    testValue = GridAzimuth - MagneticDeclination;
                }
                else
                {
                    testValue = GridAzimuth + MagneticDeclination;
                }

                if (testValue > 359.99M)
                {
                    iReply = testValue - 360.00M;
                }
                else if (testValue < 0.00M)
                {
                    iReply = testValue + 360.00M;
                }
                else
                {
                    iReply = testValue;
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal resColAH(Decimal GridAzimuth, Decimal iMagneticDeclination, Int32 iMDecCID)
        {
            try
            {
                Decimal iReply = -99.0000M, testValue = -99.0000M;
                if (iMDecCID.Equals(9))
                {
                    testValue = GridAzimuth - iMagneticDeclination;
                }
                else
                {
                    testValue = GridAzimuth + iMagneticDeclination;
                }

                if (testValue > 359.99M)
                {
                    iReply = testValue - 360.00M;
                }
                else if (testValue < 0.00M)
                {
                    iReply = testValue + 360.00M;
                }
                else
                {
                    iReply = testValue;
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal resGx(Decimal GTotal, Decimal Inclination, Decimal GTF)
        {
            try
            {
                return Decimal.Multiply( Decimal.Multiply( Decimal.Multiply(-1, GTotal) , ( (decimal)Math.Sin( Units.ToRadians( (double)Inclination ) ) ) ) , ( (decimal)Math.Cos( Units.ToRadians( (double)GTF ) ) ) );
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal resGy(Decimal GTotal, Decimal Inclination, Decimal GTF)
        {
            try
            {
                return Decimal.Multiply( Decimal.Multiply( GTotal , (decimal)Math.Sin( (double)Units.ToRadians( (double)Inclination ) ) ) , (decimal)Math.Sin( ( (double)Units.ToRadians( (double)GTF ) ) ) );
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal resGz(Decimal GTotal, Decimal Inclination)
        {
            try
            {
                return Decimal.Multiply( GTotal , (decimal)Math.Cos( (double)Units.ToRadians( (double)Inclination ) ) );
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal resBx( Decimal BTotal, Decimal Dip, Decimal Inclination, Decimal GTF, Decimal resColU)
        {
            try
            {
                Decimal val1 = Decimal.Multiply((decimal)Math.Cos(Units.ToRadians((double)Dip)) , Decimal.Multiply( (decimal)Math.Cos(Units.ToRadians((double)resColU)) , Decimal.Multiply( (decimal)Math.Cos(Units.ToRadians((double)Inclination)) , (decimal)Math.Cos(Units.ToRadians((double)GTF))) ));
                Decimal val2 = Decimal.Multiply( (decimal)Math.Sin(Units.ToRadians((double)Dip)) , Decimal.Multiply( (decimal)Math.Sin(Units.ToRadians((double)Inclination))  , (decimal)Math.Cos(Units.ToRadians((double)GTF))) );
                Decimal val3 = Decimal.Multiply( (decimal)Math.Cos(Units.ToRadians((double)Dip)) , Decimal.Multiply( (decimal)Math.Sin(Units.ToRadians((double)resColU)) , (decimal)Math.Sin(Units.ToRadians((double)GTF))));
                return Decimal.Multiply( BTotal , ( Decimal.Subtract( val1 , Decimal.Subtract( val2, val3 ) ) ) );
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal resBy( Decimal BTotal, Decimal Inclination, Decimal Dip, Decimal GTF, Decimal resColU)
        {
            try
            {
                Decimal val1 = Decimal.Multiply( (decimal)Math.Cos( Units.ToRadians( (double)Dip ) ) , Decimal.Multiply( (decimal)Math.Cos( Units.ToRadians( (double)resColU ) ) , Decimal.Multiply( (decimal)Math.Cos( Units.ToRadians( (double)Inclination ) ) , (decimal)Math.Sin( Units.ToRadians( (double)GTF ) ) ) ) );
                Decimal val2 = Decimal.Multiply( (decimal)Math.Cos( Units.ToRadians( (double)Dip ) ) , ( Decimal.Multiply( (decimal)Math.Sin( Units.ToRadians( (double)resColU ) ) , (decimal)Math.Cos( Units.ToRadians( (double)GTF ) ) ) ) );
                Decimal val3 = Decimal.Multiply( ( (decimal)Math.Sin( Units.ToRadians( (double)Dip ) ) ) , ( Decimal.Multiply( (decimal)Math.Sin( Units.ToRadians( (double)Inclination ) ) , (decimal)Math.Sin( Units.ToRadians( (double)GTF ) ) ) ) );

                return Decimal.Multiply( Decimal.Multiply( -1 , BTotal ) , ( Decimal.Add( val1 , Decimal.Subtract( val2 , val3 ) ) ) );
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal resBz(Decimal BTotal, Decimal Inclination, Decimal Dip, Decimal GTF, Decimal resColU)
        {
            try
            {
                Decimal val1 = Decimal.Multiply( (decimal)Math.Sin( Units.ToRadians( (double)Dip ) ) , (decimal)Math.Cos( Units.ToRadians( (double)Inclination ) ) );
                Decimal val2 = Decimal.Multiply( (decimal)Math.Cos( Units.ToRadians( (double)Dip ) ) , Decimal.Multiply( (decimal)Math.Cos( Units.ToRadians( (double)resColU ) ) , (decimal)Math.Sin( Units.ToRadians( (double)Inclination ) ) ) );
                return Decimal.Multiply( BTotal , Decimal.Add( val1 , val2 ) );
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 StoreResultsData(Int32 OperatorID, Int32 ClientID, String sDate, String sTime, Decimal depth, Decimal Inc, Decimal Azm, Decimal Dip, Decimal BT, Decimal GT, Decimal GTF, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Int32 SurveyID, Int32 dataEntryMethod, String dataFileName, String LoginName, String dbConn, Int32 ServiceTypeID)
        {
            try
            {
                Int32 iResult = -99;
                //Inserting read values in the SurveyResultsData table
                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;
                String insertRawValues = "INSERT INTO [SurveyResultsData] ( [srdDate], [srdTime], [srdDepth], [srdInc], [srdAzm], [srdDip], [srdBT], [srdGT], [srdGTF], [optrID], [clntID], [wpdID], [welID], [wswID], [runID], [srvyRowID], [demID], [fileName], [login], [cTime], [uTime], [srvID] ) VALUES ( @srdDate, @srdTime, @srdDepth, @srdInc, @srdAzm, @srdDip, @srdBT, @srdGT, @srdGTF, @optrID, @clntID, @wpdID, @welID, @wswID, @runID, @srvyRowID, @demID, @fileName, @login, @cTime, @uTime, @srvID)";
                using (SqlConnection Conn = new SqlConnection(dbConn))
                {
                    Conn.Open();

                    SqlCommand insertRawData = new SqlCommand(insertRawValues, Conn);
                    insertRawData.Parameters.AddWithValue("@srdDate", SqlDbType.NVarChar).Value = sDate.ToString();
                    insertRawData.Parameters.AddWithValue("@srdTime", SqlDbType.NVarChar).Value = sTime.ToString();
                    insertRawData.Parameters.AddWithValue("@srdDepth", SqlDbType.Decimal).Value = depth.ToString(); ;
                    insertRawData.Parameters.AddWithValue("@srdInc", SqlDbType.Decimal).Value = Inc.ToString();
                    insertRawData.Parameters.AddWithValue("@srdAzm", SqlDbType.Decimal).Value = Azm.ToString();
                    insertRawData.Parameters.AddWithValue("@srdDip", SqlDbType.Decimal).Value = Dip.ToString();
                    insertRawData.Parameters.AddWithValue("@srdBT", SqlDbType.Decimal).Value = BT.ToString();
                    insertRawData.Parameters.AddWithValue("@srdGT", SqlDbType.Decimal).Value = GT.ToString();
                    insertRawData.Parameters.AddWithValue("@srdGTF", SqlDbType.Decimal).Value = GTF.ToString();
                    insertRawData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                    insertRawData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                    insertRawData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                    insertRawData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                    insertRawData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                    insertRawData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                    insertRawData.Parameters.AddWithValue("@srvyRowID", SqlDbType.Int).Value = SurveyID.ToString();
                    insertRawData.Parameters.AddWithValue("@demID", SqlDbType.Int).Value = dataEntryMethod.ToString();
                    insertRawData.Parameters.AddWithValue("@fileName", SqlDbType.NVarChar).Value = dataFileName.ToString();
                    insertRawData.Parameters.AddWithValue("@login", SqlDbType.NVarChar).Value = LoginName.ToString();
                    insertRawData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = crTime.ToString();
                    insertRawData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = updTime.ToString();
                    insertRawData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceTypeID.ToString();
                    iResult = Convert.ToInt32(insertRawData.ExecuteNonQuery());
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 Store3rdResultsData(String sDate, String sTime, Decimal depth, Decimal Inc, Decimal Azm, Decimal Dip, Decimal BT, Decimal GT, Decimal GTF, Int32 RunID, Int32 WellID, Int32 WellSectionID, Int32 SurveyID, Int32 dataEntryMethod, String dataFileName, String LoginName, String dbConn, Int32 ServiceTypeID, Int32 JobID)
        {
            try
            {
                Int32 iResult = -99;
                //Inserting read values in the SurveyResultsData table
                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;
                String insertRawValues = "INSERT INTO [3rdSurveyResultsData] ( [srdDate], [srdTime], [srdDepth], [srdInc], [srdAzm], [srdDip], [srdBT], [srdGT], [srdGTF], [runID], [welID], [wswID], [srvyRowID], [demID], [fileName], [login], [cTime], [uTime], [srvID], [jobID] ) VALUES ( @srdDate, @srdTime, @srdDepth, @srdInc, @srdAzm, @srdDip, @srdBT, @srdGT, @srdGTF, @runID, @welID, @wswID, @srvyRowID, @demID, @fileName, @login, @cTime, @uTime, @srvID, @jobID )";
                using (SqlConnection Conn = new SqlConnection(dbConn))
                {
                    Conn.Open();

                    SqlCommand insertRawData = new SqlCommand(insertRawValues, Conn);
                    insertRawData.Parameters.AddWithValue("@srdDate", SqlDbType.NVarChar).Value = sDate.ToString();
                    insertRawData.Parameters.AddWithValue("@srdTime", SqlDbType.NVarChar).Value = sTime.ToString();
                    insertRawData.Parameters.AddWithValue("@srdDepth", SqlDbType.Decimal).Value = depth.ToString(); ;
                    insertRawData.Parameters.AddWithValue("@srdInc", SqlDbType.Decimal).Value = Inc.ToString();
                    insertRawData.Parameters.AddWithValue("@srdAzm", SqlDbType.Decimal).Value = Azm.ToString();
                    insertRawData.Parameters.AddWithValue("@srdDip", SqlDbType.Decimal).Value = Dip.ToString();
                    insertRawData.Parameters.AddWithValue("@srdBT", SqlDbType.Decimal).Value = BT.ToString();
                    insertRawData.Parameters.AddWithValue("@srdGT", SqlDbType.Decimal).Value = GT.ToString();
                    insertRawData.Parameters.AddWithValue("@srdGTF", SqlDbType.Decimal).Value = GTF.ToString();
                    insertRawData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                    insertRawData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                    insertRawData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                    insertRawData.Parameters.AddWithValue("@srvyRowID", SqlDbType.Int).Value = SurveyID.ToString();
                    insertRawData.Parameters.AddWithValue("@demID", SqlDbType.Int).Value = dataEntryMethod.ToString();
                    insertRawData.Parameters.AddWithValue("@fileName", SqlDbType.NVarChar).Value = dataFileName.ToString();
                    insertRawData.Parameters.AddWithValue("@login", SqlDbType.NVarChar).Value = LoginName.ToString();
                    insertRawData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = crTime.ToString();
                    insertRawData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = updTime.ToString();
                    insertRawData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceTypeID.ToString();
                    insertRawData.Parameters.AddWithValue("@jobID", SqlDbType.Int).Value = JobID.ToString();
                    iResult = Convert.ToInt32(insertRawData.ExecuteNonQuery());
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 StoreHistResultsData(String sDate, String sTime, Decimal depth, Decimal Inc, Decimal Azm, Decimal Dip, Decimal BT, Decimal GT, Decimal GTF, Int32 RunID, Int32 WellID, Int32 WellSectionID, Int32 SurveyID, Int32 dataEntryMethod, String dataFileName, String LoginName, String dbConn, Int32 ServiceTypeID, Int32 JobID)
        {
            try
            {
                Int32 iResult = -99;
                //Inserting read values in the SurveyResultsData table
                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;
                String insertRawValues = "INSERT INTO [HistSurveyResultsData] ( [srdDate], [srdTime], [srdDepth], [srdInc], [srdAzm], [srdDip], [srdBT], [srdGT], [srdGTF], [runID], [welID], [wswID], [srvyRowID], [demID], [fileName], [login], [cTime], [uTime], [srvID], [jobID] ) VALUES ( @srdDate, @srdTime, @srdDepth, @srdInc, @srdAzm, @srdDip, @srdBT, @srdGT, @srdGTF, @runID, @welID, @wswID, @srvyRowID, @demID, @fileName, @login, @cTime, @uTime, @srvID, @jobID )";
                using (SqlConnection Conn = new SqlConnection(dbConn))
                {
                    Conn.Open();

                    SqlCommand insertRawData = new SqlCommand(insertRawValues, Conn);
                    insertRawData.Parameters.AddWithValue("@srdDate", SqlDbType.NVarChar).Value = sDate.ToString();
                    insertRawData.Parameters.AddWithValue("@srdTime", SqlDbType.NVarChar).Value = sTime.ToString();
                    insertRawData.Parameters.AddWithValue("@srdDepth", SqlDbType.Decimal).Value = depth.ToString(); ;
                    insertRawData.Parameters.AddWithValue("@srdInc", SqlDbType.Decimal).Value = Inc.ToString();
                    insertRawData.Parameters.AddWithValue("@srdAzm", SqlDbType.Decimal).Value = Azm.ToString();
                    insertRawData.Parameters.AddWithValue("@srdDip", SqlDbType.Decimal).Value = Dip.ToString();
                    insertRawData.Parameters.AddWithValue("@srdBT", SqlDbType.Decimal).Value = BT.ToString();
                    insertRawData.Parameters.AddWithValue("@srdGT", SqlDbType.Decimal).Value = GT.ToString();
                    insertRawData.Parameters.AddWithValue("@srdGTF", SqlDbType.Decimal).Value = GTF.ToString();
                    insertRawData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                    insertRawData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                    insertRawData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                    insertRawData.Parameters.AddWithValue("@srvyRowID", SqlDbType.Int).Value = SurveyID.ToString();
                    insertRawData.Parameters.AddWithValue("@demID", SqlDbType.Int).Value = dataEntryMethod.ToString();
                    insertRawData.Parameters.AddWithValue("@fileName", SqlDbType.NVarChar).Value = dataFileName.ToString();
                    insertRawData.Parameters.AddWithValue("@login", SqlDbType.NVarChar).Value = LoginName.ToString();
                    insertRawData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = crTime.ToString();
                    insertRawData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = updTime.ToString();
                    insertRawData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceTypeID.ToString();
                    insertRawData.Parameters.AddWithValue("@jobID", SqlDbType.Int).Value = JobID.ToString();
                    iResult = Convert.ToInt32(insertRawData.ExecuteNonQuery());
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 StoreFormattedData(Int32 OperatorID, Int32 ClientID, String Date, String Time, Decimal DEPTH, Int32 LengthUnit, Decimal rGx, Decimal rGy, Decimal rGz, Decimal rBx, Decimal rBy, Decimal rBz, Int32 AcceleromterUnitID, Int32 MagnetometerUnitID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Int32 SurveyID, Int32 dataEntryMethod, String dataFileName, String LoginName, String DBConn)
        {
            try
            {
                Int32 iResult = -99;
                //Inserting Raw Data, calculated values into SurveyFormattedData table
                Int32 serviceType = -99;
                serviceType = 34;
                String insertRawValues = "INSERT INTO [SurveyFormattedData] ([sfdDate], [sfdTime], [sfdDepth], [lenU], [sfdGx], [sfdGy], [sfdGz], [sfdBx], [sfdBy], [sfdBz], [acuID], [muID], [runID], [wswID], [welID], [wpdID], [clntID], [optrID], [srvyRowID], [srvID], [demID], [fileName], [login], [cTime], [uTime]) VALUES (@sfdDate, @sfdTime, @sfdDepth, @lenU, @sfdGx, @sfdGy, @sfdGz, @sfdBx, @sfdBy, @sfdBz, @acuID, @muID, @runID, @wswID, @welID, @wpdID, @clntID, @optrID, @srvyRowID, @srvID, @demID, @fileName, @login, @cTime, @uTime)";

                using (SqlConnection Conn = new SqlConnection(DBConn))
                {
                    Conn.Open();

                    SqlCommand insertData = new SqlCommand(insertRawValues, Conn);
                    insertData.Parameters.AddWithValue("@sfdDate", SqlDbType.NVarChar).Value = Date.ToString();
                    insertData.Parameters.AddWithValue("@sfdTime", SqlDbType.NVarChar).Value = Time.ToString();
                    insertData.Parameters.AddWithValue("@sfdDepth", SqlDbType.Decimal).Value = DEPTH.ToString();
                    insertData.Parameters.AddWithValue("@lenU", SqlDbType.Int).Value = LengthUnit.ToString();
                    insertData.Parameters.AddWithValue("@sfdGx", SqlDbType.Decimal).Value = rGx.ToString();
                    insertData.Parameters.AddWithValue("@sfdGy", SqlDbType.Decimal).Value = rGy.ToString();
                    insertData.Parameters.AddWithValue("@sfdGz", SqlDbType.Decimal).Value = rGz.ToString();
                    insertData.Parameters.AddWithValue("@sfdBx", SqlDbType.Decimal).Value = rBx.ToString();
                    insertData.Parameters.AddWithValue("@sfdBy", SqlDbType.Decimal).Value = rBy.ToString();
                    insertData.Parameters.AddWithValue("@sfdBz", SqlDbType.Decimal).Value = rBz.ToString();
                    insertData.Parameters.AddWithValue("@acuID", SqlDbType.Int).Value = AcceleromterUnitID.ToString();
                    insertData.Parameters.AddWithValue("@muID", SqlDbType.Int).Value = MagnetometerUnitID.ToString();
                    insertData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                    insertData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                    insertData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                    insertData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                    insertData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                    insertData.Parameters.AddWithValue("@srvyRowID", SqlDbType.Int).Value = SurveyID.ToString();
                    insertData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = serviceType.ToString();
                    insertData.Parameters.AddWithValue("@demID", SqlDbType.Int).Value = dataEntryMethod.ToString();
                    insertData.Parameters.AddWithValue("@fileName", SqlDbType.NVarChar).Value = dataFileName.ToString();
                    insertData.Parameters.AddWithValue("@login", SqlDbType.NVarChar).Value = LoginName.ToString();
                    insertData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                    insertData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();

                    iResult = insertData.ExecuteNonQuery();
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getNextSequence(Int32 RunID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT ISNULL(MAX(srvyRowID), 0) + 1 FROM [SurveyResultsData] WHERE [runID] = @RunID";
                using (SqlConnection Conn = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        Conn.Open();  // Connection is opened to get runID and next sequence
                        SqlCommand nseqCMD = new SqlCommand(selData, Conn);
                        nseqCMD.Parameters.AddWithValue("@RunID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader nseqRdr = nseqCMD.ExecuteReader())
                        {
                            try
                            {
                                while (nseqRdr.Read())
                                {
                                    iReply = nseqRdr.GetInt32(0);
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                nseqRdr.Close();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        Conn.Close();
                        Conn.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static void ResultsHeaderTOPLINE(Dictionary<string, int> header_GB, string[] arr)
        {
            try
            {
                int arrlength = arr.Length;
                for (int i = 0; i < arrlength; i++)
                {
                    if (arr[i].ToLower().Contains("date") ||
                        arr[i].ToLower().Contains("time") ||
                        arr[i].ToLower().Contains("depth") ||
                          arr[i].ToLower().Contains("inc") ||
                          arr[i].ToLower().Contains("azimuth") ||
                          arr[i].ToLower().Contains("dip") ||
                          arr[i].ToLower().Contains("btotal") ||
                          arr[i].ToLower().Contains("gtotal") ||
                          arr[i].ToLower().Contains("gtf")
                        )
                        header_GB.Add(arr[i].Trim(), i);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //public static String ResultsDetailsProcessor(Dictionary<String, Int32> header_GB, String[] arrD, Int32 RunID, Int32 dataEntryMethod, String dataFileName, Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 ServiceID, String UserName, String UserRealm, String ClientDBConnection)
        //{
        //    try
        //    {
        //        String iReply = String.Empty;
        //        //ArrayList DetailList = new ArrayList();
        //        ArrayList rowValid = new ArrayList();
        //        ArrayList rowInvalid = new ArrayList();
        //        ArrayList JobInfo = new ArrayList();
        //        ArrayList RefMags = new ArrayList();
        //        List<Int32> WellInfo = new List<Int32> { };
        //        String date = String.Empty, time = String.Empty, DT = String.Empty, TM = String.Empty, errorMessage = String.Empty, successMessage = String.Empty;
        //        Int32 arrDlength, depth, inc, azimuth, dip, btotal, gtotal, gtf, strResData, strFrmData, resDataQC, nextSequence, LatiNS, LngiEW, mdecCID, gcoID, imdecCID;
        //        depth = inc = azimuth = dip = btotal = gtotal = gtf = resDataQC = LatiNS = LngiEW = mdecCID = gcoID = imdecCID = -99;
        //        Decimal DEPTH, INC, AZIMUTH, DIP, BTOTAL, GTOTAL, GTF;
        //        DEPTH = INC = AZIMUTH = DIP = BTOTAL = GTOTAL = GTF = -99.00M;
        //        Int32 jIFR = -99, JConverg = -99;
        //        Decimal MDec = -99.000000000000M, iMDec = -99.000000000000M, GConv = -99.000000000000M;
        //        Decimal colS = -99.000000000000M, colT = -99.000000000000M, colU = -99.000000000000M, colAEVal = -99.000000000000M, colAFVal = -99.000000000000M, colAGVal = -99.000000000000M, colAHVal = -99.000000000000M;
        //        Decimal rGx = -99.000000000000M, rGy = -99.000000000000M, rGz = -99.000000000000M;
        //        Decimal rBx = -99.000000000000M, rBy = -99.000000000000M, rBz = -99.000000000000M;

        //        arrDlength = arrD.Length;

        //        foreach (KeyValuePair<String, Int32> item in header_GB)
        //        {
        //            if (item.Key.ToLower().Contains("date"))
        //            { date = item.Value.ToString(); }
        //            if (item.Key.ToLower().Contains("time"))
        //            { time = item.Value.ToString(); }
        //            if (item.Key.ToLower().Contains("depth"))
        //            { depth = item.Value; }
        //            if (item.Key.ToLower().Contains("inc"))
        //            { inc = item.Value; }
        //            if (item.Key.ToLower().Contains("azimuth"))
        //            { azimuth = item.Value; }
        //            if (item.Key.ToLower().Contains("dip"))
        //            { dip = item.Value; }
        //            if (item.Key.ToLower().Contains("btotal"))
        //            { btotal = item.Value; }
        //            if (item.Key.ToLower().Contains("gtotal"))
        //            { gtotal = item.Value; }
        //            if (item.Key.ToLower().Contains("gtf"))
        //            { gtf = item.Value; }
        //        }

        //            if (arrD[depth].Trim() == "" || arrD[inc].Trim() == "" || arrD[azimuth].Trim() == "" || arrD[dip].Trim() == "" || arrD[btotal].Trim() == "" || arrD[gtotal].Trim() == "" || arrD[gtf].Trim() == "")
        //            {
        //                //Console.WriteLine("\n The Required Fields to process the Results QC are missing. Survey at {0} is skipped ", arrD[2]);
        //                errorMessage = String.Format("{0} {1}", "Invalid Data Row at Depth: ", arrD[depth]);
        //                //DetailList.Add(errorMessage);
        //                rowInvalid.Add(errorMessage);
        //            }
        //            else
        //            {
        //                nextSequence = getNextSequence(RunID, ClientDBConnection);                        
        //                WellInfo = AST.getWellDirection(WellID, ClientDBConnection);
        //                LatiNS = WellInfo[0];
        //                LngiEW = WellInfo[1];
        //                RefMags = OPR.getRefMagList(OperatorID, ClientID, WellPadID, WellID, ClientDBConnection);
        //                jIFR = Convert.ToInt32(RefMags[0]);                        
        //                MDec = Convert.ToDecimal(RefMags[1]);
        //                mdecCID = Convert.ToInt32(RefMags[2]);
        //                GConv = Convert.ToDecimal(RefMags[8]);                        
        //                gcoID = Convert.ToInt32(RefMags[9]);  
        //                JConverg = Convert.ToInt32(RefMags[10]);
        //                DT = Convert.ToString(arrD[0]);
        //                TM = Convert.ToString(arrD[1]);
        //                DEPTH = Convert.ToDecimal(arrD[2]);
        //                INC = Convert.ToDecimal(arrD[3]);
        //                AZIMUTH = Convert.ToDecimal(arrD[4]);
        //                DIP = Convert.ToDecimal(arrD[5]);
        //                BTOTAL = Convert.ToDecimal(arrD[6]);
        //                GTOTAL = Convert.ToDecimal(arrD[7]);
        //                GTF = Convert.ToDecimal(arrD[8]);

        //                //Calculating values
        //                colAEVal = resColAE(AZIMUTH, MDec, GConv, LatiNS, mdecCID, gcoID);
        //                colAFVal = resColAF(AZIMUTH, iMDec, GConv, LatiNS, imdecCID, gcoID);
        //                colAGVal = resColAG(AZIMUTH, MDec, mdecCID);
        //                colAHVal = resColAH(AZIMUTH, iMDec, imdecCID);
        //                colS = resColS(jIFR, colAEVal, colAFVal);
        //                colT = resColT(jIFR, colAGVal, colAHVal);
        //                colU = resColU(JConverg, colS, colT);
        //                rGx = resGx(GTOTAL, INC, GTF);
        //                rGy = resGy(GTOTAL, INC, GTF);
        //                rGz = resGz(GTOTAL, INC);
        //                rBx = resBx(BTOTAL, DIP, INC, GTF, colU);
        //                rBy = resBy(BTOTAL, INC, DIP, GTF, colU);
        //                rBz = resBz(BTOTAL, INC, DIP, GTF, colU);
                        
        //                //Inserting the raw data row to the ClientDB SurveyResultsData table
        //                strResData = Convert.ToInt32(StoreResultsData(OperatorID, ClientID, DT, TM, DEPTH, INC, AZIMUTH, DIP, BTOTAL, GTOTAL, GTF, WellPadID, WellID, WellSectionID, RunID, nextSequence, dataEntryMethod, dataFileName, UserName, ClientDBConnection, ServiceID));
        //                //Inserting formatted BGs generated from above inserted row into SurveyFormattedData
        //                strFrmData = Convert.ToInt32(StoreFormattedData(OperatorID, ClientID, DT, TM, DEPTH, rGx, rGy, rGz, rBx, rBy, rBz, RunID, WellID, WellSectionID, nextSequence, JobID, dataEntryMethod, dataFileName, UserName, ClientDBConnection));
        //                //Processing above formatted data row
        //                resDataQC = ManualQCSurveyProcessor(ClientID, JobID, WellID, WellSectionID, RunID, DT, TM, DEPTH, rGx, rGy, rGz, rBx, rBy, rBz, RunID, nextSequence, UserName, UserRealm, ServiceID, ClientDBConnection);
        //            }                
        //        iReply = dataFileName;
        //        //iReply.Add(rowValid);
        //        //iReply.Add(rowInvalid);
        //        return iReply;
        //    }
        //    catch (Exception ex)
        //    { throw new System.Exception(ex.ToString()); }
        //}

        public static ArrayList ResultsFileProcessor(Int32 JobID, Int32 WellID, Int32 WellSectionID, Int32 RunID, String WellName, String RunName, String FileToBeProcessed, Int32 ServiceID, String LoginName, String UserRealm, String ClientDBConnection)
        {
            try
            {
                ArrayList iReply = new ArrayList();
                String errorMessage = String.Empty, successMessage = String.Empty, iResult = String.Empty;
                ArrayList rowValid = new ArrayList();
                ArrayList rowInvalid = new ArrayList();
                String fileName = Path.GetFileName(FileToBeProcessed);
                String fName = String.Empty, wellName = String.Empty, runName = String.Empty;
                Int32 entryType = 4;                

                ResDataQC OBJ = new ResDataQC();
                using (StreamReader LogHeadReader = new StreamReader(FileToBeProcessed))
                {
                    try
                    {
                        using (SqlConnection Conn = new SqlConnection(ClientDBConnection.ToString()))
                        {
                            try
                            {
                                Conn.Open();  // Connection opened to get next sequence                                                

                                while (!LogHeadReader.EndOfStream)
                                {
                                    String text = LogHeadReader.ReadLine();

                                    //  Survey File Details
                                    if (text.Contains(",") && text.Contains(",") && text.Contains(","))
                                    {

                                        // Reading the Header Detail TOP Row 
                                        if (text.ToLower().Contains("btotal") || text.ToLower().Contains("gtotal") || text.ToLower().Contains("gtf"))
                                        {
                                            // Console.WriteLine(text);
                                            OBJ.headerTOP = text.Split(',');
                                            ResultsHeaderTOPLINE(OBJ.HeaderGB, OBJ.headerTOP);
                                        }
                                        else
                                        {
                                            // Reading Log Details and Processing the Data
                                            OBJ.DetailList.Add(text.Trim());
                                            OBJ.det = text.Split(',');
                                            
                                            if (String.IsNullOrEmpty(OBJ.det[3]) || String.IsNullOrEmpty(OBJ.det[4]) || String.IsNullOrEmpty(OBJ.det[5]) || String.IsNullOrEmpty(OBJ.det[6]) || String.IsNullOrEmpty(OBJ.det[7]) || String.IsNullOrEmpty(OBJ.det[8]))
                                            {
                                                errorMessage = String.Format("{0} {1}", "Invalid Data Row at Depth: ", OBJ.det[2]);
                                                OBJ.DetailList.Add(errorMessage);
                                                rowInvalid.Add(errorMessage);                                                
                                            }
                                            else
                                            {
                                                successMessage = String.Format("{0} {1}", "Valid Data Row at Depth: ", OBJ.det[2]);
                                                rowValid.Add(successMessage);
                                                //iResult = ResultsDetailsProcessor(OBJ.HeaderGB, OBJ.det, RunID, entryType, fileName, JobID, WellID, WellSectionID, ServiceID, LoginName, UserRealm, ClientDBConnection);
                                            }
                                        }
                                    }
                                    // Survey Header Data
                                    else
                                    {
                                        if (!String.IsNullOrEmpty(text)) // removing empty spaces from String
                                        {
                                            // Not Required for the Results Processor. The header Data of the File
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                Conn.Close();  // Connection closed for setting the next sequence
                                Conn.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        LogHeadReader.Close();
                        LogHeadReader.Dispose();
                    }
                }
                iReply.Add(iResult);
                iReply.Add(rowInvalid);
                iReply.Add(rowValid);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 ManualQCSurveyProcessor(Int32 ClientID, Int32 JobID, Int32 WellID, Int32 WellSectionID, Int32 RunID, String srvyDate, String srvyTime, Decimal srDepth, Decimal srGx, Decimal srGy, Decimal srGz, Decimal srBx, Decimal srBy, Decimal srBz, Int32 srRun, Int32 srRowID, String UserName, String UserRealm, Int32 ServiceTypeID, String ClientDBString)
        {
            try
            {
                Int32 qcProcess = -99;
                //qcProcess = RawQC.surveyQC(UserName, UserRealm, JobID, WellID, WellSectionID, RunID, srvyDate, srvyTime, srDepth, srGx, srGy, srGz, srBx, srBy, srBz, srRowID, ClientDBString, ServiceTypeID);
                return qcProcess;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //public static Int32 processManualTextRow(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Int32 NextSequence, String inputSurvey, Int32 ServiceID, Int32 SurveyEntryType, String LoginName, String UserRealm, String ClientDBConnection)
        //{
        //    try
        //    {
        //        Int32 iReply = -99, jIFR = -99, JConverg = -99;
        //        String[] str;
        //        ArrayList RefMags = new ArrayList();
        //        List<Int32> WellInfo = new List<Int32> { };
        //        String date = String.Empty, time = String.Empty, fileName = "Manual";
        //        Decimal depth, Inc, Azm, Dip, BT, GT, GTF;
        //        Int32 LatiNS = -99, LngiEW = -99, mdecCID = -99, imdecCID = -99, gcoID = -99;
        //        Decimal MDec = -99.000000000000M, iMDec = -99.000000000000M, GConv = -99.000000000000M, colS = -99.000000000000M, colT = -99.000000000000M, colU = -99.000000000000M;
        //        Decimal colAEVal = -99.000000000000M, colAFVal = -99.000000000000M, colAGVal = -99.000000000000M, colAHVal = -99.000000000000M;
        //        Decimal rGx = -99.000000000000M, rGy = -99.000000000000M, rGz = -99.000000000000M, rBx = -99.000000000000M, rBy = -99.000000000000M, rBz = -99.000000000000M;
        //        Int32 strResData, strFormData, resQCProcess;
        //        strResData = strFormData = -99;
        //        if (inputSurvey.Contains(",") && inputSurvey.Contains(",") && inputSurvey.Contains(","))
        //        {
        //            try
        //            {
        //                str = inputSurvey.Split(',');
                        
                        
        //                WellInfo = AST.getWellDirection(WellID, ClientDBConnection);
        //                LatiNS = WellInfo[0];
        //                LngiEW = WellInfo[1];
        //                RefMags = OPR.getRefMagList(OperatorID, ClientID, WellPadID, WellID, ClientDBConnection);
        //                jIFR = Convert.ToInt32(RefMags[0]);
        //                MDec = Convert.ToDecimal(RefMags[1]);
        //                GConv = Convert.ToDecimal(RefMags[4]);
        //                mdecCID = Convert.ToInt32(RefMags[5]);
        //                gcoID = Convert.ToInt32(RefMags[6]);
        //                iMDec = Convert.ToDecimal(RefMags[7]);
        //                imdecCID = Convert.ToInt32(RefMags[8]);
        //                JConverg = Convert.ToInt32(RefMags[4]);
        //                depth = Convert.ToDecimal(str[2].Trim());
        //                Inc = Convert.ToDecimal(str[3].Trim());
        //                Azm = Convert.ToDecimal(str[4].Trim());
        //                Dip = Convert.ToDecimal(str[5].Trim());
        //                BT = Convert.ToDecimal(str[6].Trim());
        //                GT = Convert.ToDecimal(str[7].Trim());
        //                GTF = Convert.ToDecimal(str[8].Trim());
        //                colAEVal = resColAE(Azm, MDec, GConv, LatiNS, mdecCID, gcoID);
        //                colAFVal = resColAF(Azm, iMDec, GConv, LatiNS, imdecCID, gcoID);
        //                colAGVal = resColAG(Azm, MDec, mdecCID);
        //                colAHVal = resColAH(Azm, iMDec, imdecCID);
        //                colS = resColS(jIFR, colAEVal, colAFVal);
        //                colT = resColT(jIFR, colAGVal, colAHVal);
        //                colU = resColU(JConverg, colS, colT);
        //                rGx = resGx(GT, Inc, GTF);
        //                rGy = resGy(GT, Inc, GTF);
        //                rGz = resGz(GT, Inc);
        //                rBx = resBx(BT, Dip, Inc, GTF, colU);
        //                rBy = resBy(BT, Inc, Dip, GTF, colU);
        //                rBz = resBz(BT, Inc, Dip, GTF, colU);
        //                date = str[0].Trim();
        //                time = str[1].Trim();                        
        //                // Raw Survey Data Being stored into the DataBase 
        //                strResData = Convert.ToInt32(StoreResultsData(date, time, depth, Inc, Azm, Dip, BT, GT, GTF, RunID, WellID, WellSectionID, NextSequence, SurveyEntryType, fileName, LoginName, ClientDBConnection, ServiceID, JobID));
        //                strFormData = Convert.ToInt32(StoreFormattedData(date, time, depth, rGx, rGy, rGz, rBx, rBy, rBz, RunID, WellID, WellSectionID, NextSequence, JobID, SurveyEntryType, fileName, LoginName, ClientDBConnection));
        //                resQCProcess = Convert.ToInt32(ManualQCSurveyProcessor(ClientID, JobID, WellID, WellSectionID, RunID, date, time, depth, rGx, rGy, rGz, rBx, rBy, rBz, RunID, NextSequence, LoginName, UserRealm, ServiceID, ClientDBConnection));
        //                if((strResData.Equals(1)) && (strFormData.Equals(1)) && (resQCProcess.Equals(1)))
        //                {
        //                    iReply = 1;
        //                }
        //                else
        //                {
        //                    iReply = -1;
        //                }
        //            }
        //            catch (Exception ex)
        //            { throw new System.Exception(ex.ToString()); }
        //        }
        //        return iReply;
        //    }
        //    catch (Exception ex)
        //    { throw new System.Exception(ex.ToString()); }
        //}
    }
}