﻿using System;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using Units = SmartsVer1.Helpers.DemogHelpers.UnitsConverter;

namespace SmartsVer1.Helpers.QCHelpers
{
    public class NonMagCalc
    {
        static void Main()
        { }

        public static Decimal nmsTAzmMag(Int32 ifrVal, Decimal MDec, Decimal iMDec, Decimal GC, Decimal runTAzm)
        {
            try
            {
                Decimal val = runTAzm - MDec + GC;
                if( ifrVal.Equals( 1 )) //1 = No; 2 = Yes
                {
                    if( val > 359.99M )
                    {
                        return val - 360;
                    }
                    else if( val < 0 )
                    {
                        return val - 360;
                    }
                    else
                    {
                        return val;
                    }
                }
                else
                {
                    if( val > 359.99M )
                    {
                        return val - 360;
                    }
                    else if (val < 0)
                    {
                        return val - 360;
                    }
                    else
                    {
                        return val;
                    }
                }

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 nmsMPStrength( Decimal cnvMSize ) //NMCalc - D11
        {
            try
            {
                if (cnvMSize.Equals(0))
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32( ( ( 49.072M * (Decimal)(Math.Abs( (Double)cnvMSize) ) ) + 129.23M ) );
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 dcPoleStrength(Decimal cnvDSCSz) // NMCalc = D12
        {
            try
            {
               if( cnvDSCSz.Equals( 0 ) )
               {
                    return 0;
               }
               else
               {
                   return Convert.ToInt32( ( (84.764M * (Decimal)(Math.Abs((Double)cnvDSCSz))) + 93.224M ) );
               }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal dcInterference(Decimal dcPoleStrength, Decimal cnvNMSAbove) //NMCalcII I20
        {
            try
            {
                return ( dcPoleStrength / (Decimal)( 4 * Math.PI ) * ( ( 1 / (Decimal)Math.Pow( ( (Double)cnvNMSAbove + 0.01 ) , 2 ) ) ) ) * 1000;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal mtrInterference(Decimal mtrPoleStrength, Decimal disBlw, Decimal lenMtrBit) //NMCalcII I21
        {
            try
            {
                return ( mtrPoleStrength / (Decimal)(4*Math.PI) * ( ( 1 / (Decimal)Math.Pow( ( (Double)disBlw + 0.01 ) , 2 ) ) - ( 1 / (Decimal)Math.Pow( ( (Double)disBlw + (Double)lenMtrBit + 0.01 ) , 2 ) ) ) ) * 1000.00M;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal bhaInterference(Decimal dcInterference, Decimal mtrInterference) //NMCalcII I23
        {
            try
            {
                return Decimal.Add( dcInterference , mtrInterference );
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal horzBT(Decimal cnvDip, Int32 cnvBT) //NMCalcII I18
        {
            try
            {
                return cnvBT * (Decimal)Math.Cos( Units.ToRadians( (Double)cnvDip ) );
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal lcazBHAError(Decimal TInc, Decimal TAzmMag, Decimal horzBT, Decimal bhaInterference) //NMCalcII I24
        {
            try
            {
                return Convert.ToDecimal( Math.Abs(Units.ToDegrees(Math.Atan( (Double)( bhaInterference * (Decimal)Math.Sin( Units.ToRadians( (Double)TInc ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)TAzmMag ) ) ) / (Double)( horzBT + ( bhaInterference * ( (Decimal)Math.Sin( Units.ToRadians( (Double)TInc ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)TAzmMag ) ) ) ) ) ) ) ) );
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal disBitToSensor(Decimal cnvNMSpBlw, Decimal cnvMDBLen) //NMCalcII I25
        {
            try
            {
                return Decimal.Add( cnvMDBLen , cnvNMSpBlw );
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal ttlBHAInterference(Decimal dcSurfaceInterference, Decimal mtrSurfaceInterference)
        {
            try
            {
                return Decimal.Add(dcSurfaceInterference, mtrSurfaceInterference);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal btsDistance(Decimal cnvNMSpBlw, Decimal cnvMDBLen)
        {
            try
            {
                return Decimal.Add( cnvMDBLen , cnvNMSpBlw );
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal calcMGRStdMtrPoleStrength(Decimal MDBLen, Int32 mtrPoleStrength)
        {
            try
            {
                Decimal val1 = Decimal.Divide( 1M , (decimal)Math.Pow( (double)Decimal.Add(0M, 0.01M) , 2 ) );
                Decimal val2 = Decimal.Divide( 1M , (decimal)Math.Pow( (double)Decimal.Add( Decimal.Add(0M, MDBLen) , 0.01M) , 2 ) );
                Decimal val3 = Decimal.Subtract(val1, val2);
                Decimal val5 = Decimal.Divide(mtrPoleStrength, Decimal.Multiply( 4M , (decimal)Math.PI ));
                Decimal val6 = Decimal.Multiply(val5, val3);
                return Decimal.Multiply(val6, 0.01M);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal mgrStdDCPoleStrength(Int32 mtrGaussmeter, Int32 cnvMtrGaussmeter, Decimal calcMGRPoleStrength)
        {
            try
            {
                if (mtrGaussmeter.Equals(0))
                {
                    return calcMGRPoleStrength;
                }
                else
                {
                    return cnvMtrGaussmeter;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal calcDCPoleStrength(Decimal cnvDSCS, Int32 dcPoleStrength)
        {
            try
            {
                return Decimal.Multiply(Decimal.Divide(dcPoleStrength, Decimal.Multiply((Decimal.Multiply(4, (decimal)Math.PI)), (decimal)Math.Pow((double)Decimal.Multiply(Decimal.Divide(Decimal.Add(0.01M, cnvDSCS), 12), 0.3048M), 2))), 0.01M);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal dcgrStdDCPoleStrength(Decimal dscGaussmeter, Decimal cnvdscGaussmeter, Decimal calcDCPoleStrength)
        {
            try
            {
                if( dscGaussmeter.Equals( 0 ) )
                {
                    return calcDCPoleStrength;
                }
                else
                {
                    return cnvdscGaussmeter;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal mtrSurfacePoleStrength(Decimal cnvMtrSize, Decimal mtrGaussmeter, Decimal cnvGaussmeterTopDistance) //NMCalcII C12
        {
            try
            {
                return mtrGaussmeter * (Decimal)( 4 * Math.PI * Math.Pow( ( (Double)cnvGaussmeterTopDistance + (Double)cnvMtrSize / 12 * 0.3048 ) , 2 ) ) * 100.00M;                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal dscSurfacePoleStrength(Decimal cnvDSCSize, Decimal cnvDSCGaussmeter, Decimal cnvGaussmeterBtmDistance) //NMCalcII C13
        {
            try
            {
                return Decimal.Multiply( Decimal.Multiply( cnvDSCGaussmeter , Decimal.Multiply( Decimal.Multiply( 4M , (decimal)Math.PI ) , (decimal)Math.Pow( (double)Decimal.Multiply( Decimal.Add( cnvGaussmeterBtmDistance , Decimal.Divide(cnvDSCSize, 12) ) , 0.3048M ) , 2 ) ) ) , 100M );
            }
                catch(Exception ex)
            {throw new System.Exception(ex.ToString());}
        }

        public static Decimal mtrSurfaceInterference(Decimal mtrSurfacePoleStrength, Decimal blwDistance, Decimal lenMtrBit) //NMCalcII C14
        {
            try
            {
                return (mtrSurfacePoleStrength / (Decimal)( 4 * Math.PI ) * ( ( 1 / (Decimal)Math.Pow( ( (Double)blwDistance + 0.01 ) , 2 ) ) - ( 1 / (Decimal)Math.Pow( ( (Double)blwDistance + (Double)lenMtrBit + 0.01 ) , 2 ) ) ) ) * 1000.00M;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal dcSurfaceInterference(Decimal dcPoleStrength, Decimal abvDistance) //NMCalcII C15
        {
            try
            {
                return ( dcPoleStrength / (Decimal)( 4 * Math.PI ) * ( ( 1 / (Decimal)Math.Pow( ( (Double)abvDistance + 0.01 ) , 2 ) ) ) ) * 1000.00M;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal bhaTotalInterference(Decimal mtrSurfaceInterference, Decimal dcSurfaceInterference) //NMCalcII C16
        {
            try
            {
                return Decimal.Add(mtrSurfaceInterference, dcSurfaceInterference);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal bhaLCAZError(Decimal bhaTotalInterference, Decimal TInc, Decimal TAzmMag, Decimal horzBT) //NMCalcII C17
        {
            try
            {
                return Convert.ToDecimal( Math.Abs(Units.ToDegrees(Math.Atan ( (Double)( bhaTotalInterference * (Decimal)Math.Sin( Units.ToRadians( (Double)TInc ) ) * (Decimal)Math.Sin( Units.ToRadians( (Double)TAzmMag ) ) ) / (Double)( horzBT + ( bhaTotalInterference * ( (Decimal)Math.Sin( Units.ToRadians( (Double)TInc ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)TAzmMag ) ) ) ) ) ) ) ) );
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static Decimal dcMaxGaussmeter(Decimal dcPoleStrength, Decimal cnvDSCSize) //NMCalcII C22
        {
            try
            {
                return Convert.ToDecimal( ( dcPoleStrength / (Decimal)( 4 * Math.PI) * (Decimal)Math.Pow( (Double)( 0.01M + cnvDSCSize / 12.00M * 0.3048M ) , 2 ) ) * 0.01M );
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal wcMPSMotor(Decimal mtrPoleStrength, Decimal mtrSurfacePoleStrength) //NMCalcII C23
        {
            try
            {
                if( mtrPoleStrength > mtrSurfacePoleStrength )
                {
                    return mtrPoleStrength;
                }
                else
                {
                    return mtrSurfacePoleStrength;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal bptopDrillCollar(Decimal dcpsLimit, Decimal abvDistance) //NMCalcII C24
        {
            try
            {
                return 1 / (Decimal)(4 * Math.PI) * ( ( dcpsLimit / (Decimal)Math.Pow( ( (Double)abvDistance + 0.01 ) , 2 ) ) ) * 1000.00M;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal bpbtmMotor(Decimal wcMPSMotor, Decimal blwDistance, Decimal mdbLength) //NMCalcII C25
        {
            try
            {
                return 1 / (Decimal)(4 * Math.PI) * ( ( wcMPSMotor / (Decimal)Math.Pow( ( (Double)blwDistance + 0.01 ), 2 ) ) - ( wcMPSMotor / (Decimal)Math.Pow( (Double)( mdbLength + blwDistance - 0.01M ) , 2 ) ) ) * 1000.00M;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal bpTotalBz(Decimal bptopDrillCollar, Decimal bpbtmMotor) //NMCalcII C26
        {
            try
            {
                return Decimal.Add(bptopDrillCollar, bpbtmMotor);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal maxAllowableError(Decimal bpTotalBz, Decimal TInc, Decimal convProposedEndRunAzimuth, Decimal cnvBT, Decimal cnvDip) //NMCalcII C27
        {
            try
            {
                return Convert.ToDecimal( Units.ToDegrees(Math.Atan( (Double)( bpTotalBz * (Decimal)Math.Sin( Units.ToRadians( (Double)TInc) ) * (Decimal)Math.Sin(Units.ToRadians( (Double)convProposedEndRunAzimuth ) ) / ( ( cnvBT * (Decimal)Math.Cos(Units.ToRadians( (Double)cnvDip ) ) ) + bpTotalBz * (Decimal)Math.Sin( Units.ToRadians( (Double)TInc ) ) * (Decimal)Math.Cos( Units.ToRadians( (Double)convProposedEndRunAzimuth ) ) ) ) ) ) );
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal downholeMPSMotor(Decimal cnvMSize, Decimal convMtrGaussmeter, Decimal gmtrDistBtm) //NMCalcII C34
        {
            try
            {
                return convMtrGaussmeter * (Decimal)(4*Math.PI) * (Decimal)Math.Pow( (Double)( gmtrDistBtm + cnvMSize / 12.00M * 0.3048M ) , 2 ) * 100.00M;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal bpMotor(Decimal dhMPSMotor, Decimal blwDistance, Decimal lenMtrDrill) //NMCalcII C35
        {
            try
            {
                return 1 / (Decimal)(4 * Math.PI) * ( ( dhMPSMotor / (Decimal)Math.Pow( (Double)( blwDistance + 0.01M ), 2) ) - ( dhMPSMotor / (Decimal)Math.Pow( (Double)( lenMtrDrill + blwDistance - 0.01M ), 2 ) ) ) * 1000.00M;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal bpDrillCollarJar( Decimal dhAxialInterference , Decimal bpMotor ) //NMCalcII C36
        {
            try
            {
                return Math.Abs(Decimal.Subtract(Math.Abs(dhAxialInterference), Math.Abs( bpMotor )));
            }
            catch(Exception ex )
            {throw new System.Exception(ex.ToString());}
        }

        public static Decimal dhMPSDrillCollar(Decimal bpDrillCollarJar, Decimal abvDistance) //NMCalcII C37
        {
            try
            {
                return (Decimal)(4 * Math.PI) * ( ( bpDrillCollarJar * (Decimal)( Math.Pow( (Double)( abvDistance + 0.01M ) , 2 ) ) ) ) * 0.001M;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal actGaussmeterDCJar(Decimal dhMPSDrillCollar, Decimal cnvDSCZ) //NMCalcII C38
        {
            try
            {
                return dhMPSDrillCollar / (Decimal)( 4 * Math.PI ) * (Decimal)Math.Pow( (Double)( 0.01M + cnvDSCZ / 12.00M * 0.3048M ) , 2 ) * 0.01M;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal stdvalDCInterference(Decimal dcPoleStrength, Decimal currNMAbv) //NMCalc D48
        {
            try
            {
                Decimal val1 = Decimal.Divide(dcPoleStrength, Decimal.Multiply(4M, (decimal)Math.PI));
                Decimal val2 = Decimal.Divide( 1 , (decimal)Math.Pow( (double)Decimal.Add(currNMAbv, 0.01M) , 2 ) );
                return Decimal.Multiply( Decimal.Multiply(val1, val2) , 1000M );
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal stdvalMTRInterference(Decimal mtrPoleStrength, Decimal currNMBlw, Decimal lenMtrBit) //NMCalc D49
        {
            try
            {
                Decimal val1 = Decimal.Divide(mtrPoleStrength, Decimal.Multiply(4M, (decimal)Math.PI));
                Decimal val2 = Decimal.Divide( 1 , (decimal)Math.Pow( (double)Decimal.Add(currNMBlw, 0.01M) , 2 ) );
                Decimal val3 = Decimal.Divide( 1 , (decimal)Math.Pow( (double)Decimal.Add( Decimal.Add(currNMBlw, lenMtrBit) , 0.01M ) , 2 ) );
                return Decimal.Multiply( Decimal.Multiply( val1 , Decimal.Subtract(val2, val3) ) , 1000M );
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal stdvalTTLBHAInterference( Decimal stdvalDCInterference , Decimal stdvalMTRInterference ) //NMCalc D51
        {
            try
            {
                return Decimal.Add( stdvalDCInterference , stdvalMTRInterference );
            }
            catch(Exception ex)
            {throw new System.Exception(ex.ToString());}
        }

        public static Decimal stdLCAZError(Decimal stdvalTTLBHAInterference, Decimal currInc, Decimal currAzmMagnetic, Decimal hrzBt) //NMCalc D52
        {
            try
            {
                return Convert.ToDecimal(Math.Abs(Units.ToDegrees(Math.Atan(((Double)stdvalTTLBHAInterference * Math.Sin(Units.ToRadians((Double)currInc)) * Math.Sin(Units.ToRadians((Double)currAzmMagnetic))) / ((Double)hrzBt + ((Double)stdvalTTLBHAInterference * (Math.Sin(Units.ToRadians((Double)currInc)) * Math.Cos(Units.ToRadians((Double)currAzmMagnetic)))))))));
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal gsmtrTTLBHAInterference(Decimal surfaceMtrInterference, Decimal surfaceDCInterference) //NMCacl G51
        {
            try
            {
                return Decimal.Add(surfaceMtrInterference, surfaceDCInterference);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal gmtrLCAZError(Decimal gsmtrTTLBHAInterference, Decimal currInc, Decimal currAzmMag, Decimal hrzBt) //NMCalc G52
        {
            try
            {
                Decimal val1 = Decimal.Multiply( gsmtrTTLBHAInterference , (decimal)Math.Sin( ( Units.ToRadians( (double)currInc )  ) ) );
                Decimal val2 = Decimal.Multiply(val1, (decimal)Math.Sin( Units.ToRadians( (double)currAzmMag ) ) );
                Decimal val3 = Decimal.Multiply(gsmtrTTLBHAInterference, (decimal)Math.Sin((Units.ToRadians((double)currInc))));
                Decimal val4 = Decimal.Multiply(val3, Decimal.Multiply(val1, (decimal)Math.Cos(Units.ToRadians((double)currAzmMag))));
                Decimal val5 = Decimal.Add(hrzBt, val4);
                return Math.Abs( (decimal)Units.ToDegrees( Math.Atan( (double)Decimal.Divide(val2, val5) ) ) );
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        
    }
}