﻿using System;
using System.Web;
using System.Data;
using System.Globalization;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Collections.Generic;
using ACC = SmartsVer1.Helpers.QCHelpers.AccountingQC;
using Units = SmartsVer1.Helpers.DemogHelpers.UnitsConverter;

namespace SmartsVer1.Helpers.QCHelpers
{
    public class BsGs
    {
        static void Main()
        { }

        private static Decimal resColS(Int32 jIFR, Decimal MDec, Decimal GConv, Decimal iMDec, Decimal Azimuth)
        {
            try
            {
                if (jIFR.Equals(1)) // 1 = No
                {
                    if ((Decimal.Add(Decimal.Subtract(Azimuth, MDec), GConv) > 359.99M))
                        return (Decimal.Subtract(Decimal.Add(Decimal.Subtract(Azimuth, MDec), GConv), 360M));
                    else if (Decimal.Add(Decimal.Subtract(Azimuth, MDec), GConv) < 0)
                        return Decimal.Add((Decimal.Add(Decimal.Subtract(Azimuth, MDec), GConv)), 360M);
                    else
                        return Decimal.Add(Decimal.Subtract(Azimuth, MDec), GConv);
                }
                else
                {
                    if ((Decimal.Add(Decimal.Subtract(Azimuth, iMDec), GConv)) > 359.99M)
                        return Decimal.Subtract(((Decimal.Add(Decimal.Subtract(Azimuth, iMDec), GConv))), 360M);
                    else if (((Decimal.Add(Decimal.Subtract(Azimuth, iMDec), GConv))) < 0)
                        return Decimal.Add((Decimal.Add(Decimal.Subtract(Azimuth, iMDec), GConv)), 360M);
                    else
                        return (Decimal.Add(Decimal.Subtract(Azimuth, iMDec), GConv));
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal resColT(Int32 jIFR, Decimal Azimuth, Decimal MDec, Decimal iMDec)
        {
            try
            {
                if (jIFR.Equals(1))
                {
                    if ((Decimal.Subtract(Azimuth, MDec)) > 359.99M)
                        return Decimal.Subtract((Decimal.Subtract(Azimuth, MDec)), 360M);
                    else if ((Decimal.Subtract(Azimuth, MDec)) < 0)
                        return Decimal.Add((Decimal.Subtract(Azimuth, MDec)), 360M);
                    else
                        return Decimal.Subtract(Azimuth, MDec);
                }
                else
                {
                    if ((Decimal.Subtract(Azimuth, iMDec)) > 359.99M)
                        return Decimal.Subtract((Decimal.Subtract(Azimuth, iMDec)), 360M);
                    else if ((Decimal.Subtract(Azimuth, iMDec)) < 0)
                        return Decimal.Add(Decimal.Subtract(Azimuth, iMDec), 360M);
                    else
                        return (Decimal.Subtract(Azimuth, iMDec));
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal resColU(Int32 JConverg, Decimal resColS, Decimal resColT)
        {
            try
            {
                if (JConverg.Equals(2)) //Grid
                {
                    return resColS;
                }
                else
                {
                    return resColT;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal resGx(Decimal GTotal, Decimal Inclination, Decimal GTF)
        {
            try
            {
                return Decimal.Multiply(Decimal.Multiply(Decimal.Multiply(-1, GTotal), ((decimal)Math.Sin(Units.ToRadians((double)Inclination)))), ((decimal)Math.Cos(Units.ToRadians((double)GTF))));
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal resGy(Decimal GTotal, Decimal Inclination, Decimal GTF)
        {
            try
            {
                return Decimal.Multiply(Decimal.Multiply(GTotal, (decimal)Math.Sin((double)Units.ToRadians((double)Inclination))), (decimal)Math.Sin(((double)Units.ToRadians((double)GTF))));
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal resGz(Decimal GTotal, Decimal Inclination)
        {
            try
            {
                return Decimal.Multiply(GTotal, (decimal)Math.Cos((double)Units.ToRadians((double)Inclination)));
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal resBx(Decimal BTotal, Decimal Dip, Decimal Inclination, Decimal GTF, Decimal resColU)
        {
            try
            {
                Decimal val1 = Decimal.Multiply((decimal)Math.Cos(Units.ToRadians((double)Dip)), Decimal.Multiply((decimal)Math.Cos(Units.ToRadians((double)resColU)), Decimal.Multiply((decimal)Math.Cos(Units.ToRadians((double)Inclination)), (decimal)Math.Cos(Units.ToRadians((double)GTF)))));
                Decimal val2 = Decimal.Multiply((decimal)Math.Sin(Units.ToRadians((double)Dip)), Decimal.Multiply((decimal)Math.Sin(Units.ToRadians((double)Inclination)), (decimal)Math.Cos(Units.ToRadians((double)GTF))));
                Decimal val3 = Decimal.Multiply((decimal)Math.Cos(Units.ToRadians((double)Dip)), Decimal.Multiply((decimal)Math.Sin(Units.ToRadians((double)resColU)), (decimal)Math.Sin(Units.ToRadians((double)GTF))));
                return Decimal.Multiply(BTotal, (Decimal.Subtract(val1, Decimal.Subtract(val2, val3))));
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal resBy(Decimal BTotal, Decimal Inclination, Decimal Dip, Decimal GTF, Decimal resColU)
        {
            try
            {
                Decimal val1 = Decimal.Multiply((decimal)Math.Cos(Units.ToRadians((double)Dip)), Decimal.Multiply((decimal)Math.Cos(Units.ToRadians((double)resColU)), Decimal.Multiply((decimal)Math.Cos(Units.ToRadians((double)Inclination)), (decimal)Math.Sin(Units.ToRadians((double)GTF)))));
                Decimal val2 = Decimal.Multiply((decimal)Math.Cos(Units.ToRadians((double)Dip)), (Decimal.Multiply((decimal)Math.Sin(Units.ToRadians((double)resColU)), (decimal)Math.Cos(Units.ToRadians((double)GTF)))));
                Decimal val3 = Decimal.Multiply(((decimal)Math.Sin(Units.ToRadians((double)Dip))), (Decimal.Multiply((decimal)Math.Sin(Units.ToRadians((double)Inclination)), (decimal)Math.Sin(Units.ToRadians((double)GTF)))));

                return Decimal.Multiply(Decimal.Multiply(-1, BTotal), (Decimal.Add(val1, Decimal.Subtract(val2, val3))));
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal resBz(Decimal BTotal, Decimal Inclination, Decimal Dip, Decimal GTF, Decimal resColU)
        {
            try
            {
                Decimal val1 = Decimal.Multiply((decimal)Math.Sin(Units.ToRadians((double)Dip)), (decimal)Math.Cos(Units.ToRadians((double)Inclination)));
                Decimal val2 = Decimal.Multiply((decimal)Math.Cos(Units.ToRadians((double)Dip)), Decimal.Multiply((decimal)Math.Cos(Units.ToRadians((double)resColU)), (decimal)Math.Sin(Units.ToRadians((double)Inclination))));
                return Decimal.Multiply(BTotal, Decimal.Add(val1, val2));
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 generateBGs(String UserName, String Domain, Int32 wpID, Int32 JobID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Int32 SQCID, Decimal srDepth, Decimal srInc, Decimal srAzm, Decimal srDip, Decimal srBT, Decimal srGT, Decimal srGTF, Int32 srRowID, String GetClientDBString)
        {
            try
            {
                Int32 result = -99;
                Int32 insResult = -99;
                Int32 wpResult = -99;
                Int32 bgResult = -99;
                DateTime cTime = DateTime.Now;
                DateTime uTime = DateTime.Now;

                String demogConn = null;
                Decimal colSVal = 0.000000000000M;
                Decimal colTVal = 0.000000000000M;
                Decimal colUVal = 0.000000000000M;
                Decimal GxVal = 0.000000000000M;
                Decimal GyVal = 0.000000000000M;
                Decimal GzVal = 0.000000000000M;
                Decimal BxVal = 0.000000000000M;
                Decimal ByVal = 0.000000000000M;
                Decimal BzVal = 0.000000000000M;

                Int32 jIFR = -99; //Job IFR
                Int32 jAcelU = -99; //Job Accel Units
                Int32 jMagU = -99; // Job Mag Units
                Int32 jLenU = -99; // Job Length Units
                Int32 jConverg = -99; // Job Convergence
                Int32 rfmgID = -99; //Job's Ref. Mags. ID
                Decimal mdec = -99.00M; //Magnetic Declination value
                Decimal dip = -99.00M; //Dip Value
                Decimal bTotal = -99.000000M; //B-Total Value
                Decimal gTotal = -99.000000M; //G-Total Value
                Decimal gConv = -99.00M; //Job Convergence Value
                Decimal imdec = -99.00M; //IFR Mag. Declination Value
                Decimal idip = -99.00M; //IFR Dip Value
                Decimal ibTotal = -99.00M; //IFR B-Total Value
                Decimal igTotal = -99.000000M; //IFR G-Total Value
                Int32 bhaID = -99;
                Decimal sqcDip = -99.0000M; //SQC Dip Tolerance
                Decimal sqcBTotal = -99.0000M; //SQC B-Total Tolerance
                Decimal sqcGTotal = -99.0000000M; //SQC G-Total Tolerance
                Decimal rInc = -99.00M; // Run Inclination
                Decimal rAzm = -99.00M; // Run Azimuth
                Decimal rTInc = -99.00M; // Run Target Inclination
                Decimal rTAzm = -99.00M; // Run Target Azimuth
                
                demogConn = WebConfigurationManager.ConnectionStrings["FEDemogDB"].ConnectionString;

                String getJobIFRValue = "SELECT ifrvalID FROM Job WHERE jobID = @jobID";
                String getJobMetaData = "SELECT acuID, muID, luID, jcID FROM JobDetail WHERE jobID = @jobID";
                String getJobRefMags = "SELECT rfmgID, mdec, dip, bTotal, gTotal, gconv FROM RefMags WHERE jobID = @jobID AND dmgstID = '1'";
                String getJobiRefMags = "SELECT imdec, idip, ibTotal, igTotal FROM iRefMags WHERE rfmgID = @rfmgID";
                String getSQCLimits = "SELECT sqcDip, sqcBTotal, sqcGTotal FROM SQC WHERE sqcID = @sqcID";
                String getRunMetaData = "SELECT runInc, runAzm, runTInc, runTAzm from Run WHERE runID = @runID";

                //Getting Meta Data
                using (SqlConnection clntConn = new SqlConnection(GetClientDBString))
                {
                    try
                    {
                        //////Job Meta Data
                        SqlCommand jobIFR = new SqlCommand(getJobIFRValue, clntConn);
                        SqlCommand jobMetaD = new SqlCommand(getJobMetaData, clntConn);
                        SqlCommand jobRefMag = new SqlCommand(getJobRefMags, clntConn);
                        SqlCommand jobiRefMag = new SqlCommand(getJobiRefMags, clntConn);
                        SqlCommand SQCTols = new SqlCommand(getSQCLimits, clntConn);
                        //////Run Meta Data
                        SqlCommand runMetaD = new SqlCommand(getRunMetaData, clntConn);
                        clntConn.Open();

                        jobIFR.Parameters.AddWithValue("@jobID", SqlDbType.Int).Value = JobID.ToString();

                        using (SqlDataReader readIFR = jobIFR.ExecuteReader())
                        {
                            try
                            {
                                if (readIFR.HasRows)
                                {
                                    while (readIFR.Read())
                                    {
                                        jIFR = readIFR.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    jIFR = 1;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readIFR.Close();
                                readIFR.Dispose();
                            }                            
                        }
                        jobMetaD.Parameters.AddWithValue("@jobID", SqlDbType.Int).Value = JobID.ToString();

                        using (SqlDataReader readJobMetaD = jobMetaD.ExecuteReader())
                        {
                            try
                            {
                                if (readJobMetaD.HasRows)
                                {
                                    while (readJobMetaD.Read())
                                    {
                                        jAcelU = readJobMetaD.GetInt32(0);
                                        jMagU = readJobMetaD.GetInt32(1);
                                        jLenU = readJobMetaD.GetInt32(2);
                                        jConverg = readJobMetaD.GetInt32(3);
                                    }
                                }
                                else
                                {
                                    jAcelU = 3; //Acel. Due to Gravity
                                    jMagU = 1; //Gauss
                                    jLenU = 5; //Meter
                                    jConverg = 1; //True North                    
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readJobMetaD.Close();
                                readJobMetaD.Dispose();
                            }
                        }

                        jobRefMag.Parameters.AddWithValue("@jobID", SqlDbType.Int).Value = JobID.ToString();
                        using (SqlDataReader readJobRefMag = jobRefMag.ExecuteReader())
                        {
                            try
                            {
                                if (readJobRefMag.HasRows)
                                {
                                    while (readJobRefMag.Read())
                                    {
                                        rfmgID = readJobRefMag.GetInt32(0);
                                        mdec = readJobRefMag.GetDecimal(1);
                                        dip = readJobRefMag.GetDecimal(2);
                                        bTotal = readJobRefMag.GetDecimal(3);
                                        gTotal = readJobRefMag.GetDecimal(4);
                                        gConv = readJobRefMag.GetDecimal(5);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readJobRefMag.Close();
                                readJobRefMag.Dispose();
                            }
                        }

                        jobiRefMag.Parameters.AddWithValue("@rfmgID", SqlDbType.Int).Value = rfmgID.ToString();
                        using (SqlDataReader readJobiRefMag = jobiRefMag.ExecuteReader())
                        {
                            try
                            {
                                if (readJobiRefMag.HasRows)
                                {
                                    while (readJobiRefMag.Read())
                                    {
                                        imdec = readJobiRefMag.GetDecimal(0);
                                        idip = readJobiRefMag.GetDecimal(1);
                                        ibTotal = readJobiRefMag.GetDecimal(2);
                                        igTotal = readJobiRefMag.GetDecimal(3);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readJobiRefMag.Close();
                                readJobiRefMag.Dispose();
                            }
                        }

                        SQCTols.Parameters.AddWithValue("@sqcID", SqlDbType.Int).Value = SQCID.ToString();
                        using (SqlDataReader readSQCTol = SQCTols.ExecuteReader())
                        {
                            try
                            {
                                if (readSQCTol.HasRows)
                                {
                                    while (readSQCTol.Read())
                                    {
                                        sqcDip = readSQCTol.GetDecimal(0);
                                        sqcBTotal = readSQCTol.GetDecimal(1);
                                        sqcGTotal = readSQCTol.GetDecimal(2);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readSQCTol.Close();
                                readSQCTol.Dispose();
                            }
                        }

                        runMetaD.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readRunMeta = runMetaD.ExecuteReader())
                        {
                            try
                            {
                                if (readRunMeta.HasRows)
                                {
                                    while (readRunMeta.Read())
                                    {
                                        rInc = readRunMeta.GetDecimal(0);
                                        rAzm = readRunMeta.GetDecimal(1);
                                        rTInc = readRunMeta.GetDecimal(2);
                                        rTAzm = readRunMeta.GetDecimal(3);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readRunMeta.Close();
                                readRunMeta.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }
                
                Decimal cnvDip = -99.00M;
                Decimal cnvBT = -99.00M;
                Decimal cnvGT = -99.000000000M;
                Decimal cnvIFRGT = -99.00000000M;
                Decimal cnvIFRDip = -99.000000M;
                Decimal cnvIFRBT = -99.000000M;
                Decimal cnvSQCBTotal = -99.000000M;
                Decimal cnvSQCGTotal = -99.000000000M;

                cnvDip = Units.convDip(dip);
                cnvBT = Units.convBTotal(bTotal, jMagU);
                cnvIFRDip = Units.convDip(idip);
                cnvIFRBT = Units.convBTotal(ibTotal, jMagU);
                cnvGT = Units.convGTotal(gTotal, jAcelU);
                cnvIFRGT = Units.convGTotal(igTotal, jAcelU);
                cnvSQCBTotal = Units.convBTotal(sqcBTotal, jMagU);
                cnvSQCGTotal = Units.convGTotal(sqcGTotal, jAcelU);

                GxVal = resGx(cnvGT, srInc, srGTF);
                GyVal = resGy(cnvGT, srInc, srGTF);
                GzVal = resGz(cnvGT, srInc);
                colSVal = resColS(jIFR, mdec, gConv, imdec, srAzm);
                colTVal = resColT(jIFR, srAzm, mdec, imdec);
                colUVal = resColU(jConverg, colSVal, colTVal);
                BxVal = resBx(cnvBT, cnvDip, srInc, srGTF, colUVal);
                ByVal = resBy(cnvBT, srInc, cnvDip, srGTF, colUVal);
                BzVal = resBz(cnvBT, srInc, cnvDip, srGTF, colUVal);

                String BGs = "INSERT INTO [WELLPROFILE] (wpdsID, wpDepth, wpGx, wpGy, wpGz, wpBx, wpBy, wpBz, runID, srvyRowID, cTime, uTime) VALUES (@wpdsID, @wpDepth, @wpGx, @wpGy, @wpGz, @wpBx, @wpBy, @wpBz, @runID, @srvyRowID, @cTime, @uTime)";

                using (SqlConnection BGConn = new SqlConnection(GetClientDBString))                
                {
                    try
                    {
                        BGConn.Open();
                        SqlCommand insertBGs = new SqlCommand(BGs, BGConn);
                        insertBGs.Parameters.AddWithValue("@wpdsID", SqlDbType.Int).Value = wpID.ToString();
                        insertBGs.Parameters.AddWithValue("@wpDepth", SqlDbType.Decimal).Value = srDepth.ToString();
                        insertBGs.Parameters.AddWithValue("@wpGx", SqlDbType.Decimal).Value = GxVal.ToString();
                        insertBGs.Parameters.AddWithValue("@wpGy", SqlDbType.Decimal).Value = GyVal.ToString();
                        insertBGs.Parameters.AddWithValue("@wpGz", SqlDbType.Decimal).Value = GzVal.ToString();
                        insertBGs.Parameters.AddWithValue("@wpBx", SqlDbType.Decimal).Value = BxVal.ToString();
                        insertBGs.Parameters.AddWithValue("@wpBy", SqlDbType.Decimal).Value = ByVal.ToString();
                        insertBGs.Parameters.AddWithValue("@wpBz", SqlDbType.Decimal).Value = BzVal.ToString();
                        insertBGs.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        insertBGs.Parameters.AddWithValue("@srvyRowID", SqlDbType.Int).Value = srRowID.ToString();
                        insertBGs.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = cTime.ToString();
                        insertBGs.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = uTime.ToString();

                        insResult = (Int32)insertBGs.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        BGConn.Close();
                        BGConn.Dispose();
                    }
                }

                if (insResult.Equals(1))
                {                    
                    //Crediting User's Account for currently processed Survey Row
                    //wpResult = ACC.accWP(username, domain, wpID, srRowID, srDepth); //Accounting for Processing Well Profile Survey
                    bgResult = ACC.accBG(UserName, Domain, wpID, WellID, WellSectionID, RunID, bhaID, srRowID, srDepth); // Accounting for calculating BGs.

                    if (wpResult.Equals(1) && bgResult.Equals(1))
                    {
                        result = insResult;
                    }
                    else
                    {
                        result = -99;                        
                    }
                }
                return result;
            }
            catch(Exception ex)
            {throw new System.Exception(ex.ToString());}
        }
    }
}