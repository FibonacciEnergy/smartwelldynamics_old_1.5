﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using QC = SmartsVer1.QCHelpers.sdRawDataQC;
using OPR = SmartsVer1.Helpers.QCHelpers.Opr;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using DL = SmartsVer1.Helpers.DemogHelpers.DataLoader;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using Units = SmartsVer1.Helpers.DemogHelpers.UnitsConverter;
using Stat = MathNet.Numerics.Statistics.Statistics;
using MNFit = MathNet.Numerics.Fit;
using Gdns = MathNet.Numerics.GoodnessOfFit;

namespace SmartsVer1.Helpers.QCHelpers
{
    public class DipIncMSA
    {
        /// <summary>
        /// Implementing Patent 8 180 571 B2
        /// Wellbore Surveying
        /// Inventor: Anne Holmes
        /// </summary> 

        public static Decimal getHSG(Decimal Gx, Decimal Gy) //calculating highside-angle
        {
            try
            {
                Decimal iReply = -99.0000M;
                iReply = Convert.ToDecimal(Units.ToRadians(Math.Atan((Double)(Gy / (-1 * Gx)))));
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal getInclination(Decimal Gx, Decimal Gy, Decimal Gz) // calculating Inclination in process
        {
            try
            {
                Decimal iReply = -99.0000M;
                iReply = Convert.ToDecimal(Units.ToRadians(Math.Atan((Double)((Math.Pow(((Double)Gx * (Double)Gx) + ((Double)Gy * (Double)Gy), 0.5)) / (Double)Gz))));
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal correctedBx(Decimal measuredBx, Decimal biasBx, Decimal scalefactorBx) //calculating correctedBx
        {
            try
            {
                Decimal iReply = -99.0000M;
                Decimal sfBx = (1 - scalefactorBx); 
                if(sfBx.Equals(0))
                {
                    iReply = (measuredBx + biasBx);
                }
                else
                {
                    iReply = ((measuredBx / sfBx) + biasBx);
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal correctedBy(Decimal measuredBy, Decimal biasBy, Decimal scalefactorBy) //calculating correctedBy
        {
            try
            {
                Decimal iReply = -99.0000M;
                Decimal sfBy = (1 - scalefactorBy);
                if(sfBy.Equals(0))
                {
                    iReply = measuredBy + biasBy;
                }
                else
                {
                    iReply = ((measuredBy / sfBy) + biasBy);
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal correctedBz(Decimal measuredBz, Decimal biasBz) //calculating correctedBz
        {
            try
            {
                Decimal iReply = -99.0000M;
                iReply = Decimal.Add(measuredBz, biasBz);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal verticalComponentBv(Decimal correctedBx, Decimal correctedBy, Decimal correctedBz, Decimal highsideG, Decimal Inclination)
        {
            try
            {
                Decimal iReply = -99.0000M;
                iReply = Convert.ToDecimal((-1 * (Double)correctedBx * Math.Cos((Double)highsideG) * Math.Sin((Double)Inclination)) + ((Double)correctedBy *Math.Sin((Double)highsideG) * Math.Sin((Double)Inclination)) + ((Double)correctedBz * Math.Cos((Double)Inclination)));
                //iReply = (((-1 * correctedBx) * (Decimal)Math.Cos((Double)highsideG) * (Decimal)Math.Sin((Double)Inclination)) + (correctedBy * (Decimal)Math.Sin((Double)highsideG) * (Decimal)Math.Sin((Double)Inclination)) + (correctedBz * (Decimal)Math.Cos((Double)Inclination)));
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal horizontalComponentBn(Decimal correctedBx, Decimal correctedBy, Decimal correctedBz, Decimal verticalComponent)
        {
            try
            {
                Decimal iReply = -99.0000M;
                Double valA = Math.Sqrt(((Double)correctedBx * (Double)correctedBx) + ((Double)correctedBy * (Double)correctedBy) + ((Double)correctedBz * (Double)correctedBz));
                Double valB = ((Double)verticalComponent * (Double)verticalComponent);
                Double valC = valA - valB;
                iReply = Convert.ToDecimal(Math.Sqrt(valC));
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal Dip(Decimal verticalcomponentBv, Decimal horizontalcomponentBn)
        {
            try
            {
                Decimal iReply = -99.0000M;
                iReply = Convert.ToDecimal(Units.ToRadians(Math.Atan((Double)(verticalcomponentBv / horizontalcomponentBn))));
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal BTotal(Decimal Bx, Decimal By, Decimal Bz)
        {
            try
            {
                Decimal iReply = -99.0000M;
                iReply = Convert.ToDecimal(Math.Sqrt(((Double)Bx * (Double)Bx) + ((Double)By * (Double)By) + ((Double)Bz * (Double)Bz)));
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getShielding(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, System.Data.DataTable imDataTable, String ClientDBString) //Calculating Shielding/S/Variance value
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable(), transition = new System.Data.DataTable();
                
                DataColumn srID = iReply.Columns.Add("srID", typeof(Int32));
                DataColumn srDepth = iReply.Columns.Add("srDepth", typeof(Decimal));
                DataColumn srGx = iReply.Columns.Add("srGx", typeof(Decimal));
                DataColumn srGy = iReply.Columns.Add("srGy", typeof(Decimal));
                DataColumn srGz = iReply.Columns.Add("srGz", typeof(Decimal));
                DataColumn srBx = iReply.Columns.Add("srBx", typeof(Decimal));
                DataColumn srBy = iReply.Columns.Add("srBy", typeof(Decimal));
                DataColumn srBz = iReply.Columns.Add("srBz", typeof(Decimal));
                DataColumn trID = transition.Columns.Add("srID", typeof(Int32));
                DataColumn trDepth = transition.Columns.Add("srDepth", typeof(Decimal));
                DataColumn trGx = transition.Columns.Add("srGx", typeof(Decimal));
                DataColumn trGy = transition.Columns.Add("srGy", typeof(Decimal));
                DataColumn trGz = transition.Columns.Add("srGz", typeof(Decimal));
                DataColumn trBx = transition.Columns.Add("srBx", typeof(Decimal));
                DataColumn trBy = transition.Columns.Add("srBy", typeof(Decimal));
                DataColumn trBz = transition.Columns.Add("srBz", typeof(Decimal));
                DataColumn trBxBias = transition.Columns.Add("srBxBias", typeof(Decimal));
                DataColumn trBxSF = transition.Columns.Add("srBxSF", typeof(Decimal));
                DataColumn trByBias = transition.Columns.Add("srByBias", typeof(Decimal));
                DataColumn trBySF = transition.Columns.Add("srBySF", typeof(Decimal));
                DataColumn trBzBias = transition.Columns.Add("srBzBias", typeof(Decimal));
                DataColumn trShield = transition.Columns.Add("srShield", typeof(Decimal));
                Int32 sID = -99, imdecCID = -99;
                Decimal imDec = -99.00M, idip = -99.00M, iBTotal = -99.00M;
                Decimal depth = -99.00M, dip = -99.000000000000M, bt = -99.000000000000M, shielding = -99.0000M;
                Decimal gx = -99.00000000M, gy = -99.00000000M, gz = -99.00000000M, bx = -99.00000000M, by = -99.00000000M, bz = -99.00000000M;
                Decimal bxBias = -99.00000000M, bxSF = -99.00000000M, byBias = -99.00000000M, bySF = -99.00000000M, bzBias = -99.00000000M;
                ArrayList refMag = new ArrayList();
                refMag = OPR.getRefMagList(OperatorID, ClientID, WellPadID, WellID, ClientDBString);
                imDec = Convert.ToDecimal(refMag[1]);
                imdecCID = Convert.ToInt32(refMag[2]);
                idip = Convert.ToInt32(refMag[3]);
                iBTotal = Convert.ToDecimal(refMag[4]);
                foreach (System.Data.DataRow rows in imDataTable.Rows)
                {
                    sID = Convert.ToInt32(rows["srID"]);
                    depth = Convert.ToDecimal(rows["srDepth"]);
                    dip = Convert.ToDecimal(rows["srDip"]);
                    bt = Convert.ToDecimal(rows["srBTotal"]);
                    gx = Convert.ToDecimal(rows["iGx"]);
                    gy = Convert.ToDecimal(rows["iGy"]);
                    gz = Convert.ToDecimal(rows["iGz"]);
                    bx = Convert.ToDecimal(rows["iBx"]);
                    by = Convert.ToDecimal(rows["iBy"]);
                    bz = Convert.ToDecimal(rows["iBz"]);
                    bxBias = Convert.ToDecimal(rows["iBxBias"]);
                    bxSF = Convert.ToDecimal(rows["iBxSF"]);
                    byBias = Convert.ToDecimal(rows["iByBias"]);
                    bySF = Convert.ToDecimal(rows["iBySF"]);
                    bzBias = Convert.ToDecimal(rows["iBzBias"]);
                    shielding = ((Decimal)Math.Pow((((Double)iBTotal - (Double)bt) / (Double)iBTotal), 2) + ((Decimal)Math.Pow((((Double)idip - (Double)dip) / (Double)idip), 2)));
                    transition.Rows.Add(sID, depth, gx, gy, gz, bx, by, bz, bxBias, bxSF, byBias, bySF, bzBias, shielding);
                    transition.AcceptChanges();
                }
                iReply = getUpdatedBGValues(transition); 
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getUpdatedBGValues(System.Data.DataTable inputTable)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn updID = iReply.Columns.Add("updID", typeof(Int32));
                DataColumn updDepth = iReply.Columns.Add("updDepth", typeof(Decimal));
                DataColumn updGx = iReply.Columns.Add("updGx", typeof(Decimal));
                DataColumn updGy = iReply.Columns.Add("updGy", typeof(Decimal));
                DataColumn updGz = iReply.Columns.Add("updGz", typeof(Decimal));
                DataColumn updBx = iReply.Columns.Add("updBx", typeof(Decimal));
                DataColumn updBy = iReply.Columns.Add("updBy", typeof(Decimal));
                DataColumn updBz = iReply.Columns.Add("updBz", typeof(Decimal));
                List<Double> idList = new List<Double> { }, shldList = new List<Double> { };
                Double[] id = new Double[] { }, shld = new Double[] { };
                Double slope, intercept;
                Int32 nSID = -99;
                Decimal nDepth = -99.00M, nbx = -99.00000000M, nby = -99.00000000M, nbz = -99.00000000M, ngx = -99.00000000M, ngy = -99.00000000M, ngz = -99.00000000M;
                Decimal nBxBias = -99.00000000M, nBxSF = -99.00000000M, nByBias = -99.00000000M, nBySF = -99.00000000M, nBzBias = -99.00000000M, shielding = -99.00000000M;
                Decimal uBx = -99.00000000M, uBy = -99.00000000M, uBz = -99.00000000M, Sx = -99.00000000M, Sy = -99.00000000M;
                foreach (System.Data.DataRow row in inputTable.Rows)
                {
                    nSID = Convert.ToInt32(row["srID"]);
                    nDepth = Convert.ToDecimal(row["srDepth"]);
                    nbx = Convert.ToDecimal(row["srBx"]);
                    nby = Convert.ToDecimal(row["srBy"]);
                    nbz = Convert.ToDecimal(row["srBz"]);
                    ngx = Convert.ToDecimal(row["srGx"]);
                    ngy = Convert.ToDecimal(row["srGy"]);
                    ngz = Convert.ToDecimal(row["srGz"]);
                    nBxBias = Convert.ToDecimal(row["srBxBias"]);
                    nBxSF = Convert.ToDecimal(row["srBxSF"]);
                    nByBias = Convert.ToDecimal(row["srByBias"]);
                    nBySF = Convert.ToDecimal(row["srBySF"]);
                    nBzBias = Convert.ToDecimal(row["srBzBias"]);
                    shielding = Convert.ToDecimal(row["srShield"]);
                    idList.Add(Convert.ToDouble(nSID));
                    shldList.Add(Convert.ToDouble(shielding));
                    if (nSID.Equals(1))
                    {
                        iReply.Rows.Add(nSID, nDepth, ngx, ngy, ngz, nbx, nby, nbz);
                        iReply.AcceptChanges();
                    }
                    else
                    {
                        id = idList.ToArray();
                        shld = shldList.ToArray();
                        Tuple<Double, Double> s = MNFit.Line(id, shld);
                        intercept = s.Item1; //Intercept
                        slope = s.Item2; //Slope
                        Sx = ((Decimal)slope * nbx) + (Decimal)intercept;
                        uBx = (nbx / (1 - Sx)) + nBxBias;
                        Sy = ((Decimal)slope * nby) + (Decimal)intercept;
                        uBy = (nby / (1 - Sy)) + nByBias;
                        uBz = nbz + nBzBias;
                        iReply.Rows.Add(nSID, nDepth, ngx, ngy, ngz, uBx, uBy, uBz);
                        iReply.AcceptChanges();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getSolution(Int32 ServiceID, Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable(), rawData = new System.Data.DataTable(), imData = new System.Data.DataTable(), solTable = new System.Data.DataTable(), shieldingTable = new System.Data.DataTable();
                DataColumn srvRID = iReply.Columns.Add("srvRID", typeof(Int32));
                DataColumn Depth = iReply.Columns.Add("Depth", typeof(Decimal));
                DataColumn Inc = iReply.Columns.Add("Inc", typeof(Decimal));
                DataColumn LCAzm = iReply.Columns.Add("LCAzm", typeof(Decimal));
                DataColumn SCAzm = iReply.Columns.Add("SCAzm", typeof(Decimal));
                DataColumn CrrAzm = iReply.Columns.Add("CrrAzm", typeof(Decimal));
                DataColumn valDip = iReply.Columns.Add("valDip", typeof(Decimal));
                DataColumn valBTotal = iReply.Columns.Add("valBTotal", typeof(Decimal));
                DataColumn valGTotal = iReply.Columns.Add("valGTotal", typeof(Decimal));
                DataColumn sID = rawData.Columns.Add("sID", typeof(Int32));
                DataColumn rDepth = rawData.Columns.Add("rDepth", typeof(Decimal));
                DataColumn rBx = rawData.Columns.Add("rBx", typeof(Decimal));
                DataColumn rBy = rawData.Columns.Add("rBy", typeof(Decimal));
                DataColumn rBz = rawData.Columns.Add("rBz", typeof(Decimal));
                DataColumn rGx = rawData.Columns.Add("rGx", typeof(Decimal));
                DataColumn rGy = rawData.Columns.Add("rGy", typeof(Decimal));
                DataColumn rGz = rawData.Columns.Add("rGz", typeof(Decimal));
                DataColumn BM = rawData.Columns.Add("BM", typeof(Decimal));
                DataColumn BN = rawData.Columns.Add("BN", typeof(Decimal));
                DataColumn BO = rawData.Columns.Add("BO", typeof(Decimal));
                DataColumn BP = rawData.Columns.Add("BP", typeof(Decimal));
                DataColumn BzBias = rawData.Columns.Add("BzBias", typeof(Decimal));
                DataColumn BzSF = rawData.Columns.Add("BzSF", typeof(Decimal));
                DataColumn valDipC = rawData.Columns.Add("valDipC", typeof(Decimal));
                DataColumn valDD = rawData.Columns.Add("valDD", typeof(Decimal));
                DataColumn srID = imData.Columns.Add("srID", typeof(Int32));
                DataColumn srDepth = imData.Columns.Add("srDepth", typeof(Decimal));
                DataColumn iBx = imData.Columns.Add("iBx", typeof(Decimal));
                DataColumn iBy = imData.Columns.Add("iBy", typeof(Decimal));
                DataColumn iBz = imData.Columns.Add("iBz", typeof(Decimal));
                DataColumn iGx = imData.Columns.Add("iGx", typeof(Decimal));
                DataColumn iGy = imData.Columns.Add("iGy", typeof(Decimal));
                DataColumn iGz = imData.Columns.Add("iGz", typeof(Decimal));
                DataColumn iBxBias = imData.Columns.Add("iBxBias", typeof(Decimal));
                DataColumn iBxSF = imData.Columns.Add("iBxSF", typeof(Decimal));
                DataColumn iByBias = imData.Columns.Add("iByBias", typeof(Decimal));
                DataColumn iBySF = imData.Columns.Add("iBySF", typeof(Decimal));
                DataColumn iBzBias = imData.Columns.Add("iBzBias", typeof(Decimal));
                DataColumn srHsg = imData.Columns.Add("srHsg", typeof(Decimal));
                DataColumn srInc = imData.Columns.Add("srInc", typeof(Decimal));
                DataColumn srBv = imData.Columns.Add("srBv", typeof(Decimal));
                DataColumn srBn = imData.Columns.Add("srBn", typeof(Decimal));
                DataColumn srDip = imData.Columns.Add("srDip", typeof(Decimal));
                DataColumn srBTotal = imData.Columns.Add("srBTotal", typeof(Decimal));
                DataColumn azmLCC = imData.Columns.Add("azmLCC", typeof(Decimal));
                DataColumn azmSCC = imData.Columns.Add("azmSCC", typeof(Decimal));
                DataColumn stSID = solTable.Columns.Add("stID", typeof(Int32));
                DataColumn stDepth = solTable.Columns.Add("stDepth", typeof(Decimal));
                DataColumn stBx = solTable.Columns.Add("stBx", typeof(Decimal));
                DataColumn stBy = solTable.Columns.Add("stBy", typeof(Decimal));
                DataColumn stBz = solTable.Columns.Add("stBz", typeof(Decimal));
                DataColumn stGx = solTable.Columns.Add("stGx", typeof(Decimal));
                DataColumn stGy = solTable.Columns.Add("stGy", typeof(Decimal));
                DataColumn stGz = solTable.Columns.Add("stGz", typeof(Decimal));
                DataColumn stInc = solTable.Columns.Add("stInc", typeof(Decimal));
                DataColumn stAzmLcc = solTable.Columns.Add("stAzmLcc", typeof(Decimal));
                DataColumn stAzmScc = solTable.Columns.Add("stAzmScc", typeof(Decimal));
                DataColumn shID = shieldingTable.Columns.Add("shID", typeof(Int32));
                DataColumn shDepth = shieldingTable.Columns.Add("shDepth", typeof(Decimal));
                DataColumn shValue = shieldingTable.Columns.Add("shValue", typeof(Decimal));
                Int32 rowID = -99, imdecCID = -99;
                Decimal valDepth = -99.00M, valBx = -99.000000000000M, valBy = -99.000000000000M, valBz = -99.000000000000M, valGx = -99.000000000000M, valGy = -99.000000000000M, valGz = -99.000000000000M;
                Decimal rHsg = -99.000000000000M, rInc = -99.000000000000M, rBv = -99.000000000000M, rBn = -99.000000000000M, rDip = -99.000000000000M, rBTotal = -99.000000000000M, lcAzm = -99.0000M, scAzm = -99.0000M;
                Decimal biasBx = -99.000000000000M, biasBy = -99.000000000000M, biasBz = -99.000000000000M, scalefactorBx = -99.000000000000M, scalefactorBy = -99.000000000000M;
                Decimal crBx = -99.000000000000M, crBy = -99.000000000000M, crBz = -99.000000000000M;
                Decimal imDec = -99.00M, idip = -99.00M, iBTotal = -99.00M, iGTotal = -99.00M;
                ArrayList refMag = new ArrayList();
                refMag = OPR.getRefMagList(OperatorID, ClientID, WellPadID, WellID, ClientDBConnection);
                imDec = Convert.ToDecimal(refMag[1]);
                imdecCID = Convert.ToInt32(refMag[2]);
                idip = Convert.ToDecimal(refMag[3]);
                iBTotal = Convert.ToDecimal(refMag[4]);
                iGTotal = Convert.ToDecimal(refMag[6]);
                    
                rawData = getSolutionData(OperatorID, ClientID, WellPadID, WellID, WellSectionID, RunID, ClientDBConnection);
                foreach (System.Data.DataRow row in rawData.Rows)
                {
                    rowID = Convert.ToInt32(row["srvRID"]);
                    valDepth = Convert.ToDecimal(row["Depth"]);
                    valBx = Convert.ToDecimal(row["Bx"]);
                    valBy = Convert.ToDecimal(row["By"]);
                    valBz = Convert.ToDecimal(row["Bz"]);
                    valGx = Convert.ToDecimal(row["Gx"]);
                    valGy = Convert.ToDecimal(row["Gy"]);
                    valGz = Convert.ToDecimal(row["Gz"]);
                    biasBx = Convert.ToDecimal(row["BM"]);
                    scalefactorBx = Convert.ToDecimal(row["BN"]);
                    biasBy = Convert.ToDecimal(row["BO"]);
                    scalefactorBy = Convert.ToDecimal(row["BP"]);
                    biasBz = Convert.ToDecimal(row["BzBias"]);
                    rDip = Convert.ToDecimal(row["valDipC"]);
                    lcAzm = Convert.ToDecimal(row["valFinalAzm"]);
                    scAzm = Convert.ToDecimal(row["valDD"]); //Short-Collar Azm
                    rHsg = getHSG(valGx, valGy);
                    rInc = getInclination(valGx, valGy, valGz);
                    crBx = correctedBx(valBx, biasBx, scalefactorBx);
                    crBy = correctedBy(valBy, biasBy, scalefactorBy);
                    crBz = correctedBz(valBz, biasBz);
                    rBTotal = BTotal(valBx, valBy, valBz); 
                    //rBTotal = BTotal(crBx, crBy, crBz); //using corrected Bs as a test  
                    
                    rBv = iBTotal * (Decimal)Math.Sin((Double)rDip);
                    rBn = iBTotal * (Decimal)Math.Cos((Double)rDip);
                    
                    //rBv = verticalComponentBv(crBx, crBy, crBz, rHsg, rInc); //vertical Component from Patent                    
                    //rBn = horizontalComponentBn(crBx, crBy, crBz, rBv); //horizontal Component from Patent
                    
                    rDip = Dip(rBv, rBn);                    
                    imData.Rows.Add(rowID, valDepth, valBx, valBy, valBz, valGx, valGy, valGz, biasBx, scalefactorBx, biasBy, scalefactorBy, biasBz, rHsg, rInc, rBv, rBn, rDip, rBTotal, lcAzm, scAzm);
                    imData.AcceptChanges();
                    solTable.Rows.Add(rowID, valDepth, crBx, crBy, crBz, valGx, valGy, valGz, rInc, lcAzm, scAzm);
                    solTable.AcceptChanges();
                }
                ////////Send imData to sheilding method and do qc for corrected Azm
                shieldingTable = getShielding(OperatorID, ClientID, WellPadID, WellID, WellSectionID, RunID, imData, ClientDBConnection);
                iReply = getUpdatedValues(ServiceID, OperatorID, ClientID, WellPadID, WellID, WellSectionID, RunID, solTable, shieldingTable, ClientDBConnection);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static System.Data.DataTable getUpdatedValues(Int32 ServiceID, Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, System.Data.DataTable solData, System.Data.DataTable Shielding, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn srvRID = iReply.Columns.Add("srvRID", typeof(Int32));
                DataColumn Depth = iReply.Columns.Add("Depth", typeof(Decimal));
                DataColumn nGx = iReply.Columns.Add("Gx", typeof(Decimal));
                DataColumn nGy = iReply.Columns.Add("Gy", typeof(Decimal));
                DataColumn nGz = iReply.Columns.Add("Gz", typeof(Decimal));
                DataColumn nBx = iReply.Columns.Add("Bx", typeof(Decimal));
                DataColumn nBy = iReply.Columns.Add("By", typeof(Decimal));
                DataColumn nBz = iReply.Columns.Add("Bz", typeof(Decimal));
                DataColumn LCAzm = iReply.Columns.Add("LCAzm", typeof(Decimal));
                DataColumn SCAzm = iReply.Columns.Add("SCAzm", typeof(Decimal));
                DataColumn CrrAzm = iReply.Columns.Add("CrrAzm", typeof(Decimal));
                DataColumn valInc = iReply.Columns.Add("valInc", typeof(Decimal));
                DataColumn valDip = iReply.Columns.Add("valDip", typeof(Decimal));
                DataColumn valBTotal = iReply.Columns.Add("valBTotal", typeof(Decimal));
                DataColumn valGTotal = iReply.Columns.Add("valGTotal", typeof(Decimal));
                Int32 imdecCID = -99, rsID = -99;
                Decimal updBx = -99.0000M, updBy = -99.0000M, updBz = -99.0000M, rsDepth = -99.00M;
                Decimal rBx = 99.0000M, rBy = -99.0000M, rBz = -99.0000M, rGx = -99.0000M, rGy = -99.0000M, rGz = -99.0000M, rInc = -99.0000M, azmLC = -99.0000M, azmSC = -99.0000M;
                Decimal imDec = -99.00M, idip = -99.00M, iBTotal = -99.00M, iGTotal = -99.00M;
                Decimal updDip = -99.0000M, updBTotal = -99.0000M;
                Int32 ifrVal = -99, AccelU = -99, MagU = -99, jConverg = -99, LngiEW = -99, LatiNS = -99, lwrSQCBT = -99, uprSQCBT = -99, mdecCID = -99, gcoID = -99;
                Decimal lwrSQCDip = -99.000000M, lwrSQCGT = -99.00000000M, uprSQCDip = -99.0000M, uprSQCGT = -99.000000M;
                Decimal mDec = -99.00M, dip = -99.0000M, bTotal = -99.00M, gTotal = -99.000000M, gconv = -99.0000M;
                Decimal rAzm = -99.000000M, rTInc = -99.000000M, rTAzm = -99.000000M;
                Decimal MSize = -99.00M; // BHA Motor Size
                Int32 MSizeU = -99; // BHA Motor Size Unit
                Decimal DSCSize = -99.00M; // Drill String/Collar Size
                Int32 DSCSizeU = -99; // DSC Size Unit
                Decimal MDBLen = -99.00M; // Motor + Drill String Size
                Int32 MDBU = -99; // Motor + Drill String Size Unit
                Decimal NMSpAbv = -99.00M; // Non-Mag Space (Above)
                Int32 NMSpAbvU = -99;
                Decimal NMSpBlw = -99.00M; // Non-Mag Space (Below)
                Int32 NMSpBlwU = -99;
                Decimal NMSpAlw = -99.00M; // Non-Mag Space (Allowable)
                Decimal MGValue = -99.000000M; // Gaussmeter Reading (Motor)
                Int32 MGVU = -99;
                Decimal DGTop = -99.00M; //Distance from Top
                Int32 DGTU = -99;
                Decimal DSGValue = -99.000000M; // Drill String Gaussmeter Reading
                Int32 DSGU = -99;
                Decimal DGBtm = -99.00M; //Distance from Bottom
                Int32 DGBTU = -99;
                ArrayList BHAInfo = new ArrayList();
                List<Decimal> gbList = new List<Decimal>(), hzList = new List<Decimal>(), htList = new List<Decimal>();
                List<Double> srvIDValues = new List<Double>(), htValues = new List<Double>();                
                List<String> WellInfo = AST.getWellDetailList(OperatorID, ClientID, WellID, ClientDBConnection);
                LngiEW = Convert.ToInt32(WellInfo[3]);
                LatiNS = Convert.ToInt32(WellInfo[6]);
                ArrayList csqc = new ArrayList();
                csqc = DMG.getCorrSQCList();
                lwrSQCBT = Convert.ToInt32(csqc[1]);
                lwrSQCDip = Convert.ToDecimal(csqc[2]);
                lwrSQCGT = Convert.ToDecimal(csqc[3]);
                uprSQCBT = Convert.ToInt32(csqc[6]);
                uprSQCDip = Convert.ToDecimal(csqc[7]);
                uprSQCGT = Convert.ToDecimal(csqc[8]);
                ArrayList RefMags = new ArrayList();
                RefMags = OPR.getRefMagList(OperatorID, ClientID, WellPadID, WellID, ClientDBConnection);
                ifrVal = Convert.ToInt32(RefMags[0]);
                AccelU = Convert.ToInt32(RefMags[7]);
                MagU = Convert.ToInt32(RefMags[5]);
                jConverg = Convert.ToInt32(RefMags[10]);
                mDec = Convert.ToDecimal(RefMags[1]);
                imDec = Convert.ToDecimal(RefMags[2]);
                dip = Convert.ToDecimal(RefMags[3]);
                idip = Convert.ToDecimal(RefMags[3]);
                bTotal = Convert.ToDecimal(RefMags[4]);
                iBTotal = Convert.ToDecimal(RefMags[4]);
                gTotal = Convert.ToDecimal(RefMags[6]);
                iGTotal = Convert.ToDecimal(RefMags[6]);
                gconv = Convert.ToDecimal(RefMags[8]);
                mdecCID = Convert.ToInt32(RefMags[2]);
                imdecCID = Convert.ToInt32(RefMags[2]);
                gcoID = Convert.ToInt32(RefMags[9]);
                ArrayList RunInfo = AST.getRunList(RunID, ClientDBConnection);
                rInc = Convert.ToDecimal(RunInfo[0]);
                rAzm = Convert.ToDecimal(RunInfo[1]);
                rTInc = Convert.ToDecimal(RunInfo[2]);
                rTAzm = Convert.ToDecimal(RunInfo[3]);
                BHAInfo = AST.getBhaInfoList(RunID, ClientDBConnection);
                MSize = Convert.ToDecimal(BHAInfo[0]);
                MSizeU = Convert.ToInt32(BHAInfo[1]);
                DSCSize = Convert.ToDecimal(BHAInfo[2]);
                DSCSizeU = Convert.ToInt32(BHAInfo[3]);
                MDBLen = Convert.ToDecimal(BHAInfo[4]);
                MDBU = Convert.ToInt32(BHAInfo[5]);
                NMSpAbv = Convert.ToDecimal(BHAInfo[6]);
                NMSpAbvU = Convert.ToInt32(BHAInfo[7]);
                NMSpBlw = Convert.ToDecimal(BHAInfo[8]);
                NMSpBlwU = Convert.ToInt32(BHAInfo[9]);
                MGValue = Convert.ToDecimal(BHAInfo[11]);
                MGVU = Convert.ToInt32(BHAInfo[12]);
                DGTop = Convert.ToDecimal(BHAInfo[13]);
                DGTU = Convert.ToInt32(BHAInfo[14]);
                DSGValue = Convert.ToDecimal(BHAInfo[15]);
                DSGU = Convert.ToInt32(BHAInfo[16]);
                DGBtm = Convert.ToDecimal(BHAInfo[17]);
                DGBTU = Convert.ToInt32(BHAInfo[18]);
                Int32 valFX = -99, valDH = -99;
                Decimal mtf = -99.0000M, gtf = -99.0000M, cnvDip = -99.00M, cnvBT = -99.00M, cnvGT = -99.000000000M, cnvIFRGT = -99.00000000M, cnvIFRDip = -99.000000M, cnvIFRBT = -99.000000M;
                Decimal cnvSQCBTotal = -99.000000M, cnvSQCGTotal = -99.000000000M, valGB = -99.00000000M;
                Decimal valBK = -99.00000000M, valCK = -99.00000000M, valY = -99.00000000M, valCL = -99.00000000M, valCM = -99.00000000M, valCS = -99.00000000M, tsBias = -99.000000M;
                Decimal valCF = -99.00000000M, valCN = -99.00000000M, valBJ = -99.00000000M, valBI = -99.00000000M, valHT = -99.00000000M, valCT = -99.00000000M, valCU = -99.00000000M, valDI = -99.00000000M;
                Decimal cnvMSize = -99.00M, cnvNMSpAbv = -99.00M, cnvNMSpBlw = -99.00M, cnvMDBLen = -99.00M, shieldingValue = -99.0000M;
                Decimal cnvMGValue = -99.00000000M, cnvDSSize = -99.00M, cnvDSGValue = -99.00000000M, valGA = -99.00000000M, valFW = -99.00000000M, valFZ = -99.00000000M, valGZ = -99.00000000M;
                Decimal valCV = -99.00000000M, valCX = -99.00000000M, valCY = -99.00000000M, valCZ = -99.00000000M, valDA = -99.00000000M, valDB = -99.00000000M, valDC = -99.00000000M, valDD = -99.00000000M;
                Decimal cnvDGTop = -99.00M; //Distance of GMeter from Top of Motor
                Decimal cnvDGBtm = -99.00M; //Distance of GMeter from Drill String/Collar                                         
                cnvDip = Units.convDip(dip);
                cnvBT = Units.convBTotal(bTotal, MagU);
                cnvIFRDip = Units.convDip(idip);
                cnvIFRBT = Units.convBTotal(iBTotal, MagU);
                cnvGT = Units.convGTotal(gTotal, AccelU);
                cnvIFRGT = Units.convGTotal(iGTotal, AccelU);
                cnvSQCBTotal = Units.convBTotal(bTotal, MagU);
                cnvSQCGTotal = Units.convGTotal(gTotal, AccelU);
                cnvMSize = Units.convMotorSize(MSize, MSizeU);
                cnvMDBLen = Units.convMDLen(MDBLen, MDBU);
                cnvNMSpAbv = Units.convNMSp(NMSpAbv, NMSpAbvU);
                cnvNMSpBlw = Units.convNMSp(NMSpBlw, NMSpBlwU);
                cnvMGValue = Units.convGMeter(MGValue, MGVU);
                cnvDSSize = Units.convDSCSize(DSCSize, DSCSizeU);
                cnvDSGValue = Units.convGMeter(DSGValue, DSGU);
                cnvDGTop = Units.convGMtrDistance(DGTop, DGTU);
                cnvDGBtm = Units.convGMtrDistance(DGBtm, DGBTU);

                foreach (System.Data.DataRow row in solData.Rows)
                {
                    rsID = Convert.ToInt32(row["stID"]);
                    srvIDValues.Add((Double)rsID);
                    rsDepth = Convert.ToDecimal(row["stDepth"]);
                    rBx = Convert.ToDecimal(row["stBx"]);
                    rBy = Convert.ToDecimal(row["stBy"]);
                    rBz = Convert.ToDecimal(row["stBz"]);
                    rGx = Convert.ToDecimal(row["stGx"]);
                    rGy = Convert.ToDecimal(row["stGy"]);
                    rGz = Convert.ToDecimal(row["stGz"]);
                    rInc = Convert.ToDecimal(row["stInc"]);
                    azmLC = Convert.ToDecimal(row["stAzmLcc"]);
                    azmSC = Convert.ToDecimal(row["stAzmScc"]);


                        updBx = (rBx / (1 - shieldingValue));
                        updBy = (rBy / (1 - shieldingValue));
                        updBz = ((iBTotal * (Decimal)Math.Cos((Double)idip) * (Decimal)Math.Sin((Double)rInc) * (Decimal)Math.Cos((Double)azmSC)) + (iBTotal * (Decimal)Math.Sin((Double)idip) * (Decimal)Math.Cos((Double)rInc)));
                        updBTotal = QC.colBU(updBx, updBy, updBz);

                        mtf = QC.colQ(rBx, rBy);
                        gtf = QC.getGTF(rGx, rGy);
                        valY = QC.colY(gtf, rInc, rBx, rBy, rBz);
                        valBK = QC.colBK(ifrVal, cnvDip, cnvBT, cnvIFRDip, updBTotal, rInc, valY);
                        valCK = QC.colCK(ifrVal, rBz, rInc, valY, valBK, cnvDip, cnvIFRDip, cnvBT, updBTotal);
                        valCL = QC.colCL(ifrVal, rInc, valY, valCK, cnvDip, cnvIFRDip);
                        valCM = QC.colCM(valY, valCL);
                        valCN = QC.colCN(ifrVal, rInc, valCM, cnvDip, cnvIFRDip, cnvBT, updBTotal);
                        valCS = QC.colCS(rBz, valCN);
                        valBI = QC.colBI(ifrVal, cnvBT, updBTotal, cnvDip, cnvIFRDip, valY, gtf, rInc);
                        valBJ = QC.colBJ(ifrVal, cnvDip, cnvBT, cnvIFRDip, updBTotal, gtf, rInc, valY);
                        valFW = QC.colFW(cnvMSize);
                        valFX = QC.colFX(cnvDSSize);
                        valFZ = QC.colFZ(cnvNMSpAbv, valFX);
                        valGA = QC.colGA(cnvNMSpBlw, cnvMDBLen, valFW);
                        valGB = QC.colGB(valGZ, valGA);
                        gbList.Add(valGB);
                        tsBias = QC.biasCheck(ServiceID, rsID, WellID, WellSectionID, RunID, valGB, gbList, ClientDBConnection);
                        valHT = QC.colHT(rBx, rBy, rBz, valBI, valBJ, valCN, valCS, tsBias);
                        htList.Add(valHT);
                        htValues.Add((Double)valHT);
                        valCT = QC.colCT(ServiceID, rsID, srvIDValues, WellID, WellSectionID, RunID, valHT, htValues, ClientDBConnection);
                        valCU = QC.colCU(updBz, valCT);
                        valDH = QC.colDH(updBx, updBy, valCU);
                        valCF = QC.colCF(rGx, rGy, rGz);
                        valDI = QC.colDI(rGx, rGy, rGz, updBx, updBy, valCF, valCU, valDH);
                        valCV = QC.colCV(updBx, updBy, gtf, rInc, valCU);
                        valCX = QC.colCX(mDec, gconv, valCV, LatiNS, mdecCID, gcoID);
                        valCY = QC.colCY(imDec, gconv, valCV, LatiNS, imdecCID, gcoID);
                        valCZ = QC.colCZ(mdecCID, valCV, mdecCID);
                        valDA = QC.colDA(imDec, valCV, imdecCID);
                        valDB = QC.colDB(ifrVal, valCX, valCY);
                        valDC = QC.colDC(ifrVal, valCZ, valDA);
                        valDD = QC.colDD(jConverg, valDB, valDC);
                        iReply.Rows.Add(rsID, rsDepth, rGx, rGy, rGz, updBx, updBy, updBz, azmLC, azmSC, valDD, rInc, valDI, updBTotal, valCF);
                    //}
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static ArrayList processRawData(Int32 DataEntryMethodID, Int32 ServiceID, Int32 DataFileID, String FileName, String FileExtension, Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, List<System.Data.DataTable> InputDataTables, String UserName, String UserRealm, String ClientDBConnection)
        {
            try
            {
                ArrayList iReply = new ArrayList();
                Int32 iReplyF = -99, iReply5 = -99, iReplyData = -99;
                System.Data.DataTable fileHeader = new System.Data.DataTable(), fileData = new System.Data.DataTable();
                if (DataEntryMethodID.Equals(1))
                {
                    //fileID = 1 for manual entry data
                    fileData = (System.Data.DataTable)InputDataTables[2];
                }
                else
                {
                    //fileHeader = (System.Data.DataTable)InputDataTables[0];
                    //iReplyH = DL.insertDataFileHeader(ClientID, fileHeader, ClientDBConnection);
                    fileData = (System.Data.DataTable)InputDataTables[2];
                    iReplyF = DL.insertDataFileRows(OperatorID, ClientID, WellPadID, WellID, WellSectionID, RunID, ServiceID, DataEntryMethodID, DataFileID, fileData, UserName, UserRealm, ClientDBConnection);
                    //if (iReplyH.Equals(1) && iReplyF.Equals(1))
                    if (iReplyF.Equals(1))
                    {
                        iReply5 = fileData.Rows.Count;
                        //iReply12 = getMinRowCount(JobID, WellID, WellSectionID, RunID, ClientDBConnection);
                        iReply.Add(iReply5);
                        iReplyData = InputToAnalysis(ServiceID, OperatorID, ClientID, WellPadID, WellID, WellSectionID, RunID, UserName, UserRealm, ClientDBConnection);
                        iReply.Add(iReplyData);
                    }
                    else
                    {
                        iReply.Add(-1);
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Calculates Input values (Bs - converted to selected units (nT), MTF, GTF and B-Total for each Survey row for Correction analysis
        public static Int32 InputToAnalysis(Int32 ServiceID, Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, String UserName, String UserRealm, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                System.Data.DataTable rawData = new System.Data.DataTable();
                rawData = getAnalysisRawData(WellID, WellSectionID, RunID, ClientDBConnection);
                Decimal rDepth = -99.00M, rGx, rGy, rGz, rBx, rBy, rBz;
                Int32 sID = -99, ifrVal = -99, AccelU = -99, MagU = -99, LenU = -99, jConverg = -99, quad = -99, lwrSQCBT = -99, uprSQCBT = -99;
                Int32 valFX = -99;
                Decimal mDec = -99.00M, dip = -99.00M, bTotal = -99.000000M, gTotal = -99.000000M, gconv = -99.00M, imDec = -99.00M, idip = -99.00M, iBTotal = -99.00M, iGTotal = -99.00M;
                Decimal refGx = -99.000000000000M, refGy = -99.000000000000M, refGz = -99.000000000000M, refBx = -99.000000000000M, refBy = -99.000000000000M, refBz = -99.000000000000M;
                Decimal valGTF = -99.000000000000M, valMTF = -99.000000000000M, valDeclination = -99.000000000000M, valInclination = -99.000000000000M;
                Decimal valBoxy = -99.000000000000M, valBTotal = -99.000000000000M, valGoxy = -99.000000000000M, valGTotal = -99.000000000000M;
                Decimal valDip = -99.000000000000M, valMagAzimuth = -99.000000000000M, plnGridAzimuth = -99.000000000000M, ifrGridAzimuth = -99.000000000000M, valHQ = -99.000000000000M;
                Decimal plnTrueAzimuth = -99.000000000000M, ifrTrueAzimuth = -99.000000000000M, gridAzimuth = -99.000000000000M, trueAzimuth = -99.000000000000M, finalAzimuth = -99.000000000000M;
                Decimal valFW = -99.000000000000M, valFZ = -99.000000000000M, valGA = -99.000000000000M, valGB = -99.000000000000M;
                Decimal valCD = -99.000000000000M, valCF = -99.000000000000M, valCG = -99.000000000000M, valCL = -99.000000000000M, valCK = -99.000000000000M, valBM = -99.000000000000M, valCM = -99.000000000000M, valCN = -99.000000000000M;
                Decimal valBO = -99.000000000000M, valBP = -99.000000000000M, valBN = -99.000000000000M, valBQ = -99.000000000000M, valBR = -99.000000000000M, valHR = -99.000000000000M;
                Decimal valCS = -99.000000000000M, valHT = -99.000000000000M, valCT = -99.000000000000M, valCU = -99.000000000000M, valCV = -99.000000000000M, valCX = -99.000000000000M;
                Decimal valCY = -99.000000000000M, valCZ = -99.000000000000M, valDA = -99.000000000000M, valDB = -99.000000000000M, valDC = -99.000000000000M, valDD = -99.000000000000M;
                Decimal valBU = -99.000000000000M, valBV = -99.000000000000M, valBW = -99.000000000000M, valBX = -99.000000000000M;
                Decimal valBzBias = -99.000000000000M, valBzBiasConditional = -99.000000000000M, valBzScaleFactor = -99.000000000000M, valBzCorrected = -99.000000000000M, valDeltaBzCorrected = -99.000000000000M;
                Decimal valBTotalCorrected = -99.0000000000000M, valDeltaBTotalCorrect = -99.000000000000M, valDipCorrected = -99.000000000000M, valDeltaDipCorrected = -99.000000000000M;
                Decimal lwrSQCDip = -99.00M, uprSQCDip, lwrSQCGT = -99.000M, uprSQCGT = -99.000M, TSBias = -99.00M;
                Decimal MSize = -99.00M; // BHA Motor Size
                Int32 MSizeU = -99; // BHA Motor Size Unit
                Decimal DSCSize = -99.00M; // Drill String/Collar Size
                Int32 DSCSizeU = -99; // DSC Size Unit
                Decimal MDBLen = -99.00M; // Motor + Drill String Size
                Int32 MDBU = -99; // Motor + Drill String Size Unit
                Decimal NMSpAbv = -99.00M; // Non-Mag Space (Above)
                Int32 NMSpAbvU = -99;
                Decimal NMSpBlw = -99.00M; // Non-Mag Space (Below)
                Int32 NMSpBlwU = -99;
                Decimal NMSpAlw = -99.00M; // Non-Mag Space (Allowable)
                Decimal MGValue = -99.000000M; // Gaussmeter Reading (Motor)
                Int32 MGVU = -99;
                Decimal DGTop = -99.00M; //Distance from Top
                Int32 DGTU = -99;
                Decimal DSGValue = -99.000000M; // Drill String Gaussmeter Reading
                Int32 DSGU = -99;
                Decimal DGBtm = -99.00M; //Distance from Bottom
                Int32 DGBTU = -99;
                Int32 LatiNS = -99, LngiEW = -99, mdecCID = -99, imdecCID = -99, gcoID = -99;
                Decimal rInc = -99.00M, rAzm = -99.00M, rTInc = -99.00M, rTAzm = -99.00M, convRTAzm = -99.00M, convRTAzmIFR = -99.00M, convPTInc = -99.00M, convPTAzm = -99.00M;
                //Collecting Meta information
                ArrayList BHAInfo = new ArrayList();
                List<Decimal> gbList = new List<Decimal>(), htList = new List<Decimal>();
                List<Double> srvyIDValues = new List<Double>(), cnValues = new List<Double>(), hqValues = new List<Double>(), hrValues = new List<Double>(), htValues = new List<Double>();
                List<Double> bxValues = new List<Double>(), byValues = new List<Double>(), bzValues = new List<Double>(), biValues = new List<Double>(), bjValues = new List<Double>();
                List<String> WellInfo = AST.getWellDetailList(OperatorID, ClientID, WellID, ClientDBConnection);
                LngiEW = Convert.ToInt32(WellInfo[3]);
                LatiNS = Convert.ToInt32(WellInfo[6]);
                ArrayList csqc = new ArrayList();
                csqc = DMG.getCorrSQCList();
                lwrSQCBT = Convert.ToInt32(csqc[1]);
                lwrSQCDip = Convert.ToDecimal(csqc[2]);
                lwrSQCGT = Convert.ToDecimal(csqc[3]);
                uprSQCBT = Convert.ToInt32(csqc[6]);
                uprSQCDip = Convert.ToDecimal(csqc[7]);
                uprSQCGT = Convert.ToDecimal(csqc[8]);
                ArrayList RefMags = new ArrayList();
                RefMags = OPR.getRefMagList(OperatorID, ClientID, WellPadID, WellID, ClientDBConnection);
                ifrVal = Convert.ToInt32(RefMags[0]);
                mDec = Convert.ToDecimal(RefMags[1]);
                imDec = Convert.ToDecimal(RefMags[1]);
                mdecCID = Convert.ToInt32(RefMags[2]);
                imdecCID = Convert.ToInt32(RefMags[2]);
                dip = Convert.ToDecimal(RefMags[3]);
                idip = Convert.ToDecimal(RefMags[3]);
                bTotal = Convert.ToDecimal(RefMags[4]);
                iBTotal = Convert.ToDecimal(RefMags[4]);
                MagU = Convert.ToInt32(RefMags[5]);
                gTotal = Convert.ToDecimal(RefMags[6]);
                iGTotal = Convert.ToDecimal(RefMags[6]);
                AccelU = Convert.ToInt32(RefMags[7]);
                gconv = Convert.ToDecimal(RefMags[8]);
                gcoID = Convert.ToInt32(RefMags[9]);
                jConverg = Convert.ToInt32(RefMags[10]); 
                ArrayList RunInfo = AST.getRunList(RunID, ClientDBConnection);
                rInc = Convert.ToDecimal(RunInfo[0]);
                rAzm = Convert.ToDecimal(RunInfo[1]);
                rTInc = Convert.ToDecimal(RunInfo[2]);
                rTAzm = Convert.ToDecimal(RunInfo[3]);
                BHAInfo = AST.getBhaInfoList(RunID, ClientDBConnection);
                MSize = Convert.ToDecimal(BHAInfo[0]);
                MSizeU = Convert.ToInt32(BHAInfo[1]);
                DSCSize = Convert.ToDecimal(BHAInfo[2]);
                DSCSizeU = Convert.ToInt32(BHAInfo[3]);
                MDBLen = Convert.ToDecimal(BHAInfo[4]);
                MDBU = Convert.ToInt32(BHAInfo[5]);
                NMSpAbv = Convert.ToDecimal(BHAInfo[6]);
                NMSpAbvU = Convert.ToInt32(BHAInfo[7]);
                NMSpBlw = Convert.ToDecimal(BHAInfo[8]);
                NMSpBlwU = Convert.ToInt32(BHAInfo[9]);
                NMSpAlw = OPR.getAllowableSpace(convPTInc, convPTAzm);
                MGValue = Convert.ToDecimal(BHAInfo[11]);
                MGVU = Convert.ToInt32(BHAInfo[12]);
                DGTop = Convert.ToDecimal(BHAInfo[13]);
                DGTU = Convert.ToInt32(BHAInfo[14]);
                DSGValue = Convert.ToDecimal(BHAInfo[15]);
                DSGU = Convert.ToInt32(BHAInfo[16]);
                DGBtm = Convert.ToDecimal(BHAInfo[17]);
                DGBTU = Convert.ToInt32(BHAInfo[18]);

                convPTInc = QC.calConvProposedInclination(rInc, rTInc);
                convRTAzm = QC.calConvRunTargetAzimuth(rAzm, LatiNS, mDec, mdecCID, gconv, gcoID);
                convRTAzmIFR = QC.calConvRunTargetAzimuthIFR(rAzm, LatiNS, imDec, imdecCID, gconv, gcoID);
                convPTAzm = QC.calConvProposedAzimuth(ifrVal, LatiNS, mDec, imDec, mdecCID, imdecCID, gconv, gcoID, rTAzm);
                Decimal cnvDip = -99.00M, cnvBT = -99.00M, cnvGT = -99.000000000M, cnvIFRGT = -99.00000000M, cnvIFRDip = -99.000000M, cnvIFRBT = -99.000000M;
                Decimal cnvMSize = -99.00M, cnvSQCBTotal = -99.000000M, cnvSQCGTotal = -99.000000000M, cnvNMSpAbv = -99.00M, cnvNMSpBlw = -99.00M, cnvMDBLen = -99.00M;
                Decimal cnvMGValue = -99.00000000M, cnvDSSize = -99.00M, cnvDSGValue = -99.00000000M, ThreeSigmaVal = -99.00M;
                Decimal cnvDGTop = -99.00M; //Distance of GMeter from Top of Motor
                Decimal cnvDGBtm = -99.00M; //Distance of GMeter from Drill String/Collar                                                

                cnvDip = Units.convDip(dip);
                cnvBT = Units.convBTotal(bTotal, MagU);
                cnvIFRDip = Units.convDip(idip);
                cnvIFRBT = Units.convBTotal(iBTotal, MagU);
                cnvGT = Units.convGTotal(gTotal, AccelU);
                cnvIFRGT = Units.convGTotal(iGTotal, AccelU);
                cnvMSize = Units.convMotorSize(MSize, MSizeU);
                cnvSQCBTotal = Units.convBTotal(bTotal, MagU);
                cnvSQCGTotal = Units.convGTotal(gTotal, AccelU);
                cnvMDBLen = Units.convMDLen(MDBLen, MDBU);
                cnvNMSpAbv = Units.convNMSp(NMSpAbv, NMSpAbvU);
                cnvNMSpBlw = Units.convNMSp(NMSpBlw, NMSpBlwU);
                cnvMGValue = Units.convGMeter(MGValue, MGVU);
                cnvDSSize = Units.convDSCSize(DSCSize, DSCSizeU);
                cnvDSGValue = Units.convGMeter(DSGValue, DSGU);
                cnvDGTop = Units.convGMtrDistance(DGTop, DGTU);
                cnvDGBtm = Units.convGMtrDistance(DGBtm, DGBTU);

                foreach (System.Data.DataRow row in rawData.Rows)
                {
                    sID = Convert.ToInt32(row["srvRID"]);
                    srvyIDValues.Add((Double)sID);
                    rDepth = Convert.ToDecimal(row["Depth"]);
                    rBx = Convert.ToDecimal(row["Bx"]);
                    bxValues.Add((Double)rBx);
                    rBy = Convert.ToDecimal(row["By"]);
                    byValues.Add((Double)rBy);
                    rBz = Convert.ToDecimal(row["Bz"]);
                    bzValues.Add((Double)rBz);
                    rGx = Convert.ToDecimal(row["Gx"]);
                    rGy = Convert.ToDecimal(row["Gy"]);
                    rGz = Convert.ToDecimal(row["Gz"]);

                    valGTF = QC.getGTF(rGx, rGy); // GTF
                    valMTF = QC.colQ(rBx, rBy); //MTF
                    valDeclination = QC.colS(ifrVal, imDec, mDec); //Delta Declination
                    valInclination = QC.colT(rGx, rGy, rGz); //Inclination
                    valCF = QC.colCF(rGx, rGy, rGz); // G-Total
                    valCG = QC.colCG(ifrVal, valCF, gTotal, iGTotal); //Delta G-Total
                    valMagAzimuth = QC.colY(valGTF, valInclination, rBx, rBy, rBz); // LCAZ Magnetic Azimuth
                    plnGridAzimuth = QC.colZ(valMagAzimuth, mDec, gconv, LatiNS, mdecCID, gcoID); // Plain Grid Azimuth
                    ifrGridAzimuth = QC.colAA(valMagAzimuth, imDec, gconv, LatiNS, imdecCID, gcoID); // IFR Grid Azimuth
                    plnTrueAzimuth = QC.colAB(valMagAzimuth, mDec, mdecCID); // Plain True Azimuth
                    ifrTrueAzimuth = QC.colAC(valMagAzimuth, imDec, imdecCID); //IFR True Azimuth
                    gridAzimuth = QC.colAD(ifrVal, plnGridAzimuth, ifrGridAzimuth); // LCAZ Grid Azimuth value
                    trueAzimuth = QC.colAE(ifrVal, plnTrueAzimuth, ifrTrueAzimuth); // LCAZ True Azimuth value
                    finalAzimuth = QC.colAF(jConverg, gridAzimuth, trueAzimuth); // final Azimuth

                    valGoxy = QC.colCE(rGx, rGy); //Goxy
                    valBoxy = QC.colBZ(rBx, rBy); //Boxy
                    valGTotal = QC.colCF(rGx, rGy, rGz); //G-Total
                    valBTotal = QC.colCA(rBx, rBy, rBz); //B-Total
                    valCD = QC.colCD(ifrVal, cnvBT, cnvIFRBT, valBTotal); //Delta B-Total Long Collar
                    valDip = QC.colBA(rGx, rGy, rGz, rBx, rBy, rBz, valGTotal, valBTotal); // Long Collar Dip                    
                    refGx = QC.colBF(ifrVal, valGTF, valInclination, gTotal, iGTotal); // Ref. Gx
                    refGy = QC.colBG(ifrVal, valGTF, valInclination, gTotal, iGTotal); // Ref. Gy
                    refGz = QC.colBH(ifrVal, valInclination, gTotal, iGTotal); // Ref. Gz
                    refBx = QC.colBI(ifrVal, cnvBT, cnvIFRBT, cnvDip, cnvIFRDip, valMagAzimuth, valGTF, valInclination); // Ref. Bx
                    biValues.Add((Double)refBx);
                    refBy = QC.colBJ(ifrVal, cnvDip, cnvBT, cnvIFRDip, cnvIFRBT, valGTF, valInclination, valMagAzimuth); // Ref. By
                    bjValues.Add((Double)refBy);
                    refBz = QC.colBK(ifrVal, cnvDip, cnvBT, cnvIFRDip, cnvIFRBT, valInclination, valMagAzimuth); // Ref. Bz
                    valCK = QC.colCK(ifrVal, rBz, valInclination, valMagAzimuth, refBz, cnvDip, cnvIFRDip, cnvBT, cnvIFRBT);  // Delta Dip
                    valCL = QC.colCL(ifrVal, valInclination, valMagAzimuth, valCK, cnvDip, cnvIFRDip); // dAz1
                    valCM = QC.colCM(valMagAzimuth, valCL); // Corrected Azimuth
                    valCN = QC.colCN(ifrVal, valInclination, valCM, cnvDip, cnvIFRDip, cnvBT, cnvIFRBT); // Bzo
                    cnValues.Add((Double)valCN);
                    valBO = QC.colBO(ServiceID, sID, WellID, WellSectionID, RunID, rBx, bxValues, refBx, biValues, ClientDBConnection); //Bx Scale Factor
                    valBP = QC.colBP(ServiceID, sID, WellID, WellSectionID, RunID, rBy, byValues, refBy, bjValues, ClientDBConnection); //By Scale Factor      
                    valCS = QC.colCS(rBz, valCN); //Delta Bz
                    valFX = QC.colFX(cnvDSSize);  //Drill Collar Pole Strength
                    valFW = QC.colFW(cnvMSize); //Motor Pole Strength
                    valFZ = QC.colFZ(cnvNMSpAbv, valFX); //Bp Drill Collar
                    valGA = QC.colGA(cnvNMSpBlw, cnvMDBLen, valFW); //Bp-Motor - Motor Interference                                                                                                    
                    valGB = QC.colGB(valFZ, valGA); // BHA Interference (nT)
                    gbList.Add(valGB);
                    TSBias = QC.biasCheck(ServiceID, sID, WellID, WellSectionID, RunID, valGB, gbList, ClientDBConnection); //bias Check
                    valHT = QC.colHT(rBx, rBy, rBz, refBx, refBy, valCN, valCS, TSBias); // Delta Bz   
                    htList.Add(valHT);
                    htValues.Add((Double)valHT);
                    valCT = QC.colCT(ServiceID, sID, srvyIDValues, WellID, WellSectionID, RunID, valHT, htValues, ClientDBConnection); //Delta Bz Trend
                    valCU = QC.colCU(rBz, valCT); //Bz - Trend Corrected         
                    valCV = QC.colCV(rBx, rBy, valGTF, valInclination, valCU);           
                    valCX = QC.colCX(mDec, gconv, valCV, LatiNS, mdecCID, gcoID); //SCAZ Grid-Plain Azm
                    valCY = QC.colCY(imDec, gconv, valCV, LatiNS, imdecCID, gcoID); //SCAZ Grid-IFR Azm
                    valCZ = QC.colCZ(mDec, valCV, mdecCID); //SCAZ True-Plain Azm
                    valDA = QC.colDA(imDec, valCV, imdecCID); //SCAZ True-IFR Azm
                    valDB = QC.colDB(ifrVal, valCX, valCY); // SCAZ Grid Final
                    valDC = QC.colDC(ifrVal, valCZ, valDA); // SCAZ True Final
                    valDD = QC.colDD(jConverg, valDB, valDC);// SCAZ Final Azm
                    valHQ = QC.colHQ(rBx, rBy, rBz, refBx, refBy, valCN, TSBias); // Bx-Bias (Conditional) 
                    hqValues.Add((Double)valHQ);                                       
                    valHR = QC.colHR(rBx, rBy, rBz, refBx, refBy, valCN, TSBias); // By-Bias (Conditional)
                    hrValues.Add((Double)valHR);
                    valBzBias = QC.bzBias(ServiceID, sID, RunID, WellSectionID, WellID, rBx, rBz, rBz, bzValues, refBx, refBy, valCN, valHQ, TSBias, ClientDBConnection);
                    valBzBiasConditional = QC.bzBias_Conditional(rBx, rBy, rBz, refBx, refBy, valCN, TSBias);
                    valBzScaleFactor = QC.bzSF(ServiceID, sID, WellID, WellSectionID, RunID, rBz, bzValues, valCN, cnValues, ClientDBConnection);
                    valBzCorrected = QC.correctedBz(rBz, valBzScaleFactor, valBzBias);
                    valDeltaBzCorrected = QC.deltaBzCorrected(rBz, valBzCorrected);
                    valBTotalCorrected = QC.crrBTotalCorrected(valBQ, valBR, valBzCorrected);
                    valDeltaBTotalCorrect = QC.crrDeltaBTotal(ifrVal, cnvBT, cnvIFRBT, valBTotalCorrected);
                    valDipCorrected = QC.crrDipCorrected(rGx, rGy, rGz, valBQ, valBR, valBzCorrected, gTotal, valBTotalCorrected);
                    valDeltaDipCorrected = QC.crrDeltaDip(ifrVal, cnvDip, cnvIFRDip, valDipCorrected);
                    ThreeSigmaVal = QC.ThreeSigmaBValue(gbList);
                    valBM = QC.colBM(sID, rBx, rBy, rBz, refBx, refBy, valCN, ThreeSigmaVal, valHQ, biValues, bjValues, hqValues);
                    valBN = QC.colBN(sID, rBx, rBy, rBz, refBx, refBy, valCN, ThreeSigmaVal, valHR, biValues, bjValues, hrValues);
                    valBQ = QC.colBQ(rBx, valBO, valBM); //Bx Corrected                                        
                    valBR = QC.colBR(rBy, valBP, valBN); //By Corrected        
                    valBU = QC.colBU(valBQ, valBR, valBzCorrected); //Bt Corrected                    
                    valBV = QC.colBV(rGx, rGy, rGz, valBQ, valBR, valBzCorrected, valCF, valBU); //Dip Corrected
                    valBX = QC.colBX(ifrVal, cnvDip, cnvIFRDip, valBV); //Delta Dip Corrected                    
                    valBW = QC.colBW(ifrVal, cnvBT, cnvIFRBT, valBU); //Delta B-Total Corrected
                    iReply = insertIncDipCorrInputToAnalysis(sID, rDepth, rBx, rBy, rBz, rGx, rGy, rGz, valGTF, valMTF, valDeclination, valInclination, valCD, valCF, valCG, valMagAzimuth, plnGridAzimuth, ifrGridAzimuth, plnTrueAzimuth, ifrTrueAzimuth, gridAzimuth, trueAzimuth, finalAzimuth, valGoxy, valBoxy, valGTotal, valBTotal, valDip, refGx, refGy, refGz, refBx, refBy, refBz, valCK, valCL, valCM, valCN, valFX, valFW, valFZ, valGA, valGB, TSBias, valHQ, valBM, valBO, valBP, valHR, valBN, valBQ, valBR, valCS, valHT, valCT, valCU, valBU, valBV, valBW, valBX, valCY, valCZ, valDA, valDB, valDC, valDD, valBzBias, valBzBiasConditional, valBzScaleFactor, valBzCorrected, valDeltaBzCorrected, valBTotalCorrected, valDeltaBTotalCorrect, valDipCorrected, valDeltaDipCorrected, RunID, WellSectionID, WellID, ClientDBConnection);
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getAnalysisRawData(Int32 WellID, Int32 WellSectionID, Int32 RunID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn srvRID = iReply.Columns.Add("srvRID", typeof(Int32));
                DataColumn Depth = iReply.Columns.Add("Depth", typeof(Decimal));
                DataColumn Bx = iReply.Columns.Add("Bx", typeof(Decimal));
                DataColumn By = iReply.Columns.Add("By", typeof(Decimal));
                DataColumn Bz = iReply.Columns.Add("Bz", typeof(Decimal));
                DataColumn Gx = iReply.Columns.Add("Gx", typeof(Decimal));
                DataColumn Gy = iReply.Columns.Add("Gy", typeof(Decimal));
                DataColumn Gz = iReply.Columns.Add("Gz", typeof(Decimal));
                //String selData = "SELECT [srvyRowID], [crrDepth], [crrBx], [crrBy], [crrBz], [crrGx], [crrGy], [crrGz] FROM [CorrectionFormattedData] WHERE [jobID] = @jobID AND [welID] = @welID AND [runID] = @runID ORDER BY [srvyRowID] Desc";
                String selData = "SELECT [srvyRowID], [crrDepth], [crrBx], [crrBy], [crrBz], [crrGx], [crrGy], [crrGz] FROM [IncDipCorrFormattedData] WHERE [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                Int32 sID = -99;
                Decimal rdepth = -99.00M, rbx, rby, rbz, rgx, rgy, rgz;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        sID = readData.GetInt32(0);
                                        rdepth = readData.GetDecimal(1);
                                        rbx = readData.GetDecimal(2);
                                        rby = readData.GetDecimal(3);
                                        rbz = readData.GetDecimal(4);
                                        rgx = readData.GetDecimal(5);
                                        rgy = readData.GetDecimal(6);
                                        rgz = readData.GetDecimal(7);
                                        iReply.Rows.Add(sID, rdepth, rbx, rby, rbz, rgx, rgy, rgz);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertIncDipCorrInputToAnalysis(Int32 srvyRowID, Decimal ciaDepth, Decimal ciaBx, Decimal ciaBy, Decimal ciaBz, Decimal ciaGx, Decimal ciaGy, Decimal ciaGz, Decimal valGTF, Decimal valMTF, Decimal valDec, Decimal valInc, Decimal valCD, Decimal valCF, Decimal valCG, Decimal valMagAzm, Decimal valPlnGridAzm, Decimal valIfrGridAzm, Decimal valPlnTrueAzm, Decimal valIfrTrueAzm, Decimal valPlnAzm, Decimal valIfrAzm, Decimal valFinalAzm, Decimal valGoxy, Decimal valBoxy, Decimal valGTotal, Decimal valBTotal, Decimal valDip, Decimal valRefGx, Decimal valRefGy, Decimal valRefGz, Decimal valRefBx, Decimal valRefBy, Decimal valRefBz, Decimal valCK, Decimal valCL, Decimal valCM, Decimal valCN, Decimal valFX, Decimal valFW, Decimal valFZ, Decimal valGA, Decimal valGB, Decimal TSBias, Decimal valHQ, Decimal valBM, Decimal valBO, Decimal valBP, Decimal valHR, Decimal valBN, Decimal valBQ, Decimal valBR, Decimal valCS, Decimal valHT, Decimal valCT, Decimal valCU, Decimal valBU, Decimal valBV, Decimal valBW, Decimal valBX, Decimal valCY, Decimal valCZ, Decimal valDA, Decimal valDB, Decimal valDC, Decimal valDD, Decimal valBzBias, Decimal valBzBiasConditional, Decimal valBzScaleFactor, Decimal valBzCorrected, Decimal valDeltaBzCorrected, Decimal valBTotalCorrected, Decimal valDeltaBTotalCorrect, Decimal valDipCorrected, Decimal valDeltaDipCorrected, Int32 RunID, Int32 WellSectionID, Int32 WellID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String insertCommand = "INSERT INTO [IncDipCorrInputToAnalysis] ([srvyRowID], [ciaDepth], [ciaBx], [ciaBy], [ciaBz], [ciaGx], [ciaGy], [ciaGz], [valGTF], [valMTF], [valDec], [valInc], [valCD], [valCF], [valCG], [valMagAzm], [valPlnGridAzm], [valIfrGridAzm], [valPlnTrueAzm], [valIfrTrueAzm], [valPlnAzm], [valIfrAzm], [valFinalAzm], [valGoxy], [valBoxy], [valGTotal], [valBTotal], [valDip], [valRefGx], [valRefGy], [valRefGz], [valRefBx], [valRefBy], [valRefBz], [valCK], [valCL], [valCM], [valCN], [valFX], [valFW], [valFZ], [valGA], [valGB], [TSBias], [valHQ], [valBM], [valBO], [valBP], [valHR], [valBN], [valBQ], [valBR], [valCS], [valHT], [valCT], [valCU], [valBU], [valBV], [valBW], [valBX], [valCY], [valCZ], [valDA], [valDB], [valDC], [valDD], [valBzBias], [valBzBiasConditional], [valBzScaleFactor], [valBzCorrected], [valDeltaBzCorrected], [valBTotalCorrected], [valDeltaBTotalCorrect], [valDipCorrected], [valDeltaDipCorrected], [runID], [wswID], [welID]) VALUES (@srvyRowID, @ciaDepth, @ciaBx, @ciaBy, @ciaBz, @ciaGx, @ciaGy, @ciaGz, @valGTF, @valMTF, @valDec, @valInc, @valCD, @valCF, @valCG, @valMagAzm, @valPlnGridAzm, @valIfrGridAzm, @valPlnTrueAzm, @valIfrTrueAzm, @valPlnAzm, @valIfrAzm, @valFinalAzm, @valGoxy, @valBoxy, @valGTotal, @valBTotal, @valDip, @valRefGx, @valRefGy, @valRefGz, @valRefBx, @valRefBy, @valRefBz, @valCK, @valCL, @valCM, @valCN, @valFX, @valFW, @valFZ, @valGA, @valGB, @TSBias, @valHQ, @valBM, @valBO, @valBP, @valHR, @valBN, @valBQ, @valBR, @valCS, @valHT, @valCT, @valCU, @valBU, @valBV, @valBW, @valBX, @valCY, @valCZ, @valDA, @valDB, @valDC, @valDD, @valBzBias, @valBzBiasConditional, @valBzScaleFactor, @valBzCorrected, @valDeltaBzCorrected, @valBTotalCorrected, @valDeltaBTotalCorrect, @valDipCorrected, @valDeltaDipCorrected, @runID, @wswID, @welID)";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(insertCommand, dataCon);
                        insData.Parameters.AddWithValue("@srvyRowID", SqlDbType.Int).Value = srvyRowID.ToString();
                        insData.Parameters.AddWithValue("@ciaDepth", SqlDbType.Decimal).Value = ciaDepth.ToString();
                        insData.Parameters.AddWithValue("@ciaBx", SqlDbType.Decimal).Value = ciaBx.ToString();
                        insData.Parameters.AddWithValue("@ciaBy", SqlDbType.Decimal).Value = ciaBy.ToString();
                        insData.Parameters.AddWithValue("@ciaBz", SqlDbType.Decimal).Value = ciaBz.ToString();
                        insData.Parameters.AddWithValue("@ciaGx", SqlDbType.Decimal).Value = ciaGx.ToString();
                        insData.Parameters.AddWithValue("@ciaGy", SqlDbType.Decimal).Value = ciaGy.ToString();
                        insData.Parameters.AddWithValue("@ciaGz", SqlDbType.Decimal).Value = ciaGz.ToString();
                        insData.Parameters.AddWithValue("@valGTF", SqlDbType.Decimal).Value = valGTF.ToString();
                        insData.Parameters.AddWithValue("@valMTF", SqlDbType.Decimal).Value = valMTF.ToString();
                        insData.Parameters.AddWithValue("@valDec", SqlDbType.Decimal).Value = valDec.ToString();
                        insData.Parameters.AddWithValue("@valInc", SqlDbType.Decimal).Value = valInc.ToString();
                        insData.Parameters.AddWithValue("@valCD", SqlDbType.Decimal).Value = valCD.ToString();
                        insData.Parameters.AddWithValue("@valCF", SqlDbType.Decimal).Value = valCF.ToString();
                        insData.Parameters.AddWithValue("@valCG", SqlDbType.Decimal).Value = valCG.ToString();
                        insData.Parameters.AddWithValue("@valMagAzm", SqlDbType.Decimal).Value = valMagAzm.ToString();
                        insData.Parameters.AddWithValue("@valPlnGridAzm", SqlDbType.Decimal).Value = valPlnGridAzm.ToString();
                        insData.Parameters.AddWithValue("@valIfrGridAzm", SqlDbType.Decimal).Value = valIfrGridAzm.ToString();
                        insData.Parameters.AddWithValue("@valPlnTrueAzm", SqlDbType.Decimal).Value = valPlnTrueAzm.ToString();
                        insData.Parameters.AddWithValue("@valIfrTrueAzm", SqlDbType.Decimal).Value = valIfrTrueAzm.ToString();
                        insData.Parameters.AddWithValue("@valPlnAzm", SqlDbType.Decimal).Value = valPlnAzm.ToString();
                        insData.Parameters.AddWithValue("@valIfrAzm", SqlDbType.Decimal).Value = valIfrAzm.ToString();
                        insData.Parameters.AddWithValue("@valFinalAzm", SqlDbType.Decimal).Value = valFinalAzm.ToString();
                        insData.Parameters.AddWithValue("@valGoxy", SqlDbType.Decimal).Value = valGoxy.ToString();
                        insData.Parameters.AddWithValue("@valBoxy", SqlDbType.Decimal).Value = valBoxy.ToString();
                        insData.Parameters.AddWithValue("@valGTotal", SqlDbType.Decimal).Value = valGTotal.ToString();
                        insData.Parameters.AddWithValue("@valBTotal", SqlDbType.Decimal).Value = valBTotal.ToString();
                        insData.Parameters.AddWithValue("@valDip", SqlDbType.Decimal).Value = valDip.ToString();
                        insData.Parameters.AddWithValue("@valRefGx", SqlDbType.Decimal).Value = valRefGx.ToString();
                        insData.Parameters.AddWithValue("@valRefGy", SqlDbType.Decimal).Value = valRefGy.ToString();
                        insData.Parameters.AddWithValue("@valRefGz", SqlDbType.Decimal).Value = valRefGz.ToString();
                        insData.Parameters.AddWithValue("@valRefBx", SqlDbType.Decimal).Value = valRefBx.ToString();
                        insData.Parameters.AddWithValue("@valRefBy", SqlDbType.Decimal).Value = valRefBy.ToString();
                        insData.Parameters.AddWithValue("@valRefBz", SqlDbType.Decimal).Value = valRefBz.ToString();
                        insData.Parameters.AddWithValue("@valCK", SqlDbType.Decimal).Value = valCK.ToString();
                        insData.Parameters.AddWithValue("@valCL", SqlDbType.Decimal).Value = valCL.ToString();
                        insData.Parameters.AddWithValue("@valCM", SqlDbType.Decimal).Value = valCM.ToString();
                        insData.Parameters.AddWithValue("@valCN", SqlDbType.Decimal).Value = valCN.ToString();
                        insData.Parameters.AddWithValue("@valFX", SqlDbType.Decimal).Value = valFX.ToString();
                        insData.Parameters.AddWithValue("@valFW", SqlDbType.Decimal).Value = valFW.ToString();
                        insData.Parameters.AddWithValue("@valFZ", SqlDbType.Decimal).Value = valFZ.ToString();
                        insData.Parameters.AddWithValue("@valGA", SqlDbType.Decimal).Value = valGA.ToString();
                        insData.Parameters.AddWithValue("@valGB", SqlDbType.Decimal).Value = valGB.ToString();
                        insData.Parameters.AddWithValue("@TSBias", SqlDbType.Decimal).Value = TSBias.ToString();
                        insData.Parameters.AddWithValue("@valHQ", SqlDbType.Decimal).Value = valHQ.ToString();
                        insData.Parameters.AddWithValue("@valBM", SqlDbType.Decimal).Value = valBM.ToString();
                        insData.Parameters.AddWithValue("@valBO", SqlDbType.Decimal).Value = valBO.ToString();
                        insData.Parameters.AddWithValue("@valBP", SqlDbType.Decimal).Value = valBP.ToString();
                        insData.Parameters.AddWithValue("@valHR", SqlDbType.Decimal).Value = valHR.ToString();
                        insData.Parameters.AddWithValue("@valBN", SqlDbType.Decimal).Value = valBN.ToString();
                        insData.Parameters.AddWithValue("@valBQ", SqlDbType.Decimal).Value = valBQ.ToString();
                        insData.Parameters.AddWithValue("@valBR", SqlDbType.Decimal).Value = valBR.ToString();
                        insData.Parameters.AddWithValue("@valCS", SqlDbType.Decimal).Value = valCS.ToString();
                        insData.Parameters.AddWithValue("@valHT", SqlDbType.Decimal).Value = valHT.ToString();
                        insData.Parameters.AddWithValue("@valCT", SqlDbType.Decimal).Value = valCT.ToString();
                        insData.Parameters.AddWithValue("@valCU", SqlDbType.Decimal).Value = valCU.ToString();
                        insData.Parameters.AddWithValue("@valBU", SqlDbType.Decimal).Value = valBU.ToString();
                        insData.Parameters.AddWithValue("@valBV", SqlDbType.Decimal).Value = valBV.ToString();
                        insData.Parameters.AddWithValue("@valBW", SqlDbType.Decimal).Value = valBW.ToString();
                        insData.Parameters.AddWithValue("@valBX", SqlDbType.Decimal).Value = valBX.ToString();
                        insData.Parameters.AddWithValue("@valCY", SqlDbType.Decimal).Value = valCY.ToString();
                        insData.Parameters.AddWithValue("@valCZ", SqlDbType.Decimal).Value = valCZ.ToString();
                        insData.Parameters.AddWithValue("@valDA", SqlDbType.Decimal).Value = valDA.ToString();
                        insData.Parameters.AddWithValue("@valDB", SqlDbType.Decimal).Value = valDB.ToString();
                        insData.Parameters.AddWithValue("@valDC", SqlDbType.Decimal).Value = valDC.ToString();
                        insData.Parameters.AddWithValue("@valDD", SqlDbType.Decimal).Value = valDD.ToString();
                        insData.Parameters.AddWithValue("@valBzBias", SqlDbType.Decimal).Value = valBzBias.ToString();
                        insData.Parameters.AddWithValue("@valBzBiasConditional", SqlDbType.Decimal).Value = valBzBiasConditional.ToString();
                        insData.Parameters.AddWithValue("@valBzScaleFactor", SqlDbType.Decimal).Value = valBzScaleFactor.ToString();
                        insData.Parameters.AddWithValue("@valBzCorrected", SqlDbType.Decimal).Value = valBzCorrected.ToString();
                        insData.Parameters.AddWithValue("@valDeltaBzCorrected", SqlDbType.Decimal).Value = valDeltaBzCorrected.ToString();
                        insData.Parameters.AddWithValue("@valBTotalCorrected", SqlDbType.Decimal).Value = valBTotalCorrected.ToString();
                        insData.Parameters.AddWithValue("@valDeltaBTotalCorrect", SqlDbType.Decimal).Value = valDeltaBTotalCorrect.ToString();
                        insData.Parameters.AddWithValue("@valDipCorrected", SqlDbType.Decimal).Value = valDipCorrected.ToString();
                        insData.Parameters.AddWithValue("@valDeltaDipCorrected", SqlDbType.Decimal).Value = valDeltaDipCorrected.ToString();
                        insData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        insData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        insData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getIncDipInputQCData(Int32 JobID, Int32 WellID, Int32 WellSectionID, Int32 RunID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn srvRID = iReply.Columns.Add("srvRID", typeof(Int32));
                DataColumn Depth = iReply.Columns.Add("Depth", typeof(Decimal));
                DataColumn Inc = iReply.Columns.Add("Inc", typeof(Decimal));
                DataColumn LCAzm = iReply.Columns.Add("LCAzm", typeof(Decimal));
                DataColumn SCAzm = iReply.Columns.Add("SCAzm", typeof(Decimal));
                DataColumn valDip = iReply.Columns.Add("valDip", typeof(Decimal));
                DataColumn valBTotal = iReply.Columns.Add("valBTotal", typeof(Decimal));
                DataColumn valGTotal = iReply.Columns.Add("valGTotal", typeof(Decimal));
                String selData = "SELECT [srvyRowID], [ciaDepth], [valInc], [valFinalAzm], [valDD], [valDip], [valBTotal], [valGTotal] FROM [IncDipCorrInputToAnalysis] WHERE [jobID] = @jobID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID ORDER BY [srvyRowID] Desc";
                Int32 sID = -99, qd = -99;
                Decimal rdepth = -99.00M, qcInc, qcLCAzm, qcSCAzm, dip, bt, gt;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@jobID", SqlDbType.Int).Value = JobID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        sID = readData.GetInt32(0);
                                        rdepth = readData.GetDecimal(1);
                                        qcInc = readData.GetDecimal(2);
                                        qcLCAzm = readData.GetDecimal(3);
                                        qcSCAzm = readData.GetDecimal(4);
                                        dip = readData.GetDecimal(5);
                                        bt = readData.GetDecimal(6);
                                        gt = readData.GetDecimal(7);
                                        iReply.Rows.Add(sID, rdepth, qcInc, qcLCAzm, qcSCAzm, dip, bt, gt);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getSolutionData(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn srvRID = iReply.Columns.Add("srvRID", typeof(Int32));
                DataColumn Depth = iReply.Columns.Add("Depth", typeof(Decimal));
                DataColumn Bx = iReply.Columns.Add("Bx", typeof(Decimal));
                DataColumn By = iReply.Columns.Add("By", typeof(Decimal));
                DataColumn Bz = iReply.Columns.Add("Bz", typeof(Decimal));
                DataColumn Gx = iReply.Columns.Add("Gx", typeof(Decimal));
                DataColumn Gy = iReply.Columns.Add("Gy", typeof(Decimal));
                DataColumn Gz = iReply.Columns.Add("Gz", typeof(Decimal));
                DataColumn BM = iReply.Columns.Add("BM", typeof(Decimal));
                DataColumn BN = iReply.Columns.Add("BN", typeof(Decimal));
                DataColumn BO = iReply.Columns.Add("BO", typeof(Decimal));
                DataColumn BP = iReply.Columns.Add("BP", typeof(Decimal));
                DataColumn BzBias = iReply.Columns.Add("BzBias", typeof(Decimal));
                DataColumn BzSF = iReply.Columns.Add("BzSF", typeof(Decimal));
                DataColumn valDipC = iReply.Columns.Add("valDipC", typeof(Decimal));
                DataColumn valFinalAzm = iReply.Columns.Add("valFinalAzm", typeof(Decimal));
                DataColumn valDD = iReply.Columns.Add("valDD", typeof(Decimal));
                //String selData = "SELECT [srvyRowID], [crrDepth], [crrBx], [crrBy], [crrBz], [crrGx], [crrGy], [crrGz] FROM [CorrectionFormattedData] WHERE [jobID] = @jobID AND [welID] = @welID AND [runID] = @runID ORDER BY [srvyRowID] Desc";
                String selData = "SELECT [srvyRowID], [ciaDepth], [ciaBx], [ciaBy], [ciaBz], [ciaGx], [ciaGy], [ciaGz], [valBM], [valBN], [valBO], [valBP], [valBzBias], [valBzScaleFactor], [valDipCorrected], [valFinalAzm], [valDD] FROM [IncDipCorrInputToAnalysis] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                Int32 sID = -99;
                Decimal rdepth = -99.00M, rbx, rby, rbz, rgx, rgy, rgz, valBM, valBN, valBO, valBP, valBzBias, valBzSF, valDip, lcAzm, scAzm;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        sID = readData.GetInt32(0);
                                        rdepth = readData.GetDecimal(1);
                                        rbx = readData.GetDecimal(2);
                                        rby = readData.GetDecimal(3);
                                        rbz = readData.GetDecimal(4);
                                        rgx = readData.GetDecimal(5);
                                        rgy = readData.GetDecimal(6);
                                        rgz = readData.GetDecimal(7);
                                        valBM = readData.GetDecimal(8);
                                        valBN = readData.GetDecimal(9);
                                        valBO = readData.GetDecimal(10);
                                        valBP = readData.GetDecimal(11);
                                        valBzBias = readData.GetDecimal(12);
                                        valBzSF = readData.GetDecimal(13);
                                        valDip = readData.GetDecimal(14);
                                        lcAzm = readData.GetDecimal(15);
                                        scAzm = readData.GetDecimal(16);
                                        iReply.Rows.Add(sID, rdepth, rbx, rby, rbz, rgx, rgy, rgz, valBM, valBN, valBO, valBP, valBzBias, valBzSF, valDip, lcAzm, scAzm);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Double> getQCStandardDeviations(System.Data.DataTable Values)
        {
            try
            {
                List<Double> iReply = new List<Double> { }, lcValues = new List<Double>{}, scValues = new List<Double>{};
                Double[] lc = new Double[] { }, sc = new Double[] { };
                Double lcVal, lcDev, lcG, scVal, scDev, scG;
                foreach (System.Data.DataRow row in Values.Rows)
                {
                    lcVal = Convert.ToDouble(row["LCAzm"]);
                    lcValues.Add(lcVal);
                    scVal = Convert.ToDouble(row["SCAzm"]);
                    scValues.Add(scVal);
                }
                lc = lcValues.ToArray();
                lcDev = Stat.StandardDeviation(lc);                
                iReply.Add(lcDev);
                sc = scValues.ToArray();
                scDev = Stat.StandardDeviation(sc);
                iReply.Add(scDev);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Double> getSolStandardDeviations(System.Data.DataTable Values)
        {
            try
            {
                List<Double> iReply = new List<Double> { }, lcValues = new List<Double> { }, scValues = new List<Double> { }, crValues = new List<Double> { };
                Double[] lc = new Double[] { }, sc = new Double[] { }, cr = new Double[] { };
                Double lcVal, lcDev, lcG, scVal, scDev, scG , crVal, crDev, crG;
                foreach (System.Data.DataRow row in Values.Rows)
                {
                    lcVal = Convert.ToDouble(row["LCAzm"]);
                    lcValues.Add(lcVal);
                    scVal = Convert.ToDouble(row["SCAzm"]);
                    scValues.Add(scVal);
                    crVal = Convert.ToDouble(row["CrrAzm"]);
                    crValues.Add(crVal);
                }
                lc = lcValues.ToArray();
                lcDev = Stat.StandardDeviation(lc);
                iReply.Add(lcDev);
                sc = scValues.ToArray();
                scDev = Stat.StandardDeviation(sc);
                iReply.Add(scDev);
                cr = crValues.ToArray();
                crDev = Stat.StandardDeviation(cr);
                iReply.Add(crDev);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getRejectSolution(Int32 JobID, Int32 WellID, Int32 WellSectionID, Int32 RunID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99, formatted = -99, anaInput = -99;
                formatted = removeIncDipFormatted(JobID, WellID, WellSectionID, RunID, ClientDBConnection);
                anaInput = removeIncDipInput(JobID, WellID, WellSectionID, RunID, ClientDBConnection);
                if (formatted > 1 && anaInput > 1)
                {
                    iReply = 1;
                }
                else
                {
                    iReply = -1;
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 removeIncDipFormatted(Int32 JobID, Int32 WellID, Int32 WellSectionID, Int32 RunID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [IncDipCorrFormattedData] WHERE [jobID] = @jobID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand remData = new SqlCommand(selData, dataCon);
                        remData.Parameters.AddWithValue("@jobID", SqlDbType.Int).Value = JobID.ToString();
                        remData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        remData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        remData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        iReply = Convert.ToInt32(remData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 removeIncDipInput(Int32 JobID, Int32 WellID, Int32 WellSectionID, Int32 RunID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [IncDipCorrInputToAnalysis] WHERE [jobID] = @jobID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand remData = new SqlCommand(selData, dataCon);
                        remData.Parameters.AddWithValue("@jobID", SqlDbType.Int).Value = JobID.ToString();
                        remData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        remData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        remData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        iReply = Convert.ToInt32(remData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 removeIncDipQCFormatted(Int32 WellID, Int32 WellSectionID, Int32 RunID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [SurveyFormattedData] WHERE [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand remData = new SqlCommand(selData, dataCon);
                        remData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        remData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        remData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        iReply = Convert.ToInt32(remData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 removeIncDipQC(Int32 WellID, Int32 WellSectionID, Int32 RunID, String ClientDBConnection)
        {
            try
            {
                try
                {
                    Int32 iReply = -99;
                    String selData = "DELETE FROM [RawQCResults] WHERE [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                    using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                    {
                        try
                        {
                            dataCon.Open();
                            SqlCommand remData = new SqlCommand(selData, dataCon);
                            remData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                            remData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                            remData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                            iReply = Convert.ToInt32(remData.ExecuteNonQuery());
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            dataCon.Close();
                            dataCon.Dispose();
                        }
                    }
                    return iReply;
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getDeleteWellData(Int32 JobID, Int32 WellID, Int32 WellSectionID, Int32 RunID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99, crInput = -99, crFormatted = -99, qcFormatted = -99, qcAudit = -99;
                crFormatted = removeIncDipFormatted(JobID, WellID, WellSectionID, RunID, ClientDBConnection);
                crInput = removeIncDipInput(JobID, WellID, WellSectionID, RunID, ClientDBConnection);
                qcFormatted = removeIncDipQCFormatted(WellID, WellSectionID, RunID, ClientDBConnection);
                qcAudit = removeIncDipQC(WellID, WellSectionID, RunID, ClientDBConnection);
                if (crFormatted >= 1 && crInput >= 1 && qcFormatted >= 1 && qcAudit >= 1)
                {
                    iReply = 1;
                }
                else
                {
                    iReply = -1;
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        /// /////////////////////////////////////////////////
        /// Finished methods Implementing US 8 180 571 B2 ///
        /// /////////////////////////////////////////////////
        ///
    }
}