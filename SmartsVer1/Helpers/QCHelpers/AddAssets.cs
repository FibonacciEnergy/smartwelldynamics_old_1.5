﻿using SmartsVer1.Helpers.AccountHelpers;
using Microsoft.SqlServer.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Net.Mail;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using JS = Newtonsoft.Json;
using Color = System.Drawing.Color;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using CNV = SmartsVer1.Helpers.DemogHelpers.UnitsConverter;
using WPN = SmartsVer1.Helpers.QCHelpers.WellPlan;
using SRV = SmartsVer1.Helpers.DemogHelpers.ServicesConfig;
using SM = SmartsVer1.Helpers.SupportHelpers.SRequest;

namespace SmartsVer1.Helpers.QCHelpers
{
    public class AddAssets
    {

        const Int32 WellInProcess = 2, WellComplete = 1;

        static void Main()
        { }        

        private static Int32 addClientToOperator(Int32 ClientID, Int32 OperatorID, String UserName, String UserRealm)
        {
            try
            {
                Int32 iReply = -99;
                String insComm = "INSERT INTO ClientToOperator ([clntID], [optrID], [cTime], [uTime], [username], [realm]) VALUES (@clntID, @optrID, @cTime, @uTime, @username, @realm)";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(insComm, dataCon);
                        insData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        insData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserRealm.ToString();

                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }

                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static Int32 addSQC(String Name, String Dip, String BTotal, String GTotal, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99, dupName = -99;
                Decimal dipVal = -99.0000M, btVal = -99.0000M, gtVal = -99.0000000M;
                dupName = findDuplicateSQCName(Name, ClientDBString);
                if (dupName.Equals(1))
                {
                    iReply = -1;
                }
                else
                {
                    if (!Decimal.TryParse(Dip, out dipVal) || (!Decimal.TryParse(BTotal, out btVal)) || (!Decimal.TryParse(GTotal, out gtVal)))
                    {
                        iReply = -2; //Not Decimal                    
                    }
                    else
                    {
                        dipVal = Convert.ToDecimal(Dip);
                        btVal = Convert.ToDecimal(BTotal);
                        gtVal = Convert.ToDecimal(GTotal);
                        String insData = "INSERT INTO [SQC] ([sqcDip], [sqcBTotal], [sqcGTotal], [sqcName]) VALUES (@sqcDip, @sqcBTotal, @sqcGTotal, @sqcName)";
                        using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                        {
                            try
                            {
                                dataCon.Open();
                                SqlCommand getRes = new SqlCommand(insData, dataCon);
                                getRes.Parameters.AddWithValue("@sqcDip", SqlDbType.Decimal).Value = dipVal.ToString();
                                getRes.Parameters.AddWithValue("@sqcBTotal", SqlDbType.Decimal).Value = btVal.ToString();
                                getRes.Parameters.AddWithValue("@sqcGTotal", SqlDbType.Decimal).Value = gtVal.ToString();
                                getRes.Parameters.AddWithValue("@sqcName", SqlDbType.NVarChar).Value = Name.ToString();

                                iReply = Convert.ToInt32(getRes.ExecuteNonQuery());
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                dataCon.Close();
                                dataCon.Dispose();
                            }
                        }
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Add Well to Silo
        public static Int32 addWellSilo(Int32 OperatorID, String OperatorName, String WellName, Decimal WellLong, Int32 WellLongOrientation, Decimal WellLat, Int32 WellLatOrientation, Int32 WellPadID, String WellAPI, String WellUWI, String WellPermit, DateTime WellDate, Int32 MagModelID, Int32 CoordinateID, Int32 ProjectionID, Int32 WellSMARTsStatus, String domain, String DBString, String UserName, String UserRealm)
        {
            try
            {
                Int32 clntID = -99, cLatDeg = -99, cLatMin = -99, cLngDeg = -99, cLngMin = -99, wStat = -99, wpw = -99;
                String wLwrName = String.Empty, opLwrName = String.Empty, cLatitude = String.Empty, cLongitude = String.Empty;
                Decimal latitude = 0.000000000M, cLatSeconds = -99.0000M, cLngSeconds = -99.0000M, longitude = 0.000000000M;
                SqlGeography location = SqlGeography.Point((Double)latitude, (Double)longitude, 4236); //WGS84
                Int32 wlID = -99, wFolder = -99, result = -99;
                Int32 RegionID = -99, SubRegionID = -99, CountryID = -99, StateID = -99, CountyID = -99, FieldID = -99;
                List<Int32> locIDs = getLocationIDForWell(WellPadID, DBString);
                RegionID = Convert.ToInt32(locIDs[0]);
                SubRegionID = Convert.ToInt32(locIDs[1]);
                CountryID = Convert.ToInt32(locIDs[2]);
                StateID = Convert.ToInt32(locIDs[3]);
                CountyID = Convert.ToInt32(locIDs[4]);
                FieldID = Convert.ToInt32(locIDs[5]);
                clntID = getClientID(domain);

                using (SqlConnection clntConn = new SqlConnection(DBString))
                {
                    try
                    {
                        String insertCommand = "INSERT INTO [Well] ([clntID], [optrID], [welName], [welNameLower], [regID], [sregID], [ctyID], [cntyID], [fldID], [stID], [wpdID], [welAPI], [welUWI], [welPermit], [welSpudDate], [welLongitude], [welLngCardinal], [welLatitude], [welLatCardinal], [welLocation], [welLatDegree], [welLatMinutes], [welLatSeconds], [welLatOrientation], [welLngDegree], [welLngMinutes], [welLngSeconds], [welLngOrientation], [mmdlID], [cordID], [projID], [wstID], [username], [domain], [cTime], [uTime]) VALUES (@clntID, @optrID, @welName, @welNameLower, @regID, @sregID, @ctyID, @cntyID, @fldID, @stID, @wpdID, @welAPI, @welUWI, @welPermit, @welSpudDate, @welLongitude, @welLngCardinal, @welLatitude, @welLatCardinal, @welLocation, @welLatDegree, @welLatMinutes, @welLatSeconds, @welLatOrientation, @welLngDegree, @welLngMinutes, @welLngSeconds, @welLngOrientation, @mmdlID, @cordID, @projID, @wstID, @username, @domain, @cTime, @uTime); SELECT SCOPE_IDENTITY()";

                        TextInfo tiWNme = new CultureInfo("en-US", false).TextInfo;
                        wLwrName = tiWNme.ToLower(WellName);
                        wLwrName = wLwrName.Replace(" ", "");
                        TextInfo tiONme = new CultureInfo("en-US", false).TextInfo;
                        opLwrName = tiONme.ToLower(OperatorName);
                        opLwrName = opLwrName.Replace(" ", "_");
                        clntConn.Open();
                        SqlCommand insertSiloWell = new SqlCommand(insertCommand, clntConn);
                        insertSiloWell.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = clntID.ToString();
                        insertSiloWell.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        insertSiloWell.Parameters.AddWithValue("@welName", SqlDbType.Int).Value = WellName.ToString();
                        insertSiloWell.Parameters.AddWithValue("@welNameLower", SqlDbType.NVarChar).Value = wLwrName.ToString();
                        insertSiloWell.Parameters.AddWithValue("@regID", SqlDbType.Int).Value = RegionID.ToString();
                        insertSiloWell.Parameters.AddWithValue("@sregID", SqlDbType.Int).Value = SubRegionID.ToString();
                        insertSiloWell.Parameters.AddWithValue("@ctyID", SqlDbType.Int).Value = CountryID.ToString();
                        insertSiloWell.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                        insertSiloWell.Parameters.AddWithValue("@fldID", SqlDbType.Int).Value = FieldID.ToString();
                        insertSiloWell.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = StateID.ToString();
                        insertSiloWell.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        insertSiloWell.Parameters.AddWithValue("@welAPI", SqlDbType.NVarChar).Value = WellAPI.ToString();
                        insertSiloWell.Parameters.AddWithValue("@welUWI", SqlDbType.NVarChar).Value = WellUWI.ToString();
                        insertSiloWell.Parameters.AddWithValue("@welPermit", SqlDbType.NVarChar).Value = WellPermit.ToString();
                        insertSiloWell.Parameters.AddWithValue("@welSpudDate", SqlDbType.DateTime).Value = WellDate.ToString();
                        insertSiloWell.Parameters.AddWithValue("@welLongitude", SqlDbType.Decimal).Value = WellLong.ToString();
                        insertSiloWell.Parameters.AddWithValue("@welLngCardinal", SqlDbType.Int).Value = WellLongOrientation.ToString();
                        insertSiloWell.Parameters.AddWithValue("@welLatitude", SqlDbType.Decimal).Value = WellLat.ToString();
                        insertSiloWell.Parameters.AddWithValue("@welLatCardinal", SqlDbType.Int).Value = WellLatOrientation.ToString();
                        insertSiloWell.Parameters.AddWithValue("@welLocation", SqlDbType.VarChar).Value = location.ToString();
                        insertSiloWell.Parameters.AddWithValue("@welLatDegree", SqlDbType.Int).Value = cLatDeg.ToString();
                        insertSiloWell.Parameters.AddWithValue("@welLatMinutes", SqlDbType.Int).Value = cLatMin.ToString();
                        insertSiloWell.Parameters.AddWithValue("@welLatSeconds", SqlDbType.Decimal).Value = cLatSeconds.ToString();
                        insertSiloWell.Parameters.AddWithValue("@welLatOrientation", SqlDbType.Int).Value = WellLatOrientation.ToString();
                        insertSiloWell.Parameters.AddWithValue("@welLngDegree", SqlDbType.Int).Value = cLngDeg.ToString();
                        insertSiloWell.Parameters.AddWithValue("@welLngMinutes", SqlDbType.Int).Value = cLngMin.ToString();
                        insertSiloWell.Parameters.AddWithValue("@welLngSeconds", SqlDbType.Decimal).Value = cLngSeconds.ToString();
                        insertSiloWell.Parameters.AddWithValue("@welLngOrientation", SqlDbType.Int).Value = WellLongOrientation.ToString();
                        insertSiloWell.Parameters.AddWithValue("@mmdlID", SqlDbType.Int).Value = MagModelID.ToString();
                        insertSiloWell.Parameters.AddWithValue("@cordID", SqlDbType.Int).Value = CoordinateID.ToString();
                        insertSiloWell.Parameters.AddWithValue("@projID", SqlDbType.Int).Value = ProjectionID.ToString();
                        insertSiloWell.Parameters.AddWithValue("@wstID", SqlDbType.Int).Value = WellSMARTsStatus.ToString();
                        insertSiloWell.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insertSiloWell.Parameters.AddWithValue("@domain", SqlDbType.NVarChar).Value = UserRealm.ToString();
                        insertSiloWell.Parameters.AddWithValue("@cTime", SqlDbType.DateTime).Value = DateTime.Now.ToString();
                        insertSiloWell.Parameters.AddWithValue("@uTime", SqlDbType.DateTime).Value = DateTime.Now.ToString();

                        wlID = Convert.ToInt32(insertSiloWell.ExecuteScalar());
                        //Inserting Well into WellPad for Silo
                        wpw = insertWellIntoWellPad(WellPadID, wlID, DBString);
                        if (wpw >= (1))
                        {
                            wFolder = createWellFolder(domain, WellPadID, wlID, OperatorID, opLwrName);

                            if (wFolder.Equals(1))
                            {
                                result = 1;
                                wStat = getCTOStat(clntID, OperatorID);
                                if (wStat.Equals(1))
                                {
                                    return Convert.ToInt32(result);
                                }
                                else
                                {
                                    result = addClientToOperator(clntID, OperatorID, UserName, UserRealm);
                                    if (result.Equals(1))
                                    {
                                        return result;
                                    }
                                    else
                                    {
                                        return -1;
                                    }
                                }
                            }
                            else
                            {
                                return -1;
                            }
                        }
                        else
                        {
                            result.Equals(-1);
                            return Convert.ToInt32(result);
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Add Well to DemogDB
        public static Int32 addWellDemog(Int32 OperatorID, String OperatorName, String WellName, Decimal WellLong, Int32 WellLongOrientation, Decimal WellLat, Int32 WellLatOrientation, Int32 WellPadID, String WellAPI, String WellUWI, String WellPermit, DateTime WellDate, Int32 MagModelID, Int32 CoordinateID, Int32 ProjectionID, Int32 WellSMARTsStatus, String domain, String UserName, String UserRealm)
        {
            try
            {
                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;
                Int32 clntID = -99, cLatDeg = -99, cLatMin = -99, cLngDeg = -99, cLngMin = -99, result = -99, siloID = -99, wlID = -99, wpw = -99;
                Int32 latCompID = -99, lngCompID = -99; //Does ClientToOperator exist
                String wLwrName = String.Empty, cLatitude = String.Empty, cLongitude = String.Empty, DBName = String.Empty, DBString = String.Empty;
                Decimal cLatSeconds = -99.0000M, cLngSeconds = -99.0000M;
                Int32 RegionID = -99, SubRegionID = -99, CountryID = -99, StateID = -99, CountyID = -99, FieldID = -99;
                SqlGeography location = SqlGeography.Point(Convert.ToDouble(WellLat), Convert.ToDouble(WellLong), 4236);
                clntID = getClientID(domain);
                siloID = getSiloID(domain);
                clntID = getClientID(UserRealm);
                DBName = CLNT.getClientDB(clntID);
                DBString = CLNT.getDBCon(DBName);
                List<Int32> locIDs = getLocationIDForWell(WellPadID, DBString);
                RegionID = Convert.ToInt32(0);
                SubRegionID = Convert.ToInt32(1);
                CountryID = Convert.ToInt32(2);
                StateID = Convert.ToInt32(3);
                CountyID = Convert.ToInt32(4);
                FieldID = Convert.ToInt32(5);
                clntID = getClientID(domain);
                siloID = getSiloID(domain);
                clntID = getClientID(UserRealm);
                String connString = WebConfigurationManager.ConnectionStrings["FEDemogDB"].ConnectionString;

                using (SqlConnection clntConn = new SqlConnection(connString))
                {
                    try
                    {
                        String insertCommand = "INSERT INTO [dmgWell] ([clntID], [optrID], [welName], [welNameLower], [regID], [sregID], [ctyID], [cntyID], [fldID], [stID], [wpdID], [welAPI], [welUWI], [welPermit], [welSpudDate], [welLongitude], [welLngCardinal], [welLatitude], [welLatCardinal], [welLocation], [welLatDegree], [welLatMinutes], [welLatSeconds], [welLatOrientation], [welLngDegree], [welLngMinutes], [welLngSeconds], [welLngOrientation], [mmdlID], [cordID], [projID], [wstID], [username], [domain], [cTime], [uTime]) VALUES (@clntID, @optrID, @welName, @welNameLower, @regID, @sregID, @ctyID, @cntyID, @fldID, @stID, @wpdID, @welAPI, @welUWI, @welPermit, @welSpudDate, @welLongitude, @welLngCardinal, @welLatitude, @welLatCardinal, @welLocation, @welLatDegree, @welLatMinutes, @welLatSeconds, @welLatOrientation, @welLngDegree, @welLngMinutes, @welLngSeconds, @welLngOrientation, @mmdlID, @cordID, @projID, @wstID, @username, @domain, @cTime, @uTime); SELECT SCOPE_IDENTITY();";

                        TextInfo tiWNme = new CultureInfo("en-US", false).TextInfo;
                        wLwrName = tiWNme.ToLower(WellName);
                        wLwrName = wLwrName.Replace(" ", "");

                        clntConn.Open();

                        SqlCommand insertDemogWell = new SqlCommand(insertCommand, clntConn);
                        insertDemogWell.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = clntID.ToString();
                        insertDemogWell.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        insertDemogWell.Parameters.AddWithValue("@welName", SqlDbType.Int).Value = WellName.ToString();
                        insertDemogWell.Parameters.AddWithValue("@welNameLower", SqlDbType.NVarChar).Value = wLwrName.ToString();
                        insertDemogWell.Parameters.AddWithValue("@regID", SqlDbType.Int).Value = RegionID.ToString();
                        insertDemogWell.Parameters.AddWithValue("@sregID", SqlDbType.Int).Value = SubRegionID.ToString();
                        insertDemogWell.Parameters.AddWithValue("@ctyID", SqlDbType.Int).Value = CountryID.ToString();
                        insertDemogWell.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                        insertDemogWell.Parameters.AddWithValue("@fldID", SqlDbType.Int).Value = FieldID.ToString();
                        insertDemogWell.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = StateID.ToString();
                        insertDemogWell.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        insertDemogWell.Parameters.AddWithValue("@welAPI", SqlDbType.NVarChar).Value = WellAPI.ToString();
                        insertDemogWell.Parameters.AddWithValue("@welUWI", SqlDbType.NVarChar).Value = WellUWI.ToString();
                        insertDemogWell.Parameters.AddWithValue("@welPermit", SqlDbType.NVarChar).Value = WellPermit.ToString();
                        insertDemogWell.Parameters.AddWithValue("@welSpudDate", SqlDbType.DateTime).Value = WellDate.ToString();
                        insertDemogWell.Parameters.AddWithValue("@welLongitude", SqlDbType.Decimal).Value = WellLong.ToString();
                        insertDemogWell.Parameters.AddWithValue("@welLngCardinal", SqlDbType.Int).Value = "-99".ToString();
                        insertDemogWell.Parameters.AddWithValue("@welLatitude", SqlDbType.Decimal).Value = WellLat.ToString();
                        insertDemogWell.Parameters.AddWithValue("@welLatCardinal", SqlDbType.Int).Value = "-99".ToString();
                        insertDemogWell.Parameters.AddWithValue("@welLocation", SqlDbType.VarChar).Value = location.ToString();
                        insertDemogWell.Parameters.AddWithValue("@welLatDegree", SqlDbType.Int).Value = cLatDeg.ToString();
                        insertDemogWell.Parameters.AddWithValue("@welLatMinutes", SqlDbType.Int).Value = cLatMin.ToString();
                        insertDemogWell.Parameters.AddWithValue("@welLatSeconds", SqlDbType.Decimal).Value = cLatSeconds.ToString();
                        insertDemogWell.Parameters.AddWithValue("@welLatOrientation", SqlDbType.Int).Value = latCompID.ToString();
                        insertDemogWell.Parameters.AddWithValue("@welLngDegree", SqlDbType.Int).Value = cLngDeg.ToString();
                        insertDemogWell.Parameters.AddWithValue("@welLngMinutes", SqlDbType.Int).Value = cLngMin.ToString();
                        insertDemogWell.Parameters.AddWithValue("@welLngSeconds", SqlDbType.Decimal).Value = cLngSeconds.ToString();
                        insertDemogWell.Parameters.AddWithValue("@welLngOrientation", SqlDbType.Int).Value = lngCompID.ToString();
                        insertDemogWell.Parameters.AddWithValue("@mmdlID", SqlDbType.Int).Value = MagModelID.ToString();
                        insertDemogWell.Parameters.AddWithValue("@cordID", SqlDbType.Int).Value = CoordinateID.ToString();
                        insertDemogWell.Parameters.AddWithValue("@projID", SqlDbType.Int).Value = ProjectionID.ToString();
                        insertDemogWell.Parameters.AddWithValue("@wstID", SqlDbType.Int).Value = WellSMARTsStatus.ToString();
                        insertDemogWell.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insertDemogWell.Parameters.AddWithValue("@domain", SqlDbType.NVarChar).Value = UserRealm.ToString();
                        insertDemogWell.Parameters.AddWithValue("@cTime", SqlDbType.DateTime).Value = crTime.ToString();
                        insertDemogWell.Parameters.AddWithValue("@uTime", SqlDbType.DateTime).Value = updTime.ToString();

                        wlID = Convert.ToInt32(insertDemogWell.ExecuteScalar());
                        wpw = insertGlobalWellToWellPad(WellPadID, wlID);
                        if (wlID >= 1 && wpw.Equals(1))
                        {
                            result = 1;
                        }
                        else
                        {
                            result = -1;
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }
                return result;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 checkTIExitsForLastRun(Decimal MeasuredDepth, Int32 WellID, Int32 WellSectionID, Int32 RunID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selStat = "SELECT TOP 1 1 FROM [RawQCResults] WHERE [Depth] = @Depth AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selStat, dataCon);
                        getData.Parameters.AddWithValue("@Depth", SqlDbType.Int).Value = MeasuredDepth.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        //Create Well Folders 
        private static Int32 createWellFolder(String domain, Int32 siloWellPadID, Int32 siloWellID, Int32 wellOperator, String operatorName)
        {
            try
            {
                Int32 result = -99;

                //Get Folder Paths from configuration
                String genDirectory = WebConfigurationManager.AppSettings["GenPath"].ToString();
                String uplDirectory = WebConfigurationManager.AppSettings["UploadPath"].ToString();

                //Creating Well Folder and saving Permit file to it
                String clntDomain = String.Empty, oprName = String.Empty;
                clntDomain = Convert.ToString(domain);
                clntDomain = clntDomain.Replace(".", "_");
                oprName = CLNT.getClientName(wellOperator);
                oprName = oprName.Replace(" ", "_");
                String pdName = String.Concat("WellPad_", siloWellPadID);
                String wlName = String.Concat("Well_", siloWellID);
                TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                clntDomain = ti.ToLower(clntDomain);
                oprName = ti.ToLower(oprName);
                String wlgFolder = ""; //Well Gen. Folder
                String wlgFolderRaw = "";
                String wlgFolderRes = "";
                String wlgFolder3rd = "";
                String wlgFolderHis = "";
                String wlgFolderCorr = "";
                String wluFolder = ""; //Well Upl. Folder
                String wlPermit = "";
                String wluFolderRaw = "";
                String wluFolderRes = "";
                String wluFolder3rd = "";
                String wluFolderHis = "";
                String wluFolderCorr = "";

                wlgFolder = String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\", genDirectory, clntDomain, "Assets", "WellPad", "Wells", oprName, pdName, wlName);
                wlgFolderRaw = String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\{7}\\", genDirectory, clntDomain, "Assets", "WellPad", "Wells", oprName, pdName, wlName, "RawDataQC");
                wlgFolderRes = String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\{7}\\", genDirectory, clntDomain, "Assets", "WellPad", "Wells", oprName, pdName, wlName, "ResultsQC");
                wlgFolder3rd = String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\{7}\\", genDirectory, clntDomain, "Assets", "WellPad", "Wells", oprName, pdName, wlName, "3rdQC");
                wlgFolderHis = String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\{7}\\", genDirectory, clntDomain, "Assets", "WellPad", "Wells", oprName, pdName, wlName, "HistoricQC");
                wlgFolderCorr = String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\{7}\\", genDirectory, clntDomain, "Assets", "WellPad", "Wells", oprName, pdName, wlName, "Correction");
                wluFolder = String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\", uplDirectory, clntDomain, "Assets", "WellPad", "Wells", oprName, pdName, wlName);
                wlPermit = String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\{7}\\", uplDirectory, clntDomain, "Assets", "WellPad", "Wells", oprName, pdName, wlName, "Well_Permit");
                wluFolderRaw = String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\{7}\\", uplDirectory, clntDomain, "Assets", "WellPad", "Wells", oprName, pdName, wlName, "RawDataQC");
                wluFolderRes = String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\{7}\\", uplDirectory, clntDomain, "Assets", "WellPad", "Wells", oprName, pdName, wlName, "ResultsQC");
                wluFolder3rd = String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\{7}\\", uplDirectory, clntDomain, "Assets", "WellPad", "Wells", oprName, pdName, wlName, "3rdQC");
                wluFolderHis = String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\{7}\\", uplDirectory, clntDomain, "Assets", "WellPad", "Wells", oprName, pdName, wlName, "HistoricQC");
                wluFolderCorr = String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\{7}\\", uplDirectory, clntDomain, "Assets", "WellPad", "Wells", oprName, pdName, wlName, "Correction");

                DirectoryInfo clientGenFolder = new DirectoryInfo(wlgFolder);
                DirectoryInfo clientgRawFolder = new DirectoryInfo(wlgFolderRaw);
                DirectoryInfo clientgResFolder = new DirectoryInfo(wlgFolderRes);
                DirectoryInfo clientg3rdFolder = new DirectoryInfo(wlgFolder3rd);
                DirectoryInfo clientgHisFolder = new DirectoryInfo(wlgFolderHis);
                DirectoryInfo clientgCorrFolder = new DirectoryInfo(wlgFolderCorr);
                DirectoryInfo clientUplFolder = new DirectoryInfo(wluFolder);
                DirectoryInfo clientUplPermit = new DirectoryInfo(wlPermit);
                DirectoryInfo clientuRawFolder = new DirectoryInfo(wlgFolderRaw);
                DirectoryInfo clientuResFolder = new DirectoryInfo(wlgFolderRes);
                DirectoryInfo clientu3rdFolder = new DirectoryInfo(wlgFolder3rd);
                DirectoryInfo clientuHisFolder = new DirectoryInfo(wlgFolderHis);
                DirectoryInfo clientuCorrFolder = new DirectoryInfo(wlgFolderCorr);
                if ((!Directory.Exists(wlgFolder)) || (!Directory.Exists(wluFolder)) || (!Directory.Exists(wlPermit)) || (!Directory.Exists(wlgFolderRaw)) || (!Directory.Exists(wlgFolderRes)) || (!Directory.Exists(wlgFolder3rd)) || (!Directory.Exists(wlgFolderHis)) || (!Directory.Exists(wlgFolderCorr)) || (!Directory.Exists(wluFolderRaw)) || (!Directory.Exists(wluFolderRes)) || (!Directory.Exists(wluFolder3rd)) || (!Directory.Exists(wluFolderHis)) || (!Directory.Exists(wluFolderCorr)))
                {
                    clientGenFolder.Create();
                    clientgRawFolder.Create();
                    clientgResFolder.Create();
                    clientg3rdFolder.Create();
                    clientgHisFolder.Create();
                    clientgCorrFolder.Create();
                    clientUplFolder.Create();
                    clientUplPermit.Create();
                    clientuRawFolder.Create();
                    clientuResFolder.Create();
                    clientu3rdFolder.Create();
                    clientuHisFolder.Create();
                    clientuCorrFolder.Create();
                }

                if ((Directory.Exists(wlgFolder)) || (Directory.Exists(wluFolder)) || (Directory.Exists(wlPermit)) || (Directory.Exists(wlgFolderRaw)) || (Directory.Exists(wlgFolderRes)) || (Directory.Exists(wlgFolder3rd)) || (Directory.Exists(wlgFolderHis)) || (Directory.Exists(wluFolderRaw)) || (Directory.Exists(wluFolderRes)) || (Directory.Exists(wluFolder3rd)) || (Directory.Exists(wluFolderHis)))
                {
                    result = 1;
                    return result;
                }
                else
                {
                    result = -1;
                    return result;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 findDuplicateSQCName(String SQCName, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT TOP 1 1 FROM [SQC] WHERE [sqcName] = @sqcName";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@sqcName", SqlDbType.NVarChar).Value = SQCName.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 findDuplicateGlobalWellPadName(String WellPadName, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT TOP 1 1 FROM [dmgWellPad] WHERE [welPadNameLower] = @welPadNameLower";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welPadNameLower", SqlDbType.NVarChar).Value = WellPadName.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 findDuplicateWellPadName(String WellPadName, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT TOP 1 1 FROM [WellPad] WHERE [welPadNameLower] = @welPadNameLower";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welPadNameLower", SqlDbType.NVarChar).Value = WellPadName.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 findTIForRun(Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 WellPadID, Int32 ClientID, Int32 OperatorID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT TOP 1 1 FROM [RunTieIn] WHERE [runID] = @runID AND [wswID] = @wswID AND [welID] = @welID AND [wpdID] = @wpdID AND [clntID] = @clntID AND [optrID] = @optrID";
                using (SqlConnection dataConn = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataConn.Open();
                        SqlCommand getData = new SqlCommand(selData, dataConn);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataConn.Close();
                        dataConn.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 findBHAForRun(Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 WellPadID, Int32 ClientID, Int32 OperatorID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT TOP 1 1 FROM [BHA] WHERE [runID] = @runID AND [wswID] = @wswID AND [welID] = @welID AND [wpdID] = @wpdID AND [clntID] = @clntID AND [optrID] = @optrID";
                using (SqlConnection dataConn = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataConn.Open();
                        SqlCommand getData = new SqlCommand(selData, dataConn);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataConn.Close();
                        dataConn.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 findWellInRefMags(Int32 WellID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT TOP 1 1 FROM [RefMags] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 findWellInWellPlan(Int32 WellID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT TOP 1 1 FROM [WellPlanMetaInfo] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 findDuplicateWellServiceRegistration(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 ServiceGroupID, Int32 ServiceID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT TOP 1 1 FROM [WellServiceRegistration] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID AND [welID] = @welID AND [srvGrpID] = @srvGrpID AND [srvID] = @srvID ";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@srvGrpID", SqlDbType.Int).Value = ServiceGroupID.ToString();
                        getData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }                            

        public static Dictionary<Int32, String> getAccelerometerUnits()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();

                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [acuID], [acuName] FROM [dmgAccelUnit] ORDER BY [acuName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    id = readData.GetInt32(0);
                                    name = readData.GetString(1);
                                    iReply.Add(id, name);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getAccelUnitName(Int32 AccelUnitID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [acuName] FROM [dmgAccelUnit] WHERE [acuID] = @acuID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@acuID", SqlDbType.Int).Value = AccelUnitID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getActiveBHACount(Int32 RunID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(bhaID) FROM [BHA] WHERE [runID] = @runID AND [dmgstID] = 1";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                        if (iReply.Equals(0))
                                        {
                                            iReply = -99;
                                        }
                                    }
                                }
                                else
                                {
                                    iReply = -99;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getBhaCount(Int32 RunID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selBHA = "SELECT COUNT(bhaID) FROM [BHA] WHERE [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selBHA, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readCount = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readCount.HasRows)
                                {
                                    while (readCount.Read())
                                    {
                                        iReply = readCount.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    iReply = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readCount.Close();
                                readCount.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        
        
        public static DataTable getBhaSign(Int32 RunID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn bhaID = iReply.Columns.Add("bhaID", typeof(Int32));
                DataColumn bhaName = iReply.Columns.Add("bhaName", typeof(String));
                DataColumn bhaMSize = iReply.Columns.Add("bhaMSize", typeof(Decimal));
                DataColumn bhaMSizeUID = iReply.Columns.Add("bhaMSizeUID", typeof(String));
                DataColumn bhaDSCSize = iReply.Columns.Add("bhaDSCSize", typeof(Decimal));
                DataColumn bhaDSCUID = iReply.Columns.Add("bhaDSCUID", typeof(String));
                DataColumn bhaMDBLen = iReply.Columns.Add("bhaMDBLen", typeof(Decimal));
                DataColumn bhaMDBLenUID = iReply.Columns.Add("bhaMDBLenUID", typeof(String));
                DataColumn bhaNMSpAbv = iReply.Columns.Add("bhaNMSpAbv", typeof(Decimal));
                DataColumn bhaNMSAUID = iReply.Columns.Add("bhaNMSAUID", typeof(String));
                DataColumn bhaNMSpBlw = iReply.Columns.Add("bhaNMSpBlw", typeof(Decimal));
                DataColumn bhaNMSBUID = iReply.Columns.Add("bhaNMSBUID", typeof(String));
                DataColumn dmgstID = iReply.Columns.Add("dmgstID", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));

                Int32 id = -99, mU = -99, dcU = -99, lnU = -99, spaU = -99, spbU = -99, stID = -99;
                Decimal mSz = -99.00M, dcSz = -99.00M, len = -99.00M, sAbv = -99.00M, sBlw = -99.00M;
                DateTime uT = DateTime.Now;

                String mSize = String.Empty, dcSize = String.Empty, lenSize = String.Empty, abvUnit = String.Empty, blwUnit = String.Empty, status = String.Empty, name = String.Empty;
                String selData = "SELECT [bhaID], [bhaName], [bhaMSize], [bhaMSizeUID], [bhaDSCSize], [bhaDSCUID], [bhaMDBLen], [bhaMDBLenUID], [bhaNMSpAbv], [bhaNMSAUID], [bhaNMSpBlw], [bhaNMSBUID], [dmgstID], [uTime] FROM [BHA] WHERE [runID] = @runID";
                
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        mSz = readData.GetDecimal(2);
                                        mU = readData.GetInt32(3);
                                        mSize = getLengthUnitName(mU);
                                        dcSz = readData.GetDecimal(4);
                                        dcU = readData.GetInt32(5);
                                        dcSize = getLengthUnitName(dcU);
                                        len = readData.GetDecimal(6);
                                        lnU = readData.GetInt32(7);
                                        lenSize = getLengthUnitName(lnU);
                                        sAbv = readData.GetDecimal(8);
                                        spaU = readData.GetInt32(9);
                                        abvUnit = getLengthUnitName(spaU);
                                        sBlw = readData.GetDecimal(10);
                                        spbU = readData.GetInt32(11);
                                        blwUnit = getLengthUnitName(spbU);
                                        stID = readData.GetInt32(12);
                                        status = DMG.getStatusValue(stID);
                                        uT = readData.GetDateTime(13);
                                        iReply.Rows.Add(id, name, mSz, mSize, dcSz, dcSize, len, lenSize, sAbv, abvUnit, sBlw, blwUnit, status, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getBhaDetail(Int32 BhaID, String ClientDBString)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn bdID = iReply.Columns.Add("bdID", typeof(Int32));
                DataColumn MGs = iReply.Columns.Add("MGs", typeof(Decimal));
                DataColumn MGsU = iReply.Columns.Add("MGsU", typeof(String));
                DataColumn dTop = iReply.Columns.Add("dTop", typeof(Decimal));
                DataColumn dTopU = iReply.Columns.Add("dTopU", typeof(String));
                DataColumn DC = iReply.Columns.Add("DC", typeof(Decimal));
                DataColumn DCU = iReply.Columns.Add("DCU", typeof(String));
                DataColumn dBtm = iReply.Columns.Add("dBtm", typeof(Decimal));
                DataColumn dBtmU = iReply.Columns.Add("dBtmU", typeof(String));
                Int32 id = -99, motorGU = -99, dscGU = -99, dtopU = -99, dbtmU = -99;
                Decimal motorG = -99.000000M, dstringG = -99.000000M, disT = -99.00M, disB = -99.00M;
                String mgauss = String.Empty, dcgauss = String.Empty, topDis = String.Empty, btmDis = String.Empty;
                String selData = "SELECT [bdID], [bhaMGVal], [bhaMGValUID], [bhaDGTop], [bhaDGTUID], [bhaDSGVal], [bhaDSGUID], [bhaDGBottom], [bhaDGBUID] FROM BHADetail WHERE [bhaID] = @bhaID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@bhaID", SqlDbType.Int).Value = BhaID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        motorG = readData.GetDecimal(1);
                                        motorGU = readData.GetInt32(2);
                                        mgauss = getMagUnitName(motorGU);
                                        disT = readData.GetDecimal(3);
                                        dtopU = readData.GetInt32(4);
                                        topDis = getLengthUnitName(dtopU);
                                        dstringG = readData.GetDecimal(5);
                                        dscGU = readData.GetInt32(6);
                                        dcgauss = getMagUnitName(dscGU);
                                        disB = readData.GetDecimal(7);
                                        dbtmU = readData.GetInt32(8);
                                        btmDis = getLengthUnitName(dbtmU);
                                        iReply.Rows.Add(id, motorG, mgauss, disT, topDis, dstringG, dcgauss, disB, btmDis);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getJSonString(DataTable infoTable)
        {
            try
            {
                String JSONString = String.Empty;
                JSONString = JS.JsonConvert.SerializeObject(infoTable);
                return JSONString;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getWPBhaSign(Int32 WellID, Int32 RunID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn bhaID = iReply.Columns.Add("bhaID", typeof(Int32));
                DataColumn bhaName = iReply.Columns.Add("bhaName", typeof(String));
                DataColumn bhaMSize = iReply.Columns.Add("bhaMSize", typeof(Decimal));
                DataColumn bhaMSizeUID = iReply.Columns.Add("bhaMSizeUID", typeof(String));
                DataColumn bhaDSCSize = iReply.Columns.Add("bhaDSCSize", typeof(Decimal));
                DataColumn bhaDSCUID = iReply.Columns.Add("bhaDSCUID", typeof(String));
                DataColumn bhaMDBLen = iReply.Columns.Add("bhaMDBLen", typeof(Decimal));
                DataColumn bhaMDBLenUID = iReply.Columns.Add("bhaMDBLenUID", typeof(String));
                DataColumn bhaNMSpAbv = iReply.Columns.Add("bhaNMSpAbv", typeof(Decimal));
                DataColumn bhaNMSAUID = iReply.Columns.Add("bhaNMSAUID", typeof(String));
                DataColumn bhaNMSpBlw = iReply.Columns.Add("bhaNMSpBlw", typeof(Decimal));
                DataColumn bhaNMSBUID = iReply.Columns.Add("bhaNMSBUID", typeof(String));
                DataColumn dmgstID = iReply.Columns.Add("dmgstID", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));

                Int32 id = -99, mU = -99, dcU = -99, lnU = -99, spaU = -99, spbU = -99, stID = -99;
                Decimal mSz = -99.00M, dcSz = -99.00M, len = -99.00M, sAbv = -99.00M, sBlw = -99.00M;
                DateTime uT = DateTime.Now;

                String name = String.Empty, mSize = String.Empty, dcSize = String.Empty, lenSize = String.Empty, abvUnit = String.Empty, blwUnit = String.Empty, status = String.Empty;
                String selData = "SELECT [bhaID], [bhaName], [bhaMSize], [bhaMSizeUID], [bhaDSCSize], [bhaDSCUID], [bhaMDBLen], [bhaMDBLenUID], [bhaNMSpAbv], [bhaNMSAUID], [bhaNMSpBlw], [bhaNMSBUID], [dmgstID], [uTime] FROM [WPBHA] WHERE [runID] IN (SELECT [runID] FROM [WPRun] WHERE [welID] = @welID)";

                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        mSz = readData.GetDecimal(2);
                                        mU = readData.GetInt32(3);
                                        mSize = getLengthUnitName(mU);
                                        dcSz = readData.GetDecimal(4);
                                        dcU = readData.GetInt32(5);
                                        dcSize = getLengthUnitName(dcU);
                                        len = readData.GetDecimal(6);
                                        lnU = readData.GetInt32(7);
                                        lenSize = getLengthUnitName(lnU);
                                        sAbv = readData.GetDecimal(8);
                                        spaU = readData.GetInt32(9);
                                        abvUnit = getLengthUnitName(spaU);
                                        sBlw = readData.GetDecimal(10);
                                        spbU = readData.GetInt32(11);
                                        blwUnit = getLengthUnitName(spbU);
                                        stID = readData.GetInt32(12);
                                        status = DMG.getStatusValue(stID);
                                        uT = readData.GetDateTime(13);
                                        iReply.Rows.Add(id, name, mSz, mSize, dcSz, dcSize, len, lenSize, sAbv, abvUnit, sBlw, blwUnit, status, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getWPBHACount(Int32 RunID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(bhaID) FROM [WPBHA] WHERE [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    iReply = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getWPBhaDetail(Int32 BhaID, String ClientDBString)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn bdID = iReply.Columns.Add("bdID", typeof(Int32));
                DataColumn MGs = iReply.Columns.Add("MGs", typeof(Decimal));
                DataColumn MGsU = iReply.Columns.Add("MGsU", typeof(String));
                DataColumn dTop = iReply.Columns.Add("dTop", typeof(Decimal));
                DataColumn dTopU = iReply.Columns.Add("dTopU", typeof(String));
                DataColumn DC = iReply.Columns.Add("DC", typeof(Decimal));
                DataColumn DCU = iReply.Columns.Add("DCU", typeof(String));
                DataColumn dBtm = iReply.Columns.Add("dBtm", typeof(Decimal));
                DataColumn dBtmU = iReply.Columns.Add("dBtmU", typeof(String));
                Int32 id = -99, motorGU = -99, dscGU = -99, dtopU = -99, dbtmU = -99;
                Decimal motorG = -99.000000M, dstringG = -99.000000M, disT = -99.00M, disB = -99.00M;
                String mgauss = String.Empty, dcgauss = String.Empty, topDis = String.Empty, btmDis = String.Empty;
                String selData = "SELECT [bdID], [bhaMGVal], [bhaMGValUID], [bhaDGTop], [bhaDGTUID], [bhaDSGVal], [bhaDSGUID], [bhaDGBottom], [bhaDGBUID] FROM [WPBHADetail] WHERE [bhaID] = @bhaID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@bhaID", SqlDbType.Int).Value = BhaID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        motorG = readData.GetDecimal(1);
                                        motorGU = readData.GetInt32(2);
                                        mgauss = getMagUnitName(motorGU);
                                        disT = readData.GetDecimal(3);
                                        dtopU = readData.GetInt32(4);
                                        topDis = getLengthUnitName(dtopU);
                                        dstringG = readData.GetDecimal(5);
                                        dscGU = readData.GetInt32(6);
                                        dcgauss = getMagUnitName(dscGU);
                                        disB = readData.GetDecimal(7);
                                        dbtmU = readData.GetInt32(8);
                                        btmDis = getLengthUnitName(dbtmU);
                                        iReply.Rows.Add(id, motorG, mgauss, disT, topDis, dstringG, dcgauss, disB, btmDis);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static ArrayList getBhaInfoList(Int32 RunID, String ClientDBString)
        {
            try
            {
                ArrayList iReply = new ArrayList();
                Int32 id = -99, mU = -99, dcU = -99, lnU = -99, spaU = -99, spbU = -99, stID = -99, motorGU = -99, dscGU = -99, dtopU = -99, dbtmU = -99;
                Decimal mSz = -99.00M, dcSz = -99.00M, len = -99.00M, sAbv = -99.00M, sBlw = -99.00M, motorG = -99.000000M, dstringG = -99.000000M, disT = -99.00M, disB = -99.00M;
                String mgauss = String.Empty, dcgauss = String.Empty, topDis = String.Empty, btmDis = String.Empty;
                String mSize = String.Empty, dcSize = String.Empty, lenSize = String.Empty, abvUnit = String.Empty, blwUnit = String.Empty, status = String.Empty;
                String selData = "SELECT [bhaID], [bhaMSize], [bhaMSizeUID], [bhaDSCSize], [bhaDSCUID], [bhaMDBLen], [bhaMDBLenUID], [bhaNMSpAbv], [bhaNMSAUID], [bhaNMSpBlw], [bhaNMSBUID], [dmgstID] FROM [BHA] WHERE [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        mSz = iReply.Add(readData.GetDecimal(1));
                                        mU = iReply.Add(readData.GetInt32(2));
                                        dcSz = iReply.Add(readData.GetDecimal(3));
                                        dcU = iReply.Add(readData.GetInt32(4));
                                        len = iReply.Add(readData.GetDecimal(5));
                                        lnU = iReply.Add(readData.GetInt32(6));
                                        sAbv = iReply.Add(readData.GetDecimal(7));
                                        spaU = iReply.Add(readData.GetInt32(8));
                                        sBlw = iReply.Add(readData.GetDecimal(9));
                                        spbU = iReply.Add(readData.GetInt32(10));
                                        stID = iReply.Add(readData.GetInt32(11));
                                        motorG = iReply.Add(0.00M);
                                        motorGU = iReply.Add(1);
                                        disT = iReply.Add(0.00M);
                                        dtopU = iReply.Add(5);
                                        dstringG = iReply.Add(0.00M);
                                        dscGU = iReply.Add(1);
                                        disB = iReply.Add(0.00M);
                                        dbtmU = iReply.Add(5);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static ArrayList getWPBhaInfoList(Int32 BHAID, String ClientDBString)
        {
            try
            {
                ArrayList iReply = new ArrayList();
                Int32 id = -99, mU = -99, dcU = -99, lnU = -99, spaU = -99, spbU = -99, stID = -99, motorGU = -99, dscGU = -99, dtopU = -99, dbtmU = -99;
                Decimal mSz = -99.00M, dcSz = -99.00M, len = -99.00M, sAbv = -99.00M, sBlw = -99.00M, motorG = -99.000000M, dstringG = -99.000000M, disT = -99.00M, disB = -99.00M;
                String mgauss = String.Empty, dcgauss = String.Empty, topDis = String.Empty, btmDis = String.Empty;
                String mSize = String.Empty, dcSize = String.Empty, lenSize = String.Empty, abvUnit = String.Empty, blwUnit = String.Empty, status = String.Empty;
                String selData = "SELECT [bhaID], [bhaMSize], [bhaMSizeUID], [bhaDSCSize], [bhaDSCUID], [bhaMDBLen], [bhaMDBLenUID], [bhaNMSpAbv], [bhaNMSAUID], [bhaNMSpBlw], [bhaNMSBUID], [dmgstID] FROM [WPBHA] WHERE [bhaID] = @bhaID";
                String selDData = "SELECT [bhaMGVal], [bhaMGValUID], [bhaDGTop], [bhaDGTUID], [bhaDSGVal], [bhaDSGUID], [bhaDGBottom], [bhaDGBUID] FROM [WPBHADetail] WHERE [bhaID] = @bhaID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@bhaID", SqlDbType.Int).Value = BHAID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        mSz = iReply.Add(readData.GetDecimal(1));
                                        mU = iReply.Add(readData.GetInt32(2));
                                        dcSz = iReply.Add(readData.GetDecimal(3));
                                        dcU = iReply.Add(readData.GetInt32(4));
                                        len = iReply.Add(readData.GetDecimal(5));
                                        lnU = iReply.Add(readData.GetInt32(6));
                                        sAbv = iReply.Add(readData.GetDecimal(7));
                                        spaU = iReply.Add(readData.GetInt32(8));
                                        sBlw = iReply.Add(readData.GetDecimal(9));
                                        spbU = iReply.Add(readData.GetInt32(10));
                                        stID = iReply.Add(readData.GetInt32(11));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                        SqlCommand getDData = new SqlCommand(selDData, dataCon);
                        getDData.Parameters.AddWithValue("@bhaID", SqlDbType.Int).Value = id.ToString();
                        using (SqlDataReader readDData = getDData.ExecuteReader())
                        {
                            try
                            {
                                if (readDData.HasRows)
                                {
                                    while (readDData.Read())
                                    {
                                        motorG = iReply.Add(readDData.GetDecimal(0));
                                        motorGU = iReply.Add(readDData.GetInt32(1));
                                        disT = iReply.Add(readDData.GetDecimal(2));
                                        dtopU = iReply.Add(readDData.GetInt32(3));
                                        dstringG = iReply.Add(readDData.GetDecimal(4));
                                        dscGU = iReply.Add(readDData.GetInt32(5));
                                        disB = iReply.Add(readDData.GetDecimal(6));
                                        dbtmU = iReply.Add(readDData.GetInt32(7));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readDData.Close();
                                readDData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getConvergenceName(Int32 ConvergenceID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [jcName] FROM [dmgJobConvergence] WHERE [jcID] = @jcID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@jcID", SqlDbType.Int).Value = ConvergenceID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 getCTOStat(Int32 ClientID, Int32 OperatorID)
        {
            try
            {
                Int32 iReply = -99;
                String selComm = "SELECT [ctoID] FROM [ClientToOperator] WHERE [clntID] = @clntID AND [optrID] = @optrID";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand selData = new SqlCommand(selComm, dataCon);
                        selData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        selData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();

                        iReply = Convert.ToInt32(selData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Get Client Co ID for selected Well
        private static Int32 getClientID(String domain)
        {
            try
            {
                Int32 clientID = 0;
                clientID = -99;

                String connString = WebConfigurationManager.ConnectionStrings["FEUsersDB"].ConnectionString;
                String selectCommand = "SELECT [clntID] FROM [Client] WHERE [clntRealm] = @clientRealm";

                SqlConnection clntConn = new SqlConnection(connString);
                SqlCommand getClient = new SqlCommand(selectCommand, clntConn);
                getClient.Parameters.AddWithValue("@clientRealm", SqlDbType.Int).Value = domain.ToString();

                clntConn.Open();

                SqlDataReader clntDataRow = getClient.ExecuteReader();
                if (clntDataRow.HasRows)
                {
                    while (clntDataRow.Read())
                    {
                        clientID = clntDataRow.GetInt32(0);
                    }
                }
                clntDataRow.Close();
                clntConn.Close();

                return clientID;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getClientActiveWellCount(String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99 ,rID = -99, rsID = -99;
                String selARun = "SELECT [runID], [wswID] FROM [Run] WHERE [dmgstID] = 1";
                String selAWell = "SELECT [welID] FROM [WellSectionToWell] WHERE [wswID] = @wswID";
                Dictionary<Int32, Int32> runList = new Dictionary<Int32, Int32>();
                List<Int32> wellList = new List<Int32>();
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getRun = new SqlCommand(selARun, dataCon);
                        using (SqlDataReader readRun = getRun.ExecuteReader())
                        {
                            try
                            {
                                if (readRun.HasRows)
                                {
                                    while (readRun.Read())
                                    {
                                        rID = readRun.GetInt32(0);
                                        rsID = readRun.GetInt32(1);
                                        runList.Add(rID, rsID);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readRun.Close();
                                readRun.Dispose();
                            }
                        }
                        foreach (KeyValuePair<Int32, Int32> pair in runList)
                        {
                            Int32 id = pair.Value;
                            SqlCommand getWell = new SqlCommand(selAWell, dataCon);
                            getWell.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = id.ToString();
                            using (SqlDataReader readWell = getWell.ExecuteReader())
                            {
                                try
                                {
                                    if (readWell.HasRows)
                                    {
                                        while (readWell.Read())
                                        {
                                            wellList.Add(readWell.GetInt32(0));
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readWell.Close();
                                    readWell.Dispose();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply = wellList.Count;
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getClientMissingDataWellCount(String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT count(welID) FROM [Well] WHERE [welID] NOT IN (SELECT [welID] FROM [RefMags])";
                //String selWells = "SELECT [welID] FROM [Well]";
                //String selWData = "SELECT COUNT(rfmgID) FROM [RefMags] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using(SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if(readData.HasRows)
                                {
                                    while(readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch(Exception ex)
                            {throw new System.Exception(ex.ToString());}
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }                        
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch(Exception ex)
            {throw new System.Exception(ex.ToString());}            
        }

        public static Int32 getClientMissingWellPlanCount(String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT count(welID) FROM [Well] WHERE [welID] NOT IN (SELECT [welID] FROM [WellPlanCalculatedData])";
                //String selWells = "SELECT [welID] FROM [Well]";
                //String selWData = "SELECT COUNT(rfmgID) FROM [RefMags] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getClientWellTableWithResults (String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn welID = iReply.Columns.Add("welID", typeof(Int32));
                DataColumn welName = iReply.Columns.Add("welName", typeof(String));
                DataColumn oprName = iReply.Columns.Add("oprName", typeof(String));
                DataColumn fldName = iReply.Columns.Add("fldName", typeof(String));
                DataColumn cnyName = iReply.Columns.Add("cnyName", typeof(String));
                DataColumn apiuwi = iReply.Columns.Add("apiuwi", typeof(String));
                DataColumn spudDate = iReply.Columns.Add("spudDate", typeof(String));
                
                Int32 wID = -99, opID = -99, fdID = -99, cnID = -99;
                String wName = String.Empty, opName = String.Empty, field = String.Empty, county = String.Empty, api = String.Empty, uwi = String.Empty, apuw = String.Empty, date = String.Empty;
                DateTime spud = DateTime.Now;
                String selData = "SELECT [welID], [optrID], [welName], [fldID], [cntyID], [welAPI], [welUWI], [welSpudDate] FROM [WELL] where WelID in (select distinct [welID] from RawQCResults)";
                
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection.ToString()))
                {
                    try
                    {
                        dataCon.Open();

                            SqlCommand getData = new SqlCommand(selData, dataCon);
                            using (SqlDataReader readData = getData.ExecuteReader())
                                {
                                    try
                                    {
                                        if (readData.HasRows)
                                        {
                                            while (readData.Read())
                                            {
                                                wID = readData.GetInt32(0);
                                                opID = readData.GetInt32(1);
                                                opName = CLNT.getClientName(opID);
                                                wName = readData.GetString(2);
                                                fdID = readData.GetInt32(3);
                                                field = DMG.getFieldName(fdID);
                                                cnID = readData.GetInt32(4);
                                                county = DMG.getCountyName(cnID);
                                                api = readData.GetString(5);
                                                uwi = readData.GetString(6);
                                                apuw = String.Format("{0}/{1}", api, uwi);
                                                spud = readData.GetDateTime(7);
                                                date = spud.ToString("MMM d , yyyy --- hh:mm tt");
                                                iReply.Rows.Add(wID, wName, opName, field, county, apuw, date);
                                                iReply.AcceptChanges();
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    { throw new System.Exception(ex.ToString()); }
                                    finally
                                    {
                                        readData.Close();
                                        readData.Dispose();
                                    }
                                }
                            
                        
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getClientWellTableWithResultsByWellPad(Int32 WellPadID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn welID = iReply.Columns.Add("welID", typeof(Int32));
                DataColumn welName = iReply.Columns.Add("welName", typeof(String));
                DataColumn oprName = iReply.Columns.Add("oprName", typeof(String));
                DataColumn fldName = iReply.Columns.Add("fldName", typeof(String));
                DataColumn cnyName = iReply.Columns.Add("cnyName", typeof(String));
                DataColumn apiuwi = iReply.Columns.Add("apiuwi", typeof(String));
                DataColumn spudDate = iReply.Columns.Add("spudDate", typeof(String));

                Int32 wID = -99, opID = -99, fdID = -99, cnID = -99;
                String wName = String.Empty, opName = String.Empty, field = String.Empty, county = String.Empty, api = String.Empty, uwi = String.Empty, apuw = String.Empty, date = String.Empty;
                DateTime spud = DateTime.Now;
                String selData = "SELECT [welID], [optrID], [welName], [fldID], [cntyID], [welAPI], [welUWI], [welSpudDate] FROM [WELL] where [wpdID] = @wpdID AND [WelID] in (SELECT Distinct [welID] FROM [RawQCResults])";

                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection.ToString()))
                {
                    try
                    {
                        dataCon.Open();

                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        wID = readData.GetInt32(0);
                                        opID = readData.GetInt32(1);
                                        opName = CLNT.getClientName(opID);
                                        wName = readData.GetString(2);
                                        fdID = readData.GetInt32(3);
                                        field = DMG.getFieldName(fdID);
                                        cnID = readData.GetInt32(4);
                                        county = DMG.getCountyName(cnID);
                                        api = readData.GetString(5);
                                        uwi = readData.GetString(6);
                                        apuw = String.Format("{0}/{1}", api, uwi);
                                        spud = readData.GetDateTime(7);
                                        date = spud.ToString("MMM d , yyyy --- hh:mm tt");
                                        iReply.Rows.Add(wID, wName, opName, field, county, apuw, date);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }


                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal getClientWellPlannedMaxMDepth(Int32 WellID, String ClientDBConnection)
        {
            try
            {
                Decimal iReply = new Decimal();
                String selData = "SELECT MAX(md) FROM [WellPlanCalculatedData] WHERE [welID] = @welID ";
                selData += " and format( ctime , 'yyyyMMddHHmm' ) in ";
                selData += "(select  format( max(ctime) , 'yyyyMMddHHmm' ) FROM [WellPlanCalculatedData] where welID = @welID)";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        Int32 pdataFound = WPN.checkWellPlanForSelectedWell(WellID, ClientDBConnection);                        
                        if (pdataFound <= 0)
                        {
                            iReply = -99.00M;
                        }
                        else
                        {
                            SqlCommand getData = new SqlCommand(selData, dataCon);
                            getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            iReply = readData.GetDecimal(0);
                                        }
                                    }
                                    else
                                    {
                                        iReply = -99.00M;
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal getClientWellAuditedMaxMDepth(Int32 WellID, String ClientDBConnection)
        {
            try
            {
                Decimal iReply = new Decimal();
                String selData = "SELECT MAX(Depth) FROM [RawQCResults] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        Int32 pdataFound = WPN.checkWellPlanForSelectedWell(WellID, ClientDBConnection);
                        if (pdataFound <= 0)
                        {
                            iReply = -99.00M;
                        }
                        else
                        {
                            SqlCommand getData = new SqlCommand(selData, dataCon);
                            getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            //iReply = readData.GetDecimal(0);
                                            if (!readData.IsDBNull(0))
                                            {
                                                iReply = readData.GetDecimal(0);
                                            }
                                            else
                                            {
                                                iReply = 0.00M;
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getDataEntryMethodsList()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn demID = iReply.Columns.Add("demID", typeof(Int32));
                DataColumn demValue = iReply.Columns.Add("demValue", typeof(String));
                iReply.Rows.Add(1, "Manual Data Entry");
                iReply.Rows.Add(2, "Upload Data File");
                iReply.AcceptChanges();
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getCordSysList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();

                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [cordID], [cordName] FROM [dmgCoordinateSystem] ORDER BY [cordName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    id = readData.GetInt32(0);
                                    name = readData.GetString(1);
                                    iReply.Add(id, name);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getCordSysTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn cordID = iReply.Columns.Add("cordID", typeof(Int32));
                DataColumn cordName = iReply.Columns.Add("cordName", typeof(String));
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [cordID], [cordName] FROM [dmgCoordinateSystem] ORDER BY [cordName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    id = readData.GetInt32(0);
                                    name = readData.GetString(1);
                                    iReply.Rows.Add(id, name);
                                    iReply.AcceptChanges();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getCordName(Int32 cordID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [cordName] FROM [dmgCoordinateSystem] WHERE [cordID] = @cordID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@cordID", SqlDbType.Int).Value = cordID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getDatumsList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();

                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [datID], [datName] FROM [dmgDatum] ORDER BY [datName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    id = readData.GetInt32(0);
                                    name = readData.GetString(1);
                                    iReply.Add(id, name);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getDatumsTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn datID = iReply.Columns.Add("datID", typeof(Int32));
                DataColumn datName = iReply.Columns.Add("datName", typeof(String));
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [datID], [datName] FROM [dmgDatum] ORDER BY [datName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    id = readData.GetInt32(0);
                                    name = readData.GetString(1);
                                    iReply.Rows.Add(id, name);
                                    iReply.AcceptChanges();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getDatumName(Int32 DatumID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [datName] FROM [dmgDatum] WHERE [datID] = @datID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@datID", SqlDbType.Int).Value = DatumID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getIFRValue(Int32 ifrvID) //Returns Yes or No
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [ifrValue] FROM [dmgIFRValue] WHERE [ifrValID] = @ifrValID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ifrValID", SqlDbType.Int).Value = ifrvID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                                else
                                {
                                    iReply = "---";
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getIFRValues()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();

                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [ifrValID], [ifrValue] FROM dmgIFRValue ORDER BY [ifrValue]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    id = readData.GetInt32(0);
                                    name = readData.GetString(1);
                                    iReply.Add(id, name);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }  

        public static Dictionary<Int32, String> getLengthUnits()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();

                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [luID], [luName] FROM dmgLengthUnit WHERE (luID = 4) OR (luID = 5) ORDER BY [luName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    id = readData.GetInt32(0);
                                    name = readData.GetString(1);
                                    iReply.Add(id, name);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getBHALengthUnits()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [luID], [luName] FROM dmgLengthUnit WHERE [luID] IN (2, 3, 4, 5) ORDER BY [luName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    id = readData.GetInt32(0);
                                    name = readData.GetString(1);
                                    iReply.Add(id, name);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getAllLengthUnits()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();

                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [luID], [luName] FROM [dmgLengthUnit] ORDER BY [luName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    id = readData.GetInt32(0);
                                    name = readData.GetString(1);
                                    iReply.Add(id, name);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getLengthUnitName(Int32 LengthUnitID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [luName] FROM dmgLengthUnit WHERE [luID] = @luID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@luID", SqlDbType.Int).Value = LengthUnitID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getLocationIDForWell(Int32 WellPadID, String ClientDBString)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                Int32 regionID = -99, sregionID = -99, countryID = -99, stateID = -99, countyID = -99, fieldID = -99;
                String selData = "SELECT [regID], [sregID], [ctyID], [stID], [cntyID], [fldID] FROM [WellPad] WHERE [wpdID] = @wpdID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    regionID = readData.GetInt32(0);
                                    iReply.Add(regionID);
                                    sregionID = readData.GetInt32(1);
                                    iReply.Add(sregionID);
                                    countryID = readData.GetInt32(2);
                                    iReply.Add(countryID);
                                    stateID = readData.GetInt32(3);
                                    iReply.Add(stateID);
                                    countyID = readData.GetInt32(4);
                                    iReply.Add(countyID);
                                    fieldID = readData.GetInt32(5);
                                    iReply.Add(fieldID);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getMagnetometerUnits()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();

                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [muID], [muName] FROM [dmgMagUnit] ORDER BY [muName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    id = readData.GetInt32(0);
                                    name = readData.GetString(1);
                                    iReply.Add(id, name);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getMagModelList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();

                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [mmdlID], [mmdlName] FROM [dmgMagModel] ORDER BY [mmdlName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    id = readData.GetInt32(0);
                                    name = readData.GetString(1);
                                    iReply.Add(id, name);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getMagUnitName(Int32 MagUnitID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [muName] FROM dmgMagUnit WHERE [muID] = @muID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@muID", SqlDbType.Int).Value = MagUnitID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }



        public static String getMagModelName(Int32 MagModelID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [mmdlName] FROM dmgMagModel WHERE [mmdlID] = @mmdlID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@mmdlID", SqlDbType.Int).Value = MagModelID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getMotorCollarLen()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [luID], [luName] FROM dmgLengthUnit WHERE [luID] = 1 OR [luID] = 2 OR [luID] = 3 ORDER BY [luName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getOperatorID(Int32 wellID, String dbConn)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT [optrID] FROM [Well] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();                        
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = wellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }                        
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getOperatorName(Int32 oprID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [clntName] FROM [Client] WHERE [clntID] = @clntID";
                using (SqlConnection dataConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataConn.Open();
                        SqlCommand getData = new SqlCommand(selData, dataConn);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = oprID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataConn.Close();
                        dataConn.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static DataTable getOpWellGW(Int32 OperatorID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn WellID = iReply.Columns.Add("WellID", typeof(Int32));
                DataColumn WellName = iReply.Columns.Add("WellName", typeof(String));
                Int32 id = -99;
                String name = String.Empty;

                Dictionary<Int32, String> opwList = getOpWellList(OperatorID, ClientDBConnection);
                foreach (KeyValuePair<Int32, String> item in opwList)
                {
                    id = item.Key;
                    name = item.Value;
                    iReply.Rows.Add(id, name);
                    iReply.AcceptChanges();
                }

                return iReply;
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static DataTable getWellPlanWellList(Int32 OperatorID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn WellID = iReply.Columns.Add("WellID", typeof(Int32));
                DataColumn WellName = iReply.Columns.Add("WellName", typeof(String));
                Int32 id = -99, wrf = -99, wwp = -99;
                String name = String.Empty, wrfValue = String.Empty, wwpValue = String.Empty;

                Dictionary<Int32, String> opwList = getOpWellList(OperatorID, ClientDBConnection);
                foreach (KeyValuePair<Int32, String> item in opwList)
                {
                    id = item.Key;
                    name = item.Value;
                    wrf = findWellInRefMags(id, ClientDBConnection);
                    if (!wrf.Equals(1))
                    {
                        wrfValue = "Missing Reference Magnetics";
                    }
                    else
                    {
                        wrfValue = "Reference Magnetics found";
                    }
                    wwp = findWellInWellPlan(id, ClientDBConnection);
                    if (!wwp.Equals(1))
                    {
                        wwpValue = "Missing Well Plan";
                    }
                    else
                    {
                        wwpValue = "Well Plan found";
                    }
                    name = String.Format("{0} --- {1} ; {2}", name, wrfValue, wwpValue);
                    iReply.Rows.Add(id, name);
                    iReply.AcceptChanges();
                }

                return iReply;
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Dictionary<Int32, String> getOpWellPlanList(Int32 OperatorID, String OperatorDBString)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [welID], [welName] FROM [Well] WHERE [optrID] = @optrID AND [welID] NOT IN (SELECT [welID] FROM [WellPlanMetaInfo]) ORDER BY [welName]";
                Int32 id = -99;
                String name = String.Empty, wrfValue = String.Empty, wwpValue = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(OperatorDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                                else
                                {
                                    iReply.Add(-99, "No Wells found for selected Operator Company");
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getOpWellPlanListForWellPad(Int32 OperatorID, Int32 WellPadID, String OperatorDBString)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [welID], [welName] FROM [Well] WHERE [optrID] = @optrID AND [wpdID] = @wpdID AND [welID] NOT IN (SELECT [welID] FROM [WellPlanMetaInfo]) ORDER BY [welName]";
                Int32 id = -99;
                String name = String.Empty, wrfValue = String.Empty, wwpValue = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(OperatorDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                                else
                                {
                                    iReply.Add(-99, "No Wells found for selected Operator Company");
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getOptrWellCountPerClient(Int32 OperatorID, String ServiceClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(welID) FROM [Well] WHERE [optrID] = @optrID";
                using (SqlConnection dataCon = new SqlConnection(ServiceClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    iReply = -1;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getOpWellList(Int32 OperatorID, String OperatorDBString)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [welID], [welName] FROM [Well] WHERE [optrID] = @optrID ORDER BY [welName]";
                Int32 id = -99;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(OperatorDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                                else
                                {
                                    iReply.Add(-99, "No Wells found for selected Operator Company");
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getProjectionsList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();

                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [projID], [projName] FROM dmgProjection ORDER BY [projName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    id = readData.GetInt32(0);
                                    name = readData.GetString(1);
                                    iReply.Add(id, name);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getProviderWells(Int32 ProviderID, Int32 OperatorID)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 wD = -99;
                String dbConn = String.Empty;
                String wName = String.Empty;
                String selConn = "SELECT [cdbString] FROM [ClientDBMapping] WHERE [clntID] = @clntID";
                String selWells = "SELECT [welID], [welName] FROM [Well] WHERE [optrID] = @optrID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getConn = new SqlCommand(selConn, dataCon);
                        getConn.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ProviderID.ToString();
                        using (SqlDataReader readConn = getConn.ExecuteReader())
                        {
                            try
                            {
                                if (readConn.HasRows)
                                {
                                    while (readConn.Read())
                                    {
                                        dbConn = readConn.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readConn.Close();
                                readConn.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                using (SqlConnection welCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        welCon.Open();
                        SqlCommand getWell = new SqlCommand(selWells, welCon);
                        getWell.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readWells = getWell.ExecuteReader())
                        {
                            try
                            {
                                if (readWells.HasRows)
                                {
                                    while (readWells.Read())
                                    {
                                        wD = readWells.GetInt32(0);
                                        wName = readWells.GetString(1);
                                        iReply.Add(wD, wName);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readWells.Close();
                                readWells.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        welCon.Close();
                        welCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getProjectionName(Int32 ProjectionID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [projName] FROM dmgProjection WHERE [projID] = @projID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@projID", SqlDbType.Int).Value = ProjectionID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
                                         
        public static ArrayList getRunList(Int32 RunID, String dbConn)
        {
            try
            {
                ArrayList iReply = new ArrayList();
                String selData = "SELECT [runName], [runInc], [runAzm], [runTInc], [runTAzm], [sqcID], [startDepth], [endDepth], [lenU] from [Run] WHERE [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply.Add(Convert.ToString(readData.GetInt32(0)));
                                        iReply.Add(Convert.ToDecimal(readData.GetDecimal(1)));
                                        iReply.Add(Convert.ToDecimal(readData.GetDecimal(2)));
                                        iReply.Add(Convert.ToDecimal(readData.GetDecimal(3)));
                                        iReply.Add(Convert.ToDecimal(readData.GetDecimal(4)));
                                        iReply.Add(Convert.ToInt32(readData.GetInt32(5)));
                                        iReply.Add(Convert.ToDecimal(readData.GetDecimal(6)));
                                        iReply.Add(Convert.ToDecimal(readData.GetDecimal(7)));
                                        iReply.Add(Convert.ToInt32(readData.GetInt32(8)));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static ArrayList getWPRunList(Int32 RunID, String dbConn)
        {
            try
            {
                ArrayList iReply = new ArrayList();
                String selData = "SELECT [runInc], [runAzm], [runTInc], [runTAzm] from [WPRun] WHERE [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply.Add(Convert.ToDecimal(readData.GetDecimal(0)));
                                        iReply.Add(Convert.ToDecimal(readData.GetDecimal(1)));
                                        iReply.Add(Convert.ToDecimal(readData.GetDecimal(2)));
                                        iReply.Add(Convert.ToDecimal(readData.GetDecimal(3)));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }                   
                                               
        public static Dictionary<Int32, String> getRunNamesList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [rnamID], [rnamValue] FROM [dmgRunName] ORDER BY [rnamID]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    id = readData.GetInt32(0);
                                    name = readData.GetString(1);
                                    iReply.Add(id, name);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getRunNameIDFromRunID(Int32 RunID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT [runName] FROM [Run] WHERE [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getRunName(Int32 RunNameID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [rnamValue] FROM [dmgRunName] WHERE [rnamID] = @rnamID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@rnamID", SqlDbType.Int).Value = RunNameID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getRunNameValue(Int32 runnameID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [rnamValue] FROM [dmgRunName] WHERE [rnamID] = @rnamID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@rnamID", SqlDbType.Int).Value = runnameID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getRunStatusList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [dmgstID], [dmgStatus] FROM [dmgStatus] ORDER BY [dmgStatus]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    id = readData.GetInt32(0);
                                    name = readData.GetString(1);
                                    iReply.Add(id, name);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getRunTable(Int32 WellID, String ClientDBString)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn runID = iReply.Columns.Add("runID", typeof(Int32));
                DataColumn runName = iReply.Columns.Add("runName", typeof(String));
                DataColumn inc = iReply.Columns.Add("inc", typeof(Decimal));
                DataColumn azm = iReply.Columns.Add("azm", typeof(Decimal));
                DataColumn tinc = iReply.Columns.Add("tinc", typeof(Decimal));
                DataColumn tazm = iReply.Columns.Add("tazm", typeof(Decimal));
                DataColumn stat = iReply.Columns.Add("stat", typeof(String));
                DataColumn tcID = iReply.Columns.Add("tcID", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { runID };
                Int32 id = -99, nid = -99, stID = -99, tID = -99;
                String name = String.Empty, status = String.Empty, tcName = String.Empty;
                Decimal incVal = -99.00M, azmVal = -99.00M, tincVal = -99.00M, tazmVal = -99.00M;
                DateTime uT = DateTime.Now;
                String selData = "SELECT [runID], [runName], [runInc], [runAzm], [runTInc], [runTAzm], [dmgstID], [tcID], [uTime] FROM [Run] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        nid = readData.GetInt32(1);
                                        name = getRunNameValue(nid);
                                        incVal = readData.GetDecimal(2);
                                        azmVal = readData.GetDecimal(3);
                                        tincVal = readData.GetDecimal(4);
                                        tazmVal = readData.GetDecimal(5);
                                        stID = readData.GetInt32(6);
                                        status = DMG.getStatusValue(stID);
                                        tID = readData.GetInt32(7);
                                        tcName = DMG.getToolCodeName(tID);
                                        uT = readData.GetDateTime(8);
                                        iReply.Rows.Add(id, name, incVal, azmVal, tincVal, tazmVal, status, tcName, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getWellDirection(Int32 WellID, String ClientDBConnection)
        {
            try
            {
                List<Int32> iReply = new List<Int32> { };
                Int32 NS = 0, EW = 0;
                String selData = "SELECT [welLngCardinal], [welLatCardinal] FROM [Well] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        EW = readData.GetInt32(0);
                                        iReply.Add(EW);
                                        NS = readData.GetInt32(1);
                                        iReply.Add(NS);                                        
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        
        
        public static Dictionary<Int32, String> getWellRunList(Int32 ClientID, Int32 WellID, String ClientDBConnection)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                Int32 rnID = -99, noBha = -99;
                String rnName = String.Empty;
                String selRuns = "SELECT [runID], [runName] FROM [Run] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selRuns, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        noBha = getWellRunBHACount(id, ClientDBConnection);                                        
                                        rnID = readData.GetInt32(1);
                                        rnName = getRunName(rnID);
                                        if (noBha.Equals(0) || noBha.Equals(null))
                                        {
                                            rnName = rnName + "( Missing BHA Signature )";
                                        }
                                        iReply.Add(id, rnName);
                                    }
                                }
                                else
                                {
                                    id = -99;
                                    rnName = "No Run found";
                                    iReply.Add(id, rnName);
                                }

                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getWellRunBHACount(Int32 RunID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(bhaID) FROM [BHA] WHERE [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    iReply = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Get Client Silo ID for selected Well
        private static Int32 getSiloID(String domain)
        {
            try
            {
                Int32 clientID = 0;
                clientID = -99;

                String connString = WebConfigurationManager.ConnectionStrings["FEUsersDB"].ConnectionString;
                String selectCommand = "SELECT [clntID] FROM [ClientDBMapping] WHERE [clntRealm] = @clntRealm";

                SqlConnection clntConn = new SqlConnection(connString);
                SqlCommand getClient = new SqlCommand(selectCommand, clntConn);
                getClient.Parameters.AddWithValue("@clntRealm", SqlDbType.Int).Value = domain.ToString();

                clntConn.Open();

                SqlDataReader clntDataRow = getClient.ExecuteReader();
                if (clntDataRow.HasRows)
                {
                    while (clntDataRow.Read())
                    {
                        clientID = clntDataRow.GetInt32(0);
                    }
                }
                clntDataRow.Close();
                clntConn.Close();

                return clientID;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }   
                  
        public static Dictionary<Int32, String> getSpaceLen()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [luID], [luName] FROM [dmgLengthUnit] WHERE [luID] = 3 OR [luID] = 4 OR [luID] = 5 ORDER BY [luName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static ArrayList getSelectedSQCList(Int32 sqcID, String ClientDBConnection)
        {
            try
            {
                ArrayList iReply = new ArrayList();
                String selData = "SELECT [sqcDip], [sqcBTotal], [sqcGTotal] FROM [SQC] WHERE [sqcID] = @sqcID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@sqcID", SqlDbType.Int).Value = sqcID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply.Add(readData.GetDecimal(0));
                                        iReply.Add(readData.GetDecimal(1));
                                        iReply.Add(readData.GetDecimal(2));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getSQCList(String ClientDBConnection)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [sqcID], [sqcName] FROM [SQC]";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                                else
                                {
                                    id = 1;
                                    name = "No Survey Qualification Criteria found";
                                    iReply.Add(id, name);
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getSQCTable(String ClientDBConnection)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn sqcID = iReply.Columns.Add("sqcID", typeof(Int32));
                DataColumn sqcName = iReply.Columns.Add("sqcName", typeof(String));
                DataColumn sqcDip = iReply.Columns.Add("sqcDip", typeof(Decimal));
                DataColumn sqcBT = iReply.Columns.Add("sqcBT", typeof(Decimal));
                DataColumn sqcGT = iReply.Columns.Add("sqcGT", typeof(Decimal));
                iReply.PrimaryKey = new DataColumn[] { sqcID };
                Int32 id = -99;
                String name = String.Empty;
                Decimal dip = -99.0000M, bTotal = -99.0000M, gTotal = -99.0000000M;
                String selData = "SELECT [sqcID], [sqcName], [sqcDip], [sqcBTotal], [sqcGTotal] FROM SQC";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        dip = readData.GetDecimal(2);
                                        bTotal = readData.GetDecimal(3);
                                        gTotal = readData.GetDecimal(4);
                                        iReply.Rows.Add(id, name, dip, bTotal, gTotal);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getJSONSQCTable(String ClientDBConnection)
        {
            try
            {
                String iReply = String.Empty;
                DataTable infoTable = new DataTable();
                DataColumn sqcID = infoTable.Columns.Add("sqcID", typeof(Int32));
                DataColumn sqcName = infoTable.Columns.Add("sqcName", typeof(String));
                DataColumn sqcDip = infoTable.Columns.Add("sqcDip", typeof(Decimal));
                DataColumn sqcBT = infoTable.Columns.Add("sqcBT", typeof(Decimal));
                DataColumn sqcGT = infoTable.Columns.Add("sqcGT", typeof(Decimal));
                infoTable.PrimaryKey = new DataColumn[] { sqcID };
                Int32 id = -99;
                String name = String.Empty;
                Decimal dip = -99.0000M, bTotal = -99.0000M, gTotal = -99.0000000M;
                String selData = "SELECT [sqcID], [sqcName], [sqcDip], [sqcBTotal], [sqcGTotal] FROM SQC";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        dip = readData.GetDecimal(2);
                                        bTotal = readData.GetDecimal(3);
                                        gTotal = readData.GetDecimal(4);
                                        infoTable.Rows.Add(id, name, dip, bTotal, gTotal);
                                        infoTable.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply = getJSonString(infoTable);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getSmartWellCountForOperator(Int32 OperatorID)
        {
            try
            {
                Int32 iReply = 0;
                Dictionary<Int32, String> clientList = CLNT.getClientList(1);
                foreach(KeyValuePair<Int32, String> pair in clientList)
                {
                    Int32 id = pair.Key;
                    String name = pair.Value;
                    String dbName = CLNT.getClientDB(id);
                    if (String.IsNullOrEmpty(dbName))
                    {
                        iReply += 0;
                    }
                    else
                    {
                        String dbString = CLNT.getDBCon(dbName);
                        Int32 wCount = getOptrWellCountPerClient(OperatorID, dbString);
                        iReply += wCount;
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getWellPadCount(String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(wpdID) FROM [WellPad]";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getWellCount(String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(welID) FROM [Well]";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getOprWellPadList(Int32 OperatorID, Int32 ServiceID, String ClientDBConnection)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 wID = -99;
                String name = String.Empty;
                String selData = "SELECT DISTINCT(wpdID) FROM [WellServiceRegistration] WHERE [optrID] = @optrID AND [srvID] = @srvID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        using (SqlDataReader readWData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readWData.HasRows)
                                {
                                   while (readWData.Read())
                                    {
                                        wID = readWData.GetInt32(0);
                                        name = getWellPadName(wID, ClientDBConnection);
                                        iReply.Add(wID, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readWData.Close();
                                readWData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getOperatorWellList(Int32 ServiceID, Int32 OperatorID, Int32 WellPadID, String ClientDBConnection)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 wID = -99;
                String name = String.Empty;
                String selData = "SELECT [welID], [welName] FROM [Well] WHERE [welID] IN (SELECT [welID] FROM [WellPadToWell] WHERE [wpdID] = @wpdID)";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        wID = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(wID, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getWellPadList(Int32 OperatorID, Int32 FieldID, String ClientDBConnection)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [wpdID], [welPadName] FROM  [WellPad] WHERE [optrID] = @optrID AND [fldID] = @fldID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@fldID", SqlDbType.Int).Value = FieldID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getWellPadListWithAssetCount(Int32 OperatorID, Int32 FieldID, Int32 ClientID, String ClientDBConnection)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99, gwl = 0, swl = 0;
                String pdName = String.Empty, pdValue = String.Empty;
                String selData = "SELECT [wpdID], [welPadName] FROM [WellPad] WHERE [optrID] = @optrID AND [fldID] = @fldID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@fldID", SqlDbType.Int).Value = FieldID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        pdName = readData.GetString(1);
                                        gwl = getGlobalWellCountByWellPadAndField(id, FieldID);
                                        swl = getSiloWellCountByWellPadAndField(id, FieldID, ClientID, ClientDBConnection);
                                        pdValue = String.Format("{0} --- ( Global Well/Lateral: {1}; SMART Well/Lateral: {2} )", pdName, gwl, swl);
                                        iReply.Add(id, pdValue);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getWellPadListWithAssetCount(Int32 OperatorID, Int32 ClientID, String ClientDBConnection)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99, spd = 0, swl = 0;
                String pdName = String.Empty, pdValue = String.Empty;
                String selData = "SELECT [wpdID], [welPadName] FROM [WellPad] WHERE [optrID] = @optrID AND [clntID] = @clntID ORDER BY [welPadName] ASC";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        pdName = readData.GetString(1);
                                        spd = getSiloWellPadCountByOperator(OperatorID, id, ClientDBConnection);
                                        swl = getSiloWellCountByOperatorByWellID(OperatorID, id, ClientDBConnection);
                                        pdValue = String.Format("{0} --- ( Well Pad: {1}; Well/Lateral: {2} )", pdName, spd, swl);
                                        iReply.Add(id, pdValue);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getWellListWithStatusForOperator(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, String ClientDBConnection)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99, stID = -99;
                String wlName = String.Empty, sttName = String.Empty, wlValue = String.Empty;
                String selData = "SELECT [welID], [welName], [wstID] FROM [Well] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        wlName = readData.GetString(1);
                                        stID = readData.GetInt32(2);
                                        sttName = DMG.getWellStatusName(stID);
                                        wlValue = String.Format("{0} --- ( {1} )", wlName, sttName);
                                        iReply.Add(id, wlValue);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getSiloWellPadCountByOperator(Int32 OperatorID, Int32 WellPadID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(wpdID) FROM [WellPad] WHERE [optrID] = @optrID AND [wpdID] = @wpdID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getSiloWellCountByOperatorByWellID(Int32 OperatorID, Int32 WellPadID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(welID) FROM [Well] WHERE [optrID] = @optrID AND [wpdID] = @wpdID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellCountByWellPadAndField(Int32 WellPadID, Int32 FieldID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [dmgWell] WHERE [wpdID] = @wpdID AND [fldID] = @fldID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@fldID", SqlDbType.Int).Value = FieldID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static Int32 getSiloWellCountByWellPadAndField(Int32 WellPadID, Int32 FieldID, Int32 ClientID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [Well] WHERE [wpdID] = @wpdID AND [fldID] = @fldID AND [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@fldID", SqlDbType.Int).Value = FieldID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWellPadTable(Int32 OperatorID, Int32 FieldID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wpdID = iReply.Columns.Add("wpdID", typeof(Int32));
                DataColumn welPadName = iReply.Columns.Add("welPadName", typeof(String));
                DataColumn field = iReply.Columns.Add("field", typeof(String));
                DataColumn county = iReply.Columns.Add("county", typeof(String));
                DataColumn state = iReply.Columns.Add("state", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { wpdID };
                Int32 id = -99, fldID = -99, cntyID = -99, stID = -99;
                String name = String.Empty, countyN = String.Empty, fieldN = String.Empty, stateN = String.Empty;
                DateTime uT = DateTime.Now;
                String selData = "SELECT [wpdID], [welPadName], [cntyID], [fldID], [stID], [uTime] FROM  [WellPad] WHERE [optrID] = @optrID AND [fldID] = @fldID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@fldID", SqlDbType.Int).Value = FieldID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        cntyID = readData.GetInt32(2);
                                        fldID = readData.GetInt32(3);
                                        stID = readData.GetInt32(4);
                                        uT = readData.GetDateTime(5);
                                        countyN = DMG.getCountyName(cntyID);
                                        fieldN = DMG.getFieldName(fldID);
                                        stateN = DMG.getStateName(stID);
                                        iReply.Rows.Add(id, name, fieldN, countyN, stateN, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWellPadTableForOperator(Int32 OperatorID, Int32 FieldID, Int32 ClientID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wpdID = iReply.Columns.Add("wpdID", typeof(Int32));
                DataColumn welPadName = iReply.Columns.Add("welPadName", typeof(String));
                DataColumn field = iReply.Columns.Add("field", typeof(String));
                DataColumn county = iReply.Columns.Add("county", typeof(String));
                DataColumn state = iReply.Columns.Add("state", typeof(String));
                DataColumn welCount = iReply.Columns.Add("welCount", typeof(Int32));
                DataColumn swlCount = iReply.Columns.Add("swlCount", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { wpdID };
                Int32 id = -99, fldID = -99, cntyID = -99, stID = -99, gwl = 0, swl = 0;
                String name = String.Empty, countyN = String.Empty, fieldN = String.Empty, stateN = String.Empty;
                DateTime uT = DateTime.Now;
                String selData = "SELECT [wpdID], [welPadName], [cntyID], [fldID], [stID], [uTime] FROM  [WellPad] WHERE [optrID] = @optrID AND [fldID] = @fldID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@fldID", SqlDbType.Int).Value = FieldID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        cntyID = readData.GetInt32(2);
                                        fldID = readData.GetInt32(3);
                                        stID = readData.GetInt32(4);
                                        uT = readData.GetDateTime(5);
                                        countyN = DMG.getCountyName(cntyID);
                                        fieldN = DMG.getFieldName(fldID);
                                        stateN = DMG.getStateName(stID);
                                        gwl = getGlobalWellCountForOperatorByWellPad(id, OperatorID);
                                        swl = getSiloWellCountByWellPad(id, OperatorID, ClientID, ClientDBConnection);
                                        iReply.Rows.Add(id, name, fieldN, countyN, stateN, gwl, swl, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getClientWellPadTableForOperator(Int32 OperatorID, Int32 ClientID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wpdID = iReply.Columns.Add("wpdID", typeof(Int32));
                DataColumn welPadName = iReply.Columns.Add("welPadName", typeof(String));
                DataColumn field = iReply.Columns.Add("field", typeof(String));
                DataColumn county = iReply.Columns.Add("county", typeof(String));
                DataColumn state = iReply.Columns.Add("state", typeof(String));
                DataColumn country = iReply.Columns.Add("country", typeof(String));
                DataColumn sreg = iReply.Columns.Add("sreg", typeof(String));
                DataColumn reg = iReply.Columns.Add("reg", typeof(String));
                DataColumn welCount = iReply.Columns.Add("welCount", typeof(Int32));
                DataColumn swlCount = iReply.Columns.Add("swlCount", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { wpdID };
                Int32 id = -99, fldID = -99, cntyID = -99, stID = -99, ctID = -99, gwl = 0, swl = 0, rgID = -99, srgID = -99;
                String name = String.Empty, countyN = String.Empty, fieldN = String.Empty, stateN = String.Empty, countryN = String.Empty, sregionN = String.Empty, regionN = String.Empty, dbName = String.Empty, ClientDBConnection = String.Empty;
                DateTime uT = DateTime.Now;
                String selData = "SELECT [wpdID], [welPadName], [cntyID], [fldID], [stID], [ctyID], [sregID], [regID], [uTime] FROM  [WellPad] WHERE [optrID] = @optrID ORDER BY [welPadName]";
                dbName = CLNT.getClientDB(ClientID);
                ClientDBConnection = CLNT.getDBCon(dbName);
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        cntyID = readData.GetInt32(2);
                                        fldID = readData.GetInt32(3);
                                        stID = readData.GetInt32(4);
                                        ctID = readData.GetInt32(5);
                                        srgID = readData.GetInt32(6);
                                        rgID = readData.GetInt32(7);
                                        uT = readData.GetDateTime(8);
                                        countyN = DMG.getCountyName(cntyID);
                                        fieldN = DMG.getFieldName(fldID);
                                        stateN = DMG.getStateName(stID);
                                        countryN = DMG.getCountryName(ctID);
                                        sregionN = DMG.getSubRegionName(srgID);
                                        regionN = DMG.getRegionName(rgID);
                                        gwl = getGlobalWellCountForOperatorByWellPad(id, OperatorID);
                                        swl = getSiloWellCountByWellPad(id, OperatorID, ClientID, ClientDBConnection);
                                        iReply.Rows.Add(id, name, fieldN, countyN, stateN, countryN, sregionN, regionN, gwl, swl, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getJSONOperatorWellPadTable(Int32 OperatorID, Int32 ClientID, String ClientDBConnection)
        {
            try
            {
                String iReply = String.Empty;
                System.Data.DataTable infoTable = new System.Data.DataTable();
                DataColumn wpdID = infoTable.Columns.Add("wpdID", typeof(Int32));
                DataColumn welPadName = infoTable.Columns.Add("welPadName", typeof(String));
                DataColumn field = infoTable.Columns.Add("field", typeof(String));
                DataColumn county = infoTable.Columns.Add("county", typeof(String));
                DataColumn state = infoTable.Columns.Add("state", typeof(String));
                DataColumn swlCount = infoTable.Columns.Add("swlCount", typeof(Int32));
                DataColumn uTime = infoTable.Columns.Add("uTime", typeof(DateTime));
                infoTable.PrimaryKey = new DataColumn[] { wpdID };
                Int32 id = -99, fldID = -99, cntyID = -99, stID = -99, swl = 0;
                String name = String.Empty, countyN = String.Empty, fieldN = String.Empty, stateN = String.Empty;
                DateTime uT = DateTime.Now;
                String selData = "SELECT [wpdID], [welPadName], [cntyID], [fldID], [stID], [uTime] FROM  [WellPad] WHERE [optrID] = @optrID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        cntyID = readData.GetInt32(2);
                                        fldID = readData.GetInt32(3);
                                        stID = readData.GetInt32(4);
                                        uT = readData.GetDateTime(5);
                                        countyN = DMG.getCountyName(cntyID);
                                        fieldN = DMG.getFieldName(fldID);
                                        stateN = DMG.getStateName(stID);
                                        swl = getSiloWellCountByWellPad(id, OperatorID, ClientID, ClientDBConnection);
                                        infoTable.Rows.Add(id, name, fieldN, countyN, stateN, swl, uT);
                                        infoTable.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply = getJSonString(infoTable);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWellPadTableForOperatorForReviewer(Int32 OperatorID, Int32 FieldID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wpdID = iReply.Columns.Add("clientWellPadID", typeof(Int32));
                DataColumn welPadName = iReply.Columns.Add("welPadName", typeof(String));
                DataColumn field = iReply.Columns.Add("field", typeof(String));
                DataColumn county = iReply.Columns.Add("county", typeof(String));
                DataColumn state = iReply.Columns.Add("state", typeof(String));
                DataColumn welCount = iReply.Columns.Add("welCount", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { wpdID };
                Int32 id = -99, fldID = -99, cntyID = -99, stID = -99, gwl = 0;
                String name = String.Empty, countyN = String.Empty, fieldN = String.Empty, stateN = String.Empty;
                DateTime uT = DateTime.Now;
                String selData = "SELECT [wpdID], [welPadName], [cntyID], [fldID], [stID], [uTime] FROM  [dmgWellPad] WHERE [optrID] = @optrID AND [fldID] = @fldID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@fldID", SqlDbType.Int).Value = FieldID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        cntyID = readData.GetInt32(2);
                                        fldID = readData.GetInt32(3);
                                        stID = readData.GetInt32(4);
                                        uT = readData.GetDateTime(5);
                                        countyN = DMG.getCountyName(cntyID);
                                        fieldN = DMG.getFieldName(fldID);
                                        stateN = DMG.getStateName(stID);
                                        gwl = getGlobalWellCountForOperatorByWellPad(id, OperatorID);
                                        iReply.Rows.Add(id, name, fieldN, countyN, stateN, gwl, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getProviderWellPadTableForOperatorForReviewer(Int32 ProviderID, Int32 FieldID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wpdID = iReply.Columns.Add("clientWellPadID", typeof(Int32));
                DataColumn welPadName = iReply.Columns.Add("welPadName", typeof(String));
                DataColumn field = iReply.Columns.Add("field", typeof(String));
                DataColumn county = iReply.Columns.Add("county", typeof(String));
                DataColumn state = iReply.Columns.Add("state", typeof(String));
                DataColumn welCount = iReply.Columns.Add("welCount", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { wpdID };
                Int32 id = -99, fldID = -99, cntyID = -99, stID = -99, swl = 0;
                String name = String.Empty, countyN = String.Empty, fieldN = String.Empty, stateN = String.Empty;
                DateTime uT = DateTime.Now;
                String ClientDBName = CLNT.getClientDB(ProviderID);
                String ClientDBString = CLNT.getDBCon(ClientDBName);
                String selData = "SELECT [wpdID], [welPadName], [cntyID], [fldID], [stID], [uTime] FROM  [WellPad] WHERE [clntID] = @clntID AND [fldID] = @fldID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ProviderID.ToString();
                        getData.Parameters.AddWithValue("@fldID", SqlDbType.Int).Value = FieldID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        cntyID = readData.GetInt32(2);
                                        fldID = readData.GetInt32(3);
                                        stID = readData.GetInt32(4);
                                        uT = readData.GetDateTime(5);
                                        countyN = DMG.getCountyName(cntyID);
                                        fieldN = DMG.getFieldName(fldID);
                                        stateN = DMG.getStateName(stID);
                                        swl = getProviderWellCountForOperatorByWellPad(id, ProviderID, ClientDBString);
                                        iReply.Rows.Add(id, name, fieldN, countyN, stateN, swl, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getComparisonWellPadTableForOperator(Int32 OperatorID, Int32 ClientID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wpdID = iReply.Columns.Add("wpdID", typeof(Int32));
                DataColumn welPadName = iReply.Columns.Add("welPadName", typeof(String));
                DataColumn field = iReply.Columns.Add("field", typeof(String));
                DataColumn county = iReply.Columns.Add("county", typeof(String));
                DataColumn state = iReply.Columns.Add("state", typeof(String));
                DataColumn welCount = iReply.Columns.Add("welCount", typeof(Int32));
                DataColumn swlCount = iReply.Columns.Add("swlCount", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { wpdID };
                Int32 id = -99, fldID = -99, cntyID = -99, stID = -99, gwl = 0, swl = 0;
                String name = String.Empty, countyN = String.Empty, fieldN = String.Empty, stateN = String.Empty;
                DateTime uT = DateTime.Now;
                String selData = "SELECT [wpdID], [welPadName], [cntyID], [fldID], [stID], [uTime] FROM  [WellPad] WHERE [optrID] = @optrID AND [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        cntyID = readData.GetInt32(2);
                                        fldID = readData.GetInt32(3);
                                        stID = readData.GetInt32(4);
                                        uT = readData.GetDateTime(5);
                                        countyN = DMG.getCountyName(cntyID);
                                        fieldN = DMG.getFieldName(fldID);
                                        stateN = DMG.getStateName(stID);
                                        gwl = getGlobalWellCountForOperatorByWellPad(id, OperatorID);
                                        swl = getSiloWellCountByWellPad(id, OperatorID, ClientID, ClientDBConnection);
                                        iReply.Rows.Add(id, name, fieldN, countyN, stateN, gwl, swl, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellCountForOperatorByWellPad(Int32 WellPadID, Int32 OperatorID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [dmgWell] WHERE [wpdID] = @wpdID AND [optrID] = @optrID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getProviderWellCountForOperatorByWellPad(Int32 WellPadID, Int32 ProviderID, String ClientDBString)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [dmgWell] WHERE [wpdID] = @wpdID AND [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ProviderID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getSiloWellCountByWellPad(Int32 WellPadID, Int32 OperatorID, Int32 ClientID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [Well] WHERE [wpdID] = @wpdID AND [optrID] = @optrID AND [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getMSAWellForWellPadTable(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 ServiceID, String ClientDBConnection)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn welID = iReply.Columns.Add("welID", typeof(Int32));
                DataColumn welName = iReply.Columns.Add("welName", typeof(String));
                DataColumn field = iReply.Columns.Add("field", typeof(String));
                DataColumn county = iReply.Columns.Add("county", typeof(String));
                DataColumn state = iReply.Columns.Add("state", typeof(String));
                DataColumn spud = iReply.Columns.Add("spud", typeof(DateTime));
                DataColumn wstID = iReply.Columns.Add("wstID", typeof(String));
                DataColumn stClr = iReply.Columns.Add("stClr", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { welID };
                Int32 id = -99, fldID = -99, cntyID = -99, stID = -99, wsID = -99;
                String name = String.Empty, countyN = String.Empty, fieldN = String.Empty, stateN = String.Empty, wsStat = String.Empty, sClr = String.Empty;
                DateTime spudD = DateTime.Now, uT = DateTime.Now;
                String selData = "SELECT [welID], [welName], [cntyID], [fldID], [stID], [welSpudDate], [wstID], [uTime] FROM [Well] WHERE [wpdID] = @wpdID AND [welID] IN (SELECT [welID] FROM [WellServiceRegistration] WHERE [srvID] = @srvID)";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        cntyID = readData.GetInt32(2);
                                        fldID = readData.GetInt32(3);
                                        stID = readData.GetInt32(4);
                                        spudD = readData.GetDateTime(5);
                                        wsID = readData.GetInt32(6);
                                        if (wsID.Equals(2)) { sClr = "Green"; } else { sClr = "Yellow"; }
                                        wsStat = DMG.getWellProcessStatus(wsID);
                                        uT = readData.GetDateTime(7);
                                        countyN = DMG.getCountyName(cntyID);
                                        fieldN = DMG.getFieldName(fldID);
                                        stateN = DMG.getStateName(stID);
                                        iReply.Rows.Add(id, name, countyN, fieldN, stateN, spudD, wsStat, sClr, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getWellForWellPadTable(Int32 WellPadID, String ClientDBConnection)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn welID = iReply.Columns.Add("welID", typeof(Int32));
                DataColumn welName = iReply.Columns.Add("welName", typeof(String));
                DataColumn field = iReply.Columns.Add("field", typeof(String));
                DataColumn county = iReply.Columns.Add("county", typeof(String));
                DataColumn state = iReply.Columns.Add("state", typeof(String));
                DataColumn spud = iReply.Columns.Add("spud", typeof(DateTime));
                DataColumn wstID = iReply.Columns.Add("wstID", typeof(String));
                DataColumn stClr = iReply.Columns.Add("stClr", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { welID };
                Int32 id = -99, fldID = -99, cntyID = -99, stID = -99, wsID = -99;
                String name = String.Empty, countyN = String.Empty, fieldN = String.Empty, stateN = String.Empty, wsStat = String.Empty, sClr = String.Empty;
                DateTime spudD = DateTime.Now, uT = DateTime.Now;
                String selData = "SELECT [welID], [welName], [cntyID], [fldID], [stID], [welSpudDate], [wstID], [uTime] FROM [Well] WHERE [wpdID] = @wpdID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        cntyID = readData.GetInt32(2);
                                        fldID = readData.GetInt32(3);
                                        stID = readData.GetInt32(4);
                                        spudD = readData.GetDateTime(5);
                                        wsID = readData.GetInt32(6);
                                        if (wsID.Equals(2)) { sClr = "Green"; } else { sClr = "Yellow"; }
                                        wsStat = DMG.getWellProcessStatus(wsID);
                                        uT = readData.GetDateTime(7);
                                        countyN = DMG.getCountyName(cntyID);
                                        fieldN = DMG.getFieldName(fldID);
                                        stateN = DMG.getStateName(stID);
                                        iReply.Rows.Add(id, name, countyN, fieldN, stateN, spudD, wsStat, sClr, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getWellDataFromPlacement(String ClientDBConnection)
        {

            try
            {
                DataTable iReply = new DataTable();
                DataColumn welID = iReply.Columns.Add("welID", typeof(Int32));
                DataColumn welName = iReply.Columns.Add("welName", typeof(String));
                DataColumn field = iReply.Columns.Add("field", typeof(String));
                DataColumn county = iReply.Columns.Add("county", typeof(String));
                DataColumn state = iReply.Columns.Add("state", typeof(String));
                DataColumn spud = iReply.Columns.Add("spud", typeof(DateTime));
                DataColumn optrN = iReply.Columns.Add("optrN", typeof(String));
                DataColumn wstID = iReply.Columns.Add("wstID", typeof(String));
                DataColumn stClr = iReply.Columns.Add("stClr", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { welID };
                Int32 id = -99, fldID = -99, cntyID = -99, stID = -99, wsID = -99 , optrid = -99;
                String name = String.Empty, countyN = String.Empty, operatorN = String.Empty, fieldN = String.Empty, stateN = String.Empty, wsStat = String.Empty, sClr = String.Empty;
                DateTime spudD = DateTime.Now, uT = DateTime.Now;
                String selData = "SELECT [welID], [welName], [cntyID], [fldID], [stID], [welSpudDate], [wstID], [uTime] , [optrID]  FROM [Well] WHERE welID in (select distinct(welID) from [WellPlacementData]);";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        //getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        cntyID = readData.GetInt32(2);
                                        fldID = readData.GetInt32(3);
                                        stID = readData.GetInt32(4);
                                        spudD = readData.GetDateTime(5);
                                        wsID = readData.GetInt32(6);
                                        //if (wsID.Equals(2)) { sClr = "Green"; } else { sClr = "Yellow"; }
                                        wsStat = DMG.getWellProcessStatus(wsID);
                                        uT = readData.GetDateTime(7);
                                        optrid = readData.GetInt32(8);

                                        operatorN = CLNT.getClientName(optrid);
                                        countyN = DMG.getCountyName(cntyID);
                                        fieldN = DMG.getFieldName(fldID);
                                        stateN = DMG.getStateName(stID);
                                        iReply.Rows.Add(id, name, fieldN, countyN, stateN, spudD, operatorN, wsStat, sClr, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getWellDataFromPlacementSameField(Int32 WellID , String ClientDBConnection)
        {

            try
            {
                DataTable iReply = new DataTable();
                DataColumn welID = iReply.Columns.Add("welID", typeof(Int32));
                DataColumn welName = iReply.Columns.Add("welName", typeof(String));
                DataColumn field = iReply.Columns.Add("field", typeof(String));
                DataColumn county = iReply.Columns.Add("county", typeof(String));
                DataColumn state = iReply.Columns.Add("state", typeof(String));
                DataColumn spud = iReply.Columns.Add("spud", typeof(DateTime));
                DataColumn optrN = iReply.Columns.Add("optrN", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { welID };
                Int32 id = -99, fldID = -99, cntyID = -99, stID = -99, wsID = -99, optrid = -99;
                String name = String.Empty, countyN = String.Empty, operatorN = String.Empty, fieldN = String.Empty, stateN = String.Empty, wsStat = String.Empty, sClr = String.Empty;
                DateTime spudD  = DateTime.Now, uT = DateTime.Now;
                String selData  = "SELECT [welID], [welName], [cntyID], [fldID], [stID], [welSpudDate], [wstID], [uTime] , [optrID]  ";
                selData         += " FROM [Well] WHERE welID in (select distinct(welID) from [WellPlacementData]) ";
                selData += " AND fldID = ( select fldID from well where welID = @wellID ) AND WelID not like @wellID; ";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wellID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        cntyID = readData.GetInt32(2);
                                        fldID = readData.GetInt32(3);
                                        stID = readData.GetInt32(4);
                                        spudD = readData.GetDateTime(5);
                                        wsID = readData.GetInt32(6);
                                        //if (wsID.Equals(2)) { sClr = "Green"; } else { sClr = "Yellow"; }
                                        uT = readData.GetDateTime(7);
                                        optrid = readData.GetInt32(8);

                                        operatorN = CLNT.getClientName(optrid);
                                        countyN = DMG.getCountyName(cntyID);
                                        fieldN = DMG.getFieldName(fldID);
                                        stateN = DMG.getStateName(stID);
                                        iReply.Rows.Add(id, name, fieldN,  countyN, stateN, spudD, operatorN, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getWellsForWellPadWithRegistrationTable(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, String ClientDBConnection)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn welID = iReply.Columns.Add("welID", typeof(Int32));
                DataColumn welName = iReply.Columns.Add("welName", typeof(String));                
                DataColumn wstID = iReply.Columns.Add("wstID", typeof(String));
                DataColumn stClr = iReply.Columns.Add("stClr", typeof(String));
                DataColumn srv = iReply.Columns.Add("srvID", typeof(String));
                DataColumn srvName = iReply.Columns.Add("srvName", typeof(String));
                iReply.PrimaryKey = new DataColumn[] { welID };
                Int32 id = -99, wsID = -99, srvID = -99;
                String name = String.Empty, sClr = String.Empty, wsStat = String.Empty, registration = String.Empty;
                String selData = "SELECT [welID], [welName], [wstID] FROM [Well] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        wsID = readData.GetInt32(2);
                                        srvID = getWellRegServiceID(OperatorID, ClientID, WellPadID, id, ClientDBConnection);
                                        if (srvID.Equals(-99))
                                        {   registration = "Not Registered"; }                                        
                                        else
                                        { registration = SRV.getServiceName(srvID); }
                                        if (wsID.Equals(2)) { sClr = "Green"; } else { sClr = "Yellow"; }
                                        wsStat = DMG.getWellProcessStatus(wsID);
                                        iReply.Rows.Add(id, name, wsStat, sClr, srvID, registration);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getJSONWellForWellPadTable(Int32 OperatorID, Int32 WellPadID, Int32 ClientID)
        {
            try
            {
                String iReply = String.Empty;
                DataTable infoTable = new DataTable();
                DataColumn welID = infoTable.Columns.Add("welID", typeof(Int32));
                DataColumn welName = infoTable.Columns.Add("welName", typeof(String));
                DataColumn field = infoTable.Columns.Add("field", typeof(String));
                DataColumn county = infoTable.Columns.Add("county", typeof(String));
                DataColumn state = infoTable.Columns.Add("state", typeof(String));
                DataColumn spud = infoTable.Columns.Add("spud", typeof(DateTime));
                DataColumn wstID = infoTable.Columns.Add("wstID", typeof(String));
                DataColumn uTime = infoTable.Columns.Add("uTime", typeof(DateTime));
                infoTable.PrimaryKey = new DataColumn[] { welID };
                Int32 id = -99, fldID = -99, cntyID = -99, stID = -99, wsID = -99;
                String name = String.Empty, countyN = String.Empty, fieldN = String.Empty, stateN = String.Empty,  wsStat = String.Empty;
                DateTime spudD = DateTime.Now, uT = DateTime.Now;
                String ClientDBName = CLNT.getClientDB(ClientID);
                String ClientDBString = CLNT.getDBCon(ClientDBName);
                String selData = "SELECT [welID], [welName], [cntyID], [fldID], [stID], [welSpudDate], [wstID], [uTime] FROM  [Well] WHERE [wpdID] = @wpdID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        cntyID = readData.GetInt32(2);
                                        fldID = readData.GetInt32(3);
                                        stID = readData.GetInt32(4);
                                        spudD = readData.GetDateTime(5);
                                        wsID = readData.GetInt32(6);
                                        wsStat = DMG.getWellProcessStatus(wsID);
                                        uT = readData.GetDateTime(7);
                                        countyN = DMG.getCountyName(cntyID);
                                        fieldN = DMG.getFieldName(fldID);
                                        stateN = DMG.getStateName(stID);
                                        infoTable.Rows.Add(id, name, countyN, fieldN, stateN, spudD, wsStat, uT);
                                        infoTable.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply = getJSonString(infoTable);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getProviderWellForWellPadTable(Int32 WellPadID, Int32 ProviderID)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn welID = iReply.Columns.Add("welID", typeof(Int32));
                DataColumn welName = iReply.Columns.Add("welName", typeof(String));
                DataColumn field = iReply.Columns.Add("field", typeof(String));
                DataColumn county = iReply.Columns.Add("county", typeof(String));
                DataColumn state = iReply.Columns.Add("state", typeof(String));
                DataColumn spud = iReply.Columns.Add("spud", typeof(DateTime));
                DataColumn wstID = iReply.Columns.Add("wstID", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { welID };
                Int32 id = -99, fldID = -99, cntyID = -99, stID = -99, wsID = -99;
                String name = String.Empty, countyN = String.Empty, fieldN = String.Empty, stateN = String.Empty, wsStat = String.Empty;
                DateTime spudD = DateTime.Now, uT = DateTime.Now;
                String ClientDBName = CLNT.getClientDB(ProviderID);
                String ClientDBString = CLNT.getDBCon(ClientDBName);
                String selData = "SELECT [welID], [welName], [cntyID], [fldID], [stID], [welSpudDate], [wstID], [uTime] FROM  [Well] WHERE [wpdID] = @wpdID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        cntyID = readData.GetInt32(2);
                                        fldID = readData.GetInt32(3);
                                        stID = readData.GetInt32(4);
                                        spudD = readData.GetDateTime(5);
                                        wsID = readData.GetInt32(6);
                                        wsStat = DMG.getWellProcessStatus(wsID);
                                        uT = readData.GetDateTime(7);
                                        countyN = DMG.getCountyName(cntyID);
                                        fieldN = DMG.getFieldName(fldID);
                                        stateN = DMG.getStateName(stID);
                                        iReply.Rows.Add(id, name, countyN, fieldN, stateN, spudD, wsStat, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getProviderWellDetailTableForReviewer(Int32 WellID, Int32 ProviderID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn welID = iReply.Columns.Add("welID", typeof(Int32));
                DataColumn welPermit = iReply.Columns.Add("welPermit", typeof(String));
                DataColumn welAPI = iReply.Columns.Add("welAPI", typeof(String));
                DataColumn welUWI = iReply.Columns.Add("welUWI", typeof(String));
                DataColumn welLongitude = iReply.Columns.Add("welLongitude", typeof(String));
                DataColumn welLatitude = iReply.Columns.Add("welLatitude", typeof(String));
                String selData = "SELECT [welID], [welPermit], [welAPI], [welUWI], [welLongitude], [welLngCardinal], [welLatitude], [welLatCardinal] FROM [Well] WHERE [welID] = @welID";
                Int32 id = -99, latC = -99, lngC = -99;
                Decimal lngi = -99.000000000M, lati = -99.000000000M;
                String perm = String.Empty, api = String.Empty, uwi = String.Empty, ltC = String.Empty, lnC = String.Empty, latitude = String.Empty, longitude = String.Empty;
                String ClientDBName = CLNT.getClientDB(ProviderID);
                String ClientDBString = CLNT.getDBCon(ClientDBName);
                using (SqlConnection dataCon = new SqlConnection(ClientDBString.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        perm = readData.GetString(1);
                                        api = readData.GetString(2);
                                        if (String.IsNullOrEmpty(api))
                                        {
                                            api = "---";
                                        }
                                        uwi = readData.GetString(3);
                                        if (String.IsNullOrEmpty(uwi))
                                        {
                                            uwi = "---";
                                        }
                                        lngi = readData.GetDecimal(4);
                                        lngC = readData.GetInt32(5);
                                        lnC = DMG.getCardinalName(lngC);
                                        longitude = String.Format("{0} ({1})", lngi, lnC);
                                        lati = readData.GetDecimal(6);
                                        latC = readData.GetInt32(7);
                                        ltC = DMG.getCardinalName(latC);
                                        latitude = String.Format("{0} ({1})", lati, ltC);
                                        iReply.Rows.Add(id, perm, api, uwi, longitude, latitude);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getWellTable(Int32 OperatorID, String ClientDBConnection)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn welID = iReply.Columns.Add("welID", typeof(Int32));
                DataColumn welName = iReply.Columns.Add("welName", typeof(String));
                DataColumn field = iReply.Columns.Add("field", typeof(String));
                DataColumn county = iReply.Columns.Add("county", typeof(String));
                DataColumn state = iReply.Columns.Add("state", typeof(String));
                DataColumn spud = iReply.Columns.Add("spud", typeof(DateTime));
                DataColumn wstID = iReply.Columns.Add("wstID", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { welID };
                Int32 id = -99, fldID = -99, cntyID = -99, stID = -99, wsID = -99;
                String name = String.Empty, countyN = String.Empty, fieldN = String.Empty, stateN = String.Empty, wsStat = String.Empty;
                DateTime spudD = DateTime.Now, uT = DateTime.Now;
                String selData = "SELECT [welID], [welName], [cntyID], [fldID], [stID], [welSpudDate], [wstID], [uTime] FROM  [Well] WHERE [optrID] = @optrID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        cntyID = readData.GetInt32(2);
                                        fldID = readData.GetInt32(3);
                                        stID = readData.GetInt32(4);
                                        spudD = readData.GetDateTime(5);
                                        wsID = readData.GetInt32(6);
                                        wsStat = DMG.getWellProcessStatus(wsID);
                                        uT = readData.GetDateTime(7);
                                        countyN = DMG.getCountyName(cntyID);
                                        fieldN = DMG.getFieldName(fldID);
                                        stateN = DMG.getStateName(stID);
                                        iReply.Rows.Add(id, name, countyN, fieldN, stateN, spudD, wsStat, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWellDetailTable(Int32 WellID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn welID = iReply.Columns.Add("welID", typeof(Int32));
                DataColumn welPermit = iReply.Columns.Add("welPermit", typeof(String));
                DataColumn welAPI = iReply.Columns.Add("welAPI", typeof(String));
                DataColumn welUWI = iReply.Columns.Add("welUWI", typeof(String));
                DataColumn welLongitude = iReply.Columns.Add("welLongitude", typeof(String));
                DataColumn welLatitude = iReply.Columns.Add("welLatitude", typeof(String));
                String selData = "SELECT [welID], [welPermit], [welAPI], [welUWI], [welLongitude], [welLngCardinal], [welLatitude], [welLatCardinal] FROM [Well] WHERE [welID] = @welID";
                Int32 id = -99, latC = -99, lngC = -99;
                Decimal lngi = -99.000000000M, lati = -99.000000000M;
                String perm = String.Empty, api = String.Empty, uwi = String.Empty, ltC = String.Empty, lnC = String.Empty, latitude = String.Empty, longitude = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        perm = readData.GetString(1);
                                        api = readData.GetString(2);
                                        if (String.IsNullOrEmpty(api))
                                        {
                                            api = "---";
                                        }
                                        uwi = readData.GetString(3);
                                        if (String.IsNullOrEmpty(uwi))
                                        {
                                            uwi = "---";
                                        }
                                        lngi = readData.GetDecimal(4);
                                        lngC = readData.GetInt32(5);
                                        lnC = DMG.getCardinalName(lngC);
                                        longitude = String.Format("{0} ({1})", lngi, lnC);
                                        lati = readData.GetDecimal(6);
                                        latC = readData.GetInt32(7);
                                        ltC = DMG.getCardinalName(latC);
                                        latitude = String.Format("{0} ({1})", lati, ltC);
                                        iReply.Rows.Add(id, perm, api, uwi, longitude, latitude);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<String> getWellDetailList(Int32 OperatorID, Int32 ClientID, Int32 WellID, String ClientDBString)
        {
            try
            {
                List<String> iReply = new List<String>();
                String selData = "SELECT [welID], [welPermit], [welAPI], [welUWI], [welLongitude], [welLngCardinal], [welLatitude], [welLatCardinal] FROM [Well] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [welID] = @welID";
                Int32 id = -99, latC = -99, lngC = -99;
                Decimal lngi = -99.000000000M, lati = -99.000000000M;
                String perm = String.Empty, api = String.Empty, uwi = String.Empty, ltC = String.Empty, lnC = String.Empty, latitude = String.Empty, longitude = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        perm = readData.GetString(1);
                                        iReply.Add(perm.ToString());
                                        api = readData.GetString(2);
                                        if (String.IsNullOrEmpty(api))
                                        {
                                            api = "---";
                                            iReply.Add(api.ToString());
                                        }
                                        uwi = readData.GetString(3);
                                        if (String.IsNullOrEmpty(uwi))
                                        {
                                            uwi = "---";
                                            iReply.Add(uwi.ToString());
                                        }
                                        lngi = readData.GetDecimal(4);
                                        iReply.Add(lngi.ToString());
                                        lngC = readData.GetInt32(5);
                                        iReply.Add(lngC.ToString());
                                        lnC = DMG.getCardinalName(lngC);
                                        longitude = String.Format("{0} ({1})", lngi, lnC);
                                        iReply.Add(longitude.ToString());
                                        lati = readData.GetDecimal(6);
                                        iReply.Add(lati.ToString());
                                        latC = readData.GetInt32(7);
                                        iReply.Add(latC.ToString());
                                        ltC = DMG.getCardinalName(latC);
                                        latitude = String.Format("{0} ({1})", lati, ltC);
                                        iReply.Add(latitude.ToString());
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getWellName(Int32 WellID, String ClientDBString)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [welName] FROM [Well] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWellPlanFileData(Int32 WellID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wpfdID = iReply.Columns.Add("wpfdID", typeof(Int32));
                DataColumn md = iReply.Columns.Add("md", typeof(String));
                DataColumn inc = iReply.Columns.Add("inc", typeof(String));
                DataColumn azm = iReply.Columns.Add("azm", typeof(String));
                DataColumn tvd = iReply.Columns.Add("tvd", typeof(String));
                DataColumn sselev = iReply.Columns.Add("sselev", typeof(String));
                DataColumn noffset = iReply.Columns.Add("noffset", typeof(String));
                DataColumn eoffset = iReply.Columns.Add("eoffset", typeof(String));
                DataColumn dls = iReply.Columns.Add("dls", typeof(String));
                DataColumn brate = iReply.Columns.Add("brate", typeof(String));
                DataColumn trate = iReply.Columns.Add("trate", typeof(String));
                DataColumn tface = iReply.Columns.Add("tface", typeof(String));
                DataColumn vs = iReply.Columns.Add("vs", typeof(String));
                DataColumn northing = iReply.Columns.Add("northing", typeof(String));
                DataColumn easting = iReply.Columns.Add("easting", typeof(String));
                String selData = "SELECT [wpfdID], [md], [inc], [azm], [tvd], [sselev], [noffset], [eoffset], [dls], [brate], [trate], [tface], [vs], [northing], [easting] FROM [WellPlanDataFileData] WHERE [welID] = @welID";
                Int32 id = -99;
                String dMD = String.Empty, dInc = String.Empty, dAzm = String.Empty, dTVD = String.Empty, dSSE = String.Empty, dNOff = String.Empty, dEOff = String.Empty, dDLS = String.Empty, dBRate = String.Empty, dTRate = String.Empty, dTFace = String.Empty, dVS = String.Empty, dNorthing = String.Empty, dEasting = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        dMD = Convert.ToString(readData.GetDecimal(1));
                                        dInc = Convert.ToString(readData.GetDecimal(2));
                                        dAzm = Convert.ToString(readData.GetDecimal(3));
                                        dTVD = Convert.ToString(readData.GetDecimal(4));
                                        dSSE = Convert.ToString(readData.GetDecimal(5));
                                        dNOff = Convert.ToString(readData.GetDecimal(6));
                                        dEOff = Convert.ToString(readData.GetDecimal(7));
                                        dDLS = Convert.ToString(readData.GetDecimal(8));
                                        dBRate = Convert.ToString(readData.GetDecimal(9));
                                        dTRate = Convert.ToString(readData.GetDecimal(10));
                                        dTFace = Convert.ToString(readData.GetDecimal(11));
                                        dVS = Convert.ToString(readData.GetDecimal(12));
                                        dNorthing = Convert.ToString(readData.GetDecimal(13));
                                        dEasting = Convert.ToString(readData.GetDecimal(14));
                                        iReply.Rows.Add(id, dMD, dInc, dAzm, dTVD, dSSE, dNOff, dEOff, dDLS, dBRate, dTRate, dTFace, dVS, dNorthing, dEasting);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWellPlanInfoTable(Int32 WellPadID, Int32 WellID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wpiID = iReply.Columns.Add("wpiID", typeof(Int32));
                DataColumn datID = iReply.Columns.Add("datID", typeof(String));
                DataColumn wpdrID = iReply.Columns.Add("wpdrID", typeof(String));
                DataColumn wpiElev = iReply.Columns.Add("wpiElev", typeof(String));
                DataColumn luID = iReply.Columns.Add("luID", typeof(String));
                DataColumn wpiAzm = iReply.Columns.Add("wpiAzm", typeof(String));
                DataColumn wpiNorthing = iReply.Columns.Add("wpiNorthing", typeof(String));
                DataColumn wpiNorthingUnit = iReply.Columns.Add("wpiNorthingUnit", typeof(String));
                DataColumn wpiEasting = iReply.Columns.Add("wpiEasting", typeof(String));
                DataColumn wpiEastingUnit = iReply.Columns.Add("wpiEastingUnit", typeof(String));
                String selData = "SELECT [wpiID], [datID], [wpdrID], [wpiElev], [luID], [wpiAzm], [wpiNorthing], [wpiNorthingUnit], [wpiEasting], [wpiEastingUnit] FROM [WellPlanMetaInfo] WHERE [wpdID] = @wpdID AND [welID] = @welID ";
                selData += " and format( ctime , 'yyyyMMddHHmm' ) in ";
                selData += "(select  format( max(ctime) , 'yyyyMMddHHmm' ) FROM [WellPlanMetaInfo] where welID = @welID);";
                Int32 id = -99, dat = -99, dref = -99, lID = -99, nID = -99, eID = -99;
                Decimal elevD = -99.00M, azm = -99.00M;
                String datName = String.Empty, drefName = String.Empty, elevDName = String.Empty, lName = String.Empty, azmValue = String.Empty, tiName = String.Empty, northing = String.Empty, easting = String.Empty, nuName = String.Empty, euName = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        dat = readData.GetInt32(1);
                                        if (dat.Equals(-99) || dat.Equals(null))
                                        {
                                            datName = "---";
                                        }
                                        else
                                        {
                                            datName = getDatumName(dat);
                                        }
                                        dref = readData.GetInt32(2);
                                        if (dref.Equals(-99) || dref.Equals(null))
                                        {
                                            drefName = "---";
                                        }
                                        else
                                        {
                                            drefName = DMG.getDepthReferenceName(dref);
                                        }
                                        elevD = readData.GetDecimal(3);
                                        if (Convert.ToString(elevD).Equals("-99.99") || elevD.Equals(null))
                                        {
                                            elevDName = "---";
                                        }
                                        else
                                        {
                                            elevDName = Convert.ToString(elevD);
                                        }
                                        lID = readData.GetInt32(4);
                                        if (lID.Equals(-99) || lID.Equals(null))
                                        {
                                            lName = "---";
                                        }
                                        else
                                        {
                                            lName = getLengthUnitName(lID);
                                        }
                                        azm = readData.GetDecimal(5);
                                        if (Convert.ToString(azm).Equals("-99.99") || azm.Equals(null))
                                        {
                                            azmValue = "---";
                                        }
                                        else
                                        {
                                            azmValue = Convert.ToString(azm);
                                        }
                                        northing = Convert.ToString(readData.GetDecimal(6));
                                        if (String.IsNullOrEmpty(northing))
                                        {
                                            northing = "---";
                                        }
                                        nID = readData.GetInt32(7);
                                        if (nID.Equals(-99) || nID.Equals(null))
                                        {
                                            nuName = "---";
                                        }
                                        else
                                        {
                                            nuName = getLengthUnitName(nID);
                                        }
                                        easting = Convert.ToString(readData.GetDecimal(8));
                                        if (String.IsNullOrEmpty(easting))
                                        {
                                            easting = "---";
                                        }
                                        eID = readData.GetInt32(9);
                                        if (eID.Equals(-99) || eID.Equals(null))
                                        {
                                            euName = "---";
                                        }
                                        else
                                        {
                                            euName = getLengthUnitName(eID);
                                        }
                                        iReply.Rows.Add(id, datName, drefName, elevDName, lName, azmValue, northing, nuName, easting, euName);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWellPlacementTieInTable(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wpiID = iReply.Columns.Add("wpctiID", typeof(Int32));
                DataColumn datID = iReply.Columns.Add("MDepth", typeof(String));
                DataColumn wpdrID = iReply.Columns.Add("Inc", typeof(String));
                DataColumn wpiElev = iReply.Columns.Add("Azm", typeof(String));
                DataColumn luID = iReply.Columns.Add("TVD", typeof(String));
                DataColumn wpiAzm = iReply.Columns.Add("NorthOffset", typeof(String));
                DataColumn wpiNorthing = iReply.Columns.Add("EastOffset", typeof(String));
                DataColumn wpiNorthingUnit = iReply.Columns.Add("VS", typeof(String));
                DataColumn wpiEasting = iReply.Columns.Add("Subsea", typeof(String));
                String selData = "SELECT [wpctiID], [MDepth], [MDepthUnit], [Inc], [Azm], [TVD], [TVDUnit], [NorthOffset], [NOffsetUnit], [EastOffset], [EOffsetUnit], [VS], [VSUnit], [Subsea], [SubseaUnit] FROM [WellPlacementTieIn] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID AND [welID] = @welID";
                Int32 id = -99, mdU = -99, tvdU = -99, nofU = -99, eofU = -99, vsU = -99, ssU = -99;
                String mdV = String.Empty, incV = String.Empty, azmV = String.Empty, tvdV = String.Empty, nofV = String.Empty, eofV = String.Empty, vsV = String.Empty, ssV = String.Empty;
                String mdN = String.Empty, tvdN = String.Empty, nofN = String.Empty, eofN = String.Empty, vsN = String.Empty, ssN = String.Empty;
                Decimal md = -99.00M, inc = -99.00M, azm = -99.00M, tvd = -99.00M, nof = -99.00M, eof = -99.00M, vs = -99.00M, ss = -99.00M;                
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        md = readData.GetDecimal(1);
                                        mdU = readData.GetInt32(2);
                                        mdN = getLengthUnitName(mdU);
                                        if (md.Equals(-99.00M)) { mdV = "---"; } else { mdV = String.Format("{0} {1}", md, mdN); }
                                        inc = readData.GetDecimal(3);
                                        azm = readData.GetDecimal(4);
                                        tvd = readData.GetDecimal(5);
                                        tvdU = readData.GetInt32(6);
                                        tvdN = getLengthUnitName(tvdU);
                                        if (tvd.Equals(-99.00M)) { tvdV = "---"; } else { tvdV = String.Format("{0} {1}", tvd.ToString("0.0000"), tvdN); }
                                        nof = readData.GetDecimal(7);
                                        nofU = readData.GetInt32(8);
                                        nofN = getLengthUnitName(nofU);
                                        if (nof.Equals(-99.00M)) { nofV = "---"; } else { nofV = String.Format("{0} {1}", nof.ToString("0.0000"), nofN); }
                                        eof = readData.GetDecimal(9);
                                        eofU = readData.GetInt32(10);
                                        eofN = getLengthUnitName(eofU);
                                        if (eof.Equals(-99.00M)) { eofV = "---"; } else { eofV = String.Format("{0} {1}", eof.ToString("0.0000"), eofN); }
                                        vs = readData.GetDecimal(11);
                                        vsU = readData.GetInt32(12);
                                        vsN = getLengthUnitName(vsU);
                                        if (vs.Equals(-99.00M)) { vsV = "---"; } else { vsV = String.Format("{0} {1}", vs.ToString("0.0000"), vsN); }
                                        ss = readData.GetDecimal(13);
                                        ssU = readData.GetInt32(14);
                                        ssN = getLengthUnitName(ssU);
                                        if (ss.Equals(-99.00M)) { ssV = "---"; } else { ssV = String.Format("{0} {1}", ss.ToString("0.0000"), ssN); }
                                        iReply.Rows.Add(id, mdV, inc.ToString("0.00"), azm.ToString("0.00"), tvdV, nofV, eofV, vsV, ssV);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getDemogWellDetailTable(Int32 WellID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn welID = iReply.Columns.Add("welID", typeof(Int32));
                DataColumn welLat = iReply.Columns.Add("welLat", typeof(Decimal));
                DataColumn welLng = iReply.Columns.Add("welLng", typeof(Decimal));
                DataColumn welLatDMS = iReply.Columns.Add("welLatDMS", typeof(String));
                DataColumn welLngDMS = iReply.Columns.Add("welLngDMS", typeof(String));
                String selData = "SELECT [welID], [welLatitude], [welLongitude], [welLatDegree], [welLatMinutes], [welLatSeconds], [welLatOrientation], [welLngDegree], [welLngMinutes], [welLngSeconds], [welLngOrientation] FROM [dmgWell] WHERE [welID] = @welID";
                Int32 id = 0, laD = -99, laM = -99, laO = -99, lnD = -99, lnM = -99, lnO = -99;
                Decimal lat = -99.000000M, lng = -99.000000M, laS = -99.0000M, lnS = -99.0000M;
                String laDMS = String.Empty, lnDMS = String.Empty, laH = String.Empty, lnH = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        lat = readData.GetDecimal(1);
                                        lng = readData.GetDecimal(2);
                                        laD = readData.GetInt32(3);
                                        laM = readData.GetInt32(4);
                                        laS = readData.GetDecimal(5);
                                        laO = readData.GetInt32(6);
                                        lnD = readData.GetInt32(7);
                                        lnM = readData.GetInt32(8);
                                        lnS = readData.GetDecimal(9);
                                        lnO = readData.GetInt32(10);
                                        laDMS = (laD + "° " + laM + "' " + laS + "'' " + laH);
                                        lnDMS = (lnD + "° " + lnM + "' " + lnS + "'' " + lnH);
                                        iReply.Rows.Add(id, lat, lng, laDMS, lnDMS);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getWellPadName(Int32 WellPadID, String ClientDBString)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [welPadName] FROM [WellPad] WHERE [wpdID] = @wpdID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.NVarChar).Value = WellPadID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWellServiceRegistration(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wsrID = iReply.Columns.Add("wsrID", typeof(Int32));
                DataColumn optrID = iReply.Columns.Add("optrID", typeof(String));
                DataColumn clntID = iReply.Columns.Add("clntID", typeof(String));
                DataColumn wpdID = iReply.Columns.Add("wpdID", typeof(String));
                DataColumn welID = iReply.Columns.Add("welID", typeof(String));
                DataColumn srvGrpID = iReply.Columns.Add("srvGrpID", typeof(String));
                DataColumn srvID = iReply.Columns.Add("srvID", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                DataColumn isApproved = iReply.Columns.Add("isApproved", typeof(String));
                DataColumn appColor = iReply.Columns.Add("appColor", typeof(String));
                DateTime uT = DateTime.Now;
                Int32 id = -99, optr = -99, clnt = -99, pad = -99, well = -99, group = -99, srv = -99, appr = -99;
                String OpCo = String.Empty, clntCo = String.Empty, padName = String.Empty, wellName = String.Empty, groupName = String.Empty, serviceName = String.Empty, approval = String.Empty, color = String.Empty;
                String selData = "SELECT [wsrID], [optrID], [clntID], [wpdID], [welID], [srvGrpID], [srvID], [uTime], [isApproved] FROM [WellServiceRegistration] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID AND [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        optr = readData.GetInt32(1);
                                        OpCo = CLNT.getClientName(optr);
                                        clnt = readData.GetInt32(2);
                                        clntCo = CLNT.getClientName(clnt);
                                        pad = readData.GetInt32(3);
                                        padName = getWellPadName(pad, ClientDBString);
                                        well = readData.GetInt32(4);
                                        wellName = getWellName(well, ClientDBString);
                                        group = readData.GetInt32(5);
                                        groupName = SRV.getSrvGroupName(group);
                                        srv = readData.GetInt32(6);
                                        serviceName = SRV.getServiceName(srv);
                                        uT = readData.GetDateTime(7);
                                        appr = readData.GetInt32(8);
                                        switch (appr)
                                        {
                                            case 1: { approval = DMG.getWellStatusName(appr); color = "Green"; break; }
                                            case 2: { approval = DMG.getWellStatusName(appr); color = "Green"; break; }
                                            case 3: { approval = DMG.getWellStatusName(appr); color = "Yellow"; break; }
                                            case 4: { approval = DMG.getWellStatusName(appr); color = "Abondoned"; break; }
                                            default: { approval = DMG.getWellStatusName(appr); color = "Red"; break; }
                                        }
                                        iReply.Rows.Add(id, OpCo, clntCo, padName, wellName, groupName, serviceName, uT, approval, color);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getWellRegisteredServiceID(Int32 WellID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selStat = "SELECT [srvID] FROM [WellServiceRegistration] WHERE [welID] = @welID AND [isApproved] = '2'";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selStat, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getWellRegServiceID(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selStat = "SELECT [srvID] FROM [WellServiceRegistration] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID AND [welID] = @welID AND [isApproved] = '2'";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selStat, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getLastRunID(Int32 WellID, Int32 WellSectionID, String GetClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selStat = "SELECT MAX(runID) FROM [RawQCResults] WHERE [welID] = @welID AND [wswID] = @wswID";
                using (SqlConnection dataCon = new SqlConnection(GetClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selStat, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal getMaxMDForLastRun(Int32 LastRunID, Int32 WellID, Int32 WellSectionID, String GetClientDBString)
        {
            try
            {
                Decimal iReply = -99.00M;
                String selStat = "SELECT ISNULL(( SELECT MAX(Depth) FROM [RawQCResults] WHERE [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID), 0.0) ";
                using (SqlConnection dataCon = new SqlConnection(GetClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selStat, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = LastRunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetDecimal(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Decimal> getTieInValuesFromLastRun(Decimal MeasuredDepth, Int32 LastRunID, Int32 WellID, Int32 WellSectionID, String GetClientDBString)
        {
            try
            {
                List<Decimal> iReply = new List<Decimal>();
                Decimal TIInc = -99.000000000000M, TIAzm = -99.000000000000M, TITVD = -99.000000000000M, TINorth = -99.000000000000M, TIEast = -99.000000000000M, TIVS = -99.000000000000M, TISubSea = -99.000000000000M;
                String selStat = "SELECT [colT], [colDR], [rwTvdTI], [rwNorthingTI], [rwEastingTI], [rwVSTI], [rwSubseaTI] FROM [RawQCResults] WHERE [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(GetClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selStat, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = LastRunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply.Add(MeasuredDepth);
                                        TIInc = readData.GetDecimal(0);
                                        iReply.Add(TIInc);
                                        TIAzm = readData.GetDecimal(1);
                                        iReply.Add(TIAzm);
                                        TITVD = readData.GetDecimal(2);
                                        iReply.Add(TITVD);
                                        TINorth = readData.GetDecimal(3);
                                        iReply.Add(TINorth);
                                        TIEast = readData.GetDecimal(4);
                                        iReply.Add(TIEast);
                                        TIVS = readData.GetDecimal(5);
                                        iReply.Add(TIVS);
                                        TISubSea = readData.GetDecimal(6);
                                        iReply.Add(TISubSea);
                                    }
                                }
                                else
                                {
                                    iReply.Add(0.00M);
                                    iReply.Add(0.00M);
                                    iReply.Add(0.00M);
                                    iReply.Add(0.00M);
                                    iReply.Add(0.00M);
                                    iReply.Add(0.00M);
                                    iReply.Add(0.00M);
                                    iReply.Add(0.00M);
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getWellStatusID(Int32 OperatorID, Int32 WellpadID, Int32 WellID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String updateString = "SELECT [wstID] FROM [Well] WHERE [optrID] = @optrID AND [wpdID] = @wpdID AND [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(updateString, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellpadID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        iReply = Convert.ToInt32(getData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getWellRegistrationStatusName(Int32 OperatorID, Int32 WellpadID, Int32 WellID, String ClientDBString)
        {
            try
            {
                String iReply = String.Empty;
                Int32 statusID = -99;
                String updateString = "SELECT [wstID] FROM [Well] WHERE [optrID] = @optrID AND [wpdID] = @wpdID AND [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(updateString, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellpadID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        statusID = Convert.ToInt32(getData.ExecuteNonQuery());
                        iReply = DMG.getWellProcessStatus(statusID);
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertWellIntoWellPad(Int32 WellPadID, Int32 WellID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String insStat = "INSERT INTO [WellPadToWell] ([wpdID], [welID], [cTime], [uTime]) VALUES (@wpdID, @welID, @cTime, @uTime); SELECT SCOPE_IDENTITY()";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(insStat, dataCon);
                        insData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        insData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        iReply = Convert.ToInt32(insData.ExecuteScalar());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertGlobalWellToWellPad(Int32 WellPadID, Int32 WellID)
        {
            try
            {
                Int32 iReply = -99;
                String insStat = "INSERT INTO [dmgWellPadToWell] ([wpdID], [welID], [cTime], [uTime]) VALUES (@wpdID, @welID, @cTime, @uTime)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(insStat, dataCon);
                        insData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        insData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertWellPad(Int32 ClientID, Int32 OperatorID, Int32 RegionID, Int32 SubRegionID, Int32 CountryID, Int32 StateID, Int32 CountyID, Int32 FieldID, String WellPadName, String LoginName, String LoginRealm, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99, iDuplicate = -99, padID = -99;
                String LowerPadName = String.Empty;
                TextInfo tiWNme = new CultureInfo("en-US", false).TextInfo;
                LowerPadName = tiWNme.ToLower(WellPadName);
                LowerPadName = LowerPadName.Replace(" ", "");
                iDuplicate = findDuplicateWellPadName(WellPadName, ClientDBString);
                if (iDuplicate.Equals(1))
                {
                    iReply = -1;
                }
                else
                {
                    String insCmd = "INSERT INTO [WellPad] ([clntID], [optrID], [welPadName], [welPadNameLower], [regID], [sregID], [ctyID], [cntyID], [fldID], [stID], [username], [domain], [cTime], [uTime]) VALUES (@clntID, @optrID, @welPadName, @welPadNameLower, @regID, @sregID, @ctyID, @cntyID, @fldID, @stID, @username, @domain, @cTime, @uTime); SELECT SCOPE_IDENTITY();";
                    using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                    {
                        try
                        {
                            dataCon.Open();
                            SqlCommand insData = new SqlCommand(insCmd, dataCon);
                            insData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                            insData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                            insData.Parameters.AddWithValue("@welPadName", SqlDbType.NVarChar).Value = WellPadName.ToString();
                            insData.Parameters.AddWithValue("@welPadNameLower", SqlDbType.NVarChar).Value = LowerPadName.ToString();
                            insData.Parameters.AddWithValue("@regID", SqlDbType.Int).Value = RegionID.ToString();
                            insData.Parameters.AddWithValue("@sregID", SqlDbType.Int).Value = SubRegionID.ToString();
                            insData.Parameters.AddWithValue("@ctyID", SqlDbType.Int).Value = CountryID.ToString();
                            insData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                            insData.Parameters.AddWithValue("@fldID", SqlDbType.Int).Value = FieldID.ToString();
                            insData.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = StateID.ToString();
                            insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = LoginName.ToString();
                            insData.Parameters.AddWithValue("@domain", SqlDbType.NVarChar).Value = LoginRealm.ToString();
                            insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                            insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                            padID = Convert.ToInt32(insData.ExecuteScalar()); 
                            
                                iReply = insertGlobalWellPad(ClientID, OperatorID, RegionID, SubRegionID, CountryID, StateID, CountyID, FieldID, WellPadName, padID, LoginName, LoginRealm);
                            
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            dataCon.Close();
                            dataCon.Dispose();
                        }
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertGlobalWellPad(Int32 ClientID, Int32 OperatorID, Int32 RegionID, Int32 SubRegionID, Int32 CountryID, Int32 StateID, Int32 CountyID, Int32 FieldID, String WellPadName, Int32 SiloPadID, String LoginName, String LoginRealm)
        {
            try
            {
                Int32 iReply = -99, iDuplicate = -99;
                String LowerPadName = String.Empty;
                String ClientDBString = WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString();
                TextInfo tiWNme = new CultureInfo("en-US", false).TextInfo;
                LowerPadName = tiWNme.ToLower(WellPadName);
                LowerPadName = LowerPadName.Replace(" ", "");
                iDuplicate = findDuplicateGlobalWellPadName(WellPadName, ClientDBString);
                if (iDuplicate.Equals(1))
                {
                    iReply = -1;
                }
                else
                {
                    String insCmd = "INSERT INTO [dmgWellPad] ([clntID], [optrID], [welPadName], [welPadNameLower], [regID], [sregID], [ctyID], [cntyID], [fldID], [stID], [username], [domain], [cTime], [uTime], [wpdID]) VALUES (@clntID, @optrID, @welPadName, @welPadNameLower, @regID, @sregID, @ctyID, @cntyID, @fldID, @stID, @username, @domain, @cTime, @uTime, @wpdID);";
                    using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                    {
                        try
                        {
                            dataCon.Open();
                            SqlCommand insData = new SqlCommand(insCmd, dataCon);
                            insData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                            insData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                            insData.Parameters.AddWithValue("@welPadName", SqlDbType.NVarChar).Value = WellPadName.ToString();
                            insData.Parameters.AddWithValue("@welPadNameLower", SqlDbType.NVarChar).Value = LowerPadName.ToString();
                            insData.Parameters.AddWithValue("@regID", SqlDbType.Int).Value = RegionID.ToString();
                            insData.Parameters.AddWithValue("@sregID", SqlDbType.Int).Value = SubRegionID.ToString();
                            insData.Parameters.AddWithValue("@ctyID", SqlDbType.Int).Value = CountryID.ToString();
                            insData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                            insData.Parameters.AddWithValue("@fldID", SqlDbType.Int).Value = FieldID.ToString();
                            insData.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = StateID.ToString();
                            insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = LoginName.ToString();
                            insData.Parameters.AddWithValue("@domain", SqlDbType.NVarChar).Value = LoginRealm.ToString();
                            insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                            insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                            insData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = SiloPadID.ToString();
                            iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            dataCon.Close();
                            dataCon.Dispose();
                        }
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }                

        public static Int32 insertWellServiceRegistration(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 ServiceGroupID, Int32 ServiceID, String LoginName, String LoginRealm, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99, dup = -99;
                String insStat = "INSERT INTO [WellServiceRegistration] ([optrID], [clntID], [wpdID], [welID], [srvGrpID], [srvID], [username], [realm], [cTime], [uTime]) VALUES (@optrID, @clntID, @wpdID, @welID, @srvGrpID, @srvID, @username, @realm, @cTime, @uTime)";
                dup = findDuplicateWellServiceRegistration(OperatorID, ClientID, WellPadID, WellID, ServiceGroupID, ServiceID, ClientDBConnection);
                if (dup.Equals(1))
                {
                    iReply = -1;
                }
                else
                {
                    using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                    {
                        try
                        {
                            dataCon.Open();
                            SqlCommand addData = new SqlCommand(insStat, dataCon);
                            addData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                            addData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                            addData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                            addData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                            addData.Parameters.AddWithValue("@srvGrpID", SqlDbType.Int).Value = ServiceGroupID.ToString();
                            addData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                            addData.Parameters.AddWithValue("@username", SqlDbType.Int).Value = LoginName.ToString();
                            addData.Parameters.AddWithValue("@realm", SqlDbType.Int).Value = LoginRealm.ToString();
                            addData.Parameters.AddWithValue("@cTime", SqlDbType.Int).Value = DateTime.Now.ToString();
                            addData.Parameters.AddWithValue("@uTime", SqlDbType.Int).Value = DateTime.Now.ToString();
                            iReply = Convert.ToInt32(addData.ExecuteNonQuery());
                            if (iReply.Equals(1))
                            {
                                iReply = insertdmgWellServiceRegistration(OperatorID, ClientID, WellPadID, WellID, ServiceGroupID, ServiceID, LoginName, LoginRealm);
                                if (iReply.Equals(1))
                                {
                                    iReply = SM.sendWellServiceRegistrationMessage(LoginName, LoginRealm, OperatorID, ClientID, WellPadID, WellID, ServiceGroupID, ServiceID, ClientDBConnection);
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            dataCon.Close();
                            dataCon.Dispose();
                        }
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 insertdmgWellServiceRegistration(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 ServiceGroupID, Int32 ServiceID, String LoginName, String LoginRealm)
        {
            try
            {
                Int32 iReply = -99;
                String insStat = "INSERT INTO [dmgWellServiceRegistration] ([optrID], [clntID], [wpdID], [welID], [srvGrpID], [srvID], [username], [realm], [cTime], [uTime]) VALUES (@optrID, @clntID, @wpdID, @welID, @srvGrpID, @srvID, @username, @realm, @cTime, @uTime)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                    {
                        try
                        {
                            dataCon.Open();
                            SqlCommand addData = new SqlCommand(insStat, dataCon);
                            addData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                            addData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                            addData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                            addData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                            addData.Parameters.AddWithValue("@srvGrpID", SqlDbType.Int).Value = ServiceGroupID.ToString();
                            addData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                            addData.Parameters.AddWithValue("@username", SqlDbType.Int).Value = LoginName.ToString();
                            addData.Parameters.AddWithValue("@realm", SqlDbType.Int).Value = LoginRealm.ToString();
                            addData.Parameters.AddWithValue("@cTime", SqlDbType.Int).Value = DateTime.Now.ToString();
                            addData.Parameters.AddWithValue("@uTime", SqlDbType.Int).Value = DateTime.Now.ToString();
                            iReply = Convert.ToInt32(addData.ExecuteNonQuery());                            
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            dataCon.Close();
                            dataCon.Dispose();
                        }
                    }                
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        private static Int32 runFolder(String rName, Int32 runID, Int32 welPadID, Int32 welID, String ClientRealm, String dbConn)
        {
            try
            {
                Int32 result = -99;
                Int32 oprID = -99;
                String oprName = String.Empty, rD = String.Empty;

                //Get Folder Paths from configuration
                String genDirectory = WebConfigurationManager.AppSettings["GenPath"].ToString();
                String uplDirectory = WebConfigurationManager.AppSettings["UploadPath"].ToString();

                oprID = getOperatorID(welID, dbConn);
                oprName = getOperatorName(oprID);
                String clntDomain = null;
                String pdName = String.Concat("WellPad_", welPadID);
                String wlName = String.Concat("Well_", welID);
                clntDomain = Convert.ToString(ClientRealm);
                TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                clntDomain = ti.ToLower(clntDomain);
                clntDomain = clntDomain.Replace(".", "_");
                oprName = ti.ToLower(oprName);
                oprName = oprName.Replace(" ", "_");
                String rGFolderRaw = null; //Run Generated Folder for Raw QC
                String rGFolderRes = null; //Run Generated Folder for ResQC
                String rUFolderRaw = null; //Run Uploaded Documents Folder for RawQC
                String rUFolderRes = null; //Run Uploaded Documents Folder for ResQC
                rD = String.Format("{0}_{1}\\", "Run ", runID);
                rGFolderRaw = String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\{7}\\{8}\\", genDirectory, clntDomain, "Assets", "Wells", oprName, pdName, wlName, "RawDataQC", rD);
                rGFolderRes = String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\{7}\\{8}\\", genDirectory, clntDomain, "Assets", "Wells", oprName, pdName, wlName, "ResultsQC", rD);
                rUFolderRaw = String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\{7}\\{8}\\", uplDirectory, clntDomain, "Assets", "Wells", oprName, pdName, wlName, "RawDataQC", rD);
                rUFolderRes = String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\{7}\\{8}\\", uplDirectory, clntDomain, "Assets", "Wells", oprName, pdName, wlName, "ResultsQC", rD);
                DirectoryInfo runGenFolderRaw = new DirectoryInfo(rGFolderRaw);
                DirectoryInfo runGenFolderRes = new DirectoryInfo(rGFolderRes);
                DirectoryInfo runUplFolderRaw = new DirectoryInfo(rUFolderRaw);
                DirectoryInfo runUplFolderRes = new DirectoryInfo(rUFolderRes);

                if ((!Directory.Exists(rGFolderRaw)) || (!Directory.Exists(rGFolderRes)) || (!Directory.Exists(rUFolderRaw)) || (!Directory.Exists(rUFolderRes)))
                {
                    runGenFolderRaw.Create(); //Create Directory Folder
                    runGenFolderRes.Create();
                    runUplFolderRaw.Create();
                    runUplFolderRes.Create();
                }

                if ((Directory.Exists(rGFolderRaw)) || (Directory.Exists(rGFolderRes)) || (Directory.Exists(rUFolderRaw)) || (Directory.Exists(rUFolderRes))) //Checking if Folder exists then confirming to calling method
                {
                    result = 1;
                    return result;
                }
                else
                {
                    result = -1;
                    return result;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 updateWellStatus(Int32 OperatorID, Int32 WellID, Int32 WellStatusID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String updateString = "UPDATE [Well] SET [wstID] = @wstID WHERE [optrID] = @optrID AND [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand updData = new SqlCommand(updateString, dataCon);
                        updData.Parameters.AddWithValue("@wstID", SqlDbType.Int).Value = WellStatusID.ToString();
                        updData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        updData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        iReply = Convert.ToInt32(updData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}