﻿using SmartsVer1.Helpers.AccountHelpers;
using Microsoft.SqlServer.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Net.Mail;
using System.Web;
using System.Collections;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using Units = SmartsVer1.Helpers.DemogHelpers.UnitsConverter;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using CHRT = SmartsVer1.Helpers.QCHelpers.Opr;

namespace SmartsVer1.Helpers.QCHelpers
{
    public class Opr
    {
        static void Main()
        { }


        //Active Run operations
        public static Int32 addActiveRun(Decimal runInclincation, Decimal runAzimuth, Decimal runTInclination, Decimal runTAzimuth, Decimal StartDepth, Decimal EndDepth, Int32 LengthUnit, Int32 OperatorID, Int32 WellpadID, Int32 WellID, Int32 WellSectionID, String WellSectionName, Int32 ToolCodeID, Int32 SQC, Int32 RunStatusID, String WellName, Int32 RunID, String RunName, String UserName, String ClientRealm, String dbConn)
        {
            try
            {
                Int32 iReply = -99, runAdded = -99, rtf = -99;
                runAdded = insertRun(RunID, runInclincation, runAzimuth, runTInclination, runTAzimuth, StartDepth, EndDepth, LengthUnit, OperatorID, WellpadID, WellID, WellSectionID, RunStatusID, ToolCodeID, SQC, UserName, ClientRealm, dbConn);
                if(!runAdded.Equals(-99))
                {                    
                    rtf = runFolder(RunName, runAdded, WellID, WellSectionName, ClientRealm, dbConn);
                    if (rtf.Equals(1))
                    {
                        iReply = 1; //Success                                
                    }
                    else
                    {
                        iReply = -3; //Could not create Run folder                                
                    }                                          
                }
                else
                {
                    iReply = -1;//Could not add RUN to System                    
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addRefMag(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 IFRValueID, Decimal MagneticDeclination, Int32 MagDeclinationDirectionID, Decimal Dip, Decimal BTotal, Int32 MagnetometerUnitID, Decimal GTotal, Int32 AccelerometerUnitID, Decimal GridConvergence, Int32 GridConvergenceDirectionID, Int32 JobConvergenceID, String DBString)
        {
            try
            {
                Int32 result = -99, chkRF = -99;
                String refmagInsert = "INSERT INTO [RefMags] ([ifrvalID], [mdec], [cID], [dip], [bTotal], [muID], [gTotal], [acuID], [gconv], [gcoID], [jcID], [dmgstID], [cTime], [uTime], [welID], [wpdID], [clntID], [optrID]) VALUES (@ifrvalID, @mdec, @cID, @dip, @bTotal, @muID, @gTotal, @acuID, @gconv, @gcoID, @jcID, @dmgstID, @cTime, @uTime, @welID, @wpdID, @clntID, @optrID)";
                chkRF = checkReferenceMagnetics(WellID, DBString); //Checking if RefMags already exist and setting those to InActive so new data can be used for calcs
                using (SqlConnection clntConn = new SqlConnection(DBString))
                    {
                        try
                        {
                            clntConn.Open();
                            SqlCommand insertRefMags = new SqlCommand(refmagInsert, clntConn);
                            insertRefMags.Parameters.AddWithValue("@ifrvalID", SqlDbType.Int).Value = IFRValueID.ToString();
                            insertRefMags.Parameters.AddWithValue("@mdec", SqlDbType.Decimal).Value = MagneticDeclination.ToString();
                            insertRefMags.Parameters.AddWithValue("@cID", SqlDbType.Int).Value = MagDeclinationDirectionID.ToString();
                            insertRefMags.Parameters.AddWithValue("@dip", SqlDbType.Decimal).Value = Dip.ToString();
                            insertRefMags.Parameters.AddWithValue("@bTotal", SqlDbType.Decimal).Value = BTotal.ToString();
                            insertRefMags.Parameters.AddWithValue("@muID", SqlDbType.Int).Value = MagnetometerUnitID.ToString();
                            insertRefMags.Parameters.AddWithValue("@gTotal", SqlDbType.Decimal).Value = GTotal.ToString();
                            insertRefMags.Parameters.AddWithValue("@acuID", SqlDbType.Int).Value = AccelerometerUnitID.ToString();
                            insertRefMags.Parameters.AddWithValue("@gconv", SqlDbType.Decimal).Value = GridConvergence.ToString();
                            insertRefMags.Parameters.AddWithValue("@gcoID", SqlDbType.Int).Value = GridConvergenceDirectionID.ToString();
                            insertRefMags.Parameters.AddWithValue("@jcID", SqlDbType.Int).Value = JobConvergenceID.ToString();
                            insertRefMags.Parameters.AddWithValue("@dmgstID", SqlDbType.Int).Value = "1".ToString();
                            insertRefMags.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToShortDateString();
                            insertRefMags.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToShortDateString();
                            insertRefMags.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                            insertRefMags.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                            insertRefMags.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                            insertRefMags.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();

                            result = Convert.ToInt32(insertRefMags.ExecuteNonQuery());
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                    }                
                return result;                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 addRunToWellSection(Int32 RunID, Int32 WellSectionToWellID, String dbConn)
        {
            try
            {
                Int32 iReply = -99;
                String insData = "INSERT INTO [RunToWellSection] ([runID], [wswID]) VALUES (@runID, @wswID)";
                using (SqlConnection dataCon = new SqlConnection(dbConn.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand addData = new SqlCommand(insData, dataCon);
                        addData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        addData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionToWellID.ToString();
                        iReply = Convert.ToInt32(addData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 addWellSectionToWell(Int32 WellSectionID, Int32 WellID, String dbConn)
        {
            try
            {
                Int32 rtwAdded = -99, result = -99;                
                String insertCommand = "INSERT INTO [WellSectionToWell] ([wsID], [welID], [cTime], [uTime]) VALUES (@wsID, @welID, @cTime, @uTime); SELECT SCOPE_IDENTITY()";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();

                        SqlCommand insertData = new SqlCommand(insertCommand, dataCon);
                        insertData.Parameters.AddWithValue("@wsID", SqlDbType.Int).Value = WellSectionID.ToString();
                        insertData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        insertData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime).Value = DateTime.Now.ToString();
                        insertData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime).Value = DateTime.Now.ToString();

                        rtwAdded = Convert.ToInt32(insertData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                    }
                }
                if (rtwAdded.Equals(1))
                {
                    result = 1;
                    return result;
                }
                else
                {
                    result = -1;
                    return result;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //private static Int32 addWPRunToRunSection(Int32 RunID, Int32 RunSectionID, String dbConn)
        //{
        //    try
        //    {
        //        Int32 rtsAdded = -99, result = -99;

        //        String insertCommand = "INSERT INTO [RunSectionToRun] (rnsID, runID) VALUES (@rnsID, @runID); SELECT SCOPE_IDENTITY()";

        //        using (SqlConnection clntConn = new SqlConnection(dbConn))
        //        {
        //            try
        //            {
        //                clntConn.Open();

        //                SqlCommand insertRunToRunSection = new SqlCommand(insertCommand, clntConn);
        //                insertRunToRunSection.Parameters.AddWithValue("@rnsID", SqlDbType.Int).Value = RunSectionID.ToString();                        
        //                insertRunToRunSection.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();                        

        //                rtsAdded = Convert.ToInt32(insertRunToRunSection.ExecuteNonQuery());
        //            }
        //            catch (Exception ex)
        //            { throw new System.Exception(ex.ToString()); }
        //            finally
        //            {
        //                clntConn.Close();
        //            }
        //        }
        //        if (rtsAdded.Equals(1))
        //        {
        //            result = 1;
        //            return result;
        //        }
        //        else
        //        {
        //            result = -1;
        //            return result;
        //        }
        //    }
        //    catch (Exception ex)
        //    { throw new System.Exception(ex.ToString()); }
        //}

        //Bottom-Hole-Assembly (BHA) operations
        public static Int32 addBHASignature(Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 WellPadID, Int32 ClientID, Int32 OperatorID, String bhaName, Decimal MotorSize, Int32 MSizeUnitID, Decimal DrillStringCollarSize, Int32 DSCSizeUnitID, Decimal MotorDrillBitLength, Int32 MDBLenUnitID, Decimal NonMagSpaceAbove, Int32 NMSpAbvUnitID, Decimal NonMagSpaceBelow, Int32 NMSpBlwUnitID, Int32 dmgstID, String dbConn)
        {
            try
            {
                Int32 activeBHA = -99, resultBHA = -99;
                //Finding if selected Run has an Active BHA and if found, setting it InActive so new BHA can be added and new Run started
                activeBHA = findActiveBHA(RunID, WellSectionID, WellID, WellPadID, ClientID, OperatorID, dbConn);
                if (!activeBHA.Equals(-99))
                {
                    bhaSetInActive(activeBHA, dbConn);
                }
                String insertBHA = "INSERT INTO [BHA] ([runID], [wswID], [welID], [wpdID], [clntID], [optrID], [bhaName], [bhaMSize], [bhaMSizeUID], [bhaDSCSize], [bhaDSCUID], [bhaMDBLen], [bhaMDBLenUID], [bhaNMSpAbv], [bhaNMSAUID], [bhaNMSpBlw], [bhaNMSBUID], [dmgstID], [cTime], [uTime]) VALUES (@runID, @wswID, @welID, @wpdID, @clntID, @optrID, @bhaName, @bhaMSize, @bhaMSizeUID, @bhaDSCSize, @bhaDSCUID, @bhaMDBLen, @bhaMDBLenUID, @bhaNMSpAbv, @bhaNMSAUID, @bhaNMSpBlw, @bhaNMSBUID, @dmgstID, @cTime, @uTime)";                        
                using (SqlConnection clntConn = new SqlConnection(dbConn))
                {
                    try
                    {                        
                        clntConn.Open();                        
                            SqlCommand insertBHACommand = new SqlCommand(insertBHA, clntConn);
                            insertBHACommand.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                            insertBHACommand.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                            insertBHACommand.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                            insertBHACommand.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                            insertBHACommand.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                            insertBHACommand.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                            insertBHACommand.Parameters.AddWithValue("@bhaName", SqlDbType.NVarChar).Value = bhaName.ToString();
                            insertBHACommand.Parameters.AddWithValue("@bhaMSize", SqlDbType.Decimal).Value = MotorSize.ToString();
                            insertBHACommand.Parameters.AddWithValue("@bhaMSizeUID", SqlDbType.Int).Value = MSizeUnitID.ToString();
                            insertBHACommand.Parameters.AddWithValue("@bhaDSCSize", SqlDbType.Decimal).Value = DrillStringCollarSize.ToString();
                            insertBHACommand.Parameters.AddWithValue("@bhaDSCUID", SqlDbType.Int).Value = DSCSizeUnitID.ToString();
                            insertBHACommand.Parameters.AddWithValue("@bhaMDBLen", SqlDbType.Decimal).Value = MotorDrillBitLength.ToString();
                            insertBHACommand.Parameters.AddWithValue("@bhaMDBLenUID", SqlDbType.Int).Value = MDBLenUnitID.ToString();
                            insertBHACommand.Parameters.AddWithValue("@bhaNMSpAbv", SqlDbType.Decimal).Value = NonMagSpaceAbove.ToString();
                            insertBHACommand.Parameters.AddWithValue("@bhaNMSAUID", SqlDbType.Int).Value = NMSpAbvUnitID.ToString();
                            insertBHACommand.Parameters.AddWithValue("@bhaNMSpBlw", SqlDbType.Decimal).Value = NonMagSpaceBelow.ToString();
                            insertBHACommand.Parameters.AddWithValue("@bhaNMSBUID", SqlDbType.Int).Value = NMSpBlwUnitID.ToString();
                            insertBHACommand.Parameters.AddWithValue("@dmgstID", SqlDbType.Int).Value = dmgstID.ToString();
                            insertBHACommand.Parameters.AddWithValue("@cTime", SqlDbType.DateTime).Value = DateTime.Now.ToString();
                            insertBHACommand.Parameters.AddWithValue("@uTime", SqlDbType.DateTime).Value = DateTime.Now.ToString();
                            resultBHA = Convert.ToInt32(insertBHACommand.ExecuteNonQuery());    
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }
                return resultBHA;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addWPBHASignature(Int32 selectedClass, Int32 selectedRUN, String BHASignatureName, Decimal MotorSize, Int32 MSizeUnitID, Decimal DrillStringCollarSize, Int32 DSCSizeUnitID, Decimal MotorDrillBitLength, Int32 MDBLenUnitID, Decimal NonMagSpaceAbove, Int32 NMSpAbvUnitID, Decimal NonMagSpaceBelow, Int32 NMSpBlwUnitID, Decimal MotorGaussmeterReading, Int32 MGValUnitID, Decimal GaussmeterReadingTop, Int32 GRTopUnitID, Decimal GaussmeterAtDrillString, Int32 GDSUnitID, Decimal GaussmeterReadingBottom, Int32 GRBtmUnitID, Int32 dmgstID, String dbConn)
        {
            try
            {
                Int32 resultBHA = -99, addBHAID = -99, bhaCount = -99;

                Int32 rID = -99;
                Decimal bhaMSize = -99.00M;
                Int32 MSizeU = -99;
                Decimal bhaDSCSize = -99.00M;
                Int32 DSCSizeU = -99;
                Decimal bhaMDBLen = -99.00M;
                Int32 MDBLenU = -99;
                Decimal bhaNMSpAbv = -99.00M;
                Int32 NMSpAbvU = -99;
                Decimal bhaNMSpBlw = -99.00M;
                Int32 NMSpBlwU = -99;
                Decimal MGVal = -99.00M;
                Int32 MGValUID = -99;
                Decimal DGTop = -99.00M;
                Int32 DGTopUID = -99;
                Decimal DSGVal = -99.00M;
                Int32 DSGValUID = -99;
                Decimal DGBtm = -99.00M;
                Int32 DGBtmUID = -99;
                Int32 bhaStat = -99;

                rID = selectedRUN;
                bhaMSize = MotorSize;
                MSizeU = MSizeUnitID;
                bhaDSCSize = DrillStringCollarSize;
                DSCSizeU = DSCSizeUnitID;
                bhaMDBLen = MotorDrillBitLength;
                MDBLenU = MDBLenUnitID;
                bhaNMSpAbv = NonMagSpaceAbove;
                NMSpAbvU = NMSpAbvUnitID;
                bhaNMSpBlw = NonMagSpaceBelow;
                NMSpBlwU = NMSpBlwUnitID;
                MGVal = MotorGaussmeterReading;
                MGValUID = MGValUnitID;
                DGTop = GaussmeterReadingTop;
                DGTopUID = GRTopUnitID;
                DSGVal = GaussmeterAtDrillString;
                DSGValUID = GDSUnitID;
                DGBtm = GaussmeterReadingBottom;
                DGBtmUID = GRBtmUnitID;
                bhaStat = dmgstID;

                //Finding if selected Run has an Active BHA and if found, setting it InActive so new BHA can be added and new Run started
                //activeBHA = findActiveBHA(rID, dbConn);
                //if (!activeBHA.Equals(-99))
                //{
                //    bhaSetInActive(activeBHA, dbConn);
                //}
                bhaCount = AST.getWPBHACount(rID, dbConn);
                if (bhaCount > 8)
                {
                    resultBHA = -8;
                }
                else
                {
                    using (SqlConnection clntConn = new SqlConnection(dbConn))
                    {
                        try
                        {
                            String insertWPBHA = "INSERT INTO WPBHA (runID, bhaMSize, bhaMSizeUID, bhaDSCSize, bhaDSCUID, bhaMDBLen, bhaMDBLenUID, bhaNMSpAbv, bhaNMSAUID, bhaNMSpBlw, bhaNMSBUID, dmgstID, cTime, uTime, bhaName) VALUES (@runID, @bhaMSize, @bhaMSizeUID, @bhaDSCSize, @bhaDSCUID, @bhaMDBLen, @bhaMDBLenUID, @bhaNMSpAbv, @bhaNMSAUID, @bhaNMSpBlw, @bhaNMSBUID, @dmgstID, @cTime, @uTime, @bhaName); SELECT SCOPE_IDENTITY()";
                            String insertWPBHADetail = "INSERT INTO WPBHADETAIL (bhaID, bhaMGVal, bhaMGValUID, bhaDGTop, bhaDGTUID, bhaDSGVal, bhaDSGUID, bhaDGBottom, bhaDGBUID, cTime, uTime) VALUES (@bhaID, @bhaMGVal, @bhaMGValUID, @bhaDGTop, @bhaDGTUID, @bhaDSGVal, @bhaDSGUID, @bhaDGBottom, @bhaDGBUID, @cTime, @uTime)";

                            clntConn.Open();

                            if (selectedClass.Equals(3))
                            {
                                SqlCommand insertWPBHACommand = new SqlCommand(insertWPBHA, clntConn);
                                insertWPBHACommand.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = rID.ToString();
                                insertWPBHACommand.Parameters.AddWithValue("@bhaMSize", SqlDbType.Decimal).Value = bhaMSize.ToString();
                                insertWPBHACommand.Parameters.AddWithValue("@bhaMSizeUID", SqlDbType.Int).Value = MSizeU.ToString();
                                insertWPBHACommand.Parameters.AddWithValue("@bhaDSCSize", SqlDbType.Decimal).Value = bhaDSCSize.ToString();
                                insertWPBHACommand.Parameters.AddWithValue("@bhaDSCUID", SqlDbType.Int).Value = DSCSizeU.ToString();
                                insertWPBHACommand.Parameters.AddWithValue("@bhaMDBLen", SqlDbType.Decimal).Value = bhaMDBLen.ToString();
                                insertWPBHACommand.Parameters.AddWithValue("@bhaMDBLenUID", SqlDbType.Int).Value = MDBLenU.ToString();
                                insertWPBHACommand.Parameters.AddWithValue("@bhaNMSpAbv", SqlDbType.Decimal).Value = bhaNMSpAbv.ToString();
                                insertWPBHACommand.Parameters.AddWithValue("@bhaNMSAUID", SqlDbType.Int).Value = NMSpAbvU.ToString();
                                insertWPBHACommand.Parameters.AddWithValue("@bhaNMSpBlw", SqlDbType.Decimal).Value = bhaNMSpBlw.ToString();
                                insertWPBHACommand.Parameters.AddWithValue("@bhaNMSBUID", SqlDbType.Int).Value = NMSpBlwU.ToString();
                                insertWPBHACommand.Parameters.AddWithValue("@dmgstID", SqlDbType.Int).Value = bhaStat.ToString();
                                insertWPBHACommand.Parameters.AddWithValue("@cTime", SqlDbType.DateTime).Value = DateTime.Now.ToString();
                                insertWPBHACommand.Parameters.AddWithValue("@uTime", SqlDbType.DateTime).Value = DateTime.Now.ToString();
                                insertWPBHACommand.Parameters.AddWithValue("@bhaName", SqlDbType.NVarChar).Value = BHASignatureName.ToString();

                                addBHAID = Convert.ToInt32(insertWPBHACommand.ExecuteScalar());
                                resultBHA = addBHAID;

                                SqlCommand insertWPBHADetailCommand = new SqlCommand(insertWPBHADetail, clntConn);
                                insertWPBHADetailCommand.Parameters.AddWithValue("@bhaID", SqlDbType.Int).Value = addBHAID.ToString();
                                insertWPBHADetailCommand.Parameters.AddWithValue("@bhaMGVal", SqlDbType.Decimal).Value = MGVal.ToString();
                                insertWPBHADetailCommand.Parameters.AddWithValue("@bhaMGValUID", SqlDbType.Int).Value = MGValUID.ToString();
                                insertWPBHADetailCommand.Parameters.AddWithValue("@bhaDGTop", SqlDbType.Decimal).Value = DGTop.ToString();
                                insertWPBHADetailCommand.Parameters.AddWithValue("@bhaDGTUID", SqlDbType.Int).Value = DGTopUID.ToString();
                                insertWPBHADetailCommand.Parameters.AddWithValue("@bhaDSGVal", SqlDbType.Decimal).Value = DSGVal.ToString();
                                insertWPBHADetailCommand.Parameters.AddWithValue("@bhaDSGUID", SqlDbType.Int).Value = DSGValUID.ToString();
                                insertWPBHADetailCommand.Parameters.AddWithValue("@bhaDGBottom", SqlDbType.Decimal).Value = DGBtm.ToString();
                                insertWPBHADetailCommand.Parameters.AddWithValue("@bhaDGBUID", SqlDbType.Int).Value = DGBtmUID.ToString();
                                insertWPBHADetailCommand.Parameters.AddWithValue("@cTime", SqlDbType.DateTime).Value = DateTime.Now.ToString();
                                insertWPBHADetailCommand.Parameters.AddWithValue("@uTime", SqlDbType.DateTime).Value = DateTime.Now.ToString();

                                resultBHA = Convert.ToInt32(insertWPBHADetailCommand.ExecuteNonQuery());
                            }

                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            clntConn.Close();
                            clntConn.Dispose();
                        }
                    }
                }
                return resultBHA;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Set Current Active BHA as InActive
        private static Int32 bhaSetInActive(Int32 bhaID, String dbConn)
        {
            try
            {
                Int32 result = -99;
                String chngStatCommand = "UPDATE [BHA] SET [dmgstID] = '5' WHERE [bhaID] = @bhaID";

                using (SqlConnection clntConn = new SqlConnection(dbConn))
                {
                    try
                    {
                        SqlCommand chngBHA = new SqlCommand(chngStatCommand, clntConn);
                        chngBHA.Parameters.AddWithValue("bhaID", SqlDbType.Int).Value = bhaID.ToString();

                        clntConn.Open();

                        result = Convert.ToInt32(chngBHA.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }

                return result;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 checkReferenceMagnetics(Int32 WellID, String dbConn)
        {
            try
            {
                Int32 result = -99, rfmgID = -99;
                String selectRefMags = "SELECT [rfmgID], [dmgstID] FROM [RefMags] WHERE [welID] = @welID AND [dmgstID] = @dmgstID";
                String updateRefMags = "UPDATE [RefMags] SET [dmgstID] = 5 WHERE [welID] = @welID"; //5 = InActive

                using (SqlConnection clntConn = new SqlConnection(dbConn))
                {
                    SqlCommand getRF = new SqlCommand(selectRefMags, clntConn);
                    getRF.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                    getRF.Parameters.AddWithValue("@dmgstID", SqlDbType.Int).Value = '1'.ToString();

                    clntConn.Open();
                    SqlDataReader readRF = getRF.ExecuteReader();
                    if (readRF.HasRows)
                    {
                        while (readRF.Read())
                        {
                            rfmgID = readRF.GetInt32(0);
                        }
                        readRF.Close();
                        readRF.Dispose();
                        SqlCommand updateRF = new SqlCommand(updateRefMags, clntConn);
                        updateRF.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        result = Convert.ToInt32(updateRF.ExecuteScalar());
                    }
                    else
                    {
                        result = 0;
                        readRF.Close();
                        readRF.Dispose();
                        return result;
                    }
                }

                return result;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 checkRunData(Int32 WellID, Int32 SectionID, Int32 RunID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(resID) FROM [RawQCResults] WHERE [runID] = @runID AND [wswID] = @wswID AND [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = SectionID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Find if an Active BHA exists for the selected RUN
        private static Int32 findActiveBHA(Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 WellPadID, Int32 ClientID, Int32 OperatorID, String ClientDBConnection)
        {
            try
            {
                Int32 actBHA = -99;
                String findBHA = "SELECT [bhaID] FROM [BHA] WHERE [runID] = @runID AND [wswID] = @wswID AND [welID] = @welID AND [wpdID] = @wpdID AND [clntID] = @clntID AND [optrID] = @optrID AND [dmgstID] = '1'";

                using (SqlConnection clntConn = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        SqlCommand getBHA = new SqlCommand(findBHA, clntConn);
                        getBHA.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        getBHA.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getBHA.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getBHA.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getBHA.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getBHA.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        clntConn.Open();

                        using (SqlDataReader readBHA = getBHA.ExecuteReader())
                        {
                            try
                            {
                                if (readBHA.HasRows)
                                {
                                    while (readBHA.Read())
                                    {
                                        actBHA = readBHA.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readBHA.Close();
                                readBHA.Dispose();
                            }
                        }
                        return actBHA;
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        internal static int findData(Int32 WellID, Int32 WellSectionID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                Dictionary<Int32, Int32> secList = getWellSectionForWellList(WellID, ClientDBString);
                foreach (Int32 value in secList.Values)
                {
                    List<Int32> runList = getRunForWellSectionList(WellID, value, ClientDBString);
                    foreach (Int32 rID in runList)
                    {
                        iReply = checkRunData(WellID, value, rID, ClientDBString);
                    }
                }

                if (iReply >= 1)
                {
                    iReply = 1;
                }
                else
                {
                    iReply = -1;
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getActiveRFCount(Int32 WellID, String ClientDBConnection) //Active Reference Magnetics Count
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(rfmgID) FROM RefMags WHERE [welID] = @welID AND [dmgstID] = 1";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                        if (iReply.Equals(0))
                                        {
                                            iReply = -99;
                                        }
                                    }
                                }
                                else
                                {
                                    iReply = -99;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal getAllowableSpace(Decimal perInc, Decimal perAzm)
        {
            try
            {
                if((Math.Sqrt(Math.Pow(0.25, 2) + Math.Pow(Math.Abs(0.6 * (Math.Sin(Units.ToRadians((Double)perInc)) * Math.Sin(Units.ToRadians((Double)perAzm)))), 2))) < 0.5 ) 
                {
                   return Convert.ToDecimal(0.5M);
                }
                else
                {
                    return Convert.ToDecimal(Math.Sqrt(Math.Pow(0.25, 2) + Math.Pow(Math.Abs(0.6 * (Math.Sin(Units.ToRadians((Double)perInc)) * Math.Sin(Units.ToRadians((Double)perAzm)))), 2)));
                }
                //return Convert.ToDecimal(0.5 + (Math.Abs(0.35 * (Math.Sin(Units.ToRadians((Double)perInc)) * Math.Sin(Units.ToRadians((Double)perAzm))))));
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static String getBHANameFromRunID(Int32 RunID, String ClientDBConnection)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [bhaName] FROM [BHA] WHERE [runID] = @runID";
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                                else
                                {
                                    iReply = "---";
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getBHAActiveRunList(Int32 WellID, String ClientDBConnection)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [runID], [runName] FROM [RUN] WHERE [welID] = @welID AND [dmgstID] = '1'";
                Int32 id = 0, nID = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using(SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if(readData.HasRows)
                                {
                                    while(readData.Read())
                                    {
                                        id =  readData.GetInt32(0);
                                        nID = readData.GetInt32(1);
                                        name = AST.getRunName(nID);
                                        iReply.Add(id, name);
                                    }
                                }
                                else
                                {
                                    iReply.Add(0, "No Active Run found.");
                                }
                            }
                            catch(Exception ex)
                            {throw new  System.Exception(ex.ToString());}
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 getNextRun(Int32 WellSectionID, String dbConn)
        {
            try
            {
                Int32 iReply = -99;
                String maxRunCommand = "Select MAX(runID) FROM [RunToWellSection] WHERE [wswID] = @wswID";
                using (SqlConnection clntConn = new SqlConnection(dbConn))
                {
                    try
                    {
                        clntConn.Open();
                        SqlCommand getRun = new SqlCommand(maxRunCommand, clntConn);
                        getRun.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        using (SqlDataReader readData = getRun.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    iReply = 1;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                        }
                        return iReply;
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getIFR(Int32 WellID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        String selData = "SELECT [ifrvalID] FROM [RefMags] WHERE [welID] = @welID";
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getReferenceMagneticsTable(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, String ClientDBConnection)
        {
            try
            {
                DataTable iReply = new DataTable();                
                DataColumn rfmgID = iReply.Columns.Add("rfmgID", typeof(Int32));
                DataColumn ifrID = iReply.Columns.Add("ifrID", typeof(String));
                DataColumn mdec = iReply.Columns.Add("mdec", typeof(String));
                DataColumn dip = iReply.Columns.Add("dip", typeof(Decimal));
                DataColumn bTotal = iReply.Columns.Add("bTotal", typeof(String));
                DataColumn gTotal = iReply.Columns.Add("gTotal", typeof(String));
                DataColumn jConverg = iReply.Columns.Add("jcID", typeof(String));
                DataColumn gconv = iReply.Columns.Add("gconv", typeof(String));                
                iReply.PrimaryKey = new DataColumn[] { rfmgID };
                Int32 id = -99, mdecCompID = -99, gcoID = -99, ifrValID = -99, magUID = -99, acelUID = -99, jcID = -99;
                Decimal mdecVal = -99.00M, dipVal = -99.00M, bTVal = -99.00M, gTVal = -99.00M, gcVal = -99;
                DateTime uT = DateTime.Now;
                String ifrName = String.Empty, mdecValue = String.Empty, mdecCompass = String.Empty, btotal = String.Empty, gtotal = String.Empty, gcCompass = String.Empty, gcValue = String.Empty, magUnit = String.Empty, acelUnit = String.Empty, jobConvg = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        String selData = "SELECT [rfmgID], [ifrvalID], [mdec], [cID], [dip], [bTotal], [muID], [gTotal], [acuID], [gconv], [gcoID], [jcID] FROM [RefMags] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID AND [welID] = @welID AND [dmgstID] = '1'";
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        ifrValID = readData.GetInt32(1);
                                        ifrName = AST.getIFRValue(ifrValID);
                                        mdecVal = readData.GetDecimal(2);
                                        mdecCompID = readData.GetInt32(3);
                                        mdecCompass = DMG.getCardinalName(mdecCompID);
                                        mdecValue = String.Format("{0} - ({1})", mdecVal, mdecCompass);
                                        dipVal = readData.GetDecimal(4);
                                        bTVal = readData.GetDecimal(5);
                                        magUID = readData.GetInt32(6);
                                        magUnit = AST.getMagUnitName(magUID);
                                        btotal = String.Format("{0} - ({1})", bTVal, magUnit);
                                        gTVal = readData.GetDecimal(7);
                                        acelUID = readData.GetInt32(8);
                                        acelUnit = AST.getAccelUnitName(acelUID);
                                        gtotal = String.Format("{0} - ({1})", gTVal, acelUnit);
                                        gcVal = readData.GetDecimal(9);
                                        gcoID = readData.GetInt32(10);
                                        gcCompass = DMG.getGCOrientationName(gcoID);
                                        gcValue = String.Format("{0} - ({1})", gcVal, gcCompass);
                                        jcID = readData.GetInt32(11);
                                        jobConvg = AST.getConvergenceName(jcID);
                                        iReply.Rows.Add(id, ifrName, mdecValue, dipVal, btotal, gtotal, jobConvg, gcValue);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static ArrayList getRefMagList(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, String ClientDBString)
        {
            try
            {
                ArrayList iReply = new ArrayList();
                Int32 ifrValID = -99, mdecCID = -99, muID = -99, acuID = -99, gcoID = -99, jcID = -99;
                Decimal mDec = -99.00M, dip = -99.00M, bTotal = -99.000000M, gTotal = -99.000000M, gconv = -99.00M;
                String selRM = "SELECT [ifrvalID], [mdec], [cID], [dip], [bTotal], [muID], [gTotal], [acuID], [gconv], [gcoID], [jcID] FROM [RefMags] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID AND [welID] = @welID AND [dmgstID] = '1'";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    dataCon.Open();
                    SqlCommand getRM = new SqlCommand(selRM, dataCon);
                    getRM.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                    getRM.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                    getRM.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                    getRM.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                    using (SqlDataReader readRM = getRM.ExecuteReader())
                    {
                        try
                        {
                            if (readRM.HasRows)
                            {
                                while (readRM.Read())
                                {
                                    ifrValID = readRM.GetInt32(0);                                    
                                    iReply.Add(ifrValID);//Item 0
                                    mDec = readRM.GetDecimal(1);
                                    iReply.Add(mDec);//Item 1
                                    mdecCID = readRM.GetInt32(2);
                                    iReply.Add(mdecCID);//Item 2
                                    dip = readRM.GetDecimal(3);
                                    iReply.Add(dip);//Item 3
                                    bTotal = readRM.GetDecimal(4);
                                    iReply.Add(bTotal);//Item 4
                                    muID = readRM.GetInt32(5);
                                    iReply.Add(muID);//Item 5
                                    gTotal = readRM.GetDecimal(6);
                                    iReply.Add(gTotal);//Item 6
                                    acuID = readRM.GetInt32(7);
                                    iReply.Add(acuID);//Item 7
                                    gconv = readRM.GetDecimal(8);
                                    iReply.Add(gconv);//Item 8
                                    gcoID = readRM.GetInt32(9);
                                    iReply.Add(gcoID);//Item 9
                                    jcID = readRM.GetInt32(10);
                                    iReply.Add(jcID);//Item 10
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            readRM.Close();
                            readRM.Dispose();
                        }
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getRunList(Int32 WellID, Int32 WellSectionID, String ClientDBString)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99, rId = -99, stId = -99;
                String name = String.Empty, status = String.Empty;
                String selData = "SELECT [runID], [runName], [dmgstID] FROM [Run] WHERE [wswID] = @wswID AND [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        rId = readData.GetInt32(1);
                                        stId = readData.GetInt32(2);
                                        name = AST.getRunNameValue(rId);
                                        status = DMG.getStatusValue(stId);
                                        if (status.Equals("Active"))
                                        {
                                            name = String.Format("{0} --- {1}", name, status);
                                        }
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getRunCountForWellSection(Int32 WellID, Int32 WellSectionID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(runID) FROM [Run] WHERE [wswID] = @wswID AND [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    iReply = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static List<Int32> getRunForWellSectionList(Int32 WellID, Int32 WellSectionID, String dbConn)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                String selData = "SELECT [runID] FROM [Run] WHERE [wswID] = @wswID AND [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply.Add(readData.GetInt32(0));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal getRunMinimumMeasuredDepth(Int32 RunID, String ClientDBString)
        {
            try
            {
                Decimal iReply = -99.00M;
                String selData = "SELECT MIN(Depth) FROM [RawQCResults] WHERE [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetDecimal(0);
                                        if (iReply.Equals(null))
                                        {
                                            iReply = -99.00M;
                                        }
                                    }
                                }
                                else
                                {
                                    iReply = 0.00M;
                                }                                
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal getRunMaximumMeasuredDepth(Int32 RunID, String ClientDBString)
        {
            try
            {
                Decimal iReply = -99.00M;
                String selData = "SELECT MAX(Depth) FROM [RawQCResults] WHERE [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetDecimal(0);
                                        if (iReply.Equals(null))
                                        {
                                            iReply = -99.00M;
                                        }
                                    }
                                }
                                else
                                {
                                    iReply = 0.00M;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getRunName(Int32 runID)
        {
            try
            {
                String rName = String.Empty;
                String selectCommand = "SELECT [rnamValue] FROM [dmgRunName] WHERE [rnamID] = @rnamID";
                String dbConn = WebConfigurationManager.ConnectionStrings["FEDemogDB"].ConnectionString;

                using (SqlConnection clntConn = new SqlConnection(dbConn))
                {
                    try
                    {
                        clntConn.Open();
                        SqlCommand nameValue = new SqlCommand(selectCommand, clntConn);
                        nameValue.Parameters.AddWithValue("@rnamID", SqlDbType.Int).Value = runID.ToString();

                        using (SqlDataReader readVal = nameValue.ExecuteReader())
                        {
                            try
                            {
                                if (readVal.HasRows)
                                {
                                    while (readVal.Read())
                                    {
                                        rName = readVal.GetString(0);
                                    }
                                    return rName;
                                }
                                else
                                {
                                    return rName;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readVal.Close();
                                readVal.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static Int32 getRunStatus(Int32 RunID, String dbConn)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT [dmgstID] FROM [RUN] WHERE [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    iReply = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getRunForSelectedWellSectionTable(Int32 WellID, Int32 WellSectionID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn runID = iReply.Columns.Add("runID", typeof(Int32));
                DataColumn rnName = iReply.Columns.Add("rnName", typeof(String));
                DataColumn rnStatus = iReply.Columns.Add("rnStatus", typeof(String));
                DataColumn rnBHA = iReply.Columns.Add("rnBHA", typeof(String));
                DataColumn rnToolcode = iReply.Columns.Add("rnToolcode", typeof(String));
                DataColumn rnSQC = iReply.Columns.Add("rnSQC", typeof(String));
                DataColumn rnStartD = iReply.Columns.Add("rnStartD", typeof(Decimal));
                DataColumn rnEndD = iReply.Columns.Add("rnEndD", typeof(Decimal));
                List<Int32> runList = new List<Int32>();
                Int32 rID = -99, rnID = -99, tc = -99, dmg = -99, sqc = -99, runData = 0;
                Decimal startD = -99.00M, endD = -99.00M;
                String name = String.Empty, bhaName = String.Empty, toolC = String.Empty, status = String.Empty, sqcName = String.Empty;
                runList = getRunForWellSectionList(WellID, WellSectionID, ClientDBString);
                String selData = "SELECT [runID], [runName], [dmgstID], [tcID], [sqcID] FROM [Run] WHERE [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        foreach (Int32 id in runList)
                        {
                            SqlCommand getInfo = new SqlCommand(selData, dataCon);
                            getInfo.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = id.ToString();                            
                            using (SqlDataReader readInfo = getInfo.ExecuteReader())
                            {
                                if (readInfo.HasRows)
                                {
                                    while (readInfo.Read())
                                    {
                                        rID = readInfo.GetInt32(0);
                                        rnID = readInfo.GetInt32(1);
                                        name = AST.getRunNameValue(rnID);
                                        dmg = readInfo.GetInt32(2);
                                        status = DMG.getStatusValue(dmg);
                                        bhaName = getBHANameFromRunID(rID, ClientDBString);
                                        tc = readInfo.GetInt32(3);
                                        toolC = DMG.getToolCodeName(tc);
                                        sqc = readInfo.GetInt32(4);
                                        sqcName = getSQCName(sqc, ClientDBString);
                                        runData = checkRunData(WellID, WellSectionID, rID, ClientDBString);
                                        if (runData >= 1)
                                        {
                                            startD = getRunMinimumMeasuredDepth(rID, ClientDBString);
                                            endD = getRunMaximumMeasuredDepth(rID, ClientDBString);
                                        }
                                        else
                                        {
                                            startD = -99.00M;
                                            endD = -99.00M;
                                        }
                                        iReply.Rows.Add(rID, name, status, bhaName, toolC, sqcName, startD, endD);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getRunForWellSectionDictionaryList(Int32 OperatorID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, String ClientDBString)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 rID = -99, rnID = -99;
                String name = String.Empty;
                String selRInfo = "SELECT [runID], [runName] FROM [Run] WHERE [optrID] = @optrID AND [wpdID] = @wpdID AND [welID] = @welID AND [wswID] = @wswID";
                using (SqlConnection runCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        runCon.Open();
                        SqlCommand getInfo = new SqlCommand(selRInfo, runCon);
                        getInfo.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getInfo.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getInfo.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getInfo.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        using (SqlDataReader readInfo = getInfo.ExecuteReader())
                        {
                            if (readInfo.HasRows)
                            {
                                while (readInfo.Read())
                                {
                                    rID = readInfo.GetInt32(0);
                                    rnID = readInfo.GetInt32(1);
                                    name = AST.getRunNameValue(rnID);
                                    iReply.Add(rID, name);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        runCon.Close();
                        runCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getRunForWellSectionTable(Int32 OperatorID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn runID = iReply.Columns.Add("runID", typeof(Int32));
                DataColumn runName = iReply.Columns.Add("runName", typeof(String));
                DataColumn Inc = iReply.Columns.Add("Inc", typeof(Decimal));
                DataColumn Azm = iReply.Columns.Add("Azm", typeof(Decimal));
                DataColumn TInc = iReply.Columns.Add("TInc", typeof(Decimal));
                DataColumn TAzm = iReply.Columns.Add("TAzm", typeof(Decimal));
                DataColumn stat = iReply.Columns.Add("stat", typeof(String));
                DataColumn tcID = iReply.Columns.Add("tcID", typeof(String));
                DataColumn sqcID = iReply.Columns.Add("sqcID", typeof(String));
                DataColumn sDepth = iReply.Columns.Add("sDepth", typeof(String));
                DataColumn eDepth = iReply.Columns.Add("eDepth", typeof(String));
                List<Int32> runList = new List<Int32>();
                List<Int32> irunList = new List<Int32>();
                Int32 rID = -99, rnID = -99, tc = -99, dmg = -99, sqc = -99, lnU = -99;
                Decimal inc = -99.00M, azm = -99.00M, tinc = -99.00M, tazm = -99.00M, sD = -99.00M, eD = -99.00M;
                String name = String.Empty, toolC = String.Empty, status = String.Empty, sqcName = String.Empty, startDepth = String.Empty, endDepth = String.Empty, lengthU = String.Empty;
                DateTime uT = DateTime.Now;
                String selRInfo = "SELECT [runID], [runName], [runInc], [runAzm], [runTInc], [runTAzm], [startDepth], [endDepth], [lenU], [dmgstID], [tcID], [sqcID] FROM [Run] WHERE [optrID] = @optrID AND [wpdID] = @wpdID AND [welID] = @welID AND [wswID] = @wswID";
                using (SqlConnection runCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        runCon.Open();
                        SqlCommand getInfo = new SqlCommand(selRInfo, runCon);
                        getInfo.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getInfo.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getInfo.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getInfo.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();                        
                        using (SqlDataReader readInfo = getInfo.ExecuteReader())
                        {
                            if (readInfo.HasRows)
                            {
                                while (readInfo.Read())
                                {
                                    rID = readInfo.GetInt32(0);
                                    rnID = readInfo.GetInt32(1);
                                    name = AST.getRunNameValue(rnID);
                                    inc = readInfo.GetDecimal(2);
                                    azm = readInfo.GetDecimal(3);
                                    tinc = readInfo.GetDecimal(4);
                                    tazm = readInfo.GetDecimal(5);
                                    sD = readInfo.GetDecimal(6);
                                    eD = readInfo.GetDecimal(7);
                                    lnU = readInfo.GetInt32(8);
                                    lengthU = AST.getLengthUnitName(lnU);
                                    startDepth = String.Format("{0} ( {1} )", sD, lengthU);
                                    endDepth = String.Format("{0} ( {1} )", eD, lengthU);
                                    dmg = readInfo.GetInt32(9);
                                    status = DMG.getStatusValue(dmg);
                                    tc = readInfo.GetInt32(10);
                                    toolC = DMG.getToolCodeName(tc);
                                    sqc = readInfo.GetInt32(11);
                                    sqcName = getSQCName(sqc, ClientDBString);
                                    iReply.Rows.Add(rID, name, inc, azm, tinc, tazm, status, toolC, sqcName, startDepth, endDepth);
                                    iReply.AcceptChanges();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        runCon.Close();
                        runCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static List<Int32> getRunIDForSection(Int32 WellSectionID, Int32 WellID, String dbConn)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                String selData = "SELECT [runID] FROM [WellSectionToWell] WHERE [wswID] = @wswID AND [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply.Add(readData.GetInt32(0));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }    

        public static String getWellNameFromID(Int32 WellID, String ClientDBString)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [welName] FROM [Well] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getWellSectionToWellIDForWell(Int32 WellSectionID, Int32 WellID, String dbConn)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT [wswID] FROM [WellSectionToWell] WHERE [wsID] = @wsID AND [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(dbConn.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wsID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    iReply = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, Int32> getWellSectionForWellList(Int32 WellID, String dbConn)
        {
            try
            {
                Dictionary<Int32, Int32> iReply = new Dictionary<Int32, Int32>();
                Int32 id = -99, wsID = -99;
                String selData = "SELECT [wswID], [wsID] FROM [WellSectionToWell] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        wsID = readData.GetInt32(1);
                                        iReply.Add(id, wsID);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWellSectionToWellTable(Int32 WellID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wswID = iReply.Columns.Add("wswID", typeof(Int32));
                DataColumn wsName = iReply.Columns.Add("wsName", typeof(String));
                DataColumn rnCount = iReply.Columns.Add("rnCount", typeof(String));
                Int32 id = -99, count = -99;
                String name = String.Empty;
                String selData = "SELECT [wsID] FROM [WellSectionToWell] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = getWellSectionNameFromID(id, ClientDBConnection);
                                        count = getRunCountForWellSection(WellID, id, ClientDBConnection);
                                        iReply.Rows.Add(id, name, Convert.ToString(count));
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }

                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getWellSectionToWellList(Int32 WellID, String ClientDBConnection)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [wsID] FROM [WellSectionToWell] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = getWellSectionNameFromID(id, ClientDBConnection);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }

                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getSQCName(Int32 sqcID, String ClientDBString)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [sqcName] FROM [SQC] WHERE [sqcID] = @sqcID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@sqcID", SqlDbType.Int).Value = sqcID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                                else
                                {
                                    iReply = "---";
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getWellSectionNameFromID(Int32 WellSectionID, String ClientDBString)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [wsName] FROM [WellSection] WHERE [wsID] = @wsID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wsID", SqlDbType.Int).Value = WellSectionID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    iReply = readData.GetString(0);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getRunToWellSectionList(Int32 WellSectionID, String ClientDBConnection)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                String selData = "SELECT [runID] FROM [RunToWellSection] WHERE [wswID] = @wswID";
                using(SqlConnection dataCon = new SqlConnection(ClientDBConnection.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply.Add(readData.GetInt32(0));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static System.Data.DataTable getClientWellPadTable(Int32 ClientID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new DataTable();
                DataColumn wpdID = iReply.Columns.Add("wpdID", typeof(Int32));
                DataColumn welPadName = iReply.Columns.Add("welPadName", typeof(String));
                DataColumn fld = iReply.Columns.Add("fldID", typeof(String));
                DataColumn cntyID = iReply.Columns.Add("cntyID", typeof(String));
                DataColumn stID = iReply.Columns.Add("stID", typeof(String));
                DataColumn welCount = iReply.Columns.Add("welCount", typeof(Int32));
                DataColumn swlCount = iReply.Columns.Add("swlCount", typeof(Int32));
                String selData = "SELECT [wpdID], [welPadName], [fldID], [cntyID], [stID] FROM [WellPad] WHERE [clntID] = @clntID";
                Int32 id = 0, fID = 0, cID = -99, sID = -99, gwd = -99, swd = -99;
                String wName = String.Empty, field = String.Empty, county = String.Empty, state = String.Empty;                
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        wName = readData.GetString(1);
                                        fID = readData.GetInt32(2);
                                        field = DMG.getFieldName(fID);
                                        cID = readData.GetInt32(3);
                                        county = DMG.getCountyName(cID);
                                        sID = readData.GetInt32(4);
                                        state = DMG.getStateName(sID);
                                        gwd = AST.getGlobalWellCountByWellPadAndField(id, fID);
                                        swd = AST.getSiloWellCountByWellPadAndField(id, fID, ClientID, ClientDBConnection);
                                        iReply.Rows.Add(id, wName, field, county, state, gwd, swd);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32,String> getWellSectionsList(Int32 WellID, String ClientDBString)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99, secCount = -99;
                String name = String.Empty;
                String selData = "SELECT [wsID], [wsName] FROM [WellSection] ORDER BY [wsName]";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        secCount = getRunCountForWellSection(WellID, id, ClientDBString);
                                        name = readData.GetString(1);
                                        if (secCount > 0)
                                        {
                                            name = String.Format("{0} --- {1} Run(s)", name, secCount);
                                        }
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWellSectionTable(Int32 WellID, Int32 WellSectionID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 getWPRun(Int32 WellID, Int32 WellSectionID, String dbConn)
        {
            try
            {
                Int32 iReply = -99, maxID = 0;
                List<Int32> runList = new List<Int32>();
                String selRun = "SELECT [runID] FROM [RunSectionToRun] WHERE [rnsID] = @rnsID AND [welID] = @welID";
                String selWRun = "SELECT ISNULL(MAX(runName), 0) + 1 FROM [WPRun] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getRun = new SqlCommand(selRun, dataCon);
                        getRun.Parameters.AddWithValue("@rnsID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getRun.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readRun = getRun.ExecuteReader())
                        {
                            try
                            {
                                if (readRun.HasRows)
                                {
                                    while (readRun.Read())
                                    {
                                        runList.Add(readRun.GetInt32(0));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readRun.Close();
                                readRun.Dispose();
                            }
                        }
                        foreach (var id in runList)
                        {
                            SqlCommand getWRun = new SqlCommand(selWRun, dataCon);
                            getWRun.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                            using (SqlDataReader readData = getWRun.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            maxID = readData.GetInt32(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply = maxID + 1;
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 insertRun(Int32 RunID, Decimal Inclination, Decimal Azimuth, Decimal TargetInclination, Decimal TargetAzimuth, Decimal StartDepth, Decimal EndDepth, Int32 LengthUnit, Int32 OperatorID, Int32 WellpadID, Int32 WellID, Int32 WellSectionID, Int32 DemogStatusID, Int32 ToolsCodeID, Int32 SurveyQualificationCriteria, String UserName, String UserDomain, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String insertCommand = "INSERT INTO [Run] ([runName], [runInc], [runAzm], [runTInc], [runTAzm], [startDepth], [endDepth], [lenU], [optrID], [wpdID], [welID], [wswID], [dmgstID], [tcID], [sqcID], [cTime], [uTime], [username], [realm]) VALUES (@runName, @runInc, @runAzm, @runTInc, @runTAzm, @startDepth, @endDepth, @lenU, @optrID, @wpdID, @welID, @wswID, @dmgstID, @tcID, @sqcID, @cTime, @uTime, @username, @realm); SELECT SCOPE_IDENTITY()";
                
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insertRUN = new SqlCommand(insertCommand, dataCon);
                        insertRUN.Parameters.AddWithValue("@runName", SqlDbType.Int).Value = RunID.ToString();
                        insertRUN.Parameters.AddWithValue("@runInc", SqlDbType.Decimal).Value = Inclination.ToString();
                        insertRUN.Parameters.AddWithValue("@runAzm", SqlDbType.Decimal).Value = Azimuth.ToString();
                        insertRUN.Parameters.AddWithValue("@runTInc", SqlDbType.Decimal).Value = TargetInclination.ToString();
                        insertRUN.Parameters.AddWithValue("@runTAzm", SqlDbType.Decimal).Value = TargetAzimuth.ToString();
                        insertRUN.Parameters.AddWithValue("@startDepth", SqlDbType.Decimal).Value = StartDepth.ToString();
                        insertRUN.Parameters.AddWithValue("@endDepth", SqlDbType.Decimal).Value = EndDepth.ToString();
                        insertRUN.Parameters.AddWithValue("@lenU", SqlDbType.Decimal).Value = LengthUnit.ToString();
                        insertRUN.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        insertRUN.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellpadID.ToString();                        
                        insertRUN.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        insertRUN.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        insertRUN.Parameters.AddWithValue("@dmgstID", SqlDbType.Int).Value = DemogStatusID.ToString();
                        insertRUN.Parameters.AddWithValue("@tcID", SqlDbType.Int).Value = ToolsCodeID.ToString();
                        insertRUN.Parameters.AddWithValue("@sqcID", SqlDbType.Int).Value = SurveyQualificationCriteria.ToString();
                        insertRUN.Parameters.AddWithValue("@cTime", SqlDbType.DateTime).Value = DateTime.Now.ToString();
                        insertRUN.Parameters.AddWithValue("@uTime", SqlDbType.DateTime).Value = DateTime.Now.ToString();
                        insertRUN.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insertRUN.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserDomain.ToString();                        

                        iReply = Convert.ToInt32(insertRUN.ExecuteScalar());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        private static Int32 runFolder(String RunName, Int32 RunID, Int32 WellID, String WellSection, String ClientRealm, String ClientDBConnection)
        {
            try
            {
                Int32 result = -99, opID = -99;
                String oprName = null;

                //Get Folder Paths from configuration
                String genDirectory = System.Web.Configuration.WebConfigurationManager.AppSettings["GenPath"].ToString();
                String uplDirectory = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadPath"].ToString();

                //Get OperatorID for selected Well
                using (SqlConnection clntConn = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        clntConn.Open();

                        String selOPID = "SELECT [optrID] FROM [Well] WHERE [welID] = @welID";
                        SqlCommand getOPID = new SqlCommand(selOPID, clntConn);
                        getOPID.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();

                        using (SqlDataReader readOPID = getOPID.ExecuteReader())
                        {
                            try
                            {
                                if (readOPID.HasRows)
                                {
                                    while (readOPID.Read())
                                    {
                                        opID = readOPID.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readOPID.Close();
                                readOPID.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }

                //Get Operator Name for selected Well
                String OwnerConn = WebConfigurationManager.ConnectionStrings["FEUsersDB"].ConnectionString;
                using (SqlConnection nameConn = new SqlConnection(OwnerConn))
                {
                    try
                    {
                        nameConn.Open();

                        String selOPName = "SELECT clntName FROM Client WHERE clntID = @clntID";
                        SqlCommand getOPName = new SqlCommand(selOPName, nameConn);
                        getOPName.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = opID.ToString();

                        using (SqlDataReader readOPName = getOPName.ExecuteReader())
                        {
                            try
                            {
                                if (readOPName.HasRows)
                                {
                                    while (readOPName.Read())
                                    {
                                        oprName = readOPName.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readOPName.Close();
                                readOPName.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        nameConn.Close();
                        nameConn.Dispose();
                    }
                }

                //Creating Well Folder and saving Permit file to it
                String clntDomain = null;
                String wlName = String.Concat("Well_", WellID);
                clntDomain = Convert.ToString(ClientRealm);
                TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                clntDomain = ti.ToTitleCase(clntDomain);
                oprName = ti.ToTitleCase(oprName);
                String rGFolderRaw = null; //Run Generated Folder for Raw QC
                String rGFolderRes = null; //Run Generated Folder for ResQC
                String rUFolderRaw = null; //Run Uploaded Documents Folder for RawQC
                String rUFolderRes = null; //Run Uploaded Documents Folder for ResQC
                rGFolderRaw = String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\{7}\\{8}\\", genDirectory, clntDomain, "Assets", "Wells", oprName, wlName, "RawDataQC", WellSection, RunName);
                rGFolderRes = String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\{7}\\{8}\\", genDirectory, clntDomain, "Assets", "Wells", oprName, wlName, "ResultsQC", WellSection, RunName);
                rUFolderRaw = String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\{7}\\{8}\\", uplDirectory, clntDomain, "Assets", "Wells", oprName, wlName, "RawDataQC", WellSection, RunName);
                rUFolderRes = String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\{7}\\{8}\\", uplDirectory, clntDomain, "Assets", "Wells", oprName, wlName, "ResultsQC", WellSection, RunName);
                DirectoryInfo runGenFolderRaw = new DirectoryInfo(rGFolderRaw);
                DirectoryInfo runGenFolderRes = new DirectoryInfo(rGFolderRes);
                DirectoryInfo runUplFolderRaw = new DirectoryInfo(rUFolderRaw);
                DirectoryInfo runUplFolderRes = new DirectoryInfo(rUFolderRes);

                if ((!Directory.Exists(rGFolderRaw)) || (!Directory.Exists(rGFolderRes)) || (!Directory.Exists(rUFolderRaw)) || (!Directory.Exists(rUFolderRes)))
                {
                    runGenFolderRaw.Create(); //Create Directory Folder
                    runGenFolderRes.Create();
                    runUplFolderRaw.Create();
                    runUplFolderRes.Create();
                }

                if ((Directory.Exists(rGFolderRaw)) || (Directory.Exists(rGFolderRes)) || (Directory.Exists(rUFolderRaw)) || (Directory.Exists(rUFolderRes))) //Checking if Folder exists then confirming to calling method
                {
                    result = 1;
                    return result;
                }
                else
                {
                    result = -1;
                    return result;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        //Reading Run Info(T-Inc and T-Azm)
        private static Tuple<Decimal, Decimal> runInfo(Int32 rID, String dbConn)
        {
            try
            {
                Decimal val1 = -99.00M;
                Decimal val2 = -99.00M;
                String getRunInfo = "SELECT runTInc, runTAzm FROM RUN WHERE runID = @runID";

                using (SqlConnection clntConn = new SqlConnection(dbConn))
                {
                    try
                    {
                        SqlCommand getRInfo = new SqlCommand(getRunInfo, clntConn);
                        getRInfo.Parameters.AddWithValue("runID", SqlDbType.Int).Value = rID.ToString();

                        clntConn.Open();

                        using (SqlDataReader readRInfo = getRInfo.ExecuteReader())
                        {
                            try
                            {
                                if (readRInfo.HasRows)
                                {
                                    while (readRInfo.Read())
                                    {
                                        val1 = readRInfo.GetDecimal(0);
                                        val2 = readRInfo.GetDecimal(1);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readRInfo.Close();
                                readRInfo.Dispose();
                            }
                        }

                        return new Tuple<Decimal, Decimal>(val1, val2);
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 setRunComplete(Int32 RunID, String dbConn)
        {
            try
            {
                Int32 result = -99;

                String updateCommand = "UPDATE [RUN] SET [dmgstID] = '2' WHERE [runID] = @runID";
                using (SqlConnection clntConn = new SqlConnection(dbConn))
                {
                    try
                    {
                        clntConn.Open();
                        SqlCommand updRunStat = new SqlCommand(updateCommand, clntConn);
                        updRunStat.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();

                        result = updRunStat.ExecuteNonQuery();
                        return result;
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 updateRunStatus(Int32 RunID, Int32 RunStatusID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String updData = "Update [Run] SET [dmgstID] = @dmgstID WHERE [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(updData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        getData.Parameters.AddWithValue("@dmgstID", SqlDbType.Int).Value = RunStatusID.ToString();
                        iReply = Convert.ToInt32(getData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable WellSectionNameToWellTable(Int32 WellID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wswID = iReply.Columns.Add("wswID", typeof(Int32));
                DataColumn wsName = iReply.Columns.Add("wsName", typeof(String));
                DataColumn rnCount = iReply.Columns.Add("rnCount", typeof(String));
                iReply.Rows.Add(-2, "Complete Well", "All");
                iReply.AcceptChanges();
                Int32 id = -99, count = -99;
                String name = String.Empty;
                String selData = "SELECT [wsID] FROM [WellSectionToWell] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = getWellSectionNameFromID(id, ClientDBConnection);
                                        count = getRunCountForWellSection(WellID, id, ClientDBConnection);
                                        iReply.Rows.Add(id, name, Convert.ToString(count));
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }

                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> WellSectionNameToWellList(Int32 WellID, String ClientDBConnection)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99, count = -99;
                String name = String.Empty;
                String selData = "SELECT [wsID] FROM [WellSectionToWell] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = getWellSectionNameFromID(id, ClientDBConnection);
                                        count = getRunCountForWellSection(WellID, id, ClientDBConnection);
                                        name = String.Format("{0} --- ( {1} )", name, count);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }

                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 wpRunFolder(String rName, Int32 rnsID, Int32 runID, Int32 welID, String ClientRealm, String dbConn)
        {
            try
            {
                Int32 result = -99, opID = -99;
                String oprName = String.Empty, rnsName = String.Empty;

                //Get Folder Paths from configuration
                String genDirectory = System.Web.Configuration.WebConfigurationManager.AppSettings["GenPath"].ToString();
                String uplDirectory = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadPath"].ToString();

                //Get OperatorID for selected Well
                using (SqlConnection clntConn = new SqlConnection(dbConn))
                {
                    try
                    {
                        clntConn.Open();

                        String selOPID = "SELECT [optrID] FROM [Well] WHERE [welID] = @welID";
                        SqlCommand getOPID = new SqlCommand(selOPID, clntConn);
                        getOPID.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = welID.ToString();

                        using (SqlDataReader readOPID = getOPID.ExecuteReader())
                        {
                            try
                            {
                                if (readOPID.HasRows)
                                {
                                    while (readOPID.Read())
                                    {
                                        opID = readOPID.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readOPID.Close();
                                readOPID.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }

                //Get Operator Name for selected Well
                String OwnerConn = WebConfigurationManager.ConnectionStrings["FEUsersDB"].ConnectionString;
                using (SqlConnection nameConn = new SqlConnection(OwnerConn))
                {
                    try
                    {
                        nameConn.Open();

                        String selOPName = "SELECT clntName FROM Client WHERE clntID = @clntID";
                        SqlCommand getOPName = new SqlCommand(selOPName, nameConn);
                        getOPName.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = opID.ToString();

                        using (SqlDataReader readOPName = getOPName.ExecuteReader())
                        {
                            try
                            {
                                if (readOPName.HasRows)
                                {
                                    while (readOPName.Read())
                                    {
                                        oprName = readOPName.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readOPName.Close();
                                readOPName.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        nameConn.Close();
                        nameConn.Dispose();
                    }
                }

                rnsName = DMG.getRunSectionName(rnsID);

                //Creating Well Folder and saving Permit file to it
                String clntDomain = null;
                String wlName = String.Concat("Well_", welID);
                clntDomain = Convert.ToString(ClientRealm);
                TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                clntDomain = ti.ToTitleCase(clntDomain);
                oprName = ti.ToTitleCase(oprName);
                String rGFolderRes = null; //Run Generated Folder for ResQC
                String rUFolderRes = null; //Run Uploaded Documents Folder for ResQC
                rGFolderRes = String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\{7}\\{8}\\", genDirectory, clntDomain, "Assets", "Wells", oprName, wlName, "WellProfile", rnsName, rName);
                rUFolderRes = String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\{7}\\{8}\\", uplDirectory, clntDomain, "Assets", "Wells", oprName, wlName, "WellProfile", rnsName, rName);
                DirectoryInfo runGenFolderRes = new DirectoryInfo(rGFolderRes);
                DirectoryInfo runUplFolderRes = new DirectoryInfo(rUFolderRes);

                if ((!Directory.Exists(rGFolderRes)) || (!Directory.Exists(rUFolderRes)))
                {
                    runGenFolderRes.Create();
                    runUplFolderRes.Create();
                }

                if ((Directory.Exists(rGFolderRes)) || (Directory.Exists(rUFolderRes))) //Checking if Folder exists then confirming to calling method
                {
                    result = 1;
                    return result;
                }
                else
                {
                    result = -1;
                    return result;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        
    }
}