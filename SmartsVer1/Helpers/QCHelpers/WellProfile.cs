﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Collections.Generic;
using WPBG = SmartsVer1.Helpers.QCHelpers.WPBGs;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using ACC = SmartsVer1.Helpers.QCHelpers.AccountingQC;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using QC = SmartsVer1.QCHelpers.sdRawDataQC;

namespace SmartsVer1.Helpers.QCHelpers
{
    public class WellProfile
    {
        ArrayList Headlist = new ArrayList();
        ArrayList DetailList = new ArrayList();
        String[] det, headerTOP;
        Dictionary<String, String> HeaderInfo = new Dictionary<String, String>();
        Dictionary<String, Int32> HeaderGB = new Dictionary<String, Int32>();

        static void Main()
        { }

        //Method to Add Dataset name in DB
        public static Int32 addDSName(Int32 JobID, Int32 WellID, Int32 RunSectionID, Int32 RunID, Int32 SQCID, String datasetName, String dbConn)
        {
            try
            {
                Int32 dsResult = -99;
                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;

                String wpdinsertCommand = "INSERT INTO WPDataSetName (wpdsName, runID, cTime, uTime, jobID, welID, rnsID) VALUES (@wpdsName, @runID, @cTime, @uTime, @jobID, @welID, @rnsID)";
                String sqcinsertCommand = "INSERT INTO SqcToRunSection (rnsID, sqcID, cTime, uTime) VALUES (@rnsID, @sqcID, @cTime, @uTime)";

                using (SqlConnection clntConn = new SqlConnection(dbConn))
                {
                    try
                    {
                        clntConn.Open();
                        //Adding Dataset Name to the Client Database
                        SqlCommand insertDSName = new SqlCommand(wpdinsertCommand, clntConn);
                        insertDSName.Parameters.AddWithValue("@wpdsName", SqlDbType.NVarChar).Value = datasetName.ToString();                        
                        insertDSName.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        insertDSName.Parameters.AddWithValue("@cTime", SqlDbType.DateTime).Value = crTime.ToString();
                        insertDSName.Parameters.AddWithValue("@uTime", SqlDbType.DateTime).Value = updTime.ToString();
                        insertDSName.Parameters.AddWithValue("@jobID", SqlDbType.Int).Value = JobID.ToString();
                        insertDSName.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        insertDSName.Parameters.AddWithValue("@rnsiD", SqlDbType.Int).Value = RunSectionID.ToString();

                        dsResult = Convert.ToInt32(insertDSName.ExecuteNonQuery());

                        //Adding SqcToRunSection to the Client Database
                        SqlCommand insertSQCRNS = new SqlCommand(sqcinsertCommand, clntConn);
                        insertSQCRNS.Parameters.AddWithValue("@rnsID", SqlDbType.Int).Value = RunSectionID.ToString();
                        insertSQCRNS.Parameters.AddWithValue("@sqcID", SqlDbType.Int).Value = SQCID.ToString();
                        insertSQCRNS.Parameters.AddWithValue("@cTime", SqlDbType.Int).Value = crTime.ToString();
                        insertSQCRNS.Parameters.AddWithValue("@uTime", SqlDbType.Int).Value = updTime.ToString();

                        dsResult = Convert.ToInt32(insertSQCRNS.ExecuteNonQuery());

                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }
                return dsResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getWPBHANameList(Int32 RunID, String dbConn)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = 0;
                String name = String.Empty;
                String selData = "SELECT [bhaID], [bhaName] FROM [WPBHA] WHERE [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                                else
                                {
                                    iReply.Add(-99, "No BHA Signature found for selected Run");
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Method to delete Dataset Row from WPDataset table
        private static Int32 deleteWPDatasetRow(Int32 RunID, Int32 dsRowID, String dbConn)
        {
            try
            {
                Int32 iResult = -99;

                String deleteDSRow = "DELETE FROM [WPDataset] WHERE [runID] = @RunID and [srvyRowID] = @dsRowID";

                using (SqlConnection clntConn = new SqlConnection(dbConn))
                {
                    try
                    {
                        clntConn.Open();

                        SqlCommand delRow = new SqlCommand(deleteDSRow, clntConn);
                        delRow.Parameters.AddWithValue("@RunID", SqlDbType.Int).Value = RunID.ToString();
                        delRow.Parameters.AddWithValue("@dsRowID", SqlDbType.Int).Value = dsRowID.ToString();

                        iResult = Convert.ToInt32(delRow.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Method to delete unAccounted-for Dataset Row from Well Profile table
        private static Int32 deleteWPProcessedRow(Int32 RunID, Int32 dsRowID, String dbConn)
        {
            try
            {
                Int32 iResult = -99;

                String deleteDSRow = "DELETE FROM [WellProfile] WHERE [runID] = @RunID and [srvyRowID] = @dsRowID";
                //Insert routine for DS audited row

                using (SqlConnection clntConn = new SqlConnection(dbConn))
                {
                    try
                    {
                        clntConn.Open();

                        SqlCommand delRow = new SqlCommand(deleteDSRow, clntConn);
                        delRow.Parameters.AddWithValue("@RunID", SqlDbType.Int).Value = RunID.ToString();
                        delRow.Parameters.AddWithValue("@dsRowID", SqlDbType.Int).Value = dsRowID.ToString();

                        iResult = Convert.ToInt32(delRow.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Method to delete unAccounted-for Dataset Row from WP QC Results table
        private static Int32 deleteWPQCRow(Int32 RunID, Int32 dsRowID, String dbConn)
        {
            try
            {
                Int32 iResult = -99;

                String deleteDSRow = "DELETE FROM [WPQCResults] WHERE [runID] = @RunID and [srvyRowID] = @dsRowID";
                //Insert routine for DS audited row

                using (SqlConnection clntConn = new SqlConnection(dbConn))
                {
                    try
                    {
                        clntConn.Open();

                        SqlCommand delRow = new SqlCommand(deleteDSRow, clntConn);
                        delRow.Parameters.AddWithValue("@RunID", SqlDbType.Int).Value = RunID.ToString();
                        delRow.Parameters.AddWithValue("@dsRowID", SqlDbType.Int).Value = dsRowID.ToString();

                        iResult = Convert.ToInt32(delRow.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        
                           
        //Method to add Well Profile Dataset row to WPDataset table
        private static Int32 insertDataset(Int32 DatasetID, Int32 EntryMethodID, Decimal Depth, Decimal Inclination, Decimal Azimuth, Decimal GTF, Int32 runID, Int32 srvyRowID, Int32 bhaID, String dbConn)
        {
            try
            {
                Int32 iResult = -99;
                String dsEntry = null;
                DateTime crDate = DateTime.Now;
                DateTime updDate = DateTime.Now;

                dsEntry = getdsEntryType(EntryMethodID);

                String insertCommand = "INSERT INTO [WPDataset] ([wpdsID], [dsDepth], [dsInc], [dsAzm], [dsGTF], [dsEntry], [runID], [srvyRowID], [cTime], [uTime], [bhaID]) VALUES (@wpdsID, @dsDepth, @dsInc, @dsAzm, @dsGTF, @dsEntry, @runID, @srvyRowID, @cTime, @uTime, @bhaID)";

                using (SqlConnection clntConn = new SqlConnection(dbConn))
                {
                    try
                    {
                        clntConn.Open();

                        SqlCommand insertDS = new SqlCommand(insertCommand, clntConn);
                        insertDS.Parameters.AddWithValue("@wpdsID", SqlDbType.Int).Value = DatasetID.ToString();
                        insertDS.Parameters.AddWithValue("@dsDepth", SqlDbType.Decimal).Value = Depth.ToString();
                        insertDS.Parameters.AddWithValue("@dsInc", SqlDbType.Decimal).Value = Inclination.ToString();
                        insertDS.Parameters.AddWithValue("@dsAzm", SqlDbType.Decimal).Value = Azimuth.ToString();
                        insertDS.Parameters.AddWithValue("@dsGTF", SqlDbType.Decimal).Value = GTF.ToString();
                        insertDS.Parameters.AddWithValue("@dsEntry", SqlDbType.NVarChar).Value = dsEntry.ToString();
                        insertDS.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = runID.ToString();
                        insertDS.Parameters.AddWithValue("@srvyRowID", SqlDbType.Int).Value = srvyRowID.ToString();
                        insertDS.Parameters.AddWithValue("@cTime", SqlDbType.DateTime).Value = crDate.ToString();
                        insertDS.Parameters.AddWithValue("@uTime", SqlDbType.DateTime).Value = updDate.ToString();
                        insertDS.Parameters.AddWithValue("@bhaID", SqlDbType.Int).Value = bhaID.ToString();

                        iResult = Convert.ToInt32(insertDS.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getDataSetDetailTable(Int32 DataSetID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn dsID = iReply.Columns.Add("dsID", typeof(Int32));
                DataColumn dsDepth = iReply.Columns.Add("dsDepth", typeof(Decimal));
                DataColumn dsInc = iReply.Columns.Add("dsInc", typeof(Decimal));
                DataColumn dsAzm = iReply.Columns.Add("dsAzm", typeof(Decimal));
                DataColumn dsGTF = iReply.Columns.Add("dsGTF", typeof(Decimal));
                DataColumn dsEntry = iReply.Columns.Add("dsEntry", typeof(String));
                String selData = "SELECT [dsID], [dsDepth], [dsInc], [dsAzm], [dsGTF], [dsEntry] FROM [WPDataset] WHERE [wpdsID] = @wpdsID ORDER BY [dsDepth] ASC";
                Int32 id = 0;
                Decimal depth = 0.00M, inc = 0.00M, azm = 0.00M, gtf = 0.00M;
                String entry = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wpdsID", SqlDbType.Int).Value = DataSetID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        depth = readData.GetDecimal(1);
                                        inc = readData.GetDecimal(2);
                                        azm = readData.GetDecimal(3);
                                        gtf = readData.GetDecimal(4);
                                        entry = readData.GetString(5);
                                        iReply.Rows.Add(id, depth, inc, azm, gtf, entry);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getDataSetNameList(Int32 RunID, String ClientDBString)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [wpdsID], [wpdsName], [runID] FROM [WPDataSetName] WHERE [runID] = @runID";
                Int32 id = 0;
                String dName = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        dName = readData.GetString(1);
                                        iReply.Add(id, dName);
                                    }
                                }
                                else
                                {
                                    iReply.Add(1, "No DataSet defined for selected Well section");
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getDataSetNameTable(String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wpdsID = iReply.Columns.Add("wpdsID", typeof(Int32));                
                DataColumn wpdsName = iReply.Columns.Add("wpdsName", typeof(String));
                DataColumn welID = iReply.Columns.Add("welID", typeof(String));
                DataColumn rnsID = iReply.Columns.Add("rnsID", typeof(String));
                DataColumn runID = iReply.Columns.Add("runID", typeof(String));
                String selData = "SELECT [wpdsID], [wpdsName], [welID], [rnsID], [runID] FROM [WPDataSetName]";
                Int32 wpid = 0, wID = 0, sID = 0, rID = 0;
                String dName = String.Empty, wlName = String.Empty, snName = String.Empty, rnName = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        wpid = readData.GetInt32(0);
                                        dName = readData.GetString(1);
                                        wID = readData.GetInt32(3);
                                        wlName = AST.getWellName(wID, ClientDBString);
                                        sID = readData.GetInt32(4);
                                        snName = DMG.getRunSectionName(sID);
                                        rID = readData.GetInt32(5);
                                        rnName = AST.getRunName(rID);
                                        iReply.Rows.Add(wpid, dName, wlName, snName, rnName);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static String getdsEntryType(int EntryMethodID)
        {
            try
            {
                String iResult = null;
                String selData = "SELECT [demName] FROM [dmgDataEntryMethod] WHERE [demID] = @demID";

                using (SqlConnection dmgConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ConnectionString))
                {
                    try
                    {
                        dmgConn.Open();
                        SqlCommand getData = new SqlCommand(selData, dmgConn);
                        getData.Parameters.AddWithValue("@demID", SqlDbType.Int).Value = EntryMethodID.ToString();

                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iResult = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dmgConn.Close();
                        dmgConn.Dispose();
                    }
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Method to recieve WP Dataset for Processing and Generating BGs and Profile
        private static Int32 genBG(String UserName, String ClntRealm, Int32 OperatorID, Int32 ClientID, Int32 wpdsID, Int32 WellID, Int32 RunID, Int32 BHAID, Int32 SQCID, Decimal dsDepth, Decimal dsInclincation, Decimal dsAzimuth, Decimal dsGTF, Int32 srRowID, String dbConn)
        {
            try
            {
                Int32 iResult = -99;

                //Insert Routine for processing dataset datarow
                iResult = WPBG.qcDSRow(UserName, ClntRealm, OperatorID, ClientID, wpdsID, WellID, RunID, BHAID, SQCID, dsDepth, dsInclincation, dsAzimuth, dsGTF, srRowID, dbConn);

                if (iResult.Equals(1)) //Inserted and processed successfully
                {
                    return iResult;
                }
                else if (iResult.Equals(-2)) //Couldn't account for BGs generation
                {
                    return iResult;
                }
                else if (iResult.Equals(-3)) //Couldn't process dataset QC
                {
                    return iResult;
                }
                else if (iResult.Equals(-4)) //Couldn't account for QC
                {
                    return iResult;
                }
                else //Unknown error
                {
                    return iResult;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
        
        //Method to get Next Sequence Number for the current row being processed
        public static Int32 getNextSequenceWP(Int32 RunID, Int32 bhaID, String dbConn)
        {
            try
            {
                Int32 nextSeq = -99;
                    
                using (SqlConnection Conn = new SqlConnection(dbConn))
                {
                    try
                    {
                        Conn.Open();  // Connection is opened to get runID and next sequence

                        // Get Next Sequence before Survey information is stored
                        String NextSequenceQuery = "SELECT ISNULL(MAX(srvyRowID), 0) + 1 FROM [WPDataset] WHERE [runID] = @RunID";
                        SqlCommand nseqCMD = new SqlCommand(NextSequenceQuery, Conn);
                        nseqCMD.Parameters.AddWithValue("@RunID", SqlDbType.Int).Value = RunID.ToString();

                        using (SqlDataReader nseqRdr = nseqCMD.ExecuteReader())
                        {
                            try
                            {
                                while (nseqRdr.Read())
                                {
                                    nextSeq = nseqRdr.GetInt32(0);
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                nseqRdr.Close();
                                nseqRdr.Dispose();
                            }
                        }                        
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        Conn.Close();
                        Conn.Dispose();
                    }                    
                }
                return nextSeq;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getSQCIDFromRunSection(Int32 RunSectionID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT [sqcID] FROM [SqcToRunSection] WHERE [rnsID] = @rnsID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@rnsID", SqlDbType.Int).Value = RunSectionID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getWellProfileDetailTable(Int32 DataSetID, Int32 SurveyRowID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wpqcID = iReply.Columns.Add("wpqcID", typeof(Int32));
                DataColumn colT = iReply.Columns.Add("colT", typeof(Decimal));
                DataColumn colDX = iReply.Columns.Add("colDX", typeof(Decimal));
                DataColumn colBA = iReply.Columns.Add("colBA", typeof(Decimal));
                DataColumn colCA = iReply.Columns.Add("colCA", typeof(Decimal));
                DataColumn colCF = iReply.Columns.Add("colCF", typeof(Decimal));
                DataColumn colAO = iReply.Columns.Add("colAO", typeof(Decimal));
                DataColumn colAP = iReply.Columns.Add("colAP", typeof(Decimal));
                DataColumn colDD = iReply.Columns.Add("colDD", typeof(Decimal));
                DataColumn colDE = iReply.Columns.Add("colDE", typeof(Decimal));
                Int32 id = 0;
                Decimal t = -99.0000M, dx = -99.0000M, ba = -99.0000M, ca = -99.0000M, cf = -99.0000M, ao = -99.0000M, ap = -99.0000M, dd = -99.0000M, de = -99.0000M;
                String selData = "SELECT [wpqcID], [colT], [colDX], [colBA], [colCA], [colCF], [colAO], [colAP], [colDD], [colDE] FROM [WPQCResults] WHERE [wpdsID] = @wpdsID AND [srvyRowID] = @srvyRowID";

                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wpdsID", SqlDbType.Int).Value = DataSetID.ToString();
                        getData.Parameters.AddWithValue("@srvyRowID", SqlDbType.Int).Value = SurveyRowID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        t = readData.GetDecimal(1);
                                        dx = readData.GetDecimal(2);
                                        ba = readData.GetDecimal(3);
                                        ca = readData.GetDecimal(4);
                                        cf = readData.GetDecimal(5);
                                        ao = readData.GetDecimal(6);
                                        ap = readData.GetDecimal(7);
                                        dd = readData.GetDecimal(8);
                                        de = readData.GetDecimal(9);
                                        iReply.Rows.Add(id, t, dx, ba, ca, cf, ao, ap, dd, de);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getWellProfileDetail1Table(Int32 DataSetID, Int32 SurveyRowID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wpqcID = iReply.Columns.Add("wpqcID", typeof(Int32));
                DataColumn colFY = iReply.Columns.Add("colFY", typeof(Decimal));
                DataColumn colEL = iReply.Columns.Add("colEL", typeof(Decimal));
                DataColumn colEM = iReply.Columns.Add("colEM", typeof(Decimal));
                DataColumn colEN = iReply.Columns.Add("colEN", typeof(String));
                DataColumn colP = iReply.Columns.Add("colP", typeof(Decimal));
                DataColumn colQ = iReply.Columns.Add("colQ", typeof(Decimal));                
                Int32 id = 0, en = 0;
                Decimal fy = -99.0000M, el = -99.0000M, em = -99.0000M, p = -99.00M, q = -99.00M;
                String enValue = String.Empty;
                String selData = "SELECT [wpqcID], [colFY], [colEL], [colEM], [colEN], [colP], [colQ] FROM [WPQCResults] WHERE [wpdsID] = @wpdsID AND [srvyRowID] = @srvyRowID";

                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wpdsID", SqlDbType.Int).Value = DataSetID.ToString();
                        getData.Parameters.AddWithValue("@srvyRowID", SqlDbType.Int).Value = SurveyRowID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        fy = readData.GetDecimal(1);
                                        el = readData.GetDecimal(2);
                                        em = readData.GetDecimal(3);
                                        en = readData.GetInt32(4);
                                        enValue = QC.getBHAStatement(en);
                                        p = readData.GetDecimal(5);
                                        q = readData.GetDecimal(6);

                                        iReply.Rows.Add(id, fy, el, em, enValue, p, q);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getWellProfileJobList(String ClientDBConnection)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                Int32 id = 0;
                String selData = "SELECT [jobID] From [Job] WHERE [jclID] = 3";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        iReply.Add(id);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getWellProfileTable(Int32 DataSetID, Int32 BHAID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wpID = iReply.Columns.Add("wpID", typeof(Int32));
                DataColumn wpdsID = iReply.Columns.Add("wpdsID", typeof(Int32));
                DataColumn srvyRowID = iReply.Columns.Add("srvyRowID", typeof(Int32));
                DataColumn wpDepth = iReply.Columns.Add("wpDepth", typeof(Decimal));
                DataColumn wpGx = iReply.Columns.Add("wpGx", typeof(Decimal));
                DataColumn wpGy = iReply.Columns.Add("wpGy", typeof(Decimal));
                DataColumn wpGz = iReply.Columns.Add("wpGz", typeof(Decimal));
                DataColumn wpBx = iReply.Columns.Add("wpBx", typeof(Decimal));
                DataColumn wpBy = iReply.Columns.Add("wpBy", typeof(Decimal));
                DataColumn wpBz = iReply.Columns.Add("wpBz", typeof(Decimal));
                DataColumn bhaID = iReply.Columns.Add("bhaID", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                Int32 id = 0, dsID = 0, sID = 0, bha = 0;
                Decimal depth = -99.00M, gx = -99.0000M, gy = -99.0000M, gz = -99.0000M, bx = -99.0000M, by = -99.0000M, bz = -99.0000M;
                DateTime uT = DateTime.Now;
                String bhaName = String.Empty;
                String selData = "SELECT [wpID], [wpdsID], [srvyRowID], [wpDepth], [wpGx], [wpGy], [wpGz], [wpBx], [wpBy], [wpBz], [bhaID], [uTime] FROM [WellProfile] WHERE [wpdsID] = @wpdsID AND [bhaID] = @bhaID ORDER BY [srvyRowID]";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wpdsID", SqlDbType.Int).Value = DataSetID.ToString();
                        getData.Parameters.AddWithValue("@bhaID", SqlDbType.Int).Value = BHAID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        dsID = readData.GetInt32(1);
                                        sID = readData.GetInt32(2);
                                        depth = readData.GetDecimal(3);
                                        gx = readData.GetDecimal(4);
                                        gy = readData.GetDecimal(5);
                                        gz = readData.GetDecimal(6);
                                        bx = readData.GetDecimal(7);
                                        by = readData.GetDecimal(8);
                                        bz = readData.GetDecimal(9);
                                        bha = readData.GetInt32(10);
                                        bhaName = getWPBHAName(bha, ClientDBConnection);
                                        uT = readData.GetDateTime(11);
                                        iReply.Rows.Add(id, dsID, sID, depth, gx, gy, gz, bx, by, bz, bhaName, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getWPSectionRunList(Int32 WellID, Int32 RunSectionID, String ClientDBString)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [runID] FROM [RunSectionToRun] WHERE [rnsID] = @rnsID AND [welID] = @welID";                
                Int32 id = 0, bha = 0;
                String runName = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@rnsID", SqlDbType.Int).Value = RunSectionID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        bha = AST.getWPBHACount(id, ClientDBString);
                                        if(bha.Equals(0))
                                        {
                                            runName = AST.getRunName(id);
                                            runName = runName + " (Missing BHA signature)";
                                            iReply.Add(id, runName);
                                        }
                                        else
                                        {
                                            runName = AST.getRunName(id);
                                            iReply.Add(id, runName);
                                        }
                                    }
                                }
                                else
                                {
                                    iReply.Add(-99, "No Run defined for selected Run Section");
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<KeyValuePair<Int32, Int32>> getWellProfileWellList(String ClientDBString)
        {
            try
            {
                List<KeyValuePair<Int32, Int32>> iReply = new List<KeyValuePair<Int32, Int32>>();
                Int32 id = 0;
                String selData = "SELECT [welID] FROM [WellToJob] WHERE [jobID] = @jobID";
                List<Int32> jobList = getWellProfileJobList(ClientDBString);
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();                        
                        foreach (var jd in jobList)
                        {
                            SqlCommand getData = new SqlCommand(selData, dataCon);
                            getData.Parameters.AddWithValue("@jobID", SqlDbType.Int).Value = jd.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            id = readData.GetInt32(0);
                                            iReply.Add(new KeyValuePair<Int32, Int32>(jd, id));
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getRunSectionTable(Int32 WellID, Int32 WellSectionID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn runID = iReply.Columns.Add("runID", typeof(Int32));
                DataColumn runName = iReply.Columns.Add("runName", typeof(String));
                DataColumn Inc = iReply.Columns.Add("Inc", typeof(Decimal));
                DataColumn Azm = iReply.Columns.Add("Azm", typeof(Decimal));
                DataColumn TInc = iReply.Columns.Add("TInc", typeof(Decimal));
                DataColumn TAzm = iReply.Columns.Add("TAzm", typeof(Decimal));
                DataColumn stat = iReply.Columns.Add("stat", typeof(String));
                DataColumn tcID = iReply.Columns.Add("tcID", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                List<Int32> runList = new List<Int32>();
                List<Int32> irunList = new List<Int32>();
                Int32 rID = -99, rnID = -99, tc = -99;
                Decimal inc = -99.00M, azm = -99.00M, tinc = -99.00M, tazm = -99.00M;
                String name = String.Empty, toolC = String.Empty;
                DateTime uT = DateTime.Now;
                String selRun = "SELECT [runID] FROM [RunSectionToRun] WHERE [rnsID] = @rnsID AND [welID] = @welID";
               String selRInfo = "SELECT [runID], [runName], [runInc], [runAzm], [runTInc], [runTAzm], [tcID], [uTime] FROM [WPRun] WHERE [runID] = @runID";
                using (SqlConnection runCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        runCon.Open();
                        SqlCommand getRun = new SqlCommand(selRun, runCon);
                        getRun.Parameters.AddWithValue("@rnsID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getRun.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readRun = getRun.ExecuteReader())
                        {
                            try
                            {
                                if (readRun.HasRows)
                                {
                                    while (readRun.Read())
                                    {
                                        runList.Add(readRun.GetInt32(0));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readRun.Close();
                                readRun.Dispose();
                            }
                        }
                        foreach (var id in runList)
                        {
                            SqlCommand getInfo = new SqlCommand(selRInfo, runCon);
                            getInfo.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = id.ToString();
                            using (SqlDataReader readInfo = getInfo.ExecuteReader())
                            {
                                if (readInfo.HasRows)
                                {
                                    while (readInfo.Read())
                                    {
                                        rID = readInfo.GetInt32(0);
                                        rnID = readInfo.GetInt32(1);
                                        name = AST.getRunNameValue(rID);
                                        inc = readInfo.GetDecimal(2);
                                        azm = readInfo.GetDecimal(3);
                                        tinc = readInfo.GetDecimal(4);
                                        tazm = readInfo.GetDecimal(5);
                                        tc = readInfo.GetInt32(6);
                                        toolC = DMG.getToolCodeName(tc);
                                        uT = readInfo.GetDateTime(7);
                                        iReply.Rows.Add(rID, name, inc, azm, tinc, tazm, "---", toolC, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        runCon.Close();
                        runCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Method to process WP Dataset Manually entered Data Row
        public static Int32 processDSRow(String UserName, String ClntRealm, Int32 OperatorID, Int32 ClientID, Int32 wpdsID, Int32 WellID, Int32 RNSID, Int32 RunID, Int32 BHAID, Int32 SQCID, String dsDataRow, String dbConn)
        {
            try
            {
                Int32 iReply = -99, iInsert = -99, iProcess = -99, iAccount = -99, srRowID = -99;
                Int32 demID = 1; //Demog DataEntryMethod Type selector - Manual Dataset Row entry.
                String dsInput = null;
                String[] str;
                Decimal dsDepth = -99.00M, dsInclination = -99.000000000000M, dsAzimuth = -99.000000000000M, dsGTF = -99.000000000000M;

                srRowID = getNextSequenceWP(RunID, BHAID, dbConn);
                dsInput = dsDataRow;
                if (dsInput.Contains(",") && dsInput.Contains(",") && dsInput.Contains(","))
                {
                    str = dsInput.Split(',');
                    dsDepth = Convert.ToDecimal(str[2].Trim());
                    dsInclination = Convert.ToDecimal(str[3].Trim());
                    dsAzimuth = Convert.ToDecimal(str[4].Trim());
                    dsGTF = Convert.ToDecimal(str[5].Trim());
                }

                iInsert = insertDataset(wpdsID, demID, dsDepth, dsInclination, dsAzimuth, dsGTF, RunID, srRowID, BHAID, dbConn);
                if (iInsert.Equals(1))
                {
                    iProcess = genBG(UserName, ClntRealm, OperatorID, ClientID, wpdsID, WellID, RunID, BHAID, SQCID, dsDepth, dsInclination, dsAzimuth, dsGTF, srRowID, dbConn);
                    if (iProcess.Equals(1))
                    {
                        iAccount = ACC.accWP(UserName, ClntRealm, wpdsID, RunID, BHAID, srRowID, dsDepth);
                        if (iAccount.Equals(1))
                        {
                            iReply = 1;
                        }
                        else
                        {
                            iReply = -3;
                            deleteWPDatasetRow(RunID, srRowID, dbConn); //Delete unAccounted-for Row from WPDataset
                            deleteWPProcessedRow(RunID, srRowID, dbConn); //Delete unAccounted-for Row from WellProfile (BGs)
                            deleteWPQCRow(RunID, srRowID, dbConn);//Delete unAccounted-for Row from WPQCResults
                        }
                    }
                    else
                    {
                        iReply = -2;
                        deleteWPDatasetRow(RunID, srRowID, dbConn); //Deleting unProcessed Row from WPDataset
                    }
                }
                else
                {
                    iReply = -1;
                }

                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Method to create WP Dataset file filepath and process file
        public static Int32 processDSFile(Int32 OperatorID, Int32 ClientID, String UserName, String ClntRealm, Int32 wpdsID, Int32 JobID, Int32 WellID, Int32 RNSID, Int32 RunID, Int32 BHAID, String filePath, String dbConn)
        {
            try
            {
                Int32 iReply = -99, nextSeq = -99; ;
                Int32 sqcID = getSQCIDFromRunSection(RNSID, dbConn); 
                //Int32 demID = -99; //Demog DataEntryMethod Type selector
                WellProfile OBJ = new WellProfile();
                using (StreamReader LogHeadReader = new StreamReader(filePath))
                {
                    try
                    {
                        using (SqlConnection Conn = new SqlConnection(dbConn))
                        {
                            try
                            {
                                Conn.Open();  // Connection opened to get next sequence

                                // Get Next Sequence before Survey information is stored
                                nextSeq = getNextSequenceWP(RunID, BHAID, dbConn);

                                while (!LogHeadReader.EndOfStream)
                                {
                                    String text = LogHeadReader.ReadLine();

                                    //  Survey File Details
                                    if (text.Contains(",") && text.Contains(",") && text.Contains(","))
                                    {

                                        // Reading the Header Detail TOP Row 
                                        if (text.ToLower().Contains("depth") || text.ToLower().Contains("azm") || text.ToLower().Contains("gtf"))
                                        {
                                            // Console.WriteLine(text);
                                            OBJ.headerTOP = text.Split(',');
                                            ResultsHeaderTOPLINE(OBJ.HeaderGB, OBJ.headerTOP);
                                        }
                                        else
                                        {
                                            // Reading Log Details and Processing the Data
                                            OBJ.DetailList.Add(text.Trim());
                                            OBJ.det = text.Split(',');
                                            iReply = ResultsDetailsProcessor(OBJ.HeaderGB, OBJ.det, OperatorID, ClientID, wpdsID, WellID, RNSID, RunID, BHAID, sqcID, nextSeq, UserName, ClntRealm, dbConn);
                                        }
                                    }
                                    // Survey Header Data
                                    else
                                    {
                                        if (!String.IsNullOrEmpty(text)) // removing empty spaces from String
                                        {
                                            // Not Required for the Results Processor. The header Data of the File
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                Conn.Close();  // Connection closed for setting the next sequence
                                Conn.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        LogHeadReader.Close();
                        LogHeadReader.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static void ResultsHeaderTOPLINE(Dictionary<String, Int32> header_GB, String[] arr)
        {
            try
            {
                int arrlength = arr.Length;
                for (int i = 0; i < arrlength; i++)
                {
                    if (arr[i].ToLower().Contains("depth") ||
                          arr[i].ToLower().Contains("inc") ||
                          arr[i].ToLower().Contains("azimuth") ||
                          arr[i].ToLower().Contains("gtf")
                        )
                        header_GB.Add(arr[i].Trim(), i);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        protected static Int32 ResultsDetailsProcessor(Dictionary<String, Int32> header_GB, String[] arrD, Int32 OperatorID, Int32 ClientID, Int32 wpdsID, Int32 WellID, Int32 RNSID, Int32 RunID, Int32 BHAID, Int32 SQCID, Int32 nextSequence, String LoginName, String LoginRealm, String ClientDBString)
        {
            try
            {
                Int32 arrDlength, depth, inc, azimuth, gtf, iResult;
                depth = inc = azimuth = gtf = 0;
                Decimal DEPTH, INC, AZIMUTH, GTF;

                arrDlength = arrD.Length;

                foreach (KeyValuePair<String, Int32> item in header_GB)
                {
                    if (item.Key.ToLower().Contains("depth"))
                    { depth = item.Value; }
                    if (item.Key.ToLower().Contains("inc"))
                    { inc = item.Value; }
                    if (item.Key.ToLower().Contains("azimuth"))
                    { azimuth = item.Value; }
                    if (item.Key.ToLower().Contains("gtf"))
                    { gtf = item.Value; }
                }


                if (arrD[depth].Trim() == "" || arrD[inc].Trim() == "" || arrD[azimuth].Trim() == "" || arrD[gtf].Trim() == "")
                {
                    //Console.WriteLine("\n The Required Fields to process the Results QC are missing. Survey at {0} is skipped ", arrD[2]);
                    iResult = -1;
                }
                else
                {
                    DEPTH = Convert.ToDecimal(arrD[depth]);
                    INC = Convert.ToDecimal(arrD[inc]);
                    AZIMUTH = Convert.ToDecimal(arrD[azimuth]);
                    GTF = Convert.ToDecimal(arrD[gtf]);
                    String dRow = String.Format("{0},{1},{2},{3},{4},{5}", DateTime.Now.Date, DateTime.Now.TimeOfDay, DEPTH, INC, AZIMUTH, GTF);
                    //StoreResultsData(DT, TM, DEPTH, INC, AZIMUTH, DIP, BTOTAL, GTOTAL, GTF, RunID, nextSequence);
                    iResult = processDSRow(LoginName, LoginRealm, OperatorID, ClientID, wpdsID, WellID, RNSID, RunID, BHAID, SQCID, dRow, ClientDBString);
                    //processDSRow(wpdsID, DEPTH, INC, AZIMUTH, GTF, JobID, WellID, RNSID, RunID, SQCID, dRow, ClientDBString);
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getWPBHAName(Int32 BhaID, String ClientDBString)        
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [bhaName] FROM [WPBHA] WHERE [bhaID] = @bhaID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@bhaID", SqlDbType.Int).Value = BhaID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                                else
                                {
                                    iReply = "---";
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getBHACountPerRun(Int32 RunID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(DISTINCT(bhaID)) FROM [WPQCResults] WHERE [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    iReply = -1;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getDataTableforWellProfile(Int32 WPDataSetID, String dbConn)
        {
            try
            {
                DataSet dsData = new DataSet();
                String SelectQuery = "Select [srvyRowID] as Survey_number , [colAP] as AzimuthError , [colDE] as Difference , colFY as AzimuthBHA FROM [WPQCResults] WHERE [wpdsID] = @wpdsID ";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@wpdsID", SqlDbType.Int).Value = WPDataSetID.ToString();
                        using (SqlDataAdapter sqlCmd = new SqlDataAdapter(cmd))
                        {
                            try
                            {
                                sqlCmd.Fill(dsData);  // Fetching the data
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                sqlCmd.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return dsData.Tables[0];
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }          
}