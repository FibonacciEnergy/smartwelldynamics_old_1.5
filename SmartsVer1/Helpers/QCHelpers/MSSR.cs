﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Collections.Generic;
using QC = SmartsVer1.QCHelpers.sdRawDataQC;
using ACC = SmartsVer1.Helpers.QCHelpers.AccountingQC;

namespace SmartsVer1.Helpers.QCHelpers
{
    public class MSSR
    {               
        static void Main(){}

        public static Int32 addStormName(String sName, String dbConn)
        {
            try
            {
                Int32 rowsAffec = 0;
                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;

                SqlConnection clntConn = new SqlConnection(dbConn);

                String insertSName = "INSERT INTO MSDataSetName (msdsName, cTime, uTime) VALUES (@msdsName, @crTime, @updTime)";

                clntConn.Open();
                SqlCommand clntCmd = new SqlCommand(insertSName, clntConn);
                clntCmd.Parameters.AddWithValue("@msdsName", SqlDbType.NVarChar).Value = sName.ToString();
                clntCmd.Parameters.AddWithValue("@crTime", SqlDbType.DateTime).Value = crTime.ToString();
                clntCmd.Parameters.AddWithValue("@updTime", SqlDbType.DateTime).Value = updTime.ToString();

                rowsAffec = clntCmd.ExecuteNonQuery();
                clntCmd.Dispose();
                clntConn.Dispose();

                if (rowsAffec.Equals(1))
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addStormData(Int32 stormID, Decimal depth, Decimal Azm, Int32 BT, Decimal Dip, String dbConn)
        {
            try
            {
                Int32 resultAdd = 0;
                Int32 srvyNo = 0;
                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;

                srvyNo = getSurveyNumber(stormID, dbConn);

                SqlConnection clntConn = new SqlConnection(dbConn);

                String insertCommand = "INSERT INTO MSStormData (msdsID, mssdSrvyNo, mssdDepth, mssdAzm, mssdBT, mssdDip, cTime, uTime) VALUES (@msdsID, @mssdSrvyNo, @mssdDepth, @mssdAzm, @mssdBT, @mssdDip, @cTime, @uTime)";

                clntConn.Open();
                SqlCommand clntCmd = new SqlCommand(insertCommand, clntConn);
                clntCmd.Parameters.AddWithValue("@msdsID", SqlDbType.Int).Value = stormID.ToString();
                clntCmd.Parameters.AddWithValue("@mssdSrvyNo", SqlDbType.Int).Value = srvyNo.ToString();
                clntCmd.Parameters.AddWithValue("@mssdDepth", SqlDbType.Decimal).Value = depth.ToString();
                clntCmd.Parameters.AddWithValue("@mssdAzm", SqlDbType.Decimal).Value = Azm.ToString();
                clntCmd.Parameters.AddWithValue("@mssdBT", SqlDbType.Int).Value = BT.ToString();
                clntCmd.Parameters.AddWithValue("@mssdDip", SqlDbType.Decimal).Value = Dip.ToString();
                clntCmd.Parameters.AddWithValue("@cTime", SqlDbType.DateTime).Value = crTime.ToString();
                clntCmd.Parameters.AddWithValue("@uTime", SqlDbType.DateTime).Value = updTime.ToString();

                resultAdd = clntCmd.ExecuteNonQuery();                
                clntCmd.Dispose();
                clntConn.Close();
                clntConn.Dispose();

                if (resultAdd.Equals(1))
                {
                    return resultAdd;
                }
                else
                {
                    resultAdd = resultAdd - 99;
                    return resultAdd;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addReSurveyData(String userName, String domain, Int32 stormID, Decimal rsDepth, Decimal rsAzm, Int32 rsBT, Decimal rsDip, String dbConn)
        {
            try
            {
                Int32 resultAdd = -1;
                Int32 corrtdAdd = -1;
                Int32 srvyNo = -1;
                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;

                srvyNo = getReSurveyNumber(stormID, dbConn);

                SqlConnection clntConn = new SqlConnection(dbConn);

                String insertCommand = "INSERT INTO MSReSurveyData (msdsID, rsSrvyNo, rsDepth, rsAzm, rsBT, rsDip, cTime, uTime) VALUES (@msdsID, @rsSrvyNo, @rsDepth, @rsAzm, @rsBT, @rsDip, @cTime, @uTime)";

                clntConn.Open();
                SqlCommand clntCmd = new SqlCommand(insertCommand, clntConn);
                clntCmd.Parameters.AddWithValue("@msdsID", SqlDbType.Int).Value = stormID.ToString();
                clntCmd.Parameters.AddWithValue("@rsSrvyNo", SqlDbType.Int).Value = srvyNo.ToString();
                clntCmd.Parameters.AddWithValue("@rsDepth", SqlDbType.Decimal).Value = rsDepth.ToString();
                clntCmd.Parameters.AddWithValue("@rsAzm", SqlDbType.Decimal).Value = rsAzm.ToString();
                clntCmd.Parameters.AddWithValue("@rsBT", SqlDbType.Int).Value = rsBT.ToString();
                clntCmd.Parameters.AddWithValue("@rsDip", SqlDbType.Decimal).Value = rsDip.ToString();
                clntCmd.Parameters.AddWithValue("@cTime", SqlDbType.DateTime).Value = crTime.ToString();
                clntCmd.Parameters.AddWithValue("@uTime", SqlDbType.DateTime).Value = updTime.ToString();

                resultAdd = clntCmd.ExecuteNonQuery();
                clntCmd.Dispose();
                clntConn.Dispose();

                if (resultAdd.Equals(1))
                {
                    corrtdAdd = processRow(userName, domain, stormID, srvyNo, dbConn); //Calculating Corrected values for current Row and inserting in DB
                    if (corrtdAdd.Equals(1))
                    {
                        return resultAdd;
                    }
                    else
                    {
                        return (resultAdd - 99);
                    }
                }
                else
                {
                    return (resultAdd - 99);
                }
                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Column Q
        private static Int32 calcDeltaAzm(Int32 srvyNo, Int32 msdsID, String dbConn) 
        {
            try
            {
                if (srvyNo.Equals(1) || srvyNo.Equals(2))
                {
                    return 0;
                }
                else
                {
                    Int32 result = -99;
                    Int32 sNo = -99;
                    Decimal stAzm = -99.00M;
                    Decimal rsAzm = -99.00M;
                    sNo = srvyNo - 1;
                    String stormDataCmd = "SELECT mssdAzm FROM MSStormData WHERE msdsID = @msdsID AND mssdsrvyNo = @sNo";
                    String resrvyDataCmd = "SELECT rsAzm FROM MSReSurveyData WHERE msdsID = @msdsID AND rsSrvyNo = @sNo";

                    SqlConnection dataCon = new SqlConnection(dbConn);

                    SqlCommand getStormData = new SqlCommand(stormDataCmd, dataCon);
                    SqlCommand getReSrvyData = new SqlCommand(resrvyDataCmd, dataCon);

                    dataCon.Open();
                    getStormData.Parameters.AddWithValue("@msdsID", SqlDbType.Int).Value = msdsID.ToString();
                    getStormData.Parameters.AddWithValue("@sNo", SqlDbType.Int).Value =sNo.ToString();
                    getReSrvyData.Parameters.AddWithValue("@msdsID", SqlDbType.Int).Value = msdsID.ToString();
                    getReSrvyData.Parameters.AddWithValue("@sNo", SqlDbType.Int).Value = sNo.ToString();

                    SqlDataReader strmRdr = getStormData.ExecuteReader();
                    if(strmRdr.HasRows)
                    {
                        while(strmRdr.Read())
                        {
                            stAzm = strmRdr.GetDecimal(0);
                        }
                    }
                    strmRdr.Close();

                    SqlDataReader resvyRdr = getReSrvyData.ExecuteReader();
                    if (resvyRdr.HasRows)
                    {
                        while (resvyRdr.Read())
                        {
                            rsAzm = resvyRdr.GetDecimal(0);
                        }
                    }
                    resvyRdr.Close();

                    dataCon.Close();
                    result = Convert.ToInt32( Decimal.Subtract(stAzm, rsAzm) );
                    return result;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Column R
        private static Int32 calcDeltaBT(Int32 srvyNo, Int32 msdsID, String dbConn) 
        {
            try
            {
                if (srvyNo.Equals(1) || srvyNo.Equals(2))
                {
                    return 0;
                }
                else
                {
                    Int32 sNo = -99;
                    Int32 stBT = -99;
                    Int32 rsBT = -99;
                    sNo = (srvyNo - 1);
                    String stormDataCmd = "SELECT mssdBT FROM MSStormData WHERE msdsID = @msdsID AND mssdsrvyNo = @sNo";
                    String resrvyDataCmd = "SELECT rsBT FROM MSReSurveyData WHERE msdsID = @msdsID AND rsSrvyNo = @sNo";

                    SqlConnection dataCon = new SqlConnection(dbConn);

                    SqlCommand getStormData = new SqlCommand(stormDataCmd, dataCon);
                    SqlCommand getReSrvyData = new SqlCommand(resrvyDataCmd, dataCon);

                    dataCon.Open();
                    getStormData.Parameters.AddWithValue("@msdsID", SqlDbType.Int).Value = msdsID.ToString();
                    getStormData.Parameters.AddWithValue("@sNo", SqlDbType.Int).Value = sNo.ToString();
                    getReSrvyData.Parameters.AddWithValue("@msdsID", SqlDbType.Int).Value = msdsID.ToString();
                    getReSrvyData.Parameters.AddWithValue("@sNo", SqlDbType.Int).Value = sNo.ToString();

                    SqlDataReader strmRdr = getStormData.ExecuteReader();
                    if (strmRdr.HasRows)
                    {
                        while (strmRdr.Read())
                        {
                            stBT = strmRdr.GetInt32(0);
                        }
                    }
                    strmRdr.Close();

                    SqlDataReader resvyRdr = getReSrvyData.ExecuteReader();
                    if (resvyRdr.HasRows)
                    {
                        while (resvyRdr.Read())
                        {
                            rsBT = resvyRdr.GetInt32(0);
                        }
                    }
                    resvyRdr.Close();

                    dataCon.Close();
                    return ( stBT - rsBT );                    
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Column S
        private static Decimal calcDeltaDip(Int32 srvyNo, Int32 msdsID, String dbConn) 
        {
            try
            {
                if (srvyNo.Equals(1) || srvyNo.Equals(0))
                {
                    return 0.00M;
                }
                else
                {
                    Int32 sNo = -99;
                    Decimal stDip = -99.00M;
                    Decimal rsDip = -99.00M;
                    sNo = (srvyNo - 1);
                    String stormDataCmd = "SELECT mssdDip FROM MSStormData WHERE msdsID = @msdsID AND mssdsrvyNo = @sNo";
                    String resrvyDataCmd = "SELECT rsDip FROM MSReSurveyData WHERE msdsID = @msdsID AND rsSrvyNo = @sNo";

                    SqlConnection dataCon = new SqlConnection(dbConn);

                    SqlCommand getStormData = new SqlCommand(stormDataCmd, dataCon);
                    SqlCommand getReSrvyData = new SqlCommand(resrvyDataCmd, dataCon);

                    dataCon.Open();
                    getStormData.Parameters.AddWithValue("@msdsID", SqlDbType.Int).Value = msdsID.ToString();
                    getStormData.Parameters.AddWithValue("@sNo", SqlDbType.Int).Value = sNo.ToString();
                    getReSrvyData.Parameters.AddWithValue("@msdsID", SqlDbType.Int).Value = msdsID.ToString();
                    getReSrvyData.Parameters.AddWithValue("@sNo", SqlDbType.Int).Value = sNo.ToString();

                    SqlDataReader strmRdr = getStormData.ExecuteReader();
                    if (strmRdr.HasRows)
                    {
                        while (strmRdr.Read())
                        {
                            stDip = strmRdr.GetDecimal(0);
                        }
                    }
                    strmRdr.Close();

                    SqlDataReader resvyRdr = getReSrvyData.ExecuteReader();
                    if (resvyRdr.HasRows)
                    {
                        while (resvyRdr.Read())
                        {
                            rsDip = resvyRdr.GetDecimal(0);
                        }
                    }
                    resvyRdr.Close();

                    dataCon.Close();

                    return Decimal.Subtract(stDip, rsDip);                     
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Column T
        private static Int32 sumCalcDeltaAzm(Int32 srvyNo, Int32 msdsID, String dbConn, Decimal deltaAzm) 
        {
            try
            {
                if (srvyNo.Equals(1) || srvyNo.Equals(0))
                {
                    return 0;
                }
                else
                {
                    Int32 result = -99;
                    String selCmd = "SELECT SUM(deltaAzm) FROM MSCorrectedDataset WHERE msdsID = @msdsID";
                    SqlConnection dataCon = new SqlConnection(dbConn);
                    SqlCommand getDAzm = new SqlCommand(selCmd, dataCon);
                    dataCon.Open();

                    getDAzm.Parameters.AddWithValue("@msdsID", SqlDbType.Int).Value = msdsID.ToString();

                    SqlDataReader readDAzm = getDAzm.ExecuteReader();
                    if (readDAzm.HasRows)
                    {
                        while (readDAzm.Read())
                        {
                            result = readDAzm.GetInt32(0);
                        }
                    }
                    dataCon.Close();
                    dataCon.Dispose();

                    result = result + Convert.ToInt32(deltaAzm);
                    return result;// Sum( Column Q )
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
        
        private static Int32 sumDeltaAzmCond(Int32 stormCond, Int32 sumDeltaAzm)
        {
            try
            {
                if (stormCond.Equals(0))
                {
                    return 0;
                }
                else
                {
                    return sumDeltaAzm; //Column U
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 sumDeltaBT(Int32 srvyNo, Int32 sumDeltaBT)
        {
            try
            {
                if (srvyNo.Equals(1))
                {
                    return 0;
                }
                else
                {
                    return sumDeltaBT; //Column R
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 deltaBTCond(Int32 stormCond, Int32 sumDeltaBT)
        {
            try
            {
                if (stormCond.Equals(0))
                {
                    return 0;
                }
                else
                {
                    return sumDeltaBT; //Column W
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal sumDeltaDip(Int32 srvyNo, Decimal sumDeltaDip)
        {
            try
            {
                if (srvyNo.Equals(1))
                {
                    return 0;
                }
                else
                {
                    return sumDeltaDip; //Column S
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
        
        private static Decimal DeltaDipCond(Int32 stormCond, Decimal sumDeltaDip)
        {
            try
            {
                if (stormCond.Equals(0))
                {
                    return 0;
                }
                else
                {
                    return sumDeltaDip; //Column X
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Column AB
        private static Int32 corrDeltaAzm(Int32 srvyNo, Int32 msdsID, String dbConn) 
        {
            try
            {
                if (srvyNo.Equals(1) || srvyNo.Equals(2))
                {
                    return 0;
                }
                else
                {
                    Int32 result = -99;
                    Decimal clnAzm = -99.00M; //Current storm 1st clean Survey Azimuth
                    Decimal strmAzm = -99.00M; //Previous row Storm Azimuth
                    Int32 sNo = -99;
                    sNo = (srvyNo - 1);

                    String selClnData = "SELECT mssdAzm FROM MSStormData WHERE msdsID = @msdsID AND mssdSrvyNo = @srvyNo";
                    String selStrmData = "SELECT mssdAzm FROM MSStormData WHERE msdsID = @msdsID AND mssdSrvyNo = @sNo";

                    SqlConnection dataCon = new SqlConnection(dbConn);
                    dataCon.Open();

                    SqlCommand getInitialData = new SqlCommand(selClnData, dataCon);
                    getInitialData.Parameters.AddWithValue("@msdsID", SqlDbType.Int).Value = msdsID.ToString();
                    getInitialData.Parameters.AddWithValue("@srvyNo", SqlDbType.Int).Value = '1'; //The first Survey for current data set
                    SqlCommand getCurrData = new SqlCommand(selStrmData, dataCon);
                    getCurrData.Parameters.AddWithValue("@msdsID", SqlDbType.Int).Value = msdsID.ToString();
                    getCurrData.Parameters.AddWithValue("@sNo", SqlDbType.Int).Value = sNo.ToString();

                    SqlDataReader readInit = getInitialData.ExecuteReader();
                    if (readInit.HasRows)
                    {
                        while (readInit.Read())
                        {
                            clnAzm = readInit.GetDecimal(0);
                        }
                    }
                    readInit.Close();
                    readInit.Dispose();

                    SqlDataReader readCurr = getCurrData.ExecuteReader();
                    if (readCurr.HasRows)
                    {
                        while (readCurr.Read())
                        {
                            strmAzm = readCurr.GetDecimal(0);
                        }
                    }
                    readCurr.Close();
                    readCurr.Dispose();

                    dataCon.Close();
                    dataCon.Dispose();

                    result = Convert.ToInt32( Decimal.Subtract( clnAzm, strmAzm ) );
                    return result;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Column AC
        private static Int32 corrDeltaBT(Int32 srvyNo, Int32 msdsID, String dbConn) 
        {
            try
            {
                if (srvyNo.Equals(1) || srvyNo.Equals(2))
                {
                    return 0;
                }
                else
                {
                    Int32 clnBT = -99; //First Clean Survy B-Total
                    Int32 strmBT = -99;
                    Int32 sNo = -99;
                    sNo = (srvyNo - 1);

                    String selClnData = "SELECT mssdBT FROM MSStormData WHERE msdsID = @msdsID AND mssdSrvyNo = @srvyNo";
                    String selStrmData = "SELECT mssdBT FROM MSStormData WHERE msdsID = @msdsID AND mssdSrvyNo = @sNo";

                    SqlConnection dataCon = new SqlConnection(dbConn);
                    dataCon.Open();

                    SqlCommand getInitialData = new SqlCommand(selClnData, dataCon);
                    getInitialData.Parameters.AddWithValue("@msdsID", SqlDbType.Int).Value = msdsID.ToString();
                    getInitialData.Parameters.AddWithValue("@srvyNo", SqlDbType.Int).Value = '1'; //The first Survey for current data set
                    SqlCommand getCurrData = new SqlCommand(selStrmData, dataCon);
                    getCurrData.Parameters.AddWithValue("@msdsID", SqlDbType.Int).Value = msdsID.ToString();
                    getCurrData.Parameters.AddWithValue("@sNo", SqlDbType.Int).Value = sNo.ToString();

                    SqlDataReader readInit = getInitialData.ExecuteReader();
                    if (readInit.HasRows)
                    {
                        while (readInit.Read())
                        {
                            clnBT = readInit.GetInt32(0);
                        }
                    }
                    readInit.Close();
                    readInit.Dispose();

                    SqlDataReader readCurr = getCurrData.ExecuteReader();
                    if (readCurr.HasRows)
                    {
                        while (readCurr.Read())
                        {
                            strmBT = readCurr.GetInt32(0);
                        }
                    }
                    readCurr.Close();
                    readCurr.Dispose();

                    dataCon.Close();
                    dataCon.Dispose();

                    return (clnBT - strmBT);                    
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Column AD
        private static Decimal corrDeltaDip(Int32 srvyNo, Int32 msdsID, String dbConn) 
        {
            try
            {
                if (srvyNo.Equals(1) || srvyNo.Equals(2))
                {
                    return 0.00M;
                }
                else
                {
                    Decimal result = -99.00M;
                    Decimal clnDip = -99.00M; //Dip for First Clean Dataset row
                    Decimal strmDip = -99.00M; //Dip for Storm for current Survey Row
                    Int32 sNo = -99;
                    sNo = srvyNo - 1; //Previous Row Survey Number

                    String clnData = "SELECT mssdDip FROM MSStormData WHERE msdsID = @msdsID AND mssdSrvyNo = @srvyNo";
                    String strmData = "SELECT mssdDip FROM MSStormData WHERE msdsID = @msdsID AND mssdSrvyNo = @sNo";

                    SqlConnection dataCon = new SqlConnection(dbConn);

                    dataCon.Open();

                    SqlCommand getClnData = new SqlCommand(clnData, dataCon);
                    getClnData.Parameters.AddWithValue("@msdsID", SqlDbType.Int).Value = msdsID.ToString();
                    getClnData.Parameters.AddWithValue("@srvyNo", SqlDbType.Int).Value = '1';
                    SqlCommand getStrmData = new SqlCommand(strmData, dataCon);
                    getStrmData.Parameters.AddWithValue("@msdsID", SqlDbType.Int).Value = msdsID.ToString();
                    getStrmData.Parameters.AddWithValue("@sNo", SqlDbType.Int).Value = sNo.ToString();

                    SqlDataReader readClnD = getClnData.ExecuteReader();
                    if (readClnD.HasRows)
                    {
                        while (readClnD.Read())
                        {
                            clnDip = readClnD.GetDecimal(0);
                        }
                    }
                    readClnD.Close();
                    readClnD.Dispose();

                    SqlDataReader readStrmD = getStrmData.ExecuteReader();
                    if (readStrmD.HasRows)
                    {
                        while (readStrmD.Read())
                        {
                            strmDip = readStrmD.GetDecimal(0);
                        }
                    }
                    readStrmD.Close();
                    readStrmD.Dispose();

                    dataCon.Close();
                    dataCon.Dispose();

                    result = Decimal.Subtract(clnDip, strmDip);
                    return result;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Column AE
        private static Int32 stormIndicator(Int32 srvyNo, Int32 currDeltaAzm, Int32 currDeltaBT, Decimal currDeltaDip, Int32 currCorrDeltaBT, Decimal currCorrDeltaDip)
        {
            try
            {
                Int32 diffDeltaBT = -1, diffAzm = -1, diffBT = -1;
                Decimal diffDeltaDip = -1.00M, diffDip = -1.00M, btTol = -99.00M, azmTol = -99.00M, dipTol = -99.00M;
                List<Decimal> mssrQList = getMagStormQualifierList();
                btTol = mssrQList[0]; dipTol = mssrQList[1]; azmTol = mssrQList[2];
                diffDeltaBT = Math.Abs(0 - currCorrDeltaBT);    //Column AC diff
                diffDeltaDip = Math.Abs(0 - currCorrDeltaDip);  //Column AD diff
                diffAzm = Math.Abs(0 - currDeltaAzm);           //Column Q diff
                diffBT = Math.Abs(0 - currDeltaBT);             //Column R diff
                diffDip = Math.Abs(0 - currDeltaDip);           //Column S diff

                if ( (diffDeltaBT > btTol) || (diffDeltaDip > dipTol) || (diffAzm > azmTol) || (diffBT > dipTol) || (diffDip > dipTol) )
                {
                    return 1; 
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal corrAzm(Int32 stormCond, Decimal stormAzm, Decimal deltaAzm, Decimal sumDeltaAzmCond)
        {
            try
            {
                if (stormCond.Equals(0))
                {
                    return Decimal.Add(stormAzm , deltaAzm);//( Column C + Column Q )
                }
                else
                {
                    return Decimal.Add(stormAzm , sumDeltaAzmCond);//( Column C + Column U )
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 corrBT(Int32 stormCond, Int32 stormBT, Int32 deltaBT, Int32 sumDeltaBTCond)
        {
            try
            {
                if (stormCond.Equals(0))
                {
                    return (stormBT + deltaBT);//( Column D + Column R )
                }
                else
                {
                    return (stormBT + sumDeltaBTCond);//( Column D + Column W )
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal corrDip(Int32 stormCond, Decimal stormDip, Decimal deltaDip, Decimal sumDeltaDipCond)
        {
            try
            {
                if (stormCond.Equals(0))
                {
                    return (stormDip + deltaDip);//( Column E + Column S )
                }
                else
                {
                    return (stormDip + sumDeltaDipCond);//( Column E + Column Y )
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 processRow(String userName, String domain, Int32 srvySetID, Int32 srvyNo, String DBString)
        {
            try
            {
                Int32 result = -99;
                Decimal srvyDepth = -99.00M;             //Survey Depth
                Decimal stormAzm = -99.00M;              //Column C
                Int32 stormBT = -99;                     //Column D
                Decimal stormDip = -99.00M;              //Column E
                Decimal rsAzm = -99.00M;                 //Column H
                Int32 rsBT = -99;                        //Column I
                Decimal rsDip = -99.00M;                 //Column J
                Int32 deltaAzm = -99;                    //Column Q
                Int32 deltaBT = -99;                     //Column R
                Decimal deltaDip = -99.00M;              //Column S
                Int32 sumCalcAzm = -99;                  //Column T - part 1
                Int32 sumDeltaAzm = -99;                 //Column T
                Decimal prevStormAzm = -99.00M;          //Column AB - part 1
                Int32 corrDeltaAzmVal = -99;             //Column AB
                Int32 prevStormBT = -99;                 //Column AC - part 1
                Int32 corrDeltaBTVal = -99;              //Column AC
                Decimal prevStormDip = -99.00M;          //Column AD - part 1
                Decimal corrDeltaDipVal = -99.00M;       //Column AD
                Int32 stormInd = -99;                    //Column AE
                Int32 sumAzmCond = -99;                  //Column U
                Int32 sumBT = -99;                       //Column V - part 1
                Int32 sumDeltaBT = -99;                  //Column V
                Int32 sumDeltaBTCond = -99;              //Column W
                Decimal sumDip = -99.00M;                //Column X - part 1
                Decimal sumDeltaDip = -99.00M;           //Column X
                Decimal sumDeltaDipCond = -99.00M;       //Column Y
                Decimal corrAzmVal = -99.00M;            //Column AF
                Int32 corrBTVal = -99;                   //Column AG
                Decimal corrDipVal = -99.00M;            //Column AH
                
                SqlConnection clntConn = new SqlConnection(DBString);

                if((srvyNo.Equals(1)) || (srvyNo.Equals(2)))
                {
                    String stmDataCommand = "SELECT mssdDepth, mssdAzm, mssdBT, mssdDip FROM MSStormData WHERE msdsID = @msdsID AND mssdSrvyNo = @mssdSrvyNo";
                    SqlCommand getStrmData = new SqlCommand(stmDataCommand, clntConn);
                    getStrmData.Parameters.Add("@msdsID", SqlDbType.Int).Value = srvySetID.ToString();
                    getStrmData.Parameters.Add("@mssdSrvyNo", SqlDbType.Int).Value = srvyNo.ToString();
                    clntConn.Open();

                    SqlDataReader strmDataRow = getStrmData.ExecuteReader();
                    if (strmDataRow.HasRows)
                    {
                        while (strmDataRow.Read())
                        {
                            srvyDepth = strmDataRow.GetDecimal(0);  //Read Storm Depth
                            stormAzm = strmDataRow.GetDecimal(1);   //Read Column C
                            stormBT = strmDataRow.GetInt32(2);      //Read Column D
                            stormDip = strmDataRow.GetDecimal(3);   //Read Column E
                        }
                    }
                    strmDataRow.Close();
                    deltaAzm = 0;                       //Calculated Column Q
                    deltaBT = 0;                        //Calculated Column R
                    deltaDip = 0.00M;                   //Calculated Column S
                    sumDeltaAzm = 0;                    //Calculated Column T
                    corrDeltaAzmVal = 0;                //Calculated Column AB
                    corrDeltaBTVal = 0;                 //Calculated Column AC
                    corrDeltaDipVal = 0.00M;            //Calculated Column AD
                    stormInd = 0;                       //Calculated Column AE
                    sumAzmCond = 0;                     //Calculated Column U
                    sumDeltaBT = 0;                     //Calculated Column V
                    sumDeltaBTCond = 0;                 //Calculated Column W
                    sumDeltaDip = 0.00M;                //Calculated Column X
                    sumDeltaDipCond = 0.00M;            //Calculated Column Y
                    corrAzmVal = stormAzm;              //Calculated Column AF
                    corrBTVal = stormBT;                //Calculated Column AG
                    corrDipVal = stormDip;              //Calculated Column AH
                }
                else
                {
                    String stmDataCommand = "SELECT mssdDepth, mssdAzm, mssdBT, mssdDip FROM MSStormData WHERE msdsID = @msdsID AND mssdSrvyNo = @mssdSrvyNo";
                    String rsDataCommand = "SELECT rsAzm, rsBT, rsDip FROM MSReSurveyData WHERE msdsID = @msdsID AND rsSrvyNo = @rsSrvyNo";
                    String sumCorrDataCommand = "SELECT SUM(sumDeltaAzm) FROM MSCorrectedDataset WHERE msdsID = @msdsID";
                    String prevStmAzmBTDip = "SELECT mssdAzm, mssdBT, mssdDip FROM MSStormData WHERE msdsID = @msdsID AND mssdSrvyNo = @mssdSrvyNo";
                    String sumDBT = "SELECT SUM(deltaBT), SUM(deltaDip) FROM MSCorrectedDataset WHERE msdsID = @msdsID";

                    SqlCommand getStrmData = new SqlCommand(stmDataCommand, clntConn);
                    getStrmData.Parameters.Add("@msdsID", SqlDbType.Int).Value = srvySetID.ToString();
                    getStrmData.Parameters.Add("@mssdSrvyNo", SqlDbType.Int).Value = srvyNo.ToString();
                    SqlCommand getRSvyData = new SqlCommand(rsDataCommand, clntConn);
                    getRSvyData.Parameters.Add("@msdsID", SqlDbType.Int).Value = srvySetID.ToString();
                    getRSvyData.Parameters.Add("@rsSrvyNo", SqlDbType.Int).Value = srvyNo.ToString();
                    SqlCommand getSumCorrData = new SqlCommand(sumCorrDataCommand, clntConn);
                    getSumCorrData.Parameters.Add("@msdsID", SqlDbType.Int).Value = srvySetID.ToString();
                    SqlCommand getPrevStmAzm = new SqlCommand(prevStmAzmBTDip, clntConn);
                    getPrevStmAzm.Parameters.Add("@msdsID", SqlDbType.Int).Value = srvySetID.ToString();
                    getPrevStmAzm.Parameters.Add("@mssdSrvyNo", SqlDbType.Int).Value = srvyNo.ToString();
                    SqlCommand getSumDeltaBT = new SqlCommand(sumDBT, clntConn);
                    getSumDeltaBT.Parameters.Add("@msdsID", SqlDbType.Int).Value = srvySetID.ToString();

                    clntConn.Open();

                    SqlDataReader strmDataRow = getStrmData.ExecuteReader();
                    if (strmDataRow.HasRows)
                    {
                        while (strmDataRow.Read())
                        {
                            srvyDepth = strmDataRow.GetDecimal(0);  //Read Storm Depth
                            stormAzm = strmDataRow.GetDecimal(1);   //Read Column C
                            stormBT = strmDataRow.GetInt32(2);      //Read Column D
                            stormDip = strmDataRow.GetDecimal(3);   //Read Column E
                        }
                    }
                    strmDataRow.Close();

                    SqlDataReader rsvyDataRow = getRSvyData.ExecuteReader();
                    if (rsvyDataRow.HasRows)
                    {
                        while (rsvyDataRow.Read())
                        {
                            rsAzm = rsvyDataRow.GetDecimal(0);  //Read Column H
                            rsBT = rsvyDataRow.GetInt32(1);     //Read Column I
                            rsDip = rsvyDataRow.GetDecimal(2);  //Read Column J
                        }
                    }
                    rsvyDataRow.Close();

                    if (srvyNo.Equals(1) || srvyNo.Equals(2))
                    {
                        sumCalcAzm = 0;
                    }
                    else
                    {
                        SqlDataReader corrSumData = getSumCorrData.ExecuteReader();
                        if (corrSumData.HasRows)
                        {
                            while (corrSumData.Read())
                            {
                                sumCalcAzm = corrSumData.GetInt32(0);       //Read Sum(Column H)
                            }
                        }
                        corrSumData.Close();
                    }

                    if (srvyNo.Equals(1) || srvyNo.Equals(2))
                    {
                        prevStormAzm = 0;
                        prevStormBT = 0;
                        prevStormDip = 0;
                    }
                    else
                    {
                        SqlDataReader prvStormAzmData = getPrevStmAzm.ExecuteReader();
                        if (prvStormAzmData.HasRows)
                        {
                            while (prvStormAzmData.Read())
                            {
                                prevStormAzm = prvStormAzmData.GetDecimal(0);   //Reading Previous Row StormAzm value
                                prevStormBT = prvStormAzmData.GetInt32(1);      //Reading Previous Row StormBT value     
                                prevStormDip = prvStormAzmData.GetDecimal(2);   //Reading Previous Row StormDip value
                            }
                        }
                        prvStormAzmData.Close();
                    }

                    if (srvyNo.Equals(1) || srvyNo.Equals(2))
                    {
                        sumBT = 0;
                        sumDip = 0;
                    }
                    else
                    {
                        SqlDataReader sumDBTReader = getSumDeltaBT.ExecuteReader();
                        if (sumDBTReader.HasRows)
                        {
                            while (sumDBTReader.Read())
                            {
                                sumBT = sumDBTReader.GetInt32(0);
                                sumDip = sumDBTReader.GetDecimal(1);
                            }
                        }
                        sumDBTReader.Close();
                    }

                    clntConn.Close();

                    deltaAzm = calcDeltaAzm(srvyNo, srvySetID, DBString);                       //Calculated Column Q
                    deltaBT = calcDeltaBT(srvyNo, srvySetID, DBString);                         //Calculated Column R
                    deltaDip = calcDeltaDip(srvyNo, srvySetID, DBString);                       //Calculated Column S
                    sumDeltaAzm = sumCalcDeltaAzm(srvyNo, srvySetID, DBString, deltaAzm);                 //Calculated Column T
                    corrDeltaAzmVal = corrDeltaAzm(srvyNo, srvySetID, DBString);                //Calculated Column AB
                    corrDeltaBTVal = corrDeltaBT(srvyNo, srvySetID, DBString);                  //Calculated Column AC
                    corrDeltaDipVal = corrDeltaDip(srvyNo, srvySetID, DBString);                //Calculated Column AD
                    stormInd = stormIndicator(srvyNo, deltaAzm, deltaBT, deltaDip, corrDeltaBTVal, corrDeltaDipVal);    //Calculated Column AE
                    sumAzmCond = sumDeltaAzmCond(stormInd, sumDeltaAzm);                    //Calculated Column U
                    sumDeltaBT = (sumBT + deltaBT);                                         //Calculated Column V
                    sumDeltaBTCond = deltaBTCond(stormInd, sumDeltaBT);                     //Calculated Column W
                    sumDeltaDip = (sumDip + deltaDip);                                      //Calculated Column X
                    sumDeltaDipCond = DeltaDipCond(stormInd, sumDeltaDip);                  //Calculated Column Y
                    corrAzmVal = corrAzm(stormInd, stormAzm, deltaAzm, sumAzmCond);         //Calculated Column AF
                    corrBTVal = corrBT(stormInd, stormBT, deltaBT, sumDeltaBTCond);         //Calculated Column AG
                    corrDipVal = corrDip(stormInd, stormDip, deltaDip, sumDeltaDipCond);    //Calculated Column AH
                }
                //Inserting values into CorrectedDataset table
                SqlConnection insertConn = new SqlConnection(DBString);

                String insertCommand = "INSERT INTO MSCorrectedDataset (msdsID, srvyNo, srvyDepth, deltaAzm, deltaBT, deltaDip, sumDeltaAzm, sumDeltaAzmCond, sumDeltaBT, sumDeltaBTCond, sumDeltaDip, sumDeltaDipCond, corrDeltaAzm, corrDeltaBT, corrDeltaDip, strmCondition, corrAzm, corrBT, corrDip, cTime, uTime) VALUES (@msdsID, @srvyNo, @srvyDepth, @deltaAzm, @deltaBT, @deltaDip, @sumDeltaAzm, @sumDeltaAzmCond, @sumDeltaBT, @sumDeltaBTCond, @sumDeltaDip, @sumDeltaDipCond, @corrDeltaAzm, @corrDeltaBT, @corrDeltaDip, @strmCondition, @corrAzm, @corrBT, @corrDip, @cTime, @uTime)";

                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;

                insertConn.Open();
                SqlCommand insertCmd = new SqlCommand(insertCommand, insertConn);
                insertCmd.Parameters.AddWithValue("@msdsID", SqlDbType.Int).Value = srvySetID.ToString();
                insertCmd.Parameters.AddWithValue("@srvyNo", SqlDbType.Int).Value = srvyNo.ToString();
                insertCmd.Parameters.AddWithValue("@srvyDepth", SqlDbType.Decimal).Value = srvyDepth.ToString();
                insertCmd.Parameters.AddWithValue("@deltaAzm", SqlDbType.Int).Value = deltaAzm.ToString();
                insertCmd.Parameters.AddWithValue("@deltaBT", SqlDbType.Int).Value = deltaBT.ToString();
                insertCmd.Parameters.AddWithValue("@deltaDip", SqlDbType.Decimal).Value = deltaDip.ToString();
                insertCmd.Parameters.AddWithValue("@sumDeltaAzm", SqlDbType.Int).Value = sumDeltaAzm.ToString();
                insertCmd.Parameters.AddWithValue("@sumDeltaAzmCond", SqlDbType.Int).Value = sumAzmCond.ToString();
                insertCmd.Parameters.AddWithValue("@sumDeltaBT", SqlDbType.Int).Value = sumDeltaBT.ToString();
                insertCmd.Parameters.AddWithValue("@sumDeltaBTCond", SqlDbType.Int).Value = sumDeltaBTCond.ToString();
                insertCmd.Parameters.AddWithValue("@sumDeltaDip", SqlDbType.Decimal).Value = sumDeltaDip.ToString();
                insertCmd.Parameters.AddWithValue("@sumDeltaDipCond", SqlDbType.Decimal).Value = sumDeltaDipCond.ToString();
                insertCmd.Parameters.AddWithValue("@corrDeltaAzm", SqlDbType.Int).Value = corrDeltaAzmVal.ToString();
                insertCmd.Parameters.AddWithValue("@corrDeltaBT", SqlDbType.Int).Value = corrDeltaBTVal.ToString();
                insertCmd.Parameters.AddWithValue("@corrDeltaDip", SqlDbType.Decimal).Value = corrDeltaDipVal.ToString();
                insertCmd.Parameters.AddWithValue("@strmCondition", SqlDbType.Int).Value = stormInd.ToString();
                insertCmd.Parameters.AddWithValue("@corrAzm", SqlDbType.Decimal).Value = corrAzmVal.ToString();
                insertCmd.Parameters.AddWithValue("@corrBT", SqlDbType.Int).Value = corrBTVal.ToString();
                insertCmd.Parameters.AddWithValue("@corrDip", SqlDbType.Decimal).Value = corrDipVal.ToString();
                insertCmd.Parameters.AddWithValue("@cTime", SqlDbType.DateTime).Value = crTime.ToString();
                insertCmd.Parameters.AddWithValue("@uTime", SqlDbType.DateTime).Value = updTime.ToString();

                result = insertCmd.ExecuteNonQuery();
                insertCmd.Dispose();                

                if (result.Equals(1))
                {
                    //result = ACC.accMSSR(userName, domain, srvySetID, srvyNo, srvyDepth);
                    return result;
                }
                else
                {
                    return (result - 99);
                }                 
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getCorrectedDatasetTable(Int32 DatasetID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn corrID = iReply.Columns.Add("corrID", typeof(Int32));
                DataColumn srvyNo = iReply.Columns.Add("srvyNo", typeof(Int32));
                DataColumn srvyDepth = iReply.Columns.Add("srvyDepth", typeof(Decimal));
                DataColumn corrAzm = iReply.Columns.Add("corrAzm", typeof(Decimal));
                DataColumn corrBT = iReply.Columns.Add("corrBT", typeof(Int32));
                DataColumn corrDip = iReply.Columns.Add("corrDip", typeof(Decimal));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [corrID], [srvyNo], [srvyDepth], [corrAzm], [corrBT], [corrDip], [uTime] FROM [MSCorrectedDataset] WHERE [msdsID] = @msdsID";
                Int32 id = 0, srID = 0, bt = 0;
                Decimal depth = -99.00M, azm = -99.00M, dp = -99.00M;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@msdsID", SqlDbType.Int).Value = DatasetID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        srID = readData.GetInt32(1);
                                        depth = readData.GetDecimal(2);
                                        azm = readData.GetDecimal(3);
                                        bt = readData.GetInt32(4);
                                        dp = readData.GetDecimal(5);
                                        uT = readData.GetDateTime(6);
                                        iReply.Rows.Add(id, srID, depth, azm, bt, dp, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getStormDatasetTable(Int32 DatasetID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn mssdID = iReply.Columns.Add("mssdID", typeof(Int32));
                DataColumn mssdSrvyNo = iReply.Columns.Add("mssdSrvyNo", typeof(Int32));
                DataColumn mssdDepth = iReply.Columns.Add("mssdDepth", typeof(Decimal));
                DataColumn mssdAzm = iReply.Columns.Add("mssdAzm", typeof(Decimal));
                DataColumn mssdBT = iReply.Columns.Add("mssdBT", typeof(Decimal));
                DataColumn mssdDip = iReply.Columns.Add("mssdDip", typeof(Decimal));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [mssdID], [mssdSrvyNo], [mssdDepth], [mssdAzm], [mssdBT], [mssdDip], [uTime] FROM [MSStormData] WHERE [msdsID] = @msdsID ORDER BY [mssdSrvyNo] DESC";
                Int32 id = 0, sID = 0, bt = -99;
                Decimal depth = -99.00M, azm = -99.0000M, dip = -99.0000M;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@msdsID", SqlDbType.Int).Value = DatasetID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        sID = readData.GetInt32(1);
                                        depth = readData.GetDecimal(2);
                                        azm = readData.GetDecimal(3);
                                        bt = readData.GetInt32(4);
                                        dip = readData.GetDecimal(5);
                                        uT = readData.GetDateTime(6);
                                        iReply.Rows.Add(id, sID, depth, azm, bt, dip, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getMagStormNameList(String ClientDBString)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [msdsID], [msdsName] FROM [MSDataSetName] ORDER BY [msdsName]";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getMagStormNameTable(String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn msdsID = iReply.Columns.Add("msdsID", typeof(Int32));
                DataColumn msdsName = iReply.Columns.Add("msdsName", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [msdsID], [msdsName], [uTime] FROM [MSDataSetName] ORDER BY [uTime] DESC";
                Int32 id = 0;
                String name = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, name, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Decimal> getMagStormQualifierList()
        {
            try
            {
                List<Decimal> iReply = new List<Decimal>();
                String selData = "SELECT [mssrqBTotal], [mssrqDip], [mssrqAzm] FROM [dmgMagStormSurveyQualifier]";
                Decimal bt = -99.00M, dp = -99.00M, azm = -99.00M;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        bt = readData.GetDecimal(0);
                                        iReply.Add(bt);
                                        dp = readData.GetDecimal(1);
                                        iReply.Add(dp);
                                        azm = readData.GetDecimal(2);
                                        iReply.Add(azm);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getMagStormQualifierTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn mssrqID = iReply.Columns.Add("mssrqID", typeof(Int32));
                DataColumn mssrqBTotal = iReply.Columns.Add("mssrqBTotal", typeof(Decimal));
                DataColumn mssrqDip = iReply.Columns.Add("mssrqDip", typeof(Decimal));
                DataColumn mssrqAzm = iReply.Columns.Add("mssrqAzm", typeof(Decimal));
                String selData = "SELECT [mssrqID], [mssrqBTotal], [mssrqDip], [mssrqAzm] FROM [dmgMagStormSurveyQualifier]";
                Int32 id = -99;
                Decimal bt = -99.00M, dp = -99.00M, azm = -99.00M;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        bt = readData.GetDecimal(1);
                                        dp = readData.GetDecimal(2);
                                        azm = readData.GetDecimal(3);
                                        iReply.Rows.Add(id, bt, dp, azm);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 getSurveyNumber(Int32 stormID, String DBString)
        {
            try
            {
                Int32 result = 0;                

                SqlConnection clntConn = new SqlConnection(DBString);

                String selectCommand = "SELECT MAX(mssdSrvyNo) FROM MSStormData GROUP BY msdsID HAVING msdsID = @msdsID";
                
                SqlCommand clntCmd = new SqlCommand(selectCommand, clntConn);
                clntCmd.Parameters.Add("@msdsID", SqlDbType.Int).Value = stormID.ToString();
                
                clntConn.Open();
                SqlDataReader readDataRow = clntCmd.ExecuteReader();
                if (readDataRow.HasRows)
                {
                    while (readDataRow.Read())
                    {
                        result = readDataRow.GetInt32(0);
                        result = result + 1;
                    }
                }
                else
                {
                    result = 1;
                }
                
                readDataRow.Close();
                clntConn.Close();

                return result;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
        
        private static Int32 getReSurveyNumber(Int32 stormID, String DBString)
        {
            try
            {
                Int32 result = 0;

                SqlConnection clntConn = new SqlConnection(DBString);

                String selectCommand = "SELECT MAX(rsSrvyNo) FROM MSReSurveyData GROUP BY msdsID HAVING msdsID = @msdsID";

                SqlCommand clntCmd = new SqlCommand(selectCommand, clntConn);
                clntCmd.Parameters.Add("@msdsID", SqlDbType.Int).Value = stormID.ToString();

                clntConn.Open();
                SqlDataReader readDataRow = clntCmd.ExecuteReader();
                if (readDataRow.HasRows)
                {
                    while (readDataRow.Read())
                    {
                        result = readDataRow.GetInt32(0);
                        result = result + 1;
                    }
                }
                else
                {
                    result = 1;
                }

                readDataRow.Close();
                clntConn.Close();

                return result;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getReSurveyDataSet(Int32 DatasetID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn rsID = iReply.Columns.Add("rsID", typeof(Int32));
                DataColumn rsSrvyNo = iReply.Columns.Add("rsSrvyNo", typeof(Int32));
                DataColumn rsDepth = iReply.Columns.Add("rsDepth", typeof(Decimal));
                DataColumn rsAzm = iReply.Columns.Add("rsAzm", typeof(Decimal));
                DataColumn rsBT = iReply.Columns.Add("rsBT", typeof(Int32));
                DataColumn rsDip = iReply.Columns.Add("rsDip", typeof(Decimal));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [rsID], [rsSrvyNo], [rsDepth], [rsAzm], [rsBT], [rsDip], [uTime] FROM [MSReSurveyData] WHERE [msdsID] = @msdsID ORDER BY [msdsID] DESC";
                Int32 id = 0, srID = 0, bt = 0;
                Decimal depth = -99.00M, azm = -99.00M, dip = -99.00M;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@msdsID", SqlDbType.Int).Value = DatasetID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        srID = readData.GetInt32(1);
                                        depth = readData.GetDecimal(2);
                                        azm = readData.GetDecimal(3);
                                        bt = readData.GetInt32(4);
                                        dip = readData.GetDecimal(5);
                                        uT = readData.GetDateTime(6);
                                        iReply.Rows.Add(id, srID, depth, azm, bt, dip, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        internal static System.Collections.ArrayList ProcessRawData(Int32 OperatorID, Int32 SysClientID, Int32 ServiceID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Int32 DataType, Int32 LengthUnit, Int32 AccelUnit, Int32 MagUnit, List<DataTable> UploadedData, DataTable SelectedValues, String LoginName, String LoginDomain, String ClientDBString)
        {
            System.Collections.ArrayList iReply = new System.Collections.ArrayList();
            Int32 nextSeq = -99, acID = -99;
            Decimal depth = -99.00M, Gx = -99.00M, Gy = -99.00M, Gz = -99.00M, Bx = -99.00M, By = -99.00M, Bz = -99.00M;
            List<Int32> srvyRowIDList = new List<Int32>();
            List<Double> srvyIDValues = new List<Double>();
            List<Decimal> bxList = new List<Decimal>(), byList = new List<Decimal>(), bzList = new List<Decimal>();
            List<Double> bxValues = new List<Double>(), byValues =new List<Double>();
            String srDate = String.Empty, srTime = String.Empty;
            Decimal calcRawAzimuth = -99.00M, calcRawDip = -99.00M, calcRawBT = -99.00M, calcRawGT = -99.00M, calcRawInc = -99.00M, calcRawGTF = -99.00M, calcRawMTF = -99.00M;
            Decimal calcDec = -99.00M;
            Decimal revGx = -99.00M, revGy = -99.00M, revGz = -99.00M, revBx = -99.00M, revBy = -99.00M, revBz = -99.00M;
            foreach (DataRow dRow in SelectedValues.Rows)
            {
                nextSeq = QC.getNextSequence(OperatorID, SysClientID, WellPadID, WellID, WellSectionID, RunID, ClientDBString);
                if (nextSeq.Equals(1))
                {
                    acID = 3;
                }
                else
                {
                    acID = QC.getAzimuthCriteriaID(OperatorID, WellPadID, WellID, WellSectionID, RunID, (nextSeq - 1), ClientDBString);
                }
                srvyRowIDList.Add(nextSeq);
                srvyIDValues.Add((Double)nextSeq);
                depth = Convert.ToDecimal(dRow["Depth"]);
                Gx = Convert.ToDecimal(dRow["rGx"]);
                Gy = Convert.ToDecimal(dRow["rGy"]);
                Gz = Convert.ToDecimal(dRow["rGz"]);
                Bx = Convert.ToDecimal(dRow["rBx"]);
                bxList.Add(Bx);
                bxValues.Add((Double)Bx);
                By = Convert.ToDecimal(dRow["rBy"]);
                byList.Add(By);
                byValues.Add((Double)By);
                Bz = Convert.ToDecimal(dRow["rBz"]);
                bzList.Add(Bz);
                srDate = Convert.ToDateTime(dRow["rtimestamp"]).ToShortDateString();
                srTime = Convert.ToDateTime(dRow["rtimestamp"]).ToShortTimeString();

                calcRawGTF = QC.getGTF(Gx, Gy);
                calcRawMTF = QC.colQ(Bx, By);
                calcRawInc = QC.colT(Gx, Gy, Gx);
                calcRawAzimuth = QC.colY(calcRawGTF, calcRawInc, Bx, By, Bz);
                calcRawBT = QC.colCA(Bx, By, Bz);
                calcRawGT = QC.colCF(Gx, Gy, Gz);
                calcRawDip = QC.colBA(Gx, Gy, Gz, Bx, By, Bz, calcRawGT, calcRawBT);
                
                revGx = SmartsVer1.Helpers.QCHelpers.BsGs.resGx(calcRawGT, calcRawInc, calcRawGTF);
                revGy = SmartsVer1.Helpers.QCHelpers.BsGs.resGy(calcRawGT, calcRawInc, calcRawGTF);
                revGz = SmartsVer1.Helpers.QCHelpers.BsGs.resGz(calcRawGT, calcRawInc);                
            }

            return iReply;
        }
    }
}