﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Linq;
using System.Collections;
using System.Globalization;
using System.Data.SqlClient;
using System.Web.SessionState;
using System.Web.Configuration;
using System.Collections.Generic;
using MNST = MathNet.Numerics.Statistics;
using MNFit = MathNet.Numerics.Fit;
using NMS = SmartsVer1.Helpers.QCHelpers.NonMagCalc;
using ACC = SmartsVer1.Helpers.QCHelpers.AccountingQC;

namespace SmartsVer1.Helpers.QCHelpers
{
    public class RemoveData
    {
        void main()
        { }

        public static List<Int32> getFileID(Int32 ServiceID, Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 JobID, String ClientDBString)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                Int32 id = -99;
                String selData = "SELECT [cfID] FROM [CorrectionDataFileHeader] WHERE [JobID] = @JobID AND [WellID] = @WellID AND [WSWID] = @WSWID AND [RunID] = @RunID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@JobID", SqlDbType.Int).Value = JobID.ToString();
                        getData.Parameters.AddWithValue("@WellID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@WSWID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@RunID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        iReply.Add(id);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getRawQCData(Int32 ServiceID, Int32 RunID, Int32 WellSectionID, Int32 WellID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn sfdID = iReply.Columns.Add("sfdID", typeof(Int32));
                DataColumn srvyRowID = iReply.Columns.Add("srvyRowID", typeof(Int32));
                DataColumn sfdDepth = iReply.Columns.Add("sfdDepth", typeof(Decimal));                
                DataColumn sfdGx = iReply.Columns.Add("sfdGx", typeof(Decimal));
                DataColumn sfdGy = iReply.Columns.Add("sfdGy", typeof(Decimal));
                DataColumn sfdGz = iReply.Columns.Add("sfdGz", typeof(Decimal));
                DataColumn sfdBx = iReply.Columns.Add("sfdBx", typeof(Decimal));
                DataColumn sfdBy = iReply.Columns.Add("sfdBy", typeof(Decimal));
                DataColumn sfdBz = iReply.Columns.Add("sfdBz", typeof(Decimal));
                DataColumn cTime = iReply.Columns.Add("cTime", typeof(DateTime));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [sfdID], [srvyRowID], [sfdDepth], [sfdGx], [sfdGy], [sfdGz], [sfdBx], [sfdBy], [sfdBz], [cTime], [uTime] FROM [SurveyFormattedData] WHERE [RunID] = @RunID AND [wswID] = @wswID AND [welID] = @welID AND [srvID] = @srvID ORDER BY [srvyRowID] DESC";
                Int32 id = 0, sid = 0;
                Decimal dpth = -99.00M, gx = -99.000000000000M, gy = -99.000000000000M, gz = -99.000000000000M, bx = -99.000000000000M, by = -99.000000000000M, bz = -99.000000000000M;
                DateTime uT = DateTime.Now, cT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@RunID", SqlDbType.Int).Value = RunID.ToString();                        
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        sid = readData.GetInt32(1);
                                        dpth = readData.GetDecimal(2);
                                        gx = readData.GetDecimal(3);
                                        gy = readData.GetDecimal(4);
                                        gz = readData.GetDecimal(5);
                                        bx = readData.GetDecimal(6);
                                        by = readData.GetDecimal(7);
                                        bz = readData.GetDecimal(8);
                                        cT = readData.GetDateTime(9);
                                        uT = readData.GetDateTime(10);
                                        iReply.Rows.Add(id, sid, dpth, gx, gy, gz, bx, by, bz, cT, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getResQCData(Int32 ServiceID, Int32 OperatorID, Int32 RunID, Int32 WellSectionID, Int32 WellID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn sfdID = iReply.Columns.Add("sfdID", typeof(Int32));
                DataColumn srvyRowID = iReply.Columns.Add("srvyRowID", typeof(Int32));
                DataColumn sfdDepth = iReply.Columns.Add("sfdDepth", typeof(Decimal));
                DataColumn sfdGx = iReply.Columns.Add("sfdGx", typeof(Decimal));
                DataColumn sfdGy = iReply.Columns.Add("sfdGy", typeof(Decimal));
                DataColumn sfdGz = iReply.Columns.Add("sfdGz", typeof(Decimal));
                DataColumn sfdBx = iReply.Columns.Add("sfdBx", typeof(Decimal));
                DataColumn sfdBy = iReply.Columns.Add("sfdBy", typeof(Decimal));
                DataColumn sfdBz = iReply.Columns.Add("sfdBz", typeof(Decimal));
                DataColumn cTime = iReply.Columns.Add("cTime", typeof(DateTime));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [sfdID], [srvyRowID], [sfdDepth], [sfdGx], [sfdGy], [sfdGz], [sfdBx], [sfdBy], [sfdBz], [cTime], [uTime] FROM [SurveyResultsData] WHERE [RunID] = @RunID AND [wswID] = @wswID AND [welID] = @welID AND [srvID] = @srvID ORDER BY [srvyRowID] DESC";
                Int32 id = 0, sid = 0;
                Decimal dpth = -99.00M, gx = -99.000000000000M, gy = -99.000000000000M, gz = -99.000000000000M, bx = -99.000000000000M, by = -99.000000000000M, bz = -99.000000000000M;
                DateTime uT = DateTime.Now, cT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@RunID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        sid = readData.GetInt32(1);
                                        dpth = readData.GetDecimal(2);
                                        gx = readData.GetDecimal(3);
                                        gy = readData.GetDecimal(4);
                                        gz = readData.GetDecimal(5);
                                        bx = readData.GetDecimal(6);
                                        by = readData.GetDecimal(7);
                                        bz = readData.GetDecimal(8);
                                        cT = readData.GetDateTime(9);
                                        uT = readData.GetDateTime(10);
                                        iReply.Rows.Add(id, sid, dpth, gx, gy, gz, bx, by, bz, cT, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable get3rdRawQCData(Int32 ServiceID, Int32 OperatorID, Int32 RunID, Int32 WellSectionID, Int32 WellID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn sfdID = iReply.Columns.Add("sfdID", typeof(Int32));
                DataColumn srvyRowID = iReply.Columns.Add("srvyRowID", typeof(Int32));
                DataColumn sfdDepth = iReply.Columns.Add("sfdDepth", typeof(Decimal));
                DataColumn sfdGx = iReply.Columns.Add("sfdGx", typeof(Decimal));
                DataColumn sfdGy = iReply.Columns.Add("sfdGy", typeof(Decimal));
                DataColumn sfdGz = iReply.Columns.Add("sfdGz", typeof(Decimal));
                DataColumn sfdBx = iReply.Columns.Add("sfdBx", typeof(Decimal));
                DataColumn sfdBy = iReply.Columns.Add("sfdBy", typeof(Decimal));
                DataColumn sfdBz = iReply.Columns.Add("sfdBz", typeof(Decimal));
                DataColumn cTime = iReply.Columns.Add("cTime", typeof(DateTime));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [sfdID], [srvyRowID], [sfdDepth], [sfdGx], [sfdGy], [sfdGz], [sfdBx], [sfdBy], [sfdBz], [cTime], [uTime] FROM [3rdSurveyFormattedData] WHERE [RunID] = @RunID AND [wswID] = @wswID AND [welID] = @welID AND [srvID] = @srvID ORDER BY [srvyRowID] DESC";
                Int32 id = 0, sid = 0;
                Decimal dpth = -99.00M, gx = -99.000000000000M, gy = -99.000000000000M, gz = -99.000000000000M, bx = -99.000000000000M, by = -99.000000000000M, bz = -99.000000000000M;
                DateTime uT = DateTime.Now, cT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@RunID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        sid = readData.GetInt32(1);
                                        dpth = readData.GetDecimal(2);
                                        gx = readData.GetDecimal(3);
                                        gy = readData.GetDecimal(4);
                                        gz = readData.GetDecimal(5);
                                        bx = readData.GetDecimal(6);
                                        by = readData.GetDecimal(7);
                                        bz = readData.GetDecimal(8);
                                        cT = readData.GetDateTime(9);
                                        uT = readData.GetDateTime(10);
                                        iReply.Rows.Add(id, sid, dpth, gx, gy, gz, bx, by, bz, cT, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getCorrData(Int32 RunID, Int32 WellSectionID, Int32 WellID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn sfdID = iReply.Columns.Add("sfdID", typeof(Int32));
                DataColumn srvyRowID = iReply.Columns.Add("srvyRowID", typeof(Int32));
                DataColumn sfdDepth = iReply.Columns.Add("sfdDepth", typeof(Decimal));
                DataColumn sfdGx = iReply.Columns.Add("sfdGx", typeof(Decimal));
                DataColumn sfdGy = iReply.Columns.Add("sfdGy", typeof(Decimal));
                DataColumn sfdGz = iReply.Columns.Add("sfdGz", typeof(Decimal));
                DataColumn sfdBx = iReply.Columns.Add("sfdBx", typeof(Decimal));
                DataColumn sfdBy = iReply.Columns.Add("sfdBy", typeof(Decimal));
                DataColumn sfdBz = iReply.Columns.Add("sfdBz", typeof(Decimal));
                DataColumn cTime = iReply.Columns.Add("cTime", typeof(DateTime));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [crrID], [srvyRowID], [crrDepth], [crrGx], [crrGy], [crrGz], [crrBx], [crrBy], [crrBz], [cTime], [uTime] FROM [CorrectionFormattedData] WHERE [RunID] = @RunID AND [wswID] = @wswID AND [welID] = @welID ORDER BY [srvyRowID] DESC";
                Int32 id = 0, sid = 0;
                Decimal dpth = -99.00M, gx = -99.000000000000M, gy = -99.000000000000M, gz = -99.000000000000M, bx = -99.000000000000M, by = -99.000000000000M, bz = -99.000000000000M;
                DateTime uT = DateTime.Now, cT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@RunID", SqlDbType.Int).Value = RunID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        sid = readData.GetInt32(1);
                                        dpth = readData.GetDecimal(2);
                                        gx = readData.GetDecimal(3);
                                        gy = readData.GetDecimal(4);
                                        gz = readData.GetDecimal(5);
                                        bx = readData.GetDecimal(6);
                                        by = readData.GetDecimal(7);
                                        bz = readData.GetDecimal(8);
                                        cT = readData.GetDateTime(9);
                                        uT = readData.GetDateTime(10);
                                        iReply.Rows.Add(id, sid, dpth, gx, gy, gz, bx, by, bz, cT, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteCorrectionDataFileHeader(Int32 FileID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [CorrectionDataFileHeader] WHERE [cfID] = @cfID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@cfID", SqlDbType.Int).Value = FileID.ToString();
                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteCorrectionDataFileName(Int32 FileID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [CorrectionDataFileName] WHERE [cfID] = @cfID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@cfID", SqlDbType.Int).Value = FileID.ToString();
                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply; 
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteRawQCResults(Int32 SurveyRowID, Decimal SurveyDepth, Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 ServiceID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [RawQCResults] WHERE [srvID] = @srvID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID AND [srvyID] = @srvyID AND [Depth] = @Depth";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        delData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();                        
                        delData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();                        
                        delData.Parameters.AddWithValue("@Depth", SqlDbType.Decimal).Value = SurveyDepth.ToString();
                        delData.Parameters.AddWithValue("@srvyID", SqlDbType.Int).Value = SurveyRowID.ToString();
                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteAllRawQCResults(Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 ServiceID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [RawQCResults] WHERE [srvID] = @srvID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        delData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        delData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteAllRawQCResults(Int32 SurveyRowID, Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 ServiceID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [RawQCResults] WHERE [srvID] = @srvID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID AND [srvyID] = @srvyID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        delData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        delData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        delData.Parameters.AddWithValue("@srvyID", SqlDbType.Int).Value = SurveyRowID.ToString();
                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteSurveyCorrFormattedData(Decimal Depth, Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 ServiceID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [CorrectionFormattedData] WHERE [crrDepth] = @crrDepth AND [srvID] = @srvID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        delData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        delData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        delData.Parameters.AddWithValue("@crrDepth", SqlDbType.Decimal).Value = Depth.ToString();                                                
                        updateAccountingRawQC(Depth, RunID);
                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteAllSurveyCorrFormattedData(Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 ServiceID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [CorrectionFormattedData] WHERE [srvID] = @srvID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        delData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        delData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        //updateAccountingRawQC(Depth, RunID);
                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteSurveyRawFormattedData(Decimal Depth, Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 ServiceID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [SurveyFormattedData] WHERE [sfdDepth] = @sfdDepth AND [srvID] = @srvID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        delData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        delData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        delData.Parameters.AddWithValue("@sfdDepth", SqlDbType.Decimal).Value = Depth.ToString();                                                
                        updateAccountingRawQC(Depth, RunID);
                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch(Exception ex)
            {throw new System.Exception(ex.ToString());}
        }

        public static Int32 deleteAllSurveyRawFormattedData(Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 ServiceID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [SurveyFormattedData] WHERE [srvID] = @srvID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        delData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        delData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        //updateAccountingRawQC(Depth, RunID);
                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteSurveyResFormattedData(Decimal Depth, Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 ServiceID,  String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [SurveyResultsData] WHERE [srdDepth] = @srdDepth [srvID] = @srvID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        delData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        delData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        delData.Parameters.AddWithValue("@srdDepth", SqlDbType.Decimal).Value = Depth.ToString();
                        using (SqlDataReader readData = delData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteAllSurveyResFormattedData(Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 ServiceID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [SurveyResultsData] WHERE [srvID] = @srvID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        delData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        delData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = delData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 updateAccountingRawQC(Decimal SurveyDepth, Int32 RunID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "UPDATE [accRawQC] SET [delFlag] = @delFlag WHERE ([depth] = @depth AND [runID] = @runID)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEAccountingDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand updData = new SqlCommand(selData, dataCon);
                        updData.Parameters.AddWithValue("@delFlag", 1);
                        updData.Parameters.AddWithValue("@depth", SurveyDepth);
                        updData.Parameters.AddWithValue("@runID", RunID);
                        iReply = Convert.ToInt32(updData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 updateAccountingResQC(Decimal SurveyDepth, Int32 RunID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "UPDATE [accResQC] SET [delFlag] = @delFlag WHERE ([depth] = @depth AND [runID] = @runID)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEAccountingDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand updData = new SqlCommand(selData, dataCon);
                        updData.Parameters.AddWithValue("@delFlag", 1);
                        updData.Parameters.AddWithValue("@depth", SurveyDepth);
                        updData.Parameters.AddWithValue("@runID", RunID);
                        iReply = Convert.ToInt32(updData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteWPCalcData(Decimal Depth, Int32 RunID, Int32 WellSectionID, Int32 WellID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [WellPlanCalculatedData] WHERE [md] = @md AND [runID] = @runID AND [wsID] = @wsID and [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@md", SqlDbType.Int).Value = Depth.ToString();
                        delData.Parameters.AddWithValue("@runID", SqlDbType.Decimal).Value = RunID.ToString();
                        delData.Parameters.AddWithValue("@wsID", SqlDbType.Int).Value = WellSectionID.ToString();
                        delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        updateAccountingRawQC(Depth, RunID);
                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        internal static int deleteSorveyCorrAnalysisData(Decimal Depth, Int32 RunID, Int32 WellSectionID, Int32 WellID, String GetClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [CorrectionInputToAnalysis] WHERE [ciaDepth] = @ciaDepth AND [runID] = @runID AND [wswID] = @wswID AND [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(GetClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@ciaDepth", SqlDbType.Decimal).Value = Depth.ToString();
                        delData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        delData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        updateAccountingRawQC(Depth, RunID);
                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        internal static int deleteAllSurveyCorrAnalysisData(Int32 RunID, Int32 WellSectionID, Int32 WellID, String GetClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [CorrectionInputToAnalysis] WHERE [runID] = @runID AND [wswID] = @wswID AND [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(GetClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        delData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        //updateAccountingRawQC(Depth, RunID);
                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 delete3rdSurveyRawFormattedData(Decimal Depth, Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 ServiceID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [3rdSurveyFormattedData] WHERE [sfdDepth] = @sfdDepth AND [srvID] = @srvID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        delData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        delData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        delData.Parameters.AddWithValue("@sfdDepth", SqlDbType.Decimal).Value = Depth.ToString();
                        updateAccountingRawQC(Depth, RunID);
                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteAll3rdSurveyRawFormattedData(Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 ServiceID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [3rdSurveyFormattedData] WHERE [srvID] = @srvID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        delData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        delData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        //updateAccountingRawQC(Depth, RunID);
                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 delete3rdSurveyResFormattedData(Decimal Depth, Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 ServiceID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [3rdSurveyResultsData] WHERE [srdDepth] = @srdDepth [srvID] = @srvID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        delData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        delData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        delData.Parameters.AddWithValue("@srdDepth", SqlDbType.Decimal).Value = Depth.ToString();
                        using (SqlDataReader readData = delData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteAll3rdSurveyResFormattedData(Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 ServiceID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [3rdSurveyResultsData] WHERE [srvID] = @srvID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        delData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        delData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = delData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteHistSurveyRawFormattedData(Decimal Depth, Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 ServiceID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [HistSurveyFormattedData] WHERE [sfdDepth] = @sfdDepth AND [srvID] = @srvID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        delData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        delData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        delData.Parameters.AddWithValue("@sfdDepth", SqlDbType.Decimal).Value = Depth.ToString();
                        updateAccountingRawQC(Depth, RunID);
                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteAllHistSurveyRawFormattedData(Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 ServiceID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [HistSurveyFormattedData] WHERE [sfdDepth] = @sfdDepth AND [srvID] = @srvID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        delData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        delData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        //updateAccountingRawQC(Depth, RunID);
                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteHistSurveyResFormattedData(Decimal Depth, Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 ServiceID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [HistSurveyResultsData] WHERE [sfdDepth] = @sfdDepth AND [srvID] = @srvID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        delData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        delData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        delData.Parameters.AddWithValue("@sfdDepth", SqlDbType.Decimal).Value = Depth.ToString();
                        updateAccountingRawQC(Depth, RunID);
                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteAllHistSurveyResFormattedData(Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 ServiceID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [HistSurveyResultsData] WHERE AND [srvID] = @srvID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        delData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        delData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        //updateAccountingRawQC(Depth, RunID);
                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteWPSurveyResFormattedData(Decimal Depth, Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 ServiceID, String ClientDBConnection)
        {
            try
            {
                throw new NotImplementedException();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteAllWPSurveyResFormattedData(Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 ServiceID, String ClientDBConnection)
        {
            try
            {
                throw new NotImplementedException();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteIncDipCorrFormattedData(Decimal Depth, Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 ServiceID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [IncDipCorrFormattedData] WHERE [crrDepth] = @crrDepth AND [srvID] = @srvID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        delData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        delData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        delData.Parameters.AddWithValue("@crrDepth", SqlDbType.Decimal).Value = Depth.ToString();
                        updateAccountingRawQC(Depth, RunID);
                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteAllIncDipCorrFormattedData(Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 ServiceID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [IncDipCorrFormattedData] WHERE [srvID] = @srvID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        delData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        delData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        //updateAccountingRawQC(Depth, RunID);
                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteIncDipCorrInputToAnalysis(Decimal Depth, Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 ServiceID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [IncDipCorrInputToAnalysis] WHERE [ciaDepth] = @ciaDepth AND [srvID] = @srvID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        delData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        delData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        delData.Parameters.AddWithValue("@ciaDepth", SqlDbType.Decimal).Value = Depth.ToString();
                        updateAccountingRawQC(Depth, RunID);
                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteAllIncDipCorrInputToAnalysis(Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 ServiceID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [IncDipCorrInputToAnalysis] WHERE [ciaDepth] = @ciaDepth AND [srvID] = @srvID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        delData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        delData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        //updateAccountingRawQC(Depth, RunID);
                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        internal static int deleteWellPlacementData(Int32 DataRowID, Decimal sDepth, Int32 WellID, Int32 WellPadID, Int32 ClientID, Int32 OperatorID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [WellPlacementData] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID AND [welID] = @welID AND [md] = @md AND [datarowID] = @datarowID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        delData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        delData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        delData.Parameters.AddWithValue("@md", SqlDbType.Int).Value = sDepth.ToString();
                        delData.Parameters.AddWithValue("@datarowID", SqlDbType.Int).Value = DataRowID.ToString();
                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        internal static Int32 deleteAllWellPlacementData(Int32 WellID, Int32 WellPadID, Int32 ClientID, Int32 OperatorID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "DELETE FROM [WellPlacementData] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID AND [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delData = new SqlCommand(selData, dataCon);
                        delData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        delData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        delData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}