﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using UNT = SmartsVer1.Helpers.DemogHelpers.UnitsConverter;

namespace SmartsVer1.Helpers.QCHelpers
{
    public class WellPlan
    {
        static void Main()
        { }        

        public static Decimal getTVD(Int32 DataRowID, Decimal TieInTVD, Decimal DeltaTVD, Decimal PreviousTVD)
        {
            try
            {
                if (DataRowID.Equals(1))
                {
                    return Decimal.Add(TieInTVD, DeltaTVD);
                }
                else
                {
                    return Decimal.Add(DeltaTVD, PreviousTVD);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal getVerticalSection(Int32 DataRowID, Decimal TieInVerticalSection, Decimal PreviousVerticalSection, Decimal DeltaVerticalSection)
        {
            try
            {
                if (DataRowID.Equals(1))
                {
                    return Decimal.Add(TieInVerticalSection, DeltaVerticalSection);
                }
                else
                {
                    return Decimal.Add(PreviousVerticalSection, DeltaVerticalSection);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal getNorthing(Int32 DataRowID, Decimal WPMetaInfoNorthing, Decimal PreviousNorthing, Decimal DeltaNorth)
        {
            try
            {
                if (DataRowID.Equals(1))
                {
                    return WPMetaInfoNorthing;
                }
                else
                {
                    return Decimal.Add(PreviousNorthing, DeltaNorth);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal getEasting(Int32 DataRowID, Decimal WPMetaInfoEasting, Decimal PreviousEasting, Decimal DeltaEast)
        {
            try
            {
                if (DataRowID.Equals(1))
                {
                    return WPMetaInfoEasting;
                }
                else
                {
                    return Decimal.Add(PreviousEasting, DeltaEast);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal getMeasuredDepthDiff(Int32 DataRowID, Decimal PlanTieInMeasuredDepth, Decimal CurrentDepth, Decimal PreviousDepth)
        {            
            if (DataRowID.Equals(1)) { return Decimal.Subtract(CurrentDepth, PlanTieInMeasuredDepth); }
            else { return Decimal.Subtract(CurrentDepth, PreviousDepth); }
        }

        public static Decimal getTieInMeasuredDepth(Decimal dataDepth, Decimal convNonMagSpaceBelowSensor, Decimal convMotorPlusDrillBitLength)
        {            
            Decimal iReply = -99.00M;
            iReply = dataDepth + convNonMagSpaceBelowSensor + convMotorPlusDrillBitLength;
            return iReply;
        }

        public static Decimal getD1(Int32 DataRowID, Decimal CurrentInclination, Decimal PreviousInclination, Decimal CurrentAzimuth, Decimal PreviousAzimuth)
        {            
            if (DataRowID.Equals(1)) { return 0.00M; }
            else { return (Decimal)(Math.Acos(Math.Cos(UNT.ToRadians((Double)CurrentInclination - (Double)PreviousInclination)) - (Math.Sin(UNT.ToRadians((Double)PreviousInclination)) * Math.Sin(UNT.ToRadians((Double)CurrentInclination)) * (1 - Math.Cos(UNT.ToRadians((Double)CurrentAzimuth - (Double)PreviousAzimuth)))))); }
        }

        public static Decimal getDogLeg1(Decimal colD1Value)
        {
            return (Decimal)UNT.ToDegrees((Double)colD1Value);
        }

        public static Decimal getRF(Int32 DataRowID, Decimal colD1Value)
        {            
            if(DataRowID.Equals(1)) { return 0.00M; }
            else if(colD1Value.Equals(0)) { return 1.00M; }
            else { return (2 / colD1Value) * (Decimal)Math.Tan((Double)colD1Value / 2); }
        }

        public static Decimal getDeltaTVD(Int32 DataRowID, Decimal DeltaMDValue, Decimal CurrentInclination, Decimal PreviousInclination, Decimal RFValue)
        {            
            if(DataRowID.Equals(1))
            { return DeltaMDValue * (Decimal)Math.Cos(UNT.ToRadians((Double)CurrentInclination)); }
            else
            {return (DeltaMDValue/2)*(Decimal)(Math.Cos(UNT.ToRadians((Double)PreviousInclination)) + Math.Cos(UNT.ToRadians((Double)CurrentInclination))) * RFValue;}
        }

        public static Decimal getCummulativeDeltaNorthing(Int32 DataRowID, Decimal TieInNorth, Decimal PreviousCummulativeDeltaNorthing, Decimal DeltaNorthing)
        {            
            if (DataRowID.Equals(1)) { return Decimal.Add(TieInNorth, DeltaNorthing); } else { return Decimal.Add(PreviousCummulativeDeltaNorthing, DeltaNorthing); }
        }

        public static Decimal getDeltaNorthing(Int32 DataRowID, Decimal DeltaTVD, Decimal DeltaMeasuredDepth, Decimal CurrentInclination, Decimal PreviousInclination, Decimal CurrentAzimuth, Decimal PreviousAzimuth, Decimal RFValue)
        {            
            if (DataRowID.Equals(1))
            {
                return (DeltaTVD * (Decimal)Math.Cos(UNT.ToRadians((Double)CurrentAzimuth)) * (Decimal)(Math.Sin(UNT.ToRadians((Double)CurrentInclination))));
            }
            else
            {
                return (DeltaMeasuredDepth / 2) * (((Decimal)Math.Sin(UNT.ToRadians((Double)CurrentInclination)) * (Decimal)Math.Cos(UNT.ToRadians((Double)CurrentAzimuth))) + ((Decimal)Math.Sin(UNT.ToRadians((Double)PreviousInclination)) * (Decimal)Math.Cos(UNT.ToRadians((Double)PreviousAzimuth)))) * RFValue;
            }
        }

        public static Decimal getCummulativeDeltaEasting(Int32 DataRowID, Decimal TieInEasting, Decimal DeltaEasting, Decimal PreviousCummulativeDeltaEasting)
        {            
            if (DataRowID.Equals(1)) { return TieInEasting; } else { return Decimal.Add(DeltaEasting, PreviousCummulativeDeltaEasting); }
        }

        public static Decimal getDeltaEasting(Int32 DataRowID, Decimal CurrentDeltaMeasuredDepth, Decimal DeltaTVD, Decimal PreviousInclination, Decimal CurrentInclination, Decimal PreviousAzimuth, Decimal CurrentAzimuth, Decimal FCValue)
        {            
            if (DataRowID.Equals(1))
            { return DeltaTVD * (Decimal)Math.Sin(UNT.ToRadians( (Double)CurrentInclination)) * (Decimal)Math.Sin(UNT.ToRadians( (Double)CurrentAzimuth)); } 
            else 
            { return (CurrentDeltaMeasuredDepth / 2) * (Decimal)((Math.Sin(UNT.ToRadians((Double)CurrentInclination)) * Math.Sin(UNT.ToRadians((Double)CurrentAzimuth))) + (Math.Sin(UNT.ToRadians((Double)PreviousInclination)) * Math.Sin(UNT.ToRadians((Double)PreviousAzimuth)))) * FCValue; }
        }

        public static Decimal getDeltaVerticalSection(Decimal MetaInfoVerticalSection, Decimal ClosureDistance, Decimal ClosureDirectAngle2)
        {
            return ClosureDistance * (Decimal)Math.Cos(UNT.ToRadians((Double)(MetaInfoVerticalSection - ClosureDirectAngle2)));
        }

        public static Decimal getClosureDistance(Decimal cummDeltaNorth, Decimal cummDeltaEast)
        {
            return (Decimal)Math.Sqrt((Double)(cummDeltaNorth * cummDeltaNorth) + (Double)(cummDeltaEast * cummDeltaEast));
        }

        public static Decimal getColABValue(Decimal TieInNorth, Decimal TieInEast, Decimal cummDeltaNorthing, Decimal cummDeltaEasting, Decimal ClosureDistance)
        {
            if( ( ClosureDistance.Equals(0.00M) ) || ( cummDeltaNorthing - TieInNorth ).Equals( 0.00M ) )
            { return 90.00M; }
            else
            { return (Decimal)UNT.ToDegrees(Math.Atan((Double)(cummDeltaEasting - TieInEast) / (Double)(cummDeltaNorthing - TieInNorth))); }
        }

        public static Decimal getColACValue(Decimal TieInNorth, Decimal TieInEast, Decimal cummDeltaNorthing, Decimal cummDeltaEasting, Decimal ClosureDirectAngle1)
        {
            if ((cummDeltaEasting - TieInEast) >= 0.00M)
            { if (ClosureDirectAngle1 > 0.00M) { return ClosureDirectAngle1; } else { return (180.00M + ClosureDirectAngle1); } }
            else
            { if ((cummDeltaNorthing - TieInNorth) < 0.00M) { return (180.00M + ClosureDirectAngle1); } else { return (360.00M + ClosureDirectAngle1); } }
        }

        public static Decimal getDogleg(Int32 DataRowID, Decimal CurrentInclination, Decimal PreviousInclination, Decimal CurrentAzimuth, Decimal PreviousAzimuth)
        {
            if (DataRowID.Equals(1)) { return 0.00M; } else { return Convert.ToDecimal(UNT.ToDegrees(Math.Acos(Math.Cos(UNT.ToRadians((Double)CurrentInclination - (Double)PreviousInclination)) - (Math.Sin(UNT.ToRadians((Double)PreviousInclination)) * Math.Sin(UNT.ToRadians((Double)CurrentInclination)) * (1 - Math.Cos(UNT.ToRadians((Double)CurrentAzimuth - (Double)PreviousAzimuth))))))); }
        }

        public static Decimal getDLS(Int32 DataRowID, Int32 MetaLengthUnit,  Decimal DeltaMeasuredDepth, Decimal Dogleg) //Dogleg (degree/30 m)
        {
            
            if (DataRowID.Equals(1))
            {
                return 0.00M;
            }
            else if (DeltaMeasuredDepth.Equals(0.00M))
            {
                return 0.00M;
            }
            else if (MetaLengthUnit.Equals(5))
            {
                return (30 / DeltaMeasuredDepth) * Dogleg;
            }
            else
            {
                return (100 / DeltaMeasuredDepth) * Dogleg;
            }
        }

        public static Decimal getSubsea(Decimal TieInSubSea, Decimal CurrentTVD)
        {            
            return Decimal.Add(CurrentTVD , TieInSubSea);
        }

        public static Int32 insertWellPlanCalculations(Decimal MeasuredDepth, Decimal Inclination, Decimal Azimuth, Decimal TVD, Decimal VS, Decimal Northing, Decimal Easting, Decimal DeltaMD, Decimal D1, Decimal DogLeg1, Decimal RF, Decimal deltaTVD, Decimal cummDeltaNorthing, Decimal deltaNorthing, Decimal CummDeltaEasting, Decimal deltaEasting, Decimal deltaVS, Decimal ClosureDistance, Decimal closureDirectAngle1, Decimal closureDirectAngle2, Decimal dogleg, Decimal dogleg100, Decimal subsea, Int32 OperatorID, Int32 wellpadID, Int32 welID, Int32 datarowID, Int32 WellPlanDataFileID, String UserName, String UserRealm, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String insertValues = "INSERT INTO [WellPlanCalculatedData] ([md], [inc], [azm], [tvd], [vs], [northing], [easting], [deltaMD], [d1], [dogleg1], [rf], [deltaTVD], [cummDeltaNorthing], [deltaNorthing], [cummDeltaEasting], [deltaEasting], [deltaVS], [closureDistance], [closureDirectAngle1], [closureDirectAngle2], [dogleg], [dogleg100], [subsea], [optrID], [wpdID], [welID], [datarowID], [wpfID], [username], [realm], [cTime], [uTime]) VALUES (@md, @inc, @azm, @tvd, @vs, @northing, @easting, @deltaMD, @d1, @dogleg1, @rf, @deltaTVD, @cummDeltaNorthing, @deltaNorthing, @cummDeltaEasting, @deltaEasting, @deltaVS, @closureDistance, @closureDirectAngle1, @closureDirectAngle2, @dogleg, @dogleg100, @subsea, @optrID, @wpdID, @welID, @datarowID, @wpfID, @username, @realm, @cTime, @uTime)";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(insertValues, dataCon);
                        insData.Parameters.AddWithValue("@md", SqlDbType.Decimal).Value = MeasuredDepth.ToString();
                        insData.Parameters.AddWithValue("@inc", SqlDbType.Decimal).Value = Inclination.ToString();
                        insData.Parameters.AddWithValue("@azm", SqlDbType.Decimal).Value = Azimuth.ToString();
                        insData.Parameters.AddWithValue("@tvd", SqlDbType.Decimal).Value = TVD.ToString();
                        insData.Parameters.AddWithValue("@vs", SqlDbType.Decimal).Value = VS.ToString();
                        insData.Parameters.AddWithValue("@northing", SqlDbType.Decimal).Value = Northing.ToString();
                        insData.Parameters.AddWithValue("@easting", SqlDbType.Decimal).Value = Easting.ToString();
                        insData.Parameters.AddWithValue("@deltaMD", SqlDbType.Decimal).Value = DeltaMD.ToString();
                        insData.Parameters.AddWithValue("@d1", SqlDbType.Decimal).Value = D1.ToString();
                        insData.Parameters.AddWithValue("@dogleg1", SqlDbType.Decimal).Value = DogLeg1.ToString();
                        insData.Parameters.AddWithValue("@rf", SqlDbType.Decimal).Value = RF.ToString();
                        insData.Parameters.AddWithValue("@deltaTVD", SqlDbType.Decimal).Value = deltaTVD.ToString();
                        insData.Parameters.AddWithValue("@cummDeltaNorthing", SqlDbType.Decimal).Value = cummDeltaNorthing.ToString();
                        insData.Parameters.AddWithValue("@deltaNorthing", SqlDbType.Decimal).Value = deltaNorthing.ToString();
                        insData.Parameters.AddWithValue("@cummDeltaEasting", SqlDbType.Decimal).Value = CummDeltaEasting.ToString();
                        insData.Parameters.AddWithValue("@deltaEasting", SqlDbType.Decimal).Value = deltaEasting.ToString();
                        insData.Parameters.AddWithValue("@deltaVS", SqlDbType.Decimal).Value = deltaVS.ToString();
                        insData.Parameters.AddWithValue("@closureDistance", SqlDbType.Decimal).Value = ClosureDistance.ToString();
                        insData.Parameters.AddWithValue("@closureDirectAngle1", SqlDbType.Decimal).Value = closureDirectAngle1.ToString();
                        insData.Parameters.AddWithValue("@closureDirectAngle2", SqlDbType.Decimal).Value = closureDirectAngle2.ToString();
                        insData.Parameters.AddWithValue("@dogleg", SqlDbType.Decimal).Value = dogleg.ToString();
                        insData.Parameters.AddWithValue("@dogleg100", SqlDbType.Decimal).Value = dogleg100.ToString();
                        insData.Parameters.AddWithValue("@subsea", SqlDbType.Decimal).Value = subsea.ToString();
                        insData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        insData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = wellpadID.ToString();
                        insData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = welID.ToString();
                        insData.Parameters.AddWithValue("@datarowID", SqlDbType.Int).Value = datarowID.ToString();
                        insData.Parameters.AddWithValue("@wpfID", SqlDbType.Int).Value = WellPlanDataFileID.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserRealm.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();

                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertWellPlanMetaInfo(Int32 WellPadID, Int32 WellID, Int32 DatumID, Int32 WellPlanDepthReferenceID, Decimal WellPlanElevation, Int32 WellPlanElevationUnitID, Decimal WellPlanAzimuth, Int32 LengthUnitID, Decimal WellPlanNorthing, Int32 NorthingUnit, Decimal WellPlanEasting, Int32 EastingUnit, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String insData = "INSERT INTO [WellPlanMetaInfo] ([wpdID], [welID], [datID], [wpdrID], [wpiElev], [wpiElevUnit], [luID], [wpiAzm], [wpiNorthing], [wpiNorthingUnit], [wpiEasting], [wpiEastingUnit]) VALUES (@wpdID, @welID, @datID, @wpdrID, @wpiElev, @wpiElevUnit, @luID, @wpiAzm, @wpiNorthing, @wpiNorthingUnit, @wpiEasting, @wpiEastingUnit)";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insertCommand = new SqlCommand(insData, dataCon);
                        insertCommand.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        insertCommand.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        insertCommand.Parameters.AddWithValue("@datID", SqlDbType.Int).Value = DatumID.ToString();
                        insertCommand.Parameters.AddWithValue("@wpdrID", SqlDbType.Int).Value = WellPlanDepthReferenceID.ToString();
                        insertCommand.Parameters.AddWithValue("@wpiElev", SqlDbType.Decimal).Value = WellPlanElevation.ToString();
                        insertCommand.Parameters.AddWithValue("@wpiElevUnit", SqlDbType.Int).Value = WellPlanElevationUnitID.ToString();
                        insertCommand.Parameters.AddWithValue("@luID", SqlDbType.Int).Value = WellPlanElevationUnitID.ToString();
                        insertCommand.Parameters.AddWithValue("@wpiAzm", SqlDbType.Decimal).Value = WellPlanAzimuth.ToString();
                        insertCommand.Parameters.AddWithValue("@wpiNorthing", SqlDbType.Decimal).Value = WellPlanNorthing.ToString();
                        insertCommand.Parameters.AddWithValue("@wpiNorthingUnit", SqlDbType.Int).Value = NorthingUnit.ToString();
                        insertCommand.Parameters.AddWithValue("@wpiEasting", SqlDbType.Decimal).Value = WellPlanEasting.ToString();
                        insertCommand.Parameters.AddWithValue("@wpiEastingUnit", SqlDbType.Int).Value = EastingUnit.ToString();
                        iReply = Convert.ToInt32(insertCommand.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable processWPQCData(Int32 RunID, Int32 WellSectionID, Int32 WellID, Decimal ReferenceElevation, Decimal VertSecAzimuth, Int32 LengthUnit, System.Data.DataTable WellPlanTable, String UserName, String UserRealm, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn md = iReply.Columns.Add("md", typeof(Decimal));
                DataColumn inc = iReply.Columns.Add("inc", typeof(Decimal));
                DataColumn azm = iReply.Columns.Add("azm", typeof(Decimal));
                DataColumn tvd = iReply.Columns.Add("tvd", typeof(Decimal));
                DataColumn vs = iReply.Columns.Add("vs", typeof(Decimal));
                DataColumn northing = iReply.Columns.Add("northing", typeof(Decimal));
                DataColumn easting = iReply.Columns.Add("easting", typeof(Decimal));
                DataColumn deltaMD = iReply.Columns.Add("deltaMD", typeof(Decimal));
                DataColumn d1 = iReply.Columns.Add("d1", typeof(Decimal));
                DataColumn d2 = iReply.Columns.Add("d2", typeof(Decimal));
                DataColumn fc = iReply.Columns.Add("fc", typeof(Decimal));
                DataColumn deltaTVD = iReply.Columns.Add("deltaTVD", typeof(Decimal));
                DataColumn cummNorthing = iReply.Columns.Add("cummNorthing", typeof(Decimal));
                DataColumn deltaNorth = iReply.Columns.Add("deltaNorth", typeof(Decimal));
                DataColumn cummEasting = iReply.Columns.Add("cummEasting", typeof(Decimal));
                DataColumn deltaEasting = iReply.Columns.Add("deltaEasting", typeof(Decimal));
                DataColumn deltaVS = iReply.Columns.Add("deltaVS", typeof(Decimal));
                DataColumn clsrDistance = iReply.Columns.Add("clsrDistance", typeof(Decimal));
                DataColumn deltaClsrDistance = iReply.Columns.Add("deltaClsrDistance", typeof(Decimal));
                DataColumn clsrDirectAngle1 = iReply.Columns.Add("clsrDirectAngle1", typeof(Decimal));
                DataColumn clsrDirectAngle2 = iReply.Columns.Add("clsrDirectAngle2", typeof(Decimal));
                DataColumn dogleg = iReply.Columns.Add("dogleg", typeof(Decimal));
                DataColumn dogleg100 = iReply.Columns.Add("dogleg100", typeof(Decimal));
                DataColumn subsea = iReply.Columns.Add("subsea", typeof(Decimal));
                DataColumn welID = iReply.Columns.Add("welID", typeof(Int32));
                DataColumn datarowID = iReply.Columns.Add("datarowID", typeof(Int32));
                DataColumn wpfID = iReply.Columns.Add("runID", typeof(Int32));
                //Tie In Survey Numbers
                ArrayList tieInList = new ArrayList();
                Int32 checkTI = -99, nextSeq = -99;                
                Decimal tiMD = 0.00M, tiInc = 0.00M, tiAzm = 0.00M, tiTVD = 0.00M, tiNorth = 0.00M, tiEast = 0.00M, tiVS = 0.00M, tiSubSea = 0.00M;
                Decimal nVal = 0.00M, eVal = 0.00M, FirstDataRowSubSeaValue = 0.00M;
                Decimal tvdVal = -99.000000000000M, vsVal = -99.000000000000M, subseaVal = -99.000000000000M;
                Decimal northingVal = -99.000000000000M, eastingVal = -99.000000000000M;
                Decimal deltaMDVal = -99.000000000000M, d1Val = -99.000000000000M, d2Val = -99.000000000000M, fcVal = -99.000000000000M, deltaTVDVal = -99.000000000000M;
                Decimal cumDeltaNorthingVal = -99.000000000000M, deltaNVal = -99.000000000000M, cumDeltaEastingVal = -99.000000000000M, deltaEVal = -99.000000000000M;
                Decimal deltaVSVal = -99.000000000000M, clDistVal = -99.000000000000M, deltaClDisVal = -99.000000000000M, clDirectAngleVal1 = -99.000000000000M, clDirectAngleVal2 = -99.000000000000M;
                Decimal dogLegVal = -99.000000000000M, dls100Val = -99.000000000000M, dataMD = -99.00M, dataInc = -99.00M, dataAzm = -99.00M;
                Decimal PreviousDepth = 0.00M, PreviousTVD = 0.00M, PreviousInclination = 0.000000000000M, PreviousAzimuth = 0.000000000000M, PreviousCumDeltaNorth = 0.000000000000M, PreviousCumDeltaEast = 0.000000000000M;
                Decimal PreviousClosureDistance = 0.000000000000M, PreviousVerticalSection = 0.000000000000M, PreviousNorthing = 0.000000000000M, PreviousEasting = 0.000000000000M, PreviousDeltaVerticalSection = 0.000000000000M;                
                checkTI = checkTieIn(RunID, WellSectionID, WellID, ClientDBString);
                if (checkTI.Equals(1))
                {
                    tieInList = getWellPlanTieInSurveyList(RunID, WellSectionID, WellID, ClientDBString);
                    tiMD = Convert.ToDecimal(tieInList[1]);
                    tiInc = Convert.ToDecimal(tieInList[2]);
                    tiAzm = Convert.ToDecimal(tieInList[3]);
                    tiTVD = Convert.ToDecimal(tieInList[4]);
                    tiNorth = Convert.ToDecimal(tieInList[5]);
                    tiEast = Convert.ToDecimal(tieInList[6]);
                    tiVS = Convert.ToDecimal(tieInList[7]);
                    tiSubSea = Convert.ToDecimal(tieInList[8]);
                }
                else
                {
                    tiMD = 0.000000000000M;
                    tiInc = 0.000000000000M;
                    tiAzm = 0.000000000000M;
                    tiTVD = 0.000000000000M;
                    tiNorth = 0.000000000000M;
                    tiEast = 0.000000000000M;
                    tiVS = 0.000000000000M;
                    tiSubSea = 0.000000000000M;
                }

                foreach (DataRow row in WellPlanTable.Rows)
                {
                    nextSeq = Convert.ToInt32(row[0]);
                    dataMD = Convert.ToDecimal(row[1]);
                    dataInc = Convert.ToDecimal(row[2]);
                    dataAzm = Convert.ToDecimal(row[3]);
                    nVal = Convert.ToDecimal(row[13]);
                    eVal = Convert.ToDecimal(row[14]);
                    deltaMDVal = getMeasuredDepthDiff(nextSeq, tiMD, dataMD, PreviousDepth);
                    d1Val = getD1(nextSeq, dataInc, PreviousInclination, dataAzm, PreviousAzimuth);
                    d2Val = getDogLeg1(d1Val);
                    fcVal = getRF(nextSeq, d1Val);
                    deltaTVDVal = getDeltaTVD(nextSeq, deltaMDVal, dataInc, PreviousInclination, fcVal);
                    deltaNVal = getDeltaNorthing(nextSeq, deltaTVDVal, deltaMDVal, dataInc, PreviousInclination, dataAzm, PreviousAzimuth, fcVal);
                    cumDeltaNorthingVal = getCummulativeDeltaNorthing(nextSeq, tiNorth, PreviousCumDeltaNorth, deltaNVal);
                    deltaEVal = getDeltaEasting(nextSeq, deltaMDVal, deltaTVDVal, PreviousInclination, dataInc, PreviousAzimuth, dataAzm, fcVal);
                    cumDeltaEastingVal = getCummulativeDeltaEasting(nextSeq, tiEast, deltaEVal, PreviousCumDeltaEast);
                    clDistVal = getClosureDistance(deltaNVal, deltaEVal);
                    clDirectAngleVal1 = getColABValue(tiNorth, tiEast, cumDeltaNorthingVal, cumDeltaEastingVal, clDistVal);
                    clDirectAngleVal2 = getColACValue(tiNorth, tiEast, cumDeltaNorthingVal, cumDeltaEastingVal, clDirectAngleVal1);
                    deltaVSVal = getDeltaVerticalSection(VertSecAzimuth, clDistVal, clDirectAngleVal2);
                    dogLegVal = getDogleg(nextSeq, dataInc, PreviousInclination, dataAzm, PreviousAzimuth);
                    dls100Val = getDLS(nextSeq, LengthUnit, deltaMDVal, dogLegVal);
                    tvdVal = getTVD(nextSeq, tiTVD, deltaTVDVal, PreviousTVD);
                    vsVal = getVerticalSection(nextSeq, tiVS, PreviousVerticalSection, deltaVSVal);
                    subseaVal = getSubsea(tiSubSea, tvdVal);
                    northingVal = getNorthing(nextSeq, nVal, PreviousNorthing, deltaNVal);
                    eastingVal = getEasting(nextSeq, eVal, PreviousEasting, deltaEVal);
                    iReply.Rows.Add(dataMD, dataInc, dataAzm, tvdVal, vsVal, northingVal, eastingVal, deltaMDVal, d1Val, d2Val, fcVal, deltaTVDVal, cumDeltaNorthingVal, deltaNVal, cumDeltaEastingVal, deltaEVal, deltaVSVal, clDistVal, deltaClDisVal, clDirectAngleVal1, clDirectAngleVal2, dogLegVal, dls100Val, subseaVal, WellID, nextSeq, RunID);
                    iReply.AcceptChanges();
                    PreviousDepth = dataMD;
                    PreviousInclination = dataInc;
                    PreviousAzimuth = dataAzm;
                    PreviousTVD = tvdVal;
                    PreviousCumDeltaNorth = cumDeltaNorthingVal;
                    PreviousCumDeltaEast = cumDeltaEastingVal;
                    PreviousClosureDistance = clDistVal;
                    PreviousDeltaVerticalSection = deltaVSVal;
                    PreviousVerticalSection = vsVal;
                    PreviousNorthing = northingVal;
                    PreviousEasting = eastingVal;
                    if (nextSeq.Equals(1)) { FirstDataRowSubSeaValue = subseaVal; }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 checkTieIn(Int32 RunID, Int32 WellSectionID, Int32 WellID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT TOP 1 1 FROM [WellPlanTieIn] WHERE [runID] = @runID AND [wsID] = @wsID AND [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        getData.Parameters.AddWithValue("@wsID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using(SqlDataReader readData = getData.ExecuteReader())
                        {
                            if(readData.HasRows) //Table has data for selected Run
                            {
                                while(readData.Read())
                                {
                                    iReply = readData.GetInt32(0);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                if (iReply.Equals(1) || iReply > 1)
                {
                    iReply = 1; //Found Data
                }
                else
                {
                    iReply = -1; //Not Found Data
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable processWPData(Int32 OperatorID, Int32 ClientID, Int32 WellPlanDataFileID, Int32 WellPadID, Int32 WellID, ArrayList PlanMetaInfo, ArrayList PlanTieInSurvey, System.Data.DataTable WellPlanTable, String UserName, String UserRealm, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn md = iReply.Columns.Add("md", typeof(Decimal));
                DataColumn inc = iReply.Columns.Add("inc", typeof(Decimal));
                DataColumn azm = iReply.Columns.Add("azm", typeof(Decimal));
                DataColumn tvd = iReply.Columns.Add("tvd", typeof(Decimal));
                DataColumn vs = iReply.Columns.Add("vs", typeof(Decimal));
                DataColumn northing = iReply.Columns.Add("northing", typeof(Decimal));
                DataColumn easting = iReply.Columns.Add("easting", typeof(Decimal));
                DataColumn deltaMD = iReply.Columns.Add("deltaMD", typeof(Decimal));
                DataColumn d1 = iReply.Columns.Add("d1", typeof(Decimal));
                DataColumn dogleg1 = iReply.Columns.Add("dogleg1", typeof(Decimal));
                DataColumn rf = iReply.Columns.Add("rf", typeof(Decimal));
                DataColumn deltaTVD = iReply.Columns.Add("deltaTVD", typeof(Decimal));                
                DataColumn cummNorthing = iReply.Columns.Add("cummNorthing", typeof(Decimal));
                DataColumn deltaNorthing = iReply.Columns.Add("deltaNorthing", typeof(Decimal));
                DataColumn cummEasting = iReply.Columns.Add("cummEasting", typeof(Decimal));
                DataColumn deltaEasting = iReply.Columns.Add("deltaEasting", typeof(Decimal));
                DataColumn deltaVS = iReply.Columns.Add("deltaVS", typeof(Decimal));
                DataColumn ClsrDistance = iReply.Columns.Add("ClsrDistance", typeof(Decimal));
                DataColumn clsrDirectAngle1 = iReply.Columns.Add("clsrDirectAngle1", typeof(Decimal));
                DataColumn clsrDirectAngle2 = iReply.Columns.Add("clsrDirectAngle2", typeof(Decimal));
                DataColumn dogleg = iReply.Columns.Add("dogleg", typeof(Decimal));
                DataColumn dogleg100 = iReply.Columns.Add("dogleg100", typeof(Decimal));
                DataColumn subsea = iReply.Columns.Add("subsea", typeof(Decimal));
                DataColumn wpdID = iReply.Columns.Add("wpdID", typeof(Int32));
                DataColumn welID = iReply.Columns.Add("welID", typeof(Int32));
                DataColumn datarowID = iReply.Columns.Add("datarowID", typeof(Int32));
                DataColumn wpfID = iReply.Columns.Add("wpfID", typeof(Int32));
                //Tie In Survey Numbers
                Int32 tiMDUnit = -99, tiTVDUnit = -99, tiNOffUnit = -99, tiEOffUnit = -99, tiVSUnit = -99, tiDGUnit = -99, tiBRUnit = -99, tiTRUnit = -99, tiSSUnit = -99;
                Decimal tiMD = -99.00M, tiTVD = -99.00M, tiNorth = -99.00M, tiEast = -99.00M, tiVS = -99.00M, tiDG = -99.00M, tiTR = -99.00M, tiBR = -99.00M, tiSubSea = -99.00M;
                Decimal nVal = 0.00M, eVal = 0.00M;
                Int32 metaInfoSuccess = -99, tieinSucces = -99, metaDatum = -99, metaDepthRef = -99, metaDRElevUnit = -99, metaLengthUnit = -99, metaNorthingUnit = -99, metaEastingUnit = -99;
                Decimal tvdVal = -99.000000000000M, vsVal = -99.000000000000M, subseaVal = -99.000000000000M, metaVSection = -99.00000000M, metaNorth = -99.00000000M, metaEast = -99.00000000M;
                Decimal northingVal = -99.000000000000M, eastingVal = -99.000000000000M, PreviousVS = 0.000000000000M;
                Decimal deltaMDVal = -99.000000000000M, d1Val = -99.000000000000M, dogleg1Val = -99.000000000000M, rfVal = -99.000000000000M, deltaTVDVal = -99.000000000000M;
                Decimal cumDeltaNorthingVal = -99.000000000000M, deltaNVal = -99.000000000000M, cumDeltaEastingVal = -99.000000000000M, deltaEVal = -99.000000000000M;
                Decimal deltaVSVal = -99.000000000000M, clDistVal = -99.000000000000M, clDirectAngleVal1 = -99.000000000000M, clDirectAngleVal2 = -99.000000000000M;
                Decimal dogLegVal = -99.000000000000M, dls100Val = -99.000000000000M;
                Decimal dataMD = -99.00M, dataInc = -99.00M, dataAzm = -99.00M;
                Decimal convMD = -99.00M, convN = -99.00M, convE = -99.00M, FirstDataRowSubSeaValue = -99.00M;
                Decimal PreviousDepth = 0.00M, PreviousTVD = 0.00M, PreviousInclination = 0.000000000000M, PreviousAzimuth = 0.000000000000M, PreviousCumDeltaNorth = 0.000000000000M, PreviousCumDeltaEast = 0.000000000000M;
                Decimal PreviousVerticalSection = 0.000000000000M, PreviousNorthing = 0.000000000000M, PreviousEasting = 0.000000000000M;
                Int32 nextSeq = -99;
                Decimal ReferenceElevation = -99.00M;
                metaDatum = Convert.ToInt32(PlanMetaInfo[0]); //Permanent Datum (1: Mea Sea Level, 2: Ground Elevation)
                metaDepthRef = Convert.ToInt32(PlanMetaInfo[1]); //Depth Reference ID (3: Ground Elevation, 1: Kelly Bushing, 2: Rig Floor)
                metaDRElevUnit = Convert.ToInt32(PlanMetaInfo[2]); //Depth Reference Elevation Unit
                ReferenceElevation = Convert.ToDecimal(PlanMetaInfo[3]); //Reference Elevation                
                metaLengthUnit = Convert.ToInt32(PlanMetaInfo[4]); //Length Unit
                metaVSection = Convert.ToDecimal(PlanMetaInfo[5]); //Vertical Section Azimuth
                metaNorthingUnit = Convert.ToInt32(PlanMetaInfo[6]);
                metaNorth = Convert.ToDecimal(PlanMetaInfo[7]);                
                if (metaNorthingUnit.Equals(4)) { tiNorth = tiNorth * 0.3048M; }
                metaEastingUnit = Convert.ToInt32(PlanMetaInfo[8]);
                metaEast = Convert.ToDecimal(PlanMetaInfo[9]);                
                if (metaEastingUnit.Equals(4)) { tiEast = tiEast * 0.3048M; }
                //Insert Well Plan Meta Info
                metaInfoSuccess = insertWellPlanMetaInfo(WellPadID, WellID, metaDatum, metaDepthRef, ReferenceElevation, metaDRElevUnit, metaVSection, metaLengthUnit, metaNorth, metaNorthingUnit, metaEast, metaEastingUnit, ClientDBString);
                //Well Plan Tie-In values
                tiMDUnit = Convert.ToInt32(PlanTieInSurvey[0]); tiMD = Convert.ToDecimal(PlanTieInSurvey[1]);
                tiTVDUnit = Convert.ToInt32(PlanTieInSurvey[2]); tiTVD = Convert.ToDecimal(PlanTieInSurvey[3]);
                tiNOffUnit = Convert.ToInt32(PlanTieInSurvey[4]); tiNorth = Convert.ToDecimal(PlanTieInSurvey[5]);
                tiEOffUnit = Convert.ToInt32(PlanTieInSurvey[6]); tiEast = Convert.ToDecimal(PlanTieInSurvey[7]);
                tiVSUnit = Convert.ToInt32(PlanTieInSurvey[8]); tiVS = Convert.ToDecimal(PlanTieInSurvey[9]);
                tiDGUnit = Convert.ToInt32(PlanTieInSurvey[10]); tiDG = Convert.ToDecimal(PlanTieInSurvey[11]);
                tiBRUnit = Convert.ToInt32(PlanTieInSurvey[12]); tiBR = Convert.ToDecimal(PlanTieInSurvey[13]);
                tiTRUnit = Convert.ToInt32(PlanTieInSurvey[14]); tiTR = Convert.ToDecimal(PlanTieInSurvey[15]);
                tiSSUnit = Convert.ToInt32(PlanTieInSurvey[16]); tiSubSea = Convert.ToDecimal(PlanTieInSurvey[17]);
                tieinSucces = insertWellPlanTieInSurvey(OperatorID, ClientID, WellPadID, WellID, tiMD, tiMDUnit, tiTVD, tiTVDUnit, tiNorth, tiNOffUnit, tiEast, tiEOffUnit, tiVS, tiVSUnit, tiDG, tiDGUnit, tiBR, tiBRUnit, tiTR, tiTRUnit, tiSubSea, tiSSUnit, UserName, UserRealm, ClientDBString);
                foreach (DataRow row in WellPlanTable.Rows)
                {                    
                    nextSeq = Convert.ToInt32(row[0]);
                    dataMD = Convert.ToDecimal(row[1]);
                    if (tiMDUnit.Equals(4)) { convMD = dataMD * 0.3048M; } else { convMD = dataMD; }
                    dataInc = Convert.ToDecimal(row[2]);
                    dataAzm = Convert.ToDecimal(row[3]);
                    nVal = Convert.ToDecimal(row[13]);
                    if (tiNOffUnit.Equals(4)) { convN = nVal * 0.3048M; } else { convN = nVal; }
                    eVal = Convert.ToDecimal(row[14]);
                    if (tiEOffUnit.Equals(4)) { convE = eVal * 0.3048M; } else { convE = eVal; }
                    deltaMDVal = getMeasuredDepthDiff(nextSeq, tiMD, convMD, PreviousDepth);
                    d1Val = getD1(nextSeq, dataInc, PreviousInclination, dataAzm, PreviousAzimuth);
                    dogleg1Val = getDogLeg1(d1Val);
                    rfVal = getRF(nextSeq, d1Val);
                    deltaTVDVal = getDeltaTVD(nextSeq, deltaMDVal, dataInc, PreviousInclination, rfVal);
                    deltaNVal = getDeltaNorthing(nextSeq, deltaTVDVal, deltaMDVal, dataInc, PreviousInclination, dataAzm, PreviousAzimuth, rfVal);
                    cumDeltaNorthingVal = getCummulativeDeltaNorthing(nextSeq, tiNorth, PreviousCumDeltaNorth, deltaNVal);
                    deltaEVal = getDeltaEasting(nextSeq, deltaMDVal, deltaTVDVal, PreviousInclination, dataInc, PreviousAzimuth, dataAzm, rfVal);
                    cumDeltaEastingVal = getCummulativeDeltaEasting(nextSeq, tiEast, deltaEVal, PreviousCumDeltaEast);                    
                    clDistVal = getClosureDistance(cumDeltaNorthingVal, cumDeltaEastingVal);
                    clDirectAngleVal1 = getColABValue(tiNorth, tiEast, cumDeltaNorthingVal, cumDeltaEastingVal, clDistVal);
                    clDirectAngleVal2 = getColACValue(tiNorth, tiEast, cumDeltaNorthingVal, cumDeltaEastingVal, clDirectAngleVal1);
                    deltaVSVal = getDeltaVerticalSection(metaVSection, clDistVal, clDirectAngleVal2);
                    dogLegVal = getDogleg(nextSeq, dataInc, PreviousInclination, dataAzm, PreviousAzimuth);
                    dls100Val = getDLS(nextSeq, metaLengthUnit, deltaMDVal, dogLegVal);
                    tvdVal = getTVD(nextSeq, tiTVD, deltaTVDVal, PreviousTVD);
                    vsVal = getVerticalSection(nextSeq, tiVS, PreviousVerticalSection, deltaVSVal);
                    subseaVal = getSubsea(tiSubSea, tvdVal);
                    northingVal = getNorthing(nextSeq, convN, PreviousNorthing, deltaNVal);
                    eastingVal = getEasting(nextSeq, convE, PreviousEasting, deltaEVal);
                    iReply.Rows.Add(convMD, dataInc, dataAzm, tvdVal, vsVal, northingVal, eastingVal, deltaMDVal, d1Val, dogleg1Val, rfVal, deltaTVDVal, cumDeltaNorthingVal, deltaNVal, cumDeltaEastingVal, deltaEVal, deltaVSVal, clDistVal, clDirectAngleVal1, clDirectAngleVal2, dogLegVal, dls100Val, subseaVal, WellPadID, WellID, nextSeq, WellPlanDataFileID);
                    iReply.AcceptChanges();
                    insertWellPlanCalculations(convMD, dataInc, dataAzm, tvdVal, vsVal, northingVal, eastingVal, deltaMDVal, d1Val, dogleg1Val, rfVal, deltaTVDVal, cumDeltaNorthingVal, deltaNVal, cumDeltaEastingVal, deltaEVal, deltaVSVal, clDistVal, clDirectAngleVal1, clDirectAngleVal2, dogLegVal, dls100Val, subseaVal, OperatorID, WellPadID, WellID, nextSeq, WellPlanDataFileID, UserName, UserRealm, ClientDBString);
                    PreviousDepth = convMD;
                    PreviousInclination = dataInc;
                    PreviousAzimuth = dataAzm;
                    PreviousTVD = tvdVal;
                    PreviousCumDeltaNorth = cumDeltaNorthingVal;
                    PreviousCumDeltaEast = cumDeltaEastingVal;
                    PreviousVerticalSection = vsVal;
                    PreviousNorthing = northingVal;
                    PreviousEasting = eastingVal;
                    if (nextSeq.Equals(1)) { FirstDataRowSubSeaValue = subseaVal; }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static ArrayList processMetaInfo(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 PermanentDatumID, Int32 DepthReferenceID, Decimal DepthReferenceElevation, Int32 DepthReferenceElevationID, Decimal VerticalSectionAzimuth, Int32 LengthUnit, Decimal Northing, Int32 NorthingUnit, Decimal Easting, Int32 EastingUnit, String FileName, String FileExtension, List<String> UploadedFileData, String UserName, String UserRealm, String ClientDBString)
        {
            try
            {
                ArrayList iReply = new ArrayList(), Headlist = new ArrayList(), DetailList = new ArrayList(), rowValid = new ArrayList(), rowInvalid = new ArrayList(), infoArray = new ArrayList();
                Decimal[] gb_decimals = new Decimal[6];
                String[] detail;
                Int32 fileAdded = -99, nextSeq = 0, dataFileID = -99;
                System.Data.DataTable planTable = new System.Data.DataTable(), resultTable = new System.Data.DataTable(), nullValueTable = new System.Data.DataTable();
                DataColumn datarowID = planTable.Columns.Add("datarowID", typeof(Int32));
                DataColumn md = planTable.Columns.Add("md", typeof(Decimal));
                DataColumn inc = planTable.Columns.Add("inc", typeof(Decimal));
                DataColumn azm = planTable.Columns.Add("azm", typeof(Decimal));
                DataColumn tvd = planTable.Columns.Add("tvd", typeof(Decimal));
                DataColumn sselev = planTable.Columns.Add("sselev", typeof(Decimal));
                DataColumn noff = planTable.Columns.Add("noff", typeof(Decimal));
                DataColumn eoff = planTable.Columns.Add("eoff", typeof(Decimal));
                DataColumn dls = planTable.Columns.Add("dls", typeof(Decimal));
                DataColumn brate = planTable.Columns.Add("brate", typeof(Decimal));
                DataColumn trate = planTable.Columns.Add("trate", typeof(Decimal));
                DataColumn tface = planTable.Columns.Add("tface", typeof(Decimal));
                DataColumn vs = planTable.Columns.Add("vs", typeof(Decimal));
                DataColumn north = planTable.Columns.Add("north", typeof(Decimal));
                DataColumn east = planTable.Columns.Add("east", typeof(Decimal));
                String rdMD, rdInc, rdAzm, rdTVD, rdSselev, rdNoff, rdEoff, rdDls, rdBrate, rdTrate, rdTface, rdVS, rdN, rdE;
                Decimal dataDepth, dataInc, dataAzm, dataTVD, dataSselev, dataNOff, dataEOff, dataDLS, dataBRate, dataTRate, dataTFace, dataVS, dataNorth, dataEast;

                dataFileID = insertWellPlanDataFile(FileName, FileExtension, OperatorID, ClientID, WellPadID, WellID, UserName, UserRealm, ClientDBString);
                if (dataFileID >= 1)
                {
                    fileAdded = 1;
                }
                else
                {
                    fileAdded = -1;
                }

                foreach (var item in UploadedFileData.Skip(1))
                {
                    detail = item.Split(',');
                    nextSeq++;
                    rdMD = Convert.ToString(detail[0]);
                    if (String.IsNullOrEmpty(rdMD) || rdMD.Equals("0.00"))
                    {
                        dataDepth = 0.00M;
                    }
                    else
                    {
                        dataDepth = Convert.ToDecimal(rdMD);
                    }
                    rdInc = Convert.ToString(detail[1]);
                    if (String.IsNullOrEmpty(rdInc) || rdInc.Equals("0.00"))
                    {
                        dataInc = 0.00M;
                    }
                    else
                    {
                        dataInc = Convert.ToDecimal(rdInc);
                    }
                    rdAzm = Convert.ToString(detail[2]);
                    if (String.IsNullOrEmpty(rdAzm) || rdAzm.Equals("0.00"))
                    {
                        dataAzm = 0.00M;
                    }
                    else
                    {
                        dataAzm = Convert.ToDecimal(rdAzm);
                    }
                    rdTVD = Convert.ToString(detail[3]);
                    if (String.IsNullOrEmpty(rdTVD) || rdTVD.Equals("0.00"))
                    {
                        dataTVD = 0.00M;
                    }
                    else
                    {
                        dataTVD = Convert.ToDecimal(rdTVD);
                    }
                    rdSselev = Convert.ToString(detail[4]);
                    if (String.IsNullOrEmpty(rdSselev) || rdSselev.Equals("0.00"))
                    {
                        dataSselev = 0.00M;
                    }
                    else
                    {
                        dataSselev = Convert.ToDecimal(rdSselev);
                    }
                    rdNoff = Convert.ToString(detail[5]);
                    if (String.IsNullOrEmpty(rdNoff) || rdNoff.Equals("0.00"))
                    {
                        dataNOff = 0.00M;
                    }
                    else
                    {
                        dataNOff = Convert.ToDecimal(rdNoff);
                    }
                    rdEoff = Convert.ToString(detail[6]);
                    if (String.IsNullOrEmpty(rdEoff) || rdEoff.Equals("0.00"))
                    {
                        dataEOff = 0.00M;
                    }
                    else
                    {
                        dataEOff = Convert.ToDecimal(rdEoff);
                    }
                    rdDls = Convert.ToString(detail[7]);
                    if (String.IsNullOrEmpty(rdDls) || rdDls.Equals("0.00"))
                    {
                        dataDLS = 0.00M;
                    }
                    else
                    {
                        dataDLS = Convert.ToDecimal(rdDls);
                    }
                    rdBrate = Convert.ToString(detail[8]);
                    if (String.IsNullOrEmpty(rdBrate) || rdBrate.Equals("0.00"))
                    {
                        dataBRate = 0.00M;
                    }
                    else
                    {
                        dataBRate = Convert.ToDecimal(rdBrate);
                    }
                    rdTrate = Convert.ToString(detail[9]);
                    if (String.IsNullOrEmpty(rdTrate) || rdTrate.Equals("0.00"))
                    {
                        dataTRate = 0.00M;
                    }
                    else
                    {
                        dataTRate = Convert.ToDecimal(rdTrate);
                    }
                    rdTface = Convert.ToString(detail[10]);
                    if (String.IsNullOrEmpty(rdTface) || rdTface.Equals("0.00"))
                    {
                        dataTFace = 0.00M;
                    }
                    else
                    {
                        dataTFace = Convert.ToDecimal(rdTface);
                    }
                    rdVS = Convert.ToString(detail[11]);
                    if (String.IsNullOrEmpty(rdVS) || rdVS.Equals("0.00"))
                    {
                        dataVS = 0.00M;
                    }
                    else
                    {
                        dataVS = Convert.ToDecimal(rdVS);
                    }
                    rdN = Convert.ToString(detail[12]);
                    if (String.IsNullOrEmpty(rdN) || rdN.Equals("0.00"))
                    {
                        dataNorth = 0.00M;
                    }
                    else
                    {
                        dataNorth = Convert.ToDecimal(rdN);
                    }
                    rdE = Convert.ToString(detail[13]);
                    if (String.IsNullOrEmpty(rdE) || rdE.Equals("0.00"))
                    {
                        dataEast = 0.00M;
                    }
                    else
                    {
                        dataEast = Convert.ToDecimal(rdE);
                    }
                    //rowAdded = insertWellPlanDataFileDataRow(dataFileID, dataDepth, dataInc, dataAzm, dataTVD, dataSselev, dataNOff, dataEOff, dataDLS, dataBRate, dataTRate, dataTFace, dataVS, dataNorth, dataEast, nextSeq, WellPadID, WellID, ClientDBString);
                    planTable.Rows.Add(nextSeq, dataDepth, dataInc, dataAzm, dataTVD, dataSselev, dataNOff, dataEOff, dataDLS, dataBRate, dataTRate, dataTFace, dataVS, dataNorth, dataEast);                    
                    planTable.AcceptChanges();
                }
                HttpContext.Current.Session["uploadedPlanTable"] = planTable;
                //infoAdded = insertWellPlanMetaInfo(WellPadID, WellID, PermanentDatumID, DepthReferenceElevationID, DepthReferenceElevation, DepthReferenceElevationID, VerticalSectionAzimuth, LengthUnit, Northing, NorthingUnit, Easting, EastingUnit, ClientDBString);
                infoArray.Add(WellPadID);
                infoArray.Add(WellID);
                infoArray.Add(PermanentDatumID);
                infoArray.Add(DepthReferenceID);
                infoArray.Add(DepthReferenceElevation);
                infoArray.Add(DepthReferenceElevationID);
                infoArray.Add(VerticalSectionAzimuth);
                infoArray.Add(LengthUnit);
                infoArray.Add(Northing);
                infoArray.Add(NorthingUnit);
                infoArray.Add(Easting);
                infoArray.Add(EastingUnit);
                HttpContext.Current.Session["uploadedPlanInfo"] = infoArray;

                iReply.Add(planTable);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static ArrayList processPlacementInfo(String FileName, String FileExtension, List<String> UploadedFileData, Int32 MeasuredDepthUnits)
        {
            try
            {
                ArrayList iReply = new ArrayList();
                Decimal[] gb_decimals = new Decimal[6];
                String[] detail;
                Int32 nextSeq = 0;
                System.Data.DataTable placementTable = new System.Data.DataTable();
                DataColumn datarowID = placementTable.Columns.Add("datarowID", typeof(Int32));
                DataColumn md = placementTable.Columns.Add("md", typeof(Decimal));
                DataColumn lenU = placementTable.Columns.Add("lenU", typeof(Decimal));
                DataColumn inc = placementTable.Columns.Add("inc", typeof(Decimal));
                DataColumn azm = placementTable.Columns.Add("azm", typeof(Decimal));                
                String rdMD, rdInc, rdAzm;
                Decimal dataDepth, dataInc, dataAzm;

                foreach (var item in UploadedFileData.Skip(1))
                {
                    detail = item.Split(',');
                    nextSeq++;
                    rdMD = Convert.ToString(detail[0]);
                    if (String.IsNullOrEmpty(rdMD) || rdMD.Equals("0.00"))
                    {
                        dataDepth = 0.00M;
                    }
                    else
                    {
                        dataDepth = Convert.ToDecimal(rdMD);
                    }
                    rdInc = Convert.ToString(detail[1]);
                    if (String.IsNullOrEmpty(rdInc) || rdInc.Equals("0.00"))
                    {
                        dataInc = 0.00M;
                    }
                    else
                    {
                        dataInc = Convert.ToDecimal(rdInc);
                    }
                    rdAzm = Convert.ToString(detail[2]);
                    if (String.IsNullOrEmpty(rdAzm) || rdAzm.Equals("0.00"))
                    {
                        dataAzm = 0.00M;
                    }
                    else
                    {
                        dataAzm = Convert.ToDecimal(rdAzm);
                    }
                    placementTable.Rows.Add(nextSeq, dataDepth, MeasuredDepthUnits, dataInc, dataAzm);
                    placementTable.AcceptChanges();
                }
                HttpContext.Current.Session["uploadedPlacementTable"] = placementTable;
                HttpContext.Current.Session["WellPlanDataFileName"] = FileName;
                HttpContext.Current.Session["WellPlanDataFileExtension"] = FileExtension;
                iReply.Add(placementTable);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getNextSequence(Int32 WellPadID, Int32 WellID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT ISNULL(MAX(datarowID), 0) + 1 FROM [WellPlanDataFileData] WHERE [welID] = @welID";
                using (SqlConnection Conn = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        Conn.Open();  // Connection is opened to get runID and next sequence
                        SqlCommand nseqCMD = new SqlCommand(selData, Conn);
                        nseqCMD.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        nseqCMD.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader nseqRdr = nseqCMD.ExecuteReader())
                        {
                            try
                            {
                                while (nseqRdr.Read())
                                {
                                    iReply = nseqRdr.GetInt32(0);
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                nseqRdr.Close();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        Conn.Close();
                        Conn.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertWellPlanDataFileDataRow(Int32 FileID, Decimal MDepth, Decimal Inclination, Decimal Azimuth, Decimal TVD, Decimal SubSeaElev, Decimal NorthingOffset, Decimal EastingOffset, Decimal DLS, Decimal BRate, Decimal TRate, Decimal TFace, Decimal VertSec, Decimal Northing, Decimal Easting, Int32 DataRowID, Int32 WellPadID, Int32 WellID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String dataInsert = "INSERT INTO [WellPlanDataFileData] ([wpfID], [md], [inc], [azm], [tvd], [sselev], [noffset], [eoffset], [dls], [brate], [trate], [tface], [vs], [northing], [easting], [datarowID], [wpdID], [welID]) VALUES (@wpfID, @md, @inc, @azm, @tvd, @sselev, @noffset, @eoffset, @dls, @brate, @trate, @tface, @vs, @northing, @easting, @datarowID, @wpdID, @welID)";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(dataInsert, dataCon);
                        insData.Parameters.AddWithValue("@wpfID", SqlDbType.Int).Value = FileID.ToString();
                        insData.Parameters.AddWithValue("@md", SqlDbType.Decimal).Value = MDepth.ToString();
                        insData.Parameters.AddWithValue("@inc", SqlDbType.Decimal).Value = Inclination.ToString();
                        insData.Parameters.AddWithValue("@azm", SqlDbType.Decimal).Value = Azimuth.ToString();
                        insData.Parameters.AddWithValue("@tvd", SqlDbType.Decimal).Value = TVD.ToString();
                        insData.Parameters.AddWithValue("@sselev", SqlDbType.Decimal).Value = SubSeaElev.ToString();
                        insData.Parameters.AddWithValue("@noffset", SqlDbType.Decimal).Value = NorthingOffset.ToString();
                        insData.Parameters.AddWithValue("@eoffset", SqlDbType.Decimal).Value = EastingOffset.ToString();
                        insData.Parameters.AddWithValue("@dls", SqlDbType.Decimal).Value = DLS.ToString();
                        insData.Parameters.AddWithValue("@brate", SqlDbType.Decimal).Value = BRate.ToString();
                        insData.Parameters.AddWithValue("@trate", SqlDbType.Decimal).Value = TRate.ToString();
                        insData.Parameters.AddWithValue("@tface", SqlDbType.Decimal).Value = TFace.ToString();
                        insData.Parameters.AddWithValue("@vs", SqlDbType.Decimal).Value = VertSec.ToString();
                        insData.Parameters.AddWithValue("@northing", SqlDbType.Decimal).Value = Northing.ToString();
                        insData.Parameters.AddWithValue("@easting", SqlDbType.Decimal).Value = Easting.ToString();
                        insData.Parameters.AddWithValue("@datarowID", SqlDbType.Int).Value = DataRowID.ToString();
                        insData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        insData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getCalcWellPlan(Int32 WellID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wpcID = iReply.Columns.Add("wpcID", typeof(Int32));
                DataColumn md = iReply.Columns.Add("md", typeof(Decimal));
                DataColumn inc = iReply.Columns.Add("inc", typeof(Decimal));
                DataColumn azm = iReply.Columns.Add("azm", typeof(Decimal));
                DataColumn tvd = iReply.Columns.Add("tvd", typeof(Decimal));
                DataColumn cummNorthing = iReply.Columns.Add("cummDeltaNorthing", typeof(Decimal));
                DataColumn cummEasting = iReply.Columns.Add("cummDeltaEasting", typeof(Decimal));
                DataColumn vs = iReply.Columns.Add("vs", typeof(Decimal));
                DataColumn subsea = iReply.Columns.Add("subsea", typeof(Decimal));
                DataColumn dogleg100 = iReply.Columns.Add("dogleg100", typeof(Decimal));
                DataColumn northing = iReply.Columns.Add("northing", typeof(Decimal));
                DataColumn easting = iReply.Columns.Add("easting", typeof(Decimal));
                Int32 id = -99;
                Decimal dataDepth = -99.00M, dataInc = -99.00M, dataAzm = -99.00M, dataTVD = -99.00M, dataCNorthing = -99.00M, dataCEasting = -99.00M, dataVS = -99.00M, dataSubsea = -99.00M, dataDLS = -99.00M, dataN = -99.00M, dataE = -99.00M;
                String selData = "SELECT [wpcID], [md], [inc], [azm], [tvd], [cummDeltaNorthing], [cummDeltaEasting], [vs], [subsea], [dogleg100], [northing], [easting] FROM [WellPlanCalculatedData] WHERE [welID] = @welID ";
                selData += " and format( ctime , 'yyyyMMddHHmm' ) in ";
                selData += "(select  format( max(ctime) , 'yyyyMMddHHmm' ) FROM [WellPlanCalculatedData] where welID = @welID);";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        dataDepth = readData.GetDecimal(1);
                                        dataInc = readData.GetDecimal(2);
                                        dataAzm = readData.GetDecimal(3);
                                        dataTVD = readData.GetDecimal(4);
                                        dataCNorthing = readData.GetDecimal(5);
                                        dataCEasting = readData.GetDecimal(6);
                                        dataVS = readData.GetDecimal(7);
                                        dataSubsea = readData.GetDecimal(8);
                                        dataDLS = readData.GetDecimal(9);
                                        dataN = readData.GetDecimal(10);
                                        dataE = readData.GetDecimal(11);
                                        iReply.Rows.Add(id, dataDepth, dataInc, dataAzm, dataTVD, dataCNorthing, dataCEasting, dataVS, dataSubsea, dataDLS, dataN, dataE);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally 
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getCalcWellPlacement(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wpcID = iReply.Columns.Add("wpcdID", typeof(Int32));
                DataColumn md = iReply.Columns.Add("md", typeof(Decimal));
                DataColumn inc = iReply.Columns.Add("inc", typeof(Decimal));
                DataColumn azm = iReply.Columns.Add("azm", typeof(Decimal));
                DataColumn tvd = iReply.Columns.Add("tvd", typeof(Decimal));
                DataColumn cummNorthing = iReply.Columns.Add("cummDeltaNorthing", typeof(Decimal));
                DataColumn cummEasting = iReply.Columns.Add("cummDeltaEasting", typeof(Decimal));
                DataColumn vs = iReply.Columns.Add("vs", typeof(Decimal));
                DataColumn subsea = iReply.Columns.Add("subsea", typeof(Decimal));
                DataColumn dogleg100 = iReply.Columns.Add("dogleg100", typeof(Decimal));
                DataColumn northing = iReply.Columns.Add("northing", typeof(Decimal));
                DataColumn easting = iReply.Columns.Add("easting", typeof(Decimal));
                Int32 id = -99;
                Decimal dataDepth = -99.00M, dataInc = -99.00M, dataAzm = -99.00M, dataTVD = -99.00M, dataCNorthing = -99.00M, dataCEasting = -99.00M, dataVS = -99.00M, dataSubsea = -99.00M, dataDLS = -99.00M, dataN = -99.00M, dataE = -99.00M;
                String selData = "SELECT [wpcdID], [md], [rwinc], [rwazm], [rwTVD], [rwcummDeltaNorthing], [rwcummDeltaEasting], [rwVS], [rwSubsea], [rwDogleg100], [rwNorthing], [rwEasting] FROM [WellPlacementData] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        dataDepth = readData.GetDecimal(1);
                                        dataInc = readData.GetDecimal(2);
                                        dataAzm = readData.GetDecimal(3);
                                        dataTVD = readData.GetDecimal(4);
                                        dataCNorthing = readData.GetDecimal(5);
                                        dataCEasting = readData.GetDecimal(6);
                                        dataVS = readData.GetDecimal(7);
                                        dataSubsea = readData.GetDecimal(8);
                                        dataDLS = readData.GetDecimal(9);
                                        dataN = readData.GetDecimal(10);
                                        dataE = readData.GetDecimal(11);
                                        iReply.Rows.Add(id, dataDepth, dataInc, dataAzm, dataTVD, dataCNorthing, dataCEasting, dataVS, dataSubsea, dataDLS, dataN, dataE);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertWellPlanDataFile(String FileName, String FileExtension, Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, String LoginName, String LoginRealm, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String dataInsert = "INSERT INTO [WellPlacementDataFileName] ([wpfName], [wpfExtension], [wpdID], [welID], [clntID], [optrID], [username], [realm], [cTime], [uTime]) VALUES (@wpfName, @wpfExtension, @wpdID, @welID, @clntID, @optrID, @username, @realm, @cTime, @uTime); SELECT SCOPE_IDENTITY()";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(dataInsert, dataCon);
                        insData.Parameters.AddWithValue("@wpfName", SqlDbType.NVarChar).Value = FileName.ToString();
                        insData.Parameters.AddWithValue("@wpfExtension", SqlDbType.NVarChar).Value = FileExtension.ToString();
                        insData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        insData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        insData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        insData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = LoginName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = LoginRealm.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        iReply = Convert.ToInt32(insData.ExecuteScalar());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }            
        }

        public static Int32 checkDuplicateWellPlanDataFile(String FileName, String FileExtension, Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String dataInsert = "SELECT COUNT(wplcfID) FROM [WellPlacementDataFileName] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID AND [welID] = @welID AND [wpfName] = @wpfName AND [wpfExtension] = @wpfExtension";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(dataInsert, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();                                                
                        getData.Parameters.AddWithValue("@wpfName", SqlDbType.NVarChar).Value = FileName.ToString();
                        getData.Parameters.AddWithValue("@wpfExtension", SqlDbType.NVarChar).Value = FileExtension.ToString();
                        iReply = Convert.ToInt32(getData.ExecuteScalar());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 resetWellPlan(Int32 WellID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99, data = -99, file = -99, info = -99;
                info = deleteWellMetaInfo(WellID, ClientDBString);
                file = deleteWellDataFile(WellID, ClientDBString);
                data = deleteWellPlanFileData(WellID, ClientDBString);
                if ( ( info >= 1 ) && ( file >= 1 ) && ( data >= 1 ) )
                {
                    iReply = 1;
                }
                else
                {
                    iReply = -1;
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteWellMetaInfo(Int32 WellID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String delData = "DELETE FROM [WellPlanMetaInfo] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delete = new SqlCommand(delData, dataCon);
                        delete.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        iReply = Convert.ToInt32(delete.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteWellDataFile(Int32 WellID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String delData = "DELETE FROM [WellPlanDataFileName] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delete = new SqlCommand(delData, dataCon);
                        delete.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        iReply = Convert.ToInt32(delete.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteWellPlanFileData(Int32 WellID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String delData = "DELETE FROM [WellPlanDataFileData] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand delete = new SqlCommand(delData, dataCon);
                        delete.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        iReply = Convert.ToInt32(delete.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWellPlanTieInSurveyTable(Int32 RunID, Int32 WellSectionID, Int32 WellID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wptiID = iReply.Columns.Add("wptiID", typeof(Int32));
                DataColumn MDepth = iReply.Columns.Add("MDepth", typeof(Decimal));
                DataColumn Inc = iReply.Columns.Add("Inc", typeof(Decimal));
                DataColumn Azm = iReply.Columns.Add("Azm", typeof(Decimal));
                DataColumn TVD = iReply.Columns.Add("TVD", typeof(Decimal));
                DataColumn North = iReply.Columns.Add("North", typeof(Decimal));
                DataColumn East = iReply.Columns.Add("East", typeof(Decimal));
                DataColumn VS = iReply.Columns.Add("VS", typeof(Decimal));
                DataColumn Subsea = iReply.Columns.Add("Subsea", typeof(Decimal));
                String selData = "SELECT [wptiID], [MDepth], [Inc], [Azm], [TVD], [North], [East], [VS], [Subsea] FROM [WellPlanTieIn] WHERE [runID] = @runID AND [wsID] = @wsID AND [welID] = @welID";
                Int32 id = -99;
                Decimal vMD = -99.00M, vInc = -99.000000000000M, vAzm = -99.000000000000M, vTVD = -99.000000000000M, vNorth = -99.000000000000M, vEast = -99.000000000000M, vVs = -99.000000000000M, vSubsea = -99.000000000000M;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        getData.Parameters.AddWithValue("@wsID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        vMD = readData.GetDecimal(1);
                                        vInc = readData.GetDecimal(2);
                                        vAzm = readData.GetDecimal(3);
                                        vTVD = readData.GetDecimal(4);
                                        vNorth = readData.GetDecimal(5);
                                        vEast = readData.GetDecimal(6);
                                        vVs = readData.GetDecimal(7);
                                        vSubsea = readData.GetDecimal(8);
                                        iReply.Rows.Add(id, vMD, vInc, vAzm, vTVD, vNorth, vEast, vVs, vSubsea);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static ArrayList getRunTieInSurveyList(Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 WellPadID, Int32 ClientID, Int32 OperatorID, String ClientDBString)
        {
            try
            {
                ArrayList iReply = new ArrayList();
                String selData = "SELECT [rtiID], [MDepth], [MDepthUnit], [Inclination], [Azimuth], [TVD], [TVDUnit], [NorthOffset], [NOffsetUnit], [EastOffset], [EOffsetUnit], [VS], [VSUnit], [Subsea], [SubseaUnit] FROM [RunTieIn] WHERE [runID] = @runID AND [wswID] = @wswID AND [welID] = @welID AND [wpdID] = @wpdID AND [clntID] = @clntID AND [optrID] = @optrID";
                Int32 id = -99, mdUnit = -99, tvdUnit = -99, NUnit = -99, EUnit = -99, vsUnit = -99, ssUnit = -99;
                Decimal vMD = -99.00M, vInc = -99.000000000000M, vAzm = -99.000000000000M, vTVD = -99.000000000000M, vNorth = -99.000000000000M, vEast = -99.000000000000M, vVs = -99.000000000000M, vSubsea = -99.000000000000M;
                String mduName = String.Empty, tvduName = String.Empty, nuName = String.Empty, euName = String.Empty, vsuName = String.Empty, ssuName = String.Empty;
                string mdValue = String.Empty, tvdValue = String.Empty, nValue = String.Empty, eValue = String.Empty, vsValue = String.Empty, ssValue = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        vMD = readData.GetDecimal(1);
                                        iReply.Add(vMD);
                                        mdUnit = readData.GetInt32(2);
                                        iReply.Add(mdUnit);
                                        vInc = readData.GetDecimal(3);
                                        iReply.Add(vInc);
                                        vAzm = readData.GetDecimal(4);
                                        iReply.Add(vAzm);
                                        vTVD = readData.GetDecimal(5);
                                        iReply.Add(vTVD);
                                        tvdUnit = readData.GetInt32(6);
                                        iReply.Add(tvdUnit);
                                        vNorth = readData.GetDecimal(7);
                                        iReply.Add(vNorth);
                                        NUnit = readData.GetInt32(8);
                                        iReply.Add(NUnit);
                                        vEast = readData.GetDecimal(9);
                                        iReply.Add(vEast);
                                        EUnit = readData.GetInt32(10);
                                        iReply.Add(EUnit);
                                        vVs = readData.GetDecimal(11);
                                        iReply.Add(vVs);
                                        vsUnit = readData.GetInt32(12);
                                        iReply.Add(vsUnit);
                                        vSubsea = readData.GetDecimal(13);
                                        iReply.Add(vSubsea);
                                        ssUnit = readData.GetInt32(14);
                                        iReply.Add(ssUnit);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getRunTieInSurveyTable(Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 WellPadID, Int32 ClientID, Int32 OperatorID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn rtiID = iReply.Columns.Add("rtiID", typeof(Int32));
                DataColumn MDepth = iReply.Columns.Add("MDepth", typeof(String));
                DataColumn Inc = iReply.Columns.Add("Inc", typeof(String));
                DataColumn Azm = iReply.Columns.Add("Azm", typeof(String));
                DataColumn TVD = iReply.Columns.Add("TVD", typeof(String));
                DataColumn North = iReply.Columns.Add("North", typeof(String));
                DataColumn East = iReply.Columns.Add("East", typeof(String));
                DataColumn VS = iReply.Columns.Add("VS", typeof(String));
                DataColumn Subsea = iReply.Columns.Add("Subsea", typeof(String));
                String selData = "SELECT [rtiID], [MDepth], [MDepthUnit], [Inclination], [Azimuth], [TVD], [TVDUnit], [NorthOffset], [NOffsetUnit], [EastOffset], [EOffsetUnit], [VS], [VSUnit], [Subsea], [SubseaUnit] FROM [RunTieIn] WHERE [runID] = @runID AND [wswID] = @wswID AND [welID] = @welID AND [wpdID] = @wpdID AND [clntID] = @clntID AND [optrID] = @optrID";
                Int32 id = -99, mdUnit = -99, tvdUnit = -99, NUnit = -99, EUnit = -99, vsUnit = -99, ssUnit = -99;
                Decimal vMD = -99.00M, vInc = -99.000000000000M, vAzm = -99.000000000000M, vTVD = -99.000000000000M, vNorth = -99.000000000000M, vEast = -99.000000000000M, vVs = -99.000000000000M, vSubsea = -99.000000000000M;
                String mduName = String.Empty, tvduName = String.Empty, nuName = String.Empty, euName = String.Empty, vsuName = String.Empty, ssuName = String.Empty;
                string mdValue = String.Empty, tvdValue = String.Empty, nValue = String.Empty, eValue = String.Empty, vsValue = String.Empty, ssValue = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        vMD = readData.GetDecimal(1);
                                        mdUnit = readData.GetInt32(2);
                                        mduName = AST.getLengthUnitName(mdUnit);
                                        mdValue = String.Format("{0} - ({1})", vMD.ToString("0.00"), mduName);
                                        vInc = readData.GetDecimal(3);
                                        vAzm = readData.GetDecimal(4);
                                        vTVD = readData.GetDecimal(5);
                                        tvdUnit = readData.GetInt32(6);
                                        tvduName = AST.getLengthUnitName(tvdUnit);
                                        tvdValue = String.Format("{0} - ({1})", vTVD.ToString("0.00"), tvduName);
                                        vNorth = readData.GetDecimal(7);
                                        NUnit = readData.GetInt32(8);
                                        nuName = AST.getLengthUnitName(NUnit);
                                        nValue = String.Format("{0} - ({1})", vNorth.ToString("0.00"), nuName);
                                        vEast = readData.GetDecimal(9);
                                        EUnit = readData.GetInt32(10);
                                        euName = AST.getLengthUnitName(EUnit);
                                        eValue = String.Format("{0} - ({1})", vEast.ToString("0.00"), euName);
                                        vVs = readData.GetDecimal(11);
                                        vsUnit = readData.GetInt32(12);
                                        vsuName = AST.getLengthUnitName(vsUnit);
                                        vsValue = String.Format("{0} - ({1})", vVs.ToString("0.00"), vsuName);
                                        vSubsea = readData.GetDecimal(13);
                                        ssUnit = readData.GetInt32(14);
                                        ssuName = AST.getLengthUnitName(ssUnit);
                                        ssValue = String.Format("{0} - ({1})", vSubsea.ToString("0.00"), ssuName);
                                        iReply.Rows.Add(id, mdValue, vInc, vAzm, tvdValue, nValue, eValue, vsValue, ssValue);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static ArrayList getWellPlanTieInSurveyList(Int32 RunID, Int32 WellSectionID, Int32 WellID, String ClientDBString)
        {
            try
            {
                ArrayList iReply = new ArrayList();
                String selData = "SELECT [rtiID], [MDepth], [MDepthUnit], [Inclination], [Azimuth], [TVD], [TVDUnit], [NorthOffset], [NOffsetUnit], [EastOffset], [EOffsetUnit], [VS], [VSUnit], [Subsea], [SubseaUnit] FROM [RunTieIn] WHERE [runID] = @runID AND [wswID] = @wswID AND [welID] = @welID";
                Int32 id = -99, mdU = -99, tvdU = -99, vsU = -99, nU = -99, eU = -99, ssU = -99;
                Decimal vMD = -99.00M, vInc = -99.000000000000M, vAzm = -99.000000000000M, vTVD = -99.000000000000M, vNorth = -99.000000000000M, vEast = -99.000000000000M, vVs = -99.000000000000M, vSubsea = -99.000000000000M;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        iReply.Add(id);
                                        vMD = readData.GetDecimal(1);
                                        iReply.Add(vMD);
                                        mdU = readData.GetInt32(2);                                        
                                        iReply.Add(mdU);
                                        vInc = readData.GetDecimal(3);
                                        iReply.Add(vInc);
                                        vAzm = readData.GetDecimal(4);
                                        iReply.Add(vAzm);
                                        vTVD = readData.GetDecimal(5);
                                        iReply.Add(vTVD);
                                        tvdU = readData.GetInt32(6);                                        
                                        iReply.Add(tvdU);
                                        vNorth = readData.GetDecimal(7);
                                        iReply.Add(vNorth);
                                        nU = readData.GetInt32(8);                                        
                                        iReply.Add(nU);
                                        vEast = readData.GetDecimal(9);
                                        iReply.Add(vEast);
                                        eU = readData.GetInt32(10);                                        
                                        iReply.Add(eU);
                                        vVs = readData.GetDecimal(11);
                                        iReply.Add(vVs);
                                        vsU = readData.GetInt32(12);                                        
                                        iReply.Add(vsU);
                                        vSubsea = readData.GetDecimal(13);
                                        iReply.Add(vSubsea);
                                        ssU = readData.GetInt32(14);                                        
                                        iReply.Add(ssU);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static ArrayList getWellMetaInfoList(Int32 WellID, Int32 WellPadID, String ClientDBString)
        {
            try
            {
                ArrayList iReply = new ArrayList();
                String selData = "SELECT [wpiElev], [wpiElevUnit], [luID], [wpiAzm], [wpiNorthing], [wpiNorthingUnit], [wpiEasting], [wpiEastingUnit] FROM [WellPlanMetaInfo] WHERE [welID] = @welID AND [wpdID] = @wpdID";
                Int32 lengthUnitID = -99, elevUnitID = -99, NUnitID = -99, EUnitID = -99;
                Decimal vElav = -99.00M, vAzm = -99.00M, vN = -99.00M, vE = -99.00M;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        vElav = readData.GetDecimal(0);
                                        iReply.Add(vElav);
                                        elevUnitID = readData.GetInt32(1);
                                        iReply.Add(elevUnitID);
                                        lengthUnitID = readData.GetInt32(2);
                                        iReply.Add(lengthUnitID);
                                        vAzm = readData.GetDecimal(3);
                                        iReply.Add(vAzm);                                        
                                        vN = readData.GetDecimal(4);
                                        iReply.Add(vN);
                                        NUnitID = readData.GetInt32(5);
                                        iReply.Add(NUnitID);
                                        vE = readData.GetDecimal(6);
                                        iReply.Add(vE);
                                        EUnitID = readData.GetInt32(7);
                                        iReply.Add(EUnitID);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertWellPlanTieInSurvey(Int32 OperatorID, Int32 ClientID, Int32 WellPad, Int32 WellID, Decimal MDepth, Int32 MDepthUnit, Decimal TVD, Int32 TVDUnit, Decimal NorthOffset, Int32 NOffsetUnit, Decimal EastOffset, Int32 EOffsetUnit, Decimal VerticalSection, Int32 VSUnit, Decimal DogLeg, Int32 DGLGUnit, Decimal BuildRate, Int32 BRUnit, Decimal TurnRate, Int32 TRUnit, Decimal SubSea, Int32 SubseaUnit, String UserName, String UserDomain, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String insertData = "INSERT INTO [WellPlanTieIn] ([optrID], [clntID], [wpdID], [welID], [MDepth], [MDepthUnit], [TVD], [TVDUnit], [NorthOffset], [NOffsetUnit], [EastOffset], [EOffsetUnit], [VS], [VSUnit], [DGLG], [DGLGUnit], [BR], [BRUnit], [TR], [TRUnit], [Subsea], [SubseaUnit], [username], [realm], [cTime], [uTime]) VALUES (@optrID, @clntID, @wpdID, @welID, @MDepth, @MDepthUnit, @TVD, @TVDUnit, @NorthOffset, @NOffsetUnit, @EastOffset, @EOffsetUnit, @VS, @VSUnit, @DGLG, @DGLGUnit, @BR, @BRUnit, @TR, @TRUnit, @Subsea, @SubseaUnit, @username, @realm, @cTime, @uTime)";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(insertData, dataCon);
                        insData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        insData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        insData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPad.ToString();
                        insData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        insData.Parameters.AddWithValue("@MDepth", SqlDbType.Decimal).Value = MDepth.ToString();
                        insData.Parameters.AddWithValue("@MDepthUnit", SqlDbType.Int).Value = MDepthUnit.ToString();
                        insData.Parameters.AddWithValue("@TVD", SqlDbType.Decimal).Value = TVD.ToString();
                        insData.Parameters.AddWithValue("@TVDUnit", SqlDbType.Int).Value = TVDUnit.ToString();
                        insData.Parameters.AddWithValue("@NorthOffset", SqlDbType.Decimal).Value = NorthOffset.ToString();
                        insData.Parameters.AddWithValue("@NOffsetUnit", SqlDbType.Int).Value = NOffsetUnit.ToString();
                        insData.Parameters.AddWithValue("@EastOffset", SqlDbType.Decimal).Value = EastOffset.ToString();
                        insData.Parameters.AddWithValue("@EOffsetUnit", SqlDbType.Int).Value = EOffsetUnit.ToString();
                        insData.Parameters.AddWithValue("@VS", SqlDbType.Decimal).Value = VerticalSection.ToString();
                        insData.Parameters.AddWithValue("@VSUnit", SqlDbType.Int).Value = VSUnit.ToString();
                        insData.Parameters.AddWithValue("@DGLG", SqlDbType.Decimal).Value = DogLeg.ToString();
                        insData.Parameters.AddWithValue("@DGLGUnit", SqlDbType.Int).Value = DGLGUnit.ToString();
                        insData.Parameters.AddWithValue("@BR", SqlDbType.Decimal).Value = BuildRate.ToString();
                        insData.Parameters.AddWithValue("@BRUnit", SqlDbType.Int).Value = BRUnit.ToString();
                        insData.Parameters.AddWithValue("@TR", SqlDbType.Decimal).Value = TurnRate.ToString();
                        insData.Parameters.AddWithValue("@TRUnit", SqlDbType.Int).Value = TRUnit.ToString();
                        insData.Parameters.AddWithValue("@Subsea", SqlDbType.Decimal).Value = SubSea.ToString();
                        insData.Parameters.AddWithValue("@SubseaUnit", SqlDbType.Int).Value = SubseaUnit.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserDomain.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertRunTieInSurvey(Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 WellPadID, Int32 ClientID, Int32 OperatorID, Decimal MeasuredDepth, Int32 MDepthUnit, Decimal Inclination, Decimal Azimuth, Decimal TVD, Int32 TVDUnit, Decimal North, Int32 NorthUnit, Decimal East, Int32 EastUnit, Decimal VS, Int32 VSUnit, Decimal Subsea, Int32 SubseaUnit, String UserName, String UserRealm, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String insertData = "INSERT INTO [RunTieIn] ([optrID], [clntID], [wpdID], [welID], [wswID], [runID], [MDepth], [MDepthUnit], [Inclination], [Azimuth], [TVD], [TVDUnit], [NorthOffset], [NOffsetUnit], [EastOffset], [EOffsetUnit], [VS], [VSUnit], [Subsea], [SubseaUnit], [username], [realm], [cTime], [uTime]) VALUES (@optrID, @clntID, @wpdID, @welID, @wswID, @runID, @MDepth, @MDepthUnit, @Inclination, @Azimuth, @TVD, @TVDUnit, @NorthOffset, @NOffsetUnit, @EastOffset, @EOffsetUnit, @VS, @VSUnit, @Subsea, @SubseaUnit, @username, @realm, @cTime, @uTime)";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(insertData, dataCon);
                        insData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        insData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        insData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        insData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        insData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        insData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        insData.Parameters.AddWithValue("@MDepth", SqlDbType.Decimal).Value = MeasuredDepth.ToString();
                        insData.Parameters.AddWithValue("@MDepthUnit", SqlDbType.Int).Value = MDepthUnit.ToString();
                        insData.Parameters.AddWithValue("@Inclination", SqlDbType.Decimal).Value = Inclination.ToString();
                        insData.Parameters.AddWithValue("@Azimuth", SqlDbType.Decimal).Value = Azimuth.ToString();
                        insData.Parameters.AddWithValue("@TVD", SqlDbType.Decimal).Value = TVD.ToString();
                        insData.Parameters.AddWithValue("@TVDUnit", SqlDbType.Int).Value = TVDUnit.ToString();
                        insData.Parameters.AddWithValue("@NorthOffset", SqlDbType.Decimal).Value = North.ToString();
                        insData.Parameters.AddWithValue("@NOffsetUnit", SqlDbType.Int).Value = NorthUnit.ToString();
                        insData.Parameters.AddWithValue("@EastOffset", SqlDbType.Decimal).Value = East.ToString();
                        insData.Parameters.AddWithValue("@EOffsetUnit", SqlDbType.Int).Value = EastUnit.ToString();
                        insData.Parameters.AddWithValue("@VS", SqlDbType.Decimal).Value = VS.ToString();
                        insData.Parameters.AddWithValue("@VSUnit", SqlDbType.Int).Value = VSUnit.ToString();
                        insData.Parameters.AddWithValue("@Subsea", SqlDbType.Decimal).Value = Subsea.ToString();
                        insData.Parameters.AddWithValue("@SubseaUnit", SqlDbType.Int).Value = SubseaUnit.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserRealm.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 checkWellPlanForSelectedWell(Int32 WellID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT TOP 1 1 FROM [WellPlanCalculatedData] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    iReply = readData.GetInt32(0);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                if (iReply.Equals(1) || iReply > 1)
                {
                    iReply = 1;
                }
                else
                {
                    iReply = -1;
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 deleteWellPlacementInputData(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, String ClientDBString)
        {
            Int32 dataDel = deleteWellPlacementData(OperatorID, ClientID, WellPadID, WellID, ClientDBString);
            Int32 tieinDel = deleteWellPlacementTieIn(OperatorID, ClientID, WellPadID, WellID, ClientDBString);
            Int32 dFileDel = deleteWellPlacementDataFile(OperatorID, ClientID, WellPadID, WellID, ClientDBString);
            if (dataDel >= 1 || tieinDel >= 1 || dFileDel >= 1)
            { return 1; }
            else
            { return 0; }
        }

        private static int deleteWellPlacementDataFile(int OperatorID, int ClientID, int WellPadID, int WellID, string ClientDBString)
        {
            Int32 iReply = -99;
            String delStat = "DELETE FROM [WellPlanDataFileName] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID AND [welID] = @welID";
            using (SqlConnection dataCon = new SqlConnection(ClientDBString))
            {
                try
                {
                    dataCon.Open();
                    SqlCommand delData = new SqlCommand(delStat, dataCon);
                    delData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                    delData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                    delData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                    delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                    iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
                finally
                {
                    dataCon.Close();
                    dataCon.Dispose();
                }
            }
            return iReply;
        }

        internal static Int32 deleteWellPlacementData(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, String ClientDBString)
        {
            Int32 iReply = -99;
            String delStat = "DELETE FROM [WellPlacementData] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID AND [welID] = @welID";
            using (SqlConnection dataCon = new SqlConnection(ClientDBString))
            {
                try
                {
                    dataCon.Open();
                    SqlCommand delData = new SqlCommand(delStat, dataCon);
                    delData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                    delData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                    delData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                    delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                    iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
                finally
                {
                    dataCon.Close();
                    dataCon.Dispose();
                }
            }
            return iReply;
        }

        internal static Int32 deleteWellPlacementTieIn(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, String ClientDBString)
        {
            Int32 iReply = -99;
            String delStat = "DELETE FROM [WellPlacementTieIn] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID AND [welID] = @welID";
            using (SqlConnection dataCon = new SqlConnection(ClientDBString))
            {
                try
                {
                    dataCon.Open();
                    SqlCommand delData = new SqlCommand(delStat, dataCon);
                    delData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                    delData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                    delData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                    delData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                    iReply = Convert.ToInt32(delData.ExecuteNonQuery());
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
                finally
                {
                    dataCon.Close();
                    dataCon.Dispose();
                }
            }
            return iReply;
        }

        public static ArrayList processPlacementData(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, String WellPlanDataFileName, String WellPlanDataFileExtension, Decimal TieInMeasuredDepth, Int32 MDepthUnit, Decimal TieInInclination, Decimal TieInAzimuth, Decimal TieInTVD, Int32 TieInTVDUnit, Decimal TieInNorthOffset, Int32 TieInNorthOffsetUnit, Decimal TieInEastOffset, Int32 TieInEastOffsetUnit, Decimal TieInVerticalSection, Int32 TieInVerticalSectionUnit, Decimal TieInSubSeaElevation, Int32 TieInSubseaElevationUnit, DataTable WellPlacementRawDataTable, String LoginName, String LoginRealm, String ClientDBString)
        {
            ArrayList iReply = new ArrayList(), wellMetaInfo = new ArrayList();
            Int32 tiID = -99, dataRowInserted = -99, nextSeq = 0, fileID = -99, dupFile = -99;
            Int32 LengthUnit = -99;
            Decimal VertSecAzimuth = -99.00M, ReferenceElevation = -99.00M;
            System.Data.DataTable plcTable = new System.Data.DataTable();
            DataColumn plcID = plcTable.Columns.Add("datarowID", typeof(Int32));
            DataColumn plcMD = plcTable.Columns.Add("plcMD", typeof(Decimal));
            DataColumn plcInc = plcTable.Columns.Add("plcInc", typeof(Decimal));
            DataColumn plcAzm = plcTable.Columns.Add("plcAzm", typeof(Decimal));
            DataColumn plcTvd = plcTable.Columns.Add("plcTvd", typeof(Decimal));
            DataColumn plcCummNorthing = plcTable.Columns.Add("plcCummNorthing", typeof(Decimal));
            DataColumn plcCummEasting = plcTable.Columns.Add("plcCummEasting", typeof(Decimal));
            DataColumn plcVs = plcTable.Columns.Add("plcVs", typeof(Decimal));
            DataColumn plcSubsea = plcTable.Columns.Add("plcSubsea", typeof(Decimal));
            DataColumn plcDogleg100 = plcTable.Columns.Add("plcDogleg100", typeof(Decimal));
            DataColumn plcNorthing = plcTable.Columns.Add("plcNorthing", typeof(Decimal));
            DataColumn plcEasting = plcTable.Columns.Add("plcEasting", typeof(Decimal));
            //Insert Tie In and get ID
            tiID = insertWellPlacementTieIn(OperatorID, ClientID, WellPadID, WellID, TieInMeasuredDepth, MDepthUnit, TieInInclination, TieInAzimuth, TieInTVD, TieInTVDUnit, TieInNorthOffset, TieInNorthOffsetUnit, TieInEastOffset, TieInEastOffsetUnit, TieInVerticalSection, TieInVerticalSectionUnit, TieInSubSeaElevation, TieInSubseaElevationUnit, LoginName, LoginRealm, ClientDBString);                        
            //Insert Data File to PlanFileTable
            dupFile = checkDuplicateWellPlanDataFile(WellPlanDataFileName, WellPlanDataFileExtension, OperatorID, ClientID, WellPadID, WellID, ClientDBString);
            if (dupFile >= 1)
            {
                fileID = -1; //Found Duplicate File
            }
            else
            {
                fileID = insertWellPlanDataFile(WellPlanDataFileName, WellPlanDataFileExtension, OperatorID, ClientID, WellPadID, WellID, LoginName, LoginRealm, ClientDBString);
            
                //Process WEll Placement columns                
                Decimal rw_SupNorthing = -99.00M, rw_SupEasting = -99.00M, rw_nVal = 0.00M, rw_eVal = 0.00M, rw_tvdVal = -99.000000000000M, rw_vsVal = -99.000000000000M, rw_subseaVal = -99.000000000000M;
                Decimal rw_northingVal = -99.000000000000M, rw_eastingVal = -99.000000000000M, rw_deltaMDVal = -99.000000000000M, rw_d1Val = -99.000000000000M, rw_dogleg1Val = -99.000000000000M, rw_rfVal = -99.000000000000M, rw_deltaTVDVal = -99.000000000000M;
                Decimal rw_cumDeltaNorthingVal = -99.000000000000M, rw_deltaNVal = -99.000000000000M, rw_cumDeltaEastingVal = -99.000000000000M, rw_deltaEVal = -99.000000000000M;
                Decimal rw_deltaVSVal = -99.000000000000M, rw_ClosureDistanceVal = -99.000000000000M, rw_clDirectAngleVal1 = -99.000000000000M, rw_clDirectAngleVal2 = -99.000000000000M;
                Decimal rw_dogLegVal = -99.000000000000M, rw_dls100Val = -99.000000000000M;
                Decimal rw_PreviousDepth = 0.00M, rw_PreviousTVD = 0.00M, rw_PreviousInclination = 0.000000000000M, rw_PreviousAzimuth = 0.000000000000M, rw_PreviousCumDeltaNorth = 0.000000000000M, rw_PreviousCumDeltaEast = 0.000000000000M;
                Decimal rw_PreviousVerticalSection = 0.000000000000M, rw_PreviousNorthing = 0.000000000000M, rw_PreviousEasting = 0.000000000000M;
                Decimal tiMD = -99.0000M, tiNorth = -99.000000M, tiEast = -99.000000M, tiTVD = -99.000000M, tiVS = -99.000000M, tiSubSea = -99.000000M;
                Decimal rw_FirstDataRowSubSeaValue = 0.00M;
                if (TieInNorthOffsetUnit.Equals(4)) { tiNorth = TieInNorthOffset * 0.3048M; } else { tiNorth = TieInNorthOffset; }
                if (TieInEastOffsetUnit.Equals(4)) { tiEast = TieInEastOffset * 0.3048M; } else { tiEast = TieInEastOffset; }
                if (TieInTVDUnit.Equals(4)) { tiTVD = TieInTVD * 0.3048M; } else { tiTVD = TieInTVD; }
                if (TieInVerticalSectionUnit.Equals(4)) { tiVS = TieInVerticalSection * 0.3048M; } else { tiVS = TieInVerticalSection; }
                if (TieInSubseaElevationUnit.Equals(4)) { tiSubSea = TieInSubSeaElevation * 0.3048M; } else { tiSubSea = TieInSubSeaElevation; }
                wellMetaInfo = getWellMetaInfoList(WellID, WellPadID, ClientDBString);
                ReferenceElevation = Convert.ToDecimal(wellMetaInfo[0]);
                Int32 elevUnit = Convert.ToInt32(wellMetaInfo[1]);
                if (elevUnit.Equals(4)) { ReferenceElevation = ReferenceElevation / 0.3048M; }
                LengthUnit = Convert.ToInt32(wellMetaInfo[2]);
                VertSecAzimuth = Convert.ToDecimal(wellMetaInfo[3]);
                if (MDepthUnit.Equals(4)) { tiMD = TieInMeasuredDepth * 0.3048M; } else { tiMD = TieInMeasuredDepth; }
                //Calculations for Placement data
                foreach(DataRow dRow in WellPlacementRawDataTable.Rows)
                {
                    Decimal pcMeasuredDepth = Convert.ToDecimal(dRow[1]);
                    Int32 mdUnit = Convert.ToInt32(dRow[2]);
                    Decimal pcInclination = Convert.ToDecimal(dRow[3]);
                    Decimal pcAzimuth = Convert.ToDecimal(dRow[4]);
                    nextSeq++;
                    if (mdUnit.Equals(4)) { pcMeasuredDepth = pcMeasuredDepth / 0.3048M; }
                    rw_PreviousDepth = Convert.ToDecimal(HttpContext.Current.Session["PrevDepth"]);
                    rw_PreviousInclination = Convert.ToDecimal(HttpContext.Current.Session["rw_PreviousInclination"]);
                    rw_PreviousAzimuth = Convert.ToDecimal(HttpContext.Current.Session["rw_PreviousAzimuth"]);
                    rw_PreviousTVD = Convert.ToDecimal(HttpContext.Current.Session["rw_PreviousTVD"]);
                    rw_PreviousCumDeltaNorth = Convert.ToDecimal(HttpContext.Current.Session["rw_PreviousCumDeltaNorth"]);
                    rw_PreviousCumDeltaEast = Convert.ToDecimal(HttpContext.Current.Session["rw_PreviousCumDeltaEast"]);
                    //rw_PreviousOldClosureDistance = Convert.ToDecimal(HttpContext.Current.Session["rw_PreviousOldClosureDistance"]);
                    rw_PreviousVerticalSection = Convert.ToDecimal(HttpContext.Current.Session["rw_PreviousVerticalSection"]);
                    rw_PreviousNorthing = Convert.ToDecimal(HttpContext.Current.Session["rw_PreviousNorthing"]);
                    rw_PreviousEasting = Convert.ToDecimal(HttpContext.Current.Session["rw_PreviousEasting"]);

                    rw_deltaMDVal = getMeasuredDepthDiff(nextSeq, tiMD, pcMeasuredDepth, rw_PreviousDepth);
                    rw_d1Val = getD1(nextSeq, pcInclination, rw_PreviousInclination, pcAzimuth, rw_PreviousAzimuth);
                    rw_dogleg1Val = getDogLeg1(rw_d1Val);
                    rw_rfVal = getRF(nextSeq, rw_d1Val);
                    rw_deltaTVDVal = getDeltaTVD(nextSeq, rw_deltaMDVal, pcInclination, rw_PreviousInclination, rw_rfVal);
                    rw_deltaNVal = getDeltaNorthing(nextSeq, rw_deltaTVDVal, rw_deltaMDVal, pcInclination, rw_PreviousInclination, pcAzimuth, rw_PreviousAzimuth, rw_rfVal);
                    rw_cumDeltaNorthingVal = getCummulativeDeltaNorthing(nextSeq, tiNorth, rw_PreviousCumDeltaNorth, rw_deltaNVal);
                    rw_deltaEVal = getDeltaEasting(nextSeq, rw_deltaMDVal, rw_deltaTVDVal, rw_PreviousInclination, pcInclination, rw_PreviousAzimuth, pcAzimuth, rw_rfVal);
                    rw_cumDeltaEastingVal = getCummulativeDeltaEasting(nextSeq, tiEast, rw_deltaEVal, rw_PreviousCumDeltaEast);
                    rw_ClosureDistanceVal = getClosureDistance(rw_cumDeltaNorthingVal, rw_cumDeltaEastingVal);
                    rw_clDirectAngleVal1 = getColABValue(tiNorth, tiEast, rw_cumDeltaNorthingVal, rw_cumDeltaEastingVal, rw_ClosureDistanceVal);
                    rw_clDirectAngleVal2 = getColACValue(tiNorth, tiEast, rw_cumDeltaNorthingVal, rw_cumDeltaEastingVal, rw_clDirectAngleVal1);
                    rw_deltaVSVal = getDeltaVerticalSection(VertSecAzimuth, rw_ClosureDistanceVal, rw_clDirectAngleVal2);
                    rw_dogLegVal = getDogleg(nextSeq, pcInclination, rw_PreviousInclination, pcAzimuth, rw_PreviousAzimuth);
                    rw_dls100Val = getDLS(nextSeq, LengthUnit, rw_deltaMDVal, rw_dogLegVal);
                    rw_tvdVal = getTVD(nextSeq, tiTVD, rw_deltaTVDVal, rw_PreviousTVD);
                    rw_vsVal = getVerticalSection(nextSeq, tiVS, rw_PreviousVerticalSection, rw_deltaVSVal);
                    rw_subseaVal = getSubsea(tiSubSea, rw_tvdVal);
                    rw_northingVal = getNorthing(nextSeq, tiNorth, rw_PreviousNorthing, rw_deltaNVal);
                    rw_eastingVal = getEasting(nextSeq, tiEast, rw_PreviousEasting, rw_deltaEVal);
                    
                    HttpContext.Current.Session["rw_PreviousDepth"] = pcMeasuredDepth;
                    HttpContext.Current.Session["rw_PreviousInclination"] = pcInclination;
                    HttpContext.Current.Session["rw_PreviousAzimuth"] = pcAzimuth;
                    HttpContext.Current.Session["rw_PreviousTVD"] = rw_tvdVal;
                    HttpContext.Current.Session["rw_PreviousCumDeltaNorth"] = rw_cumDeltaNorthingVal;
                    HttpContext.Current.Session["rw_PreviousCumDeltaEast"] = rw_cumDeltaEastingVal;
                    HttpContext.Current.Session["rw_PreviousVerticalSection"] = rw_vsVal;
                    HttpContext.Current.Session["rw_PreviousNorthing"] = rw_northingVal;
                    HttpContext.Current.Session["rw_PreviousEasting"] = rw_eastingVal;
                    if (nextSeq.Equals(1)) { rw_FirstDataRowSubSeaValue = rw_subseaVal; }
                    dataRowInserted = insertWellPlacementCalculatedData(OperatorID, ClientID, WellPadID, WellID, fileID, tiID, nextSeq, pcMeasuredDepth, mdUnit, pcInclination, pcAzimuth, rw_tvdVal, rw_vsVal, rw_northingVal, rw_eastingVal, rw_deltaMDVal, rw_d1Val, rw_dogleg1Val, rw_rfVal, rw_deltaTVDVal, rw_nVal, rw_deltaNVal, rw_cumDeltaEastingVal, rw_deltaEVal, rw_deltaVSVal, rw_ClosureDistanceVal, rw_clDirectAngleVal1, rw_clDirectAngleVal2, rw_dogLegVal, rw_dls100Val, rw_subseaVal, rw_SupNorthing, rw_SupEasting, LoginName, LoginRealm, ClientDBString);
                    if (dataRowInserted >= 1)
                    {
                        plcTable.Rows.Add(nextSeq, pcMeasuredDepth, pcInclination, pcAzimuth, rw_tvdVal, rw_cumDeltaNorthingVal, rw_cumDeltaEastingVal, rw_vsVal, rw_subseaVal, rw_dls100Val, rw_northingVal, rw_eastingVal);
                        plcTable.AcceptChanges();
                    }
                }
            }
            //Insert Well Placement columns and insert data into table (raw, sc, bha and crr data)
            if (dataRowInserted >= 1 && fileID >= 1 && tiID >= 1)
            {
                iReply.Add(1); //Processed and inserted values
                iReply.Add(plcTable);
            }
            else if (fileID.Equals(-1))
            {
                iReply.Add(-2);
            }
            else
            {
                iReply.Add(-3); //Couldn't process and inserted values
            }
            return iReply;
        }

        public static Int32 insertWellPlacementTieIn(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Decimal TieInMeasuredDepth, Int32 MDepthUnit, Decimal TieInInclination, Decimal TieInAzimuth, Decimal TieInTVD, Int32 TieInTVDUnit, Decimal TieInNorthOffset, Int32 TieInNorthOffsetUnit, Decimal TieInEastOffset, Int32 TieInEastOffsetUnit, Decimal TieInVerticalSection, Int32 TieInVerticalSectionUnit, Decimal TieInSubSeaElevation, Int32 TieInSubseaElevationUnit, String LoginName, String LoginRealm, String ClientDBString)
        {
            Int32 iReply = -99;
            String selData = "INSERT INTO [WellPlacementTieIn] ([optrID], [clntID], [wpdID], [welID], [MDepth], [MDepthUnit], [Inc], [Azm], [TVD], [TVDUnit], [NorthOffset], [NOffsetUnit], [EastOffset], [EOffsetUnit], [VS], [VSUnit], [Subsea], [SubseaUnit], [username], [realm], [cTime], [uTime]) VALUES (@optrID, @clntID, @wpdID, @welID, @MDepth, @MDepthUnit, @Inc, @Azm, @TVD, @TVDUnit, @NorthOffset, @NOffsetUnit, @EastOffset, @EOffsetUnit, @VS, @VSUnit, @Subsea, @SubseaUnit, @username, @realm, @cTime, @uTime); SELECT SCOPE_IDENTITY();";
            using (SqlConnection dataCon = new SqlConnection(ClientDBString))
            {
                try
                {
                    dataCon.Open();
                    SqlCommand insData = new SqlCommand(selData, dataCon);
                    insData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                    insData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                    insData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                    insData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                    insData.Parameters.AddWithValue("@MDepth", SqlDbType.Decimal).Value = TieInMeasuredDepth.ToString();
                    insData.Parameters.AddWithValue("@MDepthUnit", SqlDbType.Int).Value = MDepthUnit.ToString();
                    insData.Parameters.AddWithValue("@Inc", SqlDbType.Decimal).Value = TieInInclination.ToString();
                    insData.Parameters.AddWithValue("@Azm", SqlDbType.Decimal).Value = TieInAzimuth.ToString();
                    insData.Parameters.AddWithValue("@TVD", SqlDbType.Decimal).Value = TieInTVD.ToString();
                    insData.Parameters.AddWithValue("@TVDUnit", SqlDbType.Int).Value = TieInTVDUnit.ToString();
                    insData.Parameters.AddWithValue("@NorthOffset", SqlDbType.Decimal).Value = TieInNorthOffset.ToString();
                    insData.Parameters.AddWithValue("@NOffsetUnit", SqlDbType.Int).Value = TieInNorthOffsetUnit.ToString();
                    insData.Parameters.AddWithValue("@EastOffset", SqlDbType.Decimal).Value = TieInEastOffset.ToString();
                    insData.Parameters.AddWithValue("@EOffsetUnit", SqlDbType.Int).Value = TieInEastOffsetUnit.ToString();
                    insData.Parameters.AddWithValue("@VS", SqlDbType.Decimal).Value = TieInVerticalSection.ToString();
                    insData.Parameters.AddWithValue("@VSUnit", SqlDbType.Int).Value = TieInVerticalSectionUnit.ToString();
                    insData.Parameters.AddWithValue("@Subsea", SqlDbType.Decimal).Value = TieInSubSeaElevation.ToString();
                    insData.Parameters.AddWithValue("@SubseaUnit", SqlDbType.Int).Value = TieInSubseaElevationUnit.ToString();
                    insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = LoginName.ToString();
                    insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = LoginRealm.ToString();
                    insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                    insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                    iReply = Convert.ToInt32(insData.ExecuteScalar());
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
                finally
                {
                    dataCon.Close();
                    dataCon.Dispose();
                }
            }
            return iReply;
        }

        public static Int32 insertWellPlacementCalculatedData(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 WellPlanDataFileID, Int32 WellPlacementTieInID, Int32 DataRowID, Decimal dataMDepth, Int32 dataMDepthUnit, Decimal Inclination, Decimal Azimuth, Decimal rawTVD, Decimal rawVS, Decimal rawNorthing, Decimal rawEasting, Decimal rawDeltaMD, Decimal rawD1, Decimal rawDogLeg1, Decimal rawRF, Decimal rawDeltaTVD, Decimal rawCummDeltaNorthing, Decimal rawDeltaNorth, Decimal rawCummDeltaEasting, Decimal rawDeltaEasting, Decimal rawDeltaVS, Decimal rawClosureDistance, Decimal rawClosureDirectAngle1, Decimal rawClosureDirectAngle2, Decimal rawDogLeg, Decimal rawDogLeg100, Decimal rawSubSea, Decimal rawSupNorthing, Decimal rawSupEasting, String LoginName, String LoginRealm, String ClientDBString)
        {
            Int32 iReply = -99;
            String selData = "INSERT INTO [WellPlacementData] ([md], [mdUnit], [rwInc], [rwAzm], [rwTVD], [rwvs], [rwNorthing], [rwEasting], [rwdeltaMD], [rwd1], [rwdogleg1], [rwrf], [rwdeltaTVD], [rwcummDeltaNorthing], [rwdeltaNorthing], [rwcummDeltaEasting], [rwdeltaEasting], [rwdeltaVS], [rwclosureDistance], [rwclosureDirectAngle1], [rwclosureDirectAngle2], [rwdogleg], [rwdogleg100], [rwsubsea], [rwSupNorthing], [rwSupEasting], [scinc], [scazm], [sctvd], [scvs], [scnorthing], [sceasting], [scdeltaMD], [scd1], [scdogleg1], [scrf], [scdeltaTVD], [sccummDeltaNorthing], [scdeltaNorthing], [sccummDeltaEasting], [scdeltaEasting], [scdeltaVS], [scclosureDistance], [scclosureDirectAngle1], [scclosureDirectAngle2], [scdogleg], [scdogleg100], [scsubsea], [scSupNorthing], [scSupEasting], [bhinc], [bhazm], [bhtvd], [bhvs], [bhnorthing], [bheasting], [bhdeltaMD], [bhd1], [bhdogleg1], [bhrf], [bhdeltaTVD], [bhcummDeltaNorthing], [bhdeltaNorthing], [bhcummDeltaEasting], [bhdeltaEasting], [bhdeltaVS], [bhclosureDistance], [bhclosureDirectAngle1], [bhclosureDirectAngle2], [bhdogleg], [bhdogleg100], [bhsubsea], [bhSupNorthing], [bhSupEasting], [crinc], [crazm], [crtvd], [crvs], [crnorthing], [creasting], [crdeltaMD], [crd1], [crdogleg1], [crrf], [crdeltaTVD], [crcummDeltaNorthing], [crdeltaNorthing], [crcummDeltaEasting], [crdeltaEasting], [crdeltaVS], [crclosureDistance], [crclosureDirectAngle1], [crclosureDirectAngle2], [crdogleg], [crdogleg100], [crsubsea], [crSupNorthing], [crSupEasting], [optrID], [clntID], [wpdID], [welID], [datarowID], [wpfID], [wpctiID], [username], [realm], [cTime], [uTime]) VALUES (@md, @mdUnit, @rwInc, @rwAzm, @rwTVD, @rwvs, @rwNorthing, @rwEasting, @rwdeltaMD, @rwd1, @rwdogleg1, @rwrf, @rwdeltaTVD, @rwcummDeltaNorthing, @rwdeltaNorthing, @rwcummDeltaEasting, @rwdeltaEasting, @rwdeltaVS, @rwclosureDistance, @rwclosureDirectAngle1, @rwclosureDirectAngle2, @rwdogleg, @rwdogleg100, @rwsubsea, @rwSupNorthing, @rwSupEasting, @scinc, @scazm, @sctvd, @scvs, @scnorthing, @sceasting, @scdeltaMD, @scd1, @scdogleg1, @scrf, @scdeltaTVD, @sccummDeltaNorthing, @scdeltaNorthing, @sccummDeltaEasting, @scdeltaEasting, @scdeltaVS, @scclosureDistance, @scclosureDirectAngle1, @scclosureDirectAngle2, @scdogleg, @scdogleg100, @scsubsea, @scSupNorthing, @scSupEasting, @bhinc, @bhazm, @bhtvd, @bhvs, @bhnorthing, @bheasting, @bhdeltaMD, @bhd1, @bhdogleg1, @bhrf, @bhdeltaTVD, @bhcummDeltaNorthing, @bhdeltaNorthing, @bhcummDeltaEasting, @bhdeltaEasting, @bhdeltaVS, @bhclosureDistance, @bhclosureDirectAngle1, @bhclosureDirectAngle2, @bhdogleg, @bhdogleg100, @bhsubsea, @bhSupNorthing, @bhSupEasting, @crinc, @crazm, @crtvd, @crvs, @crnorthing, @creasting, @crdeltaMD, @crd1, @crdogleg1, @crrf, @crdeltaTVD, @crcummDeltaNorthing, @crdeltaNorthing, @crcummDeltaEasting, @crdeltaEasting, @crdeltaVS, @crclosureDistance, @crclosureDirectAngle1, @crclosureDirectAngle2, @crdogleg, @crdogleg100, @crsubsea, @crSupNorthing, @crSupEasting, @optrID, @clntID, @wpdID, @welID, @datarowID, @wpfID, @wpctiID, @username, @realm, @cTime, @uTime)";
            using (SqlConnection dataCon = new SqlConnection(ClientDBString))
            {
                try
                {
                    dataCon.Open();
                    SqlCommand insData = new SqlCommand(selData, dataCon);
                    insData.Parameters.AddWithValue("@md", SqlDbType.Int).Value = dataMDepth.ToString();
                    insData.Parameters.AddWithValue("@mdUnit", SqlDbType.Int).Value = dataMDepthUnit.ToString();
                    insData.Parameters.AddWithValue("@rwinc", SqlDbType.Decimal).Value = Inclination.ToString();
                    insData.Parameters.AddWithValue("@rwazm", SqlDbType.Decimal).Value = Azimuth.ToString();
                    insData.Parameters.AddWithValue("@rwtvd", SqlDbType.Decimal).Value = rawTVD.ToString();
                    insData.Parameters.AddWithValue("@rwvs", SqlDbType.Decimal).Value = rawVS.ToString();
                    insData.Parameters.AddWithValue("@rwnorthing", SqlDbType.Decimal).Value = rawNorthing.ToString();
                    insData.Parameters.AddWithValue("@rweasting", SqlDbType.Decimal).Value = rawEasting.ToString();
                    insData.Parameters.AddWithValue("@rwdeltaMD", SqlDbType.Decimal).Value = rawDeltaMD.ToString();
                    insData.Parameters.AddWithValue("@rwd1", SqlDbType.Decimal).Value = rawD1.ToString();
                    insData.Parameters.AddWithValue("@rwdogleg1", SqlDbType.Decimal).Value = rawDogLeg1.ToString();
                    insData.Parameters.AddWithValue("@rwrf", SqlDbType.Decimal).Value = rawRF.ToString();
                    insData.Parameters.AddWithValue("@rwdeltaTVD", SqlDbType.Decimal).Value = rawDeltaTVD.ToString();
                    insData.Parameters.AddWithValue("@rwcummDeltaNorthing", SqlDbType.Decimal).Value = rawCummDeltaNorthing.ToString();
                    insData.Parameters.AddWithValue("@rwdeltaNorthing", SqlDbType.Decimal).Value = rawDeltaNorth.ToString();
                    insData.Parameters.AddWithValue("@rwcummDeltaEasting", SqlDbType.Decimal).Value = rawCummDeltaEasting.ToString();
                    insData.Parameters.AddWithValue("@rwdeltaEasting", SqlDbType.Decimal).Value = rawDeltaEasting.ToString();
                    insData.Parameters.AddWithValue("@rwdeltaVS", SqlDbType.Decimal).Value = rawDeltaVS.ToString();
                    insData.Parameters.AddWithValue("@rwclosureDistance", SqlDbType.Decimal).Value = rawClosureDistance.ToString();
                    insData.Parameters.AddWithValue("@rwclosureDirectAngle1", SqlDbType.Decimal).Value = rawClosureDirectAngle1.ToString();
                    insData.Parameters.AddWithValue("@rwclosureDirectAngle2", SqlDbType.Decimal).Value = rawClosureDirectAngle2.ToString();
                    insData.Parameters.AddWithValue("@rwdogleg", SqlDbType.Decimal).Value = rawDogLeg.ToString();
                    insData.Parameters.AddWithValue("@rwdogleg100", SqlDbType.Decimal).Value = rawDogLeg100.ToString();
                    insData.Parameters.AddWithValue("@rwsubsea", SqlDbType.Decimal).Value = rawSubSea.ToString();
                    insData.Parameters.AddWithValue("@rwSupNorthing", SqlDbType.Decimal).Value = rawSupNorthing.ToString();
                    insData.Parameters.AddWithValue("@rwSupEasting", SqlDbType.Decimal).Value = rawSupEasting.ToString();
                    insData.Parameters.AddWithValue("@scinc", SqlDbType.Decimal).Value = Inclination.ToString();
                    insData.Parameters.AddWithValue("@scazm", SqlDbType.Decimal).Value = Azimuth.ToString();
                    insData.Parameters.AddWithValue("@sctvd", SqlDbType.Decimal).Value = rawTVD.ToString();
                    insData.Parameters.AddWithValue("@scvs", SqlDbType.Decimal).Value = rawVS.ToString();
                    insData.Parameters.AddWithValue("@scnorthing", SqlDbType.Decimal).Value = rawNorthing.ToString();
                    insData.Parameters.AddWithValue("@sceasting", SqlDbType.Decimal).Value = rawEasting.ToString();
                    insData.Parameters.AddWithValue("@scdeltaMD", SqlDbType.Decimal).Value = rawDeltaMD.ToString();
                    insData.Parameters.AddWithValue("@scd1", SqlDbType.Decimal).Value = rawD1.ToString();
                    insData.Parameters.AddWithValue("@scdogleg1", SqlDbType.Decimal).Value = rawDogLeg1.ToString();
                    insData.Parameters.AddWithValue("@scrf", SqlDbType.Decimal).Value = rawRF.ToString();
                    insData.Parameters.AddWithValue("@scdeltaTVD", SqlDbType.Decimal).Value = rawDeltaTVD.ToString();
                    insData.Parameters.AddWithValue("@sccummDeltaNorthing", SqlDbType.Decimal).Value = rawCummDeltaNorthing.ToString();
                    insData.Parameters.AddWithValue("@scdeltaNorthing", SqlDbType.Decimal).Value = rawDeltaNorth.ToString();
                    insData.Parameters.AddWithValue("@sccummDeltaEasting", SqlDbType.Decimal).Value = rawCummDeltaEasting.ToString();
                    insData.Parameters.AddWithValue("@scdeltaEasting", SqlDbType.Decimal).Value = rawDeltaEasting.ToString();
                    insData.Parameters.AddWithValue("@scdeltaVS", SqlDbType.Decimal).Value = rawDeltaVS.ToString();
                    insData.Parameters.AddWithValue("@scclosureDistance", SqlDbType.Decimal).Value = rawClosureDistance.ToString();
                    insData.Parameters.AddWithValue("@scclosureDirectAngle1", SqlDbType.Decimal).Value = rawClosureDirectAngle1.ToString();
                    insData.Parameters.AddWithValue("@scclosureDirectAngle2", SqlDbType.Decimal).Value = rawClosureDirectAngle2.ToString();
                    insData.Parameters.AddWithValue("@scdogleg", SqlDbType.Decimal).Value = rawDogLeg.ToString();
                    insData.Parameters.AddWithValue("@scdogleg100", SqlDbType.Decimal).Value = rawDogLeg100.ToString();
                    insData.Parameters.AddWithValue("@scsubsea", SqlDbType.Decimal).Value = rawSubSea.ToString();
                    insData.Parameters.AddWithValue("@scSupNorthing", SqlDbType.Decimal).Value = rawSupNorthing.ToString();
                    insData.Parameters.AddWithValue("@scSupEasting", SqlDbType.Decimal).Value = rawSupEasting.ToString();
                    insData.Parameters.AddWithValue("@bhinc", SqlDbType.Decimal).Value = Inclination.ToString();
                    insData.Parameters.AddWithValue("@bhazm", SqlDbType.Decimal).Value = Azimuth.ToString();
                    insData.Parameters.AddWithValue("@bhtvd", SqlDbType.Decimal).Value = rawTVD.ToString();
                    insData.Parameters.AddWithValue("@bhvs", SqlDbType.Decimal).Value = rawVS.ToString();
                    insData.Parameters.AddWithValue("@bhnorthing", SqlDbType.Decimal).Value = rawNorthing.ToString();
                    insData.Parameters.AddWithValue("@bheasting", SqlDbType.Decimal).Value = rawEasting.ToString();
                    insData.Parameters.AddWithValue("@bhdeltaMD", SqlDbType.Decimal).Value = rawDeltaMD.ToString();
                    insData.Parameters.AddWithValue("@bhd1", SqlDbType.Decimal).Value = rawD1.ToString();
                    insData.Parameters.AddWithValue("@bhdogleg1", SqlDbType.Decimal).Value = rawDogLeg1.ToString();
                    insData.Parameters.AddWithValue("@bhrf", SqlDbType.Decimal).Value = rawRF.ToString();
                    insData.Parameters.AddWithValue("@bhdeltaTVD", SqlDbType.Decimal).Value = rawDeltaTVD.ToString();
                    insData.Parameters.AddWithValue("@bhcummDeltaNorthing", SqlDbType.Decimal).Value = rawCummDeltaNorthing.ToString();
                    insData.Parameters.AddWithValue("@bhdeltaNorthing", SqlDbType.Decimal).Value = rawDeltaNorth.ToString();
                    insData.Parameters.AddWithValue("@bhcummDeltaEasting", SqlDbType.Decimal).Value = rawCummDeltaEasting.ToString();
                    insData.Parameters.AddWithValue("@bhdeltaEasting", SqlDbType.Decimal).Value = rawDeltaEasting.ToString();
                    insData.Parameters.AddWithValue("@bhdeltaVS", SqlDbType.Decimal).Value = rawDeltaVS.ToString();
                    insData.Parameters.AddWithValue("@bhclosureDistance", SqlDbType.Decimal).Value = rawClosureDistance.ToString();
                    insData.Parameters.AddWithValue("@bhclosureDirectAngle1", SqlDbType.Decimal).Value = rawClosureDirectAngle1.ToString();
                    insData.Parameters.AddWithValue("@bhclosureDirectAngle2", SqlDbType.Decimal).Value = rawClosureDirectAngle2.ToString();
                    insData.Parameters.AddWithValue("@bhdogleg", SqlDbType.Decimal).Value = rawDogLeg.ToString();
                    insData.Parameters.AddWithValue("@bhdogleg100", SqlDbType.Decimal).Value = rawDogLeg100.ToString();
                    insData.Parameters.AddWithValue("@bhsubsea", SqlDbType.Decimal).Value = rawSubSea.ToString();
                    insData.Parameters.AddWithValue("@bhSupNorthing", SqlDbType.Decimal).Value = rawSupNorthing.ToString();
                    insData.Parameters.AddWithValue("@bhSupEasting", SqlDbType.Decimal).Value = rawSupEasting.ToString();
                    insData.Parameters.AddWithValue("@crinc", SqlDbType.Decimal).Value = Inclination.ToString();
                    insData.Parameters.AddWithValue("@crazm", SqlDbType.Decimal).Value = Azimuth.ToString();
                    insData.Parameters.AddWithValue("@crtvd", SqlDbType.Decimal).Value = rawTVD.ToString();
                    insData.Parameters.AddWithValue("@crvs", SqlDbType.Decimal).Value = rawVS.ToString();
                    insData.Parameters.AddWithValue("@crnorthing", SqlDbType.Decimal).Value = rawNorthing.ToString();
                    insData.Parameters.AddWithValue("@creasting", SqlDbType.Decimal).Value = rawEasting.ToString();
                    insData.Parameters.AddWithValue("@crdeltaMD", SqlDbType.Decimal).Value = rawDeltaMD.ToString();
                    insData.Parameters.AddWithValue("@crd1", SqlDbType.Decimal).Value = rawD1.ToString();
                    insData.Parameters.AddWithValue("@crdogleg1", SqlDbType.Decimal).Value = rawDogLeg1.ToString();
                    insData.Parameters.AddWithValue("@crrf", SqlDbType.Decimal).Value = rawRF.ToString();
                    insData.Parameters.AddWithValue("@crdeltaTVD", SqlDbType.Decimal).Value = rawDeltaTVD.ToString();
                    insData.Parameters.AddWithValue("@crcummDeltaNorthing", SqlDbType.Decimal).Value = rawCummDeltaNorthing.ToString();
                    insData.Parameters.AddWithValue("@crdeltaNorthing", SqlDbType.Decimal).Value = rawDeltaNorth.ToString();
                    insData.Parameters.AddWithValue("@crcummDeltaEasting", SqlDbType.Decimal).Value = rawCummDeltaEasting.ToString();
                    insData.Parameters.AddWithValue("@crdeltaEasting", SqlDbType.Decimal).Value = rawDeltaEasting.ToString();
                    insData.Parameters.AddWithValue("@crdeltaVS", SqlDbType.Decimal).Value = rawDeltaVS.ToString();
                    insData.Parameters.AddWithValue("@crclosureDistance", SqlDbType.Decimal).Value = rawClosureDistance.ToString();
                    insData.Parameters.AddWithValue("@crclosureDirectAngle1", SqlDbType.Decimal).Value = rawClosureDirectAngle1.ToString();
                    insData.Parameters.AddWithValue("@crclosureDirectAngle2", SqlDbType.Decimal).Value = rawClosureDirectAngle2.ToString();
                    insData.Parameters.AddWithValue("@crdogleg", SqlDbType.Decimal).Value = rawDogLeg.ToString();
                    insData.Parameters.AddWithValue("@crdogleg100", SqlDbType.Decimal).Value = rawDogLeg100.ToString();
                    insData.Parameters.AddWithValue("@crsubsea", SqlDbType.Decimal).Value = rawSubSea.ToString();
                    insData.Parameters.AddWithValue("@crSupNorthing", SqlDbType.Decimal).Value = rawSupNorthing.ToString();
                    insData.Parameters.AddWithValue("@crSupEasting", SqlDbType.Decimal).Value = rawSupEasting.ToString();
                    insData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                    insData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                    insData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                    insData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                    insData.Parameters.AddWithValue("@datarowID", SqlDbType.Int).Value = DataRowID.ToString();
                    insData.Parameters.AddWithValue("@wpfID", SqlDbType.Int).Value = WellPlanDataFileID.ToString();
                    insData.Parameters.AddWithValue("@wpctiID", SqlDbType.Int).Value = WellPlacementTieInID.ToString();
                    insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = LoginName.ToString();
                    insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = LoginRealm.ToString();
                    insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                    insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                    iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
                finally
                {
                    dataCon.Close();
                    dataCon.Dispose();
                }
            }
            return iReply;
        }
    }
}