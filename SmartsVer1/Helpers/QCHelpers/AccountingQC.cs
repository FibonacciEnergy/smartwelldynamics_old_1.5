﻿using System;
using System.Web;
using System.Linq;
using System.Data;
using System.Globalization;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Collections.Generic;

namespace SmartsVer1.Helpers.QCHelpers
{
    public class AccountingQC
    {
        static void Main()
        { }

        //Method to Account for Raw QC Survey Data. Return Number of Rows affected in Int32
        public static Int32 accServiceRendered(String UserName, String domain, Int32 ServiceID, Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Decimal Depth, Int32 CheckShot)
        {
            try
            {
                Int32 iReply = -99;
                switch (ServiceID)
                {
                    //Client Resource Management
                    case 31: { break; }
                    //Client Account Setup and Configuration
                    case 32: { break; }
                    //SMARTs Raw Data Audit/QC
                    case 33:{   iReply = accRawQC(UserName, domain, OperatorID, ClientID, WellPadID, WellID, WellSectionID, RunID, Depth, CheckShot); break; }
                    //SMARTs Results Data Audit/QC
                    case 34: { iReply = accResQC(UserName, domain, OperatorID, ClientID, WellPadID, WellID, WellSectionID, RunID, Depth, CheckShot); break; }
                    //Well Log Print Data Audit/QC Service
                    case 35: { break; }
                    //Well Log Digital Data Audit/QC Service
                    case 36: { break; }
                    //SMARTs 3rd Party Raw Data Audit/QC
                    case 37: { break; }
                    //SMARTs 3rd Party Results Data Audit/QC
                    case 38: { break; }
                    //Historic Well Raw Data Audit/QC Service
                    case 39: { break; }
                    //Historic Well Results Data Audit/QC
                    case 40: { break; }
                    //SMARTs Well Profile Generation
                    case 41: { break; }
                    //SMARTs Magnetic Storm Survey Rectifier
                    case 42: { break; }
                    //SMARTs Non-Mag Spacing Calculator
                    case 43: { break; }
                    //SMARTs Datum Converter
                    case 44: { break; }
                    //SMARTs Units Converter
                    case 45: { break; }
                    //SMARTs Raw Data Audit/QC with Correction
                    case 46: { break; }
                    //ServiceID = 47, SMARTs Azimuth Correction
                    default: { break; }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 accRawQC(String UserName, String domain, Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Decimal Depth, Int32 CheckShot)
        {
            try
            {
                using (SqlConnection accConn = new SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["FEAccountingDB"].ConnectionString))
                {
                    try
                    {
                        String creditAccount = "INSERT INTO [accRawQC] ([clntUsername], [clntRealm], [optrID], [clntID], [wpdID], [wellID], [wswID], [runID], [depth], [chkShot], [pTime]) VALUES (@clntUsername, @clntRealm, @optrID, @clntID, @wpdID, @wellID, @wswID, @runID, @depth, @chkShot, @pTime)";

                        accConn.Open();
                        
                        SqlCommand accCredit = new SqlCommand(creditAccount, accConn);
                        accCredit.Parameters.AddWithValue("@clntUsername", SqlDbType.NVarChar).Value = UserName.ToString();
                        accCredit.Parameters.AddWithValue("@clntRealm", SqlDbType.NVarChar).Value = domain.ToString();
                        accCredit.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        accCredit.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        accCredit.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        accCredit.Parameters.AddWithValue("@wellID", SqlDbType.Int).Value = WellID.ToString();
                        accCredit.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        accCredit.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        accCredit.Parameters.AddWithValue("@depth", SqlDbType.Decimal).Value = Depth.ToString();
                        accCredit.Parameters.AddWithValue("@chkShot", SqlDbType.Int).Value = CheckShot.ToString();
                        accCredit.Parameters.AddWithValue("@pTime", SqlDbType.DateTime).Value = HttpContext.Current.Timestamp.ToString();

                        return Convert.ToInt32(accCredit.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        accConn.Close();
                        accConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Method to Account for Results Data QC. Return Number of Rows affected in Int32
        public static Int32 accResQC(String UserName, String domain, Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Decimal Depth, Int32 CheckShot)
        {
            try
            {
                using (SqlConnection accConn = new SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["FEAccountingDB"].ConnectionString))
                {
                    try
                    {
                        String creditAccount = "INSERT INTO [accResQC] ([clntUsername], [clntRealm], [optrID], [clntID], [wpdID], [wellID], [wswID], [runID], [depth], [chkShot], [pTime]) VALUES (@clntUsername, @clntRealm, @optrID, @clntID, @wpdID, @wellID, @wswID, @runID, @depth, @chkShot, @pTime)";

                        accConn.Open();

                        SqlCommand accCredit = new SqlCommand(creditAccount, accConn);
                        accCredit.Parameters.AddWithValue("@clntUsername", SqlDbType.NVarChar).Value = UserName.ToString();
                        accCredit.Parameters.AddWithValue("@clntRealm", SqlDbType.NVarChar).Value = domain.ToString();
                        accCredit.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        accCredit.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        accCredit.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        accCredit.Parameters.AddWithValue("@wellID", SqlDbType.Int).Value = WellID.ToString();
                        accCredit.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        accCredit.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        accCredit.Parameters.AddWithValue("@depth", SqlDbType.Decimal).Value = Depth.ToString();
                        accCredit.Parameters.AddWithValue("@chkShot", SqlDbType.Int).Value = CheckShot.ToString();
                        accCredit.Parameters.AddWithValue("@pTime", SqlDbType.DateTime).Value = HttpContext.Current.Timestamp.ToString();

                        return Convert.ToInt32(accCredit.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        accConn.Close();
                        accConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Method to Account for calculating BGs. Returns Numbers of Rows affected in Int32
        public static Int32 accBG(String UserName, String domain, Int32 WPDataSetID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Int32 BHAID, Int32 SurveyNo, Decimal Depth)
        {
            Int32 accResult = -99;

            using (SqlConnection accConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEAccountingDB"].ConnectionString))
            {
                try
                {
                    String creditAccount = "INSERT INTO [accBGs] (clientUsername, clientRealm, wpID, wellID, wswID, runID, bhaID, srvyNo, depth, pTime) VALUES (@clntUsername, @clntRealm, @wpdsID, @wellID, @wswID, @runID, @bhaID, @srvyNo, @depth, @pTime)";

                    accConn.Open();
                    SqlCommand accCredit = new SqlCommand(creditAccount, accConn);
                    accCredit.Parameters.AddWithValue("@clntUsername", SqlDbType.UniqueIdentifier).Value = UserName.ToString();
                    accCredit.Parameters.AddWithValue("@clntRealm", SqlDbType.Int).Value = domain.ToString();
                    accCredit.Parameters.AddWithValue("@wpdsID", SqlDbType.Int).Value = WPDataSetID.ToString();
                    accCredit.Parameters.AddWithValue("@wellID", SqlDbType.Int).Value = WellID.ToString();
                    accCredit.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                    accCredit.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                    accCredit.Parameters.AddWithValue("@bhaID", SqlDbType.Int).Value = BHAID.ToString();
                    accCredit.Parameters.AddWithValue("@srvyNo", SqlDbType.Int).Value = SurveyNo.ToString();
                    accCredit.Parameters.AddWithValue("@depth", SqlDbType.Decimal).Value = Depth.ToString();
                    accCredit.Parameters.AddWithValue("@pTime", SqlDbType.DateTime).Value = HttpContext.Current.Timestamp.ToString();

                    accResult = accCredit.ExecuteNonQuery();
                    return accResult;
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
                finally
                {
                    accConn.Close();
                    accConn.Dispose();
                }
            }
        }

        //Method to Account for calculating BGs. Returns Numbers of Rows affected in Int32
        public static Int32 accWPBG(String UserName, String domain, Int32 WPDataSetID, Int32 RunID, Int32 BhaID, Int32 SurveyNo, Decimal Depth)
        {
            Int32 accResult = -99;

            using (SqlConnection accConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEAccountingDB"].ConnectionString))
            {
                try
                {
                    String creditAccount = "INSERT INTO [accBGs] (clientUsername, clientRealm, wpID, runID, bhaID, srvyNo, depth, pTime) VALUES (@clntUsername, @clntRealm, @wpdsID, @runID, @bhaID, @srvyNo, @depth, @pTime)";

                    accConn.Open();
                    SqlCommand accCredit = new SqlCommand(creditAccount, accConn);
                    accCredit.Parameters.AddWithValue("@clntUsername", SqlDbType.UniqueIdentifier).Value = UserName.ToString();
                    accCredit.Parameters.AddWithValue("@clntRealm", SqlDbType.Int).Value = domain.ToString();
                    accCredit.Parameters.AddWithValue("@wpdsID", SqlDbType.Int).Value = WPDataSetID.ToString();
                    accCredit.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                    accCredit.Parameters.AddWithValue("@bhaID", SqlDbType.Int).Value = BhaID.ToString();
                    accCredit.Parameters.AddWithValue("@srvyNo", SqlDbType.Int).Value = SurveyNo.ToString();
                    accCredit.Parameters.AddWithValue("@depth", SqlDbType.Decimal).Value = Depth.ToString();
                    accCredit.Parameters.AddWithValue("@pTime", SqlDbType.DateTime).Value = HttpContext.Current.Timestamp.ToString();

                    accResult = accCredit.ExecuteNonQuery();
                    return accResult;
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
                finally
                {
                    accConn.Close();
                    accConn.Dispose();
                }
            }
        }

        //Method to Account for Well Profile data. Return Number of Rows affected in Int32
        public static Int32 accWP(String UserName, String domain, Int32 WPDataSetID, Int32 RunID, Int32 BhaID, Int32 SurveyNo, Decimal Depth)
        {
            try
            {
                Int32 accResult = -99;

                using (SqlConnection accConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEAccountingDB"].ConnectionString))
                {
                    try
                    {
                        String creditAccount = "INSERT INTO [accWellProfile] (clntUsername, clntRealm, wpdsID, runID, bhaID, srvyNo, depth, pTime) VALUES (@clntUsername, @clntRealm, @wpdsID, @runID, @bhaID, @srvyNo, @depth, @pTime)";

                        accConn.Open();
                        SqlCommand accCredit = new SqlCommand(creditAccount, accConn);
                        accCredit.Parameters.AddWithValue("@clntUsername", SqlDbType.UniqueIdentifier).Value = UserName.ToString();
                        accCredit.Parameters.AddWithValue("@clntRealm", SqlDbType.Int).Value = domain.ToString();
                        accCredit.Parameters.AddWithValue("@wpdsID", SqlDbType.Int).Value = WPDataSetID.ToString();
                        accCredit.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        accCredit.Parameters.AddWithValue("@bhaID", SqlDbType.Int).Value = BhaID.ToString();
                        accCredit.Parameters.AddWithValue("@srvyNo", SqlDbType.Int).Value = SurveyNo.ToString();
                        accCredit.Parameters.AddWithValue("@depth", SqlDbType.Decimal).Value = Depth.ToString();
                        accCredit.Parameters.AddWithValue("@pTime", SqlDbType.DateTime).Value = HttpContext.Current.Timestamp.ToString();

                        accResult = accCredit.ExecuteNonQuery();
                        return accResult;
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        accConn.Close();
                        accConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Method to Account for calculating Log Print and DvD Orders. Returns Numbers of Rows affected in Int32
        public static void accLogPrint(String ordername , String UserName, String domain, Int32 printsorder, Int32 dvdorder)
        {
            //Int32 accResult = -99;
            Int32 delFlag = 0;

            using (SqlConnection accConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEAccountingDB"].ConnectionString))
            {
                try
                {
                    String creditAccount = "INSERT INTO [accLogPrintOrder] (orderName, clientUserName, clientRealm, printsOrder, dvdOrder, pTime , delFlag) VALUES (@orderName, @clientUserName, @clntRealm, @printsOrder, @dvdOrder, @pTime , @delFlag)";

                    accConn.Open();
                    SqlCommand accCredit = new SqlCommand(creditAccount, accConn);
                    accCredit.Parameters.AddWithValue("@orderName", SqlDbType.UniqueIdentifier).Value = ordername.ToString();
                    accCredit.Parameters.AddWithValue("@clientUserName", SqlDbType.UniqueIdentifier).Value = UserName.ToString();
                    accCredit.Parameters.AddWithValue("@clntRealm", SqlDbType.Int).Value = domain.ToString();
                    accCredit.Parameters.AddWithValue("@printsOrder", SqlDbType.Int).Value = printsorder.ToString();
                    accCredit.Parameters.AddWithValue("@dvdOrder", SqlDbType.Int).Value = dvdorder.ToString();
                    accCredit.Parameters.AddWithValue("@pTime", SqlDbType.DateTime).Value = HttpContext.Current.Timestamp.ToString();
                    accCredit.Parameters.AddWithValue("@delFlag", SqlDbType.Int).Value = delFlag.ToString();

                    //accResult = accCredit.ExecuteNonQuery();
                    //return accResult;

                    accCredit.ExecuteNonQuery();
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
                finally
                {
                    accConn.Close();
                    accConn.Dispose();
                }
            }
        }

        //Method to mark order as deleted 
        public static Int32 accDeleteLogPrint(String ordername)
        {
            Int32 accResult = -99;

            using (SqlConnection accConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEAccountingDB"].ConnectionString))
            {
                try
                {
                    String creditAccount = "Update [accLogPrintOrder] SET [delFlag] = 1 WHERE [orderName] like '" + ordername + "'";

                    accConn.Open();
                    SqlCommand accCredit = new SqlCommand(creditAccount, accConn);
                    accCredit.Parameters.AddWithValue("@orderName", SqlDbType.UniqueIdentifier).Value = ordername.ToString();

                    accResult = accCredit.ExecuteNonQuery();
                    return accResult;
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
                finally
                {
                    accConn.Close();
                    accConn.Dispose();
                }
            }
        }

        //Method to Account for MSSR. Return Number of Rows affected in Int32
        public static Int32 accMSSR(String username, String domain, Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Int32 SurveySetID, Int32 SurveyNo, Decimal Depth)
        {
            try
            {
                String creditAccount = "INSERT INTO [accMSSR] ([clntUsername], [clntRealm], [msdsID], [srvyNo], [depth], [optrID], [clntID], [wpdID], [welID], [wswID], [runID], [pTime]) VALUES (@clntUsername, @clntRealm, @msdsID, @srvyNo, @depth, @optrID, @clntID, @wpdID, @welID, @wswID, @welID, @runID, @pTime)";
                using (SqlConnection accConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEAccountingDB"].ConnectionString))
                {
                    try
                    {
                        accConn.Open();
                        SqlCommand accCredit = new SqlCommand(creditAccount, accConn);
                        accCredit.Parameters.AddWithValue("@clntUsername", SqlDbType.UniqueIdentifier).Value = username.ToString();
                        accCredit.Parameters.AddWithValue("@clntRealm", SqlDbType.Int).Value = domain.ToString();
                        accCredit.Parameters.AddWithValue("@msdsID", SqlDbType.Int).Value = SurveySetID.ToString();
                        accCredit.Parameters.AddWithValue("@srvyNo", SqlDbType.Int).Value = SurveyNo.ToString();
                        accCredit.Parameters.AddWithValue("@depth", SqlDbType.Int).Value = Depth.ToString();
                        accCredit.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        accCredit.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        accCredit.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        accCredit.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        accCredit.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        accCredit.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        accCredit.Parameters.AddWithValue("@pTime", SqlDbType.DateTime).Value = DateTime.Now.ToString();
                        return Convert.ToInt32(accCredit.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        accConn.Close();
                        accConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        
    }
}