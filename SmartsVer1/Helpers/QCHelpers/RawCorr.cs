﻿using System;
using System.Data;
using System.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Collections.Generic;
using MNST = MathNet.Numerics.Statistics;
using MNFit = MathNet.Numerics.Fit;
using OPR = SmartsVer1.Helpers.QCHelpers.Opr;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using QC = SmartsVer1.QCHelpers.sdRawDataQC;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using Units = SmartsVer1.Helpers.DemogHelpers.UnitsConverter;
using DL = SmartsVer1.Helpers.DemogHelpers.DataLoader;
using RMN = MathNet.Numerics.RootFinding.RobustNewtonRaphson;

namespace SmartsVer1.Helpers.QCHelpers
{
    public class RawCorr
    {
        static void Main()
        { }

        public static Decimal GTF(Decimal Gx, Decimal Gy) //GTF - φ
        {
            try
            {
                if (Units.ToDegrees(Math.Atan2(((Double)Gy), -1 * ((Double)Gx))) < 0)
                {
                    return Convert.ToDecimal(Units.ToDegrees(Math.Atan2(((Double)Gy), -1 * ((Double)Gx))) + 360);
                }
                else
                {
                    return Convert.ToDecimal(Units.ToDegrees(Math.Atan2(((Double)Gy), -1 * ((Double)Gx))));
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal MTF(Decimal Bx, Decimal By) //MTF
        {
            try
            {
                Double BxVal = Convert.ToDouble(Bx);
                Double ByVal = Convert.ToDouble(By);
                if (BxVal > 0)
                {
                    if (ByVal > 0)
                    {
                        return Convert.ToDecimal((Math.Atan((-1 * ByVal) / ((BxVal))) * 180 / Math.PI) + 360);
                    }
                    else
                    {
                        return Convert.ToDecimal((Math.Atan((-1 * ByVal) / ((BxVal))) * 180 / Math.PI));
                    }
                }
                else
                {
                    return Convert.ToDecimal((Math.Atan((-1 * ByVal) / ((BxVal))) * 180 / Math.PI) + 180);
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal Declination(int IFR, Decimal ifrMDec, Decimal MDec) //Δ Dec
        {
            try
            {
                if (IFR.Equals(2))  /* 1 = No; 2 = Yes (FEDemog.tblIFRValue) */
                {
                    return (ifrMDec - MDec);
                }
                else
                {
                    return 0.25M;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal Inclination(Decimal Gx, Decimal Gy, Decimal Gz) //Inc - θ
        {
            try
            {
                return Decimal.Multiply((Decimal)(Math.Atan2(Math.Sqrt((Math.Pow((Double)Gx, 2)) + (Math.Pow((Double)Gy, 2))), (Double)Gz)), Decimal.Divide(180M, (Decimal)Math.PI));
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Int32 colV(Decimal colTVal) //Inc - Angle Condition 
        {
            try
            {
                if (colTVal <= 4.99M)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colW(Int32 chkShotValue, Int32 srvyID, Decimal colTCurr, Decimal colTPrev) //Inc - Fluctuations
        {
            try
            {
                if (chkShotValue.Equals(1) || srvyID.Equals(1))
                {
                    return 0.0000M;
                }
                else if (srvyID.Equals(2))
                {
                    return Decimal.Divide(colTCurr, 2);
                }
                else
                {
                    return Math.Abs(Decimal.Subtract(colTCurr, colTPrev));
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal MagAzimuth(Decimal GTF, Decimal Inclination, Decimal cGx, Decimal cGy, Decimal cGz, Decimal cBx, Decimal cBy, Decimal cBz) //Mag Azm - Ψm
        {
            try
            {
                Decimal constA = 0.0000M;
                Decimal constB = 0.0000M;
                Decimal valA = 0.0000M;
                Decimal valB = 0.0000M;
                Decimal valC = 0.0000M;
                Decimal valD = 0.0000M;
                Decimal valE = 0.0000M;
                Decimal valF = 0.0000M;

                constA = Convert.ToDecimal(Math.PI / 180);
                constB = Convert.ToDecimal(180 / Math.PI);
                valB = Decimal.Multiply(GTF, constA);
                valC = Decimal.Multiply(Inclination, constA);
                valD = Decimal.Multiply(Decimal.Subtract(Decimal.Multiply(cBx, (decimal)Math.Cos((double)valB)), Decimal.Multiply(cBy, (decimal)Math.Sin((double)valB))), (decimal)Math.Cos((double)valC));
                valE = Decimal.Multiply(cBz, (decimal)Math.Sin((double)valC));
                valF = Decimal.Add((Decimal.Multiply(cBx, (decimal)Math.Sin((double)valB))), (Decimal.Multiply(cBy, (decimal)Math.Cos((double)valB))));
                valA = Decimal.Add((Decimal.Multiply(((decimal)Math.Atan2((double)(Decimal.Multiply(-1, valF)), (double)(Decimal.Add(valD, valE)))), constB)), 360);

                if ((valA % 360) > 360)
                {
                    return (valA % 360) - 360;
                }
                else
                {
                    return (valA % 360);
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal GridAzimuthPlain(Decimal MagAzimuth, Decimal MDec, Decimal GridConvergence) //Azm - Ψ - Plain
        {
            try
            {
                Decimal valA = 0.00M;
                valA = Decimal.Subtract(Decimal.Add(MagAzimuth, MDec), GridConvergence);
                if (valA > (decimal)359.99)
                {
                    return Decimal.Subtract(valA, 360);
                }
                else if (valA < 0)
                {
                    return Decimal.Add(valA, 360);
                }
                else
                {
                    return valA;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal GridAzimuthIFR(Decimal MagAzimuth, Decimal ifrMDec, Decimal GridConvergence) //Azm - Ψ - IFR
        {
            try
            {
                Decimal valA = 0.00M;
                valA = Decimal.Add(MagAzimuth, ifrMDec);
                valA = Decimal.Subtract(valA, GridConvergence);
                if (valA > 359.99M)
                {
                    return Decimal.Subtract(valA, 360M);
                }
                else if (valA < 0)
                {
                    return Decimal.Add(valA, 360M);
                }
                else
                {
                    return valA;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal TrueAzimuthPlain(Decimal MagAzimuth, Decimal MDec) //Azm - Ψ - Plain
        {
            try
            {
                Decimal valA = 0.00M;
                valA = Decimal.Add(MagAzimuth, MDec);
                if (valA > (decimal)359.99)
                {
                    return Decimal.Subtract(valA, 360);
                }
                else if (valA < 0)
                {
                    return Decimal.Add(valA, 360);
                }
                else
                {
                    return valA;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal TrueAzimuthIFR(Decimal MagAzimuth, Decimal iMDec) //Azm - Ψ - IFR
        {
            try
            {
                Decimal valA = 0.00M;
                valA = Decimal.Add(MagAzimuth, iMDec);
                if (valA > (decimal)359.99)
                {
                    return Decimal.Subtract(valA, 360);
                }
                else if (valA < 0)
                {
                    return Decimal.Add(valA, 360);
                }
                else
                {
                    return valA;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal GridAzimuthFinal(Int32 ifrVal, Decimal MagAzimuthPlain, Decimal MagAzimuthIFR) //Grid - LC Azm - Ψ - Final
        {
            try
            {
                if (ifrVal.Equals(1)) /* 1 = No; 2 = Yes (FEDemog.tblIFRValue) */
                {
                    return MagAzimuthPlain;
                }
                else
                {
                    return MagAzimuthIFR;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal TrueAzimuthFinal(Int32 ifrVal, Decimal TrueAzimuthPlain, Decimal TrueAzimuthIFR) //True - LC Azm - Ψ - Final
        {
            try
            {
                if (ifrVal == 1) /* 1 = No; 2 = Yes (FEDemog.tblIFRValue) */
                {
                    return TrueAzimuthPlain;
                }
                else
                {
                    return TrueAzimuthIFR;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal FinalAzimuth(Int32 jobConvVal, Decimal GridAzimuthFinal, Decimal TrueAzimuthFinal) //LC Azm - Ψ - Final
        {
            try
            {
                if (jobConvVal.Equals(2)) /* 1 = True; 2 = Grid (FEDemog.tblJobConvergence) */
                {
                    return GridAzimuthFinal;
                }
                else
                {
                    return TrueAzimuthFinal;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colAH(Decimal colP, Decimal colT, Decimal correctedBx, Decimal correctedBy, Decimal colCR) //Mag Azm - Ψm'
        {
            try
            {
                if (((Math.Atan2((Double)(-1 * ((correctedBx) * (Decimal)Math.Sin((Double)colP * 3.1415926 / 180) + (correctedBy) * (Decimal)Math.Cos((Double)colP * 3.1415926 / 180))), (Double)((((correctedBx) * (Decimal)Math.Cos((Double)colP * 3.1415926 / 180) - (correctedBy) * (Decimal)Math.Sin((Double)colP * 3.1415926 / 180)) * (Decimal)Math.Cos((Double)colT * 3.1415926 / 180) + (colCR) * (Decimal)Math.Sin((Double)colT * 3.1415926 / 180)))) * (180 / 3.1415926) + 360) % 360) > 360)
                {
                    return (Decimal)(((Math.Atan2((Double)(-1 * ((correctedBx) * (Decimal)Math.Sin((Double)colP * 3.1415926 / 180) + (correctedBy) * (Decimal)Math.Cos((Double)colP * 3.1415926 / 180))), (Double)((((correctedBx) * (Decimal)Math.Cos((Double)colP * 3.1415926 / 180) - (correctedBy) * (Decimal)Math.Sin((Double)colP * 3.1415926 / 180)) * (Decimal)Math.Cos((Double)colT * 3.1415926 / 180) + (colCR) * (Decimal)Math.Sin((Double)colT * 3.1415926 / 180)))) * (180 / 3.1415926) + 360) % 360) - 360);
                }
                else
                {
                    return (Decimal)((Math.Atan2((Double)(-1 * (((correctedBx) * (Decimal)Math.Sin((Double)colP * 3.1415926 / 180) + (correctedBy) * (Decimal)Math.Cos((Double)colP * 3.1415926 / 180)))), (Double)((((correctedBx) * (Decimal)Math.Cos((Double)colP * 3.1415926 / 180) - (correctedBy) * (Decimal)Math.Sin((Double)colP * 3.1415926 / 180)) * (Decimal)Math.Cos((Double)colT * 3.1415926 / 180) + (colCR) * (Decimal)Math.Sin((Double)colT * 3.1415926 / 180)))) * (180 / 3.1415926) + 360) % 360);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal colAI(Decimal colAH, Decimal MagDec, Decimal GridConverg) //Azm - Ψ - Plain
        {
            try
            {
                Decimal val1 = (Decimal.Add(colAH, (Decimal.Subtract(MagDec, GridConverg))));

                if (val1 > 359.99M)
                {
                    return Decimal.Subtract(val1, 360.00M);
                }
                else if (val1 < 0.00M)
                {
                    return Decimal.Add(val1, 360.00M);
                }
                else
                {
                    return val1;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colAJ(Decimal colAH, Decimal ifrMagDec, Decimal GridConverg) //Azm - Ψ - IFR
        {
            try
            {
                Decimal val1 = (Decimal.Add(colAH, (Decimal.Subtract(ifrMagDec, GridConverg))));

                if (val1 > 359.99M)
                {
                    return Decimal.Subtract(val1, 360.00M);
                }
                else if (val1 < 0.00M)
                {
                    return Decimal.Add(val1, 360.00M);
                }
                else
                {
                    return val1;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colAK(Decimal colAH, Decimal MagDec) //Azm - Ψ - Plain
        {
            try
            {
                Decimal val1 = Decimal.Add(colAH, MagDec);

                if (val1 > 359.99M)
                {
                    return Decimal.Subtract(val1, 360.00M);
                }
                else if (val1 < 0.00M)
                {
                    return Decimal.Add(val1, 360.00M);
                }
                else
                {
                    return val1;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colAL(Decimal colAH, Decimal ifrMagDec) //Azm - Ψ - IFR
        {
            try
            {
                Decimal val1 = Decimal.Add(colAH, ifrMagDec);

                if (val1 > 359.99M)
                {
                    return Decimal.Subtract(val1, 360.00M);
                }
                else if (val1 < 0.00M)
                {
                    return Decimal.Add(val1, 360.00M);
                }
                else
                {
                    return val1;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colAM(Int32 ifrVal, Decimal colAI, Decimal colAJ) //Grid - LC Azm - Ψ - Final
        {
            try
            {
                if (ifrVal.Equals(1)) //1 = NO; 2 = YES
                {
                    return colAI;
                }
                else
                {
                    return colAJ;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colAN(Int32 ifrVal, Decimal colAK, Decimal colAL) //True - LC Azm - Ψ - Final
        {
            try
            {
                if (ifrVal.Equals(1)) //1 = NO ; 2 = YES
                {
                    return colAK;
                }
                else
                {
                    return colAL;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colAO(Int32 JobConverg, Decimal colAM, Decimal colAN) //Corrected Azm - Ψ' - Final
        {
            try
            {
                /* 2 = Grid , 1 = True */
                if (JobConverg.Equals(2))
                {
                    return colAM;
                }
                else
                {
                    return colAN;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal colAP(Decimal colY, Decimal colAH) //Δ Azm Corrected
        {
            try
            {
                return Decimal.Subtract(colAH, colY);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal colAR(Int32 SrvyID, Decimal colYCurr, Decimal colYPrev) //Azm - Fluctuations
        {
            try
            {
                if (SrvyID.Equals(1))
                {
                    return 0.00M;
                }
                else
                {
                    return Convert.ToDecimal(Math.Abs(Decimal.Subtract(colYCurr, colYPrev)));
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal colAS(Decimal colW, Decimal colAR) //Inc-Azm Dir Resultant Vector
        {
            try
            {
                Double colWVal = Convert.ToDouble(colW);
                Double colARVal = Convert.ToDouble(colAR);
                return Convert.ToDecimal(Math.Sqrt((((Math.Pow(colWVal, 2)) + (Math.Pow(colARVal, 2))))));
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colAT(Int32 srvID, Int32 colV, Decimal colW, Decimal colAS, Decimal currDepth, Decimal prevDepth) //DLS/Unit
        {
            try
            {
                if (colV.Equals(1))
                {
                    if (srvID.Equals(1))
                    {
                        return colW;
                    }
                    else if (Decimal.Subtract(currDepth, prevDepth).Equals(0))
                    {
                        return 0.00M;
                    }
                    else
                    {
                        return Decimal.Divide(colW, Decimal.Subtract(currDepth, prevDepth));
                    }
                }
                else
                {
                    if (srvID == 1)
                    {
                        return colAS;
                    }
                    else if (Decimal.Subtract(currDepth, prevDepth).Equals(0))
                    {
                        return 0.00M;
                    }
                    else
                    {
                        return Decimal.Divide(colAS, Decimal.Subtract(currDepth, prevDepth));
                    }
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colAU(Int32 srvID, Int32 depU, Int32 colV, Decimal colAT, Decimal ref1, Decimal ref2, Decimal ref3, Decimal ref4) //Dir Cond Ref
        {
            try
            {
                if (srvID.Equals(1))
                {
                    return 0.00M;
                }
                else
                {
                    if (colV.Equals(1) && depU.Equals(5))
                    {
                        return Decimal.Subtract(colAT, ref3);
                    }
                    else if (colV.Equals(1) && depU.Equals(4))
                    {
                        return Decimal.Subtract(colAT, ref4);
                    }
                    else if (colV.Equals(0) && depU.Equals(5))
                    {
                        return Decimal.Subtract(colAT, ref1);
                    }
                    else
                    {
                        return Decimal.Subtract(colAT, ref2);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Int32 colAV(Decimal colAU) //Dir Cond
        {
            if (colAU > 0.01M)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }

        public static Int32 colAW(Decimal colT) //SC - Condt 1
        {
            if (colT > 70 || colT.Equals(70))
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }

        public static Int32 colAX(Decimal colY) //SC - Condt 2
        {
            try
            {
                if (colY.Equals(0) || colY <= 69.99M)
                {
                    return 1;
                }
                else if (colY.Equals(70.00M) || colY <= 110.00M)
                {
                    return 0;
                }
                else if (colY.Equals(110.01M) || colY <= 249.99M)
                {
                    return 1;
                }
                else if (colY.Equals(250.00M) || colY <= 290.00M)
                {
                    return 0;
                }
                else if (colY.Equals(290.01M) || colY <= 359.99M)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 colAY(Int32 colAW, Int32 colAX) //Short Collar Application Condition
        {
            try
            {
                if (colAW.Equals(1))
                {
                    return 1;
                }
                else if (colAX.Equals(1))
                {
                    return 1;
                }
                else if (colAX.Equals(1) && colAW.Equals(1))
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal Dip(Decimal Gx, Decimal Gy, Decimal Gz, Decimal Bx, Decimal By, Decimal Bz, Decimal colCF, Decimal colCA) //LC - Dip
        {
            try
            {
                Double valGx = Convert.ToDouble(Gx);
                Double valGy = Convert.ToDouble(Gy);
                Double valGz = Convert.ToDouble(Gz);
                Double valBx = Convert.ToDouble(Bx);
                Double valBy = Convert.ToDouble(By);
                Double valBz = Convert.ToDouble(Bz);
                Double colCFVal = Convert.ToDouble(colCF);
                Double colCAVal = Convert.ToDouble(colCA);
                return Convert.ToDecimal(Math.Asin(((valGx * valBx) + (valGy * valBy) + (valGz * valBz)) / (colCFVal * colCAVal)) * (180 / Math.PI));
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal colBB(Int32 srvID, Decimal colBACurr, Decimal colBAPrev) //∆Dip LC - Fluctuations
        {
            try
            {
                if (srvID.Equals(1))
                {
                    return 0.00M;
                }
                else if (srvID.Equals(2))
                {
                    return Decimal.Divide(Decimal.Subtract(colBACurr, colBAPrev), 2);
                }
                else
                {
                    return Decimal.Subtract(colBACurr, colBAPrev);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal colBC(Int32 ifrVal, Decimal ifrDip, Decimal cnvDip, Decimal colBA) //∆Dip LC
        {
            try
            {
                if (ifrVal.Equals(2)) /* 1 = No; 2 = Yes*/
                {
                    return Decimal.Subtract(colBA, ifrDip);
                }
                else
                {
                    return Decimal.Subtract(colBA, cnvDip);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 colBD(Int32 srvID, Decimal dflLowerDip, Decimal dflUpperDip, Decimal colBC, String dbConn) //∆Dip LC Condition for Trend Analysis
        {
            try
            {
                Decimal prevCol1 = -99.0000M;

                Int32 srvID1 = -99;

                if (!srvID.Equals(1))
                {
                    srvID1 = srvID - 1;

                    using (SqlConnection metaCon = new SqlConnection(dbConn))
                    {
                        try
                        {
                            String selMetaBC1 = "SELECT colBC FROM RawQCResults WHERE srvyID = @srvyID";
                            SqlCommand getMetaBC1 = new SqlCommand(selMetaBC1, metaCon);
                            getMetaBC1.Parameters.AddWithValue("@srvyID", SqlDbType.Int).Value = srvID1.ToString();

                            metaCon.Open();
                            using (SqlDataReader readBC1 = getMetaBC1.ExecuteReader())
                            {
                                try
                                {
                                    if (readBC1.HasRows)
                                    {
                                        while (readBC1.Read())
                                        {
                                            prevCol1 = readBC1.GetDecimal(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readBC1.Close();
                                    readBC1.Dispose();
                                }
                            }

                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            metaCon.Close();
                            metaCon.Dispose();
                        }
                    }
                }

                if (srvID.Equals(1))
                {
                    if (colBC < (-1 * dflLowerDip))
                    {
                        return 3;
                    }
                    else if (colBC > dflLowerDip)
                    {
                        return 2;
                    }
                    else
                    {
                        return 1;
                    }
                }
                else if (((colBC - prevCol1) > dflUpperDip) || ((colBC - prevCol1) < (-1 * dflUpperDip)))
                {
                    return 4;
                }
                else if ((colBC) < (-1 * dflLowerDip))
                {
                    return 3;
                }
                else if (colBC > dflLowerDip)
                {
                    return 2;
                }
                else
                {
                    return 1;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal GxRef(Decimal colP, Decimal colT, Decimal cnvIFRGT) // Gx'
        {
            try
            {
                return (-1 * cnvIFRGT * (Decimal)Math.Sin((Double)Units.ToRadians((Double)colT)) * (Decimal)Math.Cos((Double)Units.ToRadians((Double)colP)));                
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal GyRef(Decimal colP, Decimal colT, Decimal cnvIFRGT) //GY'
        {
            try
            {
                return (cnvIFRGT * (Decimal)Math.Sin((Double)Units.ToRadians((Double)colT)) * (Decimal)Math.Sin((Double)Units.ToRadians((Double)colP)));                
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal GzRef(Decimal colT, Decimal cnvIFRGT) //Gz'
        {
            try
            {
                return (cnvIFRGT * (Decimal)Math.Cos((Double)Units.ToRadians((Double)colT)));                
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal BxRef(Decimal cnvIFRBT, Decimal cnvIFRDip, Decimal colP, Decimal colT, Decimal colY) //Bx'-ref
        {
            try
            {
                Double const2 = Units.ToRadians((Double)colY);
                Double const3 = Units.ToRadians((Double)colT);
                Double const4 = Units.ToRadians((Double)colP);
                Double const5 = Units.ToRadians((Double)cnvIFRDip);

                return Decimal.Multiply(cnvIFRBT, (Decimal.Subtract((Decimal.Subtract((Decimal.Multiply((Decimal)Math.Sin((Double)const5), (Decimal)Math.Sin((Double)const3))), (Decimal.Multiply((Decimal.Multiply((Decimal)Math.Sin((Double)const5), (Decimal)Math.Sin((Double)const3))), (Decimal)Math.Cos((Double)const4))))), (Decimal.Multiply((Decimal.Multiply((Decimal)Math.Cos((Double)const5), (Decimal)Math.Sin((Double)const2))), (Decimal)Math.Sin((Double)const4))))));                
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal ByRef(Decimal cnvIFRDip, Decimal cnvIFRBT, Decimal colP, Decimal colT, Decimal colY) //By'-ref
        {
            try
            {
                Double iDip = Convert.ToDouble(cnvIFRDip);
                Double iBT = Convert.ToDouble(cnvIFRBT);
                Double colPVal = Convert.ToDouble(colP);
                Double colTVal = Convert.ToDouble(colT);
                Double colYVal = Convert.ToDouble(colY);

                return Convert.ToDecimal((-1 * iBT) * (((Math.Cos(Units.ToRadians(iDip)) * (Math.Cos(Units.ToRadians(colYVal)) * (Math.Cos(Units.ToRadians(colTVal)) * Math.Sin(Units.ToRadians(colPVal)))))) + ((((Math.Cos(Units.ToRadians(iDip)) * (Math.Sin(Units.ToRadians(colYVal)) * Math.Cos(Units.ToRadians(colPVal))))) - ((Math.Sin(Units.ToRadians(iDip)) * (Math.Sin(Units.ToRadians(colTVal)) * Math.Sin(Units.ToRadians(colPVal)))))))));                
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal BzRef(Decimal cnvIFRDip, Decimal cnvIFRBT, Decimal colT, Decimal colY) //Bz'-ref
        {
            try
            {
                Double iDip = Convert.ToDouble(cnvIFRDip);
                Double iBT = Convert.ToDouble(cnvIFRBT);
                Double colTVal = Convert.ToDouble(colT);
                Double colYVal = Convert.ToDouble(colY);

                return Convert.ToDecimal((iBT * ((((Math.Sin(((iDip * (Math.PI / 180)))) * Math.Cos(((colTVal * (Math.PI / 180)))))) + ((Math.Cos((iDip * (Math.PI / 180)))) * ((Math.Cos(((colYVal * (Math.PI / 180)))) * Math.Sin((colTVal * (Math.PI / 180))))))))));                
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal BxBias(Int32 srvyID, Int32 runID, Decimal IBTotal, Decimal iGTotal, Decimal iDip, Decimal currBx, Decimal currBy, Decimal currBz, Decimal colBIVal, Decimal colBJVal, Decimal colCNVal, Decimal colHQVal, Decimal TSBias, DataTable RawData) //Bx_Bias
        {
            try
            {
                Int32 ValueA = -99, sID = 1;
                Decimal ValueB = -99.0000M;
                Decimal igx, igy, igz, ibx, iby, ibz, prevBIVal, prevBJVal, prevBKVal, prevCKVal, prevCLVal, prevCMVal, prevCNVal, prevPVal, prevTVal, prevYVal;
                List<Double> ByList = new List<Double>();
                List<Double> HQList = new List<Double>();
                Double[] byArr = new Double[] { };
                Double[] hqArr = new Double[] { };

                if (!srvyID.Equals(1))
                {
                    foreach (DataRow row in RawData.Rows)
                    {
                        while (sID < srvyID)
                        {
                            igx = Convert.ToDecimal(row["rGx"]);
                            igy = Convert.ToDecimal(row["rGy"]);
                            igz = Convert.ToDecimal(row["rGz"]);
                            ibx = Convert.ToDecimal(row["rBx"]);
                            iby = Convert.ToDecimal(row["rBy"]);                            
                            ibz = Convert.ToDecimal(row["rBz"]);
                            ByList.Add(Convert.ToDouble(iby));
                            ByList.Add(Convert.ToDouble(currBy));
                            byArr = ByList.ToArray();
                            prevPVal = GTF(igx, igy);
                            prevTVal = Inclination(igx, igy, igz);
                            prevYVal = MagAzimuth(prevPVal, prevTVal, igx, igy, igz, ibx, iby, ibz);
                            prevBIVal = BxRef(IBTotal, iDip, prevPVal, prevTVal, prevYVal);
                            prevBJVal = ByRef(iDip, IBTotal, prevPVal, prevTVal, prevYVal);
                            prevBKVal = BzRef(iDip, IBTotal, prevTVal, prevYVal);
                            prevCKVal = colCK(ibz, prevTVal, prevYVal, prevBKVal, iDip, IBTotal);
                            prevCLVal = colCL(prevTVal, prevYVal, prevCKVal, iDip);
                            prevCMVal = colCM(prevYVal, prevCLVal);
                            prevCNVal = colCN(prevTVal, prevCMVal, iDip, IBTotal);
                            HQList.Add(Convert.ToDouble(colHQ(ibx, iby, ibz, prevBIVal, prevBJVal, prevCNVal, TSBias)));
                            HQList.Add(Convert.ToDouble(colHQVal));
                            hqArr = HQList.ToArray();
                            sID++;
                        }
                    }
                }

                if (((currBx - colBIVal) > TSBias) || ((currBy - colBJVal) > TSBias) || ((currBz - colCNVal) > TSBias) || ((currBx - colBIVal) > (-1 * TSBias)) || ((currBy - colBJVal) < (-1 * TSBias)) || ((currBz - colCNVal) < (-1 * TSBias)))
                {
                    ValueA = 0;
                }
                else
                {
                    ValueA = 1;
                }
                if (ValueA.Equals(0))
                {
                    ValueB = 0;
                }
                else
                {
                    ValueB = 1;
                }

                if (ValueB.Equals(0))
                {
                    if (srvyID.Equals(1))
                    {
                        return colHQVal;
                    }
                    else if (Convert.ToDecimal(MNST.Statistics.Variance(ByList)).Equals(0))
                    {
                        if (ValueB.Equals(0))
                        {
                            ValueB = colBJVal;
                        }
                        else
                        {
                            ValueB = 0.00M;
                        }
                        return ValueB - currBy;
                    }
                    else
                    {
                        Tuple<Double, Double> p = MNFit.Line(byArr, hqArr);
                        return Convert.ToDecimal(p.Item1);
                    }
                }
                else
                {
                    return 0.0000M;
                }

            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal ByBias(Int32 srvyID, Int32 runID, Decimal IBTotal, Decimal iGTotal, Decimal iDip, Decimal currBx, Decimal currBy, Decimal currBz, Decimal colBIVal, Decimal colBJVal, Decimal colCNVal, Decimal colHRVal, Decimal TSBias, DataTable RawData) //By_Bias
        {
            try
            {
                Int32 ValueA = -99, sID = 1;
                Decimal ValueB = -99.0000M;
                Decimal igx, igy, igz, ibx, iby, ibz, prevBIVal, prevBJVal, prevBKVal, prevCKVal, prevCLVal, prevCMVal, prevCNVal, prevPVal, prevTVal, prevYVal;
                List<Double> ByList = new List<Double>();
                List<Double> HRList = new List<Double>();
                Double[] byArr = new Double[] { };
                Double[] hrArr = new Double[] { };

                if (!srvyID.Equals(1))
                {
                    foreach (DataRow row in RawData.Rows)
                    {
                        while (sID < srvyID)
                        {
                            igx = Convert.ToDecimal(row["rGx"]);
                            igy = Convert.ToDecimal(row["rGy"]);
                            igz = Convert.ToDecimal(row["rGz"]);
                            ibx = Convert.ToDecimal(row["rBx"]);
                            iby = Convert.ToDecimal(row["rBy"]);
                            ibz = Convert.ToDecimal(row["rBz"]);
                            ByList.Add(Convert.ToDouble(iby));
                            ByList.Add(Convert.ToDouble(currBy));
                            //byArr = ByList.ToArray();
                            prevPVal = GTF(igx, igy);
                            prevTVal = Inclination(igx, igy, igz);
                            prevYVal = MagAzimuth(prevPVal, prevTVal, igx, igy, igz, ibx, iby, ibz);
                            prevBIVal = BxRef(IBTotal, iDip, prevPVal, prevTVal, prevYVal);
                            prevBJVal = ByRef(iDip, IBTotal, prevPVal, prevTVal, prevYVal);
                            prevBKVal = BzRef(iDip, IBTotal, prevTVal, prevYVal);
                            prevCKVal = colCK(ibz, prevTVal, prevYVal, prevBKVal, iDip, IBTotal);
                            prevCLVal = colCL(prevTVal, prevYVal, prevCKVal, iDip);
                            prevCMVal = colCM(prevYVal, prevCLVal);
                            prevCNVal = colCN(prevTVal, prevCMVal, iDip, IBTotal);
                            HRList.Add(Convert.ToDouble(colHQ(ibx, iby, ibz, prevBIVal, prevBJVal, prevCNVal, TSBias)));
                            HRList.Add(Convert.ToDouble(colHRVal));
                            //hrArr = HRList.ToArray();
                            sID++;
                        }
                    }
                }
                byArr = ByList.ToArray();
                hrArr = HRList.ToArray();

                if (((currBx - colBIVal) > TSBias) || ((currBy - colBJVal) > TSBias) || ((currBz - colCNVal) > TSBias) || ((currBx - colBIVal) > (-1 * TSBias)) || ((currBy - colBJVal) < (-1 * TSBias)) || ((currBz - colCNVal) < (-1 * TSBias)))
                {
                    ValueA = 0;
                }
                else
                {
                    ValueA = 1;
                }
                if (ValueA.Equals(0))
                {
                    ValueB = 0;
                }
                else
                {
                    ValueB = 1;
                }

                if (ValueB.Equals(0))
                {
                    if (srvyID.Equals(1))
                    {
                        return colHRVal;
                    }
                    else if (Convert.ToDecimal(MNST.Statistics.Variance(byArr)).Equals(0))
                    {
                        if (ValueB.Equals(0))
                        {
                            ValueB = colBJVal;
                        }
                        else
                        {
                            ValueB = 0.00M;
                        }
                        return ValueB - currBy;
                    }
                    else
                    {
                        Tuple<Double, Double> p = MNFit.Line(byArr, hrArr);
                        return Convert.ToDecimal(p.Item1);
                    }
                }
                else
                {
                    return 0.0000M;
                }

            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal BxScaleFactor(Int32 srvyID, Decimal iBTotal, Decimal iDip, Decimal BxCurr, Decimal colBICurr, DataTable RawData) //Bx_SF
        {
            try
            {
                Decimal iReply = -99.00M;
                Int32 sID = 1;
                Decimal igx, igy, igz, ibx, iby, ibz, iColP, iColT, iColY, iBxRef;
                List<Double> colBIList = new List<Double>();
                List<Double> BxList = new List<Double>();
                Double[] bx = new Double[] { };
                Double[] bi = new Double[] { };
                if (srvyID.Equals(1) || srvyID.Equals(2))
                {
                    iReply = 1.00M;
                }
                else
                {
                    foreach (DataRow row in RawData.Rows)
                    {
                        while (sID < srvyID)
                        {
                            igx = Convert.ToDecimal(row["rGx"]);
                            igy = Convert.ToDecimal(row["rGy"]);
                            igz = Convert.ToDecimal(row["rGz"]);
                            ibx = Convert.ToDecimal(row["rBx"]);
                            BxList.Add(Convert.ToDouble(ibx));
                            BxList.Add(Convert.ToDouble(BxCurr));
                            iby = Convert.ToDecimal(row["rBy"]);
                            ibz = Convert.ToDecimal(row["rBz"]);
                            iColP = GTF(igx, igy);
                            iColT = Inclination(igx, igy, igz);
                            iColY = MagAzimuth(iColP, iColT, igx, igy, igz, ibx, iby, ibz);
                            iBxRef = BxRef(iBTotal, iDip, iColP, iColT, iColY);
                            colBIList.Add(Convert.ToDouble(iBxRef));
                            colBIList.Add(Convert.ToDouble(colBICurr));
                            sID++;
                        }
                    }                
                    bx = BxList.ToArray();
                    bi = colBIList.ToArray();

                    if (MNST.Statistics.Variance(bx).Equals(0))
                    {
                        iReply = 1.00M;
                    }
                    else
                    {
                        Tuple<Double, Double> p = MNFit.Line(bx, bi);
                        iReply = Convert.ToDecimal(p.Item2);
                    }                    
                }
                return (Decimal)iReply;
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal ByScaleFactor(Int32 srvyID, Decimal iBTotal, Decimal iDip, Decimal ByCurr,  Decimal colBJCurr, DataTable RawData) //By_SF
        {
            try
            {
                Decimal iReply = -99.00M;
                Int32 sID = 1;
                Decimal igx, igy, igz, ibx, iby, ibz, iColP, iColT, iColY, iByRef;
                List<Double> colBJList = new List<Double>();
                List<Double> ByList = new List<Double>();
                Double[] by = new Double[] { };
                Double[] bj = new Double[] { };
                if (srvyID.Equals(1))
                {
                    return 1.00M;
                }
                else
                {
                    foreach (DataRow row in RawData.Rows)
                    {
                        while (sID < srvyID)
                        {
                            igx = Convert.ToDecimal(row["rGx"]);
                            igy = Convert.ToDecimal(row["rGy"]);
                            igz = Convert.ToDecimal(row["rGz"]);
                            ibx = Convert.ToDecimal(row["rBx"]);                            
                            iby = Convert.ToDecimal(row["rBy"]);
                            ByList.Add(Convert.ToDouble(iby));
                            ByList.Add(Convert.ToDouble(ByCurr));
                            ibz = Convert.ToDecimal(row["rBz"]);
                            iColP = GTF(igx, igy);
                            iColT = Inclination(igx, igy, igz);
                            iColY = MagAzimuth(iColP, iColT, igx, igy, igz, ibx, iby, ibz);
                            iByRef = ByRef(iDip, iBTotal, iColP, iColT, iColY);
                            colBJList.Add(Convert.ToDouble(iByRef));
                            colBJList.Add(Convert.ToDouble(colBJCurr));
                            sID++;
                        }
                    }
                    by = ByList.ToArray();
                    bj = colBJList.ToArray();

                    if (MNST.Statistics.Variance(by).Equals(0))
                    {
                        iReply = 1.00M;
                    }
                    else
                    {
                        Tuple<Double, Double> p = MNFit.Line(by, bj);
                        iReply = Convert.ToDecimal(p.Item2);
                    }
                    return Convert.ToDecimal(iReply);
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal correctedBx(Decimal Bx, Decimal BxScaleFactor, Decimal BxBias) //Bx_corrected
        {
            try
            {
                return Decimal.Add(Bx, Decimal.Add(BxScaleFactor, BxBias));
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal correctedBy(Decimal By, Decimal ByScaleFactor, Decimal ByBias) //By_corrected
        {
            try
            {
                return Decimal.Add(By, Decimal.Add(ByScaleFactor, ByBias));

            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal deltaBxCorrected(Decimal Bx, Decimal correctedBx) //ΔBx_corrected
        {
            try
            {
                return Decimal.Subtract(correctedBx, Bx);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal deltaByCorrected(Decimal By, Decimal correctedBy) //ΔBy_corrected
        {
            try
            {
                return Decimal.Subtract(correctedBy, By);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal correctedBTotal(Decimal correctedBx, Decimal correctedBy, Decimal colCU) //Bt Corrected
        {
            try
            {
                return Convert.ToDecimal(Math.Sqrt(Math.Abs((Math.Pow((Double)(correctedBx), 2)) + (Math.Pow((Double)(correctedBy), 2)) + (Math.Pow((Double)(colCU), 2)))));
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal correctedDip(Decimal Gx, Decimal Gy, Decimal Gz, Decimal correctedBx, Decimal correctedBy, Decimal colCU, Decimal colCF, Decimal colBU) //Dip corrected
        {
            try
            {
                return Decimal.Divide((Decimal.Multiply((Convert.ToDecimal(Math.Asin((double)(Decimal.Divide((Decimal.Add((Decimal.Add((Decimal.Multiply(correctedBx, Gx)), (Decimal.Multiply(correctedBy, Gy)))), (Decimal.Multiply(colCU, Gz)))), (Decimal.Multiply(colCF, colBU))))))), 180.00M)), ((decimal)Math.PI));
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal deltaCorrectedBTotal(Int32 ifrVal, Decimal cnvBT, Decimal cnvIFRBT, Decimal correctedBTotal) //ΔBt corrected
        {
            try
            {
                if (ifrVal.Equals(1))
                {
                    return (-1 * (cnvBT - correctedBTotal));
                }
                else
                {
                    return (-1 * (cnvIFRBT - correctedBTotal));
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal deltaCorrectedDip(Int32 ifrVal, Decimal cnvDip, Decimal cnvIFRDip, Decimal correctedDip) //ΔDip corrected
        {
            try
            {
                if (ifrVal.Equals(1))
                {
                    return Decimal.Multiply(-1, Decimal.Subtract(cnvDip, correctedDip));
                }
                else
                {
                    return Decimal.Multiply(-1, Decimal.Subtract(cnvIFRDip, correctedDip));
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal Boxy(Decimal Bx, Decimal By) //Boxy
        {
            try
            {
                return Convert.ToDecimal(Math.Sqrt((Double)Math.Abs(Decimal.Add((Decimal)Math.Pow((Double)Bx, (Double)2), (Decimal)Math.Pow((Double)By, (Double)2)))));
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal BTotal(Decimal Bx, Decimal By, Decimal Bz) //Btotal
        {
            try
            {
                Double BxVal = Convert.ToDouble(Bx);
                Double ByVal = Convert.ToDouble(By);
                Double BzVal = Convert.ToDouble(Bz);

                return Convert.ToDecimal(Math.Sqrt((Math.Abs((((Math.Pow(BxVal, 2) + Math.Pow(ByVal, 2))) + Math.Pow(BzVal, 2))))));
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colCB(Int32 srvID, Decimal colCAcurr, Decimal colCAprev) //∆Bt - Fluctuations
        {
            try
            {
                Double CACurr = Convert.ToDouble(colCAcurr);
                Double CAPrev = Convert.ToDouble(colCAprev);

                if (srvID.Equals(1))
                {
                    return 0.00M;
                }
                else if (srvID.Equals(2))
                {
                    return Convert.ToDecimal((CACurr - CAPrev) / 2);
                }
                else
                {
                    return Convert.ToDecimal(CACurr - CAPrev);
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Int32 colCC(Int32 srvID, Decimal colCD, Decimal dflLBT, Decimal dflUBT, String dbConn) //∆Bt Condition for Trend Analysis
        {
            try
            {
                Decimal prevCol1 = -99.0000M;

                Int32 srvID1 = -99;

                if (!srvID.Equals(1))
                {
                    srvID1 = srvID - 1;

                    using (SqlConnection metaCon = new SqlConnection(dbConn))
                    {
                        try
                        {
                            String selMetaCD1 = "SELECT colCD FROM RawQCResults WHERE srvyID = @srvyID";
                            SqlCommand getMetaCD1 = new SqlCommand(selMetaCD1, metaCon);
                            getMetaCD1.Parameters.AddWithValue("@srvyID", SqlDbType.Int).Value = srvID1.ToString();

                            metaCon.Open();
                            using (SqlDataReader readCD1 = getMetaCD1.ExecuteReader())
                            {
                                try
                                {
                                    if (readCD1.HasRows)
                                    {
                                        while (readCD1.Read())
                                        {
                                            prevCol1 = readCD1.GetDecimal(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readCD1.Close();
                                    readCD1.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            metaCon.Close();
                            metaCon.Dispose();
                        }
                    }
                }

                if (srvID.Equals(1))
                {
                    if (colCD < -1 * dflLBT)
                    {
                        return 3;
                    }
                    if (colCD > dflLBT)
                    {
                        return 2;
                    }
                    else
                    {
                        return 1;
                    }
                }
                else if (((colCD - prevCol1) > dflUBT) || ((colCD - prevCol1) < (-1 * dflUBT)))
                {
                    return 4;
                }
                else if (colCD < (-1 * dflLBT))
                {
                    return 3;
                }
                else if (colCD > dflLBT)
                {
                    return 2;
                }
                else
                {
                    return 1;
                }

            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colCD(Int32 ifrVal, Decimal cnvBT, Decimal cnvIFRBT, Decimal colCA) //∆Btotal - LC
        {
            try
            {
                if (ifrVal.Equals(2))
                {
                    return Decimal.Subtract(colCA, cnvIFRBT);
                }
                else
                {
                    return Decimal.Subtract(colCA, cnvBT);
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal Goxy(Decimal Gx, Decimal Gy) //Goxy
        {
            try
            {
                Double GxVal = Convert.ToDouble(Gx);
                Double GyVal = Convert.ToDouble(Gy);

                return Convert.ToDecimal(Math.Sqrt(Math.Abs((Math.Pow(GxVal, 2) + Math.Pow(GyVal, 2)))));
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal GTotal(Decimal Gx, Decimal Gy, Decimal Gz) //Gtotal
        {
            try
            {
                Double GxVal = Convert.ToDouble(Gx);
                Double GyVal = Convert.ToDouble(Gy);
                Double GzVal = Convert.ToDouble(Gz);

                return Convert.ToDecimal(Math.Sqrt(Math.Abs((Math.Pow(GxVal, 2) + ((Math.Pow(GyVal, 2) + Math.Pow(GzVal, 2)))))));
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colCG(Int32 ifrVal, Decimal colCF, Decimal cnvGT, Decimal cnvIFRGT) //∆G 
        {
            try
            {
                if (ifrVal.Equals(2))
                {
                    return Decimal.Subtract(colCF, cnvIFRGT);
                }
                else
                {
                    return Decimal.Subtract(colCF, cnvGT);
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colCH(Int32 srvID, Decimal colCFcurr, Decimal colCFprev) //∆G- Fluctuations
        {
            try
            {
                if (srvID.Equals(1))
                {
                    return 0.00M;
                }
                else if (srvID.Equals(2))
                {
                    return Decimal.Divide(Decimal.Subtract(colCFcurr, colCFprev), 2);
                }
                else
                {
                    return Decimal.Subtract(colCFcurr, colCFprev);
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Int32 colCI(Int32 srvID, Decimal colCG, Decimal dflLowerGT, Decimal dflUpperGT, Decimal Gx, Decimal Gy, Decimal Gz) //∆G Condition for Trend Analysis
        {
            try
            {
                if ((Math.Abs(Gx) > dflUpperGT) || (Math.Abs(Gy) > dflUpperGT) || (Math.Abs(Gz) > dflUpperGT))
                {
                    return 4;
                }
                else if (Gx.Equals(0.00M) && Gy.Equals(0.00M))
                {
                    return 4;
                }
                if (colCG > dflLowerGT)
                {
                    return 2;
                }
                else if (colCG < (-1 * dflLowerGT))
                {
                    return 3;
                }
                else
                {
                    return 1;
                }

            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        private static Decimal dBTotal(Decimal valBTotal, Decimal ifrBTotal)
        {
            try
            {
                return Decimal.Subtract(valBTotal , ifrBTotal);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal dDip(Decimal valDip, Decimal ifrDip)
        {
            try
            {
                return Decimal.Subtract(valDip, ifrDip);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal colCK(Decimal rawBz, Decimal Inclination, Decimal MagAzimuth, Decimal BzRef, Decimal cnvIFRDip, Decimal cnvIFRBT) //dλ = Del Dip
        {
            try
            {
                Double rawBzVal = Convert.ToDouble(rawBz);
                Double valInclination = Convert.ToDouble(Inclination);
                Double valMagAzimuth = Convert.ToDouble(MagAzimuth);
                Double valBzRef = Convert.ToDouble(BzRef);
                Double cnvIFRDipVal = Convert.ToDouble(cnvIFRDip);
                Double cnvIFRBTVal = Convert.ToDouble(cnvIFRBT);

                return Convert.ToDecimal((((((((Units.ToRadians(valInclination)) - (Units.ToRadians(cnvIFRDipVal)))) / cnvIFRBTVal)) * ((((Math.Cos(Units.ToRadians(valInclination))) * (Math.Cos(Units.ToRadians(cnvIFRDipVal))))) - (((Math.Sin(Units.ToRadians(valInclination))) * ((Math.Cos(Units.ToRadians(valMagAzimuth))) * (Math.Sin(Units.ToRadians(cnvIFRDipVal))))))))) * (180 / Math.PI));                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal colCL(Decimal Inclination, Decimal MagAzimuth, Decimal colCK, Decimal cnvIFRDip) //dAz1
        {
            try
            {
                Double valInclination = Convert.ToDouble(Inclination);
                Double valMagAzimuth = Convert.ToDouble(MagAzimuth);
                Double colCKVal = Convert.ToDouble(colCK);
                Double cnvIFRDipVal = Convert.ToDouble(cnvIFRDip);

                return Convert.ToDecimal(((-1 * ((colCKVal * ((Math.Sin(Units.ToRadians(valInclination))) * (Math.Sin(Units.ToRadians(valMagAzimuth)))))))) / (((Math.Cos(Units.ToRadians(cnvIFRDipVal))) * ((((Math.Sin(Units.ToRadians(valInclination))) * ((Math.Cos(Units.ToRadians(valMagAzimuth))) * (Math.Sin(Units.ToRadians(cnvIFRDipVal)))))) - (((Math.Cos(Units.ToRadians(valInclination))) * (Math.Cos(Units.ToRadians(cnvIFRDipVal)))))))));                
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colCM(Decimal colY, Decimal colCL) //Corr Azm
        {
            try
            {
                if (Decimal.Add(colCL, colY) > 359.99M)
                {
                    return Decimal.Subtract(Decimal.Add(colCL, colY), 360.00M);
                }
                else
                {
                    return Decimal.Add(colCL, colY);
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colCN(Decimal colT, Decimal colCM, Decimal cnvIFRDip, Decimal cnvIFRBT) //Bzo'
        {
            try
            {
                return Decimal.Add(Decimal.Multiply(cnvIFRBT, Decimal.Multiply(((decimal)Math.Cos(Units.ToRadians((double)cnvIFRDip))), (Decimal.Multiply(((decimal)Math.Sin(Units.ToRadians((double)colT))), ((decimal)Math.Cos(Units.ToRadians((double)colCM))))))), (Decimal.Multiply(cnvIFRBT, Decimal.Multiply(((decimal)Math.Sin(Units.ToRadians((double)cnvIFRDip))), ((decimal)Math.Cos(Units.ToRadians((double)colT)))))));                
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colCO(Decimal Bz, Decimal colCN, Decimal colCS, Decimal colGV) //∆Bz-Corrected - Conditioned = Bzsc-BZLC
        {
            try
            {
                if (Math.Abs(colCN - Bz) > Math.Abs(colGV))
                {
                    return Decimal.Multiply(Math.Sign(colCS), colGV);
                }
                else
                {
                    return Decimal.Subtract(colCN, Bz);
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colCP(Int32 srvyRowID, Double RandomNumber, DataTable InputTable, Decimal currcolCO, Decimal currcolCS, Decimal ifrDip, Decimal ifrBTotal, Decimal convMotorSize, Decimal convGMeter, Decimal convDistanceTop, Decimal convDistanceBtm, Decimal convSpaceAbv, Decimal convSpaceBlw, Decimal convLength, Decimal wDrillCollarPoleStrenth, Decimal TSBias) //Ave -∆Bz-Corrected - Conditioned = Bzsc-BZLC
        {
            try
            {
                Decimal result = -99.00M;
                Int32 sID = 1;
                Decimal igx, igy, igz, ibx, iby, ibz, iGTF, iInclination, iMagAzimuth;
                Decimal iCKVal = -99.0000M, iCLVal = -99.0000M, iCMVal = -99.0000M, iCNVal = -99.0000M, iCOVal = -99.0000M, iCSVal = -99.0000M;
                Decimal iFWVal = -99.0000M, iGHVal = -99.0000M, iGIVal = -99.0000M, iGRVal = -99.0000M, iGSVal = -99.0000M, iGTVal = -99.0000M, iGUVal = -99.0000M, iGVVal = -99.0000M, iHTVal = -99.0000M;
                Decimal iBxRef, iByRef, iBzRef;
                List<Decimal> lstCO = new List<Decimal>();
                List<Decimal> lstCS = new List<Decimal>();
                if (Math.Abs(currcolCO) < Math.Abs(currcolCS))
                {
                    if (srvyRowID.Equals(1))
                    {
                        lstCO.Add(currcolCO);
                        lstCS.Add(currcolCS);
                        result = Decimal.Divide(Decimal.Add(lstCO.Average(), lstCS.Average()), 2);
                        return result;
                    }
                    else
                    {
                        foreach (DataRow row in InputTable.Rows)
                        {
                            while (sID < srvyRowID)
                            {
                                igx = Convert.ToDecimal(row["rGx"]);
                                igy = Convert.ToDecimal(row["rGy"]);
                                igz = Convert.ToDecimal(row["rGz"]);
                                ibx = Convert.ToDecimal(row["rBx"]);
                                iby = Convert.ToDecimal(row["rBy"]);
                                ibz = Convert.ToDecimal(row["rBz"]);
                                iGTF = GTF(igx, igy);
                                iInclination = Inclination(igx, igy, igz);
                                iMagAzimuth = MagAzimuth(iGTF, iInclination, igx, igy, igz, ibx, iby, ibz);
                                iBzRef = BzRef(ifrDip, ifrBTotal, iInclination, iMagAzimuth);
                                iCKVal = colCK(ibz, iInclination, iMagAzimuth, iBzRef, ifrDip, ifrBTotal);
                                iCLVal = colCL(iInclination, iMagAzimuth, iCKVal, ifrDip);
                                iCMVal = colCM(iMagAzimuth, iCLVal);
                                iCNVal = colCN(iInclination, iCMVal, ifrDip, ifrBTotal);
                                iCSVal = colCS(ibz, iCNVal);
                                iFWVal = colFW(convMotorSize);
                                iGHVal = colGH(iFWVal, convMotorSize, convGMeter, convDistanceBtm);
                                iGIVal = colGI(iGHVal, convSpaceBlw, convLength);
                                iBxRef = BxRef(ifrBTotal, ifrDip, iGTF, iInclination, iMagAzimuth);
                                iByRef = ByRef(ifrDip, ifrBTotal, iGTF, iInclination, iMagAzimuth);
                                iHTVal = colHT(ibx, iby, ibz, iBxRef, iByRef, iCNVal, iCSVal, TSBias);
                                iFWVal = colFW(convMotorSize);
                                iGHVal = colGH(iFWVal, convMotorSize, convGMeter, convDistanceBtm);
                                iGRVal = colGR(iFWVal, iGHVal);
                                iGSVal = colGS(iGRVal, convSpaceBlw, convLength);
                                iGTVal = colGT(wDrillCollarPoleStrenth);
                                iGUVal = colGU(convSpaceAbv, iGTVal);
                                iGVVal = colGV(iGSVal, iGUVal);
                                iCOVal = colCO(ibz, iCNVal, iCSVal, iGVVal);
                                lstCO.Add(iCOVal);
                                lstCS.Add(iCSVal);
                                sID++;
                            }
                        }
                        result = Decimal.Divide(Decimal.Add(lstCO.Average(), lstCS.Average()), 2);
                        return result;
                    }
                }
                else
                {
                    return currcolCO;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal colCQ(Int32 srvyRowID, Decimal currHSVal, Decimal TSBias, DataTable RawData, Decimal ifrDip, Decimal ifrBTotal) //∆BzSC - Trend
        {
            try
            {
                //Using Math.net to calculate colCP (Least Squares Trend line)
                Decimal result = -99.0000M;
                Decimal igx, igy, igz, ibx, iby, ibz;
                List<Double> hsList = new List<Double>();
                List<Double> svyVals = new List<Double>();
                Int32 svyVal = 0, sID = 1;
                Double[] trend = new Double[] { };
                Decimal iGTF = -99.0000M, iInclination = -99.0000M, iMagAzimuth = -99.0000M;
                Decimal valBxRef = -99.0000M, valByRef = -99.0000M, valBzRef = -99.0000M, iHSVal = -99.0000M, iCKVal = -99.0000M, iCLVal = -99.0000M, iCMVal = -99.0000M, iCNVal = -99.0000M, iCSVal = -99.0000M;

                if (srvyRowID.Equals(1))
                {
                    return currHSVal;
                }
                else
                {
                    foreach (DataRow row in RawData.Rows)
                    {
                        while (sID < srvyRowID)
                        {
                            igx = Convert.ToDecimal(row["rGx"]);
                            igy = Convert.ToDecimal(row["rGy"]);
                            igz = Convert.ToDecimal(row["rGz"]);
                            ibx = Convert.ToDecimal(row["rBx"]);
                            iby = Convert.ToDecimal(row["rBy"]);
                            ibz = Convert.ToDecimal(row["rBz"]);
                            svyVal = Convert.ToInt32(row["rSID"]);
                            iGTF = GTF(igx, igy);
                            iInclination = Inclination(igx, igy, igz);
                            iMagAzimuth = MagAzimuth(iGTF, iInclination, igx, igy, igz, ibx, iby, ibz);
                            valBxRef = BxRef(ifrBTotal, ifrDip, iGTF, iInclination, iMagAzimuth);
                            valByRef = ByRef(ifrDip, ifrBTotal, iGTF, iInclination, iMagAzimuth);
                            valBzRef = BzRef(ifrDip, ifrBTotal, iInclination, iMagAzimuth);
                            iCKVal = colCK(ibz, iInclination, iMagAzimuth, valBzRef, ifrDip, ifrBTotal);
                            iCLVal = colCL(iInclination, iMagAzimuth, iCKVal, ifrDip);
                            iCMVal = colCM(iMagAzimuth, iCLVal);
                            iCNVal = colCN(iInclination, iCMVal, ifrDip, ifrBTotal);
                            iCSVal = colCS(ibz, iCNVal);
                            iHSVal = colHS(ibx, iby, ibz, valBxRef, valByRef, iCNVal, iCSVal, TSBias);
                            svyVals.Add(svyVal);
                            hsList.Add(Convert.ToDouble(iHSVal));
                            sID++;
                        }
                    }
                    svyVals.Add((Double)srvyRowID);
                    hsList.Add((Double)currHSVal);

                    Double[] s = new Double[] { };
                    Double[] hsv = new Double[] { };

                    s = svyVals.ToArray();
                    hsv = hsList.ToArray();

                    {
                        //var parameters = MNRG.SimpleRegression.Fit(x, cp);
                        Tuple<Double, Double> p = MathNet.Numerics.Fit.Line(s, hsv);

                        Double intercept = p.Item1;
                        Double slope = p.Item2;
                        //result = Convert.ToDecimal(intercept + (slope * srvyRowID));
                        result = Convert.ToDecimal(intercept + slope);
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colCR(Decimal Bz, Decimal colCQ) //Bzo'-Trend Corrected - Conditioned
        {
            try
            {
                return Decimal.Add(colCQ, Bz);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colCS(Decimal Bz, Decimal colCN) //∆Bz = Bzsc-BZLC
        {
            try
            {
                return Decimal.Subtract(colCN, Bz);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colCT(Int32 srvyRowID, Int32 runID, Decimal currHTVal, Decimal ifrDip, Decimal ifrBTotal, Decimal TSBias, DataTable RawData) //∆Bz - Trend
        {
            try
            {
                //Using Math.net to calculate colCP (Least Squares Trend line)
                Decimal result = -99.0000M, igx, igy, igz, ibx, iby, ibz;
                Decimal valBxRef = -99.0000M, valByRef = -99.0000M, valBzRef = -99.0000M, iGTF = -99.0000M, iInclination = -99.0000M, iMagAzimuth = -99.0000M, iCKVal = -99.0000M, iCLVal = -99.0000M, iCMVal = -99.0000M, iCNVal = -99.0000M, iCSVal = -99.0000M, iHTVal = -99.0000M;
                List<Double> htList = new List<Double>();
                List<Double> svyVals = new List<Double>();
                Int32 svyVal = 0, sID = 1;
                Double[] s = new Double[] { };
                Double[] htv = new Double[] { };
                Double[] trend = new Double[] { };
                if (srvyRowID.Equals(1))
                {
                    return currHTVal;
                }
                else
                {
                    foreach(DataRow row in RawData.Rows)
                    {
                        while(sID < srvyRowID)
                        {
                            igx = Convert.ToDecimal(row["rGx"]);
                            igy = Convert.ToDecimal(row["rGy"]);
                            igz = Convert.ToDecimal(row["rGz"]);
                            ibx = Convert.ToDecimal(row["rBx"]);
                            iby = Convert.ToDecimal(row["rBy"]);
                            ibz = Convert.ToDecimal(row["rBz"]);
                            svyVal = Convert.ToInt32(row["rSID"]);
                            iGTF = GTF(igx, igy);
                            iInclination = Inclination(igx, igy, igz);
                            iMagAzimuth = MagAzimuth(iGTF, iInclination, igx, igy, igz, ibx, iby, ibz);
                            valBxRef = BxRef(ifrBTotal, ifrDip, iGTF, iInclination, iMagAzimuth);
                            valByRef = ByRef(ifrDip, ifrBTotal, iGTF, iInclination, iMagAzimuth);
                            valBzRef = BzRef(ifrDip, ifrBTotal, iInclination, iMagAzimuth);
                            iCKVal = colCK(ibz, iInclination, iMagAzimuth, valBzRef, ifrDip, ifrBTotal);
                            iCLVal = colCL(iInclination, iMagAzimuth, iCKVal, ifrDip);
                            iCMVal = colCM(iMagAzimuth, iCLVal);
                            iCNVal = colCN(iInclination, iCMVal, ifrDip, ifrBTotal);
                            iCSVal = colCS(ibz, iCNVal);
                            iHTVal = colHT(ibx, iby, ibz, valBxRef, valByRef, iCNVal, iCSVal, TSBias);
                            htList.Add(Convert.ToDouble(iHTVal));
                            svyVals.Add(svyVal);
                            sID++;
                        }
                    }
                    svyVals.Add((Double)srvyRowID);
                    htList.Add((Double)currHTVal);

                    s = svyVals.ToArray();
                    htv = htList.ToArray();

                    {
                        //var parameters = MNRG.SimpleRegression.Fit(x, cp);
                        Tuple<Double, Double> p = MathNet.Numerics.Fit.Line(s, htv);

                        Double intercept = p.Item1;
                        Double slope = p.Item2;
                        //result = Convert.ToDecimal(intercept + (slope * srvyRowID));
                        result = Convert.ToDecimal(intercept + slope);
                    }
                    return result;
                }                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal colCU(Decimal Bz, Decimal colCT) //Bzo'-Trend Corrected
        {
            try
            {
                return Decimal.Add(Bz, colCT);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal colCV(Decimal Bx, Decimal By, Decimal colP, Decimal colT, Decimal colCU) //Corr Mag_SCAZ_Final
        {
            try
            {
                Decimal constA = 0.0000M;
                Decimal constB = 0.0000M;
                Decimal valA = 0.0000M;
                Decimal valB = 0.0000M;
                Decimal valC = 0.0000M;
                Decimal valD = 0.0000M;
                Decimal valE = 0.0000M;
                Decimal valF = 0.0000M;

                constA = Convert.ToDecimal(Math.PI / 180);
                constB = Convert.ToDecimal(180 / Math.PI);
                valB = Decimal.Multiply(colP, constA);
                valC = Decimal.Multiply(colT, constA);
                valD = Decimal.Multiply(Decimal.Subtract(Decimal.Multiply(Bx, (decimal)Math.Cos((double)valB)), Decimal.Multiply(By, (decimal)Math.Sin((double)valB))), (decimal)Math.Cos((double)valC));
                valE = Decimal.Multiply(colCU, (decimal)Math.Sin((double)valC));
                valF = Decimal.Add((Decimal.Multiply(Bx, (decimal)Math.Sin((double)valB))), (Decimal.Multiply(By, (decimal)Math.Cos((double)valB))));
                valA = Decimal.Add((Decimal.Multiply(((decimal)Math.Atan2((double)(Decimal.Multiply(-1, valF)), (double)(Decimal.Add(valD, valE)))), constB)), 360);

                if ((valA % 360) > 360)
                {
                    return (valA % 360) - 360;
                }
                else
                {
                    return (valA % 360);
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colCW(Decimal colY, Decimal colCV) //ΔSCAZmag_Final
        {
            try
            {
                return Decimal.Subtract(colCV, colY);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colCX(Decimal MDec, Decimal gConv, Decimal colCV) //SCAZ - Ψsc - Plain
        {
            try
            {
                Decimal valA = (colCV + MDec - gConv);
                if (valA > 359.99M)
                {
                    return valA - 360M;
                }
                else if (valA < 0)
                {
                    return (valA + 360M);
                }
                else
                {
                    return valA;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colCY(Decimal iMDec, Decimal gConv, Decimal colCV) //SCAZ - Ψsc - IFR
        {
            try
            {
                Decimal valA = (colCV + iMDec - gConv);
                if (valA > 359.99M)
                {
                    return Decimal.Subtract(valA, 360M);
                }
                else if (valA < 0)
                {
                    return Decimal.Add(valA, 360M);
                }
                else
                {
                    return valA;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colCZ(Decimal MDec, Decimal colCV) //SCAZ - Ψsc - Plain
        {
            try
            {
                if ((colCV + MDec) > 359.99M)
                {
                    return (colCV + MDec) - 360M;
                }
                else if ((colCV + MDec) < 0)
                {
                    return (colCV + MDec) + 360M;
                }
                else
                {
                    return (colCV + MDec);
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colDA(Decimal iMDec, Decimal colCV) //SCAZ - Ψsc - IFR
        {
            try
            {
                if ((colCV + iMDec) > 359.99M)
                {
                    return ((colCV + iMDec) - 360M);
                }
                else if ((colCV + iMDec) < 0)
                {
                    return ((colCV + iMDec) + 360M);
                }
                else
                {
                    return (colCV + iMDec);
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colDB(Int32 ifrVal, Decimal colCX, Decimal colCY) //Grid - SCAZ - Ψsc - Final
        {
            try
            {
                if (ifrVal.Equals(1)) //1 = No, 2 = Yes
                {
                    return colCX;
                }
                else
                {
                    return colCY;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colDC(Int32 ifrVal, Decimal colCZ, Decimal colDA) //True - SCAZ - Ψsc - Final
        {
            try
            {
                if (ifrVal.Equals(1))
                {
                    return colCZ;
                }
                else
                {
                    return colDA;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colDD(Decimal jConverg, Decimal colDB, Decimal colDC) //SCAZ - Ψsc - Final
        {
            try
            {
                if (jConverg.Equals(2))
                {
                    return colDB;
                }
                else
                {
                    return colDC;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colDE(Decimal colAF, Decimal colDD) //ΔSCAZFinal
        {
            try
            {
                return Decimal.Subtract(colDD, colAF);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Int32 colDF(Decimal colDE) //Short Collar  Azimuth Error - ∆SCAz° Condition for Trend Analysis
        {
            try
            {
                if ((colDE <= 0.50M && colDE >= -0.50M))
                {
                    return 1;
                }
                else if ((colDE > 0.50M || colDE < -0.50M))
                {
                    return 2;
                }
                else
                {
                    return 3;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Int32 colDG(Decimal colCS, Decimal colGB, Decimal colGV) //∆∆Bz = ∆Bp - ∆Bzsz-zLC - Condition for Trend Analysis
        {
            try
            {
                if ((Math.Abs(colCS) - colGV) > 0)
                {
                    return 3;
                }
                else if ((Math.Abs(colCS) - colGB) > 0)
                {
                    return 2;
                }
                else
                {
                    return 1;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Int32 colDH(Decimal Bx, Decimal By, Decimal colCU) //BtSCAZ corrected
        {
            try
            {
                Double cBx = Convert.ToDouble(Bx);
                Double cBy = Convert.ToDouble(By);
                Double cCU = Convert.ToDouble(colCU);
                Double fVal = Math.Pow(cBx, 2);
                Double sVal = Math.Pow(cBy, 2);
                Double tVal = Math.Pow(cCU, 2);
                Double lVal = Math.Abs(fVal + sVal + tVal);
                Double sqrt = Math.Sqrt(lVal);
                Int32 result = Convert.ToInt32(sqrt);
                //return Convert.ToInt32( Math.Sqrt( Math.Abs( Math.Pow( (cBx) , 2 ) + Math.Pow( (cBy) , 2 ) + Math.Pow( (cCU) , 2 ) ) ) );
                return result;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal colDI(Decimal Gx, Decimal Gy, Decimal Gz, Decimal Bx, Decimal By, Decimal colCF, Decimal colCU, Int32 colDH) //DipSCAZ corrected
        {
            try
            {
                return Convert.ToDecimal(Units.ToDegrees(Math.Asin((Double)((Decimal.Multiply(Bx, Gx) + Decimal.Multiply(By, Gy) + Decimal.Multiply(colCU, Gz)) / (colCF * colDH)))));
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 colDJ(Int32 ifrVal, Decimal convBT, Decimal conviBT, Decimal colDH) //ΔBtSCAZ corrected
        {
            try
            {
                if (ifrVal.Equals(1))
                {
                    return Convert.ToInt32(-1 * (convBT - colDH));
                }
                else
                {
                    return Convert.ToInt32(-(conviBT - colDH));
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal colDK(Int32 ifrVal, Decimal cnvDip, Decimal cnviDip, Decimal colDI) //ΔDipSCAZ corrected
        {
            try
            {
                if (ifrVal.Equals(1))
                {
                    return -1 * (cnvDip - colDI);
                }
                else
                {
                    return -1 * (cnviDip - colDI);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal colDR(Decimal colAF) //Azimuth° (Grid)
        {
            try
            {
                Double colAFVal = Convert.ToDouble(colAF);
                if (colAFVal >= 360)
                {
                    return Convert.ToDecimal(colAF - 360);
                }
                else
                {
                    return Convert.ToDecimal(colAFVal);
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Int32 colDW(Decimal sqcDip, Decimal colBC) //Dip Qualifier
        {
            try
            {
                Double sDip = Convert.ToDouble(sqcDip);
                Double colBCVal = Convert.ToDouble(colBC);
                if ((colBCVal <= sDip) && (colBCVal >= (-1 * sDip)))
                {
                    return 1; /* Dip Angle Good */
                }
                else
                {
                    return 2; /* Dip Angle Bad */
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Int32 colDY(Decimal sqccnvBT, Decimal colCD) //Btotal Qualifier
        {
            try
            {
                Double sqcBT = Convert.ToDouble(sqccnvBT);
                Double colCDVal = Convert.ToDouble(colCD);
                if ((colCDVal.Equals(sqcBT) || colCDVal < sqcBT) && (colCDVal.Equals(-1 * sqcBT) || colCDVal > (-1 * sqcBT)))
                {
                    return 1; /* BT Good */
                }
                else
                {
                    return 2; /* BT Bad */
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Int32 colEA(Decimal sqccnvGT, Decimal colCG) //Gtotal Qualifier
        {
            try
            {
                Double sqcGT = Convert.ToDouble(sqccnvGT);
                Double colCGVal = Convert.ToDouble(colCG);
                if ((colCGVal <= sqcGT) && (colCGVal >= (-1 * sqcGT)))
                {
                    return 1; /* GT Good */
                }
                else
                {
                    return 2; /* GT Bad */
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Int32 colEG(Decimal colAY) //Short Collar Application
        {
            try
            {
                if (colAY.Equals(0))
                {
                    return 1; //Short Collar  - Not Applicable
                }
                else
                {
                    return 2; //SHort Collar - Applicable
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Int32 colEK(Decimal colEG) //BHA Non Mag Spacing (Std. Pole Strength)
        {
            try
            {
                if (Math.Abs(colEG) > 0.5M)
                {
                    return 3;
                }
                else
                {
                    return 4;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colEI(Decimal colGC, Decimal colGE) //ΔAzimuth due to Motor only (Std. Pole Strength)°
        {
            try
            {
                return Decimal.Multiply(Math.Sign(colGE), colGC);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal colEJ(Decimal colGD, Decimal colGE) //ΔAzimuth due to Drill Collar only (Std. Pole Strength)°
        {
            try
            {
                return Decimal.Multiply(Math.Sign(colGE), colGD);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 colEO(Decimal colEL) //BHA Non Mag Spacing (Gaussmeter based Pole Strength)
        {
            try
            {
                if (Math.Abs((Double)colEL) > 0.5)
                {
                    return 3;
                }
                else
                {
                    return 4;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal colEQ(Decimal colGW, Decimal colGY) //ΔAzimuth due to Motor only (Worst Case - Std. Pole Strength)°
        {
            try
            {
                return Decimal.Multiply(Math.Sign(colGY), colGW);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal colER(Decimal colGX, Decimal colGY) //ΔAzimuth due to Drill Collar only (Worst Case - Std. Pole Strength)°
        {
            try
            {
                return Decimal.Multiply(Math.Sign(colGY), colGX);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 colET(Decimal colEP) //BHA Non Mag Spacing (Worst Case)
        {
            try
            {
                if (Math.Abs((Double)colEP) > 0.5)
                {
                    return 3;
                }
                else
                {
                    return 4;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal colEU(Int32 srvyRowID, Int32 RunID, Decimal colBACurr, String dbConn) //Minimum Dip
        {
            try
            {
                Decimal result = -99.000000000000M;

                if (srvyRowID.Equals(1))
                {
                    return colBACurr;
                }
                else
                {
                    String selectCommand = "SELECT MIN(colBA) FROM RawQCResults WHERE runID = @runID";

                    using (SqlConnection clntConn = new SqlConnection(dbConn))
                    {
                        try
                        {
                            clntConn.Open();

                            SqlCommand getMin = new SqlCommand(selectCommand, clntConn);
                            getMin.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();

                            using (SqlDataReader readMin = getMin.ExecuteReader())
                            {
                                try
                                {
                                    if (readMin.HasRows)
                                    {
                                        while (readMin.Read())
                                        {
                                            result = readMin.GetDecimal(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readMin.Close();
                                    readMin.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            clntConn.Close();
                            clntConn.Dispose();
                        }
                    }
                    return Math.Min(colBACurr, result);
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colEV(Int32 srvyRowID, Int32 RunID, Decimal colBACurr, String dbConn) //Maximum Dip
        {
            try
            {
                Decimal result = -99.000000000000M;

                if (srvyRowID.Equals(1))
                {
                    return colBACurr;
                }
                else
                {
                    String selectCommand = "SELECT MAX(colBA) FROM RawQCResults WHERE runID = @runID";

                    using (SqlConnection clntConn = new SqlConnection(dbConn))
                    {
                        try
                        {
                            clntConn.Open();

                            SqlCommand getMin = new SqlCommand(selectCommand, clntConn);
                            getMin.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();

                            using (SqlDataReader readMax = getMin.ExecuteReader())
                            {
                                try
                                {
                                    if (readMax.HasRows)
                                    {
                                        while (readMax.Read())
                                        {
                                            result = readMax.GetDecimal(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readMax.Close();
                                    readMax.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            clntConn.Close();
                            clntConn.Dispose();
                        }
                    }
                    return Math.Max(colBACurr, result);
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colEW(Decimal colEU, Decimal colEV) //Dip Spread
        {
            try
            {
                return Decimal.Subtract(colEV, colEU);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Int32 colEX(Int32 srvyRowID, Int32 RunID, Decimal colCACurr, String dbConn) //Minimum B-Total
        {
            try
            {
                Decimal result = -99.000000000000M;

                if (srvyRowID.Equals(1))
                {
                    return Convert.ToInt32(colCACurr);
                }
                else
                {
                    String selectCommand = "SELECT MIN(colCA) FROM RawQCResults WHERE runID = @runID";

                    using (SqlConnection clntConn = new SqlConnection(dbConn))
                    {
                        try
                        {
                            clntConn.Open();

                            SqlCommand getMin = new SqlCommand(selectCommand, clntConn);
                            getMin.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();

                            using (SqlDataReader readMin = getMin.ExecuteReader())
                            {
                                try
                                {
                                    if (readMin.HasRows)
                                    {
                                        while (readMin.Read())
                                        {
                                            result = readMin.GetDecimal(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readMin.Close();
                                    readMin.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            clntConn.Close();
                            clntConn.Dispose();
                        }
                    }
                    return Convert.ToInt32(Math.Min(colCACurr, result));
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Int32 colEY(Int32 srvyRowID, Int32 RunID, Decimal colCACurr, String dbConn) //Maximum B-Total
        {
            try
            {
                Decimal result = -99.000000000000M;

                if (srvyRowID.Equals(1))
                {
                    return Convert.ToInt32(colCACurr);
                }
                else
                {
                    String selectCommand = "SELECT MAX(colCA) FROM RawQCResults WHERE runID = @runID";

                    using (SqlConnection clntConn = new SqlConnection(dbConn))
                    {
                        try
                        {
                            clntConn.Open();

                            SqlCommand getMin = new SqlCommand(selectCommand, clntConn);
                            getMin.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();

                            using (SqlDataReader readMax = getMin.ExecuteReader())
                            {
                                try
                                {
                                    if (readMax.HasRows)
                                    {
                                        while (readMax.Read())
                                        {
                                            result = readMax.GetDecimal(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readMax.Close();
                                    readMax.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            clntConn.Close();
                            clntConn.Dispose();
                        }
                    }
                    return Convert.ToInt32(Math.Max(colCACurr, result));
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Int32 colEZ(Int32 colEX, Int32 colEY) //B-Total Spread
        {
            try
            {
                return (Int32)(colEY - colEX);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colFA(Int32 srvyRowID, Int32 RunID, Decimal colCFCurr, String dbConn) //Minimum G-Total
        {
            try
            {
                Decimal result = -99.000000000000M;

                if (srvyRowID.Equals(1))
                {
                    return colCFCurr;
                }
                else
                {
                    String selectCommand = "SELECT MIN(colCF) FROM RawQCResults WHERE runID = @runID";

                    using (SqlConnection clntConn = new SqlConnection(dbConn))
                    {
                        try
                        {
                            clntConn.Open();

                            SqlCommand getMin = new SqlCommand(selectCommand, clntConn);
                            getMin.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();

                            using (SqlDataReader readMin = getMin.ExecuteReader())
                            {
                                try
                                {
                                    if (readMin.HasRows)
                                    {
                                        while (readMin.Read())
                                        {
                                            result = readMin.GetDecimal(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readMin.Close();
                                    readMin.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            clntConn.Close();
                            clntConn.Dispose();
                        }
                    }
                    return Math.Min(colCFCurr, result);
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colFB(Int32 srvyRowID, Int32 RunID, Decimal colCFCurr, String dbConn) //Maximum G-Total
        {
            try
            {
                Decimal result = -99.000000000000M;

                if (srvyRowID.Equals(1))
                {
                    return colCFCurr;
                }
                else
                {
                    String selectCommand = "SELECT MAX(colCF) FROM RawQCResults WHERE runID = @runID";

                    using (SqlConnection clntConn = new SqlConnection(dbConn))
                    {
                        try
                        {
                            clntConn.Open();

                            SqlCommand getMin = new SqlCommand(selectCommand, clntConn);
                            getMin.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();

                            using (SqlDataReader readMax = getMin.ExecuteReader())
                            {
                                try
                                {
                                    if (readMax.HasRows)
                                    {
                                        while (readMax.Read())
                                        {
                                            result = readMax.GetDecimal(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readMax.Close();
                                    readMax.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            clntConn.Close();
                            clntConn.Dispose();
                        }
                    }
                    return Math.Max(colCFCurr, result);
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colFC(Decimal colFA, Decimal colFB) // G-Total spread
        {
            try
            {
                return Decimal.Subtract(colFB, colFA);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Int32 colFW(Decimal cnvMSize) //Motor Pole strength (µWb)
        {
            try
            {
                Double MSize = Convert.ToDouble(cnvMSize);
                if (MSize.Equals(0))
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32((((((((-1.2952 * (Math.Pow(Math.Abs(MSize), 3)))) + ((26.353 * (Math.Pow(Math.Abs(MSize), 2)))))) - ((122.55 * Math.Abs(MSize))))) + 486.47));
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Int32 colFX(Decimal cnvDSSize) //Drill Collar Pole Strength  (µWb)
        {
            try
            {
                Double DSSize = Convert.ToDouble(cnvDSSize);
                if (DSSize.Equals(0))
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32((-1.0486 * Math.Pow(Math.Abs(DSSize), 3)) + (23.539 * Math.Pow(Math.Abs(DSSize), 2)) - (85.581 * Math.Abs(DSSize)) + 488.27);
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colFY(Decimal colBA, Decimal colCA) //Bhor=Bon= Horizontal Component of Bt (nT)
        {
            try
            {
                Double colBAVal = Convert.ToDouble(colBA);
                Double colCAVal = Convert.ToDouble(colCA);
                
                return Convert.ToDecimal(colCAVal * Math.Cos(Units.ToRadians(colBAVal)));
                
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colFZ(Decimal BtCorrected, Decimal DipCorrected, Decimal Inclination, Decimal FinalAzimuth) //Bp-Drill Collar - DC Interference (nT)
        {
            try
            {
                return ((BtCorrected * (Decimal)Math.Cos((Double)DipCorrected)) * (Decimal)Math.Tan(0.25)) / (((Decimal)Math.Sin((Double)Inclination) * (Decimal)Math.Sin((Double)FinalAzimuth)) - ( (Decimal)Math.Tan(0.25) * (Decimal)Math.Sin((Double)Inclination) * (Decimal)Math.Cos((Double)FinalAzimuth)));
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colGA(Decimal NMSpBlw, Decimal cnvMDBLen, Decimal colFW) // Bp-Motor - Motor Interference (nT)
        {
            try
            {

                return ((colFW / (4 * (Decimal)Math.PI)) * ((1 / (Decimal)Math.Pow(((Double)NMSpBlw + 0.01), 2)) - (1 / (Decimal)Math.Pow(((Double)NMSpBlw + (Double)cnvMDBLen + 0.01), 2)))) * 1000;

            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colGB(Decimal colFZ, Decimal colGA) //Bp - BHA Interference (nT)
        {
            try
            {
                return Decimal.Add(colFZ, colGA);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colGC(Decimal colT, Decimal colY, Decimal colFY, Decimal colGA) //Downhole - Δ LCAZ Error due to Motor°
        {
            try
            {
                return (Decimal)Math.Abs(Units.ToDegrees(Math.Atan((Double)(colGA * (Decimal)Math.Sin(Units.ToRadians((Double)colT)) * (Decimal)Math.Sin(Units.ToRadians((Double)colY))) / (Double)(colFY + (colGA * ((Decimal)Math.Sin(Units.ToRadians((Double)colT)) * (Decimal)Math.Cos(Units.ToRadians((Double)colY))))))));
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colGD(Decimal colT, Decimal colY, Decimal colFY, Decimal colFZ) //Downhole - Δ LCAZ Error due to Drill Collars° (with Std. Pole Strength)
        {
            try
            {
                return (Decimal)Math.Abs(Units.ToDegrees(Math.Atan((Double)(colFZ * (Decimal)Math.Sin(Units.ToRadians((Double)colT)) * (Decimal)Math.Sin(Units.ToRadians((Double)colY))) / (Double)(colFY + (colFZ * ((Decimal)Math.Sin(Units.ToRadians((Double)colT)) * (Decimal)Math.Cos(Units.ToRadians((Double)colY))))))));
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colGE(Decimal colT, Decimal colY, Decimal colFY, Decimal colGB, Decimal colDE) //Downhole - Δ LCAZ Error due to BHA°
        {
            try
            {
                Double colTVal = Convert.ToDouble(colT);
                Double colYVal = Convert.ToDouble(colY);
                Double colFYVal = Convert.ToDouble(colFY);
                Double colGBVal = Convert.ToDouble(colGB);
                Double colDEVal = Convert.ToDouble(colDE);

                return Convert.ToDecimal(Math.Sign(colDEVal) * Math.Abs(((Math.Atan((((colGBVal * (((Math.Sin(Units.ToRadians(colTVal))) * (Math.Sin(Units.ToRadians(colYVal))))))) / ((colFYVal + ((colGBVal * (((Math.Sin(Units.ToRadians(colTVal))) * (Math.Cos(Units.ToRadians(colYVal)))))))))))) * ((180 / Math.PI)))));
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Int32 colGF(Decimal colGE) //Downhole - Δ LCAZ Error due to BHA Condition for Trend Analysis
        {
            try
            {
                if (Math.Abs(colGE) <= 0.5M)
                {
                    return 1;
                }
                else
                {
                    return 2;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colGH(Decimal colFW, Decimal cnvbhaMSize, Decimal bhaMGVal, Decimal bhaDGBottom) //Downhole - MPS Motor (μWb)
        {
            try
            {
                if (bhaMGVal.Equals(0))
                {
                    return (colFW * (4 * (Decimal)Math.PI) * (Decimal)Math.Pow((Double)(bhaDGBottom + ((cnvbhaMSize / 12) * 0.3048M)), 2)) * 100;
                }
                else
                {
                    return (bhaMGVal * (4 * (Decimal)Math.PI) * (Decimal)Math.Pow((Double)(bhaDGBottom + ((cnvbhaMSize / 12) * 0.3048M)), 2)) * 100;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colGI(Decimal colGH, Decimal bhaNMSpBlw, Decimal cnvMDBLen) //Bp-Motor (nTesla)
        {
            try
            {
                return ((Decimal)(1 / (4 * Math.PI)) * ((colGH / (Decimal)Math.Pow((Double)(bhaNMSpBlw + 0.01M), 2)) - (colGH / (Decimal)Math.Pow((Double)(cnvMDBLen + bhaNMSpBlw - 0.01M), 2)))) * 1000;
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colGJ(Decimal colCT, Decimal colGI) //Bp-Drill Collar/Jar (nTesla)
        {
            try
            {
                Double colCTVal = Convert.ToDouble(colCT);
                Double colGIVal = Convert.ToDouble(colGI);

                return Convert.ToDecimal(Math.Abs((Math.Abs(colCTVal) - Math.Abs(colGIVal))));
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colGK(Decimal colGJ, Decimal bhaNMSpAbv) //Downhole - MPS Drill Collar  (μWb)
        {
            try
            {
                Double colGJVal = Convert.ToDouble(colGJ);
                Double bhaNMSpAbvVal = Convert.ToDouble(bhaNMSpAbv);
                Double constA = (4 * Math.PI);
                Double val1 = Math.Pow((bhaNMSpAbvVal + 0.01), 2);

                return Convert.ToDecimal(constA * ((colGJVal * val1) * 0.001));
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Int32 colGL(Decimal colGK, Decimal dcpsLimit) //Warning - Downhole - MPS Drill Collar  (μWb)
        {
            try
            {
                /* magLimit comes from tblBHADownholeMagnetizationLimit */
                if (colGK > dcpsLimit)
                {
                    return 1; /* Outside Limit - tblBHAMagnetizationWarning ID */
                }
                else
                {
                    return 2; /* Within Limit */
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colGM(Decimal colGK, Decimal bhaDSCSize) //Actual - Gaussmeter (DRILL Collar/Jar) (nTesla)
        {
            try
            {
                Double colGKVal = Convert.ToDouble(colGK);
                Double DSCSizeVal = Convert.ToDouble(bhaDSCSize);

                return Convert.ToDecimal((colGKVal / ((4 * Math.PI) * (Math.Pow((((DSCSizeVal / 12) * 0.3048) + 0.01), 2)))) * 0.01);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colGN(Decimal colT, Decimal colY, Decimal colFY, Decimal colDE, Decimal colGI) //Actual Azimuth Error due to Motor °
        {
            try
            {
                return Math.Sign(colDE) * (Decimal)Math.Abs((Double)Units.ToDegrees((Double)Math.Atan((Double)(colGI * (Decimal)Math.Sin((Double)colT) * (Decimal)Math.Sin((Double)colY)) / (Double)((colFY + colGI * (Decimal)Math.Sin((Double)colT) * (Decimal)Math.Cos((Double)colY))))));
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colGO(Decimal colT, Decimal colY, Decimal colFY, Decimal colDE, Decimal colGJ) //Actual Azimuth Error  due to Drill Collars°
        {
            try
            {
                return Math.Sign(colDE) * (Decimal)Math.Abs((Double)Units.ToDegrees((Double)Math.Atan((Double)(colGJ * (Decimal)Math.Sin((Double)colT) * (Decimal)Math.Sin((Double)colY)) / (Double)((colFY + colGJ * (Decimal)Math.Sin((Double)colT) * (Decimal)Math.Cos((Double)colY))))));
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colGP(Decimal colT, Decimal colY, Decimal colFY, Decimal colDE, Decimal colCT) //Actual Azimuth Error °
        {
            try
            {
                Double colTVal = Convert.ToDouble(colT);
                Double colYVal = Convert.ToDouble(colY);
                Double colFYVal = Convert.ToDouble(colFY);
                Double colDEVal = Convert.ToDouble(colDE);
                Double colCTVal = Convert.ToDouble(colCT);

                return Convert.ToDecimal(Math.Sign(colDEVal) * Math.Abs(((Math.Atan(((((colCTVal * ((Math.Sin(colTVal)) * (Math.Sin(colYVal))))) / (((colFYVal + (colCTVal * ((Math.Sin(colTVal)) * (Math.Cos(colYVal))))))))))) * ((180 / Math.PI)))));
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colGR(Decimal colFW, Decimal colGH) //Worst Case - Downhole - MPS Motor (μWb)
        {
            try
            {
                Double colFWVal = Convert.ToDouble(colFW);
                Double colGHVal = Convert.ToDouble(colGH);

                if (Math.Abs(colGHVal) < Math.Abs(colFWVal))
                {
                    return Convert.ToDecimal(Math.Abs(colFWVal));
                }
                else
                {
                    return Convert.ToDecimal(Math.Abs(colGHVal));
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colGS(Decimal colGR, Decimal bhaNMSpBlw, Decimal bhaMDBLen) //Worst Case - Bp-Motor (nTesla)
        {
            try
            {
                Double colGRVal = Convert.ToDouble(colGR);
                Double NMSpBlwVal = Convert.ToDouble(bhaNMSpBlw);
                Double MDBLenVal = Convert.ToDouble(bhaMDBLen);

                return Convert.ToDecimal(1 / (4 * Math.PI) * ((colGRVal / Math.Pow((NMSpBlwVal + 0.01), 2)) - (colGRVal / Math.Pow((MDBLenVal + NMSpBlwVal - 0.01), 2))) * 1000);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colGT(Decimal bhadmlValue) //Worst Case - Downhole - MPS Drill Collar  (μWb)
        {
            try
            {
                return bhadmlValue;
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colGU(Decimal bhaNMSpAbv, Decimal colGT) //Worst Case - Gaussmeter (DRILL Collar/Jar) (nTesla)
        {
            try
            {
                Double NMSpAbvVal = Convert.ToDouble(bhaNMSpAbv);
                Double colGTVal = Convert.ToDouble(colGT);

                return Convert.ToDecimal((colGTVal / (4 * Math.PI) * ((1 / Math.Pow((NMSpAbvVal + 0.01), 2)))) * 1000);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colGV(Decimal colGS, Decimal colGU) //WORST CASE Bz Bias - Bp Worst (nT)
        {
            try
            {
                return Decimal.Add(colGS, colGU);
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colGW(Decimal colT, Decimal colY, Decimal colFY, Decimal colGS) //Worst Case Azimuth Error due to Motor°
        {
            try
            {
                Double colTVal = Convert.ToDouble(colT);
                Double colYVal = Convert.ToDouble(colY);
                Double colFYVal = Convert.ToDouble(colFY);
                Double colGSVal = Convert.ToDouble(colGS);

                return Convert.ToDecimal(Math.Abs((180 / Math.PI) * (Math.Atan(colGSVal * Math.Sin(colTVal) * Math.Sin(colYVal) / (colFYVal + colGSVal * Math.Sin(colTVal) * Math.Cos(colYVal))))));
            }
            catch (Exception ex)
            {

                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colGX(Decimal colT, Decimal colY, Decimal colFY, Decimal colGU) //Worst Case Azimuth Error due to Drill Collars°
        {
            try
            {
                Double colTVal = Convert.ToDouble(colT);
                Double colYVal = Convert.ToDouble(colY);
                Double colFYVal = Convert.ToDouble(colFY);
                Double colGUVal = Convert.ToDouble(colGU);

                return Convert.ToDecimal(Math.Abs((180 / Math.PI) * (Math.Atan(colGUVal * Math.Sin(colTVal) * Math.Sin(colYVal) / (colFYVal + colGUVal * Math.Sin(colTVal) * Math.Cos(colYVal))))));
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colGY(Decimal colDE, Decimal colFY, Decimal colGV, Decimal colT, Decimal colY) //Worst Case -Actual Azimuth Error °
        {
            try
            {
                Double colDEVal = Convert.ToDouble(colDE);
                Double colFYVal = Convert.ToDouble(colFY);
                Double colGVVal = Convert.ToDouble(colGV);
                Double colTVal = Convert.ToDouble(colT);
                Double colYVal = Convert.ToDouble(colY);

                return Convert.ToDecimal(Math.Sign(colDEVal) * Math.Abs((180 / Math.PI) * (Math.Atan(colGVVal * Math.Sin(colTVal) * Math.Sin(colYVal) / (colFYVal + colGVVal * Math.Sin(colTVal) * Math.Cos(colYVal))))));
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colGZ(Decimal colGV, Decimal colCT) //Δ (Worst Case Bz Bias - SCBz Bias) (nT)
        {
            try
            {
                Double colGVVal = Convert.ToDouble(colGV);
                Double colCTVal = Convert.ToDouble(colCT);

                return Convert.ToDecimal(Math.Abs(colGVVal) - Math.Abs(colCTVal));
            }
            catch (Exception ex)
            {

                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal colHA(Decimal colGY, Decimal colDE) //Δ (Worst Case Azimuth Error due to BHA - SCAZ)°
        {
            try
            {
                Double colGYVal = Convert.ToDouble(colGY);
                Double colDEVal = Convert.ToDouble(colDE);

                return Convert.ToDecimal(Math.Abs(colGYVal) - Math.Abs(colDEVal));
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Int32 colHB(Decimal colGZ) //Hot BHA - Anomaly Indicator- 1 (Yes = 1, No = 0)
        {
            try
            {
                if (colGZ <= 0)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Int32 colHC(Int32 ifrVal) //Magnetic Model Anomaly Indicator - 1 (IFR = 0, else = 1)
        {
            try
            {
                if (ifrVal.Equals(2))
                {
                    return 0;
                }
                else
                {
                    return 1;
                }
            }
            catch (Exception ex)
            {

                throw new System.Exception(ex.ToString());
            }
        }

        public static Int32 colHD(Decimal colHB, Decimal colHC) //Background Anomaly
        {
            try
            {
                if (colHB.Equals(1) && colHC.Equals(1))
                {
                    return 2;
                }
                else if (colHB.Equals(1) && colHC.Equals(0))
                {
                    return 3;
                }
                else if (colHB.Equals(0) && colHC.Equals(1))
                {
                    return 4;
                }
                else
                {
                    return 1;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Int32 colHE(Decimal colHA) //Maximum Allowable Azimuth Error - Cuttoff Limit ( Above =1 (Not Good), Within limit = 0)
        {
            try
            {
                if (colHA.Equals(0) || (colHA < 0))
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Int32 colHG(Int32 ifrVal, Decimal colBD, Decimal colCS, Decimal colFZ, Decimal colGA, Decimal colGB) //Reference Magnetics
        {
            try
            {
                // value from database 1 = No; 2 = Yes (checking for Yes)
                if (ifrVal.Equals(2))
                {
                    return 1;
                }
                else if (((colCS > colGB) && ifrVal.Equals(1)) || ((colBD.Equals(2)) && (colFZ > colGA) && (ifrVal.Equals(1))) || ((colBD.Equals(3)) && (colGA > colFZ) && (ifrVal.Equals(1))))
                {
                    return 2;
                }
                else
                {
                    return 1;
                }
            }
            catch (Exception ex)
            {

                throw new System.Exception(ex.ToString());
            }
        }

        public static Int32 colHH(Decimal colT) //Inclination
        {
            try
            {
                if (colT <= 5)
                {
                    return 1;
                }
                else
                {
                    return 2;
                }
            }
            catch (Exception ex)
            {

                throw new System.Exception(ex.ToString());
            }
        }

        private static Decimal colHQ(Decimal Bx, Decimal By, Decimal Bz, Decimal colBIVal, Decimal colBJVal, Decimal colCNVal, Decimal TSBias)
        {
            try
            {
                Decimal iResult = -99.0000M;
                Int32 check = -99;
                Int32 i1 = -99;
                Int32 i2 = -99;
                Int32 i3 = -99;
                Int32 i4 = -99;
                Int32 i5 = -99;
                Int32 i6 = -99;

                if ((Bx - colBIVal) > TSBias)
                { i1 = 1; }
                else
                { i1 = 0; }
                if ((By - colBJVal) > TSBias)
                { i2 = 1; }
                else
                { i2 = 0; }
                if ((Bz - colCNVal) > TSBias)
                { i3 = 1; }
                else
                { i3 = 0; }
                if ((Bx - colBIVal) < (-1 * TSBias))
                { i4 = 1; }
                else
                { i4 = 0; }
                if ((By - colBJVal) < (-1 * TSBias))
                { i5 = 1; }
                else
                { i5 = 0; }
                if ((Bz - colCNVal) < (-1 * TSBias))
                { i6 = 1; }
                else
                { i6 = 0; }

                if (i1.Equals(1) || i2.Equals(1) || i3.Equals(1) || i4.Equals(1) || i5.Equals(1) || i6.Equals(1))
                {
                    check = 0;
                }
                else
                {
                    check = 1;
                }

                if (check.Equals(1))
                {
                    iResult = colBIVal - Bx;
                }
                else
                {
                    iResult = 0.0000M;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal colHR(Decimal Bx, Decimal By, Decimal Bz, Decimal colBIVal, Decimal colBJVal, Decimal colCNVal, Decimal TSBias)
        {
            try
            {
                Decimal iResult = -99.0000M;
                Int32 check = -99;
                if (((Bx - colBIVal) > TSBias) || ((By - colBJVal) > TSBias) || ((Bz - colCNVal) > TSBias) || ((Bx - colBIVal) < (-1 * TSBias)) || ((By - colBJVal) < (-1 * TSBias)) || ((Bz - colCNVal) < (-1 * TSBias)))
                {
                    check = 0;
                }
                else
                {
                    check = 1;
                }

                if (check.Equals(1))
                {
                    iResult = (colBJVal - By);
                }
                else
                {
                    iResult = 0.0000M;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal colHS(Decimal Bx, Decimal By, Decimal Bz, Decimal colBIVal, Decimal colBJVal, Decimal colCNVal, Decimal colCSVal, Decimal TSBias)
        {
            try
            {
                Decimal iResult = -99.0000M;
                Int32 check = -99;

                if (((Bx - colBIVal) > TSBias) || ((By - colBJVal) > TSBias) || ((Bz - colCNVal) > TSBias) || ((Bx - colBIVal) < (-1 * TSBias)) || ((By - colBJVal) < (-1 * TSBias)) || ((Bz - colCNVal) < (-1 * TSBias)))
                {
                    check = 0;
                }
                else
                {
                    check = 1;
                }

                if (check.Equals(1))
                {
                    iResult = colCSVal;
                }
                else
                {
                    iResult = 0.0000M;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal colHT(Decimal Bx, Decimal By, Decimal Bz, Decimal colBIVal, Decimal colBJVal, Decimal colCNVal, Decimal colCSVal, Decimal TSBias)
        {
            try
            {
                Decimal iResult = -99.0000M;
                Int32 check = -99;

                if (((Bx - colBIVal) > TSBias) || ((By - colBJVal) > TSBias) || ((Bz - colCNVal) > TSBias) || ((Bx - colBIVal) < (-1 * TSBias)) || ((By - colBJVal) < (-1 * TSBias)) || ((Bz - colCNVal) < (-1 * TSBias)))
                {
                    check = 0;
                }
                else
                {
                    check = 1;
                }

                if (check.Equals(1))
                {
                    iResult = colCSVal;
                }
                else
                {
                    iResult = 0.0000M;
                }

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
        
        //Calculating No-Go-Zone
        private static Int32 getNGZ(Decimal colTVal, Decimal colYVal, Int32 ngzInc, Int32 ngzAzm)
        {
            try
            {
                Int32 iReply = -99;
                if (colTVal >= ngzInc)
                {
                    if (colYVal >= (90 - ngzAzm) && colYVal <= (90 + ngzAzm))
                    {
                        if ((colYVal >= (270 - ngzAzm)) && (colYVal <= (270 + ngzAzm)))
                        {
                            iReply = 1;
                        }
                    }
                }
                else
                {
                    iReply = 0;
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new Exception(ex.ToString()); }
        }
        
        //Checking for BHA-Interference - Bias due to BHA poles
        private static Decimal biasCheck(Int32 srvyID, Int32 RunID, Decimal CorrectedBTotal, Decimal valDip, Decimal valInclination, Decimal finalAzimuth, Decimal CurrentRowGB, Decimal convMotorSize, Decimal convDCSize, Decimal convMDBLength, Decimal convSpAbove, Decimal convSpBelow, String dbConnection)
        {
            try
            {
                Decimal iResult = -99.00M;
                ArrayList gbList = new ArrayList();
                Decimal[] gb = new Decimal[]{};
                Decimal maxGB = -99.00M, colFZVal = -99.00M, colGAVal = -99.00M, colGBVal = -99.00M;
                Int32 colFWVal = -99, colFXVal = -99, counter = 1;

                if (srvyID.Equals(1))
                {
                    iResult = CurrentRowGB + 1000.00M;
                }
                else
                {
                    while(counter <= srvyID)
                    {
                        colFWVal = colFW(convMotorSize);
                        colFXVal = colFX(convDCSize);
                        colFZVal = colFZ(CorrectedBTotal, valDip, valInclination, finalAzimuth);
                        colGAVal = colGA(convSpBelow, convMDBLength, colFWVal);
                        colGBVal = colGB(colFZVal, colGAVal);
                        gbList.Add(colGBVal);
                        counter++;
                    }
                    gb = gbList.ToArray(typeof(Decimal)) as Decimal[];
                    maxGB = gb.Max();
                    iResult = Math.Max(maxGB, CurrentRowGB) + 1000.00M;
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
        
        //Calculating Survey Quadrant
        protected static Int32 getQuadrant(Decimal MTF)
        {
            try
            {
                Int32 iReply = -99;
                if ((MTF > 0.0000M) && (MTF < 90.0000M))
                {
                    iReply = 1;
                }
                else if ((MTF > 90.0001M) && (MTF < 180.0000M))
                {
                    iReply = 2;
                }
                else if ((MTF > 180.0001M) && (MTF < 270.0000M))
                {
                    iReply = 3;
                }
                else
                {
                    iReply = 4;
                }

                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
        
        //Collecting meta info for FE Drill Collar PoleStrength limits used in BHA calculations
        protected static Decimal getDrillCollarPoleStrength()
        {
            try
            {
                Decimal iReply = -99.00M;
                String selData = "SELECT [dcpsValue] FROM [dmgDrillCollarPoleStrength]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetDecimal(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
        
        //Collecting Meta Information for Bottom-Hole-Assembly (BHA) used in calculations
        public static ArrayList getBHAData(Int32 RunID, String ClientDBConnection)
        {
            try
            {
                ArrayList iReply = new ArrayList();
                Int32 bhaID = -99;
                Decimal MotorSize = -99.00M;
                Int32 MSU = -99;
                Decimal DCSize = -99.00M;
                Int32 DCSU = -99;
                Decimal MDBLen = -99.00M;
                Int32 MDBU = -99;
                Decimal SpAbv = -99.00M;
                Int32 SpAbvU = -99;
                Decimal SpBlw = -99.00M;
                Int32 SpBlwU = -99;
                Decimal NMSpAlw = -99.00M;
                Decimal MGVal = -99.00M;
                Int32 MGVU = -99;
                Decimal DGTop = -99.00M;
                Int32 DGTU = -99;
                Decimal DSG = -99.00M;
                Int32 DSGU = -99;
                Decimal DGBtm = -99.00M;
                Int32 DGBtmU = -99;
                String selBHA = "SELECT [bhaID], [bhaMSize], [bhaMSizeUID], [bhaDSCSize], [bhaDSCUID], [bhaMDBLen], [bhaMDBLenUID], [bhaNMSpAbv], [bhaNMSAUID], [bhaNMSpBlw], [bhaNMSBUID], [bhaNMSAlw] FROM [BHA] WHERE [runID] = @runID";
                String selBHADet = "SELECT [bhaMGVal], [bhaMGValUID], [bhaDGTop], [bhaDGTUID], [bhaDSGVal], [bhaDSGUID], [bhaDGBottom], [bhaDGBUID] FROM [BHADetail] WHERE [bhaID] = @bhaID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getBHA = new SqlCommand(selBHA, dataCon);
                        getBHA.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readBHA = getBHA.ExecuteReader())
                        {
                            if (readBHA.HasRows)
                            {
                                while (readBHA.Read())
                                {
                                    bhaID = readBHA.GetInt32(0);
                                    iReply.Add(bhaID);
                                    MotorSize = readBHA.GetDecimal(1);
                                    iReply.Add(MotorSize);
                                    MSU = readBHA.GetInt32(2);
                                    iReply.Add(MSU);
                                    DCSize = readBHA.GetDecimal(3);
                                    iReply.Add(DCSize);
                                    DCSU = readBHA.GetInt32(4);
                                    iReply.Add(DCSU);
                                    MDBLen = readBHA.GetDecimal(5);
                                    iReply.Add(MDBLen);
                                    MDBU = readBHA.GetInt32(6);
                                    iReply.Add(MDBU);
                                    SpAbv = readBHA.GetDecimal(7);
                                    iReply.Add(SpAbv);
                                    SpAbvU = readBHA.GetInt32(8);
                                    iReply.Add(SpAbvU);
                                    SpBlw = readBHA.GetDecimal(9);
                                    iReply.Add(SpBlw);
                                    SpBlwU = readBHA.GetInt32(10);
                                    iReply.Add(SpBlwU);
                                    NMSpAlw = readBHA.GetDecimal(11);
                                    iReply.Add(NMSpAlw);
                                }
                            }
                        }
                        SqlCommand getBHADet = new SqlCommand(selBHADet, dataCon);
                        getBHADet.Parameters.AddWithValue("@bhaID", SqlDbType.Int).Value = bhaID.ToString();
                        using (SqlDataReader readBHADet = getBHADet.ExecuteReader())
                        {
                            if (readBHADet.HasRows)
                            {
                                while (readBHADet.Read())
                                {
                                    MGVal = readBHADet.GetDecimal(0);
                                    iReply.Add(MGVal);
                                    MGVU = readBHADet.GetInt32(1);
                                    iReply.Add(MGVU);
                                    DGTop = readBHADet.GetDecimal(2);
                                    iReply.Add(DGTop);
                                    DGTU = readBHADet.GetInt32(3);
                                    iReply.Add(DGTU);
                                    DSG = readBHADet.GetDecimal(4);
                                    iReply.Add(DSG);
                                    DSGU = readBHADet.GetInt32(5);
                                    iReply.Add(DSGU);
                                    DGBtm = readBHADet.GetDecimal(6);
                                    iReply.Add(DGBtm);
                                    DGBtmU = readBHADet.GetInt32(7);
                                    iReply.Add(DGBtmU);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getDipSQC(Decimal lowerSQCDip, Decimal upperSQCDip, Decimal calcDip)
        {
            try
            {
                // 0 = Yellow Survey; 1 = Green Survey; -1 = Red Survey; 
                Int32 iReply = -99;
                if (calcDip >= (-1*lowerSQCDip) && calcDip <= lowerSQCDip)
                {
                    iReply = 1;
                }
                else if (calcDip < (-1*upperSQCDip) || calcDip > upperSQCDip)
                {
                    iReply = -1;
                }
                else
                {
                    iReply = 0;
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getBTSQC(Int32 lowerSQCBTotal, Int32 upperSQCBTotal, Int32 calcBTotal)
        {
            try
            {
                // 0 = Yellow Survey; 1 = Green Survey; -1 = Red Survey; 
                Int32 iReply = -99;
                if (calcBTotal >= (-1*lowerSQCBTotal) && calcBTotal <= lowerSQCBTotal)
                {
                    iReply = 1;
                }
                else if (calcBTotal < (-1*upperSQCBTotal) || calcBTotal > upperSQCBTotal)
                {
                    iReply = -1;
                }
                else
                {
                    iReply = 0;
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGTSQC(Decimal lowerSQCGTotal, Decimal upperSQCGTotal, Decimal calcGTotal)
        {
            try
            {
                // 0 = Yellow Survey; 1 = Green Survey; -1 = Red Survey; 
                Int32 iReply = -99;
                if (calcGTotal >= (-1*lowerSQCGTotal) && calcGTotal <= lowerSQCGTotal)
                {
                    iReply = 1;
                }
                else if (calcGTotal < (-1*upperSQCGTotal) || calcGTotal > upperSQCGTotal)
                {
                    iReply = -1;
                }
                else
                {
                    iReply = 0;
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getSQCMet(Int32 lowerSQCBTotal, Decimal lowerSQCDip, Decimal lowerSQCGTotal, Int32 upperSQCBTotal, Decimal upperSQCDip, Decimal upperSQCGTotal, Int32 calcBTotal, Decimal calcDip, Decimal calcGTotal)
        {
            try
            {
                // 0 = Yellow Survey; 1 = Green Survey; -1 = Red Survey; 
                Int32 iReply = -99;
                if((calcBTotal >= lowerSQCBTotal && calcBTotal <= upperSQCBTotal) && (calcDip >= lowerSQCDip && calcDip <= upperSQCDip) && (calcGTotal >= lowerSQCGTotal && calcGTotal <= upperSQCGTotal))
                { 
                    iReply = 1;
                }
                else if((calcGTotal >= upperSQCGTotal) || (calcDip >= upperSQCDip) || (calcDip <= (-1*upperSQCDip)) || (calcGTotal >= upperSQCGTotal) || (calcGTotal <= (-1 * upperSQCGTotal)))
                {
                    iReply = -1;
                }
                else
                {
                    iReply = 0;
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
        
        //Stores Raw Formatted Data in Client's DB Correction Formatted Data Table
        public static Int32 StoreRawFormatedData(String Date, String Time, Decimal depth, Decimal Gx, Decimal Gy, Decimal Gz, Decimal Bx, Decimal By, Decimal Bz, Int32 JobID, Int32 RunID, Int32 SurveyID, Int32 dataRowType, Int32 serviceID, String fileName, String UserName, String UserRealm, String DBConn)
        {
            try
            {
                Int32 iResult = -99;
                
                using (SqlConnection conRawData = new SqlConnection(DBConn))
                {
                    try
                    {
                        conRawData.Open();
                        String enterRawData = "INSERT INTO [CorrectionFormattedData] ([crrDate], [crrTime], [crrDepth], [crrGx], [crrGy], [crrGz], [crrBx], [crrBy], [crrBz], [jobID], [runID], [srvyRowID], [srvID], [demID], [fileName], [cTime], [uTime], [username], [realm]) VALUES (@crrDate, @crrTime, @crrDepth, @crrGx, @crrGy, @crrGz, @crrBx, @crrBy, @crrBz, @jobID, @runID, @srvyRowID, @srvID, @demID, @fileName, @cTime, @uTime, @username, @realm)";
                        SqlCommand insertData = new SqlCommand(enterRawData, conRawData);
                        insertData.Parameters.AddWithValue("@crrDate", SqlDbType.NVarChar).Value = Date.ToString();
                        insertData.Parameters.AddWithValue("@crrTime", SqlDbType.NVarChar).Value = Time.ToString();
                        insertData.Parameters.AddWithValue("@crrDepth", SqlDbType.Decimal).Value = depth.ToString();
                        insertData.Parameters.AddWithValue("@crrGx", SqlDbType.Decimal).Value = Gx.ToString();
                        insertData.Parameters.AddWithValue("@crrGy", SqlDbType.Decimal).Value = Gy.ToString();
                        insertData.Parameters.AddWithValue("@crrGz", SqlDbType.Decimal).Value = Gz.ToString();
                        insertData.Parameters.AddWithValue("@crrBx", SqlDbType.Decimal).Value = Bx.ToString();
                        insertData.Parameters.AddWithValue("@crrBy", SqlDbType.Decimal).Value = By.ToString();
                        insertData.Parameters.AddWithValue("@crrBz", SqlDbType.Decimal).Value = Bz.ToString();
                        insertData.Parameters.AddWithValue("@jobID", SqlDbType.Int).Value = JobID.ToString();
                        insertData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        insertData.Parameters.AddWithValue("@srvyRowID", SqlDbType.Int).Value = SurveyID.ToString();
                        insertData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = serviceID.ToString();
                        insertData.Parameters.AddWithValue("@demID", SqlDbType.Int).Value = dataRowType.ToString();
                        insertData.Parameters.AddWithValue("@fileName", SqlDbType.NVarChar).Value = fileName.ToString();                        
                        insertData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insertData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insertData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insertData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserRealm.ToString();

                        iResult = Convert.ToInt32(insertData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        conRawData.Close();
                        conRawData.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
        
        //Process each Survey row 
        static void DetailsProcessor(Dictionary<String, Int32> header_GB, String[] arrD, ArrayList list, Int32[] fliparray, Decimal[] GB_decimal)
        {
            Int32 arrDlength, gx, gy, gz, bx, by, bz;
            gx = gy = gz = bx = by = bz = 0;
            Decimal Gx, Gy, Gz, Bx, By, Bz;
            String info, decimal_vals = String.Empty;
            arrDlength = arrD.Length;

            foreach (KeyValuePair<String, Int32> item in header_GB)
            {
                if (item.Key.ToLower().Contains("gx"))
                { gx = item.Value; }
                if (item.Key.ToLower().Contains("gy"))
                { gy = item.Value; }
                if (item.Key.ToLower().Contains("gz"))
                { gz = item.Value; }
                if (item.Key.ToLower().Contains("bx"))
                { bx = item.Value; }
                if (item.Key.ToLower().Contains("by"))
                { by = item.Value; }
                if (item.Key.ToLower().Contains("bz"))
                { bz = item.Value; }
            }

            //if (arrD[2].Trim() == "" || arrD[gx].Trim() == "" || arrD[gy].Trim() == "" || arrD[gz].Trim() == "" || arrD[bx].Trim() == "" || arrD[by].Trim() == "" || arrD[bz].Trim() == "")
            if (String.IsNullOrEmpty(arrD[2]) || String.IsNullOrEmpty(arrD[gx]) || String.IsNullOrEmpty(arrD[gy]) || String.IsNullOrEmpty(arrD[gz]) || String.IsNullOrEmpty(arrD[bx]) || String.IsNullOrEmpty(arrD[by]) || String.IsNullOrEmpty(arrD[bz]))
            {
                info = String.Format("{0} {1} {2}", "Invalid Data at Depth: ", arrD[2], arrD[1]);
                //info = String.Format("The Required Fields to process the Survey are missing. Survey at " + arrD[2] + " is skipped. Please fix the issue and process it Manually..", arrD[1]);
                list.Add(info);
                info = "";
            }
            else
            {
                Gx = Convert.ToDecimal(arrD[gx]);
                Gy = Convert.ToDecimal(arrD[gy]);
                Gz = Convert.ToDecimal(arrD[gz]);
                Bx = Convert.ToDecimal(arrD[bx]);
                By = Convert.ToDecimal(arrD[by]);
                Bz = Convert.ToDecimal(arrD[bz]);

                Gx = Gx * fliparray[0];
                Gy = Gy * fliparray[1];
                Gz = Gz * fliparray[2];
                Bx = Bx * fliparray[3];
                By = By * fliparray[4];
                Bz = Bz * fliparray[5];

                GB_decimal[0] = Gx;
                GB_decimal[1] = Gy;
                GB_decimal[2] = Gz;
                GB_decimal[3] = Bx;
                GB_decimal[4] = By;
                GB_decimal[5] = Bz;

                info = "Processed at Depth " + arrD[2] + ": " + Gx + ", " + Gy + ", " + Gz + ", " + Bx + ", " + By + ", " + Bz + " ";
                list.Add(info);
                info = "";
            }
        }
        
        //Process Survey's header column to determine column positions for Bs and Gs
        static void HeaderTOPLINE(Dictionary<String, Int32> header_GB, String[] arr)
        {
            try
            {
                Int32 arrlength = arr.Length;
                for (Int32 i = 0; i < arrlength; i++)
                {
                    if (arr[i].ToLower().Contains("gx") ||
                          arr[i].ToLower().Contains("gy") ||
                          arr[i].ToLower().Contains("gz") ||
                          arr[i].ToLower().Contains("bx") ||
                          arr[i].ToLower().Contains("by") ||
                          arr[i].ToLower().Contains("bz")
                        )
                        header_GB.Add(arr[i].Trim(), i);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static ArrayList processRawData(Int32 DataEntryMethodID, Int32 ServiceID, Int32 DataFileID, String FileName, String FileExtension, Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, List<System.Data.DataTable> InputDataTables, ArrayList IFRList, String UserName, String UserRealm, String ClientDBConnection)
        {
            try
            {
                ArrayList iReply = new ArrayList();
                Int32 iReply12 = -99;
                System.Data.DataTable iReplyData = new System.Data.DataTable();
                System.Data.DataTable fileData = new System.Data.DataTable();
                fileData = (System.Data.DataTable)InputDataTables[1];
                iReply12 = getMSAMinRowCount(fileData);
                if (iReply12 < 12) 
                { 
                    iReply.Add(-1); 
                } 
                else 
                { 
                    iReply.Add(1);
                    iReplyData = AveragingFilter(ServiceID, OperatorID, ClientID, WellPadID, WellID, WellSectionID, RunID, fileData, IFRList, UserName, UserRealm, ClientDBConnection);
                }
                iReply.Add(fileData);
                iReply.Add(iReplyData);                               
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getMSAMinRowCount(System.Data.DataTable RawDataTable)
        {
            try
            {
                //Quads : 0.00 <= quad1 => 90.00; 90.01 <= quad2 => 180.00; 180.01 <= quad3 => 270.00; 270.01 <= quad4 => 360.00; quad5 = bogus
                Int32 iReply = -99, quad1 = 0, quad2 = 0, quad3 = 0, quad4 = 0, quad5 = -99;
                List<Decimal> GTFList = new List<Decimal> { };
                Decimal gxVal = -99.00M, gyVal = -99.00M;
                foreach (System.Data.DataRow dRow in RawDataTable.Rows)
                {
                    gxVal = Convert.ToDecimal(dRow["Gx"]);
                    gyVal = Convert.ToDecimal(dRow["Gy"]);
                    GTFList.Add(QC.getGTF(gxVal, gyVal));
                }
                foreach (Decimal value in GTFList)
                {
                    if ((value >= 0.00M) || (value <= 90.00M)) { quad1++; }
                    else if ((value >= 90.01M) || (value <= 180.00M)) { quad2++; }
                    else if ((value >= 180.01M) || (value <= 270.00M)) { quad3++; }
                    else if ((value >= 270.01M) || (value <= 360.00M)) { quad4++; }
                    else { quad5++; }
                }
                iReply = quad1 + quad2 + quad3 + quad4;
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getAnalysisRawData(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn srvRID = iReply.Columns.Add("srvRID", typeof(Int32));
                DataColumn Depth = iReply.Columns.Add("Depth", typeof(Decimal));
                DataColumn Len = iReply.Columns.Add("LenU", typeof(Int32));
                DataColumn Bx = iReply.Columns.Add("Bx", typeof(Decimal));
                DataColumn By = iReply.Columns.Add("By", typeof(Decimal));
                DataColumn Bz = iReply.Columns.Add("Bz", typeof(Decimal));
                DataColumn Gx = iReply.Columns.Add("Gx", typeof(Decimal));
                DataColumn Gy = iReply.Columns.Add("Gy", typeof(Decimal));
                DataColumn Gz = iReply.Columns.Add("Gz", typeof(Decimal));
                DataColumn Acel = iReply.Columns.Add("acelU", typeof(Int32));
                DataColumn Mag = iReply.Columns.Add("magU", typeof(Int32));
                String selData = "SELECT [srvyRowID], [crrDepth], [lenU], [crrBx], [crrBy], [crrBz], [crrGx], [crrGy], [crrGz], [acuID], [muID] FROM [CorrectionFormattedData] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID AND [wswID] = @wswID AND [welID] = @welID AND [runID] = @runID";
                Int32 sID = -99, lengthUnit = -99, magUnit = -99, acelUnit = -99;
                Decimal rdepth = -99.00M, rbx, rby, rbz, rgx, rgy, rgz;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        sID = readData.GetInt32(0);
                                        rdepth = readData.GetDecimal(1);
                                        lengthUnit = readData.GetInt32(2);
                                        rbx = readData.GetDecimal(3);
                                        rby = readData.GetDecimal(4);
                                        rbz = readData.GetDecimal(5);
                                        rgx = readData.GetDecimal(6);
                                        rgy = readData.GetDecimal(7);
                                        rgz = readData.GetDecimal(8);
                                        acelUnit = readData.GetInt32(9);
                                        magUnit = readData.GetInt32(10);                                        
                                        iReply.Rows.Add(sID, rdepth, lengthUnit, rbx, rby, rbz, rgx, rgy, rgz, acelUnit, magUnit);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getQuadrantRawData(Int32 JobID, Int32 WellID, Int32 WellSectionID, Int32 RunID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn srvRID = iReply.Columns.Add("srvRID", typeof(Int32));
                DataColumn Depth = iReply.Columns.Add("Depth", typeof(Decimal));
                DataColumn Bx = iReply.Columns.Add("Bx", typeof(Decimal));
                DataColumn By = iReply.Columns.Add("By", typeof(Decimal));
                DataColumn Bz = iReply.Columns.Add("Bz", typeof(Decimal));
                DataColumn Gx = iReply.Columns.Add("Gx", typeof(Decimal));
                DataColumn Gy = iReply.Columns.Add("Gy", typeof(Decimal));
                DataColumn Gz = iReply.Columns.Add("Gz", typeof(Decimal));
                DataColumn MTF = iReply.Columns.Add("MTF", typeof(Decimal));
                DataColumn mtfQuad = iReply.Columns.Add("mtfQuad", typeof(Int32));
                DataColumn GTF = iReply.Columns.Add("GTF", typeof(Decimal));
                String selData = "SELECT [srvyRowID], [crrDepth], [crrBx], [crrBy], [crrBz], [crrGx], [crrGy], [crrGz] FROM [CorrectionFormattedData] WHERE [jobID] = @jobID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID ORDER BY [srvyRowID] Desc";
                Int32 sID = -99, qd = -99;
                Decimal rdepth = -99.00M, rbx, rby, rbz, rgx, rgy, rgz, rmtf, rgtf;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@jobID", SqlDbType.Int).Value = JobID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        sID = readData.GetInt32(0);
                                        rdepth = readData.GetDecimal(1);
                                        rbx = readData.GetDecimal(2);
                                        rby = readData.GetDecimal(3);
                                        rbz = readData.GetDecimal(4);
                                        rgx = readData.GetDecimal(5);
                                        rgy = readData.GetDecimal(6);
                                        rgz = readData.GetDecimal(7);
                                        rmtf = QC.colQ(rbx, rby);
                                        qd = getQuadrant(rmtf);
                                        rgtf = QC.getGTF(rgx, rgy);
                                        iReply.Rows.Add(sID, rdepth, rbx, rby, rbz, rgx, rgy, rgz, rmtf, qd, rgtf);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable AveragingFilter(Int32 ServiceID, Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, System.Data.DataTable RawDataTable, ArrayList selectedIFR, String UserName, String UserRealm, String ClientDBConnection)
        {
            try
            {
                //DataTable contain uploaded Raw Data not converted to computable units
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn srvID = iReply.Columns.Add("srvID", typeof(Int32));
                DataColumn ciaDepth = iReply.Columns.Add("avgDepth", typeof(Decimal));
                DataColumn ciaGx = iReply.Columns.Add("avgGx", typeof(Decimal));
                DataColumn ciaGy = iReply.Columns.Add("avgGy", typeof(Decimal));
                DataColumn ciaGz = iReply.Columns.Add("avgGz", typeof(Decimal));
                DataColumn ciaGTotal = iReply.Columns.Add("avgGTotal", typeof(Decimal));
                DataColumn ciaGTAvg = iReply.Columns.Add("avgGTAvg", typeof(Decimal));
                DataColumn ciaGTAvgDelta = iReply.Columns.Add("avgGTDelta", typeof(Decimal));
                DataColumn ciaBx = iReply.Columns.Add("avgBx", typeof(Decimal));
                DataColumn ciaBy = iReply.Columns.Add("avgBy", typeof(Decimal));
                DataColumn ciaBz = iReply.Columns.Add("avgBz", typeof(Decimal));
                DataColumn ciaBp = iReply.Columns.Add("avgBp", typeof(Decimal));
                DataColumn ciaBpDelta = iReply.Columns.Add("avgBpDelta", typeof(Decimal));
                DataColumn ciaBTotal = iReply.Columns.Add("avgBTotal", typeof(Decimal));
                DataColumn ciaBTAvg = iReply.Columns.Add("avgBTAvg", typeof(Decimal));
                DataColumn ciaBTDelta = iReply.Columns.Add("avgBTDelta", typeof(Decimal));
                DataColumn dDip = iReply.Columns.Add("avgDip", typeof(Decimal));
                DataColumn dDipAvg = iReply.Columns.Add("avgDipAvg", typeof(Decimal));
                DataColumn dDipDelta = iReply.Columns.Add("avgDipDelta", typeof(Decimal));
                DataColumn metGTSQC = iReply.Columns.Add("metGTAvg", typeof(String));
                DataColumn metBTSQC = iReply.Columns.Add("metBTAvg", typeof(String));                
                DataColumn metDipSQC = iReply.Columns.Add("metDipAvg", typeof(String));
                DataColumn metBpSQC = iReply.Columns.Add("metBpAvg", typeof(String));
                Decimal rDepth = -99.00M, rGx, rGy, rGz, rBx, rBy, rBz;
                Int32 sID = -99, ifrVal = -99, AccelU = -99, MagU = -99, LenU = -99, mdecCID = -99, imdecCID = -99;
                String dipAvgMet = String.Empty, btAvgMet = String.Empty, gtAvgMet = String.Empty, bzAvgMet = String.Empty;
                Decimal lwrSQCBT = -99.00M, lwrSQCDip = -99.00M, lwrSQCGT = -99.00M, uprSQCBT = -99.00M, uprSQCDip = -99.00M, uprSQCGT = -99.00M;
                Decimal dip = -99.00M, idip = -99.00M, bTotal = -99.00M, iBTotal = -99.00M, gTotal = -99.00M, iGTotal = -99.00M, mDec = -99.00M, imDec = -99.00M;
                Decimal avgBT = -99.00M, avgGT = -99.00M, avgDip = -99.00M, lowerBT = -99.00M, upperBT = -99.00M, lowerGT = -99.00M, upperGT = -99.00M, lowerDip = -99.00M, upperDip = -99.00M;
                Decimal calcBT = -99.00M, calcGT = -99.00M, calcDip = -99.00M;
                Decimal deltaBT = -99.00M, deltaGT = -99.00M, deltaDip = -99.00M;
                Decimal valGTF = -99.00M, mAzm = -99.00M, valInc = -99.00M, valBp = -99.00M, bpDelta = -99.00M;
                List<Decimal> btList = new List<Decimal>(), gtList = new List<Decimal>(), dipList = new List<Decimal>();
                //Collecting Meta information
                ArrayList csqc = new ArrayList();
                csqc = DMG.getCorrSQCList();
                lwrSQCBT = Convert.ToInt32(csqc[1]);
                lwrSQCDip = Convert.ToDecimal(csqc[2]);
                lwrSQCGT = Convert.ToDecimal(csqc[3]);
                uprSQCBT = Convert.ToInt32(csqc[6]);
                uprSQCDip = Convert.ToDecimal(csqc[7]);
                uprSQCGT = Convert.ToDecimal(csqc[8]);
                ArrayList RefMags = new ArrayList();
                RefMags = OPR.getRefMagList(OperatorID, ClientID, WellPadID, WellID, ClientDBConnection);
                ifrVal = Convert.ToInt32(RefMags[0]);
                Decimal refStartD = -99.00M, refEndD = -99.00M, refDip = -99.00M, refDec = -99.00M, refBTotal = -99.00M, refGTotal = -99.00M;
                Decimal cnvRefStartD = -99.0M, cnvRefEndD = -99.00M, cnvRefBTotal = -99.00M, cnvRefGTotal = -99.00M;
                Int32 refLenU = -99, refDecO = -99, refMagU = -99, refAcelU = -99;
                foreach(var value in selectedIFR)
                {
                    refStartD = Convert.ToDecimal(selectedIFR[0]);
                    refEndD = Convert.ToDecimal(selectedIFR[1]);
                    refLenU = Convert.ToInt32(selectedIFR[2]);
                    if (refLenU.Equals(4)) { cnvRefStartD = refStartD / 3.2808M; cnvRefEndD = refEndD / 3.2808M; } else { cnvRefStartD = refStartD; cnvRefEndD = refEndD; }
                    refDip = Convert.ToDecimal(selectedIFR[3]);
                    refDec = Convert.ToDecimal(selectedIFR[4]);
                    refDecO = Convert.ToInt32(selectedIFR[5]);
                    refBTotal = Convert.ToDecimal(selectedIFR[6]);
                    refMagU = Convert.ToInt32(selectedIFR[7]);
                    cnvRefBTotal = Units.convBTotal(refBTotal, refMagU);
                    refGTotal = Convert.ToDecimal(selectedIFR[8]);
                    refAcelU = Convert.ToInt32(selectedIFR[9]);
                    cnvRefGTotal = Units.convGTotal(refGTotal, refAcelU);
                }
                Decimal cnvDepth = -99.00M, cnvDip = -99.00M, cnvBT = -99.00M, cnvGT = -99.000000000M, cnvIFRGT = -99.00000000M, cnvIFRDip = -99.000000M, cnvIFRBT = -99.000000M;
                Decimal cnvGx = -99.00M, cnvGy = -99.00M, cnvGz = -99.00M, cnvBx = -99.00M, cnvBy = -99.00M, cnvBz = -99.00M;
                foreach (DataRow row in RawDataTable.Rows)
                {
                    sID = Convert.ToInt32(row["drID"]);
                    rDepth = Convert.ToDecimal(row["Depth"]);
                    LenU = Convert.ToInt32(row["LenU"]);
                    rBx = Convert.ToDecimal(row["Bx"]);
                    rBy = Convert.ToDecimal(row["By"]);
                    rBz = Convert.ToDecimal(row["Bz"]);
                    rGx = Convert.ToDecimal(row["Gx"]);
                    rGy = Convert.ToDecimal(row["Gy"]);
                    rGz = Convert.ToDecimal(row["Gz"]);
                    AccelU = Convert.ToInt32(row["acU"]);
                    MagU = Convert.ToInt32(row["magU"]);
                    if (rDepth >= cnvRefStartD || rDepth <= cnvRefEndD)
                    {
                        if (LenU.Equals(4)) { cnvDepth = rDepth / 3.2808M; } else { cnvDepth = rDepth; }
                        cnvGx = Units.convGTotal(rGx, AccelU);
                        cnvGy = Units.convGTotal(rGy, AccelU);
                        cnvGz = Units.convGTotal(rGz, AccelU);
                        cnvBx = Units.convBTotal(rBx, MagU);
                        cnvBy = Units.convBTotal(rBy, MagU);
                        cnvBz = Units.convBTotal(rBz, MagU);
                        cnvDip = Units.convDip(dip);
                        cnvBT = Units.convBTotal(bTotal, MagU);
                        cnvIFRDip = Units.convDip(idip);
                        cnvIFRBT = Units.convBTotal(iBTotal, MagU);
                        cnvGT = Units.convGTotal(gTotal, AccelU);
                        cnvIFRGT = Units.convGTotal(iGTotal, AccelU);

                        Decimal valGTotal = -99.00000M, valBTotal = -99.00M, valDip = -99.00M;
                        valGTotal = QC.colCF(cnvGx, cnvGy, cnvGz); //G-Total   
                        gtList.Add(valGTotal);
                        valBTotal = QC.colCA(cnvBx, cnvBy, cnvBz); //B-Total
                        btList.Add(valBTotal);
                        valDip = QC.colBA(cnvGx, cnvGy, cnvGz, cnvBx, cnvBy, cnvBz, valGTotal, valBTotal); // Long Collar Dip                    
                        dipList.Add(valDip);
                        valInc = QC.colT(cnvGx, cnvGy, cnvGz);
                        valGTF = QC.getGTF(cnvGx, cnvGy);
                        mAzm = QC.colY(valGTF, valInc, cnvBx, cnvBy, cnvBz);
                        valBp = (valBTotal * (Decimal)Math.Cos((Double)valDip) * (Decimal)Math.Tan((Double)0.25)) / (((Decimal)Math.Sin((Double)valInc) * (Decimal)Math.Sin((Double)mAzm)) - ((Decimal)Math.Tan(0.25) * (Decimal)Math.Sin((Double)valInc) * (Decimal)Math.Cos((Double)mAzm)));
                        bpDelta = cnvBz - valBp;
                        //GTAvg = +/-0.003g; BTAvg = +/-250nT; DipAvg = +/-0.3 deg
                        iReply.Rows.Add(sID, cnvDepth, rGx, rGy, rGz, valGTotal, avgGT, deltaGT, rBx, rBy, rBz, valBp, bpDelta, valBTotal, avgBT, deltaBT, valDip, avgDip, deltaDip, gtAvgMet, btAvgMet, dipAvgMet);
                        iReply.AcceptChanges();
                    }
                }
                avgBT = btList.Average();
                lowerBT = -1 * (avgBT + 250.00M);
                upperBT = avgBT + 250.00M;
                avgGT = gtList.Average();
                lowerGT = -1 * (avgGT + 0.003M);
                upperGT = avgGT + 0.003M;                                
                avgDip = dipList.Average();
                lowerDip = -1 * (avgDip + 0.3M);
                upperDip = avgDip + 0.3M;
                foreach (DataRow row in iReply.Rows)
                {
                    calcBT = Convert.ToDecimal(row["avgBTotal"]);                        
                    calcGT = Convert.ToDecimal(row["avgGTotal"]);
                    calcDip = Convert.ToDecimal(row["avgDip"]);
                    deltaBT = calcBT - avgBT;
                    if (deltaBT <= -250.00M) { row.SetField("metBTAvg", "Yellow"); } else if (deltaBT >= 250.00M) { row.SetField("metBTAvg", "Yellow"); } else { row.SetField("metBTAvg", "Green"); }
                    row.SetField("avgBTavg", avgBT);
                    row.SetField("avgBTDelta", deltaBT);
                    if (cnvBz <= (-1 * valBp)) { row.SetField("metBpAvg", "Yellow"); } else if (cnvBz >= valBp) { row.SetField("metBpAvg", "Yellow"); } else { row.SetField("metBpAvg", "Green"); }
                    deltaGT = calcGT - avgGT;
                    if ((deltaGT <= -0.003M) || (deltaGT >= 0.003M)) { row.SetField("metGTAvg", "Yellow"); } else { row.SetField("metGTAvg", "Green"); }                    
                    row.SetField("avgGTavg", avgGT);
                    row.SetField("avgGTDelta", deltaGT);
                    deltaDip = calcDip - avgDip;
                    if ((deltaDip <= -0.3M) || (deltaDip >= 0.3M)) { row.SetField("metDipAvg", "Yellow"); } else { row.SetField("metDipAvg", "Green"); }                    
                    row.SetField("avgDipavg", avgDip);
                    row.SetField("avgDipDelta", deltaDip);
                    iReply.AcceptChanges();
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //Calculates Input values (Bs - converted to selected units (nT), MTF, GTF and B-Total for each Survey row for Correction analysis
        public static System.Data.DataTable InputToAnalysis(Int32 ServiceID, Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, System.Data.DataTable RawDataTable, List<KeyValuePair<String, Decimal>> selectedIFR, String UserName, String UserRealm, String ClientDBConnection)
        {
            try
            {
                //DataTable contain uploaded Raw Data not converted to computable units
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn srvID = iReply.Columns.Add("srvID", typeof(Int32));
                DataColumn ciaDepth = iReply.Columns.Add("ciaDepth", typeof(Decimal));
                DataColumn ciaGx = iReply.Columns.Add("ciaGx", typeof(Decimal));
                DataColumn ciaGy = iReply.Columns.Add("ciaGy", typeof(Decimal));
                DataColumn ciaGz = iReply.Columns.Add("ciaGz", typeof(Decimal));
                DataColumn ciaGoxy = iReply.Columns.Add("ciaGoxy", typeof(Decimal));
                DataColumn ciaGTotal = iReply.Columns.Add("ciaGTotal", typeof(Decimal));
                DataColumn dGt = iReply.Columns.Add("delGT", typeof(Decimal));
                DataColumn ciaBx = iReply.Columns.Add("ciaBx", typeof(Decimal));
                DataColumn ciaBy = iReply.Columns.Add("ciaBy", typeof(Decimal));
                DataColumn ciaBz = iReply.Columns.Add("ciaBz", typeof(Decimal));
                DataColumn ciaBoxy = iReply.Columns.Add("ciaBoxy", typeof(Decimal));
                DataColumn ciaBTotal = iReply.Columns.Add("ciaBTotal", typeof(Decimal));
                DataColumn dBt = iReply.Columns.Add("delBT", typeof(Decimal));
                DataColumn dDip = iReply.Columns.Add("delDip", typeof(Decimal));                                
                DataColumn metDipSQC = iReply.Columns.Add("metDipSQC", typeof(String));
                DataColumn metBTSQC = iReply.Columns.Add("metBTSQC", typeof(String));
                DataColumn metGTSQC = iReply.Columns.Add("metGTSQC", typeof(String));
                Decimal rDepth = -99.00M, rGx, rGy, rGz, rBx, rBy, rBz;
                Int32 sID = -99, ifrVal = -99, AccelU = -99, MagU = -99, LenU = -99, jConverg = -99, lwrSQCBT = -99, uprSQCBT = -99;                
                Int32 valFX = -99;
                String dipMet = String.Empty, btMet = String.Empty, gtMet = String.Empty;
                Decimal mDec = -99.00M, dip = -99.00M, bTotal = -99.000000M, gTotal = -99.000000M, gconv = -99.00M, imDec = -99.00M, idip = -99.00M, iBTotal = -99.00M, iGTotal = -99.00M;
                Decimal refGx = -99.000000000000M, refGy = -99.000000000000M, refGz = -99.000000000000M, refBx = -99.000000000000M, refBy = -99.000000000000M, refBz = -99.000000000000M;
                Decimal valGTF = -99.000000000000M, valMTF = -99.000000000000M, valDeclination = -99.000000000000M, valInclination = -99.000000000000M;
                Decimal valBoxy = -99.000000000000M, valBTotal = -99.000000000000M, valGoxy = -99.000000000000M, valGTotal = -99.000000000000M;
                Decimal valDip = -99.000000000000M, valMagAzimuth = -99.000000000000M, plnGridAzimuth = -99.000000000000M, ifrGridAzimuth = -99.000000000000M, valHQ = -99.000000000000M;
                Decimal plnTrueAzimuth = -99.000000000000M, ifrTrueAzimuth = -99.000000000000M, gridAzimuth = -99.000000000000M, trueAzimuth = -99.000000000000M, finalAzimuth = -99.000000000000M;
                Decimal valFW  = -99.000000000000M, valFZ = -99.000000000000M, valGA = -99.000000000000M, valGB = -99.000000000000M;
                Decimal valCD = -99.000000000000M, valCF = -99.000000000000M, valCG = -99.000000000000M, valCL = -99.000000000000M, valCK = -99.000000000000M, valBM = -99.000000000000M, valCM = -99.000000000000M, valCN = -99.000000000000M;
                Decimal valBO = -99.000000000000M, valBP = -99.000000000000M, valBN = -99.000000000000M, valBQ = -99.000000000000M, valBR = -99.000000000000M, valHR = -99.000000000000M;
                Decimal valCS = -99.000000000000M, valHT = -99.000000000000M, valCT = -99.000000000000M, valCU = -99.000000000000M;
                Decimal valBU = -99.000000000000M, valBV = -99.000000000000M, valBW = -99.000000000000M, valBX = -99.000000000000M;
                Decimal valBzBias = -99.000000000000M, valBzBiasConditional = -99.000000000000M, valBzScaleFactor = -99.000000000000M, valBzCorrected = -99.000000000000M, valDeltaBzCorrected = -99.000000000000M;
                Decimal valBTotalCorrected = -99.0000000000000M, valDeltaBTotalCorrect = -99.000000000000M, valDipCorrected = -99.000000000000M, valDeltaDipCorrected = -99.000000000000M;
                Decimal lwrSQCDip = -99.00M, uprSQCDip, lwrSQCGT = -99.000M, uprSQCGT = -99.000M, TSBias = -99.00M, ThreeSigmaVal = -99.00M;
                Decimal MSize = -99.00M; // BHA Motor Size
                Int32 MSizeU = -99; // BHA Motor Size Unit
                Decimal DSCSize = -99.00M; // Drill String/Collar Size
                Int32 DSCSizeU = -99; // DSC Size Unit
                Decimal MDBLen = -99.00M; // Motor + Drill String Size
                Int32 MDBU = -99; // Motor + Drill String Size Unit
                Decimal NMSpAbv = -99.00M; // Non-Mag Space (Above)
                Int32 NMSpAbvU = -99;
                Decimal NMSpBlw = -99.00M; // Non-Mag Space (Below)
                Int32 NMSpBlwU = -99;
                Decimal NMSpAlw = -99.00M; // Non-Mag Space (Allowable)
                Decimal MGValue = -99.000000M; // Gaussmeter Reading (Motor)
                Int32 MGVU = -99;
                Decimal DGTop = -99.00M; //Distance from Top
                Int32 DGTU = -99;
                Decimal DSGValue = -99.000000M; // Drill String Gaussmeter Reading
                Int32 DSGU = -99;
                Decimal DGBtm = -99.00M; //Distance from Bottom
                Int32 DGBTU = -99;
                Int32 LatiNS = -99, LngiEW = -99, mdecCID = -99, imdecCID = -99, gcoID = -99;
                Decimal rInc = -99.00M, rAzm = -99.00M, rTInc = -99.00M, rTAzm = -99.00M, convRTAzm = -99.00M, convRTAzmIFR = -99.00M, convPTInc = -99.00M, convPTAzm = -99.00M;
                List<Decimal> htList = new List<Decimal>();
                List<Double> srvIDValues = new List<Double>(), cnValues = new List<Double>(), htValues = new List<Double>(), hqValues = new List<Double>(), hrValues = new List<Double>();
                List<Double> bxValues = new List<Double>(), byValues = new List<Double>(), bzValues = new List<Double>(), biValues = new List<Double>(), bjValues = new List<Double>();
                //Collecting Meta information
                ArrayList BHAInfo = new ArrayList();
                List<Decimal> gbList = new List<Decimal>();                                
                List<String> WellInfo = AST.getWellDetailList(OperatorID, ClientID, WellID, ClientDBConnection);
                LngiEW = Convert.ToInt32(WellInfo[3]);
                LatiNS = Convert.ToInt32(WellInfo[6]);
                ArrayList csqc = new ArrayList();
                csqc = DMG.getCorrSQCList();
                lwrSQCBT = Convert.ToInt32(csqc[1]);
                lwrSQCDip = Convert.ToDecimal(csqc[2]);
                lwrSQCGT = Convert.ToDecimal(csqc[3]);
                uprSQCBT = Convert.ToInt32(csqc[6]);
                uprSQCDip = Convert.ToDecimal(csqc[7]);
                uprSQCGT = Convert.ToDecimal(csqc[8]);
                ArrayList RefMags = new ArrayList();
                RefMags = OPR.getRefMagList(OperatorID, ClientID, WellPadID, WellID, ClientDBConnection);
                ifrVal = Convert.ToInt32(RefMags[0]);
                gconv = Convert.ToDecimal(RefMags[8]);
                gcoID = Convert.ToInt32(RefMags[9]);                
                jConverg = Convert.ToInt32(RefMags[10]);

                foreach (KeyValuePair<String, Decimal> pair in selectedIFR)
                {
                    if (pair.Key.Equals("Dip")) { dip = pair.Value; idip = pair.Value; }
                    else if (pair.Key.Equals("BT")) { bTotal = pair.Value; iBTotal = pair.Value; }
                    else if (pair.Key.Equals("GT")) { gTotal = pair.Value; iGTotal = pair.Value; }
                    else if (pair.Key.Equals("MDec")) { mDec = pair.Value; imDec = pair.Value; }
                    else { mdecCID = Convert.ToInt32(pair.Value); imdecCID = Convert.ToInt32(pair.Value); }
                }               

                ArrayList RunInfo = AST.getRunList(RunID, ClientDBConnection);
                rInc = Convert.ToDecimal(RunInfo[0]);
                rAzm = Convert.ToDecimal(RunInfo[1]);
                rTInc = Convert.ToDecimal(RunInfo[2]);
                rTAzm = Convert.ToDecimal(RunInfo[3]);                
                BHAInfo = AST.getBhaInfoList(RunID, ClientDBConnection);
                MSize = Convert.ToDecimal(BHAInfo[0]);
                MSizeU = Convert.ToInt32(BHAInfo[1]);
                DSCSize = Convert.ToDecimal(BHAInfo[2]);
                DSCSizeU = Convert.ToInt32(BHAInfo[3]);
                MDBLen = Convert.ToDecimal(BHAInfo[4]);
                MDBU = Convert.ToInt32(BHAInfo[5]);
                NMSpAbv = Convert.ToDecimal(BHAInfo[6]);
                NMSpAbvU = Convert.ToInt32(BHAInfo[7]);
                NMSpBlw = Convert.ToDecimal(BHAInfo[8]);
                NMSpBlwU = Convert.ToInt32(BHAInfo[9]);
                NMSpAlw = OPR.getAllowableSpace(convPTInc, convPTAzm);
                MGValue = Convert.ToDecimal(BHAInfo[11]);
                MGVU = Convert.ToInt32(BHAInfo[12]);
                DGTop = Convert.ToDecimal(BHAInfo[13]);
                DGTU = Convert.ToInt32(BHAInfo[14]);
                DSGValue = Convert.ToDecimal(BHAInfo[15]);
                DSGU = Convert.ToInt32(BHAInfo[16]);
                DGBtm = Convert.ToDecimal(BHAInfo[17]);
                DGBTU = Convert.ToInt32(BHAInfo[18]);

                convPTInc = QC.calConvProposedInclination(rInc, rTInc);
                convRTAzm = QC.calConvRunTargetAzimuth(rAzm, LatiNS, mDec, mdecCID, gconv, gcoID);
                convRTAzmIFR = QC.calConvRunTargetAzimuthIFR(rAzm, LatiNS, imDec, imdecCID, gconv, gcoID);
                convPTAzm = QC.calConvProposedAzimuth(ifrVal, LatiNS, mDec, imDec, mdecCID, imdecCID, gconv, gcoID, rTAzm);
                Decimal cnvDip = -99.00M, cnvBT = -99.00M, cnvGT = -99.000000000M, cnvIFRGT = -99.00000000M, cnvIFRDip = -99.000000M, cnvIFRBT = -99.000000M;
                Decimal cnvMSize = -99.00M, cnvSQCBTotal = -99.000000M, cnvSQCGTotal = -99.000000000M, cnvNMSpAbv = -99.00M, cnvNMSpBlw = -99.00M, cnvMDBLen = -99.00M;
                Decimal cnvMGValue = -99.00000000M, cnvDSSize = -99.00M, cnvDSGValue = -99.00000000M;
                Decimal cnvDGTop = -99.00M; //Distance of GMeter from Top of Motor
                Decimal cnvDGBtm = -99.00M; //Distance of GMeter from Drill String/Collar                                                                


                foreach (DataRow row in RawDataTable.Rows)
                {
                    sID = Convert.ToInt32(row["drID"]);
                    //sID = Convert.ToInt32(row["srvRID"]);
                    srvIDValues.Add((Double)sID);
                    rDepth = Convert.ToDecimal(row["Depth"]);
                    LenU = Convert.ToInt32(row["LenU"]);
                    rBx = Convert.ToDecimal(row["Bx"]);
                    bxValues.Add((Double)rBx);
                    rBy = Convert.ToDecimal(row["By"]);
                    byValues.Add((Double)rBy);
                    rBz = Convert.ToDecimal(row["Bz"]);
                    bzValues.Add((Double)rBz);
                    rGx = Convert.ToDecimal(row["Gx"]);
                    rGy = Convert.ToDecimal(row["Gy"]);
                    rGz = Convert.ToDecimal(row["Gz"]);
                    AccelU = Convert.ToInt32(row["acU"]);
                    MagU = Convert.ToInt32(row["magU"]);

                    cnvDip = Units.convDip(dip);
                    cnvBT = Units.convBTotal(bTotal, MagU);
                    cnvIFRDip = Units.convDip(idip);
                    cnvIFRBT = Units.convBTotal(iBTotal, MagU);
                    cnvGT = Units.convGTotal(gTotal, AccelU);
                    cnvIFRGT = Units.convGTotal(iGTotal, AccelU);
                    cnvMSize = Units.convMotorSize(MSize, MSizeU);
                    cnvSQCBTotal = Units.convBTotal(bTotal, MagU);
                    cnvSQCGTotal = Units.convGTotal(gTotal, AccelU);
                    cnvMDBLen = Units.convMDLen(MDBLen, MDBU);
                    cnvNMSpAbv = Units.convNMSp(NMSpAbv, NMSpAbvU);
                    cnvNMSpBlw = Units.convNMSp(NMSpBlw, NMSpBlwU);
                    cnvMGValue = Units.convGMeter(MGValue, MGVU);
                    cnvDSSize = Units.convDSCSize(DSCSize, DSCSizeU);
                    cnvDSGValue = Units.convGMeter(DSGValue, DSGU);
                    cnvDGTop = Units.convGMtrDistance(DGTop, DGTU);
                    cnvDGBtm = Units.convGMtrDistance(DGBtm, DGBTU);


                    valGTF = QC.getGTF(rGx, rGy); // GTF
                    valMTF = QC.colQ(rBx, rBy); //MTF
                    valDeclination = QC.colS(ifrVal, imDec, mDec); //Delta Declination
                    valInclination = QC.colT(rGx, rGy, rGz); //Inclination
                    valCF = QC.colCF(rGx, rGy, rGz); // G-Total
                    valCG = QC.colCG(ifrVal, valCF, gTotal, iGTotal); //Delta G-Total
                    valMagAzimuth = QC.colY(valGTF, valInclination, rBx, rBy, rBz); // LCAZ Magnetic Azimuth
                    plnGridAzimuth = QC.colZ(valMagAzimuth, mDec, gconv, LatiNS, mdecCID, gcoID); // Plain Grid Azimuth
                    ifrGridAzimuth = QC.colAA(valMagAzimuth, imDec, gconv, LatiNS, imdecCID, gcoID); // IFR Grid Azimuth
                    plnTrueAzimuth = QC.colAB(valMagAzimuth, mDec, mdecCID); // Plain True Azimuth
                    ifrTrueAzimuth = QC.colAC(valMagAzimuth, imDec, imdecCID); //IFR True Azimuth
                    gridAzimuth = QC.colAD(ifrVal, plnGridAzimuth, ifrGridAzimuth); // LCAZ Grid Azimuth value
                    trueAzimuth = QC.colAE(ifrVal, plnTrueAzimuth, ifrTrueAzimuth); // LCAZ True Azimuth value
                    finalAzimuth = QC.colAF(jConverg, gridAzimuth, trueAzimuth); // final Azimuth
                    
                    valGoxy = QC.colCE(rGx, rGy); //Goxy
                    valBoxy = QC.colBZ(rBx, rBy); //Boxy
                    valGTotal = QC.colCF(rGx, rGy, rGz); //G-Total
                    valBTotal = QC.colCA(rBx, rBy, rBz); //B-Total
                    valCD = QC.colCD(ifrVal, cnvBT, cnvIFRBT, valBTotal); //Delta B-Total Long Collar
                    valDip = QC.colBA(rGx, rGy, rGz, rBx, rBy, rBz, valGTotal, valBTotal); // Long Collar Dip                    
                    refGx = QC.colBF(ifrVal, valGTF, valInclination, gTotal, iGTotal); // Ref. Gx
                    refGy = QC.colBG(ifrVal, valGTF, valInclination, gTotal, iGTotal); // Ref. Gy
                    refGz = QC.colBH(ifrVal, valInclination, gTotal, iGTotal); // Ref. Gz
                    refBx = QC.colBI(ifrVal, cnvBT, cnvIFRBT, cnvDip, cnvIFRDip, valMagAzimuth, valGTF, valInclination); // Ref. Bx
                    biValues.Add((Double)refBx);
                    refBy = QC.colBJ(ifrVal, cnvDip, cnvBT, cnvIFRDip, cnvIFRBT, valGTF, valInclination, valMagAzimuth); // Ref. By
                    bjValues.Add((Double)refBy);
                    refBz = QC.colBK(ifrVal, cnvDip, cnvBT, cnvIFRDip, cnvIFRBT, valInclination, valMagAzimuth); // Ref. Bz
                    valCK = QC.colCK(ifrVal, rBz, valInclination, valMagAzimuth, refBz, cnvDip, cnvIFRDip, cnvBT, cnvIFRBT);  // Delta Dip
                    valCL = QC.colCL(ifrVal, valInclination, valMagAzimuth, valCK, cnvDip, cnvIFRDip); // dAz1
                    valCM = QC.colCM(valMagAzimuth, valCL); // Corrected Azimuth
                    valCN = QC.colCN(ifrVal, valInclination, valCM, cnvDip, cnvIFRDip, cnvBT, cnvIFRBT); // Bzo
                    cnValues.Add((Double)valCN);
                    valBO = QC.colBO(ServiceID, sID, WellID, WellSectionID, RunID, rBx, bxValues, refBx, biValues, ClientDBConnection); //Bx Scale Factor
                    valBP = QC.colBP(ServiceID, sID, WellID, WellSectionID, RunID, rBy, byValues, refBy, bjValues, ClientDBConnection); //By Scale Factor      
                    valCS = QC.colCS(rBz, valCN); //Delta Bz
                    valFX = QC.colFX(cnvDSSize);  //Drill Collar Pole Strength
                    valFW = QC.colFW(cnvMSize); //Motor Pole Strength
                    valFZ = QC.colFZ(cnvNMSpAbv, valFX); //Bp Drill Collar
                    valGA = QC.colGA(cnvNMSpBlw, cnvMDBLen, valFW); //Bp-Motor - Motor Interference                                                                                                    
                    valGB = QC.colGB(valFZ, valGA); // BHA Interference (nT)
                    gbList.Add(valGB);
                    ThreeSigmaVal = QC.ThreeSigmaBValue(gbList);
                    TSBias = QC.biasCheck(ServiceID, sID, WellID, WellSectionID, RunID, valGB, gbList, ClientDBConnection); //bias Check
                    valHT = QC.colHT(rBx, rBy, rBz, refBx, refBy, valCN, valCS, TSBias); // Delta Bz 
                    htList.Add(valHT);
                    htValues.Add((Double)valHT);
                    valCT = QC.colCT(ServiceID, sID, srvIDValues, WellID, WellSectionID, RunID, valHT, htValues, ClientDBConnection); //Delta Bz Trend
                    valCU = QC.colCU(rBz, valCT); //Bz - Trend Corrected                                        
                    valHQ = QC.colHQ(rBx, rBy, rBz, refBx, refBy, valCN, TSBias); // Bx-Bias (Conditional)                                                            
                    hqValues.Add((Double)valHQ);
                    valHR = QC.colHR(rBx, rBy, rBz, refBx, refBy, valCN, TSBias); // By-Bias (Conditional)
                    hrValues.Add((Double)valHR);
                    valBzBias = QC.bzBias(ServiceID, sID, RunID, WellSectionID, WellID, rBx, rBz, rBz, bzValues, refBx, refBy, valCN, valHQ, TSBias, ClientDBConnection);
                    valBzBiasConditional = QC.bzBias_Conditional(rBx, rBy, rBz, refBx, refBy, valCN, TSBias);
                    valBzScaleFactor = QC.bzSF(ServiceID, sID, WellID, WellSectionID, RunID, rBz, bzValues, valCN, cnValues, ClientDBConnection);
                    valBzCorrected = QC.correctedBz(rBz, valBzScaleFactor, valBzBias);
                    valBM = QC.colBM(sID, rBx, rBy, rBz, refBx, refBy, valCN, ThreeSigmaVal, valHQ, biValues, bjValues, hqValues);
                    valBN = QC.colBN(sID, rBx, rBy, rBz, refBx, refBy, valCN, ThreeSigmaVal, valHR, biValues, bjValues, hrValues);
                    valBQ = QC.colBQ(rBx, valBO, valBM); //Bx Corrected                                        
                    valBR = QC.colBR(rBy, valBP, valBN); //By Corrected        
                    valBU = QC.colBU(valBQ, valBR, valBzCorrected); //Bt Corrected                    
                    valBV = QC.colBV(rGx, rGy, rGz, valBQ, valBR, valBzCorrected, valCF, valBU); //Dip Corrected
                    valBX = QC.colBX(ifrVal, cnvDip, cnvIFRDip, valBV); //Delta Dip Corrected                    
                    valBW = QC.colBW(ifrVal, cnvBT, cnvIFRBT, valBU); //Delta B-Total Corrected
                    valDeltaBzCorrected = QC.deltaBzCorrected(rBz, valBzCorrected);
                    valBTotalCorrected = QC.crrBTotalCorrected(valBQ, valBR, valBzCorrected);
                    valDeltaBTotalCorrect = QC.crrDeltaBTotal(ifrVal, cnvBT, cnvIFRBT, valBTotalCorrected);
                    valDipCorrected = QC.crrDipCorrected(rGx, rGy, rGz, valBQ, valBR, valBzCorrected, gTotal, valBTotalCorrected);
                    valDeltaDipCorrected = QC.crrDeltaDip(ifrVal, cnvDip, cnvIFRDip, valDipCorrected);
                    if ((valDeltaDipCorrected >= (-1 * idip)) && (valDeltaDipCorrected <= idip)) { dipMet = "Green"; } else { dipMet = "Yellow"; }
                    if ((valDeltaBTotalCorrect >= (-1 * iBTotal)) && (valDeltaBTotalCorrect <= iBTotal)) { btMet = "Green"; } else { btMet = "Yellow"; }
                    if ((valCG >= (-1 * iGTotal)) && (valCG <= iGTotal)) { gtMet = "Green"; } else { gtMet = "Yellow"; }
                    iReply.Rows.Add(sID, rDepth, rGx, rGy, rGz, valGoxy, valGTotal, valCG, rBx, rBy, rBz, valBoxy, valBTotal, valDeltaBTotalCorrect, valDeltaDipCorrected, dipMet, btMet, gtMet);
                    iReply.AcceptChanges();
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static DataTable getRawFormattedData(Int32 JobID, Int32 RunID, ArrayList SelectedData, String ClientDBConnection)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn rD = iReply.Columns.Add("rD", typeof(Decimal));
                DataColumn rGx = iReply.Columns.Add("rGx", typeof(Decimal));
                DataColumn rGy = iReply.Columns.Add("rGy", typeof(Decimal));
                DataColumn rGz = iReply.Columns.Add("rGz", typeof(Decimal));
                DataColumn rBx = iReply.Columns.Add("rBx", typeof(Decimal));
                DataColumn rBy = iReply.Columns.Add("rBy", typeof(Decimal));
                DataColumn rBz = iReply.Columns.Add("rBz", typeof(Decimal));
                DataColumn rSID = iReply.Columns.Add("rSID", typeof(Int32));
                Decimal rawDepth, rawGx, rawGy, rawGz, rawBx, rawBy, rawBz;
                Int32 rawSrvyRowID = -99;
                String selData = "SELECT [crrDepth], [crrGx], [crrGy], [crrGz], [crrBx], [crrBy], [crrBz], [srvyRowID] FROM [CorrectionFormattedData] WHERE [jobID] = @jobID AND [runID] = @runID AND [srvyRowID] = @srvyRowID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        foreach (Int32 selID in SelectedData)
                        {
                            SqlCommand getData = new SqlCommand(selData, dataCon);
                            getData.Parameters.AddWithValue("@jobID", SqlDbType.Int).Value = JobID.ToString();
                            getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                            getData.Parameters.AddWithValue("@srvyRowID", SqlDbType.Int).Value = selID.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            rawDepth = readData.GetDecimal(0);
                                            rawGx = readData.GetDecimal(1);
                                            rawGy = readData.GetDecimal(2);
                                            rawGz = readData.GetDecimal(3);
                                            rawBx = readData.GetDecimal(4);
                                            rawBy = readData.GetDecimal(5);
                                            rawBz = readData.GetDecimal(6);
                                            rawSrvyRowID = readData.GetInt32(7);
                                            iReply.Rows.Add(rawDepth, rawGx, rawGy, rawGz, rawBx, rawBy, rawBz, rawSrvyRowID);
                                            iReply.AcceptChanges();
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<String> getSolution(Int32 JobID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Int32 IterationCount, List<Decimal> selectedData, String ClientDBConnection)
        {
            try
            {
                List<String> iReply = new List<String>();
                Decimal xBias = -99.00000000M, yBias = -99.00000000M, zBias = -99.00000000M, xSF = -99.00M, ySF = -99.00M;
                Double[] srID = getSurveyRowIDArray(JobID, WellID, WellSectionID, RunID, selectedData, ClientDBConnection);
                Double[] btotal = getBTotalArray(JobID, WellID, WellSectionID, RunID, selectedData, ClientDBConnection);
                Double upperVal, lowerVal;
                upperVal = Convert.ToDouble(btotal.Max());
                lowerVal = Convert.ToDouble(btotal.Min());

                if (srID.Length != btotal.Length)
                {
                    iReply.Add("NaN");
                }
                else
                {
                    var coeff = MathNet.Numerics.Fit.Polynomial(srID, btotal, 5, MathNet.Numerics.LinearRegression.DirectRegressionMethod.QR);
                    Console.WriteLine("Test - QR");
                    for (int i = 0; i < coeff.Length; i++)
                    {
                        iReply.Add(i.ToString() + "     " + coeff[i].ToString());
                    }

                    Func<Double, Double> f1 = x => ( x * x * x * x * x * x ) + ( x * x * x * x * x ) + ( x * x * x * x ) + ( x * x * x ) + ( x * x ) + x;
                    Func<Double, Double> df1 = x => ( x * x * x * x * x ) + ( x * x * x * x ) + ( x * x * x ) + ( x * x ) + x + 1;
                    iReply.Add(RMN.FindRoot(f1, df1, upperVal, lowerVal, 1e-8, 100, 20).ToString());
                }
                //List<Tuple<Double, Double>> valList = getBTotalFunction(JobID, WellID, WellSectionID, RunID, selectedData, ClientDBConnection);
                //Func<Double, Double> fD = MathNet.Numerics.Differentiate.FirstDerivative(valList);
                
                
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Double[] getBTotalArray(Int32 JobID, Int32 WellID, Int32 WellSectionID, Int32 RunID, List<Decimal> SelectedDepths, String ClientDBConnection)
        {
            try
            {
                Double[] iReply = new Double[] { };
                String selData = "SELECT [valBTotal] FROM [CorrectionInputToAnalysis] WHERE [jobID] = @jobID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID AND [ciaDepth] = @ciaDepth";
                Double btVal = 0;
                List<Double> valList = new List<Double>();
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection.ToString()))
                {
                    try
                    {
                        dataCon.Open();                        
                        foreach (Decimal dph in SelectedDepths)
                        {
                            SqlCommand getData = new SqlCommand(selData, dataCon);
                            getData.Parameters.AddWithValue("@jobID", SqlDbType.Int).Value = JobID.ToString();
                            getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                            getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                            getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                            getData.Parameters.AddWithValue("@ciaDepth", SqlDbType.Int).Value = dph.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            btVal = Convert.ToDouble(readData.GetDecimal(0));
                                            valList.Add(btVal);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply = valList.ToArray();
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Double[] getSurveyRowIDArray(Int32 JobID, Int32 WellID, Int32 WellSectionID, Int32 RunID, List<Decimal> SelectedDepths, String ClientDBConnection)
        {
            try
            {
                Double[] iReply = new Double[]{};
                String selData = "SELECT [srvyRowID], [valCD] FROM [CorrectionInputToAnalysis] WHERE [jobID] = @jobID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID AND [ciaDepth] = @ciaDepth";
                Double rID = 0;
                List<Double> valList = new List<Double>();
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection.ToString()))
                {
                    try
                    {
                        dataCon.Open();                        
                        foreach (Decimal dph in SelectedDepths)
                        {
                            SqlCommand getData = new SqlCommand(selData, dataCon);
                            getData.Parameters.AddWithValue("@jobID", SqlDbType.Int).Value = JobID.ToString();
                            getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                            getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                            getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                            getData.Parameters.AddWithValue("@ciaDepth", SqlDbType.Int).Value = dph.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            rID = Convert.ToDouble(readData.GetInt32(0));
                                            valList.Add(rID);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply = valList.ToArray();
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertCorrectionInputToAnalysis(Int32 srvyRowID, Decimal ciaDepth, Decimal ciaBx, Decimal ciaBy, Decimal ciaBz, Decimal ciaGx, Decimal ciaGy, Decimal ciaGz, Decimal valGTF, Decimal valMTF, Decimal valDec, Decimal valInc, Decimal valCD, Decimal valCF, Decimal valCG, Decimal valMagAzm, Decimal valPlnGridAzm, Decimal valIfrGridAzm, Decimal valPlnTrueAzm, Decimal valIfrTrueAzm, Decimal valPlnAzm, Decimal valIfrAzm, Decimal valFinalAzm, Decimal valGoxy, Decimal valBoxy, Decimal valGTotal, Decimal valBTotal, Decimal valDip, Decimal valRefGx, Decimal valRefGy, Decimal valRefGz, Decimal valRefBx, Decimal valRefBy, Decimal valRefBz, Decimal valCK, Decimal valCL, Decimal valCM, Decimal valCN, Decimal valFX, Decimal valFW, Decimal valFZ, Decimal valGA, Decimal valGB, Decimal TSBias, Decimal valHQ, Decimal valBM, Decimal valBO, Decimal valBP, Decimal valHR, Decimal valBN, Decimal valBQ, Decimal valBR, Decimal valCS, Decimal valHT, Decimal valCT, Decimal valCU, Decimal valBU, Decimal valBV, Decimal valBW, Decimal valBX, Decimal valBzBias, Decimal valBzBiasConditional, Decimal valBzScaleFactor, Decimal valBzCorrected, Decimal valDeltaBzCorrected, Decimal valBTotalCorrected, Decimal valDeltaBTotalCorrect, Decimal valDipCorrected, Decimal valDeltaDipCorrected, Int32 RunID, Int32 WellSectionID, Int32 WellID, Int32 WellPadID, Int32 ClientID, Int32 OperatorID, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String insertCommand = "INSERT INTO [CorrectionInputToAnalysis] ([srvyRowID], [ciaDepth], [ciaBx], [ciaBy], [ciaBz], [ciaGx], [ciaGy], [ciaGz], [valGTF], [valMTF], [valDec], [valInc], [valCD], [valCF], [valCG], [valMagAzm], [valPlnGridAzm], [valIfrGridAzm], [valPlnTrueAzm], [valIfrTrueAzm], [valPlnAzm], [valIfrAzm], [valFinalAzm], [valGoxy], [valBoxy], [valGTotal], [valBTotal], [valDip], [valRefGx], [valRefGy], [valRefGz], [valRefBx], [valRefBy], [valRefBz], [valCK], [valCL], [valCM], [valCN], [valFX], [valFW], [valFZ], [valGA], [valGB], [TSBias], [valHQ], [valBM], [valBO], [valBP], [valHR], [valBN], [valBQ], [valBR], [valCS], [valHT], [valCT], [valCU], [valBU], [valBV], [valBW], [valBX], [valBzBias], [valBzBiasConditional], [valBzScaleFactor], [valBzCorrected], [valDeltaBzCorrected], [valBTotalCorrected], [valDeltaBTotalCorrect], [valDipCorrected], [valDeltaDipCorrected], [runID], [wswID], [welID], [wpdID], [clntID], [optrID]) VALUES (@srvyRowID, @ciaDepth, @ciaBx, @ciaBy, @ciaBz, @ciaGx, @ciaGy, @ciaGz, @valGTF, @valMTF, @valDec, @valInc, @valCD, @valCF, @valCG, @valMagAzm, @valPlnGridAzm, @valIfrGridAzm, @valPlnTrueAzm, @valIfrTrueAzm, @valPlnAzm, @valIfrAzm, @valFinalAzm, @valGoxy, @valBoxy, @valGTotal, @valBTotal, @valDip, @valRefGx, @valRefGy, @valRefGz, @valRefBx, @valRefBy, @valRefBz, @valCK, @valCL, @valCM, @valCN, @valFX, @valFW, @valFZ, @valGA, @valGB, @TSBias, @valHQ, @valBM, @valBO, @valBP, @valHR, @valBN, @valBQ, @valBR, @valCS, @valHT, @valCT, @valCU, @valBU, @valBV, @valBW, @valBX, @valBzBias, @valBzBiasConditional, @valBzScaleFactor, @valBzCorrected, @valDeltaBzCorrected, @valBTotalCorrected, @valDeltaBTotalCorrect, @valDipCorrected, @valDeltaDipCorrected, @runID, @wswID, @welID, @wpdID, @clntID, @optrID)";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(insertCommand, dataCon);
                        insData.Parameters.AddWithValue("@srvyRowID", SqlDbType.Int).Value = srvyRowID.ToString();
                        insData.Parameters.AddWithValue("@ciaDepth", SqlDbType.Decimal).Value = ciaDepth.ToString();
                        insData.Parameters.AddWithValue("@ciaBx", SqlDbType.Decimal).Value = ciaBx.ToString();
                        insData.Parameters.AddWithValue("@ciaBy", SqlDbType.Decimal).Value = ciaBy.ToString();
                        insData.Parameters.AddWithValue("@ciaBz", SqlDbType.Decimal).Value = ciaBz.ToString();
                        insData.Parameters.AddWithValue("@ciaGx", SqlDbType.Decimal).Value = ciaGx.ToString();
                        insData.Parameters.AddWithValue("@ciaGy", SqlDbType.Decimal).Value = ciaGy.ToString();
                        insData.Parameters.AddWithValue("@ciaGz", SqlDbType.Decimal).Value = ciaGz.ToString();
                        insData.Parameters.AddWithValue("@valGTF", SqlDbType.Decimal).Value = valGTF.ToString();
                        insData.Parameters.AddWithValue("@valMTF", SqlDbType.Decimal).Value = valMTF.ToString();
                        insData.Parameters.AddWithValue("@valDec", SqlDbType.Decimal).Value = valDec.ToString();
                        insData.Parameters.AddWithValue("@valInc", SqlDbType.Decimal).Value = valInc.ToString();
                        insData.Parameters.AddWithValue("@valCD", SqlDbType.Decimal).Value = valCD.ToString();
                        insData.Parameters.AddWithValue("@valCF", SqlDbType.Decimal).Value = valCF.ToString();
                        insData.Parameters.AddWithValue("@valCG", SqlDbType.Decimal).Value = valCG.ToString();
                        insData.Parameters.AddWithValue("@valMagAzm", SqlDbType.Decimal).Value = valMagAzm.ToString();
                        insData.Parameters.AddWithValue("@valPlnGridAzm", SqlDbType.Decimal).Value = valPlnGridAzm.ToString();
                        insData.Parameters.AddWithValue("@valIfrGridAzm", SqlDbType.Decimal).Value = valIfrGridAzm.ToString();
                        insData.Parameters.AddWithValue("@valPlnTrueAzm", SqlDbType.Decimal).Value = valPlnTrueAzm.ToString();
                        insData.Parameters.AddWithValue("@valIfrTrueAzm", SqlDbType.Decimal).Value = valIfrTrueAzm.ToString();
                        insData.Parameters.AddWithValue("@valPlnAzm", SqlDbType.Decimal).Value = valPlnAzm.ToString();
                        insData.Parameters.AddWithValue("@valIfrAzm", SqlDbType.Decimal).Value = valIfrAzm.ToString();
                        insData.Parameters.AddWithValue("@valFinalAzm", SqlDbType.Decimal).Value = valFinalAzm.ToString();
                        insData.Parameters.AddWithValue("@valGoxy", SqlDbType.Decimal).Value = valGoxy.ToString();
                        insData.Parameters.AddWithValue("@valBoxy", SqlDbType.Decimal).Value = valBoxy.ToString();
                        insData.Parameters.AddWithValue("@valGTotal", SqlDbType.Decimal).Value = valGTotal.ToString();
                        insData.Parameters.AddWithValue("@valBTotal", SqlDbType.Decimal).Value = valBTotal.ToString();
                        insData.Parameters.AddWithValue("@valDip", SqlDbType.Decimal).Value = valDip.ToString();
                        insData.Parameters.AddWithValue("@valRefGx", SqlDbType.Decimal).Value = valRefGx.ToString();
                        insData.Parameters.AddWithValue("@valRefGy", SqlDbType.Decimal).Value = valRefGy.ToString();
                        insData.Parameters.AddWithValue("@valRefGz", SqlDbType.Decimal).Value = valRefGz.ToString();
                        insData.Parameters.AddWithValue("@valRefBx", SqlDbType.Decimal).Value = valRefBx.ToString();
                        insData.Parameters.AddWithValue("@valRefBy", SqlDbType.Decimal).Value = valRefBy.ToString();
                        insData.Parameters.AddWithValue("@valRefBz", SqlDbType.Decimal).Value = valRefBz.ToString();
                        insData.Parameters.AddWithValue("@valCK", SqlDbType.Decimal).Value = valCK.ToString();
                        insData.Parameters.AddWithValue("@valCL", SqlDbType.Decimal).Value = valCL.ToString();
                        insData.Parameters.AddWithValue("@valCM", SqlDbType.Decimal).Value = valCM.ToString();
                        insData.Parameters.AddWithValue("@valCN", SqlDbType.Decimal).Value = valCN.ToString();
                        insData.Parameters.AddWithValue("@valFX", SqlDbType.Decimal).Value = valFX.ToString();
                        insData.Parameters.AddWithValue("@valFW", SqlDbType.Decimal).Value = valFW.ToString();
                        insData.Parameters.AddWithValue("@valFZ", SqlDbType.Decimal).Value = valFZ.ToString();
                        insData.Parameters.AddWithValue("@valGA", SqlDbType.Decimal).Value = valGA.ToString();
                        insData.Parameters.AddWithValue("@valGB", SqlDbType.Decimal).Value = valGB.ToString();
                        insData.Parameters.AddWithValue("@TSBias", SqlDbType.Decimal).Value = TSBias.ToString();
                        insData.Parameters.AddWithValue("@valHQ", SqlDbType.Decimal).Value = valHQ.ToString();
                        insData.Parameters.AddWithValue("@valBM", SqlDbType.Decimal).Value = valBM.ToString();
                        insData.Parameters.AddWithValue("@valBO", SqlDbType.Decimal).Value = valBO.ToString();
                        insData.Parameters.AddWithValue("@valBP", SqlDbType.Decimal).Value = valBP.ToString();
                        insData.Parameters.AddWithValue("@valHR", SqlDbType.Decimal).Value = valHR.ToString();
                        insData.Parameters.AddWithValue("@valBN", SqlDbType.Decimal).Value = valBN.ToString();
                        insData.Parameters.AddWithValue("@valBQ", SqlDbType.Decimal).Value = valBQ.ToString();
                        insData.Parameters.AddWithValue("@valBR", SqlDbType.Decimal).Value = valBR.ToString();
                        insData.Parameters.AddWithValue("@valCS", SqlDbType.Decimal).Value = valCS.ToString();
                        insData.Parameters.AddWithValue("@valHT", SqlDbType.Decimal).Value = valHT.ToString();
                        insData.Parameters.AddWithValue("@valCT", SqlDbType.Decimal).Value = valCT.ToString();
                        insData.Parameters.AddWithValue("@valCU", SqlDbType.Decimal).Value = valCU.ToString();
                        insData.Parameters.AddWithValue("@valBU", SqlDbType.Decimal).Value = valBU.ToString();
                        insData.Parameters.AddWithValue("@valBV", SqlDbType.Decimal).Value = valBV.ToString();
                        insData.Parameters.AddWithValue("@valBW", SqlDbType.Decimal).Value = valBW.ToString();
                        insData.Parameters.AddWithValue("@valBX", SqlDbType.Decimal).Value = valBX.ToString();
                        insData.Parameters.AddWithValue("@valBzBias", SqlDbType.Decimal).Value = valBzBias.ToString();
                        insData.Parameters.AddWithValue("@valBzBiasConditional", SqlDbType.Decimal).Value = valBzBiasConditional.ToString();
                        insData.Parameters.AddWithValue("@valBzScaleFactor", SqlDbType.Decimal).Value = valBzScaleFactor.ToString();
                        insData.Parameters.AddWithValue("@valBzCorrected", SqlDbType.Decimal).Value = valBzCorrected.ToString();
                        insData.Parameters.AddWithValue("@valDeltaBzCorrected", SqlDbType.Decimal).Value = valDeltaBzCorrected.ToString();
                        insData.Parameters.AddWithValue("@valBTotalCorrected", SqlDbType.Decimal).Value = valBTotalCorrected.ToString();
                        insData.Parameters.AddWithValue("@valDeltaBTotalCorrect", SqlDbType.Decimal).Value = valDeltaBTotalCorrect.ToString();
                        insData.Parameters.AddWithValue("@valDipCorrected", SqlDbType.Decimal).Value = valDipCorrected.ToString();
                        insData.Parameters.AddWithValue("@valDeltaDipCorrected", SqlDbType.Decimal).Value = valDeltaDipCorrected.ToString();
                        insData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        insData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        insData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        insData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        insData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        insData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getCorrectionInputToAnalysisTable(Int32 JobID, Int32 WellID, Int32 WellSectionID, Int32 RunID, List<KeyValuePair<String, Decimal>> MagRef, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn ciaID = iReply.Columns.Add("ciaID", typeof(Int32));
                DataColumn srvyRowID = iReply.Columns.Add("srvyRowID", typeof(Int32));
                DataColumn ciaDepth = iReply.Columns.Add("ciaDepth", typeof(Decimal));
                DataColumn ciaBx = iReply.Columns.Add("ciaBx", typeof(Decimal));
                DataColumn ciaBy = iReply.Columns.Add("ciaBy", typeof(Decimal));
                DataColumn ciaBz = iReply.Columns.Add("ciaBz", typeof(Decimal));
                DataColumn ciaGx = iReply.Columns.Add("ciaGx", typeof(Decimal));
                DataColumn ciaGy = iReply.Columns.Add("ciaGy", typeof(Decimal));
                DataColumn ciaGz = iReply.Columns.Add("ciaGz", typeof(Decimal));
                DataColumn refF = iReply.Columns.Add("refF", typeof(Decimal));
                DataColumn refDec = iReply.Columns.Add("refDec", typeof(Decimal));
                DataColumn refDip = iReply.Columns.Add("refDip", typeof(Decimal));
                DataColumn delBT = iReply.Columns.Add("delBT", typeof(Decimal));
                DataColumn delDip = iReply.Columns.Add("delDip", typeof(Decimal));
                DataColumn delGT = iReply.Columns.Add("delGT", typeof(Decimal));
                DataColumn metDipSQC = iReply.Columns.Add("metDipSQC", typeof(Int32));
                DataColumn metBTSQC = iReply.Columns.Add("metBTSQC", typeof(Int32));
                DataColumn metGTSQC = iReply.Columns.Add("metGTSQC", typeof(Int32));
                String selData = "SELECT [ciaID], [srvyRowID], [ciaDepth], [ciaBx], [ciaBy], [ciaBz], [ciaGx], [ciaGy], [ciaGz], [valCD], [valCK], [valCG] FROM [CorrectionInputToAnalysis] WHERE [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                Int32 id = -99, srvID = -99, sqc = -99, lwrSQCBT = -99, uprSQCBT = -99, sqcDip = -99, sqcBT = -99, sqcGT = -99;
                Decimal lwrSQCDip = -99.00M, uprSQCDip = -99.00M, lwrSQCGT = -99.000M, uprSQCGT = -99.000M;
                Decimal depth = -99.00M, bx = -99.000000000M, by = -99.000000000M, bz = -99.000000000M, gx = -99.000000000M, gy = -99.000000000M, gz = -99.000000000M;
                Decimal dBT = -99.000000000M, dDip = -99.000000000M, dGT = -99.000000000M;
                Decimal rBT = -99.000000000M, rDip = -99.000000000M, rDec = -99.000000000M, rGT = -99.000000000M;
                foreach (KeyValuePair<String, Decimal> pair in MagRef)
                {
                    String name = pair.Key;
                    Decimal value = pair.Value;
                    if (name.Equals("Dip"))
                    {
                        rDip = value;
                    }
                    else if (name.Equals("MDec"))
                    {
                        rDec = value;
                    }
                    else if (name.Equals("BT"))
                    {
                        rBT = value;
                    }
                    else
                    {
                        rGT = value;
                    }
                }
                ArrayList csqc = new ArrayList();
                csqc = DMG.getCorrSQCList();
                lwrSQCBT = Convert.ToInt32(csqc[1]);
                lwrSQCDip = Convert.ToDecimal(csqc[2]);
                lwrSQCGT = Convert.ToDecimal(csqc[3]);
                uprSQCBT = Convert.ToInt32(csqc[6]);
                uprSQCDip = Convert.ToDecimal(csqc[7]);
                uprSQCGT = Convert.ToDecimal(csqc[8]);

                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        srvID = readData.GetInt32(1);
                                        depth = readData.GetDecimal(2);
                                        bx = readData.GetDecimal(3);
                                        by = readData.GetDecimal(4);
                                        bz = readData.GetDecimal(5);
                                        gx = readData.GetDecimal(6);
                                        gy = readData.GetDecimal(7);
                                        gz = readData.GetDecimal(8);
                                        dBT = readData.GetDecimal(9);
                                        dDip = readData.GetDecimal(10);
                                        dGT = readData.GetDecimal(11);
                                        sqcDip = getDipSQC(lwrSQCDip, uprSQCDip, dDip);
                                        sqcBT = getBTSQC(lwrSQCBT, uprSQCBT, Convert.ToInt32(dBT));
                                        sqcGT = getGTSQC(lwrSQCGT, uprSQCGT, dGT);
                                        //sqc = getSQCMet(lwrSQCBT, lwrSQCDip, lwrSQCGT, uprSQCBT, uprSQCDip, uprSQCGT, Convert.ToInt32(dBT), dDip, dGT);
                                        iReply.Rows.Add(id, srvID, depth, bx, by, bz, gx, gy, gz, rBT, rDec, rDip, dBT, dDip, dGT, sqcDip, sqcBT, sqcGT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getDataTableforCorrAnalysisGraph(Int32 WellID, String dbConn)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn Survey_number = iReply.Columns.Add("Survey_number", typeof(Int32));
                DataColumn MDepth = iReply.Columns.Add("MDepth", typeof(Decimal));
                DataColumn DBTotal = iReply.Columns.Add("Delta B-Total", typeof(Decimal));
                DataColumn DDip = iReply.Columns.Add("Delta Dip", typeof(Decimal));
                DataColumn DGTotal = iReply.Columns.Add("Delta G-Total", typeof(Decimal));
                Int32 count = 0;
                Decimal bt = -99.00M, depth = -99.00M, dp = -99.00M, gt = -99.00M;
                String SelectQuery = "Select [srvyRowID], [ciaDepth], [valCD], [valCK], [valCG] FROM [CorrectionInputToAnalysis] WHERE [welID] = @welID ORDER BY [ciaDepth] ASC ";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        //count++;
                                        count = readData.GetInt32(0);                                        
                                        depth = readData.GetDecimal(1);
                                        bt = readData.GetDecimal(2);
                                        dp = readData.GetDecimal(3);
                                        gt = readData.GetDecimal(4);
                                        iReply.Rows.Add(count, depth, bt, dp, gt);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close(); // this is used for SQLDataReader
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getVertHorzComponents(Int32 JobID, Int32 WellID, Int32 WellSectionID, Int32 RunID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable(), BGTable = new System.Data.DataTable();
                DataColumn srvID = iReply.Columns.Add("srvID", typeof(Int32));
                DataColumn ciaDepth = iReply.Columns.Add("ciaDepth", typeof(Decimal));
                DataColumn Bv = iReply.Columns.Add("Bv", typeof(Decimal));
                DataColumn Bh = iReply.Columns.Add("Bh", typeof(Decimal));
                Int32 id = -99;
                BGTable = getBiasRemoved(JobID, WellID, WellSectionID, RunID, ClientDBConnection);
                Decimal rDepth = -99.00M, rGx = -99.00M, rGy = -99.00M, rGz = -99.00M, rBxc = -99.00000000M, rByc = -99.00000000M, rBzc = -99.00000000M, rBv = -99.00000000M, rBh = -99.00000000M;
                foreach (DataRow row in BGTable.Rows)
                {
                    id = Convert.ToInt32(row[0]);
                    rDepth = Convert.ToDecimal(row[1]);
                    rGx = Convert.ToDecimal(row[2]);
                    rGy = Convert.ToDecimal(row[3]);
                    rGz = Convert.ToDecimal(row[4]);
                    rBxc = Convert.ToDecimal(row[7]);
                    rByc = Convert.ToDecimal(row[10]);
                    rBzc = Convert.ToDecimal(row[13]);
                    rBv = getVerticalComponent(rGx, rGy, rGz, rBxc, rByc, rBzc);
                    rBh = getHorizontalComponent(rBxc, rByc, rBzc, rBv);
                    iReply.Rows.Add(id, rDepth, rBv, rBh);
                    iReply.AcceptChanges();
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal getVerticalComponent(Decimal Gx, Decimal Gy, Decimal Gz, Decimal Bx, Decimal By, Decimal Bz)
        {
            try
            {
                Decimal iReply = -99.00000000M;
                Decimal gTotal = QC.colCF(Gx, Gy, Gz);
                iReply = ((Bx * Gx) + (By * Gy) + (Bz * Gz)) / Math.Abs(gTotal);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal getHorizontalComponent(Decimal Bx, Decimal By, Decimal Bz, Decimal VerticalComponent)
        {
            try
            {
                Decimal iReply = -99.00000000M;
                iReply = Convert.ToDecimal(Math.Pow((Double)((Bx * Bx) + (By * By) + (Bz * Bz) - (VerticalComponent * VerticalComponent)), 0.5));
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getDataTableforBsGraph(Int32 WellID, String dbConn)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn Survey_number = iReply.Columns.Add("Survey_number", typeof(Int32));
                DataColumn MDepth = iReply.Columns.Add("MDepth", typeof(Decimal));
                DataColumn Bxm = iReply.Columns.Add("Bx(m)", typeof(Decimal));
                DataColumn Bym = iReply.Columns.Add("By(m)", typeof(Decimal));
                DataColumn Bzm = iReply.Columns.Add("Bz(m)", typeof(Decimal));
                DataColumn Bxc = iReply.Columns.Add("Bx(c)", typeof(Decimal));
                DataColumn Byc = iReply.Columns.Add("By(c)", typeof(Decimal));
                DataColumn Bzc = iReply.Columns.Add("Bz(c)", typeof(Decimal));
                Int32 count = 0;
                Decimal depth = -99.00M, bx = -99.00000000M, by = -99.00000000M, bz = -99.00000000M, bxc = -99.00000000M, byc = -99.00000000M, bzc = -99.00000000M;
                String SelectQuery = "Select [srvyRowID], [ciaDepth], [ciaBx], [ciaBy], [ciaBz], [valBQ], [valBR], [valBzCorrected] FROM [CorrectionInputToAnalysis] WHERE [welID] = @welID ORDER BY [ciaDepth] ASC ";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        //count++;
                                        count = readData.GetInt32(0);
                                        depth = readData.GetDecimal(1);
                                        bx = readData.GetDecimal(2);
                                        by = readData.GetDecimal(3);
                                        bz = readData.GetDecimal(4);
                                        bxc = readData.GetDecimal(5);
                                        byc = readData.GetDecimal(6);
                                        bzc = readData.GetDecimal(7);
                                        iReply.Rows.Add(count, depth, bx, by, bz, bxc, byc, bzc);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close(); // this is used for SQLDataReader
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getBiasRemoved(Int32 JobID, Int32 WellID, Int32 SecID, Int32 RunID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn srvID = iReply.Columns.Add("srvID", typeof(Int32));
                DataColumn ciaDepth = iReply.Columns.Add("ciaDepth", typeof(Decimal));
                DataColumn ciaGx = iReply.Columns.Add("ciaGx", typeof(Decimal));
                DataColumn ciaGy = iReply.Columns.Add("ciaGy", typeof(Decimal));
                DataColumn ciaGz = iReply.Columns.Add("ciaGz", typeof(Decimal));
                DataColumn ciaBx = iReply.Columns.Add("ciaBx", typeof(Decimal));
                DataColumn bxBias = iReply.Columns.Add("bxBias", typeof(Decimal));
                DataColumn ciaBxc = iReply.Columns.Add("ciaBxc", typeof(Decimal));
                DataColumn ciaBy = iReply.Columns.Add("ciaBy", typeof(Decimal));
                DataColumn byBias = iReply.Columns.Add("byBias", typeof(Decimal));
                DataColumn ciaByc = iReply.Columns.Add("ciaByc", typeof(Decimal));
                DataColumn ciaBz = iReply.Columns.Add("ciaBz", typeof(Decimal));
                DataColumn bzBias = iReply.Columns.Add("bzBias", typeof(Decimal));
                DataColumn ciaBzc = iReply.Columns.Add("ciaBzc", typeof(Decimal));
                Int32 id = -99;
                Decimal depth = -99.00M, gx = -99.000000000000M, gy = -99.000000000000M, gz = -99.000000000000M, bx = -99.000000000000M, bxB = -99.000000000000M, bxC = -99.000000000000M;
                Decimal by = -99.000000000000M, byB = -99.000000000000M, byC = -99.000000000000M, bz = -99.000000000000M, bzB = -99.000000000000M, bzC = -99.000000000000M;
                String selData = "SELECT [srvyRowID], [ciaDepth], [ciaGx], [ciaGy], [ciaGz], [valBQ], [valBM], [valBR], [valBN], [valBzCorrected], [valBzBias] FROM [CorrectionInputToAnalysis] WHERE [jobID] = @jobID AND [welID ] = @welID AND [wswID] = @wswID AND [runID] = @runID ORDER BY [srvyRowID]";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@jobID", SqlDbType.Int).Value = JobID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = SecID.ToString();
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        depth = readData.GetDecimal(1);
                                        gx = readData.GetDecimal(2);
                                        gy = readData.GetDecimal(3);
                                        gz = readData.GetDecimal(4);
                                        bx = readData.GetDecimal(5);
                                        bxB = readData.GetDecimal(6);
                                        bxC = (bx - bxB);
                                        by = readData.GetDecimal(7);
                                        byB = readData.GetDecimal(8);
                                        byC = (by - byB);
                                        bz = readData.GetDecimal(9);
                                        bzB = readData.GetDecimal(10);
                                        bzC = (bz - bzB);
                                        iReply.Rows.Add(id, depth, gx, gy, gz, bx, bxB, bxC, by, byB, byC, bz, bzB, bzC);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable FindBias(Int32 JobID, Int32 WellID, Int32 SecID, Int32 RunID, String GetClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();

                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getRevVectorAnalysis(Int32 OperatorID, Int32 ClientID, Int32 WellpadID, Int32 WellID, Int32 SectionID, Int32 RunID, System.Data.DataTable dataTable, ArrayList SelectedIFR, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn sID = iReply.Columns.Add("sID", typeof(Int32));
                DataColumn Depth = iReply.Columns.Add("Depth", typeof(Decimal));
                DataColumn rawGx = iReply.Columns.Add("Gx", typeof(Decimal));
                DataColumn calcGx = iReply.Columns.Add("calcGx", typeof(Decimal));
                DataColumn deltaGx = iReply.Columns.Add("deltaGx", typeof(Decimal));
                DataColumn rawGy = iReply.Columns.Add("Gy", typeof(Decimal));
                DataColumn calcGy = iReply.Columns.Add("calcGy", typeof(Decimal));
                DataColumn deltaGy = iReply.Columns.Add("deltaGy", typeof(Decimal));
                DataColumn rawGz = iReply.Columns.Add("Gz", typeof(Decimal));
                DataColumn calcGz = iReply.Columns.Add("calcGz", typeof(Decimal));
                DataColumn deltaGz = iReply.Columns.Add("deltaGz", typeof(Decimal));
                DataColumn rawBx = iReply.Columns.Add("Bx", typeof(Decimal));
                DataColumn calcBx = iReply.Columns.Add("calcBx", typeof(Decimal));
                DataColumn deltaBx = iReply.Columns.Add("deltaBx", typeof(Decimal));
                DataColumn rawBy = iReply.Columns.Add("By", typeof(Decimal));
                DataColumn calcBy = iReply.Columns.Add("calcBy", typeof(Decimal));
                DataColumn deltaBy = iReply.Columns.Add("deltaBy", typeof(Decimal));
                DataColumn rawBz = iReply.Columns.Add("Bz", typeof(Decimal));
                DataColumn calcBz = iReply.Columns.Add("calcBz", typeof(Decimal));
                DataColumn deltaBz = iReply.Columns.Add("deltaBz", typeof(Decimal));
                DataColumn clrGx = iReply.Columns.Add("clrGx", typeof(String));
                DataColumn clrGy = iReply.Columns.Add("clrGy", typeof(String));
                DataColumn clrGz = iReply.Columns.Add("clrGz", typeof(String));
                DataColumn clrBx = iReply.Columns.Add("clrBx", typeof(String));
                DataColumn clrBy = iReply.Columns.Add("clrBy", typeof(String));
                DataColumn clrBz = iReply.Columns.Add("clrBz", typeof(String));
                Decimal refStartD = -99.00M, refEndD = -99.00M, refDip = -99.00M, refDec = -99.00M, refBTotal = -99.00M, refGTotal = -99.00M;
                Int32 refLenU = -99, refDecO = -99, refMagU = -99, refAcelU = -99;
                Decimal cnvRefStartD = -99.00M, cnvRefEndD = -99.00M, cnvRefBTotal = -99.00M, cnvRefGTotal = -99.00M;
                Decimal calcInc = -99.00M, calcDip = -99.00M, calcBTotal = -99.00M, calcGTotal = -99.00M, calcGTF = -99.00M;                
                Int32 rID = -99, jcID = -99; //1 = North, 2 = Grid
                Int32 ifrVal = -99, LngiEW = -99, LatiNS = -99, mdecOID = -99, gcOID = -99;
                Decimal rDepth = -99.00M, rBx = -99.00M, rBy = -99.00M, rBz = -99.00M, rGx = -99.00M, rGy = -99.00M, rGz = -99.00M, mDec = -99.00M, gConv = -99.00M;
                Decimal colAEVal = -99.00M, colAFVal = -99.00M, colYVal = -99.00M, colZVal = -99.00M, colAAVal = -99.00M, colABVal = -99.00M, colACVal = -99.00M, colADVal = -99.00M;
                Decimal cGx = -99.00M, cGy = -99.00M, cGz = -99.00M, cBx = -99.00M, cBy = -99.00M, cBz = -99.00M, delGx = -99.00M, delGy = -99.00M, delGz = -99.00M, delBx = -99.00M, delBy = -99.00M, delBz = -99.00M;
                String colorGx = String.Empty, colorGy = String.Empty, colorGz = String.Empty, colorBx = String.Empty, colorBy = String.Empty, colorBz = String.Empty;
                List<String> WellInfo = AST.getWellDetailList(OperatorID, ClientID, WellID, ClientDBConnection);
                LngiEW = Convert.ToInt32(WellInfo[3]);
                LatiNS = Convert.ToInt32(WellInfo[6]);
                ArrayList RefMags = new ArrayList();
                RefMags = OPR.getRefMagList(OperatorID, ClientID, WellpadID, WellID, ClientDBConnection);
                ifrVal = Convert.ToInt32(RefMags[0]);
                mDec = Convert.ToDecimal(RefMags[1]);
                mdecOID = Convert.ToInt32(RefMags[2]);
                gConv = Convert.ToDecimal(RefMags[8]);
                gcOID = Convert.ToInt32(RefMags[9]);
                jcID = Convert.ToInt32(RefMags[10]);
                foreach (var value in SelectedIFR)
                {
                    refStartD = Convert.ToDecimal(SelectedIFR[0]);
                    refEndD = Convert.ToDecimal(SelectedIFR[1]);
                    refLenU = Convert.ToInt32(SelectedIFR[2]);
                    if (refLenU.Equals(4)) { cnvRefStartD = refStartD / 3.2808M; cnvRefEndD = refEndD / 3.2808M; } else { cnvRefStartD = refStartD; cnvRefEndD = refEndD; }
                    refDip = Convert.ToDecimal(SelectedIFR[3]);
                    refDec = Convert.ToDecimal(SelectedIFR[4]);
                    refDecO = Convert.ToInt32(SelectedIFR[5]);
                    refBTotal = Convert.ToDecimal(SelectedIFR[6]);
                    refMagU = Convert.ToInt32(SelectedIFR[7]);
                    cnvRefBTotal = Units.convBTotal(refBTotal, refMagU);
                    refGTotal = Convert.ToDecimal(SelectedIFR[8]);
                    refAcelU = Convert.ToInt32(SelectedIFR[9]);
                    cnvRefGTotal = Units.convGTotal(refGTotal, refAcelU);
                }
                foreach (DataRow row in dataTable.Rows)
                {
                    rID = Convert.ToInt32(row["sID"]);
                    rDepth = Convert.ToDecimal(row["Depth"]);                    
                    rBx = Convert.ToDecimal(row["Bx"]);
                    rBy = Convert.ToDecimal(row["By"]);
                    rBz = Convert.ToDecimal(row["Bz"]);
                    rGx = Convert.ToDecimal(row["Gx"]);
                    rGy = Convert.ToDecimal(row["Gy"]);
                    rGz = Convert.ToDecimal(row["Gz"]);
                    if (rDepth >= cnvRefStartD || rDepth <= cnvRefEndD)
                    {
                        calcInc = QC.colT(rGx, rGy, rGz);
                        calcBTotal = QC.colCA(rBx, rBy, rBz);
                        calcGTotal = QC.colCF(rGx, rGy, rGz);
                        calcDip = QC.colBA(rGx, rGy, rGz, rBx, rBy, rBz, calcGTotal, calcBTotal);
                        calcGTF = QC.getGTF(rGx, rGy);
                        colYVal = QC.colY(calcGTF, calcInc, rBx, rBy, rBz);
                        colZVal = QC.colZ(colYVal, mDec, gConv, LatiNS, mdecOID, gcOID);
                        colAAVal = QC.colAA(colYVal, mDec, gConv, LatiNS, mdecOID, gcOID);
                        colABVal = QC.colAB(colYVal, mDec, mdecOID);
                        colACVal = QC.colAC(colYVal, mDec, mdecOID);
                        colADVal = QC.colAD(ifrVal, colZVal, colAAVal);
                        colAEVal = QC.colAE(ifrVal, colABVal, colACVal);
                        colAFVal = QC.colAF(jcID, colADVal, colAEVal);
                        cGx = ResDataQC.resGx(calcGTotal, calcInc, calcGTF);
                        cGy = ResDataQC.resGy(calcGTotal, calcInc, calcGTF);
                        cGz = ResDataQC.resGz(calcGTotal, calcInc);
                        cBx = ResDataQC.resBx(calcBTotal, calcDip, calcInc, calcGTF, colADVal);
                        cBy = ResDataQC.resBy(calcBTotal, calcInc, calcDip, calcGTF, colADVal);
                        cBz = ResDataQC.resBz(calcBTotal, calcInc, calcDip, calcGTF, colADVal);
                        delGx = rGx - cGx;
                        if (delGx >= (-1 * 0.003M) || delGx <= 0.003M) { colorGx = "Green"; } else { colorGx = "Yellow"; }
                        delGy = rGy - cGy;
                        if (delGy >= (-1 * 0.003M) || delGy <= 0.003M) { colorGy = "Green"; } else { colorGy = "Yellow"; }
                        delGz = rGz - cGz;
                        if (delGz >= (-1 * 0.003M) || delGz <= 0.003M) { colorGz = "Green"; } else { colorGz = "Yellow"; }
                        delBx = rBx - cBx;
                        if (delBx >= (-1 * 250.00M) || delBx <= 250.00M) { colorBx = "Green"; } else { colorBx = "Yellow"; }
                        delBy = rBy - cBy;
                        if (delBy >= (-1 * 250.00M) || delBy <= 250.00M) { colorBy = "Green"; } else { colorBy = "Yellow"; }
                        delBz = rBz - cBz;
                        if (delBz >= (-1 * 250.00M) || delBz <= 250.00M) { colorBz = "Green"; } else { colorBz = "Yellow"; }
                        iReply.Rows.Add(rID, rDepth, rGx, cGx, delGx, rGy, cGy, delGy, rGz, cGz, delGz, rBx, cBx, delBx, rBy, cBy, delBy, rBz, cBz, delBz, colorGx, colorGy, colorGz, colorBx, colorBy, colorBz);
                        iReply.AcceptChanges();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getBHABzFilter(Int32 OperatorID, Int32 ClientID, Int32 WellpadID, Int32 WellID, Int32 SectionID, Int32 RunID, System.Data.DataTable selBHAFilterTable, ArrayList ifrList, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn srvID = iReply.Columns.Add("srvID", typeof(Int32));
                DataColumn Depth = iReply.Columns.Add("Depth", typeof(Decimal));
                DataColumn Gx = iReply.Columns.Add("Gx", typeof(Decimal));
                DataColumn Gy = iReply.Columns.Add("Gy", typeof(Decimal));
                DataColumn Gz = iReply.Columns.Add("Gz", typeof(Decimal));
                DataColumn Bx = iReply.Columns.Add("Bx", typeof(Decimal));
                DataColumn By = iReply.Columns.Add("By", typeof(Decimal));
                DataColumn Bz = iReply.Columns.Add("Bz", typeof(Decimal));
                DataColumn Boxy = iReply.Columns.Add("Boxy", typeof(Decimal));
                DataColumn dBoxy = iReply.Columns.Add("dBoxy", typeof(Decimal));
                DataColumn BHA = iReply.Columns.Add("BHA", typeof(Decimal));
                DataColumn dBHABoxy = iReply.Columns.Add("dBHABoxy", typeof(Decimal));
                ArrayList BHAInfo = new ArrayList();
                Int32 rID = -99, refLenU = -99, refDecO = -99, refAcelU = -99, refMagU = -99;
                Int32 MSizeU = -99, DSCSizeU = -99, MDBU = -99, NMSpAbvU = -99, NMSpBlwU = -99, MGVU = -99, DGTU = -99, DSGU = -99, DGBTU = -99;
                Decimal rDepth = -99.00M, rBx = -99.00M, rBy = -99.00M, rBz = -99.00M, rGx = -99.00M, rGy = -99.00M, rGz = -99.00M;
                Decimal refStartD = -99.00M, refEndD = -99.00M, refDip = -99.00M, refDec = -99.00M, refBTotal = -99.00M, refGTotal = -99.00M;
                Decimal cnvRefStartD = -99.00M, cnvRefEndD = -99.00M, cnvRefBTotal = -99.00M, cnvRefGTotal = -99.00M;
                Decimal rawBoxy = -99.00M, MSize = -99.00M, DSCSize = -99.00M, MDBLen = -99.00M, NMSpAbv = -99.00M, NMSpBlw = -99.00M, NMSpAlw = -99.00M;
                Decimal MGValue = -99.00M, DGTop = -99.00M, DSGValue = -99.00M, DGBtm = -99.00M;
                Decimal convPTInc = -99.00M, convPTAzm = -99.00M, cnvMSize = -99.00M, cnvMDBLen = -99.00M;
                Decimal MotorPoleStrength = -99.00M, MotorInterference = -99.00M;
                Decimal diffBoxy = 0, diffBoxyBHA = 0, prevDiffBoxyBHA = 0;
                BHAInfo = AST.getBhaInfoList(RunID, ClientDBString);
                MSize = Convert.ToDecimal(BHAInfo[0]);
                MSizeU = Convert.ToInt32(BHAInfo[1]);
                DSCSize = Convert.ToDecimal(BHAInfo[2]);
                DSCSizeU = Convert.ToInt32(BHAInfo[3]);
                MDBLen = Convert.ToDecimal(BHAInfo[4]);
                MDBU = Convert.ToInt32(BHAInfo[5]);
                NMSpAbv = Convert.ToDecimal(BHAInfo[6]);
                NMSpAbvU = Convert.ToInt32(BHAInfo[7]);
                NMSpBlw = Convert.ToDecimal(BHAInfo[8]);
                NMSpBlwU = Convert.ToInt32(BHAInfo[9]);
                NMSpAlw = OPR.getAllowableSpace(convPTInc, convPTAzm);
                MGValue = Convert.ToDecimal(BHAInfo[11]);
                MGVU = Convert.ToInt32(BHAInfo[12]);
                DGTop = Convert.ToDecimal(BHAInfo[13]);
                DGTU = Convert.ToInt32(BHAInfo[14]);
                DSGValue = Convert.ToDecimal(BHAInfo[15]);
                DSGU = Convert.ToInt32(BHAInfo[16]);
                DGBtm = Convert.ToDecimal(BHAInfo[17]);
                DGBTU = Convert.ToInt32(BHAInfo[18]);
                cnvMSize = Units.convMotorSize(MSize, MSizeU);
                cnvMDBLen = Units.convMDLen(MDBLen, MDBU);
                foreach (var value in ifrList)
                {
                    refStartD = Convert.ToDecimal(ifrList[0]);
                    refEndD = Convert.ToDecimal(ifrList[1]);
                    refLenU = Convert.ToInt32(ifrList[2]);
                    if (refLenU.Equals(4)) { cnvRefStartD = refStartD / 3.2808M; cnvRefEndD = refEndD / 3.2808M; } else { cnvRefStartD = refStartD; cnvRefEndD = refEndD; }
                    refDip = Convert.ToDecimal(ifrList[3]);
                    refDec = Convert.ToDecimal(ifrList[4]);
                    refDecO = Convert.ToInt32(ifrList[5]);
                    refBTotal = Convert.ToDecimal(ifrList[6]);
                    refMagU = Convert.ToInt32(ifrList[7]);
                    cnvRefBTotal = Units.convBTotal(refBTotal, refMagU);
                    refGTotal = Convert.ToDecimal(ifrList[8]);
                    refAcelU = Convert.ToInt32(ifrList[9]);
                    cnvRefGTotal = Units.convGTotal(refGTotal, refAcelU);
                }
                foreach (DataRow row in selBHAFilterTable.Rows)
                {
                    rID = Convert.ToInt32(row["srvID"]);
                    rDepth = Convert.ToDecimal(row["Depth"]);
                    rGx = Convert.ToDecimal(row["Gx"]);
                    rGy = Convert.ToDecimal(row["Gy"]);
                    rGz = Convert.ToDecimal(row["Gz"]);
                    rBx = Convert.ToDecimal(row["Bx"]);
                    rBy = Convert.ToDecimal(row["By"]);
                    rBz = Convert.ToDecimal(row["Bz"]);
                    rawBoxy = QC.colBZ(rBx, rBy);
                    MotorPoleStrength = QC.colFW(cnvMSize);
                    MotorInterference = QC.colGA(NMSpBlw, cnvMDBLen, MotorPoleStrength);
                    diffBoxy = prevDiffBoxyBHA - rawBoxy;
                    diffBoxyBHA = MotorInterference - rawBoxy;
                    prevDiffBoxyBHA = diffBoxy;
                    iReply.Rows.Add(rID, rDepth, rGx, rGy, rGz, rBx, rBy, rBz, rawBoxy, diffBoxy, MotorInterference, diffBoxyBHA);
                    iReply.AcceptChanges();
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }



        public static System.Data.DataTable getNoGoZoneTable(Int32 OperatorID, Int32 SysClientID, Int32 WellpadID, Int32 WellID, Int32 SectionID, Int32 RunID, System.Data.DataTable selDataTable, ArrayList ifrValues, String GetClientDBString)
        {
            Int32 rID = -99, ngzInc = -99, ngzAzm = -99, ngzVal = -99;
            Decimal colTVal = -99.00M, colYVal = -99.00M, GTF = -99.00M;
            Decimal rDepth = -99.00M, rGx = -99.00M, rGy = -99.00M, rGz = -99.00M, rBx = -99.00M, rBy = -99.00M, rBz = -99.00M;
            String ngzStt = String.Empty, ngzC = String.Empty;
            System.Data.DataTable iReply = new System.Data.DataTable();
            DataColumn srvID = iReply.Columns.Add("srvID", typeof(Int32));
            DataColumn Depth = iReply.Columns.Add("Depth", typeof(Decimal));
            DataColumn Gx = iReply.Columns.Add("Gx", typeof(Decimal));
            DataColumn Gy = iReply.Columns.Add("Gy", typeof(Decimal));
            DataColumn Gz = iReply.Columns.Add("Gz", typeof(Decimal));
            DataColumn Bx = iReply.Columns.Add("Bx", typeof(Decimal));
            DataColumn By = iReply.Columns.Add("By", typeof(Decimal));
            DataColumn Bz = iReply.Columns.Add("Bz", typeof(Decimal));
            DataColumn ngz = iReply.Columns.Add("ngz", typeof(String));
            DataColumn ngzClr = iReply.Columns.Add("ngzClr", typeof(String));
            ArrayList ngzInfo = new ArrayList();
            ngzInfo = DMG.getngzDataList();
            ngzInc = Convert.ToInt32(ngzInfo[0]);
            ngzAzm = Convert.ToInt32(ngzInfo[1]);
            foreach (DataRow dRow in selDataTable.Rows)
            {
                rID = Convert.ToInt32(dRow["sID"]);
                rDepth = Convert.ToDecimal(dRow["Depth"]);
                rGx = Convert.ToDecimal(dRow["Gx"]);
                rGy = Convert.ToDecimal(dRow["Gy"]);
                rGz = Convert.ToDecimal(dRow["Gz"]);
                rBx = Convert.ToDecimal(dRow["Bx"]);
                rBy = Convert.ToDecimal(dRow["By"]);
                rBz = Convert.ToDecimal(dRow["Bz"]);
                colTVal = QC.colT(rGx, rGy, rGz);
                GTF = QC.getGTF(rGx, rGy);
                colYVal = QC.colY(GTF, colTVal, rBx, rBy, rBz);
                ngzVal = getNGZ(colTVal, colYVal, ngzInc, ngzAzm);
                if (ngzVal.Equals(1)) { ngzStt = "No"; ngzC = "Green"; } else { ngzStt ="Yes"; ngzC = "Yellow"; }
                iReply.Rows.Add(rID, rDepth, rGx, rGy, rGz, rBx, rBy, rBz, ngzStt, ngzC);
                iReply.AcceptChanges();
            }
            return iReply;
        }
    }
}