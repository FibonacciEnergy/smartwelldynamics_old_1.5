﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using SMO = Microsoft.SqlServer.Management.Smo;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using CON = Microsoft.SqlServer.Management.Common.ServerConnection;

namespace SmartsVer1.Helpers.DatabaseHelpers
{
    public class DBConfigs
    {
        static void Main()
        { }

        public static System.Data.DataTable getDatabaseListTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn cmID = iReply.Columns.Add("cmID", typeof(Int32));
                DataColumn cmName = iReply.Columns.Add("cmName", typeof(String));
                DataColumn dbSize = iReply.Columns.Add("dbSize", typeof(Decimal));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [cmID], [cmName], [uTime] FROM [ConMap] ORDER BY [cmName] ASC";
                Int32 id = -99;
                String name = String.Empty;
                DateTime uT = DateTime.Now;
                Decimal size = -99.0000M;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        size = getDBSize(name);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, name, size, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getClientConnectedDatabaseListTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn cdbID = iReply.Columns.Add("cdbID", typeof(Int32));
                DataColumn clntName = iReply.Columns.Add("clntName", typeof(String));
                DataColumn clntRealm = iReply.Columns.Add("clntRealm", typeof(String));
                DataColumn cmName = iReply.Columns.Add("cmName", typeof(String));
                DataColumn dbSize = iReply.Columns.Add("dbSize", typeof(Decimal));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [cdbID], [clntID], [clntRealm], [cmName], [uTime] FROM [ClientDBMapping]";
                Int32 id = -99, clntID = -99;
                String name = String.Empty, realm = String.Empty, dbName = String.Empty;
                DateTime uT = DateTime.Now;
                Decimal size = -99.0000M;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        clntID = readData.GetInt32(1);
                                        name = CLNT.getClientName(clntID);
                                        realm = readData.GetString(2);
                                        dbName = readData.GetString(3);
                                        size = getDBSize(dbName);
                                        uT = readData.GetDateTime(4);
                                        iReply.Rows.Add(id, name, realm, dbName, size, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply.DefaultView.Sort = "clntName";
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Decimal getDBSize(String DataBaseName)
        {
            try
            {
                Decimal iReply = -99.0000M;                 
                CON srvCon = new CON();
                srvCon.ServerInstance = WebConfigurationManager.AppSettings["DBServer"].ToString();
                srvCon.LoginSecure = false;
                srvCon.Login = WebConfigurationManager.AppSettings["DBUser"].ToString();
                srvCon.Password = WebConfigurationManager.AppSettings["DBPassword"].ToString();
                SMO.Server dbServer = new SMO.Server(srvCon);
                dbServer.Refresh();
                SMO.Database clntDB = new SMO.Database();
                clntDB = dbServer.Databases[DataBaseName];
                if (!clntDB.Equals(null) && clntDB.ID > 0)
                {
                    iReply = Convert.ToDecimal(clntDB.Size);
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getClientConnectionStringsTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn cmID = iReply.Columns.Add("cmID", typeof(Int32));
                DataColumn cmName = iReply.Columns.Add("cmName", typeof(String));
                DataColumn cmString = iReply.Columns.Add("cmString", typeof(String));
                DataColumn dbSize = iReply.Columns.Add("dbSize", typeof(Decimal));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [cmID], [cmName], [cmString], [uTime] FROM [ConMap] ORDER BY [uTime] DESC";
                Int32 id = -99;
                String dbName = String.Empty, dbString = String.Empty;
                DateTime uT = DateTime.Now;
                Decimal size = -99.0000M;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        dbName = readData.GetString(1);
                                        dbString = readData.GetString(2);
                                        size = getDBSize(dbName);
                                        uT = readData.GetDateTime(3);
                                        iReply.Rows.Add(id, dbName, dbString, size, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertClientConnectionStringMapping(String DatabaseName, String LoginName, String LoginRealm)
        {
            try
            {
                Int32 iReply = -99;
                GetClientConnections cCon = new GetClientConnections();
                iReply = cCon.createConnection(DatabaseName, LoginName, LoginRealm);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 createClientDataBase(String ClientDatabaseName)
        {
            Int32 iReply = -99;
            String dbScriptFile = HttpContext.Current.Server.MapPath("~/App_Data/ClientDBCreatorText.txt");
            String createScript = File.ReadAllText(dbScriptFile);
            createScript = createScript.Replace("##DatabaseName##", ClientDatabaseName.ToString());             
            CON srvCon = new CON();
            srvCon.ServerInstance = WebConfigurationManager.AppSettings["DBServer"].ToString();
            srvCon.LoginSecure = false;
            srvCon.Login = WebConfigurationManager.AppSettings["DBCreator"].ToString();
            srvCon.Password = WebConfigurationManager.AppSettings["DBPassword"].ToString();
            SMO.Server dbServer = new SMO.Server(srvCon);
            iReply = dbServer.ConnectionContext.ExecuteNonQuery(createScript);
            dbServer.Refresh();
            //Inserting SMARTs SQC Default parameters
            String sqcScript = HttpContext.Current.Server.MapPath("~/App_Data/insertSQC.txt");
            String sqcInsert = File.ReadAllText(sqcScript);
            sqcInsert = sqcInsert.Replace("##DatabaseName##", ClientDatabaseName.ToString());
            iReply = dbServer.ConnectionContext.ExecuteNonQuery(sqcInsert);
            dbServer.Refresh();
            return iReply;
        }

        public static String getNextDataBaseName()
        {
            try
            {
                String iReply = String.Empty, newDB = String.Empty;
                String selData = "SELECT MAX(CAST(SUBSTRING(cmName, 11, LEN(cmName)-5) AS INT)) FROM [ConMap]", maxDB = String.Empty;
                Int32 dbNumber = -99;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    dataCon.Open();
                    SqlCommand getData = new SqlCommand(selData, dataCon);
                    using (SqlDataReader readData = getData.ExecuteReader())
                    {
                        if (readData.HasRows)
                        {
                            while (readData.Read())
                            {
                                dbNumber = readData.GetInt32(0);
                            }
                        }
                    }
                }
                Int32 newDBNumb = Convert.ToInt32(dbNumber) + 1;
                newDB = Convert.ToString(newDBNumb);
                newDB = newDB.PadLeft(5, '0');
                iReply = String.Format("FEClientDB{0}", newDB);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}