﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Web.Security;
using System.Data.SqlClient;
using System.Web.Configuration;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using USR = SmartsVer1.Helpers.AccountHelpers.CreateUser;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using OP = SmartsVer1.Helpers.QCHelpers.Opr;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using MAP = SmartsVer1.Helpers.ChartHelpers.MapBox;

namespace SmartsVer1.Helpers.ChartHelpers
{
    public class GMaps
    {        
        void main()
        { }

        public static Dictionary<Int32, Int32> getWellSectionIDListForActiveRun(String ClientDBString)
        {
            try
            {
                Dictionary<Int32, Int32> iReply = new Dictionary<Int32, Int32>();
                Int32 rID = -99, wID = -99;
                String selData = "SELECT [runID], [wswID] FROM [Run] WHERE [dmgstID] = @dmgstID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.Add("@dmgstID", SqlDbType.Int).Value = 1.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        rID = readData.GetInt32(0);
                                        wID = readData.GetInt32(1);
                                        iReply.Add(rID, wID);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, Int32> getActiveWellIDList(String ClientDBString)
        {
            try
            {
                Dictionary<Int32, Int32> iReply = new Dictionary<Int32, Int32>(), wswList = new Dictionary<Int32, Int32>();
                Int32 RunID = -99, WellSectionID = -99, WellID = -99;
                wswList = getWellSectionIDListForActiveRun(ClientDBString);
                String selData = "SELECT [welID] FROM [WellSectionToWell] WHERE [wswID] = @wswID";
                using(SqlConnection dataCon = new SqlConnection(ClientDBString.ToString()))
                {
                    try
                    {
                        dataCon.Open();                        
                        foreach (KeyValuePair<Int32, Int32> pair in wswList)
                        {
                            RunID = pair.Key;
                            WellSectionID = pair.Value;
                            SqlCommand getData = new SqlCommand(selData, dataCon);
                            getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            WellID = Convert.ToInt32(readData.GetInt32(0));
                                            iReply.Add(RunID, WellID);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getActiveWellTable(Int32 ClientID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn welID = iReply.Columns.Add("welID", typeof(Int32));
                DataColumn welName = iReply.Columns.Add("welName", typeof(String));
                DataColumn welOpr = iReply.Columns.Add("welOpr", typeof(String));
                DataColumn welApiUwi = iReply.Columns.Add("welApiUwi", typeof(String));
                DataColumn welSpud = iReply.Columns.Add("welSpud", typeof(DateTime));
                DataColumn welRun = iReply.Columns.Add("welRun", typeof(String));
                DataColumn welSec = iReply.Columns.Add("welSec", typeof(String));
                DataColumn jobName = iReply.Columns.Add("jobName", typeof(String));
                DataColumn welDepth = iReply.Columns.Add("welDepth", typeof(String));
                DataColumn welPTime = iReply.Columns.Add("welPTime", typeof(String));
                List<Int32> ActiveWellIDList = new List<Int32>();
                Int32 id = -99, runID = -99, wID = -99, cID = -99, oID = -99, rID = -99, srID = -99, cyID = -99, cnID = -99, fID = -99, stID = -99;
                String clntCo = String.Empty, opCo = String.Empty, name = String.Empty, permit = String.Empty, API = String.Empty, UWI = String.Empty, maxDepth = String.Empty, RunStatus = String.Empty, RunName = String.Empty, APIUWI = String.Empty, welSection = String.Empty, jName = String.Empty;
                Decimal lat = -99.000000M, lng = -99.000000M;
                DateTime spud = DateTime.Now;                
                String selData = "SELECT [welID], [clntID], [optrID], [welName], [regID], [sregID], [ctyID], [cntyID], [fldID], [stID], [welAPI], [welUWI], [welPermit], [welSpudDate], [welLatitude], [welLongitude] FROM [Well] WHERE [welID] = @welID;";
                String lastSurveyProcessed = String.Empty;
                List<String> runInfo = new List<String>();
                Dictionary<Int32, Int32> wellList = getActiveWellIDList(ClientDBString);

                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        
                        foreach (KeyValuePair<Int32, Int32> pair in wellList)
                        {
                            runID = pair.Key;
                            id = pair.Value;
                            SqlCommand getData = new SqlCommand(selData, dataCon);
                            getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = id.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            wID = readData.GetInt32(0);
                                            cID = readData.GetInt32(1);
                                            clntCo = CLNT.getClientName(cID);
                                            oID = readData.GetInt32(2);
                                            opCo = CLNT.getClientName(oID);
                                            name = readData.GetString(3);
                                            rID = readData.GetInt32(4);
                                            srID = readData.GetInt32(5);
                                            cyID = readData.GetInt32(6);
                                            cnID = readData.GetInt32(7);
                                            fID = readData.GetInt32(8);
                                            stID = readData.GetInt32(9);
                                            API = readData.GetString(10);
                                            if (String.IsNullOrEmpty(API))
                                            {
                                                API = "---";
                                            }
                                            UWI = readData.GetString(11);
                                            if (String.IsNullOrEmpty(UWI))
                                            {
                                                UWI = "---";
                                            }
                                            permit = readData.GetString(12);
                                            spud = readData.GetDateTime(13);
                                            lat = readData.GetDecimal(14);
                                            lng = readData.GetDecimal(15);
                                            runInfo = getRunInfo(wID, ClientDBString);
                                            RunName = Convert.ToString(runInfo[1]);
                                            if (RunName.Equals("0") || String.IsNullOrEmpty(RunName))
                                            {
                                                maxDepth = "---";
                                                RunStatus = "---";
                                                lastSurveyProcessed = "---";
                                            }
                                            else
                                            {
                                                if (runInfo.Count < 5)
                                                {
                                                    RunStatus = Convert.ToString(runInfo[2]);
                                                    maxDepth = "---";
                                                    lastSurveyProcessed = "---";
                                                }
                                                else
                                                {
                                                    RunStatus = Convert.ToString(runInfo[2]);
                                                    maxDepth = Convert.ToString(runInfo[3]);
                                                    lastSurveyProcessed = Convert.ToString(runInfo[4]);
                                                }
                                            }
                                            welSection = getRunSectionName(Convert.ToInt32(runInfo[0]), ClientDBString);
                                            if(String.IsNullOrEmpty(welSection))
                                            {
                                                welSection = "---";
                                            }
                                            if(String.IsNullOrEmpty(jName))
                                            {
                                                jName = "---";
                                            }
                                            iReply.Rows.Add(wID, name, opCo, API + " / " + UWI, spud, RunName, welSection, jName, maxDepth, lastSurveyProcessed);
                                            iReply.AcceptChanges();
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static String getRunSectionName(Int32 RunID, String ClientDBString)
        {
            try
            {
                String iReply = String.Empty;
                
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static String getAddressContent(Int32 ClientID, Int32 AddressID)
        {
            try
            {
                StringBuilder iReply = new StringBuilder();
                String[] addArray = new String[] { };
                String addLine = String.Empty, coName = String.Empty, title = String.Empty, street = String.Empty, city = String.Empty, state = String.Empty, zip = String.Empty, country = String.Empty, subReg = String.Empty, reg = String.Empty, phone = String.Empty;
                coName = CLNT.getClientName(ClientID);
                addLine = CLNT.getAddressLineWithPhone(ClientID, AddressID);
                addArray = addLine.Split(',');
                title = addArray[0];
                street = addArray[1];
                city = addArray[2];
                state = addArray[3];
                zip = addArray[4];
                country = addArray[5];
                phone = addArray[6];
                iReply.Append("<div id='content' runat='server'>");
                iReply.Append("<h1>" + coName + "</h1>");
                iReply.Append("<div id='contentBody' runat='server'>");
                iReply.Append("<table><tr><td><b>Title</b></td><td>" + title + "</td></tr>");
                iReply.Append("<tr><td><b>Street</b></td><td>" + street + "</td></tr>");
                iReply.Append("<tr><td><b>City/Town</b></td><td>" + city + "</td></tr>");
                iReply.Append("<tr><td><b>State/Province</b></td><td>" + state + "</td></tr>");
                iReply.Append("<tr><td><b>Zip/Postal Code</b></td><td>" + zip + "</td></tr>");
                iReply.Append("<tr><td><b>Country/Nation</b></td><td>" + country + "</td></tr>");
                iReply.Append("<tr><td><b>Phone</b></td><td>" + phone + "</td></tr>");
                iReply.Append("</table>");
                iReply.Append("</div>");
                iReply.Append("</div>");
                return iReply.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getAddressAssignment(Int32 ClientID, Int32 AddressID, String ClientDBConnection)
        {
            try
            {
                StringBuilder iReply = new StringBuilder();
                Int32 directors = -99, coords = -99, fhands = -99, labtechs = -99, reviewers = -99;
                String[] addArray = new String[] { };
                List<Int32> clientUsers = new List<Int32>();
                String addLine = String.Empty, coName = String.Empty, title = String.Empty, street = String.Empty, city = String.Empty, state = String.Empty, zip = String.Empty, country = String.Empty, subReg = String.Empty, reg = String.Empty, phone = String.Empty;
                coName = CLNT.getClientName(ClientID);
                clientUsers = USR.getLocationUserCount(ClientID, AddressID, ClientDBConnection);
                directors = clientUsers[0];
                coords = clientUsers[1];
                fhands = clientUsers[2];
                labtechs = clientUsers[3];
                reviewers = clientUsers[4];
                addLine = CLNT.getAddressLineWithPhone(ClientID, AddressID);
                addArray = addLine.Split(',');
                title = addArray[0];
                street = addArray[1];
                city = addArray[2];
                state = addArray[3];
                zip = addArray[4];
                country = addArray[5];
                phone = addArray[6];
                iReply.Append("<div id='content' runat='server'>");
                iReply.Append("<h1>" + title + "</h1>");
                iReply.Append("<div id='contentBody' runat='server'>");
                iReply.Append("<table><tr><td width='105px' align='right'><b>Street  :</b></td><td>" + street + "</td></tr>");
                iReply.Append("<tr><td width='105px' align='right'><b>City/Town  :</b></td><td>" + city + "</td></tr>");
                iReply.Append("<tr><td width='105px' align='right'><b>State/Province  :</b></td><td>" + state + "</td></tr>");
                iReply.Append("<tr><td width='105px' align='right'><b>Zip/Postal Code  :</b></td><td>" + zip + "</td></tr>");
                iReply.Append("<tr><td width='105px' align='right'><b>Country/Nation  :</b></td><td>" + country + "</td></tr>");
                iReply.Append("<tr><td width='105px' align='right'><b>Phone  :</b></td><td>" + phone + "</td></tr></table>");
                iReply.Append("<h1>SMARTs User Assignments</h1>");
                iReply.Append("</div>");
                iReply.Append("<table><tr><td width='105px' align='right'><b>Director(s)  :</b></td><td>" + directors + "</td></tr>");
                iReply.Append("<table><tr><td width='105px' align='right'><b>Coordinator(s)  :</b></td><td>" + coords + "</td></tr>");
                iReply.Append("<table><tr><td width='105px' align='right'><b>Fieldhand(s)  :</b></td><td>" + fhands + "</td></tr>");
                iReply.Append("<table><tr><td width='105px' align='right'><b>Lab. Tech(s)  :</b></td><td>" + labtechs + "</td></tr>");
                iReply.Append("<table><tr><td width='105px' align='right'><b>Log Reviewer(s)  :</b></td><td>" + reviewers + "</td></tr></table>");
                iReply.Append("</div>");
                return iReply.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<String> getClientLabLocation(Int32 ClientID)
        {
            try
            {
                List<String> iReply = new List<String>();
                String selData = "SELECT [clntAddID], [dmgID], [latitude], [longitude], [title] FROM [ClientAddress] WHERE [clntID] = @clntID AND [dmgID] = @dmgID";
                Int32 id = 0, type = 0;
                String name = String.Empty, list = String.Empty, addressContent = String.Empty;
                String iconM = "https://maps.google.com/mapfiles/kml/paddle/L.png";
                Decimal lati, longi;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@dmgID", SqlDbType.Int).Value = 6.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        type = readData.GetInt32(1);
                                        name = DMG.getDemogTypeName(type);                                        
                                        lati = readData.GetDecimal(2);
                                        longi = readData.GetDecimal(3);
                                        addressContent = getAddressContent(ClientID, id);
                                        list = name + "," + lati + "," + longi + "," + iconM + "," + addressContent;
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply.Add(list);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<String> getClientMainLocation(Int32 ClientID)
        {
            try
            {
                List<String> iReply = new List<String> { };
                String selData = "SELECT [clntAddID], [dmgID], [latitude], [longitude] FROM [ClientAddress] WHERE [clntID] = @clntID AND [dmgID] = @dmgID";
                Int32 id = 0, type = 0;
                String name = String.Empty, addressContent = String.Empty, list = String.Empty;
                String iconM = "https://maps.google.com/mapfiles/kml/paddle/M.png";
                Decimal lati, longi;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@dmgID", SqlDbType.Int).Value = 2.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        type = readData.GetInt32(1);
                                        name = DMG.getDemogTypeName(type);                                        
                                        lati = readData.GetDecimal(2);
                                        longi = readData.GetDecimal(3);
                                        addressContent = getAddressContent(ClientID, id);
                                        list = name + "," + lati + "," + longi + "," + iconM + "," + addressContent;
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply.Add(list);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<String> getClientMainLocationAssignments(Int32 ClientID, String ClientDBConnection)
        {
            try
            {
                List<String> iReply = new List<String> { };
                String selData = "SELECT [clntAddID], [dmgID], [latitude], [longitude] FROM [ClientAddress] WHERE [clntID] = @clntID AND [dmgID] = @dmgID";
                Int32 id = 0, type = 0;
                String name = String.Empty, addressContent = String.Empty, list = String.Empty;
                String iconM = "https://maps.google.com/mapfiles/kml/paddle/M.png";
                Decimal lati, longi;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@dmgID", SqlDbType.Int).Value = 2.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        type = readData.GetInt32(1);
                                        name = DMG.getDemogTypeName(type);
                                        lati = readData.GetDecimal(2);
                                        longi = readData.GetDecimal(3);
                                        addressContent = getAddressAssignment(ClientID, id, ClientDBConnection);
                                        list = name + "," + lati + "," + longi + "," + iconM + "," + addressContent;
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply.Add(list);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<String> getClientOfficeLocations(Int32 ClientID)
        {
            try
            {
                List<String> iReply = new List<String> {};
                String selData = "SELECT [clntAddID], [dmgID], [latitude], [longitude] FROM [ClientAddress] WHERE [clntID] = @clntID AND [dmgID] = @dmgID";
                Int32 type = 0, id = 0;
                String name = String.Empty, list = String.Empty, addressContent = String.Empty;
                String iconO = "https://maps.google.com/mapfiles/kml/paddle/O.png";
                Decimal lati, longi;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@dmgID", SqlDbType.Int).Value = 3.ToString(); //Location type is Office
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        type = readData.GetInt32(1);
                                        name = DMG.getDemogTypeName(type);
                                        lati = readData.GetDecimal(2);
                                        longi = readData.GetDecimal(3);
                                        addressContent = getAddressContent(ClientID, id);
                                        list = name + "," + lati + "," + longi + "," + iconO + ", " + addressContent;  
                                        iReply.Add(list);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }                
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<String> getClientUserMarkers(Int32 ClientID, String ClientDBConnection)
        {
            try
            {
                List<String> iReply = new List<String>(), locationInfo = new List<String>();
                System.Data.DataTable userLocData = new System.Data.DataTable();
                Int32 addressID = -99, gender = -99;
                String title = String.Empty, st1 = String.Empty, st2 = String.Empty, st3 = String.Empty;
                String city = String.Empty, state = String.Empty, zip = String.Empty, country = String.Empty, phone = String.Empty, addressContent = String.Empty, marker = String.Empty;
                String fName = String.Empty, lName = String.Empty, designation = String.Empty, username = String.Empty, headingName = String.Empty, icon = String.Empty;
                Decimal latitude, longitude;
                DateTime stDate = DateTime.Now, enDate = DateTime.Now;
                System.Guid UserId = new System.Guid();
                String mIcon = "http://maps.google.com/mapfiles/kml/shapes/man.png", wIcon = "http://maps.google.com/mapfiles/kml/shapes/woman.png";
                userLocData = USR.getUserAssignedLocation(ClientDBConnection);
                foreach (System.Data.DataRow row in userLocData.Rows)
                {
                    UserId = (System.Guid)row[0];
                    addressID = Convert.ToInt32(row[1]);
                    stDate = Convert.ToDateTime(row[2]);
                    enDate = Convert.ToDateTime(row[3]);
                    
                        locationInfo = CLNT.getAddressListWithPhone(ClientID, addressID);
                        title = Convert.ToString(locationInfo[0]);
                        st1 = Convert.ToString(locationInfo[1]);
                        st2 = Convert.ToString(locationInfo[2]);
                        st3 = Convert.ToString(locationInfo[3]);
                        city = Convert.ToString(locationInfo[4]);
                        state = Convert.ToString(locationInfo[5]);
                        zip = Convert.ToString(locationInfo[6]);
                        country = Convert.ToString(locationInfo[7]);
                        phone = Convert.ToString(locationInfo[8]);
                        latitude = Convert.ToDecimal(locationInfo[9]);
                        longitude = Convert.ToDecimal(locationInfo[10]);

                        gender = USR.getUserGenderID(UserId);
                        username = USR.getUserNameFromId(UserId);
                        UP usrP = UP.GetUserProfile(username);
                        fName = usrP.FirstName;
                        lName = usrP.LastName;
                        designation = usrP.designation;
                        String[] roles = Roles.GetRolesForUser(username);
                        if (!roles[0].Equals("Support"))
                        {
                            headingName = String.Format("{0}, {1} ( {2} )", lName, fName, designation);
                            if (gender.Equals(1))
                            {
                                icon = mIcon;
                            }
                            else
                            {
                                icon = wIcon;
                            }
                            addressContent = getUserAddressContent(title, st1, st2, st3, city, state, zip, country, phone);
                            marker = headingName + "," + latitude + "," + longitude + "," + icon + "," + addressContent;
                            iReply.Add(marker);
                        }                                                          
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<String> getClientWellLocations(String ClientDBString)
        {
            try
            {
                List<String> iReply = new List<String> { };
                //String icon = "https://maps.google.com/mapfiles/kml/paddle/W.png";
                String icon = "http://maps.google.com/mapfiles/kml/pal5/icon22l.png";
                String selData = "SELECT [welID], [clntID], [optrID], [welName], [regID], [sregID], [ctyID], [cntyID], [fldID], [stID], [welAPI], [welUWI], [welPermit], [welSpudDate], [welLatitude], [welLongitude] FROM [Well]";
                String name = String.Empty, value = String.Empty, welContent = String.Empty, permit = String.Empty, API = String.Empty, UWI = String.Empty, clntCo = String.Empty, opCo = String.Empty;
                Int32 wID = -99, cID = -99, oID = -99, rID = -99, srID = -99, cyID = -99, cnID = -99, fID = -99, stID = -99;
                Decimal lat, lng;
                DateTime spud = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        wID = readData.GetInt32(0);
                                        cID = readData.GetInt32(1);
                                        clntCo = CLNT.getClientName(cID);
                                        oID = readData.GetInt32(2);
                                        opCo = CLNT.getClientName(oID);
                                        name = readData.GetString(3);
                                        rID = readData.GetInt32(4);
                                        srID = readData.GetInt32(5);
                                        cyID = readData.GetInt32(6);
                                        cnID = readData.GetInt32(7);
                                        fID = readData.GetInt32(8);
                                        stID = readData.GetInt32(9);
                                        API = readData.GetString(10);
                                        if (String.IsNullOrEmpty(API))
                                        {
                                            API = "---";
                                        }
                                        UWI = readData.GetString(11);
                                        if (String.IsNullOrEmpty(UWI))
                                        {
                                            UWI = "---";
                                        }
                                        permit = readData.GetString(12);
                                        spud = readData.GetDateTime(13);
                                        lat = readData.GetDecimal(14);
                                        lng = readData.GetDecimal(15);
                                        welContent = getLocalWellContent(wID, name, opCo, clntCo, rID, srID, cyID, stID, cnID, fID, API, UWI, spud, permit, lat, lng, ClientDBString);
                                        value = name + "," + lat + "," + lng + "," + icon + "," + welContent;
                                        iReply.Add(value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getGlobalWellContent(String WellName, String OperatorName, String ServiceProviderName, Int32 RegionID, Int32 SubRegionID, Int32 CountryID, Int32 StateID, Int32 CountyID, Int32 FieldID, String API, String UWI, Decimal Latitude, Decimal Longitude)
        {
            try
            {
                StringBuilder iReply = new StringBuilder();
                String Region = String.Empty, SubRegion = String.Empty, Country = String.Empty, State = String.Empty, County = String.Empty, Field = String.Empty;
                Region = DMG.getRegionName(RegionID);
                SubRegion = DMG.getSubRegionName(SubRegionID);
                Country = DMG.getCountryName(CountryID);
                State = DMG.getStateName(StateID);
                County = DMG.getCountyName(CountyID);
                Field = DMG.getFieldName(FieldID);
                iReply.Append("<div id='content' runat='server'>");
                iReply.Append("<h1>" + WellName + "</h1>");
                iReply.Append("<div id='contentBody' runat='server'>");
                iReply.Append("<table><tr><td><b>Operator/Owner</b></td><td>" + OperatorName + "</td><td></td><td></td></tr>");
                iReply.Append("<tr><td><b>Provider</b></td><td>" + ServiceProviderName + "</td><td></td><td></td></tr>");
                iReply.Append("<tr><td><b>API/UWI</b></td><td>" + API  + "</td><td> " + UWI + "</td><td></td></tr>");
                iReply.Append("<tr><td><b>Field</b></td><td>" + Field + "</td><td></td><td></td></tr>");
                iReply.Append("<tr><td><b>Country/State/County</b></td><td>" + Country + "</td><td>" + State + "</td><td>" + County + "</td></tr>");
                iReply.Append("<tr><td><b>SubRegion/Region</b></td><td>" + SubRegion + "</td><td>" + Region + "</td><td></td></tr>");
                iReply.Append("<tr><td><b>Longitude</b></td><td>" + Longitude + "</td><td></td><td></td></tr>");
                iReply.Append("<tr><td><b>Latitude</b></td><td>" + Latitude + "</td><td></td><td></td></tr>");
                iReply.Append("</div>");
                iReply.Append("</div>");

                return iReply.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static List<String> getGlobalWellLocations(Int32 ClientID)
        {
            try
            {
                List<String> iReply = new List<String> { };
                String icon = "http://maps.google.com/mapfiles/kml/pal5/icon22l.png";
                String selData = "SELECT [welID], [clntID], [optrID], [welName], [regID], [sregID], [ctyID], [cntyID], [fldID], [stID], [welAPI], [welUWI], [welLatitude], [welLongitude] FROM [dmgWell] WHERE [clntID] <> @clntID";
                Int32 wID = -99, cID = -99, oID = -99, rID = -99, srID = -99, cyID = -99, stID = -99, cnID = -99, fID = -99;
                String name = String.Empty, value = String.Empty, cName = String.Empty, oName = String.Empty, wellContent = String.Empty, API = String.Empty, UWI = String.Empty;
                Decimal lat, lng;                
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        wID = readData.GetInt32(0);
                                        cID = readData.GetInt32(1);
                                        cName = CLNT.getClientName(cID);
                                        oID = readData.GetInt32(2);
                                        oName = CLNT.getClientName(oID);
                                        name = readData.GetString(3);
                                        rID = readData.GetInt32(4);
                                        srID = readData.GetInt32(5);
                                        cyID = readData.GetInt32(6);
                                        cnID = readData.GetInt32(7);
                                        fID = readData.GetInt32(8);
                                        stID = readData.GetInt32(9);
                                        API = readData.GetString(10);
                                        if (String.IsNullOrEmpty(API))
                                        {
                                            API = "---";
                                        }
                                        UWI = readData.GetString(11);
                                        if (String.IsNullOrEmpty(UWI))
                                        {
                                            UWI = "---";
                                        }
                                        lat = readData.GetDecimal(12);
                                        lng = readData.GetDecimal(13);
                                        wellContent = getGlobalWellContent(name, oName, cName, rID, srID, cyID, stID, cnID, fID, API, UWI, lat, lng);
                                        value = name + "," + lat + "," + lng + "," + icon + "," + wellContent;
                                        iReply.Add(value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        } 

        public static String getHRAssignmentMap(Int32 ClientID, String ControlObjectName, String ClientDBConnection)
        {
            try
            {
                StringBuilder iReply = new StringBuilder();
                System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<String> mainClntData = new List<String>(), offClntData = new List<String>(), labClntData = new List<String>(), allLocs = new List<String>();
                mainClntData = getClientMainLocationAssignments(ClientID, ClientDBConnection);
                offClntData = getClientOfficeLocations(ClientID);
                labClntData = getClientLabLocation(ClientID);
                mainClntData.Add(mainClntData[0]);
                allLocs.Add(mainClntData[0]);
                foreach (String oval in offClntData)
                {
                    allLocs.Add(oval);
                }
                foreach (String lval in labClntData)
                {
                    allLocs.Add(lval);
                }                
                
                String mainData = oSerializer.Serialize(allLocs);
                iReply.Append("<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>");
                iReply.Append("<script type='text/javascript' src='https://www.gstatic.com/charts/loader.js'></script>");
                iReply.Append("<script type='text/javascript'>");
                iReply.Append("var mainValues =" + mainData + ";");
                iReply.Append("var mname, mlat, mlon, micon, mContent, mapbounds, contentString, marker, i, lname, llat, llon, licon, lContent, latlng;");                
                iReply.Append("var mData = mainValues[0].split(',');");
                iReply.Append("mlat = mData[1];");
                iReply.Append("mlon = mData[2];");
                iReply.Append("function initMap() {");
                iReply.Append("var landingMapDiv = document.getElementById('" + ControlObjectName + "');");
                iReply.Append("var landingMapOptions = { maptypeId: google.maps.MapTypeId.ROADMAP };");
                iReply.Append("var map = new google.maps.Map(landingMapDiv, landingMapOptions);");
                iReply.Append("mapbounds = new google.maps.LatLngBounds();");
                iReply.Append("for(i=0; i < mainValues.length; i++){");
                iReply.Append("var lData = mainValues[i].split(',');");
                iReply.Append("lname = lData[0];");
                iReply.Append("llat = lData[1];");
                iReply.Append("llon = lData[2];");
                iReply.Append("licon = lData[3];");
                iReply.Append("lContent = lData[4];");
                iReply.Append("latlng = new google.maps.LatLng(llat, llon);");
                iReply.Append("marker = new google.maps.Marker({position: new google.maps.LatLng(llat, llon), map: map, icon: licon});");
                iReply.Append("mapbounds.extend(latlng);");
                iReply.Append("var locInfo = new google.maps.InfoWindow({content: lContent});");
                iReply.Append("google.maps.event.addListener(marker, 'click', (function (marker, lContent, locInfo) {");
                iReply.Append("return function () {");
                iReply.Append("locInfo.setContent(lContent);");
                iReply.Append("locInfo.open(map, marker);};");
                iReply.Append("})(marker, lContent, locInfo));}");
                iReply.Append("map.fitBounds(mapbounds);");
                iReply.Append("}</script>");
                iReply.Append("<script type='text/javascript'>google.charts.load('current', {packages: ['corechart']});google.charts.setOnLoadCallback(initMap);</script>");
                iReply.Append("<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAY_dHOcYdykHMGnACxox4cGT9m4Rhf44c&callback=initMap' async defer></script>");

                return iReply.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static List<string> getMainAssignments(Int32 ClientID, String ClientDBString)
        {
            try
            {
                List<String> iReply = new List<String>(), mainLoc = new List<String>();
                mainLoc = getClientMainLocation(ClientID);

                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getLandingMap(Int32 ClientID, String ClientDBString)
        {
            try
            {
                System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<String> mainClntData = new List<String> { };
                mainClntData = getClientMainLocation(ClientID);
                String mainData = oSerializer.Serialize(mainClntData);
                List<String> WellData = getLandingWellLocations(ClientDBString);
                String WellLoc = oSerializer.Serialize(WellData);
                StringBuilder iReply = new StringBuilder();
                iReply.Append("<script type='text/javascript'>");
                iReply.Append("var mainValues =" + mainData + ";");
                iReply.Append("var wellValues =" + WellLoc + ";");
                iReply.Append("var mname, mlat, mlon, micon, mContent, wname, wlat, wlon, wicon, wContent, contentString;");
                iReply.Append("var marker, i;");
                iReply.Append("var mData = mainValues[0].split(',');");
                iReply.Append("mname = mData[0];");
                iReply.Append("mlat = mData[1];");
                iReply.Append("mlon = mData[2];");
                iReply.Append("micon = mData[3];");
                iReply.Append("mContent = mData[4];");
                iReply.Append("function initMap() {");
                iReply.Append("map = new google.maps.Map(document.getElementById('MainContent_ctrlCRDViewer1_divMap'),{");
                iReply.Append("zoom: 3,");
                iReply.Append("center: new google.maps.LatLng(mlat, mlon),");
                iReply.Append("maptypeId: 'roadmap'});");
                iReply.Append("var mwindow = new google.maps.InfoWindow({ content: contentString });");
                iReply.Append("marker = new google.maps.Marker({");
                iReply.Append("position: new google.maps.LatLng(mlat, mlon),");
                iReply.Append("map: map,");
                iReply.Append("icon: micon});");
                iReply.Append("google.maps.event.addListener(marker, 'click', (function (marker, mContent, mwindow) {");
                iReply.Append("return function () {");
                iReply.Append("mwindow.setContent(mContent);");
                iReply.Append("mwindow.open(map, marker);};");
                iReply.Append("})(marker, mContent, mwindow));");
                iReply.Append("for (var j = 0; j < wellValues.length; j++) {");
                iReply.Append("for (var k = 0; k < wellValues[j].length; k++) {");
                iReply.Append("var wData = wellValues[j].split(',');");
                iReply.Append("wname = wData[0];");
                iReply.Append("wlat = wData[1];");
                iReply.Append("wlon = wData[2];");
                iReply.Append("wicon = wData[3];");
                iReply.Append("wContent = wData[4];");
                iReply.Append("var wwindow = new google.maps.InfoWindow({ content: wContent });");
                iReply.Append("var wmarker = new google.maps.Marker({");
                iReply.Append("title: wname,");
                iReply.Append("position: new google.maps.LatLng(wlat, wlon),");
                iReply.Append("map: map,");
                iReply.Append("icon: wicon});");
                iReply.Append("google.maps.event.addListener(wmarker, 'click', (function (wmarker, wContent, wwindow) {");
                iReply.Append("return function () {");
                iReply.Append("wwindow.setContent(wContent);");
                iReply.Append("wwindow.open(map, wmarker);};");
                iReply.Append("})(wmarker, wContent, wwindow));}}");
                iReply.Append("}</script>");
                iReply.Append("<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAY_dHOcYdykHMGnACxox4cGT9m4Rhf44c&callback=initMap' async defer></script>");
                return iReply.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<String> getLandingWellLocations(String ClientDBString)
        {
            try
            {
                List<String> iReply = new List<String> { };
                List<Int32> wellList = new List<Int32> { };
                String icon = "http://maps.google.com/mapfiles/kml/pal5/icon22l.png";
                String selWell = "SELECT [welID] FROM [RunToWell] WHERE [runID] IN (SELECT [runID] FROM [Run] WHERE [dmgstID] = 1);";
                String selData = "SELECT [welID], [clntID], [optrID], [welName], [regID], [sregID], [ctyID], [cntyID], [fldID], [stID], [welAPI], [welUWI], [welPermit], [welSpudDate], [welLatitude], [welLongitude] FROM [Well] WHERE [welID] = @welID;";
                String name = String.Empty, value = String.Empty, welContent = String.Empty, permit = String.Empty, API = String.Empty, UWI = String.Empty, clntCo = String.Empty, opCo = String.Empty;
                Int32 wID = -99, cID = -99, oID = -99, rID = -99, srID = -99, cyID = -99, cnID = -99, fID = -99, stID = -99;
                Decimal lat, lng;
                DateTime spud = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getList = new SqlCommand(selWell, dataCon);
                        using (SqlDataReader readList = getList.ExecuteReader())
                        {
                            try
                            {
                                if (readList.HasRows)
                                {
                                    while (readList.Read())
                                    {
                                        wellList.Add(readList.GetInt32(0));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readList.Close();
                                readList.Dispose();
                            }
                        }
                        foreach (var id in wellList)
                        {
                            SqlCommand getData = new SqlCommand(selData, dataCon);
                            getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = id.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            wID = readData.GetInt32(0);
                                            cID = readData.GetInt32(1);
                                            clntCo = CLNT.getClientName(cID);
                                            oID = readData.GetInt32(2);
                                            opCo = CLNT.getClientName(oID);
                                            name = readData.GetString(3);
                                            rID = readData.GetInt32(4);
                                            srID = readData.GetInt32(5);
                                            cyID = readData.GetInt32(6);
                                            cnID = readData.GetInt32(7);
                                            fID = readData.GetInt32(8);
                                            stID = readData.GetInt32(9);
                                            API = readData.GetString(10);
                                            if (String.IsNullOrEmpty(API))
                                            {
                                                API = "---";
                                            }
                                            UWI = readData.GetString(11);
                                            if (String.IsNullOrEmpty(UWI))
                                            {
                                                UWI = "---";
                                            }
                                            permit = readData.GetString(12);
                                            spud = readData.GetDateTime(13);
                                            lat = readData.GetDecimal(14);
                                            lng = readData.GetDecimal(15);
                                            welContent = getLocalWellContent(wID, name, opCo, clntCo, rID, srID, cyID, stID, cnID, fID, API, UWI, spud, permit, lat, lng, ClientDBString);
                                            value = name + "," + lat + "," + lng + "," + icon + "," + welContent;
                                            iReply.Add(value);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getLocalWellContent(Int32 WellID, String WellName, String OperatorName, String ServiceProviderName, Int32 RegionID, Int32 SubRegionID, Int32 CountryID, Int32 StateID, Int32 CountyID, Int32 FieldID, String API, String UWI, DateTime SpudDate, String WellPermit, Decimal Latitude, Decimal Longitude, String ClientDBString)
        {
            try
            {
                StringBuilder iReply = new StringBuilder();
                String Region = String.Empty, SubRegion = String.Empty, Country = String.Empty, State = String.Empty, County = String.Empty, Field = String.Empty;
                Region = DMG.getRegionName(RegionID);
                SubRegion = DMG.getSubRegionName(SubRegionID);
                Country = DMG.getCountryName(CountryID);
                State = DMG.getStateName(StateID);
                County = DMG.getCountyName(CountyID);
                Field = DMG.getFieldName(FieldID);
                List<String> runInfo = new List<String> { };
                String RunName = String.Empty, RunStatus = String.Empty, maxDepth = String.Empty, lastSurveyProcessed = String.Empty;
                runInfo = getRunInfo(WellID, ClientDBString);
                RunName = Convert.ToString(runInfo[0]);
                if (RunName.Equals("0") || String.IsNullOrEmpty(RunName))
                {
                    maxDepth = "---";
                    RunStatus = "---";
                    lastSurveyProcessed = "---";
                }
                else if (runInfo.Count < 4)
                {
                    RunStatus = Convert.ToString(runInfo[2]);
                    maxDepth = "---";
                    lastSurveyProcessed = "---";
                }
                else
                {
                    RunStatus = Convert.ToString(runInfo[2]);
                    maxDepth = Convert.ToString(runInfo[3]);
                    lastSurveyProcessed = Convert.ToString(runInfo[4]);
                }
                iReply.Append("<div id='content' runat='server' style=' font-size:smaller;'>");
                iReply.Append("<h1>" + WellName + "</h1>");
                iReply.Append("<div id='contentBody' runat='server'>");
                iReply.Append("<table><tr><td><b>Operator/Owner</b></td><td>" + OperatorName + "</td><td></td><td></td></tr>");
                iReply.Append("<tr><td><b>Provider</b></td><td>" + ServiceProviderName + "</td><td></td><td></td></tr>");
                iReply.Append("<tr><td><b>API/UWI/Permit</b></td><td>" + API + "</td><td>" + UWI + "</td><td>" + WellPermit + "</td></tr>");
                iReply.Append("<tr><td><b>Spud Date</b></td><td>" + SpudDate + "</td><td></td><td></td></tr>");
                iReply.Append("<tr><td><b>Run/Status</b></td><td>" + RunName + "</td><td>" + RunStatus + "</td><td></td></tr>");
                iReply.Append("<tr><td><b>Measured Depth/Processed</b></td><td>" + maxDepth + "</td><td>" + lastSurveyProcessed + "</td><td></td></tr>");
                iReply.Append("<tr><td><b>Field</b></td><td>" + Field + "</td><td></td><td></td></tr>");
                iReply.Append("<tr><td><b>Country/State/County</b></td><td>" + Country + "</td><td>" + State + "</td><td>" + County + "</td></tr>");
                iReply.Append("<tr><td><b>Region/Sub-Region</b></td><td>" + Region + "</td><td>" + SubRegion + "</td><td></td></tr>");
                iReply.Append("<tr><td><b>Longitude/Latitude</b></td><td>" + Longitude + "</td><td>" + Latitude + "</td><td></td></tr>");
                iReply.Append("</div>");
                iReply.Append("</div>");

                return iReply.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getServiceLandingMap(Int32 ClientID, String ControlObjectName, String ClientDBConnection)
        {
            try
            {
                StringBuilder iReply = new StringBuilder();
                System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<String> mainClntData = new List<String>(), offClntData = new List<String>(), allLocs = new List<String>();
                mainClntData = getClientMainLocation(ClientID);
                offClntData = getClientOfficeLocations(ClientID);
                allLocs.Add(mainClntData[0]);
                foreach (String val in offClntData)
                {
                    allLocs.Add(val);
                }
                String mainData = oSerializer.Serialize(allLocs);
                iReply.Append("<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>");
                iReply.Append("<script type='text/javascript' src='https://www.gstatic.com/charts/loader.js'></script>");
                iReply.Append("<script type='text/javascript'>");
                iReply.Append("var mainValues =" + mainData + ";");
                iReply.Append("var mname, mlat, mlon, micon, mContent, mapbounds, contentString, marker, i, lname, llat, llon, licon, lContent, latlng;");                
                iReply.Append("var mData = mainValues[0].split(',');");
                iReply.Append("mlat = mData[1];");
                iReply.Append("mlon = mData[2];");
                iReply.Append("function initMap() {");
                iReply.Append("var landingMapDiv = document.getElementById('" + ControlObjectName + "');");
                iReply.Append("var landingMapOptions = { center: new google.maps.LatLng(mlat, mlon), zoom: 6, maptypeId: google.maps.MapTypeId.ROADMAP };");
                iReply.Append("var map = new google.maps.Map(landingMapDiv, landingMapOptions);");
                iReply.Append("mapbounds = new google.maps.LatLngBounds();");
                iReply.Append("for(i=0; i < mainValues.length; i++){");
                iReply.Append("var lData = mainValues[i].split(',');");
                iReply.Append("lname = lData[0];");
                iReply.Append("llat = lData[1];");
                iReply.Append("llon = lData[2];");
                iReply.Append("licon = lData[3];");
                iReply.Append("lContent = lData[4];");
                iReply.Append("latlng = new google.maps.LatLng(llat, llon);");
                iReply.Append("marker = new google.maps.Marker({position: new google.maps.LatLng(llat, llon), map: map, icon: licon});");
                iReply.Append("mapbounds.extend(latlng);");
                iReply.Append("var locInfo = new google.maps.InfoWindow({content: lContent});");
                iReply.Append("google.maps.event.addListener(marker, 'click', (function (marker, lContent, locInfo) {");
                iReply.Append("return function () {");
                iReply.Append("locInfo.setContent(lContent);");
                iReply.Append("locInfo.open(map, marker);};");
                iReply.Append("})(marker, lContent, locInfo));}");
                iReply.Append("map.fitBounds(mapbounds);");                
                iReply.Append("}</script>");
                iReply.Append("<script type='text/javascript'>google.charts.load('current', {packages: ['corechart']});google.charts.setOnLoadCallback(initMap);</script>");
                iReply.Append("<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAY_dHOcYdykHMGnACxox4cGT9m4Rhf44c&callback=initMap' async defer></script>");

                return iReply.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static String getServicesWellMap(Int32 ClientID, Int32 WellID, String ControlObjectName, String ClientDBString)
        {
            try
            {
                System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<String> mainClntData = new List<String>(), offClntData = new List<String>(), WellData = new List<String>(), allLocs = new List<String>();
                mainClntData = getClientMainLocation(ClientID);
                offClntData = getClientOfficeLocations(ClientID);
                WellData = getServicesWellLocations(WellID, ClientDBString);
                allLocs.Add(mainClntData[0]);
                foreach (String val in offClntData)
                {
                    allLocs.Add(val);
                }
                foreach (String val in WellData)
                {
                    allLocs.Add(val);
                }
                StringBuilder iReply = new StringBuilder();
                String mainData = oSerializer.Serialize(allLocs);
                iReply.Append("<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>");
                iReply.Append("<script type='text/javascript' src='https://www.gstatic.com/charts/loader.js'></script>");
                iReply.Append("<script type='text/javascript'>");
                iReply.Append("var mainValues =" + mainData + ";");
                iReply.Append("var mname, mlat, mlon, micon, mContent, mapbounds, contentString, marker, i, lname, llat, llon, licon, lContent, latlng;");
                iReply.Append("var mData = mainValues[0].split(',');");
                iReply.Append("mlat = mData[1];");
                iReply.Append("mlon = mData[2];");
                iReply.Append("function initMap() {");
                iReply.Append("var landingMapDiv = document.getElementById('" + ControlObjectName + "');");
                iReply.Append("var landingMapOptions = { center: new google.maps.LatLng(mlat, mlon), zoom: 6, maptypeId: google.maps.MapTypeId.ROADMAP };");
                iReply.Append("var map = new google.maps.Map(landingMapDiv, landingMapOptions);");
                iReply.Append("mapbounds = new google.maps.LatLngBounds();");
                iReply.Append("for(i=0; i < mainValues.length; i++){");
                iReply.Append("var lData = mainValues[i].split(',');");
                iReply.Append("lname = lData[0];");
                iReply.Append("llat = lData[1];");
                iReply.Append("llon = lData[2];");
                iReply.Append("licon = lData[3];");
                iReply.Append("lContent = lData[4];");
                iReply.Append("latlng = new google.maps.LatLng(llat, llon);");
                iReply.Append("marker = new google.maps.Marker({position: new google.maps.LatLng(llat, llon), map: map, icon: licon});");
                iReply.Append("mapbounds.extend(latlng);");
                iReply.Append("var locInfo = new google.maps.InfoWindow({content: lContent});");
                iReply.Append("google.maps.event.addListener(marker, 'click', (function (marker, lContent, locInfo) {");
                iReply.Append("return function () {");
                iReply.Append("locInfo.setContent(lContent);");
                iReply.Append("locInfo.open(map, marker);};");
                iReply.Append("})(marker, lContent, locInfo));}");
                iReply.Append("map.fitBounds(mapbounds);");
                iReply.Append("}</script>");
                iReply.Append("<script type='text/javascript'>google.charts.load('current', {packages: ['corechart']});google.charts.setOnLoadCallback(initMap);</script>");
                iReply.Append("<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAY_dHOcYdykHMGnACxox4cGT9m4Rhf44c&callback=initMap' async defer></script>");
                return iReply.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static List<String> getServicesWellLocations(Int32 WellID, String ClientDBString)
        {
            try
            {
                List<String> iReply = new List<String> { };
                List<Int32> wellList = new List<Int32> { };
                String icon = "https://maps.google.com/mapfiles/kml/paddle/W.png";
                String selData = "SELECT [welID], [clntID], [optrID], [welName], [regID], [sregID], [ctyID], [cntyID], [fldID], [stID], [welAPI], [welUWI], [welPermit], [welSpudDate], [welLatitude], [welLongitude] FROM [Well] WHERE [welID] = @welID;";
                String name = String.Empty, value = String.Empty, welContent = String.Empty, permit = String.Empty, API = String.Empty, UWI = String.Empty, clntCo = String.Empty, opCo = String.Empty;
                Int32 wID = -99, cID = -99, oID = -99, rID = -99, srID = -99, cyID = -99, cnID = -99, fID = -99, stID = -99;
                Decimal lat, lng;
                DateTime spud = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();                                                
                            SqlCommand getData = new SqlCommand(selData, dataCon);
                            getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            wID = readData.GetInt32(0);
                                            cID = readData.GetInt32(1);
                                            clntCo = CLNT.getClientName(cID);
                                            oID = readData.GetInt32(2);
                                            opCo = CLNT.getClientName(oID);
                                            name = readData.GetString(3);
                                            rID = readData.GetInt32(4);
                                            srID = readData.GetInt32(5);
                                            cyID = readData.GetInt32(6);
                                            cnID = readData.GetInt32(7);
                                            fID = readData.GetInt32(8);
                                            stID = readData.GetInt32(9);
                                            API = readData.GetString(10);
                                            if (String.IsNullOrEmpty(API))
                                            {
                                                API = "---";
                                            }
                                            UWI = readData.GetString(11);
                                            if (String.IsNullOrEmpty(UWI))
                                            {
                                                UWI = "---";
                                            }
                                            permit = readData.GetString(12);
                                            spud = readData.GetDateTime(13);
                                            lat = readData.GetDecimal(14);
                                            lng = readData.GetDecimal(15);
                                            welContent = getLocalWellContent(wID, name, opCo, clntCo, rID, srID, cyID, stID, cnID, fID, API, UWI, spud, permit, lat, lng, ClientDBString);
                                            value = name + "," + lat + "," + lng + "," + icon + "," + welContent;
                                            iReply.Add(value);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }                        
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getProfileLandingMap(UP LoginProfile, String ControlObjectName, String ClientDBString)
        {
            try
            {
                String streetAddress = String.Empty, streetNumber = String.Empty, cityName = String.Empty, stateName = String.Empty, zipName = String.Empty;
                streetNumber = Convert.ToString(LoginProfile.st1);
                cityName = DMG.getCityName(Convert.ToInt32(LoginProfile.city));
                stateName = DMG.getStateName(Convert.ToInt32(LoginProfile.state));
                zipName = DMG.getZipCode(Convert.ToInt32(LoginProfile.zip));
                streetAddress = String.Format("{0} {1} {2} {3}", streetNumber, cityName, stateName, zipName);
                List<Double> latlng = MAP.getLatLngFromAddress(streetAddress);
                Double lng = Convert.ToDouble(latlng[0]);
                Double lat = Convert.ToDouble(latlng[1]);

                StringBuilder iReply = new StringBuilder();
                iReply.Append("<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>");
                iReply.Append("<script type='text/javascript' src='https://www.gstatic.com/charts/loader.js'></script>");
                iReply.Append("<script type='text/javascript'>");
                iReply.Append("var map, mlat, mlon, marker;");
                iReply.Append("mlat =" + lat + ";");
                iReply.Append("mlon =" + lng + ";");
                iReply.Append("function initMap() {");
                iReply.Append("map = new google.maps.Map(document.getElementById('" + ControlObjectName + "'),{");
                iReply.Append("zoom: 6,");
                iReply.Append("center: new google.maps.LatLng(mlat, mlon)});");
                iReply.Append("marker = new google.maps.Marker({");
                iReply.Append("position: new google.maps.LatLng(mlat, mlon),");
                iReply.Append("map: map});");
                iReply.Append("}</script>");
                iReply.Append("<script type='text/javascript'>google.charts.load('current', {packages: ['corechart']});google.charts.setOnLoadCallback(initMap);</script>");
                iReply.Append("<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAY_dHOcYdykHMGnACxox4cGT9m4Rhf44c&callback=initMap' async defer></script>");
                return iReply.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        } 

        public static ArrayList getRunDetail(Int32 RunID, String ClientDBString)
        {
            try
            {
                ArrayList iReply = new ArrayList();
                String selData = "SELECT [runName], [dmgstID] FROM [RUN] WHERE [runID] = @runID";
                Int32 rID = -99, sID = -99;
                String rName = String.Empty, status = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        rID = readData.GetInt32(0);
                                        sID = readData.GetInt32(1);
                                        rName = AST.getRunNameValue(rID);
                                        iReply.Add(rName);
                                        status = DMG.getStatusValue(sID);
                                        iReply.Add(status);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getMaxRunForWell(Int32 WellID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99, WellSectionID = -99;
                Dictionary<Int32, Int32> WellSectionList = OP.getWellSectionForWellList(WellID, ClientDBString);
                List<Int32> runList = new List<Int32>();
                foreach (KeyValuePair<Int32, Int32> pair in WellSectionList)
                {
                    WellSectionID = pair.Value;
                    runList = OP.getRunForWellSectionList(WellID, WellSectionID, ClientDBString);
                }
                if (runList.Count > 0)
                {
                    iReply = runList.Max();
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
       
        public static List<String> getRunInfo(Int32 WellID, String ClientDBString)
        {
            try
            {
                List<String> iReply = new List<String> { };
                String rowCount = "SELECT count(resID) FROM [RawQCResults] WHERE [runID] = @runID";
                String maxDepth = "SELECT Max(Depth) FROM [RawQCResults] WHERE [runID] = @runID";
                String pTime = "SELECT [pTime] FROM [RawQCResults] WHERE [runID] = @runID AND [Depth] = @depth";
                Int32 rID = -99, count = -99, rCount = -99;
                String status = String.Empty, rName = String.Empty;
                DateTime lastU = DateTime.Now;
                Decimal mDepth = -99.00M;
                ArrayList rInfo = new ArrayList();
                rID = getMaxRunForWell(WellID, ClientDBString);
                if (!rID.Equals(-99))
                {
                    //count = getRunCount(WellID, ClientDBString);
                    rInfo = getRunDetail(rID, ClientDBString);
                    rName = Convert.ToString(rInfo[0]);
                    status = Convert.ToString(rInfo[1]);
                    //iReply.Add(Convert.ToString(count));
                    iReply.Add(Convert.ToString(rID));
                    iReply.Add(rName);
                    iReply.Add(status);
                    if (rID > 0)
                    {
                        using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                        {
                            try
                            {
                                dataCon.Open();
                                SqlCommand getRCount = new SqlCommand(rowCount, dataCon);
                                getRCount.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = rID.ToString();
                                using (SqlDataReader readCount = getRCount.ExecuteReader())
                                {
                                    try
                                    {
                                        if (readCount.HasRows)
                                        {
                                            while (readCount.Read())
                                            {
                                                rCount = readCount.GetInt32(0);
                                            }
                                        }
                                        else
                                        {
                                            rCount = -1;
                                        }
                                    }
                                    catch (Exception ex)
                                    { throw new System.Exception(ex.ToString()); }
                                    finally
                                    {
                                        readCount.Close();
                                        readCount.Dispose();
                                    }
                                }
                                if (rCount > 0)
                                {
                                    SqlCommand getDepth = new SqlCommand(maxDepth, dataCon);
                                    getDepth.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = rID.ToString();
                                    using (SqlDataReader readDepth = getDepth.ExecuteReader())
                                    {
                                        try
                                        {
                                            if (readDepth.HasRows)
                                            {
                                                while (readDepth.Read())
                                                {
                                                    mDepth = readDepth.GetDecimal(0);
                                                    if (mDepth.Equals(null))
                                                    {
                                                        mDepth = 0.00M;
                                                    }
                                                    iReply.Add(String.Format("{0} {1}", mDepth, "Meters"));
                                                }
                                            }
                                            else
                                            {
                                                mDepth = 0.00M;
                                                iReply.Add(String.Format("{0} {1}", mDepth, "Meters"));
                                            }
                                        }
                                        catch (Exception ex)
                                        { throw new System.Exception(ex.ToString()); }
                                        finally
                                        {
                                            readDepth.Close();
                                            readDepth.Dispose();
                                        }
                                    }
                                    SqlCommand getTime = new SqlCommand(pTime, dataCon);
                                    getTime.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = rID.ToString();
                                    getTime.Parameters.AddWithValue("@depth", SqlDbType.Int).Value = mDepth.ToString();
                                    using (SqlDataReader readTime = getTime.ExecuteReader())
                                    {
                                        try
                                        {
                                            if (readTime.HasRows)
                                            {
                                                while (readTime.Read())
                                                {
                                                    lastU = readTime.GetDateTime(0);
                                                    iReply.Add(Convert.ToString(lastU));
                                                }
                                            }
                                            else
                                            {
                                                lastU = DateTime.Now;
                                                iReply.Add(Convert.ToString(lastU));
                                            }
                                        }
                                        catch (Exception ex)
                                        { throw new System.Exception(ex.ToString()); }
                                        finally
                                        {
                                            readTime.Close();
                                            readTime.Dispose();
                                        }
                                    }
                                }
                                else
                                {
                                    mDepth = 0.00M;
                                    iReply.Add(String.Format("{0} {1}", mDepth, "Meters"));
                                    lastU = DateTime.Now;
                                    iReply.Add(Convert.ToString(lastU));
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                dataCon.Close();
                                dataCon.Dispose();
                            }
                        }
                    }
                    else
                    {
                        iReply.Add("0");
                    }
                }
                else
                {
                    iReply.Add("0");
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getUserAddressContent(String title, String Street1, String Street2, String Street3, String city, String state, String zip, String country, String phone)
        {
            try
            {
                String street = String.Empty;
                if (!String.IsNullOrEmpty(Street2))
                {

                }
                StringBuilder iReply = new StringBuilder();
                iReply.Append("<div id='content' runat='server'>");
                iReply.Append("<div id='contentBody' runat='server'>");
                iReply.Append("<table><tr><td><b>Title</b></td><td>" + title + "</td></tr>");
                iReply.Append("<tr><td><b>Street</b></td><td>" + street + "</td></tr>");
                iReply.Append("<tr><td><b>City/Town</b></td><td>" + city + "</td></tr>");
                iReply.Append("<tr><td><b>State/Province</b></td><td>" + state + "</td></tr>");
                iReply.Append("<tr><td><b>Zip/Postal Code</b></td><td>" + zip + "</td></tr>");
                iReply.Append("<tr><td><b>Country/Nation</b></td><td>" + country + "</td></tr>");
                iReply.Append("<tr><td><b>Phone</b></td><td>" + phone + "</td></tr>");
                iReply.Append("</table>");
                iReply.Append("</div>");
                iReply.Append("</div>");
                return iReply.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}