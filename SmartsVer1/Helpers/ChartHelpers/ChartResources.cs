﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Linq;
using System.Collections;
using System.Globalization;
using System.Data.SqlClient;
using System.Web.SessionState;
using System.Web.Configuration;
using System.Collections.Generic;

namespace SmartsVer1.Helpers.ChartHelpers
{
    public class ChartResources
    {

        void main()
        { }

        public static bool IsOdd(decimal value)
        {
            return value % 2 != 0;
        }

        public static decimal getScale(decimal div, decimal max)
        {
            decimal add = -1.00M;

            if (IsOdd(div))
            {
                div = (div + 1);

                if (div > 20)
                {
                    //add = (max * 18) / 2;
                    add = (max * 14) / 3;
                }
                if ((div > 15) && (div <= 20))
                {
                    //add = (max * 12) / 2;
                    add = (max * 9) / 3;
                }
                if ((div > 10) && (div <= 15))
                {
                    //add = (max * 7) / 2;
                    add = (max * 5) / 3;
                }
                if ((div > 5) && (div <= 10))
                {
                    //add = (max * 3) / 2;
                    add = (max * 2) / 3;
                }
                if (div <= 5)
                {
                    add = (max * 1) / 3;
                }

            }
            else
            {

                if (div > 20)
                {
                    //add = (max * 18) / 2;
                    add = (max * 14) / 3;
                }
                if ((div > 15) && (div <= 20))
                {
                    //add = (max * 12) / 2;
                    add = (max * 9) / 3;
                }
                if ((div > 10) && (div <= 15))
                {
                    //add = (max * 7) / 2;
                    add = (max * 5) / 3;
                }
                if ((div > 5) && (div <= 10))
                {
                    //add = (max * 3) / 2;
                    add = (max * 2) / 3;
                }
                if (div <= 5)
                {
                    add = (max * 1) / 3;
                }
            }

            return Math.Round( add , 1 );
        }

        public static decimal getMaxTVDforWell(Int32 WellID, String dbConn)
        {
            try
            {
                decimal maxdepth = 0;
                String SelectQuery = "Select COALESCE( max(rwTVD) , 15000 ) FROM [RawQCResults] WHERE [welID] = @welID ; ";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        //count++;
                                        maxdepth = readData.GetDecimal(0);
                                    }
                                }
                                else
                                {
                                    maxdepth = 1000;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close(); // this is used for SQLDataReader
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return maxdepth;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }


        }

        public static decimal getMaxTVDforWellPlan(Int32 WellID, String dbConn)
        {
            try
            {
                decimal maxdepth = 0;
                String SelectQuery = "Select COALESCE( max(tvd) , 15000 ) FROM [WellPlanCalculatedData] WHERE [welID] = @welID  ";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        //count++;
                                        maxdepth = readData.GetDecimal(0);
                                    }
                                }
                                else
                                {
                                    maxdepth = 1000;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close(); // this is used for SQLDataReader
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return maxdepth;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }


        }

        public static decimal getMaxTVDforWellsInComparrison(List<int> WellID, String dbConn)
        {
            try
            {
                decimal maxdepth = 0;

                String getwellid = "";

                getwellid = "(''";
                foreach (int id in WellID)
                {
                    getwellid += "," + id.ToString();
                }
                getwellid += ")";

                String SelectQuery = "Select max(rwTVD) ";
                SelectQuery += " FROM [RawQCResults] as RQCR , [Well] ";
                SelectQuery += " WHERE [Well].welID = RQCR.welID ";
                SelectQuery += " AND RQCR.welID in ";
                SelectQuery += getwellid;
                SelectQuery += " ; ";

                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        //cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        //count++;
                                        maxdepth = readData.GetDecimal(0);
                                    }
                                }
                                else
                                {
                                    maxdepth = 1000;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close(); // this is used for SQLDataReader
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return maxdepth;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }


        }

        public static DataTable getScallingFactorChartInfo_WPlan(Int32 WellID, String dbConn)
        {
            try
            {


                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn maxNorthing = iReply.Columns.Add("maxNorthing", typeof(Decimal));
                DataColumn minNorthing = iReply.Columns.Add("minNorthing", typeof(Decimal));
                DataColumn maxEasting = iReply.Columns.Add("maxEasting", typeof(Decimal));
                DataColumn minEasting = iReply.Columns.Add("minEasting", typeof(Decimal));


                String SelectQuery = "select  cast(max(northing) as decimal(10,0))  maxNorthing , cast(min(cummDeltaNorthing) as decimal(10,0)) minCmNorthing , cast(max(cummDeltaNorthing) as decimal(10,0)) maxCmNorthing , ";
                SelectQuery += " cast(max(easting) as decimal(10,0)) maxEasting, cast(min(cummDeltaEasting) as decimal(10,0)) minCmEasting, cast(max(cummDeltaEasting) as decimal(10,0)) maxCmEasting ";
                SelectQuery += " FROM [WellPlanCalculatedData] where welID = @welID ; ";

                Decimal max_northing = -99.00M, min_cm_northing = -99.00M, max_cm_northing = -99.00M, max_easting = -99.00M, min_cm_easting = -99.00M, max_cm_easting = -99.00M;
                Decimal div = -1.00M, scaler = -1.00M, scale_northing_min = -1.00M, scale_northing_max = -1.00M, scale_easting_min = -1.00M, scale_easting_max = -1.00M;


                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        max_northing = readData.GetDecimal(0);
                                        if (max_northing.Equals(0)) { max_northing = 1; }
                                        min_cm_northing = readData.GetDecimal(1);
                                        max_cm_northing = readData.GetDecimal(2);
                                        max_easting = readData.GetDecimal(3);
                                        if (max_easting.Equals(0)) { max_easting = 1; }
                                        min_cm_easting = readData.GetDecimal(4);
                                        max_cm_easting = readData.GetDecimal(5);
                                    }
                                }

                                else
                                {
                                    max_northing = 1; min_cm_northing = 0; max_cm_northing = 0; max_easting = 1; min_cm_easting = 0; max_cm_easting = 0;

                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close(); // this is used for SQLDataReader
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }

                // ********************************************************************************                
                // Calculation to get the X , Y axis limits
                // ********************************************************************************

                if (max_northing < max_easting)
                {
                    div = Math.Round( max_easting / max_northing, 0, MidpointRounding.ToEven);

                    scaler = getScale(div, max_northing);

                    scale_northing_min = min_cm_northing - scaler;
                    scale_northing_max = max_cm_northing + scaler;
                    
                    scale_easting_min = min_cm_easting;
                    scale_easting_max = max_cm_easting;

                    iReply.Rows.Add(scale_northing_max, scale_northing_min, scale_easting_max, scale_easting_min);
                    iReply.AcceptChanges();

                }

                if (max_easting < max_northing)
                {
                    div = Math.Round(max_northing / max_easting, 0, MidpointRounding.ToEven);

                    scaler = getScale(div, max_northing);

                    scale_northing_min = min_cm_northing;
                    scale_northing_max = max_cm_northing;

                    scale_easting_min = min_cm_easting - scaler;
                    scale_easting_max = max_cm_easting + scaler;

                    iReply.Rows.Add(scale_northing_max, scale_northing_min, scale_easting_max, scale_easting_min);
                    iReply.AcceptChanges();

                }



                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }


        }

        public static DataTable getScallingFactorChartInfo_Comparison(List<int> WellID, String dbConn)
        {
            try
            {
                String getwellid = "";

                getwellid = "(''";
                foreach (int id in WellID)
                {
                    getwellid += "," + id.ToString();
                }
                getwellid += ")";

                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn maxNorthing = iReply.Columns.Add("maxNorthing", typeof(Decimal));
                DataColumn minNorthing = iReply.Columns.Add("minNorthing", typeof(Decimal));
                DataColumn maxEasting = iReply.Columns.Add("maxEasting", typeof(Decimal));
                DataColumn minEasting = iReply.Columns.Add("minEasting", typeof(Decimal));


                String SelectQuery = "select  cast(max(northing) as decimal(10,0))  maxNorthing , cast(min(cummDeltaNorthing) as decimal(10,0)) minCmNorthing , cast(max(cummDeltaNorthing) as decimal(10,0)) maxCmNorthing , ";
                SelectQuery += " cast(max(easting) as decimal(10,0)) maxEasting, cast(min(cummDeltaEasting) as decimal(10,0)) minCmEasting, cast(max(cummDeltaEasting) as decimal(10,0)) maxCmEasting ";
                SelectQuery += " FROM [WellPlanCalculatedData] where welID in ";
                SelectQuery += getwellid;
                SelectQuery += ";";

                Decimal max_northing = -99.00M, min_cm_northing = -99.00M, max_cm_northing = -99.00M, max_easting = -99.00M, min_cm_easting = -99.00M, max_cm_easting = -99.00M;
                Decimal div = -1.00M, scaler = -1.00M, scale_northing_min = -1.00M, scale_northing_max = -1.00M, scale_easting_min = -1.00M, scale_easting_max = -1.00M;


                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        //cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        max_northing = readData.GetDecimal(0);
                                        if (max_northing.Equals(0)) { max_northing = 1; }
                                        min_cm_northing = readData.GetDecimal(1);
                                        max_cm_northing = readData.GetDecimal(2);
                                        max_easting = readData.GetDecimal(3);
                                        if (max_easting.Equals(0)) { max_easting = 1; }
                                        min_cm_easting = readData.GetDecimal(4);
                                        max_cm_easting = readData.GetDecimal(5);
                                    }
                                }

                                else
                                {
                                    max_northing = 1; min_cm_northing = 0; max_cm_northing = 0; max_easting = 1; min_cm_easting = 0; max_cm_easting = 0;

                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close(); // this is used for SQLDataReader
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }

                // ********************************************************************************                
                // Calculation to get the X , Y axis limits
                // ********************************************************************************

                if (max_northing < max_easting)
                {
                    div = Math.Round(max_easting / max_northing, 0, MidpointRounding.ToEven);

                    scaler = getScale(div, max_northing);

                    scale_northing_min = min_cm_northing - scaler;
                    scale_northing_max = max_cm_northing + scaler;

                    scale_easting_min = min_cm_easting;
                    scale_easting_max = max_cm_easting;

                    iReply.Rows.Add(scale_northing_max, scale_northing_min, scale_easting_max, scale_easting_min);
                    iReply.AcceptChanges();

                }

                if (max_easting < max_northing)
                {
                    div = Math.Round(max_northing / max_easting, 0, MidpointRounding.ToEven);

                    scaler = getScale(div, max_northing);

                    scale_northing_min = min_cm_northing;
                    scale_northing_max = max_cm_northing;

                    scale_easting_min = min_cm_easting - scaler;
                    scale_easting_max = max_cm_easting + scaler;

                    iReply.Rows.Add(scale_northing_max, scale_northing_min, scale_easting_max, scale_easting_min);
                    iReply.AcceptChanges();

                }



                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }


        }

        public static DataTable getScallingFactorChartInfo(Int32 WellID, String dbConn)
        {
            try
            {


                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn maxNorthing = iReply.Columns.Add("maxNorthing", typeof(Decimal));
                DataColumn minNorthing = iReply.Columns.Add("minNorthing", typeof(Decimal));
                DataColumn maxEasting = iReply.Columns.Add("maxEasting", typeof(Decimal));
                DataColumn minEasting = iReply.Columns.Add("minEasting", typeof(Decimal));

                String SelectQuery = "  select  cast(max(rwNorthing) as decimal(10,0))  maxNorthing , cast(min( rwCummNorthing) as decimal(10,0)) minCmNorthing , cast(max(rwCummNorthing) as decimal(10,0)) maxCmNorthing , ";
                SelectQuery += " cast(max(rwEasting) as decimal(10,0)) maxEasting, cast(min(rwCummEasting) as decimal(10,0)) minCmEasting, cast(max(rwCummEasting) as decimal(10,0)) maxCmEasting ";
                SelectQuery += " FROM [RawQCResults] where welID = @welID ; ";

                Decimal max_northing = -99.00M, min_cm_northing = -99.00M, max_cm_northing = -99.00M, max_easting = -99.00M, min_cm_easting = -99.00M, max_cm_easting = -99.00M;
                Decimal div = -1.00M, scaler = -1.00M, scale_northing_min = -1.00M, scale_northing_max = -1.00M, scale_easting_min = -1.00M, scale_easting_max = -1.00M;


                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        max_northing = readData.GetDecimal(0);
                                        if (max_northing.Equals(0)) { max_northing = 1; }
                                        min_cm_northing = readData.GetDecimal(1);
                                        max_cm_northing = readData.GetDecimal(2);
                                        max_easting = readData.GetDecimal(3);
                                        if (max_easting.Equals(0)) { max_easting = 1; }
                                        min_cm_easting = readData.GetDecimal(4);
                                        max_cm_easting = readData.GetDecimal(5);
                                    }
                                }

                                else
                                {
                                    max_northing = 1; min_cm_northing = 0; max_cm_northing = 0; max_easting = 1; min_cm_easting = 0; max_cm_easting = 0;

                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close(); // this is used for SQLDataReader
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }

                // ********************************************************************************                
                // Calculation to get the X , Y axis limits
                // ********************************************************************************

                if (max_northing < max_easting)
                {
                    div = Math.Round(max_easting / max_northing, 0, MidpointRounding.ToEven);

                    scaler = getScale(div, max_northing);

                    scale_northing_min = min_cm_northing - scaler;
                    scale_northing_max = max_cm_northing + scaler;

                    scale_easting_min = min_cm_easting;
                    scale_easting_max = max_cm_easting;

                    iReply.Rows.Add(scale_northing_max, scale_northing_min, scale_easting_max, scale_easting_min);
                    iReply.AcceptChanges();

                }

                if (max_easting < max_northing)
                {
                    div = Math.Round(max_northing / max_easting, 0, MidpointRounding.ToEven);

                    scaler = getScale(div, max_northing);

                    scale_northing_min = min_cm_northing;
                    scale_northing_max = max_cm_northing;

                    scale_easting_min = min_cm_easting - scaler;
                    scale_easting_max = max_cm_easting + scaler;

                    iReply.Rows.Add(scale_northing_max, scale_northing_min, scale_easting_max, scale_easting_min);
                    iReply.AcceptChanges();

                }



                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }


        }

        public static DataTable getDataTableforQCWellGraphWithPlan(Int32 WellID, String dbConn)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn Survey_number = iReply.Columns.Add("Survey_number", typeof(Int32));
                DataColumn planInclination = iReply.Columns.Add("planInclination", typeof(Decimal));
                DataColumn planAzimuth = iReply.Columns.Add("planAzimuth", typeof(Decimal));
                DataColumn planDepth = iReply.Columns.Add("planDepth", typeof(Decimal));
                DataColumn planTVD = iReply.Columns.Add("planTVD", typeof(Decimal));
                DataColumn planNorthing = iReply.Columns.Add("planNorthing", typeof(Decimal));
                DataColumn planEasting = iReply.Columns.Add("planEasting", typeof(Decimal));
                DataColumn planPlotDepth = iReply.Columns.Add("planPlotDepth", typeof(Decimal));
                DataColumn planCummDeltaNorthing = iReply.Columns.Add("planCummDeltaNorthing", typeof(Decimal));
                DataColumn planCummDeltaEasting = iReply.Columns.Add("planCummDeltaEasting", typeof(Decimal));
                Int32 count = 0;
                Decimal Pinc = -99.0000M, Pazm = -99.00M, Pdepth = -99.00M, Ptvd = -99.00M, Pplotdepth = -99.00M, Pnorthing = -99.00M, Peasting = -99.00M , PcummNorthing = -1.00M , PcummEasting = -1.00M;

                String SelectQueryPlan = "Select cast ( ROW_NUMBER() over (order by md) as int), [inc], [azm], [md] , [tvd] , [northing] , [easting], [tvd] , [cummDeltaNorthing] , [cummDeltaEasting]  FROM [WellPlanCalculatedData] WHERE [welID] = @welID ";
                SelectQueryPlan += " and format( ctime , 'yyyyMMddHHmm' ) in ";
                SelectQueryPlan += "(select  format( max(ctime) , 'yyyyMMddHHmm' ) FROM [WellPlanCalculatedData] where welID = @welID) ";
                SelectQueryPlan += " ORDER BY [md] ASC ";

                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmdPlan = new SqlCommand(SelectQueryPlan, dataCon);

                        cmdPlan.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = cmdPlan.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        //count++;
                                        count = readData.GetInt32(0);
                                        Pinc = readData.GetDecimal(1);
                                        Pazm = readData.GetDecimal(2);
                                        Pdepth = readData.GetDecimal(3);
                                        Ptvd = readData.GetDecimal(4);
                                        Pnorthing = readData.GetDecimal(5);
                                        Peasting = readData.GetDecimal(6);
                                        Pplotdepth = readData.GetDecimal(7);
                                        PcummNorthing = readData.GetDecimal(8);
                                        PcummEasting = readData.GetDecimal(9);
                                        iReply.Rows.Add(count, Pinc, Pazm, Pdepth, Ptvd, Pnorthing, Peasting, Pplotdepth, PcummNorthing, PcummEasting);
                                        iReply.AcceptChanges();
                                    }
                                }

                                else
                                {
                                    count = 0; Pinc = 0; Pazm = 0; Pdepth = 0; Ptvd = 0; Pnorthing = 0; Peasting = 0; Pplotdepth = 0; PcummEasting = 0; PcummNorthing = 0;
                                    iReply.Rows.Add(count, Pinc, Pazm, Pdepth, Ptvd, Pnorthing, Peasting, Pplotdepth, PcummNorthing, PcummEasting);
                                    iReply.AcceptChanges();
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close(); // this is used for SQLDataReader
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getDataTableforQCWellGraph(Int32 WellID, String dbConn)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn Survey_number = iReply.Columns.Add("Survey_number", typeof(Int32));
                DataColumn Inclination = iReply.Columns.Add("Inclination", typeof(Decimal));
                DataColumn Azimuth = iReply.Columns.Add("Azimuth", typeof(Decimal));
                DataColumn Dip = iReply.Columns.Add("Dip", typeof(Decimal));
                DataColumn BTotal = iReply.Columns.Add("B-Total", typeof(Decimal));
                DataColumn MDepth = iReply.Columns.Add("MDepth", typeof(Decimal));
                DataColumn TVD = iReply.Columns.Add("TVD", typeof(Decimal));
                DataColumn Northing = iReply.Columns.Add("Northing", typeof(Decimal));
                DataColumn Easting = iReply.Columns.Add("Easting", typeof(Decimal));
                DataColumn PlotDepth = iReply.Columns.Add("Depth", typeof(Decimal));
                DataColumn CummDeltaNorthing = iReply.Columns.Add("CummDeltaNorthing", typeof(Decimal));
                DataColumn CummDeltaEasting = iReply.Columns.Add("CummDeltaEasting", typeof(Decimal));

                Int32 count = 0;
                Decimal inc = -99.0000M, azm = -99.00M, dp = -99.00M, bt = -99.00M, depth = -99.00M, plotdepth = -99.00M, tvd = -99.00M, northing = -99.00M, easting = -99.00M , CEasting = -1.00M , CNorthing = -1.00M ;
                //String SelectQuery = "Select cast ( ROW_NUMBER() over (order by Depth) as int), [colT], [colDR], [colBA], [colCA] , [Depth] , [rwTVD] , [rwNorthing] , [rwEasting] , [rwTVD] FROM [RawQCResults] WHERE [welID] = @welID ORDER BY [Depth] ASC ";
                String SelectQuery = "( Select  cast ( ROW_NUMBER() over (order by Depth) as int) as rn , [colT] as INC, ([colDR] - [colGE]) as AZ, [colBA] as DIP, [colCA] as BT, [Depth] as Depth , [bhTVD] as TVD, [bhNorthing] as N, [bhEasting] as E, [bhTVD] as TVD , [bhCummNorthing] as CN ,[bhCummEasting] as CE FROM [RawQCResults] WHERE [acID] = 1 AND [welID] = @welID ";
                SelectQuery += "            UNION ALL ";
                SelectQuery += "            Select  cast ( ROW_NUMBER() over (order by Depth) as int) as rn , [colT] as INC, [colDD] as AZ, [colBA] as DIP, [colCA] as BT, [Depth] as Depth, [scTVD] as TVD , [scNorthing] as N , [scEasting] as E, [scTVD] as TVD , [scCummNorthing] as CN ,[scCummEasting] as CE FROM [RawQCResults] WHERE [acID] = 5 AND [welID] = @welID  ";
                SelectQuery += "            UNION ALL ";
                SelectQuery += "            Select  cast ( ROW_NUMBER() over (order by Depth) as int) as rn , [colT]as INC , [colDR] as AZ, [colBA] as DIP, [colCA] as BT, [Depth] as Depth, [rwTVD] as TVD , [rwNorthing] as N , [rwEasting] as E, [rwTVD] as TVD ,  [rwCummNorthing] as CN ,[rwCummEasting] as CE FROM [RawQCResults] WHERE [acID] = 4 AND [welID] = @welID  ) ";
                SelectQuery += "            ORDER BY [Depth] ASC ";

                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        //count++;
                                        count = readData.GetInt32(0);
                                        inc = readData.GetDecimal(1);
                                        azm = readData.GetDecimal(2);
                                        dp = readData.GetDecimal(3);
                                        bt = readData.GetDecimal(4);
                                        depth = readData.GetDecimal(5);
                                        tvd = readData.GetDecimal(6);
                                        northing = readData.GetDecimal(7);
                                        easting = readData.GetDecimal(8);
                                        plotdepth = readData.GetDecimal(9);
                                        CNorthing = readData.GetDecimal(10);
                                        CEasting = readData.GetDecimal(11);

                                        iReply.Rows.Add(count, inc, azm, dp, bt, depth, tvd, northing, easting, plotdepth, CNorthing, CEasting);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close(); // this is used for SQLDataReader
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getDataTableforQCWellGraph_3DPlacement(Int32 WellID, String dbConn)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn Survey_number = iReply.Columns.Add("Survey_number", typeof(Int32));
                DataColumn Inclination = iReply.Columns.Add("Inclination", typeof(Decimal));
                DataColumn Azimuth = iReply.Columns.Add("Azimuth", typeof(Decimal));
                DataColumn MDepth = iReply.Columns.Add("MDepth", typeof(Decimal));
                DataColumn TVD = iReply.Columns.Add("TVD", typeof(Decimal));
                DataColumn Northing = iReply.Columns.Add("Northing", typeof(Decimal));
                DataColumn Easting = iReply.Columns.Add("Easting", typeof(Decimal));
                DataColumn PlotDepth = iReply.Columns.Add("Depth", typeof(Decimal));
                DataColumn CummDeltaNorthing = iReply.Columns.Add("CummDeltaNorthing", typeof(Decimal));
                DataColumn CummDeltaEasting = iReply.Columns.Add("CummDeltaEasting", typeof(Decimal));

                Int32 count = 0;
                Decimal inc = -99.0000M, azm = -99.00M,  depth = -99.00M, plotdepth = -99.00M, tvd = -99.00M, northing = -99.00M, easting = -99.00M, CEasting = -1.00M, CNorthing = -1.00M;
                String SelectQuery = "( Select  cast ( ROW_NUMBER() over (order by md) as int) as rn , [bhinc] as INC, [bhazm]as AZ,  [md] as Depth , [bhtvd] as TVD, [bhnorthing] as N, [bheasting] as E, [bhtvd] as TVD , [bhcummDeltaNorthing] as CN ,[bhcummDeltaEasting] as CE FROM [WellPlacementData] WHERE [acID] = 1 AND [welID] = @welID ";
                SelectQuery += "            UNION ALL ";
                SelectQuery += "            Select  cast ( ROW_NUMBER() over (order by md) as int) as rn , [scinc] as INC, [scazm]as AZ,  [md] as Depth , [sctvd] as TVD, [scnorthing] as N, [sceasting] as E, [sctvd] as TVD , [sccummDeltaNorthing] as CN ,[sccummDeltaEasting] as CE FROM [WellPlacementData] WHERE [acID] = 5 AND [welID] = @welID  ";
                SelectQuery += "            UNION ALL ";
                SelectQuery += "            Select  cast ( ROW_NUMBER() over (order by md) as int) as rn , [rwinc]as INC , [rwazm] as AZ, [md] as Depth, [rwtvd] as TVD , [rwnorthing] as N , [rweasting] as E, [rwtvd] as TVD ,  [rwcummDeltaNorthing] as CN ,[rwcummDeltaEasting] as CE FROM [WellPlacementData] WHERE [acID] = 4 AND [welID] = @welID  ) ";
                SelectQuery += "            ORDER BY [Depth] ASC ";

                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        //count++;
                                        count = readData.GetInt32(0);
                                        inc = readData.GetDecimal(1);
                                        azm = readData.GetDecimal(2);
                                        depth = readData.GetDecimal(3);
                                        tvd = readData.GetDecimal(4);
                                        northing = readData.GetDecimal(5);
                                        easting = readData.GetDecimal(6);
                                        plotdepth = readData.GetDecimal(7);
                                        CNorthing = readData.GetDecimal(8);
                                        CEasting = readData.GetDecimal(9);

                                        iReply.Rows.Add(count, inc, azm, depth, tvd, northing, easting, plotdepth, CNorthing, CEasting);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close(); // this is used for SQLDataReader
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getDataTableforQCWellGraphRawBased(Int32 WellID, String dbConn)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn Survey_number = iReply.Columns.Add("Survey_number", typeof(Int32));
                DataColumn Inclination = iReply.Columns.Add("Inclination", typeof(Decimal));
                DataColumn Azimuth = iReply.Columns.Add("Azimuth", typeof(Decimal));
                DataColumn MDepth = iReply.Columns.Add("MDepth", typeof(Decimal));
                DataColumn TVD = iReply.Columns.Add("TVD", typeof(Decimal));
                DataColumn Northing = iReply.Columns.Add("Northing", typeof(Decimal));
                DataColumn Easting = iReply.Columns.Add("Easting", typeof(Decimal));
                DataColumn PlotDepth = iReply.Columns.Add("Depth", typeof(Decimal));
                DataColumn CummDeltaNorthing = iReply.Columns.Add("CummDeltaNorthing", typeof(Decimal));
                DataColumn CummDeltaEasting = iReply.Columns.Add("CummDeltaEasting", typeof(Decimal));
                Int32 count = 0;
                Decimal inc = -99.0000M, azm = -99.00M, depth = -99.00M, plotdepth = -99.00M, tvd = -99.00M, northing = -99.00M, easting = -99.00M , CNorthing = -1.00M , CEasting = -1.00M;
                String SelectQuery = "Select cast ( ROW_NUMBER() over (order by md) as int) as rn , [rwinc]as INC , [rwazm] as AZ, [md] as Depth, [rwtvd] as TVD , [rwnorthing] as N , [rweasting] as E, [rwtvd] as TVD ,  [rwcummDeltaNorthing] as CN ,[rwcummDeltaEasting] as CE FROM [WellPlacementData] WHERE [welID] = @welID ORDER BY [Depth] ASC ";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        //count++;
                                        count = readData.GetInt32(0);
                                        inc = readData.GetDecimal(1);
                                        azm = readData.GetDecimal(2);
                                        depth = readData.GetDecimal(3);
                                        tvd = readData.GetDecimal(4);
                                        northing = readData.GetDecimal(5);
                                        easting = readData.GetDecimal(6);
                                        plotdepth = readData.GetDecimal(7);
                                        CNorthing = readData.GetDecimal(8);
                                        CEasting = readData.GetDecimal(9);

                                        iReply.Rows.Add(count, inc, azm, depth, tvd, northing, easting, plotdepth, CNorthing, CEasting);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close(); // this is used for SQLDataReader
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getDataTableforQCWellGraphBHABased(Int32 WellID, String dbConn)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn Survey_number = iReply.Columns.Add("Survey_number", typeof(Int32));
                DataColumn Inclination = iReply.Columns.Add("Inclination", typeof(Decimal));
                DataColumn Azimuth = iReply.Columns.Add("Azimuth", typeof(Decimal));
                DataColumn MDepth = iReply.Columns.Add("MDepth", typeof(Decimal));
                DataColumn TVD = iReply.Columns.Add("TVD", typeof(Decimal));
                DataColumn Northing = iReply.Columns.Add("Northing", typeof(Decimal));
                DataColumn Easting = iReply.Columns.Add("Easting", typeof(Decimal));
                DataColumn PlotDepth = iReply.Columns.Add("Depth", typeof(Decimal));
                DataColumn CummDeltaNorthing = iReply.Columns.Add("CummDeltaNorthing", typeof(Decimal));
                DataColumn CummDeltaEasting = iReply.Columns.Add("CummDeltaEasting", typeof(Decimal));
                Int32 count = 0;
                Decimal inc = -99.0000M, azm = -99.00M, depth = -99.00M, plotdepth = -99.00M, tvd = -99.00M, northing = -99.00M, easting = -99.00M , CNorthing = -1.00M , CEasting = -1.00M;
                String SelectQuery = "Select cast ( ROW_NUMBER() over (order by md) as int) as rn , [bhinc]as INC , [bhazm] as AZ, [md] as Depth, [bhtvd] as TVD , [bhnorthing] as N , [bheasting] as E, [bhtvd] as TVD ,  [bhcummDeltaNorthing] as CN ,[bhcummDeltaEasting] as CE FROM [WellPlacementData] WHERE [welID] = @welID ORDER BY [Depth] ASC ";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        //count++;
                                        count = readData.GetInt32(0);
                                        inc = readData.GetDecimal(1);
                                        azm = readData.GetDecimal(2);
                                        depth = readData.GetDecimal(3);
                                        tvd = readData.GetDecimal(4);
                                        northing = readData.GetDecimal(5);
                                        easting = readData.GetDecimal(6);
                                        plotdepth = readData.GetDecimal(7);
                                        CNorthing = readData.GetDecimal(8);
                                        CEasting = readData.GetDecimal(9);
                                        iReply.Rows.Add(count, inc, azm, depth, tvd, northing, easting, plotdepth , CNorthing , CEasting);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close(); // this is used for SQLDataReader
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getDataTableforQCWellGraphSCBased(Int32 WellID, String dbConn)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn Survey_number = iReply.Columns.Add("Survey_number", typeof(Int32));
                DataColumn Inclination = iReply.Columns.Add("Inclination", typeof(Decimal));
                DataColumn Azimuth = iReply.Columns.Add("Azimuth", typeof(Decimal));
                DataColumn MDepth = iReply.Columns.Add("MDepth", typeof(Decimal));
                DataColumn TVD = iReply.Columns.Add("TVD", typeof(Decimal));
                DataColumn Northing = iReply.Columns.Add("Northing", typeof(Decimal));
                DataColumn Easting = iReply.Columns.Add("Easting", typeof(Decimal));
                DataColumn PlotDepth = iReply.Columns.Add("Depth", typeof(Decimal));
                DataColumn CummDeltaNorthing = iReply.Columns.Add("CummDeltaNorthing", typeof(Decimal));
                DataColumn CummDeltaEasting = iReply.Columns.Add("CummDeltaEasting", typeof(Decimal));
                Int32 count = 0;
                Decimal inc = -99.0000M, azm = -99.00M, depth = -99.00M, plotdepth = -99.00M, tvd = -99.00M, northing = -99.00M, easting = -99.00M , CNorthing = -1.00M , CEasting = -1.00M;
                String SelectQuery = "Select cast ( ROW_NUMBER() over (order by md) as int) as rn , [scinc] as INC, [scazm]as AZ,  [md] as Depth , [sctvd] as TVD, [scnorthing] as N, [sceasting] as E, [sctvd] as TVD , [sccummDeltaNorthing] as CN ,[sccummDeltaEasting] as CE FROM [WellPlacementData] WHERE [welID] = @welID ORDER BY [Depth] ASC ";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        //count++;
                                        count = readData.GetInt32(0);
                                        inc = readData.GetDecimal(1);
                                        azm = readData.GetDecimal(2);
                                        depth = readData.GetDecimal(3);
                                        tvd = readData.GetDecimal(4);
                                        northing = readData.GetDecimal(5);
                                        easting = readData.GetDecimal(6);
                                        plotdepth = readData.GetDecimal(7);
                                        CNorthing = readData.GetDecimal(8);
                                        CEasting = readData.GetDecimal(9);
                                        iReply.Rows.Add(count, inc, azm, depth, tvd, northing, easting, plotdepth, CNorthing , CEasting);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close(); // this is used for SQLDataReader
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getDataTableforQCGraph_1(Int32 WellID, Int32 SectionID, Int32 ServiceTypeID, Int32 RunID, String dbConn)
        {
            try
            {
                DataSet dsData = new DataSet();
                //String SelectQuery = "Select [srvyID] as Survey_number , [colT] as Inclination , [colDR] as Azimuth , [colBA] as Dip , [colCA] as 'B-Total' , [Depth] as 'MDepth' , [rwTVD] as 'TVD' , [rwNorthing] as 'Northing' , [rwEasting] as 'Easting' , [rwTVD] as 'Depth' FROM [RawQCResults] WHERE [runID] = @runID AND [welID] = @welID AND [wswID] = @wswID AND [srvID] = @srvID ";
                String SelectQuery = "( Select  cast ( ROW_NUMBER() over (order by Depth) as int) as Survey_number , [colT] as Inclination, ([colDR] - [colGE]) as Azimuth, [colBA] as Dip, [colCA] as 'B-Total', [Depth] as 'MDepth' , [bhTVD] as 'TVD', [bhNorthing] as Northing, [bhEasting] as Easting, [bhTVD] as Depth , [bhCummNorthing] as CummDeltaNorthing , [bhCummEasting] as CummDeltaEasting FROM [RawQCResults] WHERE [acID] = 1 AND [runID] = @runID AND [welID] = @welID AND [wswID] = @wswID AND [srvID] = @srvID ";
                SelectQuery += "            UNION ALL ";
                SelectQuery += "            Select  cast ( ROW_NUMBER() over (order by Depth) as int) as Survey_number , [colT] as Inclination, [colDD] as Azimuth, [colBA] as Dip, [colCA] as 'B-Total', [Depth] as 'MDepth', [scTVD] as 'TVD' , [scNorthing] as Northing , [scEasting] as Easting, [scTVD] as Depth , [scCummNorthing] as CummDeltaNorthing , [scCummEasting] as CummDeltaEasting FROM [RawQCResults] WHERE [acID] = 5 AND [runID] = @runID AND [welID] = @welID AND [wswID] = @wswID AND [srvID] = @srvID  ";
                SelectQuery += "            UNION ALL ";
                SelectQuery += "            Select  cast ( ROW_NUMBER() over (order by Depth) as int) as Survey_number , [colT]as Inclination , [colDR] as Azimuth, [colBA] as Dip, [colCA] as 'B-Total', [Depth] as 'MDepth', [rwTVD] as 'TVD' , [rwNorthing] as Northing , [rwEasting] as Easting, [rwTVD] as Depth , [rwCummNorthing] as CummDeltaNorthing , [rwCummEasting] as CummDeltaEasting FROM [RawQCResults] WHERE [acID] = 4 AND [runID] = @runID AND [welID] = @welID AND [wswID] = @wswID AND [srvID] = @srvID  ) ";
                SelectQuery += "            ORDER BY [Depth] ASC ";

                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        cmd.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = SectionID.ToString();
                        cmd.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceTypeID.ToString();
                        using (SqlDataAdapter sqlCmd = new SqlDataAdapter(cmd))
                        {
                            try
                            {
                                sqlCmd.Fill(dsData);  // Fetching the data
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                sqlCmd.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return dsData.Tables[0];
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getDataTableforQCGraph_1_RawBased(Int32 WellID, Int32 SectionID, Int32 ServiceTypeID, Int32 RunID, String dbConn)
        {
            try
            {
                DataSet dsData = new DataSet();
                String SelectQuery = "Select [srvyID] as Survey_number , [colT] as Inclination , [colDR] as Azimuth , [colBA] as Dip , [colCA] as 'B-Total' , [Depth] as 'MDepth' , [rwTVD] as 'TVD' , [rwNorthing] as 'Northing' , [rwEasting] as 'Easting' , [rwTVD] as 'Depth' , [rwCummNorthing] as 'CummDeltaNorthing' , [rwCummEasting] as 'CummDeltaEasting' FROM [RawQCResults] WHERE [runID] = @runID AND [welID] = @welID AND [wswID] = @wswID AND [srvID] = @srvID ";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        cmd.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = SectionID.ToString();
                        cmd.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceTypeID.ToString();
                        using (SqlDataAdapter sqlCmd = new SqlDataAdapter(cmd))
                        {
                            try
                            {
                                sqlCmd.Fill(dsData);  // Fetching the data
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                sqlCmd.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return dsData.Tables[0];
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getDataTableforQCGraph_1_BHABased(Int32 WellID, Int32 SectionID, Int32 ServiceTypeID, Int32 RunID, String dbConn)
        {
            try
            {
                DataSet dsData = new DataSet();
                String SelectQuery = "Select [srvyID] as Survey_number , [colT] as Inclination , ([colDR] - [colGE]) as Azimuth , [colBA] as Dip , [colCA] as 'B-Total' , [Depth] as 'MDepth' , [bhTVD] as 'TVD' , [bhNorthing] as 'Northing' , [bhEasting] as 'Easting' , [bhTVD] as 'Depth' , [bhCummNorthing] as 'CummDeltaNorthing' , [bhCummEasting] as 'CummDeltaEasting' FROM [RawQCResults] WHERE [runID] = @runID AND [welID] = @welID AND [wswID] = @wswID AND [srvID] = @srvID ";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        cmd.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = SectionID.ToString();
                        cmd.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceTypeID.ToString();
                        using (SqlDataAdapter sqlCmd = new SqlDataAdapter(cmd))
                        {
                            try
                            {
                                sqlCmd.Fill(dsData);  // Fetching the data
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                sqlCmd.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return dsData.Tables[0];
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getDataTableforQCGraph_1_SCBased(Int32 WellID, Int32 SectionID, Int32 ServiceTypeID, Int32 RunID, String dbConn)
        {
            try
            {
                DataSet dsData = new DataSet();
                String SelectQuery = "Select [srvyID] as Survey_number , [colT] as Inclination , [colDD] as Azimuth , [colBA] as Dip , [colCA] as 'B-Total' , [Depth] as 'MDepth' , [scTVD] as 'TVD' , [scNorthing] as 'Northing' , [scEasting] as 'Easting' , [scTVD] as 'Depth' , [scCummNorthing] as 'CummDeltaNorthing' , [scCummEasting] as 'CummDeltaEasting' FROM [RawQCResults] WHERE  [runID] = @runID AND [welID] = @welID AND [wswID] = @wswID AND [srvID] = @srvID ";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        cmd.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = SectionID.ToString();
                        cmd.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceTypeID.ToString();
                        using (SqlDataAdapter sqlCmd = new SqlDataAdapter(cmd))
                        {
                            try
                            {
                                sqlCmd.Fill(dsData);  // Fetching the data
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                sqlCmd.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return dsData.Tables[0];
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getDataTableforQCWellCompare(List<int> WellID, String dbConn)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn Survey_number = iReply.Columns.Add("Survey_number", typeof(Int32));

                DataColumn Inclination = iReply.Columns.Add("Inclination", typeof(Decimal));
                DataColumn Azimuth = iReply.Columns.Add("Azimuth", typeof(Decimal));
                DataColumn MDepth = iReply.Columns.Add("MDepth", typeof(Decimal));
                DataColumn TVD = iReply.Columns.Add("TVD", typeof(Decimal));
                DataColumn Northing = iReply.Columns.Add("Northing", typeof(Decimal));
                DataColumn Easting = iReply.Columns.Add("Easting", typeof(Decimal));
                DataColumn WellName = iReply.Columns.Add("WellName", typeof(String));
                DataColumn PlotDepth = iReply.Columns.Add("PlotDepth", typeof(Decimal));
                DataColumn CummDeltaNorthing = iReply.Columns.Add("CummDeltaNorthing", typeof(Decimal));
                DataColumn CummDeltaEasting = iReply.Columns.Add("CummDeltaEasting", typeof(Decimal));

                Int32 count = 0;
                Decimal inc = -99.0000M, azm = -99.00M, depth = -99.00M, plotdepth = -99.00M, tvd = -99.00M, northing = -99.00M, easting = -99.00M, CEasting = -1.00M, CNorthing = -1.00M;
                String wellname = "";
                String getwellid = "";

                getwellid = "(''";
                foreach (int id in WellID)
                {
                    getwellid += "," + id.ToString();
                }
                getwellid += ")";
                 
                String SelectQuery = "Select cast ( ROW_NUMBER() over (order by md) as int) as rn , [rwinc] as INC , [rwazm] as AZ, [md] as Depth, [rwtvd] as TVD , [rwnorthing] as N , [rweasting] as E, [Well].welName ,  [rwtvd] as TVD ,  [rwcummDeltaNorthing] as CN ,[rwcummDeltaEasting] as CE ";
                SelectQuery += " FROM [WellPlacementData] as WPCR , [Well] ";             
                SelectQuery += " WHERE [Well].welID = WPCR.welID ";
                SelectQuery += " AND WPCR.welID in ";
                SelectQuery += getwellid;
                SelectQuery += " ORDER BY [Well].welName,  [Depth] ASC";

                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        //cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        //count++;
                                        count = readData.GetInt32(0);
                                        inc = readData.GetDecimal(1);
                                        azm = readData.GetDecimal(2);
                                        depth = readData.GetDecimal(3);
                                        tvd = readData.GetDecimal(4);
                                        northing = readData.GetDecimal(5);
                                        easting = readData.GetDecimal(6);
                                        wellname = readData.GetString(7);
                                        plotdepth = readData.GetDecimal(8);
                                        CNorthing = readData.GetDecimal(9);
                                        CEasting = readData.GetDecimal(10);
                                        iReply.Rows.Add(count, inc, azm, depth, tvd, northing, easting, wellname, plotdepth , CNorthing , CEasting);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close(); // this is used for SQLDataReader
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getMinMaxSubSeaAC(List<int> WellID, String dbConn)
        {
            try
            {
                String getwellid = "";

                getwellid = "(''";
                foreach (int id in WellID)
                {
                    getwellid += "," + id.ToString();
                }
                getwellid += ")";

                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn maxSubSea = iReply.Columns.Add("maxSubSea", typeof(Decimal));
                DataColumn minSubSea = iReply.Columns.Add("minSubSea", typeof(Decimal));


                String SelectQuery = "select  cast(max(rwsubsea) as decimal(10,0))  maxSubSea , cast(min(rwsubsea) as decimal(10,0)) minSubSea ";
                SelectQuery += " FROM [WellPlacementData] where welID in ";
                SelectQuery += getwellid;
                SelectQuery += ";";

                Decimal max_subsea = -99.00M, min_subsea = -99.00M;

                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        max_subsea = readData.GetDecimal(0);
                                        min_subsea = readData.GetDecimal(1);

                                        iReply.Rows.Add(max_subsea, min_subsea);
                                        iReply.AcceptChanges();
                                    }
                                }

                                else
                                {
                                    max_subsea = 600; min_subsea = -500 ;

                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close(); // this is used for SQLDataReader
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }

                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getDataTableforQCWelGraphAZIM_1(Int32 WelID, Int32 SectionID, Int32 ServiceTypeID, Int32 RunID, String dbConn)
        {
            try
            {
                DataSet dsData = new DataSet();
                //String SelectQuery = "Select [srvyID] as Survey_number , [colT] as Inclination , [colDR] as Azimuth , [colBA] as Dip , [colCA] as 'B-Total' , [Depth] as 'MDepth' FROM [RawQCResults] WHERE [runID] = @runID AND [wswID] = @wswID ";
                String SelectQuery = "Select [srvyID] as Survey_number , [Depth] as 'MDepth' , [colDR] as Azimuth , [colDD] as ShortAzimuth , ([colDR] - [colGE]) as BHAAzimuth FROM [RawQCResults] WHERE [runID] = @runID AND [welID] = @welID AND [wswID] = @wswID AND [srvID] = @srvID";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WelID.ToString();
                        cmd.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = SectionID.ToString();
                        cmd.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceTypeID.ToString();
                        using (SqlDataAdapter sqlCmd = new SqlDataAdapter(cmd))
                        {
                            try
                            {
                                sqlCmd.Fill(dsData);  // Fetching the data
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                sqlCmd.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return dsData.Tables[0];
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getDataTableforQCWellGraphIncAZIMToolFace(Int32 WellID, String dbConn)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn Survey_number = iReply.Columns.Add("Survey_number", typeof(Int32));
                DataColumn MDepth = iReply.Columns.Add("MDepth", typeof(Decimal));
                DataColumn Azimuth = iReply.Columns.Add("Azimuth", typeof(Decimal));
                DataColumn ShortAzimuth = iReply.Columns.Add("ShortAzimuth", typeof(Decimal));
                DataColumn BHAAzimuth = iReply.Columns.Add("BHAAzimuth", typeof(Decimal));
                DataColumn Inclination = iReply.Columns.Add("Inclination", typeof(Decimal));
                DataColumn GTF = iReply.Columns.Add("GTF", typeof(Decimal));
                DataColumn MTF = iReply.Columns.Add("MTF", typeof(Decimal));


                Int32 count = 0;
                Decimal inc = -99.0000M, azm = -99.00M, depth = -99.00M, bhaAzm = -99.00M, shortAzm = -99.00M, gtf = -99.00M, mtf = -99.00M;
                String SelectQuery = "Select cast ( ROW_NUMBER() over (order by Depth) as int), [Depth],  [colDR] , [colDD] , ([colDR] - [colGE]) , [colT] , [colP] , [colQ]  FROM [RawQCResults] WHERE [welID] = @welID ORDER BY [Depth] ASC ";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        //count++;
                                        count = readData.GetInt32(0);
                                        depth = readData.GetDecimal(1);
                                        azm = readData.GetDecimal(2);
                                        shortAzm = readData.GetDecimal(3);
                                        bhaAzm = readData.GetDecimal(4);
                                        inc = readData.GetDecimal(5);
                                        gtf = readData.GetDecimal(6);
                                        mtf = readData.GetDecimal(7);
                                        iReply.Rows.Add(count, depth, azm, shortAzm, bhaAzm, inc, gtf, mtf);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close(); // this is used for SQLDataReader
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getDataTableforQCWelGraphIncAZIMToolFace_1(Int32 WelID, Int32 SectionID, Int32 ServiceTypeID, Int32 RunID, String dbConn)
        {
            try
            {
                DataSet dsData = new DataSet();
                //String SelectQuery = "Select [srvyID] as Survey_number , [colT] as Inclination , [colDR] as Azimuth , [colBA] as Dip , [colCA] as 'B-Total' , [Depth] as 'MDepth' FROM [RawQCResults] WHERE [runID] = @runID AND [wswID] = @wswID ";
                String SelectQuery = "Select [srvyID] as Survey_number , [Depth] as 'MDepth' , [colDR] as Azimuth , [colDD] as ShortAzimuth , ([colDR] - [colGE]) as BHAAzimuth , [colT] as Inclination, [colP] as GTF , [colQ] as MTF FROM [RawQCResults] WHERE [runID] = @runID AND [welID] = @welID AND [wswID] = @wswID AND [srvID] = @srvID";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WelID.ToString();
                        cmd.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = SectionID.ToString();
                        cmd.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceTypeID.ToString();
                        using (SqlDataAdapter sqlCmd = new SqlDataAdapter(cmd))
                        {
                            try
                            {
                                sqlCmd.Fill(dsData);  // Fetching the data
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                sqlCmd.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return dsData.Tables[0];
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getDataTableforPerfQualityScore(Int32 WellID, String dbConn)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn Survey_number = iReply.Columns.Add("Survey_number", typeof(Int32));
                DataColumn MDepth = iReply.Columns.Add("MDepth", typeof(Decimal));
                DataColumn DipQualifier = iReply.Columns.Add("DipQualifier", typeof(Int32));
                DataColumn BTQualifier = iReply.Columns.Add("BTQualifier", typeof(Int32));
                DataColumn GTQualifier = iReply.Columns.Add("GTQualifier", typeof(Int32));
                DataColumn Y0 = iReply.Columns.Add("y0", typeof(Int32));
                DataColumn Y1 = iReply.Columns.Add("y1", typeof(Int32));
                DataColumn Y2 = iReply.Columns.Add("y2", typeof(Int32));


                Int32 count = 0, dipQualifier = -99, btQualifier = -99, gtQualifier = -99, y0 = -1, y1 = -1, y2 = -2;
                Decimal depth = -99.00M;
                String SelectQuery = "Select cast ( ROW_NUMBER() over (order by Depth) as int) , [Depth] , [colDW] , [colDY] , [colEA], cast(0 as int) as y0 , cast(1 as int) as y1 , cast(2 as int) as y3 FROM [RawQCResults] WHERE [welID] = @welID ORDER BY [Depth] ASC ";
                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        //count++;
                                        count = readData.GetInt32(0);
                                        depth = readData.GetDecimal(1);
                                        dipQualifier = readData.GetInt32(2);
                                        btQualifier = readData.GetInt32(3);
                                        gtQualifier = readData.GetInt32(4);
                                        y0 = readData.GetInt32(5);
                                        y1 = readData.GetInt32(6);
                                        y2 = readData.GetInt32(7);

                                        iReply.Rows.Add(count, depth, dipQualifier, btQualifier, gtQualifier, y0, y1, y2);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close(); // this is used for SQLDataReader
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getDataTableforPerfQualityScore2(Int32 WellID, String dbConn)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();

                DataColumn Total = iReply.Columns.Add("Total", typeof(Decimal));
                DataColumn DipPass = iReply.Columns.Add("DipPass", typeof(Decimal));
                DataColumn BTPass = iReply.Columns.Add("BTPass", typeof(Decimal));
                DataColumn GTPass = iReply.Columns.Add("GTPass", typeof(Decimal));
                DataColumn DipCount = iReply.Columns.Add("DipCount", typeof(Decimal));
                DataColumn BTCuunt = iReply.Columns.Add("BTCount", typeof(Decimal));
                DataColumn GTCount = iReply.Columns.Add("GTCount", typeof(Decimal));

                Decimal dipcount = -99, btcount = -99, gtcount = -99;
                Decimal total = -99, dipAverage = -99, btAverage = -99, gtAverage = -99;

                String SelectQuery = "DECLARE @TOTAL decimal, @DIP_P decimal, @BT_P decimal , @GT_P decimal";
                SelectQuery += " set @TOTAL = ( select count(*) from [RawQCResults] where welID = @welID)";
                SelectQuery += " set @DIP_P = ( select count([colDW]) from [RawQCResults] where [colDW] = 1 and welID = @welID)";
                SelectQuery += " set @BT_P = ( select count([colDY]) from [RawQCResults] where [colDY] = 1 and welID = @welID)";
                SelectQuery += " set @GT_P = ( select count([colEA]) from [RawQCResults] where [colEA] = 1 and welID = @welID)";
                SelectQuery += " select @TOTAL as Total , @DIP_P as DipCount, @BT_P as BTCount , @GT_P as GTCount,  cast( (@DIP_P)/@TOTAL as decimal(6,3) )*100 as DipPass , cast ( (@BT_P)/@TOTAL as decimal(6,3))*100 as BTPass , cast ( (@GT_P)/@TOTAL as decimal(6,3))*100 as GTPass ; ";


                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        //count++;
                                        total = readData.GetDecimal(0);
                                        dipcount = readData.GetDecimal(1);
                                        btcount = readData.GetDecimal(2);
                                        gtcount = readData.GetDecimal(3);
                                        dipAverage = readData.GetDecimal(4);
                                        btAverage = readData.GetDecimal(5);
                                        gtAverage = readData.GetDecimal(6);

                                        iReply.Rows.Add(total, dipcount, btcount, gtcount, dipAverage, btAverage, gtAverage);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close(); // this is used for SQLDataReader
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

    }
}