﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Web.Configuration;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using CUSR = SmartsVer1.Helpers.AccountHelpers.CreateUser;
using CTRT = SmartsVer1.Helpers.AccountHelpers.CreateContract;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using SRV = SmartsVer1.Helpers.DemogHelpers.ServicesConfig;
using LPC = SmartsVer1.Helpers.LogPrint.LogComments;
using SPRT = SmartsVer1.Helpers.SupportHelpers.SRequest;
using WP = SmartsVer1.Helpers.QCHelpers.WellProfile;

namespace SmartsVer1.Helpers.ChartHelpers
{
    public class Gauges
    {
        const Int32 serviceGroupQC = 2;

        void main()
        {}

        public static Int32 checkRunData(Int32 RunID, String ClientDBString)
        {
            try
            {
                Int32 iResult = -99;
                String selData = "SELECT Count(resID) FROM [RawQCResults] WHERE [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iResult = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    iResult = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getChartsCorrAnalysisBTDelta(String div, System.Data.DataTable ChartData)
        {
            try
            {
                StringBuilder strScript = new StringBuilder();
                StringBuilder dataDepth = new StringBuilder();
                StringBuilder dataBT = new StringBuilder();

                // Loading Data

                dataDepth.Append(" data: [");
                dataBT.Append(" data: [ ");

                foreach (DataRow row in ChartData.Rows)
                {
                    dataDepth.Append(Math.Round((Decimal)row["MDepth"], 2) + ",");
                    dataBT.Append(Math.Round((Decimal)row["Delta B-Total"], 2) + ",");
                }

                dataDepth.Append(" ], ");
                dataBT.Append("],");

                strScript.Append("<script type='text/javascript' src='../../eCharts/echarts.min.js'></script>");
                strScript.Append("<script type='text/javascript'>");

                strScript.Append(" var dom = document.getElementById('" + div + "'); ");
                strScript.Append(" var myChart = echarts.init(dom); ");
                strScript.Append(@"

                            var app = {};
                            option = null;

                            option = {
                                title: {
                                    text: 'SMARTS Correction ∆B-Total Analysis ',
                                    subtext: 'Detail: ∆B-Total Analysis\nwww.smartwelldynamics.com',
                                    x: 'right'
                                },
                                tooltip: {
                                    trigger: 'axis',
                                    axisPointer: {
                                        animation: false
                                    }
                                },
                                legend: {
                                    data: [ 'Delta B-Total'],
                                    x: 'left'
                                },
                                toolbox: {
                                    show: true,
                                    showTitle: true,
                                    itemGap: 20,
                                    orient: 'vertical',
                                    left: 'right',
                                    top: '80',
                                    feature: {
                                        magicType: { show: true, type: ['line', 'bar'], title: { line: 'Line', bar: 'Bar' } },
                                        restore: { show: true, title: 'Restore' },
                                        saveAsImage: { show: true, title: 'Save' }
                                    }
                                },
                                axisPointer: {
                                    link: { xAxisIndex: 'all' }
                                },
                                dataZoom: [
                                    {
                                        type: 'slider',
                                        realtime: true,
                                        start: 0,
                                        
                                    },
                                ],
                                grid: 
                                   {  left: 80, right: 80, top: '5%',  height: '70%' },
                                
                                xAxis: [
                                    {
                                        type: 'category',
                                        boundaryGap: false,
                                        axisLine: { onZero: true },
                                        axisTick: {
                                            alignWithLabel: true
                                        },
                                        axisPointer: {
                                            label: {
                                                formatter: function (params) {
                                                    return 'Measured Depth : ' + params.value;
                                                }
                                            }
                                        },
                                    ");

                strScript.Append(dataDepth + "},");  // X Axis Graph Grid 1                
                strScript.Append(@"                                        
                                                          
                                ],
                                yAxis: [
                                    {
                                        name: '∆B-Total',
                                        type: 'value',
                                        inverse: true,
                                        min: 'dataMin',
                                        nameLocation: 'middle',
                                        nameTextStyle: {
                                            align: 'middle',
                                            verticalAlign: 'bottom',
                                            padding: [25, 25, 40, 25]
                                        }                                      
                                    },
                                ],
                                series: [
                                    {
                                        name: '∆B-Total',
                                        type: 'line',
                                        symbolSize: 6,
                                        yAxisIndex: 0,
                                        hoverAnimation: false,
                                     ");

                strScript.Append(dataBT + "},");  // Y Axis
                                 
                strScript.Append(@"
                                    
                                ]
                            };;
                        ");

                strScript.Append("  if (option && typeof option === \"object\" ) {");
                strScript.Append("     myChart.setOption(option, true); } ");

                strScript.Append("</script>");

                return strScript.ToString();
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }

        }

        public static String getChartsCorrAnalysisDIPDelta(String div, System.Data.DataTable ChartData)
        {
            try
            {
                StringBuilder strScript = new StringBuilder();
                StringBuilder dataDepth = new StringBuilder();
                StringBuilder dataDIP = new StringBuilder();

                // Loading Data

                dataDepth.Append(" data: [");
                dataDIP.Append(" data: [ ");

                foreach (DataRow row in ChartData.Rows)
                {
                    dataDepth.Append(Math.Round((decimal)row["MDepth"], 2) + ",");
                    dataDIP.Append(Math.Round((decimal)row["Delta Dip"], 2) + ",");
                }

                dataDepth.Append(" ], ");
                dataDIP.Append("],");

                strScript.Append("<script type='text/javascript' src='../../eCharts/echarts.min.js'></script>");
                strScript.Append("<script type='text/javascript'>");

                strScript.Append(" var dom = document.getElementById('" + div + "'); ");
                strScript.Append(" var myChart = echarts.init(dom); ");
                strScript.Append(@"

                            var app = {};
                            option = null;

                            option = {
                                title: {
                                    text: 'SMARTS Correction ∆DIP Analysis ',
                                    subtext: 'Detail: ∆DIP Analysis\nwww.smartwelldynamics.com',
                                    x: 'right'
                                },
                                tooltip: {
                                    trigger: 'axis',
                                    axisPointer: {
                                        animation: false
                                    }
                                },
                                legend: {
                                    data: [ 'Delta DIP'],
                                    x: 'left'
                                },
                                toolbox: {
                                    show: true,
                                    showTitle: true,
                                    itemGap: 20,
                                    orient: 'vertical',
                                    left: 'right',
                                    top: '80',
                                    feature: {
                                        magicType: { show: true, type: ['line', 'bar'], title: { line: 'Line', bar: 'Bar' } },
                                        restore: { show: true, title: 'Restore' },
                                        saveAsImage: { show: true, title: 'Save' }
                                    }
                                },
                                axisPointer: {
                                    link: { xAxisIndex: 'all' }
                                },
                                dataZoom: [
                                    {
                                        type: 'slider',
                                        realtime: true,
                                        start: 0,
                                        
                                    },
                                ],
                                grid: 
                                   {  left: 80, right: 80, top: '5%',  height: '70%' },
                                
                                xAxis: [
                                    {
                                        type: 'category',
                                        boundaryGap: false,
                                        axisLine: { onZero: true },
                                        axisTick: {
                                            alignWithLabel: true
                                        },
                                        axisPointer: {
                                            label: {
                                                formatter: function (params) {
                                                    return 'Measured Depth : ' + params.value;
                                                }
                                            }
                                        },
                                    ");

                strScript.Append(dataDepth + "},");  // X Axis Graph Grid 1                
                strScript.Append(@"                                        
                                                          
                                ],
                                yAxis: [
                                    {
                                        name: '∆DIP',
                                        type: 'value',
                                        inverse: true,
                                        min: 'dataMin',
                                        nameLocation: 'middle',
                                        nameTextStyle: {
                                            align: 'middle',
                                            verticalAlign: 'bottom',
                                            padding: [25, 25, 40, 25]
                                        }                                      
                                    },
                                ],
                                series: [
                                    {
                                        name: '∆DIP',
                                        type: 'line',
                                        symbolSize: 6,
                                        yAxisIndex: 0,
                                        hoverAnimation: false,
                                     ");

                strScript.Append(dataDIP + "},");  // Y Axis

                strScript.Append(@"
                                    
                                ]
                            };;
                        ");

                strScript.Append("  if (option && typeof option === \"object\" ) {");
                strScript.Append("     myChart.setOption(option, true); } ");

                strScript.Append("</script>");

                return strScript.ToString();
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }

        }

        public static String getChartsCorrAnalysisGT(String div, System.Data.DataTable ChartData)
        {
            try
            {
                StringBuilder strScript = new StringBuilder();
                StringBuilder dataDepth = new StringBuilder();
                StringBuilder dataGT = new StringBuilder();

                // Loading Data

                dataDepth.Append(" data: [");
                dataGT.Append(" data: [ ");

                foreach (DataRow row in ChartData.Rows)
                {
                    dataDepth.Append(Math.Round((decimal)row["MDepth"], 2) + ",");
                    dataGT.Append(Math.Round((decimal)row["Delta G-Total"], 4) + ",");
                }

                dataDepth.Append(" ], ");
                dataGT.Append("],");

                strScript.Append("<script type='text/javascript' src='../../eCharts/echarts.min.js'></script>");
                strScript.Append("<script type='text/javascript'>");

                strScript.Append(" var dom = document.getElementById('" + div + "'); ");
                strScript.Append(" var myChart = echarts.init(dom); ");
                strScript.Append(@"

                            var app = {};
                            option = null;

                            option = {
                                title: {
                                    text: 'SMARTS Correction ∆G-Total ',
                                    subtext: 'Detail: ∆Azimuth\nwww.smartwelldynamics.com',
                                    x: 'right'
                                },
                                tooltip: {
                                    trigger: 'axis',
                                    axisPointer: {
                                        animation: false
                                    }
                                },
                                legend: {
                                    data: [ 'Delta G-Total'],
                                    x: 'left'
                                },
                                toolbox: {
                                    show: true,
                                    showTitle: true,
                                    itemGap: 20,
                                    orient: 'vertical',
                                    left: 'right',
                                    top: '80',
                                    feature: {
                                        magicType: { show: true, type: ['line', 'bar'], title: { line: 'Line', bar: 'Bar' } },
                                        restore: { show: true, title: 'Restore' },
                                        saveAsImage: { show: true, title: 'Save' }
                                    }
                                },
                                axisPointer: {
                                    link: { xAxisIndex: 'all' }
                                },
                                dataZoom: [
                                    {
                                        type: 'slider',
                                        realtime: true,
                                        start: 0,
                                        
                                    },
                                ],
                                grid: 
                                   {  left: 80, right: 80, top: '5%',  height: '70%' },
                                
                                xAxis: [
                                    {
                                        type: 'category',
                                        boundaryGap: false,
                                        axisLine: { onZero: true },
                                        axisTick: {
                                            alignWithLabel: true
                                        },
                                        axisPointer: {
                                            label: {
                                                formatter: function (params) {
                                                    return 'Measured Depth : ' + params.value;
                                                }
                                            }
                                        },
                                    ");

                strScript.Append(dataDepth + "},");  // X Axis Graph Grid 1                
                strScript.Append(@"                                        
                                                          
                                ],
                                yAxis: [
                                    {
                                        name: '∆G-Total',
                                        type: 'value',
                                        inverse: true,
                                        min: 'dataMin',
                                        nameLocation: 'middle',
                                        nameTextStyle: {
                                            align: 'middle',
                                            verticalAlign: 'bottom',
                                            padding: [25, 25, 40, 25]
                                        }                                      
                                    },
                                ],
                                series: [
                                    {
                                        name: '∆G-Total',
                                        type: 'line',
                                        symbolSize: 6,
                                        yAxisIndex: 0,
                                        hoverAnimation: false,
                                     ");

                strScript.Append(dataGT + "},");  // Y Axis

                strScript.Append(@"
                                    
                                ]
                            };;
                        ");

                strScript.Append("  if (option && typeof option === \"object\" ) {");
                strScript.Append("     myChart.setOption(option, true); } ");

                strScript.Append("</script>");

                return strScript.ToString();
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }

        }

        public static String getChartsCorrAnalysisBx(String div, System.Data.DataTable ChartData)
        {
            try
            {
                StringBuilder strScript = new StringBuilder();
                StringBuilder dataDepth = new StringBuilder();
                StringBuilder dataBxm = new StringBuilder();
                StringBuilder dataBxc = new StringBuilder();
                // Loading Data

                dataDepth.Append(" data: [");
                dataBxm.Append(" data: [ ");
                dataBxc.Append(" data: [ ");
                foreach (DataRow row in ChartData.Rows)
                {
                    dataDepth.Append(Math.Round((Decimal)row["MDepth"], 2) + ",");
                    dataBxm.Append(Math.Round((Decimal)row["Bx (m)"], 6) + ",");
                    dataBxc.Append(Math.Round((Decimal)row["Bx (c)"], 6) + ",");
                }

                dataDepth.Append(" ], ");
                dataBxm.Append("],");
                dataBxc.Append("],");

                strScript.Append("<script type='text/javascript' src='../../eCharts/echarts.min.js'></script>");
                strScript.Append("<script type='text/javascript'>");

                strScript.Append(" var dom = document.getElementById('" + div + "'); ");
                strScript.Append(" var myChart = echarts.init(dom); ");
                strScript.Append(@"

                            var app = {};
                            option = null;

                            option = {
                                title: {
                                    text: 'SMARTS Correction Bx ',
                                    x: 'right'
                                },
                                tooltip: {
                                    trigger: 'axis',
                                    axisPointer: {
                                        animation: false
                                    }
                                },
                                legend: {
                                    data: [ 'Bx (m) vs Bx (c)'],
                                    x: 'left'
                                },
                                toolbox: {
                                    show: true,
                                    showTitle: true,
                                    itemGap: 20,
                                    orient: 'vertical',
                                    left: 'right',
                                    top: '80',
                                    feature: {
                                        magicType: { show: true, type: ['line', 'bar'], title: { line: 'Line', bar: 'Bar' } },
                                        restore: { show: true, title: 'Restore' },
                                        saveAsImage: { show: true, title: 'Save' }
                                    }
                                },
                                axisPointer: {
                                    link: { xAxisIndex: 'all' }
                                },
                                dataZoom: [
                                    {
                                        type: 'slider',
                                        realtime: true,
                                        start: 0,
                                        
                                    },
                                ],
                                grid: 
                                   {  left: 80, right: 80, top: '5%',  height: '70%' },
                                
                                xAxis: [
                                    {
                                        type: 'category',
                                        boundaryGap: false,
                                        axisLine: { onZero: true },
                                        axisTick: {
                                            alignWithLabel: true
                                        },
                                        axisPointer: {
                                            label: {
                                                formatter: function (params) {
                                                    return 'Measured Depth : ' + params.value;
                                                }
                                            }
                                        },
                                    ");

                strScript.Append(dataDepth + "},");  // X Axis Graph Grid 1                
                strScript.Append(@"                                        
                                                          
                                ],
                                yAxis: [
                                    {
                                        name: 'Gx (m)',
                                        type: 'value',
                                        inverse: true,
                                        min: 'dataMin',
                                        nameLocation: 'middle',
                                        nameTextStyle: {
                                            align: 'middle',
                                            verticalAlign: 'bottom',
                                            padding: [25, 25, 40, 25]
                                        }                                      
                                    },
                                ],
                                series: [
                                    {
                                        name: 'Gx (m)',
                                        type: 'line',
                                        symbolSize: 6,
                                        yAxisIndex: 0,
                                        hoverAnimation: false,
                                     ");

                strScript.Append(dataBxm + "},");  // Y Axis

                strScript.Append(@"
                                    
                                ]
                            };;
                        ");

                strScript.Append("  if (option && typeof option === \"object\" ) {");
                strScript.Append("     myChart.setOption(option, true); } ");

                strScript.Append("</script>");

                return strScript.ToString();
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }

        }

        public static String getChartsQCResultsECHRT_AZM(String div, System.Data.DataTable ChartData)
        {
            try
            {
                StringBuilder strScript = new StringBuilder();
                StringBuilder dataXaxis = new StringBuilder();
                StringBuilder dataDepth = new StringBuilder();
                StringBuilder dataAzim = new StringBuilder();
                StringBuilder dataShortAzim = new StringBuilder();
                StringBuilder dataBHAAzim = new StringBuilder();


                // Loading Data

                dataXaxis.Append(" data: [");
                dataDepth.Append(" data: [");
                dataAzim.Append(" data: [ ");
                dataShortAzim.Append(" data: [ ");
                dataBHAAzim.Append(" data: [");

                foreach (DataRow row in ChartData.Rows)
                {
                    dataXaxis.Append("\"" + row["Survey_number"] + "\"" + ",");
                    dataDepth.Append(row["MDepth"] + ",");
                    dataAzim.Append(row["Azimuth"] + ",");
                    dataBHAAzim.Append(Math.Round((decimal)row["BHAAzimuth"], 2) + ",");
                    dataShortAzim.Append(Math.Round((decimal)row["ShortAzimuth"], 2) + ",");

                }

                dataXaxis.Append(" ], ");
                dataDepth.Append(" ], ");
                dataAzim.Append("],");
                dataBHAAzim.Append("],");
                dataShortAzim.Append("],");

                strScript.Append("<script type='text/javascript' src='../../eCharts/echarts.min.js'></script>");
                strScript.Append("<script type='text/javascript'>");

                strScript.Append(" var dom = document.getElementById('" + div + "'); ");
                strScript.Append(" var myChart = echarts.init(dom); ");
                strScript.Append(@"

                            var app = {};
                            option = null;


                            option = {
                                title: {
                                    text: 'SMARTS QC Azimuth Analysis ',
                                    subtext: 'Drilling Logs Detail: Azimuth, Short Color Azimuth, BHA Azimuth\nwww.smartwelldynamics.com',
                                    x: 'right'
                                },
                                tooltip: {
                                    trigger: 'axis',
                                    axisPointer: {
                                        animation: false
                                    }
                                },
                                legend: {
                                    data: [ 'Azimuth', 'Short_Color_Azimuth', 'BHA_Azimuth'],
                                    x: 'left'
                                },
                                toolbox: {
                                    show: true,
                                    showTitle: true,
                                    itemGap: 20,
                                    orient: 'vertical',
                                    left: 'right',
                                    top: '80',
                                    feature: {
                                        magicType: { show: true, type: ['line', 'bar'], title: { line: 'Line', bar: 'Bar' } },
                                        restore: { show: true, title: 'Restore' },
                                        saveAsImage: { show: true, title: 'Save' }
                                    }
                                },
                                axisPointer: {
                                    link: { xAxisIndex: 'all' }
                                },
                                dataZoom: [
                                    {
                                        type: 'slider',
                                        realtime: true,
                                        start: 0,
                                        
                                    },
                                ],
                                grid: 
                                   { left: 80, right: 80, top: '18%',  height: '65%' },
                                
                                xAxis: [
                                    {
                                        type: 'category',
                                        boundaryGap: false,
                                        axisLine: { onZero: true },
                                        axisTick: {
                                            alignWithLabel: true
                                        },
                                        axisPointer: {
                                            label: {
                                                formatter: function (params) {
                                                    return 'Measured Depth : ' + params.value;
                                                }
                                            }
                                        },
                                    ");

                strScript.Append(dataDepth + "},");  // X Axis Graph Grid 1                
                strScript.Append(@"                                        
                                                          
                                ],
                                yAxis: [
                                    {
                                        name: 'Azimuth / Short Color Azimuth / BHA Azimuth',
                                        type: 'value',
                                        inverse: true,
                                        min: 'dataMin',
                                        nameLocation: 'middle',
                                        nameTextStyle: {
                                            align: 'middle',
                                            verticalAlign: 'bottom',
                                            padding: [25, 25, 40, 25]
                                        }
                                      
                                    },

                                ],
                                series: [
                                    {
                                        name: 'Azimuth',
                                        type: 'line',
                                        symbolSize: 6,
                                        yAxisIndex: 0,
                                        hoverAnimation: false,
                                     ");
                
                strScript.Append(dataAzim + "},");  // Y Axis
                strScript.Append(@"
                                    
                                    {
                                        name: 'Short_Color_Azimuth',
                                        type: 'line',
                                        symbolSize: 6,
                                        yAxisIndex: 0,
                                        hoverAnimation: false,
                                    ");
                
                strScript.Append(dataShortAzim + "},");  // Y Axis 
                strScript.Append(@"
                                    {   
                                        name: 'BHA_Azimuth',
                                        type: 'line',
                                        symbolSize: 6,
                                        yAxisIndex: 0,
                                        hoverAnimation: false,
                                    ");

                strScript.Append(dataBHAAzim + "},");  // Y Axis                                    
                strScript.Append(@"
                                    
                                ]
                            };;
                        ");

                strScript.Append("  if (option && typeof option === \"object\" ) {");
                strScript.Append("     myChart.setOption(option, true); } ");

                strScript.Append("</script>");


                return strScript.ToString();
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());

            }


        }

        public static String getChartsQCResultsECHRTPolar_INCAZM(String div, System.Data.DataTable ChartData)
        {
            try
            {
                StringBuilder strScript = new StringBuilder();
                StringBuilder dataFORPolar = new StringBuilder();

                strScript.Append("<script type='text/javascript' src='../../eCharts/echarts.min.js'></script>");
                strScript.Append("<script type='text/javascript'>");

                strScript.Append(" var dom = document.getElementById('" + div + "'); ");
                strScript.Append(" var myChart = echarts.init(dom); ");
                strScript.Append(@"
                        
                var app = {};
                option = null;

                option = {

                    title: {
                        text: 'Inclination and Azimuth Chart\nw.r.t Measured Depth',
                        subtext: ' (Azimuth, Short Color Azimuth, BHA Azimuth) using Polar Map\nwww.smartwelldynamics.com',
                        x: 'right'
                    },
					legend: {
                        data: ['Inclination','Azimuth','Short Color Azimuth','BHA Azimuth'],
						orient: 'vertical',
						align: 'right',
						left: 15,
                        top: 15,
                        
                    },
				     toolbox: {
                        show: true, showTitle: true, itemGap: 20, orient: 'vertical', left: 'right',
                        top: '80',
						right: '120',
                        feature: {
                            restore: { show: true, title: 'Restore' },
                            saveAsImage: { show: true, title: 'Save' }
                        }
                    },
                    polar: {},
                    tooltip: {
                        trigger: 'item',
                        axisPointer: {
                            type: 'cross'
                        }
                    },
                    angleAxis: {
                        type: 'value',
                        startAngle: 90,
						min: 0,
						max: 360
                    },
                    radiusAxis: {
                        //min: 10,
                        //max: 20
                    },
					 dataZoom: [
							{
								type: 'slider',
								radiusAxisIndex: 0,
								bottom: 10,
								showDetail: true,
								realtime: true,
								show: true,
							},
							{
								type: 'inside',
								radiusAxisIndex: 0,
							}
						],

                                    dataset: {
                                    
                                    dimensions: [ 'Measured Depth', 'Inclination','Azimuth','Short Color Azimuth','BHA Azimuth'],
                                      
                                    source: [
					                    
                ");

                dataFORPolar.Append(" ['Measured Depth', 'Inclination','Azimuth','Short Color Azimuth','BHA Azimuth'],");

                foreach (DataRow row in ChartData.Rows)
                {
                    dataFORPolar.Append(" ['" + row["MDepth"] + "','"  + Math.Round((decimal)row["Inclination"], 2) + "','" + row["Azimuth"] + "','" + Math.Round((decimal)row["ShortAzimuth"], 2) + "','" + Math.Round((decimal)row["BHAAzimuth"], 2) + "'],");

                }

                dataFORPolar.Append("] },");// End of data load sourse

                strScript.Append(dataFORPolar);
                strScript.Append(@"  

                   series: [
					{
                        coordinateSystem: 'polar',
                        name: 'Inclination',
                        type: 'scatter',
						symbolSize: 3,
						symbol: 'diamond',
						encode: {
                                radius: 'Measured Depth',
                                angle: 'Inclination',
                                tooltip: [0,  1]
                        }  
                        

                    },
					{
					    coordinateSystem: 'polar',
                        name: 'Azimuth',
						symbolSize: 3,
                        type: 'scatter',
						encode: {
                                radius: 'Measured Depth',
                                angle: 'Azimuth',
                                tooltip: [0,  2 , 3 , 4]
                        }  


                    },
					{
					    coordinateSystem: 'polar',
                        name: 'Short Color Azimuth',
						symbolSize: 3,
                        type: 'scatter',
						encode: {
                                radius: 'Measured Depth',
                                angle: 'Short Color Azimuth',
                                tooltip: [0,  2 , 3 , 4]
                        }  


                    },
					{
					    coordinateSystem: 'polar',
                        name: 'BHA Azimuth',
						symbolSize: 3,
                        type: 'scatter',
						encode: {
                                radius: 'Measured Depth',
                                angle: 'BHA Azimuth',
                                tooltip: [0,  2 , 3 , 4]
                        }  


                    },
                    ]

                    };;


                  ");
                        strScript.Append("  if (option && typeof option === \"object\" ) {");
                        strScript.Append("     myChart.setOption(option, true); } ");
                        strScript.Append("</script>");

                return strScript.ToString();
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());

            }


        }

        public static String getChartsQCResultsECHRTPolar_ToolFaces(String div, System.Data.DataTable ChartData)
        {
            try
            {
                StringBuilder strScript = new StringBuilder();
                StringBuilder dataFORPolar = new StringBuilder();

                strScript.Append("<script type='text/javascript' src='../../eCharts/echarts.min.js'></script>");
                strScript.Append("<script type='text/javascript'>");

                strScript.Append(" var dom = document.getElementById('" + div + "'); ");
                strScript.Append(" var myChart = echarts.init(dom); ");
                strScript.Append(@"
                        
                var app = {};
                option = null;

                option = {

                    title: {
                        text: 'ToolFaces w.r.t Measured Depth',
                        subtext: 'Gravity Toolface and Magnetic Toolface using Polar Map\nwww.smartwelldynamics.com',
                        x: 'right'
                    },
					legend: {
                        data: ['Gravity Toolface','Magnetic Toolface'],
						orient: 'vertical',
						align: 'right',
						left: 5,
                        
                    },
				     toolbox: {
                        show: true, showTitle: true, itemGap: 20, orient: 'vertical', left: 'right',
                        top: '80',
						right: '120',
                        feature: {
                            restore: { show: true, title: 'Restore' },
                            saveAsImage: { show: true, title: 'Save' }
                        }
                    },
                    polar: {},
                    tooltip: {
                        trigger: 'item',
                        axisPointer: {
                            type: 'cross'
                        }
                    },
                    angleAxis: {
                        type: 'value',
                        startAngle: 90,
						min: 0,
						max: 360
                    },
                    radiusAxis: {
                        //min: 10,
                        //max: 20
                    },
					 dataZoom: [
							{
								type: 'slider',
								radiusAxisIndex: 0,
								bottom: 10,
								showDetail: true,
								realtime: true,
								show: true,
							},
							{
								type: 'inside',
								radiusAxisIndex: 0,
							}
						],

                                    dataset: {
                                    
                                    dimensions: [ 'Measured Depth', 'Gravity Toolface','Magnetic Toolface'],
                                      
                                    source: [
					                    
                ");

                dataFORPolar.Append(" ['Measured Depth', 'Gravity Toolface','Magnetic Toolface'],");

                foreach (DataRow row in ChartData.Rows)
                {
                    dataFORPolar.Append(" ['" + row["MDepth"] + "','" + Math.Round((decimal)row["GTF"], 2) + "','" + row["MTF"] + "'],");

                }

                dataFORPolar.Append("] },");// End of data load sourse

                strScript.Append(dataFORPolar);
                strScript.Append(@"  

                   series: [
					    {
                            coordinateSystem: 'polar',
                            name: 'Gravity Toolface',
                            type: 'scatter',
						    symbolSize: 4,
						    symbol: 'diamond',
						    encode: {
                                    radius: 'Measured Depth',
                                    angle: 'Gravity Toolface',
                                    tooltip: [0,  1]
                            }  


                        },
					    {
					        coordinateSystem: 'polar',
                            name: 'Magnetic Toolface',
                            type: 'scatter',
						    symbolSize: 4,
						    encode: {
                                    radius: 'Measured Depth',
                                    angle: 'Magnetic Toolface',
                                    tooltip: [0,  2]
                            } 

                        },

                    ]

                    };;


                  ");
                strScript.Append("  if (option && typeof option === \"object\" ) {");
                strScript.Append("     myChart.setOption(option, true); } ");
                strScript.Append("</script>");

                return strScript.ToString();
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());

            }


        }

        public static String getChartsQCResultsECHRT01(String div, System.Data.DataTable ChartData )
        {
            try {
                StringBuilder strScript = new StringBuilder();
                StringBuilder dataAzim = new StringBuilder();
                StringBuilder dataDip = new StringBuilder();
                StringBuilder dataInc = new StringBuilder();
                StringBuilder dataBT = new StringBuilder();
                StringBuilder dataDepth = new StringBuilder();
                StringBuilder dataXaxis = new StringBuilder();
                StringBuilder dataTVD = new StringBuilder();
                StringBuilder dataNorthing = new StringBuilder();
                StringBuilder dataEasting = new StringBuilder();
                // Loading Data

                dataInc.Append(" data: [ ");
                dataDip.Append(" data: [ ");
                dataAzim.Append(" data: [ ");
                dataBT.Append(" data: [ ");
                dataDepth.Append(" data: [");
                dataXaxis.Append(" data: [");
                dataTVD.Append(" data: [");
                dataNorthing.Append(" data: [");
                dataEasting.Append(" data: [");

                foreach (DataRow row in ChartData.Rows)
                {
                    dataXaxis.Append("\"" + row["Survey_number"] + "\"" + ",");
                    dataInc.Append(Math.Round((decimal)row["Inclination"], 2) + ",");
                    dataAzim.Append(Math.Round((decimal)row["Azimuth"],2) + ",");
                    dataDip.Append(Math.Round((decimal)row["Dip"], 2) + ",");
                    dataBT.Append(Math.Round((decimal)row["B-Total"], 0) + ",");
                    dataDepth.Append(row["MDepth"] + ",");
                    dataTVD.Append(Math.Round((decimal)row["TVD"], 2) + ",");
                    dataNorthing.Append(Math.Round((decimal)row["Northing"], 2) + ",");
                    dataEasting.Append(Math.Round((decimal)row["Easting"], 2) + ",");
                }

                dataInc.Append("],");
                dataAzim.Append("],");
                dataDip.Append("],");
                dataBT.Append("],");
                dataDepth.Append(" ], ");
                dataXaxis.Append(" ], ");
                dataTVD.Append(" ], ");
                dataNorthing.Append(" ], ");
                dataEasting.Append(" ], ");

                strScript.Append("<script type='text/javascript' src='../../eCharts/echarts.min.js'></script>");
                strScript.Append("<script type='text/javascript'>");

                
                strScript.Append(" var dom = document.getElementById('" + div + "'); "); 
                strScript.Append(" var myChart = echarts.init(dom); ");
                strScript.Append(@"

                            var app = {};
                            option = null;


                            option = {
                                title: {
                                    text: 'SMARTS QC Analysis ',
                                    subtext: 'Drilling Logs Detail: DIP,Inclication,BTotal,Measured Depth,Northing,Easting and TVD\nwww.smartwelldynamics.com',
                                    x: 'right'
                                },
                                tooltip: {
                                    trigger: 'axis',
                                    axisPointer: {
                                        animation: false
                                    }
                                },
                                legend: {
                                    data: ['TVD', 'Northing', 'Easting' , 'Azimuth', 'DIP', 'Inclication', 'BTotal'],
                                    x: 'left'
                                },
                                toolbox: {
                                    show: true,
                                    showTitle: true,
                                    itemGap: 20,
                                    orient: 'vertical',
                                    left: 'right',
                                    top: '80',
                                    feature: {
                                        magicType: { show: true, type: ['line', 'bar'], title: { line: 'Line', bar: 'Bar' } },
                                        restore: { show: true, title: 'Restore' },
                                        saveAsImage: { show: true, title: 'Save' }
                                    }
                                },
                                axisPointer: {
                                    link: { xAxisIndex: 'all' }
                                },
                                dataZoom: [
                                    {
                                        show: true,
                                        realtime: true,
                                        start: 0,
                                        //end: max,
                                        xAxisIndex: [0, 1]
                                    },
                                    {
                                        type: 'inside',
                                        realtime: true,
                                        start: 0,
                                        //end: max,
                                        xAxisIndex: [0, 1]
                                    }
                                ],
                                grid: [
                                    { left: 80, right: 80, height: '30%' },
                                    { left: 80, right: 80, top: '50%', height: '35%' }
                                ],
                                xAxis: [
                                    {
                                        type: 'category',
                                        boundaryGap: false,
                                        axisLine: { onZero: true },
                                        axisTick: {
                                            alignWithLabel: true
                                        },
                                        axisPointer: {
                                            label: {
                                                formatter: function (params) {
                                                    return 'Measured Depth : ' + params.value;
                                                }
                                            }
                                        },
                                    ");
                                    strScript.Append(dataDepth + "},");  // X Axis Graph Grid 1  
                                    strScript.Append(@"
                                                                            
                                    {
                                        gridIndex: 1,
                                        type: 'category',
                                        boundaryGap: false,
                                        axisLine: { onZero: true },
                                        axisTick: {
                                            alignWithLabel: true
                                        },
                                        axisPointer: {
                                            label: {
                                                formatter: function (params) {
                                                    return 'Measured Depth : ' + params.value;
                                                }
                                            }
                                        },
                                    ");
                                    strScript.Append(dataDepth + "}");  // X Axis Graph Grid 2  
                                    strScript.Append(@"                                        
                                                          
                                ],
                                yAxis: [
                                    {
                                        name: 'TVD / Northing / Easting',
                                        type: 'value',
                                        inverse: true,
                                        nameLocation: 'middle',
                                        nameTextStyle: {
                                            align: 'middle',
                                            verticalAlign: 'bottom',
                                            padding: [25, 25, 40, 25]
                                        }

                                       
                                    },

                                    {
                                        gridIndex: 1,
                                        name: ' Inclication / Azimuth / Dip',
                                        nameLocation: 'middle',
                                        type: 'value',
                                        nameTextStyle: {
                                            align: 'middle',
                                            verticalAlign: 'bottom',
                                            padding: [25, 25, 25, 25]
                                        }
                                    },
                                    {
                                        gridIndex: 1,
                                        name: ' BTotal',
                                        type: 'value',
                                        position: 'right',
                                        nameLocation: 'middle',
                                        min: 'dataMin',
                                        nameTextStyle: {
                                            align: 'center',
                                            verticalAlign: 'middle',
                                            padding: [40, 0, 0, 25]
                                        }
                                        
                                    }
                                ],
                                series: [
                                    {
                                        name: 'TVD',
                                        type: 'line',
                                        symbolSize: 6,
                                        yAxisIndex: 0,
                                        hoverAnimation: false,
                                     ");
                                        strScript.Append(dataTVD + "},");  // Y Axis Graph Grid 1
                                        strScript.Append(@"
                                    
                                    {
                                        name: 'Northing',
                                        type: 'line',
                                        symbolSize: 6,
                                        yAxisIndex: 0,
                                        hoverAnimation: false,
                                    ");
                                        strScript.Append(dataNorthing + "},");  // Y Axis Graph Grid 1 
                                        strScript.Append(@"
                                    {   
                                        name: 'Easting',
                                        type: 'line',
                                        symbolSize: 6,
                                        yAxisIndex: 0,
                                        hoverAnimation: false,
                                    ");

                                        strScript.Append(dataEasting + "},");  // Y Axis Graph Grid 1 
                                        strScript.Append(@"
                                    
                                    {
                                        name: 'Azimuth',
                                        type: 'line',
                                        xAxisIndex: 1,
                                        yAxisIndex: 1,
                                        symbolSize: 8,
                                        hoverAnimation: false,
                                    ");
                                        strScript.Append(dataAzim + "},");  // Y Axis Graph Grid 1  
                                        strScript.Append(@"
                                    
                                    {
                                        name: 'DIP',
                                        type: 'line',
                                        xAxisIndex: 1,
                                        yAxisIndex: 1,
                                        symbolSize: 8,
                                        hoverAnimation: false,
                                    ");
                                        strScript.Append(dataDip + "},");  // Y Axis Graph Grid 1  
                                        strScript.Append(@"
                                    
                                    {
                                        name: 'Inclication',
                                        type: 'line',
                                        xAxisIndex: 1,
                                        yAxisIndex: 1,
                                        symbolSize: 8,
                                        hoverAnimation: false,
                                    ");
                                        strScript.Append(dataInc + "},");  // Y Axis Graph Grid 1  
                                        strScript.Append(@"
                                    
                                    {
                                        name: 'BTotal',
                                        type: 'line',
                                        xAxisIndex: 1,
                                        yAxisIndex: 2,
                                        symbolSize: 8,
                                        hoverAnimation: false,
                                     ");
                                        strScript.Append(dataBT + "}");  // Y Axis Graph Grid 1  
                                        strScript.Append(@"
                                    
                                ]
                            };;
                        ");

                  strScript.Append("  if (option && typeof option === \"object\" ) {");
                  strScript.Append("     myChart.setOption(option, true); } ");                                                  
                  strScript.Append("</script>");                
                return strScript.ToString();             
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); 
            
            }    
            
            
        }

        public static String getChartsQCResultsECHRTWithPlan(String div, System.Data.DataTable ChartData, System.Data.DataTable ChartDataWithPlan )
        {
            try
            {
                StringBuilder strScript = new StringBuilder();
                StringBuilder dataAzim = new StringBuilder();
                StringBuilder dataDip = new StringBuilder();
                StringBuilder dataInc = new StringBuilder();
                StringBuilder dataBT = new StringBuilder();
                StringBuilder dataDepth = new StringBuilder();
                StringBuilder dataXaxis = new StringBuilder();
                StringBuilder dataTVD = new StringBuilder();
                StringBuilder dataNorthing = new StringBuilder();
                StringBuilder dataEasting = new StringBuilder();

                StringBuilder plandataXaxis = new StringBuilder();
                StringBuilder plandataAzim = new StringBuilder();
                StringBuilder plandataInc = new StringBuilder();
                StringBuilder plandataDepth = new StringBuilder();
                StringBuilder plandataTVD = new StringBuilder();
                StringBuilder plandataNorthing = new StringBuilder();
                StringBuilder plandataEasting = new StringBuilder();

                // Loading Data

                dataInc.Append(" data: [ ");
                dataDip.Append(" data: [ ");
                dataAzim.Append(" data: [ ");
                dataBT.Append(" data: [ ");
                dataDepth.Append(" data: [");
                dataXaxis.Append(" data: [");
                dataTVD.Append(" data: [");
                dataNorthing.Append(" data: [");
                dataEasting.Append(" data: [");

                plandataXaxis.Append(" data: [");
                plandataInc.Append(" data: [ ");
                plandataAzim.Append(" data: [ ");
                plandataDepth.Append(" data: [");
                plandataTVD.Append(" data: [");
                plandataNorthing.Append(" data: [");
                plandataEasting.Append(" data: [");

                foreach (DataRow row in ChartData.Rows)
                {
                    dataXaxis.Append("\"" + row["Survey_number"] + "\"" + ",");
                    dataInc.Append(Math.Round((decimal)row["Inclination"], 2) + ",");
                    dataAzim.Append(Math.Round((decimal)row["Azimuth"], 2) + ",");
                    dataDip.Append(Math.Round((decimal)row["Dip"], 2) + ",");
                    dataBT.Append(Math.Round((decimal)row["B-Total"], 0) + ",");
                    dataDepth.Append(row["MDepth"] + ",");
                    dataTVD.Append(Math.Round((decimal)row["TVD"] , 2) + ",");
                    dataNorthing.Append(Math.Round((decimal)row["Northing"], 2) + ",");
                    dataEasting.Append(Math.Round((decimal)row["Easting"], 2) + ",");
                }

                foreach (DataRow row in ChartDataWithPlan.Rows)
                {
                    plandataXaxis.Append("\"" + row["Survey_number"] + "\"" + ",");
                    plandataInc.Append(Math.Round((decimal)row["planInclination"], 2) + ",");
                    plandataAzim.Append(Math.Round((decimal)row["planAzimuth"], 2) + ",");
                    plandataDepth.Append(row["planDepth"] + ",");
                    plandataTVD.Append(Math.Round((decimal)row["planTVD"], 2) + ",");
                    plandataNorthing.Append(Math.Round((decimal)row["planNorthing"], 2) + ",");
                    plandataEasting.Append(Math.Round((decimal)row["planEasting"], 2) + ",");
                }

                dataInc.Append("],");
                dataAzim.Append("],");
                dataDip.Append("],");
                dataBT.Append("],");
                dataDepth.Append(" ], ");
                dataXaxis.Append(" ], ");
                dataTVD.Append(" ], ");
                dataNorthing.Append(" ], ");
                dataEasting.Append(" ], ");

                plandataXaxis.Append(" ], ");
                plandataInc.Append("],");
                plandataAzim.Append("],");
                plandataDepth.Append(" ], ");
                plandataTVD.Append(" ], ");
                plandataNorthing.Append(" ], ");
                plandataEasting.Append(" ], ");

                strScript.Append("<script type='text/javascript' src='../../eCharts/echarts.min.js'></script>");
                strScript.Append("<script type='text/javascript'>");


                strScript.Append(" var dom = document.getElementById('" + div + "'); ");
                strScript.Append(" var myChart = echarts.init(dom); ");
                strScript.Append(@"

                            var app = {};
                            option = null;


                            option = {
                                title: {
                                    text: 'SMARTS QC Analysis with Well Plan Data',
                                    subtext: 'Drilling Logs Detail: Measured Depth, Northing, Easting and TVD (Planned vs Raw)\nwww.smartwelldynamics.com',
                                    x: 'right'
                                },
                                tooltip: {
                                    trigger: 'axis',
                                    axisPointer: {
                                        animation: false,
                                        type: 'cross',
                                        label: {
                                            backgroundColor: '#283b56'
                                        }
                                    }
                                },
                                legend: {
                                    data: ['TVD', 'Planned TVD', 'Planned Northing', 'Northing', 'Planned Easting', 'Easting' ],
                                    x: 'left'
                                },
                                toolbox: {
                                    show: true,
                                    showTitle: true,
                                    itemGap: 20,
                                    orient: 'vertical',
                                    left: 'right',
                                    top: '80',
                                    feature: {
                                        magicType: { show: true, type: ['line', 'bar'], title: { line: 'Line', bar: 'Bar' } },
                                        restore: { show: true, title: 'Restore' },
                                        saveAsImage: { show: true, title: 'Save' }
                                    }
                                },
                                axisPointer: {
                                    link: { xAxisIndex: 'all' }
                                },
                                dataZoom: [
                                    {
                                        show: true,
                                        realtime: true,
                                        start: 0,
                                        //end: max,
                                        xAxisIndex: [0, 1]
                                    }

                                ],
                                grid: [
                                    { left: 80, right: 80,  top: '18%' , height: '68%' }

                                ],
                                xAxis: [
                                    {
                                        type: 'category',
                                        axisLine: { onZero: true },
                                        axisTick: {
                                            alignWithLabel: true
                                        },
                                        axisPointer: {
                                            label: {
                                                formatter: function (params) {
                                                    return 'Measured Depth : ' + params.value;
                                                }
                                            }
                                        },
                                    ");
                strScript.Append(dataDepth + "},");   
                strScript.Append(@"
                                                                            
                                    {   
                                        name:  'Planned Depth',
                                        nameLocation: 'middle',                                    
                                        type: 'category',
                                        axisLine: { onZero: true , lineStyle: {  color: '#675bba' } },
                                        axisTick: {
                                            alignWithLabel: true
                                        },
                                        nameTextStyle: {
                                            align: 'middle',
                                            //verticalAlign: 'bottom',
                                            padding: [0, 0, 25, 0]
                                        },
                                        axisPointer: {
                                            label: {
                                                formatter: function (params) {
                                                    return 'Planned Measured Depth : ' + params.value;
                                                }
                                            }
                                        },
                                    ");
                strScript.Append(plandataDepth + "}");    
                strScript.Append(@"                                        
                                                          
                                ],
                                yAxis: [
                                    {
                                        name: 'TVD / Northing / Easting (Raw vs Planned)',
                                        type: 'value',
                                        inverse: true,
                                        nameLocation: 'middle',
                                        nameTextStyle: {
                                            align: 'middle',
                                            verticalAlign: 'bottom',
                                            padding: [25, 25, 40, 25]
                                        }

                                       
                                    }

                                ],
                                series: [
                                    {
                                        name: 'TVD',
                                        type: 'line',
                                        symbolSize: 6,
                                        yAxisIndex: 0,
                                        hoverAnimation: false,
                                     ");
                strScript.Append(dataTVD + "},");  
                strScript.Append(@"
                                    {
                                        name: 'Planned TVD',
                                        type: 'line',
                                        symbolSize: 6,
                                        xAxisIndex: 1,
                                        yAxisIndex: 0,
                                        hoverAnimation: true,
                                     ");
                strScript.Append(plandataTVD + "},");  
                strScript.Append(@"                    
                                    {
                                        name: 'Northing',
                                        type: 'line',
                                        symbolSize: 6,
                                        yAxisIndex: 0,
                                        hoverAnimation: false,
                                    ");
                strScript.Append(dataNorthing + "},");  
                strScript.Append(@"
                                    {
                                        name: 'Planned Northing',
                                        type: 'line',
                                        symbolSize: 6,
                                        xAxisIndex: 1,                                        
                                        yAxisIndex: 0,
                                        hoverAnimation: true,
                                    ");
                strScript.Append(plandataNorthing + "},");  
                strScript.Append(@"
                                    {   
                                        name: 'Easting',
                                        type: 'line',
                                        symbolSize: 6,
                                        yAxisIndex: 0,
                                        hoverAnimation: false,
                                    ");

                strScript.Append(dataEasting + "},");   
                strScript.Append(@"
                                    {   
                                        name: 'Planned Easting',
                                        type: 'line',
                                        symbolSize: 6,
                                        xAxisIndex: 1,
                                        yAxisIndex: 0,
                                        hoverAnimation: true,
                                    ");

                strScript.Append(plandataEasting + "},");  // Y Axis Graph Grid 1 

                strScript.Append(@"
                                    
                                ]
                            };;
                        ");

                strScript.Append("  if (option && typeof option === \"object\" ) {");
                strScript.Append("     myChart.setOption(option, true); } ");
                strScript.Append("</script>");
                return strScript.ToString();
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());

            }


        }

        public static String getChartsQCResultsECHRT_AZMWithPlan(String div, System.Data.DataTable ChartData, System.Data.DataTable ChartDataWithPlan)
        {
            try
            {
                StringBuilder strScript = new StringBuilder();
                StringBuilder dataXaxis = new StringBuilder();
                StringBuilder dataDepth = new StringBuilder();
                StringBuilder dataAzim = new StringBuilder();
                StringBuilder dataShortAzim = new StringBuilder();
                StringBuilder dataBHAAzim = new StringBuilder();
                StringBuilder plandataXaxis = new StringBuilder();
                StringBuilder plandataDepth = new StringBuilder();
                StringBuilder plandataAzim = new StringBuilder();


                // Loading Data

                dataXaxis.Append(" data: [");
                dataDepth.Append(" data: [");
                dataAzim.Append(" data: [ ");
                dataShortAzim.Append(" data: [ ");
                dataBHAAzim.Append(" data: [");

                plandataXaxis.Append(" data: [");
                plandataDepth.Append(" data: [");
                plandataAzim.Append(" data: [ ");

                foreach (DataRow row in ChartData.Rows)
                {
                    dataXaxis.Append("\"" + row["Survey_number"] + "\"" + ",");
                    dataDepth.Append(row["MDepth"] + ",");
                    dataAzim.Append(row["Azimuth"] + ",");
                    dataBHAAzim.Append(Math.Round((decimal)row["BHAAzimuth"], 2) + ",");
                    dataShortAzim.Append(Math.Round((decimal)row["ShortAzimuth"], 2) + ",");

                }

                foreach (DataRow row in ChartDataWithPlan.Rows)
                {
                    plandataXaxis.Append("\"" + row["Survey_number"] + "\"" + ",");
                    plandataAzim.Append(Math.Round((decimal)row["planAzimuth"], 2) + ",");
                    plandataDepth.Append(row["planDepth"] + ",");

                }

                dataXaxis.Append(" ], ");
                dataDepth.Append(" ], ");
                dataAzim.Append("],");
                dataBHAAzim.Append("],");
                dataShortAzim.Append("],");

                plandataXaxis.Append(" ], ");
                plandataDepth.Append(" ], ");
                plandataAzim.Append("],");

                strScript.Append("<script type='text/javascript' src='../../eCharts/echarts.min.js'></script>");
                strScript.Append("<script type='text/javascript'>");

                strScript.Append(" var dom = document.getElementById('" + div + "'); ");
                strScript.Append(" var myChart = echarts.init(dom); ");
                strScript.Append(@"

                            var app = {};
                            option = null;


                            option = {
                                title: {
                                    text: 'SMARTS QC Azimuth Analysis',
                                    subtext: 'Drilling Logs Detail: Azimuth, Short Color Azimuth, BHA Azimuth, Planned Azimuth\nwww.smartwelldynamics.com',
                                    x: 'right'
                                },
                                tooltip: {
                                    trigger: 'axis',
                                    axisPointer: {
                                        animation: false
                                    }
                                },
                                legend: {
                                    data: [ 'Azimuth', 'Short_Color_Azimuth', 'BHA_Azimuth' , 'Planned_Azimuth'],
                                    x: 'left'
                                },
                                toolbox: {
                                    show: true,
                                    showTitle: true,
                                    itemGap: 20,
                                    orient: 'vertical',
                                    left: 'right',
                                    top: '80',
                                    feature: {
                                        magicType: { show: true, type: ['line', 'bar'], title: { line: 'Line', bar: 'Bar' } },
                                        restore: { show: true, title: 'Restore' },
                                        saveAsImage: { show: true, title: 'Save' }
                                    }
                                },
                                axisPointer: {
                                    link: { xAxisIndex: 'all' }
                                },
                                dataZoom: [
                                    {
                                        type: 'slider',
                                        realtime: true,
                                        start: 0,
                                        
                                    },
                                ],
                                grid: 
                                   {  left: 80, right: 80, top: '18%',  height: '65%' },
                                
                                xAxis: [
                                    {
                                        type: 'category',
                                        boundaryGap: false,
                                        axisLine: { onZero: true },
                                        axisTick: {
                                            alignWithLabel: true
                                        },
                                        axisPointer: {
                                            label: {
                                                formatter: function (params) {
                                                    return 'Measured Depth : ' + params.value;
                                                }
                                            }
                                        },
                                    ");

                strScript.Append(dataDepth + "},");  
                strScript.Append(@"
                                                                            
                                    {   
                                        name:  'Planned Depth',
                                        nameLocation: 'middle',                                    
                                        type: 'category',
                                        axisLine: { onZero: true , lineStyle: {  color: '#675bba' } },
                                        axisTick: {
                                            alignWithLabel: true
                                        },
                                        nameTextStyle: {
                                            align: 'middle',
                                            //verticalAlign: 'bottom',
                                            padding: [0, 0, 25, 0]
                                        },
                                        axisPointer: {
                                            label: {
                                                formatter: function (params) {
                                                    return 'Planned Measured Depth : ' + params.value;
                                                }
                                            }
                                        },
                                    ");
                strScript.Append(plandataDepth + "}");    
                strScript.Append(@"                                        
                                                          
                                ],
                                yAxis: [
                                    {
                                        name: 'Azimuth ( Raw / Short Color / BHA Azimuth / Planned )',
                                        type: 'value',
                                        inverse: true,
                                        min: 'dataMin',
                                        nameLocation: 'middle',
                                        nameTextStyle: {
                                            align: 'middle',
                                            verticalAlign: 'bottom',
                                            padding: [25, 25, 40, 25]
                                        }
                                      
                                    },

                                ],
                                series: [
                                    {
                                        name: 'Azimuth',
                                        type: 'line',
                                        symbolSize: 6,
                                        yAxisIndex: 0,
                                        hoverAnimation: false,
                                     ");

                strScript.Append(dataAzim + "},");  // Y Axis
                strScript.Append(@"
                                    
                                    {
                                        name: 'Short_Color_Azimuth',
                                        type: 'line',
                                        symbolSize: 6,
                                        yAxisIndex: 0,
                                        hoverAnimation: false,
                                    ");

                strScript.Append(dataShortAzim + "},");  // Y Axis
                strScript.Append(@"
                                    
                                    {
                                        name: 'Planned_Azimuth',
                                        type: 'line',
                                        symbolSize: 6,
                                        xAxisIndex: 1,
                                        yAxisIndex: 0,
                                        hoverAnimation: false,
                                    ");

                strScript.Append(plandataAzim + "},");  // Y Axis  
                strScript.Append(@"
                                    {   
                                        name: 'BHA_Azimuth',
                                        type: 'line',
                                        symbolSize: 6,
                                        yAxisIndex: 0,
                                        hoverAnimation: false,
                                    ");

                strScript.Append(dataBHAAzim + "},");  // Y Axis                                    
                strScript.Append(@"
                                    
                                ]
                            };;
                        ");

                strScript.Append("  if (option && typeof option === \"object\" ) {");
                strScript.Append("     myChart.setOption(option, true); } ");

                strScript.Append("</script>");


                return strScript.ToString();
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());

            }


        }

        public static String getChartsQCResultsECHRT_3D_Definitive(String divID, String WellName, System.Data.DataTable ChartData, System.Data.DataTable  EastingNorthingVals ,decimal maxTVD)
        {
            try
            {
                StringBuilder strScript = new StringBuilder();
                StringBuilder dataFOR3D = new StringBuilder();

                StringBuilder minNorthing = new StringBuilder(); StringBuilder maxNorthing = new StringBuilder();
                StringBuilder minEasting = new StringBuilder(); StringBuilder maxEasting = new StringBuilder();

                strScript.Append("<script type='text/javascript' src='../../eCharts/echarts3D.min.js'></script>");
                strScript.Append("<script type='text/javascript' src='../../eCharts/echarts-gl.min.js'></script>");
                strScript.Append("<script type='text/javascript'>");


                strScript.Append(" var dom3D = document.getElementById('" + divID + "'); ");
                strScript.Append(" var myChart3D = echarts.init(dom3D); ");
                strScript.Append(" var MAXTVD =" + ((maxTVD + 500) * -1) + ";");   // MAXTVD is multiplied by Max TVD and extra 500 units are added

                foreach (DataRow row in EastingNorthingVals.Rows)
                {

                    maxNorthing.Append(row["maxNorthing"]);
                    minNorthing.Append(row["minNorthing"]);
                    maxEasting.Append(row["maxEasting"]);
                    minEasting.Append(row["minEasting"]);

                }

                strScript.Append("  var maxX =" + maxEasting + ";");
                strScript.Append("  var minX =" + minEasting + ";");
                strScript.Append("  var maxY =" + maxNorthing + ";");
                strScript.Append("  var minY =" + minNorthing + ";");

                strScript.Append(@"

                var sizeValue = '65%';
                var width2D = '20%';
                var colors = ['#5793f3', '#d14a61', '#675bba'];           
                         myChart3D.setOption({
                                    title: {
                                        text: 'SMARTS QC Analysis in 3D',
                                        subtext: 'www.smartwelldynamics.com',
                                        x: 'right'
                                    },
                                    toolbox: {
                                        show: true, showTitle: true, itemGap: 20, orient: 'vertical', left: 'right',
                                        top: '80',
                                        feature: {
                                            restore: { show: true, title: 'Restore' },
                                            saveAsImage: { show: true, title: 'Save' }
                                        }
                                    },
					                legend: {
                                        data: ['Definitive (Placement)'],
						                align: 'left',
						                left: 10,
                                        top: 15,
    					                x: 'left',
                        
                                    },
                                    visualMap: {
                                    max: MAXTVD,
                                    dimension: 'Depth',
                                    type: 'continuous',
                                    itemHeight: 250,
                                    calculable: true,
                                    text: ['Depth (start)', 'Depth (lowest)'],
                                    top: 150,
                                    left: 5,
                                            
                                    inRange: {
                                        colorLightness: [0.6, 0.5],                           
                                        color: ['#313695', '#4575b4', '#74add1', '#abd9e9', '#e0f3f8', '#ffffbf', '#fee090', '#fdae61', '#f2b77d', '#e8d4c0']
                                    },
                                    controller: {
                                        inRange: {
                                            //color: '#cc1a1a'
                                            color: '#9b420f'
                                        }
                                    },
                                   // outOfRange: {
                                   //     color: '#e50dcf'
                                   // },


                                    },                                   


                                    xAxis3D: [
                                        { type: 'value', name: 'Easting',  min: minX , max : maxX  },
                                    ],

                                    yAxis3D: [
                                        { type: 'value', name: 'Northing',  min: minY , max : maxY  },

                                    ],

                                    zAxis3D: [
                                        { type: 'value', name: 'Depth', axisLabel: { formatter: function (value)  {   return value*-1; }  }  }

                                    ],

                                    xAxis: [
                                        { type: 'value', gridIndex: 0, name: 'Easting', axisLabel: { rotate: 50, interval: 0 } },
                                        { type: 'value', gridIndex: 1, name: 'Northing', axisLabel: { rotate: 50, interval: 0 } },
                                        { type: 'value', gridIndex: 2, name: 'Easting', axisLabel: { rotate: 50, interval: 0 } },
                                        { type: 'value', gridIndex: 3, name: 'TVD', axisLabel: { rotate: 50, interval: 0 } }
                                    ],

                                    yAxis: [
                                        { type: 'value', gridIndex: 0, name: 'TVD', },
                                        { type: 'value', gridIndex: 1, name: 'TVD', },
                                        { type: 'value', gridIndex: 2, name: 'Northing', },
                                        { type: 'value', gridIndex: 3, name: 'MDepth', },
                                    ],

                                    grid: [
                                    { left: '73%', width: width2D, bottom: '80%' },
                                    { left: '73%', width: width2D, bottom: '55%', top: '30%' },
                                    { left: '73%', width: width2D, bottom: '27%', top: '55%' },
                                    { left: '73%', width: width2D, top: '80%' },
                                    ],

                                    legend: { },

                                    tooltip: {
                                        backgroundColor: '#0a2131'
                                    },
                                    grid3D: {
                                        boxWidth: 75,
                                        width: '75%',
                                        axisLine: {
                                            lineStyle: {
                                                color: '#04131c'
                                            }
                                        },
                                        containLabel: true,
                                        axisPointer: {
                                            lineStyle: {
                                                color: '#ff67cc'
                                            }
                                        }
                                    },
 
                                    dataset:{
                                    
                                    
                                    dimensions: ['Measured Depth', 'Inclination', 'Azimuth', 'TVD', 'Northing', 'Easting', 'Name', 'Depth' , 'Northing_UTM' , 'Easting_UTM'],                
                                      
                                    source: [
					                    
                ");

                // data set continue ... Loading Data


                dataFOR3D.Append(" ['Measured Depth', 'Inclination', 'Azimuth', 'TVD', 'Northing', 'Easting', 'Name',  'Depth' , 'Northing_UTM' , 'Easting_UTM'],");

                foreach (DataRow row in ChartData.Rows)
                {
                    dataFOR3D.Append(" ['" + row["MDepth"] + "','" + Math.Round((decimal)row["Inclination"], 2) + "','" + row["Azimuth"] + "','" + Math.Round((decimal)row["TVD"], 2) + "','" + Math.Round((decimal)row["Northing"], 2) + "','" + Math.Round((decimal)row["Easting"], 2) + "','" + WellName + "','" + Math.Round((decimal)row["Depth"], 2) * -1.0M + "','" + Math.Round((decimal)row["CummDeltaNorthing"], 2) + "','" + Math.Round((decimal)row["CummDeltaEasting"], 2) + "'],");

                }

                dataFOR3D.Append("] },");// End of data load sourse

                strScript.Append(dataFOR3D);

                strScript.Append(@"                              
                                    series: [
                                    {                        
                                    name: 'Definitive (Placement)',
                                    type: 'scatter3D',
                                    symbolSize: 4.5,
                                    itemStyle: {
                                        borderWidth: 3,
                                        borderColor: 'rgba(255,255,255,0.9'
                                    },
                                    encode: {
                                        x: 'Easting_UTM',
                                        y: 'Northing_UTM',
                                        z: 'Depth',
                                        tooltip: [0, 1, 2, 3, 4, 5, 8, 9 , 6]
                                    },
                                    emphasis: {
                                        label: {
                                            textStyle: {
                                                fontSize: 20,
                                                color: '#900'
                                            }
                                        },
                                        itemStyle: {
                                            color: '#900'
                                        }
                                    }
                                },
                                  {
                                      type: 'scatter',
                                        symbolSize: 3.5,
                                      xAxisIndex: 0,
                                      yAxisIndex: 0,
                                      encode: {
                                          x: 'Easting',
                                          y: 'TVD',
                                          tooltip: [0, 1, 2, 3, 4, 5, 6]
                                      }
                                  },
                                    {
                                        type: 'scatter',
                                            symbolSize: 3.5,
                                        xAxisIndex: 1,
                                        yAxisIndex: 1,
                                        encode: {
                                            x: 'Northing',
                                            y: 'TVD',
                                            tooltip: [0, 1, 2, 3, 4, 5, 6]
                                        }
                                    },
                                    {
                                        type: 'scatter',
                                            symbolSize: 3.5,
                                        xAxisIndex: 2,
                                        yAxisIndex: 2,
                                        encode: {
                                            x: 'Easting',
                                            y: 'Northing',
                                            tooltip: [0, 1, 2, 3, 4, 5, 6]
                                        }
                                    },
                                    {
                                        type: 'scatter',
                                            symbolSize: 3.5,
                                        xAxisIndex: 3,
                                        yAxisIndex: 3,
                                        encode: {
                                            x: 'TVD',
                                            y: 'MDepth',
                                            tooltip: [0, 1, 2, 3, 4, 5, 6]
                                        }
                                    },

                                   ]
                                });
                                window.addEventListener('resize', function () {
                                myChart3D.resize();
                                });
                
               ");

                //strScript.Append("  if (option && typeof option === \"object\" ) {");
                //strScript.Append("     myChart.setOption(option, true); } ");

                strScript.Append("</script>");

                return strScript.ToString();

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }


        }
       
        public static String getChartsQCResultsECHRT_3D(String divID , String WellName , System.Data.DataTable ChartData, System.Data.DataTable ChartDataRAW , System.Data.DataTable ChartDataBHA , System.Data.DataTable ChartDataSC , System.Data.DataTable  EastingNorthingVals , decimal maxTVD)
        {
            try
            {
                StringBuilder strScript = new StringBuilder();
                StringBuilder dataFOR3D = new StringBuilder();

                StringBuilder minNorthing = new StringBuilder(); StringBuilder maxNorthing = new StringBuilder();
                StringBuilder minEasting = new StringBuilder(); StringBuilder maxEasting = new StringBuilder();
                
                strScript.Append("<script type='text/javascript' src='../../eCharts/echarts3D.min.js'></script>");
                strScript.Append("<script type='text/javascript' src='../../eCharts/echarts-gl.min.js'></script>");                
                strScript.Append("<script type='text/javascript'>");


                strScript.Append(" var dom3D = document.getElementById('" + divID + "'); ");
                strScript.Append(" var myChart3D = echarts.init(dom3D); ");
                strScript.Append(" var MAXTVD =" + ((maxTVD + 500) * -1) +";");   // MAXTVD is multiplied by Max TVD and extra 500 units are added

                foreach (DataRow row in EastingNorthingVals.Rows)
                {

                    maxNorthing.Append(row["maxNorthing"]);
                    minNorthing.Append(row["minNorthing"]);
                    maxEasting.Append(row["maxEasting"]);
                    minEasting.Append(row["minEasting"]);

                }

                strScript.Append("  var maxX =" + maxEasting + ";");
                strScript.Append("  var minX =" + minEasting + ";");
                strScript.Append("  var maxY =" + maxNorthing + ";");
                strScript.Append("  var minY =" + minNorthing + ";");

                strScript.Append(@"

                var sizeValue = '65%';
                var width2D = '20%';
                var colors = ['#5793f3', '#d14a61', '#675bba'];           
                         myChart3D.setOption({
                                    title: {
                                        text: 'SMARTS QC Analysis in 3D',
                                        subtext: 'www.smartwelldynamics.com',
                                        x: 'right'
                                    },
                                    toolbox: {
                                        show: true, showTitle: true, itemGap: 20, orient: 'vertical', left: 'right',
                                        top: '80',
                                        feature: {
                                            restore: { show: true, title: 'Restore' },
                                            saveAsImage: { show: true, title: 'Save' }
                                        }
                                    },
					                legend: {
                                        data: ['Plan', 'RAW', 'BHA' , 'Short collar' ,'Placement (Definitive)'],
						                align: 'left',
						                left: 10,
                                        top: 15,
    					                x: 'left',
                        
                                    },
                                    visualMap: {
                                    max: MAXTVD,
                                    dimension: 'Depth',
                                    type: 'continuous',
                                    itemHeight: 250,
                                    calculable: true,
                                    text: ['Depth (start)', 'Depth (lowest)'],
                                    top: 150,
                                    left: 5,
                                            
                                    inRange: {
                                        colorLightness: [0.6, 0.5],                           
                                        color: ['#313695', '#4575b4', '#74add1', '#abd9e9', '#e0f3f8', '#ffffbf', '#fee090', '#fdae61', '#f2b77d', '#e8d4c0']
                                    },
                                    controller: {
                                        inRange: {
                                            //color: '#cc1a1a'
                                            color: '#9b420f'
                                        }
                                    },


                                    },                                   


                                    xAxis3D: [
                                        { type: 'value', name: 'Easting', min: minX , max : maxX  },
                                    ],

                                    yAxis3D: [
                                        { type: 'value', name: 'Northing', min: minY , max : maxY  },

                                    ],

                                    zAxis3D: [
                                        { type: 'value', name: 'Depth', axisLabel: { formatter: function (value)  {   return value*-1; }  }  }

                                    ],

                                    xAxis: [
                                        { type: 'value', gridIndex: 0, name: 'Easting', axisLabel: { rotate: 50, interval: 0 } },
                                        { type: 'value', gridIndex: 1, name: 'Northing', axisLabel: { rotate: 50, interval: 0 } },
                                        { type: 'value', gridIndex: 2, name: 'Easting', axisLabel: { rotate: 50, interval: 0 } },
                                        { type: 'value', gridIndex: 3, name: 'TVD', axisLabel: { rotate: 50, interval: 0 } }
                                    ],

                                    yAxis: [
                                        { type: 'value', gridIndex: 0, name: 'TVD', },
                                        { type: 'value', gridIndex: 1, name: 'TVD', },
                                        { type: 'value', gridIndex: 2, name: 'Northing', },
                                        { type: 'value', gridIndex: 3, name: 'MDepth', },
                                    ],

                                    grid: [
                                    { left: '73%', width: width2D, bottom: '80%' },
                                    { left: '73%', width: width2D, bottom: '55%', top: '30%' },
                                    { left: '73%', width: width2D, bottom: '27%', top: '55%' },
                                    { left: '73%', width: width2D, top: '80%' },
                                    ],

                                    legend: { },

                                    tooltip: {
                                        backgroundColor: '#0a2131'
                                    },
                                    grid3D: {
                                        boxWidth: 75,
                                        width: '75%',
                                        axisLine: {
                                            lineStyle: {
                                                color: '#04131c'
                                            }
                                        },
                                        containLabel: true,
                                        axisPointer: {
                                            lineStyle: {
                                                color: '#ff67cc'
                                            }
                                        }
                                    },
 
                                    dataset:[
                                    {
                                    
                                    dimensions: ['Measured Depth', 'Inclination', 'Azimuth', 'TVD', 'Northing', 'Easting', 'Name', 'Depth', 'Northing_UTM' , 'Easting_UTM'],                
                                      
                                    source: [
					                    
                ");

                // data set continue ... Loading Data

                dataFOR3D.Append(" ['Measured Depth', 'Inclination', 'Azimuth', 'TVD', 'Northing', 'Easting', 'Name',  'Depth' , 'Northing_UTM' , 'Easting_UTM'],");

                foreach (DataRow row in ChartData.Rows)
                {
                    dataFOR3D.Append(" ['" + row["MDepth"] + "','" + Math.Round((decimal)row["Inclination"], 2) + "','" + row["Azimuth"] + "','" + Math.Round((decimal)row["TVD"], 2) + "','" + Math.Round((decimal)row["Northing"], 2) + "','" + Math.Round((decimal)row["Easting"], 2) + "','" + WellName + "','" + Math.Round((decimal)row["Depth"], 2) * -1.0M + "','" + Math.Round((decimal)row["CummDeltaNorthing"], 2) + "','" + Math.Round((decimal)row["CummDeltaEasting"], 2) + "'],");

                }

                dataFOR3D.Append("] },");// End of data load sourse

                // Start of data load 
                dataFOR3D.Append(@"{                    
                                   dimensions: ['Measured Depth', 'Inclination', 'Azimuth', 'TVD', 'Northing', 'Easting', 'Name', 'Depth', 'Northing_UTM' , 'Easting_UTM'],                                                      
                                   source: [  
                                      
                    ");// Start of Well plan data source 

                dataFOR3D.Append(" ['Measured Depth', 'Inclination', 'Azimuth', 'TVD', 'Northing', 'Easting', 'Name',  'Depth', 'Northing_UTM' , 'Easting_UTM'],");


                foreach (DataRow row in ChartDataRAW.Rows)
                {
                    dataFOR3D.Append(" ['" + row["MDepth"] + "','" + Math.Round((decimal)row["Inclination"], 2) + "','" + row["Azimuth"] + "','" + Math.Round((decimal)row["TVD"], 2) + "','" + Math.Round((decimal)row["Northing"], 2) + "','" + Math.Round((decimal)row["Easting"], 2) + "','" + WellName + "','" + Math.Round((decimal)row["Depth"], 2) * -1.0M + "','" + Math.Round((decimal)row["CummDeltaNorthing"], 2) + "','" + Math.Round((decimal)row["CummDeltaEasting"], 2) + "'],");

                }

                dataFOR3D.Append("] },");// End of data load sourse

                // Start of data load 
                dataFOR3D.Append(@"{                    
                                   dimensions: ['Measured Depth', 'Inclination', 'Azimuth', 'TVD', 'Northing', 'Easting', 'Name', 'Depth', 'Northing_UTM' , 'Easting_UTM'],                                                      
                                   source: [  
                                      
                    ");// Start of Well plan data source 

                dataFOR3D.Append(" ['Measured Depth', 'Inclination', 'Azimuth', 'TVD', 'Northing', 'Easting', 'Name',  'Depth', 'Northing_UTM' , 'Easting_UTM'],");


                foreach (DataRow row in ChartDataBHA.Rows)
                {
                    dataFOR3D.Append(" ['" + row["MDepth"] + "','" + Math.Round((decimal)row["Inclination"], 2) + "','" + row["Azimuth"] + "','" + Math.Round((decimal)row["TVD"], 2) + "','" + Math.Round((decimal)row["Northing"], 2) + "','" + Math.Round((decimal)row["Easting"], 2) + "','" + WellName + "','" + Math.Round((decimal)row["Depth"], 2) * -1.0M + "','" + Math.Round((decimal)row["CummDeltaNorthing"], 2) + "','" + Math.Round((decimal)row["CummDeltaEasting"], 2) + "'],");

                }

                dataFOR3D.Append("] },");// End of data load sourse

                // Start of data load 
                dataFOR3D.Append(@"{                    
                                   dimensions: ['Measured Depth', 'Inclination', 'Azimuth', 'TVD', 'Northing', 'Easting', 'Name', 'Depth', 'Northing_UTM' , 'Easting_UTM'],                                                      
                                   source: [  
                                      
                    ");// Start of Well plan data source 

                dataFOR3D.Append(" ['Measured Depth', 'Inclination', 'Azimuth', 'TVD', 'Northing', 'Easting', 'Name',  'Depth', 'Northing_UTM' , 'Easting_UTM'],");


                foreach (DataRow row in ChartDataSC.Rows)
                {
                    dataFOR3D.Append(" ['" + row["MDepth"] + "','" + Math.Round((decimal)row["Inclination"], 2) + "','" + row["Azimuth"] + "','" + Math.Round((decimal)row["TVD"], 2) + "','" + Math.Round((decimal)row["Northing"], 2) + "','" + Math.Round((decimal)row["Easting"], 2) + "','" + WellName + "','" + Math.Round((decimal)row["Depth"], 2) * -1.0M + "','" + Math.Round((decimal)row["CummDeltaNorthing"], 2) + "','" + Math.Round((decimal)row["CummDeltaEasting"], 2) + "'],");

                }

                dataFOR3D.Append("] }");// End of data load sourse

                dataFOR3D.Append("],");// End of Complete data for all the series 

                strScript.Append(dataFOR3D);                
                
                strScript.Append(@"                              
                                    series: [
                                    {                        
                                    name: 'Definitive (Placement)',
                                    type: 'scatter3D',
                                    symbolSize: 4.5,
                                    itemStyle: {
                                        borderWidth: 3,
                                        borderColor: 'rgba(255,255,255,0.9'
                                    },
                                    encode: {
                                        x: 'Easting_UTM',
                                        y: 'Northing_UTM',
                                        z: 'Depth',
                                        tooltip: [0, 1, 2, 3, 4, 5, 8, 9, 6]
                                    },
                                    emphasis: {
                                        label: {
                                            textStyle: {
                                                fontSize: 20,
                                                color: '#900'
                                            }
                                        },
                                        itemStyle: {
                                            color: '#900'
                                        }
                                    }
                                },

                                    {                        
                                    name: 'RAW',
                                    type: 'scatter3D',
					                datasetIndex: 1,
                                    symbolSize: 4.5,
                                    itemStyle: {
                                        borderWidth: 0,
                                        borderColor: 'rgba(255,255,255,0.9)'
                                    },
                                    encode: {
                                        x: 'Easting_UTM',
                                        y: 'Northing_UTM',
                                        z: 'Depth',
                                        tooltip: [0, 1, 2, 3, 4, 5, 8, 9, 6]
                                    },
                                    emphasis: {
                                        label: {
                                            textStyle: {
                                                fontSize: 20,
                                                color: '#900'
                                            }
                                        },
                                        itemStyle: {
                                            color: '#900'
                                        }
                                    }
                                },

                                    {                        
                                    name: 'BHA',
                                    type: 'scatter3D',
					                datasetIndex: 2,
                                    symbolSize: 4.5,
                                    itemStyle: {
                                        borderWidth: 0,
                                        borderColor: 'rgba(255,255,255,0.9)'
                                    },
                                    encode: {
                                        x: 'Easting_UTM',
                                        y: 'Northing_UTM',
                                        z: 'Depth',
                                        tooltip: [0, 1, 2, 3, 4, 5, 8, 9, 6]
                                    },
                                    emphasis: {
                                        label: {
                                            textStyle: {
                                                fontSize: 20,
                                                color: '#900'
                                            }
                                        },
                                        itemStyle: {
                                            color: '#900'
                                        }
                                    }
                                },
                                    {                        
                                    name: 'Short Collar',
                                    type: 'scatter3D',
					                datasetIndex: 3,
                                    symbolSize: 4.5,
                                    itemStyle: {
                                        borderWidth: 0,
                                        borderColor: 'rgba(255,255,255,0.9)'
                                    },
                                    encode: {
                                        x: 'Easting_UTM',
                                        y: 'Northing_UTM',
                                        z: 'Depth',
                                        tooltip: [0, 1, 2, 3, 4, 5, 8, 9, 6]
                                    },
                                    emphasis: {
                                        label: {
                                            textStyle: {
                                                fontSize: 20,
                                                color: '#900'
                                            }
                                        },
                                        itemStyle: {
                                            color: '#900'
                                        }
                                    }
                                },

                                  {
                                      type: 'scatter',
                                        symbolSize: 3.5,
                                      xAxisIndex: 0,
                                      yAxisIndex: 0,
                                      encode: {
                                          x: 'Easting',
                                          y: 'TVD',
                                          tooltip: [0, 1, 2, 3, 4, 5, 6]
                                      }
                                  },
                                    {
                                        type: 'scatter',
                                            symbolSize: 3.5,
                                        xAxisIndex: 1,
                                        yAxisIndex: 1,
                                        encode: {
                                            x: 'Northing',
                                            y: 'TVD',
                                            tooltip: [0, 1, 2, 3, 4, 5, 6]
                                        }
                                    },
                                    {
                                        type: 'scatter',
                                            symbolSize: 3.5,
                                        xAxisIndex: 2,
                                        yAxisIndex: 2,
                                        encode: {
                                            x: 'Easting',
                                            y: 'Northing',
                                            tooltip: [0, 1, 2, 3, 4, 5, 6]
                                        }
                                    },
                                    {
                                        type: 'scatter',
                                            symbolSize: 3.5,
                                        xAxisIndex: 3,
                                        yAxisIndex: 3,
                                        encode: {
                                            x: 'TVD',
                                            y: 'MDepth',
                                            tooltip: [0, 1, 2, 3, 4, 5, 6]
                                        }
                                    },

                                   ]
                                });
                                window.addEventListener('resize', function () {
                                myChart3D.resize();
                                });
                
               ");

                //strScript.Append("  if (option && typeof option === \"object\" ) {");
                //strScript.Append("     myChart.setOption(option, true); } ");

                strScript.Append("</script>");

                return strScript.ToString();

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }


        }

        public static String getChartsQCResultsECHRT_3DWithPlan(String divID, String WellName, System.Data.DataTable ChartData, System.Data.DataTable ChartDataRAW , System.Data.DataTable ChartDataBHA , System.Data.DataTable ChartDataSC , System.Data.DataTable ChartDataWithPlan , System.Data.DataTable  EastingNorthingVals, decimal maxTVD)
        {
            try
            {
                StringBuilder strScript = new StringBuilder();
                StringBuilder dataFOR3D = new StringBuilder();

                StringBuilder minNorthing = new StringBuilder(); StringBuilder maxNorthing = new StringBuilder();
                StringBuilder minEasting = new StringBuilder(); StringBuilder maxEasting = new StringBuilder();

                strScript.Append("<script type='text/javascript' src='../../eCharts/echarts3D.min.js'></script>");
                strScript.Append("<script type='text/javascript' src='../../eCharts/echarts-gl.min.js'></script>");
                strScript.Append("<script type='text/javascript'>");

                strScript.Append(" var dom3D = document.getElementById('" + divID + "'); ");
                strScript.Append(" var myChart3D = echarts.init(dom3D); ");
                strScript.Append(" var MAXTVD =" + ((maxTVD + 500) * -1) + ";");   // MAXTVD is multiplied by Max TVD and extra 500 units are added

                foreach (DataRow row in EastingNorthingVals.Rows)
                {

                    maxNorthing.Append(row["maxNorthing"]);
                    minNorthing.Append(row["minNorthing"]);
                    maxEasting.Append(row["maxEasting"]);
                    minEasting.Append(row["minEasting"]);

                }

                strScript.Append("  var maxX =" + maxEasting + ";");
                strScript.Append("  var minX =" + minEasting + ";");
                strScript.Append("  var maxY =" + maxNorthing + ";");
                strScript.Append("  var minY =" + minNorthing + ";");

                strScript.Append(@"

                var sizeValue = '65%';
                var width2D = '20%';
                var colors = ['#5793f3', '#d14a61', '#675bba'];           
                         myChart3D.setOption({
                                    title: {
                                        text: 'SMARTS QC Analysis in 3D (with Planned Well data)',
                                        subtext: 'www.smartwelldynamics.com',
                                        x: 'right'
                                    },
                                    toolbox: {
                                        show: true, showTitle: true, itemGap: 20, orient: 'vertical', left: 'right',
                                        top: '80',
                                        feature: {
                                            restore: { show: true, title: 'Restore' },
                                            saveAsImage: { show: true, title: 'Save' }
                                        }
                                    },
					                legend: {
                                        data: ['Plan', 'RAW', 'BHA' , 'Short collar' ,'Placement (Definitive)'],
						                align: 'left',
						                left: 10,
                                        top: 15,
    					                x: 'left',
                        
                                    },
                                    visualMap: {
                                    max: MAXTVD,
                                    dimension: 'Depth',
                                    type: 'continuous',
                                    itemHeight: 250,
                                    calculable: true,
                                    text: ['Depth (start)', 'Depth (lowest)'],
                                    top: 100,
                                    left: 5,
                                            
                                    inRange: {
                                        colorLightness: [0.6, 0.5],                           
                                        color: ['#313695', '#4575b4', '#74add1', '#abd9e9', '#e0f3f8', '#ffffbf', '#fee090', '#fdae61', '#f2b77d', '#e8d4c0']
                                    },
                                    controller: {
                                        inRange: {
                                            //color: '#cc1a1a'
                                            color: '#9b420f'
                                        }
                                    },

                                    },                                   


                                    xAxis3D: [
                                        { type: 'value', name: 'Easting', min: minX , max : maxX  },
                                    ],

                                    yAxis3D: [
                                        { type: 'value', name: 'Northing', min: minY , max : maxY },

                                    ],

                                    zAxis3D: [
                                        { type: 'value', name: 'Depth', axisLabel: { formatter: function (value)  {   return value*-1; }  }  }

                                    ],

                                    xAxis: [
                                        { type: 'value', gridIndex: 0, name: 'Easting', axisLabel: { rotate: 50, interval: 0 } },
                                        { type: 'value', gridIndex: 1, name: 'Northing', axisLabel: { rotate: 50, interval: 0 } },
                                        { type: 'value', gridIndex: 2, name: 'Easting', axisLabel: { rotate: 50, interval: 0 } },
                                        { type: 'value', gridIndex: 3, name: 'TVD', axisLabel: { rotate: 50, interval: 0 } }
                                    ],

                                    yAxis: [
                                        { type: 'value', gridIndex: 0, name: 'TVD', },
                                        { type: 'value', gridIndex: 1, name: 'TVD', },
                                        { type: 'value', gridIndex: 2, name: 'Northing', },
                                        { type: 'value', gridIndex: 3, name: 'MDepth', },
                                    ],

                                    grid: [
                                    { left: '73%', width: width2D, bottom: '80%' },
                                    { left: '73%', width: width2D, bottom: '55%', top: '30%' },
                                    { left: '73%', width: width2D, bottom: '27%', top: '55%' },
                                    { left: '73%', width: width2D, top: '80%' },
                                    ],

                                    legend: {
                                        alight: 'left',
                                        left: 30,
        
                                    },

                                    tooltip: {
                                        backgroundColor: '#0a2131'
                                    },
                                    grid3D: {
                                        boxWidth: 75,
                                        width: '75%',
                                        axisLine: {
                                            lineStyle: {
                                                color: '#04131c'
                                            }
                                        },
                                        containLabel: true,
                                        axisPointer: {
                                            lineStyle: {
                                                color: '#ff67cc'
                                            }
                                        }
                                    },
 
                                    dataset:[
                                    {
                                    
                                    dimensions: ['Measured Depth', 'Inclination', 'Azimuth', 'TVD', 'Northing', 'Easting', 'Name', 'Depth' , 'Northing_UTM' , 'Easting_UTM'],                
                                      
                                    source: [
					                    
                ");

                // data set continue ... Loading Data


                dataFOR3D.Append(" ['Measured Depth', 'Inclination', 'Azimuth', 'TVD', 'Northing', 'Easting', 'Name',  'Depth' , 'Northing_UTM' , 'Easting_UTM'],");

               foreach (DataRow row in ChartData.Rows)
               {
                  dataFOR3D.Append(" ['" + row["MDepth"] + "','" + Math.Round((decimal)row["Inclination"], 2) + "','" + row["Azimuth"] + "','" + Math.Round((decimal)row["TVD"], 2) + "','" + Math.Round((decimal)row["Northing"], 2) + "','" + Math.Round((decimal)row["Easting"], 2) + "','" + WellName + "','" + Math.Round((decimal)row["Depth"], 2) * -1.0M + "','" + Math.Round((decimal)row["CummDeltaNorthing"], 2) + "','" + Math.Round((decimal)row["CummDeltaEasting"], 2) + "'],");

               }

                dataFOR3D.Append("] },");// End of data load sourse

                // Start of data load 
                dataFOR3D.Append(@"{                    
                                   dimensions: ['Measured Depth', 'Inclination', 'Azimuth', 'TVD', 'Northing', 'Easting', 'Name', 'Depth', 'Northing_UTM' , 'Easting_UTM'],                                                      
                                   source: [  
                                      
                    ");// Start of Well plan data source 

                dataFOR3D.Append(" ['Measured Depth', 'Inclination', 'Azimuth', 'TVD', 'Northing', 'Easting', 'Name',  'Depth', 'Northing_UTM' , 'Easting_UTM'],");


                foreach (DataRow row in ChartDataRAW.Rows)
                {
                    dataFOR3D.Append(" ['" + row["MDepth"] + "','" + Math.Round((decimal)row["Inclination"], 2) + "','" + row["Azimuth"] + "','" + Math.Round((decimal)row["TVD"], 2) + "','" + Math.Round((decimal)row["Northing"], 2) + "','" + Math.Round((decimal)row["Easting"], 2) + "','" + WellName + "','" + Math.Round((decimal)row["Depth"], 2) * -1.0M + "','" + Math.Round((decimal)row["CummDeltaNorthing"], 2) + "','" + Math.Round((decimal)row["CummDeltaEasting"], 2) + "'],");

                }

                dataFOR3D.Append("] },");// End of data load sourse

                // Start of data load 
                dataFOR3D.Append(@"{                    
                                   dimensions: ['Measured Depth', 'Inclination', 'Azimuth', 'TVD', 'Northing', 'Easting', 'Name', 'Depth', 'Northing_UTM' , 'Easting_UTM'],                                                      
                                   source: [  
                                      
                    ");// Start of Well plan data source 

                dataFOR3D.Append(" ['Measured Depth', 'Inclination', 'Azimuth', 'TVD', 'Northing', 'Easting', 'Name',  'Depth', 'Northing_UTM' , 'Easting_UTM'],");


                foreach (DataRow row in ChartDataBHA.Rows)
                {
                    dataFOR3D.Append(" ['" + row["MDepth"] + "','" + Math.Round((decimal)row["Inclination"], 2) + "','" + row["Azimuth"] + "','" + Math.Round((decimal)row["TVD"], 2) + "','" + Math.Round((decimal)row["Northing"], 2) + "','" + Math.Round((decimal)row["Easting"], 2) + "','" + WellName + "','" + Math.Round((decimal)row["Depth"], 2) * -1.0M + "','" + Math.Round((decimal)row["CummDeltaNorthing"], 2) + "','" + Math.Round((decimal)row["CummDeltaEasting"], 2) + "'],");

                }

                dataFOR3D.Append("] },");// End of data load sourse

                // Start of data load 
                dataFOR3D.Append(@"{                    
                                   dimensions: ['Measured Depth', 'Inclination', 'Azimuth', 'TVD', 'Northing', 'Easting', 'Name', 'Depth', 'Northing_UTM' , 'Easting_UTM'],                                                      
                                   source: [  
                                      
                    ");// Start of Well plan data source 

                dataFOR3D.Append(" ['Measured Depth', 'Inclination', 'Azimuth', 'TVD', 'Northing', 'Easting', 'Name',  'Depth', 'Northing_UTM' , 'Easting_UTM'],");


                foreach (DataRow row in ChartDataSC.Rows)
                {
                    dataFOR3D.Append(" ['" + row["MDepth"] + "','" + Math.Round((decimal)row["Inclination"], 2) + "','" + row["Azimuth"] + "','" + Math.Round((decimal)row["TVD"], 2) + "','" + Math.Round((decimal)row["Northing"], 2) + "','" + Math.Round((decimal)row["Easting"], 2) + "','" + WellName + "','" + Math.Round((decimal)row["Depth"], 2) * -1.0M + "','" + Math.Round((decimal)row["CummDeltaNorthing"], 2) + "','" + Math.Round((decimal)row["CummDeltaEasting"], 2) + "'],");

                }

                dataFOR3D.Append("] },");// End of data load sourse



                // Start of data load 
                dataFOR3D.Append(@"{                    
                                   dimensions: ['Measured Depth', 'Inclination', 'Azimuth', 'TVD', 'Northing', 'Easting', 'Name', 'Depth' , 'Northing_UTM' , 'Easting_UTM'],                                                      
                                   source: [  
                                      
                    ");// Start of Well plan data source 

                dataFOR3D.Append(" ['Measured Depth', 'Inclination', 'Azimuth', 'TVD', 'Northing', 'Easting', 'Name',  'Depth' , 'Northing_UTM' , 'Easting_UTM'],");


                            foreach (DataRow row in ChartDataWithPlan.Rows)
                            {
                                dataFOR3D.Append(" ['" + row["planDepth"] + "','" + Math.Round((decimal)row["planInclination"], 2) + "','" + row["planAzimuth"] + "','" + Math.Round((decimal)row["planTVD"], 2) + "','" + Math.Round((decimal)row["planNorthing"], 2) + "','" + Math.Round((decimal)row["planEasting"], 2) + "','" + WellName + "_(Planned)" + "  ','" + Math.Round((decimal)row["planPlotDepth"], 2) * -1.0M + "','" + Math.Round((decimal)row["planCummDeltaNorthing"], 2) + "','" + Math.Round((decimal)row["planCummDeltaEasting"], 2) + "'],");

                            }

                dataFOR3D.Append("] }");// End of data load sourse

                dataFOR3D.Append(" ],");  // END of Complete Data Series Load

                strScript.Append(dataFOR3D);

                strScript.Append(@"                              
                                    series: [
                                    {                        
                                    name: 'Definitive (Placement)',
                                    type: 'scatter3D',
                                    symbolSize: 4.5,
                                    itemStyle: {
                                        borderWidth: 0,
                                        borderColor: 'rgba(255,255,255,0.9)'
                                    },
                                    encode: {
                                        x: 'Easting_UTM',
                                        y: 'Northing_UTM',
                                        z: 'Depth',
                                        tooltip: [0, 1, 2, 3, 4, 5, 8, 9, 6]
                                    },
                                    emphasis: {
                                        label: {
                                            textStyle: {
                                                fontSize: 20,
                                                color: '#900'
                                            }
                                        },
                                        itemStyle: {
                                            color: '#900'
                                        }
                                    }
                                },
                                    {                        
                                    name: 'RAW',
                                    type: 'scatter3D',
					                datasetIndex: 1,
                                    symbolSize: 4.5,
                                    itemStyle: {
                                        borderWidth: 0,
                                        borderColor: 'rgba(255,255,255,0.9)'
                                    },
                                    encode: {
                                        x: 'Easting_UTM',
                                        y: 'Northing_UTM',
                                        z: 'Depth',
                                        tooltip: [0, 1, 2, 3, 4, 5, 8, 9, 6]
                                    },
                                    emphasis: {
                                        label: {
                                            textStyle: {
                                                fontSize: 20,
                                                color: '#900'
                                            }
                                        },
                                        itemStyle: {
                                            color: '#900'
                                        }
                                    }
                                },

                                    {                        
                                    name: 'BHA',
                                    type: 'scatter3D',
					                datasetIndex: 2,
                                    symbolSize: 4.5,
                                    itemStyle: {
                                        borderWidth: 0,
                                        borderColor: 'rgba(255,255,255,0.9)'
                                    },
                                    encode: {
                                        x: 'Easting_UTM',
                                        y: 'Northing_UTM',
                                        z: 'Depth',
                                        tooltip: [0, 1, 2, 3, 4, 5, 8 , 9, 6]
                                    },
                                    emphasis: {
                                        label: {
                                            textStyle: {
                                                fontSize: 20,
                                                color: '#900'
                                            }
                                        },
                                        itemStyle: {
                                            color: '#900'
                                        }
                                    }
                                },
                                    {                        
                                    name: 'Short Collar',
                                    type: 'scatter3D',
					                datasetIndex: 3,
                                    symbolSize: 4.5,
                                    itemStyle: {
                                        borderWidth: 0,
                                        borderColor: 'rgba(255,255,255,0.9)'
                                    },
                                    encode: {
                                        x: 'Easting_UTM',
                                        y: 'Northing_UTM',
                                        z: 'Depth',
                                        tooltip: [0, 1, 2, 3, 4, 5, 8, 9 , 6]
                                    },
                                    emphasis: {
                                        label: {
                                            textStyle: {
                                                fontSize: 20,
                                                color: '#900'
                                            }
                                        },
                                        itemStyle: {
                                            color: '#900'
                                        }
                                    }
                                },

				                {
					                name: 'Plan (Definitive)',
                                    type: 'scatter3D',
					                datasetIndex: 4,
                                    symbolSize: 3,
					                symbol: 'circle',
                                    itemStyle: {
                                        borderWidth: .4,
                                        borderColor: 'rgba(0,0,050,0.4)'
                                    },
 					                encode: {
						                x: 'Easting_UTM',
						                y: 'Northing_UTM',
						                z: 'Depth',
						                tooltip: [ 0, 1 , 2 , 3 , 4, 5 , 8, 9, 6 ]
					                }, 
					                emphasis: {
                                        itemStyle: {
                                            color: '#900'
                                        }
						
                                    }

                                },
                                  {
                                      type: 'scatter',
                                        symbolSize: 3.5,
                                      xAxisIndex: 0,
                                      yAxisIndex: 0,
                                      encode: {
                                          x: 'Easting',
                                          y: 'TVD',
                                          tooltip: [0, 1, 2, 3, 4, 5, 6]
                                      }
                                  },
                                    {
                                        type: 'scatter',
                                            symbolSize: 3.5,
                                        xAxisIndex: 1,
                                        yAxisIndex: 1,
                                        encode: {
                                            x: 'Northing',
                                            y: 'TVD',
                                            tooltip: [0, 1, 2, 3, 4, 5, 6]
                                        }
                                    },
                                    {
                                        type: 'scatter',
                                            symbolSize: 3.5,
                                        xAxisIndex: 2,
                                        yAxisIndex: 2,
                                        encode: {
                                            x: 'Easting',
                                            y: 'Northing',
                                            tooltip: [0, 1, 2, 3, 4, 5, 6]
                                        }
                                    },
                                    {
                                        type: 'scatter',
                                            symbolSize: 3.5,
                                        xAxisIndex: 3,
                                        yAxisIndex: 3,
                                        encode: {
                                            x: 'TVD',
                                            y: 'MDepth',
                                            tooltip: [0, 1, 2, 3, 4, 5, 6]
                                        }
                                    },

                                   ]
                                });
                                window.addEventListener('resize', function () {
                                myChart3D.resize();
                                });
                
               ");

                //strScript.Append("  if (option && typeof option === \"object\" ) {");
                //strScript.Append("     myChart.setOption(option, true); } ");

                strScript.Append("</script>");

                return strScript.ToString();

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }


        }

        public static String getChartsQCResultsECHRT_3DWellPlanOnly(String divID, String WellName, System.Data.DataTable ChartDataWithPlan, System.Data.DataTable  EastingNorthingVals  , decimal maxTVD)
        {
            try
            {
                StringBuilder strScript = new StringBuilder();
                StringBuilder dataFOR3D = new StringBuilder();

                StringBuilder minNorthing = new StringBuilder(); StringBuilder maxNorthing = new StringBuilder(); 
                StringBuilder minEasting = new StringBuilder(); StringBuilder maxEasting = new StringBuilder();


                strScript.Append("<script type='text/javascript' src='../../eCharts/echarts3D.min.js'></script>");
                strScript.Append("<script type='text/javascript' src='../../eCharts/echarts-gl.min.js'></script>");
                strScript.Append("<script type='text/javascript'>");

                strScript.Append(" var dom3D = document.getElementById('" + divID + "'); ");
                strScript.Append(" var myChart3D = echarts.init(dom3D); ");
                strScript.Append(" var MAXTVD =" + ((maxTVD + 500) * -1) + ";");   // MAXTVD is multiplied by Max TVD and extra 500 units are added

                foreach (DataRow row in EastingNorthingVals.Rows)
                {

                    maxNorthing.Append( row["maxNorthing"] );
                    minNorthing.Append( row["minNorthing"] );
                    maxEasting.Append( row["maxEasting"] );
                    minEasting.Append( row["minEasting"] );
                    
                }
                
                strScript.Append("  var maxX =" + maxEasting + ";");
                strScript.Append("  var minX =" + minEasting + ";");
                strScript.Append("  var maxY =" + maxNorthing + ";");
                strScript.Append("  var minY =" + minNorthing + ";");

                strScript.Append(@"

                var sizeValue = '65%';
                var width2D = '20%';
                var colors = ['#5793f3', '#d14a61', '#675bba'];           
                         myChart3D.setOption({
                                    title: {
                                        text: 'SMARTS QC Analysis in 3D (Planned Well data)',
                                        subtext: 'www.smartwelldynamics.com',
                                        x: 'right'
                                    },
                                    toolbox: {
                                        show: true, showTitle: true, itemGap: 20, orient: 'vertical', left: 'right',
                                        top: '80',
                                        feature: {
                                            restore: { show: true, title: 'Restore' },
                                            saveAsImage: { show: true, title: 'Save' }
                                        }
                                    },
                                    visualMap: {
                                    max: MAXTVD,
                                    dimension: 'Depth',
                                    type: 'continuous',
                                    itemHeight: 250,
                                    calculable: true,
                                    text: ['Depth (start)', 'Depth (lowest)'],
                                    top: 100,
                                    left: 5,
                                            
                                    inRange: {
                                        colorLightness: [0.6, 0.5],                           
                                        color: ['#313695', '#4575b4', '#74add1', '#abd9e9', '#e0f3f8', '#ffffbf', '#fee090', '#fdae61', '#f2b77d', '#e8d4c0']
                                    },
                                    controller: {
                                        inRange: {
                                            //color: '#cc1a1a'
                                            color: '#9b420f'
                                        }
                                    },
                                   // outOfRange: {
                                   //     color: '#e50dcf'
                                   // },


                                    },                                   


                                    xAxis3D: [
                                        { type: 'value', name: 'Easting', min: minX , max : maxX },
                                    ],

                                    yAxis3D: [
                                        { type: 'value', name: 'Northing', min: minY , max: maxY},

                                    ],

                                    zAxis3D: [
                                        { type: 'value', name: 'Depth', axisLabel: { formatter: function (value)  {   return value*-1; }  }  }

                                    ],

                                    xAxis: [
                                        { type: 'value', gridIndex: 0, name: 'Easting', axisLabel: { rotate: 50, interval: 0 } },
                                        { type: 'value', gridIndex: 1, name: 'Northing', axisLabel: { rotate: 50, interval: 0 } },
                                        { type: 'value', gridIndex: 2, name: 'Easting', axisLabel: { rotate: 50, interval: 0 } },
                                        { type: 'value', gridIndex: 3, name: 'TVD', axisLabel: { rotate: 50, interval: 0 } }
                                    ],

                                    yAxis: [
                                        { type: 'value', gridIndex: 0, name: 'TVD', },
                                        { type: 'value', gridIndex: 1, name: 'TVD', },
                                        { type: 'value', gridIndex: 2, name: 'Northing', },
                                        { type: 'value', gridIndex: 3, name: 'MDepth', },
                                    ],

                                    grid: [
                                    { left: '73%', width: width2D, bottom: '80%' },
                                    { left: '73%', width: width2D, bottom: '55%', top: '30%' },
                                    { left: '73%', width: width2D, bottom: '27%', top: '55%' },
                                    { left: '73%', width: width2D, top: '80%' },
                                    ],

                                    legend: { },

                                    tooltip: {
                                        backgroundColor: '#0a2131'
                                    },
                                    grid3D: {
                                        boxWidth: 75,
                                        width: '75%',
                                        axisLine: {
                                            lineStyle: {
                                                color: '#04131c'
                                            }
                                        },
                                        containLabel: true,
                                        axisPointer: {
                                            lineStyle: {
                                                color: '#ff67cc'
                                            }
                                        }
                                    },
 
                                    dataset: {
                                    
                                    dimensions: ['Measured Depth', 'Inclination', 'Azimuth', 'TVD', 'Northing', 'Easting', 'Name', 'Depth', 'Northing_UTM', 'Easting_UTM'],                
                                      
                                    source: [
					                    
                ");

                // data set continue ... Loading Data


                dataFOR3D.Append(" ['Measured Depth', 'Inclination', 'Azimuth', 'TVD', 'Northing', 'Easting', 'Name',  'Depth' , 'Northing_UTM', 'Easting_UTM'],");


                foreach (DataRow row in ChartDataWithPlan.Rows)
                {
                    dataFOR3D.Append(" ['" + row["planDepth"] + "','" + Math.Round((decimal)row["planInclination"], 2) + "','" + row["planAzimuth"] + "','" + Math.Round((decimal)row["planTVD"], 2) + "','" + Math.Round((decimal)row["planNorthing"], 2) + "','" + Math.Round((decimal)row["planEasting"], 2) + "','" + WellName + "_(Planned)" + "  ','" + Math.Round((decimal)row["planPlotDepth"], 2) * -1.0M + "','" + Math.Round((decimal)row["planCummDeltaNorthing"], 2) + "','" + Math.Round((decimal)row["planCummDeltaEasting"], 2) + "'],");

                }

                dataFOR3D.Append("] },");// End of data load sourse

                strScript.Append(dataFOR3D);

                strScript.Append(@"                              
                                    series: [
                                    {                        
                                    name: 'Plan (Definitive)',
                                    type: 'scatter3D',
                                    symbolSize: 4.5,
                                    itemStyle: {
                                        borderWidth: 3,
                                        borderColor: 'rgba(255,255,255,0.9'
                                    },
                                    encode: {
                                        x: 'Easting_UTM',
                                        y: 'Northing_UTM',
                                        z: 'Depth',
                                        tooltip: [0, 1, 2, 3, 4, 5, 8 , 9 , 6]
                                    },
                                    emphasis: {
                                        label: {
                                            textStyle: {
                                                fontSize: 20,
                                                color: '#900'
                                            }
                                        },
                                        itemStyle: {
                                            color: '#900'
                                        }
                                    }
                                },
                                  {
                                      type: 'scatter',
                                        symbolSize: 3.5,
                                      xAxisIndex: 0,
                                      yAxisIndex: 0,
                                      encode: {
                                          x: 'Easting',
                                          y: 'TVD',
                                          tooltip: [0, 1, 2, 3, 4, 5, 6]
                                      }
                                  },
                                    {
                                        type: 'scatter',
                                            symbolSize: 3.5,
                                        xAxisIndex: 1,
                                        yAxisIndex: 1,
                                        encode: {
                                            x: 'Northing',
                                            y: 'TVD',
                                            tooltip: [0, 1, 2, 3, 4, 5, 6]
                                        }
                                    },
                                    {
                                        type: 'scatter',
                                            symbolSize: 3.5,
                                        xAxisIndex: 2,
                                        yAxisIndex: 2,
                                        encode: {
                                            x: 'Easting',
                                            y: 'Northing',
                                            tooltip: [0, 1, 2, 3, 4, 5, 6]
                                        }
                                    },
                                    {
                                        type: 'scatter',
                                            symbolSize: 3.5,
                                        xAxisIndex: 3,
                                        yAxisIndex: 3,
                                        encode: {
                                            x: 'TVD',
                                            y: 'MDepth',
                                            tooltip: [0, 1, 2, 3, 4, 5, 6]
                                        }
                                    },

                                   ]
                                });
                                window.addEventListener('resize', function () {
                                myChart3D.resize();
                                });
                
               ");

                //strScript.Append("  if (option && typeof option === \"object\" ) {");
                //strScript.Append("     myChart.setOption(option, true); } ");

                strScript.Append("</script>");

                return strScript.ToString();

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }


        }

        public static string getChartsQCResultsECHRTComparison_3D(String divID, System.Data.DataTable ChartData , System.Data.DataTable  EastingNorthingVals , decimal maxTVD)
        {
            try
            {
                StringBuilder strScript = new StringBuilder();
                StringBuilder dataFOR3D = new StringBuilder();
                StringBuilder minNorthing = new StringBuilder(); StringBuilder maxNorthing = new StringBuilder();
                StringBuilder minEasting = new StringBuilder(); StringBuilder maxEasting = new StringBuilder();

                strScript.Append("<script type='text/javascript' src='../../eCharts/echarts3D.min.js'></script>");
                strScript.Append("<script type='text/javascript' src='../../eCharts/echarts-gl.min.js'></script>");
                strScript.Append("<script type='text/javascript'>");

                strScript.Append(" var dom3D = document.getElementById('" + divID + "'); ");
                strScript.Append(" var myChart3D = echarts.init(dom3D); ");
                strScript.Append(" var MAXTVD =" + ((maxTVD + 500) * -1) + ";");   // MAXTVD is multiplied by Max TVD and extra 500 units are added

                foreach (DataRow row in EastingNorthingVals.Rows)
                {

                    maxNorthing.Append(row["maxNorthing"]);
                    minNorthing.Append(row["minNorthing"]);
                    maxEasting.Append(row["maxEasting"]);
                    minEasting.Append(row["minEasting"]);

                }

                strScript.Append("  var maxX =" + maxEasting + ";");
                strScript.Append("  var minX =" + minEasting + ";");
                strScript.Append("  var maxY =" + maxNorthing + ";");
                strScript.Append("  var minY =" + minNorthing + ";");

                strScript.Append(@"           
                var sizeValue = '65%';
                var width2D = '20%';
                var colors = ['#5793f3', '#d14a61', '#675bba'];           
                         myChart3D.setOption({
                                    title: {
                                        text: 'SMARTS QC Analysis in 3D',
                                        subtext: 'www.smartwelldynamics.com',
                                        x: 'right'
                                    },
                                    toolbox: {
                                        show: true, showTitle: true, itemGap: 20, orient: 'vertical', left: 'right',
                                        top: '80',
                                        feature: {
                                            restore: { show: true, title: 'Restore' },
                                            saveAsImage: { show: true, title: 'Save' }
                                        }
                                    },
					                legend: {
                                        data: ['Placement (Definitive)'],
						                align: 'left',
						                left: 10,
                                        top: 15,
    					                x: 'left',
                        
                                    },
                                    visualMap: {
                                    max: MAXTVD,
                                    dimension: 'Depth',
                                    type: 'continuous',
                                    itemHeight: 250,
                                    calculable: true,
                                    text: ['Depth (start)', 'Depth (lowest)'],
                                    top: 100,
                                    left: 5,
                                            
                                    inRange: {
                                        colorLightness: [0.6, 0.5],                           
                                        color: ['#313695', '#4575b4', '#74add1', '#abd9e9', '#e0f3f8', '#ffffbf', '#fee090', '#fdae61', '#f2b77d', '#e8d4c0']                                                                                
                                    },
                                    controller: {
                                        inRange: {
                                            //color: '#cc1a1a'
                                            color: '#9b420f'
                                        }
                                    },


                                    },                                   


                                    xAxis3D: [
                                        { type: 'value', name: 'Easting', min: minX , max : maxX },
                                    ],

                                    yAxis3D: [
                                        { type: 'value', name: 'Northing', min: minY , max : maxY },

                                    ],

                                    zAxis3D: [
                                        { type: 'value', name: 'Depth', axisLabel: { formatter: function (value)  {   return value*-1; }  }  }

                                    ],

                                    xAxis: [
                                        { type: 'value', gridIndex: 0, name: 'Easting', axisLabel: { rotate: 50, interval: 0 } },
                                        { type: 'value', gridIndex: 1, name: 'Northing', axisLabel: { rotate: 50, interval: 0 } },
                                        { type: 'value', gridIndex: 2, name: 'Easting', axisLabel: { rotate: 50, interval: 0 } },
                                        { type: 'value', gridIndex: 3, name: 'TVD', axisLabel: { rotate: 50, interval: 0 } }
                                    ],

                                    yAxis: [
                                        { type: 'value', gridIndex: 0, name: 'TVD', },
                                        { type: 'value', gridIndex: 1, name: 'TVD', },
                                        { type: 'value', gridIndex: 2, name: 'Northing', },
                                        { type: 'value', gridIndex: 3, name: 'MDepth', },
                                    ],

                                    grid: [
                                    { left: '73%', width: width2D, bottom: '80%' },
                                    { left: '73%', width: width2D, bottom: '55%', top: '30%' },
                                    { left: '73%', width: width2D, bottom: '27%', top: '55%' },
                                    { left: '73%', width: width2D, top: '80%' },
                                    ],

                                    legend: { },

                                    tooltip: {
                                        backgroundColor: '#0a2131'
                                    },
                                    grid3D: {
                                        boxWidth: 75,
                                        width: '75%',
                                        axisLine: {
                                            lineStyle: {
                                                color: '#04131c'
                                            }
                                        },
                                        containLabel: true,
                                        axisPointer: {
                                            lineStyle: {
                                                color: '#ff67cc'
                                            }
                                        }
                                    },
 
                                    dataset: {
                                    
                                    dimensions: ['Measured Depth', 'Inclination', 'Azimuth', 'TVD', 'Northing', 'Easting', 'Name' , 'Depth' , 'Northing_UTM', 'Easting_UTM'],                
                                      
                                    source: [
					                    
                ");

                // data set continue ... Loading Data


                dataFOR3D.Append(" ['Measured Depth', 'Inclination', 'Azimuth', 'TVD', 'Northing', 'Easting', 'Name', 'Depth', 'Northing_UTM', 'Easting_UTM' ],");

                foreach (DataRow row in ChartData.Rows)
                {
                    dataFOR3D.Append(" ['" + row["MDepth"] + "','" + Math.Round((decimal)row["Inclination"], 2) + "','" + row["Azimuth"] + "','" + Math.Round((decimal)row["TVD"], 2) + "','" + Math.Round((decimal)row["Northing"], 2) + "','" + Math.Round((decimal)row["Easting"], 2) + "','" + row["WellName"] + "','" + Math.Round((decimal)row["PlotDepth"], 2) * -1.0M + "','" + Math.Round((decimal)row["CummDeltaNorthing"], 2) + "','" + Math.Round((decimal)row["CummDeltaEasting"], 2) + "'],");

                }

                dataFOR3D.Append("] },");// End of data load sourse

                strScript.Append(dataFOR3D);

                strScript.Append(@"                              
                                    series: [
                                    {                        
                                    name: 'Definitive (Placement)',
                                    type: 'scatter3D',
                                    symbolSize: 4.5,
                                    itemStyle: {
                                        borderWidth: 3,
                                        borderColor: 'rgba(255,255,255,0.9'
                                    },
                                    encode: {
                                        x: 'Easting_UTM',
                                        y: 'Northing_UTM',
                                        z: 'Depth',
                                        tooltip: [0, 1, 2, 3, 4, 5, 8, 9, 6]
                                    },
                                    emphasis: {
                                        label: {
                                            textStyle: {
                                                fontSize: 20,
                                                color: '#900'
                                            }
                                        },
                                        itemStyle: {
                                            color: '#900'
                                        }
                                    }
                                },
                                  {
                                      type: 'scatter',
                                        symbolSize: 3.5,
                                      xAxisIndex: 0,
                                      yAxisIndex: 0,
                                      encode: {
                                          x: 'Easting',
                                          y: 'TVD',
                                          tooltip: [0, 1, 2, 3, 4, 5, 6]
                                      }
                                  },
                                    {
                                        type: 'scatter',
                                            symbolSize: 3.5,
                                        xAxisIndex: 1,
                                        yAxisIndex: 1,
                                        encode: {
                                            x: 'Northing',
                                            y: 'TVD',
                                            tooltip: [0, 1, 2, 3, 4, 5, 6]
                                        }
                                    },
                                    {
                                        type: 'scatter',
                                            symbolSize: 3.5,
                                        xAxisIndex: 2,
                                        yAxisIndex: 2,
                                        encode: {
                                            x: 'Easting',
                                            y: 'Northing',
                                            tooltip: [0, 1, 2, 3, 4, 5, 6]
                                        }
                                    },
                                    {
                                        type: 'scatter',
                                            symbolSize: 3.5,
                                        xAxisIndex: 3,
                                        yAxisIndex: 3,
                                        encode: {
                                            x: 'TVD',
                                            y: 'MDepth',
                                            tooltip: [0, 1, 2, 3, 4, 5, 6]
                                        }
                                    },

                                   ]
                                });
                                window.addEventListener('resize', function () {
                                myChart3D.resize();
                                });
                
               ");

                //strScript.Append("  if (option && typeof option === \"object\" ) {");
                //strScript.Append("     myChart.setOption(option, true); } ");

                strScript.Append("</script>");

                return strScript.ToString();

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }


        }

        public static string getChartsQCResultsECHRTAntiCollisionCent2Cent_3D(String divID, String WellName, System.Data.DataTable ChartDataMain, System.Data.DataTable ChartDataSurrounding, System.Data.DataTable ChartDataMain_Filtered, System.Data.DataTable ChartDataSurroundWells_Filtered, System.Data.DataTable ChartDataWithPlan, System.Data.DataTable EastingNorthingVals, System.Data.DataTable  max_min_subSea)
        {
            try
            {
                StringBuilder strScript = new StringBuilder();
                StringBuilder dataFOR3D = new StringBuilder();

                StringBuilder maxsubsea = new StringBuilder(); StringBuilder minsubsea = new StringBuilder(); 
                StringBuilder minNorthing = new StringBuilder(); StringBuilder maxNorthing = new StringBuilder();
                StringBuilder minEasting = new StringBuilder(); StringBuilder maxEasting = new StringBuilder();

                strScript.Append("<script type='text/javascript' src='../../eCharts/echarts3D.min.js'></script>");
                strScript.Append("<script type='text/javascript' src='../../eCharts/echarts-gl.min.js'></script>");
                strScript.Append("<script type='text/javascript'>");

                strScript.Append(" var dom3D = document.getElementById('" + divID + "'); ");
                strScript.Append(" var myChart3D = echarts.init(dom3D); ");
                

                foreach (DataRow ss in max_min_subSea.Rows)
                {
                    maxsubsea.Append(ss["maxSubSea"].ToString());
                    minsubsea.Append(ss["minSubSea"].ToString());

                }

                decimal maxsub = decimal.Parse(maxsubsea.ToString());
                decimal minsub = decimal.Parse(minsubsea.ToString());

                if (minsub < maxsub)
                {
                    strScript.Append(" var MAXSUBSEA =" + (minsub) + ";");
                    strScript.Append(" var MINSUBSEA =" + (maxsub) + ";");
                }
                else {
                    strScript.Append(" var MAXSUBSEA =" + (maxsub) + ";");
                    strScript.Append(" var MINSUBSEA =" + (minsub) + ";");                
                }

  


                foreach (DataRow row in EastingNorthingVals.Rows)
                {

                    maxNorthing.Append(row["maxNorthing"]);
                    minNorthing.Append(row["minNorthing"]);
                    maxEasting.Append(row["maxEasting"]);
                    minEasting.Append(row["minEasting"]);

                }

                strScript.Append("  var maxX =" + maxEasting + ";");
                strScript.Append("  var minX =" + minEasting + ";");
                strScript.Append("  var maxY =" + maxNorthing + ";");
                strScript.Append("  var minY =" + minNorthing + ";");

                strScript.Append(@"

                var sizeValue = '75%';
                var width2D = '20%';
                var colors = ['#5793f3', '#d14a61', '#675bba'];           
                         myChart3D.setOption({
                                    title: {
                                        text: 'SMARTS Proximity Analysis in 3D',
                                        subtext: 'www.smartwelldynamics.com',
                                        x: 'right',
                                        top: '5',
                                    },
                                    toolbox: {
                                        show: true, showTitle: true, itemGap: 20, orient: 'vertical', left: 'right',
                                        top: '60',
                                        feature: {
                                            restore: { show: true, title: 'Restore' },
                                            saveAsImage: { show: true, title: 'Save' }
                                        }
                                    },
					                legend: {
                                        data: ['Main Lateral Placement (Definitive)','Surrounding Laterals','Main Lateral (Surveys Influenced by Threshold)','Surrounding Lateral (Meeting Analysis Threshold)','Plan (Definitive)'],
                                        orient: 'vertical',
                                        align: 'auto',
						                right: 'left',
                                        top: '5',
    					                x: 'left',
                        
                                    },
                                    visualMap: {
                                    min: MINSUBSEA,
                                    max: MAXSUBSEA,                                        
                                    dimension: 'SubSeaDepth',
                                    type: 'continuous',
                                    itemHeight: 300,
                                    calculable: true,
                                    text: ['SubSea Depth '],
                                    top: 160,
                                    left: 5,
                                            
                                    inRange: {
                                        colorLightness: [0.6, 0.5],                           
                                        color: ['#313695', '#4575b4', '#74add1', '#abd9e9', '#e0f3f8', '#ffffbf', '#fee090', '#fdae61', '#f2b77d', '#e8d4c0']
                                    },
                                    controller: {
                                        inRange: {
                                            //color: '#cc1a1a'
                                            color: '#9b420f'
                                        }
                                    },

                                    },                                   


                                    xAxis3D: [
                                        { type: 'value', name: 'Easting', min: minX , max : maxX  },
                                    ],

                                    yAxis3D: [
                                        { type: 'value', name: 'Northing', min: minY , max : maxY },

                                    ],

                                    zAxis3D: [
                                        { type: 'value', inverse: false, name: 'SubSea Depth', axisLabel: { formatter: function (value)  {   return value * -1; }  }  }

                                    ],


                                    tooltip: {
                                        backgroundColor: '#0a2131'
                                    },
                                    grid3D: {
                                        boxWidth: 95,
                                        width: '100%',        
                                        axisLine: {
                                            lineStyle: {
                                                color: '#04131c'
                                            }
                                        },
                                        containLabel: true,
                                        axisPointer: {
                                            lineStyle: {
                                                color: '#ff67cc'
                                            }
                                        }
                                    },
 
                                    dataset:[
                                    {
                                    
                                    dimensions: ['Measured Depth', 'TVD', 'Northing', 'Easting', 'Name', 'SubSeaDepth' , 'SubDepth', 'Northing_UTM' , 'Easting_UTM'],                
                                      
                                    source: [
					                    
                ");

                // data set continue ... Loading Data


                dataFOR3D.Append(" ['Measured Depth', 'TVD', 'Northing', 'Easting', 'Name',  'SubSeaDepth' , 'SubDepth' , 'Northing_UTM' , 'Easting_UTM'],");

                foreach (DataRow row in ChartDataMain.Rows)
                {
                    dataFOR3D.Append(" ['" + row["MDepth"] + "','" + Math.Round((decimal)row["TVD"], 2) + "','" + Math.Round((decimal)row["Northing"], 2) + "','" + Math.Round((decimal)row["Easting"], 2) + "','" + row["WellName"] + "','" + +Math.Round((decimal)row["SubDepth"], 2) + "','" + Math.Round((decimal)row["SubDepth"], 2) * -1.0M + "','" + Math.Round((decimal)row["CummDeltaNorthing"], 2) + "','" + Math.Round((decimal)row["CummDeltaEasting"], 2) + "'],");

                }

                dataFOR3D.Append("] },");// End of data load sourse

                // Start of data load 
                dataFOR3D.Append(@"{                    
                                   dimensions: ['Measured Depth', 'TVD', 'Northing', 'Easting', 'Name', 'SubSeaDepth', 'SubDepth', 'Northing_UTM' , 'Easting_UTM'],                                                      
                                   source: [  
                                      
                    ");// Start of Well plan data source 

                dataFOR3D.Append(" ['Measured Depth', 'TVD', 'Northing', 'Easting', 'Name',  'SubSeaDepth', 'SubDepth', 'Northing_UTM' , 'Easting_UTM'],");


                foreach (DataRow row in ChartDataSurrounding.Rows)
                {
                    dataFOR3D.Append(" ['" + row["MDepth"] + "','" + Math.Round((decimal)row["TVD"], 2) + "','" + Math.Round((decimal)row["Northing"], 2) + "','" + Math.Round((decimal)row["Easting"], 2) + "','" + row["WellName"] + "','" + +Math.Round((decimal)row["SubDepth"], 2) + "','" + Math.Round((decimal)row["SubDepth"], 2) * -1.0M + "','" + Math.Round((decimal)row["CummDeltaNorthing"], 2) + "','" + Math.Round((decimal)row["CummDeltaEasting"], 2) + "'],");

                }

                dataFOR3D.Append("] },");// End of data load sourse

                // Start of data load 
                dataFOR3D.Append(@"{                    
                                   dimensions: ['Measured Depth', 'TVD', 'Northing', 'Easting', 'Name', 'SubSeaDepth', 'SubDepth', 'Northing_UTM' , 'Easting_UTM' , 'Comments'],                                                      
                                   source: [  
                                      
                    ");// Start of Well plan data source 

                dataFOR3D.Append(" ['Measured Depth', 'TVD', 'Northing', 'Easting', 'Name',  'SubSeaDepth', 'SubDepth', 'Northing_UTM' , 'Easting_UTM' , 'Comments'],");


                foreach (DataRow row in ChartDataMain_Filtered.Rows)
                {
                    dataFOR3D.Append(" ['" + row["MDepth"] + "','" + Math.Round((decimal)row["TVD"], 2) + "','" + Math.Round((decimal)row["Northing"], 2) + "','" + Math.Round((decimal)row["Easting"], 2) + "','" + row["WellName"] + "','" + +Math.Round((decimal)row["SubDepth"], 2) + "','" + Math.Round((decimal)row["SubDepth"], 2) * -1.0M + "','" + Math.Round((decimal)row["CummDeltaNorthing"], 2) + "','" + Math.Round((decimal)row["CummDeltaEasting"], 2) + "','" + row["Distance_Comment"] +  "'],");

                }

                dataFOR3D.Append("] },");// End of data load sourse

                // Start of data load 
                dataFOR3D.Append(@"{                    
                                   dimensions: ['Measured Depth', 'TVD', 'Northing', 'Easting', 'Name', 'SubSeaDepth', 'SubDepth', 'Northing_UTM' , 'Easting_UTM' , 'Comments'],                                                      
                                   source: [  
                                      
                    ");// Start of Well plan data source 

                dataFOR3D.Append(" ['Measured Depth', 'TVD', 'Northing', 'Easting', 'Name',  'SubSeaDepth', 'SubDepth', 'Northing_UTM' , 'Easting_UTM' , 'Comments'],");


                foreach (DataRow row in ChartDataSurroundWells_Filtered.Rows)
                {
                    dataFOR3D.Append(" ['" + row["MDepth"] + "','" + Math.Round((decimal)row["TVD"], 2) + "','" + Math.Round((decimal)row["Northing"], 2) + "','" + Math.Round((decimal)row["Easting"], 2) + "','" + row["WellName"] + "','" + +Math.Round((decimal)row["SubDepth"], 2) + "','" + Math.Round((decimal)row["SubDepth"], 2) * -1.0M + "','" + Math.Round((decimal)row["CummDeltaNorthing"], 2) + "','" + Math.Round((decimal)row["CummDeltaEasting"], 2) + "','" + row["Distance_Comment"] + "'],");

                }

                dataFOR3D.Append("] },");// End of data load sourse



                // Start of data load 
                dataFOR3D.Append(@"{                    
                                   dimensions: ['Measured Depth', 'TVD', 'Northing', 'Easting', 'Name', 'SubSeaDepth' , 'SubDepth', 'Northing_UTM' , 'Easting_UTM'],                                                      
                                   source: [  
                                      
                    ");// Start of Well plan data source 

                dataFOR3D.Append(" ['Measured Depth', 'TVD', 'Northing', 'Easting', 'Name',  'SubSeaDepth' , 'SubDepth', 'Northing_UTM' , 'Easting_UTM'],");


                foreach (DataRow row in ChartDataWithPlan.Rows)
                {
                    dataFOR3D.Append(" ['" + row["planDepth"] + "','" + Math.Round((decimal)row["planTVD"], 2) + "','" + Math.Round((decimal)row["planNorthing"], 2) + "','" + Math.Round((decimal)row["planEasting"], 2) + "','" + WellName + "_(Planned)" + "','" + +Math.Round((decimal)row["planSubSeaDepth"], 2) + "','" + Math.Round((decimal)row["planSubSeaDepth"], 2) * -1.0M + "','" + Math.Round((decimal)row["planCummDeltaNorthing"], 2) + "','" + Math.Round((decimal)row["planCummDeltaEasting"], 2) + "'],");

                }

                dataFOR3D.Append("] }");// End of data load sourse

                dataFOR3D.Append(" ],");  // END of Complete Data Series Load

                strScript.Append(dataFOR3D);

                strScript.Append(@"                              
                                    series: [
                                    {                        
                                    name: 'Main Lateral Placement (Definitive)',
                                    type: 'scatter3D',
                                    symbolSize: 2,
                                    itemStyle: {
                                        borderWidth: .3,
                                        borderColor: 'rgba(140,140,140,0.5)'
                                    },
                                    encode: {
                                        x: 'Easting_UTM',
                                        y: 'Northing_UTM',
                                        z: 'SubDepth',
                                        tooltip: [0, 1, 2, 3, 4, 5, 7 , 8]
                                    },
                                    emphasis: {
                                        label: {
                                            textStyle: {
                                                fontSize: 20,
                                                color: '#900'
                                            }
                                        },
                                        itemStyle: {
                                            color: '#900'
                                        }
                                    }
                                },
                                    {                        
                                    name: 'Surrounding Laterals',
                                    type: 'scatter3D',
					                datasetIndex: 1,
                                    symbolSize: 2,
                                    symbol: 'circle',
                                    itemStyle: {
                                        borderWidth: .3,
                                        borderColor: 'rgba(140,140,240,0.8)'
                                    },
                                    encode: {
                                        x: 'Easting_UTM',
                                        y: 'Northing_UTM',
                                        z: 'SubDepth',
                                        tooltip: [0, 1, 2, 3, 4, 5, 7, 8 ]
                                    },
                                    emphasis: {
                                        label: {
                                            textStyle: {
                                                fontSize: 20,
                                                color: '#900'
                                            }
                                        },
                                        itemStyle: {
                                            color: '#900'
                                        }
                                    }
                                },

                                    {                        
                                    name: 'Main Lateral (Surveys Influenced by Threshold)',
                                    type: 'scatter3D',
					                datasetIndex: 2,
                                    symbolSize: 4.5,
                                    itemStyle: {
                                        borderWidth: 0,
                                        borderColor: 'rgba(255,255,255,0.9)'
                                    },
                                    encode: {
                                        x: 'Easting_UTM',
                                        y: 'Northing_UTM',
                                        z: 'SubDepth',
                                        tooltip: [0, 1, 2, 3, 4, 5, 7, 8]
                                    },
                                    emphasis: {
                                        label: {
                                            textStyle: {
                                                fontSize: 20,
                                                color: '#900'
                                            }
                                        },
                                        itemStyle: {
                                            color: '#900'
                                        }
                                    }
                                },
                                    {                        
                                    name: 'Surrounding Lateral (Meeting Analysis Threshold)',
                                    type: 'scatter3D',
					                datasetIndex: 3,
                                    symbolSize: 4.5,
                                    symbol: 'triangle',
                                    itemStyle: {
                                        borderWidth: 0,
                                        borderColor: 'rgba(255,255,255,0.9)'
                                    },
                                    encode: {
                                        x: 'Easting_UTM',
                                        y: 'Northing_UTM',
                                        z: 'SubDepth',
                                        tooltip: [0, 1, 2, 3, 4, 5, 7, 8 , 9 ]
                                    },
                                    emphasis: {
                                        label: {
                                            textStyle: {
                                                fontSize: 20,
                                                color: '#900'
                                            }
                                        },
                                        itemStyle: {
                                            color: '#900'
                                        }
                                    }
                                },

				                {
					                name: 'Plan (Definitive)',
                                    type: 'scatter3D',
					                datasetIndex: 4,
                                    symbolSize: 2.5,
					                symbol: 'circle',
                                    itemStyle: {
                                        borderWidth: .1,
                                        borderColor: 'rgba(0,0,050,0.4)'
                                    },
 					                encode: {
						                x: 'Easting_UTM',
						                y: 'Northing_UTM',
						                z: 'SubDepth',
						                tooltip: [ 0, 1, 2, 3, 4, 5, 7, 8  ]
					                }, 
					                emphasis: {
                                        itemStyle: {
                                            color: '#900'
                                        }
						
                                    }

                                },

                                   ]
                                });
                                window.addEventListener('resize', function () {
                                myChart3D.resize();
                                });
                
               ");

                //strScript.Append("  if (option && typeof option === \"object\" ) {");
                //strScript.Append("     myChart.setOption(option, true); } ");

                strScript.Append("</script>");

                return strScript.ToString();

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }


        }                  

        private static long DirectorySize(DirectoryInfo dInfo, bool includeSubDir)
        {
            try
            {
                long totalSize = dInfo.EnumerateFiles().Sum(file => file.Length);
                if (includeSubDir)
                {
                    totalSize += dInfo.EnumerateDirectories().Sum(dir => DirectorySize(dir, true));
                }
                return totalSize;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }


   

        public static String getClientRunInfo(Int32 WellID, String ClientDBString)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [runName], [dmgstID] FROM [RUN] WHERE [welID] = @welID";
                Int32 id = 0, stID = 0;
                String rnName = String.Empty, stName = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        rnName = AST.getRunName(id);
                                        stID = readData.GetInt32(1);
                                        stName = DMG.getStatusValue(stID);
                                        iReply = String.Format("{0} ( {1} )", rnName, stName);
                                    }
                                }
                                else
                                {
                                    iReply = "---";
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getIFRJobCount(String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(jobID) FROM [Job] WHERE [ifrvalID] = '2'";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }              
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getAssetJobCountPerService(String ClientDBConnection)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                Dictionary<Int32, String> servicesList = new Dictionary<Int32, String>();
                Int32 count = -99;
                servicesList = SRV.getServiceList(serviceGroupQC);
                foreach (KeyValuePair<Int32, String> data in servicesList)
                {
                    Int32 srvID = data.Key;
                    String srvName = data.Value;
                    String selData = "SELECT COUNT(jobID) FROM [Job] WHERE [srvID] = @srvID";
                    using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                    {
                        try
                        {
                            dataCon.Open();
                            SqlCommand getData = new SqlCommand(selData, dataCon);
                            getData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = srvID.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            count = readData.GetInt32(0);
                                            iReply.Add(count);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            dataCon.Close();
                            dataCon.Dispose();
                        }
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getAssetJobCount(String ClientDBConnection)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                Int32 acCount = 0, ifCount = 0, tlCount = 0, gridCount = 0, trueCount = 0;
                String selData = "SELECT (SELECT COUNT(jobID) FROM [Job] WHERE [ifrvalID] = '2') AS countIFR, (SELECT COUNT(jobID) FROM [Job]) AS countJob, (SELECT COUNT(jobID) FROM [Job] WHERE [dmgstID] = '1') AS acCount, (SELECT COUNT(jobID) FROM [JobDetail] WHERE [jcID] = '1') AS trueCount, (SELECT COUNT(jobID) FROM [JobDetail] WHERE [jcID] = '2') AS gridCount";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        ifCount = readData.GetInt32(0);
                                        iReply.Add(ifCount);
                                        tlCount = readData.GetInt32(1);
                                        iReply.Add(tlCount);
                                        acCount = readData.GetInt32(2);                                                                                
                                        iReply.Add(acCount);
                                        gridCount = readData.GetInt32(3);
                                        iReply.Add(gridCount);
                                        trueCount = readData.GetInt32(4);
                                        iReply.Add(trueCount);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getWPAssociatedWellCount(String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                List<KeyValuePair<Int32, Int32>> wlCount = WP.getWellProfileWellList(ClientDBConnection);
                iReply = wlCount.Count;
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getWPDetailCount(String ClientDBConnection)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                Int32 jbCount = 0, dsCount = 0, wlCount = 0;                
                String selData = "SELECT (SELECT COUNT(jobID) FROM [Job] WHERE [jclID] = 3) AS jobCount, (SELECT COUNT(wpdsID) FROM [WPDataSetName]) AS dsCount, (SELECT COUNT(welID) FROM [Well] AS wlCount)";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        jbCount = readData.GetInt32(0);
                                        dsCount = readData.GetInt32(1);
                                        wlCount = readData.GetInt32(2);
                                        iReply.Add(jbCount);
                                        iReply.Add(dsCount);
                                        iReply.Add(wlCount);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getAssetWellCount(String ClientDBConnection)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                Int32 aCount = 0, oCount = 0, tCount = 0, ascCount = 0, mCount = 0, pCount = 0, pdCount = 0; //a: Active Wells, o: Operators, asc: Associated, m: Missing data, p: Missing Plan
                pdCount = AST.getWellPadCount(ClientDBConnection);
                iReply.Add(pdCount);
                tCount = AST.getWellCount(ClientDBConnection);
                iReply.Add(tCount);
                oCount = CLNT.getOperatorCoCount();
                iReply.Add(oCount);
                aCount = AST.getClientActiveWellCount(ClientDBConnection);
                iReply.Add(aCount);
                //ascCount = AST.getClientAssociatedWellCount(ClientDBConnection);
                //iReply.Add(ascCount);
                mCount = AST.getClientMissingDataWellCount(ClientDBConnection);
                iReply.Add(mCount);
                pCount = AST.getClientMissingWellPlanCount(ClientDBConnection);
                iReply.Add(pCount);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getClientDataReviewSummary(Int32 ClientID, String ClientDBConnection)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                Int32 jobCount = 0, wellCount = 0, runCount = 0;
                String selData = "SELECT ( SELECT COUNT(wsrID) FROM [WellServiceRegistration] WHERE [isApproved] = '2' AND [clntID] = @clntID) AS countReg, (SELECT COUNT(welID) FROM [Well]) AS wellCount, (SELECT COUNT(runID) FROM [Run]) AS runCount";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        jobCount = readData.GetInt32(0);
                                        wellCount = readData.GetInt32(1);
                                        runCount = readData.GetInt32(2);
                                    }
                                }
                                else
                                {
                                    jobCount = 0;
                                    wellCount = 0;
                                    runCount = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                    }
                }
                ArrayList pInfo = new ArrayList();
                pInfo = getTotalQCProcessed(ClientDBConnection);
                Int32 processedQCCount1 = Convert.ToInt32(pInfo[0]);
                Int32 processedQCCount2 = Convert.ToInt32(pInfo[1]);
                DateTime date24 = Convert.ToDateTime(pInfo[2]);
                DateTime date14 = Convert.ToDateTime(pInfo[3]);
                iReply.Add(jobCount);
                iReply.Add(wellCount);
                iReply.Add(runCount);
                iReply.Add(processedQCCount1);
                iReply.Add(processedQCCount2);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }            
        }

        public static List<Int32> getClientRawQCSummary(String ClientDBConnection)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                String selData = "SELECT (SELECT count(wsrID) FROM [WellServiceRegistration] WHERE [srvID] = '46' OR [srvID] = '34' OR [srvID] = '37' OR [srvID] = '38' OR [srvID] = '39' OR [srvID] = '40') AS regCount, (SELECT count(welID) FROM [WellServiceRegistration] WHERE [isApproved] = '2') as WellCount, (SELECT count(rfmgID) FROM [RefMags] WHERE [welID] IN (SELECT [welID] FROM [WellServiceRegistration])) as RefMagsCount, (SELECT COUNT(runID) FROM [Run] WHERE [dmgstID] = '1' AND [wswID] IN (SELECT [wsID] FROM [WellSection] WHERE [welID] IN (SELECT [welID] FROM [WellServiceRegistration]))) As RunCount, (SELECT COUNT(bhaID) FROM [BHA] WHERE [runID] IN (SELECT [runID] FROM [Run] WHERE [wswID] IN (SELECT [wsID] FROM [WellSectionToWell] WHERE [welID] IN (SELECT [welID] FROM [WellServiceRegistration])))) As BHACount";
                Int32 regCount = -99, inProcess = -99, rmCount = -99, rnCount = -99, bhaCount = -99;
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        regCount = readData.GetInt32(0);
                                        iReply.Add(regCount);
                                        inProcess = readData.GetInt32(1);
                                        iReply.Add(inProcess);
                                        rmCount = readData.GetInt32(2);
                                        iReply.Add(rmCount);
                                        rnCount = readData.GetInt32(3);
                                        iReply.Add(rnCount);
                                        bhaCount = readData.GetInt32(4);
                                        iReply.Add(bhaCount);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getClientCorrQCSummary(String ClientDBConnection)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                String selData = "SELECT (SELECT count(wsrID) FROM [WellServiceRegistration] WHERE [srvID] = '46') AS regCount, (SELECT count(welID) FROM [WellServiceRegistration] WHERE [srvID] = '46' AND [isApproved] = '2') as WellCount, (SELECT count(rfmgID) FROM [RefMags] WHERE [welID] IN (SELECT [welID] FROM [WellServiceRegistration] WHERE [srvID] = '46')) as RefMagsCount, (SELECT COUNT(runID) FROM [Run] WHERE [dmgstID] = '1' AND [wswID] IN (SELECT [wsID] FROM [WellSection] WHERE [welID] IN (SELECT [welID] FROM [WellServiceRegistration] WHERE [srvID] = '46'))) As RunCount, (SELECT COUNT(bhaID) FROM [BHA] WHERE [runID] in (SELECT [runID] FROM [Run] WHERE [wswID] IN (SELECT [wswID] FROM [WellSectionToWell] WHERE [welID] IN (SELECT [welID] FROM [WellServiceRegistration] WHERE [srvID] = '46'))) AND [dmgstID] = '1') As BHACount";
                Int32 regCount = -99, inProcess = -99, rmCount = -99, rnCount = -99, bhaCount = -99;
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        regCount = readData.GetInt32(0);
                                        iReply.Add(regCount);
                                        inProcess = readData.GetInt32(1);
                                        iReply.Add(inProcess);
                                        rmCount = readData.GetInt32(2);
                                        iReply.Add(rmCount);
                                        rnCount = readData.GetInt32(3);
                                        iReply.Add(rnCount);
                                        bhaCount = readData.GetInt32(4);
                                        iReply.Add(bhaCount);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //public static List<Int32> getClientCorrQCSummary(String ClientDBConnection)
        //{
        //    try
        //    {
        //        List<Int32> iReply = new List<Int32>();
        //        List<Int32> jbC = new List<Int32>();
        //        List<Int32> wlC = new List<Int32>();
        //        List<Int32> rnC = new List<Int32>();
        //        Int32 jobCount = -99, wellCount = -99, rmCount = 0, runCount = -99, bhaCount = 0, jItem = -99, wItem = -99, rItem = -99;

        //        jbC = getCorrJobCount(ClientDBConnection);
        //        if (jbC.Count > 0)
        //        {
        //            jItem = Convert.ToInt32(jbC[0]);
        //            if (!jItem.Equals(0))
        //            {
        //                jobCount = jbC.Count;
        //                wlC = getQCWellCount(jbC, ClientDBConnection);
        //                if (wlC.Count > 0)
        //                {
        //                    wItem = Convert.ToInt32(wlC[0]);
        //                    if (!wItem.Equals(0))
        //                    {
        //                        wellCount = wlC.Count;
        //                        rmCount = getQCRMCount(wlC, ClientDBConnection);

        //                        rnC = getQCRunCount(wlC, ClientDBConnection);
        //                        rItem = Convert.ToInt32(rnC[0]);
        //                        if (!rItem.Equals(0))
        //                        {
        //                            runCount = rnC.Count;
        //                            bhaCount = getQCBHACount(rnC, ClientDBConnection);
        //                        }
        //                        else
        //                        {
        //                            runCount = 0;
        //                            bhaCount = 0;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        wellCount = 0;
        //                        rmCount = 0;
        //                        runCount = 0;
        //                        bhaCount = 0;
        //                    }
        //                }
        //                else
        //                {
        //                    wellCount = 0;
        //                    runCount = 0;
        //                    rmCount = 0;
        //                    bhaCount = 0;
        //                }
        //            }
        //            else
        //            {
        //                jobCount = 0;
        //                wellCount = 0;
        //                runCount = 0;
        //                rmCount = 0;
        //                bhaCount = 0;
        //            }
        //        }
        //        else
        //        {
        //            jobCount = 0;
        //            wellCount = 0;
        //            runCount = 0;
        //            rmCount = 0;
        //            bhaCount = 0;
        //        }
        //        iReply.Add(jobCount);
        //        iReply.Add(wellCount);
        //        iReply.Add(rmCount);
        //        iReply.Add(runCount);
        //        iReply.Add(bhaCount);
        //        return iReply;
        //    }
        //    catch (Exception ex)
        //    { throw new System.Exception(ex.ToString()); }
        //}

        private static Int32 getQCRMCount(List<Int32> WellList, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = 0;
                Int32 rmC = -99;
                String selData = "SELECT [rfmgID] FROM [RefMags] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        foreach (Int32 id in WellList)
                        {
                            SqlCommand getData = new SqlCommand(selData, dataCon);
                            getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = id.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            rmC = readData.GetInt32(0);
                                            if (rmC < 0)
                                            {
                                                iReply++;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        iReply++;
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 getQCBHACount(List<Int32> RunList, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = 0, rMC = -99;
                String selData = "SELECT [bhaID] FROM [BHA] WHERE [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        foreach (Int32 id in RunList)
                        {
                            SqlCommand getData = new SqlCommand(selData, dataCon);
                            getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = id.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            rMC = readData.GetInt32(0);
                                            if (rMC < 0)
                                            {
                                                iReply++;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        iReply++;
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static List<Int32> getQCRunCount(List<Int32> WellList, String ClientDBConnection)
        {
            try
            {
                try
                {
                    List<Int32> iReply = new List<Int32>();                    
                    String selData = "SELECT COUNT(runID) FROM [Run] WHERE [dmgstID] = '1'";
                    using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                    {
                        try
                        {
                            dataCon.Open();
                            foreach (Int32 id in WellList)
                            {
                                SqlCommand getData = new SqlCommand(selData, dataCon);
                                getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = id.ToString();
                                using (SqlDataReader readData = getData.ExecuteReader())
                                {
                                    try
                                    {
                                        if (readData.HasRows)
                                        {
                                            while (readData.Read())
                                            {
                                                iReply.Add(readData.GetInt32(0));
                                            }
                                        }
                                        else
                                        {
                                            iReply.Add(0);
                                        }
                                    }
                                    catch (Exception ex)
                                    { throw new System.Exception(ex.ToString()); }
                                    finally
                                    {
                                        readData.Close();
                                        readData.Dispose();
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            dataCon.Close();
                            dataCon.Dispose();
                        }
                    }    
                    return iReply;
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static List<Int32> getQCWellCount(List<Int32> JobList, String ClientDBConnection)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                Int32 wID = -99;
                String selData = "SELECT [welID] FROM [WellToJob] WHERE [jobID] = @jobID";
                using(SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        foreach(Int32 id in JobList)
                        {
                            SqlCommand getData = new SqlCommand(selData, dataCon);
                            getData.Parameters.AddWithValue("@jobID", SqlDbType.Int).Value = id.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                             wID = readData.GetInt32(0);
                                             if (!wID.Equals(0))
                                             {
                                                 iReply.Add(wID);
                                             }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                    }
                    catch(Exception ex)
                    {throw new System.Exception(ex.ToString());}
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }                
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static List<Int32> getQCSrvRegistrationCount(String ClientDBConnection)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                String selData = "SELECT COUNT(wsrID) FROM [WellServiceRegistration] WHERE [srvID] = '33' OR [srvID] = '34' OR [srvID] = '37' OR [srvID] = '38' OR [srvID] = '39' OR [srvID] = '40'";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply.Add(readData.GetInt32(0));
                                    }
                                }
                                else
                                {
                                    iReply.Add(0);
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static List<Int32> getCorrJobCount(String ClientDBConnection)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                Int32 JobClassID = 2;
                String selData = "SELECT [jobID] FROM [Job] WHERE [jclID] = @jclID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@jclID", SqlDbType.Int).Value = JobClassID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply.Add(readData.GetInt32(0));
                                    }
                                }
                                else
                                {
                                    iReply.Add(0);
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getClientWellBHA(String ClientDBConnection)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                Int32 jobcount = -99, wellcount = -99, runcount = -99;
                String selData = "SELECT  ( SELECT COUNT(jobID) FROM [Job] ) AS countJob, ( SELECT COUNT(welID) FROM [Well] ) AS countWell, ( SELECT COUNT(runID) from [Run] WHERE dmgstID = '1' ) AS countRun";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        jobcount = readData.GetInt32(0);
                                        wellcount = readData.GetInt32(1);
                                        runcount = readData.GetInt32(2);
                                        iReply.Add(jobcount);
                                        iReply.Add(wellcount);
                                        iReply.Add(runcount);
                                    }
                                }
                                else
                                {
                                    wellcount = readData.GetInt32(0);
                                    runcount = readData.GetInt32(1);
                                    iReply.Add(wellcount);
                                    iReply.Add(runcount);
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getClientAssets(Int32 ClientID, String ClientDBConnection)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                Int32 regcount = -99, wellpadcount = -99, wellcount = -99, runcount = -99;
                String selData = "SELECT  ( SELECT COUNT(wsrID) FROM [WellServiceRegistration] WHERE [isApproved] = '2' AND [clntID] = @clntID) AS countReg, ( SELECT COUNT(wpdID) FROM [WellPad] ) AS countWellPad, ( SELECT COUNT(welID) FROM [Well] ) AS countWell, ( SELECT COUNT(runID) from [Run] WHERE dmgstID = '1' ) AS countRun";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        regcount = readData.GetInt32(0);
                                        wellpadcount = readData.GetInt32(1);
                                        wellcount = readData.GetInt32(2);
                                        runcount = readData.GetInt32(3);
                                        iReply.Add(regcount);
                                        iReply.Add(wellpadcount);
                                        iReply.Add(wellcount);
                                        iReply.Add(runcount);
                                    }
                                }
                                else
                                {
                                    wellcount = readData.GetInt32(0);
                                    runcount = readData.GetInt32(1);
                                    iReply.Add(wellcount);
                                    iReply.Add(runcount);
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getClientWellList(String ClientDBConnection)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [welID], [welName] FROM [WELL] ORDER BY [welName]";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                                else
                                {
                                    id = 1;
                                    name = "No Well(s) found";
                                    iReply.Add(id, name);
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<String> getClientWellInfo(Int32 WellID, String ClientDBString)
        {
            try
            {
                List<String> iReply = new List<String>();
                String selData = "SELECT [optrID], [welLong], [welLat] FROM [WELL] WHERE [welID] = @welID";
                Int32 id = 0;
                String opName = String.Empty, longi = String.Empty, lati = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        opName = CLNT.getClientName(id);
                                        iReply.Add(opName);
                                        longi = Convert.ToString(readData.GetDecimal(1));
                                        iReply.Add(longi);
                                        lati = Convert.ToString(readData.GetDecimal(2));
                                        iReply.Add(lati);
                                    }
                                }
                                else
                                {
                                    iReply.Add("---");
                                    iReply.Add("---");
                                    iReply.Add("---");
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //public static String getDatabaseSize()
        //{
        //    try
        //    {
        //        var list = new List<KeyValuePair<String, Double>>();
        //        Double clntSize = 0.00, dmgSize = 0.00, accSize = 0.00, usrSize = 0.00, logSize = 0.00, spSize = 0.00, ssSize = 0.00;
        //        Dictionary<Int32, String> clntDatabases = CLNT.getClientDBNamesList();
        //        foreach (KeyValuePair<Int32, String> pair in clntDatabases)
        //        {
        //            clntSize += DatabaseSize(pair.Value);
        //        }
        //        list.Add(new KeyValuePair<String, Double>("Clients", clntSize));
        //        dmgSize = DatabaseSize("FEDemogDB");
        //        list.Add(new KeyValuePair<String, Double>("Demographics", dmgSize));
        //        accSize = DatabaseSize("FEAccountingDB");
        //        list.Add(new KeyValuePair<String, Double>("Accounting", accSize));
        //        usrSize = DatabaseSize("FEUsersDB");
        //        list.Add(new KeyValuePair<String, Double>("Users", usrSize));
        //        logSize = DatabaseSize("FELogPrint");
        //        list.Add(new KeyValuePair<String, Double>("Log Print", logSize));
        //        spSize = DatabaseSize("FESupportDB");
        //        list.Add(new KeyValuePair<String, Double>("Support", spSize));
        //        ssSize = DatabaseSize("ASPState");
        //        list.Add(new KeyValuePair<String, Double>("Session", ssSize));
        //        var dt = new GG.DataTable();
        //        dt.AddColumn(new GG.Column(GG.ColumnType.String, "0", "Database"));
        //        dt.AddColumn(new GG.Column(GG.ColumnType.Number, "1", "Size (MB)"));
        //        foreach (var pair in list)
        //        {
        //            GG.Row gr = dt.NewRow();
        //            gr.AddCellRange(new GG.Cell[]{                        
        //                new GG.Cell(pair.Key),
        //                new GG.Cell(pair.Value)
        //            });
        //            dt.AddRow(gr);
        //        }

        //        System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        //        String mainData = oSerializer.Serialize(dt.GetJson());
        //        StringBuilder strDBStatement = new StringBuilder();
        //        strDBStatement.Append(@"<script type='text/javascript'>");
        //        strDBStatement.Append("google.charts.load('current', { 'packages': ['corechart'] });");
        //        strDBStatement.Append("google.charts.setOnLoadCallback(dbChart);");
        //        strDBStatement.Append("function dbChart() {");
        //        strDBStatement.Append("var data = new google.visualization.DataTable(" + mainData + ");");
        //        strDBStatement.Append("var options = {");
        //        strDBStatement.Append(" pieSliceText: 'label', ");
        //        strDBStatement.Append(" legend: { position: 'bottom', alignment: 'start',  fontSize: '8' },");
        //        strDBStatement.Append(" chartArea: { width: '90%', height: '60%' },");
        //        strDBStatement.Append("title: 'Database Sizes (GB)'};");
        //        strDBStatement.Append("var chartDB = new google.visualization.PieChart(document.getElementById(dbChart));");
        //        strDBStatement.Append("chartDB.draw(data, options);");
        //        strDBStatement.Append("};");
        //        strDBStatement.Append("</script>");
        //        return strDBStatement.ToString();
        //    }
        //    catch (Exception ex)
        //    { throw new System.Exception(ex.ToString()); }
        //}

        //public static Decimal getClientDatabaseSize(Int32 ClientID)
        //{
        //    try
        //    {
        //        String dbName = CLNT.getClientDB(ClientID);
        //        Decimal dbSize = Convert.ToDecimal(DatabaseSize(dbName));
        //        return dbSize;
        //    }
        //    catch (Exception ex)
        //    { throw new System.Exception(ex.ToString()); }
        //}

        public static Int32 getGlobalActiveRun()
        {
            try
            {
                Int32 iReply = 0;
                Dictionary<Int32, String> clntDatabases = CLNT.getAssociatedClientDBNamesList();
                String selData = "SELECT count(runID) FROM [Run] WHERE [dmgstID] = 1";
                
                foreach (KeyValuePair<Int32, String> pair in clntDatabases)
                {
                    String dbName = pair.Value;
                    GetClientConnections clntConn = new GetClientConnections();
                    String conString = clntConn.getClientDBString(dbName);
                    using (SqlConnection dataCon = new SqlConnection(conString))
                    {
                        try
                        {
                            dataCon.Open();
                            SqlCommand getData = new SqlCommand(selData, dataCon);
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            iReply += readData.GetInt32(0);
                                        }
                                    }
                                    else
                                    {
                                        iReply += 0;
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            dataCon.Close();
                            dataCon.Dispose();
                        }
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //public static String getGlobalContractsChart()
        //{
        //    try
        //    {
        //        System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        //        String ctrtData = CTRT.getContractsData();
        //        String mainData = oSerializer.Serialize(ctrtData);
        //        StringBuilder strCtrtStatement = new StringBuilder();
        //        strCtrtStatement.Append(@"<script type ='text/javascript'>");
        //        strCtrtStatement.Append("google.charts.load('current', { 'packages': ['corechart'] });");
        //        strCtrtStatement.Append("google.charts.setOnLoadCallback(contractChart);");
        //        strCtrtStatement.Append("function contractChart() {");
        //        strCtrtStatement.Append("var cttData = new google.visualization.DataTable(" + mainData + ");");
        //        strCtrtStatement.Append("var options = {");
        //        strCtrtStatement.Append(" pieSliceText: 'label', ");
        //        strCtrtStatement.Append(" legend: { position: 'bottom', alignment: 'start',  fontSize: '8' },");
        //        strCtrtStatement.Append(" chartArea: { width: '90%', height: '60%' },");
        //        strCtrtStatement.Append("title: 'Smarts Contracts'};");
        //        strCtrtStatement.Append("var chartCtrt = new google.visualization.PieChart(document.getElementById(ctrtChart));");
        //        strCtrtStatement.Append("chartCtrt.draw(cttData, options);");
        //        strCtrtStatement.Append("};");
        //        strCtrtStatement.Append("</script>");
        //        return strCtrtStatement.ToString();
        //    }
        //    catch (Exception ex)
        //    { throw new System.Exception(ex.ToString()); }
        //}
        
        //public static String getGlobalFolderSize()
        //{
        //    try
        //    {
        //        System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        //        var list = new List<KeyValuePair<String, long>>();
        //        DirectoryInfo dInfo = new DirectoryInfo(WebConfigurationManager.AppSettings["UploadPath"]); //uploaded items directory
        //        DirectoryInfo gInfo = new DirectoryInfo(WebConfigurationManager.AppSettings["GenPath"]); // generated items directory
        //        DirectoryInfo lpInfo = new DirectoryInfo(WebConfigurationManager.AppSettings["LogsPath"]); // log print items directory
        //        DirectoryInfo lgInfo = new DirectoryInfo(WebConfigurationManager.AppSettings["Audit"]); // site audit log items directory
        //        long sizeOfD = ( DirectorySize(dInfo, true) / 1024 );
        //        long sizeOfG = ( DirectorySize(gInfo, true) / 1024);
        //        long sizeOfS = sizeOfD + sizeOfG;
        //        list.Add(new KeyValuePair<String, long>("Services", sizeOfS));
        //        long sizeOfP = ( DirectorySize(lpInfo, true) / 1024 );
        //        list.Add(new KeyValuePair<String, long>("Log Print", sizeOfP));
        //        long sizeOfL = ( DirectorySize(lgInfo, true) / 1024 );
        //        list.Add(new KeyValuePair<String, long>("Audit Logs", sizeOfL));
        //        var dt = new GG.DataTable();
        //        dt.AddColumn(new GG.Column(GG.ColumnType.String, "0", "Folder Type"));
        //        dt.AddColumn(new GG.Column(GG.ColumnType.Number, "1", "Size (MB)"));
        //        foreach (var pair in list)
        //        {
        //            GG.Row gr = dt.NewRow();
        //            gr.AddCellRange(new GG.Cell[]{                        
        //                new GG.Cell(pair.Key),
        //                new GG.Cell(pair.Value)
        //            });
        //            dt.AddRow(gr);
        //        }

        //        String mainData = oSerializer.Serialize(dt.GetJson());
        //        StringBuilder strFLDStatement = new StringBuilder();
        //        strFLDStatement.Append(@"<script type='text/javascript'>");
        //        strFLDStatement.Append("google.charts.load('current', { 'packages': ['corechart'] });");
        //        strFLDStatement.Append("google.charts.setOnLoadCallback(fdChart);");
        //        strFLDStatement.Append("function fdChart() {");
        //        strFLDStatement.Append("var fdData = new google.visualization.DataTable(" + mainData + ");");
        //        strFLDStatement.Append("var options = {");
        //        strFLDStatement.Append(" pieSliceText: 'label', ");
        //        strFLDStatement.Append(" legend: { position: 'bottom', alignment: 'start',  fontSize: '8' },");
        //        strFLDStatement.Append(" chartArea: { width: '90%', height: '60%' },");
        //        strFLDStatement.Append("title: 'Smarts Folder Sizes (MB)'};");
        //        strFLDStatement.Append("var chartFLD = new google.visualization.PieChart(document.getElementById(fldChart));");
        //        strFLDStatement.Append("chartFLD.draw(fdData, options);");
        //        strFLDStatement.Append("};");
        //        strFLDStatement.Append("</script>");

        //        return strFLDStatement.ToString();
        //    }
        //    catch (Exception ex)
        //    { throw new System.Exception(ex.ToString()); }
        //}

        public static string getFolderSizeGlobal()
        {
            DataTable dtData = new DataTable();
            StringBuilder strScript = new StringBuilder();
            StringBuilder dataAppender = new StringBuilder();
            StringBuilder labelAppender = new StringBuilder();

            try
            {
                System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var list = new List<KeyValuePair<String, long>>();
                DirectoryInfo dInfo = new DirectoryInfo(WebConfigurationManager.AppSettings["UploadPath"]); //uploaded items directory
                DirectoryInfo gInfo = new DirectoryInfo(WebConfigurationManager.AppSettings["GenPath"]); // generated items directory
                DirectoryInfo lpInfo = new DirectoryInfo(WebConfigurationManager.AppSettings["LogsPath"]); // log print items directory
                DirectoryInfo lgInfo = new DirectoryInfo(WebConfigurationManager.AppSettings["Audit"]); // site audit log items directory
                long sizeOfD = (DirectorySize(dInfo, true) / 1024);
                long sizeOfG = (DirectorySize(gInfo, true) / 1024);
                long sizeOfS = sizeOfD + sizeOfG;
                list.Add(new KeyValuePair<String, long>("Services", sizeOfS));
                long sizeOfP = (DirectorySize(lpInfo, true) / 1024);
                list.Add(new KeyValuePair<String, long>("Log Print", sizeOfP));
                long sizeOfL = (DirectorySize(lgInfo, true) / 1024);
                list.Add(new KeyValuePair<String, long>("Audit Logs", sizeOfL));

                dtData.Columns.Add("Folder", typeof(string));
                dtData.Columns.Add("Size", typeof(long));

                dtData.Rows.Add("Services", sizeOfS);
                dtData.Rows.Add("Log Print", sizeOfP);
                dtData.Rows.Add("Audit Logs", sizeOfL);



                // Create Chart from charts.js 
                    //strScript.Append("<script src='../Scripts/Chart.bundle.js'></script>");
                    //strScript.Append("<script src='../Scripts/utils.js'></script>");
                    strScript.Append("<script>");

                    strScript.Append("var randomScalingFactor = function() {");
                    strScript.Append(" return Math.round(Math.random() * 100); }; ");

                    strScript.Append(" var configFolderSizeGlobal = {  type: 'doughnut', data: { datasets: [{ ");
                    dataAppender.Append(" data: [ ");
                    labelAppender.Append(" labels: [ ");


                    foreach (DataRow row in dtData.Rows)
                    {
                        dataAppender.Append(row["Size"] + ",");
                        labelAppender.Append("\"" + row["Folder"] + " : " + row["Size"] + "\"" + ",");
                    }

                    dataAppender.Append("],");
                    labelAppender.Append("] },");
                    strScript.Append(dataAppender);

                    strScript.Append(" backgroundColor: [ '#F8C471', '#FF0000', '#008080' , ], ");
                    strScript.Append(" borderWidth: 2, ");
                    strScript.Append(" label: 'Dataset 3' }], ");

                    strScript.Append(labelAppender);
                    strScript.Append(@"
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        legend: {
                            position: 'left',
                            
                        },
                        title: {
                            display: true,
                            text: 'SMART Folder Sizes (MB)'
                        },
                        animation: {
                            animateScale: true,
                            animateRotate: true
                        },
                        tooltips: {
                             mode: 'single',  // this is the Chart.js default, no need to set
                                    callbacks: {
                                    label: function (tooltipItems, data) {
                                        var i, label = [], l = data.datasets.length;
                                        for (i = 0; i < l; i += 1) {
                                            label[i] = data.labels[tooltipItems.index] ;
                                        }
                                        return label;
                                    }
                                }
                         }
                    }
                };
                ");

                    strScript.Append("</script>");

                    return strScript.ToString();

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<long> getFolderSizeGlobalList()
        {            
            try
            {
                List<long> iReply = new List<long>();
                DirectoryInfo dInfo = new DirectoryInfo(WebConfigurationManager.AppSettings["UploadPath"]); //uploaded items directory
                DirectoryInfo gInfo = new DirectoryInfo(WebConfigurationManager.AppSettings["GenPath"]); // generated items directory
                DirectoryInfo lpInfo = new DirectoryInfo(WebConfigurationManager.AppSettings["LogsPath"]); // log print items directory
                DirectoryInfo lgInfo = new DirectoryInfo(WebConfigurationManager.AppSettings["Audit"]); // site audit log items directory
                long sizeOfD = (DirectorySize(dInfo, true) / 1024);
                long sizeOfG = (DirectorySize(gInfo, true) / 1024);
                long sizeOfS = sizeOfD + sizeOfG;
                long sizeOfP = (DirectorySize(lpInfo, true) / 1024);
                long sizeOfL = (DirectorySize(lgInfo, true) / 1024);
                iReply.Add(sizeOfS);
                iReply.Add(sizeOfP);
                iReply.Add(sizeOfL);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }


        //public static String getGlobalHRChart()
        //{
        //    try
        //    {
        //        System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        //        String hrData = CUSR.getGlobalUserPerRole();
        //        String mainData = oSerializer.Serialize(hrData);
        //        StringBuilder strHRStatement = new StringBuilder();
        //        strHRStatement.Append(@"<script type='text/javascript'>");
        //        strHRStatement.Append("google.charts.load('current', { 'packages': ['corechart'] });");
        //        strHRStatement.Append("google.charts.setOnLoadCallback(hrChart);");
        //        strHRStatement.Append("function hrChart() {");
        //        strHRStatement.Append("var hrdata = new google.visualization.DataTable(" + mainData + ");");
        //        strHRStatement.Append("var options = {");
        //        strHRStatement.Append(" chartArea: { width: '90%', height: '60%' },");
        //        strHRStatement.Append(" legend: { position: 'bottom', alignment: 'start',  fontSize: '8' },");
        //        strHRStatement.Append(" pieSliceText: 'label', ");
        //        strHRStatement.Append(" title: 'Smarts Users/Role'}; ");
        //        strHRStatement.Append("var chartHR = new google.visualization.PieChart(document.getElementById(hrChart));");
        //        strHRStatement.Append("chartHR.draw(hrdata, options);");
        //        strHRStatement.Append("};");
        //        strHRStatement.Append("</script>");
        //        return strHRStatement.ToString();
        //    }
        //    catch (Exception ex)
        //    { throw new System.Exception(ex.ToString()); }
        //}

        //public static String getGlobalUserChart()
        //{
        //    try
        //    {
        //        System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        //        String usrData = CUSR.getLoggedUserStat();
        //        String mainData = oSerializer.Serialize(usrData);
        //        StringBuilder strUsrStatement = new StringBuilder();
        //        strUsrStatement.Append(@"<script type='text/javascript'>");
        //        strUsrStatement.Append("google.charts.load('current', { 'packages': ['corechart'] });");
        //        strUsrStatement.Append("google.charts.setOnLoadCallback(usrChart);");
        //        strUsrStatement.Append("function usrChart() {");
        //        strUsrStatement.Append("var usrdata = new google.visualization.DataTable(" + mainData + ");");
        //        strUsrStatement.Append("var options = {");
        //        strUsrStatement.Append(" pieSliceText: 'label', ");
        //        strUsrStatement.Append(" legend: { position: 'bottom', alignment: 'start',  fontSize: '8' },");
        //        strUsrStatement.Append(" chartArea: { width: '90%', height: '60%' },");
        //        strUsrStatement.Append(" title: 'Smarts Users'}; ");
        //        strUsrStatement.Append("var chartUser = new google.visualization.PieChart(document.getElementById(usrChart));");
        //        strUsrStatement.Append("chartUser.draw(usrdata, options);");
        //        strUsrStatement.Append("};");
        //        strUsrStatement.Append("</script>");
        //        return strUsrStatement.ToString();
        //    }
        //    catch (Exception ex)
        //    { throw new System.Exception(ex.ToString()); }
        //}

        public static List<Int32> getGlobalBGCount()
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                DateTime date1 = DateTime.Today.AddDays(-1);  // previous 24 hours
                DateTime date2 = DateTime.Today.AddDays(-14); // previous 2 weeks
                DateTime date3 = DateTime.Today.AddDays(-30); // previous 4 weeks
                String selData = "SELECT count(accBGID) FROM [accBGs] WHERE [pTime] > @dt;";
                Int32 count24 = 0, count2 = 0, count4 = 0;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEAccountingDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData24 = new SqlCommand(selData, dataCon);
                        getData24.Parameters.AddWithValue("@dt", SqlDbType.DateTime).Value = date1.ToString();
                        using (SqlDataReader readData = getData24.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count24 = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    count24 = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                        SqlCommand getData2 = new SqlCommand(selData, dataCon);
                        getData2.Parameters.AddWithValue("@dt", SqlDbType.DateTime).Value = date2.ToString();
                        using (SqlDataReader readData = getData2.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count2 = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    count2 = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                        SqlCommand getData4 = new SqlCommand(selData, dataCon);
                        getData4.Parameters.AddWithValue("@dt", SqlDbType.DateTime).Value = date3.ToString();
                        using (SqlDataReader readData = getData4.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count4 = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    count4 = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply.Add(count24);
                iReply.Add(count2);
                iReply.Add(count4);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellCount()
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(welID) FROM [dmgWell]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    iReply = readData.GetInt32(0);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getGlobalWellRegistrations()
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                String selData = "SELECT (SELECT COUNT(wsrID) FROM [dmgWellServiceRegistration] WHERE [isApproved] = 0) as requested, (SELECT COUNT(wsrID) FROM [dmgWellServiceRegistration] WHERE [isApproved] = 1) as approved";
                Int32 count1 = 0, count2 = 0;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count1 = readData.GetInt32(0);
                                        count2 = readData.GetInt32(1);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply.Add(count1);
                iReply.Add(count2);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getGlobalRawQCCount()
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                DateTime date1 = DateTime.Today.AddDays(-1);  // previous 24 hours
                DateTime date2 = DateTime.Today.AddDays(-14); // previous 2 weeks
                DateTime date3 = DateTime.Today.AddDays(-30); // previous 4 weeks
                String selData = "SELECT count(acctID) FROM [accRawQC] WHERE [pTime] > @dt;";
                Int32 count24 = 0, count2 = 0, count4 = 0;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEAccountingDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData24 = new SqlCommand(selData, dataCon);
                        getData24.Parameters.AddWithValue("@dt", SqlDbType.DateTime).Value = date1.ToString();
                        using (SqlDataReader readData = getData24.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count24 = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    count24 = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                        SqlCommand getData2 = new SqlCommand(selData, dataCon);
                        getData2.Parameters.AddWithValue("@dt", SqlDbType.DateTime).Value = date2.ToString();
                        using (SqlDataReader readData = getData2.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count2 = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    count2 = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                        SqlCommand getData4 = new SqlCommand(selData, dataCon);
                        getData4.Parameters.AddWithValue("@dt", SqlDbType.DateTime).Value = date3.ToString();
                        using (SqlDataReader readData = getData4.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count4 = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    count4 = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply.Add(count24);
                iReply.Add(count2);
                iReply.Add(count4);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getGlobalResQCCount()
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                DateTime date1 = DateTime.Today.AddDays(-1);  // previous 24 hours
                DateTime date2 = DateTime.Today.AddDays(-14); // previous 2 weeks
                DateTime date3 = DateTime.Today.AddDays(-30); // previous 4 weeks
                String selData = "SELECT count(acctID) FROM [accResQC] WHERE [pTime] > @dt;";
                Int32 count24 = 0, count2 = 0, count4 = 0;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEAccountingDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData24 = new SqlCommand(selData, dataCon);
                        getData24.Parameters.AddWithValue("@dt", SqlDbType.DateTime).Value = date1.ToString();
                        using (SqlDataReader readData = getData24.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count24 = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    count24 = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                        SqlCommand getData2 = new SqlCommand(selData, dataCon);
                        getData2.Parameters.AddWithValue("@dt", SqlDbType.DateTime).Value = date2.ToString();
                        using (SqlDataReader readData = getData2.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count2 = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    count2 = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                        SqlCommand getData4 = new SqlCommand(selData, dataCon);
                        getData4.Parameters.AddWithValue("@dt", SqlDbType.DateTime).Value = date3.ToString();
                        using (SqlDataReader readData = getData4.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count4 = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    count4 = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply.Add(count24);
                iReply.Add(count2);
                iReply.Add(count4);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getGlobalLogPrintCount() 
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                DateTime date1 = DateTime.Today.AddDays(-1);  // previous 24 hours
                DateTime date2 = DateTime.Today.AddDays(-14); // previous 2 weeks
                DateTime date3 = DateTime.Today.AddDays(-30); // previous 4 weeks
                String selData = "SELECT count(orderID) FROM [accLogPrintOrder] WHERE [pTime] > @dt;";
                Int32 count24 = 0, count2 = 0, count4 = 0;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEAccountingDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData24 = new SqlCommand(selData, dataCon);
                        getData24.Parameters.AddWithValue("@dt", SqlDbType.DateTime).Value = date1.ToString();
                        using (SqlDataReader readData = getData24.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count24 = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    count24 = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                        SqlCommand getData2 = new SqlCommand(selData, dataCon);
                        getData2.Parameters.AddWithValue("@dt", SqlDbType.DateTime).Value = date2.ToString();
                        using (SqlDataReader readData = getData2.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count2 = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    count2 = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                        SqlCommand getData4 = new SqlCommand(selData, dataCon);
                        getData4.Parameters.AddWithValue("@dt", SqlDbType.DateTime).Value = date3.ToString();
                        using (SqlDataReader readData = getData4.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count4 = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    count4 = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply.Add(count24);
                iReply.Add(count2);
                iReply.Add(count4);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getGlobalMSSRCount()
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                DateTime date1 = DateTime.Today.AddDays(-1);  // previous 24 hours
                DateTime date2 = DateTime.Today.AddDays(-14); // previous 2 weeks
                DateTime date3 = DateTime.Today.AddDays(-30); // previous 4 weeks
                String selData = "SELECT count(acctID) FROM [accMSSR] WHERE [pTime] > @dt;";
                Int32 count24 = 0, count2 = 0, count4 = 0;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEAccountingDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData24 = new SqlCommand(selData, dataCon);
                        getData24.Parameters.AddWithValue("@dt", SqlDbType.DateTime).Value = date1.ToString();
                        using (SqlDataReader readData = getData24.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count24 = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    count24 = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                        SqlCommand getData2 = new SqlCommand(selData, dataCon);
                        getData2.Parameters.AddWithValue("@dt", SqlDbType.DateTime).Value = date2.ToString();
                        using (SqlDataReader readData = getData2.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count2 = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    count2 = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                        SqlCommand getData4 = new SqlCommand(selData, dataCon);
                        getData4.Parameters.AddWithValue("@dt", SqlDbType.DateTime).Value = date3.ToString();
                        using (SqlDataReader readData = getData4.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count4 = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    count4 = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply.Add(count24);
                iReply.Add(count2);
                iReply.Add(count4);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getGlobalWellProfileCount()
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                DateTime date1 = DateTime.Today.AddDays(-1);  // previous 24 hours
                DateTime date2 = DateTime.Today.AddDays(-14); // previous 2 weeks
                DateTime date3 = DateTime.Today.AddDays(-30); // previous 4 weeks
                String selData = "SELECT count(accID) FROM [accWellProfile] WHERE [pTime] > @dt;";
                Int32 count24 = 0, count2 = 0, count4 = 0;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEAccountingDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData24 = new SqlCommand(selData, dataCon);
                        getData24.Parameters.AddWithValue("@dt", SqlDbType.DateTime).Value = date1.ToString();
                        using (SqlDataReader readData = getData24.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count24 = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    count24 = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                        SqlCommand getData2 = new SqlCommand(selData, dataCon);
                        getData2.Parameters.AddWithValue("@dt", SqlDbType.DateTime).Value = date2.ToString();
                        using (SqlDataReader readData = getData2.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count2 = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    count2 = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                        SqlCommand getData4 = new SqlCommand(selData, dataCon);
                        getData4.Parameters.AddWithValue("@dt", SqlDbType.DateTime).Value = date3.ToString();
                        using (SqlDataReader readData = getData4.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count4 = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    count4 = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply.Add(count24);
                iReply.Add(count2);
                iReply.Add(count4);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //public static String getGlobalSupportRequestChart()
        //{
        //    try
        //    {
        //        System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        //        String spData = SPRT.getSupReqData();
        //        String mainData = oSerializer.Serialize(spData);
        //        StringBuilder strSRStatement = new StringBuilder();
        //        strSRStatement.Append(@"<script type='text/javascript'>");
        //        strSRStatement.Append("google.charts.load('current', { 'packages': ['corechart'] });");
        //        strSRStatement.Append("google.charts.setOnLoadCallback(srChart);");
        //        strSRStatement.Append("function srChart() {");
        //        strSRStatement.Append("var sprqData = new google.visualization.DataTable(" + mainData + ");");
        //        strSRStatement.Append("var options = {");
        //        strSRStatement.Append(" pieSliceText: 'label', ");
        //        strSRStatement.Append(" legend: { position: 'bottom', alignment: 'start',  fontSize: '8' },");
        //        strSRStatement.Append(" chartArea: { width: '90%', height: '60%' },");
        //        strSRStatement.Append("title: 'Smarts Support Requests'};");
        //        strSRStatement.Append("var chartSpRq = new google.visualization.PieChart(document.getElementById(sqChart));");
        //        strSRStatement.Append("chartSpRq.draw(sprqData, options);");
        //        strSRStatement.Append("};");
        //        strSRStatement.Append("</script>");
        //        return strSRStatement.ToString();
        //    }
        //    catch (Exception ex)
        //    { throw new System.Exception(ex.ToString()); }
        //}


        public static String getContractStatusGlobal()
        {
            DataTable dsChartData = new DataTable();

            dsChartData = CTRT.getContractsDataGlobal();

            StringBuilder strScript = new StringBuilder();
            StringBuilder dataAppender = new StringBuilder();
            StringBuilder labelAppender = new StringBuilder();
            try
            {
                //strScript.Append("<script src='../Scripts/Chart.bundle.js'></script>");
                //strScript.Append("<script src='../Scripts/utils.js'></script>");
                strScript.Append("<script>");

                strScript.Append("var randomScalingFactor = function() {");
                strScript.Append(" return Math.round(Math.random() * 100); }; ");

                strScript.Append(" var configContractsGlobal = {  type: 'doughnut', data: { datasets: [{ ");
                dataAppender.Append(" data: [ ");
                labelAppender.Append(" labels: [ ");


                foreach (DataRow row in dsChartData.Rows)
                {
                    dataAppender.Append(row["Count"] + ",");
                    labelAppender.Append("\"" + row["Status"] + " : " + row["Count"] + "\"" + ",");
                }

                dataAppender.Append("],");
                labelAppender.Append("] },");
                strScript.Append(dataAppender);

                strScript.Append(" backgroundColor: [ '#E67E22', '#5499C7', '#A569BD',  ], ");
                strScript.Append(" borderWidth: 2, ");
                strScript.Append(" label: 'Dataset 3' }], ");

                strScript.Append(labelAppender);
                strScript.Append(@"
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        legend: {
                            position: 'left',
                            
                        },
                        title: {
                            display: true,
                            text: 'SMART Contracts Status'
                        },
                        animation: {
                            animateScale: true,
                            animateRotate: true
                        },
                        tooltips: {
                             mode: 'single',  // this is the Chart.js default, no need to set
                                    callbacks: {
                                    label: function (tooltipItems, data) {
                                        var i, label = [], l = data.datasets.length;
                                        for (i = 0; i < l; i += 1) {
                                            label[i] = data.labels[tooltipItems.index] ;
                                        }
                                        return label;
                                    }
                                }
                         }
                    }
                };
                ");

                strScript.Append("</script>");

                return strScript.ToString();

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }

        }

        public static string GetRandomHexColor()
        {
            var result = "#" + Guid.NewGuid().ToString().Substring(0, 6);
            return result;
        }

        public static String getGlobalSupportRequestChartGlobal()
        {
            DataTable dsChartData = new DataTable();

            dsChartData = SPRT.getSupReqDataGlobal(); 

            StringBuilder strScript = new StringBuilder();
            StringBuilder dataAppender = new StringBuilder();
            StringBuilder labelAppender = new StringBuilder();
            StringBuilder colorAppender = new StringBuilder();
            try
            {
                //strScript.Append("<script src='../Scripts/Chart.bundle.js'></script>");
                //strScript.Append("<script src='../Scripts/utils.js'></script>");
                strScript.Append("<script>");

                strScript.Append("var randomScalingFactor = function() {");
                strScript.Append(" return Math.round(Math.random() * 100); }; ");

                strScript.Append(" var configSupportRequestsGlobal = {  type: 'doughnut', data: { datasets: [{ ");
                dataAppender.Append(" data: [ ");
                colorAppender.Append(" backgroundColor: [ ");
                labelAppender.Append(" labels: [ ");


                foreach (DataRow row in dsChartData.Rows)
                {
                    string color = GetRandomHexColor();
                    dataAppender.Append(row["rCount"] + ",");
                    colorAppender.Append("'" + color + "'" + ",");
                    labelAppender.Append("\"" + row["rStatus"] + " : " + row["rCount"] + "\"" + ",");
                    color = null;
                }

                dataAppender.Append("],");
                colorAppender.Append("],");
                labelAppender.Append("] },");
                strScript.Append(dataAppender);

                strScript.Append(colorAppender);
                //strScript.Append(" backgroundColor: [ '#A569BD', window.chartColors.orange,  ], ");
                strScript.Append(" borderWidth: 2, ");
                strScript.Append(" label: 'Dataset 3' }], ");

                strScript.Append(labelAppender);
                strScript.Append(@"
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        legend: {
                            position: 'left',
                            
                        },
                        title: {
                            display: true,
                            text: 'SMART Support Requests'
                        },
                        animation: {
                            animateScale: true,
                            animateRotate: true
                        },
                        tooltips: {
                             mode: 'single',  // this is the Chart.js default, no need to set
                                    callbacks: {
                                    label: function (tooltipItems, data) {
                                        var i, label = [], l = data.datasets.length;
                                        for (i = 0; i < l; i += 1) {
                                            label[i] = data.labels[tooltipItems.index] ;
                                        }
                                        return label;
                                    }
                                }
                         }
                    }
                };
                ");

                strScript.Append("</script>");

                return strScript.ToString();

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }

        }

        public static String getGuageRunProcessedCount(Int32 RunID, String counter, String maxCounter, String label , String title, String ControlName)
        {
            try
            {
                StringBuilder strScript = new StringBuilder();

                //strScript.Append(@"<script src='../../Scripts/raphael-2.1.4.min.js'></script>");
                //strScript.Append("<script src='../../Scripts/justgage.js'></script>");
                strScript.Append("<script async defer>");
                strScript.Append("var g1;");
                strScript.Append("document.addEventListener(\"DOMContentLoaded\", function(event) {");
                strScript.Append("g1 = new JustGage({");
                strScript.Append("id: \"MainContent_" + ControlName + "_divGuage1\", ");
                strScript.Append("value: " + counter + ",");
                strScript.Append("min: 0,");
                strScript.Append("max: " + maxCounter + ",");
                strScript.Append("donut: false,");
                strScript.Append("gaugeWidthScale: 0.5,");
                strScript.Append("counter: true,");
                strScript.Append("title: \"" + title + "\", ");
                strScript.Append("label: \"" + label + "\", ");
                strScript.Append("labelFontColor: \"Black\" , ");
                strScript.Append("levelColors: [ \"#EE1B45\" , \"#E1AC28\"  , \"#E6E31E\", \"#7AE91D\"] ");
                strScript.Append("}); ");
                strScript.Append("});");
                strScript.Append("</script>");

                return strScript.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getGuageRunTotalCount(Int32 RunID, Int32 Counter , String label, String title, String color, String ControlName)
        {
            try
            {
                Int32 max = getMaxCount(Counter);  // get max val for the guage
                StringBuilder strScript = new StringBuilder();

                //strScript.Append(@"<script src='../../Scripts/raphael-2.1.4.min.js'></script>");
                //strScript.Append("<script src='../../Scripts/justgage.js'></script>");
                strScript.Append("<script async defer>");
                strScript.Append("var g1;");
                strScript.Append("document.addEventListener(\"DOMContentLoaded\", function(event) {");
                strScript.Append("g1 = new JustGage({");
                strScript.Append("id: \"MainContent_" + ControlName + "_divGuage2\", ");
                strScript.Append("value: " + Counter + ",");
                strScript.Append("min: 0,");
                strScript.Append("max: " + max + ",");
                strScript.Append("donut: false,");
                strScript.Append("gaugeWidthScale: 0.6,");
                strScript.Append("counter: true,");
                strScript.Append("title: \"" + title + "\", ");
                strScript.Append("label: \"" +label+ "\", ");
                strScript.Append("levelColors: [ \"Blue\" ] , ");
                strScript.Append("labelFontColor: \"Black\" , ");
                strScript.Append("levelColors: [ \"" +color+ "\" ] , ");
                strScript.Append("pointer: true, ");
                strScript.Append("pointerOptions: { ");
                strScript.Append("toplength: -15,");
                strScript.Append("bottomlength: 10,");
                strScript.Append("bottomwidth: 12,");
                strScript.Append("color: '#8e8e93',");
                strScript.Append("stroke: '#ffffff',");
                strScript.Append("stroke_width: 3,");
                strScript.Append("stroke_linecap: 'round' }, ");
                strScript.Append("}); ");
                strScript.Append("});");
                strScript.Append("</script>");

                return strScript.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getGuageTotalCount(Int32 Counter, String color , String label , String title ,  String divID)
        {
            try
            {
                Int32 max = getMaxCount(Counter);  // get max val for the guage
                StringBuilder strScript = new StringBuilder();

                //strScript.Append("<script src='../../Scripts/justgage.js'></script>");
                //strScript.Append("<script src='../../Scripts/raphael-2.1.4.min.js'></script>");
                strScript.Append("<script async defer>");
                strScript.Append("var g1;");
                strScript.Append("document.addEventListener(\"DOMContentLoaded\", function(event) {");
                strScript.Append("g1 = new JustGage({");
                strScript.Append("id: \""+ divID +"\", "); 
                strScript.Append("value: " + Counter + ",");
                strScript.Append("min: 0,");
                strScript.Append("max: " + max + ",");
                strScript.Append("donut: false,");
                strScript.Append("gaugeWidthScale: 0.6,");
                strScript.Append("counter: true,");
                strScript.Append("title: \"" + title + "\", ");
                strScript.Append("label: \""+label+"\", ");
                strScript.Append("levelColors: [ \"Blue\" ] , ");
                strScript.Append("labelFontColor: \"Black\" , ");
                strScript.Append("levelColors: [\"" + color + "\" ] , ");
                strScript.Append("pointer: true, ");
                strScript.Append("pointerOptions: { ");
                strScript.Append("toplength: -15,");
                strScript.Append("bottomlength: 10,");
                strScript.Append("bottomwidth: 12,");
                strScript.Append("color: '#8e8e93',");
                strScript.Append("stroke: '#ffffff',");
                strScript.Append("stroke_width: 3,");
                strScript.Append("stroke_linecap: 'round' }, ");
                strScript.Append("}); ");
                strScript.Append("});");
                strScript.Append("</script>");

                return strScript.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getGuageTotalVisitors(Int32 Counter, String color, String label, String title, String divID)
        {
            try
            {
                Int32 max = getMaxCount(Counter);  // get max val for the guage
                StringBuilder strScript = new StringBuilder();

                //strScript.Append(@"<script src='../../Scripts/raphael-2.1.4.min.js'></script>");
                //strScript.Append("<script src='../../Scripts/justgage.js'></script>");
                strScript.Append("<script async defer>");
                strScript.Append("var g1;");
                strScript.Append("document.addEventListener(\"DOMContentLoaded\", function(event) {");
                strScript.Append("g1 = new JustGage({");
                strScript.Append("id: \"" + divID + "\", ");
                strScript.Append("value: " + Counter + ",");
                strScript.Append("min: 0,");
                strScript.Append("max: " + max + ",");
                strScript.Append("donut: false,");
                strScript.Append("gaugeWidthScale: 0.6,");
                strScript.Append("counter: true,");
                strScript.Append("title: \"" + title + "\", ");
                strScript.Append("label: \"" + label + "\", ");
                strScript.Append("levelColors: [ \"Blue\" ] , ");
                strScript.Append("labelFontColor: \"Black\" , ");
                strScript.Append("levelColors: [\"" + color + "\" ] , ");
                strScript.Append("pointer: true, ");
                strScript.Append("pointerOptions: { ");
                strScript.Append("toplength: -15,");
                strScript.Append("bottomlength: 10,");
                strScript.Append("bottomwidth: 12,");
                strScript.Append("color: '#8e8e93',");
                strScript.Append("stroke: '#ffffff',");
                strScript.Append("stroke_width: 3,");
                strScript.Append("stroke_linecap: 'round' }, ");
                strScript.Append("}); ");
                strScript.Append("});");
                strScript.Append("</script>");

                return strScript.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static String getRawQC24Chart(Int32 Counter, String color, String label, String title, String divID)
        {
            try
            {
                Int32 max = getMaxCount(Counter);  // get max val for the guage
                StringBuilder strScript = new StringBuilder();

                //strScript.Append(@"<script src='../../Scripts/raphael-2.1.4.min.js'></script>");
                //strScript.Append("<script src='../../Scripts/justgage.js'></script>");
                strScript.Append("<script async defer>");
                strScript.Append("var g1;");
                strScript.Append("document.addEventListener(\"DOMContentLoaded\", function(event) {");
                strScript.Append("g1 = new JustGage({");
                strScript.Append("id: \"" + divID + "\", ");
                strScript.Append("value: " + Counter + ",");
                strScript.Append("min: 0,");
                strScript.Append("max: " + max + ",");
                strScript.Append("donut: false,");
                strScript.Append("gaugeWidthScale: 0.6,");
                strScript.Append("counter: true,");
                strScript.Append("title: \"" + title + "\", ");
                strScript.Append("label: \"" + label + "\", ");
                strScript.Append("levelColors: [ \"Blue\" ] , ");
                strScript.Append("labelFontColor: \"Black\" , ");
                strScript.Append("levelColors: [\"" + color + "\" ] , ");
                strScript.Append("pointer: true, ");
                strScript.Append("pointerOptions: { ");
                strScript.Append("toplength: -15,");
                strScript.Append("bottomlength: 10,");
                strScript.Append("bottomwidth: 12,");
                strScript.Append("color: '#8e8e93',");
                strScript.Append("stroke: '#ffffff',");
                strScript.Append("stroke_width: 3,");
                strScript.Append("stroke_linecap: 'round' }, ");
                strScript.Append("}); ");
                strScript.Append("});");
                strScript.Append("</script>");

                return strScript.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getGuageGenericCounterFull(Int32 Counter, String color, String label, String title, String titlePosition, String fontcolor, String divID)
        {
            try
            {
                StringBuilder strScript = new StringBuilder();

                //strScript.Append(@"<script src='../../Scripts/raphael-2.1.4.min.js'></script>");
                //strScript.Append("<script src='../../Scripts/justgage.js'></script>");
                strScript.Append("<script async defer>");
                strScript.Append("var g1;");
                strScript.Append("document.addEventListener(\"DOMContentLoaded\", function(event) {");
                strScript.Append("g1 = new JustGage({");
                strScript.Append("id: \"" + divID + "\", ");
                strScript.Append("value: " + Counter + ",");
                strScript.Append("min: 0,");
                strScript.Append("max: " + Counter + ",");
                strScript.Append("donut: true,");
                strScript.Append("gaugeWidthScale: 0.3,");
                strScript.Append("counter: true,");
                strScript.Append("title: \"" + title + "\", ");
                strScript.Append("titlePosition: \"" + titlePosition + "\", ");
                strScript.Append("label: \"" + label + "\", ");
                strScript.Append("levelColors: [ \"Blue\" ] , ");
                strScript.Append("labelFontColor: \"Black\" , ");
                strScript.Append("levelColors: [\"" + color + "\" ] , ");
                strScript.Append("valueFontColor: [\"" + fontcolor + "\" ] , ");
                strScript.Append("}); ");
                strScript.Append("});");
                strScript.Append("</script>");

                return strScript.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getMaxCount(Int32 Value)
        {
            try
            {
                Int32 iReply = 0;
                if (Value > 0 && Value <= 10) { iReply = 15; }
                else if (Value >= 11 && Value <= 20) { iReply = 25; }
                else if (Value >= 21 && Value <= 45) { iReply = 50; }
                else if (Value >= 46 && Value <= 75) { iReply = 100; }
                else if (Value >= 76 && Value <= 130) { iReply = 150; }
                else if (Value >= 130 && Value <= 200) { iReply = 250; }
                else if (Value >= 201 && Value <= 325) { iReply = 350; }
                else if (Value >= 326 && Value <= 450) { iReply = 475; }
                else if (Value >= 451 && Value <= 650) { iReply = 700; }
                else if (Value >= 651 && Value <= 850) { iReply = 950; }
                else if (Value >= 851 && Value <= 1150) { iReply = 1300; }
                else if (Value >= 1151 && Value <= 1550) { iReply = 1675; }
                else if (Value >= 1551 && Value <= 1900) { iReply = 2050; }
                else if (Value >= 1901 && Value <= 2450) { iReply = 2500; }
                else if (Value >= 2451 && Value <= 2850) { iReply = 3000; }
                else if (Value >= 2851 && Value <= 3500) { iReply = 3650; }
                else if (Value >= 3501 && Value <= 4000) { iReply = 4150; }
                else if (Value >= 4001 && Value <= 4500) { iReply = 4650; }
                else if (Value >= 4501 && Value <= 5000) { iReply = 5250; }
                else if (Value >= 5001 && Value <= 5700) { iReply = 5800; }
                else if (Value >= 5701 && Value <= 6250) { iReply = 6300; }
                else if (Value >= 6251 && Value <= 6600) { iReply = 6800; }
                else if (Value >= 6601 && Value <= 7250) { iReply = 7400; }
                else if (Value >= 7251 && Value <= 7900) { iReply = 8000; }
                else if (Value >= 7901 && Value <= 8500) { iReply = 8750; }
                else if (Value >= 8501 && Value <= 9500) { iReply = 9750; }
                else if (Value >= 9501 && Value <= 10500) { iReply = 11000; }
                else if (Value >= 10501 && Value <= 12000) { iReply = 12500; }
                else if (Value >= 12001 && Value <= 15000) { iReply = 15000; }
                else if (Value >= 15001 && Value <= 20000) { iReply = 20000; }
                else if (Value >= 20001 && Value <= 30000) { iReply = 30000; }
                else if (Value >= 30001 && Value <= 50000) { iReply = 50000; }
                else if (Value >= 50001 && Value <= 75000) { iReply = 75000; }
                else if (Value >= 75001 && Value <= 150000) { iReply = 150000; }
                else if (Value >= 150001 && Value <= 250000) { iReply = 250000; }
                else if (Value >= 250001 && Value <= 500000) { iReply = 500000; }
                else if (Value >= 500001 && Value <= 750000) { iReply = 750000; }
                else if (Value >= 750001 && Value <= 1000000) { iReply = 1000000; }
                else { iReply = 2000000; }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }                  

        public static String getLPStatement()
        {
            try
            {
                StringBuilder strLPStatement = new StringBuilder();
                strLPStatement.Append(@"<script type='text/javascript' async defer>");
                strLPStatement.Append("google.charts.load('current', { 'packages': ['corechart'] });");
                strLPStatement.Append("google.charts.setOnLoadCallback(lpChart);");
                strLPStatement.Append("function lpChart() {");
                strLPStatement.Append("var lpdata = new google.visualization.DataTable(document.getElementById(lpData).value);");
                strLPStatement.Append("var options = {");
                strLPStatement.Append(" pieSliceText: 'label', ");
                strLPStatement.Append(" title: 'Log Print Service'}; ");
                strLPStatement.Append("var chartLP = new google.visualization.PieChart(document.getElementById(lpChart));");
                strLPStatement.Append("chartLP.draw(lpdata, options);");
                strLPStatement.Append("};");
                strLPStatement.Append("</script>");

                return strLPStatement.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static ArrayList getTotalQCProcessed(String ClientDBConnection)
        {
            try
            {
                ArrayList iReply = new ArrayList();
                Int32 counter1 = 0, counter2 = 0;
                DateTime date1 = DateTime.Today.AddDays(-1);  // previous 24 hours
                DateTime date2 = DateTime.Today.AddDays(-14); // previous 2 weeks
                //String name = String.Empty;
                String selData = "SELECT count(resID) FROM [RawQCResults] WHERE [pTime] > @dt;";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData1 = new SqlCommand(selData, dataCon);
                        getData1.Parameters.AddWithValue("@dt", SqlDbType.DateTime).Value = date1.ToString();
                        using (SqlDataReader readData = getData1.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        counter1 = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    counter1 = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                        SqlCommand getData2 = new SqlCommand(selData, dataCon);
                        getData2.Parameters.AddWithValue("@dt", SqlDbType.DateTime).Value = date2.ToString();
                        using (SqlDataReader readData = getData2.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        counter2 = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    counter2 = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply.Add(counter1);
                iReply.Add(counter2);
                iReply.Add(date1);
                iReply.Add(date2);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }


        public static Int32 getTotalCorrectionProcessed(String ClientDBConnection)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 counter = 0;
                String name = String.Empty;
                String selData = "SELECT count(resID) FROM [RawQCResults] WHERE [srvID] = 46";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        counter = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    counter = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return counter;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getChartsIncDipLongShortAzimuth(String div, System.Data.DataTable ChartData)
        {
            try
            {
                StringBuilder strScript = new StringBuilder(), dataDepth = new StringBuilder(), dataLC = new StringBuilder(), dataSC = new StringBuilder();
                // Loading Data

                dataDepth.Append(" data: [");
                dataLC.Append(" data: [ ");
                dataSC.Append(" data: [ ");

                foreach (DataRow row in ChartData.Rows)
                {
                    dataDepth.Append(Math.Round((Decimal)row["Depth"], 2) + ",");
                    dataLC.Append(Math.Round((Decimal)row["LCAzm"], 2) + ",");
                    dataSC.Append(Math.Round((Decimal)row["SCAzm"], 2) + ",");
                }

                dataDepth.Append(" ], ");
                dataLC.Append("], ");
                dataSC.Append("],");

                strScript.Append("<script type='text/javascript' src='../../eCharts/echarts.min.js'></script>");
                strScript.Append("<script type='text/javascript'>");

                strScript.Append(" var dom = document.getElementById('" + div + "'); ");
                strScript.Append(" var myChart = echarts.init(dom); ");
                strScript.Append(@"

                            var app = {};
                            option = null;

                            option = {
                                title: {
                                    text: 'SMARTs Azimuth Audit/QC with Correction',
                                    subtext: 'Detail: Long-Collar vs Short-Collar Azimuth Analysis\nwww.smartwelldynamics.com',
                                    x: 'right'
                                },
                                tooltip: {
                                    trigger: 'axis',
                                    axisPointer: { animation: false }
                                },
                                legend: {
                                    data: [ 'Long-Collar Azimuth' , 'Short-Collar Azimuth'],
                                    x: 'left'
                                },
                                toolbox: {
                                    show: false
                                },
                                axisPointer: {
                                    link: { xAxisIndex: 'all' }
                                },
                                dataZoom: [
                                    {
                                        type: 'slider',
                                        realtime: true,
                                        start: 0,
                                        
                                    },
                                ],
                                grid: 
                                   {  left: 80, right: 80, top: '8%',  height: '70%' },
                                
                                xAxis: [
                                    {
                                        type: 'category',
                                        boundaryGap: false,
                                        axisLine: { onZero: true },
                                        axisTick: {
                                            alignWithLabel: true
                                        },
                                        axisPointer: {
                                            label: {
                                                formatter: function (params) {
                                                    return 'Measured Depth : ' + params.value;
                                                }
                                            }
                                        },
                                    ");

                strScript.Append(dataDepth + "}],");  // X Axis Graph Grid 1                
                strScript.Append(@"                                                                                                                                  
                                yAxis: [
                                    {
                                        name: 'Long-Collar Azimuth',
                                        type: 'value',
                                        inverse: true,
                                        min: 'dataMin',
                                        nameLocation: 'middle',
                                        nameTextStyle: { align: 'middle', verticalAlign: 'bottom', padding: [25, 25, 40, 25] }                                      
                                    },
                                ],
                                series: [
                                    {
                                        name: 'Long-Collar Azimuth',
                                        type: 'line',
                                        symbolSize: 6,
                                        yAxisIndex: 0,
                                        hoverAnimation: false,
                                     ");

                strScript.Append(dataLC + "},");  // Y Axis
                strScript.Append(@"
                                    
                                    {
                                        name: 'Short-Collar Azimuth',
                                        type: 'line',
                                        symbolSize: 6,
                                        yAxisIndex: 0,
                                        hoverAnimation: false,
                                    ");
                strScript.Append(dataSC + "},");  // Y Axis Graph Grid 1 
                strScript.Append(@"
                                    
                                ]
                            };
                        ");

                strScript.Append("  if (option && typeof option === \"object\" ) {");
                strScript.Append("     myChart.setOption(option, true); } ");

                strScript.Append("</script>");

                return strScript.ToString();
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }

        }

        public static String getChartsIncDipLongShortCorrAzimuth(String div, System.Data.DataTable ChartData)
        {
            try
            {
                StringBuilder strScript = new StringBuilder(), dataDepth = new StringBuilder(), dataLC = new StringBuilder(), dataSC = new StringBuilder(), dataCR = new StringBuilder();
                // Loading Data

                dataDepth.Append(" data: [");
                dataLC.Append(" data: [ ");
                dataSC.Append(" data: [ ");
                dataCR.Append(" data: [ ");
                foreach (DataRow row in ChartData.Rows)
                {
                    dataDepth.Append(Math.Round((Decimal)row["Depth"], 2) + ",");
                    dataLC.Append(Math.Round((Decimal)row["LCAzm"], 2) + ",");
                    dataSC.Append(Math.Round((Decimal)row["SCAzm"], 2) + ",");
                    dataCR.Append(Math.Round((Decimal)row["CrrAzm"], 2) + ",");
                }

                dataDepth.Append(" ], ");
                dataLC.Append("], ");
                dataSC.Append("], ");
                dataCR.Append("],");

                strScript.Append("<script type='text/javascript' src='../../eCharts/echarts.min.js'></script>");
                strScript.Append("<script type='text/javascript'>");

                strScript.Append(" var dom = document.getElementById('" + div + "'); ");
                strScript.Append(" var myChart = echarts.init(dom); ");
                strScript.Append(@"

                            var app = {};
                            option = null;

                            option = {
                                title: {
                                    text: 'SMARTs Azimuth Audit/QC with Correction',
                                    subtext: 'Detail: Long-Collar Azimuth vs Short-Collar Azimuth vs Correct Azimuth Analysis\nwww.smartwelldynamics.com',
                                    x: 'right'
                                },
                                tooltip: {
                                    trigger: 'axis',
                                    axisPointer: { animation: false }
                                },
                                legend: {
                                    data: [ 'Long-Collar Azimuth' , 'Short-Collar Azimuth', 'Corrected Azimuth'],
                                    x: 'left'
                                },
                                toolbox: {
                                    show: false
                                },
                                axisPointer: {
                                    link: { xAxisIndex: 'all' }
                                },
                                dataZoom: [
                                    {
                                        type: 'slider',
                                        realtime: true,
                                        start: 0,
                                        
                                    },
                                ],
                                grid: 
                                   {  left: 80, right: 80, top: '8%',  height: '70%' },
                                
                                xAxis: [
                                    {
                                        type: 'category',
                                        boundaryGap: false,
                                        axisLine: { onZero: true },
                                        axisTick: {
                                            alignWithLabel: true
                                        },
                                        axisPointer: {
                                            label: {
                                                formatter: function (params) {
                                                    return 'Measured Depth : ' + params.value;
                                                }
                                            }
                                        },
                                    ");

                strScript.Append(dataDepth + "}],");  // X Axis Graph Grid 1                
                strScript.Append(@"                                                                                                                                  
                                yAxis: [
                                    {
                                        name: 'Corrected Azimuth',
                                        type: 'value',
                                        inverse: true,
                                        min: 'dataMin',
                                        nameLocation: 'middle',
                                        nameTextStyle: { align: 'middle', verticalAlign: 'bottom', padding: [25, 25, 40, 25] }                                      
                                    },
                                ],
                                series: [
                                    {
                                        name: 'Corrected Azimuth',
                                        type: 'line',
                                        symbolSize: 6,
                                        yAxisIndex: 0,
                                        hoverAnimation: false,
                                     ");

                strScript.Append(dataCR + "},");  // Y Axis
                strScript.Append(@"
                                    
                                    {
                                        name: 'Long-Collar Azimuth',
                                        type: 'line',
                                        symbolSize: 6,
                                        yAxisIndex: 0,
                                        hoverAnimation: false,
                                    ");
                strScript.Append(dataLC + "},");  // Y Axis Graph Grid 1 
                strScript.Append(@"
                                    
                                    {
                                        name: 'Short-Collar Azimuth',
                                        type: 'line',
                                        symbolSize: 6,
                                        yAxisIndex: 0,
                                        hoverAnimation: false,
                                    ");
                strScript.Append(dataSC + "},");  // Y Axis Graph Grid 1 
                strScript.Append(@"
                                    
                                ]
                            };
                        ");

                strScript.Append("  if (option && typeof option === \"object\" ) {");
                strScript.Append("     myChart.setOption(option, true); } ");

                strScript.Append("</script>");

                return strScript.ToString();
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }

        }

        public static String getChartsPerformanceQS01(String div, System.Data.DataTable ChartData)
        {
            try
            {
                StringBuilder strScript = new StringBuilder();

                StringBuilder dataDepth = new StringBuilder();
                StringBuilder dataDip = new StringBuilder();
                StringBuilder dataBT = new StringBuilder();
                StringBuilder dataGT = new StringBuilder();
                // Loading Data

                dataDepth.Append(" [");
                dataDip.Append(" [ ");
                dataBT.Append(" [ ");
                dataGT.Append(" [ ");

                foreach (DataRow row in ChartData.Rows)
                {
                    dataDepth.Append(row["MDepth"] + ",");
                    dataDip.Append("[" + row["Y0"] + "," + row["Survey_number"] + "," + row["DipQualifier"] + "],");
                    dataBT.Append("[" + row["Y1"] + "," + row["Survey_number"] + "," + row["BTQualifier"] + "],");
                    dataGT.Append("[" + row["Y2"] + "," + row["Survey_number"] + "," + row["GTQualifier"] + "],");
                }

                dataDepth.Append(" ] ");
                dataDip.Append("]");
                dataBT.Append("]");
                dataGT.Append("]");



                strScript.Append("<script type='text/javascript' src='../../eCharts/echarts.min.js'></script>");
                strScript.Append("<script type='text/javascript'>");


                strScript.Append(" var dom = document.getElementById('" + div + "'); ");
                strScript.Append(" var myChart = echarts.init(dom); ");
                strScript.Append(@"

                            var app = {};
                            option = null;
                
                ");

                //  Appending the data

                strScript.Append("var Depth = " + dataDepth + "; "); 
                strScript.Append("var Dip  =  " + dataDip + "; ");
                strScript.Append("var BT = " + dataBT + "; ");
                strScript.Append("var GT = " + dataGT + "; ");


                // Script starts here

                strScript.Append(@"

			    Dip = Dip.map(function(item) {   return [item[1], item[0], item[2]]; });
			    BT = BT.map(function(item) {   return [item[1], item[0], item[2]]; });
			    GT = GT.map(function(item) {   return [item[1], item[0], item[2]]; });

		            option = {
				            title: {
				            text: 'Pass / Fail Score Report',
				            subtext: 'www.smartwelldynamics.com',
				            x: 'right',
				            right: 20
				            },			
							
					            visualMap: [
					            {
						            show: false,
						            min: 1,
						            max: 2,
						            calculable: true,
						            color: ['#D8BFD8', '#3CB371']    
					            }],
					
					                grid: [
						            { top: '15.00%', height: '15%' },
						            { top: '37.00%', height: '15%' },
						            { top: '59.00%', height: '15%' },

						            ],
						
				             dataZoom:[{
						            type:'slider',
						            xAxisIndex:[0,1,2],
						            realtime: true,
						            start: 0,
						            //end: 30,
						            top: 400,
						            bordercolor: '#dae',
						            showdetail: true,
						            handlesize: '20%',
						            }],	
						
				            tooltip: {
					            show: true,

					            position: 'top',
					            formatter: function(params) {
						            if (params.data[2] == '1') {
							            return params.seriesName + ': With in Limits !';
						            }
						            if (params.data[2] == '2') {
							            return params.seriesName + ': * Not with in Limits *';
						            }
					            }
				            },

				     toolbox: {
                        show: true, showTitle: true, itemGap: 20, orient: 'vertical', left: 'right',
                        top: '80',
						right: '120',
                        feature: {
                            restore: { show: true, title: 'Restore' },
                            saveAsImage: { show: true, title: 'Save' }
                        }
                    },

				            legend: {
					            data: [ 'B-Total', 'G-Total', 'Dip'],
					            //orient: 'vertical',
					            align: 'left',
					            top: 10,
					            left: 20,
					            x: 'left'
				            },
				

				            xAxis: [
					 
					            { type: 'category', gridIndex: 0, data: Depth , axislabel: {show: false }, axisTick: {show: false }, axisLine: {show: false }, splitLine: {show: false } ,
					            },
					
					            { type: 'category', gridIndex: 1, data: Depth , axislabel: {show: false }, axisTick: {show: false }, axisLine: {show: false }, splitLine: {show: false } ,
					            },
										
					            { type: 'category', gridIndex: 2, data: Depth, axislabel: {show: false },  axisTick: {show: false }, axisLine: {show: false } , splitLine:{show: false },						
					            },

				            ],
				            yAxis: [{
						            type: 'category',
						            gridIndex: 0,
						            splitNumber: 1,
						            name: 'B-Total',
						            nameLocation: 'middle',
						            nameGap: 20,
						            nameRotate: 0,
						            nameTextStyle: { fontWeight: 'bold' },           
						            axisLabel: { show: false }, axisLine: { show: false }, axisTick: {show: false }, splitLine:{show: false }
					            },
					            {
						            type: 'category',
						            gridIndex: 1,
						            splitNumber: 1,
						            name: 'G-Total',
						            nameLocation: 'middle',
						            nameGap: 20,
						            nameRotate: 0,
						            nameTextStyle: { fontWeight: 'bold' },
						            axisLabel: { show: false }, axisLine: { show: false }, axisTick: {show: false }, splitLine:{show: false }
					            },
					            {
						            type: 'category',
						            gridIndex: 2,
						            splitNumber: 1,
						            name: 'Dip',
						            nameLocation: 'middle',
						            nameGap: 20,
						            nameRotate: 0,
						            nameTextStyle: { fontWeight: 'bold' },
						            axisLabel: { show: false }, axisLine: { show: false }, axisTick: {show: false }, splitLine:{show: false }
					            },

				            ],
	
	
                series: [{
                        type: 'heatmap',
			            name: 'B-Total',
                        label: {
                            show: false,
                            formatter: function (params) { return parseInt(params.value[2]) }
                        },
                        xAxisIndex: '0',
                        yAxisIndex: '0',
                        data: BT		
			
                    },
                    {
                        type: 'heatmap',
			            name: 'G-Total',			
                        label: {
                            show: false,
                            formatter: function (params) { return parseInt(params.value[2]) }
                        },
                        xAxisIndex: 1,
                        yAxisIndex: 1,
                        data: GT	
			
                    },
                    {
                        type: 'heatmap',
			            name: 'Dip',			
                        label: {
                            show: false,
                            formatter: function (params) { return parseInt(params.value[2]) }
                        },
                        xAxisIndex: 2,
                        yAxisIndex: 2,
                        data: Dip
			
                    },

                ],

	
	            };


                        ");

                strScript.Append("  if (option && typeof option === \"object\" ) {");
                strScript.Append("     myChart.setOption(option, true); } ");
                strScript.Append("</script>");
                return strScript.ToString();
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());

            }


        }


        public static String getChartsPerformanceQS02(String div, System.Data.DataTable ChartData)
        {
            try
            {
                StringBuilder strScript = new StringBuilder();

                StringBuilder total = new StringBuilder();
                StringBuilder value = new StringBuilder();
                StringBuilder YAxis = new StringBuilder();
               
                
                // Loading Data

                value.Append(" [ ");
                YAxis.Append(" [ ");


                foreach (DataRow row in ChartData.Rows)
                {
                    total.Append(row["TOTAL"] + "");
                    value.Append("" + row["BTPass"] + "," + row["GTPass"] + "," + row["DipPass"] + "");
                    YAxis.Append("'" + row["btcount"] + "%','" + row["gtcount"] + "%','" + row["dipcount"] + "%'");
                    
                }

                value.Append("]");
                YAxis.Append("]");


                strScript.Append("<script type='text/javascript' src='../../eCharts/echarts.min.js'></script>");
                strScript.Append("<script type='text/javascript'>");


                strScript.Append(" var dom = document.getElementById('" + div + "'); ");
                strScript.Append(" var myChart = echarts.init(dom); ");
                strScript.Append(@"

                            var app = {};
                            option = null;
                
                ");

                //  Appending the data

                strScript.Append("var max = " + total + "; ");
                strScript.Append("var value  =  " + value + "; ");
                strScript.Append("var YAxis = " + YAxis + "; ");


                // Script starts here

                strScript.Append(@"


                        option = {
                            //backgroundColor: '#000A2A',
	
	                        title: {
		                        text: 'KPIs Success Rate',
		                        subtext: 'www.smartwelldynamics.com',
		                        x: 'right',
		                        right: 30,
		                        //subtextStyle: { color: '#900C3F' , fontWeight: 'bold'}
		                        },
	
                            grid:{
                                left:'10%',
                                right:'15%',
                                height: '40%',
                            },
                            xAxis: {
                                type: 'value',
                                max: max,
                                splitLine: {
                                    show: false
                                },
                                axisLine: {
                                    show: false
                                },
                                axisLabel: {
                                    show: false
                                },
                                axisTick: {
                                    show: false
                                }
                            },
                            yAxis: [{
                                type: 'category',
                                inverse: true,

                                data: ['B-Total','G-Total', 'Dip'],

                                data: ['B Total','G Total','Dip'],

                                axisLine: {
                                    show: false
                                },
                                axisTick: {
                                    show: false
                                },
                                axisLabel: {
                                    margin: 10,
                                    fontweight: 'bold',
                                    textStyle: {
                                        color: '#o3FF',
                                        fontSize: 10
                                    }
                                }
                            },{
                                type: 'category',
                                inverse: true,
                                data: YAxis,
                                axisLine: {
                                    show: false
                                },
                                axisTick: {
                                    show: false
                                },
                                axisLabel: {
                                    margin: 5,
                                    fontweight: 'bold',
                                    textStyle: {
                                        color: '#00FF',
                                        fontSize: 10
                                    }
                                }
                            }],
                            series: [{ 
                                    type: 'bar',
                                    barWidth: 40,
            
                                    legendHoverLink: false,
                                    silent: true,
                                    itemStyle: {
                                        color: {
                                            type: 'linear',
                                            x: 0,
                                            y: 0,
                                            x2: 1,
                                            y2: 0,
                                            colorStops: [{
                                                offset: 0,
                                                color: '#00DEFF' // 0%  #054996  #0078FF
                                            }, {
                                                offset: 1,
                                                color: '#054996' // 100%   #00DEFF
                                            }],
                                            globalCoord: false // false
                                        } 
                                    },
                                    data: value
                                },
                                { 
                                    type: 'bar',
                                    barWidth: 50,
                                    barGap: '-110%',
                                    label: {
                                        normal: {
                                            show: false,

                                            position: 'right',
                                            textStyle: {
                                                color: '#000'
                                            }
                                        }
                                    },
                                    itemStyle: {
                                        normal: {
                                            color: 'rgba(0,0,0,0)', //
                                            borderWidth: 2,
                                            borderColor: '#004E77'
                                        }
                                    },
                                    data: [max,max,max],
                                    z: 1,
                                },
                                { 
                                    type: 'pictorialBar',
                                    itemStyle: {
                                        color: '#000A2A'
                                    },
                                    symbolRepeat: 'fixed',
                                    symbolMargin: '10',
                                    symbol: 'rect',
                                    symbolClip: true,
                                    symbolSize: [4, 48],
                                    symbolPosition: 'start',
                                    symbolOffset: [
                                        0, -2
                                    ],
                                    symbolBoundingData: max,
                                    data: value
                                }
                            ]
                        };


                        ");

                strScript.Append("  if (option && typeof option === \"object\" ) {");
                strScript.Append("     myChart.setOption(option, true); } ");
                strScript.Append("</script>");
                return strScript.ToString();
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());

            }


        }

    }
}