﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Data;
using System.Collections.Generic;
using AF = SmartsVer1.Helpers.ChartHelpers.AddressFeatures;
using NJ = Newtonsoft.Json;

namespace SmartsVer1.Helpers.ChartHelpers
{
    public class MapBox
    {
        const String mapboxToken = "pk.eyJ1Ijoic21hcnR3ZWxsZHluYW1pY3MiLCJhIjoiY2p1azQ0NW5uMGVuejRhcXZuYXYwY2h3aiJ9.KaZugI6zditvnHdzZzNtWQ";

        void main()
        { }

        public static String getUserAssignedLocation(Int32 ClientID, System.Guid UserID, String ContainerDiv, String ClientDBConnection)
        {
            try
            {
                StringBuilder strScript = new StringBuilder();
                strScript.Append(@"<script>
                                    mapboxgl.accessToken = 'pk.eyJ1Ijoic21hcnR3ZWxsZHluYW1pY3MiLCJhIjoiY2p1azQ0NW5uMGVuejRhcXZuYXYwY2h3aiJ9.KaZugI6zditvnHdzZzNtWQ';
                                    var map = new mapboxgl.Map({
                                        container: " + ContainerDiv + "," +
                                        "style: 'mapbox://styles/mapbox/streets-v11', zoom: 16});" +
                                        "map.addControl(new mapboxgl.NavigationControl());</script>");
                return strScript.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getLeadPersonDetailMap(String ControlDivID, System.Data.DataTable addressTable)
        {
            try
            {
                System.Text.StringBuilder mapScript = new System.Text.StringBuilder();
                Decimal latitude = -99.00M, longitude = -99.00M;
                foreach (DataRow row in addressTable.Rows)
                {
                    latitude = Convert.ToDecimal(row["lati"]);
                    longitude = Convert.ToDecimal(row["longi"]);
                }
                mapScript.Append(@"<script>
                                    mapboxgl.accessToken = 'pk.eyJ1Ijoic21hcnR3ZWxsZHluYW1pY3MiLCJhIjoiY2p1azQ0NW5uMGVuejRhcXZuYXYwY2h3aiJ9.KaZugI6zditvnHdzZzNtWQ';
                                    var map = new mapboxgl.Map({
                                        container: " + ControlDivID + "," +
                                       "style: 'mapbox://styles/mapbox/streets-v11', center: [" + longitude + ", " + latitude + "], zoom: 16});" +
                                       "map.addControl(new mapboxgl.NavigationControl());</script>");
                return mapScript.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Double> getLatLngFromAddress(String StreetAddress)
        {
            try
            {
                List<Double> iReply = new List<Double>();

                String pathURL = "https://api.mapbox.com/geocoding/v5/mapbox.places/" + StreetAddress + ".json?access_token=" + mapboxToken;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(pathURL);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream resStream = response.GetResponseStream();

                var responseString = new StreamReader(resStream).ReadToEnd();

                AF.RootObject myOjb = NJ.JsonConvert.DeserializeObject<AF.RootObject>(responseString);
                List<Double> cords = new List<Double>();
                //List<Double> latlng = (longitude, latitude)
                foreach (var item in myOjb.features)
                {
                    iReply = item.geometry.coordinates;
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}