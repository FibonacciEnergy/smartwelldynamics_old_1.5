﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Net.Mail;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;

namespace SmartsVer1.Helpers.LogPrint
{
    public class LogComments
    {
        public LogComments()
        { // Empty constructor 
        }

        public string getLogComments(Int32 clntID, Int32 providerID , Int32 wellID , out String commenttime)
        {
            try
            {
                String message, msg , QueryString1, QueryString2;
                message = msg = QueryString1 = commenttime = null;
                DateTime time = DateTime.Now;


                QueryString1 = "select count(message) from LogComments where  clientID = '" + clntID + "' and providerID = '" + providerID + "' and wellID ='" + wellID + "' ";
                QueryString2 = "select message , datetime from LogComments where  clientID = '" + clntID + "' and providerID = '" + providerID + "' and wellID ='" + wellID + "' ";

                // ESTABLISHING CONNECTION
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["FELogPrint"].ToString());

                conn.Open();
                SqlCommand cmd = new SqlCommand(QueryString1, conn);
                int count = Convert.ToInt32(cmd.ExecuteScalar());
                conn.Close();

                if (count == 0)
                {
                    message = "";
                }
                else 
                {
                               
                    conn.Open();

                    SqlCommand cmdval = new SqlCommand(QueryString2, conn);
                    using (SqlDataReader clntRdr = cmdval.ExecuteReader())
                    {
                        while (clntRdr.Read())
                        {
                            msg = clntRdr.GetString(0);
                            time = clntRdr.GetDateTime(1);
                        }
                        clntRdr.Close();
                    }
                    conn.Close();
                    commenttime = time.ToString("yyyy-MM-dd hh:m:ss tt");
                    message = msg;
                    
                }

                return message;

            }
            catch(Exception ex)
            { 
                throw new System.Exception(ex.ToString()); }  
            
        }


        public void updateLogComments(int clntID, int providerID, int wellID, string inputmessage , out string time)
        {
            

            try
            {
                string message, QueryString1, insertQuery , updateQuery;
                message = QueryString1 = time = null;

                QueryString1 = "select count(message) from LogComments where  clientID = '" + clntID + "' and providerID = '" + providerID + "' and wellID='" + wellID + "' ";


                // ESTABLISHING CONNECTION
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["FELogPrint"].ToString());

                conn.Open();
                SqlCommand cmd = new SqlCommand(QueryString1, conn);
                int count = Convert.ToInt32(cmd.ExecuteScalar());
                conn.Close();

                if (count == 0)
                {
                    DateTime updateTime = DateTime.Now;
                    insertQuery = "insert into LogComments (clientID,providerID,wellID,datetime,message) Values ( @clntID , @providerID , @wellID , @updateTime  , @inputmessage)";

                    SqlCommand command = new SqlCommand(insertQuery, conn);

                    command.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value      = clntID.ToString();
                    command.Parameters.AddWithValue("@providerID", SqlDbType.Int).Value  = providerID.ToString();
                    command.Parameters.AddWithValue("@wellID", SqlDbType.Int).Value      =  wellID.ToString();
                    command.Parameters.AddWithValue("@updateTime", SqlDbType.DateTime2).Value = updateTime.ToString();
                    command.Parameters.AddWithValue("@inputmessage", SqlDbType.NVarChar).Value  =  inputmessage.ToString();

                    conn.Open();
                    command.ExecuteNonQuery();
                    conn.Close();

                    command.Dispose();

                    time = updateTime.ToString();
                }
                else
                {
                    DateTime updateTime = DateTime.Now;
                    updateQuery = "update LogComments SET datetime='" + updateTime + "' , message='" + inputmessage + "'  where  clientID = '" + clntID + "' and providerID = '" + providerID + "' and wellID='" + wellID + "' ";

                    SqlCommand command = new SqlCommand(updateQuery, conn);

                    command.Parameters.AddWithValue("@updateTime", SqlDbType.DateTime2).Value = updateTime.ToString();
                    command.Parameters.AddWithValue("@inputmessage", SqlDbType.NVarChar).Value = inputmessage.ToString();

                    conn.Open();
                    command.ExecuteNonQuery();
                    conn.Close();

                    command.Dispose();

                    time = updateTime.ToString();
                }


            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }

        }


        public void insertPrintOrder(int clientID, int wellID, string orderName, string fileSystemPath, string filesToPrint, string printStatus, string printComments, string deliveryAddress, string orderBy, string clientDomain, int delFlag)
        {


            try
            {
                string insertQuery;
                

                // ESTABLISHING CONNECTION
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["FELogPrint"].ToString());

                    DateTime updateTime = DateTime.Now;
                    insertQuery = "insert into LogPrintOrder (clientID,wellID,orderName,fileSystemPath,filesToPrint,printStatus,printComments,deliveryAddress,orderBy,clientDomain,datetime)";
                    insertQuery += "Values ( @clientID, @wellID, @orderName, @fileSystemPath, @filesToPrint, @printStatus, @printComments, @deliveryAddress, @orderBy,@clientDomain, @datetime)";

                    SqlCommand command = new SqlCommand(insertQuery, conn);

                    command.Parameters.AddWithValue("@clientID", SqlDbType.Int).Value = clientID.ToString();
                    command.Parameters.AddWithValue("@wellID", SqlDbType.Int).Value = wellID.ToString();
                    command.Parameters.AddWithValue("@orderName", SqlDbType.NVarChar).Value = orderName.ToString();
                    command.Parameters.AddWithValue("@fileSystemPath", SqlDbType.NVarChar).Value = fileSystemPath.ToString();
                    command.Parameters.AddWithValue("@filesToPrint", SqlDbType.NVarChar).Value = filesToPrint.ToString();
                    command.Parameters.AddWithValue("@printStatus", SqlDbType.NVarChar).Value = printStatus.ToString();
                    command.Parameters.AddWithValue("@printComments", SqlDbType.NVarChar).Value = printComments.ToString();
                    command.Parameters.AddWithValue("@deliveryAddress", SqlDbType.NVarChar).Value = deliveryAddress.ToString();
                    command.Parameters.AddWithValue("@orderBy", SqlDbType.NVarChar).Value = orderBy.ToString();
                    command.Parameters.AddWithValue("@clientDomain", SqlDbType.NVarChar).Value = clientDomain.ToString();
                    command.Parameters.AddWithValue("@datetime", SqlDbType.DateTime2).Value = updateTime.ToString();
                    
                    conn.Open();
                    command.ExecuteNonQuery();
                    conn.Close();

                    command.Dispose();
             

            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }

        }

        public static System.Data.DataTable getClientsOrders(Int32 ClientID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn clientID = iReply.Columns.Add("clientID", typeof(Int32));
                DataColumn wellID = iReply.Columns.Add("wellID", typeof(Int32));
                DataColumn orderName = iReply.Columns.Add("orderName", typeof(String));
                DataColumn fileSystemPath = iReply.Columns.Add("fileSystemPath", typeof(String));
                DataColumn filesToPrint = iReply.Columns.Add("filesToPrint", typeof(String));
                DataColumn printStatus = iReply.Columns.Add("printStatus", typeof(String));
                DataColumn printComments = iReply.Columns.Add("printComments", typeof(String));
                DataColumn deliveryAddress = iReply.Columns.Add("deliveryAddress", typeof(String));
                DataColumn orderBy = iReply.Columns.Add("orderBy", typeof(String));
                DataColumn datetime = iReply.Columns.Add("datetime", typeof(DateTime));

                String selData = "SELECT [clientID], [wellID], [orderName], [fileSystemPath], [filesToPrint], [printStatus], [printComments] , [deliveryAddress] , [orderBy] , [datetime] FROM [LogPrintOrder] WHERE [clientID] = @ClientID ORDER BY [datetime] DESC;";
                
                Int32 clientId = 0;
                Int32 wellid = 0;
                
                String ordername = String.Empty, filepath = String.Empty, fileprint = String.Empty, printstatus = String.Empty , comments = String.Empty , address = String.Empty, orderby = String.Empty ;
                
                DateTime uT = DateTime.Now;

                using (SqlConnection dataCon = new SqlConnection(ConfigurationManager.ConnectionStrings["FELogPrint"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ClientID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        clientId = readData.GetInt32(0);
                                        wellid = readData.GetInt32(1);
                                        ordername = readData.GetString(2);
                                        filepath = readData.GetString(3);
                                        fileprint = readData.GetString(4);
                                        printstatus = readData.GetString(5);
                                        comments = readData.GetString(6);
                                        address = readData.GetString(7);
                                        orderby = readData.GetString(8);
                                        uT = readData.GetDateTime(9);
                                        iReply.Rows.Add(clientId, wellid, ordername, filepath, fileprint, printstatus, comments , address , orderby, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        
        
        }


        public static System.Data.DataTable getAllClientsOrders()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn clientID = iReply.Columns.Add("clientID", typeof(Int32));
                DataColumn wellID = iReply.Columns.Add("wellID", typeof(Int32));
                DataColumn orderName = iReply.Columns.Add("orderName", typeof(String));
                DataColumn fileSystemPath = iReply.Columns.Add("fileSystemPath", typeof(String));
                DataColumn filesToPrint = iReply.Columns.Add("filesToPrint", typeof(String));
                DataColumn printStatus = iReply.Columns.Add("printStatus", typeof(String));
                DataColumn printComments = iReply.Columns.Add("printComments", typeof(String));
                DataColumn deliveryAddress = iReply.Columns.Add("deliveryAddress", typeof(String));
                DataColumn orderBy = iReply.Columns.Add("orderBy", typeof(String));
                DataColumn clientDomain = iReply.Columns.Add("clientDomain", typeof(String));
                DataColumn datetime = iReply.Columns.Add("datetime", typeof(DateTime));

                String selData = "SELECT [clientID], [wellID], [orderName], [fileSystemPath], [filesToPrint], [printStatus], [printComments] , [deliveryAddress] , [orderBy] , [clientDomain],  [datetime] FROM [LogPrintOrder]  ";
                selData += " ORDER BY [datetime] desc";
                //String selData = "SELECT [clientID], [wellID], [orderName], [fileSystemPath], [filesToPrint], [printStatus], [printComments] , [deliveryAddress] , [orderBy] , [clientDomain],  [datetime] , [delFlag] FROM [LogPrintOrder]  ";
                //selData += " WHERE [delFlag] = 0 ORDER BY [datetime] desc";

                Int32 clientId = 0;
                Int32 wellid = 0;

                String ordername = String.Empty, filepath = String.Empty, fileprint = String.Empty, printstatus = String.Empty, comments = String.Empty, address = String.Empty, orderby = String.Empty , clientdomain = String.Empty;

                DateTime uT = DateTime.Now;

                using (SqlConnection dataCon = new SqlConnection(ConfigurationManager.ConnectionStrings["FELogPrint"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        clientId = readData.GetInt32(0);
                                        wellid = readData.GetInt32(1);
                                        ordername = readData.GetString(2);
                                        filepath = readData.GetString(3);
                                        fileprint = readData.GetString(4);
                                        printstatus = readData.GetString(5);
                                        comments = readData.GetString(6);
                                        address = readData.GetString(7);
                                        orderby = readData.GetString(8);
                                        clientdomain = readData.GetString(9);
                                        uT = readData.GetDateTime(10);
                                        iReply.Rows.Add(clientId, wellid, ordername, filepath, fileprint, printstatus, comments, address, orderby, clientdomain, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }


        }


        public static System.Data.DataTable getOrderCounts(Int32 ClientID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();

                DataColumn TotalCount = iReply.Columns.Add("totalcount", typeof(Int32));
                DataColumn Submitted = iReply.Columns.Add("submitted", typeof(Int32));
                DataColumn Inprogress = iReply.Columns.Add("inprogress", typeof(Int32));
                DataColumn Complete = iReply.Columns.Add("complete", typeof(Int32));

                String selData = "SELECT ";
                selData += "( select count(clientID)  FROM LogPrintOrder WHERE ( clientID = @ClientID ) ) as totalcount, ";
                selData += "( select count(clientID)  FROM LogPrintOrder WHERE ( clientID = @ClientID ) and printStatus = 'SUBMITTED' ) as submitted ,";
                selData += "( select count(clientID)  FROM LogPrintOrder WHERE ( clientID = @ClientID ) and printStatus = 'IN PROGRESS' ) as inprogress ,";
                selData += "( select count(clientID)  FROM LogPrintOrder WHERE ( clientID = @ClientID ) and printStatus = 'COMPLETE' ) as complete; ";
                //String selData = "SELECT ";
                //selData += "( select count(clientID)  FROM LogPrintOrder WHERE ( clientID = @ClientID ) and [delFlag] = 0  ) as totalcount, ";
                //selData += "( select count(clientID)  FROM LogPrintOrder WHERE ( clientID = @ClientID ) and printStatus = 'SUBMITTED' and [delFlag] = 0 ) as submitted ,";
                //selData += "( select count(clientID)  FROM LogPrintOrder WHERE ( clientID = @ClientID ) and printStatus = 'IN PROGRESS' and [delFlag] = 0 ) as inprogress ,";
                //selData += "( select count(clientID)  FROM LogPrintOrder WHERE ( clientID = @ClientID ) and printStatus = 'COMPLETE' and [delFlag] = 0 ) as complete; ";
                
                Int32 totalorders , submitted , inprogress , complete = 0;
               

                using (SqlConnection dataCon = new SqlConnection(ConfigurationManager.ConnectionStrings["FELogPrint"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ClientID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        totalorders = readData.GetInt32(0);
                                        submitted = readData.GetInt32(1);
                                        inprogress = readData.GetInt32(2);
                                        complete = readData.GetInt32(3);
                                        iReply.Rows.Add(totalorders, submitted, inprogress, complete);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        
        }


        public static System.Data.DataTable getAllOrderCounts()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();

                DataColumn TotalCount = iReply.Columns.Add("totalcount", typeof(Int32));
                DataColumn Submitted = iReply.Columns.Add("submitted", typeof(Int32));
                DataColumn Inprogress = iReply.Columns.Add("inprogress", typeof(Int32));
                DataColumn Complete = iReply.Columns.Add("complete", typeof(Int32));

                String selData = "SELECT ";
                selData += "( select count(clientID)  FROM LogPrintOrder  ) as totalcount, ";
                selData += "( select count(clientID)  FROM LogPrintOrder WHERE  printStatus = 'SUBMITTED' ) as submitted ,";
                selData += "( select count(clientID)  FROM LogPrintOrder WHERE  printStatus = 'IN PROGRESS' ) as inprogress ,";
                selData += "( select count(clientID)  FROM LogPrintOrder WHERE  printStatus = 'COMPLETE' ) as complete; ";
                //String selData = "SELECT ";
                //selData += "( select count(clientID)  FROM LogPrintOrder WHERE  delFlag = 0 ) as totalcount, ";
                //selData += "( select count(clientID)  FROM LogPrintOrder WHERE  printStatus = 'SUBMITTED' and delFlag = 0 ) as submitted ,";
                //selData += "( select count(clientID)  FROM LogPrintOrder WHERE  printStatus = 'IN PROGRESS' and delFlag = 0 ) as inprogress ,";
                //selData += "( select count(clientID)  FROM LogPrintOrder WHERE  printStatus = 'COMPLETE' and delFlag = 0 ) as complete; ";

                Int32 totalorders, submitted, inprogress, complete = 0;


                using (SqlConnection dataCon = new SqlConnection(ConfigurationManager.ConnectionStrings["FELogPrint"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        totalorders = readData.GetInt32(0);
                                        submitted = readData.GetInt32(1);
                                        inprogress = readData.GetInt32(2);
                                        complete = readData.GetInt32(3);
                                        iReply.Rows.Add(totalorders, submitted, inprogress, complete);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }

        }


        public static String getOrderComments(String orderID)
        {
            String message = String.Empty;
            try
            {
                String QueryString = "SELECT [printComments] FROM [LogPrintOrder] WHERE [orderName] Like '" + orderID + "' ";

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["FELogPrint"].ToString()))
                {
                    try
                    {
                        conn.Open();

                        SqlCommand cmdval = new SqlCommand(QueryString, conn);
                        using (SqlDataReader clntRdr = cmdval.ExecuteReader())
                        {
                            while (clntRdr.Read())
                            {
                                message = clntRdr.GetString(0);
                            }
                            clntRdr.Close();
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }
                return message;
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public void updatePrintStatus(String orderID , String status)
        {
            try
            {
                String QueryString = "UPDATE [LogPrintOrder] SET [printStatus] = '" + status + "' WHERE [orderName] like '" + orderID + "' ";

                // ESTABLISHING CONNECTION
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["FELogPrint"].ToString()))
                {
                    try
                    {
                        SqlCommand command = new SqlCommand(QueryString, conn);
                        command.Parameters.AddWithValue("@status", SqlDbType.DateTime2).Value = status.ToString();
                        command.Parameters.AddWithValue("@orderID", SqlDbType.NVarChar).Value = orderID.ToString();
                        conn.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }                
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }        
        }

        public void updateDeleteStatus(String orderID)
        {
            try
            {
                String QueryString = "UPDATE [LogPrintOrder] SET [delFlag] = 1 WHERE [orderName] LIKE '" + orderID + "' ";
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["FELogPrint"].ToString()))
                {
                    try
                    {
                        SqlCommand command = new SqlCommand(QueryString, conn);
                        command.Parameters.AddWithValue("@orderID", SqlDbType.NVarChar).Value = orderID.ToString();
                        conn.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }
    }
}