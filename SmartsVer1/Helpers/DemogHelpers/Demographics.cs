﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;
using Microsoft.SqlServer.Types;
using System.Web.Configuration;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using ACC = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using SRV = SmartsVer1.Helpers.DemogHelpers.ServicesConfig;
using SM = SmartsVer1.Helpers.SupportHelpers.SRequest;
using JS = Newtonsoft.Json;

/// <summary>
/// Methods to manipulate SMARTs Demographics (and related) Data
/// </summary>

namespace SmartsVer1.Helpers.DemogHelpers
{
    public class Demographics
    {
        static readonly String d1 = Convert.ToString(WebConfigurationManager.AppSettings["Dev1"].ToString());
        static readonly String d2 = Convert.ToString(WebConfigurationManager.AppSettings["Dev2"].ToString());
        static readonly String d3 = Convert.ToString(WebConfigurationManager.AppSettings["Dev3"].ToString());

        static void Main()
        { }

        public static String getJSonString(System.Data.DataTable infoTable)
        {
            try
            {
                String JSONString = String.Empty;
                JSONString = JS.JsonConvert.SerializeObject(infoTable);
                return JSONString;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addBTQ(String BTQStatement, String UserName, String Realm)
        {
            try
            {
                Int32 iReply = -99;
                if (UserName.Equals(d1) || UserName.Equals(d2) || UserName.Equals(d3))
                {
                    String insData = "INSERT INTO [dmgBTQualifier] ([btqValue]) VALUES (@btqValue)";
                    using (SqlConnection insCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                    {
                        try
                        {
                            insCon.Open();
                            SqlCommand addData = new SqlCommand(insData, insCon);
                            addData.Parameters.AddWithValue("@btqValue", SqlDbType.NVarChar).Value = BTQStatement.ToString();
                            iReply = Convert.ToInt32(addData.ExecuteNonQuery());
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            insCon.Close();
                            insCon.Dispose();
                        }
                    }                    
                }
                else
                {
                    iReply = 2; //Not Authorized
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addColorType(String ColorTypeStatement, String UserName, String Realm)
        {
            try
            {
                Int32 iReply = -99;
                if (UserName.Equals(d1) || UserName.Equals(d2) || UserName.Equals(d3))
                {
                    String insData = "INSERT INTO [dmgColorType] ([lclrValue]) VALUES (@lclrValue)";
                    using (SqlConnection insCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                    {
                        try
                        {
                            insCon.Open();
                            SqlCommand addData = new SqlCommand(insData, insCon);
                            addData.Parameters.AddWithValue("@lclrValue", SqlDbType.NVarChar).Value = ColorTypeStatement.ToString();
                            iReply = Convert.ToInt32(addData.ExecuteNonQuery());
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            insCon.Close();
                            insCon.Dispose();
                        }
                    }
                }
                else
                {
                    iReply = 2; //Not Authorized
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addDataEntryType(String DataEntryTypeStatement, String UserName, String Realm)
        {
            try
            {
                Int32 iReply = -99;
                if (UserName.Equals(d1) || UserName.Equals(d2) || UserName.Equals(d3))
                {
                    String insData = "INSERT INTO [dmgDataEntryMethod] ([demName]) VALUES (@demName)";
                    using (SqlConnection insCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                    {
                        try
                        {
                            insCon.Open();
                            SqlCommand addData = new SqlCommand(insData, insCon);
                            addData.Parameters.AddWithValue("@demName", SqlDbType.NVarChar).Value = DataEntryTypeStatement.ToString();
                            iReply = Convert.ToInt32(addData.ExecuteNonQuery());
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            insCon.Close();
                            insCon.Dispose();
                        }
                    }
                }
                else
                {
                    iReply = 2; //Not Authorized
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addDemogType(String DemogTypeStatement, String UserName, String Realm)
        {
            try
            {
                Int32 iReply = -99;
                if (UserName.Equals(d1) || UserName.Equals(d2) || UserName.Equals(d3))
                {
                    String insData = "INSERT INTO [dmgDemogType] ([dmgName]) VALUES (@dmgName)";
                    using (SqlConnection insCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                    {
                        try
                        {
                            insCon.Open();
                            SqlCommand addData = new SqlCommand(insData, insCon);
                            addData.Parameters.AddWithValue("@dmgName", SqlDbType.NVarChar).Value = DemogTypeStatement.ToString();
                            iReply = Convert.ToInt32(addData.ExecuteNonQuery());
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            insCon.Close();
                            insCon.Dispose();
                        }
                    }
                }
                else
                {
                    iReply = 2; //Not Authorized
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addDisclaimerText(String DisclaimerText, Int32 DisclaimerType)
        {
            try
            {
                Int32 iReply = -99;
                {
                    String insData = "INSERT INTO [dmgDisclaimer] ([dscValue], [dsctID], [cTime], [uTime]) VALUES (@dscValue, @dscID, @cTime, @uTime)";
                    using (SqlConnection insCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                    {
                        try
                        {
                            insCon.Open();
                            SqlCommand addData = new SqlCommand(insData, insCon);
                            addData.Parameters.AddWithValue("@dscValue", SqlDbType.NVarChar).Value = DisclaimerText.ToString();
                            addData.Parameters.AddWithValue("@dscID", SqlDbType.NVarChar).Value = DisclaimerType.ToString();
                            addData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                            addData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                            iReply = Convert.ToInt32(addData.ExecuteNonQuery());
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            insCon.Close();
                            insCon.Dispose();
                        }
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addDisclaimerType(String DisclaimerTypeValue)
        {
            try
            {
                Int32 iReply = -99;
                {
                    String insData = "INSERT INTO [dmgDisclaimerType] ([dsctValue], [cTime], [uTime]) VALUES (@dsctValue, @cTime, @uTime)";
                    using (SqlConnection insCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                    {
                        try
                        {
                            insCon.Open();
                            SqlCommand addData = new SqlCommand(insData, insCon);
                            addData.Parameters.AddWithValue("@dsctValue", SqlDbType.NVarChar).Value = DisclaimerTypeValue.ToString();
                            addData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                            addData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                            iReply = Convert.ToInt32(addData.ExecuteNonQuery());
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            insCon.Close();
                            insCon.Dispose();
                        }
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addDipQ(String DipQStatement, String UserName, String Realm)
        {
            try
            {
                Int32 iReply = -99;
                if (UserName.Equals(d1) || UserName.Equals(d2) || UserName.Equals(d3))
                {
                    String insData = "INSERT INTO [dmgDipQualifier] ([dpqValue]) VALUES (@dpqValue)";
                    using (SqlConnection insCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                    {
                        try
                        {
                            insCon.Open();
                            SqlCommand addData = new SqlCommand(insData, insCon);
                            addData.Parameters.AddWithValue("@dpqValue", SqlDbType.NVarChar).Value = DipQStatement.ToString();
                            iReply = Convert.ToInt32(addData.ExecuteNonQuery());
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            insCon.Close();
                            insCon.Dispose();
                        }
                    }
                }
                else
                {
                    iReply = 2; //Not Authorized
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addGTQ(String GTQStatement, String UserName, String Realm)
        {
            try
            {
                Int32 iReply = -99;
                if (UserName.Equals(d1) || UserName.Equals(d2) || UserName.Equals(d3))
                {
                    String insData = "INSERT INTO [dmgGTQualifier] ([gtqValue]) VALUES (@gtqValue)";
                    using (SqlConnection insCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                    {
                        try
                        {
                            insCon.Open();
                            SqlCommand addData = new SqlCommand(insData, insCon);
                            addData.Parameters.AddWithValue("@gtqValue", SqlDbType.NVarChar).Value = GTQStatement.ToString();
                            iReply = Convert.ToInt32(addData.ExecuteNonQuery());
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            insCon.Close();
                            insCon.Dispose();
                        }
                    }
                }
                else
                {
                    iReply = 2; //Not Authorized
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addImagePurpose(String ImagePurpose)
        {
            try
            {
                Int32 iResult = -99;
                String insertData = "INSERT INTO [dmgImagePurpose] ([imgpValue]) VALUES (@imgpValue)";
                using (SqlConnection insCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        insCon.Open();
                        SqlCommand insData = new SqlCommand(insertData, insCon);
                        insData.Parameters.AddWithValue("@imgpValue", SqlDbType.NVarChar).Value = ImagePurpose.ToString();

                        iResult = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        insCon.Close();
                        insCon.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addCity(String CityName, Int32 cntyCode, String UserName, String Realm)
        {
            try
            {
                Int32 iResult = -99;
                String insertData = "INSERT INTO dmgCity ([cityName], [cntyID], [cTime], [uTime], [username], [realm]) VALUES (@cityName, @cntyID, @cTime, @uTime, @username, @realm)";
                using (SqlConnection insCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        insCon.Open();
                        SqlCommand insData = new SqlCommand(insertData, insCon);
                        insData.Parameters.AddWithValue("@cityName", SqlDbType.NVarChar).Value = CityName.ToString();
                        insData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = cntyCode.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = Realm.ToString();

                        iResult = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        insCon.Close();
                        insCon.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addClientPhoneToAddress(Int32 ClientAddressID, String PhoneNumber, Int32 PhoneType, String UserName, String Realm)
        {
            try
            {
                Int32 iReply = -99;
                String insertData = "INSERT INTO [ClientPhoneToAddress] ([ptaValue], [ptaType], [clntAddID], [username], [domain], [cTime], [uTime]) VALUES (@ptaValue, @ptaType, @clntAddID, @username, @realm, @cTime, @uTime)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(insertData, dataCon);
                        insData.Parameters.AddWithValue("@ptaValue", SqlDbType.NVarChar).Value = PhoneNumber.ToString();
                        insData.Parameters.AddWithValue("@ptaType", SqlDbType.Int).Value = PhoneType.ToString();
                        insData.Parameters.AddWithValue("@clntAddID", SqlDbType.Int).Value = ClientAddressID.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = Realm.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime).Value = DateTime.Now.ToString();
                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static Int32 addCounty(String CountyName, String CountyCode, String CountyFIPS, Int32 StateID, String UserName, String Realm)
        {
            try
            {
                Int32 iResult = -99;
                String insertData = "INSERT INTO dmgCountyDistrict ([cntyName], [cntyCode], [cntyFIPS], [stID], [cTime], [uTime], [username], [realm]) VALUES (@cntyName, @cntyCode, @cntyFIPS, @stID, @cTime, @uTime, @username, @realm)";
                using (SqlConnection insCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        insCon.Open();
                        SqlCommand insData = new SqlCommand(insertData, insCon);
                        insData.Parameters.AddWithValue("@cntyName", SqlDbType.NVarChar).Value = CountyName.ToString();
                        insData.Parameters.AddWithValue("@cntyCode", SqlDbType.NVarChar).Value = CountyCode.ToString();
                        insData.Parameters.AddWithValue("@cntyFIPS", SqlDbType.NVarChar).Value = CountyFIPS.ToString();
                        insData.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = StateID.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = Realm.ToString();

                        iResult = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        insCon.Close();
                        insCon.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static Int32 addJobClass(String JobClassStatement, String UserName, String Realm)
        {
            try
            {
                Int32 iReply = -99;
                if (UserName.Equals(d1) || UserName.Equals(d2) || UserName.Equals(d3))
                {
                    String insData = "INSERT INTO [dmgJobClass] ([jclName], [cTime], [uTime], [username], [realm]) VALUES (@jclName, @cTime, @uTime, @UserName, @UserRealm)";
                    using (SqlConnection insCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                    {
                        try
                        {
                            insCon.Open();
                            SqlCommand addData = new SqlCommand(insData, insCon);
                            addData.Parameters.AddWithValue("@jclName", SqlDbType.NVarChar).Value = JobClassStatement.ToString();
                            addData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now;
                            addData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now;
                            addData.Parameters.AddWithValue("@UserName", SqlDbType.NVarChar).Value = UserName.ToString();
                            addData.Parameters.AddWithValue("@UserRealm", SqlDbType.NVarChar).Value = Realm.ToString();
                            iReply = Convert.ToInt32(addData.ExecuteNonQuery());
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            insCon.Close();
                            insCon.Dispose();
                        }
                    }
                }
                else
                {
                    iReply = 2; //Not Authorized
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addOprStatus(String OprStatusStatement, String UserName, String Realm)
        {
            try
            {
                Int32 iReply = -99;
                if (UserName.Equals(d1) || UserName.Equals(d2) || UserName.Equals(d3))
                {
                    String insData = "INSERT INTO [dmgOprStatus] ([osValue]) VALUES (@osValue)";
                    using (SqlConnection insCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                    {
                        try
                        {
                            insCon.Open();
                            SqlCommand addData = new SqlCommand(insData, insCon);
                            addData.Parameters.AddWithValue("@osValue", SqlDbType.NVarChar).Value = OprStatusStatement.ToString();
                            iReply = Convert.ToInt32(addData.ExecuteNonQuery());
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            insCon.Close();
                            insCon.Dispose();
                        }
                    }
                }
                else
                {
                    iReply = 2; //Not Authorized
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addStatus(String StatusStatement, String UserName, String Realm)
        {
            try
            {
                Int32 iReply = -99;
                if (UserName.Equals(d1) || UserName.Equals(d2) || UserName.Equals(d3))
                {
                    String insData = "INSERT INTO [dmgStatus] ([dmgStatus]) VALUES (@dmgStatus)";
                    using (SqlConnection insCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                    {
                        try
                        {
                            insCon.Open();
                            SqlCommand addData = new SqlCommand(insData, insCon);
                            addData.Parameters.AddWithValue("@dmgStatus", SqlDbType.NVarChar).Value = StatusStatement.ToString();
                            iReply = Convert.ToInt32(addData.ExecuteNonQuery());
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            insCon.Close();
                            insCon.Dispose();
                        }
                    }
                }
                else
                {
                    iReply = 2; //Not Authorized
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static Int32 addCountry(String CountryName, String CountryCode, String CountryDialCode, Int32 GeoSubRegionID, String UserName, String Realm)
        {
            try
            {
                Int32 iResult = -99;
                String insertData = "INSERT INTO dmgCountry ([ctyName], [ctyCode], [ctyDialCode], [gsrID], [cTime], [uTime], [username], [realm]) VALUES (@ctyName, @ctyCode, @ctyDialCode, @gsrID, @cTime, @uTime, @username, @realm)";
                using (SqlConnection insCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        insCon.Open();
                        SqlCommand insData = new SqlCommand(insertData, insCon);
                        insData.Parameters.AddWithValue("@ctyName", SqlDbType.NVarChar).Value = CountryName.ToString();
                        insData.Parameters.AddWithValue("@ctyCode", SqlDbType.NVarChar).Value = CountryCode.ToString();
                        insData.Parameters.AddWithValue("@ctyDialCode", SqlDbType.NVarChar).Value = CountryDialCode.ToString();
                        insData.Parameters.AddWithValue("@gsrID", SqlDbType.Int).Value = GeoSubRegionID.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = Realm.ToString();

                        iResult = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        insCon.Close();
                        insCon.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addField(Int32 StateID, Int32 CountyID, String FieldName, String FieldCode, String FieldYear, Int32 FOil, Int32 FGas, Int32 FAsc, Int32 FUnk, String UserName, String Realm)
        {
            try
            {
                Int32 iResult = -99;
                String insertData = "INSERT INTO dmgField ([fldName], [stID], [cntyID], [fldCode], [hcOil], [hcGas], [hcAssoc], [hcUnknown], [Year], [cTime], [uTime], [username], [realm]) VALUES (@fldName, @stID, @cntyID, @fldCode, @hcOil, @hcGas, @hcAssoc, @hcUnknown, @Year, @cTime, @uTime, @username, @realm)";
                using (SqlConnection insCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        insCon.Open();
                        SqlCommand insData = new SqlCommand(insertData, insCon);

                        insData.Parameters.AddWithValue("@fldName", SqlDbType.NVarChar).Value = FieldName.ToString();
                        insData.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = StateID.ToString();
                        insData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                        insData.Parameters.AddWithValue("@fldCode", SqlDbType.NVarChar).Value = FieldCode.ToString();
                        insData.Parameters.AddWithValue("@hcOil", SqlDbType.Int).Value = FOil.ToString();
                        insData.Parameters.AddWithValue("@hcGas", SqlDbType.Int).Value = FGas.ToString();
                        insData.Parameters.AddWithValue("@hcAssoc", SqlDbType.Int).Value = FAsc.ToString();
                        insData.Parameters.AddWithValue("@hcUnknown", SqlDbType.Int).Value = FUnk.ToString();
                        insData.Parameters.AddWithValue("@Year", SqlDbType.NVarChar).Value = FieldYear.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = Realm.ToString();

                        iResult = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        insCon.Close();
                        insCon.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addGSRegion(Int32 MRegionID, String SRegionName, String UserName, String ClientRealm)
        {
            try
            {
                Int32 iResult = -99;
                String insertData = "INSERT INTO dmgGeoSubRegion ([gsrName], [grID], [cTime], [uTime], [username], [realm]) VALUES (@gsrName, @grID, @cTime, @uTime, @username, @realm)";
                using (SqlConnection insCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        insCon.Open();
                        SqlCommand insData = new SqlCommand(insertData, insCon);
                        insData.Parameters.AddWithValue("@gsrName", SqlDbType.NVarChar).Value = SRegionName.ToString();
                        insData.Parameters.AddWithValue("@grID", SqlDbType.Int).Value = MRegionID.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = ClientRealm.ToString();

                        iResult = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        insCon.Close();
                        insCon.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addMajorGRegion(String RegionName, String UserName, String ClientRealm)
        {
            try
            {
                Int32 iResult = -99;                
                String insertData = "INSERT INTO dmgGeoRegion ([grName], [cTime], [uTime], [username], [realm]) VALUES (@grName, @cTime, @uTime, @username, @realm)";
                using (SqlConnection insCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        insCon.Open();
                        SqlCommand insData = new SqlCommand(insertData, insCon);
                        insData.Parameters.AddWithValue("@grName", SqlDbType.NVarChar).Value = RegionName.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = ClientRealm.ToString();

                        iResult = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        insCon.Close();
                        insCon.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        

        public static Int32 addState(String StateName, String StateCode, String StateAPI, String StateANSI, String StateFIPS, Int32 CountryID, String UserName, String Realm)
        {
            try
            {
                Int32 iResult = -99;
                String insertData = "INSERT INTO dmgState ([stName], [stCode], [stAPI], [stANSI], [stFIPS], [ctyID], [cTime], [uTime], [username], [realm]) VALUES (@stName, @stCode, @stAPI, @stANSI, @stFIPS, @ctyID, @cTime, @uTime, @username, @realm)";
                using (SqlConnection insCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        insCon.Open();
                        SqlCommand insData = new SqlCommand(insertData, insCon);
                        insData.Parameters.AddWithValue("@stName", SqlDbType.NVarChar).Value = StateName.ToString();
                        insData.Parameters.AddWithValue("@stCode", SqlDbType.NVarChar).Value = StateCode.ToString();
                        insData.Parameters.AddWithValue("@stAPI", SqlDbType.NVarChar).Value = StateAPI.ToString();
                        insData.Parameters.AddWithValue("@stFIPS", SqlDbType.NVarChar).Value = StateFIPS.ToString();
                        insData.Parameters.AddWithValue("@stANSI", SqlDbType.NVarChar).Value = StateANSI.ToString();
                        insData.Parameters.AddWithValue("@ctyID", SqlDbType.Int).Value = CountryID.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = Realm.ToString();

                        iResult = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        insCon.Close();
                        insCon.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addWellSection(String WellSectionName, String UserName, String UserRealm, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                Int32 findD = findDuplicateWellSectionName(WellSectionName, ClientDBConnection);
                if (findD.Equals(0))
                {
                    String insData = "INSERT INTO [WellSection] ([wsName], [cTime], [uTime], [username], [realm]) VALUES (@wsName, @cTime, @uTime, @username, @realm)";
                    using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                    {
                        try
                        {
                            dataCon.Open();
                            SqlCommand addData = new SqlCommand(insData, dataCon);
                            addData.Parameters.AddWithValue("@wsName", SqlDbType.NVarChar).Value = WellSectionName.ToString();
                            addData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                            addData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                            addData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                            addData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserRealm.ToString();
                            iReply = Convert.ToInt32(addData.ExecuteNonQuery());
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            dataCon.Close();
                            dataCon.Dispose();
                        }
                    }
                }
                else
                {
                    iReply = -1;
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addWellSectionToWell(Int32 WellSectionID, Int32 WellID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String insData = "INSERT INTO [WellSectionToWell] ([wsID], [welID], [cTime], [uTime]) VALUES (@wsID, @welID, @cTime, @uTime)";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insCmd = new SqlCommand(insData, dataCon);
                        insCmd.Parameters.AddWithValue("@wsID", SqlDbType.Int).Value = WellSectionID.ToString();
                        insCmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        insCmd.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insCmd.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        iReply = Convert.ToInt32(insCmd.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addZip(String ZipValue, Int32 cntyCode, Int32 datCode, String Latitude, String Longitude,String UserName, String Realm)
        {
            try
            {
                Int32 iResult = -99;
                String insertData = "INSERT INTO dmgZip ([zipCode], [cntyID], [Latitude], [Longitude], [Location], [datID], [cTime], [uTime], [username], [realm]) VALUES (@zipCode, @cntyID, @Latitude, @Longitude, @Location, @datID, @cTime, @uTime, @username, @realm)";
                using (SqlConnection insCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        insCon.Open();
                        SqlCommand insData = new SqlCommand(insertData, insCon);

                        Decimal zLat = -99.000000M;
                        Decimal zLon = -99.000000M;
                        zLat = Convert.ToDecimal(Latitude);
                        zLon = Convert.ToDecimal(Longitude);

                        SqlGeography zLoc = SqlGeography.Point((Double)zLat, (Double)zLon, 4326);
                        //DbGeography zLoc = DbGeography.PointFromText("POINT (" + zLon + " " + zLat + ")", 4326);
                        
                        insData.Parameters.AddWithValue("@zipCode", SqlDbType.NVarChar).Value = ZipValue.ToString();
                        insData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = cntyCode.ToString();
                        insData.Parameters.AddWithValue("@Latitude", SqlDbType.Decimal).Value = zLat.ToString();
                        insData.Parameters.AddWithValue("@Longitude", SqlDbType.Decimal).Value = zLon.ToString();
                        insData.Parameters.AddWithValue("@Location", SqlDbType.Decimal).Value = zLoc.ToString();
                        insData.Parameters.AddWithValue("@datID", SqlDbType.Int).Value = datCode.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = Realm.ToString();

                        iResult = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        insCon.Close();
                        insCon.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 findDuplicateWellSectionName(String WellSectionName, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(wsID) FROM [WellSection] WHERE [wsName] = @wsName";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wsName", SqlDbType.NVarChar).Value = WellSectionName.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    iReply = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getAzimuthCriteriaValueFromID(Int32 AzimuthCriteriaID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [acValue] FROM [dmgAzimuthCriteria] WHERE [acID] = @acID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@acID", SqlDbType.Int).Value = AzimuthCriteriaID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    iReply = readData.GetString(0);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getAzimuthCriteriaIDFromValue(String AzimuthCriteriaValue)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT [acID] FROM [dmgAzimuthCriteria] WHERE [acValue] = @acValue";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@acValue", SqlDbType.NVarChar).Value = AzimuthCriteriaValue.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    iReply = readData.GetInt32(0);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getAzimuthCriteriaList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99; String value = String.Empty;
                String selData = "SELECT [acID], [acValue] FROM [dmgAzimuthCriteria] ORDER BY [acValue] ASC";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    id = readData.GetInt32(0);
                                    value = readData.GetString(1);
                                    iReply.Add(id, value);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getClientPhoneToAddressTable(Int32 ClientAddressID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn ptaID = iReply.Columns.Add("ptaID", typeof(Int32));
                DataColumn ptaValue = iReply.Columns.Add("ptaValue", typeof(String));
                DataColumn ptaType = iReply.Columns.Add("ptaType", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [ptaID], [ptaValue], [ptaType], [uTime] FROM [ClientPhoneToAddress] WHERE [clntAddID] = @clntAddID";
                Int32 id = 0, tID = 0;
                String value = String.Empty, type = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntAddID", SqlDbType.Int).Value = ClientAddressID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    id = readData.GetInt32(0);
                                    value = readData.GetString(1);
                                    tID = readData.GetInt32(2);
                                    type = getDemogTypeName(tID);
                                    uT = readData.GetDateTime(3);
                                    iReply.Rows.Add(id, value, type, uT);
                                    iReply.AcceptChanges();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static ArrayList getCorrSQCList()
        {
            try
            {
                ArrayList iReply = new ArrayList();
                Int32 id = -99, bt = -99;
                Decimal dip = -99.00M, gt = -99.000M;
                String color = String.Empty, selData = "SELECT [csqcID], [csqcBTotal], [csqcDip], [csqcGTotal], [csqcName] FROM [dmgCorrectionSQC]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    id = readData.GetInt32(0);
                                    iReply.Add(id);
                                    bt = readData.GetInt32(1);
                                    iReply.Add(bt);
                                    dip = readData.GetDecimal(2);
                                    iReply.Add(dip);
                                    gt = readData.GetDecimal(3);
                                    iReply.Add(gt);
                                    color = readData.GetString(4);
                                    iReply.Add(color);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getCorrSQCTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn csqcID = iReply.Columns.Add("csqcID", typeof(Int32));
                DataColumn csqcBTotal = iReply.Columns.Add("csqcBTotal", typeof(Int32));
                DataColumn csqcDip = iReply.Columns.Add("csqcDip", typeof(Decimal));
                DataColumn csqcGTotal = iReply.Columns.Add("csqcGTotal", typeof(Decimal));
                DataColumn csqcName = iReply.Columns.Add("csqcName", typeof(String));
                DataColumn csqcDesc = iReply.Columns.Add("csqcDesc", typeof(String));
                Int32 id = -99, bt = -99;
                Decimal dip = -99.00M, gt = -99.000M;
                String color = String.Empty, desc = String.Empty, selData = "SELECT [csqcID], [csqcBTotal], [csqcDip], [csqcGTotal], [csqcName], [csqcDesc] FROM [dmgCorrectionSQC]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    id = readData.GetInt32(0);
                                    bt = readData.GetInt32(1);
                                    dip = readData.GetDecimal(2);
                                    gt = readData.GetDecimal(3);
                                    color = readData.GetString(4);
                                    desc = readData.GetString(5);
                                    iReply.Rows.Add(id, bt, dip, gt, color, desc);
                                    iReply.AcceptChanges();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getClientMissingWellSectionForWellList(Int32 WellID, String ClientDBString)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selData = "select [wsID], [wsName] FROM [WellSection] ORDER BY [wsName]";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                                else
                                {
                                    iReply.Add(-99, String.Empty);
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static String getClientWellSectionNameFromID(Int32 WellSectionID, String ClientDBString)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [wsName] FROM [WellSection] WHERE [wsID] = @wsID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wsID", SqlDbType.Int).Value = WellSectionID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getClientWellSectionForWellList(Int32 WellID, String ClientDBString)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = 0;
                String name = String.Empty;
                String selData = "SELECT [wsID] FROM [WellSectionToWell] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = getClientWellSectionNameFromID(id, ClientDBString);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getClientWellSectionForWellTable(Int32 WellID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wsID = iReply.Columns.Add("wsID", typeof(Int32));
                DataColumn wsName = iReply.Columns.Add("wsName", typeof(String));
                Int32 id = 0;
                String name = String.Empty;
                String selData = "SELECT [wsID] FROM [WellSectionToWell] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString.ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = getClientWellSectionNameFromID(id, ClientDBString);
                                        iReply.Rows.Add(id, name);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getClientWellSectionList(String ClientDBString)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [wsID], [wsName] FROM [WellSection] ORDER BY [wsName]";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getClientWellSectionTable(String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wsID = iReply.Columns.Add("wsID", typeof(Int32));
                DataColumn wsName = iReply.Columns.Add("wsName", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                Int32 id = -99;
                String name = String.Empty;
                DateTime uT = DateTime.Now;
                String selData = "SELECT [wsID], [wsName], [uTime] FROM [WellSection] ORDER BY [wsName]";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, name, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getAccelUnitsTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn acuID = iReply.Columns.Add("acuID", typeof(Int32));
                DataColumn acuName = iReply.Columns.Add("acuName", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [acuID], [acuName], [uTime] FROM [dmgAccelUnit] ORDER BY [acuName]";
                Int32 id = 0;
                String name = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, name, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getBGATable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn bgaID = iReply.Columns.Add("bgaID", typeof(Int32));
                DataColumn bgaStatement = iReply.Columns.Add("bgaStatement", typeof(String));
                String selData = "SELECT [bgaID], [bgaStatement] FROM dmgBackGroundAnamoly ORDER BY [bgaStatement]";
                Int32 id = 0;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Rows.Add(id, value);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getBHAMagLimitTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn mlID = iReply.Columns.Add("mlID", typeof(Int32));
                DataColumn mlValue = iReply.Columns.Add("mlValue", typeof(String));
                String selData = "SELECT [mlID], [mlValue] FROM bhaMagLimit ORDER BY [mlValue]";
                Int32 id = 0;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Rows.Add(id, value);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static DataTable getBHAnalysisTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn bhaqID = iReply.Columns.Add("bhaqID", typeof(Int32));
                DataColumn bhaqValue = iReply.Columns.Add("bhaqValue", typeof(String));
                String selData = "SELECT [bhaqID], [bhaqValue] FROM dmgBHAAnalysis ORDER BY [bhaqValue]";
                Int32 id = 0;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Rows.Add(id, value);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getBHACollarUnits()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [luID], [luName] FROM [dmgLengthUnit] WHERE ([luID] = 1) OR ([luID] = 2) OR ([luID] = 3) ORDER BY [luName]";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getBHALengthUnits()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [luID], [luName] FROM [dmgLengthUnit] WHERE ([luID] = 3) OR ([luID] = 4) OR ([luID] = 5) ORDER BY [luName]";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getBoxyError()
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT [baeValue] FROM dmgBoxyAvgError";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    iReply = readData.GetInt32(0);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getBTQualifierTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn btqID = iReply.Columns.Add("btqID", typeof(Int32));
                DataColumn btqValue = iReply.Columns.Add("btqValue", typeof(String));
                String selData = "SELECT [btqID], [btqValue] FROM dmgBTQualifier ORDER BY [btqValue]";
                Int32 id = 0;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Rows.Add(id, value);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static String getCardinalName(Int32 CardinalID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [compassPoint] FROM [dmgCompass] WHERE [cID] = @cID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@cID", SqlDbType.Int).Value = CardinalID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getCardinals()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn cID = iReply.Columns.Add("cID", typeof(Int32));
                DataColumn compassPoint = iReply.Columns.Add("compassPoint", typeof(String));
                DataColumn abbreviation = iReply.Columns.Add("abbreviation", typeof(String));
                DataColumn traditionalName = iReply.Columns.Add("traditionalName", typeof(String));
                DataColumn minAzimuth = iReply.Columns.Add("minAzimuth", typeof(Decimal));
                DataColumn midAzimuth = iReply.Columns.Add("midAzimuth", typeof(Decimal));
                DataColumn maxAzimuth = iReply.Columns.Add("maxAzimuth", typeof(Decimal));
                Int32 id = 0;
                String name = String.Empty, Abbrv = String.Empty, tName= String.Empty;
                Decimal min = -99.00M, mid = -99.00M, max = -99.00M;
                String selData = "SELECT [cID], [compassPoint], [abbreviation], [traditionalName], [minAzimuth], [midAzimuth], [maxAzimuth] FROM [dmgCompass] ORDER BY [compassPoint]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        Abbrv = readData.GetString(2);
                                        tName = readData.GetString(3);
                                        min = readData.GetDecimal(4);
                                        mid = readData.GetDecimal(5);
                                        max = readData.GetDecimal(6);
                                        iReply.Rows.Add(id, name, Abbrv, tName, min, mid, max);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getCityName(Int32 CityID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [cityName] FROM dmgCity WHERE [cityID] = @cityID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@cityID", SqlDbType.Int).Value = CityID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getCityList(Int32 CountyID)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();

                Int32 id = -99;
                String name = String.Empty;
                String selCity = "SELECT [cityID], [cityName] FROM dmgCity WHERE [cntyID] = @cntyID ORDER BY [cityName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getCity = new SqlCommand(selCity, dataCon);
                        getCity.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                        using (SqlDataReader readCounty = getCity.ExecuteReader())
                        {
                            try
                            {
                                if (readCounty.HasRows)
                                {
                                    while (readCounty.Read())
                                    {
                                        id = readCounty.GetInt32(0);
                                        name = readCounty.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readCounty.Close();
                                readCounty.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getCityTable(Int32 CountyID)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn cityID = iReply.Columns.Add("cityID", typeof(Int32));
                DataColumn cityName = iReply.Columns.Add("cityName", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { cityID };

                Int32 id = -99;
                String name = String.Empty;
                DateTime uT = DateTime.Now;
                String selCity = "SELECT [cityID], [cityName], [uTime] FROM dmgCity WHERE [cntyID] = @cntyID ORDER BY [cityName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getCity = new SqlCommand(selCity, dataCon);
                        getCity.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                        using (SqlDataReader readCounty = getCity.ExecuteReader())
                        {
                            try
                            {
                                if (readCounty.HasRows)
                                {
                                    while (readCounty.Read())
                                    {
                                        id = readCounty.GetInt32(0);
                                        name = readCounty.GetString(1);
                                        uT = readCounty.GetDateTime(2);
                                        iReply.Rows.Add(id, name, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                                else
                                {
                                    id = 1;
                                    name = "No City/Town found for selected County/District/Municipality";
                                    iReply.Rows.Add(id, name, uT);
                                    iReply.AcceptChanges();
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readCounty.Close();
                                readCounty.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getContactTypeList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [ctID], [ctValue] FROM [dmgContactType] ORDER BY [ctValue]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getContactValueFromID(Int32 ContactTypeID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [ctValue] FROM [dmgContactType] WHERE [ctID] = @ctID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ctID", SqlDbType.Int).Value = ContactTypeID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getCountyFieldsList(Int32 StateID)
        {
            try
            {
                Int32 countyID = -99;
                Int32 fldCount = -99;
                String countyName = null;
                Dictionary<Int32, String> countyList = new Dictionary<Int32, String>();
                Dictionary<Int32, String> fcList = new Dictionary<Int32, String>();
                String selCounty = "SELECT [cntyID], [cntyName] FROM [dmgCountyDistrict] WHERE [stID] = @stID ORDER BY [cntyName]";
                
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getCounty = new SqlCommand(selCounty, dataCon);
                        getCounty.Parameters.AddWithValue("stID", SqlDbType.Int).Value = StateID.ToString();
                        using (SqlDataReader readCounty = getCounty.ExecuteReader())
                        {
                            try
                            {
                                if (readCounty.HasRows)
                                {
                                    while (readCounty.Read())
                                    {
                                        countyID = readCounty.GetInt32(0);
                                        countyName = readCounty.GetString(1);
                                        countyList.Add(countyID, countyName);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readCounty.Close();
                                readCounty.Dispose();
                            }
                        }
                        foreach (var pair in countyList)
                        {
                            Int32 cID = pair.Key;
                            String cN = pair.Value;
                            fldCount = getCountyFieldCount(cID);                            
                                cN = String.Format("{0} - ( {1} )", pair.Value, fldCount);
                                fcList.Add(cID, cN);                            
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return fcList;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 getCountyFieldCount(Int32 CountyID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(fldID) FROM [dmgField] WHERE [cntyID] = @cntyID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    iReply = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getCountyList(Int32 StateID)
        {
            try
            {
                Int32 cntID = -99;
                String cntName = null;

                Dictionary<int, string> cntList = new Dictionary<int, string>();
                String selCounty = "SELECT [cntyID], [cntyName] FROM dmgCountyDistrict WHERE [stID] = @stID ORDER BY [cntyName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getCounty = new SqlCommand(selCounty, dataCon);
                        getCounty.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = StateID.ToString();
                        using (SqlDataReader readCounty = getCounty.ExecuteReader())
                        {
                            try
                            {
                                if (readCounty.HasRows)
                                {
                                    while (readCounty.Read())
                                    {
                                        cntID = readCounty.GetInt32(0);
                                        cntName = readCounty.GetString(1);
                                        cntList.Add(cntID, cntName);
                                    }
                                }
                                else
                                {
                                    cntList.Add(0, " --- No County/District/Division/Municipality found for selected State/Province --- ");
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readCounty.Close();
                                readCounty.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return cntList;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getCountyWithFieldCountList(Int32 StateID)
        {
            try
            {
                Int32 cntID = -99, fCount = -99;
                String cntName = String.Empty;
                Dictionary<Int32, String> cntList = new Dictionary<Int32, String>();
                String selData = "SELECT a.cntyID, a.cntyName, (select count(fldID) FROM dmgField b WHERE b.cntyID = a.cntyID) FROM dmgCountyDistrict a WHERE a.stID = @stID ORDER BY a.cntyName";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = StateID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        cntID = readData.GetInt32(0);
                                        cntName = readData.GetString(1);
                                        fCount = readData.GetInt32(2);
                                        cntName = String.Format("{0} - ( {1} )", cntName, fCount);
                                        cntList.Add(cntID, cntName);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return cntList;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getGlobalCountyListWithAssetCount(Int32 StateID, Int32 ClientID, String ClientDBString)
        {
            try
            {
                Int32 cntID = -99, gpd = 0, gwl = 0, swl = 0;
                String cntName = String.Empty, cntValue = String.Empty;
                Dictionary<Int32, String> cntList = new Dictionary<Int32, String>();
                String selData = "SELECT a.cntyID, a.cntyName, (select count(fldID) FROM dmgField b WHERE b.cntyID = a.cntyID) FROM dmgCountyDistrict a WHERE a.stID = @stID ORDER BY a.cntyName";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = StateID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        cntID = readData.GetInt32(0);
                                        cntName = readData.GetString(1);
                                        gpd = getGlobalWellPadCountByCounty(cntID);
                                        gwl = getGlobalWellCountByCounty(cntID);
                                        swl = getSiloWellCountByCounty(cntID, ClientID, ClientDBString);
                                        cntValue = String.Format("{0} --- ( Global Well Pad: {1}; Global Well/Lateral: {2}, SMART Well/Lateral: {3} )", cntName, gpd, gwl, swl);
                                        cntList.Add(cntID, cntValue);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return cntList;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellPadCountByCounty(Int32 CountyID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(wpdID) FROM [dmgWellPad] WHERE [cntyID] = @cntyID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellCountByCounty(Int32 CountyID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [dmgWell] WHERE [cntyID] = @cntyID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getSiloWellCountByCounty(Int32 CountyID, Int32 ClientID, String ClientDBString)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [Well] WHERE [cntyID] = @cntyID AND [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getFieldCountForCounty(Int32 CountyID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(fldID) FROM [dmgField] WHERE [cntyID] = @cntyID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    iReply = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getCountyTable(Int32 StateID)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn cntyID = iReply.Columns.Add("cntyID", typeof(Int32));
                DataColumn cntyName = iReply.Columns.Add("cntyName", typeof(String));
                DataColumn cntyCode = iReply.Columns.Add("cntyCode", typeof(String));
                DataColumn cntyFIPS = iReply.Columns.Add("cntyFIPS", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { cntyID };
                Int32 id = -99;
                String name = String.Empty;
                String code = String.Empty;
                String fips = String.Empty;
                DateTime uT = DateTime.Now;

                String selCounty = "SELECT [cntyID], [cntyName], [cntyCode], [cntyFIPS], [uTime] FROM [dmgCountyDistrict] WHERE [stID] = @stID ORDER BY [cntyName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getCounty = new SqlCommand(selCounty, dataCon);
                        getCounty.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = StateID.ToString();
                        using (SqlDataReader readCounty = getCounty.ExecuteReader())
                        {
                            try
                            {
                                if (readCounty.HasRows)
                                {
                                    while (readCounty.Read())
                                    {
                                        id = readCounty.GetInt32(0);
                                        name = readCounty.GetString(1);
                                        code = readCounty.GetString(2);
                                        if (String.IsNullOrEmpty(code) || code.Equals("-99"))
                                        {
                                            code = "---";
                                        }
                                        fips = readCounty.GetString(3);
                                        if (string.IsNullOrEmpty(fips) || fips.Equals("-99"))
                                        {
                                            fips = "---";
                                        }
                                        uT = readCounty.GetDateTime(4);
                                        iReply.Rows.Add(id, name, code, fips, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                                else
                                {
                                    id = 1;
                                    name = "No County/District/Division/Municipality found for selected State/Province";
                                    code = "---";
                                    fips = "---";
                                    iReply.Rows.Add(id, name, code, fips, uT);
                                    iReply.AcceptChanges();
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readCounty.Close();
                                readCounty.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getCountyTableForOperatorByState(Int32 StateID, Int32 OperatorID, Int32 ClientID, String ClientDBString)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn cntyID = iReply.Columns.Add("cntyID", typeof(Int32));
                DataColumn cntyName = iReply.Columns.Add("cntyName", typeof(String));
                DataColumn cntyCode = iReply.Columns.Add("cntyCode", typeof(String));
                DataColumn cntyFIPS = iReply.Columns.Add("cntyFIPS", typeof(String));
                DataColumn padCount = iReply.Columns.Add("padCount", typeof(Int32));
                DataColumn welCount = iReply.Columns.Add("welCount", typeof(Int32));
                DataColumn swlCount = iReply.Columns.Add("swlCount", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { cntyID };
                Int32 id = -99, gpd = 0, gwl = 0, swl = 0;
                String name = String.Empty;
                String code = String.Empty;
                String fips = String.Empty;
                DateTime uT = DateTime.Now;

                String selCounty = "SELECT [cntyID], [cntyName], [cntyCode], [cntyFIPS], [uTime] FROM [dmgCountyDistrict] WHERE [stID] = @stID ORDER BY [cntyName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getCounty = new SqlCommand(selCounty, dataCon);
                        getCounty.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = StateID.ToString();
                        using (SqlDataReader readCounty = getCounty.ExecuteReader())
                        {
                            try
                            {
                                if (readCounty.HasRows)
                                {
                                    while (readCounty.Read())
                                    {
                                        id = readCounty.GetInt32(0);
                                        name = readCounty.GetString(1);
                                        code = readCounty.GetString(2);
                                        if (String.IsNullOrEmpty(code) || code.Equals("-99"))
                                        {
                                            code = "---";
                                        }
                                        fips = readCounty.GetString(3);
                                        if (string.IsNullOrEmpty(fips) || fips.Equals("-99"))
                                        {
                                            fips = "---";
                                        }
                                        gpd = getGlobalWellPadCountForOperatorByCounty(id, OperatorID);
                                        gwl = getGlobalWellCountForOperatorByCounty(id, OperatorID);
                                        swl = getSiloWellCountByCounty(id, OperatorID, ClientID, ClientDBString);
                                        uT = readCounty.GetDateTime(4);
                                        iReply.Rows.Add(id, name, code, fips, gpd, gwl, swl, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                                else
                                {
                                    id = 1;
                                    name = "No County/District/Division/Municipality found for selected State/Province";
                                    code = "---";
                                    fips = "---";
                                    iReply.Rows.Add(id, name, code, fips, gpd, gwl, swl, uT);
                                    iReply.AcceptChanges();
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readCounty.Close();
                                readCounty.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getCountyTableForOperatorByStateForReviewer(Int32 StateID, Int32 OperatorID)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn cntyID = iReply.Columns.Add("cntyID", typeof(Int32));
                DataColumn cntyName = iReply.Columns.Add("cntyName", typeof(String));
                DataColumn cntyCode = iReply.Columns.Add("cntyCode", typeof(String));
                DataColumn cntyFIPS = iReply.Columns.Add("cntyFIPS", typeof(String));
                DataColumn padCount = iReply.Columns.Add("padCount", typeof(Int32));
                DataColumn welCount = iReply.Columns.Add("welCount", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { cntyID };
                Int32 id = -99, gpd = 0, gwl = 0;
                String name = String.Empty, code = String.Empty, fips = String.Empty;
                DateTime uT = DateTime.Now;

                String selCounty = "SELECT [cntyID], [cntyName], [cntyCode], [cntyFIPS], [uTime] FROM [dmgCountyDistrict] WHERE [stID] = @stID ORDER BY [cntyName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getCounty = new SqlCommand(selCounty, dataCon);
                        getCounty.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = StateID.ToString();
                        using (SqlDataReader readCounty = getCounty.ExecuteReader())
                        {
                            try
                            {
                                if (readCounty.HasRows)
                                {
                                    while (readCounty.Read())
                                    {
                                        id = readCounty.GetInt32(0);
                                        name = readCounty.GetString(1);
                                        code = readCounty.GetString(2);
                                        if (String.IsNullOrEmpty(code) || code.Equals("-99"))
                                        {
                                            code = "---";
                                        }
                                        fips = readCounty.GetString(3);
                                        if (string.IsNullOrEmpty(fips) || fips.Equals("-99"))
                                        {
                                            fips = "---";
                                        }
                                        gpd = getGlobalWellPadCountForOperatorByCounty(id, OperatorID);
                                        gwl = getGlobalWellCountForOperatorByCounty(id, OperatorID);
                                        uT = readCounty.GetDateTime(4);
                                        iReply.Rows.Add(id, name, code, fips, gpd, gwl, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readCounty.Close();
                                readCounty.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getProviderCountyTableForOperatorByStateForReviewer(Int32 StateID, Int32 ProviderID)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn cntyID = iReply.Columns.Add("cntyID", typeof(Int32));
                DataColumn cntyName = iReply.Columns.Add("cntyName", typeof(String));
                DataColumn cntyCode = iReply.Columns.Add("cntyCode", typeof(String));
                DataColumn cntyFIPS = iReply.Columns.Add("cntyFIPS", typeof(String));
                DataColumn padCount = iReply.Columns.Add("padCount", typeof(Int32));
                DataColumn welCount = iReply.Columns.Add("welCount", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { cntyID };
                Int32 id = -99, gpd = 0, gwl = 0;
                String name = String.Empty, code = String.Empty, fips = String.Empty;
                DateTime uT = DateTime.Now;

                String selCounty = "SELECT [cntyID], [cntyName], [cntyCode], [cntyFIPS], [uTime] FROM [dmgCountyDistrict] WHERE [stID] = @stID ORDER BY [cntyName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getCounty = new SqlCommand(selCounty, dataCon);
                        getCounty.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = StateID.ToString();
                        using (SqlDataReader readCounty = getCounty.ExecuteReader())
                        {
                            try
                            {
                                if (readCounty.HasRows)
                                {
                                    while (readCounty.Read())
                                    {
                                        id = readCounty.GetInt32(0);
                                        name = readCounty.GetString(1);
                                        code = readCounty.GetString(2);
                                        if (String.IsNullOrEmpty(code) || code.Equals("-99"))
                                        {
                                            code = "---";
                                        }
                                        fips = readCounty.GetString(3);
                                        if (string.IsNullOrEmpty(fips) || fips.Equals("-99"))
                                        {
                                            fips = "---";
                                        }
                                        gpd = getProviderGlobalWellPadCountForOperatorByCounty(id, ProviderID);
                                        gwl = getProviderGlobalWellCountForOperatorByCounty(id, ProviderID);
                                        uT = readCounty.GetDateTime(4);
                                        iReply.Rows.Add(id, name, code, fips, gpd, gwl, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readCounty.Close();
                                readCounty.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellPadCountForOperatorByCounty(Int32 CountyID, Int32 OperatorID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(wpdID) FROM [dmgWellPad] WHERE [cntyID] = @cntyID AND [optrID] = @optrID";
                String DemogDBString = WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString();
                using (SqlConnection dataCon = new SqlConnection(DemogDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getProviderGlobalWellPadCountForOperatorByCounty(Int32 CountyID, Int32 ProviderID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(wpdID) FROM [dmgWellPad] WHERE [cntyID] = @cntyID AND [clntID] = @clntID";
                String DemogDBString = WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString();
                using (SqlConnection dataCon = new SqlConnection(DemogDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ProviderID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellCountForOperatorByCounty(Int32 CountyID, Int32 OperatorID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [dmgWell] WHERE [cntyID] = @cntyID AND [optrID] = @optrID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getProviderGlobalWellCountForOperatorByCounty(Int32 CountyID, Int32 ProviderID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [dmgWell] WHERE [cntyID] = @cntyID AND [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ProviderID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getSiloWellCountByCounty(Int32 CountyID, Int32 OperatorID, Int32 ClientID, String ClientDBString)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [Well] WHERE [cntyID] = @cntyID AND [optrID] = @optrID AND [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getCountyName(Int32 CountyID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [cntyName] FROM [dmgCountyDistrict] WHERE [cntyID] = @cntyID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getCountryName(Int32 CountryID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [ctyName] FROM [dmgCountry] WHERE [ctyID] = @ctyID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ctyID", SqlDbType.Int).Value = CountryID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                                else
                                {
                                    iReply = "---";
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    {throw new System.Exception(ex.ToString());}
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getCountryLetterCode(Int32 CountryID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [ctyCode] FROM [dmgCountry] WHERE [ctyID] = @ctyID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ctyID", SqlDbType.Int).Value = CountryID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getCountryList(Int32 SubRegionID)
        {
            try
            {
                Int32 ctyID = -99;
                String ctyName = null;

                Dictionary<Int32, String> countryList = new Dictionary<Int32, String>();
                String selCountry = "SELECT [ctyID], [ctyName] FROM [dmgCountry] WHERE [gsrID] = @gsrID ORDER BY [ctyName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getCountry = new SqlCommand(selCountry, dataCon);
                        getCountry.Parameters.AddWithValue("@gsrID", SqlDbType.Int).Value = SubRegionID.ToString();
                        using (SqlDataReader readCountry = getCountry.ExecuteReader())
                        {
                            try
                            {
                                if (readCountry.HasRows)
                                {
                                    while (readCountry.Read())
                                    {
                                        ctyID = readCountry.GetInt32(0);
                                        ctyName = readCountry.GetString(1);
                                        countryList.Add(ctyID, ctyName);
                                    }
                                }
                                else
                                {
                                    countryList.Add(0, " --- No Country/Nation found for selected Sub-Region --- ");
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readCountry.Close();
                                readCountry.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return countryList;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getGlobalCountryListWithAssetCount(Int32 SubRegionID, Int32 ClientID, String ClientDBString)
        {
            try
            {
                Int32 ctyID = -99, gpd = 0, gwl = 0, swl = 0;
                String ctyName = String.Empty, ctyValue = String.Empty;
                Dictionary<Int32, String> countryList = new Dictionary<Int32, String>();
                String selCountry = "SELECT [ctyID], [ctyName] FROM [dmgCountry] WHERE [gsrID] = @gsrID ORDER BY [ctyName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getCountry = new SqlCommand(selCountry, dataCon);
                        getCountry.Parameters.AddWithValue("@gsrID", SqlDbType.Int).Value = SubRegionID.ToString();
                        using (SqlDataReader readCountry = getCountry.ExecuteReader())
                        {
                            try
                            {
                                if (readCountry.HasRows)
                                {
                                    while (readCountry.Read())
                                    {
                                        ctyID = readCountry.GetInt32(0);
                                        ctyName = readCountry.GetString(1);
                                        gpd = getGlobalWellPadCountByCountry(ctyID);
                                        gwl = getGlobalWellCountByCountry(ctyID);
                                        swl = getSiloWellCountByCountry(ctyID, ClientID, ClientDBString);
                                        ctyValue = String.Format("{0} --- ( Global Well Pad: {1}; Global Well/Lateral: {2}; SMART Well/Lateral: {3} )", ctyName, gpd, gwl, swl);
                                        countryList.Add(ctyID, ctyValue);
                                    }
                                }
                                else
                                {
                                    countryList.Add(0, " --- No Country/Nation found for selected Sub-Region --- ");
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readCountry.Close();
                                readCountry.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return countryList;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellPadCountByCountry(Int32 CountryID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(wpdID) FROM [dmgWellPad] WHERE [ctyID] = @ctyID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ctyID", SqlDbType.Int).Value = CountryID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellCountByCountry(Int32 CountryID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(welID) FROM [dmgWell] WHERE [ctyID] = @ctyID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ctyID", SqlDbType.Int).Value = CountryID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getSiloWellCountByCountry(Int32 CountryID, Int32 ClientID, String ClientDBString)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [Well] WHERE [ctyID] = @ctyID AND [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ctyID", SqlDbType.Int).Value = CountryID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getCountryDialCode(Int32 CountryID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT [ctyDialCode] FROM [dmgCountry] WHERE [ctyID] = @ctyID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ctyID", SqlDbType.Int).Value = CountryID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getCountryTable(Int32 SubRegionID)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn ctyID = iReply.Columns.Add("ctyID", typeof(Int32));
                DataColumn ctyName = iReply.Columns.Add("ctyName", typeof(String));
                DataColumn ctyCode = iReply.Columns.Add("ctyCode", typeof(String));
                DataColumn ctyDialCode = iReply.Columns.Add("ctyDialCode", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { ctyID };
                Int32 id = -99;
                String name = String.Empty;
                String code = String.Empty;
                Int32 dCode = -99;
                DateTime uT = DateTime.Now;
                String selCountry = "SELECT [ctyID], [ctyName], [ctyCode], [ctyDialCode], [uTime] FROM [dmgCountry] WHERE [gsrID] = @gsrID ORDER BY [ctyName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selCountry, dataCon);
                        getData.Parameters.AddWithValue("@gsrID", SqlDbType.Int).Value = SubRegionID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        code = readData.GetString(2);
                                        dCode = readData.GetInt32(3);
                                        uT = readData.GetDateTime(4);
                                        iReply.Rows.Add(id, name, code, dCode, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getCountryTableForOperatorBySubRegion(Int32 SubRegionID, Int32 OperatorID, Int32 ClientID, String ClientDBString)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn ctyID = iReply.Columns.Add("ctyID", typeof(Int32));
                DataColumn ctyName = iReply.Columns.Add("ctyName", typeof(String));
                DataColumn ctyCode = iReply.Columns.Add("ctyCode", typeof(String));
                DataColumn ctyDialCode = iReply.Columns.Add("ctyDialCode", typeof(Int32));
                DataColumn padCount = iReply.Columns.Add("padCount", typeof(Int32));
                DataColumn welCount = iReply.Columns.Add("welCount", typeof(Int32));
                DataColumn swlCount = iReply.Columns.Add("swlCount", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { ctyID };
                Int32 id = -99, gpd = 0, gwl = 0, swl = 0;
                String name = String.Empty;
                String code = String.Empty;
                Int32 dCode = -99;
                DateTime uT = DateTime.Now;
                String selCountry = "SELECT [ctyID], [ctyName], [ctyCode], [ctyDialCode], [uTime] FROM [dmgCountry] WHERE [gsrID] = @gsrID ORDER BY [ctyName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selCountry, dataCon);
                        getData.Parameters.AddWithValue("@gsrID", SqlDbType.Int).Value = SubRegionID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        code = readData.GetString(2);
                                        dCode = readData.GetInt32(3);
                                        gpd = getGlobalWellPadCountForOperatorByCountry(id, OperatorID);
                                        gwl = getGlobalWellCountForOperatorByCountry(id, OperatorID);
                                        swl = getSiloWellCountByCountry(id, OperatorID, ClientID, ClientDBString);
                                        uT = readData.GetDateTime(4);
                                        iReply.Rows.Add(id, name, code, dCode, gpd, gwl, swl, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getCountryTableForOperatorBySubRegionForReviewer(Int32 SubRegionID, Int32 OperatorID)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn ctyID = iReply.Columns.Add("ctyID", typeof(Int32));
                DataColumn ctyName = iReply.Columns.Add("ctyName", typeof(String));
                DataColumn ctyCode = iReply.Columns.Add("ctyCode", typeof(String));
                DataColumn ctyDialCode = iReply.Columns.Add("ctyDialCode", typeof(Int32));
                DataColumn padCount = iReply.Columns.Add("padCount", typeof(Int32));
                DataColumn welCount = iReply.Columns.Add("welCount", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { ctyID };
                Int32 id = -99, gpd = 0, gwl = 0;
                String name = String.Empty;
                String code = String.Empty;
                Int32 dCode = -99;
                DateTime uT = DateTime.Now;
                String selCountry = "SELECT [ctyID], [ctyName], [ctyCode], [ctyDialCode], [uTime] FROM [dmgCountry] WHERE [gsrID] = @gsrID ORDER BY [ctyName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selCountry, dataCon);
                        getData.Parameters.AddWithValue("@gsrID", SqlDbType.Int).Value = SubRegionID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        code = readData.GetString(2);
                                        dCode = readData.GetInt32(3);
                                        gpd = getGlobalWellPadCountForOperatorByCountry(id, OperatorID);
                                        gwl = getGlobalWellCountForOperatorByCountry(id, OperatorID);
                                        uT = readData.GetDateTime(4);
                                        iReply.Rows.Add(id, name, code, dCode, gpd, gwl, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getProviderCountryTableForOperatorBySubRegionForReviewer(Int32 SubRegionID, Int32 ProviderID)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn ctyID = iReply.Columns.Add("ctyID", typeof(Int32));
                DataColumn ctyName = iReply.Columns.Add("ctyName", typeof(String));
                DataColumn ctyCode = iReply.Columns.Add("ctyCode", typeof(String));
                DataColumn ctyDialCode = iReply.Columns.Add("ctyDialCode", typeof(Int32));
                DataColumn padCount = iReply.Columns.Add("padCount", typeof(Int32));
                DataColumn welCount = iReply.Columns.Add("welCount", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { ctyID };
                Int32 id = -99, gpd = 0, gwl = 0;
                String name = String.Empty;
                String code = String.Empty;
                Int32 dCode = -99;
                DateTime uT = DateTime.Now;
                String selCountry = "SELECT [ctyID], [ctyName], [ctyCode], [ctyDialCode], [uTime] FROM [dmgCountry] WHERE [gsrID] = @gsrID ORDER BY [ctyName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selCountry, dataCon);
                        getData.Parameters.AddWithValue("@gsrID", SqlDbType.Int).Value = SubRegionID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        code = readData.GetString(2);
                                        dCode = readData.GetInt32(3);
                                        gpd = getProviderGlobalWellPadCountForOperatorByCountry(id, ProviderID);
                                        gwl = getProviderGlobalWellCountForOperatorByCountry(id, ProviderID);
                                        uT = readData.GetDateTime(4);
                                        iReply.Rows.Add(id, name, code, dCode, gpd, gwl, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellPadCountForOperatorByCountry(Int32 CountryID, Int32 OperatorID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(wpdID) FROM [dmgWellPad] WHERE [ctyID] = @ctyID AND [optrID] = @optrID";
                String DemogDBString = WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString();
                using (SqlConnection dataCon = new SqlConnection(DemogDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ctyID", SqlDbType.Int).Value = CountryID.ToString();
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getProviderGlobalWellPadCountForOperatorByCountry(Int32 CountryID, Int32 ProviderID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(wpdID) FROM [dmgWellPad] WHERE [ctyID] = @ctyID AND [clntID] = @clntID";
                String DemogDBString = WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString();
                using (SqlConnection dataCon = new SqlConnection(DemogDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ctyID", SqlDbType.Int).Value = CountryID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ProviderID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellCountForOperatorByCountry(Int32 CountryID, Int32 OperatorID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [dmgWell] WHERE [ctyID] = @ctyID AND [optrID] = @optrID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ctyID", SqlDbType.Int).Value = CountryID.ToString();
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getProviderGlobalWellCountForOperatorByCountry(Int32 CountryID, Int32 ProviderID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [dmgWell] WHERE [ctyID] = @ctyID AND [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ctyID", SqlDbType.Int).Value = CountryID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ProviderID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getSiloWellCountByCountry(Int32 CountryID, Int32 OperatorID, Int32 ClientID, String ClientDBString)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [Well] WHERE [ctyID] = @ctyID AND [optrID] = @optrID AND [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ctyID", SqlDbType.Int).Value = CountryID.ToString();
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getColorTypeTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn lclrID = iReply.Columns.Add("lclrID", typeof(Int32));
                DataColumn lclrValue = iReply.Columns.Add("lclrValue", typeof(String));
                String selData = "SELECT [lclrID], [lclrValue] FROM [dmgColorType] ORDER BY [lclrValue]";
                Int32 id = 0;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Rows.Add(id, value);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getDatumTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn datID = iReply.Columns.Add("datID", typeof(Int32));
                DataColumn datName = iReply.Columns.Add("datName", typeof(String));
                Int32 id = -99;
                String name = String.Empty;
                String selStat = "SELECT [datID], [datName] FROM [dmgDatum] ORDER BY [datName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selStat, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Rows.Add(id, name);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getDatumList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selStat = "SELECT [datID], [datName] FROM [dmgDatum] ORDER BY [datName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selStat, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getWPDepthReferenceList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [wpdrID], [wpdrName] FROM [dmgWellPlanDepthReference] ORDER BY [wpdrName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getDepthReferenceName(Int32 DepthReferenceID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [wpdrName] FROM [dmgWellPlanDepthReference] WHERE [wpdrID] = @wpdrID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wpdrID", SqlDbType.NVarChar).Value = DepthReferenceID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getDataEntryTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn demID = iReply.Columns.Add("demID", typeof(Int32));
                DataColumn demName = iReply.Columns.Add("demName", typeof(String));
                String selData = "SELECT [demID], [demName] FROM [dmgDataEntryMethod] ORDER BY [demName]";
                Int32 id = 0;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Rows.Add(id, value);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getDCPoleStrengthTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn dcpsID = iReply.Columns.Add("dcpsID", typeof(Int32));
                DataColumn dcpsParameter = iReply.Columns.Add("dcpsParameter", typeof(String));
                DataColumn dcpsValue = iReply.Columns.Add("dcpsValue", typeof(Decimal));
                String selData = "SELECT [dcpsID], [dcpsParameter], [dcpsValue] FROM [dmgDrillCollarPoleStrength] ORDER BY [dcpsParameter]";
                Int32 id = 0;
                String param = String.Empty;
                decimal value = -99.00M;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        param = readData.GetString(1);
                                        value = readData.GetDecimal(2);
                                        iReply.Rows.Add(id, param, value);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static ArrayList getdcpsDataList()
        {
            try
            {
                ArrayList iReply = new ArrayList();
                String selData = "SELECT [dcpsValue] FROM [dmgDrillCollarPoleStrength] WHERE [dcpsID] = '1'";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    dataCon.Open();
                    SqlCommand getData = new SqlCommand(selData, dataCon);
                    using (SqlDataReader readData = getData.ExecuteReader())
                    {
                        try
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    iReply.Add(readData.GetDecimal(0));
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            readData.Close();
                            readData.Dispose();
                        }
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getDemogTypeList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [dmgID], [dmgName] FROM [dmgDemogType] ORDER BY [dmgName] ASC";
                Int32 id = 0;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Add(id, value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getDemogTypeTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn dmgID = iReply.Columns.Add("dmgID", typeof(Int32));
                DataColumn dmgName = iReply.Columns.Add("dmgName", typeof(String));
                String selData = "SELECT [dmgID], [dmgName] FROM dmgDemogType ORDER BY [dmgName]";
                Int32 id = 0;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Rows.Add(id, value);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getDemogTypeName(Int32 DemogID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [dmgName] FROM dmgDemogType WHERE [dmgID] = @dmgID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@dmgID", SqlDbType.Int).Value = DemogID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getDepthRefTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wpdrID = iReply.Columns.Add("wpdrID", typeof(Int32));
                DataColumn wpdrName = iReply.Columns.Add("wpdrName", typeof(String));
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [wpdrID], [wpdrName] FROM [dmgWellPlanDepthReference] ORDER BY [wpdrName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Rows.Add(id, name);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getDepthRefList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [wpdrID], [wpdrName] FROM [dmgWellPlanDepthReference] ORDER BY [wpdrName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static ArrayList getdflMetaDataList()
        {
            try
            {
                ArrayList iReply = new ArrayList();
                String getDFLMetaData = "SELECT [dflLowerBT], [dflLowerDip], [dflLowerGT], [dflUpperBT], [dflUpperDip], [dflUpperGT] FROM [dmgDeltaFluctuationLimit]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    dataCon.Open();
                    SqlCommand dflMetaData = new SqlCommand(getDFLMetaData, dataCon);
                    using (SqlDataReader readdflData = dflMetaData.ExecuteReader())
                    {
                        try
                        {
                            if (readdflData.HasRows)
                            {
                                while (readdflData.Read())
                                {
                                    iReply.Add(readdflData.GetDecimal(0));
                                    iReply.Add(readdflData.GetDecimal(1));
                                    iReply.Add(readdflData.GetDecimal(2));
                                    iReply.Add(readdflData.GetDecimal(3));
                                    iReply.Add(readdflData.GetDecimal(4));
                                    iReply.Add(readdflData.GetDecimal(5));
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            readdflData.Close();
                            readdflData.Dispose();
                        }
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getDipQualifierTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn dpqID = iReply.Columns.Add("dpqID", typeof(Int32));
                DataColumn dpqValue = iReply.Columns.Add("dpqValue", typeof(String));
                String selData = "SELECT [dpqID], [dpqValue] FROM dmgDipQualifier ORDER BY [dpqValue]";
                Int32 id = 0;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Rows.Add(id, value);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getDisclaimerTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn dscID = iReply.Columns.Add("dscID", typeof(Int32));
                DataColumn dscValue = iReply.Columns.Add("dscValue", typeof(String));
                DataColumn dsctID = iReply.Columns.Add("dsctID", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [dscID], [dscValue], [dsctID], [uTime] FROM [dmgDisclaimer] ORDER BY [dsctID]";
                Int32 id = 0, typeid = 0;
                String value = String.Empty, type = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        typeid = readData.GetInt32(2);
                                        type = getDiscType(typeid);
                                        uT = readData.GetDateTime(3);
                                        iReply.Rows.Add(id, value, type, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getDiscType(Int32 DiscTypeID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [dsctValue] FROM [dmgDisclaimerType] WHERE [dsctID] = @dsctID";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@dsctID", SqlDbType.Int).Value = DiscTypeID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                                else
                                {
                                    iReply = "---";
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getDisclaimerTypeList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [dsctID], [dsctValue] FROM [dmgDisclaimerType] ORDER BY [dsctValue]";
                Int32 id = 0;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Add(id, value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getDisclaimerTypeTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn dsctID = iReply.Columns.Add("dsctID", typeof(Int32));
                DataColumn dsctValue = iReply.Columns.Add("dsctValue", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [dsctID], [dsctValue], [uTime] FROM [dmgDisclaimerType] ORDER BY [dsctValue]";
                Int32 id = 0;
                String value = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, value, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }       

        public static String getFieldName(Int32 FieldID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [fldName] FROM [dmgField] WHERE [fldID] = @fldID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@fldID", SqlDbType.Int).Value = FieldID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getFieldList(Int32 CountyDistrictID)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selStat = "SELECT [fldID], [fldName] FROM [dmgField] WHERE [cntyID] = @cntyID ORDER BY [fldName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selStat, dataCon);
                        getData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyDistrictID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getGlobalFieldListWithAssetCount(Int32 CountyID, Int32 ClientID, String ClientDBString)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99, gpd = 0, gwl = 0, swl = 0;
                String fldName = String.Empty, fldValue = String.Empty;
                String selStat = "SELECT [fldID], [fldName] FROM [dmgField] WHERE [cntyID] = @cntyID ORDER BY [fldName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selStat, dataCon);
                        getData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        fldName = readData.GetString(1);
                                        gpd = getGlobalWellPadCountByField(id);
                                        gwl = getGlobalWellCountByField(id);
                                        swl = getSiloWellCountByField(id, ClientID, ClientDBString);
                                        fldValue = String.Format("{0} --- ( Global Well Pad: {1}; Global Well/Lateral: {2}; SMART Well/Lateral: {3} )", fldName, gpd, gwl, swl);
                                        iReply.Add(id, fldValue);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellPadCountByField(Int32 FieldID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(wpdID) FROM [dmgWellPad] WHERE [fldID] = @fldID";
                String DemogDBString = WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString();
                using (SqlConnection dataCon = new SqlConnection(DemogDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@fldID", SqlDbType.Int).Value = FieldID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellCountByField(Int32 FieldID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [dmgWell] WHERE [fldID] = @fldID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@fldID", SqlDbType.Int).Value = FieldID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getSiloWellCountByField(Int32 FieldID, Int32 ClientID, String ClientDBString)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [Well] WHERE [fldID] = @fldID AND [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@fldID", SqlDbType.Int).Value = FieldID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getFieldNameListForCounty(Int32 CountyDistrictID)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selStat = "SELECT [fldID], [fldName] FROM [dmgField] WHERE [cntyID] = @cntyID ORDER BY [fldName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selStat, dataCon);
                        getData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyDistrictID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getFieldWithWellCountList(Int32 CountyDistrictID)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99, wCount = -99;
                String name = String.Empty;
                String selData = "SELECT [fldID], [fldName] FROM [dmgField] WHERE [cntyID] = @cntyID ORDER BY [fldName]";
                //String selData = "SELECT a.fldID, a.fldName, count(b.welID) FROM dmgWell b, dmgField a WHERE b.fldID = a.fldID AND a.cntyID = @cntyID ORDER BY a.fldName";                                    
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyDistrictID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        wCount = getFieldWellCount(id);
                                        //wCount = readData.GetInt32(2);
                                        name = String.Format("{0} - ( {1} )", name, wCount);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getFieldWellCount(Int32 FieldID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(welID) FROM [dmgWell] WHERE [fldID] = @fldID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@fldID", SqlDbType.Int).Value = FieldID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    iReply = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getFieldWellPadsCount(Int32 FieldID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(wpdID) FROM [dmgWellPad] WHERE [fldID] = @fldID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@fldID", SqlDbType.Int).Value = FieldID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    iReply = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getFieldTable(Int32 CountyID)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn fldID = iReply.Columns.Add("fldID", typeof(Int32));
                DataColumn fldName = iReply.Columns.Add("fldName", typeof(String));
                DataColumn fldCode = iReply.Columns.Add("fldCode", typeof(String));
                DataColumn hcOil = iReply.Columns.Add("hcOil", typeof(String));
                DataColumn hcGas = iReply.Columns.Add("hcGas", typeof(String));
                DataColumn hcAssoc = iReply.Columns.Add("hcAssoc", typeof(String));
                DataColumn hcUnknown = iReply.Columns.Add("hcUnknown", typeof(String));
                DataColumn Year = iReply.Columns.Add("Year", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { fldID };
                Int32 id = -99;
                String name = String.Empty, code = String.Empty;
                Int32 hcO = -99, hcG = -99, hcA = -99, hcU = -99;
                String year = String.Empty, codeVal = String.Empty, hcOVal = String.Empty, hcGVal = String.Empty, hcAVal = String.Empty, hcUVal = String.Empty;
                DateTime uT = DateTime.Now;
                String selField = "SELECT [fldID], [fldName], [fldCode], [hcOil], [hcGas], [hcAssoc], [hcUnknown], [Year], [uTime] FROM [dmgField] WHERE [cntyID] = @cntyID ORDER BY [fldName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selField, dataCon);
                        getData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        code = readData.GetString(2);
                                        if (code.Equals("-99"))
                                        {
                                            codeVal = "---";
                                        }
                                        hcO = readData.GetInt32(3);
                                        if (hcO.Equals(-99))
                                        {
                                            hcOVal = "---";
                                        }
                                        else
                                        {
                                            hcOVal = getFieldOption(hcO);
                                        }
                                        hcG = readData.GetInt32(4);
                                        if (hcG.Equals(-99))
                                        {
                                            hcGVal = "---";
                                        }
                                        else
                                        {
                                            hcGVal = getFieldOption(hcG);
                                        }
                                        hcA = readData.GetInt32(5);
                                        if (hcA.Equals(-99))
                                        {
                                            hcAVal = "---";
                                        }
                                        else
                                        {
                                            hcAVal = getFieldOption(hcU);
                                        }
                                        hcU = readData.GetInt32(6);
                                        if (hcU.Equals(-99))
                                        {
                                            hcUVal = "---";
                                        }
                                        else
                                        {
                                            hcUVal = getFieldOption(hcU);
                                        }
                                        year = readData.GetString(7);
                                        if (String.IsNullOrEmpty(year) || year.Equals(-99))
                                        {
                                            year = "---";
                                        }
                                        uT = readData.GetDateTime(8);
                                        iReply.Rows.Add(id, name, codeVal, hcOVal, hcGVal, hcAVal, hcUVal, year, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                                else
                                {
                                    id = 1;
                                    name = "No Field(s) found for selected County/District/Municipality";
                                    code = "---";
                                    year = "---";
                                    hcOVal = "---";
                                    hcGVal = "---";
                                    hcAVal = "---";
                                    hcUVal = "---";
                                    year = "---";
                                    uT = DateTime.Now;
                                    iReply.Rows.Add(id, name, code, hcOVal, hcGVal, hcAVal, hcUVal, year, uT);
                                    iReply.AcceptChanges();
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getFieldTableForOperatorByCounty(Int32 CountyID, Int32 OperatorID, Int32 ClientID, String ClientDBString)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn fldID = iReply.Columns.Add("fldID", typeof(Int32));
                DataColumn fldName = iReply.Columns.Add("fldName", typeof(String));
                DataColumn fldCode = iReply.Columns.Add("fldCode", typeof(String));
                DataColumn hcOil = iReply.Columns.Add("hcOil", typeof(String));
                DataColumn hcGas = iReply.Columns.Add("hcGas", typeof(String));
                DataColumn hcAssoc = iReply.Columns.Add("hcAssoc", typeof(String));
                DataColumn hcUnknown = iReply.Columns.Add("hcUnknown", typeof(String));
                DataColumn Year = iReply.Columns.Add("Year", typeof(String));
                DataColumn padCount = iReply.Columns.Add("padCount", typeof(Int32));
                DataColumn welCount = iReply.Columns.Add("welCount", typeof(Int32));
                DataColumn swlCount = iReply.Columns.Add("swlCount", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { fldID };
                Int32 id = -99, gpd = 0, gwl = 0, swl = 0;
                String name = String.Empty, code = String.Empty;
                Int32 hcO = -99, hcG = -99, hcA = -99, hcU = -99;
                String year = String.Empty, codeVal = String.Empty, hcOVal = String.Empty, hcGVal = String.Empty, hcAVal = String.Empty, hcUVal = String.Empty;
                DateTime uT = DateTime.Now;
                String selField = "SELECT [fldID], [fldName], [fldCode], [hcOil], [hcGas], [hcAssoc], [hcUnknown], [Year], [uTime] FROM [dmgField] WHERE [cntyID] = @cntyID ORDER BY [fldName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selField, dataCon);
                        getData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        code = readData.GetString(2);
                                        if (code.Equals("-99"))
                                        {
                                            codeVal = "---";
                                        }
                                        hcO = readData.GetInt32(3);
                                        if (hcO.Equals(-99))
                                        {
                                            hcOVal = "---";
                                        }
                                        else
                                        {
                                            hcOVal = getFieldOption(hcO);
                                        }
                                        hcG = readData.GetInt32(4);
                                        if (hcG.Equals(-99))
                                        {
                                            hcGVal = "---";
                                        }
                                        else
                                        {
                                            hcGVal = getFieldOption(hcG);
                                        }
                                        hcA = readData.GetInt32(5);
                                        if (hcA.Equals(-99))
                                        {
                                            hcAVal = "---";
                                        }
                                        else
                                        {
                                            hcAVal = getFieldOption(hcU);
                                        }
                                        hcU = readData.GetInt32(6);
                                        if (hcU.Equals(-99))
                                        {
                                            hcUVal = "---";
                                        }
                                        else
                                        {
                                            hcUVal = getFieldOption(hcU);
                                        }
                                        year = readData.GetString(7);
                                        if (String.IsNullOrEmpty(year) || year.Equals(-99))
                                        {
                                            year = "---";
                                        }
                                        gpd = getGlobalWellPadCountForOperatorByField(id, OperatorID);
                                        gwl = getGlobalWellCountForOperatorByField(id, OperatorID);
                                        swl = getSiloWellCountByField(id, OperatorID, ClientID, ClientDBString);
                                        uT = readData.GetDateTime(8);
                                        iReply.Rows.Add(id, name, codeVal, hcOVal, hcGVal, hcAVal, hcUVal, year, gpd, gwl, swl, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                                else
                                {
                                    id = 1;
                                    name = "No Field(s) found for selected County/District/Municipality";
                                    code = "---";
                                    year = "---";
                                    hcOVal = "---";
                                    hcGVal = "---";
                                    hcAVal = "---";
                                    hcUVal = "---";
                                    year = "---";
                                    uT = DateTime.Now;
                                    iReply.Rows.Add(id, name, code, hcOVal, hcGVal, hcAVal, hcUVal, year, gpd, gwl, swl, uT);
                                    iReply.AcceptChanges();
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getFieldTableForOperatorByCountyForReviewer(Int32 CountyID, Int32 OperatorID)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn fldID = iReply.Columns.Add("fldID", typeof(Int32));
                DataColumn fldName = iReply.Columns.Add("fldName", typeof(String));
                DataColumn fldCode = iReply.Columns.Add("fldCode", typeof(String));
                DataColumn hcOil = iReply.Columns.Add("hcOil", typeof(String));
                DataColumn hcGas = iReply.Columns.Add("hcGas", typeof(String));
                DataColumn hcAssoc = iReply.Columns.Add("hcAssoc", typeof(String));
                DataColumn hcUnknown = iReply.Columns.Add("hcUnknown", typeof(String));
                DataColumn Year = iReply.Columns.Add("Year", typeof(String));
                DataColumn padCount = iReply.Columns.Add("padCount", typeof(Int32));
                DataColumn welCount = iReply.Columns.Add("welCount", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { fldID };
                Int32 id = -99, gpd = 0, gwl = 0;
                String name = String.Empty, code = String.Empty;
                Int32 hcO = -99, hcG = -99, hcA = -99, hcU = -99;
                String year = String.Empty, codeVal = String.Empty, hcOVal = String.Empty, hcGVal = String.Empty, hcAVal = String.Empty, hcUVal = String.Empty;
                DateTime uT = DateTime.Now;
                String selField = "SELECT [fldID], [fldName], [fldCode], [hcOil], [hcGas], [hcAssoc], [hcUnknown], [Year], [uTime] FROM [dmgField] WHERE [cntyID] = @cntyID ORDER BY [fldName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selField, dataCon);
                        getData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        code = readData.GetString(2);
                                        if (code.Equals("-99"))
                                        {
                                            codeVal = "---";
                                        }
                                        hcO = readData.GetInt32(3);
                                        if (hcO.Equals(-99))
                                        {
                                            hcOVal = "---";
                                        }
                                        else
                                        {
                                            hcOVal = getFieldOption(hcO);
                                        }
                                        hcG = readData.GetInt32(4);
                                        if (hcG.Equals(-99))
                                        {
                                            hcGVal = "---";
                                        }
                                        else
                                        {
                                            hcGVal = getFieldOption(hcG);
                                        }
                                        hcA = readData.GetInt32(5);
                                        if (hcA.Equals(-99))
                                        {
                                            hcAVal = "---";
                                        }
                                        else
                                        {
                                            hcAVal = getFieldOption(hcU);
                                        }
                                        hcU = readData.GetInt32(6);
                                        if (hcU.Equals(-99))
                                        {
                                            hcUVal = "---";
                                        }
                                        else
                                        {
                                            hcUVal = getFieldOption(hcU);
                                        }
                                        year = readData.GetString(7);
                                        if (String.IsNullOrEmpty(year) || year.Equals(-99))
                                        {
                                            year = "---";
                                        }
                                        gpd = getGlobalWellPadCountForOperatorByField(id, OperatorID);
                                        gwl = getGlobalWellCountForOperatorByField(id, OperatorID);
                                        uT = readData.GetDateTime(8);
                                        iReply.Rows.Add(id, name, codeVal, hcOVal, hcGVal, hcAVal, hcUVal, year, gpd, gwl, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getProviderFieldTableForOperatorByCountyForReviewer(Int32 CountyID, Int32 ProviderID)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn fldID = iReply.Columns.Add("fldID", typeof(Int32));
                DataColumn fldName = iReply.Columns.Add("fldName", typeof(String));
                DataColumn fldCode = iReply.Columns.Add("fldCode", typeof(String));
                DataColumn hcOil = iReply.Columns.Add("hcOil", typeof(String));
                DataColumn hcGas = iReply.Columns.Add("hcGas", typeof(String));
                DataColumn hcAssoc = iReply.Columns.Add("hcAssoc", typeof(String));
                DataColumn hcUnknown = iReply.Columns.Add("hcUnknown", typeof(String));
                DataColumn Year = iReply.Columns.Add("Year", typeof(String));
                DataColumn padCount = iReply.Columns.Add("padCount", typeof(Int32));
                DataColumn welCount = iReply.Columns.Add("welCount", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { fldID };
                Int32 id = -99, gpd = 0, gwl = 0;
                String name = String.Empty, code = String.Empty;
                Int32 hcO = -99, hcG = -99, hcA = -99, hcU = -99;
                String year = String.Empty, codeVal = String.Empty, hcOVal = String.Empty, hcGVal = String.Empty, hcAVal = String.Empty, hcUVal = String.Empty;
                DateTime uT = DateTime.Now;
                String selField = "SELECT [fldID], [fldName], [fldCode], [hcOil], [hcGas], [hcAssoc], [hcUnknown], [Year], [uTime] FROM [dmgField] WHERE [cntyID] = @cntyID ORDER BY [fldName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selField, dataCon);
                        getData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        code = readData.GetString(2);
                                        if (code.Equals("-99"))
                                        {
                                            codeVal = "---";
                                        }
                                        hcO = readData.GetInt32(3);
                                        if (hcO.Equals(-99))
                                        {
                                            hcOVal = "---";
                                        }
                                        else
                                        {
                                            hcOVal = getFieldOption(hcO);
                                        }
                                        hcG = readData.GetInt32(4);
                                        if (hcG.Equals(-99))
                                        {
                                            hcGVal = "---";
                                        }
                                        else
                                        {
                                            hcGVal = getFieldOption(hcG);
                                        }
                                        hcA = readData.GetInt32(5);
                                        if (hcA.Equals(-99))
                                        {
                                            hcAVal = "---";
                                        }
                                        else
                                        {
                                            hcAVal = getFieldOption(hcU);
                                        }
                                        hcU = readData.GetInt32(6);
                                        if (hcU.Equals(-99))
                                        {
                                            hcUVal = "---";
                                        }
                                        else
                                        {
                                            hcUVal = getFieldOption(hcU);
                                        }
                                        year = readData.GetString(7);
                                        if (String.IsNullOrEmpty(year) || year.Equals(-99))
                                        {
                                            year = "---";
                                        }
                                        gpd = getProviderGlobalWellPadCountForOperatorByField(id, ProviderID);
                                        gwl = getProviderGlobalWellCountForOperatorByField(id, ProviderID);
                                        uT = readData.GetDateTime(8);
                                        iReply.Rows.Add(id, name, codeVal, hcOVal, hcGVal, hcAVal, hcUVal, year, gpd, gwl, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getWellForWellPadTableForReviewer(Int32 WellPadID)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn welID = iReply.Columns.Add("welID", typeof(Int32));
                DataColumn welName = iReply.Columns.Add("welName", typeof(String));
                DataColumn field = iReply.Columns.Add("field", typeof(String));
                DataColumn county = iReply.Columns.Add("county", typeof(String));
                DataColumn state = iReply.Columns.Add("state", typeof(String));
                DataColumn spud = iReply.Columns.Add("spud", typeof(DateTime));
                DataColumn wstID = iReply.Columns.Add("wstID", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { welID };
                Int32 id = -99, fldID = -99, cntyID = -99, stID = -99, wsID = -99;
                String name = String.Empty, countyN = String.Empty, fieldN = String.Empty, stateN = String.Empty, jobName = String.Empty, wsStat = String.Empty;
                DateTime spudD = DateTime.Now, uT = DateTime.Now;
                String selData = "SELECT [welID], [welName], [cntyID], [fldID], [stID], [welSpudDate], [wstID], [uTime] FROM  [dmgWell] WHERE [wpdID] = @wpdID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        cntyID = readData.GetInt32(2);
                                        fldID = readData.GetInt32(3);
                                        stID = readData.GetInt32(4);
                                        spudD = readData.GetDateTime(5);
                                        wsID = readData.GetInt32(6);
                                        wsStat = getWellProcessStatus(wsID);
                                        uT = readData.GetDateTime(7);
                                        countyN = getCountyName(cntyID);
                                        fieldN = getFieldName(fldID);
                                        stateN = getStateName(stID);
                                        iReply.Rows.Add(id, name, countyN, fieldN, stateN, spudD, wsStat, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWellDetailTableForReviewer(Int32 WellID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn welID = iReply.Columns.Add("welID", typeof(Int32));
                DataColumn welPermit = iReply.Columns.Add("welPermit", typeof(String));
                DataColumn welAPI = iReply.Columns.Add("welAPI", typeof(String));
                DataColumn welUWI = iReply.Columns.Add("welUWI", typeof(String));
                DataColumn welLongitude = iReply.Columns.Add("welLongitude", typeof(String));
                DataColumn welLatitude = iReply.Columns.Add("welLatitude", typeof(String));
                String selData = "SELECT [welID], [welPermit], [welAPI], [welUWI], [welLongitude], [welLngCardinal], [welLatitude], [welLatCardinal] FROM [dmgWell] WHERE [welID] = @welID";
                Int32 id = -99, latC = -99, lngC = -99;
                Decimal lngi = -99.000000000M, lati = -99.000000000M;
                String perm = String.Empty, api = String.Empty, uwi = String.Empty, ltC = String.Empty, lnC = String.Empty, latitude = String.Empty, longitude = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        perm = readData.GetString(1);
                                        api = readData.GetString(2);
                                        if (String.IsNullOrEmpty(api))
                                        {
                                            api = "---";
                                        }
                                        uwi = readData.GetString(3);
                                        if (String.IsNullOrEmpty(uwi))
                                        {
                                            uwi = "---";
                                        }
                                        lngi = readData.GetDecimal(4);
                                        lngC = readData.GetInt32(5);
                                        lnC = getCardinalName(lngC);
                                        longitude = String.Format("{0} ({1})", lngi, lnC);
                                        lati = readData.GetDecimal(6);
                                        latC = readData.GetInt32(7);
                                        ltC = getCardinalName(latC);
                                        latitude = String.Format("{0} ({1})", lati, ltC);
                                        iReply.Rows.Add(id, perm, api, uwi, longitude, latitude);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellPadCountForOperatorByField(Int32 FieldID, Int32 OperatorID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(wpdID) FROM [dmgWellPad] WHERE [fldID] = @fldID AND [optrID] = @optrID";
                String DemogDBString = WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString();
                using (SqlConnection dataCon = new SqlConnection(DemogDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@fldID", SqlDbType.Int).Value = FieldID.ToString();
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getProviderGlobalWellPadCountForOperatorByField(Int32 FieldID, Int32 ProviderID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(wpdID) FROM [dmgWellPad] WHERE [fldID] = @fldID AND [clntID] = @clntID";
                String DemogDBString = WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString();
                using (SqlConnection dataCon = new SqlConnection(DemogDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@fldID", SqlDbType.Int).Value = FieldID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ProviderID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellCountForOperatorByField(Int32 FieldID, Int32 OperatorID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [dmgWell] WHERE [fldID] = @fldID AND [optrID] = @optrID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@fldID", SqlDbType.Int).Value = FieldID.ToString();
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getProviderGlobalWellCountForOperatorByField(Int32 FieldID, Int32 ProviderID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [dmgWell] WHERE [fldID] = @fldID AND [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@fldID", SqlDbType.Int).Value = FieldID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ProviderID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getSiloWellCountByField(Int32 FieldID, Int32 OperatorID, Int32 ClientID, String ClientDBString)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [Well] WHERE [fldID] = @fldID AND [optrID] = @optrID AND [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@fldID", SqlDbType.Int).Value = FieldID.ToString();
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static String getFieldOption(Int32 OptionID)
        {
            try
            {
                String iReply = String.Empty;
                String name = String.Empty, desc = String.Empty;
                String selData = "SELECT [hcoName], [hcoDesc] FROM [dmgHCOption] WHERE [hcoID] = @hcoID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@hcoID", SqlDbType.Int).Value = OptionID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        name = readData.GetString(0);
                                        desc = readData.GetString(1);
                                        iReply = String.Format("{0} - {1}", name, desc);
                                    }
                                }
                                else
                                {
                                    iReply = "---";
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getGCOrientationList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [gcoID], [gcoValue] FROM [dmgGCOrientation]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getGCOrientationName(Int32 GCOrientationID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [gcoValue] FROM [dmgGCOrientation] WHERE [gcoID] = @gcoID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@gcoID", SqlDbType.Int).Value = GCOrientationID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getGCOrientationTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [gcoID], [gcoValue] FROM [dmgGCOrientation]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Rows.Add(id, name);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getGenderList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String value = String.Empty;
                String selData = "SELECT [gndID], [gndValue] FROM [dmgGender] ORDER BY [gndValue]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Add(id, value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch(Exception ex)
            {throw new System.Exception(ex.ToString());}
        }

        public static System.Data.DataTable getGenderTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn gndID = iReply.Columns.Add("gndID", typeof(Int32));
                DataColumn gndValue = iReply.Columns.Add("gndValue", typeof(String));
                Int32 id = -99;
                String value = String.Empty;
                String selData = "SELECT [gndID], [gndValue] FROM [dmgGender] ORDER BY [gndValue]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Rows.Add(id, value);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getGenderValue(Int32 GenderId)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [gndValue] FROM [dmgGender] WHERE [gndID] = @gndID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@gndID", SqlDbType.Int).Value = GenderId.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }            
        }

        public static Int32 getGenderIDFromValue(String GenderValue)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT [gndID] FROM [dmgGender] WHERE [gndValue] = @gndValue";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@gndValue", SqlDbType.Int).Value = GenderValue.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellCount()
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(welID) FROM [dmgWell]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);                                        
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellCountForOperator(Int32 OperatorID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(welID) FROM [dmgWell] WHERE [optrID] = @optrID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellCountForOperatorByRegion(Int32 RegionID, Int32 OperatorID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(welID) FROM [dmgWell] WHERE [regID] = @regID AND [optrID] = @optrID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@regID", SqlDbType.Int).Value = RegionID.ToString();
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getProviderGlobalWellCountForOperatorByRegion(Int32 RegionID, Int32 ProviderID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(welID) FROM [dmgWell] WHERE [regID] = @regID AND [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@regID", SqlDbType.Int).Value = RegionID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ProviderID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getProviderGlobalWellCountForOperator(Int32 ProviderID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(welID) FROM [dmgWell] WHERE [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ProviderID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellPadCountForOperator(Int32 OperatorID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(wpdID) FROM [dmgWellPad] WHERE [optrID] = @optrID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getGlobalWellPadName(Int32 WellPadID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [welPadName] FROM [dmgWellPad] WHERE [wpdID] = @wpdID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getGlobalWellName(Int32 WellID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [welName] FROM [dmgWell] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getGlobalWellTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn welID = iReply.Columns.Add("welID", typeof(Int32));
                DataColumn welName = iReply.Columns.Add("welName", typeof(String));
                DataColumn welClient = iReply.Columns.Add("welClient", typeof(String));
                DataColumn welOptr = iReply.Columns.Add("welOptr", typeof(String));
                DataColumn welFld = iReply.Columns.Add("welFld", typeof(String));
                DataColumn welCnty = iReply.Columns.Add("welCnty", typeof(String));
                DataColumn welSt = iReply.Columns.Add("welSt", typeof(String));
                DataColumn welCty = iReply.Columns.Add("welCty", typeof(String));
                DataColumn welSR = iReply.Columns.Add("welSR", typeof(String));
                DataColumn welR = iReply.Columns.Add("welR", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [welID], [welName], [clntID], [optrID], [fldID], [cntyID], [stID], [ctyID], [sregID], [regID], [uTime] FROM [dmgWell] ORDER BY [welName]";
                Int32 id = 0, cID = 0, oID = 0, fID = 0, dID = 0, st = 0, cty = 0, rID = 0, srID = 0;
                String name = String.Empty, client = String.Empty, opr = String.Empty, field = String.Empty, district = String.Empty, state = String.Empty, country = String.Empty, subRegion = String.Empty, region = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        cID = readData.GetInt32(2);
                                        client = ACC.getClientName(cID);
                                        oID = readData.GetInt32(3);
                                        opr = ACC.getClientName(oID);
                                        fID = readData.GetInt32(4);
                                        field = getFieldName(fID);
                                        dID = readData.GetInt32(5);
                                        district = getCountyName(dID);
                                        st = readData.GetInt32(6);
                                        state = getStateName(st);
                                        cty = readData.GetInt32(7);
                                        country = getCountryName(cty);
                                        srID = readData.GetInt32(8);
                                        subRegion = getSubRegionName(srID);
                                        rID = readData.GetInt32(9);
                                        region = getRegionName(rID);
                                        uT = readData.GetDateTime(10);
                                        iReply.Rows.Add(id, name, client, opr, field, district, state, country, subRegion, region, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getGlobalWellTableForServiceProvider(Int32 ClientID)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn welID = iReply.Columns.Add("welID", typeof(Int32));
                DataColumn welName = iReply.Columns.Add("welName", typeof(String));
                DataColumn welLongitude = iReply.Columns.Add("welLongitude", typeof(Decimal));
                DataColumn welLatitude = iReply.Columns.Add("welLatitude", typeof(Decimal));
                DataColumn welFld = iReply.Columns.Add("welFld", typeof(String));
                DataColumn welCnty = iReply.Columns.Add("welCnty", typeof(String));
                DataColumn welSt = iReply.Columns.Add("welSt", typeof(String));
                DataColumn welCty = iReply.Columns.Add("welCty", typeof(String));
                DataColumn welAPI = iReply.Columns.Add("welAPI", typeof(String));
                DataColumn welUWI = iReply.Columns.Add("welUWI", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [welID], [welName], [welLongitude], [welLatitude], [fldID], [cntyID], [stID], [ctyID], [welAPI], [welUWI], [uTime] FROM [dmgWell] WHERE [clntID] = @clntID ORDER BY [welName]";
                Int32 id = 0, fID = 0, dID = 0, st = 0, cty = 0;
                Decimal lati = -99.000000000M, longi = -99.000000000M;
                String name = String.Empty, client = String.Empty, opr = String.Empty, field = String.Empty, district = String.Empty, state = String.Empty, country = String.Empty, subRegion = String.Empty, region = String.Empty, api = String.Empty, uwi = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        longi = readData.GetDecimal(2);
                                        lati = readData.GetDecimal(3);
                                        fID = readData.GetInt32(4);
                                        field = getFieldName(fID);
                                        dID = readData.GetInt32(5);
                                        district = getCountyName(dID);
                                        st = readData.GetInt32(6);
                                        state = getStateName(st);
                                        cty = readData.GetInt32(7);                                        
                                        country = getCountryName(cty);
                                        uwi = readData.GetString(8);
                                        api = readData.GetString(9);
                                        uT = readData.GetDateTime(10);
                                        iReply.Rows.Add(id, name, longi, lati, field, district, state, country, uwi, api, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getGlobalOperatorWellTable(Int32 WellID)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn welID = iReply.Columns.Add("welID", typeof(Int32));
                DataColumn welFld = iReply.Columns.Add("welFld", typeof(String));
                DataColumn welCnty = iReply.Columns.Add("welCnty", typeof(String));
                DataColumn welSt = iReply.Columns.Add("welSt", typeof(String));
                DataColumn welCty = iReply.Columns.Add("welCty", typeof(String));
                DataColumn welSR = iReply.Columns.Add("welSR", typeof(String));
                DataColumn welR = iReply.Columns.Add("welR", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [welID], [fldID], [cntyID], [stID], [ctyID], [sregID], [regID], [uTime] FROM [dmgWell] WHERE [welID] = @welID";
                Int32 id = 0, fID = 0, dID = 0, st = 0, cty = 0, rID = 0, srID = 0;
                String field = String.Empty, district = String.Empty, state = String.Empty, country = String.Empty, subRegion = String.Empty, region = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        fID = readData.GetInt32(1);
                                        field = getFieldName(fID);
                                        dID = readData.GetInt32(2);
                                        district = getCountyName(dID);
                                        st = readData.GetInt32(3);
                                        state = getStateName(st);
                                        cty = readData.GetInt32(4);
                                        country = getCountryName(cty);
                                        srID = readData.GetInt32(5);
                                        subRegion = getSubRegionName(srID);
                                        rID = readData.GetInt32(6);
                                        region = getRegionName(rID);
                                        uT = readData.GetDateTime(7);
                                        iReply.Rows.Add(id, field, district, state, country, subRegion, region, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getGlobalFieldWellTable(Int32 FieldID)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn welID = iReply.Columns.Add("welID", typeof(Int32));
                DataColumn welName = iReply.Columns.Add("welName", typeof(String));
                DataColumn welClient = iReply.Columns.Add("welClient", typeof(String));
                DataColumn welOptr = iReply.Columns.Add("welOptr", typeof(String));
                DataColumn welCnty = iReply.Columns.Add("welCnty", typeof(String));
                DataColumn welSt = iReply.Columns.Add("welSt", typeof(String));
                DataColumn welCty = iReply.Columns.Add("welCty", typeof(String));
                DataColumn welSR = iReply.Columns.Add("welSR", typeof(String));
                DataColumn welR = iReply.Columns.Add("welR", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [welID], [welName], [clntID], [optrID], [cntyID], [stID], [ctyID], [sregID], [regID], [uTime] FROM [dmgWell] WHERE [fldID] = @fldID ORDER BY [welName]";
                Int32 id = 0, cID = 0, oID = 0, dID = 0, st = 0, cty = 0, rID = 0, srID = 0;
                String name = String.Empty, client = String.Empty, opr = String.Empty, field = String.Empty, district = String.Empty, state = String.Empty, country = String.Empty, subRegion = String.Empty, region = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@fldID", SqlDbType.Int).Value = FieldID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        cID = readData.GetInt32(2);
                                        client = ACC.getClientName(cID);
                                        oID = readData.GetInt32(3);
                                        opr = ACC.getClientName(oID);
                                        dID = readData.GetInt32(4);
                                        district = getCountyName(dID);
                                        st = readData.GetInt32(5);
                                        state = getStateName(st);
                                        cty = readData.GetInt32(6);
                                        country = getCountryName(cty);
                                        srID = readData.GetInt32(7);
                                        subRegion = getSubRegionName(srID);
                                        rID = readData.GetInt32(8);
                                        region = getRegionName(rID);
                                        uT = readData.GetDateTime(9);
                                        iReply.Rows.Add(id, name, client, opr, district, state, country, subRegion, region, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getGTQualifierTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn gtqID = iReply.Columns.Add("gtqID", typeof(Int32));
                DataColumn gtqValue = iReply.Columns.Add("gtqValue", typeof(String));
                String selData = "SELECT [gtqID], [gtqValue] FROM [dmgGTQualifier] ORDER BY [gtqValue]";
                Int32 id = 0;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Rows.Add(id, value);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getHCOptionsTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn hcoID = iReply.Columns.Add("hcoID", typeof(Int32));
                DataColumn hcoName = iReply.Columns.Add("hcoName", typeof(String));
                DataColumn hcoDesc = iReply.Columns.Add("hcoDesc", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                Int32 id = -99;
                String value = String.Empty, desc = String.Empty;
                DateTime upd = DateTime.Now;
                String selStat = "SELECT [hcoID], [hcoName], [hcoDesc], [uTime] FROM [dmgHCOption]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selStat, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        desc = readData.GetString(2);
                                        upd = readData.GetDateTime(3);
                                        iReply.Rows.Add(id, value, desc, upd);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getHCOptionList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selStat = "SELECT [hcoID], [hcoName] FROM [dmgHCOption]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selStat, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getIFRValueTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn ifrValID = iReply.Columns.Add("ifrValID", typeof(Int32));
                DataColumn ifrValue = iReply.Columns.Add("ifrValue", typeof(String));
                String selData = "SELECT [ifrValID], [ifrValue] FROM dmgIFRValue ORDER BY [ifrValue]";
                Int32 id = 0;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Rows.Add(id, value);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static String getImagePurposeName(Int32 ImagePurposeID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [imgpValue] FROM dmgImagePurpose WHERE [imgpID] = @imgpID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@imgpID", SqlDbType.Int).Value = ImagePurposeID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                                else
                                {
                                    iReply = "---";
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getImagePurposeList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [imgpID], [imgpValue] FROM dmgImagePurpose ORDER BY [imgpValue]";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                                else
                                {
                                    iReply.Add(1, "---");
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getImageColorName(Int32 ImageColorID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [lclrValue] FROM dmgColorType WHERE [lclrID] = @lclrID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@lclrID", SqlDbType.Int).Value = ImageColorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                                else
                                {
                                    iReply = "---";
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getImageColorList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [lclrID], [lclrValue] FROM dmgColorType ORDER BY [lclrValue] ";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                                else
                                {
                                    iReply.Add(1, "---");
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getImageTypeName(Int32 ImageTypeID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [imgtypValue] FROM dmgImageType WHERE [imgtypID] = @imgtypID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@imgtypID", SqlDbType.Int).Value = ImageTypeID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                                else
                                {
                                    iReply = "---";
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getImageTypeList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [imgtypID], [imgtypValue] FROM dmgImageType ORDER BY [imgtypValue]";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                                else
                                {
                                    iReply.Add(1, "---");
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getImagePurposeTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn imgpID = iReply.Columns.Add("imgpID", typeof(Int32));
                DataColumn imgpValue = iReply.Columns.Add("imgpValue", typeof(String));
                String selData = "SELECT [imgpID], [imgpValue] FROM dmgImagePurpose ORDER BY [imgpValue]";
                Int32 id = 0;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Rows.Add(id, value);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getImageTypeTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn imgtypID = iReply.Columns.Add("imgtypID", typeof(Int32));
                DataColumn imgtypValue = iReply.Columns.Add("imgtypValue", typeof(String));
                String selData = "SELECT [imgtypID], [imgtypValue] FROM dmgImageType ORDER BY [imgtypID]";
                Int32 id = 0;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Rows.Add(id, value);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getJobClassTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn jclID = iReply.Columns.Add("jclID", typeof(Int32));
                DataColumn jclName = iReply.Columns.Add("jclName", typeof(String));
                String selData = "SELECT [jclID], [jclName] FROM dmgJobClass ORDER BY [jclName]";
                Int32 id = 0;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Rows.Add(id, value);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getConvergenceTable()
        {
            try
            {
                System.Data.DataTable iReply = new DataTable();
                DataColumn jcID = iReply.Columns.Add("jcID", typeof(Int32));
                DataColumn jcName = iReply.Columns.Add("jcName", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [jcID], [jcName], [uTime] FROM [dmgJobConvergence] ORDER BY [jcName]";
                Int32 id = 0;
                String name = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try                    
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, name, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getLatCardinals()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [cID], [compassPoint], [abbreviation] FROM [dmgCompass] WHERE [cID] = '1' OR [cID] = '17'";
                Int32 id = 0;
                String name = String.Empty, abbr = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        abbr = readData.GetString(2);
                                        iReply.Add(id, abbr + " - ( " + name + " )");
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getLngCardinals()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [cID], [compassPoint], [abbreviation] FROM [dmgCompass] WHERE [cID] = 9 OR [cID] = 25";
                Int32 id = 0;
                String name = String.Empty, abbr = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        abbr = readData.GetString(2);
                                        iReply.Add(id, abbr + " - ( " + name + " )");
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getLCRefTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn refID = iReply.Columns.Add("refID", typeof(Int32));
                DataColumn refName = iReply.Columns.Add("refName", typeof(String));
                DataColumn refValue = iReply.Columns.Add("refValue", typeof(Decimal));
                String selData = "SELECT [refID], [refName], [refValue] FROM dmgLongCollarSurveyDirReference ORDER BY [refName]";
                Int32 id = 0;
                String name = String.Empty;
                Decimal size = -99.0000M;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        size = readData.GetDecimal(2);
                                        iReply.Rows.Add(id, size, size);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getLenUnitsTable()
        {
            try
            {
                System.Data.DataTable iReply = new DataTable();
                DataColumn luID = iReply.Columns.Add("luID", typeof(Int32));
                DataColumn luName = iReply.Columns.Add("luName", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [luID], [luName], [cTime], [uTime] FROM [dmgLengthUnit] ORDER BY [luName]";
                Int32 id = 0;
                String name = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, name, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getLogTypeTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn logID = iReply.Columns.Add("logID", typeof(Int32));
                DataColumn logType = iReply.Columns.Add("logType", typeof(String));
                String selData = "SELECT [logID], [logType] FROM dmgLogType ORDER BY [logType]";
                Int32 id = 0;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Rows.Add(id, value);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getMMDLTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn mmdlID = iReply.Columns.Add("mmdlID", typeof(Int32));
                DataColumn mmdlName = iReply.Columns.Add("mmdlName", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [mmdlID], [mmdlName], [uTime] FROM [dmgMagModel] ORDER BY [mmdlName]";
                Int32 id = 0;
                String name = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, name, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getMagUnitsTable()
        {
            try
            {
                System.Data.DataTable iReply = new DataTable();
                DataColumn muID = iReply.Columns.Add("muID", typeof(Int32));
                DataColumn muName = iReply.Columns.Add("muName", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [muID], [muName], [uTime] FROM [dmgMagUnit] ORDER BY [muName]";
                Int32 id = 0;
                String name = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, name, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getMagUnitsList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [muID], [muName] FROM [dmgMagUnit] ORDER BY [muName]";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }            
        }

        public static DataTable getMtrPoleStrengthTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn mpsID = iReply.Columns.Add("mpsID", typeof(Int32));
                DataColumn mpsSize = iReply.Columns.Add("mpsSize", typeof(Decimal));
                DataColumn mpsValue = iReply.Columns.Add("mpsValue", typeof(Int32));
                String selData = "SELECT [mpsID], [mpsSize], [mpsValue] FROM dmgMtrPoleStrength ORDER BY [mpsSize]";
                Int32 id = 0, value = 0;
                Decimal size = -99.00M;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        size = readData.GetDecimal(1);
                                        value = readData.GetInt32(2);
                                        iReply.Rows.Add(id, size, value);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static ArrayList getngzDataList()
        {
            try
            {
                ArrayList iReply = new ArrayList();
                String selData = "SELECT [ngzValue] FROM dmgNGZone WHERE [ngzID] = @ngzID";
                String selData2 = "SELECT [ngzValue] FROM dmgNGZone WHERE [ngzID] = @ngzID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    dataCon.Open();
                    SqlCommand getData = new SqlCommand(selData, dataCon);
                    getData.Parameters.AddWithValue("@ngzID", SqlDbType.Int).Value = 1; //Read Inclination criterion value
                    using (SqlDataReader readData = getData.ExecuteReader())
                    {
                        try
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    iReply.Add(readData.GetInt32(0));
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            readData.Close();
                            readData.Dispose();
                        }
                    }
                    SqlCommand getData2 = new SqlCommand(selData2, dataCon);
                    getData2.Parameters.AddWithValue("@ngzID", SqlDbType.Int).Value = 2; //Read Azimuth criterion value
                    using (SqlDataReader readData = getData2.ExecuteReader())
                    {
                        try
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    iReply.Add(readData.GetInt32(0));
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            readData.Close();
                            readData.Dispose();
                        }
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getNGZTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn ngzID = iReply.Columns.Add("ngzID", typeof(Int32));
                DataColumn ngzCriteria = iReply.Columns.Add("ngzCriteria", typeof(String));
                DataColumn ngzValue = iReply.Columns.Add("ngzValue", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [ngzID], [ngzCriteria], [ngzValue], [uTime] FROM dmgNGZone ORDER BY [ngzCriteria]";
                Int32 id = 0, ngz = 0;
                String value = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        ngz = readData.GetInt32(2);
                                        uT = readData.GetDateTime(3);
                                        iReply.Rows.Add(id, value, ngz, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getOprStatusTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn osID = iReply.Columns.Add("osID", typeof(Int32));
                DataColumn osValue = iReply.Columns.Add("osValue", typeof(String));
                String selData = "SELECT [osID], [osValue] FROM dmgOprStatus ORDER BY [osValue]";
                Int32 id = 0;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Rows.Add(id, value);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getProjTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn projID = iReply.Columns.Add("projID", typeof(Int32));
                DataColumn projName = iReply.Columns.Add("projName", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [projID], [projName], [cTime], [uTime] FROM [dmgProjection] ORDER BY [projName]";
                Int32 id = 0;
                String name = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, name, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static String getRegionName(Int32 RegionID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [grName] FROM dmgGeoRegion WHERE [grID] = @grID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@grID", SqlDbType.Int).Value = RegionID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    iReply = readData.GetString(0);
                                }
                            }
                            else
                            {
                                iReply = "---";
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getRegionIDFromName(String MajorRegionName)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT [grID] FROM [dmgGeoRegion] WHERE [grName] = @grName";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@grName", SqlDbType.Int).Value = MajorRegionName.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    iReply = readData.GetInt32(0);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getRegionList()
        {
            try
            {
                Int32 regID = -99;
                String regName = null;

                Dictionary<int, string> regionList = new Dictionary<int, string>();
                String selRegion = "SELECT [grID], [grName] FROM [dmgGeoRegion] ORDER BY [grName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getRegion = new SqlCommand(selRegion, dataCon);
                        using (SqlDataReader readRegion = getRegion.ExecuteReader())
                        {
                            try
                            {
                                if (readRegion.HasRows)
                                {
                                    while (readRegion.Read())
                                    {
                                        regID = readRegion.GetInt32(0);
                                        regName = readRegion.GetString(1);
                                        regionList.Add(regID, regName);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readRegion.Close();
                                readRegion.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return regionList;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getGlobalRegionListWithAssetCounts(Int32 ClientID, String ClientDBString)
        {
            try
            {
                Int32 regID = -99, gpd = 0, gwl = 0, swl = 0;
                String regName = String.Empty, regValue = String.Empty;
                Dictionary<Int32, String> regionList = new Dictionary<Int32, String>();
                String selRegion = "SELECT [grID], [grName] FROM dmgGeoRegion ORDER BY [grName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getRegion = new SqlCommand(selRegion, dataCon);
                        using (SqlDataReader readRegion = getRegion.ExecuteReader())
                        {
                            try
                            {
                                if (readRegion.HasRows)
                                {
                                    while (readRegion.Read())
                                    {
                                        regID = readRegion.GetInt32(0);
                                        regName = readRegion.GetString(1);
                                        gpd = getGlobalWellPadCountByRegion(regID);
                                        gwl = getGlobalWellCountByRegion(regID);
                                        swl = getSiloWellCountByRegion(regID, ClientID, ClientDBString);
                                        regValue = String.Format("{0} --- ( Global Well Pad: {1}; Global Well/Lateral: {2}; SMART Well/Lateral: {3} )", regName, gpd, gwl, swl);
                                        regionList.Add(regID, regValue);
                                    }
                                }
                                else
                                {
                                    regionList.Add(0, " --- No Major Geographic Regions found --- ");
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readRegion.Close();
                                readRegion.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return regionList;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellPadCountByRegion(Int32 RegionID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(wpdID) FROM [dmgWellPad] WHERE [regID] = @regID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@regID", SqlDbType.Int).Value = RegionID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellCountByRegion(Int32 RegionID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(welID) FROM [dmgWell] WHERE [regID] = @regID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@regID", SqlDbType.Int).Value = RegionID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getSiloWellCountByRegion(Int32 RegionID, Int32 ClientID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(welID) FROM [Well] WHERE [regID] = @regID AND [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@regID", SqlDbType.Int).Value = RegionID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getRegionsTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn grID = iReply.Columns.Add("grID", typeof(Int32));
                DataColumn grName = iReply.Columns.Add("grName", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { grID };
                Int32 id = -99;
                String name = String.Empty;
                DateTime uT = DateTime.Now;                
                
                String selRegion = "SELECT [grID], [grName], [uTime] FROM [dmgGeoRegion] ORDER BY [grName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selRegion, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, name, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                                else
                                {
                                    id = 1;
                                    name = "No Major Geographic Region found. (This is an Error. Please report to FE Support";
                                    iReply.Rows.Add(id, name, uT);
                                    iReply.AcceptChanges();
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }                        
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getRegionsWithCountTable(Int32 OperatorID, Int32 ClientID, String ClientDBString)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn grID = iReply.Columns.Add("grID", typeof(Int32));
                DataColumn grName = iReply.Columns.Add("grName", typeof(String));
                DataColumn padCount = iReply.Columns.Add("padCount", typeof(Int32));
                DataColumn welCount = iReply.Columns.Add("welCount", typeof(Int32));
                DataColumn swlCount = iReply.Columns.Add("swlCount", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { grID };
                Int32 id = -99, gpdCount = 0, gpwCount = 0, swCount = 0;
                String name = String.Empty;
                DateTime uT = DateTime.Now;

                String selRegion = "SELECT [grID], [grName], [uTime] FROM [dmgGeoRegion] ORDER BY [grName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selRegion, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        gpdCount = getGlobalWellPadCountForRegion(id, OperatorID);
                                        gpwCount = getGlobalWellCountForOperatorByRegion(id, OperatorID);
                                        swCount = getSiloWellCountForRegion(id, OperatorID, ClientID, ClientDBString);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, name, gpdCount, gpwCount, swCount, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                                else
                                {
                                    id = 1;
                                    name = "No Major Geographic Region found. (This is an Error. Please report to FE Support";
                                    iReply.Rows.Add(id, name, uT);
                                    iReply.AcceptChanges();
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getRegionsWithCountTableForReviewers(Int32 OperatorID)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn grID = iReply.Columns.Add("grID", typeof(Int32));
                DataColumn grName = iReply.Columns.Add("grName", typeof(String));
                DataColumn padCount = iReply.Columns.Add("padCount", typeof(Int32));
                DataColumn welCount = iReply.Columns.Add("welCount", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { grID };
                Int32 id = -99, gpdCount = 0, gpwCount = 0;
                String name = String.Empty;
                DateTime uT = DateTime.Now;

                String selRegion = "SELECT [grID], [grName], [uTime] FROM [dmgGeoRegion] ORDER BY [grName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selRegion, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        gpdCount = getGlobalWellPadCountForRegion(id, OperatorID);
                                        gpwCount = getGlobalWellCountForOperatorByRegion(id, OperatorID);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, name, gpdCount, gpwCount, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                                else
                                {
                                    id = 1;
                                    name = "No Major Geographic Region found. (This is an Error. Please report to FE Support";
                                    iReply.Rows.Add(id, name, uT);
                                    iReply.AcceptChanges();
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getProviderRegionsWithCountTableForReviewers(Int32 ProviderID)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn grID = iReply.Columns.Add("grID", typeof(Int32));
                DataColumn grName = iReply.Columns.Add("grName", typeof(String));
                DataColumn padCount = iReply.Columns.Add("padCount", typeof(Int32));
                DataColumn welCount = iReply.Columns.Add("welCount", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { grID };
                Int32 id = -99, gpdCount = 0, gpwCount = 0;
                String name = String.Empty;
                DateTime uT = DateTime.Now;

                String selRegion = "SELECT [grID], [grName], [uTime] FROM [dmgGeoRegion] ORDER BY [grName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selRegion, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        gpdCount = getProviderGlobalWellPadCountForRegion(id, ProviderID);
                                        gpwCount = getProviderGlobalWellCountForOperatorByRegion(id, ProviderID);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, name, gpdCount, gpwCount, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                                else
                                {
                                    id = 1;
                                    name = "No Major Geographic Region found. (This is an Error. Please report to FE Support";
                                    iReply.Rows.Add(id, name, uT);
                                    iReply.AcceptChanges();
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellPadCountForRegion(Int32 RegionID, Int32 OperatorID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(wpdID) FROM [dmgWellPad] WHERE [regID] = @regID AND [optrID] = @optrID";
                String DemogDBString = WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString();
                using (SqlConnection dataCon = new SqlConnection(DemogDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@regID", SqlDbType.Int).Value = RegionID.ToString();
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getProviderGlobalWellPadCountForRegion(Int32 RegionID, Int32 ProviderID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(wpdID) FROM [dmgWellPad] WHERE [regID] = @regID AND [clntID] = @clntID";
                String DemogDBString = WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString();
                using (SqlConnection dataCon = new SqlConnection(DemogDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@regID", SqlDbType.Int).Value = RegionID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ProviderID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getProviderGlobalWellPadCount(Int32 ProviderID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(wpdID) FROM [dmgWellPad] WHERE [clntID] = @clntID";
                String DemogDBString = WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString();
                using (SqlConnection dataCon = new SqlConnection(DemogDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ProviderID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getSiloWellCountForRegion(Int32 RegionID, Int32 OperatorID, Int32 ClientID, String ClientDBString)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [Well] WHERE [regID] = @regID AND [optrID] = @optrID AND [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@regID", SqlDbType.Int).Value = RegionID.ToString();
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getReportTypeList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String value = String.Empty;
                String selData = "SELECT [rtID], [rtValue] FROM [dmgReportTypes] ORDER BY [rtValue] ASC";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Add(id, value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getReportTypeTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn rtID = iReply.Columns.Add("rtID", typeof(Int32));
                DataColumn rtValue = iReply.Columns.Add("rtValue", typeof(String));
                Int32 id = -99;
                String value = String.Empty;
                String selData = "SELECT [rtID], [rtValue] FROM [dmgReportTypes] ORDER BY [rtValue] ASC";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Rows.Add(id, value);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getRunNameTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn rnamID = iReply.Columns.Add("rnamID", typeof(Int32));
                DataColumn rnamValue = iReply.Columns.Add("rnamValue", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [rnamID], [rnamValue], [uTime] FROM dmgRunName ORDER BY [rnamID]";
                Int32 id = 0;
                String value = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, value, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getRunSectionList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [rnsID], [rnsName] FROM [dmgRunSection] ORDER BY [rnsName]";
                Int32 id = 0;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Add(id, value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getRunSectionName(Int32 RunSectionID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [rnsName] FROM [dmgRunSection] WHERE [rnsID] = @rnsID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@rnsID", SqlDbType.Int).Value = RunSectionID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                                else
                                {
                                    iReply = "---";
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
        
        public static DataTable getRunSectionTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn rnsID = iReply.Columns.Add("rnsID", typeof(Int32));
                DataColumn rnsName = iReply.Columns.Add("rnsName", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [rnsID], [rnsName], [uTime] FROM dmgRunSection ORDER BY [rnsName]";
                Int32 id = 0;
                String value = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, value, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getRunStatusListForClients()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [dmgstID], [dmgStatus] FROM [dmgStatus] WHERE [dmgstID] = 1 OR [dmgstID] = 2 ORDER BY [dmgStatus]";
                Int32 id = 0;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Add(id, value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getRunStatusTableForClients()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn dmgstID = iReply.Columns.Add("dmgstID", typeof(Int32));
                DataColumn dmgStatus = iReply.Columns.Add("dmgStatus", typeof(String));
                String selData = "SELECT [dmgstID], [dmgStatus] FROM [dmgStatus] WHERE [dmgstID] = 1 AND [dmgstID] = 2 ORDER BY [rnsName]";
                Int32 id = 0;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Rows.Add(id, value);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getServiceAvailability(Int32 ServiceID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT [srvStatID] FROM [dmgService] WHERE ([srvID] = @srvID)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getServiceStat(Int32 ServiceGroupID)
        {
            try
            {
                Int32 iReply = -99;
                Int32 srvStat = -99;
                Int32 grpStat = -99;
                String selStat = "SELECT [srvStatID] FROM [dmgService] WHERE [srvGrpID] = @srvGrpID";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();

                        SqlCommand getStat = new SqlCommand(selStat, dataCon);
                        getStat.Parameters.AddWithValue("@srvGrpID", SqlDbType.Int).Value = ServiceGroupID.ToString();
                        using (SqlDataReader readStat = getStat.ExecuteReader())
                        {
                            try
                            {
                                if (readStat.HasRows)
                                {
                                    while (readStat.Read())
                                    {
                                        srvStat = readStat.GetInt32(0);
                                        if (!srvStat.Equals(grpStat))
                                        {
                                            grpStat = srvStat;
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readStat.Close();
                                readStat.Dispose();
                            }
                        }
                        iReply = grpStat;
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }

                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getSFileTypeTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn sftID = iReply.Columns.Add("sftID", typeof(Int32));
                DataColumn sftName = iReply.Columns.Add("sftName", typeof(String));
                DataColumn sftDesc = iReply.Columns.Add("sftDesc", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [sftID], [sftName], [sftDesc], [uTime] FROM dmgSrvyFileType ORDER BY [sftName]";
                Int32 id = 0;
                String value = String.Empty, desc = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        desc = readData.GetString(2);
                                        uT = readData.GetDateTime(3);
                                        iReply.Rows.Add(id, value, desc, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getSubRegionName(Int32 SubRegionID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [gsrName] FROM dmgGeoSubRegion WHERE [gsrID] = @gsrID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@gsrID", SqlDbType.Int).Value = SubRegionID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                                else
                                {
                                    iReply = "---";
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getSubRegionList(Int32 RegionID)
        {
            try
            {
                Int32 sregID = -99;
                String sregName = null;

                Dictionary<int, string> sregionList = new Dictionary<int, string>();
                String selSRegion = "SELECT [gsrID], [gsrName] FROM dmgGeoSubRegion WHERE [grID] = @grID ORDER BY [gsrName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getSRegion = new SqlCommand(selSRegion, dataCon);
                        getSRegion.Parameters.AddWithValue("@grID", SqlDbType.Int).Value = RegionID.ToString();
                        using (SqlDataReader readSRegion = getSRegion.ExecuteReader())
                        {
                            try
                            {
                                if (readSRegion.HasRows)
                                {
                                    while (readSRegion.Read())
                                    {
                                        sregID = readSRegion.GetInt32(0);
                                        sregName = readSRegion.GetString(1);
                                        sregionList.Add(sregID, sregName);
                                    }
                                }
                                else
                                {
                                    sregionList.Add(0, " --- No Geographic Sub-Region(s) found --- ");
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readSRegion.Close();
                                readSRegion.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return sregionList;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getGlobalSubRegionListWithAssetCount(Int32 RegionID, Int32 ClientID, String ClientDBString)
        {
            try
            {
                Int32 sregID = -99, gpd = 0, gwl = 0, swl = 0;
                String sregName = String.Empty, sregValue = String.Empty;
                Dictionary<Int32, String> sregionList = new Dictionary<Int32, String>();
                String selSRegion = "SELECT [gsrID], [gsrName] FROM [dmgGeoSubRegion] WHERE [grID] = @grID ORDER BY [gsrName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getSRegion = new SqlCommand(selSRegion, dataCon);
                        getSRegion.Parameters.AddWithValue("@grID", SqlDbType.Int).Value = RegionID.ToString();
                        using (SqlDataReader readSRegion = getSRegion.ExecuteReader())
                        {
                            try
                            {
                                if (readSRegion.HasRows)
                                {
                                    while (readSRegion.Read())
                                    {
                                        sregID = readSRegion.GetInt32(0);
                                        sregName = readSRegion.GetString(1);
                                        gpd = getGlobalWellPadCountBySubRegion(sregID);
                                        gwl = getGlobalWellCountBySubRegion(sregID);
                                        swl = getSiloWellCountBySubRegion(sregID, ClientID, ClientDBString);
                                        sregValue = String.Format("{0} --- ( Global Well Pad: {1}; Global Well/Lateral: {2}; SMART Well/Lateral: {3} )", sregName, gpd, gwl, swl);
                                        sregionList.Add(sregID, sregValue);
                                    }
                                }
                                else
                                {
                                    sregionList.Add(0, " --- No Geographic Sub-Region(s) found --- ");
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readSRegion.Close();
                                readSRegion.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return sregionList;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellPadCountBySubRegion(Int32 SubRegionID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(wpdID) FROM [dmgWellPad] WHERE [sregID] = @sregID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@sregID", SqlDbType.Int).Value = SubRegionID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellCountBySubRegion(Int32 SubRegionID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(welID) FROM [dmgWell] WHERE [sregID] = @sregID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@sregID", SqlDbType.Int).Value = SubRegionID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getSiloWellCountBySubRegion(Int32 SubRegionID, Int32 ClientID, String ClientDBString)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [Well] WHERE [sregID] = @sregID AND [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@sregID", SqlDbType.Int).Value = SubRegionID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getSubRegionTable(Int32 RegionID)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn gsrID = iReply.Columns.Add("gsrID", typeof(Int32));
                DataColumn gsrName = iReply.Columns.Add("gsrName", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { gsrID };
                Int32 id = -99;
                String name = String.Empty;
                DateTime uT = DateTime.Now;
                String selSRegion = "SELECT [gsrID], [gsrName], [uTime] FROM dmgGeoSubRegion WHERE [grID] = @grID ORDER BY [gsrName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selSRegion, dataCon);
                        getData.Parameters.AddWithValue("@grID", SqlDbType.Int).Value = RegionID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, name, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                                else
                                {
                                    id = 1;
                                    name = "No Geographic Sub-Region found";
                                    iReply.Rows.Add(id, name, uT);
                                    iReply.AcceptChanges();
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getSubRegionTableForOperatorBySubRegion(Int32 RegionID, Int32 OperatorID, Int32 ClientID, String ClientDBString)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn gsrID = iReply.Columns.Add("gsrID", typeof(Int32));
                DataColumn gsrName = iReply.Columns.Add("gsrName", typeof(String));
                DataColumn padCount = iReply.Columns.Add("padCount", typeof(Int32));
                DataColumn welCount = iReply.Columns.Add("welCount", typeof(Int32));
                DataColumn swlCount = iReply.Columns.Add("swlCount", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { gsrID };
                Int32 id = -99, gpd = 0, gwl = 0, swl = 0;
                String name = String.Empty;                
                DateTime uT = DateTime.Now;
                String selSRegion = "SELECT [gsrID], [gsrName], [uTime] FROM [dmgGeoSubRegion] WHERE [grID] = @grID ORDER BY [gsrName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selSRegion, dataCon);
                        getData.Parameters.AddWithValue("@grID", SqlDbType.Int).Value = RegionID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        gpd = getGlobalWellPadCountForOperatorBySubRegion(id, OperatorID);
                                        gwl = getGlobalWellCountForOperatorBySubRegion(id, OperatorID);
                                        swl = getSiloWellCountForOperatorBySubRegion(id, OperatorID, ClientID, ClientDBString);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, name, gpd, gwl, swl, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                                else
                                {
                                    id = 1;
                                    name = "No Geographic Sub-Region found. This is an Error. Please report to FE Support";
                                    iReply.Rows.Add(id, name, uT);
                                    iReply.AcceptChanges();
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getSubRegionTableForOperatorBySubRegionForReviewer(Int32 RegionID, Int32 OperatorID)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn gsrID = iReply.Columns.Add("gsrID", typeof(Int32));
                DataColumn gsrName = iReply.Columns.Add("gsrName", typeof(String));
                DataColumn padCount = iReply.Columns.Add("padCount", typeof(Int32));
                DataColumn welCount = iReply.Columns.Add("welCount", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { gsrID };
                Int32 id = -99, gpd = 0, gwl = 0;
                String name = String.Empty;
                DateTime uT = DateTime.Now;
                String selSRegion = "SELECT [gsrID], [gsrName], [uTime] FROM [dmgGeoSubRegion] WHERE [grID] = @grID ORDER BY [gsrName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selSRegion, dataCon);
                        getData.Parameters.AddWithValue("@grID", SqlDbType.Int).Value = RegionID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        gpd = getGlobalWellPadCountForOperatorBySubRegion(id, OperatorID);
                                        gwl = getGlobalWellCountForOperatorBySubRegion(id, OperatorID);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, name, gpd, gwl, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getProviderSubRegionTableForOperatorBySubRegionForReviewer(Int32 RegionID, Int32 ProviderID)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn gsrID = iReply.Columns.Add("gsrID", typeof(Int32));
                DataColumn gsrName = iReply.Columns.Add("gsrName", typeof(String));
                DataColumn padCount = iReply.Columns.Add("padCount", typeof(Int32));
                DataColumn welCount = iReply.Columns.Add("welCount", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { gsrID };
                Int32 id = -99, gpd = 0, gwl = 0;
                String name = String.Empty;
                DateTime uT = DateTime.Now;
                String selSRegion = "SELECT [gsrID], [gsrName], [uTime] FROM [dmgGeoSubRegion] WHERE [grID] = @grID ORDER BY [gsrName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selSRegion, dataCon);
                        getData.Parameters.AddWithValue("@grID", SqlDbType.Int).Value = RegionID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        gpd = getProviderGlobalWellPadCountForOperatorBySubRegion(id, ProviderID);
                                        gwl = getProviderGlobalWellCountForOperatorBySubRegion(id, ProviderID);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, name, gpd, gwl, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getSiloWellCountForOperatorBySubRegion(Int32 SubRegionID, Int32 OperatorID, Int32 ClientID, String ClientDBString)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [Well] WHERE [sregID] = @sregID AND [optrID] = @optrID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@sregID", SqlDbType.Int).Value = SubRegionID.ToString();
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellCountForOperatorBySubRegion(Int32 SubRegionID, Int32 OperatorID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [dmgWell] WHERE [sregID] = @sregID AND [optrID] = @optrID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@sregID", SqlDbType.Int).Value = SubRegionID.ToString();
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getProviderGlobalWellCountForOperatorBySubRegion(Int32 SubRegionID, Int32 ProviderID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [dmgWell] WHERE [sregID] = @sregID AND [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@sregID", SqlDbType.Int).Value = SubRegionID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ProviderID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellPadCountForOperatorBySubRegion(Int32 SubRegionID, Int32 OperatorID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(wpdID) FROM [dmgWellPad] WHERE [sregID] = @sregID AND [optrID] = @optrID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@sregID", SqlDbType.Int).Value = SubRegionID.ToString();
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getProviderGlobalWellPadCountForOperatorBySubRegion(Int32 SubRegionID, Int32 ProviderID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(wpdID) FROM [dmgWellPad] WHERE [sregID] = @sregID AND [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@sregID", SqlDbType.Int).Value = SubRegionID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ProviderID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getStateList(Int32 CountryID)
        {
            try
            {
                Int32 stID = -99;
                String stName = null;

                Dictionary<int, string> stateList = new Dictionary<int, string>();
                String selState = "SELECT [stID], [stName] FROM dmgState WHERE [ctyID] = @ctyID ORDER BY [stName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getState = new SqlCommand(selState, dataCon);
                        getState.Parameters.AddWithValue("@ctyID", SqlDbType.Int).Value = CountryID.ToString();
                        using (SqlDataReader readState = getState.ExecuteReader())
                        {
                            try
                            {
                                if (readState.HasRows)
                                {
                                    while (readState.Read())
                                    {
                                        stID = readState.GetInt32(0);
                                        stName = readState.GetString(1);
                                        stateList.Add(stID, stName);
                                    }
                                }
                                else
                                {
                                    stateList.Add(0, " --- No State/Province found for selected Country/Nation --- ");
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readState.Close();
                                readState.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return stateList;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getGlobalStateListWithAssetCount(Int32 CountryID, Int32 ClientID, String ClientDBString)
        {
            try
            {
                Int32 stID = -99, gpd = -99, gwl = -99, swl = -99;
                String stName = String.Empty, stValue = String.Empty;

                Dictionary<int, string> stateList = new Dictionary<int, string>();
                String selState = "SELECT [stID], [stName] FROM dmgState WHERE [ctyID] = @ctyID ORDER BY [stName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getState = new SqlCommand(selState, dataCon);
                        getState.Parameters.AddWithValue("@ctyID", SqlDbType.Int).Value = CountryID.ToString();
                        using (SqlDataReader readState = getState.ExecuteReader())
                        {
                            try
                            {
                                if (readState.HasRows)
                                {
                                    while (readState.Read())
                                    {
                                        stID = readState.GetInt32(0);
                                        stName = readState.GetString(1);
                                        gpd = getGlobalWellPadCountByState(stID);
                                        gwl = getGlobalWellCountByState(stID);
                                        swl = getSiloWellCountByState(stID, ClientID, ClientDBString);
                                        stValue = String.Format("{0} --- ( Global Well Pad: {1}; Global Well/Lateral: {2}; SMART Well/Lateral: {3} )", stName, gpd, gwl, swl);
                                        stateList.Add(stID, stValue);
                                    }
                                }
                                else
                                {
                                    stateList.Add(0, " --- No State/Province found for selected Country/Nation --- ");
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readState.Close();
                                readState.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return stateList;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellPadCountByState(Int32 StateID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(wpdID) FROM [dmgWellPad] WHERE [stID] = @stID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = StateID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellCountByState(Int32 StateID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [dmgWell] WHERE [stID] = @stID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = StateID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getSiloWellCountByState(Int32 StateID, Int32 ClientID, String ClientDBString)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [Well] WHERE [stID] = @stID AND [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = StateID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getStateTable(Int32 CountryID)
        {
            try
            {                                
                DataTable iReply = new DataTable();
                DataColumn stID = iReply.Columns.Add("stID", typeof(Int32));
                DataColumn stName = iReply.Columns.Add("stName", typeof(String));
                DataColumn stCode = iReply.Columns.Add("stCode", typeof(String));
                DataColumn stAPI = iReply.Columns.Add("stAPI", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { stID };

                Int32 id = -99;
                String name = String.Empty;
                String code = String.Empty;
                String api = String.Empty;
                DateTime uT = DateTime.Now;

                String selState = "SELECT [stID], [stName], [stCode], [stAPI], [uTime] FROM dmgState WHERE [ctyID] = @ctyID ORDER BY [stName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getState = new SqlCommand(selState, dataCon);
                        getState.Parameters.AddWithValue("@ctyID", SqlDbType.Int).Value = CountryID.ToString();
                        using (SqlDataReader readState = getState.ExecuteReader())
                        {
                            try
                            {
                                if (readState.HasRows)
                                {
                                    while (readState.Read())
                                    {
                                        id = readState.GetInt32(0);
                                        name = readState.GetString(1);
                                        code = readState.GetString(2);
                                        api = readState.GetString(3);
                                        uT = readState.GetDateTime(4);
                                        iReply.Rows.Add(id, name, code, api, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                                else
                                {
                                    id = 1;
                                    name = "No State/Province found for selected Country/Nation";
                                    code = "---";
                                    api = "---";
                                    iReply.Rows.Add(id, name, code, api, uT);
                                    iReply.AcceptChanges();
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readState.Close();
                                readState.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getStateTableForOperatorByCountry(Int32 CountryID, Int32 OperatorID, Int32 ClientID, String ClientDBString)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn stID = iReply.Columns.Add("stID", typeof(Int32));
                DataColumn stName = iReply.Columns.Add("stName", typeof(String));
                DataColumn stCode = iReply.Columns.Add("stCode", typeof(String));
                DataColumn stAPI = iReply.Columns.Add("stAPI", typeof(String));
                DataColumn padCount = iReply.Columns.Add("padCount", typeof(Int32));
                DataColumn welCount = iReply.Columns.Add("welCount", typeof(Int32));
                DataColumn swlCount = iReply.Columns.Add("swlCount", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { stID };

                Int32 id = -99, gpd = 0, gwl = 0, swl = 0;
                String name = String.Empty;
                String code = String.Empty;
                String api = String.Empty;
                DateTime uT = DateTime.Now;

                String selState = "SELECT [stID], [stName], [stCode], [stAPI], [uTime] FROM dmgState WHERE [ctyID] = @ctyID ORDER BY [stName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getState = new SqlCommand(selState, dataCon);
                        getState.Parameters.AddWithValue("@ctyID", SqlDbType.Int).Value = CountryID.ToString();
                        using (SqlDataReader readState = getState.ExecuteReader())
                        {
                            try
                            {
                                if (readState.HasRows)
                                {
                                    while (readState.Read())
                                    {
                                        id = readState.GetInt32(0);
                                        name = readState.GetString(1);
                                        code = readState.GetString(2);
                                        api = readState.GetString(3);
                                        gpd = getGlobalWellPadCountForOperatorByState(id, OperatorID);
                                        gwl = getGlobalWellCountForOperatorByState(id, OperatorID);
                                        swl = getSiloWellCountForOperatorByState(id, OperatorID, ClientID, ClientDBString);
                                        uT = readState.GetDateTime(4);
                                        iReply.Rows.Add(id, name, code, api, gpd, gwl, swl, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                                else
                                {
                                    id = 1;
                                    name = "No State/Province found for selected Country/Nation";
                                    code = "---";
                                    api = "---";
                                    iReply.Rows.Add(id, name, code, api, gpd, gwl, swl, uT);
                                    iReply.AcceptChanges();
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readState.Close();
                                readState.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getStateTableForOperatorByCountryForReviewer(Int32 CountryID, Int32 OperatorID)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn stID = iReply.Columns.Add("stID", typeof(Int32));
                DataColumn stName = iReply.Columns.Add("stName", typeof(String));
                DataColumn stCode = iReply.Columns.Add("stCode", typeof(String));
                DataColumn stAPI = iReply.Columns.Add("stAPI", typeof(String));
                DataColumn padCount = iReply.Columns.Add("padCount", typeof(Int32));
                DataColumn welCount = iReply.Columns.Add("welCount", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { stID };

                Int32 id = -99, gpd = 0, gwl = 0;
                String name = String.Empty, code = String.Empty, api = String.Empty;
                DateTime uT = DateTime.Now;

                String selState = "SELECT [stID], [stName], [stCode], [stAPI], [uTime] FROM dmgState WHERE [ctyID] = @ctyID ORDER BY [stName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getState = new SqlCommand(selState, dataCon);
                        getState.Parameters.AddWithValue("@ctyID", SqlDbType.Int).Value = CountryID.ToString();
                        using (SqlDataReader readState = getState.ExecuteReader())
                        {
                            try
                            {
                                if (readState.HasRows)
                                {
                                    while (readState.Read())
                                    {
                                        id = readState.GetInt32(0);
                                        name = readState.GetString(1);
                                        code = readState.GetString(2);
                                        api = readState.GetString(3);
                                        gpd = getGlobalWellPadCountForOperatorByState(id, OperatorID);
                                        gwl = getGlobalWellCountForOperatorByState(id, OperatorID);
                                        uT = readState.GetDateTime(4);
                                        iReply.Rows.Add(id, name, code, api, gpd, gwl, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readState.Close();
                                readState.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getProviderStateTableForOperatorByCountryForReviewer(Int32 CountryID, Int32 ProviderID)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn stID = iReply.Columns.Add("stID", typeof(Int32));
                DataColumn stName = iReply.Columns.Add("stName", typeof(String));
                DataColumn stCode = iReply.Columns.Add("stCode", typeof(String));
                DataColumn stAPI = iReply.Columns.Add("stAPI", typeof(String));
                DataColumn padCount = iReply.Columns.Add("padCount", typeof(Int32));
                DataColumn welCount = iReply.Columns.Add("welCount", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { stID };

                Int32 id = -99, gpd = 0, gwl = 0;
                String name = String.Empty, code = String.Empty, api = String.Empty;
                DateTime uT = DateTime.Now;

                String selState = "SELECT [stID], [stName], [stCode], [stAPI], [uTime] FROM dmgState WHERE [ctyID] = @ctyID ORDER BY [stName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getState = new SqlCommand(selState, dataCon);
                        getState.Parameters.AddWithValue("@ctyID", SqlDbType.Int).Value = CountryID.ToString();
                        using (SqlDataReader readState = getState.ExecuteReader())
                        {
                            try
                            {
                                if (readState.HasRows)
                                {
                                    while (readState.Read())
                                    {
                                        id = readState.GetInt32(0);
                                        name = readState.GetString(1);
                                        code = readState.GetString(2);
                                        api = readState.GetString(3);
                                        gpd = getProviderGlobalWellPadCountForOperatorByState(id, ProviderID);
                                        gwl = getProviderGlobalWellCountForOperatorByState(id, ProviderID);
                                        uT = readState.GetDateTime(4);
                                        iReply.Rows.Add(id, name, code, api, gpd, gwl, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readState.Close();
                                readState.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellPadCountForOperatorByState(Int32 StateID, Int32 OperatorID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(wpdID) FROM [dmgWellPad] WHERE [stID] = @stID AND [optrID] = @optrID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = StateID.ToString();
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getProviderGlobalWellPadCountForOperatorByState(Int32 StateID, Int32 ProviderID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(wpdID) FROM [dmgWellPad] WHERE [stID] = @stID AND [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = StateID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ProviderID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellCountForOperatorByState(Int32 StateID, Int32 OperatorID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [dmgWell] WHERE [stID] = @stID AND [optrID] = @optrID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = StateID.ToString();
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getProviderGlobalWellCountForOperatorByState(Int32 StateID, Int32 ProviderID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [dmgWell] WHERE [stID] = @stID AND [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = StateID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ProviderID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getSiloWellCountForOperatorByState(Int32 StateID, Int32 OperatorID, Int32 ClientID, String ClientDBString)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [Well] WHERE [stID] = @stID AND [optrID] = @optrID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = StateID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getStateName(Int32 StateID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [stName] FROM dmgState WHERE [stID] = @stID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = StateID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getStatusValue(Int32 statusID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [dmgStatus] FROM [dmgStatus] WHERE [dmgstID] = @dmgstID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@dmgstID", SqlDbType.Int).Value = statusID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                                else
                                {
                                    iReply = "---";
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }     

        public static DataTable getStatusTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn dmgstID = iReply.Columns.Add("dmgstID", typeof(Int32));
                DataColumn dmgStatus = iReply.Columns.Add("dmgStatus", typeof(String));
                String selData = "SELECT [dmgstID], [dmgStatus] FROM [dmgStatus] ORDER BY [dmgStatus]";
                Int32 id = 0;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Rows.Add(id, value);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getShortCollarAppTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn scappID = iReply.Columns.Add("scappID", typeof(Int32));
                DataColumn scappValue = iReply.Columns.Add("scappValue", typeof(String));
                String selData = "SELECT [scappID], [scappValue] FROM dmgShortCollarApplication ORDER BY [scappValue]";
                Int32 id = 0;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Rows.Add(id, value);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getToolCodesList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [tcID], [tcLong], [tcDesc] FROM [dmgToolCodes] ORDER BY [tcLong]";
                Int32 id = 0;
                String name = String.Empty, desc = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        desc = readData.GetString(2);
                                        iReply.Add(id, String.Format("{0} - ( {1} )", name, desc));
                                    }
                                }
                                else
                                {
                                    iReply.Add(1, "---");
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }            
        }

        public static String getToolCodeName(Int32 ToolCodeID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [tcLong] FROM [dmgToolCodes] WHERE [tcID] = @tcID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@tcID", SqlDbType.Int).Value = ToolCodeID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                                else
                                {
                                    iReply = "---";
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getZipCode(Int32 ZipID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [zipCode] FROM dmgZip WHERE [zipID] = @zipID";                
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@zipID", SqlDbType.Int).Value = ZipID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }            
        }

        public static Dictionary<Int32, String> getZipList(Int32 CountyID)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selZip = "SELECT [zipID], [zipCode] FROM dmgZip WHERE [cntyID] = @cntyID ORDER BY [zipCode]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getZip = new SqlCommand(selZip, dataCon);
                        getZip.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                        using (SqlDataReader readZip = getZip.ExecuteReader())
                        {
                            try
                            {
                                if (readZip.HasRows)
                                {
                                    while (readZip.Read())
                                    {
                                        id = readZip.GetInt32(0);
                                        name = readZip.GetString(1);
                                        iReply.Add(id, name);                                        
                                    }
                                }                                
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readZip.Close();
                                readZip.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getZipTable(Int32 CountyID)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn zipID = iReply.Columns.Add("zipID", typeof(Int32));
                DataColumn zipCode = iReply.Columns.Add("zipCode", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                iReply.PrimaryKey = new DataColumn[] { zipID };
                Int32 id = -99;
                String name = String.Empty;
                DateTime uT = DateTime.Now;
                String selZip = "SELECT [zipID], [zipCode], [uTime] FROM dmgZip WHERE [cntyID] = @cntyID ORDER BY [zipCode]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getZip = new SqlCommand(selZip, dataCon);
                        getZip.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                        using (SqlDataReader readZip = getZip.ExecuteReader())
                        {
                            try
                            {
                                if (readZip.HasRows)
                                {
                                    while (readZip.Read())
                                    {
                                        id = readZip.GetInt32(0);
                                        name = readZip.GetString(1);
                                        uT = readZip.GetDateTime(2);
                                        iReply.Rows.Add(id, name, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                                else
                                {
                                    id = 1;
                                    name = "No Zip/Postal Code found for selected County/District/Municipality";
                                    iReply.Rows.Add(id, name, uT);
                                    iReply.AcceptChanges();
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readZip.Close();
                                readZip.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }  

        public static System.Data.DataTable getWellProfileIterationTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wpiID = iReply.Columns.Add("wpiID", typeof(Int32));
                DataColumn wpimin = iReply.Columns.Add("wpiMin", typeof(Int32));
                DataColumn wpimed = iReply.Columns.Add("wpiMed", typeof(Int32));
                DataColumn wpimax = iReply.Columns.Add("wpiMax", typeof(Int32));
                Int32 id = -99, min = -99, med = -99, max = -99;
                String selData = "SELECT [wpiID], [wpiMin], [wpiThreshold], [wpiMax] FROM [dmgWPIterations]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        min = readData.GetInt32(1);
                                        med = readData.GetInt32(2);
                                        max = readData.GetInt32(3);
                                        iReply.Rows.Add(id, min, med, max);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getWellProcessStatus(Int32 WellStatusID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [wstValue] FROM [dmgWellStatus] WHERE [wstID] = @wstID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wstID", SqlDbType.Int).Value = WellStatusID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    iReply = readData.GetString(0);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getWellStatusList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32,String>();
                String selData = "SELECT [wstID], [wstValue] FROM [dmgWellStatus] ORDER BY [wstValue]";
                Int32 id = -99;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    id = readData.GetInt32(0);
                                    name = readData.GetString(1);
                                    iReply.Add(id, name);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getWellStatusListForClients()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [wstID], [wstValue] FROM [dmgWellStatus] WHERE [wstID] = 1 OR [wstID] = 4 ORDER BY [wstValue]";
                Int32 id = -99;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    id = readData.GetInt32(0);
                                    name = readData.GetString(1);
                                    iReply.Add(id, name);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getWellStatusName(Int32 StatusID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [wstValue] FROM [dmgWellStatus] WHERE [wstID] = @wstID";
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wstID", SqlDbType.Int).Value = StatusID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    iReply = readData.GetString(0);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWellStatusTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                String selData = "SELECT [wstID], [wstValue] FROM [dmgWellStatus] ORDER BY [wstValue]";
                Int32 id = -99;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    id = readData.GetInt32(0);
                                    name = readData.GetString(1);
                                    iReply.Rows.Add(id, name);
                                    iReply.AcceptChanges();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getWellSectionTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn wsID = iReply.Columns.Add("wsID", typeof(Int32));
                DataColumn wsName = iReply.Columns.Add("wsName", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [wsID], [wsName], [uTime] FROM dmgWellSection ORDER BY [wsName]";
                Int32 id = 0;
                String value = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    id = readData.GetInt32(0);
                                    value = readData.GetString(1);
                                    uT = readData.GetDateTime(2);
                                    iReply.Rows.Add(id, value, uT);
                                    iReply.AcceptChanges();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        internal static System.Data.DataTable getDataEntryTypeTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn dtID = iReply.Columns.Add("dtID", typeof(Int32));
                DataColumn dtName = iReply.Columns.Add("dtName", typeof(String));
                String selData = "SELECT [dtID], [dtName] FROM [dmgDataType] ORDER BY [dtName]";
                Int32 id = 0;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    id = readData.GetInt32(0);
                                    value = readData.GetString(1);
                                    iReply.Rows.Add(id, value);
                                    iReply.AcceptChanges();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getFieldWithWellPadCountList(Int32 CountyID)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99, wCount = 0;
                String name = String.Empty;
                String selData = "SELECT [fldID], [fldName] FROM [dmgField] WHERE [cntyID] = @cntyID ORDER BY [fldName]";
                //String selData = "SELECT a.fldID, a.fldName, count(b.welID) FROM dmgWell b, dmgField a WHERE b.fldID = a.fldID AND a.cntyID = @cntyID ORDER BY a.fldName";                                    
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = CountyID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        wCount = getFieldWellPadsCount(id);
                                        //wCount = readData.GetInt32(2);
                                        name = String.Format("{0} - ( {1} )", name, wCount);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getGlobalWellRegistrationRequest()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wsrID = iReply.Columns.Add("wsrID", typeof(Int32));
                DataColumn optrID = iReply.Columns.Add("optrID", typeof(Int32));
                DataColumn oprNm = iReply.Columns.Add("oprNm", typeof(String));
                DataColumn clntID = iReply.Columns.Add("clntID", typeof(Int32));
                DataColumn clntNm = iReply.Columns.Add("clntNm", typeof(String));
                DataColumn wpdID = iReply.Columns.Add("wpdID", typeof(String));
                DataColumn welID = iReply.Columns.Add("welID", typeof(String));
                DataColumn srvGrpID = iReply.Columns.Add("srvGrpID", typeof(String));
                DataColumn srvID = iReply.Columns.Add("srvID", typeof(String));
                DataColumn username = iReply.Columns.Add("username", typeof(String));
                DataColumn userRole = iReply.Columns.Add("userRole", typeof(String));
                DataColumn realm = iReply.Columns.Add("realm", typeof(String));
                DataColumn isApproved = iReply.Columns.Add("isApproved", typeof(String));
                DataColumn cApp = iReply.Columns.Add("cApp", typeof(String));
                DataColumn cTime = iReply.Columns.Add("cTime", typeof(DateTime));
                DataColumn ApprovedBy = iReply.Columns.Add("ApprovedBy", typeof(String));
                DataColumn ApprovedStatus = iReply.Columns.Add("ApprovedStatus", typeof(String));
                DataColumn appColor = iReply.Columns.Add("appColor", typeof(String));
                DataColumn appTime = iReply.Columns.Add("appTime", typeof(DateTime));
                Int32 id = -99, opCoID = -99, clntCoID = -99, padID = -99, latID = -99, sgID = -99, sID = -99, clntIsApproved = -99, approvedStat = -99;
                String opCoName = String.Empty, clntCoName = String.Empty, name = String.Empty, domain = String.Empty, appUser = String.Empty, clntAppStat= String.Empty, appStat = String.Empty, apClr = String.Empty, usrRole = String.Empty;
                String padName = String.Empty, latName = String.Empty, groupName = String.Empty, serviceName = String.Empty, clntColor = String.Empty;
                DateTime cT = DateTime.Now, aT = DateTime.Now;
                String unF = String.Empty, unL = String.Empty, unD = String.Empty, usrName = String.Empty;
                String selData = "SELECT [wsrID], [optrID], [clntID], [wpdID], [welID], [srvGrpID], [srvID], [username], [realm], [isApproved], [cTime], [ApprovedBy], [ApprovedStatus], [appTime] FROM [dmgWellServiceRegistration]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        opCoID = readData.GetInt32(1);
                                        opCoName = ACC.getClientName(opCoID);
                                        clntCoID = readData.GetInt32(2);
                                        clntCoName = ACC.getClientName(clntCoID);
                                        padID = readData.GetInt32(3);
                                        padName = getGlobalWellPadName(padID);
                                        latID = readData.GetInt32(4);
                                        latName = getGlobalWellName(latID);
                                        sgID = readData.GetInt32(5);
                                        groupName = SRV.getSrvGroupName(sgID); 
                                        sID = readData.GetInt32(6);
                                        serviceName = SRV.getServiceName(sID);
                                        name = readData.GetString(7);
                                        UP userP = UP.GetUserProfile(name);
                                        unF = userP.FirstName;
                                        unL = userP.LastName;
                                        unD = userP.designation;
                                        usrName = String.Format("{0}, {1} - ({2})", unL, unF, unD);
                                        usrRole = Convert.ToString(Roles.GetRolesForUser(name)[0]);
                                        domain = readData.GetString(8);
                                        //0 = On Hold; 1 = Registered
                                        clntIsApproved = readData.GetInt32(9);
                                        clntAppStat = getWellStatusName(clntIsApproved);
                                        if (clntIsApproved.Equals(1))
                                        { clntColor = "Green"; }
                                        else if (clntIsApproved.Equals(2))
                                        { clntColor = "Green"; }
                                        else if (clntIsApproved.Equals(3))
                                        { clntColor = "Yellow"; }
                                        else if (clntIsApproved.Equals(4))
                                        { clntColor = "Yellow"; }
                                        else
                                        { clntColor = "Red"; }
                                        cT = readData.GetDateTime(10);
                                        appUser = readData.GetString(11);
                                        approvedStat = readData.GetInt32(12);
                                        apClr = "Yellow";
                                        aT = readData.GetDateTime(13);
                                        iReply.Rows.Add(id, opCoID, opCoName, clntCoID, clntCoName, padName, latName, groupName, serviceName, usrName, usrRole, domain, clntAppStat, clntColor, cT, appUser, approvedStat, apClr, aT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getGlobalWellRegistrationRequesterDetail(Int32 WellServiceRegReqID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wsrID = iReply.Columns.Add("wsrID", typeof(Int32));
                DataColumn firstName = iReply.Columns.Add("firstName", typeof(String));
                DataColumn lastName = iReply.Columns.Add("lastName", typeof(String));
                DataColumn designation = iReply.Columns.Add("designation", typeof(String));
                DataColumn userRole = iReply.Columns.Add("userRole", typeof(String));
                DataColumn realm = iReply.Columns.Add("realm", typeof(String));
                Int32 id = -99;
                String name = String.Empty, domain = String.Empty, usrRole = String.Empty;
                String unF = String.Empty, unL = String.Empty, unD = String.Empty, usrName = String.Empty;
                String selData = "SELECT [wsrID], [username], [realm] FROM [dmgWellServiceRegistration] WHERE [wsrID] = @wsrID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wsrID", SqlDbType.Int).Value = WellServiceRegReqID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        UP userP = UP.GetUserProfile(name);
                                        unF = userP.FirstName;
                                        unL = userP.LastName;
                                        unD = userP.designation;
                                        usrRole = Convert.ToString(Roles.GetRolesForUser(name)[0]);
                                        domain = readData.GetString(2);
                                        iReply.Rows.Add(id, unF, unL, unD, usrRole, domain);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getGlobalWellRegistrationApproverDetail(Int32 WellServiceRegReqID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wsrID = iReply.Columns.Add("wsrID", typeof(Int32));
                DataColumn appLast = iReply.Columns.Add("appLast", typeof(String));
                DataColumn appFirst = iReply.Columns.Add("appFirst", typeof(String));                
                DataColumn appRole = iReply.Columns.Add("appRole", typeof(String));
                DataColumn appStat = iReply.Columns.Add("appStat", typeof(String));
                DataColumn appTime = iReply.Columns.Add("appTime", typeof(String));
                DataColumn appWait = iReply.Columns.Add("appWait", typeof(String));
                DataColumn appClr = iReply.Columns.Add("appClr", typeof(String));
                Int32 id = -99, appSt = -99;
                String name = String.Empty, usrRole = String.Empty, apStatus= String.Empty;
                String unF = String.Empty, unL = String.Empty, usrName = String.Empty, color = String.Empty;
                DateTime cT = DateTime.Now, aT = DateTime.Now;
                TimeSpan wTime = new TimeSpan();
                String selData = "SELECT [wsrID], [ApprovedBy], [ApprovedStatus], [cTime], [appTime] FROM [dmgWellServiceRegistration] WHERE [wsrID] = @wsrID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wsrID", SqlDbType.Int).Value = WellServiceRegReqID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        if (!name.Equals("---"))
                                        {
                                            UP userP = UP.GetUserProfile(name);
                                            unF = userP.FirstName;
                                            unL = userP.LastName;
                                            usrRole = Convert.ToString(Roles.GetRolesForUser(name)[0]);
                                            appSt = readData.GetInt32(2);
                                            apStatus = getWellStatusName(appSt);
                                            cT = readData.GetDateTime(3);
                                            aT = readData.GetDateTime(4);
                                            wTime = aT.Subtract(cT);
                                            if (appSt.Equals(1))
                                            { color = "Green"; }
                                            else if (appSt.Equals(2))
                                            { color = "Green"; }
                                            else if (appSt.Equals(3))
                                            { color = "Yellow"; }
                                            else if (appSt.Equals(4))
                                            { color = "Yellow"; }
                                            else
                                            { color = "Red"; }                                            
                                        }
                                        else
                                        {
                                            unF = "---";
                                            unL = "---";
                                            usrRole = "---";
                                            apStatus = "---";
                                            aT = DateTime.MinValue;
                                            wTime = TimeSpan.Zero;
                                            color = "Empty";
                                        }
                                        iReply.Rows.Add(id, unL, unF, usrRole, apStatus, aT, wTime, color);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getRegReqClientsList()
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                Int32 id = -99;
                String selData = "SELECT DISTINCT(clntID) FROM [dmgWellServiceRegistration]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        iReply.Add(id);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getWellRegReqList(Int32 ClientID)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99, padID = -99, latID = -99, srvc = -99, sttID = -99;
                String padName = String.Empty, latName = String.Empty, serviceName = String.Empty, value = String.Empty, sttName = String.Empty;
                String selData = "SELECT [wsrID], [wpdID], [welID], [srvID], [isApproved] FROM [dmgWellServiceRegistration] WHERE [clntID] = @clntID ORDER BY [cTime] DESC";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        padID = readData.GetInt32(1);
                                        padName = getGlobalWellPadName(padID);
                                        latID = readData.GetInt32(2);
                                        latName = getGlobalWellName(latID);
                                        srvc = readData.GetInt32(3);                                        
                                        serviceName = SRV.getServiceName(srvc);
                                        sttID = readData.GetInt32(4);
                                        sttName = getWellStatusName(sttID);
                                        value = String.Format("Well Pad: {0} - Well/Lateral: {1} - Service: {2} - Current Status: {3}", padName, latName, serviceName, sttName);
                                        iReply.Add(id, value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getRegReqPriceList(Int32 RegReqID, Int32 ContractID)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99, unit = -99;
                Decimal price = -99.00M;
                String priceU = String.Empty, value = String.Empty;
                Int32 serviceID = getRegReqServiceID(RegReqID);
                String selData = "SELECT [spID], [spCharge], [scuID] FROM [ServicePrice] WHERE [ctrtID] = @ctrtID AND [srvID] = @srvID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ctrtID", SqlDbType.Int).Value = ContractID.ToString();
                        getData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = serviceID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        price = readData.GetDecimal(1);
                                        unit = readData.GetInt32(2);
                                        priceU = SRV.getServiceChargeUnit(unit);
                                        value = String.Format("US$ {0} - {1}", price, priceU);
                                        iReply.Add(id, value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getRegReqServiceID(Int32 RegReqID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT [srvID] FROM [dmgWellServiceRegistration] WHERE [wsrID] = @wsrID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wsrID", SqlDbType.Int).Value = RegReqID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 updateGlobalWellRegistrationRequest(Int32 RequestID, Int32 ClientID, Int32 ContractTypeID, Int32 ContractID, Int32 ApprovedStatus, Int32 ServicePriceID, String confirmationCode, String LoginName, String LoginDomain)
        {
            Int32 iReply = -99, iReplySiloReg = -99, iReplyGlobalReg = -99, iReplySiloWell = -99, iReplyGlobalWell = -99;
            String updData = "UPDATE [dmgWellServiceRegistration] SET [uTime] = @uTime, [isApproved] = @isApproved, [ApprovedBy] = @ApprovedBy, [ApprovedStatus] = @ApprovedStatus, [appTime] = @appTime, [ctrtID] = @ctrtID, [spID] = @spID, [confirmCode] = @confirmCode WHERE [wsrID] = @wsrID AND [clntID] = @clntID";
            using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
            {
                try
                {
                    dataCon.Open();
                    SqlCommand updateCommand = new SqlCommand(updData, dataCon);
                    updateCommand.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                    updateCommand.Parameters.AddWithValue("@isApproved", SqlDbType.Int).Value = ApprovedStatus.ToString();
                    updateCommand.Parameters.AddWithValue("@ApprovedBy", SqlDbType.NVarChar).Value = LoginName.ToString();
                    updateCommand.Parameters.AddWithValue("@ApprovedStatus", SqlDbType.Int).Value = ApprovedStatus.ToString();
                    updateCommand.Parameters.AddWithValue("@appTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                    updateCommand.Parameters.AddWithValue("@ctrtID", SqlDbType.Int).Value = ContractID.ToString();
                    updateCommand.Parameters.AddWithValue("@spID", SqlDbType.Int).Value = ServicePriceID.ToString();
                    updateCommand.Parameters.AddWithValue("@confirmCode", SqlDbType.NVarChar).Value = confirmationCode.ToString();
                    updateCommand.Parameters.AddWithValue("@wsrID", SqlDbType.Int).Value = RequestID.ToString();
                    updateCommand.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                    iReply = Convert.ToInt32(updateCommand.ExecuteNonQuery());
                    if (iReply.Equals(1))
                    {
                        List<String> paramList = getRegReqParametersList(RequestID, ClientID);
                        Int32 operatorID = -99, serviceGroup = -99, serviceID = -99, PadID = -99, LateralID = -99;
                        String RequestingUser = String.Empty, RequestingEmail = String.Empty, ApprovedStatusValue = String.Empty;
                        DateTime creationTime = DateTime.Now;
                        operatorID = Convert.ToInt32(paramList[0]);
                        serviceGroup = Convert.ToInt32(paramList[1]);
                        serviceID = Convert.ToInt32(paramList[2]);
                        RequestingUser = Convert.ToString(paramList[3]);
                        RequestingEmail = Convert.ToString(paramList[4]);
                        creationTime = Convert.ToDateTime(paramList[5]);
                        PadID = Convert.ToInt32(paramList[6]);
                        LateralID = Convert.ToInt32(paramList[7]);
                        ApprovedStatusValue = getWellStatusName(ApprovedStatus);
                        String dbName = SmartsVer1.Helpers.AccountHelpers.CreateClient.getClientDB(ClientID);
                        String dbString = SmartsVer1.Helpers.AccountHelpers.CreateClient.getDBCon(dbName);
                        if (ContractTypeID.Equals(1)) //Demo Contract
                        {
                            iReplySiloReg = updateSiloWellRegistrationRequest(operatorID, ClientID, serviceGroup, serviceID, RequestingUser, creationTime, ContractID, 2, ServicePriceID, "Approved", LoginName, dbString);
                            iReplySiloWell = updateSiloWellTable(operatorID, ClientID, PadID, LateralID, 2, dbString);
                            iReplyGlobalWell = updateGlobalWellTable(operatorID, ClientID, PadID, LateralID, 2);
                        }
                        else
                        {
                            iReplySiloReg = updateSiloWellRegistrationRequest(operatorID, ClientID, serviceGroup, serviceID, RequestingUser, creationTime, ContractID, ApprovedStatus, ServicePriceID, confirmationCode, LoginName, dbString);
                            iReplySiloWell = updateSiloWellTable(operatorID, ClientID, PadID, LateralID, ApprovedStatus, dbString);
                            iReplyGlobalWell = updateGlobalWellTable(operatorID, ClientID, PadID, LateralID, ApprovedStatus);
                        }                        
                        
                        if (iReplySiloReg.Equals(1) && iReplySiloWell.Equals(1) && iReplyGlobalWell.Equals(1))
                        {                            
                            {
                                iReply = SM.sendWellServiceRegistrationUpdateMessage(LoginName, LoginDomain, operatorID, ClientID, PadID, LateralID, serviceGroup, serviceID, ApprovedStatusValue, confirmationCode, DateTime.Now, RequestingUser, RequestingEmail, dbString);
                            }
                        }
                    }
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
                finally
                {
                    dataCon.Close();
                    dataCon.Dispose();
                }
            }
            return iReply;
        }

        public static Int32 updateWellProcessRegistration(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 ServiceGroupID, Int32 ServiceID, Int32 ApprovedStatus, String ApprovedBy, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                Int32 globalStatus = updateGlobalWellProcessRegistration(OperatorID, ClientID, WellPadID, WellID, ServiceGroupID, ServiceID, ApprovedStatus);
                Int32 siloStatus = updateSiloWellProcessRegistration(OperatorID, ClientID, WellPadID, WellID, ServiceGroupID, ServiceID, ApprovedStatus, ClientDBString);
                if(globalStatus.Equals(1) && siloStatus.Equals(1))
                { return 1; } else { return -1; }                
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 updateGlobalWellProcessRegistration(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 ServiceGroupID, Int32 ServiceID, Int32 ApprovedStatus)
        {
            try
            {
                Int32 iReply = -99;
                String updGlobalString = "UPDATE [dmgWellServiceRegistration] SET [isApproved] = @isApproved, [ApprovedBy] = @ApprovedBy, [ApprovedStatus] = @ApprovedStatus, [appTime] = @appTime, [uTime] = @uTime WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID AND [welID] = @welID AND [srvGrpID] = @srvGrpID AND [srvID] = @srvID";                
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand updateData = new SqlCommand(updGlobalString, dataCon);
                        updateData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        updateData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        updateData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        updateData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        updateData.Parameters.AddWithValue("@srvGrpID", SqlDbType.Int).Value = ServiceGroupID.ToString();
                        updateData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        updateData.Parameters.AddWithValue("@isApproved", SqlDbType.Int).Value = ApprovedStatus.ToString();
                        updateData.Parameters.AddWithValue("@ApprovedBy", SqlDbType.NVarChar).Value = "Audit Service".ToString();
                        updateData.Parameters.AddWithValue("@ApprovedStatus", SqlDbType.Int).Value = ApprovedStatus.ToString();
                        updateData.Parameters.AddWithValue("@appTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        updateData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        iReply = Convert.ToInt32(updateData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 updateSiloWellProcessRegistration(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 ServiceGroupID, Int32 ServiceID, Int32 ApprovedStatus, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String updGlobalString = "UPDATE [WellServiceRegistration] SET [isApproved] = @isApproved, [ApprovedBy] = @ApprovedBy, [ApprovedStatus] = @ApprovedStatus, [appTime] = @appTime, [uTime] = @uTime WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID AND [welID] = @welID AND [srvGrpID] = @srvGrpID AND [srvID] = @srvID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand updateData = new SqlCommand(updGlobalString, dataCon);
                        updateData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        updateData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        updateData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        updateData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        updateData.Parameters.AddWithValue("@srvGrpID", SqlDbType.Int).Value = ServiceGroupID.ToString();
                        updateData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        updateData.Parameters.AddWithValue("@isApproved", SqlDbType.Int).Value = ApprovedStatus.ToString();
                        updateData.Parameters.AddWithValue("@ApprovedBy", SqlDbType.NVarChar).Value = "Audit Service".ToString();
                        updateData.Parameters.AddWithValue("@ApprovedStatus", SqlDbType.Int).Value = ApprovedStatus.ToString();
                        updateData.Parameters.AddWithValue("@appTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        updateData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        iReply = Convert.ToInt32(updateData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 updateSiloWellTable(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 ApprovedStatus, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String updString = "UPDATE [Well] SET[wstID] = @wstID, [uTime] = @uTime WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID AND [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand updateData = new SqlCommand(updString, dataCon);
                        updateData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        updateData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        updateData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        updateData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        updateData.Parameters.AddWithValue("@wstID", SqlDbType.Int).Value = ApprovedStatus.ToString();
                        updateData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        iReply = Convert.ToInt32(updateData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 updateGlobalWellTable(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 ApprovedStatus)
        {
            try
            {
                Int32 iReply = -99;
                String updString = "UPDATE [dmgWell] SET[wstID] = @wstID, [uTime] = @uTime WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID AND [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand updateData = new SqlCommand(updString, dataCon);
                        updateData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        updateData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        updateData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        updateData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        updateData.Parameters.AddWithValue("@wstID", SqlDbType.Int).Value = ApprovedStatus.ToString();
                        updateData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        iReply = Convert.ToInt32(updateData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 updateSiloWellRegistrationRequest(Int32 OperatorID, Int32 ClientID, Int32 ServiceGroupID, Int32 ServiceID, String RequestingUserName, DateTime CreatedTimeStamp, Int32 ContractID, Int32 ApprovedStatus, Int32 ServicePriceID, String confirmationCode, String LoginName, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;                
                String updData = "UPDATE [WellServiceRegistration] SET [uTime] = @uTime, [isApproved] = @isApproved, [ApprovedBy] = @ApprovedBy, [ApprovedStatus] = @ApprovedStatus, [appTime] = @appTime, [ctrtID] = @ctrtID, [spID] = @spID, [confirmCode] = @confirmCode WHERE [optrID] = @optrID AND [clntID] = @clntID AND [srvGrpID] = @srvGrpID AND [srvID] = @srvID AND [username] = @username AND [cTime] = @cTime";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand updateCommand = new SqlCommand(updData, dataCon);
                        updateCommand.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        updateCommand.Parameters.AddWithValue("@isApproved", SqlDbType.Int).Value = ApprovedStatus.ToString();
                        updateCommand.Parameters.AddWithValue("@ApprovedBy", SqlDbType.NVarChar).Value = LoginName.ToString();
                        updateCommand.Parameters.AddWithValue("@ApprovedStatus", SqlDbType.Int).Value = ApprovedStatus.ToString();
                        updateCommand.Parameters.AddWithValue("@appTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        updateCommand.Parameters.AddWithValue("@ctrtID", SqlDbType.Int).Value = ContractID.ToString();
                        updateCommand.Parameters.AddWithValue("@spID", SqlDbType.Int).Value = ServicePriceID.ToString();
                        updateCommand.Parameters.AddWithValue("@confirmCode", SqlDbType.NVarChar).Value = confirmationCode.ToString();
                        updateCommand.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        updateCommand.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        updateCommand.Parameters.AddWithValue("@srvGrpID", SqlDbType.Int).Value = ServiceGroupID.ToString();
                        updateCommand.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        updateCommand.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = RequestingUserName.ToString();
                        updateCommand.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = CreatedTimeStamp.ToString();
                        iReply = Convert.ToInt32(updateCommand.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }            
        }

        public static List<String> getRegReqParametersList(Int32 RequestID, Int32 ClientID)
        {
            try
            {
                List<String> iReply = new List<String>();
                //String dbName = SmartsVer1.Helpers.AccountHelpers.CreateClient.getClientDB(ClientID);
                //String dbString = SmartsVer1.Helpers.AccountHelpers.CreateClient.getDBCon(dbName);
                Int32 opID = -99, grpID = -99, svcID = -99, pdID = -99, wlID = -99;
                String user = String.Empty, uEmail = String.Empty;
                DateTime cT = DateTime.Now;
                String selData = "SELECT [optrID], [srvGrpID], [srvID], [username], [cTime], [wpdID], [welID] FROM [dmgWellServiceRegistration] WHERE [wsrID] = @wsrID AND [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wsrID", SqlDbType.Int).Value = RequestID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    opID = readData.GetInt32(0);
                                    iReply.Add(Convert.ToString(opID));
                                    grpID = readData.GetInt32(1);
                                    iReply.Add(Convert.ToString(grpID));
                                    svcID = readData.GetInt32(2);
                                    iReply.Add(Convert.ToString(svcID));
                                    user = readData.GetString(3);
                                    iReply.Add(user);
                                    uEmail = Membership.GetUser(user).Email;
                                    iReply.Add(uEmail);
                                    cT = readData.GetDateTime(4);
                                    iReply.Add(Convert.ToString(cT));
                                    pdID = readData.GetInt32(5);
                                    iReply.Add(Convert.ToString(pdID));
                                    wlID = readData.GetInt32(6);
                                    iReply.Add(Convert.ToString(wlID));                                                                        
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}