﻿using System;
using System.IO;
using System.Data;
using System.Collections;
using System.Globalization;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
namespace SmartsVer1.Helpers.DemogHelpers
{
    /*Data entry method: 1 = Manual; 2 = File; 3 = QC Data Service*/
    public class DataLoader
    {
        static void DetailsProcessor(Dictionary<String, Int32> header_GB, String[] arrD, ArrayList list, Int32[] fliparray, Decimal[] GB_decimal)
        {
            try
            {
                Int32 arrDlength, enb, gx, gy, gz, bx, by, bz;
                gx = gy = gz = bx = by = bz = 0;
                Decimal Gx, Gy, Gz, Bx, By, Bz;
                String info, decimal_vals = String.Empty;
                arrDlength = arrD.Length;

                foreach (KeyValuePair<String, Int32> item in header_GB)                
                {
                    if (item.Key.ToLower().Contains("enabled")){ enb = item.Value; }
                    if (item.Key.ToLower().Contains("gx")){ gx = item.Value; }
                    if (item.Key.ToLower().Contains("gy")){ gy = item.Value; }
                    if (item.Key.ToLower().Contains("gz")){ gz = item.Value; }
                    if (item.Key.ToLower().Contains("bx")){ bx = item.Value; }
                    if (item.Key.ToLower().Contains("by")){ by = item.Value; }
                    if (item.Key.ToLower().Contains("bz")){ bz = item.Value; }
                }

                if (String.IsNullOrEmpty(arrD[2]) || String.IsNullOrEmpty(arrD[gx]) || String.IsNullOrEmpty(arrD[gy]) || String.IsNullOrEmpty(arrD[gz]) || String.IsNullOrEmpty(arrD[bx]) || String.IsNullOrEmpty(arrD[by]) || String.IsNullOrEmpty(arrD[bz]))
                {
                    info = String.Format("{0} {1} {2}", "Invalid Data at Depth: ", arrD[2], arrD[1]);
                    list.Add(info);
                    info = "";
                }
                else
                {
                    if (arrD[gx].Equals(Decimal.TryParse(arrD[gx], NumberStyles.Any, CultureInfo.InvariantCulture, out Gx)))
                    { Gx = Decimal.Parse(arrD[gx], NumberStyles.AllowExponent); }
                    else
                    { Gx = Convert.ToDecimal(arrD[gx]); }                    
                    if (arrD[gy].Equals(Decimal.TryParse(arrD[gy], NumberStyles.Any, CultureInfo.InvariantCulture, out Gy)))
                    { Gy = Decimal.Parse(arrD[gy], NumberStyles.AllowExponent); }
                    else
                    { Gy = Convert.ToDecimal(arrD[gy]); }
                    if (arrD[gz].Equals(Decimal.TryParse(arrD[gz], NumberStyles.Any, CultureInfo.InvariantCulture, out Gz)))
                    { Gz = Decimal.Parse(arrD[gz], NumberStyles.AllowExponent); }
                    else
                    { Gz = Convert.ToDecimal(arrD[gz]); }
                    Bx = Convert.ToDecimal(arrD[bx]);
                    By = Convert.ToDecimal(arrD[by]);
                    Bz = Convert.ToDecimal(arrD[bz]);

                    Gx = Gx * fliparray[0];
                    Gy = Gy * fliparray[1];
                    Gz = Gz * fliparray[2];
                    Bx = Bx * fliparray[3];
                    By = By * fliparray[4];
                    Bz = Bz * fliparray[5];

                    GB_decimal[0] = Gx;
                    GB_decimal[1] = Gy;
                    GB_decimal[2] = Gz;
                    GB_decimal[3] = Bx;
                    GB_decimal[4] = By;
                    GB_decimal[5] = Bz;

                    info = "Processed at Depth " + arrD[2] + ": " + Gx + ", " + Gy + ", " + Gz + ", " + Bx + ", " + By + ", " + Bz + " ";
                    list.Add(info);
                    info = "";
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        static void StandardDetailsProcessor(Dictionary<String, Int32> header_GB, String[] arrD, ArrayList list, Int32[] fliparray, Decimal[] GB_decimal)
        {
            try
            {
                Int32 arrDlength, enb, gx, gy, gz, bx, by, bz;
                gx = gy = gz = bx = by = bz = 0;
                Decimal Gx, Gy, Gz, Bx, By, Bz;
                String info, decimal_vals = String.Empty;
                arrDlength = arrD.Length;

                foreach (KeyValuePair<String, Int32> item in header_GB)
                {
                    if (item.Key.ToLower().Contains("gx/ax")) { gx = item.Value; }
                    if (item.Key.ToLower().Contains("gy/ay")) { gy = item.Value; }
                    if (item.Key.ToLower().Contains("gz/az")) { gz = item.Value; }
                    if (item.Key.ToLower().Contains("bx/mx")) { bx = item.Value; }
                    if (item.Key.ToLower().Contains("by/my")) { by = item.Value; }
                    if (item.Key.ToLower().Contains("bz/mz")) { bz = item.Value; }
                }

                if (String.IsNullOrEmpty(arrD[2]) || String.IsNullOrEmpty(arrD[gx]) || String.IsNullOrEmpty(arrD[gy]) || String.IsNullOrEmpty(arrD[gz]) || String.IsNullOrEmpty(arrD[bx]) || String.IsNullOrEmpty(arrD[by]) || String.IsNullOrEmpty(arrD[bz]))
                {
                    info = String.Format("{0} {1} {2}", "Invalid Data at Depth: ", arrD[2], arrD[1]);
                    list.Add(info);
                    info = "";
                }
                else
                {
                    if (arrD[gx].Equals(Decimal.TryParse(arrD[gx], NumberStyles.Any, CultureInfo.InvariantCulture, out Gx)))
                    { Gx = Decimal.Parse(arrD[gx], NumberStyles.AllowExponent); }
                    else
                    { Gx = Convert.ToDecimal(arrD[gx]); }
                    if (arrD[gy].Equals(Decimal.TryParse(arrD[gy], NumberStyles.Any, CultureInfo.InvariantCulture, out Gy)))
                    { Gy = Decimal.Parse(arrD[gy], NumberStyles.AllowExponent); }
                    else
                    { Gy = Convert.ToDecimal(arrD[gy]); }
                    if (arrD[gz].Equals(Decimal.TryParse(arrD[gz], NumberStyles.Any, CultureInfo.InvariantCulture, out Gz)))
                    { Gz = Decimal.Parse(arrD[gz], NumberStyles.AllowExponent); }
                    else
                    { Gz = Convert.ToDecimal(arrD[gz]); }
                    Bx = Convert.ToDecimal(arrD[bx]);
                    By = Convert.ToDecimal(arrD[by]);
                    Bz = Convert.ToDecimal(arrD[bz]);

                    Gx = Gx * fliparray[0];
                    Gy = Gy * fliparray[1];
                    Gz = Gz * fliparray[2];
                    Bx = Bx * fliparray[3];
                    By = By * fliparray[4];
                    Bz = Bz * fliparray[5];

                    GB_decimal[0] = Gx;
                    GB_decimal[1] = Gy;
                    GB_decimal[2] = Gz;
                    GB_decimal[3] = Bx;
                    GB_decimal[4] = By;
                    GB_decimal[5] = Bz;

                    info = "Processed at Depth " + arrD[2] + ": " + Gx + ", " + Gy + ", " + Gz + ", " + Bx + ", " + By + ", " + Bz + " ";
                    list.Add(info);
                    info = "";
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        static void DetailsProcessor2(Dictionary<String, Int32> header_GB, String[] arrD, ArrayList list, Int32[] fliparray, String[] GB_decimal)
        {
            try
            {
                Int32 arrDlength, enb, sol, gx, gy, gz, bx, by, bz, inc, azm, gti, bti, dip, bT, ga, tp;
                enb = sol = gx = gy = gz = bx = by = bz = inc = azm = gti = bti = bT = ga = tp = 0;
                String Gx, Gy, Gz, Bx, By, Bz;
                String info, decimal_vals = String.Empty;
                Decimal dGx, dGy, dGz, dBx, dBy, dBz;
                arrDlength = arrD.Length;

                foreach (KeyValuePair<String, Int32> item in header_GB)
                {
                    if (item.Key.ToLower().Contains("enabled")){ enb = item.Value; }
                    if (item.Key.ToLower().Contains("solution")){ sol = item.Value; }
                    if (item.Key.ToLower().Contains("gx")){ gx = item.Value; }
                    if (item.Key.ToLower().Contains("gy")){ gy = item.Value; }
                    if (item.Key.ToLower().Contains("gz")){ gz = item.Value; }
                    if (item.Key.ToLower().Contains("bx")){ bx = item.Value; }
                    if (item.Key.ToLower().Contains("by")){ by = item.Value; }
                    if (item.Key.ToLower().Contains("bz")){ bz = item.Value; }
                    if (item.Key.ToLower().Contains("inc")){ inc = item.Value; }
                    if (item.Key.ToLower().Contains("azm")){ azm = item.Value; }
                    if (item.Key.ToLower().Contains("gti")){ gti = item.Value; }
                    if (item.Key.ToLower().Contains("bti")){ bti = item.Value; }
                    if (item.Key.ToLower().Contains("dip")){ dip = item.Value; }
                    if (item.Key.ToLower().Contains("btotal")){ bT = item.Value; }
                    if (item.Key.ToLower().Contains("G Axes")){ ga = item.Value; }
                    if (item.Key.ToLower().Contains("type")){ tp = item.Value; }
                }

                if (String.IsNullOrEmpty(arrD[2]) || String.IsNullOrEmpty(arrD[gx]) || String.IsNullOrEmpty(arrD[gy]) || String.IsNullOrEmpty(arrD[gz]) || String.IsNullOrEmpty(arrD[bx]) || String.IsNullOrEmpty(arrD[by]) || String.IsNullOrEmpty(arrD[bz]))
                {
                    info = String.Format("{0} {1} {2}", "Invalid Data at Depth: ", arrD[2], arrD[1]);
                    list.Add(info);
                    info = "";
                }
                else
                {                    
                    dGx = Convert.ToDecimal(arrD[gx]);
                    dGy = Convert.ToDecimal(arrD[gy]);
                    dGz = Convert.ToDecimal(arrD[gz]);
                    dBx = Convert.ToDecimal(arrD[bx]);
                    dBy = Convert.ToDecimal(arrD[by]);
                    dBz = Convert.ToDecimal(arrD[bz]);

                    Gx = Convert.ToString(dGx * fliparray[0]);
                    Gy = Convert.ToString(dGy * fliparray[1]);
                    Gz = Convert.ToString(dGz * fliparray[2]);
                    Bx = Convert.ToString(dBx * fliparray[3]);
                    By = Convert.ToString(dBy * fliparray[4]);
                    Bz = Convert.ToString(dBz * fliparray[5]);

                    GB_decimal[0] = Gx;
                    GB_decimal[1] = Gy;
                    GB_decimal[2] = Gz;
                    GB_decimal[3] = Bx;
                    GB_decimal[4] = By;
                    GB_decimal[5] = Bz;

                    info = "Processed at Depth " + arrD[2] + ": " + Gx + ", " + Gy + ", " + Gz + ", " + Bx + ", " + By + ", " + Bz + " ";
                    list.Add(info);
                    info = "";
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getCorrectionNextSequence(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT ISNULL(MAX(srvyRowID), 0) + 1 FROM [CorrectionUploadedData] WHERE [optrID] = @optrID AND [clntID] = @clntID AND [wpdID] = @wpdID AND [welID] = @welID AND [wswID] = @wswID AND [runID] = @RunID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        getData.Parameters.AddWithValue("@RunID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader dataReader = getData.ExecuteReader())
                        {
                            try
                            {
                                while (dataReader.Read())
                                {
                                    iReply = dataReader.GetInt32(0);
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                dataReader.Close();
                                dataReader.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }                
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getCorrectionNextSequenceIncDip(Int32 RunID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT ISNULL(MAX(srvyRowID), 0) + 1 FROM [IncDipCorrFormattedData] WHERE [runID] = @RunID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@RunID", SqlDbType.Int).Value = RunID.ToString();

                        using (SqlDataReader dataReader = getData.ExecuteReader())
                        {
                            try
                            {
                                while (dataReader.Read())
                                {
                                    iReply = dataReader.GetInt32(0);
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                dataReader.Close();
                                dataReader.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

		static void HeaderTOPLINE(Dictionary<String, Int32> header_GB, String[] arr)
		{
			try
			{
				Int32 arrlength = arr.Length;
				for (Int32 i = 0; i < arrlength; i++)
				{
					if (arr[i].ToLower().Contains("enabled") ||
						arr[i].ToLower().Contains("time") ||
						arr[i].ToLower().Contains("depth") ||
						arr[i].ToLower().Contains("solution") ||
						arr[i].ToLower().Contains("gx") ||
						arr[i].ToLower().Contains("gy") ||
						arr[i].ToLower().Contains("gz") ||
						arr[i].ToLower().Contains("bx") ||
						arr[i].ToLower().Contains("by") ||
						arr[i].ToLower().Contains("bz") ||
						arr[i].ToLower().Contains("inc") ||
						arr[i].ToLower().Contains("azm") ||
						arr[i].ToLower().Contains("gti") ||
						arr[i].ToLower().Contains("bti") ||
						arr[i].ToLower().Contains("dip") ||
						arr[i].ToLower().Contains("btotal") ||
						arr[i].ToLower().Contains("g axes") ||
						arr[i].ToLower().Contains("type")
						)
						header_GB.Add(arr[i].Trim(), i);
				}
			}
			catch (Exception ex)
			{ throw new System.Exception(ex.ToString()); }
		}

        static void StandardHeaderTOPLINE(Dictionary<String, Int32> header_GB, String[] arr)
        {
            try
            {
                Int32 arrlength = arr.Length;
                for (Int32 i = 0; i < arrlength; i++)
                {
                    if (arr[i].ToLower().Contains("time") ||
                        arr[i].ToLower().Contains("depth") ||
                        arr[i].ToLower().Contains("solution") ||
                        arr[i].ToLower().Contains("gx") ||
                        arr[i].ToLower().Contains("gy") ||
                        arr[i].ToLower().Contains("gz") ||
                        arr[i].ToLower().Contains("bx") ||
                        arr[i].ToLower().Contains("by") ||
                        arr[i].ToLower().Contains("bz")
                        )
                        header_GB.Add(arr[i].Trim(), i);
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //public static Int32 insertRawResultDataFile(String FileName, String FileExtension, String UserName, String UserDomain, String ClientDBString)
        //{
        //    try
        //    {
        //        Int32 iReply = -99;
        //        String insData = "INSERT INTO [AuditQCDataFileName] ([qcfName], [qcfExtension], [username], [realm], [cTime], [uTime]) VALUES (@qcfName, @qcfExtension, @username, @realm, @cTime, @uTime); SELECT SCOPE_IDENTITY();";
        //        using (SqlConnection dataCon = new SqlConnection(ClientDBString))
        //        {
        //            try
        //            {
        //                dataCon.Open();
        //                SqlCommand insCmd = new SqlCommand(insData, dataCon);
        //                insCmd.Parameters.AddWithValue("@qcfName", SqlDbType.NVarChar).Value = FileName.ToString();
        //                insCmd.Parameters.AddWithValue("@qcfExtension", SqlDbType.NVarChar).Value = FileExtension.ToString();
        //                insCmd.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
        //                insCmd.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserDomain.ToString();
        //                insCmd.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
        //                insCmd.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
        //                iReply = Convert.ToInt32(insCmd.ExecuteScalar());
        //            }
        //            catch (Exception ex)
        //            { throw new System.Exception(ex.ToString()); }
        //            finally
        //            {
        //                dataCon.Close();
        //                dataCon.Dispose();
        //            }
        //        }
        //        return iReply;
        //    }
        //    catch (Exception ex)
        //    { throw new System.Exception(ex.ToString()); }
        //}

        //public static Int32 insertRawResultDataFileHeader(Int32 SysClientID, System.Data.DataTable HeaderTable, String ClientDBString)
        //{
        //    try
        //    {
        //        Int32 iReply = -99;
        //        String insCommand = "INSERT [AuditQCDataFileHeader] ([qcfID], [JobID], [WellID], [WSWID], [RunID], [qcfnSurveyDate], [qcfnCustomer], [qcfnJob], [qcfnField], [qcfnWell], [qcfnRig], [qcfnLoc], [qcfnEndDepth], [qcfnEndTVD], [qcfnVerticalSection], [qcfnHzDisplacement], [qcfnMethod], [qcfnMagRef], [qcfnTargetDir], [qcfnTTLMagField], [qcfnMagDip], [qcfnMagDec], [qcfnGridConvergence], [qcfnTTLCorrection], [qcfnTODepth], [qcfnTOTVD], [qcfnTOInc], [qcfnTOAzm], [qcfnTONS], [qcfnTOEW], [demID], [srvID], [cTime], [uTime], [username], [realm]) VALUES (@qcfID, @JobID, @WellID, @WSWID, @RunID, @qcfnSurveyDate, @qcfnCustomer, @qcfnJob, @qcfnField, @qcfnWell, @qcfnRig, @qcfnLoc, @qcfnEndDepth, @qcfnEndTVD, @qcfnVerticalSection, @qcfnHzDisplacement, @qcfnMethod, @qcfnMagRef, @qcfnTargetDir, @qcfnTTLMagField, @qcfnMagDip, @qcfnMagDec, @qcfnGridConvergence, @qcfnTTLCorrection, @qcfnTODepth, @qcfnTOTVD, @qcfnTOInc, @qcfnTOAzm, @qcfnTONS, @qcfnTOEW, @demID, @srvID, @cTime, @uTime, @username, @realm)";
        //        using (SqlConnection dataCon = new SqlConnection(ClientDBString))
        //        {
        //            try
        //            {
        //                dataCon.Open();
        //                foreach (DataRow row in HeaderTable.Rows)
        //                {
        //                    Int32 cfID = Convert.ToInt32(row[0]);
        //                    Int32 jobID = Convert.ToInt32(row[1]);
        //                    Int32 welID = Convert.ToInt32(row[2]);
        //                    Int32 wsID = Convert.ToInt32(row[3]);
        //                    Int32 runID = Convert.ToInt32(row[4]);
        //                    String date = Convert.ToString(row[5]);
        //                    String cust = Convert.ToString(row[6]);
        //                    String job = Convert.ToString(row[7]);
        //                    String field = Convert.ToString(row[8]);
        //                    String well = Convert.ToString(row[9]);
        //                    String rig = Convert.ToString(row[10]);
        //                    String loc = Convert.ToString(row[11]);
        //                    String endD = Convert.ToString(row[12]);
        //                    String endTVD = Convert.ToString(row[13]);
        //                    String vertical = Convert.ToString(row[14]);
        //                    String horiz = Convert.ToString(row[15]);
        //                    String method = Convert.ToString(row[16]);
        //                    String magref = Convert.ToString(row[17]);
        //                    String tDir = Convert.ToString(row[18]);
        //                    String ttlMF = Convert.ToString(row[19]);
        //                    String magdip = Convert.ToString(row[20]);
        //                    String magdec = Convert.ToString(row[21]);
        //                    String gc = Convert.ToString(row[22]);
        //                    String ttlCrr = Convert.ToString(row[23]);
        //                    String toDepth = Convert.ToString(row[24]);
        //                    String toTVD = Convert.ToString(row[25]);
        //                    String toInc = Convert.ToString(row[26]);
        //                    String toAzm = Convert.ToString(row[27]);
        //                    String toNS = Convert.ToString(row[28]);
        //                    String toEW = Convert.ToString(row[29]);
        //                    Int32 demID = Convert.ToInt32(row[30]);
        //                    Int32 srvID = Convert.ToInt32(row[31]);
        //                    DateTime cT = Convert.ToDateTime(row[32]);
        //                    DateTime uT = Convert.ToDateTime(row[33]);
        //                    String uName = Convert.ToString(row[34]);
        //                    String uRealm = Convert.ToString(row[35]);

        //                    SqlCommand insData = new SqlCommand(insCommand, dataCon);
        //                    insData.Parameters.AddWithValue("@qcfID", SqlDbType.Int).Value = cfID.ToString();
        //                    insData.Parameters.AddWithValue("@JobID", SqlDbType.Int).Value = jobID.ToString();
        //                    insData.Parameters.AddWithValue("@WellID", SqlDbType.Int).Value = welID.ToString();
        //                    insData.Parameters.AddWithValue("@WSWID", SqlDbType.Int).Value = wsID.ToString();
        //                    insData.Parameters.AddWithValue("@RunID", SqlDbType.Int).Value = runID.ToString();
        //                    insData.Parameters.AddWithValue("@qcfnSurveyDate", SqlDbType.NVarChar).Value = date.ToString();
        //                    insData.Parameters.AddWithValue("@qcfnCustomer", SqlDbType.NVarChar).Value = cust.ToString();
        //                    insData.Parameters.AddWithValue("@qcfnJob", SqlDbType.NVarChar).Value = job.ToString();
        //                    insData.Parameters.AddWithValue("@qcfnField", SqlDbType.NVarChar).Value = field.ToString();
        //                    insData.Parameters.AddWithValue("@qcfnWell", SqlDbType.NVarChar).Value = well.ToString();
        //                    insData.Parameters.AddWithValue("@qcfnRig", SqlDbType.NVarChar).Value = rig.ToString();
        //                    insData.Parameters.AddWithValue("@qcfnLoc", SqlDbType.NVarChar).Value = loc.ToString();
        //                    insData.Parameters.AddWithValue("@qcfnEndDepth", SqlDbType.NVarChar).Value = endD.ToString();
        //                    insData.Parameters.AddWithValue("@qcfnEndTVD", SqlDbType.NVarChar).Value = endTVD.ToString();
        //                    insData.Parameters.AddWithValue("@qcfnVerticalSection", SqlDbType.NVarChar).Value = vertical.ToString();
        //                    insData.Parameters.AddWithValue("@qcfnHzDisplacement", SqlDbType.NVarChar).Value = horiz.ToString();
        //                    insData.Parameters.AddWithValue("@qcfnMethod", SqlDbType.NVarChar).Value = method.ToString();
        //                    insData.Parameters.AddWithValue("@qcfnMagRef", SqlDbType.NVarChar).Value = magref.ToString();
        //                    insData.Parameters.AddWithValue("@qcfnTargetDir", SqlDbType.NVarChar).Value = tDir.ToString();
        //                    insData.Parameters.AddWithValue("@qcfnTTLMagField", SqlDbType.NVarChar).Value = ttlMF.ToString();
        //                    insData.Parameters.AddWithValue("@qcfnMagDip", SqlDbType.NVarChar).Value = magdip.ToString();
        //                    insData.Parameters.AddWithValue("@qcfnMagDec", SqlDbType.NVarChar).Value = magdec.ToString();
        //                    insData.Parameters.AddWithValue("@qcfnGridConvergence", SqlDbType.NVarChar).Value = gc.ToString();
        //                    insData.Parameters.AddWithValue("@qcfnTTLCorrection", SqlDbType.NVarChar).Value = ttlCrr.ToString();
        //                    insData.Parameters.AddWithValue("@qcfnTODepth", SqlDbType.NVarChar).Value = toDepth.ToString();
        //                    insData.Parameters.AddWithValue("@qcfnTOTVD", SqlDbType.NVarChar).Value = toTVD.ToString();
        //                    insData.Parameters.AddWithValue("@qcfnTOInc", SqlDbType.NVarChar).Value = toInc.ToString();
        //                    insData.Parameters.AddWithValue("@qcfnTOAzm", SqlDbType.NVarChar).Value = toAzm.ToString();
        //                    insData.Parameters.AddWithValue("@qcfnTONS", SqlDbType.NVarChar).Value = toNS.ToString();
        //                    insData.Parameters.AddWithValue("@qcfnTOEW", SqlDbType.NVarChar).Value = toEW.ToString();
        //                    insData.Parameters.AddWithValue("@demID", SqlDbType.Int).Value = demID.ToString();
        //                    insData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = srvID.ToString();
        //                    insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = cT.ToString();
        //                    insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = uT.ToString();
        //                    insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = uName.ToString();
        //                    insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = uRealm.ToString();

        //                    iReply = Convert.ToInt32(insData.ExecuteNonQuery());
        //                }
        //            }
        //            catch (Exception ex)
        //            { throw new System.Exception(ex.ToString()); }
        //            finally
        //            {
        //                dataCon.Close();
        //                dataCon.Dispose();
        //            }
        //        }
        //        return iReply;
        //    }
        //    catch (Exception ex)
        //    { throw new System.Exception(ex.ToString()); }
        //}

        //public static Int32 insertDataFile(String FileName, String FileExtension, String UserName, String UserDomain, String ClientDBString)
        //{
        //    try
        //    {
        //        Int32 iReply = -99;
        //        String insData = "INSERT INTO [CorrectionDataFileName] ([cfName], [cfExtension], [username], [realm], [cTime], [uTime]) VALUES (@cfName, @cfExtension, @username, @realm, @cTime, @uTime); SELECT SCOPE_IDENTITY();";
        //        using (SqlConnection dataCon = new SqlConnection(ClientDBString))
        //        {
        //            try
        //            {
        //                dataCon.Open();
        //                SqlCommand insCmd = new SqlCommand(insData, dataCon);
        //                insCmd.Parameters.AddWithValue("@cfName", SqlDbType.NVarChar).Value = FileName.ToString();
        //                insCmd.Parameters.AddWithValue("@cfExtension", SqlDbType.NVarChar).Value = FileExtension.ToString();
        //                insCmd.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
        //                insCmd.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserDomain.ToString();
        //                insCmd.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
        //                insCmd.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
        //                iReply = Convert.ToInt32(insCmd.ExecuteScalar());
        //            }
        //            catch (Exception ex)
        //            { throw new System.Exception(ex.ToString()); }
        //            finally
        //            {
        //                dataCon.Close();
        //                dataCon.Dispose();
        //            }
        //        }
        //        return iReply;
        //    }
        //    catch (Exception ex)
        //    { throw new System.Exception(ex.ToString()); }
        //}

        //public static Int32 insertDataFileHeader(Int32 SysClientID, System.Data.DataTable HeaderTable, String ClientDBString)
        //{
        //    try
        //    {
        //        Int32 iReply = -99;
        //        String insCommand = "INSERT [CorrectionDataFileHeader] ([cfID], [JobID], [WellID], [WSWID], [RunID], [cfnSurveyDate], [cfnCustomer], [cfnJob], [cfnField], [cfnWell], [cfnRig], [cfnLoc], [cfnEndDepth], [cfnEndTVD], [cfnVerticalSection], [cfnHzDisplacement], [cfnMethod], [cfnMagRef], [cfnTargetDir], [cfnTTLMagField], [cfnMagDip], [cfnMagDec], [cfnGridConvergence], [cfnTTLCorrection], [cfnTODepth], [cfnTOTVD], [cfnTOInc], [cfnTOAzm], [cfnTONS], [cfnTOEW], [demID], [srvID], [cTime], [uTime], [username], [realm]) VALUES (@cfID, @JobID, @WellID, @WSWID, @RunID, @cfnSurveyDate, @cfnCustomer, @cfnJob, @cfnField, @cfnWell, @cfnRig, @cfnLoc, @cfnEndDepth, @cfnEndTVD, @cfnVerticalSection, @cfnHzDisplacement, @cfnMethod, @cfnMagRef, @cfnTargetDir, @cfnTTLMagField, @cfnMagDip, @cfnMagDec, @cfnGridConvergence, @cfnTTLCorrection, @cfnTODepth, @cfnTOTVD, @cfnTOInc, @cfnTOAzm, @cfnTONS, @cfnTOEW, @demID, @srvID, @cTime, @uTime, @username, @realm)";
        //        using (SqlConnection dataCon = new SqlConnection(ClientDBString))
        //        {
        //            try
        //            {
        //                dataCon.Open();
        //                foreach (DataRow row in HeaderTable.Rows)
        //                {
        //                    Int32 cfID = Convert.ToInt32(row[0]);
        //                    Int32 jobID = Convert.ToInt32(row[1]);
        //                    Int32 welID = Convert.ToInt32(row[2]);
        //                    Int32 wsID = Convert.ToInt32(row[3]);
        //                    Int32 runID = Convert.ToInt32(row[4]);
        //                    String date = Convert.ToString(row[5]);
        //                    String cust = Convert.ToString(row[6]);
        //                    String job = Convert.ToString(row[7]);
        //                    String field = Convert.ToString(row[8]);
        //                    String well = Convert.ToString(row[9]);
        //                    String rig = Convert.ToString(row[10]);
        //                    String loc = Convert.ToString(row[11]);
        //                    String endD = Convert.ToString(row[12]);
        //                    String endTVD = Convert.ToString(row[13]);
        //                    String vertical = Convert.ToString(row[14]);
        //                    String horiz = Convert.ToString(row[15]);
        //                    String method = Convert.ToString(row[16]);
        //                    String magref = Convert.ToString(row[17]);
        //                    String tDir = Convert.ToString(row[18]);
        //                    String ttlMF = Convert.ToString(row[19]);
        //                    String magdip = Convert.ToString(row[20]);
        //                    String magdec = Convert.ToString(row[21]);
        //                    String gc = Convert.ToString(row[22]);
        //                    String ttlCrr = Convert.ToString(row[23]);
        //                    String toDepth = Convert.ToString(row[24]);
        //                    String toTVD = Convert.ToString(row[25]);
        //                    String toInc = Convert.ToString(row[26]);
        //                    String toAzm = Convert.ToString(row[27]);
        //                    String toNS = Convert.ToString(row[28]);
        //                    String toEW = Convert.ToString(row[29]);
        //                    Int32 demID = Convert.ToInt32(row[30]);
        //                    Int32 srvID = Convert.ToInt32(row[31]);
        //                    DateTime cT = Convert.ToDateTime(row[32]);
        //                    DateTime uT = Convert.ToDateTime(row[33]);
        //                    String uName = Convert.ToString(row[34]);
        //                    String uRealm = Convert.ToString(row[35]);

        //                    SqlCommand insData = new SqlCommand(insCommand, dataCon);
        //                    insData.Parameters.AddWithValue("@cfID", SqlDbType.Int).Value = cfID.ToString();
        //                    insData.Parameters.AddWithValue("@JobID", SqlDbType.Int).Value = jobID.ToString();
        //                    insData.Parameters.AddWithValue("@WellID", SqlDbType.Int).Value = welID.ToString();
        //                    insData.Parameters.AddWithValue("@WSWID", SqlDbType.Int).Value = wsID.ToString();
        //                    insData.Parameters.AddWithValue("@RunID", SqlDbType.Int).Value = runID.ToString();
        //                    insData.Parameters.AddWithValue("@cfnSurveyDate", SqlDbType.NVarChar).Value = date.ToString();
        //                    insData.Parameters.AddWithValue("@cfnCustomer", SqlDbType.NVarChar).Value = cust.ToString();
        //                    insData.Parameters.AddWithValue("@cfnJob", SqlDbType.NVarChar).Value = job.ToString();
        //                    insData.Parameters.AddWithValue("@cfnField", SqlDbType.NVarChar).Value = field.ToString();
        //                    insData.Parameters.AddWithValue("@cfnWell", SqlDbType.NVarChar).Value = well.ToString();
        //                    insData.Parameters.AddWithValue("@cfnRig", SqlDbType.NVarChar).Value = rig.ToString();
        //                    insData.Parameters.AddWithValue("@cfnLoc", SqlDbType.NVarChar).Value = loc.ToString();
        //                    insData.Parameters.AddWithValue("@cfnEndDepth", SqlDbType.NVarChar).Value = endD.ToString();
        //                    insData.Parameters.AddWithValue("@cfnEndTVD", SqlDbType.NVarChar).Value = endTVD.ToString();
        //                    insData.Parameters.AddWithValue("@cfnVerticalSection", SqlDbType.NVarChar).Value = vertical.ToString();
        //                    insData.Parameters.AddWithValue("@cfnHzDisplacement", SqlDbType.NVarChar).Value = horiz.ToString();
        //                    insData.Parameters.AddWithValue("@cfnMethod", SqlDbType.NVarChar).Value = method.ToString();
        //                    insData.Parameters.AddWithValue("@cfnMagRef", SqlDbType.NVarChar).Value = magref.ToString();
        //                    insData.Parameters.AddWithValue("@cfnTargetDir", SqlDbType.NVarChar).Value = tDir.ToString();
        //                    insData.Parameters.AddWithValue("@cfnTTLMagField", SqlDbType.NVarChar).Value = ttlMF.ToString();
        //                    insData.Parameters.AddWithValue("@cfnMagDip", SqlDbType.NVarChar).Value = magdip.ToString();
        //                    insData.Parameters.AddWithValue("@cfnMagDec", SqlDbType.NVarChar).Value = magdec.ToString();
        //                    insData.Parameters.AddWithValue("@cfnGridConvergence", SqlDbType.NVarChar).Value = gc.ToString();
        //                    insData.Parameters.AddWithValue("@cfnTTLCorrection", SqlDbType.NVarChar).Value = ttlCrr.ToString();
        //                    insData.Parameters.AddWithValue("@cfnTODepth", SqlDbType.NVarChar).Value = toDepth.ToString();
        //                    insData.Parameters.AddWithValue("@cfnTOTVD", SqlDbType.NVarChar).Value = toTVD.ToString();
        //                    insData.Parameters.AddWithValue("@cfnTOInc", SqlDbType.NVarChar).Value = toInc.ToString();
        //                    insData.Parameters.AddWithValue("@cfnTOAzm", SqlDbType.NVarChar).Value = toAzm.ToString();
        //                    insData.Parameters.AddWithValue("@cfnTONS", SqlDbType.NVarChar).Value = toNS.ToString();
        //                    insData.Parameters.AddWithValue("@cfnTOEW", SqlDbType.NVarChar).Value = toEW.ToString();
        //                    insData.Parameters.AddWithValue("@demID", SqlDbType.Int).Value = demID.ToString();
        //                    insData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = srvID.ToString();
        //                    insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = cT.ToString();
        //                    insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = uT.ToString();
        //                    insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = uName.ToString();
        //                    insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = uRealm.ToString();

        //                    iReply = Convert.ToInt32(insData.ExecuteNonQuery());
        //                }
        //            }
        //            catch (Exception ex)
        //            { throw new System.Exception(ex.ToString()); }
        //            finally
        //            {
        //                dataCon.Close();
        //                dataCon.Dispose();
        //            }
        //        }
        //        return iReply;  
        //    }
        //    catch (Exception ex)
        //    { throw new System.Exception(ex.ToString()); }
        //}        

        public static Int32 insertDataFileRows(Int32 OperatorID, Int32 ClientID, Int32 WellPadID, Int32 WellID, Int32 WellSectionID, Int32 RunID, Int32 ServiceID, Int32 DataEntryMethodID, Int32 DataFileID, System.Data.DataTable FileDataTable, String UserName, String UserDomain, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99, sID, rLen, rAcu, rMu;                
                DateTime rStamp = DateTime.Now;
                String insCommand = String.Empty;
                Decimal rMD, rGx, rGy, rGz, rBx, rBy, rBz;
                if (ServiceID.Equals(46)) //RawData MSA
                {
                    insCommand = "INSERT INTO [CorrectionUploadedData] ([cudDateTime], [cudDepth], [lenU], [cudGx], [cudGy], [cudGz], [cudBx], [cudBy], [cudBz], [acuID], [muID], [optrID], [clntID], [wpdID], [welID], [runID], [wswID], [srvyRowID], [srvID], [demID], [cfID], [cTime], [uTime], [username], [realm]) VALUES (@cudDateTime, @cudDepth, @lenU, @cudGx, @cudGy, @cudGz, @cudBx, @cudBy, @cudBz, @acuID, @muID, @optrID, @clntID, @wpdID, @welID, @runID, @wswID, @srvyRowID, @srvID, @demID, @cfID, @cTime, @uTime, @username, @realm)";
                }
                else //IncDipMSA
                {
                    insCommand = "INSERT INTO [IncDipCorrFormattedData] ([crrDateTime], [crrDepth], [lenU], [crrGx], [crrGy], [crrGz], [crrBx], [crrBy], [crrBz], [acuID], [muID], [optrID], [clntID], [welID], [runID], [wswID], [srvyRowID], [srvID], [demID], [cfID], [cTime], [uTime], [username], [realm]) VALUES (@crrDateTime, @crrDepth, @lenU, @crrGx, @crrGy, @crrGz, @crrBx, @crrBy, @crrBz, @acuID, @muID, @optrID, @clntID, @welID, @runID, @wswID, @srvyRowID, @srvID, @demID, @cfID, @cTime, @uTime, @username, @realm)";
                }
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        foreach (DataRow row in FileDataTable.Rows)
                        {
                            if (ServiceID.Equals(46))
                            {
                                sID = getCorrectionNextSequence(OperatorID, ClientID, WellPadID, WellID, WellSectionID, RunID, ClientDBString);
                            }
                            else
                            {
                                sID = getCorrectionNextSequenceIncDip(RunID, ClientDBString);
                            }
                            rStamp = Convert.ToDateTime(row["Timestamp"]);
                            rMD = Convert.ToDecimal(row["Depth"]);
                            rLen = Convert.ToInt32(row["lenU"]);
                            rGx = Convert.ToDecimal(row["Gx"]);
                            rGy = Convert.ToDecimal(row["Gy"]);
                            rGz = Convert.ToDecimal(row["Gz"]);
                            rBx = Convert.ToDecimal(row["Bx"]);
                            rBy = Convert.ToDecimal(row["By"]);
                            rBz = Convert.ToDecimal(row["Bz"]);
                            rAcu = Convert.ToInt32(row["acU"]);
                            rMu = Convert.ToInt32(row["magU"]);

                            SqlCommand insData = new SqlCommand(insCommand, dataCon);
                            insData.Parameters.AddWithValue("@cudDateTime", SqlDbType.DateTime2).Value = rStamp.ToString();
                            insData.Parameters.AddWithValue("@cudDepth", SqlDbType.Decimal).Value = rMD.ToString();
                            insData.Parameters.AddWithValue("@lenU", SqlDbType.Int).Value = rLen.ToString();
                            insData.Parameters.AddWithValue("@cudGx", SqlDbType.Decimal).Value = rGx.ToString();
                            insData.Parameters.AddWithValue("@cudGy", SqlDbType.Decimal).Value = rGy.ToString();
                            insData.Parameters.AddWithValue("@cudGz", SqlDbType.Decimal).Value = rGz.ToString();
                            insData.Parameters.AddWithValue("@cudBx", SqlDbType.Decimal).Value = rBx.ToString();
                            insData.Parameters.AddWithValue("@cudBy", SqlDbType.Decimal).Value = rBy.ToString();
                            insData.Parameters.AddWithValue("@cudBz", SqlDbType.Decimal).Value = rBz.ToString();
                            insData.Parameters.AddWithValue("@acuID", SqlDbType.Int).Value = rAcu.ToString();
                            insData.Parameters.AddWithValue("@muID", SqlDbType.Int).Value = rMu.ToString();
                            insData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                            insData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                            insData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                            insData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                            insData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                            insData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                            insData.Parameters.AddWithValue("@srvyRowID", SqlDbType.Int).Value = sID.ToString();
                            insData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                            insData.Parameters.AddWithValue("@demID", SqlDbType.Int).Value = DataEntryMethodID.ToString();
                            insData.Parameters.AddWithValue("@cfID", SqlDbType.Int).Value = DataFileID.ToString();
                            insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                            insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                            insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                            insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserDomain.ToString();

                            iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable uploadManualRawDataRow(Int32 ServiceID, Int32 WellID, Int32 SectionID, Int32 RunID, Int32 DataEntryMethodID, Int32 DataFormatID, String DataRowText, String LoginName, String LoginRealm, String ClientDBString)
        {
            try
            {
                String[] str;
                Int32 id = 0;
                String depth, Gx, Gy, Gz, Bx, By, Bz, timeStamp;
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn drID = iReply.Columns.Add("drID", typeof(String));
                DataColumn rStamp = iReply.Columns.Add("Timestamp", typeof(String));
                DataColumn rMD = iReply.Columns.Add("Depth", typeof(String));
                DataColumn rGx = iReply.Columns.Add("Gx", typeof(String));
                DataColumn rGy = iReply.Columns.Add("Gy", typeof(String));
                DataColumn rGz = iReply.Columns.Add("Gz", typeof(String));
                DataColumn rBx = iReply.Columns.Add("Bx", typeof(String));
                DataColumn rBy = iReply.Columns.Add("By", typeof(String));
                DataColumn rBz = iReply.Columns.Add("Bz", typeof(String));
                // Block for Manual QC survey Processing
                try
                {
                    {
                        if (DataRowText.Contains(",") && DataRowText.Contains(",") && DataRowText.Contains(","))
                        {
                            str = DataRowText.Split(',');
                            {
                                id++;
                                if (String.IsNullOrEmpty(str[0].Trim()))
                                {
                                    timeStamp = "-99";
                                }
                                else
                                {
                                    timeStamp = str[0]; if (String.IsNullOrEmpty(timeStamp)) { timeStamp = "---"; }
                                    if (!timeStamp.Equals("---")) { timeStamp = Convert.ToString(timeStamp); } else { timeStamp = Convert.ToString(TimeSpan.Zero); }
                                }
                                if (String.IsNullOrEmpty(str[1].Trim()))
                                {
                                    depth = "-99.00M";
                                }
                                else
                                {
                                    depth = Convert.ToString(str[1]);
                                }
                                if (String.IsNullOrEmpty(str[2].Trim()))
                                {
                                    Gx = "-99.00M";
                                }
                                else
                                {
                                    Gx = Convert.ToString(str[2].Trim());
                                }
                                if (String.IsNullOrEmpty(str[3].Trim()))
                                {
                                    Gy = "-99.00M";
                                }
                                else
                                {
                                    Gy = Convert.ToString(str[3].Trim());
                                }
                                if (String.IsNullOrEmpty(str[4].Trim()))
                                {
                                    Gz = "-99.00M";
                                }
                                else
                                {
                                    Gz = Convert.ToString(str[4].Trim());
                                }
                                if (String.IsNullOrEmpty(str[5].Trim()))
                                {
                                    Bx = "-99.00M";
                                }
                                else
                                {
                                    Bx = Convert.ToString(str[5].Trim());
                                }
                                if (String.IsNullOrEmpty(str[6].Trim()))
                                {
                                    By = "-99.00M";
                                }
                                else
                                {
                                    By = Convert.ToString(str[6].Trim());
                                }
                                if (String.IsNullOrEmpty(str[7].Trim()))
                                {
                                    Bz = "-99.00M";
                                }
                                else
                                {
                                    Bz = Convert.ToString(str[7].Trim());
                                }
                                iReply.Rows.Add(id, timeStamp, depth, Gx, Gy, Gz, Bx, By, Bz);
                                iReply.AcceptChanges();
                            }
                        }
                    }
                    return iReply;
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable uploadManualResultsDataRow(Int32 ServiceID, Int32 WellID, Int32 SectionID, Int32 RunID, Int32 DataEntryMethodID, Int32 DataFormatID, String DataRowText, String LoginName, String LoginRealm, String ClientDBString)
        {
            try
            {
                String[] str;
                String depth, Inc, Azm, Dip, BTotal, GTotal, GTF;
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn drID = iReply.Columns.Add("drID", typeof(String));
                DataColumn rMD = iReply.Columns.Add("Depth", typeof(String));
                DataColumn ResInc = iReply.Columns.Add("ResInc", typeof(String));
                DataColumn ResAzm = iReply.Columns.Add("ResAzm", typeof(String));
                DataColumn ResDip = iReply.Columns.Add("ResDip", typeof(String));
                DataColumn ResBT = iReply.Columns.Add("ResBT", typeof(String));
                DataColumn ResGT = iReply.Columns.Add("ResGT", typeof(String));
                DataColumn ResGTF = iReply.Columns.Add("ResGTF", typeof(String));                
                                    
                        if (DataRowText.Contains(",") && DataRowText.Contains(",") && DataRowText.Contains(","))
                        {
                            str = DataRowText.Split(',');
                            {
                                if (String.IsNullOrEmpty(str[0].Trim()))
                                {
                                    depth = "-99.00M";
                                }
                                else
                                {
                                    depth = Convert.ToString(str[0]);
                                }
                                if (String.IsNullOrEmpty(str[1].Trim()))
                                {
                                    Inc = "-99.00M";
                                }
                                else
                                {
                                    Inc = Convert.ToString(str[1].Trim());
                                }
                                if (String.IsNullOrEmpty(str[2].Trim()))
                                {
                                    Azm = "-99.00M";
                                }
                                else
                                {
                                    Azm = Convert.ToString(str[2].Trim());
                                }
                                if (String.IsNullOrEmpty(str[3].Trim()))
                                {
                                    Dip = "-99.00M";
                                }
                                else
                                {
                                    Dip = Convert.ToString(str[3].Trim());
                                }
                                if (String.IsNullOrEmpty(str[4].Trim()))
                                {
                                    BTotal = "-99";
                                }
                                else
                                {
                                    BTotal = Convert.ToString(str[4].Trim());
                                }
                                if (String.IsNullOrEmpty(str[5].Trim()))
                                {
                                    GTotal = "-99.00M";
                                }
                                else
                                {
                                    GTotal = Convert.ToString(str[5].Trim());
                                }
                                if (String.IsNullOrEmpty(str[6].Trim()))
                                {
                                    GTF = "-99.00M";
                                }
                                else
                                {
                                    GTF = Convert.ToString(str[6].Trim());
                                }
                                iReply.Rows.Add(1, depth, Inc, Azm, Dip, BTotal, GTotal, GTF);
                                iReply.AcceptChanges();                               
                            }                            
                        }                    
                    return iReply;
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 parkRawResultDataRow(String Enabled, String Timestamp, String Depth, String Solution, String Gx, String Gy, String Gz, String Bx, String By, String Bz, String Inc, String Azm, String GTI, String BTI, String Dip, String BTotal, String GAxes, String Type, String ResInc, String ResAzm, String ResDip, String ResBTotal, String ResGTotal, String ResGTF, Int32 RunID, Int32 SectionID, Int32 WellID, Int32 JobID, Int32 DataEntryMethodID, Int32 DataTypeID, Int32 ServiceID, Int32 DataFileID, String LoginName, String LoginRealm, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String insData = "INSERT INTO [AuditQCUploadedData] ([dEnabled], [dDateTime], [dDepth], [dSolution], [dGx], [dGy], [dGz], [dBx], [dBy], [dBz], [dInc], [dAzm], [dGTI], [dBTI], [dDip], [dBTotal], [dGAxes], [dType], [dResInc], [dResAzm], [dResDip], [dResBTotal], [dResGTotal], [dResGTF], [runID], [wswID], [welID], [jobID], [demID], [dtID], [srvID], [cfID], [dataProcessed], [LoginName], [domain], [cTime]) VALUES (@dEnabled, @dDateTime, @dDepth, @dSolution, @dGx, @dGy, @dGz, @dBx, @dBy, @dBz, @dInc, @dAzm, @dGTI, @dBTI, @dDip, @dBTotal, @dGAxes, @dType, @dResInc, @dResAzm, @dResDip, @dResBTotal, @dResGTotal, @dResGTF, @runID, @wswID, @welID, @jobID, @demID, @dtID, @srvID, @cfID, @dataProcessed, @LoginName, @domain, @cTime)";
                Int32 dEnb, dSol, dBT, dGAxes, dType;
                DateTime TimeNow = DateTime.Now;
                Decimal dDepth, dGx, dGy, dGz, dBx, dBy, dBz, dInc, dAzm, dGTI, dBTI, dDip, dResInc, dResAzm, dResDip, dResBTotal, dResGTotal, dResGTF;
                if (Enabled.Equals("-99")) { dEnb = -99; } else { dEnb = Convert.ToInt32(Enabled); }
                if (Timestamp.Equals("-99")) { TimeNow = DateTime.Now; } else { TimeNow = Convert.ToDateTime(Timestamp); }
                if (Depth.Equals("-99.00M")) { dDepth = -99.00M; } else { dDepth = Convert.ToDecimal(Depth); }
                if (Solution.Equals("-99")) { dSol = -99; } else { dSol = Convert.ToInt32(Solution); }
                if (Gx.Equals("-99.00M")) { dGx = -99.00M; } else { dGx = Convert.ToDecimal(Gx); }
                if (Gy.Equals("-99.00M")) { dGy = -99.00M; } else { dGy = Convert.ToDecimal(Gy); }
                if (Gz.Equals("-99.00M")) { dGz = -99.00M; } else { dGz = Convert.ToDecimal(Gz); }
                if (Bx.Equals("-99.00M")) { dBx = -99.00M; } else { dBx = Convert.ToDecimal(Bx); }
                if (By.Equals("-99.00M")) { dBy = -99.00M; } else { dBy = Convert.ToDecimal(By); }
                if (Bz.Equals("-99.00M")) { dBz = -99.00M; } else { dBz = Convert.ToDecimal(Bz); }
                if (Inc.Equals("-99.00M")) { dInc = -99.00M; } else { dInc = Convert.ToDecimal(Inc); }
                if (Azm.Equals("-99.00M")) { dAzm = -99.00M; } else { dAzm = Convert.ToDecimal(Azm); }
                if (GTI.Equals("-99.00M")) { dGTI = -99.00M; } else { dGTI = Convert.ToDecimal(GTI); }
                if (BTI.Equals("-99.00M")) { dBTI = -99.00M; } else { dBTI = Convert.ToDecimal(BTI); }
                if (Dip.Equals("-99.00M")) { dDip = -99.00M; } else { dDip = Convert.ToDecimal(Dip); }
                if (BTotal.Equals("-99")) { dBT = -99; } else { dBT = Convert.ToInt32(BTotal); }
                if (GAxes.Equals("-99")) { dGAxes = -99; } else { dGAxes = Convert.ToInt32(GAxes); }
                if (Type.Equals("-99")) { dType = -99; } else { dType = Convert.ToInt32(Type); }
                if (ResInc.Equals("-99.00M")) { dResInc = -99.00M; } else { dResInc = Convert.ToDecimal(ResInc); }
                if (ResAzm.Equals("-99.00M")) { dResAzm = -99.00M; } else { dResAzm = Convert.ToDecimal(ResAzm); }
                if (ResDip.Equals("-99.00M")) { dResDip = -99.00M; } else { dResDip = Convert.ToDecimal(ResDip); }
                if (ResBTotal.Equals("-99")) { dResBTotal = -99; } else { dResBTotal = Convert.ToDecimal(ResBTotal); }
                if (ResGTotal.Equals("-99.00M")) { dResGTotal = -99.00M; } else { dResGTotal = Convert.ToDecimal(ResGTotal); }
                if (ResGTF.Equals("-99.00M")) { dResGTF = -99.00M; } else { dResGTF = Convert.ToDecimal(ResGTF); }
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insertCom = new SqlCommand(insData, dataCon);
                        insertCom.Parameters.AddWithValue("@dEnabled", SqlDbType.Int).Value = dEnb.ToString();
                        insertCom.Parameters.AddWithValue("@dDateTime", SqlDbType.DateTime2).Value = TimeNow.ToString();
                        insertCom.Parameters.AddWithValue("@dDepth", SqlDbType.Decimal).Value = dDepth.ToString();
                        insertCom.Parameters.AddWithValue("@dSolution", SqlDbType.Int).Value = dSol.ToString();
                        insertCom.Parameters.AddWithValue("@dGx", SqlDbType.Decimal).Value = dGx.ToString();
                        insertCom.Parameters.AddWithValue("@dGy", SqlDbType.Decimal).Value = dGy.ToString();
                        insertCom.Parameters.AddWithValue("@dGz", SqlDbType.Decimal).Value = dGz.ToString();
                        insertCom.Parameters.AddWithValue("@dBx", SqlDbType.Decimal).Value = dBx.ToString();
                        insertCom.Parameters.AddWithValue("@dBy", SqlDbType.Decimal).Value = dBy.ToString();
                        insertCom.Parameters.AddWithValue("@dBz", SqlDbType.Decimal).Value = dBz.ToString();
                        insertCom.Parameters.AddWithValue("@dInc", SqlDbType.Decimal).Value = dInc.ToString();
                        insertCom.Parameters.AddWithValue("@dAzm", SqlDbType.Decimal).Value = dAzm.ToString();
                        insertCom.Parameters.AddWithValue("@dGTI", SqlDbType.Decimal).Value = dGTI.ToString();
                        insertCom.Parameters.AddWithValue("@dBTI", SqlDbType.Decimal).Value = dBTI.ToString();
                        insertCom.Parameters.AddWithValue("@dDip", SqlDbType.Decimal).Value = dDip.ToString();
                        insertCom.Parameters.AddWithValue("@dBTotal", SqlDbType.Int).Value = dBT.ToString();
                        insertCom.Parameters.AddWithValue("@dGAxes", SqlDbType.Int).Value = dGAxes.ToString();
                        insertCom.Parameters.AddWithValue("@dType", SqlDbType.Int).Value = dType.ToString();
                        insertCom.Parameters.AddWithValue("@dResInc", SqlDbType.Decimal).Value = dResInc.ToString();
                        insertCom.Parameters.AddWithValue("@dResAzm", SqlDbType.Decimal).Value = dResAzm.ToString();
                        insertCom.Parameters.AddWithValue("@dResDip", SqlDbType.Decimal).Value = dResDip.ToString();
                        insertCom.Parameters.AddWithValue("@dResBTotal", SqlDbType.Int).Value = Convert.ToInt32(dResBTotal).ToString();
                        insertCom.Parameters.AddWithValue("@dResGTotal", SqlDbType.Decimal).Value = dResGTotal.ToString();
                        insertCom.Parameters.AddWithValue("@dResGTF", SqlDbType.Decimal).Value = dResGTF.ToString();
                        insertCom.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        insertCom.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = SectionID.ToString();
                        insertCom.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        insertCom.Parameters.AddWithValue("@jobID", SqlDbType.Int).Value = JobID.ToString();
                        insertCom.Parameters.AddWithValue("@demID", SqlDbType.Int).Value = DataEntryMethodID.ToString();
                        insertCom.Parameters.AddWithValue("@dtID", SqlDbType.Int).Value = DataTypeID.ToString();
                        insertCom.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        insertCom.Parameters.AddWithValue("@cfID", SqlDbType.Int).Value = DataFileID.ToString();
                        insertCom.Parameters.AddWithValue("@dataProcessed", SqlDbType.Int).Value = '0'.ToString();
                        insertCom.Parameters.AddWithValue("@LoginName", SqlDbType.NVarChar).Value = LoginName.ToString();
                        insertCom.Parameters.AddWithValue("@domain", SqlDbType.NVarChar).Value = LoginRealm.ToString();
                        insertCom.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();

                        iReply = Convert.ToInt32(insertCom.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<System.Data.DataTable> uploadDataTextFile(Int32 WellNumberID, Int32 WellSectionID, Int32 RunNumberID, String DataFileName, String DataFileExtension, String DataFilePath, Int32 ServiceID, Int32 DataInputMethod, String UserName, String UserRealm, String ClientDBConnection)
        {
            try
            {
                List<System.Data.DataTable> iReply = new List<System.Data.DataTable>();
                String[] lineDetail, val, columnOrder;
                Int32[] flip = new Int32[6];
                Decimal[] gb_decimals = new Decimal[6];
                String dataLine;
                Int32 lineCount = 0, drowID = 0, dfileID = -99;
                ArrayList DetailList = new ArrayList();
                Dictionary<String, String> lineValue = new Dictionary<String, String>();
                Dictionary<String, Int32> HeaderGB = new Dictionary<String, Int32>();
                String fSurveyDate, fCustomer, fJob, fWell, fField, fRig, fLocation, fEndDepth, fEndTVD, fVertical, fHorizontal, fMethod, fMagRef, fTDir, fTTLMag, fDip, fMDec, fGC, fTTLC, fTOD;
                fSurveyDate = fCustomer = fJob = fWell = fField = fRig = fLocation = fEndDepth = fEndTVD = fVertical = fHorizontal = fMethod = fMagRef = fTDir = fTTLMag = fDip = fMDec = fGC = fTTLC = fTOD = String.Empty;
                String fTOTVD, fTOInc, fTOAzm, fTONS, fTOEW;
                fTOTVD = fTOInc = fTOAzm = fTONS = fTOEW = String.Empty;
                String timeStamp, depth, Gx, Gy, Gz, Bx, By, Bz, Inc, Azm, GTI, BTI, Dip;
                String enabled, solution, BTotal, GAxes, cnType;
                DateTime rdStamp;
                Int32 rdEnab, rdSol, rdBT, rdGA, rdTyp;
                Decimal rdDepth, rdGx, rdGy, rdGz, rdBx, rdBy, rdBz, rdInc, rdAzm, rdGTI, rdBTI, rdDip;
                System.Data.DataTable dataTable = new System.Data.DataTable();
                DataColumn ddrID = dataTable.Columns.Add("drID", typeof(Int32));
                DataColumn dEnabled = dataTable.Columns.Add("Enabled", typeof(String));
                DataColumn dStamp = dataTable.Columns.Add("Timestamp", typeof(String));
                DataColumn dMD = dataTable.Columns.Add("Depth", typeof(String));
                DataColumn dSol = dataTable.Columns.Add("Solution", typeof(String));
                DataColumn dGx = dataTable.Columns.Add("Gx", typeof(String));
                DataColumn dGy = dataTable.Columns.Add("Gy", typeof(String));
                DataColumn dGz = dataTable.Columns.Add("Gz", typeof(String));
                DataColumn dBx = dataTable.Columns.Add("Bx", typeof(String));
                DataColumn dBy = dataTable.Columns.Add("By", typeof(String));
                DataColumn dBz = dataTable.Columns.Add("Bz", typeof(String));
                DataColumn dInc = dataTable.Columns.Add("Inc", typeof(String));
                DataColumn dAzm = dataTable.Columns.Add("Azm", typeof(String));
                DataColumn dGTI = dataTable.Columns.Add("GTI", typeof(String));
                DataColumn dBTI = dataTable.Columns.Add("BTI", typeof(String));
                DataColumn dDip = dataTable.Columns.Add("Dip", typeof(String));
                DataColumn dBT = dataTable.Columns.Add("BTotal", typeof(String));
                DataColumn dGAxes = dataTable.Columns.Add("GAxes", typeof(String));
                DataColumn dType = dataTable.Columns.Add("Type", typeof(String));
                System.Data.DataTable fileData = new System.Data.DataTable();
                DataColumn drID = fileData.Columns.Add("drID", typeof(Int32));
                DataColumn rEnabled = fileData.Columns.Add("Enabled", typeof(Int32));
                DataColumn rStamp = fileData.Columns.Add("Timestamp", typeof(DateTime));
                DataColumn rMD = fileData.Columns.Add("Depth", typeof(Decimal));
                DataColumn rSol = fileData.Columns.Add("Solution", typeof(Int32));
                DataColumn rGx = fileData.Columns.Add("Gx", typeof(Decimal));
                DataColumn rGy = fileData.Columns.Add("Gy", typeof(Decimal));
                DataColumn rGz = fileData.Columns.Add("Gz", typeof(Decimal));
                DataColumn rBx = fileData.Columns.Add("Bx", typeof(Decimal));
                DataColumn rBy = fileData.Columns.Add("By", typeof(Decimal));
                DataColumn rBz = fileData.Columns.Add("Bz", typeof(Decimal));
                DataColumn rInc = fileData.Columns.Add("Inc", typeof(Decimal));
                DataColumn rAzm = fileData.Columns.Add("Azm", typeof(Decimal));
                DataColumn rGTI = fileData.Columns.Add("GTI", typeof(Decimal));
                DataColumn rBTI = fileData.Columns.Add("BTI", typeof(Decimal));
                DataColumn rDip = fileData.Columns.Add("Dip", typeof(Decimal));
                DataColumn rBT = fileData.Columns.Add("BTotal", typeof(Int32));
                DataColumn rGAxes = fileData.Columns.Add("GAxes", typeof(Int32));
                DataColumn rType = fileData.Columns.Add("Type", typeof(Int32));
                StreamReader fileReader = new StreamReader(DataFilePath);
                do
                {
                    dataLine = Convert.ToString(fileReader.ReadLine());
                    lineCount++;
                    drowID++;
                    if (dataLine.Contains(",") && dataLine.Contains(",") && dataLine.Contains(","))
                    {
                        if (dataLine.ToLower().Contains("btotal") || dataLine.ToLower().Contains("gx") || dataLine.ToLower().Contains("bx") || dataLine.ToLower().Contains("type"))
                        {
                            columnOrder = dataLine.Split(',');
                            HeaderTOPLINE(HeaderGB, columnOrder);
                        }
                        else
                        {
                            lineDetail = dataLine.Split(',');

                            if (String.IsNullOrEmpty(lineDetail[5]) || String.IsNullOrEmpty(lineDetail[4]) || String.IsNullOrEmpty(lineDetail[5]) || String.IsNullOrEmpty(lineDetail[6]) || String.IsNullOrEmpty(lineDetail[7]) || String.IsNullOrEmpty(lineDetail[8]))
                            {
                            }
                            else
                            {                                
                                DetailsProcessor(HeaderGB, lineDetail, DetailList, flip, gb_decimals);
                                enabled = lineDetail[0]; if (String.IsNullOrEmpty(enabled)) { enabled = "---"; }
                                if (!enabled.Equals("---")){ rdEnab = Convert.ToInt32(enabled); } else { rdEnab = -99; }
                                timeStamp = lineDetail[1]; if (String.IsNullOrEmpty(timeStamp)) { timeStamp = "---"; }
                                if(!timeStamp.Equals("---")){ rdStamp = Convert.ToDateTime(timeStamp); } else {rdStamp = DateTime.Now;}
                                depth = lineDetail[2]; if (String.IsNullOrEmpty(depth)) { depth = "---"; }
                                if (!depth.Equals("---")) { rdDepth = Convert.ToDecimal(depth); } else { rdDepth = -99.00M; }
                                solution = lineDetail[3]; if (String.IsNullOrEmpty(solution)) { solution = "---"; }
                                if (!solution.Equals("---")) { rdSol = Convert.ToInt32(solution); } else { rdSol = -99; }
                                Gx = lineDetail[4]; if (String.IsNullOrEmpty(Gx)) { Gx = "---"; }
                                if (!Gx.Equals("---") || !Decimal.TryParse(Gx, NumberStyles.Any, CultureInfo.InvariantCulture, out rdGx)) { rdGx = Convert.ToDecimal(Gx); } else { rdGx = -99.00M; }
                                Gy = lineDetail[5]; if (String.IsNullOrEmpty(Gy)) { Gy = "---"; }
                                if (!Gy.Equals("---") || !Decimal.TryParse(Gy, NumberStyles.Any, CultureInfo.InvariantCulture, out rdGy)) { rdGy = Convert.ToDecimal(Gy); } else { rdGy = -99.00M; }
                                Gz = lineDetail[6]; if (String.IsNullOrEmpty(Gz)) { Gz = "---"; }
                                if (!Gz.Equals("---") || !Decimal.TryParse(Gz, NumberStyles.Any, CultureInfo.InvariantCulture, out rdGz)) { rdGz = Convert.ToDecimal(Gz); } else { rdGz = -99.00M; }
                                Bx = lineDetail[7]; if (String.IsNullOrEmpty(Bx)) { Bx = "---"; }
                                if (!Bx.Equals("---")) { rdBx = Convert.ToDecimal(Bx); } else { rdBx = -99.00M; }
                                By = lineDetail[8]; if (String.IsNullOrEmpty(By)) { By = "---"; }
                                if (!By.Equals("---")) { rdBy = Convert.ToDecimal(By); } else { rdBy = -99.00M; }
                                Bz = lineDetail[9]; if (String.IsNullOrEmpty(Bz)) { Bz = "---"; }
                                if (!Bz.Equals("---")) { rdBz = Convert.ToDecimal(Bz); } else { rdBz = -99.00M; }                                
                                Inc = lineDetail[10]; if (String.IsNullOrEmpty(Inc)) { Inc = "---"; }
                                if (!Inc.Equals("---")) { rdInc = Convert.ToDecimal(Inc); } else { rdInc = -99.00M; }
                                Azm = lineDetail[11]; if (String.IsNullOrEmpty(Azm)) { Azm = "---"; }
                                if (!Azm.Equals("---")) { rdAzm = Convert.ToDecimal(Azm); } else { rdAzm = -99.00M; }
                                GTI = lineDetail[12]; if (String.IsNullOrEmpty(GTI)) { GTI = "---"; }
                                if (!GTI.Equals("---")) { rdGTI = Convert.ToDecimal(GTI); } else { rdGTI = -99.00M; }
                                BTI = lineDetail[13]; if (String.IsNullOrEmpty(BTI)) { BTI = "---"; }
                                if (!BTI.Equals("---")) { rdBTI = Convert.ToDecimal(BTI); } else { rdBTI = -99.00M; }
                                Dip = lineDetail[14]; if (String.IsNullOrEmpty(Dip)) { Dip = "---"; }
                                if (!Dip.Equals("---")) { rdDip = Convert.ToDecimal(Dip); } else { rdDip = -99.00M; }
                                BTotal = lineDetail[15]; if (String.IsNullOrEmpty(BTotal)) { BTotal = "---"; }
                                if (!BTotal.Equals("---")) { rdBT = Convert.ToInt32(BTotal); } else { rdBT = -99; }
                                GAxes = lineDetail[16]; if (String.IsNullOrEmpty(GAxes)) { GAxes = "---"; }
                                if (!GAxes.Equals("---")) { rdGA = Convert.ToInt32(GAxes); } else { rdGA = -99; }
                                cnType = lineDetail[17]; if (String.IsNullOrEmpty(cnType)) { cnType = "---"; }
                                if (!cnType.Equals("---")) { rdTyp = Convert.ToInt32(cnType); } else { rdTyp = -99; }
                                dataTable.Rows.Add(drowID, enabled, timeStamp, depth, solution, Gx, Gy, Gz, Bx, By, Bz, Inc, Azm, GTI, BTI, Dip, BTotal, GAxes, cnType);
                                dataTable.AcceptChanges();
                                fileData.Rows.Add(drowID, rdEnab, rdStamp, rdDepth, rdSol, rdGx, rdGy, rdGz, rdBx, rdBy, rdBz, rdInc, rdAzm, rdGTI, rdBTI, rdDip, rdBT, rdGA, rdTyp);
                                fileData.AcceptChanges();
                            }
                        }
                    }                    
                } while (fileReader.Peek() != -1);
                fileReader.Close();                
                iReply.Add(dataTable);
                iReply.Add(fileData);
                drowID = 0; lineCount = 0; dfileID = 0;
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<System.Data.DataTable> uploadStandardDataTextFile(Int32 WellNumberID, Int32 WellSectionID, Int32 RunNumberID, String DataFileName, String DataFileExtension, String DataFilePath, Int32 ServiceID, Int32 DataInputMethod, String UserName, String UserRealm, String ClientDBConnection)
        {
            try
            {
                List<System.Data.DataTable> iReply = new List<System.Data.DataTable>();
                String[] lineDetail, columnOrder;
                Int32[] flip = new Int32[6];
                Decimal[] gb_decimals = new Decimal[6];
                String dataLine;
                Int32 lineCount = 0, drowID = 0, lengthU = -99, acelU = -99, magneticU = -99;
                ArrayList DetailList = new ArrayList();
                Dictionary<String, String> lineValue = new Dictionary<String, String>();
                Dictionary<String, Int32> HeaderGB = new Dictionary<String, Int32>();                
                String timeStamp, depth, Gx, Gy, Gz, Bx, By, Bz;
                DateTime rdStamp;
                Decimal rdDepth, rdGx, rdGy, rdGz, rdBx, rdBy, rdBz;
                System.Data.DataTable dataTable = new System.Data.DataTable();
                DataColumn ddrID = dataTable.Columns.Add("drID", typeof(Int32));
                DataColumn dStamp = dataTable.Columns.Add("Timestamp", typeof(String));
                DataColumn dMD = dataTable.Columns.Add("Depth", typeof(String));
                DataColumn dGx = dataTable.Columns.Add("Gx", typeof(String));
                DataColumn dGy = dataTable.Columns.Add("Gy", typeof(String));
                DataColumn dGz = dataTable.Columns.Add("Gz", typeof(String));
                DataColumn dBx = dataTable.Columns.Add("Bx", typeof(String));
                DataColumn dBy = dataTable.Columns.Add("By", typeof(String));
                DataColumn dBz = dataTable.Columns.Add("Bz", typeof(String));
                System.Data.DataTable fileData = new System.Data.DataTable();
                DataColumn drID = fileData.Columns.Add("drID", typeof(Int32));
                DataColumn rStamp = fileData.Columns.Add("Timestamp", typeof(DateTime));
                DataColumn rMD = fileData.Columns.Add("Depth", typeof(Decimal));
                DataColumn lenU = fileData.Columns.Add("lenU", typeof(Int32));
                DataColumn rGx = fileData.Columns.Add("Gx", typeof(Decimal));
                DataColumn rGy = fileData.Columns.Add("Gy", typeof(Decimal));
                DataColumn rGz = fileData.Columns.Add("Gz", typeof(Decimal));
                DataColumn rBx = fileData.Columns.Add("Bx", typeof(Decimal));
                DataColumn rBy = fileData.Columns.Add("By", typeof(Decimal));
                DataColumn rBz = fileData.Columns.Add("Bz", typeof(Decimal));
                DataColumn acU = fileData.Columns.Add("acU", typeof(Int32));
                DataColumn magU = fileData.Columns.Add("magU", typeof(Int32));
                StreamReader fileReader = new StreamReader(DataFilePath);
                do
                {
                    dataLine = Convert.ToString(fileReader.ReadLine());
                    lineCount++;
                    
                    if (dataLine.Contains(",") && dataLine.Contains(",") && dataLine.Contains(","))
                    {
                        if (dataLine.ToLower().Contains("depth") || dataLine.ToLower().Contains("gx") || dataLine.ToLower().Contains("bx"))
                        {
                            columnOrder = dataLine.Split(',');
                            StandardHeaderTOPLINE(HeaderGB, columnOrder);
                        }
                        else
                        {
                            lineDetail = dataLine.Split(',');

                            if (String.IsNullOrEmpty(lineDetail[2]) || String.IsNullOrEmpty(lineDetail[3]) || String.IsNullOrEmpty(lineDetail[4]) || String.IsNullOrEmpty(lineDetail[5]) || String.IsNullOrEmpty(lineDetail[6]) || String.IsNullOrEmpty(lineDetail[7]))
                            {
                            }
                            else
                            {
                                drowID++;
                                StandardDetailsProcessor(HeaderGB, lineDetail, DetailList, flip, gb_decimals);
                                timeStamp = lineDetail[0]; if (String.IsNullOrEmpty(timeStamp)) { timeStamp = "---"; }
                                if (!timeStamp.Equals("---")) { rdStamp = Convert.ToDateTime(timeStamp); } else { rdStamp = Convert.ToDateTime(TimeSpan.Zero); }
                                depth = lineDetail[1]; if (String.IsNullOrEmpty(depth)) { depth = "---"; }
                                if (!depth.Equals("---")) { rdDepth = Convert.ToDecimal(depth); } else { rdDepth = -99.00M; }
                                Gx = lineDetail[2]; if (String.IsNullOrEmpty(Gx)) { Gx = "---"; }
                                if (!Gx.Equals("---") || !Decimal.TryParse(Gx, NumberStyles.Any, CultureInfo.InvariantCulture, out rdGx)) { rdGx = Convert.ToDecimal(Gx); } else { rdGx = -99.00M; }
                                Gy = lineDetail[3]; if (String.IsNullOrEmpty(Gy)) { Gy = "---"; }
                                if (!Gy.Equals("---") || !Decimal.TryParse(Gy, NumberStyles.Any, CultureInfo.InvariantCulture, out rdGy)) { rdGy = Convert.ToDecimal(Gy); } else { rdGy = -99.00M; }
                                Gz = lineDetail[4]; if (String.IsNullOrEmpty(Gz)) { Gz = "---"; }
                                if (!Gz.Equals("---") || !Decimal.TryParse(Gz, NumberStyles.Any, CultureInfo.InvariantCulture, out rdGz)) { rdGz = Convert.ToDecimal(Gz); } else { rdGz = -99.00M; }
                                Bx = lineDetail[5]; if (String.IsNullOrEmpty(Bx)) { Bx = "---"; }
                                if (!Bx.Equals("---")) { rdBx = Convert.ToDecimal(Bx); } else { rdBx = -99.00M; }
                                By = lineDetail[6]; if (String.IsNullOrEmpty(By)) { By = "---"; }
                                if (!By.Equals("---")) { rdBy = Convert.ToDecimal(By); } else { rdBy = -99.00M; }
                                Bz = lineDetail[7]; if (String.IsNullOrEmpty(Bz)) { Bz = "---"; }
                                if (!Bz.Equals("---")) { rdBz = Convert.ToDecimal(Bz); } else { rdBz = -99.00M; }
                                dataTable.Rows.Add(drowID, timeStamp, depth, Gx, Gy, Gz, Bx, By, Bz);
                                dataTable.AcceptChanges();
                                fileData.Rows.Add(drowID, rdStamp, rdDepth, lengthU, rdGx, rdGy, rdGz, rdBx, rdBy, rdBz, acelU, magneticU);
                                fileData.AcceptChanges();
                            }
                        }
                    }
                } while (fileReader.Peek() != -1);
                fileReader.Close();
                iReply.Add(dataTable);
                iReply.Add(fileData);
                drowID = 0; lineCount = 0;
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertQCData(Int32 dEnabled, DateTime SurveyTimeStamp, Decimal Depth, Int32 Solution, Decimal Gx, Decimal Gy, Decimal Gz, Decimal Bx, Decimal By, Decimal Bz, Decimal Inclination, Decimal Azimuth, Decimal GTI, Decimal BTI, Decimal Dip, Int32 BTotal, Int32 GAxes, Int32 Type, Decimal ResInclination, Decimal ResAzimuth, Decimal ResDip, Int32 ResBTotal, Decimal ResGTotal, Decimal ResGTF, Int32 RunID, Int32 SectionID, Int32 WellID, Int32 DataEntryMethodID, Int32 DataTypeID, Int32 ServiceID, Int32 DataFileID, String LoginName, String Realm, String ClientDBConnection)
        {
            try
            {
                Int32 iReply = -99;
                String insData = "INSERT INTO [AuditQCUploadedData] ([dEnabled], [dDateTime], [dDepth], [dSolution], [dGx], [dGy], [dGz], [dBx], [dBy], [dBz], [dInc], [dAzm], [dGTI], [dBTI], [dDip], [dBTotal], [dGAxes], [dType], [dResInc], [dResAzm], [dResDip], [dResBTotal], [dResGTotal], [dResGTF], [runID], [wswID], [welID], [demID], [dtID], [srvID], [cfID], [LoginName], [domain], [cTime]) VALUES (@dEnabled, @dDateTime, @dDepth, @dSolution, @dGx, @dGy, @dGz, @dBx, @dBy, @dBz, @dInc, @dAzm, @dGTI, @dBTI, @dDip, @dBTotal, @dGAxes, @dType, @dResInc, @dResAzm, @dResDip, @dResBTotal, @dResGTotal, @dResGTF, @runID, @wswID, @welID, @demID, @dtID, @srvID, @cfID, @LoginName, @domain, @cTime)";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insertCom = new SqlCommand(insData, dataCon);
                        insertCom.Parameters.AddWithValue("@dEnabled", SqlDbType.Int).Value = dEnabled.ToString();
                        insertCom.Parameters.AddWithValue("@dDateTime", SqlDbType.DateTime2).Value = SurveyTimeStamp.ToString();
                        insertCom.Parameters.AddWithValue("@dDepth", SqlDbType.Decimal).Value = Depth.ToString();
                        insertCom.Parameters.AddWithValue("@dSolution", SqlDbType.Int).Value = Solution.ToString();
                        insertCom.Parameters.AddWithValue("@dGx", SqlDbType.Decimal).Value = Gx.ToString();
                        insertCom.Parameters.AddWithValue("@dGy", SqlDbType.Decimal).Value = Gy.ToString();
                        insertCom.Parameters.AddWithValue("@dGz", SqlDbType.Decimal).Value = Gz.ToString();
                        insertCom.Parameters.AddWithValue("@dBx", SqlDbType.Decimal).Value = Bx.ToString();
                        insertCom.Parameters.AddWithValue("@dBy", SqlDbType.Decimal).Value = By.ToString();
                        insertCom.Parameters.AddWithValue("@dBz", SqlDbType.Decimal).Value = Bz.ToString();
                        insertCom.Parameters.AddWithValue("@dInc", SqlDbType.Decimal).Value = Inclination.ToString();
                        insertCom.Parameters.AddWithValue("@dAzm", SqlDbType.Decimal).Value = Azimuth.ToString();
                        insertCom.Parameters.AddWithValue("@dGTI", SqlDbType.Decimal).Value = GTI.ToString();
                        insertCom.Parameters.AddWithValue("@dBTI", SqlDbType.Decimal).Value = BTI.ToString();
                        insertCom.Parameters.AddWithValue("@dDip", SqlDbType.Decimal).Value = Dip.ToString();
                        insertCom.Parameters.AddWithValue("@dBTotal", SqlDbType.Int).Value = BTotal.ToString();
                        insertCom.Parameters.AddWithValue("@dGAxes", SqlDbType.Int).Value = GAxes.ToString();
                        insertCom.Parameters.AddWithValue("@dType", SqlDbType.Int).Value = Type.ToString();
                        insertCom.Parameters.AddWithValue("@dResInc", SqlDbType.Decimal).Value = ResInclination.ToString();
                        insertCom.Parameters.AddWithValue("@dResAzm", SqlDbType.Decimal).Value = ResAzimuth.ToString();
                        insertCom.Parameters.AddWithValue("@dResDip", SqlDbType.Decimal).Value = ResDip.ToString();
                        insertCom.Parameters.AddWithValue("@dResBTotal", SqlDbType.Int).Value = ResBTotal.ToString();
                        insertCom.Parameters.AddWithValue("@dResGTotal", SqlDbType.Decimal).Value = ResGTotal.ToString();
                        insertCom.Parameters.AddWithValue("@dResGTF", SqlDbType.Decimal).Value = ResGTF.ToString();
                        insertCom.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        insertCom.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = SectionID.ToString();
                        insertCom.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        insertCom.Parameters.AddWithValue("@demID", SqlDbType.Int).Value = DataEntryMethodID.ToString();
                        insertCom.Parameters.AddWithValue("@dtID", SqlDbType.Int).Value = DataTypeID.ToString();
                        insertCom.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        insertCom.Parameters.AddWithValue("@cfID", SqlDbType.Int).Value = DataFileID.ToString();
                        insertCom.Parameters.AddWithValue("@LoginName", SqlDbType.NVarChar).Value = LoginName.ToString();
                        insertCom.Parameters.AddWithValue("@domain", SqlDbType.NVarChar).Value = Realm.ToString();
                        insertCom.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();

                        iReply = Convert.ToInt32(insertCom.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        
    }
}