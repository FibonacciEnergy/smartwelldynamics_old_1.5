﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace SmartsVer1.Helpers.DemogHelpers
{
    public class TrendAnalysis
    {

        void main()
        { }

        public static Dictionary<Int32, Int32> getBoxyParametersList()
        {
            try
            {
                Dictionary<Int32, Int32> iReply = new Dictionary<Int32, Int32>();
                String selData = "SELECT [baeID], [baeValue] FROM [dmgBoxyAvgError] ORDER BY [baeValue]";
                Int32 id = -99, value = -99;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetInt32(1);
                                        iReply.Add(id, value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getBoxyParametersTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn baeID = iReply.Columns.Add("baeID", typeof(Int32));
                DataColumn baeValue = iReply.Columns.Add("baeValue", typeof(Int32));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [baeID], [baeValue], [uTime] FROM [dmgBoxyAvgError] ORDER BY [baeValue]";
                Int32 id = -99, value = -99;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetInt32(1);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, value, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getBTotalParametersList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [tabtID], [tabtValue] FROM [dmgTABTotal] ORDER BY [tabtValue]";
                Int32 id = -99;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Add(id, value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getBTotalParametersTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn tabtID = iReply.Columns.Add("tabtID", typeof(Int32));
                DataColumn tabtValue = iReply.Columns.Add("tabtValue", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [tabtID], [tabtValue], [uTime] FROM [dmgTABTotal] ORDER BY [tabtValue]";
                Int32 id = -99;
                String value = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, value, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getDBzParametersList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [tabzID], [tabzValue] FROM [dmgTADeltaBz] ORDER BY [tabzValue]";
                Int32 id = -99;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Add(id, value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getDBzParametersTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn tabzID = iReply.Columns.Add("tabzID", typeof(Int32));
                DataColumn tabzValue = iReply.Columns.Add("tabzValue", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [tabzID], [tabzValue], [uTime] FROM [dmgTADeltaBz] ORDER BY [tabzValue]";
                Int32 id = -99;
                String value = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, value, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getDFLimitTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn dflID = iReply.Columns.Add("dflID", typeof(Int32));
                DataColumn dflLowerBT = iReply.Columns.Add("dflLowerBT", typeof(Decimal));
                DataColumn dflLowerDip = iReply.Columns.Add("dflLowerDip", typeof(Decimal));
                DataColumn dflLowerGT = iReply.Columns.Add("dflLowerGT", typeof(Decimal));
                DataColumn dflUpperBT = iReply.Columns.Add("dflUpperBT", typeof(Decimal));
                DataColumn dflUpperDip = iReply.Columns.Add("dflUpperDip", typeof(Decimal));
                DataColumn dflUpperGT = iReply.Columns.Add("dflUpperGT", typeof(Decimal));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                Int32 id = -99;
                Decimal lBT = -99.00M, lDip = -99.00M, lGT = -99.0000M, uBT = -99.00M, uDip = -99.00M, uGT = -99.0000M;
                DateTime uT = DateTime.Now;
                String selData = "SELECT [dflID], [dflLowerBT], [dflLowerDip], [dflLowerGT], [dflUpperBT], [dflUpperDip], [dflUpperGT], [uTime] FROM [dmgDeltaFluctuationLimit]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        lBT = readData.GetDecimal(1);
                                        lDip = readData.GetDecimal(2);
                                        lGT = readData.GetDecimal(3);
                                        uBT = readData.GetDecimal(4);
                                        uDip = readData.GetDecimal(5);
                                        uGT = readData.GetDecimal(6);
                                        uT = readData.GetDateTime(7);
                                        iReply.Rows.Add(id, lBT, lDip, lGT, uBT, uDip, uGT, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getDipParametersList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [tadipID], [tadipValue] FROM [dmgTADip] ORDER BY [tadipValue]";
                Int32 id = -99;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Add(id, value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getDipParametersTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn tadipID = iReply.Columns.Add("tadipID", typeof(Int32));
                DataColumn tadipValue = iReply.Columns.Add("tadipValue", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [tadipID], [tadipValue], [uTime] FROM [dmgTADip] ORDER BY [tadipValue]";
                Int32 id = -99;
                String value = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, value, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getGTotalParametersList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [tagtID], [tagtValue] FROM [dmgTAGTotal] ORDER BY [tagtValue]";
                Int32 id = -99;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Add(id, value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getGTotalParametersTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn tagtID = iReply.Columns.Add("tagtID", typeof(Int32));
                DataColumn tagtValue = iReply.Columns.Add("tagtValue", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [tagtID], [tagtValue], [uTime] FROM [dmgTAGTotal] ORDER BY [tagtValue]";
                Int32 id = -99;
                String value = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, value, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getIncParametersList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [taiID], [taiValue] FROM [dmgTAInc] ORDER BY [taiValue]";
                Int32 id = -99;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Add(id, value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getIncParametersTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn taiID = iReply.Columns.Add("taiID", typeof(Int32));
                DataColumn taiValue = iReply.Columns.Add("taiValue", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [taiID], [taiValue], [uTime] FROM [dmgTAInc] ORDER BY [taiValue]";
                Int32 id = -99;
                String value = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, value, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getNMSParametersList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [tanmspID], [tanmspValue] FROM [dmgTANonMagSpace] ORDER BY [tanmspValue]";
                Int32 id = -99;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Add(id, value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getNMSParametersTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn tanmspID = iReply.Columns.Add("tanmspID", typeof(Int32));
                DataColumn tanmspValue = iReply.Columns.Add("tanmspValue", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [tanmspID], [tanmspValue], [uTime] FROM [dmgTANonMagSpace] ORDER BY [tanmspValue]";
                Int32 id = -99;
                String value = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, value, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getRefMagParametersList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [tarmID], [tarmValue] FROM [dmgTARefMags] ORDER BY [tarmValue]";
                Int32 id = -99;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Add(id, value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getRefMagParametersTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn tarmID = iReply.Columns.Add("tarmID", typeof(Int32));
                DataColumn tarmValue = iReply.Columns.Add("tarmValue", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [tarmID], [tarmValue], [uTime] FROM [dmgTARefMags] ORDER BY [tarmValue]";
                Int32 id = -99;
                String value = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, value, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getSCAZParametersList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [tascazID], [tascazValue] FROM [dmgTASCAZ] ORDER BY [tascazValue]";
                Int32 id = -99;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        iReply.Add(id, value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getSCAZParametersTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn tascazID = iReply.Columns.Add("tascazID", typeof(Int32));
                DataColumn tascazValue = iReply.Columns.Add("tascazValue", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [tascazID], [tascazValue], [uTime] FROM [dmgTASCAZ] ORDER BY [tascazValue]";
                Int32 id = -99;
                String value = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, value, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getTAStatement(Int32 BTotal, Int32 GTotal, Int32 Dip, Int32 Inclination, Int32 DeltaBz, Int32 SCAZ, Int32 NonMagSp, Int32 RefMag)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [taAnalysis] FROM [dmgQCTrendAnalysis] WHERE [tarmID] = @tarmID AND [taiID] = @taiID AND [tagtID] = @tagtID AND [tabtID] = @tabtID AND [tadipID] = @tadipID AND [tascazID] = @tascazID AND [tanmspID] = @tanmspID AND [tabzID] = @tabzID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@tarmID", SqlDbType.Int).Value = RefMag.ToString();
                        getData.Parameters.AddWithValue("@taiID", SqlDbType.Int).Value = Inclination.ToString();
                        getData.Parameters.AddWithValue("@tagtID", SqlDbType.Int).Value = GTotal.ToString();
                        getData.Parameters.AddWithValue("@tabtID", SqlDbType.Int).Value = BTotal.ToString();
                        getData.Parameters.AddWithValue("@tadipID", SqlDbType.Int).Value = Dip.ToString();
                        getData.Parameters.AddWithValue("@tascazID", SqlDbType.Int).Value = SCAZ.ToString();
                        getData.Parameters.AddWithValue("@tanmspID", SqlDbType.Int).Value = NonMagSp.ToString();
                        getData.Parameters.AddWithValue("@tabzID", SqlDbType.Int).Value = DeltaBz.ToString();
                        iReply = Convert.ToString(getData.ExecuteScalar());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertBTParameter(String BTParameter)
        {
            try
            {
                Int32 iReply = -99;
                String insData = "INSERT INTO [dmgTABTotal] ([tabtValue], [cTime], [uTime]) VALUES (@tabtValue, @cTime, @uTime)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insertData = new SqlCommand(insData, dataCon);
                        insertData.Parameters.AddWithValue("@tabtValue", SqlDbType.NVarChar).Value = BTParameter.ToString();
                        insertData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insertData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        iReply = Convert.ToInt32(insertData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }            
        }

        public static Int32 insertDBzParameter(String DBzParameter)
        {
            try
            {
                Int32 iReply = -99;
                String insData = "INSERT INTO [dmgTADeltaBz] ([tadipValue], [cTime], [uTime]) VALUES (@tabzValue, @cTime, @uTime)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insertData = new SqlCommand(insData, dataCon);
                        insertData.Parameters.AddWithValue("@tabzValue", SqlDbType.NVarChar).Value = DBzParameter.ToString();
                        insertData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insertData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        iReply = Convert.ToInt32(insertData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertDipParameter(String DipParameter)
        {
            try
            {
                Int32 iReply = -99;
                String insData = "INSERT INTO [dmgTADip] ([tadipValue], [cTime], [uTime]) VALUES (@tagtValue, @cTime, @uTime)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insertData = new SqlCommand(insData, dataCon);
                        insertData.Parameters.AddWithValue("@tadipValue", SqlDbType.NVarChar).Value = DipParameter.ToString();
                        insertData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insertData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        iReply = Convert.ToInt32(insertData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertGTParameter(String GTParameter)
        {
            try
            {
                Int32 iReply = -99;
                String insData = "INSERT INTO [dmgTAGTotal] ([tagtValue], [cTime], [uTime]) VALUES (@tagtValue, @cTime, @uTime)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insertData = new SqlCommand(insData, dataCon);
                        insertData.Parameters.AddWithValue("@tagtValue", SqlDbType.NVarChar).Value = GTParameter.ToString();
                        insertData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insertData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        iReply = Convert.ToInt32(insertData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertIncParameter(String IncParameter)
        {
            try
            {
                Int32 iReply = -99;
                String insData = "INSERT INTO [dmgTAInc] ([taiValue], [cTime], [uTime]) VALUES (@taiValue, @cTime, @uTime)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insertData = new SqlCommand(insData, dataCon);
                        insertData.Parameters.AddWithValue("@taiValue", SqlDbType.NVarChar).Value = IncParameter.ToString();
                        insertData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insertData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        iReply = Convert.ToInt32(insertData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertNMSParameter(String NMSParameter)
        {
            try
            {
                Int32 iReply = -99;
                String insData = "INSERT INTO [dmgTANonMagSpace] ([tanmspValue], [cTime], [uTime]) VALUES (@tanmspValue, @cTime, @uTime)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insertData = new SqlCommand(insData, dataCon);
                        insertData.Parameters.AddWithValue("@tanmspValue", SqlDbType.NVarChar).Value = NMSParameter.ToString();
                        insertData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insertData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        iReply = Convert.ToInt32(insertData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertRefMagParameter(String RefMagParameter)
        {
            try
            {
                Int32 iReply = -99;
                String insData = "INSERT INTO [dmgTARefMags] ([tarmValue], [cTime], [uTime]) VALUES (@tarmValue, @cTime, @uTime)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insertData = new SqlCommand(insData, dataCon);
                        insertData.Parameters.AddWithValue("@tarmValue", SqlDbType.NVarChar).Value = RefMagParameter.ToString();
                        insertData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insertData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        iReply = Convert.ToInt32(insertData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertSCAZParameter(String SCAZParameter)
        {
            try
            {
                Int32 iReply = -99;
                String insData = "INSERT INTO [dmgTASCAZ] ([tascazValue], [cTime], [uTime]) VALUES (@tascazValue, @cTime, @uTime)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insertData = new SqlCommand(insData, dataCon);
                        insertData.Parameters.AddWithValue("@tascazValue", SqlDbType.NVarChar).Value = SCAZParameter.ToString();
                        insertData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insertData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        iReply = Convert.ToInt32(insertData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}