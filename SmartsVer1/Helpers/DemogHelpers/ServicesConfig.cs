﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using NJ = Newtonsoft.Json;

/// <summary>
/// Methods to manipulate SMARTs Services
/// </summary>

namespace SmartsVer1.Helpers.DemogHelpers
{
    public class ServicesConfig
    {
        static void Main()
        { }

        public static Dictionary<Int32, String> getSrvGroupList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [srvGrpID], [srvGroupLabel] FROM [dmgServiceGroup] ORDER BY [srvGroupLabel]";
                Int32 id = -99;
                String name = null;

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getSrvGroupTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn srvGrpID = iReply.Columns.Add("srvGrpID", typeof(Int32));
                DataColumn srvGroupLabel = iReply.Columns.Add("srvGroupLabel", typeof(String));
                DataColumn srvGrpCount = iReply.Columns.Add("srvGrpCount", typeof(Int32));
                String selData = "SELECT [srvGrpID], [srvGroupLabel] FROM [dmgServiceGroup] ORDER BY [srvGroupLabel]";
                Int32 id = -99, count = -99;
                String name = null;

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        count = getSrvGroupServicesCount(id);
                                        iReply.Rows.Add(id, name, count);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getSrvGroupName(Int32 ServiceGroupID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [srvGroupLabel] FROM [dmgServiceGroup] WHERE [srvGrpID] = @srvGrpID";
                
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@srvGrpID", SqlDbType.Int).Value = ServiceGroupID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getSrvGroupServicesCount(Int32 ServiceGroupID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(srvID) FROM [dmgService] WHERE [srvGrpID] = @srvGrpID";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@srvGrpID", SqlDbType.Int).Value = ServiceGroupID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getGlobalServicesTable(Int32 ServiceGroupID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn srvID = iReply.Columns.Add("srvID", typeof(Int32));
                DataColumn srvName = iReply.Columns.Add("srvName", typeof(String));
                DataColumn srvStatID = iReply.Columns.Add("srvStatID", typeof(String));
                DataColumn srvVersion = iReply.Columns.Add("srvVersion", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [srvID], [srvName], [srvStatID], [srvVersion], [uTime] FROM [dmgService] WHERE [srvGrpID] = @srvGrpID";
                Int32 id = 0, sttID = 0;
                String name = String.Empty, version = String.Empty, status = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@srvGrpID", SqlDbType.Int).Value = ServiceGroupID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        sttID = readData.GetInt32(2);
                                        status = getServiceStatus(sttID);
                                        version = readData.GetString(3);
                                        uT = readData.GetDateTime(4);
                                        iReply.Rows.Add(id, name, status, version, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getServicesTable(Int32 ServiceGroupID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn srvID = iReply.Columns.Add("srvID", typeof(Int32));
                DataColumn srvName = iReply.Columns.Add("srvName", typeof(String));
                DataColumn srvStatID = iReply.Columns.Add("srvStatID", typeof(String));
                DataColumn srvVersion = iReply.Columns.Add("srvVersion", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [srvID], [srvName], [srvStatID], [srvVersion], [uTime] FROM [dmgService] WHERE [srvGrpID] = @srvGrpID";
                Int32 id = 0, sttID = 0;
                String name = String.Empty, version = String.Empty, status = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@srvGrpID", SqlDbType.Int).Value = ServiceGroupID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        sttID = readData.GetInt32(2);
                                        status = getServiceStatus(sttID);
                                        version = readData.GetString(3);
                                        uT = readData.GetDateTime(4);
                                        iReply.Rows.Add(id, name, status, version, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getServiceList(Int32 ServiceGroupID)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [srvID], [srvName] FROM [dmgService] WHERE [srvGrpID] = @srvGrpID";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@srvGrpID", SqlDbType.Int).Value = ServiceGroupID.ToString();

                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getServiceStatusTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn srvstatID = iReply.Columns.Add("srvstatID", typeof(Int32));
                DataColumn srvStatus = iReply.Columns.Add("srvStatus", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                Int32 id = 0;
                String name = String.Empty;
                DateTime uT = DateTime.Now;
                String selData = "SELECT [srvStatID], [srvStatus], [uTime] FROM [dmgServiceStatus] ORDER BY [srvStatus]";
                
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        uT = readData.GetDateTime(2);
                                        iReply.Rows.Add(id, name, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, Int32> getServiceStatusList()
        {
            try
            {
                Dictionary<Int32, Int32> iReply = new Dictionary<Int32, Int32>();
                String selData = "SELECT [srvID], [srvStatID] FROM [dmgService]";
                Int32 id = 0, stt = 0;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        stt = readData.GetInt32(1);
                                        iReply.Add(id, stt);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getServiceName(Int32 ServiceID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [srvName] FROM [dmgService] WHERE [srvID] = @srvID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                                else
                                {
                                    iReply = "---";
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getSrvStatList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [srvStatID], [srvStatus] FROM [dmgServiceStatus] ORDER BY [srvStatus]";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getServiceStatValue(Int32 ServiceID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT [srvStatID] FROM dmgService WHERE [srvID] = @srvID";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getServiceStatus(Int32 ServiceID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [srvStatus] FROM dmgServiceStatus WHERE [srvStatID] = @srvStatID";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@srvStatID", SqlDbType.Int).Value = ServiceID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                                else
                                {
                                    iReply = "---";
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //public static String getServicesData()
        //{
        //    try
        //    {
        //        Google.DataTable.Net.Wrapper.DataTable dt = new Google.DataTable.Net.Wrapper.DataTable();
        //        dt.AddColumn(new Google.DataTable.Net.Wrapper.Column(Google.DataTable.Net.Wrapper.ColumnType.String, "0", "Group"));
        //        dt.AddColumn(new Google.DataTable.Net.Wrapper.Column(Google.DataTable.Net.Wrapper.ColumnType.Number, "1", "Status"));
        //        Int32 gid = 0, sid = 0, sttID = 0, gst = 0;
        //        String grpName = String.Empty, srvName = String.Empty;
        //        Dictionary<Int32, String> srvGrps = new Dictionary<Int32, String>();
        //        Dictionary<Int32, String> srvList = new Dictionary<Int32, String>();
        //        var list = new List<KeyValuePair<String, Int32>>();
        //        srvGrps = getSrvGroupList();
        //        foreach (var item in srvGrps)
        //        {
        //            gid = item.Key;
        //            grpName = item.Value;
        //            srvList = getServiceList(gid);
        //            foreach (var srv in srvList)
        //            {
        //                sid = srv.Key;
        //                sttID = getServiceStatValue(sid);
        //                if (!sttID.Equals(2))
        //                {
        //                    gst = 0;
        //                    list.Add(new KeyValuePair<string, int>(grpName, gst));
        //                    break;
        //                }
        //                else
        //                {
        //                    gst = 1;
        //                    list.Add(new KeyValuePair<string, int>(grpName, gst));
        //                }
        //            }
        //        }

        //        foreach (var pair in list)
        //        {
        //            Google.DataTable.Net.Wrapper.Row gr = dt.NewRow();
        //            gr.AddCellRange(new Cell[]{                        
        //                new Google.DataTable.Net.Wrapper.Cell(pair.Key),
        //                new Google.DataTable.Net.Wrapper.Cell(pair.Value)
        //            });
        //            dt.AddRow(gr);
        //        }
        //        return dt.GetJson();
        //    }
        //    catch (Exception ex)
        //    { throw new System.Exception(ex.ToString()); }
        //}

        public static Int32 insertService(Int32 ServiceGroup, String ServiceName, Int32 ServiceStatus, String ServiceVersion)
        {
            try
            {
                Int32 iReply = -99;
                String insStat = "INSERT INTO dmgService ([srvName], [srvStatID], [srvGrpID], [srvVersion], [cTime], [uTime]) VALUES (@srvName, @srvStatID, @srvGrpID, @srvVersion, @cTime, @uTime)";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(insStat, dataCon);
                        insData.Parameters.AddWithValue("@srvName", SqlDbType.NVarChar).Value = ServiceName.ToString();
                        insData.Parameters.AddWithValue("@srvStatID", SqlDbType.Int).Value = ServiceStatus.ToString();
                        insData.Parameters.AddWithValue("@srvGrpID", SqlDbType.Int).Value = ServiceGroup.ToString();
                        insData.Parameters.AddWithValue("@srvVersion", SqlDbType.NVarChar).Value = ServiceVersion.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();

                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertStandardPrice(Int32 ServiceGroupID, Int32 ServiceID, Decimal Price, Int32 PriceUnit, DateTime StartDate, DateTime EndDate)
        {
            try
            {
                Int32 iReply = -99;
                String insData = "INSERT INTO [dmgStandardPrices] ([srvGrpID], [srvID], [stpPrice], [scuID], [stpStart], [stpEnd], [cTime], [uTime]) VALUES (@srvGrpID, @srvID, @stpPrice, @scuID, @stpStart, @stpEnd, @cTime, @uTime)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand addData = new SqlCommand(insData, dataCon);
                        addData.Parameters.AddWithValue("@srvGrpID", SqlDbType.Int).Value = ServiceGroupID.ToString();
                        addData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        addData.Parameters.AddWithValue("@stpPrice", SqlDbType.Decimal).Value = Price.ToString();
                        addData.Parameters.AddWithValue("@scuID", SqlDbType.Int).Value = PriceUnit.ToString();
                        addData.Parameters.AddWithValue("@stpStart", SqlDbType.DateTime2).Value = StartDate.ToString();
                        addData.Parameters.AddWithValue("@stpEnd", SqlDbType.DateTime2).Value = EndDate.ToString();
                        addData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        addData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();

                        iReply = Convert.ToInt32(addData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 UpdateServiceStatus(Int32 serviceID, Int32 serviceStatus)
        {
            try
            {
                Int32 iReply = -99;
                String updateService = "UPDATE dmgService SET srvStatID = @SrvStatID WHERE srvID = @SrvID";
                using (SqlConnection clntConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        clntConn.Open();

                        SqlCommand clntCmd = new SqlCommand(updateService, clntConn);
                        clntCmd.Parameters.AddWithValue("@SrvStatID", SqlDbType.Int).Value = serviceStatus.ToString();
                        clntCmd.Parameters.AddWithValue("@SrvID", SqlDbType.Int).Value = serviceID.ToString();

                        iReply = Convert.ToInt32(clntCmd.ExecuteNonQuery());
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        String msg = "Insert Error: ";
                        msg += ex.Message;
                    }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getServiceChargeTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn scuID = iReply.Columns.Add("scuID", typeof(Int32));
                DataColumn scuName = iReply.Columns.Add("scuName", typeof(String));
                String selData = "SELECT [scuID], [scuName] FROM [dmgSrvChargeUnit] ORDER BY [scuName]";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Rows.Add(id, name);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getServiceChargeUnit(Int32 ChargeUnitID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [scuName] FROM [dmgSrvChargeUnit] WHERE [scuID] = @scuID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@scuID", SqlDbType.Int).Value = ChargeUnitID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }            
        }

        public static Int32 getServiceChargeUnitID(String ChargeUnitName)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT [scuID] FROM [dmgSrvChargeUnit] WHERE [scuName] = @scuName";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@scuName", SqlDbType.NVarChar).Value = ChargeUnitName.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertServiceChargeUnit(String ChargeUnitValue)
        {
            try
            {
                Int32 iReply = -99;
                String addData = "INSERT INTO [dmgSrvChargeUnit] ([scuName]) VALUES (@scuName);";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(addData, dataCon);
                        getData.Parameters.AddWithValue("@scuName", SqlDbType.NVarChar).Value = ChargeUnitValue.ToString();
                        iReply = Convert.ToInt32(getData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getServiceChargeList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [scuID], [scuName] FROM [dmgSrvChargeUnit] ORDER BY [scuName]";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getStandardPricesTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn stpID = iReply.Columns.Add("stpID", typeof(Int32));
                DataColumn srvGrpID = iReply.Columns.Add("srvGrpID", typeof(String));
                DataColumn srvID = iReply.Columns.Add("srvID", typeof(String));
                DataColumn stpPrice = iReply.Columns.Add("stpPrice", typeof(Decimal));
                DataColumn scuID = iReply.Columns.Add("scuID", typeof(String));
                DataColumn stpStart = iReply.Columns.Add("stpStart", typeof(DateTime));
                DataColumn stpEnd = iReply.Columns.Add("stpEnd", typeof(DateTime));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                Int32 id = -99, grp = -99, srv = -99, chg = -99;
                String grpName = String.Empty, srvName = String.Empty, chgUnit = String.Empty;
                DateTime start = DateTime.Now, end = DateTime.Now, update = DateTime.Now;
                Decimal price = -99.00M;
                String selData = "SELECT [stpID], [srvGrpID], [srvID], [stpPrice], [scuID], [stpStart], [stpEnd], [uTime] FROM [dmgStandardPrices] ORDER BY [srvGrpID]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        grp = readData.GetInt32(1);
                                        srv = readData.GetInt32(2);
                                        price = readData.GetDecimal(3);
                                        chg = readData.GetInt32(4);
                                        start = readData.GetDateTime(5);
                                        end = readData.GetDateTime(6);
                                        update = readData.GetDateTime(7);
                                        grpName = getSrvGroupName(grp);
                                        srvName = getServiceName(srv);
                                        chgUnit = getServiceChargeUnit(chg);
                                        iReply.Rows.Add(id, grpName, srvName, price, chgUnit, start, end, update);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<String> getStandardPriceWithUnitID(Int32 ServiceGroupID, Int32 ServiceID)
        {
            try
            {
                List<String> iReply = new List<String>();
                Decimal price = -99.00M;
                Int32 chg = -99;
                String selData = "SELECT [stpPrice], [scuID] FROM [dmgStandardPrices] WHERE [srvGrpID] = @srvGrpID AND [srvID] = @srvID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@srvGrpID", SqlDbType.Int).Value = ServiceGroupID.ToString();
                        getData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        price = readData.GetDecimal(0);
                                        iReply.Add(Convert.ToString(price));
                                        chg = readData.GetInt32(1);
                                        iReply.Add(Convert.ToString(chg));
                                    }
                                }
                                else
                                {
                                    iReply.Add(Convert.ToString("-1"));                                    
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }                       
        }

        public static List<String> getStandardPriceWithUnitName(Int32 ServiceGroupID, Int32 ServiceID)
        {
            try
            {
                List<String> iReply = new List<String>();
                String chgName = String.Empty;
                Decimal price = -99.00M;
                Int32 chg = -99;
                String selData = "SELECT [stpPrice], [scuID] FROM [dmgStandardPrices] WHERE [srvGrpID] = @srvGrpID AND [srvID] = @srvID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@srvGrpID", SqlDbType.Int).Value = ServiceGroupID.ToString();
                        getData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        price = readData.GetDecimal(0);
                                        iReply.Add(Convert.ToString(price));
                                        chg = readData.GetInt32(1);
                                        chgName = getServiceChargeUnit(chg);
                                        iReply.Add(Convert.ToString(chgName));
                                    }
                                }
                                else
                                {
                                    iReply.Add(Convert.ToString("-1"));
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, Int32> getMDProcessingLimitList()
        {
            try
            {
                Dictionary<Int32, Int32> iReply = new Dictionary<Int32, Int32>();
                Int32 id = -99, val = -99;
                String selData = "SELECT [mdpID], [mdpValue] FROM [dmgMDProcessingLimit] ORDER BY [mdpValue]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        val = readData.GetInt32(1);
                                        iReply.Add(id, val);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getMDProcessingLimitTable()
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn stpID = iReply.Columns.Add("mdpID", typeof(Int32));
                DataColumn srvGrpID = iReply.Columns.Add("mdpValue", typeof(Int32));
                Int32 id = -99, val = -99;
                String selData = "SELECT [mdpID], [mdpValue] FROM [dmgMDProcessingLimit] ORDER BY [mdpValue]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        val = readData.GetInt32(1);
                                        iReply.Rows.Add(id, val);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertMDProcessingLimit(Int32 LimitValue)
        {
            try
            {
                Int32 iReply = -99, duplicate = -99;
                String insStat = "INSERT INTO [dmgMDProcessingLimit] ([mdpValue]) VALUES (@mdpVal)";
                duplicate = checkDuplicateMDProcessingLimit(LimitValue);
                if (duplicate > 0)
                {
                    iReply = -1;
                }
                else
                {
                    using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                    {
                        try
                        {
                            dataCon.Open();
                            SqlCommand insData = new SqlCommand(insStat, dataCon);
                            insData.Parameters.AddWithValue("@mdpVal", SqlDbType.NVarChar).Value = LimitValue.ToString();
                            iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            dataCon.Close();
                            dataCon.Dispose();
                        }
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 checkDuplicateMDProcessingLimit(Int32 LimitValue)
        {
            try
            {
                Int32 iReply = -99;
                String selStat = "SELECT COUNT(mdpID) FROM [dmgMDProcessingLimit] WHERE [mdpValue] = @mdpValue";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();

                        SqlCommand chkData = new SqlCommand(selStat, dataCon);
                        chkData.Parameters.AddWithValue("@mdpValue", SqlDbType.Int).Value = LimitValue.ToString();
                        using (SqlDataReader readData = chkData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }                        
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}