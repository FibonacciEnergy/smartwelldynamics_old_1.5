﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartsVer1.Helpers.DemogHelpers
{
    public class UnitsConverter
    {
        public const Decimal toolBGMisAlgn = 0.00M;
        public const Int32 to_Gauss = 100000;
        public const Int32 to_Tesla = 1000000000;
        public const Int32 to_MilliTesla = 1000000;
        public const Int32 to_MicroTesla = 1000;
        public const Int32 to_T = 1000;
        public const Int32 to_milT = 10;
        public const Decimal to_micT = 0.01M;
        public const Decimal to_nT = 0.00001M;
        public const Decimal to_cmsec2 = 980.665M;
        public const Decimal to_ftsec2 = 32.17404856M;
        public const Decimal to_msec2 = 9.80665M;
        public const Decimal to_mg = 1000.00M;
        public const Decimal to_millimeter = 0.0393699237404577M;
        public const Decimal to_inch = 0.0254M;
        public const Decimal to_foot = 0.3048M;
        public const Decimal Pi_Val = 3.1415926M;

        static void main()
        { }

        public static Double ToRadians(Double val)
        {
            try
            {
                return (Math.PI / 180) * val;
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Double ToDegrees(Double val)
        {
            try
            {
                return (180 / Math.PI) * val;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal convDip(Decimal dipVal)
        {
            try
            {
                return (dipVal + toolBGMisAlgn);
            }
            catch (Exception ex)
            {

                throw new System.Exception(ex.ToString());
            }
        }

        public static List<String> convDMSToDecimal(Int32 Cardinal, Int32 Degrees, Int32 Minutes, Decimal Seconds)
        {
            List<String> iReply = new List<String> { };
            Decimal dminutes = -99.000000M;
            Decimal dseconds = -99.000000M;
            dminutes = Convert.ToDecimal( Minutes / 60.00M );
            dseconds = Math.Round(( Seconds / 3600 ), 6);
            String plus = "+", minus = "-";
            String value = Convert.ToString(Degrees + dminutes + dseconds);
            if (Cardinal.Equals(1)) //North
            {
                value = String.Format("{0}{1}", plus, value);
            }
            else if (Cardinal.Equals(9)) //East
            {
                value = String.Format("{0}{1}", plus, value);
            }
            else if (Cardinal.Equals(17)) //South
            {
                value = String.Format("{0}{1}", minus, value);
            }
            else if (Cardinal.Equals(25)) //West
            {
                value = String.Format("{0}{1}", minus, value);
            }
            iReply.Add(Convert.ToString(Degrees));
            iReply.Add(Convert.ToString(dminutes));
            iReply.Add(Convert.ToString(dseconds));
            iReply.Add(value);
            return iReply;
        }

        public static List<String> convDecimalToDMS(String DecimalValue, String latORlng)
        {
            try
            {
                List<String> iReply = new List<String> { };
                String hemisphere = String.Empty;
                Int32 compassID = -99;
                Double absValue = Math.Abs(Convert.ToDouble(DecimalValue));
                Int32 degreeVal = Convert.ToInt32(Math.Truncate(absValue));
                Double delta = Math.Round((absValue - degreeVal), 8);
                Int32 minuteVal = Convert.ToInt32((delta) * 60);
                Decimal secondsVal = Math.Round(( Math.Abs(Convert.ToDecimal(delta) * 60) ), 6);
                iReply.Add(Convert.ToString(degreeVal));
                iReply.Add(Convert.ToString(minuteVal));
                iReply.Add(Convert.ToString(secondsVal));                

                if ( latORlng.Equals("lat") && degreeVal >= 0 && degreeVal <= 90)
                {
                    hemisphere = "N";
                    compassID = 1;
                }
                else if (latORlng.Equals("lat") && degreeVal < 0 && degreeVal >= -90)
                {
                    hemisphere = "S";
                    compassID = 17;
                }
                else if (latORlng.Equals("lng") && degreeVal > 90 && degreeVal <= 180)
                {
                    hemisphere = "E";
                    compassID = 9;
                }
                else
                {
                    hemisphere = "W";
                    compassID = 25;
                }
                iReply.Add(degreeVal + "° " + minuteVal + "' " + secondsVal + "''" + " " + hemisphere);
                iReply.Add(Convert.ToString(compassID));

                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal convBTotal(Decimal bTotal, Int32 MagU)
        {
            try
            {
                Double bT = Convert.ToDouble(bTotal);
                switch (MagU)
                {
                    case 1:
                        {
                            return Convert.ToDecimal(bT * to_Gauss);
                        }
                    case 2:
                        {
                            return Convert.ToDecimal(bT * to_Tesla);
                        }
                    case 3:
                        {
                            return Convert.ToDecimal(bT * to_MilliTesla);
                        }
                    case 4:
                        {
                            return Convert.ToDecimal(bT * to_MicroTesla);
                        }
                    default:
                        {
                            return Convert.ToDecimal(bT);
                        }
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal convDSCSize(Decimal dscSize, Int32 lenU)
        {
            try
            {
                if (lenU.Equals(2))
                {
                    return Decimal.Multiply(dscSize, to_millimeter);
                }
                else
                {
                    return dscSize;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal convFeetToMeters(Decimal feet)
        {
            try
            {
                Decimal iReply = -99.0000M;
                iReply = feet / 3.2808M;
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal convGMeter(Decimal gMtr, Int32 magU)
        {
            try
            {
                Decimal iReply = -99.00000000M;
                switch (magU)
                {
                    case 2:
                        {
                            iReply = Decimal.Multiply(gMtr, to_T);
                            break;
                        }
                    case 3:
                        {
                            iReply = Decimal.Multiply(gMtr, to_milT);
                            break;
                        }
                    case 4:
                        {
                            iReply = Decimal.Multiply(gMtr, to_micT);
                            break;
                        }
                    case 5:
                        {
                            iReply = Decimal.Multiply(gMtr, to_nT);
                            break;
                        }
                    default:
                        {
                            iReply = gMtr;
                            break;
                        }
                }
                return iReply;
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal convMagnetometerToNanoTesla(Decimal InputValue, Int32 InputMagnetometerUnit)
        {
            try
            {
                Decimal iReply = -99.00000000M;
                switch (InputMagnetometerUnit)
                {
                    case 1:
                        {
                            //Gauss
                            iReply = Decimal.Multiply(InputValue, 100000.00M);
                            break;
                        }
                    case 2:
                        {
                            //Tesla
                            iReply = Decimal.Multiply(InputValue, 1000000000.00M);
                            break;
                        }
                    case 3:
                        {
                            //MilliTesla
                            iReply = Decimal.Multiply(InputValue, 1000000.00M);
                            break;
                        }
                    case 4:
                        {
                            //MicroTesla
                            iReply = Decimal.Multiply(InputValue, 1000000.00M);
                            break;
                        }                    
                    default:
                        {
                            //NanoTesla
                            iReply = InputValue;
                            break;
                        }
                }
                return iReply;
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal convGMtrDistance(Decimal disTOM, Int32 lenU)
        {
            try
            {
                if (lenU.Equals(3))
                {
                    return Decimal.Add((Decimal.Multiply(disTOM, to_inch)), 0.01M);
                }
                else if (lenU.Equals(4))
                {
                    return Decimal.Add((Decimal.Multiply(disTOM, to_foot)), 0.01M);
                }
                else if (lenU.Equals(5))
                {
                    return Decimal.Add(disTOM, 0.01M);
                }
                else
                {
                    return 0.01M;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal convGTotal(Decimal gTotal, Int32 AclU)
        {
            try
            {
                switch (AclU)
                {
                    case 1:
                        {
                            return (Decimal.Divide(gTotal, to_cmsec2));
                        }
                    case 2:
                        {
                            return (Decimal.Divide(gTotal, to_ftsec2));
                        }
                    case 4:
                        {
                            return (Decimal.Divide(gTotal, to_msec2));
                        }
                    case 5:
                        {
                            return (Decimal.Divide(gTotal, to_mg));
                        }
                    default:
                        {
                            return (gTotal);
                        }
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal convMDLen(Decimal mdbLen, Int32 mdbLenU)
        {
            try
            {
                if (mdbLenU.Equals(4))
                {
                    return Decimal.Multiply(mdbLen, to_foot);
                }
                else if (mdbLenU.Equals(3))
                {
                    return Decimal.Multiply(mdbLen, to_inch);
                }
                else
                {
                    return mdbLen;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal convMotorSize(Decimal mSiz, Int32 lenU)
        {
            try
            {
                if (lenU == 2)
                {
                    return Decimal.Multiply(mSiz, to_millimeter);
                }
                else
                {
                    return mSiz;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal convNMSp(Decimal nmSp, Int32 lenU)
        {
            try
            {
                if (lenU == 3)
                {
                    return Decimal.Multiply(nmSp, to_inch);
                }
                else if (lenU == 4)
                {
                    return Decimal.Multiply(nmSp, to_foot);
                }
                else
                {
                    return nmSp;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Decimal convLengthToFeet(Decimal lengthValue, Int32 lengthUnit)
        {
            try
            {
                Decimal iReply = -99.000000000000M;
                switch (lengthUnit)
                {
                    case (1): //Centimeter
                        {
                            iReply = lengthValue / 30.48M;
                            break;
                        }
                    case (2): //Millimeter
                        {
                            iReply = lengthValue / 0.0032808M;
                            break;
                        }
                    case (3): //Inch
                        {
                            iReply = lengthValue / 12.00M;
                            break;
                        }
                    case (5): //Meter
                        {
                            iReply = lengthValue * 3.2808M;
                            break;
                        }
                    case (6): //Yard
                        {
                            iReply = lengthValue * 3.00M;
                            break;
                        }
                    case (7): //Mile
                        {
                            iReply = lengthValue * 5280.00M;
                            break;
                        }
                    default: //Kilometer
                        {
                            iReply = lengthValue * 3280.8M;
                            break;
                        }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Decimal convAccelerometerToAccelerationDueToGravity(Decimal InputValue, Int32 InputAceelerometerUnit)
        {
            try
            {
                Decimal iReply = -99.00000000M;
                switch (InputAceelerometerUnit)
                {
                    case 1:
                        {
                            //Acceleration due to Gravity - Gal (cm/sec.sq)
                            iReply = Decimal.Multiply(InputValue, 0.001019716213M);
                            break;
                        }
                    case 2:
                        {
                            //Acceleration due to Gravity (ft/sec.sq)
                            iReply = Decimal.Multiply(InputValue, 0.03108095017236M);
                            break;
                        }
                    case 5:
                        {
                            //Acceleration due to Gravity - ( mg )
                            iReply = Decimal.Multiply(InputValue, 0.03217404855561M);
                            break;
                        }                                        
                    case 4:
                        {
                            //Acceleration due to Gravity (meter/sec.sq)
                            iReply = Decimal.Multiply(InputValue, 0.1019716213M);
                            break;
                        }
                    default:
                        {
                            //NanoTesla
                            iReply = InputValue;
                            break;
                        }
                }
                return iReply;
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }
    }
}