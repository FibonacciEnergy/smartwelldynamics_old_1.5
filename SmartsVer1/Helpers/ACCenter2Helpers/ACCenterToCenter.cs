﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Linq;
using System.Collections;
using System.Globalization;
using System.Data.SqlClient;
using System.Web.SessionState;
using System.Web.Configuration;
using System.Collections.Generic;

namespace SmartsVer1.Helpers.ACCenter2Helpers
{
    public class ACCenterToCenter
    {
        void main()
        { }


        public static DataTable getSurroundingWellDataEastNorth(List<int> WellID, String dbConn)
        {
            try {
                String wellname = "";
                String getwellid = "";

                getwellid = "(''";
                foreach (int id in WellID)
                {
                    getwellid += "," + id.ToString();
                }
                getwellid += ")";

                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn Survey_number = iReply.Columns.Add("Survey_number", typeof(Int32));

                DataColumn MDepth = iReply.Columns.Add("MDepth", typeof(Decimal));
                DataColumn TVD = iReply.Columns.Add("TVD", typeof(Decimal));
                DataColumn Northing = iReply.Columns.Add("Northing", typeof(Decimal));
                DataColumn Easting = iReply.Columns.Add("Easting", typeof(Decimal));
                DataColumn WellName = iReply.Columns.Add("WellName", typeof(String));
                DataColumn SubSeaDepth = iReply.Columns.Add("SubDepth", typeof(Decimal));
                DataColumn CummDeltaNorthing = iReply.Columns.Add("CummDeltaNorthing", typeof(Decimal));
                DataColumn CummDeltaEasting = iReply.Columns.Add("CummDeltaEasting", typeof(Decimal));


                Int32 count = 0;
                Decimal  depth = -99.00M, subseadepth = -99.00M, tvd = -99.00M, northing = -99.00M, easting = -99.00M, CEasting = -1.00M, CNorthing = -1.00M;
                String SelectQuery = "( Select  cast ( ROW_NUMBER() over (order by md) as int) as rn ,   [md] as Depth , [bhtvd] as TVD, [bhnorthing] as N, [bheasting] as E, [Well].welName, [bhsubsea] as SubSeaDepth , [bhcummDeltaNorthing] as CN ,[bhcummDeltaEasting] as CE FROM [WellPlacementData] as WPCR , [Well] where [Well].welID = WPCR.welID and [acID] = 1  and WPCR.welID in " + getwellid;
                SelectQuery += "            UNION ALL ";
                SelectQuery += "            Select  cast ( ROW_NUMBER() over (order by md) as int) as rn ,   [md] as Depth , [sctvd] as TVD, [scnorthing] as N, [sceasting] as E, [Well].welName, [scsubsea] as SubSeaDepth , [sccummDeltaNorthing] as CN ,[sccummDeltaEasting] as CE FROM [WellPlacementData] as WPCR , [Well] where [Well].welID = WPCR.welID and [acID] = 5 and WPCR.welID in " + getwellid;
                SelectQuery += "            UNION ALL ";
                SelectQuery += "            Select  cast ( ROW_NUMBER() over (order by md) as int) as rn ,  [md] as Depth, [rwtvd] as TVD , [rwnorthing] as N , [rweasting] as E, [Well].welName, [rwsubsea] as SubSeaDepth ,  [rwcummDeltaNorthing] as CN ,[rwcummDeltaEasting] as CE FROM [WellPlacementData] as WPCR , [Well] where [Well].welID = WPCR.welID and [acID] = 4 and WPCR.welID in " + getwellid + " )";
                SelectQuery += "            ORDER BY [Well].welName,  [Depth] ASC ";

                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        //count++;
                                        count = readData.GetInt32(0);
                                        depth = readData.GetDecimal(1);
                                        tvd = readData.GetDecimal(2);
                                        northing = readData.GetDecimal(3);
                                        easting = readData.GetDecimal(4);
                                        wellname = readData.GetString(5);
                                        subseadepth = readData.GetDecimal(6);
                                        CNorthing = readData.GetDecimal(7);
                                        CEasting = readData.GetDecimal(8);

                                        iReply.Rows.Add(count, depth, tvd, northing, easting, wellname, subseadepth, CNorthing, CEasting);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close(); // this is used for SQLDataReader
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }


                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }

        }

        public static DataTable getDataSingleWellEastNorth(Int32 WellID, String dbConn)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();

                DataColumn Survey_number = iReply.Columns.Add("Survey_number", typeof(Int32));
                DataColumn MDepth = iReply.Columns.Add("MDepth", typeof(Decimal));
                DataColumn TVD = iReply.Columns.Add("TVD", typeof(Decimal));
                DataColumn Northing = iReply.Columns.Add("Northing", typeof(Decimal));
                DataColumn Easting = iReply.Columns.Add("Easting", typeof(Decimal));
                DataColumn WellName = iReply.Columns.Add("WellName", typeof(String));
                DataColumn SubSeaDepth = iReply.Columns.Add("SubDepth", typeof(Decimal));
                DataColumn CummDeltaNorthing = iReply.Columns.Add("CummDeltaNorthing", typeof(Decimal));
                DataColumn CummDeltaEasting = iReply.Columns.Add("CummDeltaEasting", typeof(Decimal));

                Int32 count = 0;
                String wellname = "";
                Decimal depth = -99.00M, subseadepth = -99.00M, tvd = -99.00M, northing = -99.00M, easting = -99.00M, CEasting = -1.00M, CNorthing = -1.00M;                

                String SelectQuery = "( Select  cast ( ROW_NUMBER() over (order by md) as int) as rn ,   [md] as Depth , [bhtvd] as TVD, [bhnorthing] as N, [bheasting] as E, [Well].welName, [bhsubsea] as SubSeaDepth , [bhcummDeltaNorthing] as CN ,[bhcummDeltaEasting] as CE FROM [WellPlacementData] as WPCR , [Well] where [Well].welID = WPCR.welID and [acID] = 1  and WPCR.welID = @welID  ";
                SelectQuery += "            UNION ALL ";
                SelectQuery += "            Select  cast ( ROW_NUMBER() over (order by md) as int) as rn ,   [md] as Depth , [sctvd] as TVD, [scnorthing] as N, [sceasting] as E, [Well].welName, [scsubsea] as SubSeaDepth , [sccummDeltaNorthing] as CN ,[sccummDeltaEasting] as CE FROM [WellPlacementData] as WPCR , [Well] where [Well].welID = WPCR.welID and [acID] = 5 and WPCR.welID = @welID ";
                SelectQuery += "            UNION ALL ";
                SelectQuery += "            Select  cast ( ROW_NUMBER() over (order by md) as int) as rn ,  [md] as Depth, [rwtvd] as TVD , [rwnorthing] as N , [rweasting] as E, [Well].welName, [rwsubsea] as SubSeaDepth ,  [rwcummDeltaNorthing] as CN ,[rwcummDeltaEasting] as CE FROM [WellPlacementData] as WPCR , [Well] where [Well].welID = WPCR.welID and [acID] = 4 and WPCR.welID = @welID  )";
                SelectQuery += "            ORDER BY [Well].welName,  [Depth] ASC ";

                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmd = new SqlCommand(SelectQuery, dataCon);
                        cmd.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = cmd.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        count = readData.GetInt32(0);
                                        depth = readData.GetDecimal(1);
                                        tvd = readData.GetDecimal(2);
                                        northing = readData.GetDecimal(3);
                                        easting = readData.GetDecimal(4);
                                        wellname = readData.GetString(5);
                                        subseadepth = readData.GetDecimal(6);
                                        CNorthing = readData.GetDecimal(7);
                                        CEasting = readData.GetDecimal(8);

                                        iReply.Rows.Add(count, depth, tvd, northing, easting, wellname, subseadepth, CNorthing, CEasting);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close(); // this is used for SQLDataReader
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getPLANDataSingleWellEastNorth(Int32 WellID, String dbConn)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn Survey_number = iReply.Columns.Add("Survey_number", typeof(Int32));
                DataColumn planDepth = iReply.Columns.Add("planDepth", typeof(Decimal));
                DataColumn planTVD = iReply.Columns.Add("planTVD", typeof(Decimal));
                DataColumn planNorthing = iReply.Columns.Add("planNorthing", typeof(Decimal));
                DataColumn planEasting = iReply.Columns.Add("planEasting", typeof(Decimal));
                DataColumn planSubSeaDepth = iReply.Columns.Add("planSubSeaDepth", typeof(Decimal));
                DataColumn planCummDeltaNorthing = iReply.Columns.Add("planCummDeltaNorthing", typeof(Decimal));
                DataColumn planCummDeltaEasting = iReply.Columns.Add("planCummDeltaEasting", typeof(Decimal));
                Int32 count = 0;
                Decimal Pdepth = -99.00M, Ptvd = -99.00M, Psubseadepth = -99.00M, Pnorthing = -99.00M, Peasting = -99.00M, PcummNorthing = -1.00M, PcummEasting = -1.00M;

                String SelectQueryPlan = "Select cast ( ROW_NUMBER() over (order by md) as int), [md] , [tvd] , [northing] , [easting], [subsea] , [cummDeltaNorthing] , [cummDeltaEasting]  FROM [WellPlanCalculatedData] WHERE [welID] = @welID ";
                SelectQueryPlan += " and format( ctime , 'yyyyMMddHHmm' ) in ";
                SelectQueryPlan += "(select  format( max(ctime) , 'yyyyMMddHHmm' ) FROM [WellPlanCalculatedData] where welID = @welID) ";
                SelectQueryPlan += " ORDER BY [md] ASC ";

                using (SqlConnection dataCon = new SqlConnection(dbConn))
                    try
                    {
                        dataCon.Open();
                        SqlCommand cmdPlan = new SqlCommand(SelectQueryPlan, dataCon);

                        cmdPlan.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = cmdPlan.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        //count++;
                                        count = readData.GetInt32(0);
                                        Pdepth = readData.GetDecimal(1);
                                        Ptvd = readData.GetDecimal(2);
                                        Pnorthing = readData.GetDecimal(3);
                                        Peasting = readData.GetDecimal(4);
                                        Psubseadepth = readData.GetDecimal(5);
                                        PcummNorthing = readData.GetDecimal(6);
                                        PcummEasting = readData.GetDecimal(7);
                                        iReply.Rows.Add(count, Pdepth, Ptvd, Pnorthing, Peasting, Psubseadepth, PcummNorthing, PcummEasting);
                                        iReply.AcceptChanges();
                                    }
                                }

                                else
                                {
                                    count = 0; Pdepth = 0; Ptvd = 0; Pnorthing = 0; Peasting = 0; Psubseadepth = 0; PcummEasting = 0; PcummNorthing = 0;
                                    iReply.Rows.Add(count, Pdepth, Ptvd, Pnorthing, Peasting, Psubseadepth, PcummNorthing, PcummEasting);
                                    iReply.AcceptChanges();
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close(); // this is used for SQLDataReader
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataSet getDataWithInThresholdEN(Int32 Threshold, Int32 WellIDTo2beCompared, List<int> WellID , String dbConn)
        {
            try {
                System.Data.DataTable ReceivedDataWellTobeCompared = new System.Data.DataTable();
                System.Data.DataTable FetchRequiredData = new System.Data.DataTable();
                System.Data.DataTable ReceivedDataFromSorrounding = new System.Data.DataTable();

                ReceivedDataWellTobeCompared = getDataSingleWellEastNorth(WellIDTo2beCompared, dbConn);                

                decimal CMNorth=0.0M,  SubSeaMain=0.0M, CMEast = 0.0M;
                
                DataTable PassOne = ReceivedDataWellTobeCompared.Clone();

                PassOne.Columns.Add("Distance_Comment" , typeof(String) );

                DataTable distPassOne = PassOne.Clone(); // With new column added 
                DataTable PassTwo = PassOne.Clone();

                PassTwo.Columns.Add("Surrounding_Lateral", typeof(String));

                DataTable distPassTwo = PassOne.Clone();

                decimal compN1 = -1.0M, compE1 = -1.0M, compSubSea1 = -1.0M;
                decimal compN2 = -1.0M, compE2 = -1.0M, compSubSea2 = -1.0M;
                decimal CalcN1 = -1.0M, CalcE1 = -1.0M, CalcSubSea1 = 9.0M, calcDist1 = 9.0M;
                decimal CalcN2 = -1.0M, CalcE2 = -1.0M, CalcSubSea2 = 9.0M, calcDist2 = 9.0M;

                Int32 C1 = 0, C2 = 0;
                
                /******************************************************************************/


                foreach (int wID in WellID) // Iterate through each well ID in the list
                {
                    ReceivedDataFromSorrounding = getDataSingleWellEastNorth(wID, dbConn);

                    foreach (DataRow MainWell in ReceivedDataWellTobeCompared.Rows)
                    {
                        SubSeaMain = MainWell.Field<decimal>(6);
                        CMNorth = MainWell.Field<decimal>(7);
                        CMEast = MainWell.Field<decimal>(8);

                        List<decimal> Distance = new List<decimal>();  // Array that hold the distance between two points on 3D

                        foreach (DataRow R in ReceivedDataFromSorrounding.Rows)
                        {
                            compSubSea1 = R.Field<decimal>(6);
                            compN1 = R.Field<decimal>(7);
                            compE1 = R.Field<decimal>(8);

                            CalcN1 = FindDifference(CMNorth, compN1);
                            CalcE1 = FindDifference(CMEast, compE1);
                            CalcSubSea1 = FindDifference(SubSeaMain, compSubSea1);

                            calcDist1 = ShortDist3D(CalcN1, CalcE1, CalcSubSea1);

                            if (calcDist1 <= Convert.ToDecimal(Threshold))
                            {
                                Distance.Add(calcDist1);
                                C1++;
                            }

                        }

                        if (Distance.Count > 0)
                        {

                            decimal min_distance = Distance.Min();

                            foreach (DataRow RR in ReceivedDataFromSorrounding.Rows)
                            {
                                compSubSea2 = RR.Field<decimal>(6);
                                compN2 = RR.Field<decimal>(7);
                                compE2 = RR.Field<decimal>(8);

                                CalcN2 = FindDifference(CMNorth, compN2);
                                CalcE2 = FindDifference(CMEast, compE2);
                                CalcSubSea2 = FindDifference(SubSeaMain, compSubSea2);

                                calcDist2 = ShortDist3D(CalcN2, CalcE2, CalcSubSea2);

                                if (calcDist2 <= min_distance)
                                {
                                    PassOne.Rows.Add(RR.Field<Int32>(0), RR.Field<decimal>(1), RR.Field<decimal>(2), RR.Field<decimal>(3), RR.Field<decimal>(4), RR.Field<String>(5), RR.Field<decimal>(6), RR.Field<decimal>(7), RR.Field<decimal>(8), "Distance: " + Math.Round(calcDist2, 3) + " From:" + MainWell.Field<String>(5) + " @ MD: " + MainWell.Field<decimal>(1));
                                    PassTwo.Rows.Add(MainWell.Field<Int32>(0), MainWell.Field<decimal>(1), MainWell.Field<decimal>(2), MainWell.Field<decimal>(3), MainWell.Field<decimal>(4), MainWell.Field<String>(5), MainWell.Field<decimal>(6), MainWell.Field<decimal>(7), MainWell.Field<decimal>(8), "Distance from " + RR.Field<String>(5) + ": " + Math.Round(calcDist2, 3) + " @ MD: " + RR.Field<decimal>(1), RR.Field<String>(5));
                                    C2++;
                                }

                            }

                            min_distance = -5000;
                            //Distance.Clear();
                        }

                    }
                }


                //**********************

                distPassOne = PassOne.DefaultView.ToTable(true, "Survey_number", "MDepth", "TVD", "Northing", "Easting", "WellName", "SubDepth", "CummDeltaNorthing", "CummDeltaEasting", "Distance_Comment");

                distPassTwo = PassTwo.DefaultView.ToTable(true, "Survey_number", "MDepth", "TVD", "Northing", "Easting", "WellName", "SubDepth", "CummDeltaNorthing", "CummDeltaEasting", "Distance_Comment", "Surrounding_Lateral");


                DataSet ReturnData = new DataSet();

                ReturnData.Tables.Add(distPassOne); // Surrouning Wells
                ReturnData.Tables.Add(distPassTwo); // Main Well - that is to be analyzed



                return ReturnData;
            
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        
        }

        public static DataTable getDataWithInThresholdENSurrounding(Int32 Threshold, Int32 WellIDTo2beCompared, List<int> WellID, String dbConn)
        {
            try
            {
                System.Data.DataTable ReceivedDataWellTobeCompared = new System.Data.DataTable();
                System.Data.DataTable FetchRequiredData = new System.Data.DataTable();
                System.Data.DataTable ReceivedDataFromSorrounding = new System.Data.DataTable();

                ReceivedDataWellTobeCompared = getDataSingleWellEastNorth(WellIDTo2beCompared, dbConn);
                ReceivedDataFromSorrounding = getSurroundingWellDataEastNorth(WellID, dbConn);

                DataTable PassOne = ReceivedDataWellTobeCompared.Clone();

                PassOne.Columns.Add("Distance_Comment", typeof(String));

                DataTable distPassOne = PassOne.Clone(); // With new column added 
                DataTable PassTwo = PassOne.Clone();
                DataTable distPassTwo = PassOne.Clone();

                decimal CMNorth = 0.0M, SubSeaSurr = 0.0M, CMEast = 0.0M;
                decimal compN1 = -1.0M, compE1 = -1.0M, compSubSea1 = -1.0M;
                decimal compN2 = -1.0M, compE2 = -1.0M, compSubSea2 = -1.0M;
                decimal CalcN1 = -1.0M, CalcE1 = -1.0M, CalcSubSea1 = 9.0M, calcDist1 = 9.0M;
                decimal CalcN2 = -1.0M, CalcE2 = -1.0M, CalcSubSea2 = 9.0M, calcDist2 = 9.0M;

                Int32 C1 = 0, C2 = 0;

                /******************************************************************************/

                foreach (DataRow Surr in ReceivedDataFromSorrounding.Rows)
                {
                    SubSeaSurr = Surr.Field<decimal>(6);
                    CMNorth = Surr.Field<decimal>(7);
                    CMEast = Surr.Field<decimal>(8);

                    List<decimal> Distance = new List<decimal>();  // Array that hold the distance between two points on 3D

                    foreach (DataRow R in ReceivedDataWellTobeCompared.Rows)
                    {
                        compSubSea1 = R.Field<decimal>(6);
                        compN1 = R.Field<decimal>(7);
                        compE1 = R.Field<decimal>(8);

                        CalcN1 = FindDifference(CMNorth, compN1);
                        CalcE1 = FindDifference(CMEast, compE1);
                        CalcSubSea1 = FindDifference(SubSeaSurr, compSubSea1);

                        calcDist1 = ShortDist3D(CalcN1, CalcE1, CalcSubSea1);

                        if (calcDist1 <= Convert.ToDecimal(Threshold))
                        {
                            Distance.Add(calcDist1);
                            C1++;
                        }

                    }

                    if (Distance.Count > 0)
                    {

                        decimal min_distance = Distance.Min();

                        foreach (DataRow RR in ReceivedDataWellTobeCompared.Rows)
                        {
                            compSubSea2 = RR.Field<decimal>(6);
                            compN2 = RR.Field<decimal>(7);
                            compE2 = RR.Field<decimal>(8);

                            CalcN2 = FindDifference(CMNorth, compN2);
                            CalcE2 = FindDifference(CMEast, compE2);
                            CalcSubSea2 = FindDifference(SubSeaSurr, compSubSea2);

                            calcDist2 = ShortDist3D(CalcN2, CalcE2, CalcSubSea2);

                            if (calcDist2 <= min_distance)
                            {
                                //PassOne.Rows.Add(RR.Field<Int32>(0), RR.Field<decimal>(1), RR.Field<decimal>(2), RR.Field<decimal>(3), RR.Field<decimal>(4), RR.Field<String>(5), RR.Field<decimal>(6), RR.Field<decimal>(7), RR.Field<decimal>(8), "Distance: " + Math.Round(calcDist2, 3) + " from " + Surr.Field<String>(5) + " at Measured Depth:  " + Surr.Field<decimal>(1));
                                PassTwo.Rows.Add(Surr.Field<Int32>(0), Surr.Field<decimal>(1), Surr.Field<decimal>(2), Surr.Field<decimal>(3), Surr.Field<decimal>(4), Surr.Field<String>(5), Surr.Field<decimal>(6), Surr.Field<decimal>(7), Surr.Field<decimal>(8), "Distance from " + RR.Field<String>(5) + ": " + Math.Round(calcDist2, 3) + " @ MD: " + RR.Field<decimal>(1));
                                C2++;
                            }

                        }

                        min_distance = -5000;

                    }

                }

                return PassTwo;

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }

        }

        public static decimal FindDifference(decimal nr1, decimal nr2)
        {
            return Math.Abs(nr1 - nr2);
        }

        public static decimal ShortDist3D(decimal N, decimal E, decimal SubSea)
        {
            double Nn = Convert.ToDouble(N);
            double Ee = Convert.ToDouble(E);
            double Ss = Convert.ToDouble(SubSea);
            
            decimal result =  Convert.ToDecimal ( Math.Sqrt( (Nn * Nn) + (Ee * Ee) + (Ss * Ss )) ) ;

            return result;
        }
 
    }
}