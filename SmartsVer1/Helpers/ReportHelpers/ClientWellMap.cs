﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartsVer1.Helpers.ReportHelpers
{
    public class ClientWellMap
    {
        static void Main()
        { }

        public static DataTable wellInfo(Int32 ClientID, String ClientDBConnection)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn WellOp = iReply.Columns.Add("Operator", typeof(Int32));
                DataColumn WName = iReply.Columns.Add("WellName", typeof(String));
                DataColumn WLong = iReply.Columns.Add("Longitude", typeof(Decimal));
                DataColumn WLat = iReply.Columns.Add("Latitude", typeof(Decimal));
                DataColumn WCnty = iReply.Columns.Add("County", typeof(Int32));
                DataColumn WFld = iReply.Columns.Add("Field", typeof(Int32));
                DataColumn WState = iReply.Columns.Add("State", typeof(Int32));
                DataColumn WAPI = iReply.Columns.Add("API", typeof(String));
                DataColumn WUWI = iReply.Columns.Add("UWI", typeof(String));
                DataColumn WDate = iReply.Columns.Add("Spud", typeof(DateTime));
                DataColumn WReg = iReply.Columns.Add("Region", typeof(Int32));
                DataColumn WSReg = iReply.Columns.Add("SubRegion", typeof(Int32));
                DataColumn WCountry = iReply.Columns.Add("Country", typeof(Int32));
                DataColumn WSpatial = iReply.Columns.Add("Spatial", typeof(String));
                Int32 oID = 0, cnty = 0, field = 0, state = 0, region = 0, subregion = 0, country = 0;
                String name = String.Empty, api = String.Empty, uwi = String.Empty, location = String.Empty;
                DateTime spud = DateTime.Now;
                Decimal lat = 0, lon = 0;
                String selData = "SELECT [optrID], [welName], [welLong], [welLat], [cntyID], [fldID], [stID], [welAPI], [welUWI], [welDate], [regID], [sregID], [ctyID], CONVERT(varchar(255), welLocation) FROM Well";
                
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        oID = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        lon = readData.GetDecimal(2);
                                        lat = readData.GetDecimal(3);
                                        cnty = readData.GetInt32(4);
                                        field = readData.GetInt32(5);
                                        state = readData.GetInt32(6);
                                        api = readData.GetString(7);
                                        uwi = readData.GetString(8);
                                        spud = readData.GetDateTime(9);
                                        region = readData.GetInt32(10);
                                        subregion = readData.GetInt32(11);
                                        country = readData.GetInt32(12);
                                        location = readData.GetString(13);
                                        iReply.Rows.Add(oID, name, lon, lat, cnty, field, state, api, uwi, spud, region, subregion, country, location);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}