﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Collections;
using QCAss = SmartsVer1.Helpers.QCHelpers.AddAssets;
using RQC = SmartsVer1.QCHelpers.sdRawDataQC;
using Client = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using User = SmartsVer1.Helpers.AccountHelpers.CreateUser;
using Prof = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using SRV = SmartsVer1.Helpers.DemogHelpers.ServicesConfig;
using OPR = SmartsVer1.Helpers.QCHelpers.Opr;

namespace SmartsVer1.Helpers.ReportHelpers
{
    public class Reports
    {
        static void Main()
        { }

        public static DataTable getBHA(Int32 RunID, String ClientDBString)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn bhaID = iReply.Columns.Add("bhaID", typeof(Int32));
                DataColumn mtrSize = iReply.Columns.Add("mtrSize", typeof(Decimal));
                DataColumn mtrSizeU = iReply.Columns.Add("mtrSizeU", typeof(String));
                DataColumn dsclrSize = iReply.Columns.Add("dsclrSize", typeof(Decimal));
                DataColumn dsclrSizeU = iReply.Columns.Add("dsclrSizeU", typeof(String));
                DataColumn bhaLength = iReply.Columns.Add("bhaLength", typeof(Decimal));
                DataColumn bhaLengthU = iReply.Columns.Add("bhaLengthU", typeof(String));
                DataColumn spaceAbove = iReply.Columns.Add("spaceAbove", typeof(Decimal));
                DataColumn spaceAboveU = iReply.Columns.Add("spaceAboveU", typeof(String));
                DataColumn spaceBelow = iReply.Columns.Add("spaceBelow", typeof(Decimal));
                DataColumn spaceBelowU = iReply.Columns.Add("spaceBelowU", typeof(String));
                DataColumn status = iReply.Columns.Add("status", typeof(String));
                DataColumn mGauss = iReply.Columns.Add("mGauss", typeof(Decimal));
                DataColumn mGaussU = iReply.Columns.Add("mGaussU", typeof(String));
                DataColumn top = iReply.Columns.Add("top", typeof(Decimal));
                DataColumn topU = iReply.Columns.Add("topU", typeof(String));
                DataColumn dscGauss = iReply.Columns.Add("dscGauss", typeof(Decimal));
                DataColumn dscGaussU = iReply.Columns.Add("dscGaussU", typeof(String));
                DataColumn bottom = iReply.Columns.Add("bottom", typeof(Decimal));
                DataColumn bottomU = iReply.Columns.Add("bottomU", typeof(String));

                String selBHA = "SELECT [bhaID], [bhaMSize], [bhaMSizeUID], [bhaDSCSize], [bhaDSCUID], [bhaMDBLen], [bhaMDBLenUID], [bhaNMSpAbv], [bhaNMSAUID], [bhaNMSpBlw], [bhaNMSBUID], [dmgstID] FROM BHA WHERE [runID] = @runID";
                String selData = "SELECT [bhaMGVal], [bhaMGValUID], [bhaDGTop], [bhaDGTUID], [bhaDSGVal], [bhaDSGUID], [bhaDGBottom], [bhaDGBUID] FROM BHADetail WHERE [bhaID] = @bhaID";
                Int32 bID = 0, mSizeID = 0, dscSizeID = 0, mdbLenU = 0, nmspAbvU = 0, nmspBlwU = 0, bhaST = 0, mgValU = 0, dgTopU = 0, dsGU = 0, dgBtmU = 0;
                Decimal mSize = -99.00M, dscSize = -99.00M, mdbLen = -99.00M, nmspAbv = -99.00M, nmspBlw = -99.00M, mgVal = -99.000000M, dgTop = -99.00M, dsGVal = -99.0000000M, dgBtm = -99.00M;
                String mSizeUN = String.Empty, dscSizeUN = String.Empty, mdbLenNM = String.Empty, spAbvNM = String.Empty, spBlwNM = String.Empty, bhaStNm = String.Empty, mgNm = String.Empty, topUNM = String.Empty, dsgNm = String.Empty, btmUNM = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getBHA = new SqlCommand(selBHA, dataCon);
                        getBHA.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readBHA = getBHA.ExecuteReader())
                        {
                            try
                            {
                                if (readBHA.HasRows)
                                {
                                    while (readBHA.Read())
                                    {
                                        bID = readBHA.GetInt32(0);
                                        mSize = readBHA.GetDecimal(1);
                                        mSizeID = readBHA.GetInt32(2);
                                        dscSize = readBHA.GetDecimal(3);
                                        dscSizeID = readBHA.GetInt32(4);
                                        mdbLen = readBHA.GetDecimal(5);
                                        mdbLenU = readBHA.GetInt32(6);
                                        nmspAbv = readBHA.GetDecimal(7);
                                        nmspAbvU = readBHA.GetInt32(8);
                                        nmspBlw = readBHA.GetDecimal(9);
                                        nmspBlwU = readBHA.GetInt32(10);
                                        bhaST = readBHA.GetInt32(11);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readBHA.Close();
                                readBHA.Dispose();
                            }
                        }
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@bhaID", SqlDbType.Int).Value = bID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        mgVal = readData.GetDecimal(0);
                                        mgValU = readData.GetInt32(1);
                                        dgTop = readData.GetDecimal(2);
                                        dgTopU = readData.GetInt32(3);
                                        dsGVal = readData.GetDecimal(4);
                                        dsGU = readData.GetInt32(5);
                                        dgBtm = readData.GetDecimal(6);
                                        dgBtmU = readData.GetInt32(7);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                //Getting Unit Name
                mSizeUN = QCAss.getLengthUnitName(mSizeID);
                dscSizeUN = QCAss.getLengthUnitName(dscSizeID);
                mdbLenNM = QCAss.getLengthUnitName(mdbLenU);
                spAbvNM = QCAss.getLengthUnitName(nmspAbvU);
                spBlwNM = QCAss.getLengthUnitName(nmspBlwU);
                bhaStNm = DMG.getStatusValue(bhaST);
                mgNm = QCAss.getMagUnitName(mgValU);
                topUNM = QCAss.getLengthUnitName(dgTopU);
                dsgNm = QCAss.getMagUnitName(dsGU);
                btmUNM = QCAss.getLengthUnitName(dgBtmU);
                //UpDating Datatable
                iReply.Rows.Add(bID, mSize, mSizeUN, dscSize, dscSizeUN, mdbLen, mdbLenNM, nmspAbv, spAbvNM, nmspBlw, spBlwNM, bhaStNm, mgVal, mgNm, dgTop, topUNM, dsGVal, dsgNm, dgBtm, btmUNM);
                iReply.AcceptChanges();
                //Return DataTable
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<String> getBHAForRunList(Int32 RunID, String ClientDBString)
        {
            try
            {
                List<String> iReply = new List<String>();                
                String selBHA = "SELECT [bhaID], [bhaMSize], [bhaMSizeUID], [bhaDSCSize], [bhaDSCUID], [bhaMDBLen], [bhaMDBLenUID], [bhaNMSpAbv], [bhaNMSAUID], [bhaNMSpBlw], [bhaNMSBUID], [dmgstID] FROM [BHA] WHERE [runID] = @runID";
                Int32 bID = 0, mSizeID = 0, dscSizeID = 0, mdbLenU = 0, nmspAbvU = 0, nmspBlwU = 0, bhaST = 0, mgValU = 0, dgTopU = 0, dsGU = 0, dgBtmU = 0;
                Decimal mSize = -99.00M, dscSize = -99.00M, mdbLen = -99.00M, nmspAbv = -99.00M, nmspBlw = -99.00M;
                String mSizeUN = String.Empty, dscSizeUN = String.Empty, mdbLenNM = String.Empty, spAbvNM = String.Empty, spBlwNM = String.Empty, bhaStNm = String.Empty, mgNm = String.Empty, topUNM = String.Empty, dsgNm = String.Empty, btmUNM = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getBHA = new SqlCommand(selBHA, dataCon);
                        getBHA.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readBHA = getBHA.ExecuteReader())
                        {
                            try
                            {
                                if (readBHA.HasRows)
                                {
                                    while (readBHA.Read())
                                    {
                                        bID = readBHA.GetInt32(0);
                                        mSize = readBHA.GetDecimal(1);
                                        mSizeID = readBHA.GetInt32(2);
                                        dscSize = readBHA.GetDecimal(3);
                                        dscSizeID = readBHA.GetInt32(4);
                                        mdbLen = readBHA.GetDecimal(5);
                                        mdbLenU = readBHA.GetInt32(6);
                                        nmspAbv = readBHA.GetDecimal(7);
                                        nmspAbvU = readBHA.GetInt32(8);
                                        nmspBlw = readBHA.GetDecimal(9);
                                        nmspBlwU = readBHA.GetInt32(10);
                                        bhaST = readBHA.GetInt32(11);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readBHA.Close();
                                readBHA.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                //Getting Unit Name
                mSizeUN = QCAss.getLengthUnitName(mSizeID);
                dscSizeUN = QCAss.getLengthUnitName(dscSizeID);
                mdbLenNM = QCAss.getLengthUnitName(mdbLenU);
                spAbvNM = QCAss.getLengthUnitName(nmspAbvU);
                spBlwNM = QCAss.getLengthUnitName(nmspBlwU);
                bhaStNm = DMG.getStatusValue(bhaST);
                mgNm = QCAss.getMagUnitName(mgValU);
                topUNM = QCAss.getLengthUnitName(dgTopU);
                dsgNm = QCAss.getMagUnitName(dsGU);
                btmUNM = QCAss.getLengthUnitName(dgBtmU);
                //UpDating List
                iReply.Add(Convert.ToString(bID));
                iReply.Add(Convert.ToString(mSize));
                iReply.Add(Convert.ToString(mSizeUN));
                iReply.Add(Convert.ToString(dscSize));
                iReply.Add(Convert.ToString(dscSizeUN));
                iReply.Add(Convert.ToString(mdbLen));
                iReply.Add(Convert.ToString(mdbLenNM));
                iReply.Add(Convert.ToString(nmspAbv));
                iReply.Add(Convert.ToString(spAbvNM));
                iReply.Add(Convert.ToString(nmspBlw));
                iReply.Add(Convert.ToString(spBlwNM));
                iReply.Add(Convert.ToString(bhaStNm));
                //Return DataTable
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getBHAAnalysis(Int32 bhaAnaID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [bhaqValue] FROM [dmgBHAAnalysis] WHERE [bhaqID] = @bhaqID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@bhaqID", SqlDbType.Int).Value = bhaAnaID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                                else
                                {
                                    iReply = "---";
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getWPBHAList(Int32 RunID, String ClientDBString)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32,String>();
                String selData ="SELECT [bhaID], [bhaName] FROM [WPBHA] WHERE [runID] = @runID";
                Int32 id = -99;
                String name = String.Empty;
                using(SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using(SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if(readData.HasRows)
                                {
                                    while(readData.Read())
                                    {
                                        id= readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                                else
                                {
                                    iReply.Add(-99,"No BHA Signature configured for selected Run");
                                }
                            }
                            catch(Exception ex)
                            {throw new System.Exception(ex.ToString());}
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch(Exception ex)
                    {throw new System.Exception(ex.ToString());}
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }                     
        }

        public static DataTable getWPBHA(Int32 bhaID, String ClientDBString)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn ID = iReply.Columns.Add("bhaID", typeof(Int32));
                DataColumn bhaName = iReply.Columns.Add("bhaName", typeof(String));
                DataColumn mtrSize = iReply.Columns.Add("mtrSize", typeof(Decimal));
                DataColumn mtrSizeU = iReply.Columns.Add("mtrSizeU", typeof(String));
                DataColumn dsclrSize = iReply.Columns.Add("dsclrSize", typeof(Decimal));
                DataColumn dsclrSizeU = iReply.Columns.Add("dsclrSizeU", typeof(String));
                DataColumn bhaLength = iReply.Columns.Add("bhaLength", typeof(Decimal));
                DataColumn bhaLengthU = iReply.Columns.Add("bhaLengthU", typeof(String));
                DataColumn spaceAbove = iReply.Columns.Add("spaceAbove", typeof(Decimal));
                DataColumn spaceAboveU = iReply.Columns.Add("spaceAboveU", typeof(String));
                DataColumn spaceBelow = iReply.Columns.Add("spaceBelow", typeof(Decimal));
                DataColumn spaceBelowU = iReply.Columns.Add("spaceBelowU", typeof(String));
                DataColumn status = iReply.Columns.Add("status", typeof(String));
                DataColumn mGauss = iReply.Columns.Add("mGauss", typeof(Decimal));
                DataColumn mGaussU = iReply.Columns.Add("mGaussU", typeof(String));
                DataColumn top = iReply.Columns.Add("top", typeof(Decimal));
                DataColumn topU = iReply.Columns.Add("topU", typeof(String));
                DataColumn dscGauss = iReply.Columns.Add("dscGauss", typeof(Decimal));
                DataColumn dscGaussU = iReply.Columns.Add("dscGaussU", typeof(String));
                DataColumn bottom = iReply.Columns.Add("bottom", typeof(Decimal));
                DataColumn bottomU = iReply.Columns.Add("bottomU", typeof(String));

                String selBHA = "SELECT [bhaID], [bhaMSize], [bhaMSizeUID], [bhaDSCSize], [bhaDSCUID], [bhaMDBLen], [bhaMDBLenUID], [bhaNMSpAbv], [bhaNMSAUID], [bhaNMSpBlw], [bhaNMSBUID], [dmgstID], [bhaName] FROM [WPBHA] WHERE [bhaID] = @bhaID";
                String selData = "SELECT [bhaMGVal], [bhaMGValUID], [bhaDGTop], [bhaDGTUID], [bhaDSGVal], [bhaDSGUID], [bhaDGBottom], [bhaDGBUID] FROM [WPBHADetail] WHERE [bhaID] = @bhaID";
                Int32 bID = 0, mSizeID = 0, dscSizeID = 0, mdbLenU = 0, nmspAbvU = 0, nmspBlwU = 0, bhaST = 0, mgValU = 0, dgTopU = 0, dsGU = 0, dgBtmU = 0;
                Decimal mSize = -99.00M, dscSize = -99.00M, mdbLen = -99.00M, nmspAbv = -99.00M, nmspBlw = -99.00M, mgVal = -99.000000M, dgTop = -99.00M, dsGVal = -99.0000000M, dgBtm = -99.00M;
                String name = String.Empty, mSizeUN = String.Empty, dscSizeUN = String.Empty, mdbLenNM = String.Empty, spAbvNM = String.Empty, spBlwNM = String.Empty, bhaStNm = String.Empty, mgNm = String.Empty, topUNM = String.Empty, dsgNm = String.Empty, btmUNM = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getBHA = new SqlCommand(selBHA, dataCon);
                        getBHA.Parameters.AddWithValue("@bhaID", SqlDbType.Int).Value = bhaID.ToString();
                        using (SqlDataReader readBHA = getBHA.ExecuteReader())
                        {
                            try
                            {
                                if (readBHA.HasRows)
                                {
                                    while (readBHA.Read())
                                    {
                                        bID = readBHA.GetInt32(0);
                                        mSize = readBHA.GetDecimal(1);
                                        mSizeID = readBHA.GetInt32(2);
                                        dscSize = readBHA.GetDecimal(3);
                                        dscSizeID = readBHA.GetInt32(4);
                                        mdbLen = readBHA.GetDecimal(5);
                                        mdbLenU = readBHA.GetInt32(6);
                                        nmspAbv = readBHA.GetDecimal(7);
                                        nmspAbvU = readBHA.GetInt32(8);
                                        nmspBlw = readBHA.GetDecimal(9);
                                        nmspBlwU = readBHA.GetInt32(10);
                                        bhaST = readBHA.GetInt32(11);
                                        name = readBHA.GetString(12);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readBHA.Close();
                                readBHA.Dispose();
                            }
                        }
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@bhaID", SqlDbType.Int).Value = bhaID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        mgVal = readData.GetDecimal(0);
                                        mgValU = readData.GetInt32(1);
                                        dgTop = readData.GetDecimal(2);
                                        dgTopU = readData.GetInt32(3);
                                        dsGVal = readData.GetDecimal(4);
                                        dsGU = readData.GetInt32(5);
                                        dgBtm = readData.GetDecimal(6);
                                        dgBtmU = readData.GetInt32(7);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                //Getting Unit Name
                mSizeUN = QCAss.getLengthUnitName(mSizeID);
                dscSizeUN = QCAss.getLengthUnitName(dscSizeID);
                mdbLenNM = QCAss.getLengthUnitName(mdbLenU);
                spAbvNM = QCAss.getLengthUnitName(nmspAbvU);
                spBlwNM = QCAss.getLengthUnitName(nmspBlwU);
                bhaStNm = DMG.getStatusValue(bhaST);
                mgNm = QCAss.getMagUnitName(mgValU);
                topUNM = QCAss.getLengthUnitName(dgTopU);
                dsgNm = QCAss.getMagUnitName(dsGU);
                btmUNM = QCAss.getLengthUnitName(dgBtmU);
                //UpDating Datatable
                iReply.Rows.Add(bID, name, mSize, mSizeUN, dscSize, dscSizeUN, mdbLen, mdbLenNM, nmspAbv, spAbvNM, nmspBlw, spBlwNM, bhaStNm, mgVal, mgNm, dgTop, topUNM, dsGVal, dsgNm, dgBtm, btmUNM);
                iReply.AcceptChanges();
                //Return DataTable
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getDisclaimer(Int32 DisclaimerID)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn dID = iReply.Columns.Add("ID", typeof(Int32));
                DataColumn dVal = iReply.Columns.Add("Statement", typeof(String));
                String selData = "SELECT [dscID], [dscValue] FROM [dmgDisclaimer] WHERE [dsctID] = @dsctID";
                Int32 id = 0;
                String value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@dsctID", SqlDbType.Int).Value = DisclaimerID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        value = readData.GetString(1);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply.Rows.Add(id, value);
                iReply.AcceptChanges();
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getDisclaimerID(String DisclaimerName)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT [dsctID] FROM [dmgDisclaimerType] WHERE [dsctValue] = @dsctValue";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@dsctValue", SqlDbType.Int).Value = DisclaimerName.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getIFRRefMagInfo(Int32 IFRVal, Int32 RefMagID, String ClientDBString)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn rmID = iReply.Columns.Add("rmID", typeof(Int32));
                DataColumn imdec = iReply.Columns.Add("imdec", typeof(String));
                DataColumn idip = iReply.Columns.Add("idip", typeof(String));
                DataColumn ibTotal = iReply.Columns.Add("ibTotal", typeof(String));
                DataColumn igTotal = iReply.Columns.Add("igTotal", typeof(String));
                Decimal imd = -99.00M, idp = -99.00M, ibt = -99.0000000M, igt = -99.0000000M;
                String md = String.Empty, dp = String.Empty, bt = String.Empty, gt = String.Empty;
                String selData = "SELECT [imdec], [idip], [ibTotal], [igTotal] FROM iRefMags WHERE [rfmgID] = @rfmgID";
                if (IFRVal.Equals(1)) // NO IFR
                {
                    md = " --- ";
                    dp = " --- ";
                    bt = " --- ";
                    gt = " --- ";
                    iReply.Rows.Add(RefMagID, md, dp, bt, gt);
                    iReply.AcceptChanges();
                }
                else // IFR Values exist
                {
                    using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                    {
                        try
                        {
                            dataCon.Open();
                            SqlCommand getData = new SqlCommand(selData, dataCon);
                            getData.Parameters.AddWithValue("@rfmgID", SqlDbType.Int).Value = RefMagID.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            imd = readData.GetDecimal(0);
                                            idp = readData.GetDecimal(1);
                                            ibt = readData.GetDecimal(2);
                                            igt = readData.GetDecimal(3);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                            md = Convert.ToString(imd);
                            dp = Convert.ToString(idp);
                            bt = Convert.ToString(ibt);
                            gt = Convert.ToString(igt);
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            dataCon.Close();
                            dataCon.Dispose();
                        }
                    }
                    iReply.Rows.Add(rmID, md, dp, bt, gt);
                    iReply.AcceptChanges();
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getInputDataTable(Int32 WellID, Int32 WellSectionID, Int32 RunID, Decimal Depth, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn sfdID = iReply.Columns.Add("sfdID", typeof(Int32));
                DataColumn sfdDepth = iReply.Columns.Add("sfdDepth", typeof(Decimal));
                DataColumn sfdBx = iReply.Columns.Add("sfdBx", typeof(Decimal));
                DataColumn sfdBy = iReply.Columns.Add("sfdBy", typeof(Decimal));
                DataColumn sfdBz = iReply.Columns.Add("sfdBz", typeof(Decimal));
                DataColumn sfdGx = iReply.Columns.Add("sfdGx", typeof(Decimal));
                DataColumn sfdGy = iReply.Columns.Add("sfdGy", typeof(Decimal));
                DataColumn sfdGz = iReply.Columns.Add("sfdGz", typeof(Decimal));
                String selData = "SELECT [sfdID], [sfdDepth], [sfdGx], [sfdGy], [sfdGz], [sfdBx], [sfdBy], [sfdBz] FROM [SurveyFormattedData] WHERE [runID] = @runID";
                Int32 id = 0;
                Decimal depth = -99.00M, gx = -99.000000000000M, gy = -99.000000000000M, gz = -99.000000000000M, bx = -99.000000000000M, by = -99.000000000000M, bz = -99.000000000000M;


                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        depth = readData.GetDecimal(1);
                                        gx = readData.GetDecimal(2);
                                        gy = readData.GetDecimal(3);
                                        gz = readData.GetDecimal(4);
                                        bx = readData.GetDecimal(5);
                                        by = readData.GetDecimal(6);
                                        bz = readData.GetDecimal(7);
                                        iReply.Rows.Add(id, depth, bx, by, bz, gx, gy, gz);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Decimal> getInputDataList(Int32 WellID, Int32 WellSectionID, Int32 RunID, Decimal Depth, String ClientDBString)
        {
            try
            {
                List<Decimal> iReply = new List<Decimal>();
                String selData = "SELECT [sfdID], [sfdDepth], [sfdGx], [sfdGy], [sfdGz], [sfdBx], [sfdBy], [sfdBz] FROM [SurveyFormattedData] WHERE [runID] = @runID";
                Int32 id = 0;
                Decimal depth = -99.00M, gx = -99.000000000000M, gy = -99.000000000000M, gz = -99.000000000000M, bx = -99.000000000000M, by = -99.000000000000M, bz = -99.000000000000M;


                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        depth = readData.GetDecimal(1);
                                        gx = readData.GetDecimal(2);                                        
                                        gy = readData.GetDecimal(3);
                                        gz = readData.GetDecimal(4);
                                        bx = readData.GetDecimal(5);
                                        by = readData.GetDecimal(6);
                                        bz = readData.GetDecimal(7);
                                        iReply.Add(bx);
                                        iReply.Add(by);
                                        iReply.Add(bz);
                                        iReply.Add(gx);
                                        iReply.Add(gy);
                                        iReply.Add(gz);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }                              

        public static DataTable getQCResults(Int32 WellID, Int32 SectionID, Int32 RunID, String ClientDBString)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn svyID = iReply.Columns.Add("SrvyID", typeof(Int32));
                DataColumn runID = iReply.Columns.Add("RunID", typeof(Int32));
                DataColumn rdepth = iReply.Columns.Add("Depth", typeof(Decimal));
                DataColumn rdate = iReply.Columns.Add("Date", typeof(String));
                DataColumn rtime = iReply.Columns.Add("Time", typeof(String));
                DataColumn rinc = iReply.Columns.Add("Inc", typeof(Decimal));
                DataColumn razm = iReply.Columns.Add("Azm", typeof(Decimal));
                DataColumn rdip = iReply.Columns.Add("Dip", typeof(Decimal));
                DataColumn rbt = iReply.Columns.Add("BTotal", typeof(Decimal));
                DataColumn rgt = iReply.Columns.Add("GTotal", typeof(Decimal));
                DataColumn rdipQSt = iReply.Columns.Add("DipQ", typeof(String));
                DataColumn rDipQ = iReply.Columns.Add("DipQID", typeof(Int32));
                DataColumn rddip = iReply.Columns.Add("DeltaDip", typeof(Decimal));
                DataColumn rbtQSt = iReply.Columns.Add("BTtlQ", typeof(String));
                DataColumn rbtQ = iReply.Columns.Add("BTtlID", typeof(Int32));
                DataColumn rdbt = iReply.Columns.Add("DeltaBTtl", typeof(Decimal));
                DataColumn rgtQSt = iReply.Columns.Add("GTtlQ", typeof(String));
                DataColumn rgtQ = iReply.Columns.Add("GTtlID", typeof(Int32));
                DataColumn rdgt = iReply.Columns.Add("DeltaGTtl", typeof(Decimal));
                DataColumn rcorrAzm = iReply.Columns.Add("CorrAzm", typeof(Decimal));
                DataColumn rerrAzm = iReply.Columns.Add("AzmErr", typeof(Decimal));
                DataColumn rscAzm = iReply.Columns.Add("SCAzm", typeof(Decimal));
                DataColumn rdShrtLng = iReply.Columns.Add("DAzm", typeof(Decimal));
                DataColumn razmBHA = iReply.Columns.Add("DAzmBHA", typeof(Decimal));
                DataColumn razmMtr = iReply.Columns.Add("DAzmMtr", typeof(Decimal));
                DataColumn razmDC = iReply.Columns.Add("DAzmDC", typeof(Decimal));
                DataColumn rNMSpSt = iReply.Columns.Add("NMSp", typeof(String));
                DataColumn rNMSp = iReply.Columns.Add("NMSpID", typeof(Int32));
                DataColumn razmBHAG = iReply.Columns.Add("DAzmBHAG", typeof(Decimal));
                DataColumn razmMtrG = iReply.Columns.Add("DAzmMtrG", typeof(Decimal));
                DataColumn razmDCDwn = iReply.Columns.Add("DAzmDCDwn", typeof(Decimal));
                DataColumn rNMSpGSt = iReply.Columns.Add("BHASp", typeof(String));
                DataColumn rNMSpG = iReply.Columns.Add("BHASpID", typeof(Int32));
                DataColumn rminDip = iReply.Columns.Add("minDip", typeof(Decimal));
                DataColumn rmaxDip = iReply.Columns.Add("maxDip", typeof(Decimal));
                DataColumn rspdDip = iReply.Columns.Add("spdDip", typeof(Decimal));
                DataColumn rminBT = iReply.Columns.Add("minBT", typeof(Int32));
                DataColumn rmaxBT = iReply.Columns.Add("maxBT", typeof(Int32));
                DataColumn rspdBT = iReply.Columns.Add("spdBT", typeof(Int32));
                DataColumn rminGT = iReply.Columns.Add("minGT", typeof(Decimal));
                DataColumn rmaxGT = iReply.Columns.Add("maxGT", typeof(Decimal));
                DataColumn rspdGT = iReply.Columns.Add("spdGT", typeof(Decimal));
                DataColumn rGTF = iReply.Columns.Add("GTF", typeof(Decimal));
                DataColumn rMTF = iReply.Columns.Add("MTF", typeof(Decimal));
                DataColumn rngzSt = iReply.Columns.Add("NoGoZone", typeof(String));
                DataColumn rngz = iReply.Columns.Add("NGZID", typeof(Int32));
                DataColumn rta = iReply.Columns.Add("taStat", typeof(String));
                DataColumn rtaSt = iReply.Columns.Add("taID", typeof(Int32));
                DataColumn rchkshot = iReply.Columns.Add("chkshot", typeof(Int32));
                DataColumn rBx = iReply.Columns.Add("Bx", typeof(Decimal));
                DataColumn rBy = iReply.Columns.Add("By", typeof(Decimal));
                DataColumn rBz = iReply.Columns.Add("Bz", typeof(Decimal));
                DataColumn rGx = iReply.Columns.Add("Gx", typeof(Decimal));
                DataColumn rGy = iReply.Columns.Add("Gy", typeof(Decimal));
                DataColumn rGz = iReply.Columns.Add("Gz", typeof(Decimal));
                List<Decimal> rawValues = new List<Decimal>();
                Decimal BxVal = -99.00000000M, ByVal = -99.00000000M, BzVal = -99.00000000M, GxVal = -99.00000000M, GyVal = -99.00000000M, GzVal = -99.00000000M;
                Int32 sID = 0, dipQ = 0, btQ = 0, gtQ = 0, NonMagSp = 0, NonMagSpG = 0, minbt = 0, maxbt = 0, spdbt = 0, ngz = 0, ta = 0, chkshot = 0;
                Decimal depth = -99.00M, inc = -99.00M, azm = -99.00M, dip = -99.00M, btotal = -99.00M, gtotal = -99.00M, deltadip = -99.00M, deltabt = -99.00M, deltagt = -99.00M, corrAzm = -99.00M, errAzm = -99.00M, scAzm = -99.00M, deltaShrtLong = -99.00M, dAzmBHA = -99.00M, dAzmMtr = -99.00M, dAzmDC = -99.00M, dAzmBHAG = -99.00M, dAzmMtrG = -99.00M, dAzmDCDwn = -99.00M, mindip = -99.00M, maxdip = -99.00M, spddip = -99.00M, mingt = -99.00M, maxgt = -99.00M, spdgt = -99.00M, gtf = -99.00M, mtf = -99.00M;
                String date = String.Empty, time = String.Empty, dipQStat = String.Empty, btQStat = String.Empty, gtQStat = String.Empty, NonMagSpStat = String.Empty, NonMagSpGStat = String.Empty, ngzStat = String.Empty, taStat = String.Empty;
                String selData = "SELECT [srvyID], [Depth], [colDN], [colDO], [colT], [colDR], [colBA], [colCA], [colCF], [colDW], [colBC], [colDY], [colCD], [colEA], [colCG], [colAO], [colAP], [colDD], [colDE], [colGE], [colEI], [colEJ], [colEK], [colGP], [colGN], [colGO], [colEO], [colEU], [colEV], [colEW], [colEX], [colEY], [colEZ], [colFA], [colFB], [colFC], [colP], [colQ], [colHV], [taID], [chkShot] FROM [RawQCResults] WHERE [welID] = @welID AND [wswID] = @wswID AND [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = SectionID.ToString();
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        sID = readData.GetInt32(0);
                                        depth = readData.GetDecimal(1);
                                        date = readData.GetString(2);
                                        time = readData.GetString(3);
                                        inc = readData.GetDecimal(4);
                                        azm = readData.GetDecimal(5);
                                        dip = readData.GetDecimal(6);
                                        btotal = readData.GetDecimal(7);
                                        gtotal = readData.GetDecimal(8);
                                        dipQ = readData.GetInt32(9);
                                        dipQStat = RQC.getDipQualifier(dipQ);
                                        deltadip = readData.GetDecimal(10);
                                        btQ = readData.GetInt32(11);
                                        btQStat = RQC.getBTQualifier(btQ);
                                        deltabt = readData.GetDecimal(12);
                                        gtQ = readData.GetInt32(13);
                                        gtQStat = RQC.getGTQualifier(gtQ);
                                        deltagt = readData.GetDecimal(14);
                                        corrAzm = readData.GetDecimal(15);
                                        errAzm = readData.GetDecimal(16);
                                        scAzm = readData.GetDecimal(17);
                                        deltaShrtLong = readData.GetDecimal(18);
                                        dAzmBHA = readData.GetDecimal(19);
                                        dAzmMtr = readData.GetDecimal(20);
                                        dAzmDC = readData.GetDecimal(21);
                                        NonMagSp = readData.GetInt32(22);
                                        NonMagSpStat = RQC.getBHAStatement(NonMagSp);
                                        dAzmBHAG = readData.GetDecimal(23);
                                        dAzmMtrG = readData.GetDecimal(24);
                                        dAzmDCDwn = readData.GetDecimal(25);
                                        NonMagSpG = readData.GetInt32(26);
                                        NonMagSpGStat = RQC.getBHAStatement(NonMagSpG);
                                        mindip = readData.GetDecimal(27);
                                        maxdip = readData.GetDecimal(28);
                                        spddip = readData.GetDecimal(29);
                                        minbt = readData.GetInt32(30);
                                        maxbt = readData.GetInt32(31);
                                        spdbt = readData.GetInt32(32);
                                        mingt = readData.GetDecimal(33);
                                        maxgt = readData.GetDecimal(34);
                                        spdgt = readData.GetDecimal(35);
                                        gtf = readData.GetDecimal(36);
                                        mtf = readData.GetDecimal(37);
                                        ngz = readData.GetInt32(38);
                                        if (ngz.Equals(1))
                                        {
                                            ngzStat = "Yes";
                                        }
                                        else
                                        {
                                            ngzStat = "No";
                                        }
                                        ta = readData.GetInt32(39);
                                        taStat = RQC.getTrendAnalysisStatement(ta);
                                        chkshot = readData.GetInt32(40);
                                        rawValues = getInputDataList(WellID, SectionID, RunID, depth, ClientDBString);
                                        BxVal = Convert.ToDecimal(rawValues[0]);
                                        ByVal = Convert.ToDecimal(rawValues[1]);
                                        BzVal = Convert.ToDecimal(rawValues[2]);
                                        GxVal = Convert.ToDecimal(rawValues[3]);
                                        GyVal = Convert.ToDecimal(rawValues[4]);
                                        GzVal = Convert.ToDecimal(rawValues[5]);
                                        iReply.Rows.Add(sID, RunID, depth, date, time, inc, azm, dip, btotal, gtotal, dipQStat, dipQ, deltadip, btQStat, btQ, deltabt, gtQStat, gtQ, deltagt, corrAzm, errAzm, scAzm, deltaShrtLong, dAzmBHA, dAzmMtr, dAzmDC, NonMagSpStat, NonMagSp, dAzmBHAG, dAzmMtrG, dAzmDCDwn, NonMagSpGStat, NonMagSpG, mindip, maxdip, spddip, minbt, maxbt, spdbt, mingt, maxgt, spdgt, gtf, mtf, ngzStat, ngz, taStat, ta, chkshot, BxVal, ByVal, BzVal, GxVal, GyVal, GzVal);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
        
        public static DataTable getRunInfo(Int32 RunID, String ClientDBString)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn runID = iReply.Columns.Add("runID", typeof(Int32));
                DataColumn runName = iReply.Columns.Add("runName", typeof(String));
                DataColumn TInc = iReply.Columns.Add("TInc", typeof(Decimal));
                DataColumn TAzm = iReply.Columns.Add("TAzm", typeof(Decimal));
                DataColumn rStt = iReply.Columns.Add("rStt", typeof(String));
                Int32 rID = 0, rStat = 0;
                Decimal inc = -99.00M, azm = -99.00M;
                String rN = String.Empty, rStatN = String.Empty;
                String selData = "SELECT [runName], [runTInc], [runTAzm], [dmgstID] FROM Run WHERE [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        rID = readData.GetInt32(0);
                                        inc = readData.GetDecimal(1);
                                        azm = readData.GetDecimal(2);
                                        rStat = readData.GetInt32(3);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                //Getting value names
                rN = QCAss.getRunName(rID);
                rStatN = DMG.getStatusValue(rStat);
                //Creating DataTable row
                iReply.Rows.Add(RunID, rN, inc, azm, rStatN);
                iReply.AcceptChanges();
                //Return DataTable
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getRefMagInfo(Int32 WellID, String ClientDBString)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn wID = iReply.Columns.Add("wID", typeof(Int32));
                DataColumn rfID = iReply.Columns.Add("rfID", typeof(Int32));
                DataColumn mdec = iReply.Columns.Add("mdec", typeof(Decimal));
                DataColumn dip = iReply.Columns.Add("dip", typeof(Decimal));
                DataColumn bTotal = iReply.Columns.Add("bTotal", typeof(Decimal));
                DataColumn gTotal = iReply.Columns.Add("gTotal", typeof(Decimal));
                DataColumn gConv = iReply.Columns.Add("gConv", typeof(Decimal));
                Int32 id = 0;
                Decimal md = -99.00M, dp = -99.00M, bt = -99.0000000M, gt = -99.0000000M, gc = -99.00M;
                String selData = "SELECT [rfmgID], [mdec], [dip], [bTotal], [gTotal], [gconv] FROM [RefMags] WHERE [welID] = @welID AND [dmgstID] = '1'";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        md = readData.GetDecimal(1);
                                        dp = readData.GetDecimal(2);
                                        bt = readData.GetDecimal(3);
                                        gt = readData.GetDecimal(4);
                                        gc = readData.GetDecimal(5);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                //Create Data Row
                iReply.Rows.Add(WellID, id, md, dp, bt, gt, gc);
                iReply.AcceptChanges();
                //Return DataTable
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }            
        }

        public static DataTable getSQCAssetList(String ClientDBString)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn sqcId = iReply.Columns.Add("sqcID", typeof(String));
                DataColumn sqcNm = iReply.Columns.Add("sqcName", typeof(String));
                DataColumn sqcdp = iReply.Columns.Add("sqcDip", typeof(Decimal));
                DataColumn sqcbt = iReply.Columns.Add("sqcBTotal", typeof(Decimal));
                DataColumn sqcgt = iReply.Columns.Add("sqcGTotal", typeof(Decimal));
                Int32 id = 0;
                String sqcN = String.Empty;
                Decimal dip = -99.0000M, bt = -99.0000M, gt = -99.0000000M;
                String selData = "SELECT [sqcID], [sqcName], [sqcDip], [sqcBTotal], [sqcGTotal] FROM [SQC] ORDER BY [sqcName]";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            id = readData.GetInt32(0);
                                            sqcN = readData.GetString(1);
                                            dip = readData.GetDecimal(2);
                                            bt = readData.GetDecimal(3);
                                            gt = readData.GetDecimal(4);
                                            iReply.Rows.Add(id, sqcN, dip, bt, gt);
                                            iReply.AcceptChanges();
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }     

        public static DataTable getSQC(Int32 RunID, String ClientDBString)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn sqcNm = iReply.Columns.Add("sqcName", typeof(String));
                DataColumn sqcdp = iReply.Columns.Add("sqcDip", typeof(Decimal));
                DataColumn sqcbt = iReply.Columns.Add("sqcBTotal", typeof(Decimal));
                DataColumn sqcgt = iReply.Columns.Add("sqcGTotal", typeof(Decimal));
                Int32 id = 0;
                String sqcN = String.Empty;
                Decimal dip = -99.0000M, bt = -99.0000M, gt = -99.0000000M;
                ArrayList sqcList = new ArrayList();
                String selSQC = "SELECT DISTINCT(sqcID) FROM RawQCResults WHERE [runID] = @runID";
                String selData = "SELECT [sqcName], [sqcDip], [sqcBTotal], [sqcGTotal] FROM SQC WHERE [sqcID] = @sqcID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getSQC = new SqlCommand(selSQC, dataCon);
                        getSQC.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader sqcReader = getSQC.ExecuteReader())
                        {
                            try
                            {
                                if (sqcReader.HasRows)
                                {
                                    while (sqcReader.Read())
                                    {
                                        id = sqcReader.GetInt32(0);
                                        sqcList.Add(id);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                sqcReader.Close();
                                sqcReader.Dispose();
                            }
                        }

                        foreach (var i in sqcList)
                        {
                            SqlCommand getData = new SqlCommand(selData, dataCon);
                            getData.Parameters.AddWithValue("@sqcID", SqlDbType.Int).Value = i.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            sqcN = readData.GetString(0);
                                            dip = readData.GetDecimal(1);
                                            bt = readData.GetDecimal(2);
                                            gt = readData.GetDecimal(3);
                                            iReply.Rows.Add(sqcN, dip, bt, gt);
                                            iReply.AcceptChanges();
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }        
        }

        public static DataTable getServiceVersion(Int32 ServiceID)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn sid = iReply.Columns.Add("srvID", typeof(Int32));
                DataColumn sName = iReply.Columns.Add("srvName", typeof(String));
                DataColumn sVer = iReply.Columns.Add("srvVersion", typeof(String));
                DataColumn sVal = iReply.Columns.Add("srvValue", typeof(String));
                String selData = "SELECT [srvID], [srvName], [srvVersion] FROM dmgService WHERE [srvID] = @srvID";
                Int32 id = 0;
                String name = String.Empty, version = String.Empty, value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        version = readData.GetString(2);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                value = name + " (Version:" + version + ")";
                iReply.Rows.Add(id, name, version, value);
                iReply.AcceptChanges();
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getWellList(String ClientDBString)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn welID = iReply.Columns.Add("welID", typeof(Int32));
                DataColumn welName = iReply.Columns.Add("welName", typeof(String));
                DataColumn region = iReply.Columns.Add("Region", typeof(String));
                DataColumn subregion = iReply.Columns.Add("Subregion", typeof(String));
                DataColumn country = iReply.Columns.Add("Country", typeof(String));
                DataColumn state = iReply.Columns.Add("State", typeof(String));
                DataColumn county = iReply.Columns.Add("County", typeof(String));
                DataColumn field = iReply.Columns.Add("Field", typeof(String));
                DataColumn welLat = iReply.Columns.Add("Latitude", typeof(Decimal));
                DataColumn welLatO = iReply.Columns.Add("LatO", typeof(String));
                DataColumn welLng = iReply.Columns.Add("Longitude", typeof(Decimal));
                DataColumn welLngO = iReply.Columns.Add("LngO", typeof(String));
                DataColumn wPermit = iReply.Columns.Add("Permit", typeof(String));
                DataColumn wSpud = iReply.Columns.Add("Spud", typeof(DateTime));
                DataColumn wStat = iReply.Columns.Add("Status", typeof(String));
                DataColumn wAPI = iReply.Columns.Add("API", typeof(String));
                DataColumn wUWI = iReply.Columns.Add("UWI", typeof(String));
                Int32 id = -99, rgID = -99, srgID = -99, ctyID = -99, stID = -99, cntyID = -99, fID = -99, latO = -99, lngO = -99, stat = -99;
                String wName = String.Empty, Region = String.Empty, sRegion = String.Empty, wCountry = String.Empty, wState = String.Empty, wCounty = String.Empty, wField = String.Empty;
                Decimal lat = -99.000000M, lon = -99.000000M;
                String LatOrient = String.Empty, LngOrient = String.Empty, API = String.Empty, UWI = String.Empty, Permit = String.Empty, Status = String.Empty;
                DateTime spud = DateTime.Now;
                String selData = "SELECT [welID], [welName], [regID], [sregID], [ctyID], [stID], [cntyID], [fldID], [welPermit], [welSpudDate], [welLatitude], [welLatOrientation], [welLongitude], [welLngOrientation], [welAPI], [welUWI], [wstID] FROM [Well] ORDER BY [welName]";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        wName = readData.GetString(1);
                                        rgID = readData.GetInt32(2);
                                        Region = DMG.getRegionName(rgID);
                                        srgID = readData.GetInt32(3);
                                        sRegion = DMG.getSubRegionName(srgID);
                                        ctyID = readData.GetInt32(4);
                                        wCountry = DMG.getCountryName(ctyID);
                                        stID = readData.GetInt32(5);
                                        wState = DMG.getStateName(stID);
                                        cntyID = readData.GetInt32(6);
                                        wCounty = DMG.getCountyName(cntyID);
                                        fID = readData.GetInt32(7);
                                        wField = DMG.getFieldName(fID);
                                        Permit = readData.GetString(8);
                                        spud = readData.GetDateTime(9);
                                        lat = readData.GetDecimal(10);
                                        latO = readData.GetInt32(11);
                                        LatOrient = DMG.getCardinalName(latO);
                                        lon = readData.GetDecimal(12);
                                        lngO = readData.GetInt32(13);
                                        LngOrient = DMG.getCardinalName(lngO);
                                        API = readData.GetString(14);
                                        UWI = readData.GetString(15);
                                        stat = readData.GetInt32(16);
                                        Status = DMG.getWellStatusName(stat);
                                        if (API.Equals("-99"))
                                        {
                                            API = " --- ";
                                        }
                                        if (UWI.Equals("-99"))
                                        {
                                            UWI = " --- ";                                            
                                        }
                                        iReply.Rows.Add(id, wName, Region, sRegion, wCountry, wState, wCounty, wField, lat, LatOrient, lon, LngOrient, Permit, spud, Status, API, UWI);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getWellInfo(Int32 WellID, String ClientDBString)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn welID = iReply.Columns.Add("welID", typeof(Int32));
                DataColumn oprID = iReply.Columns.Add("oprID", typeof(Int32));
                DataColumn oprName = iReply.Columns.Add("oprName", typeof(String));
                DataColumn welName = iReply.Columns.Add("welName", typeof(String));
                DataColumn welLong = iReply.Columns.Add("welLong", typeof(Decimal));
                DataColumn welLat = iReply.Columns.Add("welLat", typeof(Decimal));
                DataColumn welAPI = iReply.Columns.Add("welAPI", typeof(String));
                DataColumn welUWI = iReply.Columns.Add("welUWI", typeof(String));
                DataColumn welDate = iReply.Columns.Add("welDate", typeof(DateTime));
                Int32 oID = 0;
                Decimal lat = -99.000000M, lon = -99.000000M;
                String oName = String.Empty, wName = String.Empty, API = String.Empty, UWI = String.Empty;
                DateTime spud = DateTime.Now;
                String selData = "SELECT [optrID], [welName], [welLongitude], [welLatitude], [welAPI], [welUWI], [welSpudDate] FROM [Well] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        oID = readData.GetInt32(0);
                                        wName = readData.GetString(1);
                                        lon = readData.GetDecimal(2);
                                        lat = readData.GetDecimal(3);
                                        API = readData.GetString(4);
                                        UWI = readData.GetString(5);
                                        spud = readData.GetDateTime(6);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                //Getting values
                oName = Client.getClientName(oID);
                if (API.Equals("-99"))
                {
                    API = " --- ";
                }
                if (UWI.Equals("-99"))
                {
                    UWI = " --- ";
                }
                //Adding Table Row
                iReply.Rows.Add(WellID, oID, oName, wName, lon, lat, API, UWI, spud);
                iReply.AcceptChanges();
                //Return Table
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getWPWellInfo(Int32 WellID, String ClientDBString)
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn welID = iReply.Columns.Add("welID", typeof(Int32));
                DataColumn oprID = iReply.Columns.Add("oprID", typeof(Int32));
                DataColumn oprName = iReply.Columns.Add("oprName", typeof(String));
                DataColumn welName = iReply.Columns.Add("welName", typeof(String));
                DataColumn welLong = iReply.Columns.Add("welLong", typeof(Decimal));
                DataColumn welLat = iReply.Columns.Add("welLat", typeof(Decimal));
                DataColumn welAPI = iReply.Columns.Add("welAPI", typeof(String));
                DataColumn welUWI = iReply.Columns.Add("welUWI", typeof(String));
                DataColumn welDate = iReply.Columns.Add("welDate", typeof(DateTime));
                Int32 oID = 0;
                Decimal lat = -99.000000M, lon = -99.000000M;
                String oName = String.Empty, wName = String.Empty, API = String.Empty, UWI = String.Empty;
                DateTime spud = DateTime.Now;
                String selData = "SELECT [optrID], [welName], [welLongitude], [welLatitude], [welAPI], [welUWI], [welSpudDate] FROM [Well] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        oID = readData.GetInt32(0);
                                        wName = readData.GetString(1);
                                        lon = readData.GetDecimal(2);
                                        lat = readData.GetDecimal(3);
                                        API = readData.GetString(4);
                                        UWI = readData.GetString(5);
                                        spud = readData.GetDateTime(6);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                //Getting values
                oName = Client.getClientName(oID);
                if (API.Equals("-99"))
                {
                    API = " --- ";
                }
                if (UWI.Equals("-99"))
                {
                    UWI = " --- ";
                }
                //Adding Table Row
                iReply.Rows.Add(WellID, oID, oName, wName, lon, lat, API, UWI, spud);
                iReply.AcceptChanges();
                //Adding Well Section Table

                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWPRunTable(Int32 WellID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn runID = iReply.Columns.Add("runID", typeof(Int32));
                DataColumn runName = iReply.Columns.Add("runName", typeof(String));
                DataColumn runInc = iReply.Columns.Add("runInc", typeof(Decimal));
                DataColumn runAzm = iReply.Columns.Add("runAzm", typeof(Decimal));
                DataColumn runTInc = iReply.Columns.Add("runTInc", typeof(Decimal));
                DataColumn runTAzm = iReply.Columns.Add("runTAzm", typeof(Decimal));
                String selData = "SELECT [runID], [runName], [runInc], [runAzm], [runTInc], [runTAzm] FROM [WPRun] WHERE [welID] = @welID";
                Int32 id = -99, rID = -99;
                String rnName = String.Empty;
                Decimal inc = -99.00M, azm = -99.00M, tInc = -99.00M, tAzm = -99.00M;
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        rID = readData.GetInt32(1);
                                        rnName = OPR.getRunName(rID);
                                        inc = readData.GetDecimal(2);
                                        azm = readData.GetDecimal(3);
                                        tInc = readData.GetDecimal(4);
                                        tAzm = readData.GetDecimal(5);
                                        iReply.Rows.Add(id, rnName, inc, azm, tInc, tAzm);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWellSectionName(Int32 wellSectionID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wsID = iReply.Columns.Add("wsID", typeof(Int32));
                DataColumn wsName = iReply.Columns.Add("wsName", typeof(String));
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [rnsID], [rnsName] FROM [dmgRunSection] WHERE [rnsID] = @rnsID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@rnsID", SqlDbType.Int).Value = wellSectionID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Rows.Add(id, name);
                                        iReply.AcceptChanges();
                                    }
                                }
                                else
                                {
                                    iReply.Rows.Add(-99, " --- ");
                                    iReply.AcceptChanges();
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getWPSQC(Int32 RunSectionID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn sqcName = iReply.Columns.Add("sqcName", typeof(String));
                DataColumn sqcDip = iReply.Columns.Add("sqcDip", typeof(Decimal));
                DataColumn sqcBTotal = iReply.Columns.Add("sqcBTotal", typeof(Decimal));
                DataColumn sqcGTotal = iReply.Columns.Add("sqcGTotal", typeof(Decimal));
                String selSQC = "SELECT [sqcID] FROM [SqcToRunSection] WHERE [rnsID] = @rnsID";
                String selData = "SELECT [sqcName], [sqcDip], [sqcBTotal], [sqcGTotal] FROM [SQC] WHERE [sqcID] = @sqcID";
                Int32 sqc = -99;
                String name = String.Empty;
                Decimal dip = -99.0000M, bt = -99.0000M, gt = -99.0000000M;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getSQC = new SqlCommand(selSQC, dataCon);
                        getSQC.Parameters.AddWithValue("@rnsID", SqlDbType.Int).Value = RunSectionID.ToString();
                        using (SqlDataReader readSQC = getSQC.ExecuteReader())
                        {
                            try
                            {
                                if (readSQC.HasRows)
                                {
                                    while (readSQC.Read())
                                    {
                                        sqc = readSQC.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readSQC.Close();
                                readSQC.Dispose();
                            }
                        }
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@sqcID", SqlDbType.Int).Value = sqc.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        name = readData.GetString(0);
                                        dip = readData.GetDecimal(1);
                                        bt = readData.GetDecimal(2);
                                        gt = readData.GetDecimal(3);
                                        iReply.Rows.Add(name, dip, bt, gt);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
        
        public static DataTable getUserInfo(String UserName, String UserDomain)
        {
            try
            {
                DataTable iReply = new DataTable();                
                DataColumn uFN = iReply.Columns.Add("FName", typeof(String));
                DataColumn uLN = iReply.Columns.Add("LName", typeof(String));
                DataColumn uFL = iReply.Columns.Add("FullName", typeof(String));
                DataColumn uDg = iReply.Columns.Add("Designation", typeof(String));
                DataColumn uCl = iReply.Columns.Add("Company", typeof(String));
                String fn = String.Empty, mn = String.Empty, ln = String.Empty, fl = String.Empty, dg = String.Empty, cn = String.Empty;
                Int32 clID = 0;
                Prof UP = Prof.GetUserProfile(UserName);
                fn = UP.FirstName;
                mn = UP.MiddleName;
                ln = UP.LastName;
                fl = String.Format("{0}, {1} {2}", ln, fn, mn);
                dg = UP.designation;
                clID = Convert.ToInt32(UP.clntID);
                cn = Client.getClientName(clID);                
                iReply.Rows.Add(fn, ln, fl, dg, cn);
                iReply.AcceptChanges();
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWPWellProfile(Int32 DatasetID, Int32 RunID, Int32 BHAID, String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn wpqcID = iReply.Columns.Add("wpqcID", typeof(Int32));
                DataColumn srvyRowID = iReply.Columns.Add("srvyRowID", typeof(Int32));
                DataColumn depth = iReply.Columns.Add("depth", typeof(Decimal));
                DataColumn wpGx = iReply.Columns.Add("wpGx", typeof(Decimal));
                DataColumn wpGy = iReply.Columns.Add("wpGy", typeof(Decimal));
                DataColumn wpGz = iReply.Columns.Add("wpGz", typeof(Decimal));
                DataColumn wpBx = iReply.Columns.Add("wpBx", typeof(Decimal));
                DataColumn wpBy = iReply.Columns.Add("wpBy", typeof(Decimal));
                DataColumn wpBz = iReply.Columns.Add("wpBz", typeof(Decimal));
                DataColumn colP = iReply.Columns.Add("colP", typeof(Decimal));
                DataColumn colQ = iReply.Columns.Add("colQ", typeof(Decimal));
                DataColumn colT = iReply.Columns.Add("colT", typeof(Decimal));
                DataColumn colAO = iReply.Columns.Add("colAO", typeof(Decimal));
                DataColumn colAP = iReply.Columns.Add("colAP", typeof(Decimal));
                DataColumn colBA = iReply.Columns.Add("colBA", typeof(Decimal));
                DataColumn colCA = iReply.Columns.Add("colCA", typeof(Decimal));
                DataColumn colCF = iReply.Columns.Add("colCF", typeof(Decimal));
                DataColumn colDD = iReply.Columns.Add("colDD", typeof(Decimal));
                DataColumn colDE = iReply.Columns.Add("colDE", typeof(Decimal));
                DataColumn colDX = iReply.Columns.Add("colDX", typeof(Decimal));
                DataColumn colFY = iReply.Columns.Add("colFY", typeof(Decimal));
                DataColumn colEL = iReply.Columns.Add("colEL", typeof(Decimal));
                DataColumn colEM = iReply.Columns.Add("colEM", typeof(Decimal));
                DataColumn colEN = iReply.Columns.Add("colEN", typeof(String));
                DataColumn clrEN = iReply.Columns.Add("clrEN", typeof(String));
                Int32 id = -99, sID = -99, en = -99;
                Decimal dpth = -99.00M, gx = -99.00000000000M, gy = -99.000000000000M, gz = -99.000000000000M, bx = -99.000000000000M, by = -99.000000000000M, bz = -99.000000000000M;
                Decimal p = -99.0000M, q = -99.0000M, t = -99.0000M, ao = -99.0000M, ap = -99.0000M, ba = -99.0000M, ca = -99.00M, cf = -99.000000M, dd = -99.0000M;
                Decimal de = -99.0000M, dx = -99.00M, fy = -99.00M, el = -99.00M, em = -99.00M;
                String enVal = String.Empty, enText = String.Empty;
                String selData = "SELECT [wpqcID], [srvyRowID], [depth], [wpGx], [wpGy], [wpGz], [wpBx], [wpBy], [wpBz], [colP], [colQ], [colT], [colAO], [colAP], [colBA], [colCA], [colCF], [colDD], [colDE], [colDX], [colFY], [colEL], [colEM], [colEN] FROM [WPQCResults] WHERE [wpdsID] = @wpdsID AND [runID] = @runID AND [bhaID] = @bhaID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wpdsID", SqlDbType.Int).Value = DatasetID.ToString();
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        getData.Parameters.AddWithValue("@bhaID", SqlDbType.Int).Value = BHAID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        sID = readData.GetInt32(1);
                                        dpth = readData.GetDecimal(2);
                                        gx = readData.GetDecimal(3);
                                        gy = readData.GetDecimal(4);
                                        gz = readData.GetDecimal(5);
                                        bx = readData.GetDecimal(6);
                                        by = readData.GetDecimal(7);
                                        bz = readData.GetDecimal(8);
                                        p = readData.GetDecimal(9);
                                        q = readData.GetDecimal(10);
                                        t = readData.GetDecimal(11);
                                        ao = readData.GetDecimal(12);
                                        ap = readData.GetDecimal(13);
                                        ba = readData.GetDecimal(14);
                                        ca = readData.GetDecimal(15);
                                        cf = readData.GetDecimal(16);
                                        dd = readData.GetDecimal(17);
                                        de = readData.GetDecimal(18);
                                        dx = readData.GetDecimal(19);
                                        fy = readData.GetDecimal(20);
                                        el = readData.GetDecimal(21);
                                        em = readData.GetDecimal(22);
                                        en = readData.GetInt32(23);
                                        enText = getBHAAnalysis(en);
                                        if (en.Equals(2) || en.Equals(4) || en.Equals(7))
                                        {
                                            enVal = "Green";
                                        }
                                        else
                                        {
                                            enVal = "Maroon";
                                        }
                                        iReply.Rows.Add(id, sID, dpth, gx, gy, gz, bx, by, bz, p, q, t, ao, ap, ba, ca, cf, dd, de, dx, fy, el, em, enText, enVal);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getSOInfoTable(Int32 ServiceOrderNumber, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn soID = iReply.Columns.Add("soID", typeof(Int32));
                DataColumn soName = iReply.Columns.Add("soName", typeof(String));
                DataColumn soStat = iReply.Columns.Add("soStat", typeof(String));
                DataColumn soAccel = iReply.Columns.Add("soAccel", typeof(String));
                DataColumn soMag = iReply.Columns.Add("soMag", typeof(String));
                DataColumn soLen = iReply.Columns.Add("soLen", typeof(String));
                DataColumn soMMDL = iReply.Columns.Add("soMMDL", typeof(String));
                DataColumn soDat = iReply.Columns.Add("soCord", typeof(String));
                DataColumn soProj = iReply.Columns.Add("soProj", typeof(String));                
                Int32 acelU = -99, magU = -99, lenU = -99, mmdl = -99, cord = -99, proj = -99, stat = -99;
                String name = String.Empty, statVal = String.Empty, acelVal = String.Empty, magVal = String.Empty, lenVal = String.Empty, mmdlVal = String.Empty, cordVal = String.Empty, projVal = String.Empty;
                String selData = "SELECT ( SELECT [jobName] FROM [Job] WHERE [jobID] = @jobID) AS jbName, ( SELECT [dmgstID] FROM [Job] WHERE [jobID] = @jobID) AS jbStat, ( SELECT [acuID] FROM [JobDetail] WHERE [jobID] = @jobID) AS jbAcel, ( SELECT [muID] FROM [JobDetail] WHERE [jobID] = @jobID) AS jbMag, ( SELECT [luID] FROM [JobDetail] WHERE [jobID] = @jobID) AS jbLen, ( SELECT [mmdlID] FROM [JobDetail] WHERE [jobID] = @jobID) AS jbMMDL, ( SELECT [cordID] FROM [JobDetail] WHERE [jobID] = @jobID) AS jbDat, ( SELECT [projID] FROM [JobDetail] WHERE [jobID] = @jobID) AS jbProj";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@jobID", SqlDbType.Int).Value = ServiceOrderNumber.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        name = readData.GetString(0);
                                        stat = readData.GetInt32(1);
                                        statVal = DMG.getStatusValue(stat);
                                        acelU = readData.GetInt32(2);
                                        acelVal = AST.getAccelUnitName(acelU);
                                        magU = readData.GetInt32(3);
                                        magVal = AST.getMagUnitName(magU);
                                        lenU = readData.GetInt32(4);
                                        lenVal = AST.getLengthUnitName(lenU);
                                        mmdl = readData.GetInt32(5);
                                        mmdlVal = AST.getMagModelName(mmdl);
                                        cord = readData.GetInt32(6);
                                        cordVal = AST.getCordName(cord);
                                        proj = readData.GetInt32(7);
                                        projVal = AST.getProjectionName(proj);
                                        iReply.Rows.Add(ServiceOrderNumber, name, statVal, acelVal, magVal, lenVal, mmdlVal, cordVal, projVal);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getPadInfoTable(Int32 WellPadID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn soID = iReply.Columns.Add("padID", typeof(Int32));
                DataColumn soName = iReply.Columns.Add("padName", typeof(String));
                DataColumn soStat = iReply.Columns.Add("padOpr", typeof(String));
                DataColumn soAccel = iReply.Columns.Add("padCounty", typeof(String));
                DataColumn soMag = iReply.Columns.Add("padField", typeof(String));
                Int32 opID = -99, county = -99, field = -99;
                String name = String.Empty, opName = String.Empty, countyVal = String.Empty, fieldVal = String.Empty;
                String selData = "SELECT [welPadName], [optrID], [cntyID], [fldID] FROM [WellPad] WHERE [wpdID] = @wpdID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wpdID", SqlDbType.Int).Value = WellPadID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        name = readData.GetString(0);
                                        opID = readData.GetInt32(1);
                                        opName = Client.getClientName(opID);
                                        county = readData.GetInt32(2);
                                        countyVal = DMG.getCountyName(county);
                                        field = readData.GetInt32(3);
                                        fieldVal = DMG.getFieldName(field);
                                        iReply.Rows.Add(WellPadID, name, opName, countyVal, fieldVal);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWellInfoTable(Int32 WellID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn welID = iReply.Columns.Add("welID", typeof(Int32));
                DataColumn welName = iReply.Columns.Add("welName", typeof(String));
                DataColumn welClient = iReply.Columns.Add("welClient", typeof(String));
                DataColumn welLong = iReply.Columns.Add("welLongi", typeof(String));
                DataColumn welLat = iReply.Columns.Add("welLati", typeof(String));
                DataColumn welAPI = iReply.Columns.Add("welAPI", typeof(String));
                DataColumn welUWI = iReply.Columns.Add("welUWI", typeof(String));
                DataColumn welPermit = iReply.Columns.Add("welPermit", typeof(String));
                DataColumn welSpud = iReply.Columns.Add("welSpud", typeof(String));
                Int32 client = -99;
                DateTime spud = DateTime.Now;
                Decimal longitude = -99.000000M, latitude = -99.000000M;
                String name = String.Empty, clientName = String.Empty, APIVal = String.Empty, UWIVal = String.Empty, permit = String.Empty, spudDate = String.Empty;
                String selData = "SELECT [welName], [clntID], [welAPI], [welUWI], [welPermit], [welSpudDate], [welLongitude], [welLatitude] FROM [Well] WHERE [welID] = @welID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        name = readData.GetString(0);
                                        client = readData.GetInt32(1);
                                        clientName = Client.getClientName(client);
                                        APIVal = readData.GetString(2);
                                        if(String.IsNullOrEmpty(APIVal)){ APIVal = "---"; };
                                        UWIVal = readData.GetString(3);
                                        if(String.IsNullOrEmpty(UWIVal)){ UWIVal = "---"; };
                                        permit = readData.GetString(4);
                                        spud = readData.GetDateTime(5);
                                        spudDate = spud.ToString("MMM dd, yyyy");
                                        longitude = readData.GetDecimal(6);
                                        latitude = readData.GetDecimal(7);
                                        iReply.Rows.Add(WellID, name, clientName, longitude, latitude, APIVal, UWIVal, permit, spudDate);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getWellRefMagsTable(Int32 WellID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn rmID = iReply.Columns.Add("rfmgID", typeof(Int32));
                DataColumn ifr = iReply.Columns.Add("ifrvalID", typeof(String));
                DataColumn rmMDec = iReply.Columns.Add("mdec", typeof(String));
                DataColumn rmCID = iReply.Columns.Add("dip", typeof(String));
                DataColumn rmDip = iReply.Columns.Add("bTotal", typeof(String));
                DataColumn rmBT = iReply.Columns.Add("gTotal", typeof(String));
                DataColumn rmGT = iReply.Columns.Add("gconv", typeof(String));
                DataColumn rmGConv = iReply.Columns.Add("jcID", typeof(String));
                List<String> RefMags = new List<String>();
                Int32 refID = -99, ifrVal = -99, refCID = -99, gcOID = -99, muID = -99, acuID = -99, jcID = -99;
                Decimal mdec = -99.00M, dip = -99.0000M, bt = -99.0000M, gt = -99.000000M, gconv = -99.00M;
                String ifrName = String.Empty, refCIDVal = String.Empty, refGCOIDVal = String.Empty, irefCID = String.Empty, magName = String.Empty, aclName = String.Empty, converg = String.Empty;
                String mdecVal = String.Empty, btValue = String.Empty, gtValue = String.Empty, gconvValue = String.Empty;
                String selData = "SELECT [rfmgID], [ifrvalID], [mdec], [cID], [dip], [bTotal], [muID], [gTotal], [acuID], [gconv], [gcoID], [jcID] FROM [RefMags] WHERE [welID] = @welID AND [dmgstID] = 1";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        refID = readData.GetInt32(0);
                                        ifrVal = readData.GetInt32(1);
                                        ifrName = AST.getIFRValue(ifrVal);
                                        mdec = readData.GetDecimal(2);
                                        refCID = readData.GetInt32(3);
                                        refCIDVal = DMG.getCardinalName(refCID);
                                        mdecVal = String.Format("{0} - {1}", mdec.ToString("0.00"), refCIDVal);
                                        dip = readData.GetDecimal(4);
                                        bt = readData.GetDecimal(5);
                                        muID = readData.GetInt32(6);
                                        magName = AST.getMagUnitName(muID);
                                        btValue = String.Format("{0} - {1}", bt.ToString("0"), magName);
                                        gt = readData.GetDecimal(7);
                                        acuID = readData.GetInt32(8);
                                        aclName = AST.getAccelUnitName(acuID);
                                        gtValue = String.Format("{0} - {1}", gt.ToString("0.00000"), aclName);
                                        gconv = readData.GetDecimal(9);
                                        gcOID = readData.GetInt32(10);
                                        refGCOIDVal = DMG.getGCOrientationName(gcOID);
                                        gconvValue = String.Format("{0} - {1}", gconv.ToString("0.00"), refGCOIDVal);
                                        jcID = readData.GetInt32(11);
                                        converg = AST.getConvergenceName(jcID);
                                        iReply.Rows.Add(refID, ifrName, mdecVal, dip, btValue, gtValue, gconvValue, converg);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //public static List<String> getWellIFRRefMagsList(Int32 RefMagsID, String ClientDBString)
        //{
        //    try
        //    {
        //        List<String> iReply = new List<String>();
        //        Int32 refICID = -99;
        //        Decimal imdec = -99.00M, idip = -99.0000M, iBT = -99.00M, iGT = -99.000000M;
        //        String irefCID = String.Empty;
        //        String selData = "SELECT [imdec], [cID], [idip], [ibTotal], [igTotal] FROM [iRefMags] WHERE [rfmgID] = @rfmgID";
        //        using (SqlConnection dataCon = new SqlConnection(ClientDBString))
        //        {
        //            try
        //            {
        //                dataCon.Open();
        //                SqlCommand getData = new SqlCommand(selData, dataCon);
        //                getData.Parameters.AddWithValue("@rfmgID", SqlDbType.Int).Value = RefMagsID.ToString();
        //                using (SqlDataReader readData = getData.ExecuteReader())
        //                {
        //                    try
        //                    {
        //                        if (readData.HasRows)
        //                        {
        //                            while (readData.Read())
        //                            {
        //                                imdec = readData.GetDecimal(0);
        //                                iReply.Add(Convert.ToString(imdec));
        //                                refICID = readData.GetInt32(1);                                        
        //                                irefCID = DMG.getCardinalName(refICID);
        //                                iReply.Add(irefCID);
        //                                idip = readData.GetDecimal(2);
        //                                iReply.Add(Convert.ToString(idip));
        //                                iBT = readData.GetDecimal(3);
        //                                iReply.Add(Convert.ToString(iBT));
        //                                iGT = readData.GetDecimal(4);
        //                                iReply.Add(Convert.ToString(iGT));
        //                            }
        //                        }
        //                    }
        //                    catch (Exception ex)
        //                    { throw new System.Exception(ex.ToString()); }
        //                    finally
        //                    {
        //                        readData.Close();
        //                        readData.Dispose();
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            { throw new System.Exception(ex.ToString()); }
        //            finally
        //            {
        //                dataCon.Close();
        //                dataCon.Dispose();
        //            }
        //        }
        //        return iReply;
        //    }
        //    catch (Exception ex)
        //    { throw new System.Exception(ex.ToString()); }
        //}

        public static System.Data.DataTable getWellSectionTable(Int32 WellID, Int32 WellSectionID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn secID = iReply.Columns.Add("secID", typeof(Int32));
                DataColumn secName = iReply.Columns.Add("secName", typeof(String));
                DataColumn secRCount = iReply.Columns.Add("secRCount", typeof(Int32));
                Int32 id = -99, count = -99;
                String name = String.Empty;
                String selData = "SELECT [wsName] FROM [WellSection] WHERE [wsID] = @wsID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@wsID", SqlDbType.Int).Value = WellSectionID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        name = readData.GetString(0);
                                        count = getRunCountForSection(WellID, WellSectionID, ClientDBString);
                                        iReply.Rows.Add(WellSectionID, name, count);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getRunCountForSection(Int32 WellID, Int32 WellSectionID, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;                
                String selData = "SELECT count(runID) FROM [Run] WHERE [welID] = @welID AND [wswID] = @wswID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        getData.Parameters.AddWithValue("@wswID", SqlDbType.Int).Value = WellSectionID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getRunTable(Int32 RunID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn runID = iReply.Columns.Add("runID", typeof(Int32));
                DataColumn runName = iReply.Columns.Add("runName", typeof(String));
                DataColumn runTInc = iReply.Columns.Add("runTInc", typeof(Int32));
                DataColumn runTAzm = iReply.Columns.Add("runTAzm", typeof(Int32));
                DataColumn runStat = iReply.Columns.Add("runStat", typeof(String));
                DataColumn runSQCName = iReply.Columns.Add("runSQCName", typeof(String));
                DataColumn runSQCDip = iReply.Columns.Add("runSQCDip", typeof(Decimal));
                DataColumn runSQCBT = iReply.Columns.Add("runSQCBT", typeof(Decimal));
                DataColumn runSQCGT = iReply.Columns.Add("runSQCGT", typeof(Decimal));
                DataColumn mtrSize = iReply.Columns.Add("mtrSize", typeof(String));
                DataColumn dsclrSize = iReply.Columns.Add("dsclrSize", typeof(String));
                DataColumn bhaLength = iReply.Columns.Add("bhaLength", typeof(String));
                DataColumn spaceAbove = iReply.Columns.Add("spaceAbove", typeof(String));
                DataColumn spaceBelow = iReply.Columns.Add("spaceBelow", typeof(String));
                DataColumn status = iReply.Columns.Add("status", typeof(String));
                List<String> sqcList = new List<String>(), bhaList = new List<String>();
                Int32 id = -99, stat = -99, sqcID = -99;
                String name = String.Empty, rstatus = String.Empty, sqcName = String.Empty;
                Decimal Inc = -99.0000M, Azm = -99.0000M, sqcDip = -99.00M, sqcBT = -99.00M, sqcGT = -99.000000M;
                Decimal mSize = -99.00M, dscSize = -99.00M, mdbLen = -99.00M, nmspAbv = -99.00M, nmspBlw = -99.00M;
                String motorSize = String.Empty, collarSize = String.Empty, bhaLen = String.Empty, abvSpace = String.Empty, blwSpace = String.Empty;
                String mSizeUN = String.Empty, dscSizeUN = String.Empty, mdbLenNM = String.Empty, spAbvNM = String.Empty, spBlwNM = String.Empty, bhaStNm = String.Empty, mgNm = String.Empty, topUNM = String.Empty, dsgNm = String.Empty, btmUNM = String.Empty;
                String selData = "SELECT [runName], [runTInc], [runTAzm], [dmgstID], [sqcID] FROM [Run] WHERE [runID] = @runID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@runID", SqlDbType.Int).Value = RunID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = AST.getRunName(id);
                                        Inc = readData.GetDecimal(1);
                                        Azm = readData.GetDecimal(2);
                                        stat = readData.GetInt32(3);
                                        rstatus = DMG.getStatusValue(stat);
                                        sqcID = readData.GetInt32(4);
                                        sqcList = getSQCListForRun(sqcID, ClientDBString);
                                        sqcName = Convert.ToString(sqcList[0]);
                                        sqcDip = Convert.ToDecimal(sqcList[1]);
                                        sqcBT = Convert.ToDecimal(sqcList[2]);
                                        sqcGT = Convert.ToDecimal(sqcList[3]);
                                        bhaList = getBHAForRunList(RunID, ClientDBString);
                                        mSize = Convert.ToDecimal(bhaList[1]);
                                        mSizeUN = Convert.ToString(bhaList[2]);
                                        motorSize = String.Format("{0} - {1}", mSize.ToString("0.00"), mSizeUN);
                                        dscSize = Convert.ToDecimal(bhaList[3]);
                                        dscSizeUN = Convert.ToString(bhaList[4]);
                                        collarSize = String.Format("{0} - {1}", dscSize.ToString("0.00"), dscSizeUN);
                                        mdbLen = Convert.ToDecimal(bhaList[5]);
                                        mdbLenNM = Convert.ToString(bhaList[6]);
                                        bhaLen = String.Format("{0} - {1}", mdbLen.ToString("0.00"), mdbLenNM);
                                        nmspAbv = Convert.ToDecimal(bhaList[7]);
                                        spAbvNM = Convert.ToString(bhaList[8]);
                                        abvSpace = String.Format("{0} - {1}", nmspAbv.ToString("0.00"), spAbvNM);
                                        nmspBlw = Convert.ToDecimal(bhaList[9]);
                                        spBlwNM = Convert.ToString(bhaList[10]);
                                        blwSpace = String.Format("{0} - {1}", nmspBlw.ToString("0.00"), spBlwNM);
                                        bhaStNm = Convert.ToString(bhaList[11]);
                                        iReply.Rows.Add(RunID, name, Inc, Azm, rstatus, sqcName, sqcDip, sqcBT, sqcGT, motorSize, collarSize, bhaLen, abvSpace, blwSpace, bhaStNm);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<String> getSQCListForRun(Int32 SQCID, String ClientDBString)
        {
            try
            {
                List<String> iReply = new List<String>();
                String name = String.Empty, sqcDip = String.Empty, sqcBT = String.Empty, sqcGT = String.Empty;
                Decimal dip = -99.0000M, bt = -99.0000M, gt = -99.000000M;
                String selData = "SELECT [sqcName], [sqcDip], [sqcBTotal], [sqcGTotal] FROM [SQC] WHERE [sqcID] = @sqcID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@sqcID", SqlDbType.Int).Value = SQCID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        name = readData.GetString(0);
                                        iReply.Add(name);
                                        dip = readData.GetDecimal(1);
                                        sqcDip = Convert.ToString(dip);
                                        iReply.Add(sqcDip);
                                        bt = readData.GetDecimal(2);
                                        sqcBT = Convert.ToString(bt);
                                        iReply.Add(sqcBT);
                                        gt = readData.GetDecimal(3);
                                        sqcGT = Convert.ToString(gt);
                                        iReply.Add(sqcGT);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}