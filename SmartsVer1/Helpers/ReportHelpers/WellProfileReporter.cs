﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Net.Mail;
using System.Collections;
using System.Web.Security;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using Microsoft.SqlServer.Types;
using System.Collections.Generic;
using AC = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;

namespace SmartsVer1.Helpers.ReportHelpers
{
    public class WellProfileReporter
    {
        static void Main()
        { }

        public static DataTable getUserInfo(Int32 ClientID, String LoggedInUserName)
        {
            try
            {
                DataTable iReply = new DataTable();
                String FName = null;
                String LName = null;
                String Desig = null;
                String UserCoName = null;

                AC usrP = AC.GetUserProfile(LoggedInUserName);
                FName = usrP.FirstName;
                LName = usrP.LastName;
                Desig = usrP.designation;
                UserCoName = CLNT.getClientName(ClientID);
                DataColumn uID = iReply.Columns.Add("uID", typeof(Int32));
                DataColumn uFN = iReply.Columns.Add("FName", typeof(String));
                DataColumn uLN = iReply.Columns.Add("LName", typeof(String));
                DataColumn uDg = iReply.Columns.Add("Desig", typeof(String));
                DataColumn uCl = iReply.Columns.Add("Client", typeof(String));
                iReply.PrimaryKey = new DataColumn[] { uID };
                iReply.Rows.Add(1, FName, LName, Desig, UserCoName);
                iReply.AcceptChanges();

                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static DataTable getDisclaimer()
        {
            try
            {
                DataTable iReply = new DataTable();

                Int32 dscID = -99;
                String dscVal = String.Empty;
                String disclCommand = "SELECT dscID, dscValue FROM dmgDisclaimer WHERE dsctID = @dsctID";

                using (SqlConnection dmgCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dmgCon.Open();
                        SqlCommand getDscValCommand = new SqlCommand(disclCommand, dmgCon);
                        //1 = Report Disclaimer
                        getDscValCommand.Parameters.AddWithValue("@dsctID", SqlDbType.Int).Value = 1;
                        //Reading Disclaimer Value
                        using (SqlDataReader readDisclaimer = getDscValCommand.ExecuteReader())
                        {
                            try
                            {
                                if (readDisclaimer.HasRows)
                                {
                                    while (readDisclaimer.Read())
                                    {
                                        dscID = readDisclaimer.GetInt32(0);
                                        dscVal = readDisclaimer.GetString(1);

                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readDisclaimer.Close();
                                readDisclaimer.Dispose();
                            }
                        }
                        iReply.Load(getDscValCommand.ExecuteReader());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dmgCon.Close();
                        dmgCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}