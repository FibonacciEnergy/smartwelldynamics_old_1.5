﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Security;
using System.Web.Security;
using System.Configuration;

namespace SmartsVer1.Helpers.Literals
{
    public class MenuItems
    {
        void main()
        {}

        public static String getStandardMenu()
        {
            try
            {
                String iReply = String.Empty;
                iReply = "default"; 
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getClientMenu(String UserRole)
        {
            try
            {
                String iReply = String.Empty;
                switch (UserRole)
                {
                    case ("Director"):
                        {
                            iReply = getClientDirMenu();
                            break; 
                        }
                    case ("Coordinator"): 
                        {
                            iReply = getClientCoordMenu();
                            break; 
                        }
                    case ("Fieldhand"): { break; }
                    case ("LabTech"): { break; }
                    case ("Reviewer"): { break; }
                    case ("Support"): { break; }
                    default: { break; }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getOwnerMenu(String UserRole)
        {
            try
            {
                String iReply = String.Empty;
                switch (UserRole)
                {
                    case ("feDirector"):{break;}
                    case ("feCoordinator"):{break;}
                    case ("feSupport"):{break;}
                    default: { break; }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getClientCoordMenu()
        {
            try
            {
                String iReply = String.Empty;
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getClientDirMenu()
        {
            try
            {
                String iReply = String.Empty;
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getClientSupportMenu()
        {
            try
            {
                String iReply = String.Empty;
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getClientFieldhandMenu()
        {
            try
            {
                String iReply = String.Empty;
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getClientRwrMenu()
        {
            try
            {
                String iReply = String.Empty;
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getOwnerCoordMenu()
        {
            try
            {
                String iReply = String.Empty;                
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getOwnerDirMenu()
        {
            try
            {
                String iReply = String.Empty;
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getOwnerSupportMenu()
        {
            try
            {
                String iReply = String.Empty;
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        internal static string getPageMenu(Int32 ClientID, String LoginName)
        {
            try
            {
                String iReply = String.Empty;
                if (ClientID.Equals(1)) //FE
                {
                    if (HttpContext.Current.User.IsInRole("feCoordinator"))
                    {
                        iReply = "FESPServices";
                    }
                    else if (HttpContext.Current.User.IsInRole("feDirector"))
                    {
                        iReply = "FEDRServices";
                    }
                    else if (HttpContext.Current.User.IsInRole("feSupport"))
                    {
                        iReply = "FEAdmin";
                    }
                    else
                    {
                        iReply = "default";
                    }
                }
                else //Client
                {
                    if (HttpContext.Current.User.IsInRole("Coordinator"))
                    {
                        iReply = "FEClientCRD";
                    }
                    else if (HttpContext.Current.User.IsInRole("Director"))
                    {
                        iReply = "FEClientDR";
                    }
                    else if (HttpContext.Current.User.IsInRole("Fieldhand"))
                    {
                        iReply = "FEClientFLD";
                    }
                    else if (HttpContext.Current.User.IsInRole("LabTech"))
                    {
                        iReply = "FEClientLAB";
                    }
                    else if (HttpContext.Current.User.IsInRole("feSupport"))
                    {
                        iReply = "FEAdmin";
                    }
                    else if (HttpContext.Current.User.IsInRole("Reviewer"))
                    {
                        iReply = "FEClientRWR";
                    }
                    else
                    {
                        iReply = "default";
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}