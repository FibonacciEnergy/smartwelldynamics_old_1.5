﻿using System;
using System.Web;

namespace SmartsVer1.Helpers.Literals
{
    public class MenuLoader
    {
    
      internal static string getPageMenu(Int32 ClientID, String LoginName)
        {
            try
            {
                String iReply = String.Empty;
                if (ClientID.Equals(1))
                {
                    if (HttpContext.Current.User.IsInRole("feCoordinator"))
                    {
                        iReply = getFEDirMenu();
                    }
                    else if (HttpContext.Current.User.IsInRole("feDirector"))
                    {
                        iReply = getFEDirMenu();
                    }
                    else if (HttpContext.Current.User.IsInRole("feSupport"))
                    {
                        iReply = getFEAdminMenu();
                    }
                    else
                    {
                        iReply = getDefaultMenu();
                    }
                }
                else
                {
                    if (HttpContext.Current.User.IsInRole("Coordinator"))
                    {
                        iReply = getCordMenu();
                    }
                    else if (HttpContext.Current.User.IsInRole("Director"))
                    {
                        iReply = getDirectorMenu();
                    }
                    else if (HttpContext.Current.User.IsInRole("Fieldhand"))
                    {
                        iReply = getFieldhandMenu();
                    }
                    else if (HttpContext.Current.User.IsInRole("LabTech"))
                    {
                        iReply = getFieldhandMenu();
                    }
                    else if (HttpContext.Current.User.IsInRole("feSupport"))
                    {
                        iReply = getFEAdminMenu();
                    }
                    else if (HttpContext.Current.User.IsInRole("Reviewer"))
                    {
                        iReply = getFERwrMenu();
                    }
                    else if (HttpContext.Current.User.IsInRole("MWD_1"))
                    {
                        iReply = String.Empty;
                    }
                    else
                    {
                        iReply = getDefaultMenu();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

      public static String getDirectorMenu()
        {
          try
          {
            String path = HttpContext.Current.Server.MapPath(@"~\App_Data\FEClientDR_Menu.txt");
            String value = System.IO.File.ReadAllText(path);
            return value;
          }
          catch (Exception ex)
          { throw new System.Exception(ex.ToString()); }
        }

      public static String getCordMenu()
      {
          try
          {
              String path = HttpContext.Current.Server.MapPath(@"~\App_Data\FEClientCoord_Menu.txt");
              String value = System.IO.File.ReadAllText(path);
              return value;
          }
          catch (Exception ex)
          { throw new System.Exception(ex.ToString()); }
      }

      public static String getFieldhandMenu()
      {
          try
          {
              String path = HttpContext.Current.Server.MapPath(@"~\App_Data\FEClientFieldHand_Menu.txt");
              String value = System.IO.File.ReadAllText(path);
              return value;
          }
          catch (Exception ex)
          { throw new System.Exception(ex.ToString()); }
      }

      public static String getDefaultMenu()
      {
          try
          {
              String path = HttpContext.Current.Server.MapPath(@"~\App_Data\default_Menu.txt");
              String value = System.IO.File.ReadAllText(path);
              return value;
          }
          catch (Exception ex)
          { throw new System.Exception(ex.ToString()); }
      }

      public static String getFEAdminMenu()
      {
          try
          {
              String path = HttpContext.Current.Server.MapPath(@"~\App_Data\FEAdmin_Menu.txt");
              String value = System.IO.File.ReadAllText(path);
              return value;
          }
          catch (Exception ex)
          { throw new System.Exception(ex.ToString()); }
      }

      public static String getFEDirMenu()
      {
          try
          {
              String path = HttpContext.Current.Server.MapPath(@"~\App_Data\FEDRServices.txt");
              String value = System.IO.File.ReadAllText(path);
              return value;
          }
          catch (Exception ex)
          { throw new System.Exception(ex.ToString()); }
      }

      public static String getFERwrMenu()
      {
          try
          {
              String path = HttpContext.Current.Server.MapPath(@"~\App_Data\FEClientRWR_Menu.txt");
              String value = System.IO.File.ReadAllText(path);
              return value;
          }
          catch (Exception ex)
          { throw new System.Exception(ex.ToString()); }
      }
    }
}