﻿using System;
using System.Text;
using CTRT = SmartsVer1.Helpers.AccountHelpers.CreateContract;

namespace SmartsVer1.Helpers.Literals
{
    public class DataTables
    {        
        void main()
        {}        

        public static String getClientContractsTable(Int32 ClientID)
        {
            try
            {
                String iReply = String.Empty;
                String ctrtData = CTRT.getClientContractJSONTable(ClientID);
                StringBuilder strData = new StringBuilder();
                strData.Append("<script type='text/javascript'>");
                strData.Append("$(document).ready( function Contracts(){");
                strData.Append("$('#tblContracts').DataTable");
                strData.Append("({");
                strData.Append("destroy: true,");
                strData.Append("data: " + ctrtData + ",");
                strData.Append("columns:[");
                strData.Append("{ className: 'hidden', title: 'ctrtID', data: 'ctrtID' },");
                strData.Append("{ className: 'text-left', title: 'Type', data: 'ctID' },");
                strData.Append("{ className: 'text-left', title: 'Name', data: 'ctrtName', 'orderable': false, 'searching': false }, ");
                strData.Append("{ className: 'text-right', title: 'Status', data: 'cstatID', 'orderable': true, 'searching': true },");
                strData.Append("{ className: 'text-right', title: 'Start Date', data: 'ctrtStart' },");
                strData.Append("{ className: 'text-right', title: 'End Date', data: 'ctrtEnd' },");
                strData.Append("{ className: 'text-right', title: 'Last Update', data: 'uTime' }]");
                strData.Append("});");
                strData.Append("});");
                strData.Append("</script>");
                iReply = strData.ToString();
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}