﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace SmartsVer1.Helpers.AccountHelpers
{
    public class Tracker
    {

        void main()
        {}

        public static List<String> checkContractStatus(Int32 ClientID)
        {
            try
            {
                List<String> iReply = new List<String>();
                Int32 contractID = -99;                
                contractID = getContractID(ClientID);
                iReply = getContractInfo(contractID);                
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        protected static Int32 getContractID(Int32 ClientID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT [ctrtID] FROM [ClientInContract] WHERE [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    iReply = -1;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<String> getContractInfo(Int32 ContractID)
        {
            try
            {
                List<String> iReply = new List<String>();
                if (ContractID.Equals(-1))
                {
                    iReply.Add("No Contract Found");
                    iReply.Add(Convert.ToString(DateTime.Now));
                    iReply.Add(Convert.ToString(DateTime.Now));
                }
                else
                {
                    String selData = "SELECT [ctrtName], [ctrtStart], [ctrtEnd] FROM [Contract] WHERE [ctrtID] = @ctrtID";
                    String name = String.Empty;
                    DateTime sD = DateTime.Now, eD = DateTime.Now;
                    using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                    {
                        try
                        {
                            dataCon.Open();
                            SqlCommand getData = new SqlCommand(selData, dataCon);
                            getData.Parameters.AddWithValue("@ctrtID", SqlDbType.Int).Value = ContractID.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            name = readData.GetString(0);
                                            iReply.Add(name);
                                            sD = readData.GetDateTime(1);
                                            iReply.Add(Convert.ToString(sD));
                                            eD = readData.GetDateTime(2);
                                            iReply.Add(Convert.ToString(eD));
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            dataCon.Close();
                            dataCon.Dispose();
                        }
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String sessionCheck()
        {
            try
            {
                StringBuilder iReply = new StringBuilder();                
                iReply.Append("var inactivityTime = function () {");
                iReply.Append("var t;");
                iReply.Append("window.onload = resetTimer;");
                // DOM Events
                iReply.Append("document.onmousemove = resetTimer;");
                iReply.Append("document.onkeypress = resetTimer;");
                iReply.Append("function logout() {");
                iReply.Append("alert('You are now logged out.')");
                iReply.Append("location.href = '../../Account/SMARTAccess.aspx'");
                iReply.Append("}");
                iReply.Append("function resetTimer() {");
                iReply.Append("clearTimeout(t);");
                iReply.Append("t = setTimeout(logout, 3000)");
                // 1000 milisec = 1 sec
                iReply.Append("}");
                iReply.Append("};");
                return iReply.ToString();
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}