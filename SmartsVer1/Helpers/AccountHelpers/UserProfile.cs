﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.Profile;

namespace SmartsVer1.Helpers.AccountHelpers
{
    public class UserProfile : ProfileBase
    {
        public static UserProfile GetUserProfile(String username)
        {
            return Create(username) as UserProfile;
        }
        public static UserProfile GetUserProfile()
        {
            return Create(Membership.GetUser().UserName) as UserProfile;
        }

        [SettingsAllowAnonymous(false)]
        public String FirstName
        {
            get { return base["FirstName"] as String; }
            set { base["FirstName"] = value; }
        }

        [SettingsAllowAnonymous(false)]
        public String MiddleName
        {
            get { return base["MiddleName"] as String; }
            set { base["MiddleName"] = value; }
        }

        [SettingsAllowAnonymous(false)]
        public String LastName
        {
            get { return base["LastName"] as String; }
            set { base["LastName"] = value; }
        }

        [SettingsAllowAnonymous(false)]
        public String upPhone
        {
            get { return base["upPhone"] as String; }
            set { base["upPhone"] = value; }
        }

        [SettingsAllowAnonymous(false)]
        public String upPhoneType
        {
            get { return base["upPhoneType"] as String; }
            set { base["upPhoneType"] = value; }
        }

        [SettingsAllowAnonymous(false)]
        public String dmgID
        {
            get { return base["dmgID"] as String; }
            set { base["dmgID"] = value; }
        }

        [SettingsAllowAnonymous(false)]
        public String UserId
        {
            get { return base["UserId"] as String; }
            set { base["UserId"] = value; }
        }

        [SettingsAllowAnonymous(false)]
        public String clntID
        {
            get { return base["clntID"] as String; }
            set { base["clntID"] = value; }
        }

        [SettingsAllowAnonymous(false)]
        public String ctypeID
        {
            get { return base["ctypeID"] as String; }
            set { base["ctypeID"] = value; }
        }
        [SettingsAllowAnonymous(false)]
        public String st1
        {
            get { return base["st1"] as String; }
            set { base["st1"] = value; }
        }
        [SettingsAllowAnonymous(false)]
        public String st2
        {
            get { return base["st2"] as String; }
            set { base["st2"] = value; }
        }
        [SettingsAllowAnonymous(false)]
        public String st3
        {
            get { return base["st3"] as String; }
            set { base["st3"] = value; }
        }
        [SettingsAllowAnonymous(false)]
        public String zip
        {
            get { return base["zip"] as String; }
            set { base["zip"] = value; }
        }
        [SettingsAllowAnonymous(false)]
        public String city
        {
            get { return base["city"] as String; }
            set { base["city"] = value; }
        }
        [SettingsAllowAnonymous(false)]
        public String county
        {
            get { return base["county"] as String; }
            set { base["county"] = value; }
        }
        [SettingsAllowAnonymous(false)]
        public String state
        {
            get { return base["state"] as String; }
            set { base["state"] = value; }
        }
        [SettingsAllowAnonymous(false)]
        public String country
        {
            get { return base["country"] as String; }
            set { base["country"] = value; }
        }
        [SettingsAllowAnonymous(false)]
        public String sreg
        {
            get { return base["sreg"] as String; }
            set { base["sreg"] = value; }
        }
        [SettingsAllowAnonymous(false)]
        public String reg
        {
            get { return base["reg"] as String; }
            set { base["reg"] = value; }
        }
        [SettingsAllowAnonymous(false)]
        public String addType
        {
            get { return base["addType"] as String; }
            set { base["addType"] = value; }
        }
        [SettingsAllowAnonymous(false)]
        public String designation
        {
            get { return base["designation"] as String; }
            set { base["designation"] = value; }
        }
        [SettingsAllowAnonymous(false)]
        public String Latitude
        {
            get { return base["Latitude"] as String; }
            set { base["Latitude"] = value; }
        }
        [SettingsAllowAnonymous(false)]
        public String Longitude
        {
            get { return base["Longitude"] as String; }
            set { base["Longitude"] = value; }
        }
    }
}