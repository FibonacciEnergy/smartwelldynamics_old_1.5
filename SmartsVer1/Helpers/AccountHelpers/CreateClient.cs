﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Web.Security;
using System.Collections;
using System.Web.Configuration;
using Microsoft.SqlServer.Types;
//using Google.DataTable.Net.Wrapper;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using AST = SmartsVer1.Helpers.QCHelpers.AddAssets;
using SMO = Microsoft.SqlServer.Management.Smo;
using CON = Microsoft.SqlServer.Management.Common.ServerConnection;
using CTRT = SmartsVer1.Helpers.AccountHelpers.CreateContract;
using SL = SmartsVer1.Helpers.SalesHelpers.Sales;
using NJ = Newtonsoft.Json;
using MAP = SmartsVer1.Helpers.ChartHelpers.MapBox;
using AF = SmartsVer1.Helpers.ChartHelpers.AddressFeatures;

namespace SmartsVer1.Helpers.AccountHelpers
{
/// <summary>
/// A class that helps create system client company account, admin account for the client
/// and cients folders in the application
/// </summary>
        
    public class CreateClient
    {
        private static readonly String appID = WebConfigurationManager.AppSettings["appID"];
        const Int32 SystemClientTypeID = 1; //To get a System Client's list
        
        static void Main()
        { }

        public static Int32 clientCreate(String clientName, String clientTaxReg, String clientNotes, String clientRealm)
        {
            try
            {
                String ccName = null, ccNameLower = null, coDomain = null;
                String application = appID;
                Int32 oType = 1; //Client Account Type
                Int32 ccFold = 0, ccAdmin = 0, result = 0, nclID = 0;

                TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                ccName = ti.ToTitleCase(clientName);
                ccNameLower = ti.ToLower(clientName);
                coDomain = ti.ToLower(clientRealm);
                String insertClient = "INSERT INTO [Client] ([clntName], [clntNameLower], [clntTaxReg], [clntNotes], [clntRealm], [ApplicationID], [ctypID], [cTime], [uTime]) VALUES (@ccName, @ccNameLower, @clientTaxReg, @clientNotes, @clientRealm, @application, @oType, @crTime, @updTime); SELECT SCOPE_IDENTITY();";
                using (SqlConnection clntConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        clntConn.Open();

                        using (SqlCommand clntCmd = new SqlCommand(insertClient, clntConn))
                        {
                            try
                            {
                                clntCmd.Parameters.AddWithValue("@ccName", SqlDbType.NVarChar).Value = ccName.ToString();
                                clntCmd.Parameters.AddWithValue("@ccNameLower", SqlDbType.NVarChar).Value = ccNameLower.ToString();
                                clntCmd.Parameters.AddWithValue("@clientTaxReg", SqlDbType.NVarChar).Value = clientTaxReg.ToString();
                                clntCmd.Parameters.AddWithValue("@clientNotes", SqlDbType.NVarChar).Value = clientNotes.ToString();
                                clntCmd.Parameters.AddWithValue("@clientRealm", SqlDbType.NVarChar).Value = coDomain.ToString();
                                clntCmd.Parameters.AddWithValue("@application", SqlDbType.NVarChar).Value = application.ToString();
                                clntCmd.Parameters.AddWithValue("@oType", SqlDbType.Int).Value = oType.ToString();
                                clntCmd.Parameters.AddWithValue("@crTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                                clntCmd.Parameters.AddWithValue("@updTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                                nclID = Convert.ToInt32(clntCmd.ExecuteScalar());
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                clntCmd.Dispose();
                            }
                        }
                        ccFold = clientFolder(coDomain);
                        ccAdmin = createAdmin(coDomain, oType, nclID);
                        if (ccFold.Equals(1) && ccAdmin.Equals(1))
                        {
                            result = 1;
                        }
                        else if (ccFold.Equals(-1) && ccAdmin.Equals(-1))
                        {
                            result = -1;
                        }
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        String msg = "Insert Error: ";
                        msg += ex.Message;
                    }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }
                return result;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        private static Int32 clientFolder(String clientRealm)
        {
            try
            {
                String gcFolder = ""; /* System Client Folder - Gen. Folder */
                String ucFolder = ""; /* System Client Folder - Upl. Folder */
                String gaccFolder = ""; /* System Client's Account Folder - Gen. Folder */
                String gctrtFolder = ""; /* System Client's Contracts Folder - Gen. Folder */
                String gastFolder = ""; /* System Client's Assets Folder - Gen. Folder */
                String gsrvCTRT = ""; /* System Client's Service Contracts Folder - Gen. Folder */
                String gaccCTRT = ""; /* System Client's Account and Finance Contracts Folder - Gen. Folder */
                String gmntCTRT = ""; /* System Client's Maintenance Contracts Folder - Gen. Folder */
                String gastJob = ""; /* System Client's Jobs Folder - Gen. Folder */
                String logsFolder = ""; /* System Client's Well Logs Folder - Gen Folder containing domain info */

                //Get Folder Paths from configuration
                String genDirectory = System.Web.Configuration.WebConfigurationManager.AppSettings["GenPath"].ToString();
                String uplDirectory = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadPath"].ToString();
                String logDirectory = System.Web.Configuration.WebConfigurationManager.AppSettings["LogsPath"].ToString();

                gcFolder = (String.Format("{0}\\{1}\\", genDirectory, clientRealm));
                DirectoryInfo clientFolder = new DirectoryInfo(gcFolder);
                ucFolder = (String.Format("{0}\\{1}\\", uplDirectory, clientRealm));
                DirectoryInfo uplclientFolder = new DirectoryInfo(ucFolder);
                gaccFolder = (String.Format("{0}\\{1}\\{2}\\", genDirectory, clientRealm, "Accounts"));
                DirectoryInfo accountingcontractFolder = new DirectoryInfo(gaccFolder);
                gctrtFolder = (String.Format("{0}\\{1}\\{2}\\", genDirectory, clientRealm, "Contracts"));
                DirectoryInfo contractFolder = new DirectoryInfo(gctrtFolder);
                gastFolder = (String.Format("{0}\\{1}\\{2}\\", genDirectory, clientRealm, "Assets"));
                DirectoryInfo assetsFolder = new DirectoryInfo(gastFolder);
                gsrvCTRT = (String.Format("{0}\\{1}\\{2}\\{3}\\", genDirectory, clientRealm, "Contracts", "Service_Contracts"));
                DirectoryInfo serviceFolder = new DirectoryInfo(gsrvCTRT);
                gaccCTRT = (String.Format("{0}\\{1}\\{2}\\{3}\\", genDirectory, clientRealm, "Contracts", "Accounting_Contracts"));
                DirectoryInfo accountcontractFolder = new DirectoryInfo(gaccCTRT);
                gmntCTRT = (String.Format("{0}\\{1}\\{2}\\{3}\\", genDirectory, clientRealm, "Contracts", "Maintenance_Contracts"));
                DirectoryInfo maintenanceFolder = new DirectoryInfo(gmntCTRT);
                gastJob = (String.Format("{0}\\{1}\\{2}\\{3}\\", genDirectory, clientRealm, "Assets", "Jobs"));
                DirectoryInfo jobFolder = new DirectoryInfo(gastJob);
                logsFolder = (String.Format("{0}\\{1}\\", logDirectory, clientRealm));
                DirectoryInfo wellogFolder = new DirectoryInfo(logsFolder);
                if (!Directory.Exists(gcFolder))
                {
                    clientFolder.Create(); /* New Directory Folder for System Client - Gen. Docs */
                    uplclientFolder.Create(); /* New Directory Folder for System Client - Upl. Docs */
                    accountingcontractFolder.Create(); /* New Accounts Folder for System Client */
                    contractFolder.Create(); /* New Contract Folder for System Client */
                    assetsFolder.Create(); /* New Assets Folder for System Client */
                    serviceFolder.Create(); /* New Services Contract Sub-Folder in Contracts Folder */
                    accountcontractFolder.Create(); /* New Accounts Contract Sub-Folder in Contracts Folder */
                    maintenanceFolder.Create(); /* New Maintenance Contract Sub-Folder in Contracts Folder */
                    jobFolder.Create(); /* New Client's Job Silo main Folder */
                    wellogFolder.Create(); /* New Client's Well Logs Folder in Log Folders main silo */
                }
                return 1;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String createClientDataFileFolder(Int32 JobID, Int32 WellID, Int32 RunID, String ClientRealm, Int32 ServiceID, String ClientDBConnection)
        {
            try
            {
                String genDirectory = System.Web.Configuration.WebConfigurationManager.AppSettings["GenPath"].ToString();
                String uplDirectory = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadPath"].ToString();
                String uploadCompletePath = String.Empty, jD = String.Empty, wD = String.Empty, rD = String.Empty, oprName = String.Empty, clientDomain = String.Empty;
                Int32 oprID = -99;
                jD = String.Format("{0}_{1}\\", "Job", JobID);
                wD = String.Format("{0}_{1}\\", "Well", WellID);
                rD = String.Format("{0}_{1}\\", "Run ", RunID);
                oprID = AST.getOperatorID(WellID, ClientDBConnection);
                oprName = AST.getOperatorName(oprID);
                TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                clientDomain = ti.ToLower(ClientRealm);
                if (ServiceID.Equals(33))
                {
                    uploadCompletePath = (String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\{7}\\", uplDirectory, clientDomain, "Assets", "Wells", oprName, wD, "RawDataQC", rD));
                    DirectoryInfo di = Directory.CreateDirectory(uploadCompletePath);
                }
                else if (ServiceID.Equals(34))
                {
                    uploadCompletePath = (String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\{7}\\", uplDirectory, clientDomain, "Assets", "Wells", oprName, wD, "ResultsQC", rD));
                    DirectoryInfo di = Directory.CreateDirectory(uploadCompletePath);
                }
                else if (ServiceID.Equals(41))
                {
                    uploadCompletePath = (String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\{7}\\", uplDirectory, clientDomain, "Assets", "Wells", oprName, wD, "Well Profile", rD));
                    DirectoryInfo di = Directory.CreateDirectory(uploadCompletePath);
                }
                else if (ServiceID.Equals(46))
                {
                    uploadCompletePath = (String.Format("{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}\\{7}\\", uplDirectory, clientDomain, "Assets", "Wells", oprName, wD, "Correction", rD));
                    DirectoryInfo di = Directory.CreateDirectory(uploadCompletePath);
                }
                else
                {
                    uploadCompletePath = String.Empty;
                }
                return uploadCompletePath; 
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 createImageFile(String ImageFileName, Int32 ImageType, Int32 ImageSize, Int32 ImageColor, Int32 ImagePurpose, Byte[] ImageContent, Int32 ClientID)
        {
            try
            {
                Int32 iResult = -99;
                using (SqlConnection imgCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        imgCon.Open();
                        String insertCmd = "INSERT INTO ClientImage ([climgFileName], [imgtypID], [climgFileSize], [lclrID], [imgpID], [climgFile], [clntID], [cTime], [uTime]) VALUES (@FN, @FT, @FZ, @FC, @FP, @FS, @CID, @cT, @uT)";
                        SqlCommand insValues = new SqlCommand(insertCmd, imgCon);
                        insValues.Parameters.AddWithValue("@FN", SqlDbType.NVarChar).Value = ImageFileName.ToString();
                        insValues.Parameters.AddWithValue("@FT", SqlDbType.Int).Value = ImageType.ToString();
                        insValues.Parameters.AddWithValue("@FZ", SqlDbType.Int).Value = ImageSize.ToString();
                        insValues.Parameters.AddWithValue("@FC", SqlDbType.Int).Value = ImageColor.ToString();
                        insValues.Parameters.AddWithValue("@FP", SqlDbType.Int).Value = ImagePurpose.ToString();
                        insValues.Parameters.AddWithValue("@FS", SqlDbType.Image).Value = ImageContent;
                        insValues.Parameters.AddWithValue("@CID", SqlDbType.Int).Value = ClientID.ToString();
                        insValues.Parameters.AddWithValue("@cT", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insValues.Parameters.AddWithValue("@uT", SqlDbType.DateTime2).Value = DateTime.Now.ToString();

                        iResult = Convert.ToInt32(insValues.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        imgCon.Close();
                        imgCon.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static int createAdmin(String coName, Int32 clntType, Int32 nclID)
        {
            try
            {
                Int32 iReply = -99;
                String adminFirst = WebConfigurationManager.AppSettings["admFirstName"], adminMiddle = WebConfigurationManager.AppSettings["admMidName"], adminLast = WebConfigurationManager.AppSettings["admLastName"], adminPswrd = WebConfigurationManager.AppSettings["admPassword"];
                String st1 = WebConfigurationManager.AppSettings["admSt1"], st2 = WebConfigurationManager.AppSettings["admSt2"], st3 = WebConfigurationManager.AppSettings["admSt3"];
                String zip = WebConfigurationManager.AppSettings["admZip"], city = WebConfigurationManager.AppSettings["admCity"], county = WebConfigurationManager.AppSettings["admCounty"], state = WebConfigurationManager.AppSettings["admState"], country = WebConfigurationManager.AppSettings["admCountry"], sreg = WebConfigurationManager.AppSettings["admSReg"], reg = WebConfigurationManager.AppSettings["admReg"], lat = WebConfigurationManager.AppSettings["admLat"], lng = WebConfigurationManager.AppSettings["admLng"];
                String addType = WebConfigurationManager.AppSettings["admAddType"], desig = WebConfigurationManager.AppSettings["admDesig"];
                String adminPh = WebConfigurationManager.AppSettings["admPhone"], phType = WebConfigurationManager.AppSettings["admPhoneType"];
                
                String admUsr = "";
                String newUsr = "";
                admUsr = String.Format("{0}.{1}@{2}", adminFirst, adminLast, coName);

                /* Adding User to Memberhsip DB */
                Membership.CreateUser(admUsr, adminPswrd, admUsr);

                /* Adding New user to selected Role */
                Roles.AddUserToRole(admUsr, "Support");

                /* Updating New User's Profile Information */
                UP profile = (UP)UP.Create(admUsr);
                profile.FirstName = adminFirst;
                profile.MiddleName = adminMiddle;
                profile.LastName = adminLast;
                profile.upPhone = adminPh;
                profile.dmgID = phType;
                profile.UserId = newUsr;
                profile.clntID = Convert.ToString(nclID);
                profile.ctypeID = Convert.ToString(clntType);
                profile.addType = addType;
                profile.st1 = st1;
                profile.st2 = st2;
                profile.st3 = st3;
                profile.city = city;
                profile.zip = zip;
                profile.county = county;
                profile.country = country;
                profile.state = state;
                profile.sreg = sreg;
                profile.reg = reg;
                profile.Latitude = lat;
                profile.Longitude = lng;
                profile.designation = desig;
                profile.Save();

                Guid nUID = Guid.Empty;
                String selectNUser = "SELECT [UserId] FROM [aspnet_Users] WHERE [UserName] = @admUsr";
                String insertClientUser = "INSERT INTO [UserInClient] ([UserId], [clntID], [cTime], [uTime]) VALUES (@UserId, @clntID, @crTime, @updTime)";

                using (SqlConnection userConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        userConn.Open();
                        SqlCommand selNUser = new SqlCommand(selectNUser, userConn);
                        selNUser.Parameters.AddWithValue("@admUsr", SqlDbType.UniqueIdentifier).Value = admUsr.ToString();
                        using(SqlDataReader reader = selNUser.ExecuteReader())
                        {
                            try
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        nUID = (reader.GetGuid(0));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                reader.Close();
                                reader.Dispose();
                            }
                        }

                        SqlCommand clntCmd = new SqlCommand(insertClientUser, userConn);
                        clntCmd.Parameters.AddWithValue("@UserId", SqlDbType.NVarChar).Value = nUID.ToString();
                        clntCmd.Parameters.AddWithValue("@clntID", SqlDbType.NVarChar).Value = nclID.ToString();
                        clntCmd.Parameters.AddWithValue("@crTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        clntCmd.Parameters.AddWithValue("@updTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        
                        iReply = clntCmd.ExecuteNonQuery();

                        clntCmd.Dispose();
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        userConn.Close();
                    }
                }
                return 1;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //public static Int32 createAddress(Int32 clientID, Int32 addressType, Int32 majorRegion, Int32 subRegion, Int32 countryID, Int32 stateID, Int32 countyID, Int32 cityID, Int32 zipID, String street1, String street2, String street3, String latitude, String longitude, String AddressTitle, String UserName, String UserDomain)
        public static Int32 createAddress(Int32 clientID, Int32 addressType, Int32 majorRegion, Int32 subRegion, Int32 countryID, Int32 stateID, Int32 countyID, Int32 cityID, Int32 zipID, String street1, String street2, String street3, String AddressTitle, String UserName, String UserDomain)
        {
            try
            {
                Int32 iResult = -99;
                String regionName = DMG.getRegionName(majorRegion);
                String subregName = DMG.getSubRegionName(subRegion);
                String countryName = DMG.getCountryName(countryID);
                String stateName = DMG.getStateName(stateID);
                String countyName = DMG.getCountyName(countyID);
                String cityName = DMG.getCityName(cityID);
                String zipName = DMG.getZipCode(zipID);
                String stAdd = street1 + " " + cityName + " " + stateName + " " + zipName;
                List<Double> latlng = MAP.getLatLngFromAddress(stAdd);
                Double lng = Convert.ToDouble(latlng[0]);
                Double lat = Convert.ToDouble(latlng[1]);
                SqlGeography locat = SqlGeography.Point(lat, lng, 4236); //WGS84 

                String insertClientAddress = "INSERT INTO [ClientAddress] ([clntID], [dmgID], [regID], [sregID], [ctyID], [stID], [cntyID], [cityID], [zipID], [st1], [st2], [st3], [latitude], [longitude], [location], [cTime], [uTime], [title], [username], [realm]) VALUES (@clntID, @dmgID, @regID, @sregID, @ctyID, @stID, @cntyID, @cityID, @zipID, @st1, @st2, @st3, @lat, @lon, @loc, @crTime, @updTime, @title, @username, @realm)";

                using (SqlConnection userConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        userConn.Open();

                        SqlCommand clntCmd = new SqlCommand(insertClientAddress, userConn);

                        clntCmd.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = clientID.ToString();
                        clntCmd.Parameters.AddWithValue("@dmgID", SqlDbType.Int).Value = addressType.ToString();
                        clntCmd.Parameters.AddWithValue("@regID", SqlDbType.Int).Value = majorRegion.ToString();
                        clntCmd.Parameters.AddWithValue("@sregID", SqlDbType.Int).Value = subRegion.ToString();
                        clntCmd.Parameters.AddWithValue("@ctyID", SqlDbType.Int).Value = countryID.ToString();
                        clntCmd.Parameters.AddWithValue("@stID", SqlDbType.Int).Value = stateID.ToString();
                        clntCmd.Parameters.AddWithValue("@cntyID", SqlDbType.Int).Value = countyID.ToString();
                        clntCmd.Parameters.AddWithValue("@cityID", SqlDbType.Int).Value = cityID.ToString();
                        clntCmd.Parameters.AddWithValue("@zipID", SqlDbType.Int).Value = zipID.ToString();
                        clntCmd.Parameters.AddWithValue("@st1", SqlDbType.NVarChar).Value = street1.ToString();
                        clntCmd.Parameters.AddWithValue("@st2", SqlDbType.NVarChar).Value = street2.ToString();
                        clntCmd.Parameters.AddWithValue("@st3", SqlDbType.NVarChar).Value = street3.ToString();
                        clntCmd.Parameters.AddWithValue("@lat", SqlDbType.Decimal).Value = lat.ToString();
                        clntCmd.Parameters.AddWithValue("@lon", SqlDbType.Decimal).Value = lng.ToString();
                        clntCmd.Parameters.AddWithValue("@loc", SqlDbType.VarChar).Value = locat.ToString();
                        clntCmd.Parameters.AddWithValue("@crTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        clntCmd.Parameters.AddWithValue("@updTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        clntCmd.Parameters.AddWithValue("@title", SqlDbType.NVarChar).Value = AddressTitle.ToString();
                        clntCmd.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        clntCmd.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserDomain.ToString();

                        iResult = Convert.ToInt32(clntCmd.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        userConn.Close();
                        userConn.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 createDBMap(Int32 ClientID, String ClientRealm, String DBName, String DBConString, String UserName, String UserRealm)
        {
            try
            {
                Int32 iReply = -99;
                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;

                String insStatement = "INSERT INTO [ClientDBMapping] ([clntID], [clntRealm], [cdbString], [cmName], [cTime], [uTime], [username], [realm]) VALUES (@clntID, @clntRealm, @cdbString, @cmName, @cTime, @uTime, @username, @realm)";
                using (SqlConnection userConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        userConn.Open();
                        SqlCommand insData = new SqlCommand(insStatement, userConn);
                        insData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        insData.Parameters.AddWithValue("@clntRealm", SqlDbType.NVarChar).Value = ClientRealm.ToString();
                        insData.Parameters.AddWithValue("@cdbString", SqlDbType.NVarChar).Value = DBConString.ToString();
                        insData.Parameters.AddWithValue("@cmName", SqlDbType.NVarChar).Value = DBName.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = crTime.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = updTime.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserRealm.ToString();

                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        userConn.Close();
                        userConn.Dispose();
                    }
                }

                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 findDuplicateDBMapping(Int32 ClientID, String DBName)
        {
            try
            {
                Int32 iReply = -99;

                String selData = "SELECT [cdbID] FROM [ClientDBMapping] WHERE [clntID] = @clntID AND [cmName] = @cmName";
                using (SqlConnection userConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        userConn.Open();
                        SqlCommand getData = new SqlCommand(selData, userConn);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@cmName", SqlDbType.NVarChar).Value = DBName.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    iReply = readData.GetInt32(0);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        userConn.Close();
                        userConn.Dispose();
                    }
                }

                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 findBusyDB(String DBName)
        {
            try
            {
                Int32 iReply = -99;

                String selData = "SELECT COUNT(cdbID) FROM [ClientDBMapping] WHERE [cmName] = @cmName";
                using (SqlConnection userConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        userConn.Open();
                        SqlCommand getData = new SqlCommand(selData, userConn);
                        getData.Parameters.AddWithValue("@cmName", SqlDbType.NVarChar).Value = DBName.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    iReply = readData.GetInt32(0);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        userConn.Close();
                        userConn.Dispose();
                    }
                }

                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 opetrCreate(String clientName, String clientTaxReg, String clientNotes, String clientRealm)
        {
            try
            {
                String ccName = null, ccNameLower = null, coDomain = null;
                String application = appID;
                Int32 oType = 4; //Operator Account Type
                Int32 ccFold = 0, ccAdmin = 0, result = 0, nclID = 0;

                TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                ccName = ti.ToTitleCase(clientName);
                ccNameLower = ti.ToLower(clientName);
                coDomain = ti.ToLower(clientRealm);

                String insertClient = "INSERT INTO [Client] (clntName, clntNameLower, clntTaxReg, clntNotes, clntRealm, ApplicationID, ctypID, cTime, uTime) VALUES (@ccName, @ccNameLower, @clientTaxReg, @clientNotes, @clientRealm, @application, @oType, @crTime, @updTime); SELECT SCOPE_IDENTITY();";
                
                using (SqlConnection clntConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        clntConn.Open();
                        SqlCommand clntCmd = new SqlCommand(insertClient, clntConn);
                        clntCmd.Parameters.AddWithValue("@ccName", SqlDbType.NVarChar).Value = ccName.ToString();
                        clntCmd.Parameters.AddWithValue("@ccNameLower", SqlDbType.NVarChar).Value = ccNameLower.ToString();
                        clntCmd.Parameters.AddWithValue("@clientTaxReg", SqlDbType.NVarChar).Value = clientTaxReg.ToString();
                        clntCmd.Parameters.AddWithValue("@clientNotes", SqlDbType.NVarChar).Value = clientNotes.ToString();
                        clntCmd.Parameters.AddWithValue("@clientRealm", SqlDbType.NVarChar).Value = coDomain.ToString();
                        clntCmd.Parameters.AddWithValue("@application", SqlDbType.NVarChar).Value = application.ToString();
                        clntCmd.Parameters.AddWithValue("@oType", SqlDbType.Int).Value = oType.ToString();
                        clntCmd.Parameters.AddWithValue("@crTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        clntCmd.Parameters.AddWithValue("@updTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();

                        nclID = Convert.ToInt32(clntCmd.ExecuteScalar());//Getting ID of inserted account                    

                        ccFold = clientFolder(coDomain);
                        ccAdmin = createAdmin(coDomain, oType, nclID);
                        if (ccFold.Equals(1) && ccAdmin.Equals(1))
                        {
                            result = 1;
                        }
                        else if (ccFold.Equals(-1) && ccAdmin.Equals(-1))
                        {
                            result = -1;
                        }
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        String msg = "Insert Error: ";
                        msg += ex.Message;
                    }
                    finally
                    {
                        clntConn.Close();
                    }
                }
                return result;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }         

        public static Int32 ownerCreate(String ownerName, String ownerTaxReg, String ownerNotes, String ownerRealm)
        {
            try
            {
                String coName = null, clntNameLower = null, coDomain = null;
                String application = appID;
                Int32 oType = 3;
                Int32 ownFold = 0, ownAdmin = 0, result = 0, nclID = 0;

                TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                ///coName = ti.ToTitleCase(ownerName);
                coName = ti.ToTitleCase(ownerName);
                clntNameLower = ti.ToLower(ownerName);
                coDomain = ti.ToLower(ownerRealm);

                String insertClient = "INSERT INTO [Client] (clntName, clntNameLower, clntTaxReg, clntNotes, clntRealm, ApplicationID, ctypID, cTime, uTime) VALUES (@coName, @clntNameLower, @ownerTaxReg, @ownerNotes, @ownerRealm, @application, @oType, @crTime, @updTime); SELECT SCOPE_IDENTITY();";

                using (SqlConnection clntConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        clntConn.Open();

                        SqlCommand clntCmd = new SqlCommand(insertClient, clntConn);
                        clntCmd.Parameters.AddWithValue("@coName", SqlDbType.NVarChar).Value = coName.ToString();
                        clntCmd.Parameters.AddWithValue("@clntNameLower", SqlDbType.NVarChar).Value = clntNameLower.ToString();
                        clntCmd.Parameters.AddWithValue("@ownerTaxReg", SqlDbType.NVarChar).Value = ownerTaxReg.ToString();
                        clntCmd.Parameters.AddWithValue("@ownerNotes", SqlDbType.NVarChar).Value = ownerNotes.ToString();
                        clntCmd.Parameters.AddWithValue("@ownerRealm", SqlDbType.NVarChar).Value = coDomain.ToString();
                        clntCmd.Parameters.AddWithValue("@application", SqlDbType.NVarChar).Value = application.ToString();
                        clntCmd.Parameters.AddWithValue("@oType", SqlDbType.Int).Value = oType.ToString();
                        clntCmd.Parameters.AddWithValue("@crTime", SqlDbType.DateTime).Value = DateTime.Now.ToString();
                        clntCmd.Parameters.AddWithValue("@updTime", SqlDbType.DateTime).Value = DateTime.Now.ToString();
                        nclID = Convert.ToInt32(clntCmd.ExecuteScalar());
                        clntCmd.Dispose();
                        
                        ownFold = ownerFolder(coDomain);
                        ownAdmin = createAdmin(coDomain, oType, nclID);

                        if (ownFold.Equals(1) && ownAdmin.Equals(1))
                        {
                            result = 1;
                        }
                        else if (ownFold.Equals(-1) && ownAdmin.Equals(-1))
                        {
                            result = -1;
                        }
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        String msg = "Insert Error: ";
                        msg += ex.Message;
                    }
                    finally
                    {
                        clntConn.Close();
                    }
                }
                return result;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 ownerFolder(String ownerName)
        {
            try
            {
                String gcFolder = ""; /* System Client Folder - Gen. Folder */
                String ucFolder = ""; /* System Client Folder - Upl. Folder */
                String gaccFolder = ""; /* System Client's Account Folder - Gen. Folder */
                String gctrtFolder = ""; /* System Client's Contracts Folder - Gen. Folder */
                String gsrvCTRT = ""; /* System Client's Service Contracts Folder - Gen. Folder */
                String gaccCTRT = ""; /* System Client's Account and Finance Contracts Folder - Gen. Folder */
                String gmntCTRT = ""; /* System Client's Maintenance Contracts Folder - Gen. Folder */

                //Get Folder Paths from configuration
                String genDirectory = System.Web.Configuration.WebConfigurationManager.AppSettings["GenPath"].ToString();
                String uplDirectory = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadPath"].ToString();

                gcFolder = (String.Format("{0}\\{1}\\", genDirectory, ownerName));
                DirectoryInfo clientFolder = new DirectoryInfo(gcFolder);
                ucFolder = (String.Format("{0}\\{1}\\", uplDirectory, ownerName));
                DirectoryInfo uplclientFolder = new DirectoryInfo(ucFolder);
                gaccFolder = (String.Format("{0}\\{1}\\{2}\\", genDirectory, ownerName, "Accounts"));
                DirectoryInfo accountingcontractFolder = new DirectoryInfo(gaccFolder);
                gctrtFolder = (String.Format("{0}\\{1}\\{2}\\", genDirectory, ownerName, "Contracts"));
                DirectoryInfo contractFolder = new DirectoryInfo(gctrtFolder);
                gsrvCTRT = (String.Format("{0}\\{1}\\{2}\\{3}\\", genDirectory, ownerName, "Contracts", "Service_Contracts"));
                DirectoryInfo serviceFolder = new DirectoryInfo(gsrvCTRT);
                gaccCTRT = (String.Format("{0}\\{1}\\{2}\\{3}\\", genDirectory, ownerName, "Contracts", "Accounting_Contracts"));
                DirectoryInfo accountcontractFolder = new DirectoryInfo(gaccCTRT);
                gmntCTRT = (String.Format("{0}\\{1}\\{2}\\{3}\\", genDirectory, ownerName, "Contracts", "Maintenance_Contracts"));
                DirectoryInfo maintenanceFolder = new DirectoryInfo(gmntCTRT);
                if (!Directory.Exists(gcFolder))
                {
                    clientFolder.Create(); /* New Directory Folder for System Client - Gen. Docs */
                    uplclientFolder.Create(); /* New Directory Folder for System Client - Upl. Docs */
                    accountingcontractFolder.Create(); /* New Accounts Folder for System Client */
                    contractFolder.Create(); /* New Contract Folder for System Client */
                    serviceFolder.Create(); /* New Services Contract Sub-Folder in Contracts Folder */
                    accountcontractFolder.Create(); /* New Accounts Contract Sub-Folder in Contracts Folder */
                    maintenanceFolder.Create(); /* New Maintenance Contract Sub-Folder in Contracts Folder */
                }
                return 1;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }     

        public static Int32 partnerCreate(String partnerName, String partnerTaxReg, String partnerNotes, String partnerRealm)
        {
            try
            {
                String prtName = null, prtNameLower = null, coDomain = null; 
                String application = appID;
                Int32 oType = 2; // Partner Client Type
                Int32 prtFold = 0, prtAdmin = 0, result = 0, nclID = 0;
                TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                prtName = ti.ToTitleCase(partnerName);
                prtNameLower = ti.ToLower(partnerName);
                coDomain = ti.ToLower(partnerRealm);
                String insertClient = "INSERT INTO Client (clntName, clntNameLower, clntTaxReg, clntNotes, clntRealm, ApplicationID, ctypID, cTime, uTime) VALUES (@ccName, @ccNameLower, @clientTaxReg, @clientNotes, @clientRealm, @application, @oType, @crTime, @updTime); SELECT SCOPE_IDENTITY();";                
                using(SqlConnection clntConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                try
                {
                    clntConn.Open();

                    SqlCommand clntCmd = new SqlCommand(insertClient, clntConn);
                    clntCmd.Parameters.AddWithValue("@ccName", SqlDbType.NVarChar).Value = prtName.ToString();
                    clntCmd.Parameters.AddWithValue("@ccNameLower", SqlDbType.NVarChar).Value = prtNameLower.ToString();
                    clntCmd.Parameters.AddWithValue("@clientTaxReg", SqlDbType.NVarChar).Value = partnerTaxReg.ToString();
                    clntCmd.Parameters.AddWithValue("@clientNotes", SqlDbType.NVarChar).Value = partnerNotes.ToString();
                    clntCmd.Parameters.AddWithValue("@clientRealm", SqlDbType.NVarChar).Value = coDomain.ToString();
                    clntCmd.Parameters.AddWithValue("@application", SqlDbType.NVarChar).Value = application.ToString();
                    clntCmd.Parameters.AddWithValue("@oType", SqlDbType.Int).Value = oType.ToString();
                    clntCmd.Parameters.AddWithValue("@crTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                    clntCmd.Parameters.AddWithValue("@updTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                    nclID = Convert.ToInt32(clntCmd.ExecuteScalar());
                    clntCmd.Dispose();

                    prtFold = clientFolder(coDomain);
                    prtAdmin = createAdmin(coDomain, oType, nclID);

                    if (prtFold.Equals(1) && prtAdmin.Equals(1))
                    {
                        result = 1;
                    }
                    else if (prtFold.Equals(-1) && prtAdmin.Equals(-1))
                    {
                        result = -1;
                    }
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    String msg = "Insert Error: ";
                    msg += ex.Message;
                }
                finally
                {
                    clntConn.Close();
                }
                return result;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getAddressLine(Int32 clientID, Int32 addressID)
        {
            try
            {
                Int32 ctyID = 0, stID = 0, cityID = 0, zipID = 0;
                String country = String.Empty, state = String.Empty, city = String.Empty, zipcode = String.Empty;
                String st1 = String.Empty, st2 = String.Empty, st3 = String.Empty, addressLine = String.Empty;

                SqlConnection userConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString());
                String selectStatement = "SELECT [ctyID], [stID], [cityID], [zipID], [st1], [st2], [st3] FROM [ClientAddress] WHERE [clntID] = @clientID AND [dmgID] = @addressID";
                try
                {
                    userConn.Open();

                    SqlCommand selCAdd = new SqlCommand(selectStatement, userConn);
                    selCAdd.Parameters.AddWithValue("@clientID", SqlDbType.UniqueIdentifier).Value = clientID.ToString();
                    selCAdd.Parameters.AddWithValue("@addressID", SqlDbType.UniqueIdentifier).Value = addressID.ToString();
                    using (SqlDataReader reader = selCAdd.ExecuteReader())
                    {
                        try
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    ctyID = (reader.GetInt32(0));
                                    stID = (reader.GetInt32(1));
                                    cityID = (reader.GetInt32(2));
                                    zipID = (reader.GetInt32(3));
                                    st1 = (reader.GetString(4));
                                    st2 = (reader.GetString(5));
                                    st3 = (reader.GetString(6));
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            reader.Close();
                            reader.Dispose();
                        }
                    }                    
                    country = getCountryName(ctyID);
                    state = getStateName(stID);
                    city = getCityName(cityID);
                    zipcode = getZipCode(zipID);

                    if ((String.IsNullOrEmpty(st2) || st2.Equals("-999")) && (String.IsNullOrEmpty(st3) || st3.Equals("-999")))
                    {
                        addressLine = String.Format("{0},{1},{2},{3},{4}", st1, city, state, zipcode, country);
                    }
                    else if (String.IsNullOrEmpty(st3) || st3.Equals("-999"))
                    {
                        addressLine = String.Format("{0},{1},{2},{3},{4}", st1 + " " + st2, city, state, zipcode, country);
                    }
                    else
                    {
                        addressLine = String.Format("{0},{1},{2},{3},{4}", st1 + " " + st2 + " " + st3, city, state, zipcode, country);
                    }                    
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
                finally
                {
                    userConn.Close();
                    userConn.Dispose();
                }
                return addressLine;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getAddressLineWithPhone(Int32 clientID, Int32 addressID)
        {
            try
            {
                Int32 ctyID = 0, stID = 0, cityID = 0, zipID = 0, pTyp = 0;
                String country = String.Empty, state = String.Empty, city = String.Empty, zipcode = String.Empty;
                String st1 = String.Empty, st2 = String.Empty, st3 = String.Empty, addressLine = String.Empty, phone = String.Empty, phoneType = String.Empty, number = String.Empty, title = String.Empty;
                String selAddress = "SELECT [ctyID], [stID], [cityID], [zipID], [st1], [st2], [st3], [title] FROM [ClientAddress] WHERE [clntID] = @clientID AND [clntAddID] = @addressID";
                String selPhone = "SELECT [ptaValue], [ptaType] FROM [ClientPhoneToAddress] WHERE [clntAddID] = @clntAddID";
                SqlConnection userConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString());                
                try
                {
                    userConn.Open();

                    SqlCommand selCAdd = new SqlCommand(selAddress, userConn);
                    selCAdd.Parameters.AddWithValue("@clientID", SqlDbType.UniqueIdentifier).Value = clientID.ToString();
                    selCAdd.Parameters.AddWithValue("@addressID", SqlDbType.UniqueIdentifier).Value = addressID.ToString();
                    using (SqlDataReader reader = selCAdd.ExecuteReader())
                    {
                        try
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    ctyID = (reader.GetInt32(0));
                                    stID = (reader.GetInt32(1));
                                    cityID = (reader.GetInt32(2));
                                    zipID = (reader.GetInt32(3));
                                    st1 = (reader.GetString(4));
                                    st2 = (reader.GetString(5));
                                    st3 = (reader.GetString(6));
                                    title = (reader.GetString(7));
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            reader.Close();
                            reader.Dispose();
                        }
                    }

                    SqlCommand getPhone = new SqlCommand(selPhone, userConn);
                    getPhone.Parameters.AddWithValue("@clntAddID", SqlDbType.Int).Value = addressID.ToString();
                    using (SqlDataReader readPhone = getPhone.ExecuteReader())
                    {
                        try
                        {
                            if (readPhone.HasRows)
                            {
                                while (readPhone.Read())
                                {
                                    phone = readPhone.GetString(0);
                                    pTyp = readPhone.GetInt32(1);
                                    phoneType = DMG.getDemogTypeName(pTyp);
                                    number = phone + " ( " + phoneType + " )";
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            readPhone.Close();
                            readPhone.Dispose();
                        }
                    }
                    
                    country = getCountryName(ctyID);
                    state = getStateName(stID);
                    city = getCityName(cityID);
                    zipcode = getZipCode(zipID);

                    if ((String.IsNullOrEmpty(st2) || st2.Equals("-999")) && (String.IsNullOrEmpty(st3) || st3.Equals("-999")))
                    {
                        addressLine = String.Format("{0},{1},{2},{3},{4},{5},{6}", title, st1, city, state, zipcode, country, number);
                    }
                    else if (String.IsNullOrEmpty(st3) || st3.Equals("-999"))
                    {
                        addressLine = String.Format("{0},{1},{2},{3},{4},{5},{6}", title, st1 + " " + st2, city, state, zipcode, country, number);
                    }
                    else
                    {
                        addressLine = String.Format("{0},{1},{2},{3},{4},{5},{6}", title, st1 + " " + st2 + " " + st3, city, state, zipcode, country, number);
                    }
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
                finally
                {
                    userConn.Close();
                    userConn.Dispose();
                }
                return addressLine;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<String> getAddressListWithPhone(Int32 clientID, Int32 addressID)
        {
            try
            {
                List<String> iReply = new List<String>();
                Int32 ctyID = 0, stID = 0, cityID = 0, zipID = 0, pTyp = 0;
                String country = String.Empty, state = String.Empty, city = String.Empty, zipcode = String.Empty;
                String st1 = String.Empty, st2 = String.Empty, st3 = String.Empty, phone = String.Empty, phoneType = String.Empty, number = String.Empty, title = String.Empty;
                Decimal lati = -99.000000000M, longi = -99.000000000M;
                String selAddress = "SELECT [ctyID], [stID], [cityID], [zipID], [st1], [st2], [st3], [title], [latitude], [longitude] FROM [ClientAddress] WHERE [clntID] = @clientID AND [clntAddID] = @addressID";
                String selPhone = "SELECT [ptaValue], [ptaType] FROM [ClientPhoneToAddress] WHERE [clntAddID] = @clntAddID";
                SqlConnection userConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString());
                try
                {
                    userConn.Open();

                    SqlCommand selCAdd = new SqlCommand(selAddress, userConn);
                    selCAdd.Parameters.AddWithValue("@clientID", SqlDbType.UniqueIdentifier).Value = clientID.ToString();
                    selCAdd.Parameters.AddWithValue("@addressID", SqlDbType.UniqueIdentifier).Value = addressID.ToString();
                    using (SqlDataReader reader = selCAdd.ExecuteReader())
                    {
                        try
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    ctyID = (reader.GetInt32(0));
                                    stID = (reader.GetInt32(1));
                                    cityID = (reader.GetInt32(2));
                                    zipID = (reader.GetInt32(3));
                                    st1 = (reader.GetString(4));
                                    st2 = (reader.GetString(5));
                                    st3 = (reader.GetString(6));
                                    title = (reader.GetString(7));
                                    lati = reader.GetDecimal(8);
                                    longi = reader.GetDecimal(9);
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            reader.Close();
                            reader.Dispose();
                        }
                    }

                    SqlCommand getPhone = new SqlCommand(selPhone, userConn);
                    getPhone.Parameters.AddWithValue("@clntAddID", SqlDbType.Int).Value = addressID.ToString();
                    using (SqlDataReader readPhone = getPhone.ExecuteReader())
                    {
                        try
                        {
                            if (readPhone.HasRows)
                            {
                                while (readPhone.Read())
                                {
                                    phone = readPhone.GetString(0);
                                    pTyp = readPhone.GetInt32(1);
                                    phoneType = DMG.getDemogTypeName(pTyp);
                                    number = phone + " ( " + phoneType + " )";
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            readPhone.Close();
                            readPhone.Dispose();
                        }
                    }

                    country = getCountryName(ctyID);
                    state = getStateName(stID);
                    city = getCityName(cityID);
                    zipcode = getZipCode(zipID);
                    
                    iReply.Add(Convert.ToString(title));
                    iReply.Add(Convert.ToString(st1));
                    if (String.IsNullOrEmpty(st2) || st2.Equals("-999"))
                    {
                        st2 = "---";
                    }
                    iReply.Add(Convert.ToString(st2));
                    if (String.IsNullOrEmpty(st2) || st2.Equals("-999"))
                    {
                        st3 = "---";
                    }
                    iReply.Add(Convert.ToString(st3));
                    iReply.Add(Convert.ToString(city));
                    iReply.Add(Convert.ToString(state));
                    iReply.Add(Convert.ToString(zipcode));
                    iReply.Add(Convert.ToString(country));
                    iReply.Add(Convert.ToString(number));
                    iReply.Add(Convert.ToString(lati));
                    iReply.Add(Convert.ToString(longi));
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
                finally
                {
                    userConn.Close();
                    userConn.Dispose();
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getAddressIDs(Int32 ClientID)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                String selData = "SELECT [clntAddID] FROM [ClientAddress] WHERE [clntID] = @clntID";
                using(SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply.Add(readData.GetInt32(0));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getAddressTable(Int32 ClientID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn clntAddID = iReply.Columns.Add("clntAddID", typeof(Int32));
                DataColumn dmgID = iReply.Columns.Add("dmgID", typeof(String));
                DataColumn title = iReply.Columns.Add("title", typeof(String));
                DataColumn st1 = iReply.Columns.Add("st1", typeof(String));
                DataColumn st2 = iReply.Columns.Add("st2", typeof(String));
                DataColumn st3 = iReply.Columns.Add("st3", typeof(String));
                DataColumn cityID = iReply.Columns.Add("cityID", typeof(String));
                DataColumn cntyID = iReply.Columns.Add("cntyID", typeof(String));
                DataColumn stID = iReply.Columns.Add("stID", typeof(String));
                DataColumn ctyID = iReply.Columns.Add("ctyID", typeof(String));
                DataColumn zipID = iReply.Columns.Add("zipID", typeof(String));
                DataColumn sregID = iReply.Columns.Add("sregID", typeof(String));
                DataColumn regID = iReply.Columns.Add("regID", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [clntAddID], [dmgID], [regID], [sregID], [ctyID], [stID], [cntyID], [cityID], [zipID], [st1], [st2], [st3], [title], [uTime] FROM [ClientAddress] WHERE [clntID] = @clntID ORDER BY [title]";
                Int32 id = 0, demog = 0, city = 0, country = 0, state = 0, county = 0, zip = 0, subreg = 0, reg = 0;
                String demogN = String.Empty, titleN = String.Empty, str1 = String.Empty, str2 = String.Empty, str3 = String.Empty, cityN = String.Empty, countyN = String.Empty, stateN = String.Empty, countryN = String.Empty, zipN = String.Empty, subRegN = String.Empty, RegN = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        demog = readData.GetInt32(1);
                                        demogN = DMG.getDemogTypeName(demog);
                                        reg = readData.GetInt32(2);
                                        RegN = DMG.getRegionName(reg);
                                        subreg = readData.GetInt32(3);
                                        subRegN = DMG.getSubRegionName(subreg);
                                        country = readData.GetInt32(4);
                                        countryN = DMG.getCountryName(country);
                                        state = readData.GetInt32(5);
                                        stateN = DMG.getStateName(state);
                                        county = readData.GetInt32(6);
                                        countyN = DMG.getCountyName(county);
                                        city = readData.GetInt32(7);
                                        cityN = DMG.getCityName(city);
                                        zip = readData.GetInt32(8);
                                        zipN = DMG.getZipCode(zip);
                                        str1 = readData.GetString(9);
                                        str2 = readData.GetString(10);
                                        if (str2.Equals("-999") || String.IsNullOrEmpty(str2))
                                        { str2 = "---"; }
                                        str3 = readData.GetString(11);
                                        if (str3.Equals("-999") || String.IsNullOrEmpty(str3))
                                        { str3 = "---"; }
                                        titleN = readData.GetString(12);
                                        uT = readData.GetDateTime(13);
                                        iReply.Rows.Add(id, demogN, titleN, str1, str2, str3, cityN, countyN, stateN, countryN, zipN, subRegN, RegN, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getClientAddressTypesList(Int32 ClientID)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT DISTINCT(dmgID) FROM [ClientAddress] WHERE [clntID] = @clntID";                
                List<Int32> idList = new List<Int32>();
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        idList.Add(readData.GetInt32(0));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                        foreach (Int32 item in idList)
                        {
                            name = DMG.getDemogTypeName(item);
                            iReply.Add(item, name);
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getAddressTypeName(Int32 DemogID)
        {
            try
            {
                String iReply = string.Empty;
                String selData = "SELECT [dmgName] FROM [dmgDemogType] WHERE [dmgID] = @dmgID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@dmgID", SqlDbType.Int).Value = DemogID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getAddressTitle(Int32 AddressID, Int32 ClientID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [title] FROM [ClientAddress] WHERE [clntAddID] = @clntAddID AND [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntAddID", SqlDbType.Int).Value = AddressID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static string getCityName(int cityID)
        {
            try
            {
                String cName = null;

                SqlConnection userConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString());
                String selectStatement = "SELECT (cityName) FROM dmgCity WHERE (cityID = @cityID)";

                userConn.Open();

                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand selCity = new SqlCommand(selectStatement, userConn);
                selCity.Parameters.AddWithValue("@cityID", SqlDbType.UniqueIdentifier).Value = cityID.ToString();

                SqlDataReader reader = selCity.ExecuteReader();
                using (reader)
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            cName = (reader.GetString(0));
                        }
                    }
                }
                reader.Dispose();
                adapter.Dispose();
                userConn.Close();

                return cName;
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static System.Data.DataTable getClientsTable(Int32 ClientTypeID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn clntID = iReply.Columns.Add("clntID", typeof(Int32));
                DataColumn clntName = iReply.Columns.Add("clntName", typeof(String));
                DataColumn clntNotes = iReply.Columns.Add("clntNotes", typeof(String));
                DataColumn clntRealm = iReply.Columns.Add("clntRealm", typeof(String));
                DataColumn clntTaxReg = iReply.Columns.Add("clntTaxReg", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [clntID], [clntName], [clntNotes], [clntRealm], [clntTaxReg], [uTime] FROM [Client] WHERE ([ctypID] = @ctypID) ORDER BY [clntName]";
                Int32 id = 0;
                String name = String.Empty, notes = String.Empty, realm = String.Empty, tax = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ctypID", SqlDbType.Int).Value = ClientTypeID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        notes = readData.GetString(2);
                                        realm = readData.GetString(3);
                                        tax = readData.GetString(4);
                                        uT = readData.GetDateTime(5);
                                        iReply.Rows.Add(id, name, notes, realm, tax, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getClientName(Int32 clntID)
        {
            try
            {
                String iReply = null;
                String selData = "SELECT [clntName] FROM Client WHERE [clntID] = @clntID";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = clntID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getClientList(Int32 ClientTypeID)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [clntID], [clntName] FROM [Client] WHERE [ctypID] = @ctypID ORDER BY [clntName]";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ctypID", SqlDbType.Int).Value = SystemClientTypeID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getGlobalClientList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [clntID], [clntName] FROM [Client] ORDER BY [clntName]";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getRegReqClientList(List<Int32> ClientIDs)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [clntID], [clntName] FROM [Client] WHERE [clntID] = @clntID";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        foreach (Int32 item in ClientIDs)
                        {
                            SqlCommand getData = new SqlCommand(selData, dataCon);
                            getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = item.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            id = readData.GetInt32(0);
                                            name = readData.GetString(1);
                                            iReply.Add(id, name);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
        
        public static System.Data.DataTable getClientPhoneTable(Int32 ClientID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn ptaID = iReply.Columns.Add("ptaID", typeof(Int32));
                DataColumn dmgID = iReply.Columns.Add("dmgID", typeof(String));
                DataColumn title = iReply.Columns.Add("title", typeof(String));
                DataColumn ptaValue = iReply.Columns.Add("ptaValue", typeof(String));
                DataColumn ptaExtension = iReply.Columns.Add("ptaExtension", typeof(String));
                DataColumn ptaType = iReply.Columns.Add("ptaType", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                System.Data.DataTable addTable = new System.Data.DataTable();
                DataColumn idAdd = addTable.Columns.Add("idAdd", typeof(Int32));
                DataColumn dmgAdd = addTable.Columns.Add("dmgAdd", typeof(Int32));
                DataColumn ttlAdd = addTable.Columns.Add("ttlAdd", typeof(String));
                DateTime uT = DateTime.Now;
                Int32 aID = -99, admg = -99, pID = -99, ptID = -99;
                String atitle = String.Empty, pTitle = String.Empty, pValue = String.Empty, eValue = String.Empty, dmgName = String.Empty;
                List<KeyValuePair<Int32, String>> addList = new List<KeyValuePair<Int32, String>>();
                String selAddress = "SELECT [clntAddID], [dmgID], [title] FROM [ClientAddress] WHERE [clntID] = @clntID";
                String selPhone = "SELECT [ptaID], [ptaValue], [ptaExtension], [ptaType], [uTime] FROM [ClientPhoneToAddress] WHERE [clntAddID] = @clntAddID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getAdd = new SqlCommand(selAddress, dataCon);
                        getAdd.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readAdd = getAdd.ExecuteReader())                        
                        {
                            try
                            {
                                if (readAdd.HasRows)
                                {
                                    while (readAdd.Read())
                                    {
                                        aID = readAdd.GetInt32(0);
                                        admg = readAdd.GetInt32(1);
                                        atitle = readAdd.GetString(2);
                                        addTable.Rows.Add(aID, admg, atitle);
                                        addTable.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readAdd.Close();
                                readAdd.Dispose();
                            }
                        }
                        foreach (System.Data.DataRow row in addTable.Rows)
                        {
                            aID = Convert.ToInt32(row[0]);
                            admg = Convert.ToInt32(row[1]);
                            dmgName = DMG.getDemogTypeName(admg);
                            atitle = Convert.ToString(row[2]);
                            SqlCommand getPhone = new SqlCommand(selPhone, dataCon);
                            getPhone.Parameters.AddWithValue("@clntAddID", SqlDbType.Int).Value = aID.ToString();
                            using (SqlDataReader readPhone = getPhone.ExecuteReader())
                            {
                                try
                                {
                                    if (readPhone.HasRows)
                                    {
                                        while (readPhone.Read())
                                        {
                                            pID = readPhone.GetInt32(0);
                                            pValue = readPhone.GetString(1);
                                            eValue = readPhone.GetString(2);
                                            if (String.IsNullOrEmpty(eValue))
                                            {
                                                eValue = "---";
                                            }
                                            ptID = readPhone.GetInt32(3);
                                            pTitle = DMG.getDemogTypeName(ptID);
                                            uT = readPhone.GetDateTime(4);
                                            iReply.Rows.Add(pID, dmgName, atitle, pValue, eValue, pTitle, uT);
                                            iReply.AcceptChanges();
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readPhone.Close();
                                    readPhone.Dispose();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getClientMainOfficePhone(Int32 ClientID)
        {
            try
            {
                String iReply = String.Empty;
                Int32 addID = 0, pdmgID = 0;
                String phone = String.Empty, type = String.Empty;
                String selAddID = "SELECT [clntAddID] FROM [ClientAddress] WHERE [clntID] = @clntID] AND [dmgID] = @dmgID";
                String selPhone = "SELECT [ptaValue], [ptaType] FROM [ClientPhoneToAddress] WHERE [clntAddID] = @clntAddID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getAddID = new SqlCommand(selAddID, dataCon);
                        getAddID.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getAddID.Parameters.AddWithValue("@dmgID", SqlDbType.Int).Value = '2'.ToString();
                        using (SqlDataReader readID = getAddID.ExecuteReader())
                        {
                            try
                            {
                                if (readID.HasRows)
                                {
                                    while (readID.Read())
                                    {
                                        addID = readID.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readID.Close();
                                readID.Dispose();
                            }
                        }
                        SqlCommand getData = new SqlCommand(selPhone, dataCon);
                        getData.Parameters.AddWithValue("@clntAddID", SqlDbType.Int).Value = addID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        phone = readData.GetString(0);
                                        pdmgID = readData.GetInt32(1);
                                        type = DMG.getDemogTypeName(pdmgID);
                                        iReply = phone + "( " + type + " )";
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getClientLocationTitleList(Int32 DemogID, Int32 ClientID)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [clntAddID], [title] FROM [ClientAddress] WHERE [clntID] = @clntID AND [dmgID] = @dmgID";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        getData.Parameters.AddWithValue("@dmgID", SqlDbType.Int).Value = DemogID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                                else
                                {
                                    iReply.Add(0, "No Location found for selected Demographic type");
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getClientRealmList(Int32 ClientTypeID)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [clntID], [clntRealm] FROM [Client] WHERE [ctypID] = @ctypID ORDER BY [clntRealm]";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ctypID", SqlDbType.Int).Value = ClientTypeID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getClientRealm(Int32 clntID)
        {
            try
            {
                String iReply = null;
                String selData = "SELECT [clntRealm] FROM Client WHERE [clntID] = @clntID";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = clntID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getClientTypelist()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [ctypID], [ctypName] FROM ClientType ORDER BY [ctypName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static string getCountryName(int ctyID)
        {
            try
            {
                String ctyName = null;

                SqlConnection userConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString());
                String selectStatement = "SELECT (ctyName) FROM dmgCountry WHERE (ctyID = @ctyID)";

                userConn.Open();

                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand selCountry = new SqlCommand(selectStatement, userConn);
                selCountry.Parameters.AddWithValue("@ctyID", SqlDbType.UniqueIdentifier).Value = ctyID.ToString();

                SqlDataReader reader = selCountry.ExecuteReader();
                using (reader)
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            ctyName = (reader.GetString(0));
                        }
                    }
                }
                reader.Dispose();
                adapter.Dispose();
                userConn.Close();

                return ctyName;
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static String getClientDB(Int32 ClientID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [cmName] FROM [ClientDBMapping] WHERE [clntID] = @clntID";
                using(SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if(readData.HasRows)
                                {
                                    while(readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }                            
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getClientDBNamesList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                //String selData = "SELECT [cmID], [cmName] FROM [ConMap] WHERE ([cmName] IN (SELECT [cmName] FROM ClientDBMapping)) ORDER BY [cmName]";
                String selData = "SELECT [cmID], [cmName] FROM [ConMap] WHERE ([cmName] NOT IN (SELECT [cmName] FROM [ClientDBMapping])) ORDER BY [cmName]";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getAssociatedClientDBNamesList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [cmID], [cmName] FROM [ConMap] WHERE ([cmName] IN (SELECT [cmName] FROM ClientDBMapping)) ORDER BY [cmName]";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getClientContracts(Int32 ClientID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn ctrtID = iReply.Columns.Add("ctrtID", typeof(Int32));
                DataColumn ctID = iReply.Columns.Add("ctID", typeof(String));
                DataColumn ctrtName = iReply.Columns.Add("ctrtName", typeof(String));
                DataColumn cstatID = iReply.Columns.Add("cstatID", typeof(String));
                DataColumn ctrtStart = iReply.Columns.Add("ctrtStart", typeof(DateTime));
                DataColumn ctrtEnd = iReply.Columns.Add("ctrtEnd", typeof(DateTime));
                DataColumn ctrtTTL = iReply.Columns.Add("ctrtTTL", typeof(Double));
                DataColumn ctrtRmn = iReply.Columns.Add("ctrtRmn", typeof(Double));
                DataColumn scID = iReply.Columns.Add("scID", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT Contract.ctrtID, Contract.ctID, Contract.ctrtName, Contract.cstatID, Contract.ctrtStart, Contract.ctrtEnd, Contract.uTime, Contract.scID FROM Contract INNER JOIN ClientInContract ON Contract.ctrtID = ClientInContract.ctrtID WHERE (ClientInContract.clntID = @clntID)";
                Int32 id = 0, stt = 0, typ = 0, sID = 0;
                String name = String.Empty, status = String.Empty, type = String.Empty, campaign = String.Empty;
                DateTime start = DateTime.Now, end = DateTime.Now, uT = DateTime.Now;
                Double ttlDay = -99;
                Double rmDay = -99;

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        typ = readData.GetInt32(1);
                                        type = CTRT.getContractType(typ);
                                        name = readData.GetString(2);
                                        stt = readData.GetInt32(3);
                                        status = CTRT.getContractStatus(stt);
                                        start = readData.GetDateTime(4);
                                        end = readData.GetDateTime(5);
                                        ttlDay = (end - start).TotalDays;
                                        rmDay = (end - DateTime.Now).TotalDays;
                                        uT = readData.GetDateTime(6);
                                        sID = readData.GetInt32(7);
                                        campaign = SL.getCampaignNameFromID(sID);
                                        if (campaign.Equals("-1"))
                                        {
                                            campaign = "---";
                                        }
                                        iReply.Rows.Add(id, type, name, status, start, end, ttlDay, rmDay, campaign, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getClientContractProviderList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                iReply = getDemoContractProviderList();
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getClientImageTable(Int32 ClientID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn climgID = iReply.Columns.Add("climgID", typeof(Int32));
                DataColumn climgFileName = iReply.Columns.Add("climgFileName", typeof(String));
                DataColumn imgtypID = iReply.Columns.Add("imgtypID", typeof(String));
                DataColumn climgFileSize = iReply.Columns.Add("climgFileSize", typeof(String));
                DataColumn lclrID = iReply.Columns.Add("lclrID", typeof(String));
                DataColumn imgpID = iReply.Columns.Add("imgpID", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [climgID], [climgFileName], [imgtypID], [climgFileSize], [lclrID], [imgpID], [uTime] FROM [ClientImage] WHERE ([clntID] = @clntID) ORDER BY [climgFileName]";
                Int32 id = 0, iTyp = 0, clrID = 0, purpID = 0, size = 0;
                String fileName = String.Empty, prpName = String.Empty, clrName = String.Empty, typName = String.Empty, ImgSize = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        fileName = readData.GetString(1);
                                        iTyp = readData.GetInt32(2);
                                        typName = DMG.getImageTypeName(iTyp);
                                        size = readData.GetInt32(3);
                                        if (size >= 100 && size <= 200)
                                        {
                                            ImgSize = "2 MB";
                                        }
                                        else if (size > 200 && size <= 300)
                                        {
                                            ImgSize = "3 MB";
                                        }
                                        else if (size >= 300 && size <= 400)
                                        {
                                            ImgSize = "4 MB";
                                        }
                                        else if (size > 400 && size <= 500)
                                        {
                                            ImgSize = "5 MB";
                                        }
                                        else
                                        {
                                            ImgSize = "1 MB";
                                        }
                                        clrID = readData.GetInt32(4);
                                        clrName = DMG.getImageColorName(clrID);
                                        purpID = readData.GetInt32(5);
                                        prpName = DMG.getImagePurposeName(purpID);
                                        uT = readData.GetDateTime(6);
                                        iReply.Rows.Add(id, fileName, typName, ImgSize, clrName, prpName, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getDemoContractProviderList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [clntID], [clntName] FROM [Client] WHERE ([ctypID] = 2 OR [ctypID] = 3) ORDER BY [clntName]"; //Owner or Partner type
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getDBCon(String DBName)
        {
            try
            {
                String iResult = null;
                String selCom = "SELECT [cmString] FROM [ConMap] WHERE [cmName] = @cmName";
                using (SqlConnection userConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        userConn.Open();
                        SqlCommand getData = new SqlCommand(selCom, userConn);
                        getData.Parameters.AddWithValue("@cmName", SqlDbType.NVarChar).Value = DBName.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iResult = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        userConn.Close();
                        userConn.Dispose();
                    }
                }
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        } 

        private static string getZipCode(int zipID)
        {
            try
            {
                String zName = null;

                SqlConnection userConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString());
                String selectStatement = "SELECT (zipCode) FROM [dmgZip] WHERE (zipID = @zipID)";
            
                userConn.Open();

                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand selZip = new SqlCommand(selectStatement, userConn);
                selZip.Parameters.AddWithValue("@zipID", SqlDbType.UniqueIdentifier).Value = zipID.ToString();

                SqlDataReader reader = selZip.ExecuteReader();
                using (reader)
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            zName = (reader.GetString(0));                            
                        }
                    }
                }
                reader.Dispose();
                adapter.Dispose();
                userConn.Close();

                return zName;
            }
            catch (Exception ex)
            { 
                throw new System.Exception(ex.ToString()); 
            }
        }        

        private static string getStateName(int stID)
        {
            try
            {
                String stName = null;

                SqlConnection userConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString());
                String selectStatement = "SELECT (stName) FROM [dmgState] WHERE (stID = @stID)";

                userConn.Open();

                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand selState = new SqlCommand(selectStatement, userConn);
                selState.Parameters.AddWithValue("@stID", SqlDbType.UniqueIdentifier).Value = stID.ToString();

                SqlDataReader reader = selState.ExecuteReader();
                using (reader)
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            stName = (reader.GetString(0));
                        }
                    }
                }
                reader.Dispose();
                adapter.Dispose();
                userConn.Close();

                return stName;
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }        

        public static Dictionary<Int32, String> getTypeClientList(Int32 ClientTypeID)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [clntID], [clntName], [clntRealm] FROM [Client] WHERE ([ctypID] = @ctypID) ORDER BY [clntName]";
                Int32 id = 0;
                String name = String.Empty, realm = String.Empty;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ctypID", SqlDbType.Int).Value = ClientTypeID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        realm = readData.GetString(2);
                                        iReply.Add(id, name + " - ( " + realm + " )");
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static Dictionary<Int32, String> getGlobalClientsList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [clntID], [clntName] FROM [Client] WHERE (ctypID = 1) OR (ctypID = 2) ORDER BY [clntName]";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getClientCoCount()
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT Count(clntID) FROM [Client] WHERE [ctypID] = 1";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getOperatorCoCount()
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT Count(clntID) FROM [Client] WHERE [ctypID] = 4";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getOperatorCoList(Int32 ClientTypeID)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [clntID], [clntName] FROM [Client] WHERE [ctypID] = @ctypID ORDER BY [clntName] ASC";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ctypID", SqlDbType.Int).Value = ClientTypeID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getGlobalOperatorCoListWithAssetCount(Int32 OperatorType, Int32 ClientID, String ClientDBString)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99, gpd = 0, gwl = 0, swl = 0;
                String oprName = String.Empty, oprValue = String.Empty;
                String selData = "SELECT [clntID], [clntName] FROM [Client] WHERE [ctypID] = @ctypID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ctypID", SqlDbType.Int).Value = OperatorType.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        oprName = readData.GetString(1);
                                        gpd = getGlobalWellPadCountByOperator(id);
                                        gwl = getGlobalWellCountByOperator(id);
                                        swl = getSiloWellCountByOperator(id, ClientID, ClientDBString);
                                        oprValue = String.Format("{0} --- ( Global Well Pad: {1}; Global Well/Lateral: {2}; SMART Well/Lateral: {3} )", oprName, gpd, gwl, swl);
                                        iReply.Add(id, oprValue);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellPadCountByOperator(Int32 OperatorID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(wpdID) FROM [dmgWellPad] WHERE [optrID] = @optrID";
                String DemogDBString = WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString();
                using (SqlConnection dataCon = new SqlConnection(DemogDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getGlobalWellCountByOperator(Int32 OperatorID)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [dmgWell] WHERE [optrID] = @optrID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getSiloWellCountByOperator(Int32 OperatorID, Int32 ClientID, String ClientDBString)
        {
            try
            {
                Int32 iReply = 0;
                String selData = "SELECT COUNT(welID) FROM [Well] WHERE [optrID] = @optrID AND [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getGlobalOperatorCoWellTable(Int32 ClientTypeID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn clntID = iReply.Columns.Add("clntID", typeof(Int32));
                DataColumn clntName = iReply.Columns.Add("clntName", typeof(String));
                DataColumn wpdCount = iReply.Columns.Add("wpdCount", typeof(Int32));
                DataColumn welCount = iReply.Columns.Add("welCount", typeof(Int32));
                DataColumn welGlobal = iReply.Columns.Add("welGlobal", typeof(Int32));
                Int32 id = -99, pdCount = -99, wCount = -99, gCount = -99;
                String name = String.Empty;
                String selData = "SELECT [clntID], [clntName] FROM [Client] WHERE [ctypID] = @ctypID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ctypID", SqlDbType.Int).Value = ClientTypeID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        pdCount = DMG.getGlobalWellPadCountForOperator(id);
                                        gCount = DMG.getGlobalWellCountForOperator(id);
                                        wCount = AST.getSmartWellCountForOperator(id);
                                        iReply.Rows.Add(id, name, pdCount, wCount, gCount);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getOperatorClassTable()
        {
            try{
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn OprID = iReply.Columns.Add("OprID", typeof(Int32));
                DataColumn OprName = iReply.Columns.Add("OprName", typeof(String));
                Int32 id = -99;
                String name = String.Empty;

                Dictionary<Int32, String> jcList = getOpCoList();
                foreach (KeyValuePair<Int32, String> item in jcList)
                {
                    id = item.Key;
                    name = item.Value;
                    iReply.Rows.Add(id, name);
                    iReply.AcceptChanges();
                }

                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); 
            }
        }


        public static Dictionary<Int32, String> getOpCoList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99;
                String name = String.Empty;
                String selData = "SELECT [clntID], [clntName] FROM [Client] WHERE [ctypID] = 4";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getOperatorCoTable(Int32 ClientTypeID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn clntID = iReply.Columns.Add("clntID", typeof(Int32));
                DataColumn clntName = iReply.Columns.Add("clntName", typeof(String));
                DataColumn clntRealm = iReply.Columns.Add("clntRealm", typeof(String));
                DataColumn clntNotes = iReply.Columns.Add("clntNotes", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                Int32 id = -99;
                String name = String.Empty, realm = String.Empty, notes = String.Empty;
                DateTime uT = DateTime.Now;
                String selData = "SELECT [clntID], [clntName], [clntRealm], [clntNotes], [uTime] FROM [Client] WHERE [ctypID] = @ctypID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ctypID", SqlDbType.Int).Value = ClientTypeID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        realm = readData.GetString(2);
                                        notes = readData.GetString(3);
                                        uT = readData.GetDateTime(4);
                                        iReply.Rows.Add(id, name, realm, notes, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getReviewerAssetGlobalCounts()
        {
            try
            {
                List<Int32> iReply = new List<Int32>(), oppr = new List<Int32>(), assets = new List<Int32>();
                Int32 glblProvider = -99, glblOperator = -99, glblPads = -99, glblWells = -99;
                oppr = getReveiwerGlobalOperatorProviderCounts();
                assets = getReviewerGlobalAssetCounts();
                glblOperator = oppr[0];
                iReply.Add(glblOperator);
                glblProvider = oppr[1];
                iReply.Add(glblProvider);
                glblPads = assets[0];
                iReply.Add(glblPads);
                glblWells = assets[1];
                iReply.Add(glblWells);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getReviewerSiloAssetCounts(Int32 OperatorID)
        {
            try
            {
                List<Int32> iReply = new List<Int32>(), oppr = new List<Int32>(), assets = new List<Int32>();
                Int32 glblProvider = -99, glblOperator = -99, glblPads = -99, glblWells = -99;
                oppr = getReveiwerGlobalOperatorProviderCounts();
                assets = getSiloAssetCounts(OperatorID);
                glblOperator = oppr[0];
                iReply.Add(glblOperator);
                glblProvider = oppr[1];
                iReply.Add(glblProvider);
                glblPads = assets[0];
                iReply.Add(glblPads);
                glblWells = assets[1];
                iReply.Add(glblWells);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getReveiwerGlobalOperatorProviderCounts()
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                Int32 op = -99, pr = -99;
                String selData = "SELECT (SELECT COUNT(clntID) FROM [Client] WHERE [ctypID] = '4') AS countOperator, (SELECT COUNT(clntID) FROM [Client] WHERE [ctypID] = '1') AS countProvider";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        op = readData.GetInt32(0);
                                        iReply.Add(op);
                                        pr = readData.GetInt32(1);
                                        iReply.Add(pr);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getReviewerGlobalAssetCounts()
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                Int32 countPad = -99, countWell = -99;
                String selData = "SELECT (SELECT COUNT(wpdID) FROM [dmgWellPad]) AS countPads, (SELECT COUNT(welID) FROM [dmgWell]) AS countWell";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        countPad = readData.GetInt32(0);
                                        iReply.Add(countPad);
                                        countWell = readData.GetInt32(1);
                                        iReply.Add(countWell);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getSiloAssetCounts(Int32 OperatorID)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                Int32 countPad = -99, countWell = -99;
                String selData = "SELECT (SELECT COUNT(wpdID) FROM [WellPad] WHERE [optrID] = @optrID) AS countPads, (SELECT COUNT(welID) FROM [Well] WHERE [optrID] = @optrID) AS countWell";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEDemogDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        countPad = readData.GetInt32(0);
                                        iReply.Add(countPad);
                                        countWell = readData.GetInt32(1);
                                        iReply.Add(countWell);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getOperatorCoServiceProviderCount(Int32 ClientID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "SELECT COUNT(DISTINCT(clntID)) FROM [ClientToOperator] WHERE [optrID] = @optrID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    iReply = 0;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getOperatorCoServiceProviderList(Int32 ClientID)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                String selData = "SELECT DISTINCT(clntID) FROM [ClientToOperator] WHERE [optrID] = @optrID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply.Add(readData.GetInt32(0));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getOperatorCoServiceProviderTable(Int32 ClientID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn clntID = iReply.Columns.Add("clntID", typeof(Int32));
                DataColumn clntName = iReply.Columns.Add("clntName", typeof(String));
                DataColumn clntRealm = iReply.Columns.Add("clntRealm", typeof(String));
                Int32 id = -99;
                String name = String.Empty, domain = String.Empty;
                List<Int32> clientIDList = getOperatorCoServiceProviderList(ClientID);
                String selData = "SELECT [clntID], [clntName], [clntRealm] FROM [Client] WHERE [clntID] = @clntID ORDER BY [clntName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        foreach (Int32 cid in clientIDList)
                        {
                            SqlCommand getData = new SqlCommand(selData, dataCon);
                            getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = cid.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            id = readData.GetInt32(0);
                                            name = readData.GetString(1);
                                            domain = readData.GetString(2);
                                            iReply.Rows.Add(id, name, domain);
                                            iReply.AcceptChanges();
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getOperatorCoServiceProviderTableForReviewer(Int32 OperatorID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn clntID = iReply.Columns.Add("clntID", typeof(Int32));
                DataColumn clntName = iReply.Columns.Add("clntName", typeof(String));
                DataColumn clntRealm = iReply.Columns.Add("clntRealm", typeof(String));
                DataColumn clntPads = iReply.Columns.Add("pdCount", typeof(String));
                DataColumn clntWells = iReply.Columns.Add("wlCount", typeof(String));
                Int32 id = -99, pdCount = -99, wlCount = -99;
                String name = String.Empty, domain = String.Empty;
                List<Int32> clientIDList = getOperatorCoServiceProviderList(OperatorID);
                String selData = "SELECT [clntID], [clntName], [clntRealm] FROM [Client] WHERE [clntID] = @clntID ORDER BY [clntName]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        foreach (Int32 cid in clientIDList)
                        {
                            SqlCommand getData = new SqlCommand(selData, dataCon);
                            getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = cid.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            id = readData.GetInt32(0);
                                            name = readData.GetString(1);
                                            domain = readData.GetString(2);
                                            pdCount = DMG.getProviderGlobalWellPadCount(id);
                                            wlCount = DMG.getProviderGlobalWellCountForOperator(id);
                                            iReply.Rows.Add(id, name, domain, pdCount, wlCount);
                                            iReply.AcceptChanges();
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getLogPrintClientListForOperatorID(Int32 OperatorID)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                String selCoList = "SELECT DISTINCT[clntID] FROM [ClientToOperator] WHERE [optrID] = @optrID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selCoList, dataCon);
                        getData.Parameters.AddWithValue("@optrID", SqlDbType.Int).Value = OperatorID.ToString();                        
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply.Add(readData.GetInt32(0));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch(Exception ex)
                    {throw new System.Exception(ex.ToString());}
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getSrvProviders(Int32 OperatorID)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                List<Int32> cList = new List<Int32>();
                Int32 id = -99;
                String name = null;
                cList = getLogPrintClientListForOperatorID(OperatorID);
                String selCoName = "SELECT [clntName] FROM [Client] WHERE [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        foreach (var cid in cList)
                        {
                            SqlCommand getValues = new SqlCommand(selCoName, dataCon);
                            getValues.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = cid.ToString();
                            using (SqlDataReader readName = getValues.ExecuteReader())
                            {
                                try
                                {
                                    if (readName.HasRows)
                                    {
                                        while (readName.Read())
                                        {
                                            name = readName.GetString(0);
                                            iReply.Add(cid, name);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readName.Close();
                                    readName.Dispose();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                    return iReply;
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static ArrayList getMainLocation(Int32 ClientID)
        {
            try
            {
                ArrayList iReply = new ArrayList();
                Int32 demog = -99;
                Decimal lat = -99.000000M, lon = -99.000000M;
                String name = String.Empty, type = String.Empty;
                String icon = "https://maps.google.com/mapfiles/kml/paddle/M.png";
                name = CLNT.getClientName(ClientID);
                String selData = "SELECT [dmgID], [latitude], [longitude] FROM ClientAddress WHERE [clntID] = @clntID AND [dmgID] = '2'";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();

                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        demog = readData.GetInt32(0);
                                        type = getAddressTypeName(demog);
                                        lat = readData.GetDecimal(1);
                                        lon = readData.GetDecimal(2);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply.Add(name);
                iReply.Add(type);
                iReply.Add(lat);
                iReply.Add(lon);
                iReply.Add(icon); //icon type
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static ArrayList getOfficeLocation(Int32 ClientID)
        {
            try
            {
                ArrayList iReply = new ArrayList();
                Int32 demog = -99;
                Decimal lat = -99.000000M, lon = -99.000000M;
                String name = String.Empty, type = String.Empty;
                String icon = "https://maps.google.com/mapfiles/kml/paddle/O.png";
                name = CLNT.getClientName(ClientID);
                String selData = "SELECT [dmgID], [latitude], [longitude] FROM ClientAddress WHERE [clntID] = @clntID AND [dmgID] = '3'";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();

                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        demog = readData.GetInt32(0);
                                        type = getAddressTypeName(demog);
                                        lat = readData.GetDecimal(1);
                                        lon = readData.GetDecimal(2);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply.Add(name);
                iReply.Add(type);
                iReply.Add(lat);
                iReply.Add(lon);
                iReply.Add(icon); //icon type
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getMappedDBName(String ClientRealm)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [cmName] FROM [ClientDBMapping] WHERE [clntRealm] = @clntRealm";
                String name = String.Empty;

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {                        
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntRealm", SqlDbType.Int).Value = ClientRealm.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }                        
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static String getMappedDBNameWithSize(String DataBaseName)
        {
            try
            {
                String iReply = String.Empty;
                Double dbSize;
                CON srvCon = new CON();
                srvCon.ServerInstance = WebConfigurationManager.AppSettings["DBServer"].ToString();
                srvCon.LoginSecure = false;
                srvCon.Login = WebConfigurationManager.AppSettings["DBUser"].ToString();
                srvCon.Password = WebConfigurationManager.AppSettings["DBPassword"].ToString();
                SMO.Server dbServer = new SMO.Server(srvCon);                
                SMO.Database clntDB = dbServer.Databases[DataBaseName];
                dbSize = clntDB.Size;
                iReply = String.Format("The above realm is attached to {0} {1}", DataBaseName, "(" + dbSize + " MB)");
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getDBMapClient()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [clntID], [clntName] FROM [Client] WHERE (ctypID = 1 OR ctypID = 2 OR ctypID = 4) AND (clntID NOT IN (SELECT clntID FROM ClientDBMapping)) ORDER BY [clntName]";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                                else
                                {
                                    iReply.Add(0, "No new Mapping requirement");
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static Dictionary<Int32, String> getPartnerContractClientList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [clntID], [clntName] FROM Client WHERE ([ctypID] = '1' OR [ctypID] = '2') ORDER BY [clntName]";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }                                
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getDemoContractClientList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [clntID], [clntName] FROM Client WHERE (([ctypID] = '1') AND ([clntID] NOT IN(SELECT [clntID] FROM ClientInDemoContract))) ORDER BY [clntName]";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                                else
                                {
                                    id = 1;
                                    name = "No Demo contracts available";
                                    iReply.Add(id, name);
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getPartnershipContractProviderList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [clntID], [clntName] FROM [Client] WHERE [ctypID] = 3 ORDER BY [clntName]"; //Owner or Partner type
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getContractProviderList(Int32 ClientTypeID)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [clntID], [clntName] FROM [Client] WHERE ([ctypID] = @ctypID) ORDER BY [clntName]";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ctypID", SqlDbType.Int).Value = ClientTypeID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getContractPartnerList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<int, string>();
                String selData = "SELECT [clntID], [clntName] FROM Client WHERE ([ctypID] = 2) OR ([ctypID] = 3) ORDER BY [clntName]";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }            
        }

        public static System.Data.DataTable getClientThresholdLimitTable(Int32 ClientID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn dptID = iReply.Columns.Add("dptID", typeof(Int32));
                DataColumn dptLimit = iReply.Columns.Add("dptLimit", typeof(Decimal));
                DataColumn dptMargin = iReply.Columns.Add("dptMargin", typeof(Decimal));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [dptID], [dptLimit], [dptMargin], [uTime] FROM [DepthProcessingThreshold] WHERE [clntID] = @clntID";
                Int32 id = 0;
                Decimal lim = -99.00M, margin = -99.00M;
                DateTime uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        lim = readData.GetDecimal(1);
                                        margin = readData.GetDecimal(2);
                                        uT = readData.GetDateTime(3);
                                        iReply.Rows.Add(id, lim, margin, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertClientThresholdLimit(Decimal LimitValue, Decimal MarginValue, Int32 ClientID)
        {
            try
            {
                Int32 iReply = -99;
                String selData = "INSERT INTO [DepthProcessingThreshold] ([dptLimit], [dptMargin], [clntID], [cTime], [uTime]) VALUES (@dptLimit, @dptMargin, @clntID, @cTime, @uTime)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(selData, dataCon);
                        insData.Parameters.AddWithValue("@dptLimit", SqlDbType.Decimal).Value = LimitValue.ToString();
                        insData.Parameters.AddWithValue("@dptMargin", SqlDbType.Decimal).Value = MarginValue.ToString();
                        insData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }    
}