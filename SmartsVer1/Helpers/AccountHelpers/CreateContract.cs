﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Collections;
using System.Web.Security;
using System.Data.SqlClient;
using System.Web.Configuration;
using JS = Newtonsoft.Json;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using SRV = SmartsVer1.Helpers.DemogHelpers.ServicesConfig;
using SL = SmartsVer1.Helpers.SalesHelpers.Sales;

namespace SmartsVer1.Helpers.AccountHelpers
{
    /// <summary>
    /// A class that helps create System Client Contracts and places the generated documents
    /// in respective folders.
    /// </summary>
    /// 
    public class CreateContract
    {
        //Constants
        const Int32 GlobalClient = 1;
        static void Main()
        { }

        public static Int32 partnerContract(Int32 contractProviderID, Int32 contractClientID, String clientName, String contractName, Int32 contractType, Int32 contractStatus, DateTime sttDate, DateTime endDate, Int32 SalesCampaign, String UserName, String UserDomain)
        {
            try
            {
                Int32 contractID = -99, addressID = -99, mthdResult = -99, addProv = -99, addClnt = -99, addMgr = -99;

                String insertClient = "INSERT INTO [Contract] (ctID, ctrtName, clntAddID, cstatID, ctrtStart, ctrtEnd, cTime, uTime, username, realm, scID) VALUES (@ctID, @ctrtName, @clntAddID, @cstatID, @ctrtStart, @ctrtEnd, @crTime, @updTime, @username, @realm, @scID); SELECT Scope_Identity();";
                String getClntAddID = "SELECT [clntAddID] FROM [ClientAddress] WHERE [clntID] = @clntID AND [dmgID] = @dmgID";

                using (SqlConnection clntConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        clntConn.Open();

                        SqlCommand clntAddID = new SqlCommand(getClntAddID, clntConn);
                        clntAddID.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = contractClientID.ToString();
                        clntAddID.Parameters.AddWithValue("@dmgID", SqlDbType.Int).Value = '2'.ToString();// dmgID = 2 = Main Address
                        clntAddID.ExecuteNonQuery();

                        using (SqlDataReader readID = clntAddID.ExecuteReader())
                        {
                            try
                            {
                                if (readID.HasRows)
                                {
                                    while (readID.Read())
                                    {
                                        addressID = Convert.ToInt32(readID.GetInt32(0));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readID.Close();
                                readID.Dispose();
                            }
                        }

                        SqlCommand clntCmd = new SqlCommand(insertClient, clntConn);
                        clntCmd.Parameters.AddWithValue("@ctID", SqlDbType.Int).Value = contractType.ToString();
                        clntCmd.Parameters.AddWithValue("@ctrtName", SqlDbType.NVarChar).Value = contractName.ToString();
                        clntCmd.Parameters.AddWithValue("@clntAddID", SqlDbType.Int).Value = addressID.ToString();
                        clntCmd.Parameters.AddWithValue("@cstatID", SqlDbType.Int).Value = contractStatus.ToString();
                        clntCmd.Parameters.AddWithValue("@ctrtStart", SqlDbType.DateTime).Value = sttDate.ToString();
                        clntCmd.Parameters.AddWithValue("@ctrtEnd", SqlDbType.DateTime).Value = endDate.ToString();
                        clntCmd.Parameters.AddWithValue("@crTime", SqlDbType.DateTime).Value = DateTime.Now.ToString();
                        clntCmd.Parameters.AddWithValue("@updTime", SqlDbType.DateTime).Value = DateTime.Now.ToString();
                        clntCmd.Parameters.AddWithValue("@scID", SqlDbType.Int).Value = SalesCampaign.ToString();

                        contractID = Convert.ToInt32(clntCmd.ExecuteScalar());

                        clntCmd.Dispose();

                        //Inserting Contract Provider into ClientInContract
                        addProv = addContractProvider(contractProviderID, contractID, UserName, UserDomain);

                        //Inserting Contractee into ClientInContract
                        addClnt = addContractClient(contractClientID, contractID, UserName, UserDomain);

                        //Inserting Client Account Manager
                        addMgr = addAccountManager(contractProviderID, contractID, UserName, UserDomain);

                        if (addProv.Equals(1) && addClnt.Equals(1) && addMgr.Equals(1))
                        {
                            mthdResult = 1;
                        }
                        else
                        {
                            mthdResult = -1;
                        }
                        //Insert Contract Documents to respective Folders                   

                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        String msg = "Insert Error: ";
                        msg += ex.Message;
                    }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }
                return mthdResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 demoContract(Int32 contractProviderID, Int32 contractManager, Int32 contractClientID, String clientName, String contractName, Int32 contractType, Int32 contractStatus, DateTime sttDate, DateTime endDate, Int32 SalesCampaign, String UserName, String UserDomain)
        {
            try
            {
                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;
                Int32 contractID = -99, addressID = -99, addProv = -99, addClnt = -99, addMgr = -99, addPrice = -99, mthdResult = -99;

                String insertClient = "INSERT INTO [Contract] (ctID, ctrtName, clntAddID, cstatID, ctrtStart, ctrtEnd, cTime, uTime, username, realm, scID) VALUES (@ctID, @ctrtName, @clntAddID, @cstatID, @ctrtStart, @ctrtEnd, @crTime, @updTime, @username, @realm, @scID); SELECT Scope_Identity();";
                String getClntAddID = "SELECT [clntAddID] FROM [ClientAddress] WHERE [clntID] = @clntID AND [dmgID] = @dmgID";

                using (SqlConnection clntConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        clntConn.Open();

                        SqlCommand clntAddID = new SqlCommand(getClntAddID, clntConn);
                        clntAddID.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = contractClientID.ToString();
                        clntAddID.Parameters.AddWithValue("@dmgID", SqlDbType.Int).Value = '2'.ToString();// dmgID = 2 = Main Address
                        
                        using (SqlDataReader readID = clntAddID.ExecuteReader())
                        {
                            try
                            {
                                if (readID.HasRows)
                                {
                                    while (readID.Read())
                                    {
                                        addressID = Convert.ToInt32(readID.GetInt32(0));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readID.Close();
                                readID.Dispose();
                            }
                        }

                        SqlCommand clntCmd = new SqlCommand(insertClient, clntConn);
                        clntCmd.Parameters.AddWithValue("@ctID", SqlDbType.Int).Value = contractType.ToString();
                        clntCmd.Parameters.AddWithValue("@ctrtName", SqlDbType.NVarChar).Value = contractName.ToString();
                        clntCmd.Parameters.AddWithValue("@clntAddID", SqlDbType.Int).Value = addressID.ToString();
                        clntCmd.Parameters.AddWithValue("@cstatID", SqlDbType.Int).Value = contractStatus.ToString();
                        clntCmd.Parameters.AddWithValue("@ctrtStart", SqlDbType.DateTime).Value = sttDate.ToString();
                        clntCmd.Parameters.AddWithValue("@ctrtEnd", SqlDbType.DateTime).Value = endDate.ToString();
                        clntCmd.Parameters.AddWithValue("@crTime", SqlDbType.DateTime).Value = crTime.ToString();
                        clntCmd.Parameters.AddWithValue("@updTime", SqlDbType.DateTime).Value = updTime.ToString();
                        clntCmd.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        clntCmd.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserDomain.ToString();
                        clntCmd.Parameters.AddWithValue("@scID", SqlDbType.Int).Value = SalesCampaign.ToString();

                        contractID = Convert.ToInt32(clntCmd.ExecuteScalar());

                        //Inserting Contract Provider into ClientInContract
                        addProv = addContractProvider(contractProviderID, contractID, UserName, UserDomain);

                        //Inserting Contractee into ClientInContract
                        addClnt = addContractClient(contractClientID, contractID, UserName, UserDomain);

                        //Inserting Client Account Manager
                        addMgr = addAccountManager(contractManager, contractID, UserName, UserDomain);
                        
                        //Inserting Contract Prices
                        addPrice = insertSalesCampaignPrice(contractID, SalesCampaign, UserName, UserDomain);

                        if (addProv.Equals(1) && addClnt.Equals(1) && addMgr.Equals(1) && addPrice.Equals(1))
                        {
                            mthdResult = 1;
                        }
                        else
                        {
                            mthdResult = -1;
                        }
                        //Insert Contract Documents to respective Folders
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        String msg = "Insert Error: ";
                        msg += ex.Message;
                    }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }
                return mthdResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 clientContract(Int32 contractProviderID, Int32 contractManager, Int32 contractClientID, String clientName, String contractName, Int32 contractType, Int32 contractStatus, DateTime sttDate, DateTime endDate, Int32 SalesCampaign, String UserName, String UserDomain)
        {
            try
            {
                Int32 contractID = -99, addressID = -99, addProv = -99, addClnt = -99, addMgr = -99, mthdResult = -99, addPrice = -99;

                String insertClient = "INSERT INTO [Contract] (ctID, ctrtName, clntAddID, cstatID, ctrtStart, ctrtEnd, cTime, uTime, username, realm, scID) VALUES (@ctID, @ctrtName, @clntAddID, @cstatID, @ctrtStart, @ctrtEnd, @crTime, @updTime, @username, @realm, @scID); SELECT Scope_Identity();";
                String getClntAddID = "SELECT [clntAddID] FROM [ClientAddress] WHERE [clntID] = @clntID AND [dmgID] = @dmgID";

                using (SqlConnection clntConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        clntConn.Open();

                        SqlCommand clntAddID = new SqlCommand(getClntAddID, clntConn);
                        clntAddID.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = contractClientID.ToString();
                        clntAddID.Parameters.AddWithValue("@dmgID", SqlDbType.Int).Value = '2'.ToString();// dmgID = 2 = Main Address

                        using (SqlDataReader readID = clntAddID.ExecuteReader())
                        {
                            try
                            {
                                if (readID.HasRows)
                                {
                                    while (readID.Read())
                                    {
                                        addressID = Convert.ToInt32(readID.GetInt32(0));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readID.Close();
                                readID.Dispose();                                
                            }
                        }

                        SqlCommand clntCmd = new SqlCommand(insertClient, clntConn);
                        clntCmd.Parameters.AddWithValue("@ctID", SqlDbType.Int).Value = contractType.ToString();
                        clntCmd.Parameters.AddWithValue("@ctrtName", SqlDbType.NVarChar).Value = contractName.ToString();
                        clntCmd.Parameters.AddWithValue("@clntAddID", SqlDbType.Int).Value = addressID.ToString();
                        clntCmd.Parameters.AddWithValue("@cstatID", SqlDbType.Int).Value = contractStatus.ToString();
                        clntCmd.Parameters.AddWithValue("@ctrtStart", SqlDbType.DateTime).Value = sttDate.ToString();
                        clntCmd.Parameters.AddWithValue("@ctrtEnd", SqlDbType.DateTime).Value = endDate.ToString();
                        clntCmd.Parameters.AddWithValue("@crTime", SqlDbType.DateTime).Value = DateTime.Now.ToString();
                        clntCmd.Parameters.AddWithValue("@updTime", SqlDbType.DateTime).Value = DateTime.Now.ToString();
                        clntCmd.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        clntCmd.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserDomain.ToString();
                        clntCmd.Parameters.AddWithValue("@scID", SqlDbType.Int).Value = SalesCampaign.ToString();

                        contractID = Convert.ToInt32(clntCmd.ExecuteScalar()); //Inserts contract in table and gets record ID

                        //Inserting Contract Provider into ClientInContract
                        addProv = addContractProvider(contractProviderID, contractID, UserName, UserDomain);

                        //Inserting Contractee into ClientInContract
                        addClnt = addContractClient(contractClientID, contractID, UserName, UserDomain);

                        //Inserting Client Account Manager
                        addMgr = addAccountManager(contractManager, contractID, UserName, UserDomain);

                        //Inserting Contract Prices
                        addPrice = insertSalesCampaignPrice(contractID, SalesCampaign, UserName, UserDomain);

                        if (addProv.Equals(1) && addClnt.Equals(1) && addMgr.Equals(1))
                        {
                            mthdResult = 1;
                        }
                        else
                        {
                            mthdResult = -1;
                        }
                        //Insert Contract Documents to respective Folders
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        String msg = "Insert Error: ";
                        msg += ex.Message;
                    }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }
                return mthdResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 addContractProvider(Int32 provID, Int32 contractID, String UserName, String UserDomain)
        {
            try
            {
                Int32 iReply = -99;
                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;
                
                String insertClientInContract = "INSERT INTO ClientInContract (clntID, ctrtID, cTime, uTime, username, realm) VALUES (@clntID, @ctrtID, @crTime, @updTime, @username, @realm)";

                using (SqlConnection clntConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        clntConn.Open();

                        SqlDataAdapter adapter = new SqlDataAdapter();
                        SqlCommand clntAddID = new SqlCommand(insertClientInContract, clntConn);
                        clntAddID.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = provID.ToString();
                        clntAddID.Parameters.AddWithValue("@ctrtID", SqlDbType.Int).Value = contractID.ToString();
                        clntAddID.Parameters.AddWithValue("@crTime", SqlDbType.DateTime).Value = crTime.ToString();
                        clntAddID.Parameters.AddWithValue("@updTime", SqlDbType.DateTime).Value = updTime.ToString();
                        clntAddID.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        clntAddID.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserDomain.ToString();

                        iReply = Convert.ToInt32(clntAddID.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }            
        }

        private static Int32 addContractClient(Int32 clientID, Int32 contractID, String UserName, String UserDomain)
        {
            try
            {
                Int32 iReply = -99;
                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;

                String insertClientInContract = "INSERT INTO [ClientInContract] (clntID, ctrtID, cTime, uTime, username, realm) VALUES (@clntID, @ctrtID, @crTime, @updTime, @username, @realm)";

                using (SqlConnection clntConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        clntConn.Open();

                        SqlDataAdapter adapter = new SqlDataAdapter();
                        SqlCommand clntAddID = new SqlCommand(insertClientInContract, clntConn);
                        clntAddID.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = clientID.ToString();
                        clntAddID.Parameters.AddWithValue("@ctrtID", SqlDbType.Int).Value = contractID.ToString();
                        clntAddID.Parameters.AddWithValue("@crTime", SqlDbType.DateTime).Value = crTime.ToString();
                        clntAddID.Parameters.AddWithValue("@updTime", SqlDbType.DateTime).Value = updTime.ToString();
                        clntAddID.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        clntAddID.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserDomain.ToString();

                        iReply = Convert.ToInt32(clntAddID.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 addContractPartner(Int32 amgrID, Int32 contractID, String UserName, String UserDomain)
        {
            try
            {
                Int32 iReply = -99;
                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;

                String insertClientInContract = "INSERT INTO ClientInContract (clntID, ctrtID, cTime, uTime, username, realm) VALUES (@clntID, @ctrtID, @crTime, @updTime, @username, @realm)";

                using (SqlConnection clntConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        clntConn.Open();

                        SqlCommand clntAddID = new SqlCommand(insertClientInContract, clntConn);
                        clntAddID.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = amgrID.ToString();
                        clntAddID.Parameters.AddWithValue("@ctrtID", SqlDbType.Int).Value = contractID.ToString();
                        clntAddID.Parameters.AddWithValue("@crTime", SqlDbType.DateTime).Value = crTime.ToString();
                        clntAddID.Parameters.AddWithValue("@updTime", SqlDbType.DateTime).Value = updTime.ToString();
                        clntAddID.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        clntAddID.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserDomain.ToString();

                        iReply = Convert.ToInt32(clntAddID.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 addAccountManager(Int32 clientID, Int32 contractID, String UserName, String UserDomain)
        {
            try
            {
                Int32 iReply = -99;
                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;

                String insertCAM = "INSERT INTO ClientAccountManager(clntID, ctrtID, cTime, uTime, username, realm) VALUES (@clntID, @ctrtID, @crTime, @updTime, @username, @realm)";

                using (SqlConnection clntConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        clntConn.Open();

                        SqlCommand clntAddID = new SqlCommand(insertCAM, clntConn);
                        clntAddID.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = clientID.ToString();
                        clntAddID.Parameters.AddWithValue("@ctrtID", SqlDbType.Int).Value = contractID.ToString();
                        clntAddID.Parameters.AddWithValue("@crTime", SqlDbType.DateTime).Value = crTime.ToString();
                        clntAddID.Parameters.AddWithValue("@updTime", SqlDbType.DateTime).Value = updTime.ToString();
                        clntAddID.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        clntAddID.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserDomain.ToString();

                        iReply = Convert.ToInt32(clntAddID.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        clntConn.Close();
                        clntConn.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 addPrice(Int32 ContractID, Int32 ServiceID, Decimal ServicePrice, Int32 PriceUnit, String UserName, String UserRealm)
        {
            try
            {
                Int32 iReply = -99;
                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;

                String insertData = "INSERT INTO [ServicePrice] ([spCharge], [srvID], [scuID], [ctrtID], [cTime], [uTime], [username], [realm]) VALUES (@spCharge, @srvID, @scuID, @ctrtID, @cTime, @uTime, @username, @realm)";

                using (SqlConnection insCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        insCon.Open();
                        SqlCommand insData = new SqlCommand(insertData, insCon);
                        insData.Parameters.AddWithValue("@spCharge", SqlDbType.Money).Value = ServicePrice.ToString();
                        insData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        insData.Parameters.AddWithValue("@scuID", SqlDbType.Int).Value = PriceUnit.ToString();
                        insData.Parameters.AddWithValue("@ctrtID", SqlDbType.Int).Value = ContractID.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = crTime.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = updTime.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserRealm.ToString();

                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                        if (!iReply.Equals(1))
                        {
                            iReply = -1;
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        insCon.Close();
                        insCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static String getAddressLine(Int32 clntID, Int32 addressID)
        {
            try
            {
                return CLNT.getAddressLine(clntID, addressID);
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }               

        public static DataTable getGlobalContractTable()
        {
            try
            {
                DataTable iReply = new DataTable();
                DataColumn ctrtID = iReply.Columns.Add("ctrtID", typeof(Int32));
                DataColumn ctID = iReply.Columns.Add("ctID", typeof(String));
                DataColumn ctrtName = iReply.Columns.Add("ctrtName", typeof(String));
                DataColumn cstatID = iReply.Columns.Add("cstatID", typeof(String));
                DataColumn ctrtStart = iReply.Columns.Add("ctrtStart", typeof(DateTime));
                DataColumn ctrtEnd = iReply.Columns.Add("ctrtEnd", typeof(DateTime));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [ctrtID], [ctID], [ctrtName], [cstatID], [ctrtStart], [ctrtEnd], [uTime] FROM [Contract] ORDER BY [ctrtEnd]";
                Int32 cttID = 0, type = 0, cstt = 0;
                String cttName = String.Empty, typeName = String.Empty, statName = String.Empty;
                DateTime start = DateTime.Now, end = DateTime.Now, updated = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        cttID = readData.GetInt32(0);
                                        type = readData.GetInt32(1);
                                        typeName = getContractType(type); 
                                        cttName = readData.GetString(2);
                                        cstt = readData.GetInt32(3);
                                        statName = getContractStatus(cstt);
                                        start = readData.GetDateTime(4);
                                        end = readData.GetDateTime(5);
                                        updated = readData.GetDateTime(6);
                                        iReply.Rows.Add(cttID, typeName, cttName, statName, start, end, updated);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getClientContractList(Int32 ContractTypeID, Int32 ClientID)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                List<Int32> cList = new List<Int32>();
                Int32 id = -99, stat = -99;
                String name = String.Empty, status = String.Empty, value = String.Empty;
                DateTime start = DateTime.Now, end = DateTime.Now;
                String selContracts = "SELECT [ctrtID], [ctrtName], [cstatID], [ctrtStart], [ctrtEnd] FROM [Contract] WHERE [ctrtID] = @ctrtID AND [ctID] = @ctID";
                cList = getClientContractIDList(ClientID);
                using(SqlConnection dataCon =  new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        foreach (Int32 item in cList)
                        {
                            SqlCommand getData = new SqlCommand(selContracts, dataCon);
                            getData.Parameters.AddWithValue("@ctrtID", SqlDbType.Int).Value = item.ToString();
                            getData.Parameters.AddWithValue("@ctID", SqlDbType.Int).Value = ContractTypeID.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            id = readData.GetInt32(0);
                                            name = readData.GetString(1);
                                            stat = readData.GetInt32(2);
                                            status = getContractStatus(stat);
                                            start = readData.GetDateTime(3);
                                            end = readData.GetDateTime(4);
                                            value = String.Format("{0} ({1}) - Start Date: {2}; End Date: {3}", name, status, start, end);
                                            iReply.Add(id, value);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getClientContractListForRegistration(Int32 ClientID)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                List<Int32> cList = new List<Int32>();
                Int32 id = -99, stat = -99;
                String name = String.Empty, status = String.Empty, value = String.Empty;
                DateTime start = DateTime.Now, end = DateTime.Now;
                String selContracts = "SELECT [ctrtID], [ctrtName], [cstatID], [ctrtStart], [ctrtEnd] FROM [Contract] WHERE [ctrtID] = @ctrtID";
                cList = getClientContractIDList(ClientID);
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        foreach (Int32 item in cList)
                        {
                            SqlCommand getData = new SqlCommand(selContracts, dataCon);
                            getData.Parameters.AddWithValue("@ctrtID", SqlDbType.Int).Value = item.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            id = readData.GetInt32(0);
                                            name = readData.GetString(1);
                                            stat = readData.GetInt32(2);
                                            status = getContractStatus(stat);
                                            start = readData.GetDateTime(3);
                                            end = readData.GetDateTime(4);
                                            value = String.Format("{0} ({1}) - Start Date: {2}; End Date: {3}", name, status, start, end);
                                            iReply.Add(id, value);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getClientContractStatusForRegistration(Int32 ContractID)
        {
            try
            {
                Int32 iReply = -99;
                String selContracts = "SELECT [cstatID] FROM [Contract] WHERE [ctrtID] = @ctrtID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                            SqlCommand getData = new SqlCommand(selContracts, dataCon);
                            getData.Parameters.AddWithValue("@ctrtID", SqlDbType.Int).Value = ContractID.ToString();
                            using (SqlDataReader readData = getData.ExecuteReader())
                            {
                                try
                                {
                                    if (readData.HasRows)
                                    {
                                        while (readData.Read())
                                        {
                                            iReply = readData.GetInt32(0);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readData.Close();
                                    readData.Dispose();
                                }
                            }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getClientContractJSONTable(Int32 ClientID)
        {
            try
            {
                DataTable infoTable = new DataTable();
                DataColumn ctrtID = infoTable.Columns.Add("ctrtID", typeof(Int32));
                DataColumn ctID = infoTable.Columns.Add("ctID", typeof(String));
                DataColumn ctrtName = infoTable.Columns.Add("ctrtName", typeof(String));
                DataColumn cstatID = infoTable.Columns.Add("cstatID", typeof(String));
                DataColumn ctrtStart = infoTable.Columns.Add("ctrtStart", typeof(String));
                DataColumn ctrtEnd = infoTable.Columns.Add("ctrtEnd", typeof(String));
                DataColumn uTime = infoTable.Columns.Add("uTime", typeof(String));
                String selCtrt = "SELECT [ctrtID] FROM ClientInContract WHERE [clntID] = @clntID";
                String selData = "SELECT [ctrtID], [ctID], [ctrtName], [cstatID], [ctrtStart], [ctrtEnd], [uTime] FROM [Contract] WHERE [ctrtID] = @ctrtID ORDER BY [ctrtEnd]";
                Int32 cttID = 0, type = 0, cstt = 0, id = 0;
                String iReply = String.Empty, cttName = String.Empty, typeName = String.Empty, statName = String.Empty, cStart= String.Empty, cEnd = String.Empty, uLast = String.Empty;
                DateTime start = DateTime.Now, end = DateTime.Now, updated = DateTime.Now;
                ArrayList ctrtList = new ArrayList();
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getCtrt = new SqlCommand(selCtrt, dataCon);
                        getCtrt.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using(SqlDataReader readID = getCtrt.ExecuteReader())
                        {
                            try
                            {
                                if (readID.HasRows)
                                {
                                    while (readID.Read())
                                    {
                                        id = readID.GetInt32(0);
                                        ctrtList.Add(id);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readID.Close();
                                readID.Dispose();
                            }
                        }
                        if (ctrtList.Count > 0)
                        {
                            foreach (var item in ctrtList)
                            {
                                SqlCommand getData = new SqlCommand(selData, dataCon);
                                getData.Parameters.AddWithValue("@ctrtID", SqlDbType.Int).Value = item.ToString();
                                using (SqlDataReader readData = getData.ExecuteReader())
                                {
                                    try
                                    {
                                        if (readData.HasRows)
                                        {
                                            while (readData.Read())
                                            {
                                                cttID = readData.GetInt32(0);
                                                type = readData.GetInt32(1);
                                                typeName = getContractType(type);
                                                cttName = readData.GetString(2);
                                                cstt = readData.GetInt32(3);
                                                statName = getContractStatus(cstt);
                                                start = readData.GetDateTime(4);
                                                cStart = start.ToString("MMM dd yyyy");
                                                end = readData.GetDateTime(5);
                                                cEnd = end.ToString("MMM dd yyyy");
                                                updated = readData.GetDateTime(6);
                                                uLast = String.Format("{0:d/M/yyyy --- HH:mm:ss}", updated);
                                                infoTable.Rows.Add(cttID, typeName, cttName, statName, cStart, cEnd, uLast);
                                                infoTable.AcceptChanges();
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    { throw new System.Exception(ex.ToString()); }
                                    finally
                                    {
                                        readData.Close();
                                        readData.Dispose();
                                    }
                                }
                            }
                        }
                        else
                        {
                            infoTable.Rows.Add(0, "---", "Not Found", "---", DateTime.Now, DateTime.Now, DateTime.Now);
                            infoTable.AcceptChanges();
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply = getJSonString(infoTable);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getClientContractTable(Int32 ClientID)
        {
            try
            {
                System.Data.DataTable infoTable = new System.Data.DataTable();
                DataColumn ctrtID = infoTable.Columns.Add("ctrtID", typeof(Int32));
                DataColumn ctID = infoTable.Columns.Add("ctID", typeof(String));
                DataColumn ctrtName = infoTable.Columns.Add("ctrtName", typeof(String));
                DataColumn cstatID = infoTable.Columns.Add("cstatID", typeof(String));
                DataColumn ctrtStart = infoTable.Columns.Add("ctrtStart", typeof(String));
                DataColumn ctrtEnd = infoTable.Columns.Add("ctrtEnd", typeof(String));
                DataColumn uTime = infoTable.Columns.Add("uTime", typeof(String));
                String selCtrt = "SELECT [ctrtID] FROM [ClientInContract] WHERE [clntID] = @clntID";
                String selData = "SELECT [ctrtID], [ctID], [ctrtName], [cstatID], [ctrtStart], [ctrtEnd], [uTime] FROM [Contract] WHERE [ctrtID] = @ctrtID ORDER BY [ctrtEnd]";
                Int32 cttID = 0, type = 0, cstt = 0, id = 0;
                String cttName = String.Empty, typeName = String.Empty, statName = String.Empty, cStart = String.Empty, cEnd = String.Empty, uLast = String.Empty;
                DateTime start = DateTime.Now, end = DateTime.Now, updated = DateTime.Now;
                ArrayList ctrtList = new ArrayList();
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getCtrt = new SqlCommand(selCtrt, dataCon);
                        getCtrt.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readID = getCtrt.ExecuteReader())
                        {
                            try
                            {
                                if (readID.HasRows)
                                {
                                    while (readID.Read())
                                    {
                                        id = readID.GetInt32(0);
                                        ctrtList.Add(id);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readID.Close();
                                readID.Dispose();
                            }
                        }
                        if (ctrtList.Count > 0)
                        {
                            foreach (var item in ctrtList)
                            {
                                SqlCommand getData = new SqlCommand(selData, dataCon);
                                getData.Parameters.AddWithValue("@ctrtID", SqlDbType.Int).Value = item.ToString();
                                using (SqlDataReader readData = getData.ExecuteReader())
                                {
                                    try
                                    {
                                        if (readData.HasRows)
                                        {
                                            while (readData.Read())
                                            {
                                                cttID = readData.GetInt32(0);
                                                type = readData.GetInt32(1);
                                                typeName = getContractType(type);
                                                cttName = readData.GetString(2);
                                                cstt = readData.GetInt32(3);
                                                statName = getContractStatus(cstt);
                                                start = readData.GetDateTime(4);
                                                cStart = start.ToString("MMM dd yyyy");
                                                end = readData.GetDateTime(5);
                                                cEnd = end.ToString("MMM dd yyyy");
                                                updated = readData.GetDateTime(6);
                                                uLast = String.Format("{0:d/M/yyyy --- HH:mm:ss}", updated);
                                                infoTable.Rows.Add(cttID, typeName, cttName, statName, cStart, cEnd, uLast);
                                                infoTable.AcceptChanges();
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    { throw new System.Exception(ex.ToString()); }
                                    finally
                                    {
                                        readData.Close();
                                        readData.Dispose();
                                    }
                                }
                            }
                        }
                        else
                        {
                            infoTable.Rows.Add(0, "---", "Not Found", "---", DateTime.Now, DateTime.Now, DateTime.Now);
                            infoTable.AcceptChanges();
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return infoTable;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static DataTable getContractsDataGlobal()
        {
            DataSet dsData = new DataSet();
            DataTable dtData = new DataTable();

            try
            {
                dtData.Columns.Add("Status", typeof(string));
                dtData.Columns.Add("Count", typeof(int));
                
                Int32 id = 0, cid = 0, reqRenewal = 0, sttNegotiate = 0, valid = 0;
                Double diff = 0;
                DateTime end = DateTime.Now, today = DateTime.Now;
                String ctrtStat = String.Empty;
                String selData = "SELECT [ctrtID], [ctrtEnd] FROM Contract WHERE [cstatID] = '1'";
                var list = new List<KeyValuePair<Int32, DateTime>>();
                var values = new List<KeyValuePair<String, Int32>>();
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        end = readData.GetDateTime(1);
                                        KeyValuePair<Int32, DateTime> value = new KeyValuePair<Int32, DateTime>(id, end);
                                        list.Add(value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                foreach (var pair in list)
                {
                    cid = pair.Key;
                    diff = (Convert.ToDateTime(pair.Value) - Convert.ToDateTime(today)).TotalDays;
                    if (diff < 90)
                    {
                        reqRenewal++;
                    }
                    else if (diff > 90 && diff < 180)
                    {
                        sttNegotiate++;
                    }
                    else
                    {
                        valid++;
                    }
                }

                dtData.Rows.Add("ReNegotiate", reqRenewal);
                dtData.Rows.Add("Expiring Soon", sttNegotiate);
                dtData.Rows.Add("Valid", valid);

                dsData.Tables.Add(dtData);

                return dsData.Tables[0];
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getContractsDataGlobalList()
        {                                    
            try
            {
                List<Int32> iReply = new List<Int32>();
                Int32 id = 0, cid = 0, reqRenewal = 0, sttNegotiate = 0, valid = 0;
                Double diff = 0;
                DateTime end = DateTime.Now, today = DateTime.Now;
                String ctrtStat = String.Empty;
                String selData = "SELECT [ctrtID], [ctrtEnd] FROM Contract WHERE [cstatID] = '1'";
                var list = new List<KeyValuePair<Int32, DateTime>>();
                var values = new List<KeyValuePair<String, Int32>>();
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        end = readData.GetDateTime(1);
                                        KeyValuePair<Int32, DateTime> value = new KeyValuePair<Int32, DateTime>(id, end);
                                        list.Add(value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                foreach (var pair in list)
                {
                    cid = pair.Key;
                    diff = (Convert.ToDateTime(pair.Value) - Convert.ToDateTime(today)).TotalDays;
                    if (diff < 90)
                    {
                        reqRenewal++;
                    }
                    else if (diff > 90 && diff < 180)
                    {
                        sttNegotiate++;
                    }
                    else
                    {
                        valid++;
                    }
                }

                iReply.Add(reqRenewal);
                iReply.Add(sttNegotiate);
                iReply.Add(valid);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<String> getContractInfo(Int32 ContractID)
        {
            try
            {
                List<String> iReply = new List<String>();
                String selData = "SELECT [ctrtName], [cstatID], [ctrtStart], [ctrtEnd] FROM [Contract] WHERE [ctrtID] = @ctrtID";
                String name = String.Empty;
                Int32 status = -99;
                DateTime start, end;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ctrtID", SqlDbType.Int).Value = ContractID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        name = readData.GetString(0);
                                        iReply.Add(name);
                                        status = readData.GetInt32(1);
                                        iReply.Add(Convert.ToString(status));
                                        start = readData.GetDateTime(2);
                                        iReply.Add(Convert.ToString(start));
                                        end = readData.GetDateTime(3);
                                        iReply.Add(Convert.ToString(end));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getContractTypeList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [ctID], [ctName] FROM [ContractType] ORDER BY [ctName]";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getContractType(Int32 contractTypeID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [ctName] FROM ContractType WHERE [ctID] = @ctID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ctID", SqlDbType.Int).Value = contractTypeID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getContractStatusList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [cstatID], [cstatValue] FROM ContractStatus ORDER BY [cstatValue]";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getContractStatus(Int32 contractStatusID)
        {
            try
            {
                String iReply = String.Empty;
                String selData = "SELECT [cstatValue] FROM [ContractStatus] WHERE [cstatID] = @cstatID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@cstatID", SqlDbType.Int).Value = contractStatusID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getGlobalContractStatus()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [cstatID], [cstatValue] FROM [ContractStatus] ORDER BY [cstatValue]";
                Int32 id = -99;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getClientContractIDList(Int32 ClientID)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                String selData = "SELECT [ctrtID] FROM [ClientInContract] WHERE [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply.Add(readData.GetInt32(0));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getContractsList()
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [ctrtID], [ctrtName] FROM [Contract] ORDER BY [ctrtName]";
                Int32 id = 0;
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }                

        public static System.Data.DataTable getPriceListTable(Int32 ContractID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn spID = iReply.Columns.Add("spID", typeof(Int32));
                DataColumn spCharge = iReply.Columns.Add("spCharge", typeof(Decimal));
                DataColumn srvID = iReply.Columns.Add("srvID", typeof(String));
                DataColumn scuID = iReply.Columns.Add("scuID", typeof(String));
                DataColumn dateThru = iReply.Columns.Add("dateThru", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                Int32 id = 0, sID = 0, cuID = 0;
                Decimal charge = 0.00M;
                String cU = String.Empty, srvName = String.Empty;
                DateTime uT = DateTime.Now, thru = DateTime.Now;
                thru = getContractLastDate(ContractID);
                String selData = "SELECT [spID], [spCharge], [srvID], [scuID], [uTime] FROM [ServicePrice] WHERE ([ctrtID] = @ctrtID)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ctrtID", SqlDbType.Int).Value = ContractID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        charge = readData.GetDecimal(1);
                                        sID = readData.GetInt32(2);
                                        srvName = SRV.getServiceName(sID);
                                        cuID = readData.GetInt32(3);
                                        cU = SRV.getServiceChargeUnit(cuID);                                        
                                        uT = readData.GetDateTime(4);
                                        iReply.Rows.Add(id, charge, srvName, cU, thru, uT);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static DateTime getContractLastDate(Int32 ContractID)
        {
            DateTime iReply = DateTime.Now;
            String selData = "SELECT [ctrtEnd] FROM [Contract] WHERE [ctrtID] = @ctrtID";
            using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
            {
                dataCon.Open();
                SqlCommand getData = new SqlCommand(selData, dataCon);
                getData.Parameters.AddWithValue("@ctrtID", SqlDbType.Int).Value = ContractID.ToString();
                using (SqlDataReader readData = getData.ExecuteReader())
                {
                    if (readData.HasRows)
                    {
                        while (readData.Read())
                        {
                            iReply = readData.GetDateTime(0);
                        }
                    }
                }
            }
            return iReply;
        }

        public static Int32 insertSalesCampaignPrice(Int32 ContractID, Int32 SalesCampaignID, String UserName, String UserDomain)
        {
            try
            {
                Int32 iReply = -99, priceUnit = -99;
                Decimal reducer, ctrtPrice, stdPrice;
                String scName = String.Empty;
                scName = SL.getCampaignNameFromID(SalesCampaignID);
                Dictionary<Int32, String> ServiceGroups = SRV.getSrvGroupList();
                foreach (Int32 gid in ServiceGroups.Keys)
                {
                    Dictionary<Int32, String> ServicesInGroup = SRV.getServiceList(gid);
                    foreach (Int32 sid in ServicesInGroup.Keys)
                    {
                        List<String> srvPrice = SRV.getStandardPriceWithUnitID(gid, sid);
                        stdPrice = Convert.ToDecimal(srvPrice[0]);
                        priceUnit = Convert.ToInt32(srvPrice[1]);
                        reducer = SL.getServicePricerReducer(gid, sid, SalesCampaignID);
                        if(reducer.Equals(-99.00M))
                        {
                            ctrtPrice = stdPrice;                            
                        }
                        else
                        {
                            if (reducer.Equals(100))
                            {
                                ctrtPrice = 0.00M;
                            }
                            else
                            {
                                ctrtPrice = Decimal.Multiply(stdPrice, Decimal.Divide(reducer, 100));
                            }
                        }
                        iReply = addPrice(ContractID, sid, ctrtPrice, priceUnit, UserName, UserDomain);
                        if (iReply.Equals(-1))
                        {
                            break;
                        }                        
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 updateContractStatus(Int32 ProviderID, Int32 PartnerID, Int32 ContractID, Int32 StatusID, String LoginName)
        {
            try
            {
                Int32 iReply = -99;
                String updData = "Update [Contract] SET [cstatID] = @cstatID WHERE [ctrtID] = @ctrtID";
                List<String> contractInfo = getContractInfo(ContractID);
                String[] userRoles = Roles.GetRolesForUser(LoginName);
                String roleName = Convert.ToString(userRoles.GetValue(0));
                String contractName = contractInfo[0];
                Int32 contractStatus = Convert.ToInt32(contractInfo[1]);
                DateTime startDate = Convert.ToDateTime(contractInfo[2]);
                DateTime endDate = Convert.ToDateTime(contractInfo[3]);
                if (contractStatus.Equals(StatusID))
                {
                    iReply = -1;
                }
                else if (!roleName.Equals("Director"))
                {
                    iReply = -2;
                }
                else
                {                    
                        using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                        {
                            try
                            {
                                dataCon.Open();
                                SqlCommand updateData = new SqlCommand(updData, dataCon);
                                updateData.Parameters.AddWithValue("@cstatID", SqlDbType.Int).Value = StatusID.ToString();
                                updateData.Parameters.AddWithValue("@ctrtID", SqlDbType.Int).Value = ContractID.ToString();
                                iReply = Convert.ToInt32(updateData.ExecuteNonQuery());
                                if (!iReply.Equals(1))
                                {
                                    iReply = -3;
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                dataCon.Close();
                                dataCon.Dispose();
                            }
                        }                    
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        //This code need to be completed for writing the contract
        private static Int32 writeContract(Int32 contractType, Int32 providerID, Int32 clientID, Int32 addressID, String repName, String repDesignation, DateTime sD, DateTime eD)
        {
            try
            {
                Int32 mthdResult = -99;
                String clntAddressLine = null;
                String contFile = null;
                String newCont = null;

                clntAddressLine = Convert.ToString(getAddressLine(clientID, addressID));

                if (contractType.Equals(1)) /* Demo Contract */
                {
                    contFile = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/StandardDemoContract.txt");
                    newCont = File.ReadAllText(contFile);
                }
                else if (contractType.Equals(2)) /* Partnership Contract */
                {
                    contFile = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/StandardPartnershipContract.txt");
                    newCont = File.ReadAllText(contFile);
                }
                else /* Services Contract */
                {
                    contFile = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/StandardServicesContract.txt");
                    newCont = File.ReadAllText(contFile);
                }

                ////newCont = newCont.Replace("##ClientName##", clientName);
                //////newCont = newCont.Replace("##ClientAddress##", addressLine);
                ////newCont = newCont.Replace("##RepName##", repName);
                ////newCont = newCont.Replace("##RepDesignation##", repDesignation);
                ////newCont = newCont.Replace("##RepClientName##", clientName);
                ////newCont = newCont.Replace("##StartDate##", Convert.ToString(sD));
                ////newCont = newCont.Replace("##EndDate##", Convert.ToString(eD));

                //if (ctyp.Equals(2)) /* Partnership Contract */
                //{
                //    DataView dvRealm = (DataView)sdsAddContractPartner.Select(DataSourceSelectArguments.Empty);
                //    coName = Convert.ToString(dvRealm.Table.Rows[0]["clntRealm"]);
                //}
                //else /*Service or Demo Contract*/
                //{
                //    DataView dvRealm = (DataView)sdsAddContractClient.Select(DataSourceSelectArguments.Empty);
                //    coName = Convert.ToString(dvRealm.Table.Rows[0]["clntRealm"]);
                //}
                //TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                //coName = ti.ToTitleCase(coName);
                //genDirectory = System.Web.Configuration.WebConfigurationManager.AppSettings["GenPath"].ToString();
                //gsrvCTRT = (String.Format("{0}\\{1}\\{2}\\{3}\\", genDirectory, coName, "Contracts", "Service_Contracts"));
                //DirectoryInfo serviceFolder = new DirectoryInfo(gsrvCTRT);
                //if (!Directory.Exists(gsrvCTRT))
                //{
                //    serviceFolder.Create(); /* New Services Contract Sub-Folder in Contracts Folder and adding Contract File to it */
                //}
                //System.IO.File.WriteAllText(gsrvCTRT + @"\Service_Contract.txt", newCont);

                return mthdResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getServicePricerForContract(Int32 ServiceID, Int32 ContractID)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                Int32 id = -99, chargeID = -99;
                Decimal price = -99.00M;
                String value = String.Empty, unit = String.Empty;
                String selData = "SELECT [spID], [spCharge], [scuID] FROM [ServicePrice] WHERE [ctrtID] = @ctrtID AND [srvID] = @srvID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ctrtID", SqlDbType.Int).Value = ContractID.ToString();
                        getData.Parameters.AddWithValue("@srvID", SqlDbType.Int).Value = ServiceID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        price = readData.GetDecimal(1);
                                        chargeID = readData.GetInt32(2);
                                        unit = SmartsVer1.Helpers.DemogHelpers.ServicesConfig.getServiceChargeUnit(chargeID);
                                        value = String.Format("{0} {1}", price, unit);
                                        iReply.Add(id, value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getJSonString(System.Data.DataTable infoTable)
        {
            try
            {
                String JSONString = String.Empty;
                JSONString = JS.JsonConvert.SerializeObject(infoTable);
                return JSONString;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}