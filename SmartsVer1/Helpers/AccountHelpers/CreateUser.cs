﻿using System;
using System.IO;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Globalization;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Collections;
using System.Web.Configuration;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using SmartsVer1.Helpers.AccountHelpers;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using MAP = SmartsVer1.Helpers.ChartHelpers.MapBox;

namespace SmartsVer1.Helpers.AccountHelpers
{
    /// <summary>
    /// A class that helps create system client user account
    /// </summary>
    public class CreateUser
    {
        const String appID = "047092b7-d10b-42da-ae7a-eb223509869d";
        
        static void Main()
        { }

        public static Int32 createUser(Int32 clientID, String FirstName, String MiddleName, String LastName, String Password, String Phone, String Email, String Role, Int32 PhoneType, String Designation, String St1, String St2, String St3, Int32 Region, Int32 SubRegion, Int32 Country, Int32 State, Int32 County, Int32 City, Int32 Zip, Int32 GenderID)
        {
            try
            {
                Int32 result = -99;
                Int32 uicResult = -99; //User in Client insertion
                Int32 uigResult = -99; //User in Gender
                String FN = null, MN = null, LN = null, usrName = null, domain = null;
                String cityN = DMG.getCityName(City);
                String stateN = DMG.getStateName(State);
                String zipC = DMG.getZipCode(Zip);
                Int32 cType = -99;
                Guid newUser = new Guid();                
                DateTime crTime = DateTime.Now;
                DateTime updTime = DateTime.Now;
                Decimal Lati = -99.000000000000M;
                Decimal Longi = -99.000000000000M;

                TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                FN = ti.ToTitleCase(FirstName);
                MN = ti.ToTitleCase(MiddleName);
                LN = ti.ToTitleCase(LastName);

                String connString = WebConfigurationManager.ConnectionStrings["FEUsersDB"].ConnectionString;

                /* Selecting Client Realm from Users DB */
                domain = getDomain(clientID);     
           
                /* Selecting Client Type from Users DB */
                cType = getClientType(clientID);

                /* Creating User Name with Realm */
                usrName = getUserName(FN, MN, LN, domain);
                //usrName = String.Format("{0}.{1}@{2}", FN, LN, domain);

                /* User's profile address Latitude and Longitude */
                String uAddress = St1 + " " + cityN + " " + stateN + " " + zipC;
                List<Double> latlng = MAP.getLatLngFromAddress(uAddress);
                
                Longi = Convert.ToDecimal(latlng[0]);
                Lati = Convert.ToDecimal(latlng[1]);

                if (String.IsNullOrEmpty(usrName))
                {
                    result = -1;
                    return result;
                }
                else
                {
                    /* Adding User to Memberhsip DB */                    
                    MembershipCreateStatus createStatus;
                    Membership.CreateUser(usrName, Password, Email, null, null, false, out createStatus);
                    MembershipUser usr = Membership.GetUser(usrName);
                    usr.IsApproved = false;
                    
                    switch (createStatus)
                    {
                        case MembershipCreateStatus.Success:
                            {
                                /* Adding New user to selected Role */
                                Roles.AddUserToRole(usrName, Role);

                                /* Get newly created user's UserId */
                                String selectCommand = "SELECT [ApplicationId], [UserId], [UserName] FROM [vw_aspnet_Users] WHERE ([UserName] = @UserName)";


                                SqlConnection clntConn = new SqlConnection(connString);

                                SqlCommand getUserID = new SqlCommand(selectCommand, clntConn);
                                getUserID.Parameters.AddWithValue("@UserName", SqlDbType.NVarChar).Value = usrName.ToString();

                                clntConn.Open();

                                SqlDataReader getUID = getUserID.ExecuteReader();
                                if (getUID.HasRows)
                                {
                                    while (getUID.Read())
                                    {
                                        newUser = getUID.GetGuid(1);
                                    }
                                }
                                getUID.Close();
                                getUID.Dispose();

                                /* Updating New User's Profile Information */
                                UP profile = UP.GetUserProfile(usrName);
                                
                                profile.FirstName = FN;
                                profile.MiddleName = MN;
                                profile.LastName = LN;
                                profile.upPhone = Phone;
                                profile.dmgID = Convert.ToString(PhoneType);
                                profile.UserId = Convert.ToString(newUser);
                                profile.clntID = Convert.ToString(clientID);
                                profile.ctypeID = Convert.ToString(cType);
                                profile.designation = Convert.ToString(Designation);
                                profile.st1 = Convert.ToString(St1);
                                profile.st2 = Convert.ToString(St2);
                                profile.st3 = Convert.ToString(St3);
                                profile.reg = Convert.ToString(Region);
                                profile.sreg = Convert.ToString(SubRegion);
                                profile.country = Convert.ToString(Country);
                                profile.state = Convert.ToString(State);
                                profile.county = Convert.ToString(County);
                                profile.city = Convert.ToString(City);
                                profile.zip = Convert.ToString(Zip);
                                profile.Latitude = Convert.ToString(Lati);
                                profile.Longitude = Convert.ToString(Longi);
                                profile.Save();
                                /* Adding new user Gender */
                                uigResult = insertUserGender(newUser, GenderID);
                                /* Adding new user to User-In-Client */
                                uicResult = insertUserInClient(newUser, clientID);
                                clntConn.Close(); 
                                
                                break;
                            }
                        case MembershipCreateStatus.DuplicateEmail:
                            {
                                uicResult = -1;
                                break;
                            }
                        case MembershipCreateStatus.DuplicateUserName:
                            {
                                uicResult = -2;
                                break;
                            }
                        case MembershipCreateStatus.InvalidEmail:
                            {
                                uicResult = -3;
                                break;
                            }
                        case MembershipCreateStatus.InvalidUserName:
                            {
                                uicResult = -4;
                                break;
                            }
                        case MembershipCreateStatus.InvalidPassword:
                            {
                                uicResult = -5;
                                break;
                            }
                        case MembershipCreateStatus.ProviderError:
                            {
                                uicResult = -6;
                                break;
                            }
                        case MembershipCreateStatus.UserRejected:
                            {
                                uicResult = -7;
                                break;
                            }
                        default:
                            {
                                uicResult = -8;                                
                                break;
                            }
                    }
                    return uicResult;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static Dictionary<System.Guid, String> getAllClientRoles()
        {
            try
            {
                Dictionary<System.Guid, String> iReply = new Dictionary<System.Guid, String>();
                System.Guid id = new Guid();
                String[] roleList = { "Coordinator", "Director", "Fieldhand", "LabTech", "Reviewer", "Support" };
                String selData = "SELECT [RoleId] FROM [aspnet_Roles] WHERE [RoleName] = @RoleName";                
                foreach (String rl in roleList)
                {
                    try
                    {
                        using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                        {
                            try
                            {
                                dataCon.Open();
                                SqlCommand getData = new SqlCommand(selData, dataCon);
                                getData.Parameters.AddWithValue("@RoleName", SqlDbType.NVarChar).Value = rl.ToString();
                                using (SqlDataReader readData = getData.ExecuteReader())
                                {
                                    try
                                    {
                                        if (readData.HasRows)
                                        {
                                            while (readData.Read())
                                            {
                                                id = readData.GetGuid(0);
                                                iReply.Add(id, rl);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    { throw new System.Exception(ex.ToString()); }
                                    finally
                                    {
                                        readData.Close();
                                        readData.Dispose();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                dataCon.Close();
                                dataCon.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<System.Guid, String> getAllRoles()
        {
            try
            {
                Dictionary<System.Guid, String> iReply = new Dictionary<System.Guid, String>();
                String selData = "SELECT [RoleId], [RoleName] FROM [aspnet_Roles] WHERE [ApplicationId] = @ApplicationId ORDER BY [RoleName]";
                System.Guid id = new System.Guid();
                String name = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@ApplicationId", SqlDbType.UniqueIdentifier).Value = appID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetGuid(0);
                                        name = readData.GetString(1);
                                        iReply.Add(id, name);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static Int32 getClientType(Int32 clientID)
        {
            try
            {
                Int32 result = -99;
                String connString = WebConfigurationManager.ConnectionStrings["FEUsersDB"].ConnectionString;
                String selectType = "SELECT ctypID FROM Client WHERE clntID = @clntID";

                SqlConnection clntConn = new SqlConnection(connString);
                clntConn.Open();

                SqlCommand getType = new SqlCommand(selectType, clntConn);
                getType.Parameters.AddWithValue("clntID", SqlDbType.Int).Value = clientID.ToString();

                SqlDataReader readType = getType.ExecuteReader();
                if (readType.HasRows)
                {
                    while (readType.Read())
                    {
                        result = readType.GetInt32(0);
                    }
                }
                readType.Close();
                readType.Dispose();

                clntConn.Close();

                return result;
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static DataTable getClientUserPerRoleGlobal()
        {
            DataSet dsData = new DataSet();
            try
            {
                //Variables
                string SelectQuery = "select R.RoleName roleName , count(R.RoleId) roleCount from dbo.aspnet_Users AS U , dbo.aspnet_Roles AS R , dbo.aspnet_UsersInRoles AS UR , dbo.UserInClient AS UC";
                SelectQuery += " where U.UserId = UR.UserId and UR.UserId = UC.UserId and UR.RoleId = R.RoleId";
                SelectQuery += " group by R.RoleName , R.RoleId order by R.RoleName";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        SqlCommand getUsers = new SqlCommand(SelectQuery, dataCon);
                        using (SqlDataAdapter sqlCmd = new SqlDataAdapter(getUsers))
                        {
                            try
                            {
                                dataCon.Open();
                                sqlCmd.Fill(dsData);
                                dataCon.Close();
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }

                }

                return dsData.Tables[0];

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }


        public static System.Data.DataTable getClientUserPerRole(Int32 ClientID)
        {
            DataSet dsData = new DataSet();
            try
            {
                //Variables
                string SelectQuery = "select R.RoleName roleName , count(R.RoleId) roleCount from dbo.aspnet_Users AS U , dbo.aspnet_Roles AS R , dbo.aspnet_UsersInRoles AS UR , dbo.UserInClient AS UC";
                SelectQuery += " where U.UserId = UR.UserId and UR.UserId = UC.UserId and UR.RoleId = R.RoleId";
                SelectQuery += " and UC.clntID = @clntID";
                SelectQuery += " and U.UserName not like 'Fibonacci.Admin%' ";
                SelectQuery += " group by R.RoleName , R.RoleId order by R.RoleName";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        SqlCommand getUsers = new SqlCommand(SelectQuery, dataCon);
                        getUsers.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        //using (SqlDataReader readUsers = getUsers.ExecuteReader())
                        using (SqlDataAdapter sqlCmd = new SqlDataAdapter(getUsers))
                        {
                            try
                            {
                                dataCon.Open();
                                sqlCmd.Fill(dsData);
                                dataCon.Close();
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }

                }

                return dsData.Tables[0];

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<System.Guid, String> getClientUsersList(Int32 ClientID)
        {
            try
            {
                Dictionary<System.Guid, String> iReply = new Dictionary<System.Guid, String>();
                String fName = String.Empty, lName = String.Empty, usrName = String.Empty, rlName = String.Empty, nameInfo = String.Empty;
                ArrayList users = new ArrayList();

                String selClientUsers = "SELECT [UserId] FROM [UserInClient] WHERE [clntID] = @clntID";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getClientUsers = new SqlCommand(selClientUsers, dataCon);
                        getClientUsers.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readClientUsers = getClientUsers.ExecuteReader())
                        {
                            try
                            {
                                if (readClientUsers.HasRows)
                                {
                                    while (readClientUsers.Read())
                                    {
                                        users.Add(readClientUsers.GetGuid(0));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readClientUsers.Close();
                                readClientUsers.Dispose();
                            }
                        }
                        foreach (System.Guid uGuid in users)
                        {
                            usrName = getUserNameFromId(uGuid);
                            fName = UP.GetUserProfile(usrName).FirstName;
                            lName = UP.GetUserProfile(usrName).LastName;
                            //Getting User Role Name
                            String[] roles = Roles.GetRolesForUser(usrName);
                            rlName = Convert.ToString(roles.GetValue(0));
                            nameInfo = lName + ", " + fName + " - " + rlName + " - ( " + usrName + " )";
                            iReply.Add(uGuid, nameInfo);
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<System.Guid, String> getUserListForRole(Int32 ClientID, System.Guid UserRole)
        {
            try
            {
                Dictionary<System.Guid, String> iReply = new Dictionary<System.Guid, String>();
                String[] roleList = { "Coordinator", "Director", "Fieldhand", "LabTech", "Reviewer", "Support" };
                ArrayList users = getClientUserIDList(ClientID);//Get all user guids
                foreach(System.Guid gd in users)
                {
                    String usrName = getUserNameFromId(gd);
                    String ur = getRoleNameFromRoleId(UserRole);
                    if (Roles.IsUserInRole(usrName, ur))
                    {
                        iReply.Add(gd, usrName);
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getClientUsersTable(Int32 ClientID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn UserId = iReply.Columns.Add("UserId", typeof(Guid));
                DataColumn firstName = iReply.Columns.Add("firstName", typeof(String));
                DataColumn lasttName = iReply.Columns.Add("lastName", typeof(String));
                DataColumn userName = iReply.Columns.Add("userName", typeof(String));
                DataColumn roleName = iReply.Columns.Add("roleName", typeof(String));
                DataColumn enabled = iReply.Columns.Add("enabled", typeof(Int32));
                DataColumn locked = iReply.Columns.Add("locked", typeof(Int32));
                String fName = String.Empty, lName = String.Empty, usrName = String.Empty, rlName = String.Empty;
                Int32 enb = 0, lkd = 0;
                DateTime uT = DateTime.Now;
                ArrayList users = new ArrayList();
                users = getClientUserIDList(ClientID);
                foreach (System.Guid uGuid in users)
                {
                    usrName = getUserNameFromId(uGuid);
                    fName = UP.GetUserProfile(usrName).FirstName;
                    lName = UP.GetUserProfile(usrName).LastName;
                    //Getting User Role Name
                    String[] roles = Roles.GetRolesForUser(usrName);
                    rlName = Convert.ToString(roles.GetValue(0));
                    MembershipUser selUser = Membership.GetUser(usrName);
                    enb = Convert.ToInt32(selUser.IsApproved);
                    lkd = Convert.ToInt32(selUser.IsLockedOut);
                    if (!rlName.Equals("feSupport"))
                    {
                        iReply.Rows.Add(uGuid, fName, lName, usrName, rlName, enb, lkd);
                        iReply.AcceptChanges();
                    }
                    iReply.DefaultView.Sort = "lastName";
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static ArrayList getClientUserIDList(Int32 ClientID)
        {
            try
            {
                ArrayList iReply = new ArrayList();
                String selClientUsers = "SELECT [UserId] FROM [UserInClient] WHERE [clntID] = @clntID";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getClientUsers = new SqlCommand(selClientUsers, dataCon);
                        getClientUsers.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readClientUsers = getClientUsers.ExecuteReader())
                        {
                            try
                            {
                                if (readClientUsers.HasRows)
                                {
                                    while (readClientUsers.Read())
                                    {
                                        iReply.Add(readClientUsers.GetGuid(0));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readClientUsers.Close();
                                readClientUsers.Dispose();
                            }
                        }                        
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Guid, String> getDevelopers()
        {
            try
            {
                Dictionary<Guid, String> iReply = new Dictionary<Guid, String>();
                String selStat = "SELECT [UserId] FROM [AppDevelopers]";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        Guid dID = Guid.Empty;
                        String dName = null;
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selStat, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        dID = readData.GetGuid(0);
                                        dName = getFirstLastFromUserId(dID);
                                        iReply.Add(dID, dName);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Guid, String> getFESupportRepresentatives()
        {
            try
            {
                Dictionary<Guid, String> iReply = new Dictionary<Guid, String>();
                Guid dID = Guid.Empty;
                String usr = String.Empty;
                System.Guid roleID = getRoleIdFromRoleName("feSupport");
                String selStat = "SELECT [UserId] FROM [aspnet_UsersInRoles] WHERE [RoleId] = @RoleId";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {                        
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selStat, dataCon);
                        getData.Parameters.AddWithValue("@RoleId", SqlDbType.UniqueIdentifier).Value = roleID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        dID = readData.GetGuid(0);
                                        usr = getUserNameFromId(dID);                                        
                                        iReply.Add(dID, usr);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getDomain(Int32 clientID)
        {
            try
            {
                String result = null;
                String connString = WebConfigurationManager.ConnectionStrings["FEUsersDB"].ConnectionString;
                String selectDomain = "SELECT clntRealm FROM Client WHERE clntID = @clntID";

                SqlConnection clntConn = new SqlConnection(connString);
                clntConn.Open();

                SqlCommand getDomain = new SqlCommand(selectDomain, clntConn);
                getDomain.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = clientID.ToString();

                SqlDataReader readDomain = getDomain.ExecuteReader();
                if (readDomain.HasRows)
                {
                    while (readDomain.Read())
                    {
                        result = readDomain.GetString(0);
                    }
                }
                readDomain.Close();
                readDomain.Dispose();

                clntConn.Close();

                return result;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getFirstLastFromUserId(Guid UserId)
        {
            try
            {
                String iReply = null;
                String uName = getUserNameFromId(UserId);
                UserProfile UP = UserProfile.GetUserProfile(uName);
                iReply = UP.LastName + ", " + UP.FirstName;
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<Int32> getLocationUserCount(Int32 ClientID, Int32 AddressID, String ClientDBString)
        {
            try
            {
                List<Int32> iReply = new List<Int32>();
                List<Guid> usersList = getUsersForLocation(AddressID, ClientDBString);
                String roleName = String.Empty, userName = String.Empty;
                Int32 dirCount = 0, crdCount = 0, fhCount = 0, lbCount = 0, rwrCount = 0;
                foreach(var item in usersList)
                {
                    if(!item.Equals(Guid.Empty))
                    {    
                        userName = getUserNameFromId(item);
                        roleName = Convert.ToString(Roles.GetRolesForUser(userName));
                        switch (roleName)
                        {
                            case "Director": {
                                dirCount++;
                                break;
                            }
                            case "Coordinator": {
                                crdCount++;
                                break;
                            }
                            case "Fieldhand": {
                                fhCount++;
                                break;
                            }
                            case "LabTech": {
                                lbCount++;
                                break;
                                }
                            case "Reviewer": {
                                rwrCount++;
                                break;
                            }
                            default: {
                                break;
                            }
                        }
                    }
                }
                iReply.Add(dirCount);
                iReply.Add(crdCount);
                iReply.Add(fhCount);
                iReply.Add(lbCount);
                iReply.Add(rwrCount);
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getReviewersTable(Int32 ClientID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn UserId = iReply.Columns.Add("UserId", typeof(Guid));
                DataColumn firstName = iReply.Columns.Add("firstName", typeof(String));
                DataColumn lasttName = iReply.Columns.Add("lastName", typeof(String));
                DataColumn userName = iReply.Columns.Add("userName", typeof(String));
                DataColumn roleName = iReply.Columns.Add("roleName", typeof(String));
                DataColumn enabled = iReply.Columns.Add("enabled", typeof(Int32));
                DataColumn locked = iReply.Columns.Add("locked", typeof(Int32));
                String fName = String.Empty, lName = String.Empty, usrName = String.Empty, rlName = String.Empty;
                Int32 enb = 0, lkd = 0;
                DateTime uT = DateTime.Now;
                ArrayList users = new ArrayList();

                String selClientUsers = "SELECT [UserId] FROM [UserInClient] WHERE [clntID] = @clntID";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getClientUsers = new SqlCommand(selClientUsers, dataCon);
                        getClientUsers.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readClientUsers = getClientUsers.ExecuteReader())
                        {
                            try
                            {
                                if (readClientUsers.HasRows)
                                {
                                    while (readClientUsers.Read())
                                    {
                                        users.Add(readClientUsers.GetGuid(0));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readClientUsers.Close();
                                readClientUsers.Dispose();
                            }
                        }
                        foreach (System.Guid uGuid in users)
                        {
                            usrName = getUserNameFromId(uGuid);
                            fName = UP.GetUserProfile(usrName).FirstName;
                            lName = UP.GetUserProfile(usrName).LastName;
                            //Getting User Role Name
                            String[] roles = Roles.GetRolesForUser(usrName);
                            rlName = Convert.ToString(roles.GetValue(0));
                            if (rlName.Equals("Reviewer"))
                            {
                                MembershipUser selUser = Membership.GetUser(usrName);
                                enb = Convert.ToInt32(selUser.IsApproved);
                                lkd = Convert.ToInt32(selUser.IsLockedOut);
                                iReply.Rows.Add(uGuid, fName, lName, usrName, rlName, enb, lkd);
                                iReply.AcceptChanges();
                                iReply.DefaultView.Sort = "lastName";
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Guid, String> getResource()
        {
            try
            {
                Dictionary<Guid, String> iReply = new Dictionary<Guid, String>();
                String selStat = "SELECT [UserId] FROM AppResource";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        Guid dID = Guid.Empty;
                        String dName = null;
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selStat, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        dID = readData.GetGuid(0);
                                        dName = getFirstLastFromUserId(dID);
                                        iReply.Add(dID, dName);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
        
        public static Dictionary<Guid, String> getRoleUsersList(Int32 ClientID, String RoleName)
        {
            try
            {
                Guid uID = new Guid();
                String fN = null;
                String lN = null;
                String uN = null;
                String name = null;
                List<Guid> uidList = new List<Guid>();
                Dictionary<Guid, String> ruList = new Dictionary<Guid, String>();
                String selUsers = "SELECT [UserID] FROM UserInClient WHERE [clntID] = @clntID";
                String selUNames = "SELECT [UserName] FROM aspnet_Users WHERE [UserId] = @UserId";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getUsers = new SqlCommand(selUsers, dataCon);
                        getUsers.Parameters.AddWithValue("clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readUser = getUsers.ExecuteReader())
                        {
                            try
                            {
                                if (readUser.HasRows)
                                {
                                    while (readUser.Read())
                                    {
                                        uID = readUser.GetGuid(0);
                                        uidList.Add(uID);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readUser.Close();
                                readUser.Dispose();
                            }
                        }
                        foreach (var val in uidList)
                        {
                            SqlCommand getUserName = new SqlCommand(selUNames, dataCon);
                            getUserName.Parameters.AddWithValue("@UserId", SqlDbType.UniqueIdentifier).Value = val.ToString();
                            using (SqlDataReader readUName = getUserName.ExecuteReader())
                            {
                                try
                                {
                                    if (readUName.HasRows)
                                    {
                                        while (readUName.Read())
                                        {
                                            uN = readUName.GetString(0);
                                            if (Roles.IsUserInRole(uN, RoleName))
                                            {
                                                UserProfile UP = UserProfile.GetUserProfile(uN);
                                                fN = UP.FirstName;
                                                lN = UP.LastName;
                                                name = lN + ", " + fN;
                                                ruList.Add(val, name);
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { throw new System.Exception(ex.ToString()); }
                                finally
                                {
                                    readUName.Close();
                                    readUName.Dispose();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return ruList;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getUserAssignedLocation(String ClientDBConnection)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn userID = iReply.Columns.Add("userID", typeof(System.Guid));
                DataColumn addID = iReply.Columns.Add("addID", typeof(Int32));
                DataColumn stDate = iReply.Columns.Add("stDate", typeof(DateTime));
                DataColumn edDate = iReply.Columns.Add("edDate", typeof(DateTime));
                String selData = "SELECT [userID], [clntAddID], [sDate], [eDate] FROM [UserAssignment]";
                Int32 id = -99;
                System.Guid uID = new System.Guid();
                DateTime sD = DateTime.Now, eD = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        uID = readData.GetGuid(0);
                                        id = readData.GetInt32(1);
                                        sD = readData.GetDateTime(2);
                                        eD = readData.GetDateTime(3);
                                        iReply.Rows.Add(uID, id, sD, eD);
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static List<System.Guid> getUsersForLocation(Int32 AddressID, String ClientDBConnection)
        {
            try
            {
                List<System.Guid> iReply = new List<System.Guid>();
                System.Guid user = Guid.NewGuid();
                String selData = "SELECT [userID] FROM [UserAssignment] WHERE [clntAddID] = @clntAddID";
                using (SqlConnection dataCon = new SqlConnection(ClientDBConnection))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntAddID", SqlDbType.Int).Value = AddressID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        user = readData.GetGuid(0);
                                        iReply.Add(user);
                                    }
                                }                                
                                else
                                {
                                    iReply.Add(Guid.Empty);
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getUserDetailTable(Guid UserId)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn uId = iReply.Columns.Add("uId", typeof(Guid));
                DataColumn Email = iReply.Columns.Add("Email", typeof(String));
                DataColumn CreateDate = iReply.Columns.Add("CreateDate", typeof(DateTime));
                DataColumn LastLoginDate = iReply.Columns.Add("LastLoginDate", typeof(DateTime));
                DataColumn LastActivityDate = iReply.Columns.Add("LastActivityDate", typeof(DateTime));
                String selData = "SELECT [Email], [CreateDate], [LastLoginDate] FROM [aspnet_Membership] WHERE ([UserId] = @UserId)";
                String selLAct = "SELECT [LastActivityDate] FROM [aspnet_Users] WHERE [UserId] = @UserId";
                String uEmail = String.Empty;
                DateTime cDate = DateTime.Now, lgnDate = DateTime.Now, actDate = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getAct = new SqlCommand(selLAct, dataCon);
                        getAct.Parameters.AddWithValue("@UserId", SqlDbType.UniqueIdentifier).Value = UserId.ToString();
                        using (SqlDataReader readAct = getAct.ExecuteReader())
                        {
                            try
                            {
                                if (readAct.HasRows)
                                {
                                    while (readAct.Read())
                                    {
                                        actDate = readAct.GetDateTime(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readAct.Close();
                                readAct.Dispose();
                            }
                        }
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@UserId", SqlDbType.UniqueIdentifier).Value = UserId.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        uEmail = readData.GetString(0);
                                        cDate = readData.GetDateTime(1);
                                        lgnDate = readData.GetDateTime(2);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                iReply.Rows.Add(UserId, uEmail, cDate, lgnDate, actDate);
                iReply.AcceptChanges();
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getUserGender(System.Guid UserId)
        {
            try
            {
                String iReply = String.Empty;
                Int32 genderId = -99;
                String selData = "SELECT [gndID] FROM [UserGender] WHERE [UserId] = @UserId";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@UserId", SqlDbType.Int).Value = UserId.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        genderId = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                if (genderId.Equals("-99"))
                {
                    iReply = "---";
                }
                else
                {
                    iReply = DMG.getGenderValue(genderId);
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 getUserGenderID(System.Guid UserId)
        {
            try
            {
                Int32 iReply = -99, genderId = -99;
                String selData = "SELECT [gndID] FROM [UserGender] WHERE [UserId] = @UserId";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@UserId", SqlDbType.Int).Value = UserId.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        genderId = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Guid getUserIdFromUserName(String UserName)
        {
            try
            {
                Guid iReply = new Guid();
                String selData = "SELECT [UserId] FROM aspnet_Users WHERE [UserName] = @UserName";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@UserName", SqlDbType.NVarChar).Value = UserName.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        iReply = readData.GetGuid(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        private static String getUserName(String FirstName, String MiddleName, String LastName, String domain)
        {
            try
            {
                Int32 unCount = 0;
                String result = null, uname = null;
                FirstName = FirstName.Replace(" ", "");
                FirstName = FirstName.ToLower(new CultureInfo("en-US", false));
                LastName = LastName.Replace(" ", "");
                LastName = LastName.ToLower(new CultureInfo("en-US", false));
                
                String connString = WebConfigurationManager.ConnectionStrings["FEUsersDB"].ConnectionString;
                String selectUserName = "SELECT COUNT(UserId) FROM [aspnet_Users] WHERE [UserName] = @uname";
                
                uname = String.Format("{0}.{1}@{2}", FirstName, LastName, domain);

                SqlConnection clntConn = new SqlConnection(connString);
                clntConn.Open();

                SqlCommand checkUN = new SqlCommand(selectUserName, clntConn);
                checkUN.Parameters.AddWithValue("@uname", SqlDbType.NVarChar).Value = uname.ToString();

                SqlDataReader readUN = checkUN.ExecuteReader();
                if (readUN.HasRows)
                {
                    while (readUN.Read())
                    {
                        unCount = readUN.GetInt32(0);
                    }
                }
                readUN.Close();
                readUN.Dispose();

                clntConn.Close();

                if (unCount > 0)
                {
                    result = null;
                    return result;
                }
                else
                {
                    result = uname;
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.ToString());
            }
        }

        public static String getRoleNameFromRoleId(System.Guid RoleID)
        {
            try
            {
                String iReply = String.Empty;
                String selName = "SELECT [RoleName] FROM [aspnet_Roles] WHERE [RoleId] = @RoleId";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getName = new SqlCommand(selName, dataCon);
                        getName.Parameters.AddWithValue("@RoleId", SqlDbType.NVarChar).Value = RoleID.ToString();
                        using (SqlDataReader readName = getName.ExecuteReader())
                        {
                            try
                            {
                                if (readName.HasRows)
                                {
                                    while (readName.Read())
                                    {
                                        iReply = readName.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readName.Close();
                                readName.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Guid getRoleIdFromRoleName(String RoleName)
        {
            try
            {
                System.Guid iReply = System.Guid.Empty;
                String selName = "SELECT [RoleId] FROM [aspnet_Roles] WHERE [RoleName] = @RoleName";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getName = new SqlCommand(selName, dataCon);
                        getName.Parameters.AddWithValue("@RoleName", SqlDbType.NVarChar).Value = RoleName.ToString();
                        using (SqlDataReader readName = getName.ExecuteReader())
                        {
                            try
                            {
                                if (readName.HasRows)
                                {
                                    while (readName.Read())
                                    {
                                        iReply = readName.GetGuid(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readName.Close();
                                readName.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static String getUserNameFromId(Guid UserId)
        {
            try
            {
                String iReply = null;
                String selName = "SELECT [UserName] FROM aspnet_Users WHERE [UserId] = @UserId";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getName = new SqlCommand(selName, dataCon);
                        getName.Parameters.AddWithValue("@UserId", SqlDbType.UniqueIdentifier).Value = UserId.ToString();
                        using (SqlDataReader readName = getName.ExecuteReader())
                        {
                            try
                            {
                                if (readName.HasRows)
                                {
                                    while (readName.Read())
                                    {
                                        iReply = readName.GetString(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readName.Close();
                                readName.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getUserProfileInfo(Guid UserId)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn usrID = iReply.Columns.Add("usrID", typeof(Guid));
                DataColumn addT = iReply.Columns.Add("addT", typeof(String));
                DataColumn dsg = iReply.Columns.Add("dsg", typeof(String));
                DataColumn street1 = iReply.Columns.Add("Street1", typeof(String));
                DataColumn street2 = iReply.Columns.Add("Street2", typeof(String));
                DataColumn street3 = iReply.Columns.Add("Street3", typeof(String));
                DataColumn uCity = iReply.Columns.Add("uCity", typeof(String));
                DataColumn uCounty = iReply.Columns.Add("uCounty", typeof(String));
                DataColumn uState = iReply.Columns.Add("uState", typeof(String));
                DataColumn uCountry = iReply.Columns.Add("uCountry", typeof(String));
                DataColumn uZip = iReply.Columns.Add("uZip", typeof(String));
                DataColumn uSubReg = iReply.Columns.Add("uSubReg", typeof(String));
                DataColumn uReg = iReply.Columns.Add("uReg", typeof(String));
                DataColumn uPh = iReply.Columns.Add("uPh", typeof(String));
                DataColumn uPhT = iReply.Columns.Add("uPhT", typeof(String));
                DataColumn uLat = iReply.Columns.Add("uLat", typeof(Decimal));
                DataColumn uLon = iReply.Columns.Add("uLon", typeof(Decimal));

                String username = getUserNameFromId(UserId);
                String addTyp = String.Empty, desig = String.Empty, ph = String.Empty, phone = String.Empty, phTyp = String.Empty, str1 = String.Empty, str2 = String.Empty, str3 = String.Empty;
                String zipC = String.Empty, cityN = String.Empty, countyN = String.Empty, stateN = String.Empty, countryN = String.Empty, subRegN = String.Empty, regN = String.Empty, pT = String.Empty, uLt = String.Empty, uLg = String.Empty;
                Decimal locLatitude, locLongitude;
                Int32 dmgTyp = 0, zp = 0, cit = 0, cnty = 0, st = 0, cntry = 0, srg = 0, rg = 0;
                UP usrProf = UP.GetUserProfile(username);
                dmgTyp = Convert.ToInt32(usrProf.dmgID);
                addTyp = DMG.getDemogTypeName(dmgTyp);
                pT = Convert.ToString(usrProf.upPhoneType);
                if (String.IsNullOrEmpty(pT))
                {
                    phTyp = "---";
                }
                else
                {
                    phTyp = DMG.getDemogTypeName(Convert.ToInt32(pT));
                }
                zp = Convert.ToInt32(usrProf.zip);
                zipC = DMG.getZipCode(zp);
                cit = Convert.ToInt32(usrProf.city);
                cityN = DMG.getCityName(cit);
                cnty = Convert.ToInt32(usrProf.county);
                countyN = DMG.getCountyName(cnty);
                st = Convert.ToInt32(usrProf.state);
                stateN = DMG.getStateName(st);
                cntry = Convert.ToInt32(usrProf.country);
                countryN = DMG.getCountryName(cntry);
                srg = Convert.ToInt32(usrProf.sreg);
                subRegN = DMG.getSubRegionName(srg);
                rg = Convert.ToInt32(usrProf.reg);
                regN = DMG.getRegionName(rg);
                uLt = Convert.ToString(usrProf.Latitude);
                if (String.IsNullOrEmpty(uLt))
                {
                    locLatitude = 0.00M;
                }
                else
                {
                    locLatitude = Convert.ToDecimal(usrProf.Latitude);
                }
                uLg = Convert.ToString(usrProf.Longitude);
                if (String.IsNullOrEmpty(uLg))
                {
                    locLongitude = 0.00M;
                }
                else
                {
                    locLongitude = Convert.ToDecimal(usrProf.Longitude);
                }
                desig = Convert.ToString(usrProf.designation);
                ph = Convert.ToString(usrProf.upPhone);
                phone = "(" + ph.Substring(0, 3) + ") " + ph.Substring(3, 3) + "-" + ph.Substring(6, 4);
                str1 = Convert.ToString(usrProf.st1);
                str2 = Convert.ToString(usrProf.st2);
                str3 = Convert.ToString(usrProf.st3);

                iReply.Rows.Add(UserId, addTyp, desig, str1, str2, str3, cityN, countyN, stateN, countryN, zipC, subRegN, regN, phone, phTyp, locLatitude, locLongitude);
                iReply.AcceptChanges();

                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }         

        public static System.Data.DataTable getUserStats(Int32 ClientID)
        {            
            try
            {
                DataSet dsData = new DataSet();
                //Variables
                string SelectQuery = "select Status , Usercount from (";
                SelectQuery += "( SELECT 'Locked Users' , COUNT(AM.IsLockedOut) as usercount FROM UserInClient U , aspnet_Membership AM  WHERE AM.IsLockedOut = 'True' ";
                SelectQuery += " and  U.UserId = AM.UserId and clntID = @clntID )";
                SelectQuery += " union ";
                SelectQuery += "( SELECT 'Approved Users', COUNT(AM.IsApproved) as usercount FROM UserInClient U , aspnet_Membership AM WHERE AM.IsApproved = 'True' ";
                SelectQuery += " and U.UserId = AM.UserId and  clntID = @clntID )";
                SelectQuery += " ) as T (Status , Usercount)";


                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        SqlCommand getUsers = new SqlCommand(SelectQuery, dataCon);
                        getUsers.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        //using (SqlDataReader readUsers = getUsers.ExecuteReader())
                        using (SqlDataAdapter sqlCmd = new SqlDataAdapter(getUsers))
                        {
                            try
                            {
                                dataCon.Open();
                                sqlCmd.Fill(dsData);
                                dataCon.Close();
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }

                }

                return dsData.Tables[0];

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }

        }

        public static DataTable getUserStatsGlobal()
        {
            DataSet dsData = new DataSet();
            Int32 acctLogged = 0;

            try
            {
                acctLogged = Membership.GetNumberOfUsersOnline();
                //Variables
                string SelectQuery = "select Status , Usercount from (";
                SelectQuery += "( SELECT 'Locked Users' , COUNT(AM.IsLockedOut) as usercount FROM  aspnet_Membership AM  WHERE AM.IsLockedOut = 'True' )";
                SelectQuery += " union ";
                SelectQuery += "( SELECT 'Approved Users', COUNT(AM.IsApproved) as usercount FROM  aspnet_Membership AM WHERE AM.IsApproved = 'True' )";
                SelectQuery += " ) as T (Status , Usercount)";

                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        SqlCommand getUsers = new SqlCommand(SelectQuery, dataCon);
                        using (SqlDataAdapter sqlCmd = new SqlDataAdapter(getUsers))
                        {
                            try
                            {
                                dataCon.Open();
                                sqlCmd.Fill(dsData);
                                dataCon.Close();
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }

                }

                dsData.Tables[0].Rows.Add("Online Users", acctLogged);

                return dsData.Tables[0];

            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }

        }


        //public static String getLoggedUserStat()
        //{
        //    try
        //    {
        //        //Variables
        //        ArrayList usrlist = new ArrayList();
        //        var list = new List<KeyValuePair<String, Int32>>();
        //        String selLocked = "SELECT COUNT(IsLockedOut) FROM aspnet_Membership WHERE [IsLockedOut] = 'True'";
        //        String selApproved = "SELECT COUNT(IsApproved) FROM aspnet_Membership WHERE [IsApproved] = 'True'";
        //        String lggdLabel = String.Empty, lckdLabel = String.Empty, endbLabel = String.Empty;
        //        Int32 userCount = 0, acctLogged = 0, usrLocked = 0, usrEnabled = 0;
        //        userCount = Membership.GetAllUsers().Count;
        //        acctLogged = Membership.GetNumberOfUsersOnline();
        //        lggdLabel = "Online ( " + acctLogged + " )";
        //        list.Add(new KeyValuePair<String, Int32>(lggdLabel, userCount));
        //        using (SqlConnection usrCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
        //        {
        //            try
        //            {
        //                usrCon.Open();
        //                SqlCommand getLocked = new SqlCommand(selLocked, usrCon);
        //                using (SqlDataReader readLocked = getLocked.ExecuteReader())
        //                {
        //                    try
        //                    {
        //                        if (readLocked.HasRows)
        //                        {
        //                            while (readLocked.Read())
        //                            {
        //                                usrLocked = readLocked.GetInt32(0);
        //                                lckdLabel = "Locked ( " + usrLocked + " )";
        //                                list.Add(new KeyValuePair<String,Int32>( lckdLabel, usrLocked));
        //                            }
        //                        }
        //                    }
        //                    catch (Exception ex)
        //                    { throw new System.Exception(ex.ToString()); }
        //                    finally
        //                    {
        //                        readLocked.Close();
        //                        readLocked.Dispose();
        //                    }
        //                }

        //                 SqlCommand getEnabled = new SqlCommand(selApproved, usrCon);
        //                 using (SqlDataReader readEnabled = getEnabled.ExecuteReader())
        //                 {
        //                     try
        //                     {
        //                         if (readEnabled.HasRows)
        //                         {
        //                             while (readEnabled.Read())
        //                             {
        //                                 usrEnabled = readEnabled.GetInt32(0);
        //                                 endbLabel = "Enabled ( " + usrEnabled + " )";
        //                                 list.Add(new KeyValuePair<String, Int32>(endbLabel, usrEnabled));
        //                             }
        //                         }
        //                     }
        //                     catch (Exception ex)
        //                     { throw new System.Exception(ex.ToString()); }
        //                     finally
        //                     {
        //                         readEnabled.Close();
        //                         readEnabled.Dispose();
        //                     }
        //                 }
        //             }
        //             catch (Exception ex)
        //             { throw new System.Exception(ex.ToString()); }
        //             finally
        //             {
        //                usrCon.Close();
        //                usrCon.Dispose();
        //            }
        //        }

        //        //let's instantiate Google Chart's DataTable.
        //        var dt = new GG.DataTable();
        //        dt.AddColumn(new GG.Column(GG.ColumnType.String, "0", "Status"));
        //        dt.AddColumn(new GG.Column(GG.ColumnType.Number, "1", "Count"));
        //        foreach (var pair in list)
        //        {
        //            GG.Row gr = dt.NewRow();
        //            gr.AddCellRange(new GG.Cell[]{                        
        //                new GG.Cell(pair.Key),
        //                new GG.Cell(pair.Value)
        //            });
        //            dt.AddRow(gr);
        //        }
        //        return dt.GetJson();
        //    }
        //    catch (Exception ex)
        //    { throw new System.Exception(ex.ToString()); }
        //}

        //public static String getGlobalUserPerRole()
        //{
        //    try
        //    {
        //        //Variables
        //        String[] URoles = null;
        //        String[] rUsers = null;
        //        Int32 usrCnt = -99;
        //        String usrLbl = null;

        //        usrCnt = Membership.GetAllUsers().Count;

        //        System.Data.DataTable usrTable = new System.Data.DataTable();
        //        DataColumn usrRole = usrTable.Columns.Add("usrRole", typeof(String));
        //        DataColumn usrLabel = usrTable.Columns.Add("usrLabel", typeof(String));

        //        URoles = Roles.GetAllRoles();
        //        foreach (String rName in URoles)
        //        {
        //            if (URoles.Length > 0)
        //            {
        //                rUsers = Roles.GetUsersInRole(rName);
        //                usrCnt = rUsers.Length;
        //                usrLbl = rName + "( " + usrCnt + " )";
        //                usrTable.Rows.Add(usrLbl, usrCnt);
        //                usrTable.AcceptChanges();
        //            }
        //        }
        //        GG.DataTable dt = new GG.DataTable();
        //        dt.AddColumn(new GG.Column(GG.ColumnType.String, "0", "Role"));
        //        dt.AddColumn(new GG.Column(GG.ColumnType.Number, "1", "Count"));                  
        //        foreach (System.Data.DataRow row in usrTable.Rows)
        //        {
        //            GG.Row gr = dt.NewRow();
        //            gr.AddCellRange(new GG.Cell[]{                        
        //                new GG.Cell(row[0]),
        //                new GG.Cell(row[1])
        //            });
        //            dt.AddRow(gr);
        //        }
        //        return dt.GetJson();
        //    }
        //    catch (Exception ex)
        //    { throw new System.Exception(ex.ToString()); }
        //}

        //public static String getClientUserPerRole(Int32 ClientID)
        //{
        //    try
        //    {
        //        //Variables
        //        String[] URoles = null;
        //        List<String> cUsers = new List<String>();
        //        Int32 dCnt = 0, cCnt = 0, fCnt = 0, lCnt = 0, rCnt = 0, sCnt = 0;
        //        String usrLbl = String.Empty, uName = String.Empty, urName = String.Empty;
        //        Guid uID = new Guid();
        //        String selUsers = "SELECT [UserId] FROM UserInClient WHERE [clntID] = @clntID";
        //        System.Data.DataTable usrTable = new System.Data.DataTable();
        //        DataColumn usrRole = usrTable.Columns.Add("usrRole", typeof(String));
        //        DataColumn usrLabel = usrTable.Columns.Add("usrLabel", typeof(String));

        //        using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
        //        {
        //            try
        //            {
        //                dataCon.Open();
        //                SqlCommand getUsers = new SqlCommand(selUsers, dataCon);
        //                getUsers.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
        //                using (SqlDataReader readUsers = getUsers.ExecuteReader())
        //                {
        //                    try
        //                    {
        //                        if (readUsers.HasRows)
        //                        {
        //                            while (readUsers.Read())
        //                            {
        //                                uID = readUsers.GetGuid(0);
        //                                uName = getUserNameFromId(uID);
        //                                cUsers.Add(uName);
        //                            }
        //                        }
        //                    }
        //                    catch (Exception ex)
        //                    { throw new System.Exception(ex.ToString()); }
        //                    finally
        //                    {
        //                        readUsers.Close();
        //                        readUsers.Dispose();
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            { throw new System.Exception(ex.ToString()); }
        //            finally
        //            {
        //                dataCon.Close();
        //                dataCon.Dispose();
        //            }
        //        }

        //        URoles = Roles.GetAllRoles();
        //        foreach (String rName in URoles)
        //        {
        //            foreach (var username in cUsers)
        //            {
        //                if (Roles.IsUserInRole(username, rName))
        //                {
        //                    if (rName.Equals("Coordinator"))
        //                    {
        //                        cCnt++;
        //                    }
        //                    else if (rName.Equals("Director"))
        //                    {
        //                        dCnt++;
        //                    }
        //                    else if (rName.Equals("Fieldhand"))
        //                    {
        //                        fCnt++;
        //                    }
        //                    else if (rName.Equals("LabTech"))
        //                    {
        //                        lCnt++;
        //                    }
        //                    else if (rName.Equals("Reviewer"))
        //                    {
        //                        rCnt++;
        //                    }
        //                    else
        //                    {
        //                        sCnt++;
        //                    }
        //                }
        //            }
        //        }
        //        usrTable.Rows.Add("Coordinator (" + cCnt + ")", cCnt);
        //        usrTable.Rows.Add("Director (" + dCnt + ")", dCnt);
        //        usrTable.Rows.Add("Fieldhand (" + fCnt + ")", fCnt);
        //        usrTable.Rows.Add("LabTech (" + lCnt + ")", lCnt);
        //        usrTable.Rows.Add("Reviewer (" + rCnt + ")", rCnt);
        //        usrTable.Rows.Add("Support (" + sCnt + ")", sCnt);
        //        usrTable.AcceptChanges();

        //        GG.DataTable dt = new GG.DataTable();
        //        dt.AddColumn(new GG.Column(GG.ColumnType.String, "0", "Role"));
        //        dt.AddColumn(new GG.Column(GG.ColumnType.Number, "1", "Count"));
        //        foreach (System.Data.DataRow row in usrTable.Rows)
        //        {
        //            GG.Row gr = dt.NewRow();
        //            gr.AddCellRange(new GG.Cell[]{                        
        //                new GG.Cell(row[0]),
        //                new GG.Cell(row[1])
        //            });
        //            dt.AddRow(gr);
        //        }
        //        return dt.GetJson();
        //    }
        //    catch (Exception ex)
        //    { throw new System.Exception(ex.ToString()); }
        //}                                       
               
        public static Int32 insertFHSchedule(Int32 WellID, Guid UserId, DateTime StartDate, DateTime EndDate, String UserName, String UserRealm, String ClientDBString)
        {
            try
            {
                Int32 iReply = -99;
                String uDesig = null;
                String sDesig = null;
                String uName = null;
                uName = getUserNameFromId(UserId);
                UserProfile UP = UserProfile.GetUserProfile(uName);
                uDesig = UP.designation;
                Guid sID = getUserIdFromUserName(UserName);
                UserProfile SP = UserProfile.GetUserProfile(UserName);
                sDesig = SP.designation;
                String insertStat = "INSERT INTO FHSchedule ([welID], [fhand], [fhDesig], [fhStart], [fhEnd], [scheduler], [schDesig], [cTime], [uTime], [username], [realm]) VALUES (@welID, @fhand, @fhDesig, @fhStart, @fhEnd, @scheduler, @schDesig, @cTime, @uTime, @username, @realm)";
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(insertStat, dataCon);
                        insData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        insData.Parameters.AddWithValue("@fhand", SqlDbType.UniqueIdentifier).Value = UserId.ToString();
                        insData.Parameters.AddWithValue("@fhDesig", SqlDbType.NVarChar).Value = uDesig.ToString();
                        insData.Parameters.AddWithValue("@fhStart", SqlDbType.DateTime2).Value = StartDate.ToString();
                        insData.Parameters.AddWithValue("@fhEnd", SqlDbType.DateTime2).Value = EndDate.ToString();
                        insData.Parameters.AddWithValue("@scheduler", SqlDbType.NVarChar).Value = sID.ToString();
                        insData.Parameters.AddWithValue("@schDesig", SqlDbType.NVarChar).Value = sDesig.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserRealm.ToString();

                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertUAssignment(Guid UserId, Int32 LocationID, DateTime StartDate, DateTime EndDate, String UserName, String UserDomain, String ClientConnection)
        {
            try
            {
                Int32 iReply = -99;
                String insertStat = "INSERT INTO UserAssignment ([userID], [clntAddID], [sDate], [eDate], [cTime], [uTime], [username], [realm]) VALUES (@userId, @clntAddID, @sDate, @eDate, @cTime, @uTime, @username, @realm)";
                using (SqlConnection insCon = new SqlConnection(ClientConnection))
                {
                    try
                    {
                        insCon.Open();
                        SqlCommand insData = new SqlCommand(insertStat, insCon);
                        insData.Parameters.AddWithValue("@userID", SqlDbType.UniqueIdentifier).Value = UserId.ToString();
                        insData.Parameters.AddWithValue("@clntAddID", SqlDbType.Int).Value = LocationID.ToString();
                        insData.Parameters.AddWithValue("@sDate", SqlDbType.DateTime2).Value = StartDate.ToString();
                        insData.Parameters.AddWithValue("@eDate", SqlDbType.DateTime2).Value = EndDate.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now.ToString();
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserDomain.ToString();

                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        insCon.Close();
                        insCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertUserGender(Guid newUser, Int32 GenderID)
        {
            try
            {
                Int32 iReply = -99;
                String insertGender = "INSERT INTO [UserGender] ([UserId], [gndID]) VALUES (@UserId, @gndID)";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand insCom = new SqlCommand(insertGender, dataCon);
                        insCom.Parameters.AddWithValue("@UserId", SqlDbType.UniqueIdentifier).Value = newUser.ToString();
                        insCom.Parameters.AddWithValue("@gndID", SqlDbType.Int).Value = GenderID.ToString();
                        iReply = Convert.ToInt32(insCom.ExecuteNonQuery());
                    }
                    catch(Exception ex)
                    {throw new System.Exception(ex.ToString());}
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 insertUserInClient(Guid newUser, Int32 clientID)
        {
            try
            {
                using (SqlConnection userCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ConnectionString))
                {
                    try
                    {
                        userCon.Open();
                        Int32 iResult = -99;
                        DateTime crTime = DateTime.Now;
                        DateTime updTime = DateTime.Now;

                        String insertUserINClient = "Insert into UserInClient ([UserId], [clntID], [cTime], [uTime]) VALUES (@UserId, @clntID, @cTime, @uTime);";
                        SqlCommand insUC = new SqlCommand(insertUserINClient, userCon);
                        insUC.Parameters.AddWithValue("@UserId", SqlDbType.UniqueIdentifier).Value = newUser.ToString();
                        insUC.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = clientID.ToString();
                        insUC.Parameters.AddWithValue("@cTime", SqlDbType.DateTime).Value = crTime.ToString();
                        insUC.Parameters.AddWithValue("@uTime", SqlDbType.DateTime).Value = updTime.ToString();

                        iResult = Convert.ToInt32(insUC.ExecuteNonQuery());

                        return iResult;
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        userCon.Close();
                        userCon.Dispose();
                    }
                }
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }        

        public static Int32 updateAddressProfile(String UserName, String Region, String SubRegion, String Country, String State, String County, String City, String Zip, String Street1, String Street2, String Street3, String AddressType)
        {
            try
            {
                Int32 iResult = 1;
                String cityN = DMG.getCityName(Convert.ToInt32(City));
                String stateN = DMG.getStateName(Convert.ToInt32(State));
                String zipC = DMG.getZipCode(Convert.ToInt32(Zip));
                String uAddress = Street1 + " " + cityN + " " + stateN + " " + zipC;
                List<Double> latlng = MAP.getLatLngFromAddress(uAddress);
                Decimal Longi = Convert.ToDecimal(latlng[0]);
                Decimal Lati = Convert.ToDecimal(latlng[1]);

                UP profile = UP.GetUserProfile(UserName);
                profile.reg = Region;
                profile.sreg = SubRegion;
                profile.country = Country;
                profile.state = State;
                profile.county = County;
                profile.city = City;
                profile.zip = Zip;
                profile.st1 = Street1;
                profile.st2 = Street2;
                profile.st3 = Street3;
                profile.addType = AddressType;
                profile.Latitude = Convert.ToString(Lati);
                profile.Longitude = Convert.ToString(Longi);
                profile.Save();

                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 updateContactEmail(String UserName, String UserEmail)
        {
            try
            {
                Int32 iResult = 1;
                //UP profile = UP.GetUserProfile(UserName);
                MembershipUser Usr = Membership.GetUser(UserName);                
                Usr.Email = UserEmail;                
                Membership.UpdateUser(Usr);
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 updatePassword(String UserName, String UserPassword)
        {
            try
            {
                Int32 iResult = -99;

                MembershipUser mUser = Membership.GetUser(UserName);
                iResult = Convert.ToInt32(mUser.ChangePassword(mUser.ResetPassword(), UserPassword));

                return iResult;
            }
            catch (MembershipPasswordException ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 updatePersonalProfile(String UserName, String FirstName, String MiddleName, String LastName, Int32 GenderID)
        {
            try
            {
                Int32 iResult = -99;

                UP profile = UP.GetUserProfile(UserName);
                profile.FirstName = FirstName;
                profile.MiddleName = MiddleName;
                profile.LastName = LastName;
                profile.Save();
                iResult = updatePersonalGender(UserName, GenderID);
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 updatePersonalGender(String UserName, Int32 GenderID)
        {
            try
            {
                Int32 iReply = -99;
                UP profile = UP.GetUserProfile(UserName);
                System.Guid uID = new Guid(profile.UserId);
                String updData = "UPDATE [UserGender] SET [gndID] = @gndID WHERE [UserId] = @UserId";
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand uData = new SqlCommand(updData, dataCon);
                        uData.Parameters.AddWithValue("@gndID", SqlDbType.Int).Value = GenderID.ToString();
                        uData.Parameters.AddWithValue("@UserId", SqlDbType.UniqueIdentifier).Value = uID.ToString();

                        iReply = Convert.ToInt32(uData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 updateProfilePhone(String UserName, String PhoneType, String Phone)
        {
            try
            {
                Int32 iResult = 1;

                UP profile = UP.GetUserProfile(UserName);
                profile.upPhoneType = PhoneType;
                profile.upPhone = Phone;
                profile.Save();
                MembershipUser Usr = Membership.GetUser(UserName);
                Membership.UpdateUser(Usr);
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Int32 updateProfileDesignation(String UserName, String Designation)
        {
            try
            {
                Int32 iResult = 1;

                UP profile = UP.GetUserProfile(UserName);
                profile.designation = Designation;
                profile.Save();
                MembershipUser Usr = Membership.GetUser(UserName);
                Membership.UpdateUser(Usr);
                return iResult;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }


        internal static int updateUserRole(Guid UserID, Guid UserRole)
        {
            try
            {
                Int32 iReply = -99;
                MembershipUser memUser = Membership.GetUser(UserID);
                String userName = getUserNameFromId(UserID);
                //Roles.GetRolesForUser(userName);
                Roles.RemoveUserFromRoles(userName, Roles.GetRolesForUser(userName));
                String roleName = getRoleNameFromRoleId(UserRole);
                Roles.AddUserToRole(userName, roleName);
                String[] rName = Roles.GetRolesForUser(userName);
                Int32 findRole = Array.IndexOf(rName, roleName);
                if (findRole > -1)
                {                
                    iReply = 1;
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }    
}