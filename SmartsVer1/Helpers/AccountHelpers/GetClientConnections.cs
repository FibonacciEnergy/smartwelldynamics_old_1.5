﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Net.Mail;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;
using DB = SmartsVer1.Helpers.DatabaseHelpers.DBConfigs;

/// <summary>
/// GetClientConnections Class provides a mechanism to get 
/// the client database connection from Mapping Table
/// </summary>
public class GetClientConnections
{       
    public GetClientConnections()
	{
      // Empty Constructor
	}

    public String getClientConnection(String domain)
    {
        try
        {
            String clntString = null;

            // Get the Database Mapping info from ClientDBMapping table for provided Realm name
            String clntQuery = "SELECT [cdbID] , [clntID] , [clntRealm] , [cmName] FROM [ClientDBMapping] WHERE [clntRealm] = '" + domain + "'";
            SqlConnection clntConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString());

            clntConn.Open();

            SqlCommand clntCmd = new SqlCommand(clntQuery, clntConn);

            using (SqlDataReader clntRdr = clntCmd.ExecuteReader())
            {
                while (clntRdr.Read())
                {
                    clntString = clntRdr.GetString(3);
                }
                clntRdr.Close();
            }
            clntConn.Close();
            return clntString;        
        }
        catch(Exception ex)
        { throw new System.Exception(ex.ToString()); }        
    }

    public String getClientDBString(String DBName)
    {
        try
        {
            String iReply = String.Empty;
            String selData = "SELECT [cdbString] FROM [ClientDBMapping] WHERE [cmName] = @cmName";
            using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
            {
                try
                {
                    dataCon.Open();
                    SqlCommand getData = new SqlCommand(selData, dataCon);
                    getData.Parameters.AddWithValue("@cmName", SqlDbType.NVarChar).Value = DBName.ToString();
                    using (SqlDataReader readData = getData.ExecuteReader())
                    {
                        try
                        {
                            if (readData.HasRows)
                            {
                                while (readData.Read())
                                {
                                    iReply = readData.GetString(0);
                                }
                            }
                        }
                        catch (Exception ex)
                        { throw new System.Exception(ex.ToString()); }
                        finally
                        {
                            readData.Close();
                            readData.Dispose();
                        }
                    }
                }
                catch (Exception ex)
                { throw new System.Exception(ex.ToString()); }
                finally
                {
                    dataCon.Close();
                    dataCon.Dispose();
                }
            }
            return iReply;
        }
        catch (Exception ex)
        { throw new System.Exception(ex.ToString()); }
    }

    public String getClientDBConnection(String domain)
    {
        try
        {
            String clntString = null;

            // Get the Database Mapping info from ClientDBMapping table for provided Realm name
            String clntQuery = "SELECT cdbID , clntID , clntRealm , cmName, cdbString FROM ClientDBMapping WHERE clntRealm = '" + domain + "'";
            SqlConnection clntConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString());

            clntConn.Open();

            SqlCommand clntCmd = new SqlCommand(clntQuery, clntConn);

            using (SqlDataReader clntRdr = clntCmd.ExecuteReader())
            {
                while (clntRdr.Read())
                {
                    clntString = clntRdr.GetString(4);
                }
                clntRdr.Close();
            }
            clntConn.Close();
            return clntString;
        }
        catch (Exception ex)
        { throw new System.Exception(ex.ToString()); }
    }    

    public Int32 createConnection(String DBName, String UserName, String UserDomain)
    {
        try
        {
            Int32 iReply = -99, createDB = -99, updateRole = -99;
            String val = null;
            String dbSrvr = WebConfigurationManager.AppSettings["DBServer"].ToString();
            String dbUser = WebConfigurationManager.AppSettings["DBUser"].ToString();
            String dbPwrd = WebConfigurationManager.AppSettings["DBPassword"].ToString();
            Int32 dbConn = Convert.ToInt32(WebConfigurationManager.AppSettings["DBConnection"].ToString());
            SqlConnectionStringBuilder cBuilder = new SqlConnectionStringBuilder();
            cBuilder.DataSource = dbSrvr;
            cBuilder.InitialCatalog = DBName;
            cBuilder.IntegratedSecurity = false;
            cBuilder.UserID = dbUser;
            cBuilder.Password = dbPwrd;
            cBuilder.ConnectTimeout = dbConn;
            val = cBuilder.ConnectionString;
            //Create DB
            createDB = DB.createClientDataBase(DBName);
            using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        String insComm = "INSERT INTO [ConMap] ([cmName], [cmString], [cTime], [uTime], [username], [realm]) VALUES (@cmName, @cmString, @cTime, @uTime, @username, @realm)";
                        dataCon.Open();
                        SqlCommand insData = new SqlCommand(insComm, dataCon);
                        insData.Parameters.AddWithValue("@cmName", SqlDbType.NVarChar).Value = DBName;
                        insData.Parameters.AddWithValue("@cmString", SqlDbType.NVarChar).Value = val.ToString();
                        insData.Parameters.AddWithValue("@cTime", SqlDbType.DateTime2).Value = DateTime.Now;
                        insData.Parameters.AddWithValue("@uTime", SqlDbType.DateTime2).Value = DateTime.Now;
                        insData.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = UserName.ToString();
                        insData.Parameters.AddWithValue("@realm", SqlDbType.NVarChar).Value = UserDomain.ToString();

                        iReply = Convert.ToInt32(insData.ExecuteNonQuery());
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }          
            return iReply;
        }
        catch (Exception ex)
        { throw new System.Exception(ex.ToString()); }
    }    
}