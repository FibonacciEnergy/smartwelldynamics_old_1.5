﻿using System;
using System.IO;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Globalization;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Collections;
using System.Web.Configuration;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using SmartsVer1.Helpers.AccountHelpers;
using UP = SmartsVer1.Helpers.AccountHelpers.UserProfile;
using CLNT = SmartsVer1.Helpers.AccountHelpers.CreateClient;
using DMG = SmartsVer1.Helpers.DemogHelpers.Demographics;
using USR = SmartsVer1.Helpers.AccountHelpers.CreateUser;

namespace SmartsVer1.Helpers.AccountHelpers
{
    /// <summary>
    /// A class that helps create/manage system client user's schedule and office/location assignment
    /// </summary>
    public class ClientScheduler
    {
        const String appID = "047092b7-d10b-42da-ae7a-eb223509869d";

        static void Main()
        { }

        public static System.Data.DataTable getAddressDetail(Guid UserID)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn pAddID = iReply.Columns.Add("pAddID", typeof(Guid));
                DataColumn pAddTitle = iReply.Columns.Add("pAddTitle", typeof(String));
                DataColumn pAddSt1 = iReply.Columns.Add("pAddSt1", typeof(String));
                DataColumn pAddSt2 = iReply.Columns.Add("pAddSt2", typeof(String));
                DataColumn pAddSt3 = iReply.Columns.Add("pAddSt3", typeof(String));
                DataColumn pAddCity = iReply.Columns.Add("pAddCity", typeof(String));
                DataColumn pAddCounty = iReply.Columns.Add("pAddCounty", typeof(String));
                DataColumn pAddState = iReply.Columns.Add("pAddState", typeof(String));
                DataColumn pAddZip = iReply.Columns.Add("pAddZip", typeof(String));
                DataColumn pAddCountry = iReply.Columns.Add("pAddCountry", typeof(String));
                DataColumn pAddPh = iReply.Columns.Add("pAddPh", typeof(String));
                DataColumn pAddPhT = iReply.Columns.Add("pAddPhT", typeof(String));
                String username = USR.getUserNameFromId(UserID);
                UP userP = UP.GetUserProfile(username);
                String title = Convert.ToString(userP.dmgID);
                if (!String.IsNullOrEmpty(title))
                {
                    title = DMG.getDemogTypeName(Convert.ToInt32(title));
                }
                else
                {
                    title = "---";
                }
                String st1 = userP.st1;
                String st2 = userP.st2;
                if (!string.IsNullOrEmpty(st2))
                {
                    st2 = Convert.ToString(st2);
                }
                else
                {
                    st2 = "---";
                }
                String st3 = userP.st3;
                if (!string.IsNullOrEmpty(st3))
                {
                    st3 = Convert.ToString(st3);
                }
                else
                {
                    st3 = "---";
                }
                Int32 cityID = Convert.ToInt32(userP.city);
                String CityName = DMG.getCityName(cityID);
                Int32 countyID = Convert.ToInt32(userP.county);
                String countyName = DMG.getCountyName(countyID);
                Int32 stateID = Convert.ToInt32(userP.state);
                String stateName = DMG.getStateName(stateID);
                Int32 zipID = Convert.ToInt32(userP.zip);
                String zipCode = DMG.getZipCode(zipID);
                Int32 countryID = Convert.ToInt32(userP.country);
                String CountryName = DMG.getCountryName(countyID);
                String phone = Convert.ToString(userP.upPhone);
                String phoneType = Convert.ToString(userP.upPhoneType);
                if (!String.IsNullOrEmpty(phoneType))
                {
                    phoneType = DMG.getDemogTypeName(Convert.ToInt32(phoneType));
                }
                else
                {
                    phoneType = "---";
                }
                iReply.Rows.Add(UserID, title, st1, st2, st3, CityName, countyName, stateName, zipCode, CountryName, phone, phoneType);
                iReply.AcceptChanges();

                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getFHScheduleTable(Int32 JobID, Int32 WellID, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn uschID = iReply.Columns.Add("uschID", typeof(Int32));
                DataColumn fhand = iReply.Columns.Add("fhand", typeof(Guid));
                DataColumn lName = iReply.Columns.Add("lName", typeof(String));
                DataColumn fName = iReply.Columns.Add("fName", typeof(String));
                DataColumn fhdesig = iReply.Columns.Add("fhdesig", typeof(String));
                DataColumn fhStart = iReply.Columns.Add("fhStart", typeof(DateTime));
                DataColumn fhEnd = iReply.Columns.Add("fhEnd", typeof(DateTime));
                DataColumn scheduler = iReply.Columns.Add("scheduler", typeof(Guid));
                DataColumn schdlr = iReply.Columns.Add("schdlr", typeof(String));
                DataColumn schDesig = iReply.Columns.Add("schDesig", typeof(String));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [uschID], [fhand], [fhStart], [fhEnd], [scheduler], [uTime] FROM [FHSchedule] WHERE [jobID] = @jobID AND [welID] = @welID";
                Int32 id = 0;
                System.Guid uID = new Guid();
                System.Guid sID = new Guid();
                String lastName = String.Empty, firstName = String.Empty, usrDesig = String.Empty, uName = String.Empty;
                String slName = String.Empty, sfName = String.Empty, sName = String.Empty, schName = String.Empty, sDesig = String.Empty;
                DateTime sT = DateTime.Now, eT = DateTime.Now, uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@jobID", SqlDbType.Int).Value = JobID.ToString();
                        getData.Parameters.AddWithValue("@welID", SqlDbType.Int).Value = WellID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        uID = readData.GetGuid(1);
                                        uName = USR.getUserNameFromId(uID);
                                        UP user = UP.GetUserProfile(uName);
                                        lastName = user.LastName;
                                        firstName = user.FirstName;
                                        usrDesig = user.designation;
                                        sT = readData.GetDateTime(2);
                                        eT = readData.GetDateTime(3);
                                        sID = readData.GetGuid(4);
                                        sName = USR.getUserNameFromId(sID);
                                        UP schP = UP.GetUserProfile(sName);
                                        schName = schP.LastName + " , " + schP.FirstName;
                                        sDesig = schP.designation;
                                        uT = readData.GetDateTime(5);
                                        iReply.Rows.Add(id, uID, lastName, firstName, usrDesig, sT, eT, sID, schName, sDesig, uT);
                                        iReply.AcceptChanges();
                                        iReply.DefaultView.Sort = "lName";
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static Dictionary<Int32, String> getUserAssignmentLocation(Int32 ClientID)
        {
            try
            {
                Dictionary<Int32, String> iReply = new Dictionary<Int32, String>();
                String selData = "SELECT [clntAddID], [title], [cityID], [stID], [cntyID], [ctyID], [sregID], [regID] FROM [ClientAddress] WHERE [clntID] = @clntID ORDER BY [title]";
                Int32 id = 0, ctID = 0, sID = 0, cnID = 0, cyID = 0, srID = 0, rID = 0;
                String tName = String.Empty, cityName = String.Empty, stateName = String.Empty, countyName = String.Empty, CountryName = String.Empty, subRegName = String.Empty, regName = String.Empty, value = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@clntID", SqlDbType.Int).Value = ClientID.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        tName = readData.GetString(1);
                                        ctID = readData.GetInt32(2);
                                        cityName = DMG.getCityName(ctID);
                                        sID = readData.GetInt32(3);
                                        stateName = DMG.getStateName(sID);
                                        cnID = readData.GetInt32(4);
                                        countyName = DMG.getCountyName(cnID);
                                        cyID = readData.GetInt32(5);
                                        CountryName = DMG.getCountryName(cyID);
                                        srID = readData.GetInt32(6);
                                        subRegName = DMG.getSubRegionName(srID);
                                        rID = readData.GetInt32(7);
                                        regName = DMG.getRegionName(rID);
                                        value = tName + " ( " + cityName + ", " + countyName + ", " + stateName + ", " + CountryName + ", " + subRegName + ", " + regName + " )";
                                        iReply.Add(id, value);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getClientUserAssignmentTable(String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn asgID = iReply.Columns.Add("asgID", typeof(Int32));
                DataColumn lastName = iReply.Columns.Add("lastName", typeof(String));
                DataColumn firstName = iReply.Columns.Add("firstName", typeof(String));
                DataColumn desig = iReply.Columns.Add("desig", typeof(String));
                DataColumn addID = iReply.Columns.Add("addID", typeof(String));
                DataColumn sDate = iReply.Columns.Add("sDate", typeof(DateTime));
                DataColumn eDate = iReply.Columns.Add("eDate", typeof(DateTime));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [asgID], [userID], [clntAddID], [sDate], [eDate], [uTime] FROM UserAssignment";
                Int32 id = 0, clntAddID = 0, clientID = 0;
                System.Guid usr = new System.Guid();
                String lName = String.Empty, fName = String.Empty, uDesig = String.Empty, clntAdd = String.Empty, uName = String.Empty, rlName = String.Empty;
                DateTime start = DateTime.Now, end = DateTime.Now, uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        usr = readData.GetGuid(1);
                                        uName = USR.getUserNameFromId(usr);
                                        UP user = UP.GetUserProfile(uName);
                                        lName = user.LastName;
                                        fName = user.FirstName;
                                        uDesig = user.designation;
                                        String[] roles = Roles.GetRolesForUser(uName);
                                        rlName = Convert.ToString(roles.GetValue(0));
                                        clientID = Convert.ToInt32(user.clntID);
                                        clntAddID = readData.GetInt32(2);
                                        clntAdd = CLNT.getAddressTitle(clntAddID, clientID);
                                        start = readData.GetDateTime(3);
                                        end = readData.GetDateTime(4);
                                        uT = readData.GetDateTime(5);
                                        if (!rlName.Equals("Support"))
                                        {
                                            iReply.Rows.Add(id, lName, fName, uDesig, clntAdd, start, end, uT);
                                            iReply.AcceptChanges();
                                        }
                                        iReply.DefaultView.Sort = "lastName";
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }

        public static System.Data.DataTable getUserAssignmentTable(String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn asgID = iReply.Columns.Add("asgID", typeof(Int32));
                DataColumn lastName = iReply.Columns.Add("lastName", typeof(String));
                DataColumn firstName = iReply.Columns.Add("firstName", typeof(String));
                DataColumn desig = iReply.Columns.Add("desig", typeof(String));
                DataColumn addID = iReply.Columns.Add("addID", typeof(String));
                DataColumn sDate = iReply.Columns.Add("sDate", typeof(DateTime));
                DataColumn eDate = iReply.Columns.Add("eDate", typeof(DateTime));
                DataColumn uTime = iReply.Columns.Add("uTime", typeof(DateTime));
                String selData = "SELECT [asgID], [userID], [clntAddID], [sDate], [eDate], [uTime] FROM UserAssignment";
                Int32 id = 0, clntAddID = 0, clientID = 0;
                System.Guid usr = new System.Guid();
                String lName = String.Empty, fName = String.Empty, uDesig = String.Empty, clntAdd = String.Empty, uName = String.Empty;
                DateTime start = DateTime.Now, end = DateTime.Now, uT = DateTime.Now;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        id = readData.GetInt32(0);
                                        usr = readData.GetGuid(1);
                                        uName = USR.getUserNameFromId(usr);
                                        UP user = UP.GetUserProfile(uName);
                                        lName = user.LastName;
                                        fName = user.FirstName;
                                        uDesig = user.designation;
                                        clientID = Convert.ToInt32(user.clntID);
                                        clntAddID = readData.GetInt32(2);
                                        clntAdd = CLNT.getAddressTitle(clntAddID, clientID);
                                        start = readData.GetDateTime(3);
                                        end = readData.GetDateTime(4);
                                        uT = readData.GetDateTime(5);
                                        
                                            iReply.Rows.Add(id, lName, fName, uDesig, clntAdd, start, end, uT);
                                            iReply.AcceptChanges();
                                        
                                        iReply.DefaultView.Sort = "lastName";
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                return iReply;
            }
            catch(Exception ex)
            {throw new System.Exception(ex.ToString());}
        }

        public static System.Data.DataTable getUserOfficeAddress(System.Guid UserId, String ClientDBString)
        {
            try
            {
                System.Data.DataTable iReply = new System.Data.DataTable();
                DataColumn pAddID = iReply.Columns.Add("pAddID", typeof(Int32));
                DataColumn pAddTitle = iReply.Columns.Add("pAddTitle", typeof(String));
                DataColumn pAddSt1 = iReply.Columns.Add("pAddSt1", typeof(String));
                DataColumn pAddSt2 = iReply.Columns.Add("pAddSt2", typeof(String));
                DataColumn pAddSt3 = iReply.Columns.Add("pAddSt3", typeof(String));
                DataColumn pAddCity = iReply.Columns.Add("pAddCity", typeof(String));
                DataColumn pAddCounty = iReply.Columns.Add("pAddCounty", typeof(String));
                DataColumn pAddState = iReply.Columns.Add("pAddState", typeof(String));
                DataColumn pAddZip = iReply.Columns.Add("pAddZip", typeof(String));
                DataColumn pAddCountry = iReply.Columns.Add("pAddCountry", typeof(String));
                DataColumn pAddPh = iReply.Columns.Add("pAddPh", typeof(String));
                DataColumn pAddPhT = iReply.Columns.Add("pAddPhT", typeof(String));
                String selData = "SELECT [clntAddID] FROM [UserAssignment] WHERE [userID] = @userID";
                Int32 addID = 0;
                String selAdd = "SELECT [clntAddID], [title], [ctyID], [stID], [cntyID], [cityID], [zipID], [st1], [st2], [st3] FROM [ClientAddress] WHERE [clntAddID] = @clntAddID";
                Int32 ctID = 0, stID = 0, cnID = 0, ciID = 0, zpID = 0;
                String atyp = String.Empty, ctName = String.Empty, stName = String.Empty, cnName = String.Empty, ciName = String.Empty, zpName = String.Empty, st1 = String.Empty, st2 = String.Empty, st3 = String.Empty, ph = String.Empty, pTyp = String.Empty;
                using (SqlConnection dataCon = new SqlConnection(ClientDBString))
                {
                    try
                    {
                        dataCon.Open();
                        SqlCommand getData = new SqlCommand(selData, dataCon);
                        getData.Parameters.AddWithValue("@userID", SqlDbType.UniqueIdentifier).Value = UserId.ToString();
                        using (SqlDataReader readData = getData.ExecuteReader())
                        {
                            try
                            {
                                if (readData.HasRows)
                                {
                                    while (readData.Read())
                                    {
                                        addID = readData.GetInt32(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readData.Close();
                                readData.Dispose();
                            }
                        }                                                
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        dataCon.Close();
                        dataCon.Dispose();
                    }
                }
                using (SqlConnection usrCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["FEUsersDB"].ToString()))
                {
                    try
                    {
                    usrCon.Open();
                        SqlCommand getAdd = new SqlCommand(selAdd, usrCon);
                        getAdd.Parameters.AddWithValue("@clntAddID", SqlDbType.Int).Value = addID.ToString();
                        using (SqlDataReader readAdd = getAdd.ExecuteReader())
                        {
                            try
                            {
                                if (readAdd.HasRows)
                                {
                                    while (readAdd.Read())
                                    {
                                        atyp = readAdd.GetString(1);
                                        ctID = readAdd.GetInt32(2);
                                        ctName = DMG.getCountryName(ctID);
                                        stID = readAdd.GetInt32(3);
                                        stName = DMG.getStateName(stID);
                                        cnID = readAdd.GetInt32(4);
                                        cnName = DMG.getCountyName(cnID);
                                        ciID = readAdd.GetInt32(5);
                                        ciName = DMG.getCityName(ciID);
                                        zpID = readAdd.GetInt32(6);
                                        zpName = DMG.getZipCode(zpID);
                                        st1 = readAdd.GetString(7);
                                        st2 = readAdd.GetString(8);
                                        st3 = readAdd.GetString(9);
                                        iReply.Rows.Add(addID, atyp, st1, st2, st3, ciName, cnName, stName, zpName, ctName, "---", "---");
                                        iReply.AcceptChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            { throw new System.Exception(ex.ToString()); }
                            finally
                            {
                                readAdd.Close();
                                readAdd.Dispose();
                            }
                        }                    
                    }
                    catch (Exception ex)
                    { throw new System.Exception(ex.ToString()); }
                    finally
                    {
                        usrCon.Close();
                        usrCon.Dispose();
                    }
                }
                return iReply;
            }
            catch (Exception ex)
            { throw new System.Exception(ex.ToString()); }
        }
    }
}