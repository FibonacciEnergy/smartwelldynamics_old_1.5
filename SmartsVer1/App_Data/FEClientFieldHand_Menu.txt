﻿        <nav class="pull-left bg-black" role="navigation" style="width: 100%;">
            <div class="navbar-header">

                <ul class="nav navbar-nav" style="display: table; margin-top: 16px;">
                    <li class="active" style="vertical-align: middle; display: table-cell;"><a class="swdMenu" href="../../Viewer/crdViewer2.aspx">SMART Panel</a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle  swdMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">SMARTs <span class="caret">
                        </span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="../../Services/fecRawSMART.aspx" style="vertical-align: middle; display: table-cell;">SMARTs Data Audit/QC</a>
                            </li>
                            <li class="divider bg-black" role="separator"></li>
                            <!--<li><a href="../../Services/fesAzmCorr_2A.aspx" style="vertical-align: middle; display: table-cell;">SMARTs Azimuth Audit/QC with
                                Correction</a></li>-->
                            <!-- <li><a href="../../Services/fesRawCorr_2A.aspx" style="vertical-align: middle; display: table-cell;">SMARTs Multi-Station Analysis</a>
                            </li>
                            <li class="divider bg-black" role="separator"></li> -->
                            <li><a href="../../Services/fecDataReview_2A.aspx" style="vertical-align: middle; display: table-cell;">Well/Lateral Audit/QC Review</a>
                            </li>
                            <li><a href="../../Services/fecDataComparison.aspx" style="vertical-align: middle; display: table-cell;">Well/Lateral Comparison</a>
                            </li>
                        </ul>
                    </li>
               <!--     <li class="dropdown">
                        <a href="#" class="dropdown-toggle   swdMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Utilities <span class="caret">
                        </span></a>
                        <ul class="dropdown-menu" role="menu">
							<li><a href="../../Utilities/fecCRDMagStorm_2A.aspx" style="vertical-align: middle; display: table-cell;">Magnetic Storm Survey
                                Rectifier (MSSR)</a></li>                            
                            <li><a href="../../Utilities/fecCRDNonMag_2A.aspx" style="vertical-align: middle; display: table-cell;">BHA Non-Magnetic Spacing
                                Calculator</a></li>                            
                            <li class="divider bg-black" role="separator"></li>
                            <li><a href="../../WellProfile/fecWellProfile_2A.aspx" style="vertical-align: middle; display: table-cell;">Well Profile Generator</a>
                            </li>
                        </ul>
                    </li> -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle   swdMenu" role="menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Logs Management
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="../../LogPrint/feManageLogs_2A.aspx" style="vertical-align: middle; display: table-cell;">Manage Well/Lateral Logs</a>
                            </li>
                        <!--     <li class="divider bg-black" role="separator"></li>
                            <li><a href="../../LogPrint/fePrintOrderLogs_2A.aspx" style="vertical-align: middle; display: table-cell;">Order Log Prints</a>
                            </li>
                            <li><a href="../../LogPrint/feOrderTracker_2A.aspx" style="vertical-align: middle; display: table-cell;">Track Log Print Orders</a>
                            </li> -->
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle   swdMenu" role="menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Reports <span class="caret">
                        </span></a>
						<ul class="dropdown-menu" role="menu">
						<!--<li><a href="../../Reports/Assets/ServiceOrdersReport_2A.aspx" style="vertical-align: middle; display: table-cell;"> Service Order(s) Report</a></li>
						<li><a href="../../Reports/Assets/SQC_2A.aspx" style="vertical-align: middle; display: table-cell;"> Survey Qualification Criteria</a></li>
						<li><a href="../../Reports/Assets/Well_2A.aspx" style="vertical-align: middle; display: table-cell;"> Well(s) / Lateral(s) List</a></li>
						<li class="divider bg-black" role="separator"></li>-->
						<!--<li><a href="../../Reports/QC/DataInputReport_2A.aspx" style="vertical-align: middle; display: table-cell;"> Data Input Report</a></li>-->
						<li><a href="../../Reports/QC/QCReport_2A.aspx" style="vertical-align: middle; display: table-cell;"> Survey Data Audit/QC Report</a></li>
						<!--<li class="divider bg-black" role="separator"></li>
						<li><a href="../../Reports/Utilities/MSSRReport_2A.aspx" style="vertical-align: middle; display: table-cell;">Magnetic Storm Survey Rectifier Report</a></li>								
						<li><a href="../../Reports/Utilities/NonMagCalculator_2A.aspx" style="vertical-align: middle; display: table-cell;"> Non-Mag Spacing Calculator Reports</a></li>
						<li class="divider bg-black" role="separator"></li>
						<li><a href="../../Reports/WP/WPReport_2A.aspx" style="vertical-align: middle; display: table-cell;"> Well Profile Generator Reports</a></li>-->
						</ul>						
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle   swdMenu" role="menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Configuration <span
                            class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="../../Config/Assets/fecDRConfig.aspx" style="vertical-align: middle; display: table-cell;">Configuration</a></li>							
                            <li class="divider bg-black" role="separator"></li>
                        <!--     <li><a href="../../Config/Demogs/fecCRDDemogs_2A.aspx" style="vertical-align: middle; display: table-cell;">Demographics</a>
                            </li> 
                            <li><a href="../../Config/Geography/fecDRGeography_2A.aspx" style="vertical-align: middle; display: table-cell;">Geography</a>
                            </li>  -->
                            <li><a href="../../Config/Units/fecUnits_2A.aspx" style="vertical-align: middle; display: table-cell;">Measurement Units</a>
                            </li>
                            <li class="divider bg-black" role="separator"></li>
                        <!--    <li><a href="../../Config/Users/fecCRDHR_2A.aspx" style="vertical-align: middle; display: table-cell;">User Accounts</a></li>
                            <li><a href="../../Config/Users/fecDRHRAssign_2A.aspx" style="vertical-align: middle; display: table-cell;">User Assignments</a>
                            </li> -->
                            <li><a href="../../Account/fecCRDManage_2A.aspx" style="vertical-align: middle; display: table-cell;">Account Profile</a></li>
                            <li class="divider bg-black" role="separator"></li>
                            <!-- <li><a href="../../Config/Users/fecDRFHAssign_2A.aspx" style="vertical-align: middle; display: table-cell;">Fieldhands Schedule</a>
                            </li> 
                            <li class="divider bg-black" role="separator"></li> -->
                            <li><a href="../../Config/Clients/fecOpCo_2A.aspx" style="vertical-align: middle; display: table-cell;">Operator Companies</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle   swdMenu" role="menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Support <span class="caret">
                        </span></a>
                        <ul class="dropdown-menu   swdMenu" role="menu">
                            <li><a href="../../Support/FRequest_2A.aspx" style="vertical-align: middle; display: table-cell;">Support Request</a></li>
                        </ul>
                    </li>

                </ul>
            </div>
        </nav>