﻿using System;
using System.Web.Configuration;

namespace SmartsVer1
{
    public class AppConfiguration
    {
        public static string FromAddress
        {
            get
            {
                string result = WebConfigurationManager.AppSettings.Get("FromAddress");
                if (!string.IsNullOrEmpty(result))
                {
                    return result;
                }
                throw new Exception("AppSetting FromAddress not found in web.config file.");
            }
        }

        public static string ErrorFromAddress
        {
            get
            {
                string result = WebConfigurationManager.AppSettings.Get("ErrorFromAddress");
                if (!string.IsNullOrEmpty(result))
                {
                    return result;
                }
                throw new Exception("AppSetting ErrorFromAddress not found in web.config file.");
            }
        }

        public static string SupportFromAddress
        {
            get
            {
                string result = WebConfigurationManager.AppSettings.Get("SupportFromAddress");
                if (!string.IsNullOrEmpty(result))
                {
                    return result;
                }
                throw new Exception("AppSetting SupportFromAddress not found in web.config file.");
            }
        }

        public static string FromName
        {
            get
            {
                string result = WebConfigurationManager.AppSettings.Get("FromName");
                if (!string.IsNullOrEmpty(result))
                {
                    return result;
                }
                throw new Exception("AppSetting FromName not found in web.config file.");
            }
         }

        public static string ErrorFromName
        {
            get
            {
                string result = WebConfigurationManager.AppSettings.Get("ErrorFromName");
                if (!string.IsNullOrEmpty(result))
                {
                    return result;
                }
                throw new Exception("AppSetting SupportFromName not found in web.config file.");
            }
        }

        public static string SupportFromName
        {
            get
            {
                string result = WebConfigurationManager.AppSettings.Get("SupportFromName");
                if (!string.IsNullOrEmpty(result))
                {
                    return result;
                }
                throw new Exception("AppSetting SupportFromName not found in web.config file.");
            }
        }

        public static string ToAddress
        {
            get
            {
                string result = WebConfigurationManager.AppSettings.Get("ToAddress");
                if (!string.IsNullOrEmpty(result))
                {
                    return result;
                }
                throw new Exception("AppSetting ToAddress not found in web.config file.");
            }
        }

        public static string ErrorToAddress
        {
            get
            {
                string result = WebConfigurationManager.AppSettings.Get("ErrorToAddress");
                if (!string.IsNullOrEmpty(result))
                {
                    return result;
                }
                throw new Exception("AppSetting ToAddress not found in web.config file.");
            }
        }

        public static string SupportToAddress
        {
            get
            {
                string result = WebConfigurationManager.AppSettings.Get("SupportToAddress");
                if (!string.IsNullOrEmpty(result))
                {
                    return result;
                }
                throw new Exception("AppSetting SupportToAddress not found in web.config file.");
            }
        }

        public static string ToName
        {
            get
            {
                string result = WebConfigurationManager.AppSettings.Get("ToName");
                if (!string.IsNullOrEmpty(result))
                {
                    return result;
                }
                throw new Exception("AppSetting ToName not found in web.config file.");
            }
        }

        public static string ErrorToName
        {
            get
            {
                string result = WebConfigurationManager.AppSettings.Get("ErrorToName");
                if (!string.IsNullOrEmpty(result))
                {
                    return result;
                }
                throw new Exception("AppSetting ToName not found in web.config file.");
            }
        }

        public static string SupportToName
        {
            get
            {
                string result = WebConfigurationManager.AppSettings.Get("SupportToName");
                if (!string.IsNullOrEmpty(result))
                {
                    return result;
                }
                throw new Exception("AppSetting SupportToName not found in web.config file.");
            }
        }

        public static bool SendMailOnError
        {
            get
            {
                string result = WebConfigurationManager.AppSettings.Get("SendMailOnError");
                if (!string.IsNullOrEmpty(result))
                {
                    return Convert.ToBoolean(result);
                }
                throw new Exception("AppSetting SendMailOnError not found in web.config file.");
            }
        }
     }
}
